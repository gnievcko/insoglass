<?php
namespace customer\helpers;

use Yii;
use yii\helpers\Html;

class MenuHelper{
	
	public static function topMenuRender($menus){
		$menuString = '';
		if(!empty($menus)){
			
			foreach($menus as $menu){
				$menuString .= '<li>';
				$active = MenuHelper::isActive($menu);
				$menuString .= Html::a($menu['text'],  $menu['linkUrl'], array('id' => $menu['id'], 'class' => $active ? 'nav-active' : ''));
				$menuString .= '</li>';
			}
		}
		return $menuString;
	}
	public static function leftMenuRender($menus){
		$menuList = MenuHelper::leftMenuFind($menus);
		$menuString = '<ul class="nav navbar-nav">';
		if(!empty($menuList['menu'])){
			foreach($menuList['menu'] as $menu){
				if(!empty($menu['linkUrl'])){
					$active = MenuHelper::isActive($menu);
					$menuString .= '<li class="navbar-tile '.($active ? 'active' : '').'">';
					$menuString .= Html::a ($menu['icon'].'<span>'.$menu['text'].'</span>', $menu['linkUrl'], array (
							'id' => $menu['id'],
							'class' => ($active ? 'active' : ''),
					));
					if(!empty($menu['menu'])){
						$menuString .= MenuHelper::renderPopupMenu($menu['menu']);
					}
					$menuString .= '</li>';
				}
			}
		}
		$menuString .= '</ul>';
		
		return $menuString;
	}
	private static function renderPopupMenu($menus){
		$menuString ='';
		foreach($menus as $menu) {
			if(!empty($menu['linkUrl'])) {
				$menuString .= '<li>';
				$active = MenuHelper::isActive($menu);
				$menuString .= Html::a ($menu['icon'].'<span class="submenu-text">'.$menu['text'].'</span>', $menu['linkUrl'], array (
						'id' => $menu['id'],
						'class' => $active ? 'active' : ''
				));
				$menuString .= '</li>';
			}
		}
		if($menuString != '') {
			$menuString = '<ul class="more">'.$menuString .'</ul>';
		}
		else {
			$menuString = '';
		}
		
		
		return $menuString;
	}
	private static function leftMenuFind($menus){
		foreach($menus as $menu){
			$val = MenuHelper::isActive($menu);
			
			if($val){
				return $menu;
			}
		}
		return [];
	}
	private static function isActive($menu){
		if(!empty($menu)){
			$controllerName = Yii::$app->controller->id;
			$actionName = Yii::$app->controller->action->id;
			$list = MenuHelper::getAllLink($menu);
			foreach ($list as $row){
				if($row[0] == $controllerName && $row[1] == $actionName){
					return true;
				}
			}
		}
		return false;
	}
	private static function getAllLink($menu){
		$linkList = [];
		$linkList[] = !empty($menu['link']) ? $menu['link'] : ['',''] ;
		$array = !empty($menu['menu']) ? $menu['menu'] : [];
		if(!empty($array)){
			foreach($array as $link){
				$rLinkList = MenuHelper::getAllLink($link);
				$linkList = array_merge($linkList, $rLinkList);
			}
		}
		
		return $linkList;
	}

}