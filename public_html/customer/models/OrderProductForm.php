<?php

namespace customer\models;

use Yii;
use frontend\models\OrderProductForm as ProductForm;
use common\helpers\Utility;

class OrderProductForm extends ProductForm {
    
    public function attributeLabels() {
        $labels = parent::attributeLabels();
        $labels['remarks'] = \Yii::t('web', 'Product remarks');

        return $labels;
    }

    /**
     * @inheritdoc
     */
    public function rules() {
        return parent::rules();
    }

}

