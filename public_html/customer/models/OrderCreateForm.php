<?php

namespace customer\models;

use Yii;
use frontend\models\OrderCreateForm as CreateForm;
use common\helpers\StringHelper;

class OrderCreateForm extends CreateForm {

    public $client;

    public function attributeLabels() {
        $labels = parent::attributeLabels();
        $labels['client'] = Yii::t('web', 'Client');
        $labels['description'] = \Yii::t('web', 'Order description');
        $labels['title'] = StringHelper::translateOrderToOffer('web', 'Order name');//Yii::$app->params['isOrderTitleIsLabeledAsConcerns'] ? Yii::t('web', 'Concerns') : (Yii::$app->params['isOrderTitleLabeledAsOrderName'] ? Yii::t('web', 'Job name') : StringHelper::translateOrderToOffer('web', 'Order name'));
        $labels['descriptionCont'] = \Yii::t('web', 'Remarks');
        return $labels;
    }

    /**
     * @inheritdoc
     */
    public function rules() {
        $rules = parent::rules();
        $rules[] = [['client'], 'string', 'max' => 256];

        return $rules;
    }

}
