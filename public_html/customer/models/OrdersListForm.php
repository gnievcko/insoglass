<?php

namespace customer\models;

use Yii;
use yii\base\Model;
use common\helpers\StringHelper;

class OrdersListForm extends Model {

    public $page;
    public $orderNumber;
    public $orderNumberForClient;
    public $orderName;
    public $orderClientId;
    public $orderClientName;
    public $orderParentClientName;
    public $orderStatusesIds;
    public $cityName;
    public $dateFrom;
    public $dateTo;
    public $sortDir;
    public $sortField;
    public $orderType;
    public $orderResponsibleUser;

    const DEFAULT_PAGE = 0;
    const DEFAULT_SORT_DIR = 'desc';
    const DEFAULT_SORT_FIELD = 'create_date';

    public function init() {
        parent::init();
        $this->page = self::DEFAULT_PAGE;
        $this->sortDir = self::DEFAULT_SORT_DIR;
        $this->sortField = self::DEFAULT_SORT_FIELD;
    }

    public static function getSortFields() {
        $fields = [
            ['name' => 'number', 'label' => Yii::t('web', 'Number')],
            ['name' => 'orderNumberForClient', 'label' => Yii::t('main', 'Client number'), 'isHidden' => !Yii::$app->params['isNumberForClientVisible']],
            ['name' => 'name', 'label' => Yii::$app->params['isOrderTitleIsLabeledAsConcerns'] ? Yii::t('web', 'Concerns') : (Yii::$app->params['isOrderTitleLabeledAsOrderName'] ? Yii::t('web', 'Job name') : StringHelper::translateOrderToOffer('web', 'Order name'))],
            ['name' => 'create_date', 'label' => Yii::t('main', 'Creation date')],
            ['name' => 'date_shipping', 'label' => Yii::t('web', 'Date shipping')]

//            ['name' => 'client', 'label' => Yii::t('main', 'Client')],
                //['name' => 'modification_date', 'label' => Yii::t('main', 'Modification date'), 'style' => 'width: 8%'],
        ];
//        $contract_type = [['name' => 'contract_type', 'label' => Yii::t('main', 'Contract type'), 'style' => 'width: 8%']];
//        if(Yii::$app->params['isContractTypeVisible']) {
//            $fields = array_merge($fields, $contract_type);
//        }

        $fields[] = ['name' => 'user_responsible', 'label' => Yii::t('main', 'User Responsible'), 'isHidden' => true];
        $fields[] = ['name' => 'status', 'label' => Yii::t('main', 'Status')];


        return $fields;
    }

    public function attributeLabels() {
        return [
            'orderNumber' => StringHelper::translateOrderToOffer('web', 'Order number'),
            'orderNumberForClient' => Yii::t('main', 'Client number'),
            'orderName' => Yii::$app->params['isOrderTitleIsLabeledAsConcerns'] ? Yii::t('web', 'Concerns') : (Yii::$app->params['isOrderTitleLabeledAsOrderName'] ? Yii::t('web', 'Job name') : StringHelper::translateOrderToOffer('web', 'Order name')),
            'orderStatusesIds' => StringHelper::translateOrderToOffer('main', 'Order status'),
            'orderClientId' => Yii::t('main', 'Client'),
            'orderClientName' => Yii::t('web', 'Client (verbally)'),
            'orderParentClientName' => Yii::t('main', 'Parent company'),
            'orderType' => StringHelper::translateOrderToOffer('web', 'Order type'),
            'contractType' => StringHelper::translateOrderToOffer('web', 'Contract type'),
            'cityName' => Yii::t('web', 'City name or zip code'),
            'dateFrom' => Yii::t('web', 'Created after (inclusive)'),
            'dateTo' => Yii::t('web', 'Created before (inclusive)'),
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            [['page', 'orderClientId'], 'integer'],
            [['orderStatusesIds'], 'each', 'rule' => ['integer']],
            [['sortDir'], 'in', 'range' => ['asc', 'desc']],
            [['sortField'], 'in', 'range' => array_column(self::getSortFields(), 'name')],
            [['orderName', 'orderType', 'orderNumber', 'orderClientName', 'cityName', 'dateFrom', 'dateTo',
            'orderParentClientName' , 'orderNumberForClient'], 'safe'],
            ['page', 'default', 'value' => self::DEFAULT_PAGE],
            ['sortDir', 'default', 'value' => self::DEFAULT_SORT_DIR],
            ['sortField', 'default', 'value' => self::DEFAULT_SORT_FIELD],
        ];
    }
    
    public static function isHidden($name) {
        $fields = self::getSortFields();
        
        foreach ($fields as $field) {
            if($field['name'] == $name){
                return $field['isHidden'];
            } 
        }
        
        return false;
    }

}
