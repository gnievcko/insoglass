<?php

namespace customer\models;

use Yii;
use frontend\models\OrderCostForm as CostForm;

class OrderCostForm extends CostForm {

    public function attributeLabels() {
        $labels = parent::attributeLabels();
        $labels['remarks'] = \Yii::t('web', 'Product remarks');

        return $labels;
    }

    /**
     * @inheritdoc
     */
    public function rules() {
        return parent::rules();
    }
}

