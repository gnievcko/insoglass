<?php

namespace customer\models;

use common\components\MessageProvider;
use common\helpers\Utility;
use common\models\ar\Company;
use common\models\ar\Role;
use common\models\ar\Token;
use common\models\ar\TokenType;
use common\models\ar\User;
use common\models\ar\UserRole;
use console\controllers\RbacController;
use Yii;
use stdClass;
use yii\base\Model;
use common\models\ar\CompanyGroup;
use common\models\ar\CompanyGroupSet;
use common\models\ar\CompanyGroupTranslation;
use common\models\ar\Language;
use common\helpers\RandomSymbolGenerator;
use common\helpers\StrengthValidator;

class RegisterForm extends Model {

    public $firstName;
    public $lastName;
    public $email;
    public $companyName;
    public $password;
    public $confirmPassword;
    public $VAT_ID;
    public $representative;

    const SCENARIO_AJAX_VALIDATION = 'ajax-validation';

    public function attributeLabels() {
        return [
            'firstName' => Yii::t('main', 'First name'),
            'lastName' => Yii::t('main', 'Last name'),
            'email' => Yii::t('web', 'E-mail address'),
            'companyName' => Yii::t('web', 'Company name'),
            'password' => Yii::t('web', 'Password'),
            'confirmPassword' => Yii::t('web', 'Confirm password'),
            'VAT_ID' => Yii::t('web', 'VAT identification number'),
            'representative' => Yii::t('web', 'I represent a company'),
            
        ];
    }

    public function rules() {
        return [
            [['firstName', 'lastName', 'email', 'password', 'confirmPassword'], 'required'],
            ['password', 'compare', 'compareAttribute' => 'confirmPassword', 'enableClientValidation' => false, 'except' => self::SCENARIO_AJAX_VALIDATION],
            [['companyName', 'VAT_ID'], 'safe'],
            [['firstName', 'lastName'], 'string', 'max' => 32],
            ['representative', 'boolean'],
            [['VAT_ID'], 'validate_VAT_ID', 'skipOnEmpty' => false],
            ['email', 'unique', 'targetClass' => User::className(), 'targetAttribute' => 'email'],
            [['password'], StrengthValidator::className()] + (isset(Yii::$app->params['strengthPasswordConfig']) ? Yii::$app->params['strengthPasswordConfig'] : []),
        ];
    }

    public function validate_VAT_ID() {
        if($this->representative) {
            $normalized_VAT_ID = str_replace(['-', ' ', 'PL'], '', $this->VAT_ID);
            if ($normalized_VAT_ID === '') {
                $this->addError('VAT_ID', Yii::t('web', 'The VAT identification number must contain at least one digit!'));
            }
        }
    }

    public function register(&$flashMessage) {
        if(!$this->validate()) {
            return false;
        }

        $connection = Yii::$app->db;
        $connection->transaction(function () use (&$flashMessage) {
            $user = self::createUser($this->email, $this->password, $this->firstName, $this->lastName);

            $tags = [];
            if($this->representative) {
                $normalized_VAT_ID = str_replace(['-', ' ', 'PL'], '', $this->VAT_ID);
                if(!empty($this->VAT_ID) && !empty($company = Company::find()->where(["REPLACE(REPLACE(REPLACE(`vat_identification_number`, '-', ''), ' ', ''), 'PL', '')" => $normalized_VAT_ID])
                                ->one())) {
                    $template = Utility::EMAIL_TYPE_CUSTOMER_NEW_USER_EXISTING_COMPANY;
                } else {
                    $company = self::createCompany($this->companyName, $this->VAT_ID, false, $user->id, $user->email);
                    $template = Utility::EMAIL_TYPE_CUSTOMER_NEW_USER_NEW_COMPANY;

                    self::saveCompanyGroupSet($company);
                }
                $email = Yii::$app->params['adminEmail'];
                $tags['{full_name}'] = $this->firstName . ' ' . $this->lastName;
                $tags['{email}'] = $user->email;
                $tags['{company_name}'] = $company->name;
                $tags['{VAT_ID}'] = $company->vat_identification_number;
                $flashMessage = Yii::t('web', 'Wait until the system administrator verifies your personal data and activates your account.');
            } else {
                $company = self::createCompany($this->firstName . ' ' . $this->lastName, null, true, $user->id, $user->email);
                self::saveCompanyGroupSet($company);
                $template = Utility::EMAIL_TYPE_CUSTOMER_NEW_USER;
                $email = $user->email;
                $token = self::createToken($user->id);
                $activationLink = Yii::$app->urlManager->createAbsoluteUrl(['site/activate-account', 'token' => $token->token]);
                $tags['{activation_link}'] = $activationLink;
                $tags['{app_name}'] = Yii::$app->name;
                $flashMessage = Yii::t('web', 'An email with an activation link has been sent to the email address you supplied.');
            }
            $tags['{team_name}'] = Yii::$app->name;

            $user->company_id = $company->id;
            $save = $user->save();
            if(!$save) {
                throw new \Exception('Cannot update the user!');
            }

            $userRole = new UserRole();
            $userRole->user_id = $user->id;
            $userRole->role_id = Role::findOne(['symbol' => Utility::ROLE_CLIENT])->id;
            $save = $userRole->save();
            if(!$userRole->save()) {
                throw new \Exception('Cannot create a role for the user!');
            }

            $rbacController = new RbacController('rbac', Yii::$app->module);
            $rbacController->assignRoleToUser(Utility::ROLE_CLIENT, $user->id);

            $messageProvider = new MessageProvider();
            $tmp = new stdClass();
            $tmp->email = $email;
            $mode = MessageProvider::MODE_EMAIL;
            $sent = $messageProvider->send($tmp, NULL, $template, $mode, NULL, $tags, false);

            if(!$sent) {
                throw new \Exception('Cannot send an email!');
            }
        });

        return true;
    }

    private function saveCompanyGroupSet($company) {
        $group = new CompanyGroup();
        $group->symbol = RandomSymbolGenerator::generate(CompanyGroup::class);
        $group->is_predefined = 1;
        $group->save();

        $translation = $this->createTranslation($group);
        $translation->name = $this->companyName;
        $translation->save();

        $allDefaultGroupId = CompanyGroup::findOne(['symbol' => Utility::COMPANY_GROUP_DEFAULT])['id'];

        $companyGroupSet = new CompanyGroupSet();
        $companyGroupSet->company_group_id = $allDefaultGroupId;
        $companyGroupSet->company_id = $company['id'];
        $companyGroupSet->save();

        $companyGroupSet = new CompanyGroupSet();
        $companyGroupSet->company_group_id = $group->id;
        $companyGroupSet->company_id = $company['id'];
        $companyGroupSet->save();
    }

    private function createTranslation($group) {
        $translation = new CompanyGroupTranslation();
        $translation->company_group_id = $group->id;
        $translation->language_id = Language::find()->where(['symbol' => Yii::$app->language])->one()->id;

        return $translation;
    }

    private static function createToken($userID) {
        $token = new Token();
        $token->user_id = $userID;
        $token->token_type_id = TokenType::findOne(['symbol' => Utility::TOKEN_TYPE_CUSTOMER_ACTIVATE_ACCOUNT])->id;
        $token->date_expiration = date('Y-m-d H:i:s', time() + Yii::$app->params['customerTimeForAccountActivation']);
        do {
            $token->token = Yii::$app->security->generateRandomString();
        } while(!$token->validate(['token']));
        $save = $token->save();
        if(!$save) {
            throw new \Exception('Cannot create a token!');
        }
        return $token;
    }

    private static function createUser($email, $password, $firstName, $lastName) {
        $user = new User();
        $user->email = $email;
        $user->setPassword($password);
        $user->first_name = $firstName;
        $user->last_name = $lastName;
        $user->is_active = 1;
        $user->contact_email = $email;
        $user->can_logged = 0;
        $save = $user->save();
        if(!$save) {
            throw new \Exception('Cannot create a user!');
        }
        return $user;
    }

    private static function createCompany($name, $VAT_ID, $isArtificial, $userID, $email) {
        $company = new Company();
        $company->name = $name;
        $company->email = $email; // TODO: Olek: Kuba się nad tym zastanawiał ;)
        $company->vat_identification_number = $VAT_ID;
        $company->is_artificial = $isArtificial ? 1 : 0;
        $company->is_active = 1;
        $company->user_id = $userID;
        $save = $company->save();
        if(!$save) {
            throw new \Exception('Cannot create a company!');
        }
        return $company;
    }

}
