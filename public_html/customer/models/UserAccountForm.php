<?php

namespace customer\models;

use Yii;
use yii\base\Model;
use yii\db\Query;
use yii\web\UploadedFile;
use yii\helpers\FileHelper;
use common\helpers\UploadHelper;
use common\components\UploadTrait;
use yii\helpers\Url;
use frontend\helpers\AddressHelper;
use common\helpers\StrengthValidator;

class UserAccountForm extends Model{
    
    use UploadTrait;
    
    const SCENARIO_SHOW = 'show';
    const SCENARIO_EDIT = 'edit';
    
    public $id;
    public $email;
    public $firstName;
    public $lastName;
    public $phone1;
    public $phone2;
    public $urlPhoto;
    public $password;
    public $currentPassword;
    public $newPassword;
    public $newPasswordRepeated;
    public $filePhoto;
    public $user;
    public $deletePhoto;
    
    public $address;
    public $city;
    public $zipCode;
    public $provinceId;
    
    public $postalAddress;
    public $postalCity;
    public $postalZipCode;
    public $postalProvinceId;
    
    public $position;
    
    public function __construct($user = null, $config = []) {
        if(!empty($user)) {
            $this->loadData($user);
        }
        
        parent::__construct($config);
    }
    
    public function rules() {
        return [
            [['email'], 'required'],
            [['email'], 'email'],
            [['email'], 'validateEmailUnique'],
            [['id'], 'integer'],
            [['email', 'password', 'address', 'city', 'postalAddress', 'postalCity', 'postalZipCode', 'postalProvinceId'], 'string', 'max' => 64],
            [['firstName', 'lastName', 'phone1', 'phone2'], 'string', 'max' => 32],
            [['urlPhoto', 'position'], 'string', 'max' => 256],
            [['filePhoto'], 'file', 'skipOnEmpty' => true, 'extensions' => 'png, jpg, jpeg, gif'],
            [['currentPassword','newPassword','newPasswordRepeated'], 'validatePassword'],
            [['provinceId'], 'integer'],
            [['zipCode'], 'string', 'max' => 10],
            [['address', 'city', 'zipCode', 'provinceId'], 'validateAddress'],
            [['postalAddress', 'postalCity', 'postalZipCode', 'postalProvinceId'], 'validateShippingAddress'],
            [['position'], 'default', 'value' => null],
            [['newPassword'], StrengthValidator::className()]+Yii::$app->params['strengthPasswordConfig']
        ];
    }
    
    public function validateAddress($attribute, $params) {
        $sum = !empty($this->address) + !empty($this->city) + !empty($this->zipCode) + !empty($this->provinceId);
        if($sum > 0 && $sum < 4) {
            $this->addError('address', Yii::t('web', 'Address is incomplete.'));
            $this->addError('city', '');
            $this->addError('zipCode', '');
            $this->addError('provinceId', '');
        }
    }
    
    public function validateShippingAddress($attribute, $params) {
        $sum = !empty($this->postalAddress) + !empty($this->postalCity) + !empty($this->postalZipCode) + !empty($this->postalProvinceId);
        if($sum > 0 && $sum < 4) {
            $this->addError('postalAddress', Yii::t('web', 'Address is incomplete.'));
            $this->addError('postalCity', '');
            $this->addError('postalZipCode', '');
            $this->addError('postalProvinceId', '');
        }
    }
    
    public function attributeLabels() {
        return [
            'id' => Yii::t('main', 'ID'),
            'email' => Yii::t('main', 'E-mail address'),
            'password' => Yii::t('main', 'Password'),
            'firstName' => Yii::t('main', 'First name'),
            'lastName' => Yii::t('main', 'Last name'),
            'phone1' => Yii::t('main', 'Phone'),
            'phone2' => Yii::t('main', 'Alternative phone'),
            'urlPhoto' => Yii::t('main', 'Photo'),
            'note' => Yii::t('main', 'Note'),
            'filePhoto' => Yii::t('main', 'Photo'),
            'newPassword' => Yii::t('web', 'New password'),
            'newPasswordRepeated' => Yii::t('web', 'Repeat new password'),
            'currentPassword' => Yii::t('web', 'Current password'),
            'address' => Yii::t('main', 'Address'),
            'city' => Yii::t('main', 'City'),
            'zipCode' => Yii::t('main', 'Zip code'),
            'provinceId' => Yii::t('main', 'Voivodeship'),
            'postalAddress' => Yii::t('main', 'Address'),
            'postalCity' => Yii::t('main', 'City'),
            'postalZipCode' => Yii::t('main', 'Zip code'),
            'postalProvinceId' => Yii::t('main', 'Voivodeship'),
            'position' => Yii::t('web', 'Position'),
        ];
    }
    
    public function loadData($user) {
        $this->id = $user->id;
        $this->email = $user->email;
        $this->firstName = $user->first_name;
        $this->lastName = $user->last_name;
        $this->phone1 = $user->phone1;
        $this->phone2 = $user->phone2;
        
        $this->urlPhoto = $user->url_photo;
        $this->password = $user->password;
        $this->user = $user;
        $this->position = $user->position;
        
        if(!empty($user->company)) {
            $address = $user->company->address;
            $postalAddress = $user->company->addressPostal;
        }
        
        if(!empty($address)) {
            $this->address = $address->main.(!empty($address->complement) ? ' '.$address->complement : '');
            $this->city = $address->city->name;
            $this->zipCode = $address->city->zip_code;
            
            if(!empty($address->city->province)) {
                $this->provinceId = $address->city->province->id;
            }
        }
        
        if(!empty($postalAddress)) {
            $this->postalAddress = $postalAddress->main.(!empty($postalAddress->complement) ? ' '.$postalAddress->complement : '');
            $this->postalCity = $postalAddress->city->name;
            $this->postalZipCode = $postalAddress->city->zip_code;
            
            if(!empty($postalAddress->city->province)) {
                $this->postalProvinceId = $postalAddress->city->province->id;
            }
        }
        
    }
    
    public function saveData() {
        $transaction = Yii::$app->db->beginTransaction();
        try {
            $this->user->first_name = $this->firstName;
            $this->user->last_name = $this->lastName;
            $this->user->email = $this->email;
            $this->user->phone1 = $this->phone1;
            $this->user->phone2 = $this->phone2;
            
            $this->user->position = $this->position;
            
            $this->user->company->address_id = AddressHelper::getAddressId([
                'address' => $this->address, 'city' => $this->city,
                'zipCode' => $this->zipCode, 'provinceId' => $this->provinceId
            ]);
            
            $this->user->company->address_postal_id = AddressHelper::getAddressId([
                'address' => $this->postalAddress, 'city' => $this->postalCity,
                'zipCode' => $this->postalZipCode, 'provinceId' => $this->postalProvinceId
            ]);
            
            if(!empty($this->newPassword)){
                $this->user->setPassword($this->newPassword);
            }
            
            $this->loadModelFiles();
            
            $this->user->url_photo = $this->urlPhoto;
            $this->user->company->save();
            $this->user->save();
            
            $transaction->commit();
        }
        catch(\Exception $e) {
            $transaction->rollBack();
            throw $e;
            throw new \Exception('Could not save user data');
        }
    }
    
    public function scenarios() {
        $scenarios = parent::scenarios();
        $scenarios[self::SCENARIO_SHOW] = ['name', 'email', 'phone1', 'phone2', 'firstName', 'lastName', 'photoUrl',
            'address', 'city', 'zipCode', 'provinceId', 'postalAddress', 'postalCity', 'postalZipCode', 'postalProvinceId','position',
        ];
        $scenarios[self::SCENARIO_EDIT] = ['id', 'name', 'email', 'phone1', 'phone2', 'firstName', 'lastName',
            'address', 'city', 'zipCode', 'provinceId', 'postalAddress', 'postalCity', 'postalZipCode', 'postalProvinceId', 'position',
            'photoUrl', 'newPassword', 'newPasswordRepeated', 'currentPassword', 'deletePhoto'
        ];
        return $scenarios;
    }
    
    public function setShowScenario() {
        $this->setScenario(self::SCENARIO_SHOW);
    }
    
    public function setEditScenario() {
        $this->setScenario(self::SCENARIO_EDIT);
    }
    
    public function isShowScenarioSet() {
        return $this->getScenario() == self::SCENARIO_SHOW;
    }
    
    public function isEditScenarioSet() {
        return $this->getScenario() == self::SCENARIO_EDIT;
    }
    
    public function validatePassword($attribute, $params) {
        if(!empty($this->currentPassword) || !empty($this->newPassword) || !empty($this->newPasswordRepeated)) {
            if(!empty($this->currentPassword)) {
                if(!$this->user || !$this->user->validatePassword($this->currentPassword)) {
                    $this->addError('currentPassword', Yii::t('web', 'Incorrect current password'));
                }
            }
            else {
                $this->addError('currentPassword', Yii::t('web', 'Incorrect current password'));
            }
            if(empty($this->newPassword)) {
                $this->addError('newPassword', Yii::t('web', 'New password is empty'));
            }
            if($this->newPassword !== $this->newPasswordRepeated) {
                $this->addError('newPasswordRepeated', Yii::t('web', 'New password must be repeated correctly'));
            }
            
        }
    }
    
    public function validateEmailUnique(){
        $query = (new Query())
        ->select(['id', 'email'])
        ->from('user')
        ->where('email = :email', [':email' => $this->email]);
        
        if ($this->isEditScenarioSet()) {
            $query->andWhere('id != :id', [':id' => $this->id]);
        }
        
        if ($query->count() != 0) {
            $this->addError('email', Yii::t('web', 'E-mail must be unique.'));
        }
    }
    private function loadModelFiles() {
        $this->filePhoto = UploadedFile::getInstance($this, 'filePhoto');
        
        $newPhoto = $this->loadNewPhoto();
        
        $oldPhotoUrl = $this->urlPhoto;
        $needToRemoveOldPhoto = ($newPhoto !== false) || ($this->deletePhoto == 1);
        if($newPhoto !== false) {
            $this->urlPhoto = Url::to('@web/storage/index?f=' . basename($newPhoto));
        }
        
        if($needToRemoveOldPhoto) {
            $this->removePhotoFromDisk($oldPhotoUrl);
            $this->urlPhoto = ($newPhoto !== false) ? $this->urlPhoto : null;
        }
    }
    
    private function loadNewPhoto() {
        if(!empty($this->filePhoto) && ($filePhoto = $this->uploadFile('filePhoto')) !== false) {
            return $filePhoto;
        }
        else {
            return false;
        }
    }
    
    private function removePhotoFromDisk($photoUrl) {
        if(!empty($photoUrl)) {
            $oldPhotoRelativePath = preg_replace('|^'.Url::to('@web/storage/index\?f='.'|'), '', $photoUrl);
            $oldFile = FileHelper::normalizePath(UploadHelper::getUploadPath().'/'.$oldPhotoRelativePath);
            @unlink($oldFile);
        }
    }
    
}