<?php

namespace customer\models;

use Yii;
use frontend\helpers\ModelLoaderHelper;
use customer\models\OrderCreateForm;
use customer\models\OrderProductForm;
use customer\models\OrderCostForm;
use frontend\models\PhotoForm;
use frontend\models\DocumentForm;
use common\models\ar\Order;
use common\models\ar\OrderGroup;
use common\models\ar\OrderStatus;
use frontend\models\ProductAttributeForm;
use frontend\models\ArtificialProductAttributeForm;
use common\models\ar\OfferedProduct;
use common\models\ar\OrderHistory;
use common\models\ar\Language;
use common\models\aq\OrderQuery;
use common\helpers\Utility;
use common\helpers\RandomSymbolGenerator;
use common\helpers\UploadHelper;
use yii\helpers\Url;
use yii\base\Model;
use common\helpers\NumberTemplateHelper;
use common\alerts\UnchangedNewOrderAlertHandler;
use common\models\ar\OrderAttachment;
use common\models\ar\OrderHistoryPhoto;
use yii\helpers\Json;
use common\models\aq\OrderHistoryQuery;
use common\models\aq\OrderUserRepresentativeQuery;
use common\models\ar\Company;
use common\models\ar\Entity;
use frontend\logic\ProductionTaskLogic;
use common\models\ar\OrderPriority;
use common\helpers\ChangeHelper;
use common\models\ar\OrderModificationHistory;
use common\models\ar\OrderUserRepresentative;
use common\models\aq\OrderOfferedProductQuery;
use frontend\models\OrderHistoryChangeTracker;
use yii\db\Query;

class OrderModel {

    const SCENARIO_CREATE = 'SCENARIO_CREATE';
    const SCENARIO_STATUS_UPDATE = 'SCENARIO_STATUS_UPDATE';
    const SCENARIO_QUICK_STATUS_UPDATE = 'SCENATIO_QUICK_STSTUS_UPDATE';
    const SCENARIO_COPY = self::SCENARIO_CREATE;
    const SCENARIO_EDIT = 'SCENARIO_EDIT';
    const SCENARIO_UPDATE_EDIT = 'SCENARIO_UPDATE_EDIT';
    const SCENARIO_STATUS_UPDATE_CONT = 'SCENARIO STATUS_UPDATE_CONT';

    private $scenario;
    private $order;
    private $orderCreateForm;
    private $orderHistoryForm;
    private $orderProductsForms = [];
    private $photosForms = [];
    private $documentsForms = [];
    private $orderCostsForms = [];
    private $oldPhotosForms = [];
    private $oldDocumentsForms = [];
    private $orderCostsSummary = ['netValue' => '0.00', 'vatAmount' => '0.00', 'grossValue' => '0.00'];
    private $orderProductsSummary = ['netValue' => '0.00', 'vatAmount' => '0.00', 'grossValue' => '0.00'];
    private $orderCostsVats = [];
    private $orderProductsVats = [];
    private $productAttributeForms = [];
    private $artificialProductAttributeForms = [];
    private $historyChangeTracker;

    public function __construct($scenario, $orderId = null, $companyId = null) {
        $this->scenario = $scenario;
        if($orderId === null) {
            $this->initializeEmptyForms($companyId);
        } else if($scenario == self::SCENARIO_QUICK_STATUS_UPDATE) {
            $this->order = Order::find()->where(['id' => $orderId])->one();
            $this->restoreOrderHistoryForm();
            $this->restoreProductsForms();
            $this->restoreCostsForms();
            //$this->restoreAttributeForms();
        } else {
            $this->restoreStateFromDatabase($orderId);
        }
    }

    public function setParentOrderId($parentOrderId) {
        $this->order->parent_order_id = $parentOrderId;
    }

    private function initializeEmptyForms($companyId = null) {
        $this->orderCreateForm = new OrderCreateForm();

        $this->orderHistoryForm = new OrderHistoryForm();
        $this->orderProductsForms = [];
        $this->orderCostsForms = [new OrderCostForm()];

        $this->orderCreateForm->number = NumberTemplateHelper::getNextNumber(Utility::NUMBER_TEMPLATE_ORDER);

        if(!empty(Yii::$app->user->identity->company_id)) {
            $company = Company::findOne(Yii::$app->user->identity->company_id);
            $this->orderCreateForm->clientId = Yii::$app->user->identity->company_id;
            $this->orderCreateForm->client = isset($company->name) ? $company->name : '';
        }

        if(Yii::$app->params['isProductAttributesVisible']) {
            $this->productAttributeForms = [new ProductAttributeForm()];
            $this->artificialProductAttributeForms = [new ArtificialProductAttributeForm()];
        }
        $this->orderCreateForm->entityId = Entity::find()->one()->id;
    }

    private function restoreStateFromDatabase($orderId) {
        $this->order = Order::find()->where(['id' => $orderId])->one();

        $this->restoreOrderCreateForm();
        $this->restoreProductsForms();
        $this->restorePhotosForms();
        $this->restoreDocumentsForms();
        $this->restoreCostsForms();
        $this->restoreOrderHistoryForm();
        $this->restoreAttributeForms();
    }

    private function restoreOrderCreateForm() {
        $this->orderCreateForm = new OrderCreateForm();
        $this->orderCreateForm->title = $this->order->title;
        $this->orderCreateForm->entityId = $this->order->entity_id;
        $this->orderCreateForm->number = $this->order->number;
        $this->orderCreateForm->numberForClient = $this->order->number_for_client;
        $this->orderCreateForm->userResponsibleId = $this->order->user_responsible_id;
        $this->orderCreateForm->description = $this->order->description;
        $this->orderCreateForm->descriptionNote = $this->order->description_note;
        $this->orderCreateForm->descriptionCont = $this->order->description_cont;
        $this->orderCreateForm->clientId = $this->order->company_id;
        $this->orderCreateForm->parentOrderId = $this->order->parent_order_id;

        if(!empty($this->orderCreateForm->clientId)) {
            $company = Company::findOne($this->orderCreateForm->clientId);
            $this->orderCreateForm->client = isset($company->name) ? $company->name : '';
        }
        $representatives = OrderUserRepresentativeQuery::getUserRepresentativesByOrderId($this->order->id);
        if(!empty($representatives)) {
            foreach($representatives as $rep) {
                $this->orderCreateForm->contactPeopleIds[] = $rep['id'];
            }
        }

        $this->orderCreateForm->orderId = $this->order->id;
        $this->orderCreateForm->shippingDate = $this->order->orderHistoryLast->date_shipping;
        $this->orderCreateForm->isInvoiced = $this->order->is_invoiced;
        $this->orderCreateForm->durationTime = $this->order->duration_time;
        $this->orderCreateForm->executionTime = $this->order->execution_time;
        $this->orderCreateForm->paymentTerms = $this->order->payment_terms;
        $this->orderCreateForm->contractTypeId = $this->order->contract_type_id;
        $this->orderCreateForm->isTemplate = $this->order->is_template;

        if(Yii::$app->params['isProductionVisible']) {
            $orderPriority = OrderPriority::findOne(['id' => $this->order->order_priority_id]);
            if(!empty($orderPriority) && $orderPriority->symbol == Utility::ORDER_PRIORITY_HIGH) {
                $this->orderCreateForm->priorityCheckbox = true;
            } else if(!empty($orderPriority) && $orderPriority->symbol == Utility::ORDER_PRIORITY_MEDIUM) {
                $this->orderCreateForm->priorityCheckbox = false;
                //TODO wiem, że bez sensu ale nie wiem o co chodzi.. Bartek
            } else {
                $this->orderCreateForm->priorityCheckbox = false;
            }
        }

        if(Yii::$app->params['isOrderTypeVisible']) {
            $this->orderCreateForm->orderTypeIds = array_column($this->order->orderTypes, 'id');
        } else {
            $this->orderCreateForm->orderTypeIds = null;
        }
    }

    private function restoreProductsForms() {
        $orderProducts = OrderQuery::getOrderProducts($this->order->id);
        $this->orderProductsForms = array_map(function($orderProduct) {
            $orderProductForm = new OrderProductForm();
            $orderProductForm->id = $orderProduct['id'];
            $orderProductForm->productId = $orderProduct['productId'];
            $orderProductForm->price = $orderProduct['price'];
            $orderProductForm->currencyId = $orderProduct['currency_id'];
            $orderProductForm->count = $orderProduct['count'];
            $orderProductForm->productType = $orderProduct['type'];
            $orderProductForm->vatRate = $orderProduct['vatRate'];
            $orderProductForm->unit = $orderProduct['unit'];
            $orderProductForm->height = $orderProduct['height'];
            $orderProductForm->width = $orderProduct['width'];
            $orderProductForm->constructionId = $orderProduct['construction_id'];
            $orderProductForm->muntinId = $orderProduct['muntin_id'];
            $orderProductForm->shapeId = $orderProduct['shape_id'];
            $orderProductForm->remarks = $orderProduct['remarks'];
            $orderProductForm->remarks2 = $orderProduct['remarks2'];
            $orderProductForm->ralColourId = $orderProduct['ralColourId'];
            $orderProductForm->ralRgbHash = $orderProduct['ralRgbHash'];
            $orderProductForm->ralColourSymbol = $orderProduct['ralColourSymbol'];
            $orderProductForm->urlPhoto = $orderProduct['urlPhoto'];
            $orderProductForm->heightPhoto = $orderProduct['heightPhoto'];
            $orderProductForm->widthPhoto = $orderProduct['widthPhoto'];
            $orderProductForm->topPhoto = $orderProduct['topPhoto'];
            $orderProductForm->leftPhoto = $orderProduct['leftPhoto'];
            $orderProductForm->restoreFiles();
            return $orderProductForm;
        }, $orderProducts);
        $this->calculateSummary($this->orderProductsForms, $this->orderProductsSummary, $this->orderProductsVats);
    }

    private function restoreCostsForms() {
        $costs = OrderQuery::getCosts($this->order->id);

        $this->orderCostsForms = array_map(function($cost) {
            $costForm = new OrderCostForm();
            $costForm->id = $cost['offered_product_id'];
            $costForm->costId = $cost['id'];
            $costForm->name = $cost['name'];
            $costForm->price = $cost['price'];
            $costForm->currencyId = $cost['currency_id'];
            $costForm->count = $cost['count'];
            $costForm->vatRate = $cost['vatRate'];
            $costForm->unit = $cost['unit'];
            $costForm->height = $cost['height'];
            $costForm->width = $cost['width'];
            $costForm->constructionId = $cost['constructionId'];
            $costForm->muntinId = $cost['muntinId'];
            $costForm->shapeId = $cost['shapeId'];
            $costForm->remarks = $cost['remarks'];
            $costForm->remarks2 = $cost['remarks2'];
            $costForm->ralColourId = $cost['ralColourId'];
            $costForm->ralRgbHash = $cost['ralRgbHash'];
            $costForm->ralColourSymbol = $cost['ralColourSymbol'];
            $costForm->urlPhoto = $cost['urlPhoto'];
            $costForm->heightPhoto = $cost['heightPhoto'];
            $costForm->widthPhoto = $cost['widthPhoto'];
            $costForm->topPhoto = $cost['topPhoto'];
            $costForm->leftPhoto = $cost['leftPhoto'];
            $costForm->restoreFiles();
            return $costForm;
        }, $costs);
        $this->calculateSummary($this->orderCostsForms, $this->orderCostsSummary, $this->orderCostsVats);
    }

    private function restorePhotosForms() {
        switch($this->scenario) {
            case self::SCENARIO_COPY: {
                    $this->restorePhotosAsNewFiles();
                    break;
                }
            default: {
                    $this->restorePhotosAsReadOnly();
                }
        }
    }

    private function restorePhotosAsNewFiles() {
        $this->photosForms = array_map(function($orderPhoto) {
            $photoForm = new PhotoForm();
            $photoForm->image = UploadHelper::getFileNameByUrl($orderPhoto['urlPhoto']);
            $photoForm->thumbnail = UploadHelper::getFileNameByUrl($orderPhoto['urlThumbnail']);
            $photoForm->makeCopy = true;
            $photoForm->description = $orderPhoto['description'];

            return $photoForm;
        }, OrderQuery::getPhotos($this->order->id));
    }

    private function restorePhotosAsReadOnly() {
        $this->oldPhotosForms = array_map(function($orderPhoto) {
            $photoForm = new PhotoForm();
            $photoForm->image = $orderPhoto['urlPhoto'];
            $photoForm->thumbnail = $orderPhoto['urlThumbnail'];
            $photoForm->description = $orderPhoto['description'];

            return $photoForm;
        }, OrderQuery::getPhotos($this->order->id));
    }

    private function restoreDocumentsForms() {
        switch($this->scenario) {
            case self::SCENARIO_COPY: {
                    $this->restoreDocumentsAsNewFiles();
                    break;
                }
            default: {
                    $this->restoreDocumentsAsReadOnly();
                }
        }
    }

    private function restoreDocumentsAsNewFiles() {
        $this->documentsForms = array_map(function($orderDocument) {
            $documentForm = new DocumentForm();
            $documentForm->document = UploadHelper::getFileNameByUrl($orderDocument['urlFile']);
            $documentForm->name = $orderDocument['name'];
            $documentForm->makeCopy = true;

            return $documentForm;
        }, OrderQuery::getAttachments($this->order->id));
    }

    private function restoreDocumentsAsReadOnly() {
        $this->oldDocumentsForms = array_map(function($orderDocument) {
            $documentForm = new DocumentForm();
            $documentForm->id = $orderDocument['id'];
            $documentForm->document = $orderDocument['urlFile'];
            $documentForm->name = $orderDocument['name'];

            return $documentForm;
        }, OrderQuery::getAttachments($this->order->id));
    }

    private function restoreOrderHistoryForm() {
        $this->orderHistoryForm = new OrderHistoryForm();
        $this->orderHistoryForm->orderHistoryId = $this->order->orderHistoryLast->id;
        $this->orderHistoryForm->orderStatusId = $this->order->orderHistoryLast->order_status_id;
        $this->orderHistoryForm->dateDeadline = $this->order->orderHistoryLast->date_deadline;
        $this->orderHistoryForm->shippingDate = $this->order->orderHistoryLast->date_shipping;
        $this->orderHistoryForm->dateReminder = $this->order->orderHistoryLast->date_reminder;
    }

    private function restoreAttributeForms() {
        foreach($this->order->orderOfferedProducts as $orderOfferedProduct) {
            if($orderOfferedProduct->is_active) {
                if(!empty($orderOfferedProduct->offeredProduct)) {
                    if($orderOfferedProduct->offeredProduct->is_artificial) {
                        $productAttributeForm = new ArtificialProductAttributeForm();
                        $productAttributeForm->loadFromObjects($orderOfferedProduct->orderOfferedProductAttributes);
                        $this->artificialProductAttributeForms[] = $productAttributeForm;
                    } else {
                        $productAttributeForm = new ProductAttributeForm();
                        $productAttributeForm->loadFromObjects($orderOfferedProduct->orderOfferedProductAttributes);
                        $this->productAttributeForms[] = $productAttributeForm;
                    }
                }
            }
        }
    }

    public function load($data) {
        if($this->scenario != self::SCENARIO_QUICK_STATUS_UPDATE) {
            $this->orderCreateForm = ModelLoaderHelper::loadOne($data, OrderCreateForm::class);

            if(empty($this->orderCreateForm->client)) {
                $company = Company::findOne($this->orderCreateForm->clientId);
                $this->orderCreateForm->client = isset($company->name) ? $company->name : '';
            }
            $this->orderProductsForms = ModelLoaderHelper::loadMultiple($data, OrderProductForm::class);
            $this->photosForms = ModelLoaderHelper::loadMultiple($data, PhotoForm::class);
            $this->documentsForms = ModelLoaderHelper::loadMultiple($data, DocumentForm::class);
            $this->orderCostsForms = ModelLoaderHelper::loadMultiple($data, OrderCostForm::class);
        }
        $this->orderHistoryForm = ModelLoaderHelper::loadOne($data, OrderHistoryForm::class);

        if(isset($this->order) && isset($this->order->orderHistoryLast)) {
            $this->orderHistoryForm->oldOrderStatusId = $this->order->orderHistoryLast->order_status_id;
        }

        if(Yii::$app->params['isProductAttributesVisible']) {
            $this->productAttributeForms = ModelLoaderHelper::loadMultiple($data, ProductAttributeForm::class);
            $this->artificialProductAttributeForms = ModelLoaderHelper::loadMultiple($data, ArtificialProductAttributeForm::class);
        }
    }

    public function save() {
        if($this->validate()) {
            $this->persist();
            return true;
        } else {
            return false;
        }
    }

    private function persist() {
        $transaction = \Yii::$app->db->beginTransaction();
        try {
            switch($this->scenario) {
                case self::SCENARIO_CREATE: {
                        $this->saveNewOrder();
                        break;
                    }

                case self::SCENARIO_STATUS_UPDATE: {
                        $this->updateStatus();
                        break;
                    }

                case self::SCENARIO_QUICK_STATUS_UPDATE: {
                        $this->quickUpdateStatus();
                        break;
                    }

                case self::SCENARIO_UPDATE_EDIT: {
                        $this->updateEdit();
                        $this->edit();
                        break;
                    }

                case self::SCENARIO_STATUS_UPDATE_CONT: {
                        $this->updateHistory();
                        $this->updateEdit();
                        break;
                    }

                case self::SCENARIO_EDIT: {
                        $this->edit();
                        break;
                    }
            }

            $transaction->commit();
        } catch(\Exception $e) {
            $transaction->rollback();
            throw $e;
        }
    }

    private function saveNewOrder() {
        $orderGroup = $this->createOrderGroup();
        $this->order = $this->saveOrder($orderGroup);

        $orderHistory = $this->initializeOrderHistory($this->order);

        $this->deactivateOrderProductsAndCosts($orderHistory);
        $this->assignProductsToOrder($orderHistory);
        $this->saveCosts($orderHistory);

        if(Yii::$app->params['isProductAttributesVisible']) {
            if(!empty($this->productAttributeForms)) {
                $productIds = OrderOfferedProductQuery::getAllByHistoryId($orderHistory->id, ['isArtificial' => false]);
                $this->saveAttributes($productIds, 'productAttributeForms');
            }
            if(!empty($this->artificialProductAttributeForms)) {
                $productArtificialIds = OrderOfferedProductQuery::getAllByHistoryId($orderHistory->id, ['isArtificial' => true]);
                $this->saveAttributes($productArtificialIds, 'artificialProductAttributeForms');
            }
        }

        if(Yii::$app->params['isVisualisationVisible']) {
            if(!empty($this->productAttributeForms)) {
                $productIds = OrderOfferedProductQuery::getAllByHistoryId($orderHistory->id, ['isArtificial' => false]);
                $this->saveImgToVisualisation($productIds, 'productAttributeForms', 'orderProductsForms');
            }
            if(!empty($this->artificialProductAttributeForms)) {
                $productArtificialIds = OrderOfferedProductQuery::getAllByHistoryId($orderHistory->id, ['isArtificial' => true]);
                $this->saveImgToVisualisation($productArtificialIds, 'artificialProductAttributeForms', 'orderCostsForms');
            }
        }

        $this->saveDocuments($orderHistory);
        $this->savePhotos($orderHistory);
    }

    private function updateStatus() {
        $this->order->is_invoiced = $this->orderCreateForm->isInvoiced;

        $oldHistoryId = $this->order->order_history_last_id;
        $newHistory = $this->addNewHistory();

        $oldOfferedProducts = OrderOfferedProductQuery::getAllByHistoryId($this->order->order_history_last_id);

        if(Yii::$app->params['isChangeOfOrderHistoryTracked']) {
            $this->historyChangeTracker = new OrderHistoryChangeTracker();
            $this->historyChangeTracker->initialize($newHistory);
        }

        $this->deactivateOrderProductsAndCosts($newHistory);
        $this->assignProductsToOrder($newHistory);
        $this->saveCosts($newHistory);



        if(\Yii::$app->params['isProductionVisible']) {
            $oldOfferedProducts = OrderOfferedProductQuery::getAllByHistoryId($oldHistoryId);
            $newOfferedProducts = OrderOfferedProductQuery::getAllByHistoryId($newHistory->id);
            ProductionTaskLogic::updateOrderOfferedProductIds($oldOfferedProducts, $newOfferedProducts);
        }


        if(Yii::$app->params['isProductAttributesVisible']) {
            if(!empty($this->productAttributeForms)) {
                $productIds = OrderOfferedProductQuery::getAllByHistoryId($newHistory->id, ['isArtificial' => false]);
                $this->saveAttributes($productIds, 'productAttributeForms');
            }
            if(!empty($this->artificialProductAttributeForms)) {
                $productIds = OrderOfferedProductQuery::getAllByHistoryId($newHistory->id, ['isArtificial' => true]);
                $this->saveAttributes($productIds, 'artificialProductAttributeForms');
            }
        }

        if(Yii::$app->params['isVisualisationVisible']) {
            if(!empty($this->productAttributeForms)) {
                $productIds = OrderOfferedProductQuery::getAllByHistoryId($newHistory->id, ['isArtificial' => false]);
                $this->saveImgToVisualisation($productIds, 'productAttributeForms', 'orderProductsForms');
            }
            if(!empty($this->artificialProductAttributeForms)) {
                $productArtificialIds = OrderOfferedProductQuery::getAllByHistoryId($newHistory->id, ['isArtificial' => true]);
                $this->saveImgToVisualisation($productArtificialIds, 'artificialProductAttributeForms', 'orderCostsForms');
            }
        }

        $this->generateTasksForProduction($newHistory);
        $this->saveDocuments($newHistory);
        $this->savePhotos($newHistory);

        $this->deactivateIfInTerminalState($newHistory);

        UnchangedNewOrderAlertHandler::check(['orderId' => $newHistory->order_id]);
        $this->activeIfInNotTerminalState($newHistory);

        $this->orderHistoryForm->orderHistoryId = $newHistory->id;

        if(Yii::$app->params['isChangeOfOrderHistoryTracked']) {
            $this->historyChangeTracker->compareAndSave();
        }
    }

    private function updateEdit() {
        $orderHistory = OrderHistory::findOne($this->orderHistoryForm->orderHistoryId);
        if(empty($orderHistory)) {
            $orderHistory = $this->order->orderHistoryLast;
        }

        $this->deactivateOrderProductsAndCosts($orderHistory);
        $this->assignProductsToOrder($orderHistory);
        $this->saveCosts($orderHistory);
        //TODO remove file 
        $this->moveToTmp();

        $this->saveDocuments($orderHistory);
        $this->savePhotos($orderHistory);
    }

    private function moveToTmp() {
        $documents = OrderAttachment::findAll(['order_id' => $this->order->id]);
        foreach($documents as $document) {
            try {
                preg_match('/\?f=(.*)/', $document->url_file, $matches);
                $documentName = $matches[1];

                foreach($this->documentsForms as $documentForm) {
                    if($documentName == $documentForm->document) {
                        continue 2;
                    }
                }

                unlink(\common\helpers\UploadHelper::getUploadPath() . '/' . $documentName);
            } catch(Exception $ex) {
                
            }
        }

        $historyPhotos = OrderHistoryPhoto::findAll(['order_history_id' => $this->order->orderHistoryLast->id]);
        foreach($historyPhotos as $historyPhoto) {
            try {
                preg_match('/\?f=(.*)/', $historyPhoto->url_photo, $matchesPhoto);
                preg_match('/\?f=(.*)/', $historyPhoto->url_thumbnail, $matchesThumbnail);
                $photo = $matchesPhoto[1];
                $thumbnail = $matchesThumbnail[1];

                foreach($this->photosForms as $photoForm) {
                    if($photo == $photoForm->image) {
                        continue 2;
                    }
                }

                unlink(\common\helpers\UploadHelper::getUploadPath() . '/' . $photo);
                unlink(\common\helpers\UploadHelper::getUploadPath() . '/' . $thumbnail);
            } catch(Exception $ex) {
                
            }
        }

        OrderAttachment::deleteAll(['order_id' => $this->order->id]);
        OrderHistoryPhoto::deleteAll(['order_history_id' => $this->order->orderHistoryLast->id]);
    }

    private function deactivateOrderProductsAndCosts($orderHistory) {
        \Yii::$app->db->createCommand()->update('order_offered_product', ['is_active' => 0], ['order_id' => $orderHistory->order_id])->execute();
    }

    private function quickUpdateStatus() {
        $newHistory = $this->addNewHistory();

        $this->deactivateOrderProductsAndCosts($newHistory);
        $this->assignProductsToOrder($newHistory);
        $this->saveCosts($newHistory);

        if(Yii::$app->params['isChangeOfOrderHistoryTracked']) {
            $this->historyChangeTracker = new OrderHistoryChangeTracker();
            $this->historyChangeTracker->initialize($newHistory);
        }

        $this->deactivateIfInTerminalState($newHistory);

        UnchangedNewOrderAlertHandler::check(['orderId' => $newHistory->order_id]);
        $this->activeIfInNotTerminalState($newHistory);

        $oldHistory = OrderHistoryQuery::getPreviousOrderHistory($newHistory->id, $this->order->id);


        if(\Yii::$app->params['isProductionVisible']) {
            $oldOfferedProducts = OrderOfferedProductQuery::getAllByHistoryId($oldHistory['id']);
            $newOfferedProducts = OrderOfferedProductQuery::getAllByHistoryId($newHistory->id);
            ProductionTaskLogic::updateOrderOfferedProductIds($oldOfferedProducts, $newOfferedProducts);
        }

        if(Yii::$app->params['isChangeOfOrderHistoryTracked']) {
            $this->historyChangeTracker->compareAndSave();
        }

        $this->generateTasksForProduction($newHistory);
    }

    private function edit() {
        $this->order->company_id = $this->orderCreateForm->clientId;
        $this->order->entity_id = $this->orderCreateForm->entityId;
        $this->order->parent_order_id = $this->orderCreateForm->parentOrderId;

        $tempOldRepresentatives = OrderUserRepresentative::find()->where(['order_id' => $this->order->id])->all();
        $oldRepresentatives = [];
        if(!empty($tempOldRepresentatives)) {
            $oldRepresentatives = array_column($tempOldRepresentatives, 'user_representative_id');
        }

        $this->assignUserRepresentativesToOrder($this->order);

        $this->order->title = $this->orderCreateForm->title;
        $this->order->description = $this->orderCreateForm->description;
        $this->order->description_note = $this->orderCreateForm->descriptionNote;
        $this->order->description_cont = $this->orderCreateForm->descriptionCont;

        $this->order->is_invoiced = intval($this->orderCreateForm->isInvoiced);
        $this->order->is_template = intval($this->orderCreateForm->isTemplate);

        $this->order->execution_time = !empty($this->orderCreateForm->executionTime) ? $this->orderCreateForm->executionTime : \Yii::t('web', '2-3 weeks');
        $this->order->duration_time = !empty($this->orderCreateForm->durationTime) ? $this->orderCreateForm->durationTime : \Yii::t('web', 'One month');
        $this->order->payment_terms = !empty($this->orderCreateForm->paymentTerms) ? $this->orderCreateForm->paymentTerms : \Yii::t('web', 'To be arranged');

        if(Yii::$app->params['isProductionVisible']) {
            $this->order->order_priority_id = OrderPriority::findOne([
                        'symbol' => $this->orderCreateForm->priorityCheckbox ? Utility::ORDER_PRIORITY_HIGH : Utility::ORDER_PRIORITY_MEDIUM
                    ])->id;
        }

        $this->assignOrderTypesToOrder($this->order);

        $this->order->contract_type_id = $this->orderCreateForm->contractTypeId;
        $this->tryToSaveModel($this->order, $oldRepresentatives);
    }

    private function addNewHistory() {
        $orderHistory = new OrderHistory();
        $orderHistory->order_id = $this->order->id;
        $orderHistory->user_id = \Yii::$app->user->identity->id;
        $orderHistory->is_client_entry = 0;
        $orderHistory->order_status_id = $this->orderHistoryForm->orderStatusId;
        $orderHistory->date_deadline = $this->orderHistoryForm->dateDeadline;
        $orderHistory->date_shipping = $this->orderHistoryForm->shippingDate;
        $orderHistory->date_reminder = $this->orderHistoryForm->dateReminder;
        $orderHistory->message = $this->orderHistoryForm->message;
        $orderHistory->save();

        $this->order->order_history_last_id = $orderHistory->id;
        $this->order->save();
        return $orderHistory;
    }

    private function generateTasksForProduction($orderHistory) {
        $orderStatus = OrderStatus::findOne($orderHistory->order_status_id);
        if(Yii::$app->params['isProductionVisible'] && empty($this->order->is_tasks_generated) && $orderStatus->symbol == Utility::ORDER_STATUS_IN_PROGRESS && (!empty($this->order->activeOrderOfferedProducts))) {
            $productionTaskLogic = new ProductionTaskLogic();
            $productionTaskLogic->generateTasks($this->order->id);
            $this->order->is_tasks_generated = 1;
            $this->order->save();
        }
    }

    private function updateHistory() {
        $orderHistory = OrderHistory::findOne($this->orderHistoryForm->orderHistoryId);
        if(empty($orderHistory)) {
            $orderHistory = $this->order->orderHistoryLast;
        }

        $orderHistory->order_status_id = $this->orderHistoryForm->orderStatusId;
        $orderHistory->date_deadline = $this->orderHistoryForm->dateDeadline;
        $orderHistory->date_shipping = $this->orderHistoryForm->shippingDate;
        $orderHistory->date_reminder = $this->orderHistoryForm->dateReminder;
        $orderHistory->message = $this->orderHistoryForm->message;
        $orderHistory->save();

        $this->deactivateIfInTerminalState($orderHistory);
        UnchangedNewOrderAlertHandler::check(['orderId' => $orderHistory->order_id]);
        $this->activeIfInNotTerminalState($orderHistory);
    }

    private function createOrderGroup() {
        $orderGroup = new OrderGroup();
        $orderGroup->number = null;
        $this->tryToSaveModel($orderGroup);

        return $orderGroup;
    }

    private function saveOrder($orderGroup) {
        $numberResult = NumberTemplateHelper::getAndSaveNextNumber($this->orderCreateForm->number, Utility::NUMBER_TEMPLATE_ORDER);

        $order = new Order();

        $order->company_id = $this->orderCreateForm->clientId;
        $order->entity_id = $this->orderCreateForm->entityId;

        $order->order_group_id = $orderGroup->id;
        $order->number = $numberResult['number'];
        $order->number_for_client = $this->orderCreateForm->numberForClient;
        $order->user_responsible_id = $this->orderCreateForm->userResponsibleId;
        $order->title = $this->orderCreateForm->title;
        $order->description = $this->orderCreateForm->description;
        $order->description_note = $this->orderCreateForm->descriptionNote;
        $order->description_cont = $this->orderCreateForm->descriptionCont;
        $order->user_id = \Yii::$app->user->identity->id;
        $order->is_active = 1;
        $order->parent_order_id = $this->orderCreateForm->parentOrderId;
        $order->is_template = intval($this->orderCreateForm->isTemplate);

        $order->execution_time = !empty($this->orderCreateForm->executionTime) ? $this->orderCreateForm->executionTime : \Yii::t('web', '2-3 weeks');
        $order->duration_time = !empty($this->orderCreateForm->durationTime) ? $this->orderCreateForm->durationTime : \Yii::t('web', 'One month');
        $order->payment_terms = !empty($this->orderCreateForm->paymentTerms) ? $this->orderCreateForm->paymentTerms : \Yii::t('web', 'To be arranged');
        $order->contract_type_id = $this->orderCreateForm->contractTypeId;

        if(Yii::$app->params['isProductionVisible']) {
            $order->order_priority_id = OrderPriority::findOne([
                        'symbol' => $this->orderCreateForm->priorityCheckbox ? Utility::ORDER_PRIORITY_HIGH : Utility::ORDER_PRIORITY_MEDIUM
                    ])->id;
        }

        $this->tryToSaveModel($order);
        $this->assignUserRepresentativesToOrder($order);
        $this->assignOrderTypesToOrder($order);

        if(!empty($numberResult['isDifferent'])) {
            \Yii::$app->getSession()->setFlash('info', \Yii::t('web', 'Number of the order would be {number} because of concurrent operations.', ['number' => $numberResult['number']]));
        }

        return $order;
    }

    private function initializeOrderHistory($order) {
        $orderHistory = new OrderHistory();
        $orderHistory->order_id = $order->id;
        $orderHistory->user_id = $order->user_id;
        $orderHistory->is_client_entry = 0;
        $orderHistory->order_status_id = OrderStatus::find()->where(['symbol' => 'new'])->one()->id;
        $orderHistory->date_shipping = $this->orderCreateForm->shippingDate;
        $this->tryToSaveModel($orderHistory);

        $order->order_history_first_id = $order->order_history_last_id = $orderHistory->id;
        $this->tryToSaveModel($order);

        return $orderHistory;
    }

    private function assignUserRepresentativesToOrder($order) {
        \Yii::$app->db->createCommand()->delete('order_user_representative', ['order_id' => $order->id])->execute();

        if(!empty($this->orderCreateForm->contactPeopleIds)) {
            \Yii::$app->db->createCommand()->batchInsert('order_user_representative', ['order_id', 'user_representative_id'], array_map(function($userId) use($order) {
                        return [$order->id, $userId];
                    }, $this->orderCreateForm->contactPeopleIds ?: []))->execute();
        }
    }

    private function assignOrderTypesToOrder($order) {
        \Yii::$app->db->createCommand()->delete('order_type_set', ['order_id' => $order->id])->execute();

        if(!empty($this->orderCreateForm->orderTypeIds)) {
            \Yii::$app->db->createCommand()->batchInsert('order_type_set', ['order_id', 'order_type_id'], array_map(function($orderTypeId) use($order) {
                        return [$order->id, $orderTypeId];
                    }, $this->orderCreateForm->orderTypeIds ?: []))->execute();
        }
    }

    /**
     * 
     * @param OrderHistory $orderHistory
     */
    private function assignProductsToOrder($orderHistory) {
        \Yii::$app->db->createCommand()->batchInsert('order_offered_product', ['order_id', 'order_history_id', 'count', 'price', 'currency_id', 'is_active', 'product_id', 'offered_product_id', 'vat_rate', 'unit',
            'width', 'height', 'construction_id', 'shape_id', 'muntin_id', 'remarks', 'remarks2', 'ral_colour_id'
                ], array_map(function($orderProductForm) use($orderHistory) {
                    $productId = ($orderProductForm->productType === Utility::PRODUCT_TYPE_PRODUCT) ? $orderProductForm->productId : null;
                    $offeredProductId = ($orderProductForm->productType === Utility::PRODUCT_TYPE_OFFERED_PRODUCT) ? $orderProductForm->productId : null;

                    return [$orderHistory->order_id, $orderHistory->id, $orderProductForm->count,
                        $orderProductForm->price, $orderProductForm->currencyId, 1, $productId, $offeredProductId, $orderProductForm->vatRate, $orderProductForm->unit,
                        $orderProductForm->width, $orderProductForm->height, $orderProductForm->constructionId, $orderProductForm->shapeId, $orderProductForm->muntinId,
                        $orderProductForm->remarks, $orderProductForm->remarks2, $orderProductForm->ralColourId,
                    ];
                }, ModelLoaderHelper::skipEmpty($this->orderProductsForms)))->execute();
        if(Yii::$app->params['isAttachmentForProductVisible']) {
            $productIds = $orderHistory->getOrderOfferedProducts()->asArray()->all();
            $this->saveProductAttachment($productIds, $this->orderProductsForms);
        }
    }

    private function saveAttributes($products, $variable) {
        if(empty($products)) {
            return;
        }

        foreach($this->{$variable} as $key => $productAttributeForm) {
            $arrayObjects = $productAttributeForm->convertFieldsToArrayObjects();
            \Yii::$app->db->createCommand()->delete('order_offered_product_attribute', ['order_offered_product_id' => $products[$key]['id']])->execute();
            \Yii::$app->db->createCommand()->update('order_offered_product', ['attribute_value' => null], ['id' => $products[$key]['id']])->execute();
            if(!empty($arrayObjects)) {
                \Yii::$app->db->createCommand()
                        ->batchInsert('order_offered_product_attribute', ['order_offered_product_id', 'product_attribute_type_id', 'value'], array_map(function($obj) use($key, $products) {
                                    return [$products[$key]['id'], $obj['product_attribute_type_id'], $obj['value']];
                                }, $arrayObjects))->execute();

                \Yii::$app->db->createCommand()->update('order_offered_product', ['attribute_value' => $productAttributeForm->convertFieldsToJson()], ['id' => $products[$key]['id']])->execute();
            }
        }
    }

    private function saveImgToVisualisation($products, $variable, $form) {
        if(empty($products)) {
            return;
        }

        foreach($this->{$variable} as $key => $productAttributeForm) {
//            \Yii::$app->db->createCommand()->delete('order_visualisation_photo', ['order_offered_product_id' => $products[$key]['id']])->execute();

            if(!empty($this->{$form}[$key]['urlPhoto'])) {
                \Yii::$app->db->createCommand()->insert('order_visualisation_photo', [
                    'order_offered_product_id' => $products[$key]['id'],
                    'url_photo' => $this->{$form}[$key]['urlPhoto'],
                    'height' => intval($this->{$form}[$key]['heightPhoto']),
                    'width' => intval($this->{$form}[$key]['widthPhoto']),
                    'top' => intval($this->{$form}[$key]['topPhoto']),
                    'left' => intval($this->{$form}[$key]['leftPhoto'])
                ])->execute();
            }
        }
    }

    private function saveDocuments($orderHistory) {
        $currentUserId = \Yii::$app->user->identity->id;
        $filesToSave = [];
        $copies = [];

        foreach($this->documentsForms as $documentForm) {
            if($documentForm->makeCopy) {
                $copy = ['hash' => UploadHelper::copy($documentForm->document), 'name' => $documentForm->name];
                $copies[] = $copy;
                $filesToSave[] = $copy;
            } else {
                $filesToSave[] = [
                    'hash' => $documentForm->document,
                    'name' => $documentForm->name,
                    'description' => $documentForm->description
                ];
            }
        }

        \Yii::$app->db->createCommand()->batchInsert('file_temporary_storage', ['file_hash', 'original_file_name'], array_map(function($copy) {
                    return ['file_hash' => $copy['hash'], 'original_file_name' => $copy['name']];
                }, $copies))->execute();

        \Yii::$app->db->createCommand()->batchInsert('order_attachment', ['order_id', 'order_history_id', 'url_file', 'name', 'user_id', 'description'], array_map(function($fileToSave) use($orderHistory, $currentUserId) {
                    return [$orderHistory->order_id, $orderHistory->id, Url::to(['storage/index', 'f' => $fileToSave['hash']], true), $fileToSave['name'], $currentUserId, $fileToSave['description']];
                }, $filesToSave))->execute();

        \Yii::$app->db->createCommand()->delete('file_temporary_storage', ['in', 'file_hash', array_column($filesToSave, 'hash')])->execute();
    }

    private function savePhotos($orderHistory) {
        $currentUserId = \Yii::$app->user->identity->id;
        $filesToSave = [];
        $copies = [];

        foreach($this->photosForms as $photoForm) {
            if($photoForm->makeCopy) {
                $imageCopy = UploadHelper::copy($photoForm->image);
                $thumbnailCopy = UploadHelper::copy($photoForm->thumbnail);
                $copies[] = ['hash' => $imageCopy, 'name' => $imageCopy];
                $copies[] = ['hash' => $thumbnailCopy, 'name' => $thumbnailCopy];
                $filesToSave[] = ['imageHash' => $imageCopy, 'thumbnailHash' => $thumbnailCopy, 'description' => $photoForm->description];
            } else {
                $filesToSave[] = ['imageHash' => $photoForm->image, 'thumbnailHash' => $photoForm->thumbnail, 'description' => $photoForm->description];
            }
        }

        \Yii::$app->db->createCommand()->batchInsert('file_temporary_storage', ['file_hash', 'original_file_name'], array_map(function($copy) {
                    return ['file_hash' => $copy['hash'], 'original_file_name' => $copy['name']];
                }, $copies))->execute();

        $savedFilesHashes = [];
        \Yii::$app->db->createCommand()->batchInsert('order_history_photo', ['order_history_id', 'user_id', 'url_photo', 'url_thumbnail', 'description'], array_map(function($fileToSave) use($orderHistory, $currentUserId, $savedFilesHashes) {
                    $savedFilesHashes[] = $fileToSave['imageHash'];
                    $savedFilesHashes[] = $fileToSave['thumbnailHash'];

                    return [
                        $orderHistory->id,
                        $currentUserId,
                        Url::to(['storage/index', 'f' => $fileToSave['imageHash']], true),
                        Url::to(['storage/index', 'f' => $fileToSave['thumbnailHash']], true),
                        $fileToSave['description'],
                    ];
                }, $filesToSave))->execute();

        \Yii::$app->db->createCommand()->delete('file_temporary_storage', ['in', 'file_hash', $savedFilesHashes])->execute();
    }

    private function saveCosts($orderHistory) {
        $translationsToInsert = [];
        $translationsLanguageId = Language::find()->where(['symbol' => \Yii::$app->language])->one()->id;
        $orderCostsToInsert = [];
        $userId = \Yii::$app->user->identity->id;

        foreach($this->orderCostsForms as $key => $orderCostForm) {
            if(!empty($orderCostForm->currencyId)) {
                $cost = new OfferedProduct();
                $cost->symbol = RandomSymbolGenerator::generate(OfferedProduct::class, 'symbol', 32);
                $cost->is_artificial = 1;
                $cost->is_active = 1;
                $cost->is_available = 0;
                $cost->user_id = $userId;
                $this->tryToSaveModel($cost);

                $translationsToInsert[] = [$cost->id, $translationsLanguageId, $orderCostForm->name, $orderCostForm->price, $orderCostForm->currencyId];

                $orderCostsToInsert[] = [$orderHistory->order_id, $cost->id, $orderHistory->id, $orderCostForm->count,
                    $orderCostForm->price, $orderCostForm->currencyId, 1, $orderCostForm->vatRate, $orderCostForm->unit,
                    $orderCostForm->width, $orderCostForm->height, $orderCostForm->constructionId, $orderCostForm->shapeId,
                    $orderCostForm->muntinId, $orderCostForm->remarks, $orderCostForm->remarks2, $orderCostForm->ralColourId,
                ];
            } else {
                unset($this->orderCostsForms[$key]);
            }
        }

        \Yii::$app->db->createCommand()->batchInsert('offered_product_translation', ['offered_product_id', 'language_id', 'name', 'price', 'currency_id'], array_map(function($translation) {
                    return $translation;
                }, $translationsToInsert))->execute();

        \Yii::$app->db->createCommand()->batchInsert('order_offered_product', ['order_id', 'offered_product_id', 'order_history_id', 'count', 'price', 'currency_id', 'is_active', 'vat_rate', 'unit',
            'width', 'height', 'construction_id', 'shape_id', 'muntin_id', 'remarks', 'remarks2', 'ral_colour_id',
                ], array_map(function($orderCost) {
                    return $orderCost;
                }, $orderCostsToInsert))->execute();


        if(Yii::$app->params['isAttachmentForProductVisible']) {
            $productIds = [];
            $last = Yii::$app->db->lastInsertID;
            for($i = $last; $i <= $last + count($this->orderCostsForms) - 1; $i++) {
                $productIds[] = ['id' => $i];
            }
            $this->saveProductAttachment($productIds, $this->orderCostsForms);
        }
    }

    private function deactivateIfInTerminalState($orderHistory) {
        $currentStatus = OrderStatus::find()->where(['id' => $orderHistory->order_status_id])->one();
        if($currentStatus->is_terminal) {
            $this->order->is_active = 0;
            $this->order->save();
        }
    }

    private function activeIfInNotTerminalState($orderHistory) {
        $currentStatus = OrderStatus::find()->where(['id' => $orderHistory->order_status_id])->one();
        if(!$currentStatus->is_terminal) {
            $this->order->is_active = 1;
            $this->order->save();
        }
    }

    public function getInvalidForms() {
        $invalidForms = [];
        if($this->orderCreateForm->hasErrors()) {
            $invalidForms[] = $this->orderCreateForm;
        }
        if($this->orderHistoryForm->hasErrors()) {
            $invalidForms[] = $this->orderHistoryForm;
        }

        return $invalidForms + ModelLoaderHelper::getErrorForms($this->orderProductsForms) + ModelLoaderHelper::getErrorForms($this->photosForms) + ModelLoaderHelper::getErrorForms($this->documentsForms) + ModelLoaderHelper::getErrorForms($this->orderCostsForms);
    }

    private function validate() {
        switch($this->scenario) {
            case self::SCENARIO_CREATE:
                return $this->validateForCreate();
            case self::SCENARIO_STATUS_UPDATE:
                return $this->validateForStatusUpdate();
            case self::SCENARIO_QUICK_STATUS_UPDATE:
                return $this->validateForQuickStatusUpdate();
            case self::SCENARIO_UPDATE_EDIT:
                return $this->validateForUpdateEdit();
            case self::SCENARIO_STATUS_UPDATE_CONT:
                return $this->validateForStatusUpdateCont();
            case self::SCENARIO_EDIT:
                return $this->validateForEdit();
        }
    }

    private function validateForCreate() {
        $result = $this->orderCreateForm->validate() && Model::validateMultiple($this->orderProductsForms) && Model::validateMultiple($this->photosForms) && Model::validateMultiple($this->documentsForms) && Model::validateMultiple($this->orderCostsForms);

        if(Yii::$app->params['isProductAttributesVisible']) {
            $result = $result && Model::validateMultiple($this->productAttributeForms) && Model::validateMultiple($this->artificialProductAttributeForms);
        }

        return $result;
    }

    private function validateForStatusUpdate() {
        $result = Model::validateMultiple($this->orderProductsForms) && Model::validateMultiple($this->photosForms) && Model::validateMultiple($this->documentsForms) && Model::validateMultiple($this->orderCostsForms) && $this->orderHistoryForm->validate();

        if(Yii::$app->params['isProductAttributesVisible']) {
            $result = $result && Model::validateMultiple($this->productAttributeForms) && Model::validateMultiple($this->artificialProductAttributeForms);
        }

        return $result;
    }

    private function validateForStatusUpdateCont() {
        return Model::validateMultiple($this->orderProductsForms) && Model::validateMultiple($this->photosForms) && Model::validateMultiple($this->documentsForms) && Model::validateMultiple($this->orderCostsForms) && $this->orderHistoryForm->validate();
    }

    private function validateForUpdateEdit() {
        return $this->orderCreateForm->validate() && Model::validateMultiple($this->orderProductsForms) && Model::validateMultiple($this->photosForms) && Model::validateMultiple($this->documentsForms) && Model::validateMultiple($this->orderCostsForms);
    }

    private function validateForQuickStatusUpdate() {
        return $this->orderHistoryForm->validate();
    }

    private function validateForEdit() {
        return $this->orderCreateForm->validate();
    }

    private function validateForUpdate() {
        return $this->orderCreateForm->validate();
    }

    public function getAllForms() {

        $result = [
            'orderCreateForm' => $this->orderCreateForm,
            'orderProductsForms' => $this->orderProductsForms,
            'photosForms' => $this->photosForms,
            'documentsForms' => $this->documentsForms,
            'orderCostsForms' => $this->orderCostsForms,
            'oldPhotosForms' => $this->oldPhotosForms,
            'oldDocumentsForms' => $this->oldDocumentsForms,
            'orderHistoryForm' => $this->orderHistoryForm,
            'orderCostsSummary' => $this->orderCostsSummary,
            'orderProductsSummary' => $this->orderProductsSummary,
            'orderCostsVats' => $this->orderCostsVats,
            'orderProductsVats' => $this->orderProductsVats,
        ];

        if(Yii::$app->params['isProductAttributesVisible']) {
            $result['productAttributeForms'] = $this->productAttributeForms;
            $result['artificialProductAttributeForms'] = $this->artificialProductAttributeForms;
        }

        return $result;
    }

    public function getOrderCreateForm() {
        return $this->orderCreateForm;
    }

    public function getOrderProductsForms() {
        return $this->orderProductsForms;
    }

    public function getPhotosForms() {
        return $this->photosForms;
    }

    public function getDocumentsForms() {
        return $this->documentsForms;
    }

    public function getOrderCostsForms() {
        return $this->orderCostsForms;
    }

    public function getOrderHistoryForm() {
        return $this->orderHistoryForm;
    }

    public function getOrderCostsSummary() {
        return $this->orderCostsSummary;
    }

    public function getOrderProductsSummary() {
        return $this->orderProductsSummary;
    }

    public function getOrderCostsVats() {
        return $this->orderCostsVats;
    }

    public function getOrderProductsVats() {
        return $this->orderProductsVats;
    }

    public function getProductAttributeForms() {
        return $this->productAttributeForms;
    }

    public function getArtificialProductAttributeForms() {
        return $this->artificialProductAttributeForms;
    }

    private function tryToSaveModel($model, $oldRepresentatives = null) {
        if(Yii::$app->params['isChangeOfOrderHistoryTracked']) {
            $changeHelper = new ChangeHelper($model, 'edit', ['order_history_first_id', 'order_history_last_id']);

            $changeHelper->addAttribute(
                    'contact_people', !empty($oldRepresentatives) ? $oldRepresentatives : [], !empty($this->orderCreateForm->contactPeopleIds) ? $this->orderCreateForm->contactPeopleIds : []
            );
        }
        if(!$model->save()) {
            throw new \Exception("Model {$model->formName()} could not be saved. Errors: " . print_r($model->getErrors(), true));
        }
        if(Yii::$app->params['isChangeOfOrderHistoryTracked']) {
            $changeHelper->saveChanges(null, new OrderModificationHistory($model->id));
        }
    }

    public function getOrder() {
        return $this->order;
    }

    public static function calculateSummary(&$data, &$outTotal, &$outVats) {
        $vats = [
            '' => self::makeVatRow(Yii::t('documents', 'Vat-free'), '0.00'),
            'NP' => self::makeVatRow(Yii::t('documents', 'Not applicable'), '0.00'),
            '0.23' => self::makeVatRow('23 %', '0.23'),
            '0.08' => self::makeVatRow('8 %', '0.08'),
            '0' => self::makeVatRow('0 %', '0.00'),
        ];
        $total = ['netValue' => '0.00', 'vatAmount' => '0.00', 'grossValue' => '0.00'];
        foreach($data as &$product) {
            if(isset($product->vatRate)) {
                $vatRate = $product->vatRate;
            } else {
                $vatRate = $product['vatRate'];
            }
            if(isset($product->price)) {
                $price = $product->price;
            } else {
                $price = $product['price'];
            }
            if(isset($product->count)) {
                $count = $product->count;
            } else {
                $count = $product['count'];
            }
            if(isset($product->netValue)) {
                $netValue = &$product->netValue;
            } else {
                $product['netValue'] = '0.00';
                $netValue = &$product['netValue'];
            }
            if(isset($product->vatAmount)) {
                $vatAmount = &$product->vatAmount;
            } else {
                $product['vatAmount'] = '0.00';
                $vatAmount = &$product['vatAmount'];
            }
            if(isset($product->grossValue)) {
                $grossValue = &$product->grossValue;
            } else {
                $product['grossValue'] = '0.00';
                $grossValue = &$product['grossValue'];
            }
            if(isset($vats[(string) $vatRate])) {
                $vat = &$vats[(string) $vatRate];
                $netValue = bcmul($product['price'], $count, 2);
                $vatAmount = bcmul($netValue, $vat['vatRate'], 2);
                $grossValue = bcadd($netValue, $vatAmount, 2);
                $vat['netValue'] = bcadd($vat['netValue'], $netValue, 2);

                $vat['vatAmount'] = bcadd($vat['vatAmount'], $vatAmount, 2);
                $vat['grossValue'] = bcadd($vat['grossValue'], $grossValue, 2);

                $total = [
                    'netValue' => bcadd($total['netValue'], $netValue, 2),
                    'vatAmount' => bcadd($total['vatAmount'], $vatAmount, 2),
                    'grossValue' => bcadd($total['grossValue'], $grossValue, 2),
                ];
            }
        }

        $outTotal = $total;
        $outVats = $vats;
    }

    public static function makeVatRow($label, $rate) {
        return ['vatLabel' => $label, 'vatRate' => $rate, 'netValue' => '0.00', 'vatAmount' => '0.00', 'grossValue' => '0.00'];
    }

    public function getScenario() {
        return $this->scenario;
    }

    private function saveProductAttachment($productIds, $products) {

        $data = [];
        $hashToRemove = [];
        $productAtttachmentToRemove = [];
        foreach($products as $key => $product) {
            foreach($product->files as $file) {
                $data[] = [
                    $productIds[$key]['id'],
                    Yii::$app->user->id,
                    Url::to(['storage/index', 'f' => $file['document']]),
                    $file['name']
                ];
                $hashToRemove[] = $file['document'];
                $productAtttachmentToRemove[] = $productIds[$key]['id'];
            }
        }
        //\Yii::$app->db->createCommand()->delete('order_offered_product_attachment', ['in', 'order_offered_product_id', $productAtttachmentToRemove])->execute();
        \Yii::$app->db->createCommand()->batchInsert('order_offered_product_attachment', ['order_offered_product_id', 'user_id', 'url_file', 'name'], $data
        )->execute();
        \Yii::$app->db->createCommand()->delete('file_temporary_storage', ['in', 'file_hash', $hashToRemove])->execute();
    }

}
