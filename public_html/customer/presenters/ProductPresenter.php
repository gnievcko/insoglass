<?php
namespace customer\presenters;

use frontend\helpers\ManufacturerHelper;

class ProductPresenter {
	
	public function prepareDataForDetails($data) {		
		$output = $data;
		
		$output['details']['manufacturer'] = ManufacturerHelper::getFullManufacturerData([
				'id' => $data['details']['manufacturerId'],
				'name' => $data['details']['manufacturerName'],
				'contactPerson' => $data['details']['manufacturerContactPerson'],
				'email' => $data['details']['manufacturerEmail'],
				'phone' => $data['details']['manufacturerPhone'],
				'address' => $data['details']['manufacturerAddress'],
				'complement' => $data['details']['manufacturerComplement'],
				'city' => $data['details']['manufacturerCity'],
				'zipCode' => $data['details']['manufacturerZipCode'],
				'province' => $data['details']['manufacturerProvince'],
				'country' => $data['details']['manufacturerCountry'],
		]);
		
		return $output;
	}
	
}
