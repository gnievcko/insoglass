<?php

namespace customer\controllers;

use Yii;
use frontend\components\Controller;
use yii\filters\AccessControl;
use yii\helpers\Url;
use yii\data\Pagination;
use yii\web\Response;
use yii\web\BadRequestHttpException;
use yii\widgets\ActiveForm;
use common\models\aq\OrderQuery;
use common\models\ar\OrderStatusTranslation;
use common\models\ar\OrderStatus;
use common\models\ar\Order;
use customer\models\OrdersListForm;
use customer\models\OrderProductForm;
use customer\models\OrderCostForm;
use customer\models\OrderModel;
use frontend\models\OrderEmployeesForm;
use frontend\models\OrderHistoryForm;
use yii\helpers\ArrayHelper;
use common\models\aq\OrderHistoryQuery;
use common\helpers\Utility;
use common\models\aq\UserQuery;
use common\models\aq\UserOrderSalesmanQuery;
use common\models\aq\OrderStatusQuery;
use common\models\ar\Currency;
use common\models\ar\ProductTranslation;
use common\models\ar\User;
use common\models\ar\Entity;
use common\models\ar\Language;
use common\models\ar\OrderStatusTransition;
use common\helpers\StringHelper;
use common\models\aq\WarehouseProductQuery;
use common\alerts\OrderDeadlineAlertHandler;
use frontend\models\AlertForm;
use yii\mongodb\Query;
use common\models\aq\OrderTypeQuery;
use yii\filters\VerbFilter;
use common\models\aq\DocumentQuery;
use common\models\aq\OrderUserRepresentativeQuery;
use frontend\helpers\ModelLoaderHelper;
use common\documents\DocumentTypeUtility;
use common\models\aq\OrderTypeSetQuery;
use frontend\models\OrderTableForm;
use yii\helpers\Json;
use common\models\ar\OrderOfferedProductAttachment;
use common\models\ar\OfferedProduct;
use common\models\aq\OrderOfferedProductAttributeQuery;
use common\models\aq\ProductAttributeTypeQuery;
use customer\logic\ProductAttributeTypeLogic;
use common\models\ar\Company;
use common\models\aq\ShapeQuery;
use common\models\aq\ConstructionQuery;
use common\models\aq\MuntinQuery;
use common\models\ar\Product;
use common\models\ar\Shape;
use common\models\ar\OfferedProductTranslation;
use frontend\models\ArtificialProductAttributeForm;
use frontend\models\ProductAttributeForm;
use common\models\ar\RalColour;

class OrderController extends Controller {

    public function behaviors() {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['index', 'list', 'details', 'create', 'get-product-form', 'get-photo-form',
                    'get-document-form', 'get-employee-form', 'update', 'find', 'copy', 'edit', 'quick-update',
                    'get-user-representatives', 'delete', 'get-all-products-summary', 'get-custom-data-summary',
                    'get-custom-data-numeration', 'summary', 'get-color-and-shape'],
                'rules' => [
                    [
                        'actions' => ['index', 'list', 'details', 'create', 'get-product-form', 'get-photo-form',
                            'get-document-form', 'get-employee-form', 'update', 'copy', 'edit', 'quick-update',
                            'get-user-representatives', 'get-all-products-summary', 'get-custom-data-summary',
                            'get-custom-data-numeration', 'get-color-and-shape'],
                        'allow' => true,
                        'roles' => [Utility::ROLE_CLIENT],
                    ],
                    [
                        'actions' => ['delete', 'get-product-form', 'get-costs-form'],
                        'allow' => true,
                        'roles' => [Utility::ROLE_CLIENT],
                    ],
                    [
                        'actions' => ['summary'],
                        'allow' => true,
                        'roles' => [Utility::ROLE_CLIENT],
                        'verbs' => ['post'],
                    ],
                    [
                        'actions' => ['find'],
                        'allow' => true,
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    public function actionIndex() {
        return $this->redirect(['order/list']);
    }

    public function actionList() {
        $request = Yii::$app->request;
        $ordersListForm = new OrdersListForm();
        $ordersListForm->load($request->get());

        $activeOrdersQuery = OrderQuery::getOrdersQuery($ordersListForm->toArray() + ['isVisibleForClient' => 1]);
        $pages = new Pagination([
            'totalCount' => $activeOrdersQuery->count(),
            'page' => $ordersListForm->page,
            'defaultPageSize' => \Yii::$app->params['defaultPageSize'],
        ]);
        $orders = $activeOrdersQuery->limit($pages->defaultPageSize)->offset($pages->page * $pages->defaultPageSize)->all();

        $ordersListData = [
            'orders' => $orders,
            'pages' => $pages,
            'ordersListForm' => $ordersListForm,
        ];

        if($request->isAjax) {
            echo json_encode([
                'view' => $this->renderAjax('inc/_orders', $ordersListData),
                'url' => Url::current([], true),
            ]);
        } else {
            $ordersListData['ordersStatusesTranslations'] = OrderStatusTranslation::find()->forLanguage(\Yii::$app->language)->visibleForClient(1)->all();
            return $this->render('list', $ordersListData);
        }
    }

    public function actionDetails($id) {
        $data = OrderQuery::getDetails($id);
        if(empty($data) ||
                !empty($data['isDeleted']) ||
                (!(Yii::$app->user->can(Utility::ROLE_CLIENT) &&
                (UserQuery::isClientOrderCreator($id, Yii::$app->user->id) ||
                UserQuery::isOrderRepresentant($id, Yii::$app->user->id))) &&
                (!UserQuery::isSalesmanAssignedToCompanyByOrderId(Yii::$app->user->id, $id) &&
                !Yii::$app->user->can(Utility::ROLE_MAIN_SALESMAN) &&
                !UserQuery::isSalesmanAssignedToOrder(Yii::$app->user->id, $id)))
        ) {
            throw new \yii\web\NotFoundHttpException(Yii::t('web', 'Element does not exist or you do not have permission.'));
        }

        $reservedProducts = WarehouseProductQuery::getReservedProductsByOrderId($id)->all();

        $contactPeople = OrderUserRepresentativeQuery::getUserRepresentativesByOrderId($id);
        $orderTypes = OrderTypeSetQuery::getOrderTypesByOrderId($id);
        $entries = OrderHistoryQuery::getOrderEntries($id);
        $salesmen = UserOrderSalesmanQuery::getSalesmenByOrderId($id);
        $attachments = OrderQuery::getAttachments($id);
        $products = OrderQuery::getOrderProducts($id, ['withArtificial' => true]);
        if(Yii::$app->params['isAttachmentForProductVisible']) {
            foreach($products as &$oneProduct) {
                $oneProduct['attachments'] = OrderQuery::getOrderProductAttachments($oneProduct['id']);
            }
        }
        $suborders = OrderQuery::getChildrenOrders($id);

        $costSum = 0;
        if(!empty($products)) {
            foreach($products as $product) {
                if(!empty($product['price'])) {
                    $costSum += $product['count'] * ($product['price'] * $product['conversionValue']);
                }
            }
        }

        $photos = [];
        $photosRaw = OrderQuery::getPhotos($id);
        if(!empty($photosRaw)) {
            $photos = array_map(function($row) {
                return [
                    'url' => $row['urlPhoto'],
                    'src' => $row['urlThumbnail'],
                    'options' => [
                        'title' => $row['description'],
                    ],
                ];
            }, $photosRaw);
        }

        $summary;
        $vats;
        OrderModel::calculateSummary($products, $summary, $vats);
        $data['visibleStatusButton'] = false;

        if(!empty($data['statusId'])) {
            $statusSuccessorId = ArrayHelper::map(OrderStatusTransition::find()->where(['order_status_predecessor_id' => $data['statusId']])->all(), 'order_status_successor_id', 'order_status_successor_id');
            $statusId = OrderStatus::find()->where(['IN', 'id', $statusSuccessorId])->andWhere(['is_visible_for_client' => 1])->one();

            $data['visibleStatusButton'] = isset($statusId) && !empty($statusId) ? true : false;
        }

        if(!empty($data['customData'])) {
            $data['customDataSummary'] = OrderTableForm::calculateTotalPriceStatic(Json::decode($data['customData']));
        }

        if(Yii::$app->params['isProductAttributesVisible'] && !empty($products)) {
            foreach($products as &$product) {
                $attributes = OrderOfferedProductAttributeQuery::getProductAttributes($product['id'], false, true);
                $product['attributes'] = null;

                if(!empty($attributes)) {
                    $attrs = [];
                    foreach($attributes as $attribute) {
                        $attrs[] = $attribute['name'] . (intval($attribute['value']) > 1 ? ': ' . $attribute['value'] : '');
                    }

                    $product['attributes'] = implode(', ', $attrs);
                }
            }
        }

        $orderStatus = Order::findOne($id)->orderHistoryLast->orderStatus->symbol;
        $isInProgress = $orderStatus == Utility::ORDER_STATUS_IN_PROGRESS;
        
        return $this->render('details', [
            'data' => $data,
            'contactPeople' => $contactPeople,
            'orderTypes' => $orderTypes,
            'entries' => $entries,
            'salesmen' => $salesmen,
            'attachments' => $attachments,
            'products' => $products,
            'reservedProducts' => $reservedProducts,
            'photos' => $photos,
            'costSum' => $costSum,
            'summary' => $summary,
            'vats' => $vats,
            'plnCurrency' => Currency::find()->where('symbol = :symbol', [':symbol' => Utility::CURRENCY_PLN])->one(),
            'documents' => $this->getDocuments($id),
            'documentsAbleToDelete' => DocumentQuery::getLastInvoiceIds(),
            'availableDocuments' => DocumentTypeUtility::getAvailableDocuments(),
            'isInProgress' => $isInProgress,
        ]);
    }

    public function actionCreate($companyId = null) {

        $request = Yii::$app->request;
        $save = $request->post('save');
        $data = $request->post();
        $orderId = isset($data['OrderCreateForm']['orderId']) ? $data['OrderCreateForm']['orderId'] : null;

        if(!empty($orderId) && $orderId != null) {
            $scenario = OrderModel::SCENARIO_UPDATE_EDIT;
            //TODO
            $orderModel = new OrderModel($scenario, $orderId, $companyId);
        } else {
            $scenario = OrderModel::SCENARIO_CREATE;
            $orderModel = new OrderModel($scenario, null, $companyId);
        }

        if($request->isPost) {
            $data = json_decode(htmlspecialchars_decode($data['data']), true);
            $orderModel->load($data);
            if(isset($save) && !empty($save)) {
                try {
                    if($orderModel->save()) {
                        Yii::$app->getSession()->setFlash('success', StringHelper::translateOrderToOffer('web', 'Order has been added successfully.'));
                        return $this->redirect(['order/details', 'id' => $orderModel->getOrder()->id]);
                    }
                } catch(\Exception $e) {
                    Yii::$app->getSession()->setFlash('error', StringHelper::translateOrderToOffer('web', 'Order data cannot be saved. Check provided values.'));
                    Yii::error($e->getMessage());
                    Yii::error($e->getTraceAsString());
                }
            }
        }

        $forms = $orderModel->getAllForms();
        $total;
        $vats;
        $productForms = [];
        OrderModel::calculateSummary($productForms, $total, $vats);

        return $this->render('create', $forms + [
            'entities' => ArrayHelper::map(Entity::find()->all(), 'id', 'name'),
            'currencies' => ArrayHelper::map(Currency::find()->all(), 'id', 'short_symbol'),
            'constructions' => Yii::$app->params['isOrderProductDimensionsVisible'] ? ArrayHelper::map(ConstructionQuery::getNames(), 'id', 'name') : [],
            'shapes' => Yii::$app->params['isOrderProductDimensionsVisible'] ? ArrayHelper::map(ShapeQuery::getNames(), 'id', 'name') : [],
            'muntins' => Yii::$app->params['isOrderProductDimensionsVisible'] ? ArrayHelper::map(MuntinQuery::getNames(), 'id', 'name') : [],
            'invalidForms' => $orderModel->getInvalidForms(),
            'plnCurrency' => Currency::find()->where('symbol = :symbol', [':symbol' => Utility::CURRENCY_PLN])->one(),
            'summary' => ['total' => $total, 'vats' => $vats],
            'displayFirstAttributes' => false
        ]);
    }

    public function actionSummary($companyId = null) {

        $request = Yii::$app->request;
        $formData = $request->post();

            $scenario = OrderModel::SCENARIO_CREATE;
        $orderModel = new OrderModel($scenario, null, $companyId);

        if($request->isAjax && $request->isPost && $request->post('click') != 1) {
            $orderModel->load($formData);
            Yii::$app->response->format = Response::FORMAT_JSON;

            $result = ActiveForm::validateMultiple($orderModel->getOrderProductsForms()) + ActiveForm::validateMultiple($orderModel->getDocumentsForms()) + ActiveForm::validateMultiple($orderModel->getPhotosForms()) + ActiveForm::validateMultiple($orderModel->getOrderCostsForms()) + ActiveForm::validate($orderModel->getOrderCreateForm());

            if(Yii::$app->params['isProductAttributesVisible']) {
                $result += ActiveForm::validateMultiple($orderModel->getProductAttributeForms());
            }
            return $result;
        }

        $orderModel->load($formData);
        $forms = $orderModel->getAllForms();

        $company = Company::findOne($orderModel->getOrderCreateForm()->clientId);
        $data['companyName'] = $company->name;
        if(!empty($company->parent_company_id)) {
            $parentCompany = Company::findOne($company->parent_company_id);
            $data['parentCompanyId'] = $company->parent_company_id;
            $data['parentCompanyName'] = $parentCompany->name;
        }
        $data['entity'] = Entity::findOne($orderModel->getOrderCreateForm()->entityId)->name;

        $contactPeople = User::find()->where(['id' => $formData[$orderModel->getOrderCreateForm()->formName()]['contactPeopleIds']])->all();
        $language = Language::findOne(['symbol' => Yii::$app->language])->id;

        $products = [];
        foreach($orderModel->getOrderProductsForms() as $product) {
            $product = $product->attributes;

            if(empty($product['productId'])) {
                continue;
            }

            if($product['productType'] == Utility::PRODUCT_TYPE_PRODUCT) {
                $product['name'] = Product::findOne($product['productId'])->getTranslation()->name;
            } else {
                $product['name'] = OfferedProduct::findOne($product['productId'])->getTranslation()->name;
            }

            $product['isArtificial'] = false;
            $products[] = $product;
        }

        $costProducts = [];
        foreach($orderModel->getOrderCostsForms() as $product) {
            if(empty($product['name'])) {
                continue;
            }
            $product = $product->attributes;
            $product['isArtificial'] = true;
            $costProducts[] = $product;
        }

        $summary;
        $vats;
        OrderModel::calculateSummary($products, $summary, $vats);

        if($request->isPost) {
            return $this->render('summary', $forms + [
                        'formData' => $formData,
                        'data' => $data,
                        'contactPeople' => $contactPeople,
                        'products' => $products,
                        'costProducts' => $costProducts,
                        'summary' => $summary,
                        'plnCurrency' => Currency::find()->where('symbol = :symbol', [':symbol' => Utility::CURRENCY_PLN])->one(),
            ]);
        }
    }

    public function actionGetProductsForm() {
        $form = ActiveForm::begin([
                    'id' => Yii::$app->request->post('id'),
                    'enableAjaxValidation' => true,
                    'enableClientValidation' => false,
                    'options' => [
                        'class' => 'form',
                    ]
        ]);

        $tempOrderProductForms = ModelLoaderHelper::loadMultiple(Yii::$app->request->post(), OrderProductForm::class);

        $tempProductAttributeForms = ModelLoaderHelper::loadMultiple(Yii::$app->request->post(), ProductAttributeForm::class);

        $orderProductForms = !empty($tempOrderProductForms) ? array_values($tempOrderProductForms) : [];
        $productAttributeForms = !empty($tempProductAttributeForms) ? array_values($tempProductAttributeForms) : [];
        $summary;
        $vats;

        if(!empty(Yii::$app->request->post('copyRow'))) {
            $rowId = Yii::$app->request->post('rowId');
            $inserted = [$orderProductForms[$rowId]];

            array_splice($orderProductForms, $rowId, 0, $inserted);

            if(Yii::$app->params['isProductAttributesVisible']) {
                $inserted = [clone($productAttributeForms[$rowId])];
                array_splice($productAttributeForms, $rowId, 0, $inserted);
                foreach($productAttributeForms as $productAttributeForm) {
                    $productAttributeForm->isRowVisible = false;
                }
                $productAttributeForms[$rowId + 1]->isRowVisible = true;
            }
        }

        if(!empty(Yii::$app->request->post('addRow'))) {
            array_push($orderProductForms, new OrderProductForm());

            if(Yii::$app->params['isProductAttributesVisible']) {
                foreach($productAttributeForms as $productAttributeForm) {
                    $productAttributeForm->isRowVisible = false;
                }

                array_push($productAttributeForms, new ProductAttributeForm());
                $productAttributeForms[count($productAttributeForms) - 1]->isRowVisible = true;
            }
        }

        OrderModel::calculateSummary($orderProductForms, $summary, $vats);

        $shapes = Yii::$app->params['isOrderProductDimensionsVisible'] ? ArrayHelper::map(ShapeQuery::getNames(), 'id', 'name') : [];
        $constructions = Yii::$app->params['isOrderProductDimensionsVisible'] ? ArrayHelper::map(ConstructionQuery::getNames(), 'id', 'name') : [];
        $muntins = Yii::$app->params['isOrderProductDimensionsVisible'] ? ArrayHelper::map(MuntinQuery::getNames(), 'id', 'name') : [];

        return $this->renderAjax('inc/_products', [
                    'orderProductsForms' => $orderProductForms,
                    'productAttributeForms' => $productAttributeForms,
                    'form' => $form,
                    'displayFirstAttributes' => false,
                    'orderProductsVats' => $vats,
                    'orderProductsSummary' => $summary,
                    'currencies' => ArrayHelper::map(Currency::find()->all(), 'id', 'short_symbol'),
                    'constructions' => $constructions,
                    'shapes' => $shapes,
                    'muntins' => $muntins,
                    'plnCurrency' => Currency::find()->where('symbol = :symbol', [':symbol' => Utility::CURRENCY_PLN])->one(),
        ]);
    }

    public function actionGetCostsForm() {
        $form = ActiveForm::begin([
                    'id' => Yii::$app->request->post('id'),
                    'enableAjaxValidation' => true,
                    'enableClientValidation' => false,
                    'options' => [
                        'class' => 'form',
                    ]
        ]);
        $orderCostsForms = ModelLoaderHelper::loadMultiple(Yii::$app->request->post(), OrderCostForm::class);
        $tempProductAttributeForms = ModelLoaderHelper::loadMultiple(Yii::$app->request->post(), ArtificialProductAttributeForm::class);
        $productAttributeForms = !empty($tempProductAttributeForms) ? array_values($tempProductAttributeForms) : [];

        $summary;
        $vats;

        if(!empty(Yii::$app->request->post('copyRow'))) {
            $rowId = Yii::$app->request->post('rowId');
            $inserted = [$orderCostsForms[$rowId]];

            array_splice($orderCostsForms, $rowId, 0, $inserted);

            if(Yii::$app->params['isProductAttributesVisible']) {
                $inserted = [clone($productAttributeForms[$rowId])];
                array_splice($productAttributeForms, $rowId, 0, $inserted);
                foreach($productAttributeForms as $productAttributeForm) {
                    $productAttributeForm->isRowVisible = false;
                }
                $productAttributeForms[$rowId + 1]->isRowVisible = true;
            }
        }

        if(!empty(Yii::$app->request->post('addRow'))) {
            array_push($orderCostsForms, new OrderCostForm());

            if(Yii::$app->params['isProductAttributesVisible']) {
                foreach($productAttributeForms as $productAttributeForm) {
                    $productAttributeForm->isRowVisible = false;
                }

                array_push($productAttributeForms, new ArtificialProductAttributeForm());
                $productAttributeForms[count($productAttributeForms) - 1]->isRowVisible = true;
            }
        }
        OrderModel::calculateSummary($orderCostsForms, $summary, $vats);

        $shapes = Yii::$app->params['isOrderProductDimensionsVisible'] ? ArrayHelper::map(ShapeQuery::getNames(), 'id', 'name') : [];
        $constructions = Yii::$app->params['isOrderProductDimensionsVisible'] ? ArrayHelper::map(ConstructionQuery::getNames(), 'id', 'name') : [];
        $muntins = Yii::$app->params['isOrderProductDimensionsVisible'] ? ArrayHelper::map(MuntinQuery::getNames(), 'id', 'name') : [];

        return $this->renderAjax('inc/_costs', [
                    'orderCostsForms' => $orderCostsForms,
                    'form' => $form,
                    'displayFirstAttributes' => false,
                    'orderCostsVats' => $vats,
                    'orderCostsSummary' => $summary,
                    'currencies' => ArrayHelper::map(Currency::find()->all(), 'id', 'short_symbol'),
                    'constructions' => $constructions,
                    'shapes' => $shapes,
                    'muntins' => $muntins,
                    'plnCurrency' => Currency::find()->where('symbol = :symbol', [':symbol' => Utility::CURRENCY_PLN])->one(),
                    'productAttributeForms' => $productAttributeForms,
        ]);
    }

    public function actionGetCustomDataSummary() {
        return Json::encode(OrderTableForm::calculateTotalPrice(Yii::$app->request->post()));
    }

    public function actionGetCustomDataNumeration() {
        return Json::encode(OrderTableForm::numerateRows(Yii::$app->request->post()));
    }

    public function actionGetAllProductsSummary() {
        $orderProductForms = ModelLoaderHelper::loadMultiple(Yii::$app->request->post(), OrderProductForm::class);

        $orderCostForms = ModelLoaderHelper::loadMultiple(Yii::$app->request->post(), OrderCostForm::class);

        $data = array_merge($orderProductForms, $orderCostForms);
        $total;
        $vats;
        OrderModel::calculateSummary($data, $total, $vats);

        return $this->renderAjax('inc/_productSummary', [
                    'summary' => ['total' => $total, 'vats' => $vats],
                    'plnCurrency' => Currency::find()->where('symbol = :symbol', [':symbol' => Utility::CURRENCY_PLN])->one(),
        ]);
    }

    public function actionGetProductForm() {
        if(\Yii::$app->request->isAjax) {
            echo $this->renderAjax('inc/_product', [
                'orderProductForm' => new OrderProductForm(),
                'currencies' => ArrayHelper::map(Currency::find()->all(), 'id', 'short_symbol'),
                'prefix' => \Yii::$app->request->get('prefix'),
                'formId' => \Yii::$app->request->get('formId'),
                'plnCurrency' => Currency::find()->where('symbol = :symbol', [':symbol' => Utility::CURRENCY_PLN])->one(),
            ]);
        }
    }

    public function actionGetEmployeeForm() {
        $request = \Yii::$app->request;
        $employee = User::find()->where(['id' => $request->get('id')])->one();

        if($request->isAjax && $employee) {
            echo $this->renderAjax('inc/_employee', [
                'orderEmployeesForm' => new OrderEmployeesForm(),
                'employee' => $employee,
            ]);
        }
    }

    public function actionGetCostForm() {
        $request = \Yii::$app->request;

        if($request->isAjax) {
            echo $this->renderAjax('inc/_cost', [
                'orderCostForm' => new OrderCostForm(),
                'prefix' => $request->get('prefix'),
                'currencies' => ArrayHelper::map(Currency::find()->all(), 'id', 'short_symbol'),
                'formId' => $request->get('formId'),
                'plnCurrency' => Currency::find()->where('symbol = :symbol', [':symbol' => Utility::CURRENCY_PLN])->one(),
            ]);
        }
    }

    public function actionUpdate($id) {
        $request = Yii::$app->request;
        $data = $request->post();
        $orderHistoryId = isset($data['OrderHistoryForm']['orderHistoryId']) ? $data['OrderHistoryForm']['orderHistoryId'] : null;
        if(!empty($orderHistoryId)) {
            $orderModel = new OrderModel(OrderModel::SCENARIO_STATUS_UPDATE_CONT, $id);
        } else {
            $orderModel = new OrderModel(OrderModel::SCENARIO_STATUS_UPDATE, $id);
        }

        $order = Order::find()->where(['id' => $id])->with('orderHistoryLast')->one();
        if(empty($order) || !empty($order->is_deleted) || ($order->is_active == 0 && !(Yii::$app->user->can(Utility::ROLE_ADMIN) && Yii::$app->params['isUnlimitedOrderChangeStatusAvailable'])) ||
                (!(Yii::$app->user->can(Utility::ROLE_CLIENT) &&
                (UserQuery::isClientOrderCreator($id, Yii::$app->user->id) ||
                UserQuery::isOrderRepresentant($id, Yii::$app->user->id))) &&
                (!UserQuery::isSalesmanAssignedToCompanyByOrderId(Yii::$app->user->id, $id) &&
                !Yii::$app->user->can(Utility::ROLE_MAIN_SALESMAN) &&
                !UserQuery::isSalesmanAssignedToOrder(Yii::$app->user->id, $id)))) {
            throw new \yii\web\NotFoundHttpException(Yii::t('web', 'Element does not exist or you do not have permission.'));
        }

        //$orderModel = new OrderModel(OrderModel::SCENARIO_STATUS_UPDATE, $id);
        //$request = Yii::$app->request;
        if($request->isAjax && $request->isPost && $request->post('click') != 1) {
            $orderModel->load($request->post());
            Yii::$app->response->format = Response::FORMAT_JSON;

            return ActiveForm::validateMultiple($orderModel->getOrderProductsForms()) + ActiveForm::validateMultiple($orderModel->getDocumentsForms()) + ActiveForm::validateMultiple($orderModel->getPhotosForms()) + ActiveForm::validate($orderModel->getOrderHistoryForm()) + ActiveForm::validateMultiple($orderModel->getOrderCostsForms());
        }

        if($request->isPost) {
            $orderModel->load($request->post());

            try {
                if($orderModel->save()) {
                    if(empty($request->post('save'))) {
                        OrderDeadlineAlertHandler::check(['orderId' => $order->id]);
                        Yii::$app->getSession()->setFlash('success', StringHelper::translateOrderToOffer('web', 'Order has been updated successfully.'));
                        $orderHistoryForm = new OrderHistoryForm();
                        $orderHistoryForm->load($request->post());
                        //Yii::$app->getSession()->set('positiveTerminalStatus', OrderStatusQuery::isOrderStatusPositiveTerminal($orderHistoryForm->orderStatusId));
                        return $this->redirect(['order/details', 'id' => $id]);
                    } else {
                        Yii::$app->response->format = Response::FORMAT_JSON;
                        return ['id' => $orderModel->getOrderHistoryForm()->orderHistoryId];
                    }
                }
            } catch(\Exception $e) {
                Yii::$app->getSession()->setFlash('error', StringHelper::translateOrderToOffer('web', 'Order data cannot be saved. Check provided values.'));
                Yii::error($e->getMessage());
                Yii::error($e->getTraceAsString());
            }
        }

        $forms = $orderModel->getAllForms();
        $total;
        $vats;
        $productForms = array_merge($forms['orderProductsForms'], $forms['orderCostsForms']);
        OrderModel::calculateSummary($productForms, $total, $vats);
        $shapes = Yii::$app->params['isOrderProductDimensionsVisible'] ? ArrayHelper::map(ShapeQuery::getNames(), 'id', 'name') : [];
        $constructions = Yii::$app->params['isOrderProductDimensionsVisible'] ? ArrayHelper::map(ConstructionQuery::getNames(), 'id', 'name') : [];
        $muntins = Yii::$app->params['isOrderProductDimensionsVisible'] ? ArrayHelper::map(MuntinQuery::getNames(), 'id', 'name') : [];

        return $this->render('update', [
                    'id' => $id,
                    'currencies' => ArrayHelper::map(Currency::find()->all(), 'id', 'short_symbol'),
                    'displayFirstAttributes' => true,
                    'constructions' => $constructions,
                    'shapes' => $shapes,
                    'muntins' => $muntins,
                    'plnCurrency' => Currency::find()->where('symbol = :symbol', [':symbol' => Utility::CURRENCY_PLN])->one(),
                    'orderStatuses' => ArrayHelper::map(OrderStatusTranslation::find()->successorsOf($order->orderHistoryLast->order_status_id)->all(), 'order_status_id', 'name'),
                    'summary' => ['total' => $total, 'vats' => $vats],
                        ] + $forms + ['invalidForms' => $orderModel->getInvalidForms()]);
    }

    public function actionQuickUpdate($id) {

        $order = Order::find()->where(['id' => $id])->with('orderHistoryLast')->one();
        if(empty($order) || !empty($order->is_deleted) || ($order->is_active == 0 && !(Yii::$app->user->can(Utility::ROLE_ADMIN) && Yii::$app->params['isUnlimitedOrderChangeStatusAvailable'])) ||
                (!(Yii::$app->user->can(Utility::ROLE_CLIENT) &&
                (UserQuery::isClientOrderCreator($id, Yii::$app->user->id) ||
                UserQuery::isOrderRepresentant($id, Yii::$app->user->id))) &&
                (!UserQuery::isSalesmanAssignedToCompanyByOrderId(Yii::$app->user->id, $id) &&
                !Yii::$app->user->can(Utility::ROLE_MAIN_SALESMAN) &&
                !UserQuery::isSalesmanAssignedToOrder(Yii::$app->user->id, $id)))) {
            throw new \yii\web\NotFoundHttpException(Yii::t('web', 'Element does not exist or you do not have permission.'));
        }

        $orderModel = new OrderModel(OrderModel::SCENARIO_QUICK_STATUS_UPDATE, $id);

        //$orderHistoryForm = new OrderHistoryForm();
        //$orderHistoryForm->orderStatusId = $order->orderHistoryLast->order_status_id;
        //$orderHistoryForm->dateDeadline = $order->orderHistoryLast->date_deadline;
        //$orderHistoryForm->dateReminder = $order->orderHistoryLast->date_reminder;

        $request = Yii::$app->request;
        if($request->isAjax && $request->isPost) {
            $orderModel->load($request->post());
            Yii::$app->response->format = Response::FORMAT_JSON;

            return ActiveForm::validate($orderModel->getOrderHistoryForm());
        }

        if($request->isPost) {
            $orderModel->load($request->post());

            try {
                if($orderModel->save()) {
                    OrderDeadlineAlertHandler::check(['orderId' => $order->id]);
                    Yii::$app->getSession()->setFlash('success', StringHelper::translateOrderToOffer('web', 'Order has been updated successfully.'));
                    $orderHistoryForm = new OrderHistoryForm();
                    $orderHistoryForm->load($request->post());
                    //Yii::$app->getSession()->set('positiveTerminalStatus', OrderStatusQuery::isOrderStatusPositiveTerminal($orderHistoryForm->orderStatusId));
                    return $this->redirect(['order/details', 'id' => $id]);
                }
            } catch(\Exception $e) {
                Yii::$app->getSession()->setFlash('error', StringHelper::translateOrderToOffer('web', 'Order data cannot be saved. Check provided values.'));
                Yii::error($e->getMessage());
                Yii::error($e->getTraceAsString());
            }
        } else {
            $this->layout = 'modal';
        }

        $orderStatuses = ArrayHelper::map(OrderStatusTranslation::find()->successorsOf($order->orderHistoryLast->order_status_id)->all(), 'order_status_id', 'name');
        $keys = array_keys($orderStatuses);

        $statusesIsVisibleForClient = ArrayHelper::map(OrderStatus::find()->where(['IN', 'id', $keys])->andWhere(['is_visible_for_client' => 1])->all(), 'id', 'symbol');

        $array = [];
        foreach($statusesIsVisibleForClient as $key => $value) {
            if(in_array($key, $keys)) {
                $array[$key] = $orderStatuses[$key];
            }
        }

        $statusesIsVisibleForClient = $array;

        return $this->renderAjax('quickUpdate', [
                    'id' => $id,
                    'orderHistoryForm' => $orderModel->getOrderHistoryForm(),
                    'orderStatuses' => $orderStatuses,
                    'statusesIsVisibleForClient' => $statusesIsVisibleForClient,
        ]);
    }

    public function actionCopy($id) {
        $order = Order::find()->where(['id' => $id])->one();
        if(empty($order) || !empty($order->is_deleted)) {
            throw new \yii\web\NotFoundHttpException(Yii::t('web', 'Element does not exist or you do not have permission.'));
        }
        $orderModel = new OrderModel(OrderModel::SCENARIO_COPY, $id);

        $request = Yii::$app->request;
        if($request->isAjax && $request->isPost) {
            $orderModel->load($request->post());
            Yii::$app->response->format = Response::FORMAT_JSON;

            return ActiveForm::validateMultiple($orderModel->getOrderProductsForms()) + ActiveForm::validateMultiple($orderModel->getDocumentsForms()) + ActiveForm::validateMultiple($orderModel->getPhotosForms()) + ActiveForm::validateMultiple($orderModel->getOrderCostsForms()) + ActiveForm::validate($orderModel->getOrderCreateForm());
        }

        if($request->isPost) {
            $orderModel->load($request->post());

            try {
                if($orderModel->save()) {
                    Yii::$app->getSession()->setFlash('success', StringHelper::translateOrderToOffer('web', 'Order has been added successfully.'));
                    return $this->redirect(['order/details', 'id' => $orderModel->getOrder()->id]);
                }
            } catch(\Exception $e) {
                Yii::$app->getSession()->setFlash('error', StringHelper::translateOrderToOffer('web', 'Order data cannot be saved. Check provided values.'));
                Yii::error($e->getMessage());
                Yii::error($e->getTraceAsString());
            }
        }

        $forms = $orderModel->getAllForms();
        $total;
        $vats;
        $productForms = array_merge($forms['orderProductsForms'], $forms['orderCostsForms']);
        OrderModel::calculateSummary($productForms, $total, $vats);

        return $this->render('copy', $forms + [
                    'entities' => ArrayHelper::map(Entity::find()->all(), 'id', 'name'),
                    'currencies' => ArrayHelper::map(Currency::find()->all(), 'id', 'short_symbol'),
                    'orderTypes' => ArrayHelper::map(OrderTypeQuery::getOrderTypes(), 'id', 'name'),
                    'contractTypes' => ArrayHelper::map(OrderTypeQuery::getContractTypes(), 'id', 'name'),
                    'plnCurrency' => Currency::find()->where('symbol = :symbol', [':symbol' => Utility::CURRENCY_PLN])->one(),
                    'invalidForms' => $orderModel->getInvalidForms(),
                    'summary' => ['total' => $total, 'vats' => $vats],
        ]);
    }

    public function actionFind() {
        $request = Yii::$app->request;
        $term = $request->get('term');
        $page = $request->get('page', 1);
        $size = $request->get('size');

        if(empty($term) || empty($page) || empty($size)) {
            Yii::$app->end();
        }

        $query = OrderQuery::findOrders(['name' => $term]);
        $count = $query->count();
        $items = $query->limit($size)->offset($size * ($page - 1))->all();

        echo json_encode([
            'items' => $items,
            'more' => (intval($count) > $page * $size),
        ]);
    }

    public function actionEdit($id) {
        $order = Order::findOne($id);
        if(empty($order) || $order->is_deleted == 1 ||
                (!(Yii::$app->user->can(Utility::ROLE_CLIENT) &&
                (UserQuery::isClientOrderCreator($id, Yii::$app->user->id) ||
                UserQuery::isOrderRepresentant($id, Yii::$app->user->id))) && (!UserQuery::isSalesmanAssignedToCompanyByOrderId(Yii::$app->user->id, $id) &&
                (!Yii::$app->user->can(Utility::ROLE_MAIN_SALESMAN) && !UserQuery::isSalesmanAssignedToOrder(Yii::$app->user->id, $id))))) {
            //Yii::$app->getSession()->setFlash('error', StringHelper::translateOrderToOffer('web', 'An order does not exist or you do not have access rights.'));
            //return $this->redirect(['site/index']);

            throw new \yii\web\NotFoundHttpException(StringHelper::translateOrderToOffer('web', 'Order data cannot be saved. Check provided values.'));
        }

        $orderModel = new OrderModel(OrderModel::SCENARIO_EDIT, $id);

        $request = Yii::$app->request;
        if($request->isAjax && $request->isPost) {
            $orderModel->load($request->post());
            Yii::$app->response->format = Response::FORMAT_JSON;

            return ActiveForm::validate($orderModel->getOrderCreateForm());
        }

        if($request->isPost) {
            $orderModel->load($request->post());
            try {
                if($orderModel->save()) {
                    Yii::$app->getSession()->setFlash('success', StringHelper::translateOrderToOffer('web', 'Order has been updated successfully.'));
                    return $this->redirect(['order/details', 'id' => $id]);
                }
            } catch(\Exception $e) {
                Yii::$app->getSession()->setFlash('error', StringHelper::translateOrderToOffer('web', 'Order data cannot be saved. Check provided values.'));
                Yii::error($e->getMessage());
                Yii::error($e->getTraceAsString());
            }
        }

        $forms = $orderModel->getAllForms();
        return $this->render('edit', $forms + [
                    'entities' => ArrayHelper::map(Entity::find()->all(), 'id', 'name'),
                    'currencies' => ArrayHelper::map(Currency::find()->all(), 'id', 'short_symbol'),
                    'orderTypes' => ArrayHelper::map(OrderTypeQuery::getOrderTypes(), 'id', 'name'),
                    'contractTypes' => ArrayHelper::map(OrderTypeQuery::getContractTypes(), 'id', 'name'),
                    'invalidForms' => $orderModel->getInvalidForms(),
                    'id' => $id,
        ]);
    }

    public function actionSetAlert() {
        $request = Yii::$app->request;

        if($request->isPost) {
            $form = new AlertForm();
            $form->load($request->post());

            if($request->isAjax) {
                Yii::$app->response->format = Response::FORMAT_JSON;
                return ActiveForm::validate($form);
            }

            try {
                $form->save();
                Yii::$app->getSession()->setFlash('success', StringHelper::translateOrderToOffer('web', 'An order renewing alert has been created successfully'));
            } catch(\Exception $e) {
                Yii::$app->getSession()->setFlash('error', StringHelper::translateOrderToOffer('web', 'Cannot create an order renewing alert'));
                Yii::error($e->getMessage());
                Yii::error($e->getTraceAsString());
            }
            return $this->redirect(['order/details', 'id' => $form->orderId]);
        } else {
            throw new BadRequestHttpException(Yii::t('web', 'set-alert action can be invoked only through POST request'));
        }
    }

    private function getDocuments($id) {
        if(isset(Yii::$app->params['isDocumentStorageAvailable']) && Yii::$app->params['isDocumentStorageAvailable']) {
            $where = ['history.sections.orderId' => (int) $id];
            $query = new Query();

            $query->select([])
                    ->from('documents')
                    ->orderBy(['history.createdAt' => SORT_DESC])
                    ->where($where);

            $rows = $query->all();

            return $rows;
        } else {
            return [];
        }
    }

    public function actionGetUserRepresentatives() {
        $companyId = Yii::$app->request->get('companyId');
        Yii::$app->response->format = Response::FORMAT_JSON;
        return User::findAll(['company_id' => $companyId]);
    }

    public function actionDelete() {
        $id = Yii::$app->request->post('id');

        try {
            $order = Order::findOne($id);
            if(!empty($order->is_deleted) || $order->orderStatus->symbol != Utility::ORDER_STATUS_NEW) {
                throw new \Exception('The user has not privilege to delete this order: ' . $id);
            }

            $order->is_deleted = 1;
            $order->user_deleting_id = Yii::$app->user->id;
            $order->save();

            OrderDeadlineAlertHandler::check(['orderId' => $order->id]);

            Yii::$app->getSession()->setFlash('success', StringHelper::translateOrderToOffer('web', 'The order has been deleted.'));
        } catch(\Exception $e) {
            Yii::error(print_r($e->getMessage(), true));
            Yii::$app->getSession()->setFlash('error', StringHelper::translateOrderToOffer('web', 'The order cannot be deleted. Please contact the administrator.'));
        }

        if(strpos(Yii::$app->request->getReferrer(), Yii::$app->controller->id . '/details') !== false) {
            return $this->redirect(['list']);
        }
        return $this->redirect(Yii::$app->request->getReferrer());
    }

    public function getCostTable() {
        return Yii::$app->controller->renderPartial('inc/_costs', ['orderCostsForms' => $orderCostsForms, 'currencies' => $currencies, 'form' => $form]);
    }

    public function actionGetColorAndShape() {
        $shapeId = Yii::$app->request->get('shapeId');
        $ralColor = Yii::$app->request->get('ralColour');

        $shape = Shape::find()->where(['id' => $shapeId])->one();
        $colour = RalColour::find()->where(['symbol' => $ralColor])->one();

        echo json_encode(['shape' => !empty($shape) ? $shape->identifier : '', 'colour' => !empty($colour) ? $colour->rgb_hash : '']);
    }

}
