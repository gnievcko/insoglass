<?php

namespace customer\controllers;

use Yii;
use stdClass;
use common\helpers\Utility;
use common\models\LoginForm;
use common\models\ar\Company;
use common\models\ar\TokenType;
use frontend\components\Controller;
use frontend\models\PasswordResetRequestForm;
use frontend\models\ResetPasswordForm;
use frontend\models\ContactForm;
use frontend\models\RegisterForm;
use yii\base\InvalidParamException;
use yii\web\BadRequestHttpException;
use yii\helpers\StringHelper;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use yii\base\InvalidValueException;
use frontend\models\ProductVariantForm;
use common\components\ActiveForm;
use common\components\MessageProvider;
use yii\web\Response;
use common\models\ar\Token;
use common\models\ar\User;

/**
 * Site controller
 */
class SiteController extends Controller {
	
    /**
     * @inheritdoc
     */
    public function behaviors() {
        return [
	            'access' => [
		                'class' => AccessControl::className(),
		                'only' => ['logout', 'signup', 'maintenance', 'index', 'login', 'find-companies', 'contact',
		                		'request-password-reset', 'reset-password', 'get-product-variant-form', 'register', 'activate-account'],
		                'rules' => [
			                    [
				                        'actions' => ['logout', 'find-companies', 'get-product-variant-form'],
				                        'allow' => true,
				                        'roles' => ['@'],
			                    ],
		                		[
				                		'actions' => ['login', 'register', 'activate-account'],
				                		'allow' => true,
				                		'roles' => ['?'],
		                		],
		                		[
		                				'actions' => ['index', 'maintenance'],
		                				'allow' => true,
		                		],
		                		[
		                				'actions' => ['signup', 'contact', 'request-password-reset', 'reset-password'],
		                				'allow' => true,
		                		],
		                ],
	            ],
	            'verbs' => [
		                'class' => VerbFilter::className(),
		                'actions' => [
		                    	'logout' => ['post'],
		                ],
	            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function actions() {
        return [
            'error' => [
                	'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => $this->getCaptchaActionConfig(),
        ];
    }
    
    public function beforeAction($action) {
    	if(!parent::beforeAction($action)) {
    		return false;
    	}
    	
    	return true;
    }

    public function actionMaintenance() {
        if(!Yii::$app->params['maintenance']) {
            return $this->redirect(['order/list']);
        }       
        return $this->render('maintenance');
    }
    
    /**
     * Displays homepage.
     *
     * @return mixed
     */
    public function actionIndex() {
    	if(!Yii::$app->user->isGuest) {
    		return $this->redirect(['order/list']);
    	}
    	else {
            $this->layout = 'guest';
    		return $this->redirect(['site/login']);
    	}
    }
    
    /**
     * Logs in a user.
     *
     * @return mixed
     */
    public function actionLogin() {
    	if(!Yii::$app->user->isGuest) {
    		return $this->goHome();
    	}

        $this->layout = 'guest';
    
    	$model = new LoginForm();
    	
    	if(Yii::$app->request->isAjax && $model->load(Yii::$app->request->post())) {
    		Yii::$app->response->format = Response::FORMAT_JSON;
    		return \yii\widgets\ActiveForm::validate($model);
    	}

    	if($model->load(Yii::$app->request->post()) && $model->login()) {
    		if(StringHelper::startsWith(Yii::$app->getUser()->getReturnUrl(), Yii::$app->urlManager->getBaseUrl().'/admin/')) {
    			return $this->goHome();
    		}
    		
    		return $this->goBack();
    	}
    	else {
    		return $this->render('login', [
    				'model' => $model,
    		]);
    	}
    }

    public function actionRegister() {
        $registerForm = new \customer\models\RegisterForm();

        if (Yii::$app->request->isAjax && $registerForm->load(Yii::$app->request->post())) {
            $registerForm->scenario = \customer\models\RegisterForm::SCENARIO_AJAX_VALIDATION;
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ActiveForm::validate($registerForm);
        }

        if ($registerForm->load(Yii::$app->request->post())) {
            try {
                if ($registerForm->register($flashMessage)) {
                    $flashMessage = Yii::t('web', 'Your account has been created.') . (isset($flashMessage) ? ' ' . $flashMessage : '');
                    Yii::$app->getSession()->setFlash('success', $flashMessage);
                    return $this->redirect(['site/register']);
                }
                else {
                    Yii::$app->getSession()->setFlash('error', Yii::t('web', 'The form contains invalid data!'));
                }
            }
            catch (\Exception $e) {
                Yii::error($e->getMessage());
                Yii::error($e->getTraceAsString());
                Yii::$app->getSession()->setFlash('error', Yii::t('web', 'Internal server error!'));
            }
        }

        $this->layout = 'guest';
        return $this->render('register', ['registerForm' => $registerForm]);
    }

    public function actionActivateAccount($token) {
        $tokenRecord = Token::findOne(['token' => $token]);
        if (!empty($tokenRecord) && $tokenRecord->token_type_id === TokenType::findOne(['symbol' => Utility::TOKEN_TYPE_CUSTOMER_ACTIVATE_ACCOUNT])->id) {
            $user = User::findOne($tokenRecord->user_id);
            try {
                if (time() <= strtotime($tokenRecord->date_expiration)) {
                    self::deleteTokenActivateAccount($tokenRecord, $user);
                    Yii::$app->getSession()->setFlash('success', Yii::t('web', 'Your account has been activated. Now you can log in.'));
                }
                else {
                    self::generateAndSendNewActivationLink($tokenRecord, $user);
                    Yii::$app->getSession()->setFlash('info', Yii::t('web', 'The activation link has expired. An email with a new link has been sent to your email address.'));
                }
            }
            catch (\Exception $e) {
                Yii::error($e->getMessage());
                Yii::error($e->getTraceAsString());
                Yii::$app->getSession()->setFlash('error', Yii::t('web', 'Internal server error!'));
            }
        }
        else {
            Yii::$app->getSession()->setFlash('error', Yii::t('web', 'The activation link is invalid or the account has already been activated.'));
        }
        return $this->redirect(['site/index']);
    }

    private static function deleteTokenActivateAccount(&$tokenRecord, &$user) {
        if (!$tokenRecord->delete()) {
            throw new \Exception('Cannot delete the token!');
        }
        $user->can_logged = 1;
        if (!$user->save()) {
            throw new \Exception('Cannot update the user!');
        }
    }

    private static function generateAndSendNewActivationLink(&$tokenRecord, $user) {
        $tokenRecord->date_expiration = date('Y-m-d H:i:s', time() + Yii::$app->params['customerTimeForAccountActivation']);
        do {
            $tokenRecord->token = Yii::$app->security->generateRandomString();
        } while (!$tokenRecord->validate(['token']));
        if (!$tokenRecord->save()) {
            throw new \Exception('Cannot update the token!');
        }

        $messageProvider = new MessageProvider();
        $template = Utility::EMAIL_TYPE_CUSTOMER_NEW_ACTIVATION_LINK;
        $mode = MessageProvider::MODE_EMAIL;
        $activationLink = Yii::$app->urlManager->createAbsoluteUrl(['site/activate-account', 'token' => $tokenRecord->token]);
        $tags['{activation_link}'] = $activationLink;
        $tags['{app_name}'] = Yii::$app->name;
        $tags['{team_name}'] = Yii::$app->name;
        $sent = $messageProvider->send($user, NULL, $template, $mode, NULL, $tags, false);
        if (!$sent) {
            throw new \Exception('Cannot send an email!');
        }
    }
    
    /**
     * Logs out the current user.
     *
     * @return mixed
     */
    public function actionLogout() {
    	Yii::$app->user->logout();
    
    	return $this->goHome();
    }
    
    /**
     * Displays contact page.
     *
     * @return mixed
     */
    public function actionContact() {
    	$model = new ContactForm();
    	
    	if($model->load(Yii::$app->request->post()) && $model->validate()) {
    		
    		if($model->sendEmail(Yii::$app->params['supportEmail'])) {
    			Yii::$app->session->setFlash('warning', Yii::t('web', 'Thank you for contacting us. We will respond to you as soon as possible.'));
    		}
    		else {
    			Yii::$app->session->setFlash('error', Yii::t('web', 'There was an error sending email.'));
    		}
    
    		return $this->refresh();
    	}
    	else {
    		$this->regenerateCaptcha();
    		
    		return $this->render('contact', [
    				'model' => $model,
    		]);
    	}
    }
    
    /**
     * Signs user up.
     *
     * @return mixed
     */
    public function actionSignup() {
    	$model = new RegisterForm();    	
    	
    	if(Yii::$app->request->isAjax && $model->load(Yii::$app->request->post())) {
    		Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
    		return \yii\widgets\ActiveForm::validate($model);
    	}
    	
    	if($model->load(Yii::$app->request->post())) {
    		if($model->register()) {
	    		return $this->redirect(['site/login']);
    		}
    	}    	
    	
    	$this->regenerateCaptcha();
    
    	return $this->render('signup', [
    			'model' => $model,
    	]);
    }
    
    /**
     * Requests password reset.
     *
     * @return mixed
     */
    public function actionRequestPasswordReset() {
    	$this->layout = 'guest';
    	$model = new PasswordResetRequestForm();
    	if($model->load(Yii::$app->request->post()) && $model->validate()) {
    		if($model->sendToken()) {
    			if($model->isEmailMode()) {
    				Yii::$app->session->setFlash('warning', Yii::t('web', 'Check your email for further instructions.'));
    			}
    			
    			return $this->goHome();
    		}
    		else {
    			Yii::$app->session->setFlash('error', Yii::t('web', 'Sorry, we are unable to reset password for email provided.'));
    		}
    	}
    
    	$this->regenerateCaptcha();
    	
    	return $this->render('requestPasswordResetToken', [
    			'model' => $model,
    	]);
    }    
    
    /**
     * Resets password.
     *
     * @param string $token
     * @return mixed
     * @throws BadRequestHttpException
     */
    public function actionResetPassword($token) {
    	$this->layout = 'guest';
    	try {
    		$model = new ResetPasswordForm($token);
    	}
    	catch(InvalidParamException $e) {
    		Yii::$app->getSession()->setFlash('error', $e->getMessage());
    		
    		return $this->goHome();
    	}
    	catch(InvalidValueException $e2) {
    		Yii::$app->getSession()->setFlash('error', $e2->getMessage());
    		
    		return $this->goHome();
    	}
    
    	if($model->load(Yii::$app->request->post()) && $model->validate() && $model->resetPassword()) {
    		Yii::$app->session->setFlash('warning', Yii::t('web', 'New password was saved.'));
    
    		return $this->goHome();
    	}
    
    	return $this->render('resetPassword', [
    			'model' => $model,
    			]);
    }

    public function actionFindCompanies() {
        $request = Yii::$app->request;
        $term = $request->get('term');
        $page = $request->get('page', 1);
        $size = $request->get('size');

		if(empty($term) || empty($page) || empty($size)) {
			Yii::$app->end();
		}

        $query = Company::find()->andWhere(['like', 'name', $term]);
        $matchedCompaniesCount = $query->count();
        $companies = $query->limit($size)->offset($size*($page-1))->all();

        echo json_encode([
            'items' => array_map(function($company) {
                return ['id' => $company->id, 'name' => '[' . $company->id . '] '. $company->name]; 
            }, $companies), 
            'more' => (intval($matchedCompaniesCount) > $page*$size)]);
        Yii::$app->end();
    }

    public function actionGetProductVariantForm() {
        $request = \Yii::$app->request;
                
        if($request->isAjax) {
            echo $this->renderAjax('/inc/product_variant', [
                'productVariantForm' => new ProductVariantForm(),
                'prefix' => $request->get('prefix'),
                'formId' => $request->get('formId'),
            	'isWarehouse' => !strstr($request->getReferrer(), Utility::PRODUCT_TYPE_OFFERED_PRODUCT),
            ]);
        }
    }
}
