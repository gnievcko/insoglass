<?php
namespace customer\controllers\documents;

use Yii;
use yii\filters\AccessControl;
use common\helpers\Utility;
use common\documents\forms\OfferDetailsProductForm;
use common\documents\DocumentType;
use common\documents\dataSources\OfferDetailsDataSource;
use common\repositories\sql\OfferDetailsRepository;
use common\models\ar\Currency;
use common\documents\sections\OfferDetailsProducts;
use common\helpers\StringHelper;
use common\documents\DocumentController;
use common\models\ar\Order;
use common\documents\DocumentTypeUtility;

class OfferDetailsController extends DocumentController {
	
	public function behaviors() {
		$actions = ['index', 'getOfferDetailsProductRow', 'calculateSummary'];
		return [
				'access' => [
						'class' => AccessControl::className(),
						'only' => $actions,
						'rules' => [
								[
										'actions' => $actions,
										'allow' => true,
										'roles' => [Utility::ROLE_SALESMAN],
								],
						],
				],
		];
	}

    public function actionIndex($orderId = 0, $edit = false) {
    	if(!in_array(DocumentType::OFFER_DETAILS, DocumentTypeUtility::getAvailableDocuments())) {
    		throw new \yii\web\NotFoundHttpException(Yii::t('web', 'Element does not exist or you do not have permission.'));
    	}
    	
    	$order = Order::findOne($orderId);
    	if(!empty($order) && !empty($order->is_deleted)) {
    		throw new \yii\web\NotFoundHttpException(Yii::t('web', 'Element does not exist or you do not have permission.'));
    	}
    	
        if(Yii::$app->request->get('fastPrinting') == 1) {
            $this->renderPdf = true;
            $this->save = false;
        }
        $this->orderId = $orderId;
        $this->title = \Yii::t('documents', 'Offer details protocol');
        $this->edit = $edit;
    	$dataSource = new OfferDetailsDataSource(new OfferDetailsRepository(), $orderId);
    	
    	return $this->createAndView(DocumentType::OFFER_DETAILS, $dataSource);
    }

    public function actionGetOfferDetailsProductRow($prefix) {
    	return $this->renderFile('@common/documents/html/views/offer-details-products/product-row.php', [
    			'product' => new OfferDetailsProductForm(),
    			'form' => new \yii\widgets\ActiveForm(),
    			'prefix' => $prefix,
    			'currencies' => Currency::find()->all(),
    	]);
    }
    
    public function actionCalculateSummary() {
    	$products = new OfferDetailsProducts();
    	$products->loadInput(Yii::$app->request->post());
    	
    	\Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
		return [
				'amount' => StringHelper::getFormattedCost($products->summaryCost), 
				'currency' => $products->currency->symbol,
		];
    }
    
    public function actionTotalPrice($priceUnit, $count) {
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        return ['totalPrice' => StringHelper::getFormattedCost(bcmul((float)$priceUnit, (float)$count, 2))];
    }
}