<?php
namespace customer\controllers;

use Yii;
use frontend\components\Controller;
use yii\filters\AccessControl;
use yii\web\Response;
use yii\widgets\ActiveForm;

use common\models\ar\User;
use common\helpers\Utility;
use customer\models\UserAccountForm;
use common\models\aq\UserCompanySalesmanQuery;
use common\models\aq\RoleQuery;
use common\models\ar\Company;

class UserController extends Controller {

	public function behaviors() {
		$allActions = [
				'account', 'accountEdit',
		];

		return [
				'access' => [
						'class' => AccessControl::className(),
						'only' => $allActions,
						'rules' => [
								[
										'actions' => $allActions,
										'allow' => true,
										'roles' => [Utility::ROLE_CLIENT],
								],
						],
				],
		];
	}

	public function actionAccount(){
		$user = User::findOne(Yii::$app->user->id);
        $company = Company::findOne(Yii::$app->user->identity->company_id);
		$companyGroups = UserCompanySalesmanQuery::getCompanyGroupsTranslationsByUserId($user->id)->all();
		$warehouses = Yii::$app->params['isMoreWarehousesAvailable'] ? UserWarehouseQuery::getWarehousesByUserId($id)->all() : [];
		$roles = RoleQuery::getRolesTranslationsByUserId($user->id)->one()['name'];
		return $this->render('details', [
				'model' => $user,
                'company' => $company,
				'companyGroups' => $companyGroups,
				'warehouses' => $warehouses,
				'roles' => $roles,
		]);

	}

	public function actionAccountEdit(){
		$user = User::findOne(Yii::$app->user->id);
		$model = new UserAccountForm($user);
		$model->setEditScenario();

		if(Yii::$app->request->isAjax && $model->load(Yii::$app->request->post())) {
			Yii::$app->response->format = Response::FORMAT_JSON;
			return ActiveForm::validate($model);
		}

		if($model->load(Yii::$app->request->post()) && $model->validate()) {
			$model->saveData();
			Yii::$app->getSession()->setFlash('success', Yii::t('web', 'Informaction has been updated successfully.'));
			return $this->redirect(['account']);
		}

		return $this->render('form', [
				'model' => $model
		]);

	}
}
