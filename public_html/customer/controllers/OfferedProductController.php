<?php
namespace customer\controllers;

use Yii;
use common\models\ar\Currency;
use yii\helpers\ArrayHelper;
use frontend\components\Controller;
use frontend\models\OfferedProductModel;
use yii\filters\AccessControl;
use common\models\aq\OfferedProductQuery;
use common\helpers\Utility;
use common\models\ar\OfferedProductCategoryTranslation;
use frontend\models\OfferedProductListForm;
use yii\data\Pagination;
use yii\helpers\Url;
use common\models\aq\OfferedProductCategoryQuery;
use common\models\ar\OfferedProduct;
use common\models\ar\Product;
use common\models\ar\Language;
use common\models\aq\OfferedProductPhotoQuery;
use frontend\models\ProductAttributeForm;
use yii\widgets\ActiveForm;
use yii\web\Response;
use yii\filters\VerbFilter;
use yii\helpers\Html;
use yii\db\Expression;
use common\models\aq\ProductPhotoQuery;

class OfferedProductController extends Controller {

	public function behaviors() {
		$allActions = ['list', 'details', 'find', 'create', 'edit', 'delete', 'get-main-photo',];

		return [
				'access' => [
						'class' => AccessControl::className(),
						'only' => $allActions,
						'rules' => [
								[
										'actions' => ['list', 'edit', 'create', 'find', 'delete', 'get-main-photo'],
										'allow' => true, 
										'roles' => [Utility::ROLE_SALESMAN],
								],
								[
										'actions' => ['details'],
										'allow' => true,
										'roles' => [Utility::ROLE_SALESMAN, Utility::ROLE_STOREKEEPER],
								],
                                [
                                        'actions' => ['find', 'findAttachmentsIds'],
										'allow' => true, 
										'roles' => [Utility::ROLE_SALESMAN, Utility::ROLE_CLIENT],
                                ],
						],
				],
				'verbs' => [
						'class' => VerbFilter::className(),
						'actions' => [
								'delete' => ['post'],
						],
				],
		];
	}

	public function actionList() {
		$request = Yii::$app->request;
		$listForm = new OfferedProductListForm();
		if(!$request->isAjax) {
			$categoryOptions = OfferedProductCategoryQuery::getCategoryList();
			$listForm->allCategoriesCount = count($categoryOptions);
		}
		$listForm->load($request->get());

		$itemsQuery = OfferedProductQuery::getActiveOfferedProductsQuery($listForm->toArray());
		$pages = new Pagination([
				'totalCount' => $itemsQuery->count(),
				'page' => $listForm->page,
				'defaultPageSize' => \Yii::$app->params['defaultPageSize'],
		]);
		$items = $itemsQuery->limit($pages->defaultPageSize)->offset($pages->page * $pages->defaultPageSize)->all();

		$listData = [
				'items' => $items,
				'pages' => $pages,
				'listForm' => $listForm,
		];

		if($request->isAjax) {
			echo json_encode([
					'view' => $this->renderAjax('_products', $listData),
					'url' => Url::current([], true),
			]);
		}
		else {
			$listData['categoryTranslations'] = $categoryOptions;
			return $this->render('list', $listData);
		}
	}
	
	public function actionGetMainPhoto() {
		$request = Yii::$app->request;
		$productId = $request->get('id');
		$type = $request->get('type');
		if(!$request->isAjax || empty($productId) || empty($type)) {
			return '';
		}
	
		if($type == Utility::PRODUCT_TYPE_OFFERED_PRODUCT) {
			$urlImage = OfferedProductPhotoQuery::getMainPhoto($productId);
		}
		elseif($type == Utility::PRODUCT_TYPE_PRODUCT) {
			$urlImage = ProductPhotoQuery::getMainPhoto($productId);
		}
		
		return !empty($urlImage) ? '<img src="'.\common\helpers\UploadHelper::getNormalPath($urlImage, 'product_photo').'"/>' : '';
	}

	public function actionDetails($id, $type) {
		if(($type == Utility::PRODUCT_TYPE_OFFERED_PRODUCT && (
						empty($product = OfferedProduct::findOne($id)) || empty($product->is_active) ||
						!empty($product->is_artificial) || !empty($product->parent_offered_product_id)
				))
				||
				($type == Utility::PRODUCT_TYPE_PRODUCT && (
						empty($product = Product::findOne($id)) || empty($product->is_visible_for_client) ||
						empty($product->is_active) || !empty($product->parent_product_id)
				))) {
			//Yii::$app->getSession()->setFlash('error', Yii::t('web', 'An offered product does not exist.'));
			//return $this->redirect(['site/index']);
			throw new \yii\web\NotFoundHttpException(Yii::t('web', 'Element does not exist or you do not have permission.'));
		}
		
		if(!in_array($type, [Utility::PRODUCT_TYPE_OFFERED_PRODUCT, Utility::PRODUCT_TYPE_PRODUCT])) {
			throw new \yii\web\NotFoundHttpException();
		}
		$data = OfferedProductQuery::getDetails($id, $type);
		$attachments = OfferedProductQuery::getAttachments($id);

		$photos = [];
		$photosRaw = OfferedProductPhotoQuery::getPhotosByProductId($id, $type);
		if(!empty($photosRaw)) {
			$photos = array_map(function($row) {
				return [
						'url' => $row['urlPhoto'],
						'src' => $row['urlThumbnail'],
						'options' => [
								'title' => $row['description'],
						],
				];
			}, $photosRaw);
		}

		return $this->render('details', [
				'data' => $data,
				'photos' => $photos,
				'attachments' => $attachments,
		]);
	}

	public function actionDelete() {
		$id = Yii::$app->request->post('id');
		$type = Yii::$app->request->post('type');

		try {
			if(($type == Utility::PRODUCT_TYPE_OFFERED_PRODUCT && (
					empty($product = OfferedProduct::findOne($id)) || empty($product->is_active) ||
					!empty($product->is_artificial) || !empty($product->parent_offered_product_id)
					))
					||
					($type == Utility::PRODUCT_TYPE_PRODUCT && (
							empty($product = Product::findOne($id)) || empty($product->is_visible_for_client) ||
							empty($product->is_active) || !empty($product->parent_product_id)
					))) {
				throw new \Exception('The product cannot be deleted: '.$id.', '.$type);
			}

			$product = $type == Utility::PRODUCT_TYPE_OFFERED_PRODUCT ? OfferedProduct::findOne($id) : Product::findOne($id);
			$product->is_active = 0;
			$product->date_deletion = new Expression('NOW()');
			$product->save();
			
			$versions = $type == Utility::PRODUCT_TYPE_OFFERED_PRODUCT ? $product->offeredProducts : $product->products;
			if(!empty($versions)) {
				foreach($versions as $version) {
					$version->is_active = 0;
					$version->date_deletion = new Expression('NOW()');
					$version->save();
				}
			}

			Yii::$app->getSession()->setFlash('success', Yii::t('web', 'The offered product has been deleted.'));
		}
		catch(\Exception $e) {
			Yii::error(print_r($e->getMessage(), true));
			Yii::$app->getSession()->setFlash('error', Yii::t('web', 'The offered product cannot be deleted. Please contact with the administrator.'));
		}

		if(strpos(Yii::$app->request->getReferrer(), Yii::$app->controller->id.'/details') !== false) {
			return $this->redirect(['list']);
		}
		return $this->redirect(Yii::$app->request->getReferrer());
	}

    public function actionFind() {
        $request = Yii::$app->request;
        $term = $request->get('term');
        $page = $request->get('page', 1);
        $size = $request->get('size');

		if(empty($term) || empty($page) || empty($size)) {
			Yii::$app->end();
		}

        $offeredProductsQuery = OfferedProductQuery::findOfferedProducts(['name' => $term]);
        $offeredProductsCount = $offeredProductsQuery->count();
        $offeredProducts = $offeredProductsQuery->limit($size)->offset($size*($page-1))->all();
        
        // TODO: Sprawdzić, czy da się to "mądrzej" zrobić funkcjami tablicowymi w PHP lub ArrayHelper, bo samo "map" nie wystarcza
        $items = [];
        if(!empty($offeredProducts)) {
        	foreach($offeredProducts as $itemRaw) {
        		$item = [];
        		$item['id'] = $itemRaw['id'];
        		$item['name'] = !empty($itemRaw['parentName']) ? Html::encode($itemRaw['parentName'].' ('.mb_strtolower($itemRaw['name'], 'UTF-8').')') : Html::encode($itemRaw['name']);
        		$item['type'] = $itemRaw['type'];
        		$item['unit'] = $itemRaw['unit'];
                $item['price'] = $itemRaw['price'];
        		$items[] = $item;
        	}
        }

        echo json_encode([
            'items' => $items,
            'more' => (intval($offeredProductsCount) > $page*$size)
        ]);
    }

    public function actionCreate() {
        $productModel = new OfferedProductModel();

        $request = Yii::$app->request;
        if ($request->isAjax && $request->isPost) {
            $productModel->load($request->post());
            Yii::$app->response->format = Response::FORMAT_JSON;

            return ActiveForm::validateMultiple($productModel->getOfferedProductTranslationsForms())
                + ActiveForm::validateMultiple($productModel->getProductVariantsForms())
                + ActiveForm::validateMultiple($productModel->getOfferedProductPhotosForms())
                + ActiveForm::validateMultiple($productModel->getDocumentsForms())
                + ActiveForm::validate($productModel->getOfferedProductForm());
        }

        if($request->isPost) {
            $productModel->load($request->post());

            try {
                if($productModel->save()) {
                    Yii::$app->getSession()->setFlash('success', Yii::t('web', 'Product has been added successfully.'));
                    return $this->redirect(['offered-product/details', 'id' => $productModel->getOfferedProduct()->id, 'type' => Utility::PRODUCT_TYPE_OFFERED_PRODUCT]);
                }
            }
            catch(\Exception $e) {
				Yii::$app->getSession()->setFlash('error', Yii::t('web', 'Product data cannot be saved. Check provided values.'));
				Yii::error($e->getMessage());
				Yii::error($e->getTraceAsString());
            }
        }

        $language = Language::find()->where(['symbol' => \Yii::$app->language])->one();
        $offeredProductForm = $productModel->getOfferedProductForm();
        $offeredProductCategories = OfferedProductCategoryTranslation::find()
            ->andWhere(['in', 'offered_product_category_id', empty($offeredProductForm->categoriesIds) ? [] : $offeredProductForm->categoriesIds])
            ->andWhere(['language_id' => $language->id])
            ->orderBy('name')
            ->all();

        $forms = $productModel->getAllForms();
        return $this->render('create', $forms + [
            'currencies' => ArrayHelper::map(Currency::find()->all(), 'id', 'short_symbol'),
            'offeredProductCategories' => $offeredProductCategories,
        ]);
    }

    public function actionEdit($id) {
		if(Yii::$app->request->get('type') === Utility::PRODUCT_TYPE_PRODUCT ) {
			return $this->redirect(['product/edit/', 'id' => $id, 'type' => Utility::PRODUCT_TYPE_PRODUCT ]);
		}
        if(empty(OfferedProduct::findOne($id))) {
            throw new \yii\web\NotFoundHttpException();
        }
        $productModel = new OfferedProductModel($id);

        $request = Yii::$app->request;
        if ($request->isAjax && $request->isPost) {
            $productModel->load($request->post());
            Yii::$app->response->format = Response::FORMAT_JSON;

            return ActiveForm::validateMultiple($productModel->getOfferedProductTranslationsForms())
                + ActiveForm::validateMultiple($productModel->getProductVariantsForms())
                + ActiveForm::validateMultiple($productModel->getOfferedProductPhotosForms())
                + ActiveForm::validateMultiple($productModel->getDocumentsForms())
                + ActiveForm::validate($productModel->getOfferedProductForm());
        }

        if($request->isPost) {
            $productModel->load($request->post());

            try {
                if($productModel->update()) {
                    Yii::$app->getSession()->setFlash('success', Yii::t('web', 'Product data has been updated successfully.'));
                    return $this->redirect(['offered-product/details', 'id' => $productModel->getOfferedProduct()->id, 'type' => Utility::PRODUCT_TYPE_OFFERED_PRODUCT]);
                }
            }
            catch(\Exception $e) {
				Yii::$app->getSession()->setFlash('error', Yii::t('web', 'Product data cannot be saved. Check provided values.'));
				Yii::error($e->getMessage());
				Yii::error($e->getTraceAsString());
            }
        }

        $language = Language::find()->where(['symbol' => \Yii::$app->language])->one();
        $offeredProductForm = $productModel->getOfferedProductForm();
        $offeredProductCategories = OfferedProductCategoryTranslation::find()
            ->andWhere(['in', 'offered_product_category_id', empty($offeredProductForm->categoriesIds) ? [] : $offeredProductForm->categoriesIds])
            ->andWhere(['language_id' => $language->id])
            ->orderBy('name')
            ->all();

        $forms = $productModel->getAllForms();
        return $this->render('edit', $forms + [
            'currencies' => ArrayHelper::map(Currency::find()->all(), 'id', 'short_symbol'),
            'offeredProductCategories' => $offeredProductCategories,
        ]);
    }
    
    public function actionFindAttachmentsIds() {
        $request = Yii::$app->request;
        $productId = $request->get('productId');
        $withAttributes = Yii::$app->params['isProductAttributesVisible'];
        
        $productAttributeForm = new ProductAttributeForm();
        $offeredProduct = OfferedProduct::findOne($productId);

        $productAttributeForm->loadFromObjects(!empty($offeredProduct) ? $offeredProduct->offeredProductAttributes : []);
        return json_encode($productAttributeForm->fields);
    }
}
