<?php
$params = array_merge(
	    require(__DIR__ . '/../../common/config/params.php'),
	    require(__DIR__ . '/../../common/config/params-local.php'),
	    require(__DIR__ . '/params.php'),
	    require(__DIR__ . '/params-local.php')
);

return [
	    'id' => 'manufactory-manager-customer',
	    'name' => 'INSOGLAS',
	    'basePath' => dirname(__DIR__),
	    'bootstrap' => ['log'],
	    'controllerNamespace' => 'customer\controllers',
	    'components' => [
			    'request' => [
			    		'class' => 'common\components\Request',
   						'web'=> '/customer/web',
	    		],
			    'urlManager' => [
					    'enablePrettyUrl' => true,
					    'showScriptName' => false,
		            	'enableStrictParsing' => false,
					    'rules' => [
					    		'/' => 'site/index',
					    		
                                'documents/<controller:[a-z\-]+>/<action:[a-z\-]+>' => 'documents/<controller>/<action>',
					    		'<controller:[a-z\-]+>/<id:\d+>'=>'<controller>/view',
					    		'<controller:[a-z\-]+>/<action:[a-z\-]+>/<id:\d+>-<title:.*?>'=>'<controller>/<action>',
					    		'<controller:[a-z\-]+>/<action:[a-z\-]+>/<id:\d+>'=>'<controller>/<action>',
                                '<controller:[a-z\-]+>/<action:[a-z\-]+>/<name:.*?>'=>'<controller>/<action>',
					    		'<controller:[a-z\-]+>/<action:[a-z\-]+>/<title:.*?>'=>'<controller>/<action>',
					    		'<controller:[a-z\-]+>/<action:[a-z\-]+>'=>'<controller>/<action>',
					    ]
				],
		        'user' => [
			            'identityClass' => 'common\models\ar\User',
						'loginUrl'		=> '@web/site/login',
                        'authTimeout' => 86400,
		        ],
                'session' => [
                    	'class' => 'yii\web\Session',
                    	'timeout' => 86400,
                		'cookieParams' => ['lifetime' => 7 * 24 * 60 * 60]
                ],
		        'log' => [
			            'traceLevel' => YII_DEBUG ? 3 : 0,
			            'targets' => [
				                [
					                    'class' => 'yii\log\FileTarget',
					                    'levels' => ['error', 'warning'],
					                    'logVars' => ['_GET','_POST'],
				                ],
				                [
						                'class' => 'yii\log\FileTarget',
						                'levels' => ['info'],
						                'logFile' => '@runtime/logs/info.log',
						                'logVars' => [],
						                'categories' => ['info']
				                ],
			            ],
		        ],
		        'errorHandler' => [
		            	'errorAction' => 'site/error',
		        ],
	    ],
	    /*'on beforeRequest' => function($event) {
	    	if(Yii::$app->params['maintenance']) {
	    		Yii::$app->catchAll = ['site/maintenance'];
	    	}
	    },*/
	    'params' => $params,
];
