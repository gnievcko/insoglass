<?php
use yii\helpers\Html;

$fieldId = "{$formName}-colourId";
$inputOptions = ['type' => 'hidden', 'class' => 'form-control colour-id', 'id' => $fieldId];
?>

<div class="colour-picker-div">
	<div class="row">
        <div class="col-xs-12">
    		<button type="button" class="btn btn-default btn-sm open-modal"><?= Yii::t('web', 'Choose colour') ?></button>
    		<div class="colour-rectangle" style="background-color:<?= $form->ralRgbHash ?>"></div>
    		<?= Html::activeTextInput($form, "[{$prefix}]ralColourSymbol", ['class' => 'colour-symbol', 'readonly' => true]) ?>
    		
    		<?= Html::activeTextInput($form, "[{$prefix}]ralColourId", ['type' => 'hidden', 'class' => 'form-control colour-id']) ?>
    		<?= Html::activeTextInput($form, "[{$prefix}]ralRgbHash", ['type' => 'hidden', 'class' => 'form-control colour-rgb-hash']) ?>
        </div>
        <!-- Modal -->
        <div class="modal fade" role="dialog">
    		<div class="modal-dialog">
                <!-- Modal content-->
                <div class="modal-content">
                	<div class="modal-header">
                  		<div class="col-xs-12">
                      		<div class="row">
                      			<div style=" display:none; position: absolute;top: 5px;left: 10px;width: 30px;height: 30px;" class="revert-glyphicon glyphicon glyphicon-chevron-left"></div>
                      			<div class="modal-title text-center">
                        			<span><?= Yii::t('web', 'RAL colour chart')?></span>
                        			<span class="alt-title"></span>
                    			</div>
                    			<div class="text-right">
                      				<button type="button" class="close" data-dismiss="modal">&times;</button>
                      			</div>
                      		</div>
                 		</div> 	
                  	</div>
                  	<div class="modal-body">
                    
                  	</div>
                  	<div class="modal-footer">
                  		<input type="text" placeholder="<?= Yii::t('web', 'RAL code')?>">
                    	<button type="button" class="btn search-button" data-title="<?= Yii::t('web', 'Search result') ?>"><?= Yii::t('web', 'Search') ?></button>
                  	</div>
            	</div>
          	</div>
    	</div>
	</div>
</div>