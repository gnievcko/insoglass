<?php
use yii\web\JqueryAsset;
use customer\helpers\MenuHelper;

$this->registerJsFile('@web/js/layout/leftMenu.js?'.uniqid(), ['depends' => [JqueryAsset::className()]]);


//Yii::$app->getRequest()->getHostInfo();
?>
<nav class="navbar navbar-default ">
  
    <!-- Brand and toggle get grouped for better mobile display -->
    <div class="navbar-header">
      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-2" aria-expanded="false">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button> 
    </div>

    <!-- Collect the nav links, forms, and other content for toggling -->
    <div class="collapse navbar-collapse nav-padding" id="bs-example-navbar-collapse-2">
		<?php echo MenuHelper::leftMenuRender($menus); ?>
	</div>
	
</nav>