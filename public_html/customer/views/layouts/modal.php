<?php
/* @var $this Yii\web\View */
/* @var $content string */

use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use frontend\assets\AppAsset;
use common\helpers\Utility;
use common\widgets\Alert;
use common\helpers\StringHelper;
use frontend\helpers\MenuHelper;
use yii\web\JqueryAsset;

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
		<meta charset="<?= Yii::$app->charset ?>">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<?= Html::csrfMetaTags() ?>
		<title><?= Html::encode($this->title) ?></title>
		<link rel='shortcut icon' type='image/x-icon' href='/favicon.png' />
		<?php $this->head() ?>
		<?php if(YII_ENV_PROD) { ?>
	<?php } ?>
	<style>
		.required .help-block::before{
			content:'<?= Yii::t('web', 'Required')?>';
		}
	</style>
</head>
<body>
<?php $this->beginBody() ?>
	<div style="overflow:hidden">
		<div class="col-sm-12">
			<?= Alert::widget() ?>
			<?= $content ?>
		</div>
	</div>
<?php $this->endBody() ?>

</body>
</html>
<?php $this->endPage() ?>
