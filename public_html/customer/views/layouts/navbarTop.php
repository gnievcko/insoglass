<?php

use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\helpers\Url;
use yii\bootstrap\NavBar;
use common\helpers\StringHelper;
use frontend\helpers\MenuHelper;
use common\models\ar\User;
?>
<nav class="navbar navbar-default">
  <div class="container-fluid">
    <!-- Brand and toggle get grouped for better mobile display -->
    <div class="navbar-header">
      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
      <a href=" <?php echo Yii::$app->homeUrl ?>" class="navbar-brand logo">
      	<?php echo '<img  src="'.Yii::$app->getUrlManager()->getBaseUrl().'/images/logo_insoglas.png" />'.str_replace(['http://', 'https://', 'www.', '.'], ['', '', '', '<span class="colorize-1">.</span>'],''); ?>
      </a>
    </div>

    <!-- Collect the nav links, forms, and other content for toggling -->
    <div class="collapse navbar-collapse topPaddingOption" id="bs-example-navbar-collapse-1">
      <ul class="nav navbar-nav navbar-left" id="nav">
      <?php echo MenuHelper::topMenuRender($menus); ?>

      </ul>
      <ul class="nav navbar-nav navbar-right">
        <li class="dropdown dropdown-slide">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
          <span class="userName">
          		<?php
					$src = null;
					$user = Yii::$app->user;
					$userModel = User::findOne($user->id);
					if(empty($userModel) || empty($userModel->url_photo)) {
						$src = Url::to('@web/images/placeholders/user_placeholder.png');
						$alt = "user_placeholder";
						echo '<img src=' . $src .' alt="..." class="img-circle menu-avatar">';
					} 
					else {
                        $alt = "user_avatar";
						$src = \common\helpers\UploadHelper::getNormalPath($userModel->url_photo, $alt);
						echo '<img src=' . $src .' alt="..." class="img-circle menu-avatar">';
					}
				?>
	        	<span><?php 
		        	if(!Yii::$app->user->isGuest){
		          		echo Yii::t('main', 'Welcome') . ', ' . $user->identity->getIdentityName() . '!';
		        	}
				?></span>
				<span class="base-icon caret-icon table-caret"></span>
          </span>
           
           </a>
          <ul class="dropdown-menu dropdown-menu-top">
            <li><?php echo Html::a(Yii::t('main','Your account'), array('/user/account'))?></li>
            <li role="separator" class="divider"></li>
            <li><?php echo Html::a(Yii::t('main', 'Logout'), array('/site/logout'), ['data-method' => 'post'])?></li>
          </ul>
        </li>
      </ul>
    </div><!-- /.navbar-collapse -->
  </div><!-- /.container-fluid -->
</nav>