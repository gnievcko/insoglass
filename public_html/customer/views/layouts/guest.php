<?php
/* @var $this Yii\web\View */
/* @var $content string */

use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use frontend\assets\AppAsset;
use common\helpers\Utility;
use common\widgets\Alert;
use common\helpers\StringHelper;
use yii\helpers\Url;

!Yii::$app->controller->withoutAssets && AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
		<meta charset="<?= Yii::$app->charset ?>">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<?= Html::csrfMetaTags() ?>
		<title><?= Html::encode($this->title) ?></title>
		<link rel='shortcut icon' type='image/x-icon' href="<?= Url::to('@web/favicon.png') ?>" />
		<?php $this->head() ?>
		<?php if(YII_ENV_PROD) { ?>
	<?php } ?>
</head>
<body>
<?php $this->beginBody() ?>
    <?= $content ?>
<?php $this->endBody() ?>

</body>
</html>
<?php $this->endPage() ?>
