<h1>Nagłówek h1</h1>
<h2>Nagłówek h2</h2>
<h3>Nagłówek h3</h3>
<h4>Nagówek h4</h4>
<h5>Nagłówek h5</h5>
<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque iaculis, leo et mattis molestie, nibh nunc bldit sem, sit amet iaculis ante leo eu purus. Donec facilisis rutrum magna, ut volutpat arcu euismod vitae. Mauris enim neque, dictum nec scelerisque quis, sodales non quam. Praesent non augue felis.</p>
<a>Link przekierowujący</a>

<div class="btn btn-link">
	<span class="base-icon link-icon add-icon" aria-hidden="true"></span>Dodaj tutaj
</div>

<div class="alert alert-success" role="alert">Lorem ipsum dolor sit amet, consectetur adipiscing elit.</div>
<div class="alert alert-info" role="alert">Lorem ipsum dolor sit amet, consectetur adipiscing elit.</div>
<div class="alert alert-warning" role="alert">Lorem ipsum dolor sit amet, consectetur adipiscing elit.</div>
<div class="alert alert-danger" role="alert">Lorem ipsum dolor sit amet, consectetur adipiscing elit.</div>

<form>
    <input type="email" class="form-control" id="exampleInputEmail1" placeholder="Email">
 	<textarea class="form-control" rows="3"></textarea>
</form>

<div class="input-group">
  <input type="text" class="form-control" placeholder="Recipient's username" aria-describedby="basic-addon2">
  <span class="input-group-addon input-addon" id="basic-addon2">sztuk</span>
</div>

<!-- <div class="input-group input-shadow "> -->
<!--   <input type="text" class="form-control input-group-right" placeholder="Recipient's username" aria-describedby="basic-addon2"> -->
<!--   <span class="input-group-addon" id="basic-addon2">sztuk</span> -->
<!-- </div> -->

<div class="input-group">
	<input type="text" class="form-control" aria-label="...">
	<div class="input-group-btn">
        <button type="button" class="btn dropdown-toggle input-addon" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Action <span class="glyphicon glyphicon-menu-down" aria-hidden="true"></span></button>
        <ul class="dropdown-menu dropdown-menu-right">
          <li><a href="#">Action</a></li>
          <li><a href="#">Another action</a></li>
          <li><a href="#">Something else here</a></li>
          <li role="separator" class="divider"></li>
          <li><a href="#">Separated link</a></li>
        </ul>
	</div><!-- /btn-group -->
</div><!-- /input-group -->

	<button class="btn btn-default">btn-default</button>
	<button class="btn btn-primary">btn-primary</button>
	<button class="btn btn-info">btn-info</button>
	<button class="btn btn-success">btn-success</button>

<div>
  <input id="checkOption1" type="checkbox" name="field" value="option" class="input-shadow">
  <label for="checkOption1">Checkbox</label>
</div>

<div>
  <input id="checkOption2" type="checkbox" name="field" value="option" class="input-shadow" checked>
  <label for="checkOption2">Checkbox checked</label>
</div>

<div>
  <input id="radioOption1" type="radio" name="field" value="option" class="input-shadow">
  <label for="radioOption1">Radiobox</label>
</div>

<div>
  <input id="radioOption2" type="radio" name="field" value="option" class="input-shadow" checked>
  <label for="radioOption2">Radiobox checked</label>
</div>

<div class="table-responsive">
	<table class="table grid-table">
		<thead>
			<tr>
				<th>Nazwa</th>
				<th>Data utworzenia</th>
				<th>Klient</th>
				<th>Status</th>
				<th>Akcje</th>
			</tr>
		</thead>
		<tbody>
			<tr>
				<td>Oferta na przegląd systemów ppoż</td>
				<td>2016-04-18</td>
				<td>Rockwool Polska Sp. z o.o.</td>
				<td>dostarczony</td>
				<td><span class="glyphicon glyphicon-eye-open" aria-hidden="true"></span></td>
			</tr>
			<tr>
				<td>Oferta na przegląd systemów ppoż</td>
				<td>2016-04-18</td>
				<td>Rockwool Polska Sp. z o.o.</td>
				<td>dostarczony</td>
				<td><span class="glyphicon glyphicon-eye-open" aria-hidden="true"></span></td>
			</tr>
			<tr>
				<td>Oferta na przegląd systemów ppoż</td>
				<td>2016-04-18</td>
				<td>Rockwool Polska Sp. z o.o.</td>
				<td>dostarczony</td>
				<td><span class="glyphicon glyphicon-eye-open" aria-hidden="true"></span></td>
			</tr>
			<tr>
				<td>Oferta na przegląd systemów ppoż</td>
				<td>2016-04-18</td>
				<td>Rockwool Polska Sp. z o.o.</td>
				<td>dostarczony</td>
				<td><span class="glyphicon glyphicon-eye-open" aria-hidden="true"></span></td>
			</tr>
		</tbody>
	</table>
</div>

<div class="table-responsive">
	<table class="table list-grid-table">
		<thead>
			<tr>
				<th>Nazwa</th>
				<th>Data utworzenia</th>
				<th>Klient</th>
				<th>Status</th>
				<th>Akcje</th>
			</tr>
		</thead>
		<tbody>
			<tr>
				<td>Oferta na przegląd systemów ppoż</td>
				<td>2016-04-18</td>
				<td>Rockwool Polska Sp. z o.o.</td>
				<td>dostarczony</td>
				<td><span class="glyphicon glyphicon-eye-open" aria-hidden="true"></span></td>
			</tr>
			<tr>
				<td>Oferta na przegląd systemów ppoż</td>
				<td>2016-04-18</td>
				<td>Rockwool Polska Sp. z o.o.</td>
				<td>dostarczony</td>
				<td><span class="glyphicon glyphicon-eye-open" aria-hidden="true"></span></td>
			</tr>
			<tr>
				<td>Oferta na przegląd systemów ppoż</td>
				<td>2016-04-18</td>
				<td>Rockwool Polska Sp. z o.o.</td>
				<td>dostarczony</td>
				<td><span class="glyphicon glyphicon-eye-open" aria-hidden="true"></span></td>
			</tr>
			<tr>
				<td>Oferta na przegląd systemów ppoż</td>
				<td>2016-04-18</td>
				<td>Rockwool Polska Sp. z o.o.</td>
				<td>dostarczony</td>
				<td><span class="glyphicon glyphicon-eye-open" aria-hidden="true"></span></td>
			</tr>
		</tbody>
	</table>
</div>

<div class="table-responsive">
	<table class="table detail-table">
		<tbody>
			<tr>
				<th>Nazwa</th>
				<td>Oferta na przegląd systemów ppoż</td>
			</tr>
			<tr>
				<th>Data utworzenia</th>
				<td>2016-04-18</td>
			</tr>
			<tr>
				<th>Klient</th>
				<td>Rockwool Polska Sp. z o.o.</td>
			</tr>
			<tr>
				<th>Status</th>
				<td>dostarczony</td>
			</tr>
		</tbody>
	</table>
</div>

<ul class="nav nav-tabs">
  <li role="presentation" class="active"><a href="#">Home</a></li>
  <li role="presentation"><a href="#">Profile</a></li>
  <li role="presentation"><a href="#">Messages</a></li>
</ul>

<nav aria-label="Page navigation">
  <ul class="pagination">
    <li>
      <a href="#" aria-label="Previous">
        <div class="pag-left"></div>
      </a>
    </li>
    <li><a href="#">1</a></li>
    <li><a href="#">2</a></li>
    <li><a href="#">3</a></li>
    <li><a href="#">4</a></li>
    <li><a href="#">5</a></li>
    <li>
      <a href="#" aria-label="Next">
        <div class="pag-right"></div>
      </a>
    </li>
  </ul>
</nav>