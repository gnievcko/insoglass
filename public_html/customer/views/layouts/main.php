<?php
/* @var $this Yii\web\View */
/* @var $content string */

use yii\helpers\Html;
use yii\widgets\Breadcrumbs;
use frontend\assets\AppAsset;
use common\helpers\Utility;
use common\widgets\Alert;
use yii\web\JqueryAsset;
use yii\helpers\Url;

!Yii::$app->controller->withoutAssets && AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
    <head>
        <meta charset="<?= Yii::$app->charset ?>">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <?= Html::csrfMetaTags() ?>
        <title><?= Html::encode($this->title) ?></title>
        <link rel='shortcut icon' type='image/x-icon' href="<?= Url::to('@web/favicon.png') ?>" />
        <?php $this->head() ?>
        <?php if(YII_ENV_PROD) { ?>
        <?php } ?>
        <style>
            .required .help-block::before{
                content:'<?= Yii::t('web', 'Required') ?>';
            }
        </style>
    </head>
    <body>
        <?php $this->beginBody() ?>
        <?php
        $this->registerJsFile('@web/js/layout/hiddenClass.js?' . uniqid(), ['depends' => [JqueryAsset::className()]]);

        $controllerName = Yii::$app->controller->id;
        $actionName = Yii::$app->controller->action->id;

        $menus = [];

        $menus[] = include('menu/order.php');
        ?>
        <!-- if device is wider and equal than 768px -->
        <!-- 		<div class="desktop-design"> 
                    <div class="row-height">-->

        <?php echo Yii::$app->controller->renderPartial('/layouts/navbarTop', ['menus' => $menus]); ?>
        <div class="cont">
            <div class="table-row">
                <div class="pasek">
                    <?php echo Yii::$app->controller->renderPartial('/layouts/navbarLeft', ['menus' => $menus]); ?>
                </div> 
                <div class="right-content">
                    <div class="container">
                        <?=
                        Breadcrumbs::widget([
                            'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
                            'homeLink' => false,
                        ])
                        ?>
                        <?= Alert::widget() ?>
                        <?php //echo Yii::$app->controller->renderPartial('/layouts/index'); ?>

                        <?= $content ?>
                    </div>
                </div>
            </div>
        </div>
        <div class="">
            <?php echo Yii::$app->controller->renderPartial('/layouts/footer'); ?>
        </div>

        <!-- 		</div>	 -->
        <!-- 		if device is narrower than 768 px -->


        <?php $this->endBody() ?>

    </body>
</html>
<?php $this->endPage() ?>
