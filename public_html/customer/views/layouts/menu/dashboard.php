<?php
return [
		'link' => ['dashboard', 'index'],
		'linkUrl' => ['dashboard/index'],
		'text' => Yii::t('web', 'Dashboard'),
		'id' => 'dashboard',
		'menu' => [
				[
						'link' => ['dashboard', 'index'],
						'linkUrl' => ['dashboard/index'],
						'text' => Yii::t('web', 'Summary'),
						'icon' => '<div class="base-icon to-do-list-icon nav-left-icons"></div>',
						'id' => '',
				],
				[
						'link' => ['task', 'create'],
						'linkUrl' => ['task/create'],
						'text' => Yii::t('web', 'Add task'),
						'icon' => '<div class="base-icon add-task-icon nav-left-icons"></div>',
						'id' => '',
				],
				[
						'link' => ['task', 'edit'],
				],
		],		
];
