<?php

use common\helpers\StringHelper;

$clientOption=[];
if(Yii::$app->params['isClientManagementVisibie']) {
    $clientOption[] = [
        'link' => ['client', 'create'],
        'linkUrl' => ['client/create'],
        'text' => Yii::t('web', 'Add'),
        'icon' => '<div class="base-icon add-icon nav-more"></div>',
        'id' => '',
    ];
}

$clientOptions = [
    [
        'link' => ['client', 'list'],
        'linkUrl' => ['client/list'],
        'text' => Yii::t('web', 'List of clients'),
        'icon' => '<div class="base-icon list-icon-list nav-more"></div>',
        'id' => '',
    ],
    [
        'link' => ['client-group', 'list'],
        'linkUrl' => ['client-group/list'],
        'text' => Yii::t('web', 'List of clients groups'),
        'icon' => '<div class="base-icon list-icon-list nav-more"></div>',
        'id' => '',
    ],
    [
        'link' => ['client', 'edit'],
    ],
    [
        'link' => ['client', 'details'],
    ],
    [
        'link' => ['client', 'create-representative'],
    ],
    [
        'link' => ['client', 'edit-representative'],
    ],
    [
        'link' => ['client-group', 'edit'],
    ],
    [
        'link' => ['client-group', 'details'],
    ],
    [
        'link' => ['client-group', 'create'],
    ]
];

$clientOptions = array_merge($clientOption, $clientOptions);

$menuOptions = [
    'link' => ['order', 'index'],
    'linkUrl' => ['order/index'],
    'text' => StringHelper::translateOrderToOffer('main', 'Orders'),
    'id' => 'order',
    'menu' => [
        [
            'link' => ['order', 'list'],
            'linkUrl' => ['order/list'],
            'text' => StringHelper::translateOrderToOffer('main', 'List of orders'),
            'icon' => '<div class="base-icon list-icon-list nav-left-icons"></div>',
            'id' => '',
        ],
        [
            'link' => ['order', 'create'],
            'linkUrl' => ['order/create'],
            'text' => StringHelper::translateOrderToOffer('web', 'Create order'),
            'icon' => '<div class="base-icon add-task-icon nav-left-icons"></div>',
            'id' => '',
        ],
        [
            'link' => ['order', 'details'],
        ],
        [
            'link' => ['order', 'update'],
        ],
        [
            'link' => ['order', 'quick-update'],
        ],
        [
            'link' => ['order', 'copy'],
        ],
        [
            'link' => ['order', 'edit'],
        ],
        [
            'link' => ['order-type', 'list'],
        ],
        [
            'link' => ['order-type', 'details'],
        ],
        [
            'link' => ['order-type', 'create'],
        ],
        [
            'link' => ['order-type', 'edit'],
        ],
    ],
];

return $menuOptions;
