<?php

return [
    'link' => ['report', 'index'],
    'linkUrl' => ['report/index'],
    'text' => Yii::t('web', 'Reports'),
    'id' => 'report',
    'menu' => [
            
    ]
];
