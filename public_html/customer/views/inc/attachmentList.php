<?php
use yii\helpers\StringHelper;
use yii\helpers\Html;
use yii\helpers\Url;
?>

<div class="attachments">
	<?php foreach($attachments as $attachment) { 
		$urlPhoto = Url::to('@web/images/placeholders/attachment');
		if(StringHelper::endsWith($attachment['name'], '.pdf', false)) {
			$urlPhoto .= '-pdf';
		}
		$urlPhoto .= '.png'; ?>
		
		<div class="attachment-row">
			<span class="photo-container">
				<?= '<div class="photo" style="background: url('.$urlPhoto.')"></div>'; ?>
			</span>
          <span class="name"><?= Html::a($attachment['name'], \common\helpers\UploadHelper::getNormalPath($attachment['urlFile'], $attachment['name']), ['target' => '_blank']) ?></span>
			<?php if(!empty($attachment['description'])) { ?>
    			<span class="description"><?= ' - '.$attachment['description'] ?></span>
    		<?php } ?>
    		<span class="date"><?= '('.strtolower(Yii::t('web', 'Added')).' '.$attachment['dateCreation'] ?></span>
			<span><?= mb_strtolower(Yii::t('web', 'By')) . ' ' . $attachment['firstName'] . ' ' . $attachment['lastName'] . ')' ?></span>
		</div>
	<?php } ?>
</div>