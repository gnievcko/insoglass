<?php
use yii\helpers\Url;
use frontend\models\ProductVariantForm;
?>

<div class="col-sm-12 row">
	<h4 style="padding-left: 15px"><?= Yii::t('web', 'Product versions') ?></h4>
	<div id="product-variants" class="items-container col-sm-12">
        <?php 
            $variantsForms = empty($productVariantsForms) ? [new ProductVariantForm()] : $productVariantsForms;
            foreach($variantsForms as $prefix => $productVariantForm) {
                echo \Yii::$app->controller->renderPartial('/inc/product_variant', [
                    'form' => $form, 
                    'prefix' => $prefix, 
                    'productVariantForm' => $productVariantForm,
                    'isWarehouse' => $isWarehouse,
                ]);
            }
        ?>
	</div>
	<div class="col-sm-12">
		<div class="btn btn-link add-item-button" data-items-container-selector="#product-variants" data-item-view-url="<?= Url::to(['site/get-product-variant-form']) ?>">
			<span class="base-icon link-icon add-icon"></span>
	        <span><?= Yii::t('web', 'Add version') ?></span>
		</div>
	</div>
</div>
