<?php use yii\bootstrap\Html;
if(Yii::$app->params['isProductAttributesVisible']) { ?>
    <div class="product-attribute-container">
        
        <?php 
        if(!empty($productAttributeForm)) {

            foreach($productAttributeForm as $id => $value) {
                echo ProductAttributeTypeTranslation::find()->where(['product_attribute_type_id' => $id, 'language_id' => $language])->one()->name;
                echo '<div class="col-xs-12">';  
                
                foreach($group['attributes'] as $attribute) {
                    if($attribute['variableType'] == 'bool') {
                        echo '<div class="row attribute-static-item"><div class="col-xs-2"><span class="glyphicon glyphicon-ok">
                                </div><div class="col-xs-10">'.Html::encode($attribute['name']).' '.$hint.'</div></div>';
                    }
                    elseif($attribute['variableType'] == 'int') {
                        echo '<div class="row attribute-static-item"><div class="col-xs-7">'.Html::encode($attribute['name']).' '.$hint.'</div>
                                <div class="col-xs-5">'.Html::encode($attribute['value']).'</div></div>';
                    }
                }
                
                echo '</div>';
            }
            
            if($emptyGroups == count($groups)) {
                echo Yii::t('web', 'No additional options have been defined yet.');
            }
        }
        else {
            echo Yii::t('web', 'No additional options have been defined yet.');
        }
        ?>
    
    </div>
<?php } ?>