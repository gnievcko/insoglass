<?php
use yii\helpers\Html;
use yii\helpers\Url;
?>

<div class="photo photo-form">
    <?= Html::img('@web/images/placeholders/close.png', ['class' => 'photo-remove-btn', 'title' => \Yii::t('main', 'Delete'), 'alt' => \Yii::t('main', 'Delete')]) ?>
    <div>
        <?= Html::img(Url::to(['storage/index', 'f' => $photoForm->thumbnail])) ?>
        <?= Html::activeHiddenInput($photoForm, "[{$prefix}]thumbnail") ?>
        <?= Html::activeHiddenInput($photoForm, "[{$prefix}]image") ?>
        <?= Html::activeHiddenInput($photoForm, "[{$prefix}]makeCopy") ?>
    </div>
    <div class="photo-description">
    	<?= Html::activeLabel($photoForm, "[{$prefix}]description") ?>
		<?= Html::activeTextInput($photoForm, "[{$prefix}]description",['class' => 'form-control']) ?>
    </div>
</div>
