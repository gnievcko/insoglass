<?php
use yii\helpers\Html;
use yii\helpers\Url;
use common\models\ar\FileTemporaryStorage;
?>

<div class="document">
    <?= Html::img('@web/images/placeholders/document_icon.png') ?>
    <span><?= $documentForm->name ?></span>
</div>
