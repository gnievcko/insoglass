<?php
use yii\helpers\Html;
use yii\helpers\Url;
use common\models\ar\FileTemporaryStorage;
?>

<div class="document document-form">
    <?= Html::img('@web/images/placeholders/document_icon.png') ?>
    <span><?= $documentForm->name ?></span>
    <?= Html::img('@web/images/placeholders/garbage.png', ['alt' => \Yii::t('main', 'Delete'), 'class' => 'document-remove-btn', 'title' => \Yii::t('main', 'Delete')]) ?>
    <?= Html::activeHiddenInput($documentForm, "[{$prefix}]document") ?>
    <?= Html::activeHiddenInput($documentForm, "[{$prefix}]name") ?>
    <?= Html::activeHiddenInput($documentForm, "[{$prefix}]makeCopy") ?>
</div>
