<?php
use yii\helpers\Url;
use yii\helpers\Html;
use common\components\ActiveForm;
use common\models\ar\Language;
use common\helpers\ProvinceHelper;
use frontend\models\UserAccountForm;
use kartik\widgets\FileInput;

$subtitle = $model->isShowScenarioSet() ? Yii::t('web', 'Your account') : Yii::t('web', 'Edit your account');
$this->title = $subtitle.' - '.Yii::$app->name;

$this->params['breadcrumbs'][] = ['label' => Yii::t('web', 'User information'), 'url' => Url::to(['user/account'])];
$this->params['breadcrumbs'][] = Yii::t('web', 'Edit your account');
?>

<div class="user-account-form">
	<h2 class="page-title"><?= $subtitle ?></h2>

	<?php $form = ActiveForm::begin([
			'id' => 'user-account-form-ajax',
			'enableAjaxValidation' => true,
			'validateOnBlur' => false,
			'validateOnChange' => false,
			'options' => ['enctype' => 'multipart/form-data'],
	]); ?>

	<?php if($model->isEditScenarioSet()) {
		echo $form->field($model, 'id')->hiddenInput()->label(false);
		echo $form->field($model, 'deletePhoto')->hiddenInput()->label(false);
	} ?>

	<div class="col-md-12 row">
		<div class="col-md-8">
			<div class="row">
    			<div class="col-md-6">
    				<?= $form->field($model, 'firstName')->textInput(['readonly' => $model->isShowScenarioSet(), 'maxlength' => true, 'placeHolder' => $model->getAttributeLabel('firstName')]) ?>
    			</div>
    			<div class="col-md-6">
    				<?= $form->field($model, 'lastName')->textInput(['readonly' => $model->isShowScenarioSet(), 'maxlength' => true, 'placeHolder' => $model->getAttributeLabel('lastName')]) ?>
    			</div>
			</div>
			
			<div class="row">
    			<div class="col-md-6">
    				<?= $form->field($model, 'phone1')->textInput(['maxLength' => true, 'placeholder' => $model->getAttributeLabel('phone1'), 'class' => 'form-control']) ?>
    			</div>
    			<div class="col-md-6">
    				<?= $form->field($model, 'phone2')->textInput(['maxLength' => true, 'placeholder' => $model->getAttributeLabel('phone2'), 'class' => 'form-control']) ?>
    			</div>
			</div>
			
			<div class="row">
    			<div class="col-md-6">
    				<?= $form->field($model, 'position')->textInput(['maxLength' => true, 'placeholder' => $model->getAttributeLabel('position')]) ?>
    			</div>
			</div>

			<div class="row">
				<div class="col-md-12">
				<?= $form->field($model, 'email')->textInput(['readonly' => $model->isShowScenarioSet(), 'maxlength' => true, 'placeHolder' => $model->getAttributeLabel('email')]) ?>
				</div>
			</div>
			
			<?php $provinces = ProvinceHelper::getListOfNonArtificialProvinces(); ?>
	
    		<div class="row">
    			<div class="col-md-12">
    				<h4><?= Yii::t('main', 'Address') ?></h4>
				</div>
    			<div class="col-md-4 col-sm-8"><?= $form->field($model, 'address')->textInput(['maxlength' => true, 'placeholder' => $model->getAttributeLabel('address')]) ?></div>
    			<div class="col-md-2 col-sm-4"><?= $form->field($model, 'zipCode')->textInput(['maxlength' => true, 'placeholder' => $model->getAttributeLabel('zipCode')]) ?></div>
    			<div class="col-md-3 col-sm-6"><?= $form->field($model, 'city')->textInput(['maxlength' => true, 'placeholder' => $model->getAttributeLabel('city')]) ?></div>
    			<div class="col-md-3 col-sm-6"><?= $form->field($model, 'provinceId')->dropDownList($provinces, ['prompt' => Yii::t('main','Choose Voivodeship')]) ?></div>
    		</div>
    		
    		<div class="row">
    			<div class="col-md-12">
    				<h4><?= Yii::t('web', 'Shipping address') ?></h4>
				</div>
    			<div class="col-md-4 col-sm-8"><?= $form->field($model, 'postalAddress')->textInput(['maxlength' => true, 'placeholder' => $model->getAttributeLabel('postalAddress')]) ?></div>
    			<div class="col-md-2 col-sm-4"><?= $form->field($model, 'postalZipCode')->textInput(['maxlength' => true, 'placeholder' => $model->getAttributeLabel('postalZipCode')]) ?></div>
    			<div class="col-md-3 col-sm-6"><?= $form->field($model, 'postalCity')->textInput(['maxlength' => true, 'placeholder' => $model->getAttributeLabel('postalCity')]) ?></div>
    			<div class="col-md-3 col-sm-6"><?= $form->field($model, 'postalProvinceId')->dropDownList($provinces, ['prompt' => Yii::t('main','Choose Voivodeship')]) ?></div>
    		</div>
			
			
			<hr/>
			<?php if($model->isEditScenarioSet()): ?>
			<div class="col-md-6">
				<div class="row">
				<h3><?= Html::encode(Yii::t('web', 'Change password')) ?></h3>
				</div>
				<div class="row">
					<?= $form->field($model, 'currentPassword')->passwordInput(['maxlength' => true, 'placeHolder' => $model->getAttributeLabel('currentPassword')]) ?>
				</div>
				<div class="row">
					<?= $form->field($model, 'newPassword')->passwordInput(['maxlength' => true, 'placeHolder' => $model->getAttributeLabel('newPassword')]) ?>
				</div>
				<div class="row">
					<?= $form->field($model, 'newPasswordRepeated')->passwordInput(['maxlength' => true, 'placeHolder' => $model->getAttributeLabel('newPasswordRepeated')]) ?>
				</div>
			</div>
			<?php endif ?>
		</div>
		<div class="row col-md-4">
			<?php if($model->isShowScenarioSet()): ?>
		        <div id="photo-preview-container" class="form-group">
		            <label><?= Yii::t('main', 'Current photo') ?></label>
		            <div class="file-preview">
		                <div id="photo-preview-container" class="form-group text-center">
		                    <?= Html::img(\common\helpers\UploadHelper::getNormalPath(!empty($model->urlPhoto) ? $model->urlPhoto : Url::to('@web/images/placeholders/person.png'), 'user_photo'),
		                                  ['style' => 'max-width: 300px'],
		                                  ['class' => 'file-preview-frame', 'style' => 'display: block; height: auto; float: none;'])
		                    ?>
		                    	<!-- <button type="button" id="delete-photo-button" class="btn btn-danger"><?= Yii::t('main', 'Delete') ?></button> -->
		                </div>
		            </div>
		        </div>

	    	<?php else:
	    		echo '<h3 class="loadImage">'.Yii::t('web', 'Upload photo').'</h3>';
		        echo $form->field($model, 'filePhoto')->widget(FileInput::classname(), [
		                        'options' => [
		                                'multiple' => false,
		                        ],
		                        'pluginOptions' => [
		                                'language' => Yii::$app->language,
		                                'showCaption' => false,
		                                'showUpload' => false,
		                                'uploadAsync' => false,
		                                'resizeImageQuality' => 1.00, //not required
		                                'allowedFileTypes' => ['image'],
		                                'allowedFileExtensions' => ['jpg', 'png', 'jpeg', 'gif'],
		                                'allowedPreviewFileTypes' => ['image'],
		                                'allowedPreviewMimeTypes' => ['image/jpeg', 'image/png'],
		                                'previewFileType' => 'image',
		                                'previewSettings' => ['image' => ['width' => '300px', 'height' => 'auto']],
		                        		'initialPreview' => !empty($model->urlPhoto) ? Html::img(\common\helpers\UploadHelper::getNormalPath($model->urlPhoto, 'user_photo'),
		                                 		['style' => 'max-width: 300px'],
		                                 		['class' => 'file-preview-frame', 'style' => 'display: block; height: auto; float: none;']
		                        		) : [],
		                        ],
		        				'pluginEvents' => [
		        						'filecleared' => 'function() {$("#useraccountform-deletephoto")[0].value = 1}',
		        				]
		        ])->label(false);
		        endif;
		    ?>
		</div>
	</div>
	<?php if($model->isEditScenarioSet()):?>
		<div class="col-md-12">
			<div class="pull-left"><?= Html::a(Yii::t('web', 'Cancel'), ['account'], ['class' => 'btn btn-default']) ?></div>
			<div class="pull-right"><?= Html::submitButton(($model->isEditScenarioSet() ? Yii::t('main', 'Save') : Yii::t('web', 'Add')), ['class' => 'btn btn-primary']); ?></div>
		</div>
	<?php else: ?>
		<div class="col-md-12">
			<?= Html::a(Yii::t('main', 'Update'), ['account-edit'], ['class' => 'btn btn-primary']) ?>
		</div>
	<?php endif;?>
	<?php ActiveForm::end(); ?>
</div>
