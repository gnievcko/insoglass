<?php

use yii\helpers\Html;
use yii\helpers\Url;
use frontend\helpers\UserHelper;
use dosamigos\gallery\Gallery;
use common\helpers\StringHelper;
use frontend\helpers\AddressHelper;

$subtitle = Yii::t('web', 'User information');
$this->title = $subtitle . ' - ' . Yii::$app->name;
?>

<div class="account-details">
    <div class="row">
        <div class="col-sm-6 col-xs-9">
            <h2 class="page-title"><?= $subtitle ?></h2>
        </div>
        <div class="col-sm-6 col-xs-3">
            <div class="pull-right"><?= Html::a(Yii::t('main', 'Update'), ['account-edit'], ['class' => 'btn btn-default']) ?></div>
        </div>
    </div>
    <div class="col-sm-12 row">
        <div class="col-lg-3 col-md-4 col-sm-6 col-xs-12">
            <div class="photo-container ">
                <?php
                $src = null;
                if(empty($model->url_photo)) {
                    $src = Url::to('@web/images/placeholders/person.png');
                } else {
                    $src = \common\helpers\UploadHelper::getNormalPath($model->url_photo, 'user_photo');
                }
                ?>
                <?= '<img src=' . $src . ' alt="..." class="img-circle" align="middle" width="200px" >'; ?>
            </div>
        </div>	
        <div class="col-lg-9 col-md-8 col-sm-6 col-xs-12 user-details">
            <?php if(!empty($company)) {?>
            	<h3><?= $company->name ?></h3>
            <?php } ?>
            <h3><?= $model->first_name . ' ' . $model->last_name ?></h3>
            <h4><?= $model->email ?></h4>
            <h4><?= $model->phone1 ?></h4>
            <h4><?= $model->phone2 ?></h4>
        </div>       
        <div class="col-xs-12 ">
            <table class="data">
                <tr>
                    <td><?= Yii::t('main', 'Address') ?></td>
                    <td><?php
                        if(!empty($company->address)) {
                            echo AddressHelper::getFullAddress([
                                'addressMain' => @$company->address->main,
                                'addressComplement' => @$company->address->complement,
                                'cityName' => @$company->address->city->name,
                                'cityZipCode' => @$company->address->city->zip_code,
                                'provinceName' => @$company->address->city->province->name,
                                'countryName' => @$company->address->city->province->countryName
                            ]);
                        }
                        ?> 
                    </td>
                </tr>
                <tr>
                	<td><?= Yii::t('web', 'Shipping address') ?></td>
                    <td><?php
                        if(!empty($company->addressPostal)) {
                           echo AddressHelper::getFullAddress([
                                'addressMain' => @$company->addressPostal->main,
                                'addressComplement' => @$company->addressPostal->complement,
                                'cityName' => @$company->addressPostal->city->name,
                                'cityZipCode' => @$company->addressPostal->city->zip_code,
                                'provinceName' => @$company->addressPostal->city->province->name,
                                'countryName' => @$company->addressPostal->city->province->countryName
                            ]);
                        }
                        ?> 
                    </td>
                </tr>
                <tr>
                    <td><?= Yii::t('main', 'NIP')?></td>
                    <td><?= Html::encode(@$company->vat_identification_number) ?></td>
                </tr>
                <tr>
                    <td><?= Yii::t('web', 'Registration date') . ':' ?></td>
                    <td><?= StringHelper::getFormattedDateFromDate($model->date_creation) ?></td>
                </tr>
                <tr>
                    <td><?= Yii::t('web', 'Position') ?></td>
                    <td><?= Html::encode($model->position) ?></td>
                </tr>
            </table>
        </div>	
    </div>
</div>
