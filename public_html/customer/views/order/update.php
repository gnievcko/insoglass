<?php

use yii\helpers\Url;
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\web\View;
use common\helpers\StringHelper;
use common\helpers\Utility;

$this->registerJsFile('@web/js/forms.js?' . uniqid(), ['depends' => [\yii\web\JqueryAsset::className()]]);
$this->registerJsFile('@web/js/order-form.js?' . uniqid(), ['depends' => [\yii\web\JqueryAsset::className()]]);
$this->registerJsFile('@web/js/pages/toggle-attributes.js?' . uniqid(), ['depends' => [\yii\web\JqueryAsset::className()]]);
$this->registerJsFile('@web/js/pages/ral-colour-picker.js?' . uniqid(), ['depends' => [\yii\web\JqueryAsset::className()]]);

if(Yii::$app->params['isVisualisationVisible']) {
    $this->registerJsFile('@web/js/pages/order-visualisation.js?' . uniqid(), ['depends' => [\yii\web\JqueryAsset::className()]]);
}

$this->title = Yii::t('web', 'Update status') . ' - ' . Yii::$app->name;

$this->params['breadcrumbs'][] = ['label' => StringHelper::translateOrderToOffer('main', 'Orders'), 'url' => Url::to(['order/index'])];
$this->params['breadcrumbs'][] = ['label' => StringHelper::translateOrderToOffer('main', 'List of orders'), 'url' => Url::to(['order/list'])];
$this->params['breadcrumbs'][] = ['label' => StringHelper::translateOrderToOffer('web', 'Order details'), 'url' => Url::to(['order/details', 'id' => $id])];
$this->params['breadcrumbs'][] = StringHelper::translateOrderToOffer('web', 'Update order');
?>

<div class="order-form" id="order-update">
    <h2 class="page-title"><?= StringHelper::translateOrderToOffer('web', 'Update order') ?></h2>

    <?php
    $form = ActiveForm::begin([
                'id' => 'order-update-form',
                'enableAjaxValidation' => true,
                'enableClientValidation' => false,
                'options' => [
                    'class' => 'form',
                ]
    ]);

    echo $form->errorSummary($invalidForms);
    echo $form->field($orderCreateForm, 'orderId')->hiddenInput()->label(false);
    ?>

    <h3><?= Yii::t('web', 'Products') ?></h3>
    <div id="products">
        <?php
        echo Yii::$app->controller->renderPartial('inc/_products', [
            'orderProductsForms' => $orderProductsForms,
            'currencies' => $currencies,
            'form' => $form,
            'constructions' => $constructions,
            'shapes' => $shapes,
            'muntins' => $muntins,
            'orderProductsVats' => $orderProductsVats,
            'orderProductsSummary' => $orderProductsSummary,
            'plnCurrency' => $plnCurrency,
            'displayFirstAttributes' => $displayFirstAttributes,
            'productAttributeForms' => !empty($productAttributeForms) ? $productAttributeForms : [],
        ]);
        ?>
    </div>
    <div id="costs">
        <?=
        Yii::$app->controller->renderPartial('inc/_costs', [
            'displayFirstAttributes' => $displayFirstAttributes,
            'orderCostsForms' => $orderCostsForms,
            'currencies' => $currencies,
            'form' => $form,
            'orderCostsVats' => $orderCostsVats,
            'orderCostsSummary' => $orderCostsSummary,
            'plnCurrency' => $plnCurrency,
            'constructions' => $constructions,
            'shapes' => $shapes,
            'muntins' => $muntins,
            'productAttributeForms' => !empty($artificialProductAttributeForms) ? $artificialProductAttributeForms : [],
        ])
        ?>
    </div>

    <div id="product-summary">
        <?= Yii::$app->controller->renderPartial('inc/_productSummary', ['summary' => $summary, 'plnCurrency' => $plnCurrency]) ?>
    </div>

    <?= Yii::$app->controller->renderPartial('inc/_status', ['orderHistoryForm' => $orderHistoryForm, 'orderStatuses' => $orderStatuses, 'form' => $form]) ?>

    <?= $form->field($orderCreateForm, 'isInvoiced')->checkbox() ?>

    <?=
    Yii::$app->controller->renderPartial('inc/_files', [
        'rderCreateForm' => $orderCreateForm,
        'photosForms' => $photosForms,
        'documentsForms' => $documentsForms,
        'readOnlyPhotosForms' => $oldPhotosForms,
        'readOnlyDocumentsForms' => $oldDocumentsForms,
        'form' => $form,
    ]);
    ?>

    <div class="col-sm-12">
        <?= Html::a(Yii::t('web', 'Cancel'), Url::to(['order/details', 'id' => $id]), ['class' => 'btn btn-default']) ?>
        <input type="hidden" name="save" value="0"/>
        <input type="hidden" name="OrderHistoryForm[orderHistoryId]" value=""/>
        <button type="button" class="btn btn-primary pull-right" id="update-order"><?= Yii::t('main', 'Save') ?></button>
        <div class="save-order-div">
            <div id="info">
                .....
            </div>
            <button class="btn btn-primary pull-right" id="save-order" style="display: <?php echo Yii::$app->params['isOrderQuickSaveVisible'] ? 'block' : 'none' ?>">
                <span class="glyphicon glyphicon-floppy-disk" aria-hidden="true"></span>
            </button>
        </div>
    </div>

    <?php ActiveForm::end() ?>
</div>

<?php $this->registerJs('
    (function() {
        var dynamicForm = new DynamicForm("order-update-form");
        dynamicForm.activateRemoveButtons();
        dynamicForm.activateAddItemButtons();

        OrderForm.activateEmployeesChooser($("#employee-select"), $("#selected-employees"));

        dynamicForm.activatePhotoUpload();
        dynamicForm.activateDocumentUpload();
        dynamicForm.activateProductFileUpload();
		OrderForm.save("order-update-form", "OrderHistoryForm[orderHistoryId]", "<div class=\"alert alert-success\" role=\"alert\">' . Yii::t('web', 'Success save') . '</div>", "<div class=\"alert alert-danger\" role=\"alert\">' . Yii::t('web', 'Error') . '</div>");
        OrderForm.initRecalculate("OrderProductForm", "' . Url::to(['order/get-products-form']) . '", "order-update-form", "products", "ProductAttributeForm");
        OrderForm.initRecalculate("OrderCostForm", "' . Url::to(['order/get-costs-form']) . '", "order-update-form", "costs", "ArtificialProductAttributeForm");
		OrderForm.initRecalculateSummary("' . Url::to(['order/get-all-products-summary']) . '", "product-summary");
		OrderForm.initRecalculateCustomData("' . Url::to(['order/get-custom-data-summary']) . '", "' . Url::to(['order/get-custom-data-numeration']) . '", "custom-tables");
        toggleAttributes();
        handleColourPicker("' . Url::to(['colour/get-modal-content']) . '");
    })();
    ', View::POS_READY);
?>

<?php
$this->registerJsFile(
        '@web/js/maskInput/jquery.mask.min.js', ['depends' => [\yii\web\JqueryAsset::className()]]
);
?>

<?php
if(Yii::$app->params['isVisualisationVisible']) {
    $shapes = implode('-', Utility::getShapes());
        $this->registerJs('init("' . Url::to(['order/get-color-and-shape']) . '", "' . $shapes . '", "'.Url::to(['storage/index']).'");', View::POS_END);
}
?>
