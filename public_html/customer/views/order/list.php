<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\View;
use dosamigos\multiselect\MultiSelect;
use common\helpers\StringHelper;
use yii\web\JqueryAsset;
use kartik\datecontrol\DateControl;
use customer\models\OrdersListForm;

$this->title = StringHelper::translateOrderToOffer('main', 'List of orders') . ' - ' . Yii::$app->name;

$this->params['breadcrumbs'][] = ['label' => StringHelper::translateOrderToOffer('main', 'Orders'), 'url' => Url::to(['order/index'])];
$this->params['breadcrumbs'][] = StringHelper::translateOrderToOffer('main', 'List of orders');

$this->registerJsFile('@web/js/dynamic-table.js?' . uniqid(), []);
$this->registerJsFile('@web/js/layout/moreFilters.js?' . uniqid(), ['depends' => [JqueryAsset::className()]]);
?>

<div id="orders-list-wrapper">
	<div class="row">
        <div class="col-xs-6">
        	<h2 class="page-title"><?= StringHelper::translateOrderToOffer('main', 'List of orders') ?></h2>
        </div>
        <div class="col-xs-6 text-right">
			<?= Html::a(StringHelper::translateOrderToOffer('web', 'Add order'), ['order/create'], ['class' => 'btn btn-primary']) ?>
        </div>
    </div>
    
    <div id="orders-list-table" class="dynamic-table">
		<h5><?php echo Yii::t('main', 'Filters') ?></h5>
        <div class="row dynamic-table-filters">
            <div class="col-md-3 col-sm-6">
                <label><?= $ordersListForm->getAttributeLabel('orderNumber') ?></label>
                <input 
                    value="<?= $ordersListForm->orderNumber ?>" 
                    class="form-control dynamic-table-filter search-input" 
                    type="text"
                    placeholder="<?php echo Yii::t('web', 'Search by number') ?>"
                    name="<?= Html::getInputName($ordersListForm, 'orderNumber') ?>">
            </div>
            <div class="col-md-3 col-sm-6">
                <label><?= $ordersListForm->getAttributeLabel('orderName') ?></label>
                <input 
                    value="<?= $ordersListForm->orderName ?>" 
                    class="form-control dynamic-table-filter search-input" 
                    type="text"
                    placeholder="<?php echo Yii::t('web', 'Search by name') ?>"
                    name="<?= Html::getInputName($ordersListForm, 'orderName') ?>">
            </div>  
            <div class="col-md-3 col-sm-6">
                <label><?= $ordersListForm->getAttributeLabel('dateFrom') ?></label>
                <?=
                DateControl::widget([
                    'name' => (new OrdersListForm())->formName() . '[dateFrom]',
                    'value' => $ordersListForm->dateFrom,
                    'language' => \Yii::$app->language,
                    'type' => DateControl::FORMAT_DATE,
                    'ajaxConversion' => false,
                    'displayFormat' => 'php:Y-m-d',
                    'options' => [
                        'pluginOptions' => [
                            'autoclose' => true
                        ],
                        'options' => ['class' => 'form-control dynamic-table-filter', 'placeholder' => $ordersListForm->getAttributeLabel('dateFrom')],
                    ]
                ]);
                ?>
            </div>
            <div class="col-md-3 col-sm-6">
                <label><?= $ordersListForm->getAttributeLabel('dateTo') ?></label>
                <?=
                DateControl::widget([
                    'name' => (new OrdersListForm())->formName() . '[dateTo]',
                    'value' => $ordersListForm->dateTo,
                    'language' => \Yii::$app->language,
                    'type' => DateControl::FORMAT_DATE,
                    'ajaxConversion' => false,
                    'displayFormat' => 'php:Y-m-d',
                    'options' => [
                        'pluginOptions' => [
                            'autoclose' => true
                        ],
                        'options' => ['class' => 'form-control dynamic-table-filter', 'placeholder' => $ordersListForm->getAttributeLabel('dateTo')],
                    ]
                ]);
                ?>
            </div>
        	<div class="more-filters">
                <div class="col-md-3 col-sm-6 cat-select">
                    <label><?php echo $ordersListForm->getAttributeLabel('orderStatusesIds'); ?></label>
                    <?php
                    $orderStatusesData = array_reduce($ordersStatusesTranslations, function($options, $statusTranslation) {
                        $options[$statusTranslation->order_status_id] = Html::encode($statusTranslation->name);
                        return $options;
                    }, []);

                    if(!empty($orderStatusesData)) {

                        echo MultiSelect::widget([
                            'id' => 'order-statuses',
                            "options" => ['multiple' => 'multiple', 'class' => 'dynamic-table-filter f-multiselect'],
                            'data' => $orderStatusesData,
                            'name' => Html::getInputName($ordersListForm, 'orderStatusesIds'),
                            'value' => empty($ordersListForm->orderStatusesIds) ? array_keys($orderStatusesData) : $ordersListForm->orderStatusesIds,
                            "clientOptions" => [
                                'nonSelectedText' => '',
                                'nSelectedText' => mb_strtolower(Yii::t('main', 'Selected'), 'UTF-8'),
                                'numberDisplayed' => 2,
                                'buttonWidth' => '100%',
                            ],
                        ]);
                    }
                    ?>               
                </div>
                <?php if(!OrdersListForm::isHidden('orderNumberForClient')) { ?>
					<div class="col-md-3 col-sm-6">
						<label><?= $ordersListForm->getAttributeLabel('orderNumberForClient') ?></label>
						<input 
							value="<?= $ordersListForm->orderNumberForClient ?>" 
							class="form-control dynamic-table-filter search-input" 
							type="text"
							placeholder="<?php echo Yii::t('main', 'Client number') ?>"
							name="<?= Html::getInputName($ordersListForm, 'orderNumberForClient') ?>">
					</div>
				<?php } ?>
			</div>
			<?= Yii::$app->controller->renderPartial('/inc/listFiltersButtons', ['isMoreFilters' => true, 'isClearFilters' => true]) ?>
		</div>
		<div class="dynamic-table-data">
            <?= Yii::$app->controller->renderPartial('inc/_orders', [
                'orders' => $orders,
                'pages' => $pages,
                'ordersListForm' => $ordersListForm,
            ]) ?>
        </div>      
	</div>
	<?php
    $this->registerJs('dynamicTable("orders-list-table", "' . Url::to(['order/list']) . '");', View::POS_END);
    $this->registerJs('moreFilters(' . Yii::$app->params['isListFiltersVisibleByDefault'] . ');', View::POS_END);
    ?>
</div>
