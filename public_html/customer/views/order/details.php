
<?php

use yii\helpers\Html;
use yii\helpers\Url;
use frontend\helpers\UserHelper;
use dosamigos\gallery\Gallery;
use common\helpers\StringHelper;
use common\models\ar\User;
use yii\bootstrap\Modal;
use common\helpers\Utility;
use frontend\models\AlertForm;
use common\bundles\EnhancedDialogAsset;
use yii\helpers\Json;
use common\documents\DocumentType;

$this->title = StringHelper::translateOrderToOffer('web', 'Order details') . ' - ' . Yii::$app->name;

$this->params['breadcrumbs'][] = ['label' => StringHelper::translateOrderToOffer('main', 'Orders'), 'url' => Url::to(['order/index'])];
$this->params['breadcrumbs'][] = ['label' => StringHelper::translateOrderToOffer('main', 'List of orders'), 'url' => Url::to(['order/list'])];
$this->params['breadcrumbs'][] = StringHelper::translateOrderToOffer('web', 'Order details');
$this->registerJs('$(document).ready(function() {
    $("body").tooltip({ selector: "[data-toggle=tooltip]" });
});');

$appendToClass = $isInProgress ? ' no-click' : ''; 
?>

<div class="order-details">
    <div class="row">
        <div class="col-md-4 col-sm-6"><h2 class="page-title"><?= StringHelper::translateOrderToOffer('web', 'Order details') ?></h2></div>
        <div class="col-md-8 col-sm-6 order-buttons">
            <?php if(Yii::$app->params['isProductionVisible']) { ?>
                <div><?=
                    Html::a(Yii::t('main', 'Delete'), ['order/delete'], [
                        'class' => 'btn btn-default' . $appendToClass,
                        'disabled' => $isInProgress,
                        'data' => [
                            'confirm' => StringHelper::translateOrderToOffer('main', 'Are you sure you want to delete this order?<br/><strong>It would be not available as well as its related specific data.</strong>'),
                            'method' => 'post',
                            'params' => [
                                'id' => $data['id'],
                            ],
                        ],
                    ]);
                    ?></div>
                <div><?= Html::a(Yii::t('web', 'Edit'), ['order/edit', 'id' => $data['id']], ['class' => 'btn btn-default' . $appendToClass, 'disabled' => $isInProgress]) ?></div>
            <?php } ?>
            <?php
            $updateOption = ['class' => 'btn btn-primary' . $appendToClass, 'disabled' => $isInProgress];
            $quickUpdateOption = ['value' => Url::to(['order/quick-update', 'id' => $data['id']]), 'class' => 'btn btn-link btn-update', 'id' => 'modalButton'];
            if($data['isActive'] == 0 && !(Yii::$app->user->can(Utility::ROLE_ADMIN) && Yii::$app->params['isUnlimitedOrderChangeStatusAvailable'])) {
                $updateOption['onclick'] = 'return false;';
                $updateOption['disabled'] = 'true';
                //$updateOption['data-toggle'] = 'tooltip';
                //$updateOption['data-placement'] = 'bottom';
                //$updateOption['title'] = StringHelper::translateOrderToOffer('web', 'Order is inactive.');
                $quickUpdateOption['disabled'] = 'true';
                //$quickUpdateOption['data-toggle'] = 'tooltip';
                //$quickUpdateOption['data-placement'] = 'bottom';
                //$quickUpdateOption['title'] = StringHelper::translateOrderToOffer('web', 'Order is inactive.');
            }
            ?>
            <?php //echo Html::button(Yii::t('web', 'Update status'), $quickUpdateOption) ?>
            <?php if(Yii::$app->params['isProductionVisible']) { ?>
                <div><?= Html::a(StringHelper::translateOrderToOffer('web', 'Update order'), ['order/update', 'id' => $data['id']], $updateOption) ?></div>
            <?php } ?>
        </div>
    </div>

    <?php
    $positiveTerminalStatus = Yii::$app->getSession()->get('positiveTerminalStatus');
    Yii::$app->getSession()->remove('positiveTerminalStatus');
    Modal::begin([
        'id' => 'modal',
        'size' => 'modal-lg',
        'clientOptions' => ['show' => !empty($positiveTerminalStatus)]
    ]);
    ?>
    <div id="modalContent">
        <?php
        if(!empty($positiveTerminalStatus)) {
            echo Yii::$app->controller->renderPartial('inc/_alert', [
                'order' => $data,
                'form' => new AlertForm()
            ]);
        }
        ?>
    </div>
    <?php Modal::end($data); ?>

    <div class="col-xs-12 title-with-icon">
        <div class="base-icon basic-data-icon main-content-icons"></div>
        <h3><?= Yii::t('web', 'General information') ?></h3>
    </div>
    <div class="col-xs-12">
        <div class="row">
            <div class=" col-sm-8">
                <div class="table-responsive">

                    <table class="table table-bordered table-hover detail-table">
                        <tbody>
                            <tr>
                                <th><?= Yii::t('web', 'Number') ?></th>
                                <td><?= Html::encode($data['number']) ?></td>
                            </tr>
                            <tr>
                                <th><?= Yii::$app->params['isOrderTitleIsLabeledAsConcerns'] ? Yii::t('web', 'Concerns') : (Yii::$app->params['isOrderTitleLabeledAsOrderName'] ? Yii::t('web', 'Job name') : Yii::t('web', 'Title')) ?></th>
                                <td><?= Html::encode($data['title']) ?></td>
                            </tr>
                            <tr>
                                <th><?= Yii::t('main', 'Creation date') ?></th>
                                <td><?= Html::encode(StringHelper::getFormattedDateFromDate($data['dateCreation'])) ?></td>
                            </tr>
                            <tr>
                                <th><?= Yii::t('web', 'Suggested completion time') ?></th>
                                <td><?= Html::encode(StringHelper::getFormattedDateFromDate($data['dateDeadline'])) ?></td>
                            </tr>
                            <tr>
                                <th><?= Yii::t('main', 'Description') ?></th>
                                <td><?= Html::encode($data['description']) ?></td>
                            </tr>
                            <tr>
                                <th><?= Yii::t('web', 'Contact people') ?></th>
                                <td><?= $this->render('inc/_userRepresentatives', ['users' => $contactPeople, 'number' => HTML::encode($data['number']), 'entityName' => Html::encode($data['entity'])]) ?></td>
                            </tr>
                            <?php if(Yii::$app->params['isShippingDateAvailable']) : ?>
                                <tr>
                                    <th><?= Yii::t('web', 'Shipping date') ?></th>
                                    <td><?= Html::encode(StringHelper::getFormattedDateFromDate($data['shippingDate'])) ?></td>
                                </tr>
                            <?php endif; ?>
                            <?php if(Yii::$app->params['isProductionVisible']) { ?>
                                <tr>
                                    <th><?= Yii::t('web', 'Status') ?></th>
                                    <td><?= Html::encode($data['status']) ?></td>
                                </tr>
                            <?php } ?>
                            <tr>
                                <th><?= Yii::t('web', 'Shipping address') ?></th>
                                <?php if(!empty($data['postalCompanyAddress'])) { ?>
                                    <td><?=
                                        $data['postalCompanyAddress'] . (!empty($data['postalCompanyAddressComplement']) ? ' ' . $data['postalCompanyAddressComplement'] : '') .
                                        ', ' . $data['postalCityZipCode'] . ' ' . $data['postalCityName']
                                        ?> 
                                    </td>
                                <?php } 
                                elseif(!empty($data['companyAddress'])) { ?>
                                	<td><?=
                                        $data['companyAddress'] . (!empty($data['companyAddressComplement']) ? ' ' . $data['companyAddressComplement'] : '') .
                                        ', ' . $data['cityZipCode'] . ' ' . $data['cityName']
                                        ?> 
                                    </td>
                                <?php } ?>
                            </tr>
                            <?php if(!empty($attachments)) { ?>
                                <tr>
                                    <th><?= Yii::t('web', 'Attachments') ?></th>
                                    <td><?= $this->render('/inc/attachmentList', ['attachments' => $attachments]) ?></td>
                                </tr>
                            <?php } ?>   
                        </tbody>
                    </table>
                </div>
            </div>
            <?php if(!Yii::$app->params['isProductionVisible']) { ?>
                <div class="col-sm-4">
                    <h3> <?= Yii::t('web', 'Status') ?></h3>
                    <?= Yii::t('web', 'Order status') . ':' ?>
                    <span class="order-status"><?= Html::encode($data['status']) ?></span>
                    <?php if(!$data['visibleStatusButton']) { ?>
                        <div><?= Html::button(Yii::t('web', 'Update status'), $quickUpdateOption) ?></div>
                    <?php } ?>
                </div>
            <?php } ?>
        </div>
    </div>

    <div class="col-xs-12 title-with-icon">
        <div class="base-icon product-icon main-content-icons"></div>
        <h3><?= StringHelper::translateOrderToOffer('web', 'Order products') ?></h3>
    </div>

    <div class="col-xs-12">
        <?php if(!empty($products)) { ?>
            <?php if(Yii::$app->params['isProductionVisible']) { ?>
                <?=
                Yii::$app->controller->renderPartial('inc/_detailsProductionProducts', [
                    'products' => $products,
                    'vats' => $vats,
                    'plnCurrency' => $plnCurrency,
                    'summary' => $summary,
                ]);
                ?>
            <?php } else { ?>
                <?=
                Yii::$app->controller->renderPartial('inc/_detailsProducts', [
                    'products' => $products,
                ]);
                ?>
            <?php } ?>
        <?php } else { ?>
            <div class="row">
                <div class="col-sm-8 desc no-desc">
                    <?php echo Yii::t('web', 'No data'); ?>
                </div>
            </div>
        <?php } ?>
    </div>

    <?php if(Yii::$app->params['isProductionVisible']) { ?>
        <div class="col-xs-12 title-with-icon">
            <div class="base-icon price-icon main-content-icons"></div>
            <h3><?= StringHelper::translateOrderToOffer('web', 'Summary') ?></h3>
        </div>
        <div class="col-xs-12">  
            <div class="table-responsive">
                <table class="table table-bordered table-hover list-grid-table">
                    <thead>
                        <tr>
                            <th><?= Yii::t('web', 'Product name') ?></th>
                            <th><?= Yii::t('main', 'Price [per sq. m]') ?></th>
                            <th><?= Yii::t('web', 'Count') ?></th>
                            <th><?= Yii::t('web', 'Net value') ?></th>
                            <?php if(Yii::$app->params['isOrderGrossValuesVisible']) { ?>
                                <th><?= Yii::t('web', 'Vat rate') ?></th>
                                <th><?= Yii::t('web', 'Vat amount') ?></th>
                                <th><?= Yii::t('web', 'Gross value') ?></th>

                            <?php } ?>
                        </tr>
                    </thead>
                    <tbody>
                        <?php foreach($products as $product) { ?>
                            <tr <?= !empty($product['isArtificial']) ? 'class="artificial"' : '' ?>>
                                <td>
                                    <?= ($product['parentName'] ?: $product['name']) . (!empty($product['parentName']) ? ' (' . strtolower(Yii::t('web', 'Version')) . ': ' . $product['name'] . ')' : '') ?>
                                </td>
                                <td class="text-center"><?= (!empty($product['price']) ? StringHelper::getFormattedCost($product['price']) . ' ' . $product['currency'] : '') ?></td>
                                <td><?= $product['count'] ?></td>
                                <td><?= StringHelper::getFormattedCost($product['netValue']) . ' ' . $plnCurrency->short_symbol ?></td>
                                <?php if(Yii::$app->params['isOrderGrossValuesVisible']) { ?>
                                    <td><?= Yii::$app->params['vatRate'][$product['vatRate']] ?></td>
                                    <td><?= StringHelper::getFormattedCost($product['vatAmount']) . ' ' . $plnCurrency->short_symbol ?></td>
                                    <td><?= StringHelper::getFormattedCost($product['grossValue']) . ' ' . $plnCurrency->short_symbol ?></td>
                                <?php } ?>
                            </tr>
                            <?php
                        }
                        if(Yii::$app->params['isOrderGrossValuesVisible']) {
                            foreach($vats as $vat) {
                                ?>
                                <tr class="vat-summary">
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td><?= StringHelper::getFormattedCost($vat['netValue']) . ' ' . $plnCurrency->short_symbol ?></td>

                                    <td><?= $vat['vatLabel'] ?></td>
                                    <td><?= StringHelper::getFormattedCost($vat['vatAmount']) . ' ' . $plnCurrency->short_symbol ?></td>
                                    <td><?= StringHelper::getFormattedCost($vat['grossValue']) . ' ' . $plnCurrency->short_symbol ?></td>

                                </tr>
                            <?php } ?>
                        <?php } ?>
                    </tbody>
                    <tfoot>
                        <tr>
                            <td><?= Yii::t('web', 'Summary') ?></td>
                            <td></td>
                            <td></td>
                            <td><?= StringHelper::getFormattedCost($summary['netValue']) . ' ' . $plnCurrency->short_symbol ?></td>
                            <?php if(Yii::$app->params['isOrderGrossValuesVisible']) { ?>
                                <td></td>
                                <td><?= StringHelper::getFormattedCost($summary['vatAmount']) . ' ' . $plnCurrency->short_symbol ?></td>
                                <td><?= StringHelper::getFormattedCost($summary['grossValue']) . ' ' . $plnCurrency->short_symbol ?></td>
                            <?php } ?>
                        </tr>
                    </tfoot>
                </table>
            </div>
        </div>
    <?php } ?>

    <?php if(!empty($reservedProducts)) { ?>
        <?= $this->render('/order/inc/_reservedProductsList', ['orderId' => $data['id'], 'reservedProducts' => $reservedProducts]) ?>
    <?php } ?>

    <?php
    if(!empty($data['customData']) && Yii::$app->params['isOrderCustomDataVisible']) {
        $array = Json::decode($data['customData']);
        $customData = reset($array);
        ?>

        <div class="col-xs-12 title-with-icon">
            <div class="base-icon table-icon main-content-icons"></div>
            <h3><?= Yii::t('web', 'Additional data') ?></h3>
        </div>

        <div class="col-sm-12">
            <div class="table-responsive">
                <table class="table table-hover list-grid-table">
                    <thead>
                        <tr>
                            <?php foreach($customData['theads'] as $thead) { ?>
                                <th><?= $thead ?></th>
                            <?php } ?>
                        </tr>
                    </thead>
                    <tbody>
                        <?php foreach($customData['rows'] as $row) { ?>
                            <tr>
                                <?php
                                for($i = 0; $i < count($row); ++$i) {
                                    if($i == count($row) - 3) {
                                        ?>
                                        <td><?= StringHelper::getFormattedCost($row[$i], true) ?></td>
                                    <?php } else {
                                        ?>
                                        <td><?= $row[$i] ?></td>	
                                    <?php } ?>
                                <?php } ?>
                            </tr>
                        <?php } ?>
                    </tbody>
                    <tfoot>
                        <tr>
                            <?php for($i = 0; $i < count($customData['theads']) - 2; ++$i) { ?>
                                <td></td>
                            <?php } ?>
                            <td><?= Yii::t('main', 'Total') ?></td>
                            <td><?= $data['customDataSummary'] ?></td> 
                        </tr>
                    </tfoot>
                </table>
            </div>
        </div>
    <?php } ?>

    <?php if(!Yii::$app->params['isProductionVisible']) { ?>

        <div class="col-xs-12 title-with-icon">
            <div class="base-icon description-icon main-content-icons"></div>
            <h3><?= StringHelper::translateOrderToOffer('web', 'Order description') ?></h3>
        </div>

        <div class=" col-xs-12">
            <div class="row">
                <div class="col-sm-8 desc">
                    <?= empty($data['description']) ? Yii::t('web', 'None') : nl2br(Html::encode($data['description'])); ?>
                </div>
            </div>
        </div>
    <?php } ?>

    <?php if(Yii::$app->params['isOrderNoteVisible']) { ?>
        <div class="col-xs-12 title-with-icon">
            <div class="base-icon description-icon main-content-icons"></div>
            <h3><?= StringHelper::translateOrderToOffer('web', 'Note') ?></h3>
        </div>

        <div class=" col-sm-12">
            <div class="row">
                <div class="col-sm-8 desc">
                    <?= empty($data['descriptionNote']) ? Yii::t('web', 'None') : '<b>' . nl2br(Html::encode($data['descriptionNote'])) . '</b>'; ?>
                </div>
            </div>
        </div>
    <?php } ?>
    <?php if(Yii::$app->params['isOrderRemarksVisible']) { ?>
        <div class="col-xs-12 title-with-icon">
            <div class="base-icon description-icon main-content-icons"></div>
            <h3><?= StringHelper::translateOrderToOffer('web', 'Remarks') ?></h3>
        </div>

        <div class=" col-sm-12">
            <div class="row">
                <div class="col-sm-8 desc">
                    <?= empty($data['descriptionCont']) ? Yii::t('web', 'None') : nl2br(Html::encode($data['descriptionCont'])); ?>
                </div>
            </div>
        </div>
    <?php } ?>
</div>

<?php
EnhancedDialogAsset::register($this);
?>

