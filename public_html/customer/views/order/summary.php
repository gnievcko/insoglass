<?php
use yii\helpers\Url;
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\web\View;
use common\helpers\StringHelper;
use yii\helpers\Json;
use customer\models\OrderCreateForm;

$this->title = StringHelper::translateOrderToOffer('web', 'Create order') . ' - ' . Yii::$app->name;

$this->params['breadcrumbs'][] = ['label' => StringHelper::translateOrderToOffer('main', 'Orders'), 'url' => Url::to(['order/index'])];
$this->params['breadcrumbs'][] = ['label' => StringHelper::translateOrderToOffer('main', 'List of orders'), 'url' => Url::to(['order/list'])];
$this->params['breadcrumbs'][] = StringHelper::translateOrderToOffer('web', 'Order summary');
?>
<?php if(!empty($formData)):?>
<div class="order-form">
	<h2 class="page-title"><?= StringHelper::translateOrderToOffer('web', 'Order summary') ?></h2>

    <?= Yii::$app->controller->renderPartial('inc/summary/_basic_data', [
	        'orderCreateForm' => $orderCreateForm,
            'data' => $data,
            'contactPeople' => $contactPeople,
        ]) 
    ?>
    <div  class="col-xs-12">
    <?php  if(!empty($products) || !empty($costProducts)) { ?>
        <div class="row">
            <div class="col-xs-12 title-with-icon">
            <div class="base-icon box-icon main-content-icons"></div>
                <h3><?= StringHelper::translateOrderToOffer('web', 'Order products') ?></h3>
            </div>
            
            <?= Yii::$app->controller->renderPartial('inc/summary/_detailsProducts', [
                        'products' => $products,
                        'productAttributeForms' => $productAttributeForms,
            ]); ?>
            <?= Yii::$app->controller->renderPartial('inc/summary/_detailsProducts', [
                        'products' => $costProducts,
                        'productAttributeForms' => $artificialProductAttributeForms,
            ]); ?>
   		</div>
    <?php } ?>
    
    
        <?php if(!empty($documentsForms)){?>
                <div class="col-xs-12 title-with-icon">
                <div class="base-icon gallery-icon main-content-icons"></div>
                    <h3><?= StringHelper::translateOrderToOffer('web', 'Documents') ?></h3>
                </div>
        <?php 
        foreach($documentsForms as $documentForm) { 
	                echo Yii::$app->controller->renderPartial('inc/summary/document', ['documentForm' => $documentForm]);
	            }?>
        <?php }  ?>
    
        <?php if(!empty($orderCreateForm['descriptionCont'])) {?>
        <div class="col-xs-12 title-with-icon">
        <div class="base-icon description-icon main-content-icons"></div>
            <h3><?= Yii::t('web', 'Remarks') ?></h3>
        </div>
        <div class="col-xs-12">
            <p><?= $orderCreateForm['descriptionCont'] ?></p>
        </div>
        <?php } ?>
    
    <?php if(!empty($products)) { ?>
        <div class="col-xs-12 title-with-icon">
        <div class="base-icon price-icon main-content-icons"></div>
            <h3><?= Yii::t('web', 'Price') ?></h3>
        </div>
        <?= Yii::$app->controller->renderPartial('inc/summary/_productSummary', [
                    'products' => $products,
                    'summary' => $summary,
                    'plnCurrency' => $plnCurrency,
        ]); ?>
    <?php } ?>

    <div>
        <button  type="button" class="btn btn-primary pull-left" id="cancel-order"><?= Yii::t('web', 'Cancel') ?></button>
        <button  type="button" class="btn btn-primary pull-right" id="add-order"><?= Yii::t('web', 'Add') ?></button>
    </div>
    </div>
</div>
<?php endif;?>
<?php 
    $this->registerJsFile('@web/js/summary-order.js?'.uniqid(), ['depends' => [\yii\web\JqueryAsset::className()]]);
    $this->registerJs('
        initializeOrderSummary("'.Url::to(['order/create']).'", "'. htmlspecialchars(json_encode($formData), ENT_QUOTES).'", "'.Yii::$app->request->csrfParam.'", "'.Yii::$app->request->csrfToken.'");
        ', View::POS_END);
?>