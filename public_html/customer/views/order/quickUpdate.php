<?php
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use yii\web\View;
use common\helpers\StringHelper;

$this->registerJsFile('@web/js/forms.js?'.uniqid(), ['depends' => [\yii\web\JqueryAsset::className()]]);
$this->registerJsFile('@web/js/order-form.js?'.uniqid(), ['depends' => [\yii\web\JqueryAsset::className()]]);
$this->registerJsFile('@web/js/pages/order-quick-update-modal.js?'.uniqid(), ['depends' => [\yii\web\JqueryAsset::className()]]);

$this->title = Yii::t('web', 'Update status') . ' - ' . Yii::$app->name;

$this->params['breadcrumbs'][] = ['label' => StringHelper::translateOrderToOffer('main', 'Orders'), 'url' => Url::to(['order/index'])];
$this->params['breadcrumbs'][] = ['label' => StringHelper::translateOrderToOffer('main', 'List of orders'), 'url' => Url::to(['order/list'])];
$this->params['breadcrumbs'][] = StringHelper::translateOrderToOffer('web', 'Update order status');
?>

<script>
window.alertMessage = "<?= Yii::t('web','Input valid dates.')?>";
</script>

<div class="order-form" id="order-update">
	<h2 class="page-title"><?= Yii::t('web', 'Update status') ?></h2>

    <?php
        $form = ActiveForm::begin([
            'id' => 'order-quick-update-form',
            'enableAjaxValidation' => true,
            'validateOnBlur' => false,
			'validateOnChange' => false,
            'options' => [
                'class' => 'form',
            ]
        ]);
    ?>

    <?= Yii::$app->controller->renderPartial('inc/_status', ['orderHistoryForm' => $orderHistoryForm, 'orderStatuses' => $orderStatuses, 'statusesIsVisibleForClient' => $statusesIsVisibleForClient, 'form' => $form]) ?>


    <div class='modalButtons'>
    	<button class="btn btn-default" data-dismiss="modal" ><?=Yii::t('web', 'Cancel') ?></button>
        <button id="orderQuickUpdateButton" class="btn btn-primary pull-right"><?= Yii::t('main', 'Save') ?></button>
    </div>

    <?php ActiveForm::end() ?>
</div>

<?php  $this->registerJs('
    (function() {
        var dynamicForm = new DynamicForm("order-quick-update-form");
        dynamicForm.activateRemoveButtons();
        dynamicForm.activateAddItemButtons();

        OrderForm.activateEmployeesChooser($("#employee-select"), $("#selected-employees"));

        dynamicForm.activatePhotoUpload();
        dynamicForm.activateDocumentUpload();
    })();
    ', View::POS_END);
    $this->registerCssFile('@web/css/main.css');
?>
