<?php

use kartik\datecontrol\DateControl;
use kartik\widgets\Select2;
use yii\web\JsExpression;

$this->registerJs('window.rr = function(){var d = $("#dateDeadlineId-disp-kvdate").kvDatepicker("getDate");
                                if(d == null) d = new Date();
                    			d.setDate(d.getDate() - 3);
                    			var dateToday = new Date();
                    			dateToday.setHours(0,0,0,0);

                    			if(d.getTime() > dateToday.getTime()) {
    								
    								var dateFormat2 = d.getFullYear() + "-" + ("0" + (d.getMonth() + 1)).slice(-2) + "-" + ("0" + (d.getDate())).slice(-2);
    								var dateFormat =  ("0" + (d.getDate())).slice(-2) + "." + ("0" + (d.getMonth() + 1)).slice(-2) + "." + d.getFullYear() ;
    					
    								$("#dateReminderId").val(dateFormat2);
									$("#dateReminderId-disp-kvdate").kvDatepicker("update", dateFormat);
    					
                    			}}');
?>

<div class="row">
    <div class="col-sm-6 col-sm-offset-2">
        <div class="form-group">
            <?= $form->field($orderHistoryForm, 'orderStatusId')->dropDownList($orderStatuses, ['placeholder' => '']); ?>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-sm-6 col-sm-offset-2">
        <div class="form-group">
            <?= $form->field($orderHistoryForm, 'message')->textArea(); ?>
        </div>
    </div>
</div>
<?php
$this->registerJs('
        $("#dateDeadlineId-disp").attr("placeholder","' . Yii::t('web', 'Write deadline date') . '");
        $("#dateReminderId-disp").attr("placeholder","' . Yii::t('web', 'Write reminder date') . '");
        $("#dateShippingId-disp").attr("placeholder","' . Yii::t('web', 'Write shipping date') . '");
    ');
?>