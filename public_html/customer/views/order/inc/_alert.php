<?php
use yii\widgets\ActiveForm;
use yii\helpers\Url;
use yii\helpers\Html;
use common\helpers\StringHelper;
?>

<div class="alert-form-container">
	<h2 class="page-title"><?= StringHelper::translateOrderToOffer('web', 'You can set an alert in the form of a task so the system can remind you about the possibility of renewing the order.') ?></h2>
	
	<?php
	    $formInputs = ActiveForm::begin([
	        'id' => 'order-alert-form',
	        'enableAjaxValidation' => true,
	        'enableClientValidation' => false,
			'action' => Url::to(['order/set-alert']),
	        'options' => [
	            'class' => 'form',
	        ]
	    ]);
    ?>

	<?= Yii::$app->controller->renderFile('@common/components/views/orderReminderTrait.php', [
			'form' => $formInputs, 
			'model' => $form, 
			'order' => $order
	]) ?>

	<div class='modalButtons'>
    	<button class="btn btn-default" data-dismiss="modal" ><?= Yii::t('web', 'Cancel') ?></button>
        <?= Html::submitButton(Yii::t('main', 'Save'), ['class' => 'btn btn-primary pull-right']) ?>
    </div>
	<?php $formInputs->end(); ?>
</div>

