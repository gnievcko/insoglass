<?php
use yii\helpers\Url;
use yii\helpers\Html;
use yii\web\View;
use frontend\models\OrderTableForm;
	
$this->registerJsFile('@web/js/documents/addTable.js?'.uniqid(), ['depends' => [\yii\web\JqueryAsset::className()]]);
$this->registerJsFile('@web/js/documents/tables.js?'.uniqid(), ['depends' => [\yii\jui\JuiAsset::className()]]);
?>

<div class="order-table-form">
	<?php if(Yii::$app->params['isOrderCustomDataVisible']) { ?>
	<div class="col-xs-12 title-with-icon">
		<div class="base-icon table-icon main-content-icons"></div>
		<h3><?= Yii::t('web', 'Additional data') ?></h3>
	</div>
	<div class="col-xs-12">
		<div class="row">
			<div class="col-xs-9">
				<div class="row">
        			<div class="col-xs-3">
        				<a class="btn btn-default add-table-button">
        					<span><?= Yii::t('documents', 'Add table') ?></span>
        				</a>
        				<?= $form->field($orderTableForm, 'isVisible')->hiddenInput()->label(false) ?>
        			</div>
        			<div class="col-xs-9 column-count-block">
        				<div class="row">
        					<div class="col-xs-6 text-right">
        						<label><?= Yii::t('web', 'Number of additional columns in a table') ?>:</label>
        					</div>
            				<div class="col-xs-3 column-number">
            					<?php
            					   echo $form->field($orderTableForm, 'columnCount')->dropDownList(array_combine(range(5, 7, 1), range(1, 3, 1)), 
            							['class' => 'form-control th-count'])->label(false);
            				    ?>
            				</div>
        				</div>
					</div>
				</div>
			</div> 
			<div class="col-xs-3 text-right">
				<button class="btn btn-default remove-single-table-button" disabled="disabled">
					<span><?= Yii::t('documents', 'Remove table') ?></span>
				</button>
			</div>
		</div>
	</div>
		
		<div class="custom-tables col-sm-12"><?php
			if(empty($orderTableForm->tables)) {
				$orderTableForm->tables = OrderTableForm::EMPTY_DATA;
			}
			
			if(!OrderTableForm::isCustomDataEmpty($orderTableForm->tables)) {
				foreach($orderTableForm->tables as $table) {
					if(count($table['rows']) > 0) {
						echo $this->renderFile('@app/views/order/inc/table/table.php', ['table' => $table, 'edit' => true]);
					}
				}
			}
		?></div>
		<div class="ajax-loader">
			<?php echo Html::img('@web/images/ajax-loader.gif') ?>
		</div>
	<?php } 
	else { ?>
		<?= $form->field($orderTableForm, 'isVisible')->hiddenInput()->label(false) ?>
		<?= $form->field($orderTableForm, 'columnCount')->hiddenInput()->label(false) ?>
	<?php } ?>
</div>

<?php
    $this->registerJs('
				initializeAddTable("custom-tables", {
						rowUrl: "' . Url::to(['protocol/get-table']) . '",
	    				edit: true,
	                    sectionClass: "order-table-form",
    					limitToSingle: true,
    					tableViewsDir: "'.'@app/views/order/inc/table'.'",
		        });
	        ', View::POS_END);
?>