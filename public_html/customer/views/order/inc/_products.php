<?php
use yii\helpers\Url;
use customer\models\OrderProductForm;
use frontend\models\ProductAttributeForm;
use common\helpers\StringHelper;
?>

<div class="col-xs-12 title-with-icon">
	<div class="base-icon box-icon main-content-icons"></div><h3><?= Yii::t('web', 'Add products') ?></h3>
</div>
<div class="col-xs-12 well">
	<div class="row">
	<?php if(!Yii::$app->params['isOrderProductDimensionsVisible']) { ?>
    	<div  class="items-container col-xs-12">
			<div class="row">
                <div class="col-sm-4">
                    <?= Yii::t('web', 'Name') ?>        
                </div>
                <div class="col-sm-<?= Yii::$app->params['isOrderGrossValuesVisible'] ? 1 : 2 ?>">
                    <?= Yii::t('web', 'Count') ?>
                </div>
                <div class="col-sm-1">
                    <?= Yii::t('web', 'Count unit') ?>
                </div>
                <div class="col-sm-<?= Yii::$app->params['isOrderGrossValuesVisible'] ? 1 : 2 ?>">
                   <?= Yii::t('web', 'Price for unit') ?>
                </div>
                <div class="col-sm-1" style="display:none">
                   <?= Yii::t('main', 'Currency') ?>
                </div>
                <div class="col-sm-<?= Yii::$app->params['isOrderGrossValuesVisible'] ? 1 : 2 ?>">
                    <?= Yii::t('web', 'Net value') ?>
                </div>
                <?php if(Yii::$app->params['isOrderGrossValuesVisible']) { ?>
	                <div class="col-sm-1">
	                    <?= Yii::t('web', 'Vat rate') ?>
	                </div>
	                <div class="col-sm-1">
	                    <?= Yii::t('web', 'Vat amount') ?>
	                </div>
	                <div class="col-sm-1">
	                    <?= Yii::t('web', 'Gross value') ?>
	                </div>
                <?php } ?>
                <div class="col-sm-1">
                   <?= Yii::t('web', 'Action') ?>
                </div>
            </div>
        </div>
        <?php } ?>
		<div id="order-products" class="items-container">
            
		    <?php
		        $productsForms = empty($orderProductsForms) ? [new OrderProductForm()] : $orderProductsForms;
		        $productAttributeForms = empty($productAttributeForms) ? [new ProductAttributeForm()] : $productAttributeForms;
		        foreach($productsForms as $prefix => $orderProductForm) {
		            echo Yii::$app->controller->renderPartial('inc/_product', [
		                'displayFirstAttributes' => $displayFirstAttributes && $prefix == 0,
		                'orderProductForm' => $orderProductForm,
		                'currencies' => $currencies,
		                'constructions' => $constructions,
		                'shapes' => $shapes,
		                'muntins' => $muntins,
		                'prefix' => $prefix,
		                'form' => $form,
                        'plnCurrency' =>  $plnCurrency,
		                'productAttributeForm' => empty($productAttributeForms[$prefix]) ? new ProductAttributeForm() : $productAttributeForms[$prefix],
		            ]);
		        }
		    ?>
		    <?php if(!Yii::$app->params['isOrderProductSummaryCommon']) { ?>
	            <?php if(Yii::$app->params['isOrderGrossValuesVisible']) {
	            	foreach($orderProductsVats as $vat) { ?>
			            <div class="col-sm-12">
			            	<div class="row">
				                <div class="col-sm-1 col-sm-offset-7">
				                      <?= StringHelper::getFormattedCost($vat['netValue']). ' ' . $plnCurrency->short_symbol ?>
				                </div>
				                <div class="col-sm-1">
				                    <?= $vat['vatLabel'] ?>
				                </div>
				                <div class="col-sm-1">
				                      <?= StringHelper::getFormattedCost($vat['vatAmount']). ' ' . $plnCurrency->short_symbol ?>
				                </div>
				                <div class="col-sm-1">
				                      <?= StringHelper::getFormattedCost($vat['grossValue']). ' ' . $plnCurrency->short_symbol ?>
				                </div>
			                </div>
			            </div>
		            <?php } ?>
		        <?php } ?>
	            <div class="col-sm-12">
	            	<div class="row">
		                <div class="col-sm-2 col-sm-offset-<?= Yii::$app->params['isOrderGrossValuesVisible'] ? 5 : 6 ?>">
		                    <?= Yii::t('main', 'Total') ?>
		                </div>
		                <div class="col-sm-1 <?= Yii::$app->params['isOrderGrossValuesVisible'] ? '' : 'col-sm-offset-1' ?>">
		                    <?= StringHelper::getFormattedCost($orderProductsSummary['netValue']). ' ' . $plnCurrency->short_symbol ?>
		                </div>
		                <?php if(Yii::$app->params['isOrderGrossValuesVisible']) { ?>
			                <div class="col-sm-1 col-sm-offset-1 ">
			                    <?= StringHelper::getFormattedCost($orderProductsSummary['vatAmount'] ). ' ' . $plnCurrency->short_symbol ?>
			                </div>
			                <div class="col-sm-1">
			                    <?= StringHelper::getFormattedCost($orderProductsSummary['grossValue']). ' ' . $plnCurrency->short_symbol ?>
			                </div>
		                <?php } ?>
	               	</div>
	            </div>
            <?php } ?>
	    </div>
	     <div class="col-xs-12">
			<a href="#" id="add-product-link" class="tab-me focusable-link add-item-link">
            	<span class="glyphicon glyphicon-plus-sign"></span>
            	<span><?= Yii::t('web', 'Add product') ?></span>
			</a>
        	<div class="btn btn-link remove-all-items-btn-custom" data-items-container-selector="#order-products">
	            <span class="glyphicon glyphicon-minus-sign"></span>
	            <span>
	                <?= Yii::t('web', 'Remove all products') ?>
	            </span>
	        </div>
	    </div>
	</div>
</div>
