<?php
use yii\helpers\Html;
use frontend\helpers\UserHelper;
use yii\helpers\Url;
use common\helpers\StringHelper;
?>

<div class="internal-user-list">
	<?php foreach($users as $user) { ?>
		<div class="user-row col-sm-12">
			<span class="photo-container">
				<?= '<div class="photo" style="background: url('.(!empty($user['urlPhoto']) ? $user['urlPhoto'] : Url::to('@web/images/placeholders/person.png')).')"></div>'; ?>
			</span>
			<span class="name"><?php 
				$line = UserHelper::getPrettyUserName($user['email'], $user['firstName'], $user['lastName']); 
				if(!empty($user['contactEmail'])) {
					$line .= ', <a href="mailto:'.$user['contactEmail']
							.'?subject='.StringHelper::translateOrderToOffer('main', 'Order').' '.$number.' - '.$entityName
					.'">'.$user['contactEmail'].'</a>';
				}
				if(!empty($user['phone'])) {
					$line .= ', '.Yii::t('web', 'ph.').' '.$user['phone'];
				}
				
				echo $line;
			?></span>
		</div>
	<?php } ?>
</div>