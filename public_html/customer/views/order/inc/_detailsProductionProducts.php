<?php
use yii\helpers\Html;
use yii\helpers\Url;
use common\helpers\StringHelper;
use common\helpers\Utility;

?>
	<div class="row">
    	<?php foreach($products as $product) {?>

        <div class="col-xs-12">
    		<h4><?= Html::encode($product['symbol'])?></h4>
    		
    		<div class="table-responsive">
            	<table class="table table-bordered table-hover detail-table">
                	<tbody>
                    	<tr>
                    		<th><?= Yii::t('web', 'Product code') ?></th>
                			<td><?= Html::encode($product['symbol']) ?></td>
                		</tr>
                    	<tr>
                    		<th><?= Yii::t('web', 'Product name') ?></th>
                    		<td><?= Html::encode($product['name']) ?></td>
                		</tr>
                		<tr>
                    		<th><?= Yii::t('main', 'Price [per sq. m]')?></th>
                    		<td><?= Html::encode(!empty($product['price']) ? StringHelper::getFormattedCost($product['price']) . ' ' . $product['currency'] : '') ?></td>
                		</tr>
                    	<tr>
                    		<th><?= Yii::t('web', 'Count') ?></th>
                    		<td><?= Html::encode($product['count']) ?></td>
                		</tr>
                		<tr>
                    		<th><?= Yii::t('web', 'Net value')?></th>
                    		<td><?= Html::encode(StringHelper::getFormattedCost($product['netValue']). ' ' . $plnCurrency->short_symbol) ?></td>
                		</tr>
                		<?php if(Yii::$app->params['isOrderProductDimensionsVisible']) { ?>
                        	<tr>
                        		<th><?= Yii::t('web', 'Width') . ' [mm]' ?></th>
                        		<td><?= Html::encode($product['width']) ?></td>
                    		</tr>
                        	<tr>
                        		<th><?= Yii::t('web', 'Height') . ' [mm]'?></th>
                        		<td><?= Html::encode($product['height']) ?></td>
                    		</tr>
                        	<tr>
                        		<th><?= Yii::t('web', 'Shape')?></th>
                        		<td><?= Html::encode($product['shape']) ?></td>
                    		</tr>
                    		<tr>
                        		<th><?= Yii::t('web', 'Construction')?></th>
                        		<td><?= Html::encode($product['construction']) ?></td>
                    		</tr>
                    		<tr>
                        		<th><?= Yii::t('web', 'Muntin')?></th>
                        		<td><?= Html::encode($product['muntin']) ?></td>
                    		</tr>

                            <?php if(!empty($product['ralColourSymbol'])) {?>
                                <tr>
                                    <th><?= Yii::t('web', 'RAL colour')?></th>
                                    <td><?= Html::encode($product['ralColourSymbol']) . '&nbsp'?>
                                        <span class="colour-rectangle" style="background-color:<?= Html::encode($product['ralRgbHash'])?>"></span>
                                    </td>
                                </tr>
                            <?php }?>


                    		<?php if(!empty($product['remarks'])) {?>
                        		<tr>
                            		<th><?= Yii::t('web', 'Remarks 1')?></th>
                            		<td><?= Html::encode($product['remarks']) ?></td>
                        		</tr>
                        	<?php }?>
                        	<?php if(!empty($product['remarks2'])) {?>
                        		<tr>
                            		<th><?= Yii::t('web', 'Remarks 2')?></th>
                            		<td><?= Html::encode($product['remarks2']) ?></td>
                        		</tr>
                    		<?php }?>
                		<?php } ?>
                		<?php if(Yii::$app->params['isProductAttributesVisible']) { ?>
                    		<tr>
                        		<th><?= Yii::t('web', 'Additional options')?></th>
                        		<td><?= Html::encode($product['attributes']) ?></td>
                    		</tr>
                		<?php } ?>
                        <?php if(Yii::$app->params['isAttachmentForProductVisible'] && isset($product['attachments']) && !empty($product['attachments'])) {?>
                            <tr>
                                <th><?= Yii::t('web', 'Attachments') ?></th>
                                <td><?= $this->render('/inc/attachmentList', ['attachments' => $product['attachments'], 'disableDelete' => true]) ?></td>
                            </tr>
                        <?php } ?>
                	</tbody>
            	</table>
        	</div>
            <hr>
        </div>
    	<?php } ?>
	</div>
<?php 