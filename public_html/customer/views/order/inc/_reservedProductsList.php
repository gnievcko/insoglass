<?php
use yii\helpers\Html;
use yii\helpers\Url;
use common\helpers\StringHelper;
use common\bundles\EnhancedDialogAsset;
?>

<script>
function toggleScroll() { 
	var x = document.getElementById('reserved-products-table');
	var arrow = document.getElementById('toggle-arrow'); 
	var text = document.getElementById('toggle-text');
	if (x.style.display === 'none') {
        x.style.display = 'block';
        arrow.className = "glyphicon glyphicon-chevron-up";
        text.innerHTML = "<?php echo Yii::t('web', 'Collapse')?>"
    } else {
        x.style.display = 'none';
        arrow.className = "glyphicon glyphicon-chevron-down";
        text.innerHTML = "<?php echo Yii::t('web', 'Expand')?>"
    }
}
</script>

<?php ?>
<h4 class="page-subtitle"><?= StringHelper::translateOrderToOffer('web', 'Reserved order products') ?></h4>
<div class="btn btn-link add-item-button expand" onclick="toggleScroll();">
	<span class="glyphicon glyphicon-chevron-down" id="toggle-arrow"></span>
     <span id='toggle-text'><?= Yii::t('web', 'Expand') ?></span>
</div>
<div class="table-responsive" id="reserved-products-table"  style=" display: none">
<table class="table grid-table">
	<thead>
		<tr>
			<th><?= Yii::t('web', 'Name') ?></th>
			<th><?= Yii::t('web', 'Count') ?></th>
			<th><?= Yii::t('main', 'User') ?></th>
			<th><?= Yii::t('web', 'Action') ?></th>
		</tr>
	</thead>
    <tbody>
    	<?php foreach($reservedProducts as $product) { ?>
    		<tr <?= !empty($product['isArtificial']) ? 'class="artificial"' : '' ?>>
    			<td>
    				<?= ($product['parentName'] ?: $product['name']).(!empty($product['parentName']) ? ' ('.strtolower(Yii::t('web', 'Version')).': '.$product['name'].')' : '') ?>
    			</td>
    			<td><?= !empty($product['isArtificial']) ? '-' : $product['count'] ?></td>
    			<td><?= (!empty($product['user']) ? $product['user'] : '') ?></td>
    			<td class="action">
    				<?= !empty($product['isArtificial']) ? '-' : 
    					Html::a('', Url::to(['warehouse-product/details', 'productId' => $product['id']]), ['class' => 'base-icon details-icon action-icon', 'title' => Yii::t('web', 'Details')])
    							.'&nbsp;'.Html::a('', ['warehouse-product/undo-reservation'], [
												'class' => 'undo-reserv',
                                                'title' => Yii::t('web', 'Undo reservation'),
												'data' => [
										                'confirm' => Yii::t('web', 'Are you sure you want to undo this reservation?'),
										                'method' => 'post',
														'params' => [
																'operationId' => $product['operationId'], 
																'orderId' => $orderId,
														],
									            ],
										]);
    				?>
    			</td>
    		</tr>
    	<?php } ?>
    </tbody>
</table>
</div>

<?php EnhancedDialogAsset::register($this); ?>