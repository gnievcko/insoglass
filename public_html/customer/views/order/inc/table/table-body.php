<?php

use yii\bootstrap\Html;
use frontend\models\OrderTableForm;

$option = ['class' => 'form-control xSmallTextarea', 'placeHolder' => Yii::t('documents', 'Write thead name')];
?> 

<div>
	<table class="table page-break-inside">
		<thead>
        	<tr>
	           	<?php 
	           	$i = 0;
	           	foreach($table['theads'] as $key => $th) {
	           		//$style = ($i == 0 ? 'width: 70px' : ($i == 1 ? 'width: 350px' : ''));
	           		$style = ($i == 0 ? 'width: 6%' : ($i == 1 ? 'width: 29%' : ''));
	           		if(!empty($style)) {
	           			$style = ' style="'.$style.'"';
	           		}
	           		
	            	echo '<th'.$style.'>'.Html::textarea('tables['.$uniq.'][theads]['.$key.']', (isset($table['theads'][$key]) ? $table['theads'][$key] : ''), $option).'</td>';
	            	$i++;
	            } ?>
	        	<th></th>
	        </tr>
        </thead>
        <tbody>
            <?php 
            	foreach($table['rows'] as $idx => $row): ?>
                <?=
	                Yii::$app->controller->renderFile(__DIR__.'/row.php', [
	                		'table' => $table,
	                    	'idx' => $idx,
	                    	'row' => $row,
	                    	'edit' => $edit,
	                    	'uniq' => $uniq,
	                    	'formModel' => new OrderTableForm(),
	                ]);
                ?>
            <?php endforeach; ?>
        </tbody>
        <tfoot>
        	<tr>
	        	<?php for($i = 0; $i < count($table['theads']) - 2; ++$i) { ?>
	        		<td></td>
	        	<?php } ?>
	        	<td><?= Yii::t('main', 'Total') ?></td>
	        	<td><span name="tables[<?= $uniq ?>][summary]">0</span></td> 
	        	<td></td>
        	</tr>
        </tfoot>
    </table>
</div>