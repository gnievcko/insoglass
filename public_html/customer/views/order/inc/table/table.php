<?php

use yii\bootstrap\Html;
use yii\helpers\Url;


$uniq = uniqid();
?>
<div class="document-tables-<?= $uniq ?> document-table" >
    
    <?= Yii::$app->controller->renderFile(__DIR__.'/table-body.php', ['table' => $table, 'uniq' => $uniq, 'edit' => $edit]) ?>

    <?php if($edit): ?>
        <div class="ajax-loader text-center">
            <?php echo Html::img('@web/images/ajax-loader.gif') ?>
        </div>
    <?php endif; ?>

    <?php if($edit): ?>
        <span class="add-row-button btn btn-link">
            <span class="base-icon link-icon add-icon"></span>
            <span><?= Yii::t('web', 'Add row') ?></span>
        </span>
    <?php endif; ?>

</div>
<?php
$script = '
        (function() {
            initializeTables(
                "document-tables-'.$uniq.'",
                {
                    rowUrl: "'.Url::to(['protocol/get-row']).'",
                    uniq: "'.$uniq.'",
                    edit: '.(int) $edit.',
                    type: "",
                    count: '.count($table['theads']).',
                    tableViewsDir: "'.'@app/views/order/inc/table'.'",
        			startIdx: '.(isset($table['rows']) ? count($table['rows']) : 0).'
                }
            );
        })()
    ';
if(isset($showScript) && $showScript) {
    echo '<script>'.$script.'</script>';
} else {
    $this->registerJs($script);
}
?>
