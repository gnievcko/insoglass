<?php

use yii\helpers\Url;
use yii\helpers\Html;
use customer\models\OrdersListForm;
use yii\widgets\LinkPager;
use common\widgets\TableHeaderWidget;
use common\helpers\StringHelper;
use common\helpers\Utility;
use common\bundles\EnhancedDialogAsset;
?>

<?php if(empty($orders)) { ?>
    <label><?= Yii::t('web', 'No search results'); ?></label>
    <br><br>
<?php } else {
    ?>
    <div class="table-responsive">
        <table class="<?= TableHeaderWidget::TABLECLASS ?>">
            <?php echo TableHeaderWidget::widget(['fields' => OrdersListForm::getSortFields(), 'listForm' => $ordersListForm, 'iconCount' => 4]); ?>
            <tbody>
                <?php foreach($orders as $order): ?>
	                <tr class="clickable-table-row" data-href="<?= Url::to(['order/details', 'id' => $order['id']])?>">
                        <td><?= Html::encode($order['number']) ?></td>
                        <?php if(!OrdersListForm::isHidden('orderNumberForClient')) {?><td><?= $order['numberForClient'] ?? '-' ?></td><?php } ?>
                        <td><?= Html::encode($order['title']) ?></td>
                        <td><?= Html::encode(StringHelper::getFormattedDateFromTimestamp($order['date_creation_timestamp'])) ?></td>
                        <td><?= Html::encode($order['date_shipping']) ?></td>
                        <!--<td><?= Html::encode($order['companyName']) . (!empty($order['parentCompanyName']) ? Html::encode(' (' . $order['parentCompanyName'] . ')') : '') ?></td>-->
                        <?php if(Yii::$app->params['isContractTypeVisible']) { ?>
                                <!--<td><?= Html::encode($order['contractType']) ?></td>-->
                        <?php } ?>
                        <?php if(!OrdersListForm::isHidden('user_responsible')) {?><td><?= Html::encode($order['userResponsible']) ?></td><?php } ?>
                        <td><?= Html::encode($order['statusName']) ?></td>
                        <!--<td><?= Html::encode(StringHelper::getFormattedDateFromTimestamp($order['date_modification_timestamp'])) ?></td> -->
                        <td>
                            <?php
                                echo Html::a('', Url::to(['order/details', 'id' => $order['id']]), ['class' => 'base-icon details-icon action-icon', 'title' => Yii::t('web', 'Details')]);
                                if($order['orderStatusSymbol'] == Utility::ORDER_STATUS_NEW) {
                                    echo '&nbsp;'.Html::a('', Url::to(['order/delete']), [
                                        'class' => 'base-icon delete-icon action-icon',
                                        'title' => Yii::t('main', 'Delete'),
                                        'data' => [
                                            'confirm' => Yii::t('main', 'Are you sure you want to delete this item?'),
                                            'method' => 'post',
                                            'params' => [
                                                'id' => $order['id'],
                                            ],
                                        ],
                                    ]);
                                }
                            ?>
                        </td>
                    </tr>
                <?php endforeach ?>
            </tbody>
        </table>
    </div>
    <div class="col-sm-12">
        <div class="col-sm-4 text-left dynamic-table-counter">
            <?= Yii::t('web', 'Number of found items: {n}', ['n' => $pages->totalCount]) ?>
        </div>
        <div class="col-sm-8 text-right dynamic-table-pagination">
            <?= LinkPager::widget(['pagination' => $pages, 'firstPageLabel' => true, 'lastPageLabel' => true]) ?>
        </div>
    </div>
<?php } ?>

<input class="dynamic-table-sort-dir" type="hidden" name="<?= Html::getInputName($ordersListForm, 'sortDir') ?>" value="<?= $ordersListForm->sortDir ?>" />
<input class="dynamic-table-sort-field" type="hidden" name="<?= Html::getInputName($ordersListForm, 'sortField') ?>" value="<?= $ordersListForm->sortField ?>" />
<input class="dynamic-table-page" type="hidden" name="<?= Html::getInputName($ordersListForm, 'page') ?>" value="<?= $ordersListForm->page ?>" />

<?php
EnhancedDialogAsset::register($this);
?>
