<?php
use yii\helpers\Html;
use yii\helpers\Url;
use frontend\models\DocumentTemporaryStorageForm;
use frontend\models\PhotoTemporaryStorageForm;

$documentTemporaryStorageForm = new DocumentTemporaryStorageForm();
$photoTemporaryStorageForm = new PhotoTemporaryStorageForm();
?>
<div class="col-xs-12 title-with-icon">
	<div class="base-icon add-gallery-icon main-content-icons"></div>
    <h3><?= Yii::$app->params['isOrderGalleryVisible'] ? Yii::t('web', 'Gallery') : Yii::t('web', 'Documents') ?></h3>
</div>
<?php if(Yii::$app->params['isOrderGalleryVisible']) { ?>
	<div class="col-xs-12">
		<div class="gal-padding">
		    <div class="row">
		    	<div class="col-sm-6">
		        	<div class="file-upload-trigger">
		                <div class="ajax-loader">
		                	<?= Html::img('@web/images/ajax-loader.gif', []) ?>
		                </div>
		                
		                <div class="add-photo text-center btn-sm">
		                	<div class="icon-img add-pic"></div> 
		                    <div class="file-upload-title"><?= Yii::t('web', 'Add photo') ?></div>  
		                </div>
		            
    		            <?= $form->field($photoTemporaryStorageForm, 'file')->fileInput([
    		                'accept' => PhotoTemporaryStorageForm::getExtensionsString('.'),
    		                'class' => 'photo-upload',
    		            ])->label(false) ?>
    		            <div class="supp-form text-center">
    		            	<?= Yii::t('web', 'Supported formats') ?>: <?= PhotoTemporaryStorageForm::getExtensionsString() ?>
		            	</div>
		        	</div>
		        </div>
				<div class="col-sm-6">
        			<h4 class="text-center"><?= Yii::t('web', 'Gallery') ?></h4>
        		    <div class="gallery" data-file-storage-url="<?= Url::to(['storage/upload']) ?>" data-photo-form-url="<?= Url::to(['storage/get-photo-form']) ?>">
        		        <?php 
        		            $readOnlyPhotosForms = empty($readOnlyPhotosForms) ? [] : $readOnlyPhotosForms;
        		            foreach($readOnlyPhotosForms as $photoForm) {
        		                echo Yii::$app->controller->renderPartial('/inc/photo_preview', ['photoForm' => $photoForm]);
        		            }
        		            $prefix = 0;
        		            foreach($photosForms as $photoForm) { 
        		                echo Yii::$app->controller->renderPartial('/inc/photo', ['prefix' => $prefix, 'photoForm' => $photoForm]);
        		                ++$prefix;
        		            }
        		        ?>
        		    </div>
        		    <div class="gallery-hint" style="display:<?= empty($photosForms) && empty($readOnlyPhotosForms) ? 'block' : 'none' ?>">
        		        <?= Html::tag('span', Yii::t('web', 'No photos'), ['class' => 'hint-block text-center']); ?>
        		    </div>
		    	</div>
		    </div>
		</div>
	</div>	
<?php } ?>	
<div class="col-xs-12">
	<div class="gal-padding">
	    <div class="row">	
	    	<div class="col-sm-6">	
    	        <div class="file-upload-trigger">
    	        	<div class="ajax-loader">
    	            	<?= Html::img('@web/images/ajax-loader.gif', []) ?>
    	            </div>
    	            <div class="add-photo text-center btn-sm">
    					<div class="icon-img add-pic"></div>
    	                <div class="file-upload-title"><?= Yii::t('web', 'Add file') ?></div>
    	            </div>
    	            
    	            <?= $form->field($documentTemporaryStorageForm, 'file')->fileInput([
    	                'accept' => DocumentTemporaryStorageForm::getExtensionsString('.'),
    	                'class' => 'document-upload',
    	            ])->label(false) ?>
    	            <div class="supp-form text-center"><?= Yii::t('web', 'Supported formats') ?>: <?= DocumentTemporaryStorageForm::getExtensionsString() ?></div>
    	        </div>
	        </div>
	    
			<div class="col-sm-6">	
        	    <h4 class="text-center"><?= Yii::t('web', 'Files') ?></h4>
        	    <div class="documents-container" data-file-storage-url="<?= Url::to(['storage/upload']) ?>" data-document-form-url="<?= Url::to(['storage/get-document-form']) ?>"> 
        	        <?php 
        	            $readOnlyDocumentsForms = empty($readOnlyDocumentsForms) ? [] : $readOnlyDocumentsForms;
        	            foreach($readOnlyDocumentsForms as $documentForm) { 
        	                echo Yii::$app->controller->renderPartial('/inc/document_preview', ['documentForm' => $documentForm]);
        	            }
        	            $prefix = 0;
        	            foreach($documentsForms as $documentForm) { 
        	                echo Yii::$app->controller->renderPartial('/inc/document', ['prefix' => $prefix, 'documentForm' => $documentForm]);
        	                ++$prefix;
        	            }
        	        ?>
        	    </div>
        	    <div class="documents-hint" style="display:<?= empty($documentsForms) && empty($readOnlyDocumentsForms) ? 'block' : 'none' ?>">
        	        <?= Html::tag('span', Yii::t('web', 'No attachments'), ['class' => 'hint-block text-center']) ?>
        	    </div>
    	    </div>
	    </div>
	</div>
</div>
