<?php

use yii\helpers\Url;
use kartik\datecontrol\DateControl;
use common\helpers\StringHelper;
use kartik\select2\Select2;
use yii\web\JsExpression;
use common\models\aq\OrderTypeQuery;
use yii\helpers\ArrayHelper;
?>

<div class="col-xs-12 title-with-icon">
    <div class="base-icon basic-data-icon main-content-icons"></div>
    <h3><?= Yii::t('web', 'Basic data') ?></h3>
</div>
	<?php if(Yii::$app->params['isSuborderForOrder']) { ?>
    <div class="col-xs-12">
    	<div class="row">
	    	<div class="col-sm-6">
	    		<?php $order = Order::findOne($orderCreateForm->parentOrderId)?>
		    		<?= $form->field($orderCreateForm, 'parentOrderId')->widget(Select2::classname(), [
			                    'language' => Yii::$app->language,
			                    'showToggleAll' => false,
		    					'options' => ['placeholder' => StringHelper::translateOrderToOffer('web', 'Choose parent order'), 'id' => 'ordercreateform-parentorderid', 'class' => 'form-control'],
		    					'data' => !empty($order) ? [$orderCreateForm->parentOrderId => $order->number . ' (' . $order->title . ')'] : [],
			                    'pluginOptions' => [
			                            'allowClear' => true,
			                            'minimumInputLength' => 1,
			                            'ajax' => [
			                                    'url' => Url::to(['order/find-parents']),
			                                    'dataType' => 'json',
			                                    'delay' => 250,
			                                    'data' => new JsExpression('function(params) { 
			                                        return {term:params.term, page:params.page, size: 20}; 
			                                    }'),
			                                    'processResults' => new JsExpression('function (data, params) { 
			                                        params.page = params.page || 1; 
			                                        console.log(data.items)
			                                        return {
			                                            results: data.items,
			                                            pagination: {
			                                                more: data.more
			                                            }
			                                        };
			                                    }'),
			                                    'cache' => true
			                            ],
			                    		'templateResult' => new JsExpression('function(item) { if(item.loading) return item.text; return item.name; }'),
			                    		'templateSelection' => new JsExpression('function (item) { return item.name || item.text; }'),
			                    ],
			        		'pluginEvents' => [ ],
						]); 
			        ?>
	    	</div>
	    </div>
    </div>
    <div class="col-sm-12">
    	<div class="row">
	    	<div class="col-sm-6"><hr></div>
	    </div>
    </div>
   	<?php }?>

<?= Yii::$app->controller->renderPartial('inc/_clients', ['orderCreateForm' => $orderCreateForm, 'form' => $form]) ?>

<div class="col-sm-12">
    <div class="row">
        <div class="col-sm-6">
            <?= $form->field($orderCreateForm, 'title')->textarea(['class' => 'form-control smallTextarea', 'placeHolder' => $orderCreateForm->getAttributeLabel('title'), 'maxLength' => true]); ?>
        </div>
        <div class="col-sm-5 col-sm-offset-1">
            <?= $form->field($orderCreateForm, 'number')->textInput(['placeHolder' => $orderCreateForm->getAttributeLabel('number'), 'readonly' => true, 'maxLength' => true]) ?>
        </div>
    </div>
</div>

<?php if(Yii::$app->params['isNumberForClientVisible'] || Yii::$app->params['isUserResponsibleVisible']) { ?>
    <div class="col-xs-12">
	    <div class="row">
	        <div class="col-sm-6">
	            <?php if(Yii::$app->params['isUserResponsibleVisible']) { ?>
	                <?= Yii::$app->controller->renderPartial('inc/_userResponsible', ['orderCreateForm' => $orderCreateForm, 'form' => $form]) ?>
	            <?php } ?>
	        </div>
	        <div class="col-sm-5 col-sm-offset-1">
	            <?php if(Yii::$app->params['isNumberForClientVisible']) { ?>
	                <?= $form->field($orderCreateForm, 'numberForClient')->textInput(['placeHolder' => $orderCreateForm->getAttributeLabel('numberForClient'), 'maxLength' => true]) ?>
	            <?php } ?>
	        </div>
	    </div>
    </div>
<?php } ?>

<div class="col-xs-12">
	<div class="row"> 
	    <?php if(Yii::$app->params['isEntityVisible']) { ?>
	        <div class="col-sm-6">
	            <?= $form->field($orderCreateForm, 'entityId')->dropDownList($entities); ?>    	
	        </div>
	    <?php } else { ?>
	        <?= $form->field($orderCreateForm, 'entityId')->hiddenInput(['value' => key($entities)])->label(false); ?>
	    <?php } ?>
	    <?php if(Yii::$app->params['isOrderTemplateVisible']) { ?>
	        <div class="col-sm-5 col-sm-offset-1">
	            <div><?= $form->field($orderCreateForm, 'isTemplate')->checkbox() ?></div>   
	        </div>
	    <?php } ?>
	</div>  
</div>

<div class="col-xs-12">
	<div class="row">
	    <div class="col-sm-6">
	        <?= $form->field($orderCreateForm, 'description')->textarea(['placeholder' => $orderCreateForm->getAttributeLabel('description'), 'maxLength' => true]) ?>
	    </div>
    </div>
</div>
<?php
$this->registerJs('$("#ordercreateform-shippingdate-disp").attr("placeholder","' . Yii::t('web', 'Write shipping date') . '");');
?>
    