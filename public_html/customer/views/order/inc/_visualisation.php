<?php

use yii\helpers\Html;
use yii\helpers\Url;
use frontend\models\PhotoTemporaryStorageForm;
use yii\web\View;
use kartik\widgets\FileInput;

$temporaryStorageForm = new PhotoTemporaryStorageForm();
?>


<div class="col-md-5 visualisation">	<!-- visualisation -->
    <?= $form->field($productForm, "[{$productPrefix}]urlPhoto")->hiddenInput(['class' => 'urlPhoto'])->label(false); ?>

    <?= $form->field($productForm, "[{$productPrefix}]heightPhoto")->hiddenInput(['class' => 'urlPhoto'])->label(false); ?>
    <?= $form->field($productForm, "[{$productPrefix}]widthPhoto")->hiddenInput(['class' => 'urlPhoto'])->label(false); ?>
    <?= $form->field($productForm, "[{$productPrefix}]topPhoto")->hiddenInput(['class' => 'urlPhoto'])->label(false); ?>
    <?= $form->field($productForm, "[{$productPrefix}]leftPhoto")->hiddenInput(['class' => 'urlPhoto'])->label(false); ?>

    <div class="row">
        <div class="col-xs-12" >
            <h4><?= Yii::t('web', 'Visualisation') ?></h4>
            <?= Html::button(Yii::t('main', 'Generate visualizations'), ['class' => 'btn btn-default generateVisualizations', 'id' => $typeProduct . '-' . $productPrefix . '-generate', 'disabled' => 'true']); ?>
            <h6><?= Yii::t('web', 'To generate visualisation you must choose shape first.') ?></h6>
            <?= Html::hiddenInput('regenerateVisualizations', '', ['class' => 'regenerateVisualizations']); ?>

            <?=
            Yii::$app->controller->renderPartial('inc/_visualisationImg', [
                'files' => $photos,
                'productForm' => $productForm,
                'form' => $form,
                'productPrefix' => $productPrefix
            ]);
            ?>

            <span class="glyphicon glyphicon-plus plus-img cursor-pointer" title="<?= Yii::t('main', 'Increase photo')?>"></span>
            <span class="glyphicon glyphicon-minus minus-img cursor-pointer" title="<?= Yii::t('main', 'Decrease photo')?>"></span>
            <span class="glyphicon glyphicon-trash remove-img cursor-pointer" title="<?= Yii::t('main', 'Remove photo')?>"></span>
        </div>
        <div class="col-xs-12">
            <div class="svg-container">
                <svg class="product-visualisation" id="<?= $typeProduct . '-' . $productPrefix . '-svg' ?>" version="1.1" baseProfile="full" xmlns="http://www.w3.org/2000/svg"/>
            </div>
        </div>
    </div>
</div>

<?php
$this->registerJs("
	window['removedFileSuccessMsg'] = '" . Yii::t('web', 'The attachment has been deleted.') . "';
	window['removedFileFailMsg'] = '" . Yii::t('web', 'The attachment cannot be deleted. Please contact the administrator.') . "';", View::POS_END);
?>