<?php 
use common\helpers\StringHelper;
use yii\helpers\Html;
?>
<div class="col-xs-12 title-with-icon">
	<div class="base-icon history-icon main-content-icons"></div>
	<h3><?= StringHelper::translateOrderToOffer('web', 'History order status') ?></h3>
</div>

<div class="col-xs-12">
	<div class="row">
		<?php foreach($entries as $entry) { ?>
	    	<div class="col-sm-8 hist">
	    		<div class="row">
			        <div class="col-xs-6"><?php
		    	    	$text = '';
		                	if(!empty($entry['firstName']) && !empty($entry['lastName'])) {
		                        $text = $entry['firstName'] . ' ' . $entry['lastName'];
		                    } else {
		                        $text = $entry['email'];
		                    }
		
		                    echo Html::mailto(Html::encode($text), $entry['email']);
					?></div>
		            <div class="col-xs-6 text-right"><?php echo Yii::t('web', 'Status') . ': <strong>' . $entry['status'] . '</strong>' ?></div>
		            <div class="col-xs-12 data"><?= Html::encode(StringHelper::getFormattedDateFromDate($entry['dateCreation'], true)) ?></div>
					<div class="col-xs-12"><?= nl2br(Html::encode($entry['message'])) ?></div>
				</div>
			</div>
		<?php } ?>
	</div>
</div>