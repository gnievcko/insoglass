<?php

use yii\helpers\Url;
use yii\web\JsExpression;
use frontend\helpers\UserHelper;
use kartik\select2\Select2;
use common\helpers\Utility;
use common\models\ar\User;
?>

<?=

$form->field($orderCreateForm, 'userResponsibleId')->widget(Select2::classname(), [
    'language' => Yii::$app->language,
    'showToggleAll' => false,
    'options' => ['placeholder' => Yii::t('main', 'User Responsible')],
    'data' => empty($orderCreateForm->userResponsibleId) ? [] : array_reduce(User::find()->where(['id' => $orderCreateForm->userResponsibleId])->all(), function($users, $user) {
                        $users[$user->id] = UserHelper::getPrettyUserName($user->email, $user->first_name, $user->last_name);
                        return $users;
                    }, []),
    'pluginOptions' => [
        'allowClear' => true,
        'minimumInputLength' => 1,
        'ajax' => [
            'url' => Url::to(['employee/find']),
            'dataType' => 'json',
            'delay' => 250,
            'data' => new JsExpression('function(params) { 
                                    return {term:params.term, page:params.page, size: 20, roles: ["' . Utility::ROLE_SALESMAN . '", "' . Utility::ROLE_MAIN_SALESMAN . '"]}; 
                                }'),
            'processResults' => new JsExpression('function (data, params) { 
                                    params.page = params.page || 1; 
                                    
                                    return {
                                        results: data.items.map(function(user) {
                                            var fullName = user.firstName + ((user.firstName == null) ? "" : " " ) + user.lastName;
                                            user.name = (fullName == "") ? user.email : fullName;
                                            return user;
                                        }),
                                        pagination: {
                                            more: data.more
                                        }
                                    };
                                }'),
            'cache' => true
        ],
        'templateResult' => new JsExpression('function(item) { if(item.loading) return item.text; return item.name; }'),
        'templateSelection' => new JsExpression('function (item) { return item.name || item.text; }'),
    ],
])->label(false);
?>
