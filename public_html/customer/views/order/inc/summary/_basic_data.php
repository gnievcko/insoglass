<?php
use yii\helpers\Html;
use common\models\ar\User;
?>
<div class="col-xs-12 title-with-icon">
    <div class="base-icon basic-data-icon main-content-icons"></div>
    <h3><?= Yii::t('web', 'General information') ?></h3>
</div>
<div class="col-xs-12">
    <div class="row">
        <div class=" col-sm-8">
            <div class="table-responsive">
                <table class="table table-bordered detail-table">
                    <tbody>
                        <tr>
                            <th><?= Yii::t('web', 'Number') ?></th>
                            <td>
                                <?= Html::encode($orderCreateForm->number) ?>
                            </td>
                        </tr>
                            <?php if(Yii::$app->params['isNumberForClientVisible'] && !empty($orderCreateForm->numberForClient)) { ?>
	                        	<tr>
		                            <th><?= Yii::t('main', 'Number for client') ?></th>
		                            <td><?= Html::encode($orderCreateForm->numberForClient) ?></td>
		                        </tr>
	                        <?php } ?>
                            
                            <tr>
	                            <th><?= Yii::$app->params['isOrderTitleIsLabeledAsConcerns'] ? Yii::t('web', 'Title') : (Yii::$app->params['isOrderTitleLabeledAsOrderName'] ? Yii::t('web', 'Job name') : Yii::t('web', 'Title')) ?></th>
	                            <td>
                                    <?= Html::encode($orderCreateForm->title) ?>
                                </td>
	                        </tr>
                        <tr>
                            <th><?= $orderCreateForm->getAttributeLabel('description') ?></th>
                            <td>
                                <?= Html::encode($orderCreateForm->description) ?>
                            </td>
                        </tr>
                        <tr>
	                            <th><?= Yii::t('web', 'Client') ?></th>
	                            <td>
	                                <?php
	                                echo Html::encode($data['companyName']);
	                                if(!empty($data['parentCompanyId']) && !empty($data['parentCompanyName'])) {
	                                    echo ' (' . Html::encode($data['parentCompanyName']) . ')';
	                                }
	                                ?>
	                            </td>
                        </tr>
                        <?php if(!empty($contactPeople)) { ?>
					    		<tr>
					    			<th><?= Yii::t('web', 'Contact people') ?></th>
					    			<td><?= Yii::$app->controller->renderPartial('inc/summary/_userRepresentatives', [
                                        'users' => $contactPeople, 
                                        'number' => HTML::encode($orderCreateForm['number']), 
                                        'entityName' => Html::encode($data['entity'])]) ?></td>
                                </tr>
				    	<?php } ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>