<?php
use yii\helpers\Html;
use frontend\helpers\UserHelper;
use yii\helpers\Url;
use common\helpers\StringHelper;
?>

<div class="internal-user-list">
    <?php $prefix = 0;?>
	<?php foreach($users as $user) { ?>
		<div class="user-row col-sm-12">
			<span class="photo-container">
				<?= '<div class="photo" style="background: url('.(!empty($user['urlPhoto']) ? $user['urlPhoto'] : Url::to('@web/images/placeholders/person.png')).')"></div>'; ?>
			</span>
			<span class="name"><?php 
				$line = UserHelper::getPrettyUserName($user['email'], $user['first_name'], $user['last_name']); 
				if(!empty($user['contact_email'])) {
					$line .= ', <a href="mailto:'.$user['contact_email']
							.'?subject='.StringHelper::translateOrderToOffer('main', 'Order').' '.$number.' - '.$entityName
					.'">'.$user['contact_email'].'</a>';
				}
				if(!empty($user['phone'])) {
					$line .= ', '.Yii::t('web', 'ph.').' '.$user['phone'];
				}
				
				echo $line;
                
			?></span>
		</div>
	<?php $prefix++; } ?>
</div>