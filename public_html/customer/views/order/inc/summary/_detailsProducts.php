<?php

use yii\helpers\Html;
use common\models\ar\ProductAttributeTypeTranslation;
use common\models\ar\Product;
use common\models\ar\OfferedProduct;
use common\helpers\Utility;

$prefix = 0;
$language = common\models\ar\Language::findOne(['symbol' => Yii::$app->language])->id;
?>
<?php foreach ($products as $product) { ?>

<h4><?= Html::encode($product['name']) ?></h4>
<div class="col-xs-12">
    <div class="table-responsive">
        <table class="table table-bordered detail-table">
            <tbody>
                <tr>
                <tr>
                    <th><?= Yii::t('web', 'Number of pieces') ?></th>
                    <td>
                        <?= Html::encode($product['count']) ?>
                    </td>
                </tr>
                <?php if(!empty($product['width'])) {?>
                <tr>
                    <th><?= Yii::t('web', 'Width') ?></th>
                    <td><?= Html::encode($product['width']) ?></td>
                </tr>
                <?php } ?>
                <?php if(!empty($product['height'])) {?>
                <tr>
                    <th><?= Yii::t('web', 'Height') ?></th>
                    <td><?= Html::encode($product['height']) ?></td>
                </tr>
                <?php } ?>
                <?php if(!empty($product['shapeId'])) {?>
                <tr>
                    <th><?= Yii::t('web', 'Shape') ?></th>
                    <td>
                        <?php
                            $shape = \common\models\ar\ShapeTranslation::find()->where(['shape_id' => $product['shapeId'], 'language_id' => $language])->one();
                            if(!empty($shape)){
                                    echo Html::encode($shape->name);
                            }
                        ?>
                    </td>
                </tr>
                <?php } ?>
                <?php if(!empty($product['constructionId'])) {?>
                <tr>
                    <th><?= Yii::t('web', 'Construction') ?></th>
                    <td><?php
                        $construction = \common\models\ar\ConstructionTranslation::find()->where(['construction_id' => $product['constructionId'], 'language_id' => $language])->one();
                        if(!empty($construction)){
                                        echo Html::encode($construction->name);
                                }
                        ?>
                    </td>
                </tr>
                <?php } ?>
                <?php if(!empty($product['muntinId'])) {?>
                <tr>
                    <th><?= Yii::t('web', 'Muntin') ?></th>
                    <td>
                        <?php
                        $muntin = \common\models\ar\MuntinTranslation::find()->where(['muntin_id' => $product['muntinId'], 'language_id' => $language])->one();
                        if(!empty($muntin)){
                                        echo Html::encode($muntin->name);
                        }
                                ?>
                    </td>
                </tr>
                <?php } ?>
                <?php if(!empty($product['remarks'])) {?>
                <tr>
                    <th><?= Yii::t('web', 'Remarks') ?></th>
                    <td>
                        <?= Html::encode($product['remarks']) ?>
                    </td>
                </tr>
                <?php } ?>
                <?php if(Yii::$app->params['isAttachmentForProductVisible'] && !empty($product['files'])){ ?>
                <tr>
                    <th><?= Yii::t('web', 'Attachments') ?></th>
                    <td>
                       <?php
                            foreach($product['files'] as $file) {
                                echo Yii::$app->controller->renderPartial('inc/summary/productFile', [
                                    'file' => (object)$file,
                                ]);
                            } ?>
                    </td>
                </tr>
                <?php }
                    ?>
                <?php if(Yii::$app->params['isProductAttributesVisible'] && !(isset($product['productType']) && $product['productType'] == Utility::PRODUCT_TYPE_PRODUCT)) {?>
                        <?php 
                            $fields = array_filter($productAttributeForms[$prefix]->fields, function($value){
                                return $value === '1' || !empty($value);
                            });
                            if(!empty($fields)){
                        ?>
                <tr><th><?= Yii::t('web', 'Additional options') ?></th>
                    <td>
                        
                    <?php foreach($fields as $attributeId => $value) {
                        $type = \common\models\ar\ProductAttributeType::find()->where(['id' => $attributeId])->one();
                        $attrName = \common\models\ar\ProductAttributeTypeTranslation::find()->where(['product_attribute_type_id' => $attributeId, 'language_id' => $language])->one();
                        if(empty($type) || empty($attrName))
                            continue;
                        $type = $type->variable_type;
                        $attrName = $attrName->name;
                        
                        if($type === 'bool') {
                            if($value === '1') {
                                echo Html::encode($attrName);
                            }
                        }else {
                            if(!empty($value)) {
                                echo Html::encode($attrName.' : '.$value);
                            }
                        }
                        echo '<br/>';
                     } ?></td>
                </tr>
                <?php } 
                
                            }?>
            </tbody>
        </table>
                
    </div>
</div>
    <?php $prefix++; } 

?>
