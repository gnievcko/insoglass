<?php
use yii\helpers\Html;
?>

<div class="document document-form">
    <?= Html::img('@web/images/placeholders/document_icon.png') ?>
    <span><?= $file->name ?></span>
</div>
