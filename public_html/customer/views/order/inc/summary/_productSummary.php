<?php
use yii\helpers\Html;
use common\models\ar\ProductAttributeTypeTranslation;
use common\helpers\StringHelper;

$prefix = 0;
$language = common\models\ar\Language::findOne(['symbol' => Yii::$app->language])->id;
?>
<div class="table-responsive">
    <table class="table table-hover list-grid-table">
        <thead>
        <th><?= Yii::t('web', 'Name'); ?></th>
        <th><?= Yii::t('web', 'Price [per sq. m]'); ?></th>
        <th><?= Yii::t('web', 'Net price'); ?></th>
        <th><?= Yii::t('web', 'Vat rate'); ?></th>
        <th><?= Yii::t('web', 'Gross price'); ?></th>
        </thead>
        <tbody>
            <?php foreach($products as $product) { ?>
                <tr>
                    <td>
                        <?php
                        if(isset($product['parentName'])) {
                            echo ($product['parentName'] ?: $product['name']) . (!empty($product['parentName']) ? ' (' . strtolower(Yii::t('web', 'Version')) . ': ' . $product['name'] . ')' : '');
                        } else {
                            echo $product['name'];
                        }
                        ?>
                    </td>

                    <td><?= (StringHelper::getFormattedCost($product['price']) ?: 0) . ' ' . $plnCurrency->short_symbol ?></td>
                    <td><?= StringHelper::getFormattedCost($product['netValue']) . ' ' . $plnCurrency->short_symbol ?></td>
                    <td><?= $product['vatRate'] ?></td>
                    <td><?= StringHelper::getFormattedCost($product['grossValue']) . ' ' . $plnCurrency->short_symbol ?></td>
                </tr>
                <?php
                $prefix++;
            }
            ?>
            <tr></tr>
        <th colspan="4"><?= Yii::t('web', 'Sum'); ?></th>
        <td><?= StringHelper::getFormattedCost($summary['grossValue']) . ' ' . $plnCurrency->short_symbol ?></td>
        </tbody>
    </table>
</div>
