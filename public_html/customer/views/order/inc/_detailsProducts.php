<div class="table-responsive">
    <table class="table table-hover list-grid-table">
        <thead>
            <tr>
                <th><?= Yii::t('web', 'Name') ?></th>
                <th><?= Yii::t('web', 'Count unit') ?></th>
                <th><?= Yii::t('web', 'Count') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach($products as $product) { ?>
                <tr <?= !empty($product['isArtificial']) ? 'class="artificial"' : '' ?>>
                    <td>
                        <?= ($product['parentName'] ?: $product['name']) . (!empty($product['parentName']) ? ' (' . strtolower(Yii::t('web', 'Version')) . ': ' . $product['name'] . ')' : '') ?>
                    </td>
                    <td><?= $product['unit'] ?></td>
                    <td><?= $product['count'] ?></td>
                </tr>
            <?php } ?>
        </tbody>
    </table>
</div>