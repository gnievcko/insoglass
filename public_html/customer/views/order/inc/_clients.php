<?php

use yii\helpers\Url;
use yii\web\JsExpression;
use common\models\ar\User;
use frontend\helpers\UserHelper;
use kartik\select2\Select2;

?>

<div class="col-sm-12">
    <div class="row">
        <div class="col-sm-6">
            <?= $form->field($orderCreateForm, 'clientId')->hiddenInput(['id' => 'ordercreateform-clientid'])->label(false); ?>
            <?= $form->field($orderCreateForm, 'client')->textInput(['readonly' => true])->label(false); ?>

        </div>
        <div class="col-sm-5 col-sm-offset-1">
            <?php
            $contactPeopleData = [];
            foreach(User::find()->where(['id' => $orderCreateForm->contactPeopleIds])->all() as $contactPerson) {
                $contactPersonData = $contactPerson->toArray();
                $contactPeopleData[$contactPersonData['id']] = //UserNameHelper::generateDisplayedName($contactPersonData);
                        UserHelper::getPrettyUserName(
                                !empty($contactPersonData['email']) ? $contactPersonData['email'] : '', !empty($contactPersonData['first_name']) ? $contactPersonData['first_name'] : '', !empty($contactPersonData['last_name']) ? $contactPersonData['last_name'] : ''
                );
            }
            ?>
            <?=
            $form->field($orderCreateForm, 'contactPeopleIds')->widget(Select2::classname(), [
                'language' => Yii::$app->language,
                'showToggleAll' => false,
                'options' => ['placeholder' => Yii::t('web', 'Choose contact person'), 'id' => 'ordercreateform-contactpeopleids'],
                'data' => $contactPeopleData,
                'pluginOptions' => [
                    'multiple' => true,
                    //'maximumSelectionLength' => 1,
                    'allowClear' => true,
                    'minimumInputLength' => 0,
                    'ajax' => [
                        'url' => Url::to(['company/find-employees']),
                        'dataType' => 'json',
                        'delay' => 250,
                        'data' => new JsExpression('function(params) { 
	                                        var orderClientId = $("#ordercreateform-clientid").val();
	                                        return {term:params.term, page:params.page, size: 20, companyId: orderClientId}; 
	                                    }'),
                        'processResults' => new JsExpression('function (data, params) { 
	                                        params.page = params.page || 1; 
	                                        
	                                        return {
	                                            results: data.items,
	                                            pagination: {
	                                                more: data.more
	                                            }
	                                        };
	                                    }'),
                        'cache' => true
                    ],
                    'templateResult' => new JsExpression('function(item) { 
	                                if(item.loading) return item.text; 
	                                var fullName = item.first_name + ((item.first_name == null) ? "" : " ") + item.last_name; 
	                                return (fullName == null) ? item.email : fullName;
	                            }'),
                    'templateSelection' => new JsExpression('function (item) { 
	                                if(item.first_name == null && item.last_name == null && item.email == null) {
	                                    return item.text;
	                                }
	                                else {
	                                    var fullName = item.first_name + ((item.first_name == null) ? "" : " ") + item.last_name; 
	                                    return (fullName == null) ? item.email : fullName;
	                                }
	                            }'),
                ],
                'pluginEvents' => [
                    'change' => 'function() {
	                            
	                        }'
                ],
            ])->label(false);
            ?>
        </div>
    </div>
</div>