<?php

/* @var $this yii\web\View */
/* @var $form kartik\widgets\ActiveForm */
/* @var $model \common\models\LoginForm */

use yii\helpers\Html;
use kartik\widgets\ActiveForm;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use frontend\assets\AppAsset;
use common\helpers\Utility;
use common\widgets\Alert;
use common\helpers\StringHelper;
use yii\helpers\Url;

$this->title = Yii::t('web', 'Login').' - ' . Yii::$app->name;
//$this->params['breadcrumbs'][] = Yii::t('web', 'Login');
?>
<main role="main" class="container">
    <div class="site-login">
    	<div class="col-lg-8 col-lg-offset-2 col-xs-10 col-xs-offset-1">
		<?= Alert::widget() ?>
		<?php echo '<img  src="'.Yii::$app->getUrlManager()->getBaseUrl().'/images/logo_insoglas.png" />'.str_replace(['http://', 'https://', 'www.', '.'], ['', '', '', '<span class="colorize-1">.</span>'],'');?>
		<div class="row">
			<div class="col-md-6 col-md-offset-3 col-sm-8 col-sm-offset-2 col-xs-12">
	        <?php $form = ActiveForm::begin(['id' => 'login-form', 'enableAjaxValidation' => true, 'action' => Url::to(['site/login']) ]); ?>

	        <?= $form->field($model, 'email')->label(false)->textInput(array('placeholder'=>Yii::t('main','Type e-mail'),'type'=>'email')) ?>

	        <?= $form->field($model, 'password', ['enableAjaxValidation' => false])->passwordInput()->label(false)->textInput(array('placeholder'=>Yii::t('main','Type password'),'type'=>'password')) ?>
			<?= Html::a(Yii::t('web','If you forgot your password you can').' '.Yii::t('web','reset it'), ['site/request-password-reset']) ?>
			</div>
		</div>
	    <div class="form-group text-center">
	    	<?= Html::submitButton(Yii::t('web', 'Log in'), ['class' => 'btn btn-primary ', 'name' => 'login-button']) ?>
	    </div>
	    <div class="row text-right">
	        <?= Html::a(Yii::t('web', 'If you do not have an account, you can sign up'), ['site/register']); ?>
	    </div>

	   	<?php ActiveForm::end(); ?>
    	</div>
    </div>
</main>
<footer class="footer sticky-footer">
	<div class="col-xs-12 text-center">
		<?= Html::img(Yii::$app->getUrlManager()->getBaseUrl() . '/images/ue.png', ['class' => 'ue-logo', 'alt' => 'ue-logo']) ?>
	</div>
</footer>