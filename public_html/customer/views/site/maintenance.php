<?php

/* @var $this yii\web\View */

$this->title = Yii::t('web', 'Maintenance').' - ' . Yii::$app->name;
?>
<div class="site-maintenance">

    <div class="jumbotron">
		<div class="row alert-warning text-center">
			<h2 class="col-sm-8 col-sm-offset-2 col-xs-12"><?= Yii::t('web', 'Page under construction').'   '?><span class="glyphicon glyphicon-warning-sign"></span></h2>
        	<p class="col-sm-8 col-sm-offset-2 col-xs-12"><?= Yii::t('web', 'We apologize for any inconvenience and should be back up with a newly updated website soon')?></p>
        </div>
    </div>

</div>
