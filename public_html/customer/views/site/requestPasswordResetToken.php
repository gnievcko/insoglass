<?php
use common\components\Captcha;

/* @var $this yii\web\View */
/* @var $form kartik\widgets\ActiveForm */
/* @var $model \frontend\models\PasswordResetRequestForm */

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use common\widgets\Alert;

$this->title = Yii::t ( 'web', 'Reset password' ) . ' - ' . Yii::$app->name;
$this->params ['breadcrumbs'] [] = Yii::t ( 'web', 'Reset password' );
?>
<div class="site-login">
	<div
		class="col-lg-6 col-lg-offset-3 col-sm-8 col-sm-offset-2 col-xs-10 col-xs-offset-1">
		<?= Alert::widget()?>
	    <h1><?= Html::encode(Yii::t('web', 'Please enter your email')) ?></h1>
	    <?php $form = ActiveForm::begin(['id' => 'request-password-reset-form']); ?>
		<div class="col-sm-12">
	      	 <?= $form->field($model, 'identity')->textInput(['placeHolder' => $model->getAttributeLabel('email')])->label(false)?>
		</div>
		<div class="col-sm-12">
            <?= $form->field($model, 'verifyCode', ['enableClientValidation' => false, 'enableAjaxValidation' => false])
                     ->widget(Captcha::className(), ['options' => ['placeHolder' => $model->getAttributeLabel('verifyCode')],
                              'template' =>
                                  '<div class="row captcha-row"><div class="col-lg-5">{image}{refresh}</div><div class="col-lg-7">{input}</div></div>'])
                     ->label(false); ?>
	    </div>
		<div class="form-group">
	    	<?= Html::submitButton(Yii::t('web', 'Send'), ['class' => 'btn btn-primary'])?>
	    </div>

	    <?php ActiveForm::end(); ?>

	    </div>
</div>
</div>
