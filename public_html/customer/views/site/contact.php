<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model frontend\models\ContactForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use common\components\Captcha;

$this->title = Yii::t('web', 'Contact').' - ' . Yii::$app->name;
$this->params['breadcrumbs'][] = Yii::t('web', 'Contact');
?>
<div class="site-contact">
		<h1><?= Yii::t('web', 'Contact') ?></h1>

		<p>
				<?= Yii::t('web', 'If you have business inquiries or other questions, please fill out the following form to contact us. Thank you.') ?>
		</p>

		<div class="row">
				<div class="col-lg-5">
						<?php $form = ActiveForm::begin(['id' => 'contact-form']); ?>

								<?= $form->field($model, 'name') ?>

								<?= $form->field($model, 'email') ?>

								<?= $form->field($model, 'subject') ?>

								<?= $form->field($model, 'body')->textArea(['rows' => 6]) ?>

								<?= $form->field($model, 'verifyCode', ['enableAjaxValidation' => false])->widget(Captcha::className(), [
											'template' => '<div class="row captcha-row"><div class="col-lg-5">{image}{refresh}</div><div class="col-lg-7">{input}</div></div>',
								]) ?>

								<div class="form-group">
										<?= Html::submitButton(Yii::t('web', 'Submit'), ['class' => 'btn btn-warning', 'name' => 'contact-button']) ?>
								</div>

						<?php ActiveForm::end(); ?>
				</div>
		</div>

</div>
