<?php

use common\widgets\Alert;
use kartik\widgets\ActiveForm;
use yii\helpers\Url;
use yii\helpers\Html;
use common\helpers\StringHelper;

$this->title = Yii::t('web', 'Registration') . ' - ' . Yii::$app->name;

?>
<div class="site-register">
    <div class="col-lg-6 col-lg-offset-3 col-sm-10 col-sm-offset-1 col-xs-10 col-xs-offset-1">
        <?= Alert::widget(); ?>
        <img  src="<?= Yii::$app->getUrlManager()->getBaseUrl() . '/images/logo_insoglas.png'; ?>"/>
        <div class="row">
            <div class="col-md-6 col-md-offset-3 col-sm-8 col-sm-offset-2 col-xs-12">
            <?php
                $form = ActiveForm::begin(['id' => 'register-form', 'enableAjaxValidation' => true, 'action' => Url::to(['site/register'])]);
                $attributeLabels = $registerForm->attributeLabels();
                $mb_lcfirst = [StringHelper::class, 'mb_lcfirst'];

//                echo $form->field($registerForm, 'fullName')->textInput([
//                    'placeholder' => Yii::t('web', 'Type in') . ' ' . $mb_lcfirst($attributeLabels['fullName']),
//                ]);
                
                echo $form->field($registerForm, 'firstName')->textInput([
                    'placeholder' => Yii::t('web', 'Type in') . ' ' . $mb_lcfirst($attributeLabels['firstName']),
                ]);
                
                echo $form->field($registerForm, 'lastName')->textInput([
                    'placeholder' => Yii::t('web', 'Type in') . ' ' . $mb_lcfirst($attributeLabels['lastName']),
                ]);
                
                echo $form->field($registerForm, 'email')->textInput([
                    'placeholder' => Yii::t('web', 'Type in') . ' ' . $mb_lcfirst($attributeLabels['email']),
                    'type' => 'email',
                ]);
                echo $form->field($registerForm, 'representative')->checkbox(['class' => 'representative-checkbox']);
                echo $form->field($registerForm, 'companyName')->textInput([
                    'placeholder' => Yii::t('web', 'Type in company name'),
                    'class' => 'company-name-input',
                ]);
                echo $form->field($registerForm, 'VAT_ID')->textInput([
                    'placeholder' => Yii::t('web', 'Type in') . ' ' . $attributeLabels['VAT_ID'],
                    'class' => 'vat-id-input',
                ]);
                echo $form->field($registerForm, 'password')->passwordInput([
                    'placeholder' => Yii::t('web', 'Type in') . ' ' . $mb_lcfirst($attributeLabels['password']),
                ]);
                echo $form->field($registerForm, 'confirmPassword')->passwordInput([
                    'placeholder' => Yii::t('web', 'Confirm password'),
                ]);

                $this->registerJs('(function () {
                    "use strict";
                    Array.prototype.allValuesSame = function () {
                        for (var i = 1; i < this.length; i++) {
                            if (this[i] !== this[0]) {
                                return false;
                            }
                        }
                        return true;
                    }

                    var $allPasswordFields = $("input[type=password]");
                    var $submitButton = $("button[type=submit]");
                    $allPasswordFields.on("change keyup", function () {
                        var vals = $allPasswordFields.map(function (idx, val) { return $(val).val(); }).get();
                        if ($.inArray("", vals) === -1 && !vals.allValuesSame()) {
                            $allPasswordFields.addClass("passwords-do-not-match");
                            $submitButton.prop("disabled", true);
                        }
                        else {
                            $allPasswordFields.removeClass("passwords-do-not-match");
                            $submitButton.prop("disabled", false);
                        }
                    }).trigger("change");

                    var $companyNameInput = $(".company-name-input");
                    var $VAT_ID_input = $(".vat-id-input");
                    var $inputs = $companyNameInput.add($VAT_ID_input);
                    $(".representative-checkbox").on("change", function () {
                        var checked = $(this).prop("checked");
                        if (!checked) {
                            $inputs.val("");
                        }
                        $inputs.prop("disabled", !checked);
                    }).trigger("change");
                })();');
            ?>
            </div>
        </div>
        <div class="form-group text-center">
            <?= Html::submitButton(Yii::t('web', 'Register'), ['class' => 'btn btn-primary ', 'name' => 'register-button']) ?>
        </div>
        <div class="row text-right">
            <?= Html::a(Yii::t('web', 'If you have an account, you can log in'), ['site/login']); ?>
        </div>
        <?php ActiveForm::end(); ?>
    </div>
</div>
