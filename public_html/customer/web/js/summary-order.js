function initializeOrderSummary(url, data, tokenName, token) {
    $("#cancel-order").on("click", function () {
        var form = $('<form action="' + url + '" method="post">' +
                '<input type="hidden" name="' + tokenName + '" value="' + token + '" />' +
                '<input type="text" name="data" value="' + data + '" />' +
                '</form>');

        $('body').append(form);
        form.submit();
    });
    $("#add-order").on("click", function () {
        var form = $('<form action="' + url + '" method="post">' +
                '<input type="hidden" name="' + tokenName + '" value="' + token + '" />' +
                '<input type="text" name="data" value="' + data + '" />' +
                '<input type="text" name="save" value="1" />' +
                '</form>');

        $('body').append(form);
        form.submit();
    });
}


