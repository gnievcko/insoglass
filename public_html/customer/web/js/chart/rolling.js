var oneLineWidth = 20;
var displayLine = 4;
Chart.pluginService.register({
	afterDraw : function(chart) {
		var $obj = $(chart.chart.canvas).parents('.rollingChart');
		var $roll = $('.roll', $obj);
		if($roll.hasClass('maximize') || $roll.length == 0) {
			var value = parseInt($('.title', $obj).height());
			value += parseInt($('.title', $obj).css('margin-bottom'),10);
			value += parseInt($('canvas', $obj).height(),10);
			value += displayLine*oneLineWidth;
			if ($('.chart', $obj).height() > parseInt(value, 10)) {
				if($roll.length == 0) {
					$obj.append('<div class="roll maximize"><span class="base-icon alert-icon arrow-icon"></span></div>')
				}
			}
			$obj.children('.chart').css('height', value);
			$obj.children('.chart').css('overflow', 'hidden');
		}
	},
})
$('.rollingChart').on('click','.roll',function(){
	var $obj = $(this).parents('.rollingChart');
	var value = parseInt($('.title', $obj).height());
	value += parseInt($('.title', $obj).css('margin-bottom'),10);
	value += parseInt($('canvas', $obj).height(),10);
	value += displayLine*oneLineWidth;
	if($(this).hasClass('maximize')){
		$(this).removeClass('maximize');
		$(this).addClass('minimize');
		$obj.children('.chart').css('overflow', 'visible');
		$obj.children('.chart').css('height', 'auto');
	}
	else{
		$(this).removeClass('minimize');
		$(this).addClass('maximize');
		$obj.children('.chart').css('overflow', 'hidden');
		$obj.children('.chart').css('height', value);

	}
});
