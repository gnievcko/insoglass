function fillProvince(zipCodeTextAreaId, provinceDropdownId, url) {	
	$("#" + zipCodeTextAreaId).change(function(){
		$.get(url , { zipCode: this.value}, function(data) {
			$("#" + provinceDropdownId).val(data);
		});
	})	
}
