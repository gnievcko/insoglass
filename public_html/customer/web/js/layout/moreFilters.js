function moreFilters(initVisible) {

	var icon = $(".more-filters-button > .glyphicon");
	var textDown = $(".more-filters-button > .text-down");
	var textUp = $(".more-filters-button > .text-up");
	var moreFiltersRows = $(".more-filters");
	
	var showButtons = function() {
		icon.removeClass("glyphicon-menu-down");
		icon.addClass("glyphicon-menu-up");		
		textDown.hide();
		textUp.show();
	}
	
	var hideButtons = function() {
		icon.removeClass("glyphicon-menu-up");
		icon.addClass("glyphicon-menu-down");
		textDown.show();
		textUp.hide();
	}
	
	$(".more-filters-button").click(function() {		
		if(icon.hasClass("glyphicon-menu-down")) {
			showButtons();
			moreFiltersRows.slideDown({ "queue": false, "duration": 100, "easing": "linear" });
		}
		else {
			hideButtons();
			moreFiltersRows.slideUp({ "queue": false, "duration": 100, "easing": "linear" });
		}
	});
	
	if(!initVisible) {
		moreFiltersRows.hide();
	}
}
