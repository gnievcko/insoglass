$(document).ready(function($) {
    $(document).on("click", ".clickable-table-row", function() {
        window.location = $(this).data("href");
    });
});