function initAttributeTooltip() {
	$("body").on("mouseenter", ".extended-tooltip", function() {
		var container = $(this).find('.hint-container');
		var hint = $(this).find('.extended-hint');
		container.show();

		// Move hint container to left border of the screen
		var left = parseInt($(this).offset().left);
        if(parseInt(container.css('left')) >= 0) {
            container.css('left', "-="+(left) );
        }

        //Adjust hint X position
        var hintWidth = parseInt(hint.css('width'));

        if((hintWidth + left) > ($(window).width() - 30)) {
        	hint.css('right','30px');
		}
        else if((left - hintWidth) < 30) {
        	hint.css('left', '30px');
		}
        else {
			hint.css('left', left);
		}

        // Move hint container to proper height
        var height = parseInt(hint.css('height'));
        if(parseInt(container.css('top')) >= 0) {
            container.css('top', "-="+(height+50) );
        }

    });
	
	$("body").on("mouseleave", ".extended-tooltip", function() {
		var container = $(this).find(".hint-container");
		if(container) {
            container.hide();
		}
	});
}
