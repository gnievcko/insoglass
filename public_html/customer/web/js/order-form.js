var OrderForm = (function (OrderForm) {

    OrderForm.activateEmployeesChooser = function ($employeeChooser, $employeesList) {
        var initSelectedEmployee = function ($selectedEmployee) {
            $selectedEmployee.find('.remove-employee-btn:first').on('click', function () {
                $selectedEmployee.remove();
            });
        };

        var $selectedEmployees = $employeesList.find('.selected-employee');

        $selectedEmployees.each(function (idx, selectedEmployee) {
            initSelectedEmployee($(selectedEmployee));
        });

        $employeeChooser.on('change', function () {
            var selectedEmployeeId = $employeeChooser.val();
            if (selectedEmployeeId != undefined) {
                $.get($employeeChooser.data('form-url'), {id: selectedEmployeeId}, function (form) {
                    var $form = $(form);
                    initSelectedEmployee($form);
                    $employeesList.append($form);
                });
            }
        });
    }

    OrderForm.initRecalculate = function (form, url, id, idContainer, anotherForm = null) {

        var $container = $('#' + idContainer);
        
        var setFocus = function() {
        	 if (localStorage.getItem("lastId") != null && localStorage.getItem("lastId") != ""){
     	        setTimeout(function(){
                     var elem = document.getElementById(localStorage.getItem("lastId"));
                     elem.focus();
                 },100);
             }   
        }

        var recalculateValues = function () {
            $.post(url, $('[name*="' + form + '"]').serialize() + "&id=" + id + "&" + $('[name*="' + anotherForm + '"]').serialize(), function (data) {
                $container.html(data);
                setFocus();
            });
        };

        var addRow = function () {
            $.post(url, $('[name*="' + form + '"]').serialize() + "&id=" + id + "&" + $('[name*="' + anotherForm + '"]').serialize() + "&addRow=" + true, function (data) {
                $container.html(data);
                setFocus();
            });
        };

        var copyRow = function (index) {
            $.post(url, $('[name*="' + form + '"]').serialize() + "&id=" + id + "&" + $('[name*="' + anotherForm + '"]').serialize() + "&copyRow=" + true + "&rowId=" + index, function (data) {
                $container.html(data);
                setFocus();
            });
        };

        var clear = function (e) {
            var itemsContainerSelector = $(this).data('items-container-selector');
            $(itemsContainerSelector).html('');
        };

        var remove = function (e) {
            $(this).closest('.attributes').remove();
            $(this).closest('.item').remove();
        }

      //  $container.on('change', '[name*="' + form + '"]', recalculateValues);
        
        
        $container.on('change', '[name$="][count]"]', recalculateValues);
        $container.on('change', '[name$="][price]"]', recalculateValues);
        
        $container.on('click', '.add-item-button-custom', addRow);
        $container.on('click', '.copy-item-btn-custom', function(e){
        	var rowid = $(e.target).attr('data-index');
        	copyRow(rowid);
        	return false;
        });
        $container.on('click', '.remove-all-items-btn-custom', clear);
        $container.on('click', '.remove-item-btn-custom', remove);
        $container.on('click', '.remove-all-items-btn-custom', recalculateValues);
        $container.on('click', '.remove-item-btn-custom', recalculateValues);
        

        $container.on("click", ".add-item-link", function(e) {
            addRow();
            //TODO::needs to be commented for !Yii::$app->params['isOrderProductDimensionsVisible']
            localStorage.setItem("lastId", $(this).attr("id"));
            return false;
        });

        
        
        
        
    }

    // TODO: Formularze powinny byc podawane w parametrze, podobnie jak nazwy divów
    OrderForm.initRecalculateSummary = function (url, idContainer) {
        var $container = $('#' + idContainer);

        var recalculateSummary = function () {
            $.post(url, $('[name*="OrderProductForm"], [name*="OrderCostForm"]').serialize(), function (data) {
                $container.html(data);
            });
        };

        $('#products').on('change', '[name*="OrderProductForm"]', recalculateSummary);
        $('#costs').on('change', '[name*="OrderCostForm"]', recalculateSummary);
        $('#products').on('click', '.remove-all-items-btn-custom', recalculateSummary);
        $('#costs').on('click', '.remove-all-items-btn-custom', recalculateSummary);
        $('#products').on('click', '.remove-item-btn-custom', recalculateSummary);
        $('#costs').on('click', '.remove-item-btn-custom', recalculateSummary);
    }

    OrderForm.initRecalculateCustomData = function (urlSummary, urlNumeration, idContainer) {
        var $container = $('.' + idContainer);

        var recalculateCustomData = function () {
            $.post(urlSummary, $('[name*="tables"]').serialize(), function (data) {
                var table = JSON.parse(data);
                for (var key in table) {
                    $("textarea[name='" + key + "']").val(table[key]);
                    $("span[name='" + key + "']").html(table[key]);
                }
            });
        };

        var recalculateNumeration = function () {
            $.post(urlNumeration, $('[name*="tables"]').serialize(), function (data) {
                var table = JSON.parse(data);
                
                for (var key in table) {
                    $("textarea[name='" + key + "']").val(table[key]);
                }
            });
        };

        $container.on('change', '[name*="tables"]', recalculateCustomData);
        $container.on('change', '[name*="tables"]', recalculateNumeration);
        $container.on('click', '.add-row-button', recalculateCustomData);
        $container.on('click', '.add-row-button', recalculateNumeration);
        $(document).on('click', '.remove-row-btn', recalculateCustomData);
        $(document).on('click', '.remove-row-btn', recalculateNumeration);
        $(document).on('click', '.copy-row-btn', recalculateCustomData);
        $(document).on('click', '.copy-row-btn', recalculateNumeration);

        recalculateCustomData();
    }

    OrderForm.save = function (id, fieldName, successText, errorText) {
        $form = $('#' + id);
        var save = 1;
        $form.find('[name=save]').val(1);
        $orderId = $form.find('[name="' + fieldName + '"]');
        $form.on('beforeSubmit', function (event) {
            if (save == 1) {
                $('#save-order').prop("disabled", true);
                $.post($form.data('form-url'), $form.serialize() + '&click=1', function (response) {
                    if (response.id != undefined) {
                        $orderId.val(response.id);
                        $('#info').html(successText).fadeIn('slow');
                        setTimeout(function () {
                            $('#info').fadeOut('slow');
                            $('#save-order').prop("disabled", false);
                        }, 5000);
                    } else {
                        $('#info').html(errorText).fadeIn('slow');
                        setTimeout(function () {
                            $('#info').fadeOut('slow');
                            $('#save-order').prop("disabled", false);
                        }, 5000);
                    }
                }).fail(function () {
                    $('#info').html(errorText).fadeIn('slow');
                    setTimeout(function () {
                        $('#info').fadeOut('slow');
                        $('#save-order').prop("disabled", false);
                    }, 5000);
                });
                return false;
            }
        }).on('submit', function (e) {
            if (save == 1) {
                e.preventDefault();
            }
        });
        $('#add-order').on('click', function () {
            save = 0;
            $form.find('[name=save]').val(0);
            $('#save-order').trigger('click');
        });
        $('#update-order').on('click', function () {
            save = 0;
            $form.find('[name=save]').val(0);
            $('#save-order').trigger('click');
        });
    }

    return OrderForm;

})(OrderForm || {});


$("#ordercreateform-clientid").on("select2:select", function () {
    $companyId = $("#ordercreateform-clientid").val();
    $address = $(location).attr('href');
    $fullRequest = 'get-user-representatives';

    if ($address.lastIndexOf("/edit") > 0) {
        $fullRequest = $address.substring(0, $address.lastIndexOf("/edit")) + "/get-user-representatives";
    } else if ($address.lastIndexOf("/copy") > 0) {
        $fullRequest = $address.substring(0, $address.lastIndexOf("/copy")) + "/get-user-representatives";
    }

    $.get($fullRequest, {companyId: $companyId}, function (data) {
        if (Object.prototype.toString.call(data) === '[object Array]') {
            $contactSelect = $('#ordercreateform-contactpeopleids');
            $contactSelect.empty();
            var ids = [];
            for (i = 0; i < data.length; ++i) {
                ids[i] = data[i].id;
                $contactSelect.append('<option value="' + data[i].id + '">' + data[i].first_name + ' ' + data[i].last_name + '</option>');
            }
            $contactSelect.val(ids);
            $contactSelect.trigger('change');
        }
    }).done(function () {
    });
});


