function initializeTables(sectionClass, settings) {
    var rowUrl = settings.rowUrl;
    var edit = settings.edit;
    var uniq = settings.uniq || null;
    var count = settings.count;
    var tableViewsDir = settings.tableViewsDir ? settings.tableViewsDir : null;
    var startIdx = settings.startIdx ? settings.startIdx : null;

   /* function fixWidthHelper(e, ui) {
        ui.children().each(function () {
            $(this).width($(this).width());
        });
        return ui;
    }

    $("."+sectionClass+' .table tbody').sortable({
        helper: fixWidthHelper
    }).disableSelection();*/



    var initializeTable = function ($section) {

        var $tableBody = $section.find('tbody:first');
        var $ajaxLoader = $section.find('.ajax-loader:first');
        var $form = $section.closest('form');

        var showLoader = function () {
            //$tableBody.hide();
            $ajaxLoader.show();
        }

        var hideLoader = function () {
            //$tableBody.show();
            $ajaxLoader.hide();
        }

        var reinitializeValidation = function () {
            activateValidationForProduct($tableBody.find('.document-table-row'));
        }

        var addButtons = $section.find('.add-row-button');
        addButtons.on('click', function () {
        	var newRowCount = $(this).closest('.document-table').find('[name="input-table-add-row-number"]').val();
        	var newRowCount2 = $(this).next().val();
            if(!newRowCount) {
            	if(newRowCount2) {
            		newRowCount = newRowCount2;
            	} 
            	else {
            		newRowCount = 1;
            	}
            }
        	
            for(i = 0; i < newRowCount; ++i) {
	        	showLoader();
	            idx = $tableBody.find('.document-table-row').size();
	            if(startIdx) {
	            	idx = startIdx;
	            	startIdx++;
	            }
	            
	            $.get(rowUrl, {edit: edit, count: count, tableViewsDir: tableViewsDir, uniq: uniq, idx: idx}, function (product) {
	                var $lastProduct = $tableBody.find('.document-table-row:last');
	                $product = $(product);
	
	                if ($lastProduct.size() === 0) {
	                    $tableBody.append($product);
	                } else {
	                    $lastProduct.after($product);
	                }
	
	                activateValidationForProduct($product);
	
	                initializeCopyBtn($product.find('.copy-row-btn:first'));
	                initializeRemoveBtn($product.find('.remove-row-btn:first'));
	
	            }).done(hideLoader);
            }
        });

        var activateValidationForProduct = function ($product) {
            $product.find('input, select, textarea').each(function (idx, formControl) {
                var $formControl = $(formControl);
                var controlId = $formControl.attr('id');

                $form.yiiActiveForm('add', {
                    id: controlId,
                    name: $formControl.attr('name'),
                    container: '.field-' + controlId,
                    input: '#' + controlId,
                    enableClientValidation: false,
                    enableAjaxValidation: true
                });
            });
        }
        
        var initializeCopyButtons = function() {
        	initializeCopyBtn($tableBody.find('.copy-row-btn'));
        };
        var initializeCopyBtn = function($copyBtn) {
        	$copyBtn.on('click', function() {
	        	showLoader();
	            idx = $tableBody.find('.document-table-row').size();
	            if(startIdx) {
	            	idx = startIdx;
	            	startIdx++;
	            }
	            
	            var initData = [];
	            var $copiedProduct = $(this).closest('.document-table-row');    
	            var isCustomTables = $copiedProduct.find('[name^="tables"]').size();
	            
            	$copiedProduct.find('.xSmallTextarea').each(function(index) {
            		if(isCustomTables > 0 && index == 0) {
            			return;
            		}
            		
            		initData[index] = $(this).val();
            	});
	            
	            $.get(rowUrl, {edit: edit, count: count, tableViewsDir: tableViewsDir, uniq: uniq, idx: idx, initData: initData}, function (product) {
	                var $lastProduct = $tableBody.find('.document-table-row:last');
	                $product = $(product);
	                $copiedProduct.after($product);
	
	                activateValidationForProduct($product);
	
	                initializeCopyBtn($product.find('.copy-row-btn:first'));
	                initializeRemoveBtn($product.find('.remove-row-btn:first'));
	            }).done(hideLoader);
        	});
        }
        initializeCopyButtons();

        var initializeRemoveButtons = function () {
            initializeRemoveBtn($tableBody.find('.remove-row-btn'));
        };
        var initializeRemoveBtn = function ($removeBtn) {
            $removeBtn.on('click', function () {
                $(this).closest('.document-table-row').remove();
            });
        };
        initializeRemoveButtons();

        var initializeSelects = function () {
            initializeSelect($tableBody.find('.unit-select'));
        };
        
        $('.' + sectionClass).on('click', '.clear-data-column-btn', function() {
        	var index = $(this).parent().index();
        	if($(this).data("name") == "LightMeasurementForm") {
        		if(index == 0) {
        			$('[name^="LightMeasurementForm"][name$="[name]"]').val('');
        		}
        		else if(index == 1) {
        			$('[name^="LightMeasurementForm"][name$="[value]"]').val('');
        		}
        	}
        	else if($(this).data("name") == "RepairReplacementsProtocolForm") {
        		if(index == 0) {
        			$('[name^="RepairReplacementsProtocolForm"][name$="[name]"]').val('');
        		}
        		else if(index == 1) {
        			$('[name^="RepairReplacementsProtocolForm"][name$="[count]"]').val('');
        		}
        	}
        	else if($(this).data("name") == "RepairTestsProtocolForm") {
        		if(index == 0) {
        			$('[name^="RepairTestsProtocolForm"][name$="[device]"]').val('');
        		}
        		else if(index == 1) {
        			$('[name^="RepairTestsProtocolForm"][name$="[result]"]').val('');
        		}
        	}
        	else {
        		$(this).closest('.document-table').find('[name*="[rows]"][name$="[' + index + ']"]').val('');
        	}    		
    	});
    }

    $('.' + sectionClass).each(function (idx, section) {
        initializeTable($(section));
    })
}
