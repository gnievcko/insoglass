$('[name=addDate]').on('change', function () {
    var addDay = $('[name=addDate]').val();
    var d = $("div[id $= '-dateofissue-disp-kvdate']").kvDatepicker("getDate") || new Date();
    if(addDay != '') {
        d.setDate(d.getDate() + parseInt(addDay));

        var dateFormat = ("0" + (d.getDate())).slice(-2) + "." + ("0" + (d.getMonth() + 1)).slice(-2) + "." + d.getFullYear();
        var dateFormat2 = d.getFullYear() + "-" + ("0" + (d.getMonth() + 1)).slice(-2) + "-" + ("0" + (d.getDate())).slice(-2);

        $("#invoiceId-disp-kvdate").kvDatepicker("update", dateFormat);
        $("#invoiceId").val(dateFormat2);
    }
});

$('[name=selectMethod]').on('change', function () {
    $('[name="InvoicePayment[paymentMethod]"]').val($('[name=selectMethod]').val());
});