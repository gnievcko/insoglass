var initializeOfferDetailsProductsTables = function(settings) {

    var OFFERED_PRODUCT_SELECTOR = '.offer-details-product';
	
    var $form = $('.offer-details-products:first').closest('form');
    var productsIdSequence = $form.find(OFFERED_PRODUCT_SELECTOR).size();

    var initializeTable = function($table) {
        var $tableBody = $table.find('tbody:first');
        
        $table.find('.add-offer-details-product-button').on('click', function() {
            fetchNewTableRow();
        });
        
        var initProductRow = function($productRow) {
            $productRow.find('.remove-offer-details-product-btn').on('click', function() {
                $productRow.remove();
                recalculateSummary();
            });
            $productRow.find('.offer-details-product-count:first, .offer-details-product-price:first, .offer-details-product-currency:first')
            	.on('change', function() {
                recalculateProductTotalPrice($productRow);
            	recalculateSummary();
            });
        };
        
        $table.find(OFFERED_PRODUCT_SELECTOR).each(function(_, productRow) {
            initProductRow($(productRow));
        });
        
        var fetchNewTableRow = function() {
            $.get(settings.productRowUrl, { prefix: productsIdSequence++ }, function(productRow) {
                var $productRow = $(productRow);
                $tableBody.append($productRow);

                activateValidationForProduct($productRow);
                initProductRow($productRow);
            });
        };

        var activateValidationForProduct = function($productRow) {
            $productRow.find('input').each(function(idx, formControl) {
                var $formControl = $(formControl);
                var controlId = $formControl.attr('id');

                $form.yiiActiveForm('add', {
                    id: controlId,
                    name: $formControl.attr('name'),
                    container: '.field-' + controlId,
                    input: '#' + controlId,
                    enableClientValidation: false,
                    enableAjaxValidation: true
                });
            });
        };

        var recalculateProductTotalPrice = function($productRow) {
            $.get(settings.totalPriceUrl, {
                priceUnit: $productRow.find('.offer-details-product-price:first').val(), 
                count: $productRow.find('.offer-details-product-count:first').val()
            }, 
            function(result) {
            	$productRow.find('.offer-details-product-total-price:first').text(result.totalPrice);
            });
        };

        var $summary = $table.find('.summary-cost:first');
        var recalculateSummary = function() {
            $.post(settings.summaryUrl, $tableBody.find('input, select').add(settings.currencySelector).serialize(), 
            function(summary) {
            	$summary.html(summary.amount + ' ' + summary.currency);
            });
        };
    };

    $('.offer-details-products').each(function(_, table) {
        initializeTable($(table));
    });
};