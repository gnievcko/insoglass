function initializeInvoiceProductsTables(sectionClass, settings) {
    var calculationsUrl = settings.calculationsUrl;
    var verbalCalculationsUrl = settings.verbalCalculationsUrl;
    var productRowUrl = settings.productRowUrl;
    var edit = settings.edit;
    var currencySymbol = settings.initialCurrencySymbol;

    var initializeProductsSection = function ($section) {

        var $tableBody = $section.find('tbody:first');
        var $ajaxLoader = $section.find('.ajax-loader:first');
        var $form = $section.closest('form');
        var $currencySelect = $section.find('.currency-select:first');

        $currencySelect.on('change', function() {
            var selectedCurrencySymbol = $currencySelect.find('option:selected').text();
            currencySymbol = selectedCurrencySymbol;
            recalculateValues();
        });

        var showLoader = function() {
            $ajaxLoader.show();
        };

        var hideLoader = function() {
            $ajaxLoader.hide();
        };

        var calculateCorrectivePriceDifference = function() {
        	if($("#total-gross-value-precorrected").html() && $("#total-gross-value").html()) {
        		var oldPriceArray = $("#total-gross-value-precorrected").html().trim().split(" ");
        		var newPriceArray = $("#total-gross-value").html().trim().split(" ");
        		var currency = newPriceArray[newPriceArray.length - 1];
        		var oldPrice = oldPriceArray.slice(0, oldPriceArray.length - 1).join("");
        		var newPrice = newPriceArray.slice(0, newPriceArray.length - 1).join("");
        		var diff = newPrice.replace(",", ".") - oldPrice.replace(",", ".");
				var res = diff.toFixed(2).replace(".", ",") + " " + currency;
				$("#correctiveinvoicepayment-pricedifference").val(res);
				$("#correctiveinvoicepayment-pricedifference").html(res);
        	}
        };

        var recalculateValues = function() {
            var dataToSend = $section.find('input, textarea, select').serialize();
            showLoader();

            $.post(calculationsUrl, dataToSend, function(response) {
                var $response = $(response);
                $tableBody.html($response);
                initializeRemoveButtons();
                initializeCopyButtons();
                initializeInputs();
                reinitializeValidation();
            }).done(hideLoader).then(function() {            
	            $.post(verbalCalculationsUrl, dataToSend, function(response) {
	                $('#invoiceproducts-amountverbally').val(response);
	            });
	            
	            calculateCorrectivePriceDifference();
            });
        };
        calculateCorrectivePriceDifference();

        var reinitializeValidation = function() {
            activateValidationForProduct($tableBody.find('.invoice-product'));
        };

        var addProductButtons = $section.find('.add-invoice-product-button');
        addProductButtons.on('click', function() {
            showLoader();

            $.get(productRowUrl, { edit: edit, idx: $tableBody.find('.invoice-product').size(), currencySymbol: currencySymbol }, function(product) {
                var $lastInvoiceProduct = $tableBody.find('.invoice-product:last');
                $product = $(product);

                if($lastInvoiceProduct.size() === 0) {
                    $tableBody.prepend($product);
                }
                else {
                    $lastInvoiceProduct.after($product);
                }

                activateValidationForProduct($product);

                initializeRemoveBtn($product.find('.remove-invoice-product-btn:first'));
                initializeCopyBtn($product.find('.copy-invoice-product-btn:first'));
                initializeInput($product.find('.recalculates'));
            }).done(hideLoader);
        });
        
        $section.find('.remove-invoice-products-button').on('click', function() {
            $tableBody.find('.invoice-product').remove();
            recalculateValues();
        });

        var activateValidationForProduct = function($product) {
            $product.find('input, select, textarea').each(function(idx, formControl) {
                var $formControl = $(formControl);
                var controlId = $formControl.attr('id');

                $form.yiiActiveForm('add', {
                    id: controlId,
                    name: $formControl.attr('name'),
                    container: '.field-' + controlId,
                    input: '#' + controlId,
                    enableClientValidation: false,
                    enableAjaxValidation: true
                });
            });
        };
        
        var initializeCopyButtons = function() {
        	initializeCopyBtn($tableBody.find('.copy-invoice-product-btn'));
        };
        var initializeCopyBtn = function($copyBtn) {
        	$copyBtn.on('click', function() {
        		showLoader();
            	var $invoiceProduct = $(this).closest('.invoice-product');
            	
            	var initData = {};
            	$invoiceProduct.find('[name^="InvoiceProductForm"]').each(function(index) {
            		initData[$(this).attr('id')] = $(this).val();
            	});
            	            	
            	$.get(productRowUrl, { edit: edit, idx: $tableBody.find('.invoice-product').size(), currencySymbol: currencySymbol, initData : initData }, function(product) {
                    $product = $(product);
                    $invoiceProduct.after($product);                   
                    
                    recalculateValues();
                    
                    activateValidationForProduct($product);
                    initializeCopyBtn($product.find('.copy-invoice-product-btn:first'));
                    initializeRemoveBtn($product.find('.remove-invoice-product-btn:first'));
                    initializeInput($product.find('.recalculates'));                                 
                }).done(hideLoader);
        	});
        }
        initializeCopyButtons();

        var initializeRemoveButtons = function() {
            initializeRemoveBtn($tableBody.find('.remove-invoice-product-btn'));
        };
        var initializeRemoveBtn = function($removeBtn) {
            $removeBtn.on('click', function() {
                $(this).closest('.invoice-product').remove();
                recalculateValues();
            });
        };
        initializeRemoveButtons();

        var initializeInputs = function() {
            initializeInput($tableBody.find('.recalculates'));
        };
        var initializeInput = function($input) {
            $input.on('change', recalculateValues);
        };
        initializeInputs();
    };

    $('.' + sectionClass).each(function(idx, section) {
        initializeProductsSection($(section));
    });
}

