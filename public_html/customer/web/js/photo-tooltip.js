function initPhotoTooltip(url) {
	$("body").on("mouseenter", ".photo-medium", function() {
		var tooltip = $(this).next(".photo-tooltip");
		
		if(tooltip) {
			if(!tooltip.html()) {
				$.get(url, { id: tooltip.data("id"), type: tooltip.data("type") }, function(data) {
					tooltip.html(data);
				}).done(function() {
					tooltip.show();
				});
			}
			else {
				tooltip.show();
			}
		}
	});
	
	$("body").on("mouseleave", ".photo-medium", function() {
		var tooltip = $(this).next(".photo-tooltip");
		if(tooltip) {
			tooltip.hide();
		}
	});
}
