var initProductFormDynamicFeatures = function(url) {
	$("#productform-warehousesupplierid").on("change", function(e) {
		var supplierId = $("#productform-warehousesupplierid").val();
		if(!supplierId) {
			$("#manufacturer-info").html("");
		}
		else {
			$.ajax({
				method: "GET",
				url: url + "/get-manufacturer-details",
				data: { manufacturerId: supplierId }
			}).done(function(data) {
				if(data) {
					$("#manufacturer-info").html(data);
				}
				else {
					$("#manufacturer-info").html("");
				}
			});	
		}
	}).trigger("change");
}
