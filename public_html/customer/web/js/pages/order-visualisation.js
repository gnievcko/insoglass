var path, shapes, colour, svg, params, storageUrl;
var idProduct, typeProduct;

$(document).on('change', '.shape-id', function () {
    idProduct = ($(this).attr('id')).split("-")[1];
    typeProduct = ($(this).attr('id')).split("-")[0];

    var shapeId = $('#' + typeProduct + '-' + idProduct + '-shapeid').find(":selected").val();

    $('#' + typeProduct + '-' + idProduct + '-generate').prop('disabled', shapeId == '' ? true : false);
});

function generateVisualizations(generateVisualizationsButton) {
    idProduct = (generateVisualizationsButton.attr('id')).split("-")[1];
    typeProduct = (generateVisualizationsButton.attr('id')).split("-")[0];

    var formName = typeProduct == 'orderproductform' ? 'OrderProductForm' : 'OrderCostForm';

    var vis = generateVisualizationsButton.parents('.visualisation');
    var documentForm = vis.find('.document-form');
    var svgContainer = vis.find('.svg-container');

    var shapeId = $('#' + typeProduct + '-' + idProduct + '-shapeid').find(":selected").val();


    var height = $('#' + typeProduct + '-' + idProduct + '-height').val();
    var width = $('#' + typeProduct + '-' + idProduct + '-width').val();

    var ralColour = $('#' + typeProduct + '-' + idProduct + '-ralcoloursymbol').val();

    height = (height == '' || parseInt(height) < 1) ? '1' : height;
    width = (width == '' || parseInt(width) < 1) ? '1' : width;

    if (shapeId != '') {

        $.get(path, {shapeId: shapeId, ralColour: ralColour}, function () {
        }).done(function (data) {

            idProduct = (generateVisualizationsButton.attr('id')).split("-")[1];
            typeProduct = (generateVisualizationsButton.attr('id')).split("-")[0];

            var data = JSON.parse(data);
            var shape = data['shape'];
            colour = data['colour'] != '' ? data['colour'] : "#00ffff";

            $('#' + typeProduct + '-' + idProduct + '-svg').empty();
            svg = d3.select('#' + typeProduct + '-' + idProduct + '-svg');
            generateShape(shape, height, width);

            if (documentForm.length > 0) {
                var src = documentForm.find("input[name='" + formName + "[" + idProduct + "][files][0][document]']").val();
                $('#' + typeProduct + '-' + idProduct + '-urlphoto').val(src);
                $('#' + typeProduct + '-' + idProduct + '-heightUrl').val('0');
                $('#' + typeProduct + '-' + idProduct + '-widthphoto').val('0');
                $('#' + typeProduct + '-' + idProduct + '-leftphoto').val('0');
                $('#' + typeProduct + '-' + idProduct + '-topphoto').val('0');

            }

            generateImg(typeProduct, idProduct, svgContainer, documentForm);

        });
    }
}

$(document).on('click', '.generateVisualizations', function () {
    generateVisualizations($(this));

});

function init(url, allShapes, storagePath) {
    path = url;
    shapes = allShapes.split('-');
    storageUrl = storagePath;

    regenerateVisualizations();
}

$(document).on('click', '.regenerateVisualizations', function () {
    regenerateVisualizations();
});

function regenerateVisualizations() {
    $('.items-container').each(function () {
        $(this).find('.item').each(function () {
            var shape = $(this).find('.shape-id');
            var generateVisualizationsButton = $(this).find('.generateVisualizations');

            shape.change();
            generateVisualizations(generateVisualizationsButton);
        });
    });
}

$(document).on('click', '.remove-img', function () {
    var visualisation = $(this).parents('.visualisation');
    var element = visualisation.find('.div-img-visualisation');

    $('#' + typeProduct + '-' + idProduct + '-urlphoto').val('');
    $('#' + typeProduct + '-' + idProduct + '-heightUrl').val('0');
    $('#' + typeProduct + '-' + idProduct + '-widthphoto').val('0');
    $('#' + typeProduct + '-' + idProduct + '-leftphoto').val('0');
    $('#' + typeProduct + '-' + idProduct + '-topphoto').val('0');

    element.remove();
});

$(document).on('click', '.plus-img, .minus-img', function () {
    var bigger = $(this).hasClass('plus-img');
    var visualisation = $(this).parents('.visualisation');
    var element = visualisation.find('.img-visualisation');
    var svgContainer = visualisation.find('.svg-container');

    var heightUrl = $('#' + typeProduct + '-' + idProduct + '-heightphoto');
    var widthUrl = $('#' + typeProduct + '-' + idProduct + '-widthphoto');

    if (element.length > 0) {
        var height, width;
        if (bigger) {
            height = element.height() * 1.1;
            width = element.width() * 1.1;

            element.css({height: height, width: element.width() * 1.1});
        } else {
            height = element.height() * 0.9;
            width = element.width() * 0.9;

            element.css({height: element.height() * 0.9, width: element.width() * 0.9});
        }
        heightUrl.val(height);
        widthUrl.val(width);
        activateMove(svgContainer, element);
    }

});

function generateImg(typeProduct, idProduct, svgContainer, documentForm) {

    var photoUrl = $('#' + typeProduct + '-' + idProduct + '-urlphoto');
    var heightUrl = $('#' + typeProduct + '-' + idProduct + '-heightUrl');
    var widthUrl = $('#' + typeProduct + '-' + idProduct + '-widthphoto');
    var leftUrl = $('#' + typeProduct + '-' + idProduct + '-leftphoto');
    var topUrl = $('#' + typeProduct + '-' + idProduct + '-topphoto');

    svgContainer.find('.div-img-visualisation').remove();

    if (photoUrl.val() != '') {
        var div = $('<div />', {
            'class': 'div-img-visualisation'
        }).appendTo(svgContainer);
        var img = $('<img />', {
            src: storageUrl + '?f=' + photoUrl.val(),
            'class': 'img-visualisation'
        }).appendTo(div);

        img.on('load', function () {

            if (widthUrl.val() != '0') {
                img.width(widthUrl.val());
            } else {
                img.width(img.width() > img.height() ? '60' : (img.width() * 60) / img.height());
            }

            if (heightUrl.val() != '0') {
                img.height(heightUrl.val());
            } else {
                img.height(img.height() > img.width() ? '60' : (img.height() * 60) / img.width());
            }

            img.css({top: topUrl.val() + "px", left: leftUrl.val() + "px"});

            removeTmpImg(documentForm);
            activateMove(svgContainer, img);
        });
    }

}

function removeTmpImg(documentForm) {
    documentForm.find('.document-remove-btn').click();
}

function activateMove(svgContainer, img) {
    var posX = svgContainer.offset().left;
    var posY = svgContainer.offset().top;
    var width = svgContainer.width();
    var height = svgContainer.height();
    var topUrl = $('#' + typeProduct + '-' + idProduct + '-topphoto');
    var leftUrl = $('#' + typeProduct + '-' + idProduct + '-leftphoto');

    var widthElement = img.width();
    var heightElement = img.height();

    var start = false;
    img.mousedown(function () {
        start = true;
    });

    img.mouseup(function () {
        start = false;
    });

    svgContainer.mouseup(function () {
        start = false;
    });

    svgContainer.mousemove(function () {
        if (start == true) {
            var x = (parseFloat(event.pageX - posX) + widthElement) <= width ? (event.pageX - posX) : (width - widthElement);
            var y = (parseFloat(event.pageY - posY) + heightElement) <= height ? (event.pageY - posY) : (height - heightElement);

            img.css({top: y + "px", left: x + "px"});

            topUrl.val(y);
            leftUrl.val(x);
        }
    });
}

function generateShape(shape, height, width) {
    var factor = parseInt(height) > parseInt(width) ? height : width;

    for (var s in shapes) {
        if (shapes[s] == shape) {
            eval("generate" + shapes[s] + "()");
            svg.attr("transform", "scale(" + parseFloat(width / factor) + ", " + parseFloat(height / factor) + ")");
        }
    }
}


//Prostokąt
function generateRectangle() {
    params = {H: 200, H1: 200, H2: 200, B: 150, B1: 150, B2: 150, S: 100, R: 50};
    var H = params.H;
    var B = params.B;

    var rectangle = "M0,0v" + H + "H" + B + "V0" + "z";
    svg.append("path").attr("d", rectangle).attr("fill", colour).attr("stroke", "black").attr("transform", "translate(40, 40)");
}

//Trójkąt prostokątny
function generateTriangle() {
    params = {H: 200, H1: 200, H2: 200, B: 150, B1: 150, B2: 150, S: 100, R: 50};
    var H = params.H;
    var B = params.B;

    var triangle = "M0,0v" + H + "h" + B + "z";
    svg.append("path").attr("d", triangle).attr("fill", colour).attr("stroke", "black").attr("transform", "translate(40, 40)");
}

//Trójkąt równoboczny
function generateIsoscelesTriangle() {
    var H = params.H;
    var B = params.B;

    var rectangle = "M" + B + "," + H + "L0," + H + "L" + parseFloat(B / 2) + ",0z";
    svg.append("path").attr("d", rectangle).attr("fill", colour).attr("stroke", "black").attr("transform", "translate(40, 40)");
}

//Trójkąt dowolny
function generateCustomTriangle() {
    params = {H: 150, H1: 200, H2: 200, B: 200, B1: 50, B2: 150, S: 100, R: 50};
    var H = params.H;
    var B = params.B;
    var B1 = params.B1;

    var triangle = "M" + B + "," + H + "L0," + H + "L" + B1 + ",0z";
    svg.append("path").attr("d", triangle).attr("fill", colour).attr("stroke", "black").attr("transform", "translate(40, 40)");
}

//Trapez prostokątny prawy
function generateRightRightTrapeze() {
    params = {H: 150, H1: 200, H2: 200, B: 200, B1: 100, B2: 150, S: 100, R: 50};
    var H = params.H;
    var B = params.B;
    var B1 = params.B1;

    var trapeze = "M" + B + "," + H + "L0," + H + "L0,0L" + B1 + ",0z";
    svg.append("path").attr("d", trapeze).attr("fill", colour).attr("stroke", "black").attr("transform", "translate(40, 40)");
}

//Trapez prostokątny górny
function generateTopRightTrapeze() {
    params = {H: 200, H1: 100, H2: 200, B: 200, B1: 50, B2: 150, S: 100, R: 50};
    var H = params.H;
    var B = params.B;
    var H1 = params.H1;

    var trapeze = "M" + B + "," + parseInt(H - H1) + "L" + B + "," + H + "L0," + H + "L0,0z";
    svg.append("path").attr("d", trapeze).attr("fill", colour).attr("stroke", "black").attr("transform", "translate(40, 40)");
}

//Pięciokąt trzyprostokątny
function generateTripleRightPentagon() {
    params = {H: 200, H1: 100, H2: 200, B: 200, B1: 100, B2: 150, S: 100, R: 50};
    var H = params.H;
    var B = params.B;
    var H1 = params.H1;
    var B1 = params.B1;

    var pentagon = "M" + B + "," + parseInt(H - H1) + "L" + B + "," + H + "L0," + H + "L0,0L" + B1 + ",0z";
    svg.append("path").attr("d", pentagon).attr("fill", colour).attr("stroke", "black").attr("transform", "translate(40, 40)");
}

//Trapez
function generateTrapeze() {
    params = {H: 200, H1: 200, H2: 200, B: 200, B1: 50, B2: 100, S: 100, R: 50};
    var H = params.H;
    var B = params.B;
    var B1 = params.B1;
    var B2 = params.B2;

    var trapeze = "M" + B + "," + H + "L0," + H + "L" + B1 + ",0L" + B2 + ",0z";
    svg.append("path").attr("d", trapeze).attr("fill", colour).attr("stroke", "black").attr("transform", "translate(40, 40)");
}

//Romb
function generateRhombus() {
    params = {H: 150, H1: 200, H2: 200, B: 200, B1: 50, B2: 150, S: 100, R: 50};
    var H = params.H;
    var B = params.B;
    var B1 = params.B1;

    var rhombus = "M" + B + ",0L" + B1 + ",0L0," + H + "L" + (B - B1) + "," + H + "z";
    svg.append("path").attr("d", rhombus).attr("fill", colour).attr("stroke", "black").attr("transform", "translate(40, 40)");
}

//Trapez pochylony
function generateTrapezoidLeaning() {
    params = {H: 200, H1: 200, H2: 200, B: 200, B1: 100, B2: 180, S: 100, R: 50};
    var H = params.H;
    var B = params.B;
    var B1 = params.B1;
    var B2 = params.B2;

    var trapeze = "M" + B + ",0L" + B1 + ",0L0," + H + "L" + B2 + "," + H + "z";
    svg.append("path").attr("d", trapeze).attr("fill", colour).attr("stroke", "black").attr("transform", "translate(40, 40)");
}

//Czworokąt
function generateQuadrangle() {
    params = {H: 200, H1: 150, H2: 200, B: 200, B1: 150, B2: 150, S: 100, R: 50};
    var H = params.H;
    var H1 = params.H1;
    var B = params.B;
    var B1 = params.B1;

    var quadrangle = "M" + B + "," + H + "L0," + H + "L0," + (H - H1) + "L" + B1 + ",0z";
    svg.append("path").attr("d", quadrangle).attr("fill", colour).attr("stroke", "black").attr("transform", "translate(40, 40)");
}

//Czworokąt prostokątny wydłużony
function generateRightQuadrangleWarped() {
    params = {H: 200, H1: 100, H2: 200, B: 200, B1: 100, B2: 150, S: 100, R: 50};
    var H = params.H;
    var H1 = params.H1;
    var B = params.B;
    var B1 = params.B1;

    var quadrangle = "M" + B + ",0L0," + (H - H1) + "L0," + H + "L" + B1 + "," + H + "z";
    svg.append("path").attr("d", quadrangle).attr("fill", colour).attr("stroke", "black").attr("transform", "translate(40, 40)");
}

//Czworokąt prostokątny
function generateRightQuadrangle() {
    params = {H: 200, H1: 100, H2: 200, B: 200, B1: 170, B2: 150, S: 100, R: 50};
    var H = params.H;
    var H1 = params.H1;
    var B = params.B;
    var B1 = params.B1;

    var quadrangle = "M0,0L0," + H + "L" + B1 + "," + H + "L" + B + "," + (H - H1) + "z";
    svg.append("path").attr("d", quadrangle).attr("fill", colour).attr("stroke", "black").attr("transform", "translate(40, 40)");
}

//Czworokąt prostokątny spłaszczony
function generateRightQuadrangleFlattened() {
    params = {H: 200, H1: 170, H2: 200, B: 200, B1: 170, B2: 150, S: 100, R: 50};
    var H = params.H;
    var H1 = params.H1;
    var B = params.B;
    var B1 = params.B1;

    var quadrangle = "M" + B + "," + H + "L0," + H + "L0,0L" + B1 + "," + (H - H1) + "z";
    svg.append("path").attr("d", quadrangle).attr("fill", colour).attr("stroke", "black").attr("transform", "translate(40, 40)");
}

//Pięciokąt dwuprostokątny
function generateDoubleRightPentagon() {
    params = {H: 200, H1: 150, H2: 180, B: 200, B1: 150, B2: 150, S: 100, R: 50};
    var H = params.H;
    var H1 = params.H1;
    var H2 = params.H2;
    var B = params.B;
    var B1 = params.B1;

    var pentagon = "M" + B + "," + H + "L0," + H + "L0,0L" + B1 + "," + (H - H2) + "L" + B + "," + (H - H1) + "z";
    svg.append("path").attr("d", pentagon).attr("fill", colour).attr("stroke", "black").attr("transform", "translate(40, 40)");
}

//Czworokąt spłaszczony
function generateQuadrangleFlattened() {
    params = {H: 200, H1: 100, H2: 200, B: 200, B1: 50, B2: 175, S: 100, R: 50};
    var H = params.H;
    var H1 = params.H1;
    var B = params.B;
    var B1 = params.B1;
    var B2 = params.B2;

    var quadrangle = "M" + B + "," + H + "L0," + H + "L" + B1 + ",0L" + B2 + "," + (H - H1) + "z";
    svg.append("path").attr("d", quadrangle).attr("fill", colour).attr("stroke", "black").attr("transform", "translate(40, 40)");
}

//Czworokąt 3-nieregularny
function generateTripleIrregularQuadrangle() {
    params = {H: 200, H1: 100, H2: 200, B: 200, B1: 70, B2: 170, S: 100, R: 50};
    var H = params.H;
    var H1 = params.H1;
    var B = params.B;
    var B1 = params.B1;
    var B2 = params.B2;

    var quadrangle = "M0," + H + "L" + B1 + ",0L" + B + "," + (H - H1) + "L" + B2 + "," + H + "z";
    svg.append("path").attr("d", quadrangle).attr("fill", colour).attr("stroke", "black").attr("transform", "translate(40, 40)");
}

//Czworokąt wydłużony
function generateQuadrangleWarped() {
    params = {H: 200, H1: 150, H2: 200, B: 200, B1: 70, B2: 150, S: 100, R: 50};
    var H = params.H;
    var H1 = params.H1;
    var B = params.B;
    var B1 = params.B1;
    var B2 = params.B2;

    var quadrangle = "M0," + H + "L" + B1 + "," + (H - H1) + "L" + B + ",0L" + B2 + "," + H + "z";
    svg.append("path").attr("d", quadrangle).attr("fill", colour).attr("stroke", "black").attr("transform", "translate(40, 40)");
}

//Pięciokąt 2-spad
function generateDoublePitchPentagon() {
    params = {H: 200, H1: 150, H2: 50, B: 200, B1: 50, B2: 150, S: 100, R: 50};
    var H = params.H;
    var H1 = params.H1;
    var H2 = params.H2;
    var B = params.B;
    var B1 = params.B1;

    var pentagon = "M" + B + "," + (H - H2) + "L" + B + "," + H + "L0," + H + "L0" + "," + (H - H1) + "L" + B1 + ",0z";
    svg.append("path").attr("d", pentagon).attr("fill", colour).attr("stroke", "black").attr("transform", "translate(40, 40)");
}

//Pięciokąt jednoprostokątny
function generateSingleRightPentagon() {
    params = {H: 200, H1: 150, H2: 200, B: 200, B1: 50, B2: 150, S: 100, R: 50};
    var H = params.H;
    var H1 = params.H1;
    var B = params.B;
    var B1 = params.B1;
    var B2 = params.B2;

    var pentagon = "M" + B + "," + H + "L0," + H + "L0," + (H - H1) + "L" + B1 + ",0L" + B2 + ",0z";
    svg.append("path").attr("d", pentagon).attr("fill", colour).attr("stroke", "black").attr("transform", "translate(40, 40)");
}

//Pięciokąt jednoprostokątny wydłużony
function generateSingleRightPentagonWarped() {
    params = {H: 200, H1: 100, H2: 200, B: 200, B1: 100, B2: 150, S: 100, R: 50};
    var H = params.H;
    var H1 = params.H1;
    var B = params.B;
    var B1 = params.B1;
    var B2 = params.B2;

    var pentagon = "M" + B2 + "," + H + "L0," + H + ",L0," + (H - H1) + "L" + B1 + ",0L" + B + ",0z";
    svg.append("path").attr("d", pentagon).attr("fill", colour).attr("stroke", "black").attr("transform", "translate(40, 40)");
}

//Sześciokąt dwuprostokatny
function generateDoupleRightHexagon() {
    params = {H: 200, H1: 150, H2: 100, B: 200, B1: 50, B2: 180, S: 100, R: 50};
    var H = params.H;
    var H1 = params.H1;
    var H2 = params.H2;
    var B = params.B;
    var B1 = params.B1;
    var B2 = params.B2;

    var pentagon = "M" + B + "," + H + "L0," + H + "L0," + (H - H1) + "L" + B1 + ",0L" + B2 + ",0L" + B + "," + (H - H2) + "z";
    svg.append("path").attr("d", pentagon).attr("fill", colour).attr("stroke", "black").attr("transform", "translate(40, 40)");
}

//Czworokąt wydłużony w lewo
function generateQuadrangleWarpedInLeft() {
    params = {H: 200, H1: 50, H2: 200, B: 200, B1: 50, B2: 150, S: 100, R: 50};
    var H = params.H;
    var H1 = params.H1;
    var B = params.B;
    var B1 = params.B1;
    var B2 = params.B2;

    var quadrangle = "M" + B2 + "," + H + "L" + B1 + "," + H + "L0,0L" + B + "," + (H - H1) + "z";
    svg.append("path").attr("d", quadrangle).attr("fill", colour).attr("stroke", "black").attr("transform", "translate(40, 40)");
}

//Pięciokąt symetryczny
function generateSymmetricalPentagon() {
    params = {H: 200, H1: 150, H2: 200, B: 200, B1: 150, B2: 150, S: 100, R: 50};
    var H = params.H;
    var H1 = params.H1;
    var B = params.B;
    var B1 = params.B1;

    var pentagon = "M" + (parseInt((B - B1) / 2) + B1) + "," + H + "L" + ((B - B1) / 2) + "," + H + "L0," + (H - H1) + "L" + (B / 2) + ",0L" + B + "," + (H - H1) + "z";
    svg.append("path").attr("d", pentagon).attr("fill", colour).attr("stroke", "black").attr("transform", "translate(40, 40)");
}

//Sześciokąt dwuprostokatny wychylony
function generateDoubleRightHexagonWraped() {
    params = {H: 200, H1: 100, H2: 150, B: 200, B1: 50, B2: 150, S: 100, R: 50};
    var H = params.H;
    var H1 = params.H1;
    var H2 = params.H2;
    var B = params.B;
    var B1 = params.B1;
    var B2 = params.B2;

    var hexagon = "M" + B2 + "," + H + "L0," + H + "L0," + (H - H1) + "L" + B1 + ",0L" + B + ",0L" + B + "," + (H - H2) + "z";
    svg.append("path").attr("d", hexagon).attr("fill", colour).attr("stroke", "black").attr("transform", "translate(40, 40)");
}

//Sześciokąt symetryczny jednoosioww
function generateSymmetricalOneAxisHexagon() {
    params = {H: 150, H1: 200, H2: 200, B: 200, B1: 50, B2: 150, S: 100, R: 50};
    var H = params.H;
    var B = params.B;
    var B1 = params.B1;

    var hexagon = "M" + (parseInt((B - B1) / 2)) + "," + H + "L0," + (H / 2) + "L" + (parseInt((B - B1) / 2)) + ",0L" + (parseInt((B - B1) / 2) + B1) + ",0L" + B + "," + (H / 2) + "L" + (parseInt((B - B1) / 2) + B1) + "," + H + "z";
    svg.append("path").attr("d", hexagon).attr("fill", colour).attr("stroke", "black").attr("transform", "translate(40, 40)");
}

//Ośmiokąt symetryczny jednoosiowy
function generateSymmetricalOneAxisOctagon() {
    params = {H: 200, H1: 50, H2: 140, B: 200, B1: 50, B2: 140, S: 100, R: 50};
    var H = params.H;
    var H1 = params.H1;
    var H2 = params.H2;
    var B = params.B;
    var B1 = params.B1;
    var B2 = params.B2;

    var octagon = "M" + (parseInt((B - B1) / 2) + B1) + "," + H + "L" + (parseInt((B - B1) / 2)) + "," + H + "L0," + (H - H1) + "L0," + (H - H2) + "L" + (parseInt((B - B2) / 2)) + ",0L" + (parseInt((B - B2) / 2) + B2) + ",0L" + B + "," + (H - H2) + "L" + B + "," + (H - H1) + "z";
    svg.append("path").attr("d", octagon).attr("fill", colour).attr("stroke", "black").attr("transform", "translate(40, 40)");
}

//Okrąg
function generateCircle() {
    params = {H: 150, H1: 200, H2: 200, B: 200, B1: 50, B2: 150, S: 100, R: 100};
    var R = params.R;

    var arc = d3.arc()
            .innerRadius(0)
            .outerRadius(parseInt(R))
            .startAngle(0)
            .endAngle(2 * Math.PI);

    svg.append("path")
            .attr("d", arc)
            .attr("transform", "translate(120,120)").attr("fill", colour).attr("stroke", "black");

}

//Elipsa
function generateEllipse() {
    params = {H: 200, H1: 200, H2: 200, B: 100, B1: 50, B2: 150, S: 100, R: 50};
    var H = params.H;
    var B = params.B;

    var scale = 200;
    var R = H / 2;

    var arc = d3.arc()
            .innerRadius(0)
            .outerRadius(parseInt(R))
            .startAngle(0)
            .endAngle(2 * Math.PI);

    svg.append("path")
            .attr("d", arc)
            .attr("transform", "translate(120,120) scale(" + (B / scale) + "," + (H / scale) + ")").attr("fill", colour).attr("stroke", "black");
}

//Wycinek koła
function generateSliceCircle() {
    params = {H: 150, H1: 200, H2: 200, B: 180, B1: 50, B2: 150, S: 100, R: 50};
    var H = params.H;
    var B = params.B;

    var r = B;

    var cScale = d3.scaleLinear().domain([0, 100]).range([0, 0.45 * Math.PI]);

    data = [[0, 100, colour]];

    var arc = d3.arc()
            .innerRadius(0)
            .outerRadius(r)
            .startAngle(function (d) {
                return cScale(d[0]);
            })
            .endAngle(function (d) {
                return cScale(d[1]);
            });

    svg.selectAll("path")
            .data(data)
            .enter()
            .append("path")
            .attr("d", arc)
            .attr("stroke", "black")
            .style("fill", function (d) {
                return d[2];
            })
            .attr("transform", "translate(" + (parseInt(B) + 40) + ", " + (parseInt(B) + 40) + ") rotate(-90)");
}

//Wycinek pierścienia
function generateSliceRing() {
    params = {H: 150, H1: 200, H2: 200, B: 175, B1: 100, B2: 150, S: 100, R: 200};
    var H = params.H;
    var B = params.B;
    var B1 = params.B1;
    var R = params.R;


    var cScale = d3.scaleLinear().domain([0, 100]).range([0, 0.45 * Math.PI]);

    data = [[0, 100, colour]];

    var arc = d3.arc()
            .innerRadius(B1)
            .outerRadius(R)
            .startAngle(function (d) {
                return cScale(d[0]);
            })
            .endAngle(function (d) {
                return cScale(d[1]);
            });

    svg.selectAll("path")
            .data(data)
            .enter()
            .append("path")
            .attr("d", arc)
            .attr("stroke", "black")
            .style("fill", function (d) {
                return d[2];
            })
            .attr("transform", "translate(" + (parseInt(R) + 40) + ", " + (parseInt(R) + 40) + ") rotate(-90)");
}

//Nieregularny wycinke koła
function generateIrregularSliceCircle() {
    params = {H: 150, H1: 200, H2: 200, B: 200, B1: 50, B2: 150, S: 50, R: 50};
    var H = params.H;
    var B = params.B;
    var B1 = params.B1;
    var S = params.S;

    var l_2 = B1 * B1 + H * H;
    var r = S / 2 + l_2 / (8 * S);

    var d = "M 0 " + H + " h " + B + " l " + -(B - B1) + " " + -H + " a " + r + " " + r + " 0 0 0 " + -B1 + " " + H;

    svg.append("path").attr("d", d).attr("fill", colour).attr("stroke", "black").attr("transform", "translate(40, 40)");
}

//Symetryczna rama z łukiem okręgu
function generateSymmetricalFrameWithCircularArc() {
    params = {H: 150, H1: 200, H2: 200, B: 200, B1: 50, B2: 150, S: 100, R: 50};
    var H = params.H;
    var B = params.B;
    var S = params.S;

    var arc = d3.arc()
            .innerRadius(0)
            .outerRadius(B);

    var myString = arc({
        startAngle: 0,
        endAngle: (Math.PI / 2)
    }).split(/[A-Z]/);

    var frame = "M0," + H + "L" + B + "," + H + "L0," + H + "L0,0A" + myString[2] + "L" + B + "," + H;
    svg.append("path")
            .attr("fill", colour)
            .attr("stroke-width", 1)
            .attr("stroke", "black")
            .attr("d", frame)
            .attr("transform", "translate(40, 40)");
}

//Rama z łukiem okręgu
function generateFrameWithCircularArc() {
    params = {H: 200, H1: 150, H2: 200, B: 200, B1: 50, B2: 150, S: 100, R: 50};
    var H = params.H;
    var B = params.B;
    var H1 = params.H1;

    var arc = d3.arc()
            .innerRadius(0)
            .outerRadius(B);

    var myString = arc({
        startAngle: 0,
        endAngle: (Math.PI / 2)
    }).split(/[A-Z]/);

    var frame = "M" + B + ",0L" + B + "," + H + "L0," + H + "L0," + (H - H1) + "A" + myString[2];

    svg.append("path")
            .attr("fill", colour)
            .attr("stroke-width", 1)
            .attr("stroke", "black")
            .attr("d", frame)
            .attr("transform", "translate(40, 40)");
}

//Kształt nieokreślony
function generateIndefiniteShape() {
    params = {H: 200, H1: 200, H2: 200, B: 100, B1: 50, B2: 150, S: 100, R: 50};
    var H = params.H;
    var B = params.B;

    var rectangle = "M0,0v" + H + "H" + B + "V0" + "z";
    svg.append("path").attr("d", rectangle).attr("fill", colour).attr("stroke", "black").attr("transform", "translate(40, 40)");
}
