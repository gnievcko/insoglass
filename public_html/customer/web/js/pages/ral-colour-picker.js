function handleOpenModalButton($innerHtmlUrl) {
	$(document).on("click", ".open-modal", function(){
		var $modal = $(this).parents(".colour-picker-div").find(".modal"); 
		$.get($innerHtmlUrl, function(data) {
			$modal.find(".modal-body").html(data);
		});
		
		$modal.modal("show");
	});
}

function handleColourGroupClick() {
	$(document).on("click", "span.group", function(){
		$(this).parents(".groups").hide();
		var $title = $(this).parents(".modal-content").find(".modal-title");
		$title.find(".alt-title").text($(this).text()); 
		$title.find("span").toggle();
		$title.parent().find(".revert-glyphicon").toggle();
		$(this).parents(".modal-body").find('.group[data-id="' + $(this).attr("data-target") + '"]').show();
	});	
}


function handleColourClick() {
	$(document).on("click", ".ral-span", function(){
		var $parentDiv = $(this).parents(".colour-picker-div");
		console.log($parentDiv);
		$parentDiv.find(".colour-id").val($(this).attr("data-colour-id"));
		$parentDiv.find(".colour-rectangle").css("background-color", $(this).css("background-color"));
		$parentDiv.find(".colour-symbol").val($(this).text());
		$parentDiv.find(".colour-rgb-hash").val($(this).css("background-color"));
		$(this).parents(".modal").modal("toggle");
	});
}

function handleRevertIconClick() {
	$(document).on("click", ".revert-glyphicon", function(){
		$(this).hide();
		$(this).siblings(".modal-title").find("span").toggle();
		var $modalBody = $(this).parents(".modal-content").children(".modal-body");
		$modalBody.find("article.group").hide();
		$modalBody.find("article.groups").show();
		console.log($modalBody.find(".ral-span"));
		$modalBody.find(".ral-span").parent('li').show();
	});
}

function handleModalHide() {
	$(document).on("hidden.bs.modal", ".modal", function(){
		var $title = $(this).parents(".modal-content").find(".modal-title");
		$title.find("span").show();
		$title.find(".alt-title").hide();
		
		$(this).find(".revert-glyphicon").hide();
		$(this).find(".search-button").siblings("input").val("");
	});
}

function handleSearch() {
	$(document).on("click", ".search-button", function(){
		var $modalBody = $(this).parents(".modal-footer").siblings(".modal-body");
		var $text = $(this).siblings("input").val();
		$modalBody.find("article.group").show();
		$modalBody.find("article.groups").hide();
		$modalBody.find(".ral-span").parent('li').show();
		$modalBody.find(".ral-span:not(:contains('" + $text + "'))").parent('li').hide();
		
		$(".article.group:not(:has(li:visible))").hide();
		
		var $title = $(this).parents(".modal-content").find(".modal-title");
		$title.find(".alt-title").text($(this).attr("data-title"));
		$title.find("span").hide();
		$title.find(".alt-title").show();
		$title.parent().find(".revert-glyphicon").show();
		
	});
}



var handleColourPicker = function($innerHtmlUrl){	
	handleOpenModalButton($innerHtmlUrl);
	handleColourGroupClick();
	handleColourClick();
	handleRevertIconClick();
	handleModalHide();
	handleSearch();	
};