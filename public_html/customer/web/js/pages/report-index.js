$(function() {
	function setDate(element) {
		var now = new Date();

		var start = new Date();
		switch (parseInt($(element).val())) {
			case 0:
				start.setDate(now.getDate()-7);
				break;
			case 1:
				start.setDate(now.getDate()-30);
				break;
			case 2:
				start.setDate(now.getDate()-90);
				break;
			case 3:
				start.setDate(now.getDate()-365);
				break;
			default:
		}

		var dateFormat =  ("0" + (start.getDate())).slice(-2) + "." + ("0" + (start.getMonth() + 1)).slice(-2) + "." + start.getFullYear() ;
		$("#dateFromId").val(dateFormat);
		$("#dateFromId-disp-kvdate").kvDatepicker("update", dateFormat);

		var dateFormat =  ("0" + (now.getDate())).slice(-2) + "." + ("0" + (now.getMonth() + 1)).slice(-2) + "." + now.getFullYear() ;
		$("#dateToId").val(dateFormat);
		$("#dateToId-disp-kvdate").kvDatepicker("update", dateFormat);
	};

	$('#reportform-type').change(function() {
		setDate(this);
	});

	$('.document-icon').on('click', '.openDocument', function() {
		var dateFrom = $('#dateFromId').val();
		var dateTo = $('#dateToId').val();
		if($("#dateFromId-disp-kvdate").kvDatepicker("getDate") > $("#dateToId-disp-kvdate").kvDatepicker("getDate")) {
			alert(window.alertMessage);
			return false;
		}
		var url = $(this).attr('href');
		window.open(url+"?dateFrom="+dateFrom+"&dateTo="+dateTo,'_blank');
		return false;
	});
});
