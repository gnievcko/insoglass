$(".product-attribute-container").on("click", "input[type='checkbox']", function() {
	if($(this).is(":checked")) {
		var parentDiv = $(this).closest(".parent");
		var parentContainer = $(this).closest(".parent-container");
		var fieldId = parentDiv.data("id");
		
		if(parentDiv.data("checks")) {
			checkIds = String(parentDiv.data("checks")).split(",");
			
			for(var i = 0; i < checkIds.length; ++i) {
				$(parentContainer).find("div[data-id='" + checkIds[i] + "']").find("input[type='checkbox']").prop("checked", true);
			}
		}
		
		if(parentDiv.data("unchecks")) {
			uncheckIds = String(parentDiv.data("unchecks")).split(",");
			
			for(var i = 0; i < uncheckIds.length; ++i) {
				$(parentContainer).find("div[data-id='" + uncheckIds[i] + "']").find("input[type='checkbox']").prop("checked", false);
			}
		}
		
		/* Poniższy kod powinien zastąpić powyższy, gdy attributBlockedIds wskazywałby na idki, 
		 * które wyłączają ten checkbox, a nie które one sam wyłącza
		$("div[data-id]").each(function() {
			if($(this).data("disabledBy")) {
				disabledByIds = String($(this).data("disabledBy")).split(",");
				
				if($.inArray(fieldId, disabledByIds) >= 0) {
					$(this).find("input[type='checkbox']").prop("checked", false);
				}
			}
		});*/
	}
});