function dynamicTable(dynamicTableId, dataSourceUrl, options) {

    var options = options || {};

    var $dynamicTable = $("#" + dynamicTableId);

    var $dynamicTableData = $dynamicTable.find('.dynamic-table-data:first');
    var $sortDir;
    var $sortField;
    var $page;

    var initSortFieldVal;
    var initSortDirVal;

    var initPage = function () {
        $page = $dynamicTableData.find('.dynamic-table-page:first');
    };

    var initSorting = function () {
        $sortDir = $dynamicTableData.find('.dynamic-table-sort-dir:first');
        $sortField = $dynamicTableData.find('.dynamic-table-sort-field:first');
        initSortDirVal = $sortDir.val();
        initSortFieldVal = $sortField.val();
    };

    var initPagination = function () {
        $dynamicTableData.find('.dynamic-table-pagination:first a').on('click', function (event) {
            event.preventDefault();

            $page.val($(this).data('page'));
            update();
        });
    };

    var filters = [];
    var initFilterValues = [];
    $dynamicTable.find('.dynamic-table-filter').each(function (_, filter) {
        var $filter = $(filter);
        filters.push($filter);
        initFilterValues.push($filter.val());

        $filter.on('change', function () {
            $page.val(0);
            update();
        });
    });

    $dynamicTable.find('.clear-filters-button').on('click', function () {
        // TODO: do sprawdzenia
        sessionStorage.removeItem(getSavedFiltersStateKey());
        /*for(var i = 0; i < filters.length; ++i) {
         filters[i].val(initFilterValues[i]);
         }
         $page.val(0);
         $sortDir.val(initSortDirVal);
         $sortField.val(initSortFieldVal);*/

//        var storedQueryParameters = sessionStorage.getItem(getDefaultFiltersStateKey());
//
//        if (storedQueryParameters !== null) {
//            restoreFiltersState(JSON.parse(storedQueryParameters));
//        }

        window.open(window.location.protocol + "//" + window.location.host + window.location.pathname + "?", "_self");

        //update();
    });

    var getSavedFiltersStateKey = function () {
        return window.location.hostname + ':' + dynamicTableId;
    };

    var getDefaultFiltersStateKey = function () {
        return window.location.hostname + ':' + dynamicTableId + ':defaultFiltersState';
    }

    var update = function () {
        var params = collectParams();
        sessionStorage.setItem(getSavedFiltersStateKey(), JSON.stringify(params));

        $.getJSON(dataSourceUrl, params, function (response) {
            $dynamicTableData.html(response.view);
            history.pushState({}, {}, generateUrl(response.url));
            makeDynamic();
        });
    };

    var generateUrl = function (url) {
        var artificialUrl = options.url || null;
        if (artificialUrl !== null) {
            url = url.replace(dataSourceUrl, artificialUrl);
        }
        return url;
    };

    var collectParams = function () {
        var params = options.params || {};
        params[$sortDir.attr('name')] = $sortDir.val();
        params[$sortField.attr('name')] = $sortField.val();
        params[$page.attr('name')] = $page.val();

        filters.forEach(function ($filter) {
            params[$filter.attr('name')] = $filter.val();
        });

        return params;
    };

    var makeDynamic = function () {
        initSorting();
        initPagination();
        initPage();

        $dynamicTableData.find('.dynamic-table-sort').each(function (idx, field) {
            var $field = $(field);

            $field.on('click', function () {
                $sortDir.val($field.data('sort-dir'));
                $sortField.val($field.data('sort-field'));
                update();
            });
        });
    };
    makeDynamic();

    var hasUrlQueryParameters = function () {
        return window.location.href.indexOf('?') !== -1;
    };

    var restoreFiltersState = function (oldState) {
        $sortDir.val(oldState[$sortDir.attr('name')]);
        $sortField.val(oldState[$sortField.attr('name')]);
        $page.val(oldState[$page.attr('name')]);

        filters.forEach(function ($filter) {
            $filter.val(oldState[$filter.attr('name')]);
        });

        update();
    };

    if (!hasUrlQueryParameters()) {
        var params = collectParams();
        sessionStorage.setItem(getDefaultFiltersStateKey(), JSON.stringify(params));

        var storedQueryParameters = sessionStorage.getItem(getSavedFiltersStateKey());
        if (storedQueryParameters !== null) {
            restoreFiltersState(JSON.parse(storedQueryParameters));
        }
    }
    
    $dynamicTable.on("click", ".clickable-table-row", function(e) {
    	var elem, evt = e ? e:event;
    	if (evt.srcElement) {
    		elem = evt.srcElement;
    	}
    	else if (evt.target) {
    		elem = evt.target;  
    	}
    	  
    	if(!(elem.tagName == 'A' || elem.tagName == 'INPUT') && $(this).data("href")) {
    		window.location = $(this).data("href");
    	}
    });

    // sztuczna latka
    $dynamicTable.on("submit", function () {
        update();
    });
}