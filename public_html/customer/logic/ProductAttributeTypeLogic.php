<?php

namespace customer\logic;

use common\models\aq\ProductAttributeGroupQuery;
use common\models\aq\ProductAttributeTypeQuery;

class ProductAttributeTypeLogic {
    
    public function getAttributeGroupsWithTypes() {
        $groups = ProductAttributeGroupQuery::getAll();
        $attributes = ProductAttributeTypeQuery::getAll();
    
        return $this->mergeGroupsWithAttributes($groups, $attributes);
    }
    
    public function getAttributeGroupsWithValues($attributeValues) {
        $groups = ProductAttributeGroupQuery::getAll();
        $attributes = $attributeValues;
    
        return $this->mergeGroupsWithAttributes($groups, $attributes);
    }
    
    private function mergeGroupsWithAttributes($groups, $attributes) {
        $emptyGroup = [
            'name' => '',
            'attributes' => [],
        ];
        
        $isFirstIterationPassed = false;
        foreach($groups as &$group) {
            $group['attributes'] = [];
    
            foreach($attributes as $attribute) {
                if($attribute['groupId'] == $group['id']) {
                    $group['attributes'][] = $attribute;
                }
                elseif(empty($attribute['groupId']) && !$isFirstIterationPassed) {
                    $emptyGroup['attributes'][] = $attribute;
                }
            }
            
            $isFirstIterationPassed = true;
        }
        
        if(!empty($emptyGroup['attributes'])) {
            $groups[] = $emptyGroup;
        }
    
        return $groups;
    }
}
