# uwaga: skrypt musi byc w /home/ABC/www (lub analogicznym folderze)
# w przypadku wdrazania produkcyjnego, nalezy zamienic Beta na Production
find . ! \( -path ./uploads -prune -o -path ./script.sh \) | xargs /bin/rm -rf
unzip -o ../insoglas_beta.zip -d .
rm ../insoglas_beta.zip
chmod g-w customer/web/index.php
chmod g-w customer/web
chmod g-w customer
chmod g-w frontend/web/index.php
chmod g-w frontend/web
chmod g-w frontend
chmod g-w backend/web/index.php
chmod g-w backend/web
chmod g-w backend
php init --env=Production --overwrite=All
php yii rbac/update
php yii migrate --migrationPath=@console/migrations --interactive=0
chmod 0777 frontend/runtime/logs
chmod 0777 backend/runtime/logs
chmod 0777 console/runtime/logs
chmod 0777 customer/runtime/logs
chmod 0777 common/rbac/assignments.php
chmod 0777 vendor/kartik-v/mpdf/ttfontdata
