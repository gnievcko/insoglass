<?php

class m160822_071445_create_documents_collection extends \yii\mongodb\Migration
{
    public function up()
    {
        $this->createCollection('documents');
    }

    public function down()
    {
        $this->dropCollection('documents');
    }
}
