<?php
namespace console\controllers;

use yii\db\Query;
use common\alerts\OrderDeadlineAlertHandler;
use common\alerts\AlertConstants;
use common\alerts\UnchangedNewOrderAlertHandler;
use common\helpers\Utility;
use common\models\ar\AlertType;

class OrderController extends \console\components\Controller {

	public function actionCheckDeadlines() {
		$now = new \DateTime();
		
		$queryDeadline = (new Query())->select(['o.id'])
				->from('order o')
				->join('INNER JOIN', 'order_history oh', 'o.order_history_last_id = oh.id')
				->join('INNER JOIN', 'order_status os', 'oh.order_status_id = os.id')
				->where(['o.is_active' => 1])
				->andWhere('oh.date_deadline IS NOT NULL')
				->andWhere('oh.date_deadline <= :dateThreshold', [':dateThreshold' => $now->format('Y-m-d')])
				->andWhere(['not in', 'os.symbol', [Utility::ORDER_STATUS_INVOICED]]);
		
		$ordersExceedingDeadline = $queryDeadline->all();
		if(!empty($ordersExceedingDeadline)) {
			foreach($ordersExceedingDeadline as $order) {
				OrderDeadlineAlertHandler::add([
						'orderId' => $order['id'],
						'alertTypeSymbol' => AlertConstants::ALERT_TYPE_ORDER_DEADLINE_PROBLEM,
				]);
			}
		}
		
		$queryReminder = (new Query())->select(['o.id'])
				->from('order o')
				->where(['o.is_active' => 1])
				->join('INNER JOIN', 'order_history oh', 'o.order_history_last_id = oh.id')
				->join('INNER JOIN', 'order_status os', 'oh.order_status_id = os.id')
				->andWhere('oh.date_reminder IS NOT NULL')
				->andWhere('oh.date_reminder <= :dateThreshold', [':dateThreshold' => $now->format('Y-m-d')])
				->andWhere(['not in', 'os.symbol', [Utility::ORDER_STATUS_INVOICED]])
				->andWhere(['not in', 'o.id', array_column($ordersExceedingDeadline, 'id')]);
		
		$ordersExceedingReminder = $queryReminder->all();
		if(!empty($ordersExceedingReminder)) {
			foreach($ordersExceedingReminder as $order) {
				OrderDeadlineAlertHandler::add([
						'orderId' => $order['id'],
						'alertTypeSymbol' => AlertConstants::ALERT_TYPE_ORDER_DEADLINE_WARNING,
				]);
			}
		}
	}
    
    public function actionCheckNotUpdatedOrders() {
        if(!AlertType::findOne(['symbol' => AlertConstants::ALERT_TYPE_NEW_ORDER_STATUS_UNCHANGED])->is_enabled) {
            return;
        }
        $notUpdatedOrders = (new Query())->select(['o.id'])
                ->from('order o')
                ->innerJoin('order_history oh', 'o.order_history_last_id = oh.id')
                ->innerJoin('order_status os', 'os.id = oh.order_status_id')
				->andWhere([
                    'o.is_active' => 1,
                    'os.symbol' => Utility::ORDER_STATUS_ACCEPTED_BY_CLIENT,
                ])
				->andWhere('TIMESTAMPDIFF(HOUR, oh.date_creation, NOW()) >= 24')
                ->all();
        
        foreach($notUpdatedOrders as $order) {
            UnchangedNewOrderAlertHandler::add(['orderId' => $order['id']]);
        }
    }
}