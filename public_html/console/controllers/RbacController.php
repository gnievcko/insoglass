<?php
namespace console\controllers;

use Yii;
use yii\console\Controller;
use common\helpers\Utility;
use common\models\ar\User;
use common\models\ar\UserRole;
use common\models\ar\Role;
use common\models\aq\UserRoleQuery;

class RbacController extends Controller {
	
	private $roles = [];
	
    public function actionInit() {
        $this->initializeRbac();
    }
    
    public function actionUpdate() {
    	$auth = Yii::$app->authManager;
    	$auth->removeAll();
    	$this->initializeRbac();
    	$this->recalculateRoles();
    }
    
    public function assignRoleToUser($roleSymbol, $userId) {
    	$auth = Yii::$app->authManager;
    	$auth->assign($this->getRole($roleSymbol, $auth), $userId);
    }
    
    public function revokeRoleFromUser($roleSymbol, $userId) {
    	$auth = Yii::$app->authManager;
    	$auth->revoke($this->getRole($roleSymbol, $auth), $userId);
    }
    
    private function initializeRbac() {
    	$auth = Yii::$app->authManager;
    	
    	//Roles
    	
    	$client = $auth->createRole(Utility::ROLE_CLIENT.'_role');
    	$auth->add($client);        	
    	
    	$salesman = $auth->createRole(Utility::ROLE_SALESMAN.'_role');
    	$auth->add($salesman);
    	
    	$repairer = $auth->createRole(Utility::ROLE_REPAIRER.'_role');
    	$auth->add($repairer);
    	
    	$mainSalesman = $auth->createRole(Utility::ROLE_MAIN_SALESMAN.'_role');
    	$auth->add($mainSalesman);
    	$auth->addChild($mainSalesman, $salesman);
    	
    	$storekeeper = $auth->createRole(Utility::ROLE_STOREKEEPER.'_role');
    	$auth->add($storekeeper);
    	
    	$mainStorekeeper = $auth->createRole(Utility::ROLE_MAIN_STOREKEEPER.'_role');
    	$auth->add($mainStorekeeper);
    	$auth->addChild($mainStorekeeper, $storekeeper);
    	
    	$worker = $auth->createRole(Utility::ROLE_WORKER.'_role');
    	$auth->add($worker);
    	
    	$qaSpecialist = $auth->createRole(Utility::ROLE_QA_SPECIALIST.'_role');
    	$auth->add($qaSpecialist);
    	
    	$mainWorker = $auth->createRole(Utility::ROLE_MAIN_WORKER.'_role');
    	$auth->add($mainWorker);
    	$auth->addChild($mainWorker, $worker);
    	$auth->addChild($mainWorker, $qaSpecialist);
    	
    	$admin = $auth->createRole(Utility::ROLE_ADMIN.'_role');
    	$auth->add($admin);
    	$auth->addChild($admin, $client);
    	$auth->addChild($admin, $mainSalesman);
    	$auth->addChild($admin, $mainStorekeeper);
    	$auth->addChild($admin, $mainWorker);
    	$auth->addChild($admin, $repairer);
    	
    	//Rules
    	
    	$clientRule = $auth->createPermission(Utility::ROLE_CLIENT);
    	$auth->add($clientRule);
    	$auth->addChild($client, $clientRule); 
    	
    	$salesmanRule = $auth->createPermission(Utility::ROLE_SALESMAN);
    	$auth->add($salesmanRule);
    	$auth->addChild($salesman, $salesmanRule);
    	
    	$repairerRule = $auth->createPermission(Utility::ROLE_REPAIRER);
    	$auth->add($repairerRule);
    	$auth->addChild($repairer, $repairerRule);
    	
    	$mainSalesmanRule = $auth->createPermission(Utility::ROLE_MAIN_SALESMAN);
    	$auth->add($mainSalesmanRule);
    	$auth->addChild($mainSalesman, $mainSalesmanRule);
    	
    	$storekeeperRule = $auth->createPermission(Utility::ROLE_STOREKEEPER);
    	$auth->add($storekeeperRule);
    	$auth->addChild($storekeeper, $storekeeperRule);
    	
    	$mainStorekeeperRule = $auth->createPermission(Utility::ROLE_MAIN_STOREKEEPER);
    	$auth->add($mainStorekeeperRule);
    	$auth->addChild($mainStorekeeper, $mainStorekeeperRule);
    	
    	$workerRule = $auth->createPermission(Utility::ROLE_WORKER);
    	$auth->add($workerRule);
    	$auth->addChild($worker, $workerRule);
    	
    	$qaSpecialistRule = $auth->createPermission(Utility::ROLE_QA_SPECIALIST);
    	$auth->add($qaSpecialistRule);
    	$auth->addChild($qaSpecialist, $qaSpecialistRule);
    	
    	$mainWorkerRule = $auth->createPermission(Utility::ROLE_MAIN_WORKER);
    	$auth->add($mainWorkerRule);
    	$auth->addChild($mainWorker, $mainWorkerRule);
    	
    	$adminRule = $auth->createPermission(Utility::ROLE_ADMIN);
    	$auth->add($adminRule);
    	$auth->addChild($admin, $adminRule);
    }
    
    private function recalculateRoles() {
    	/*$users = User::find()->all();
    	
    	if(!empty($users)) {
    		$auth = Yii::$app->authManager;
    		
	    	foreach($users as $u) {
	    		foreach($u->getRoles()->all() as $r) {
	    			$auth->assign($this->getRole($r->symbol, $auth), $u->id);
	    		}
	    	} 
    	}*/
    	
    	$rules = UserRoleQuery::getAllRules();
    	
    	if(!empty($rules)) {
    		$auth = Yii::$app->authManager;
    		
    		$roles = Role::find()->all();
    		$rolesRules = [];
    		foreach($roles as $role) {
    			$roleRules[$role->symbol] = $this->getRole($role->symbol, $auth);
    		}
    		
    		foreach($rules as $r) {
    			$auth->assign($roleRules[$r['roleSymbol']], $r['userId']);
    		}
    	}
    }
    
    private function getRole($symbol, &$auth) {
    	if(empty($this->roles[$symbol])) {
    		$this->roles[$symbol] = $auth->getRole($symbol.'_role');
    	}
    	
    	return $this->roles[$symbol];
    }
}
