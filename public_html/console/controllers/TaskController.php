<?php
namespace console\controllers;

use Yii;
use yii\console\Controller;
use yii\db\Query;
use common\alerts\TaskDeadlineAlertHandler;
use common\alerts\AlertConstants;

class TaskController extends \console\components\Controller {

	public function actionCheckDeadlines() {
		$now = new \DateTime();
		
		$queryDeadline = (new Query())->select(['t.id', 't.title'])
				->from('task t')
				->where(['t.is_active' => 1, 't.is_complete' => 0])
				->andWhere('t.date_deadline IS NOT NULL')
				->andWhere('t.date_deadline <= :dateThreshold', [':dateThreshold' => $now->format('Y-m-d H:i:s')]);
		
		$tasksExceedingDeadline = $queryDeadline->all();
		if(!empty($tasksExceedingDeadline)) {
			foreach($tasksExceedingDeadline as $task) {
				TaskDeadlineAlertHandler::add([
						'taskId' => $task['id'],
						'alertTypeSymbol' => AlertConstants::ALERT_TYPE_TASK_DEADLINE_PROBLEM,
				]);
			}
		}
		
		$queryReminder = (new Query())->select(['t.id', 't.title'])
				->from('task t')
				->where(['t.is_active' => 1, 't.is_complete' => 0])
				->andWhere('t.date_reminder IS NOT NULL')
				->andWhere('t.date_reminder <= :dateThreshold', [':dateThreshold' => $now->format('Y-m-d H:i:s')])
				->andWhere(['not in', 't.id', array_column($tasksExceedingDeadline, 'id')]);
		
		$tasksExceedingReminder = $queryReminder->all();
		if(!empty($tasksExceedingReminder)) {
			foreach($tasksExceedingReminder as $task) {
				TaskDeadlineAlertHandler::add([
						'taskId' => $task['id'],
						'alertTypeSymbol' => AlertConstants::ALERT_TYPE_TASK_DEADLINE_WARNING,
				]);
			}
		}
	}
}