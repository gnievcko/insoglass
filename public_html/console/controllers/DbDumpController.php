<?php
namespace console\controllers;

use Yii;
use yii\console\Controller;
use common\models\ar\Configuration;

class DbDumpController extends \console\components\Controller {
	
	const DUMPNAME_MYSQL = 'backup_db_';
	const DUMPNAME_MONGODB = 'backup_mongodb_';
	
	public function actionDumpMysql() {
		try {
			$configuration = Configuration::find()->one();
			if(empty($configuration)) {
				return;	
			}
			
			$dumper = $configuration->path_mysqldump;
			if(empty($dumper)) {
				return;
			}
			
			$backupDir = ($configuration->path_dbbackups ?: '').DIRECTORY_SEPARATOR;			
			$username = Yii::$app->db->username;
			$password = Yii::$app->db->password;
			$dbName = explode('dbname=', Yii::$app->db->dsn)[1];
			
			$this->deleteOldMysqlBackups($configuration, self::DUMPNAME_MYSQL);
			
			// Ubuntu notation: "backup_db_"`date +"%Y-%m-%d_%H-%M"`".sql"
			$now = new \DateTime();
			
			shell_exec($dumper. ' -u '.$username.' -p'.$password.' '.$dbName.' > "'.$backupDir.self::DUMPNAME_MYSQL.$now->format('Y-m-d_H-i').'.sql"');
		}
		catch(\Exception $e) {
			Yii::error('Dumping MySQL database has just failed');
			Yii::error($e->getMessage());
			Yii::error($e->getTraceAsString());
		}
	}
	
	public function actionDumpMongodb() {
		try {
			$configuration = Configuration::find()->one();
			if(empty($configuration)) {
				return;
			}
		
			$dumper = $configuration->path_mongodbdump;
			if(empty($dumper)) {
				return;
			}
		
			$backupDir = ($configuration->path_dbbackups ?: '').DIRECTORY_SEPARATOR;	
			$dsn = explode('/', Yii::$app->mongodb->dsn);
			$dbName = end($dsn);
			
			$this->deleteOldMongodbBackups($configuration, self::DUMPNAME_MONGODB);
		
			// Ubuntu notation: "backup_db_"`date +"%Y-%m-%d_%H-%M"`".sql"
			$now = new \DateTime();
		
			shell_exec($dumper. ' --collection documents --db '.$dbName.' --out "'.$backupDir.self::DUMPNAME_MONGODB.$now->format('Y-m-d_H-i').'"');
		}
		catch(\Exception $e) {
			Yii::error('Dumping MongoDB database has just failed');
			Yii::error($e->getMessage());
			Yii::error($e->getTraceAsString());
		}
	}
	
	private function deleteOldMysqlBackups($configuration, $pattern) {
		if(empty($configuration->days_dbbackups_expiration)) {
			return;
		}
		
		$path = $configuration->path_dbbackups; 
		$filenames = $this->getFilesInFolder($path);
		
		if(!empty($filenames)) {
			$now = new \DateTime();
			foreach($filenames as $filename) {
				$fullDir = $path.DIRECTORY_SEPARATOR.$filename;
				if(file_exists($fullDir) && 
						preg_match('/^'.$pattern.'[0-9]{4}-[0-1][0-9]-[0-3][0-9]_[0-2][0-9]-[0-5][0-9].sql$/', $filename)) {
					$dateCreation = new \DateTime(date('Y-m-d H:i:s', filectime($fullDir)));
					$interval = $dateCreation->diff($now);
					if(intval($interval->format('%a')) > $configuration->days_dbbackups_expiration) {
						unlink($fullDir);
					}
				}
			}
		}
	}
	
	private function deleteOldMongodbBackups($configuration, $pattern) {
		if(empty($configuration->days_dbbackups_expiration)) {
			return;
		}
	
		$path = $configuration->path_dbbackups;
		$directories = $this->getDirectoriesInFolder($path);
	
		if(!empty($directories)) {
			$now = new \DateTime();
			foreach($directories as $directory) {
				$dirParts = explode(DIRECTORY_SEPARATOR, $directory);
				$dirName = end($dirParts);
				if(file_exists($directory) &&
						preg_match('/^'.$pattern.'[0-9]{4}-[0-1][0-9]-[0-3][0-9]_[0-2][0-9]-[0-5][0-9]$/', $dirName)) {
					$dateCreation = new \DateTime(date('Y-m-d H:i:s', filectime($directory)));
					$interval = $dateCreation->diff($now);
					if(intval($interval->format('%a')) > $configuration->days_dbbackups_expiration) {
						$this->deleteDirectory($directory);
					}
				}
			}
		}
	}
	
	private function getFilesInFolder($dir) {
		$filenames = array();
		if($handle = opendir($dir)) {
			while(false !== ($entry = readdir($handle))) {
				if($entry != '.' && $entry != '..') {
					$filenames[] = $entry;
				}
			}
	
			closedir($handle);
		}
	
		return $filenames;
	}
	
	private function getDirectoriesInFolder($dir) {
		return glob($dir.DIRECTORY_SEPARATOR.'*', GLOB_ONLYDIR);
	}
	
	private function deleteDirectory($dir) {
		if(!file_exists($dir)) {
			return true;
		}
	
		if(!is_dir($dir)) {
			return unlink($dir);
		}
	
		foreach(scandir($dir) as $item) {
			if($item == '.' || $item == '..') {
				continue;
			}
	
			if(!$this->deleteDirectory($dir . DIRECTORY_SEPARATOR . $item)) {
				return false;
			}
	
		}
	
		return rmdir($dir);
	}
}