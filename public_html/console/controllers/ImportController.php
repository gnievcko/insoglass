<?php
namespace console\controllers;

use Yii;
use common\helpers\RandomSymbolGenerator;
use common\models\aq\LanguageQuery;
use common\models\aq\UserQuery;
use common\models\aq\OfferedProductQuery;
use common\models\aq\OfferedProductCategoryQuery;
use common\models\ar\OfferedProductCategory;
use common\models\ar\OfferedProduct;
use common\models\ar\OfferedProductTranslation;
use common\models\ar\OfferedProductCategorySet;
use common\models\aq\ProductAttributeTypeQuery;
use common\models\ar\OfferedProductAttribute;
use common\models\ar\OfferedProductCategoryTranslation;
use common\models\aq\ShapeQuery;
use common\models\ar\Shape;
use common\models\ar\ShapeTranslation;
use common\models\aq\ConstructionQuery;
use common\models\ar\Construction;
use common\models\ar\ConstructionTranslation;
use common\models\aq\MuntinQuery;
use common\models\ar\Muntin;
use common\models\ar\MuntinTranslation;

class ImportController extends \console\components\Controller {

    private $isGlazed;
    private $omittedLines;
    private $codeColumnIndex;
    private $nameColumnIndex;
    private $parentCategoryId;
    private $currentCategoryId;
    private $languageId;
    private $adminId;
    private $glazingAttributeId;
    private $isNewData;
    
	public function actionProductFile($file, $isNewData, $isGlazed) {
	    $this->init();
	    
	    $this->isNewData = $isNewData;
	    $this->isGlazed = $isGlazed;
	    if(!empty($this->isGlazed)) {
	        $type = ProductAttributeTypeQuery::getBySymbol('zesp');
	        $this->glazingAttributeId = $type['id'];
	    }
	    
	    $csvFile = fopen($file, 'r');
	    
	    if(empty($csvFile)) {
	        echo 'The file '.$file.' cannot be loaded';
	    }	    
	    
	    $transaction = Yii::$app->db->beginTransaction();
	    try {
            while($columns = fgetcsv($csvFile, 0, ';')) {
                //on Linux this line causes some invalid encoding (if source file is already utf_8)
                $columns = array_map('utf8_encode', $columns);
                
    	        if($this->omittedLines < 2) {
                    $this->omittedLines++;
                    
                    if($this->omittedLines == 1) {
                        $categoryName = trim($columns[$this->nameColumnIndex]);
                        $this->parentCategoryId = $this->handleOfferedProductCategory($categoryName);
                        $this->currentCategoryId = $this->parentCategoryId;
                    }
                    elseif($this->omittedLines == 2) {
                        $this->codeColumnIndex = count($columns) - 1;
                    }
                    
                    continue;
                }
                
                if($this->isEmptyRow($columns)) {
                    continue;
                }
            
                if($this->isIncompleteRow($columns)) {
                    $categoryName = trim($columns[$this->nameColumnIndex]);
                    $this->currentCategoryId = $this->handleOfferedProductCategory($categoryName);
                    
                    continue;
                }
                
                $productName = trim($columns[$this->nameColumnIndex]);
                $productCode = trim($columns[$this->codeColumnIndex]);
                $this->handleOfferedProduct($productName, $productCode);
	       }
	       
	       $transaction->commit();
	    }
	    catch(\Exception $e) {
	        $transaction->rollBack();
	        echo $e->getMessage()."\n";
	        echo $e->getTraceAsString();
	    }
	}
	
	public function init() {
	    $this->omittedLines = 0;
	    $this->codeColumnIndex = 0;
	    $this->nameColumnIndex = 0;
	    $this->parentCategoryId = null;
	    $this->currentCategoryId = null;
	    $this->languageId = LanguageQuery::getDefaultLanguage()['id'];
	    
	    $admins = UserQuery::getAdmins();
	    $this->adminId = $admins[0];
	    
	    $this->glazingAttributeId = null; 
	}
	
	public function isIncompleteRow($columns) {
	    $c0 = trim($columns[$this->nameColumnIndex]);
	    $cn = trim($columns[$this->codeColumnIndex]);
	    
	    return empty($c0) || empty($cn);
	}
	
	public function isEmptyRow($columns) {
	    $emptyColumns = 0;
	    
	    for($i = 0; $i < count($columns); ++$i) {
	        $val = trim($columns[$i]);
	        if(empty($val)) {
	            $emptyColumns++;
	        }
	    }
	    
	    return $emptyColumns == count($columns);
	}
	
	public function handleOfferedProductCategory($name) {
	    $result = OfferedProductCategoryQuery::getByNameAndParent($name, $this->parentCategoryId);
	    
	    if(empty($result)) {
            $productCategory = new OfferedProductCategory();
            $productCategory->symbol = RandomSymbolGenerator::generate(OfferedProductCategory::className(), 'symbol');
            $productCategory->parent_category_id = $this->parentCategoryId;
            if(!$productCategory->save()) {
                throw new \Exception('Error during saving category: '.print_r($productCategory->getErrors(), true));
            }
            
            $translation = new OfferedProductCategoryTranslation();
            $translation->offered_product_category_id = $productCategory->id;
            $translation->language_id = $this->languageId;
            $translation->name = $name;
            if(!$translation->save()) {
                throw new \Exception('Error during saving category translation: '.print_r($translation->getErrors(), true));
            }
            
            return $productCategory->id;
	    }
        else {
            return $result['id'];
        }
	}
	
	public function handleOfferedProduct($name, $code) {
	    $result = OfferedProductQuery::getBySymbol($code);
	    $exists = !empty($result);
	    $symbolSuffix = '';
	    
	    if($exists) {
	        if(empty($this->isNewData)) {
	           $offeredProduct = OfferedProduct::findOne($result['id']);
	           $translation = $offeredProduct->getTranslation();
	        }
	        else {
	            $offeredProduct = new OfferedProduct();
	            $translation = new OfferedProductTranslation();
	            $symbolSuffix = '_';
	        }
	    }
	    else {
	        $offeredProduct = new OfferedProduct();
	        $translation = new OfferedProductTranslation();
	    }
	    
	    $offeredProduct->symbol = $code.$symbolSuffix;
	    $offeredProduct->user_id = $this->adminId;
	    $offeredProduct->is_artificial = 0;
	    $offeredProduct->is_available = 1;
	    $offeredProduct->is_active = 1;
	    if(!$offeredProduct->save()) {
	        throw new \Exception('Error during saving product: '.print_r($offeredProduct->getErrors(), true));
	    }
	    
	    $translation->offered_product_id = $offeredProduct->id;
	    $translation->language_id = $this->languageId;
	    $translation->name = $name;
        if(!$translation->save()) {
            throw new \Exception('Error during saving product translation: '.print_r($translation->getErrors(), true));
        }
        
        if(!empty($this->parentCategoryId)) {
            if(!$exists || !$this->isCategoryAssignedToOfferedProduct($offeredProduct, $this->parentCategoryId)) {
                $opcs = new OfferedProductCategorySet();
                $opcs->offered_product_id = $offeredProduct->id;
                $opcs->offered_product_category_id = $this->parentCategoryId;
                if(!$opcs->save()) {
                    throw new \Exception('Error during saving category set: '.print_r($opcs->getErrors(), true));
                }
            }
        }
        
        if(!empty($this->currentCategoryId) && intval($this->currentCategoryId) != intval($this->parentCategoryId)) {
            if(!$exists || !$this->isCategoryAssignedToOfferedProduct($offeredProduct, $this->currentCategoryId)) {
                $opcs = new OfferedProductCategorySet();
                $opcs->offered_product_id = $offeredProduct->id;
                $opcs->offered_product_category_id = $this->currentCategoryId;
                if(!$opcs->save()) {
                    throw new \Exception('Error during saving category set: '.print_r($opcs->getErrors(), true));
                }
            }
        }
        
        if(!empty($this->isGlazed) && !empty($this->glazingAttributeId)) {
            $opa = new OfferedProductAttribute();
            $opa->offered_product_id = $offeredProduct->id;
            $opa->product_attribute_type_id = $this->glazingAttributeId;
            $opa->value = "1";
            if(!$opa->save()) {
                throw new \Exception('Error during saving attributes: '.print_r($opa->getErrors(), true));
            }
            
            $offeredProduct->attribute_value = '[{"id":'.$this->glazingAttributeId.',"value":1}]';
            if(!$offeredProduct->save()) {
                throw new \Exception('Error during saving attributes in product: '.print_r($offeredProduct->getErrors(), true));
            }
        }
	}
	
	public function isCategoryAssignedToOfferedProduct($offeredProduct, $offeredProductCategoryId) {
	    if(empty($offeredProduct->offeredProductCategories)) {
	        return false;
	    }
	    
	    foreach($offeredProduct->offeredProductCategories as $opc) {
	        if(intval($opc->id) == intval($offeredProductCategoryId)) {
	            return true;
	        }
	    }
	        
	    return false;
	}
	
	public function actionShapeFile($file) {
	    $this->init();
	    $this->nameColumnIndex = 1;
	    
	    $csvFile = fopen($file, 'r');
	     
	    if(empty($csvFile)) {
	        echo 'The file '.$file.' cannot be loaded';
	    }
	     
	    $transaction = Yii::$app->db->beginTransaction();
	    try {
	        while($columns = fgetcsv($csvFile, 0, ';')) {
                //on Linux this line causes some invalid encoding (if source file is already utf_8)
                $columns = array_map('utf8_encode', $columns);
	    
	            if($this->omittedLines < 2) {
	                $this->omittedLines++;
	                continue;
	            }
	    
	            if($this->isEmptyRow($columns)) {
	                continue;
	            }
	    
	            $name = trim($columns[$this->nameColumnIndex]);
	            if(empty(ShapeQuery::getByName($name))) {
	                $obj = new Shape();
	                $obj->symbol = RandomSymbolGenerator::generate(Shape::class);
	                if(!$obj->save()) {
                        throw new \Exception('An error occurred during saving the object: '.print_r($obj->getErrors(), true));
	                }
	                
	                $translation = new ShapeTranslation();
	                $translation->shape_id = $obj->id;
	                $translation->language_id = $this->languageId;
	                $translation->name = $name;
	                if(!$translation->save()) {
	                    throw new \Exception('An error occurred during saving the translation: '.print_r($translation->getErrors(), true));
	                }
	            }
	        }
	    
	        $transaction->commit();
	    }
	    catch(\Exception $e) {
	        $transaction->rollBack();
	        echo $e->getMessage()."\n";
	        echo $e->getTraceAsString();
	    }
	}
	
	public function actionConstructionFile($file) {
	    $this->init();
	    $this->nameColumnIndex = 0;
	     
	    $csvFile = fopen($file, 'r');
	
	    if(empty($csvFile)) {
	        echo 'The file '.$file.' cannot be loaded';
	    }
	
	    $transaction = Yii::$app->db->beginTransaction();
	    try {
	        while($columns = fgetcsv($csvFile, 0, ';')) {

	            //on Linux this line causes some invalid encoding (if source file is already utf_8)
	            $columns = array_map('utf8_encode', $columns);
	             
	            if($this->omittedLines < 2) {
	                $this->omittedLines++;
	                continue;
	            }
	             
	            if($this->isEmptyRow($columns)) {
	                continue;
	            }
	             
	            $name = trim($columns[$this->nameColumnIndex]);
	            if(empty(ConstructionQuery::getByName($name))) {
	                $obj = new Construction();
	                $obj->symbol = RandomSymbolGenerator::generate(Construction::class);
	                if(!$obj->save()) {
	                    throw new \Exception('An error occurred during saving the object: '.print_r($obj->getErrors(), true));
	                }
	                 
	                $translation = new ConstructionTranslation();
	                $translation->construction_id = $obj->id;
	                $translation->language_id = $this->languageId;
	                $translation->name = $name;
	                if(!$translation->save()) {
	                    throw new \Exception('An error occurred during saving the translation: '.print_r($translation->getErrors(), true));
	                }
	            }
	        }
	         
	        $transaction->commit();
	    }
	    catch(\Exception $e) {
	        $transaction->rollBack();
	        echo $e->getMessage()."\n";
	        echo $e->getTraceAsString();
	    }
	}
	
	public function actionMuntinFile($file) {
	    $this->init();
	    $this->nameColumnIndex = 0;
	
	    $csvFile = fopen($file, 'r');
	
	    if(empty($csvFile)) {
	        echo 'The file '.$file.' cannot be loaded';
	    }
	
	    $transaction = Yii::$app->db->beginTransaction();
	    try {
	        while($columns = fgetcsv($csvFile, 0, ';')) {
                //on Linux this line causes some invalid encoding (if source file is already utf_8)
                $columns = array_map('utf8_encode', $columns);
	
	            if($this->omittedLines < 2) {
	                $this->omittedLines++;
	                continue;
	            }
	
	            if($this->isEmptyRow($columns)) {
	                continue;
	            }
	
	            $name = trim($columns[$this->nameColumnIndex]);
	            if(empty(MuntinQuery::getByName($name))) {
	                $obj = new Muntin();
	                $obj->symbol = RandomSymbolGenerator::generate(Muntin::class);
	                if(!$obj->save()) {
	                    throw new \Exception('An error occurred during saving the object: '.print_r($obj->getErrors(), true));
	                }
	
	                $translation = new MuntinTranslation();
	                $translation->muntin_id = $obj->id;
	                $translation->language_id = $this->languageId;
	                $translation->name = $name;
	                if(!$translation->save()) {
	                    throw new \Exception('An error occurred during saving the translation: '.print_r($translation->getErrors(), true));
	                }
	            }
	        }
	
	        $transaction->commit();
	    }
	    catch(\Exception $e) {
	        $transaction->rollBack();
	        echo $e->getMessage()."\n";
	        echo $e->getTraceAsString();
	    }
	}
}