<?php
$params = array_merge(
	    require(__DIR__ . '/../../common/config/params.php'),
	    require(__DIR__ . '/../../common/config/params-local.php'),
	    require(__DIR__ . '/params.php'),
	    require(__DIR__ . '/params-local.php')
);

return [
	    'id' => 'app-console', //don't change this name!!!
	    'basePath' => dirname(__DIR__),
	    'bootstrap' => ['log'],
	    'controllerNamespace' => 'console\controllers',
	    'components' => [
		        'log' => [
			            'targets' => [
			                [
				                    'class' => 'yii\log\FileTarget',
				                    'levels' => ['error', 'warning'],
			                ],
			            ],
		        ],
	    		'urlManager' => [
	    				'baseUrl' => '',
	    				'hostInfo' => 'http://mindseater.com/', //TODO change this line
	    				'enablePrettyUrl' => true,
	    				'showScriptName' => false,
	    				'enableStrictParsing' => false,
	    				'rules' => [
	    						'/' => 'site/index',
	    						'<controller:\w+>/<id:\d+>'=>'<controller>/view',
	    						'<controller:\w+>/<action:\w+>/<id:\d+>-<title:.*?>'=>'<controller>/<action>',
	    						'<controller:\w+>/<action:\w+>/<id:\d+>'=>'<controller>/<action>',
	    						'<controller:\w+>/<action:\w+>/<title:.*?>'=>'<controller>/<action>',
	    						'<controller:\w+>/<action:\w+>'=>'<controller>/<action>',
	    				]
	    		],
	    ],
	    'controllerMap' => [
		    	'migrate' => [
				    	'class' => 'yii\console\controllers\MigrateController',
				    	'templateFile' => 'console/views/migration.php'
			    ],
                'mongodb-migrate' => [
                    'class' => 'yii\mongodb\console\controllers\MigrateController',
                    'migrationPath' => '@app/mongodb-migrations',
                ]
	    ],
	    'params' => $params,
];
