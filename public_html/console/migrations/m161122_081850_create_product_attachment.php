<?php

use yii\db\Schema;
use yii\db\Migration;

class m161122_081850_create_product_attachment extends Migration {

    public function up() {
    	$tableOptions = null;
    	if($this->db->driverName === 'mysql') {
    		$tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
    	}
    	
    	$this->createTable('product_attachment', [
    			'id' => $this->primaryKey(11),
    			'product_id' => $this->integer(11)->notNull(),
    			'user_id' => $this->integer(11)->notNull(),
    			'url_file' => $this->string(256)->notNull(),
    			'name' => $this->string(256)->notNull(),
    			'description' => $this->string(512),
    			'date_creation' => $this->timestamp()->notNull(),
    	], $tableOptions);
    	 
    	$this->addForeignKey('fk_product_attachment_product', 'product_attachment', 'product_id', 'product', 'id');
    	$this->addForeignKey('fk_product_attachment_user', 'product_attachment', 'user_id', 'user', 'id');
    	$this->alterColumn('product_attachment', 'date_creation', 'TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP');
    	
    	$this->createTable('offered_product_attachment', [
    			'id' => $this->primaryKey(11),
    			'offered_product_id' => $this->integer(11)->notNull(),
    			'user_id' => $this->integer(11)->notNull(),
    			'url_file' => $this->string(256)->notNull(),
    			'name' => $this->string(256)->notNull(),
    			'description' => $this->string(512),
    			'date_creation' => $this->timestamp()->notNull(),
    	], $tableOptions);
    	
    	$this->addForeignKey('fk_offered_product_attachment_product', 'offered_product_attachment', 'offered_product_id', 'offered_product', 'id');
    	$this->addForeignKey('fk_offered_product_attachment_user', 'offered_product_attachment', 'user_id', 'user', 'id');
    	$this->alterColumn('offered_product_attachment', 'date_creation', 'TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP');
    }

    public function down() {
    	$this->dropTable('offered_product_attachment');
        $this->dropTable('product_attachment');
    }
}
