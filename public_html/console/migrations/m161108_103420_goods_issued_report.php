<?php

use yii\db\Schema;
use yii\db\Migration;
use common\models\ar\DocumentType;
use common\models\ar\DocumentSection;
use common\models\ar\DocumentTypeSection;
use common\documents\sections\SectionsMapper;

class m161108_103420_goods_issued_report extends Migration {

	public function safeUp() {
		$footerId = DocumentSection::find()->where(['symbol' => SectionsMapper::COMPANY_FOOTER])->one()->id;
		$headerId = DocumentSection::find()->where(['symbol' => SectionsMapper::COMPANY_SHORT_HEADER])->one()->id;
		 
		$this->insert('document_type', [
				'symbol' => 'GOODS_ISSUED_REPORT',
				'document_section_footer_id' => $footerId,
				'document_section_header_id' => $headerId,
				'margin_top' => 50,
				'margin_bottom' => 50,
				'margin_left' => 15,
				'margin_right' => 15,
				'format' => 'A4',
				'orientation' => 'P'
		]);
		$documentTypeId = $this->db->getLastInsertID();
		$this->insert('document_section', [ 'symbol' => 'GOODS_ISSUED_REPORT']);
		$documentSectionId = $this->db->getLastInsertID();	 
		$this->insert('document_type_section', [
				'document_type_id' => $documentTypeId,
				'document_section_id' => $documentSectionId,
				'sequence_number' => 1,
		]);
	}

	public function safeDown() {
		$documentTypeId = DocumentType::find()->where(['symbol' => 'GOODS_ISSUED_REPORT'])->one()->id;
		$documentSectionId = DocumentSection::find()->where(['symbol' => 'GOODS_ISSUED_REPORT'])->one()->id;
		DocumentTypeSection::deleteAll(['document_type_id' => $documentTypeId]);
		DocumentSection::deleteAll(['id'=> $documentSectionId]);
		DocumentType::deleteAll(['id' => $documentTypeId]);
	}
}
