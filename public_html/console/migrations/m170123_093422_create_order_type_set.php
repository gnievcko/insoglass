<?php

use yii\db\Schema;
use yii\db\Migration;
use yii\db\Query;

class m170123_093422_create_order_type_set extends Migration {

	public function safeUp() {
		$tableOptions = null;
    	if($this->db->driverName === 'mysql') {
    		$tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
    	}
    	
    	$this->createTable('order_type_set', [
    			'order_id' => $this->integer(11)->notNull(),
    			'order_type_id' => $this->integer(11)->notNull(),
    	], $tableOptions);
    	
    	$this->addPrimaryKey('pk_order_type_set', 'order_type_set', ['order_id', 'order_type_id']);
    	$this->addForeignKey('fk_order_type_set_order', 'order_type_set', 'order_id', 'order', 'id');
    	$this->addForeignKey('fk_order_type_set_order_type', 'order_type_set', 'order_type_id', 'order_type', 'id');
    	
    	$orderTypes = (new Query())->select(['o.id', 'o.order_type_id'])
    			->from('order o')
    			->where('o.order_type_id IS NOT NULL')
    			->orderBy(['o.id' => SORT_ASC])
    			->all();
    	
    	if(!empty($orderTypes)) {
    		foreach($orderTypes as $ot) {
    			$this->insert('order_type_set', [
    					'order_id' => intval($ot['id']), 'order_type_id' => intval($ot['order_type_id']),
    			]);
    		}
    	}
    	
    	$this->dropForeignKey('fk_order_order_type', 'order');
    	$this->dropColumn('order', 'order_type_id');
    }

    public function safeDown() {
        $this->addColumn('order', 'order_type_id', 'INT(11) NULL DEFAULT NULL AFTER payment_terms');
        $this->addForeignKey('fk_order_order_type', 'order', 'order_type_id', 'order_type', 'id');
        
        $orderTypes = (new Query())->select(['ots.order_id', 'ots.order_type_id'])
		        ->from('order_type_set ots')
		        ->orderBy(['ots.order_id' => SORT_ASC])
		        ->all();
         
        if(!empty($orderTypes)) {
        	foreach($orderTypes as $ot) {
        		$this->update('order', ['order_type_id' => intval($ot['order_type_id'])], ['id' => intval($ot['order_id'])]);
        	}
        }
        
        $this->dropTable('order_type_set');
    }
	
}
