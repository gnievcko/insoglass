<?php

use yii\db\Migration;

/**
 * Handles the creation for table `task_and_alert`.
 */
class m160712_080754_create_task_and_alert extends Migration
{
    /**
     * @inheritdoc
     */
    public function up() {
    	$tableOptions = null;
    	if($this->db->driverName === 'mysql') {
    		$tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
    	}
    	
		$this->createTable('task_type', [
    			'id' => $this->primaryKey(11),
    			'symbol' => $this->string(32)->notNull()->unique(),
    	], $tableOptions);
    	 
    	$this->createTable('task_type_translation', [
    			'task_type_id' => $this->integer(11),
    			'language_id' => $this->integer(11),
    			'name' => $this->string(128)->notNull(),
    	], $tableOptions);
    	
    	$this->addPrimaryKey('pk_task_type_translation', 'task_type_translation', ['task_type_id', 'language_id']);
    	$this->addForeignKey('fk_task_type_translation_language', 'task_type_translation', 'language_id', 'language', 'id');
    	$this->addForeignKey('fk_task_type_translation_type', 'task_type_translation', 'task_type_id', 'task_type', 'id');
    	 
    	$this->createTable('task_type_reference', [
    			'id' => $this->primaryKey(11),
    			'task_type_id' => $this->integer(11)->notNull(),
    			'symbol' => $this->string(64)->notNull(),
    			'name_table' => $this->string(64)->notNull(),
    			'name_column' => $this->string(64)->notNull(),
    	], $tableOptions);
    	
    	$this->addForeignKey('fk_task_type_reference_task_type', 'task_type_reference', 'task_type_id', 'task_type', 'id');
    	
    	$this->createTable('task', [
    			'id' => $this->primaryKey(11),
    			'task_type_id' => $this->integer(11)->notNull(),
    			'user_id' => $this->integer(11)->notNull(),
    			'message' => $this->string(2048)->notNull(),
    			'user_notified_id' => $this->integer(11),
    			'date_task_execution' => $this->timestamp()->defaultValue(null),
    			'date_deadline' => $this->timestamp()->defaultValue(null),
    			'is_active' => $this->boolean()->notNull()->defaultValue(1),
    			'user_deactivating_id' => $this->integer(11),
    			'date_deactivation' => $this->timestamp()->defaultValue(null),
    			'date_creation' => $this->timestamp()->defaultValue(null),
    	], $tableOptions);
    	
    	$this->addForeignKey('fk_task_task_type', 'task', 'task_type_id', 'task_type', 'id');
    	$this->addForeignKey('fk_task_user', 'task', 'user_id', 'user', 'id');
    	$this->addForeignKey('fk_task_user_notified', 'task', 'user_notified_id', 'user', 'id');
    	$this->addForeignKey('fk_task_user_deactivating', 'task', 'user_deactivating_id', 'user', 'id');
    	$this->alterColumn('task', 'date_task_execution', 'TIMESTAMP NULL DEFAULT NULL');
    	$this->alterColumn('task', 'date_deadline', 'TIMESTAMP NULL DEFAULT NULL');
    	$this->alterColumn('task', 'date_deactivation', 'TIMESTAMP NULL DEFAULT NULL');
    	$this->alterColumn('task', 'date_creation', 'TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP');
    	
    	$this->createTable('task_data', [
    			'task_id' => $this->integer(11),
    			'task_type_reference_id' => $this->integer(11),
    			'referenced_object_id' => $this->integer(11)->notNull(),
    	], $tableOptions);
    	
    	$this->addPrimaryKey('pk_task_data', 'task_data', ['task_type_reference_id', 'task_id']);
    	$this->addForeignKey('fk_task_data_task', 'task_data', 'task_id', 'task', 'id');
    	$this->addForeignKey('fk_task_data_reference', 'task_data', 'task_type_reference_id', 'task_type_reference', 'id');
    	
    	$this->createTable('alert_type', [
    			'id' => $this->primaryKey(11),
    			'symbol' => $this->string(32)->notNull()->unique(),
    	], $tableOptions);
    	
    	$this->createTable('alert_type_translation', [
    			'alert_type_id' => $this->integer(11),
    			'language_id' => $this->integer(11),
    			'name' => $this->string(128)->notNull(),
    	], $tableOptions);
    	 
    	$this->addPrimaryKey('pk_alert_type_translation', 'alert_type_translation', ['alert_type_id', 'language_id']);
    	$this->addForeignKey('fk_alert_type_translation_language', 'alert_type_translation', 'language_id', 'language', 'id');
    	$this->addForeignKey('fk_alert_type_translation_type', 'alert_type_translation', 'alert_type_id', 'alert_type', 'id');
    	
    	$this->createTable('alert', [
    			'id' => $this->primaryKey(11),
    			'alert_type_id' => $this->integer(11)->notNull(),
    			'user_id' => $this->integer(11)->notNull(),
    			'message' => $this->string(2048)->notNull(),
    			'is_active' => $this->boolean()->notNull()->defaultValue(1),
    			'user_deactivating_id' => $this->integer(11),
    			'date_deactivation' => $this->timestamp()->defaultValue(null),
    			'date_creation' => $this->timestamp()->defaultValue(null),
    			'order_id' => $this->integer(11),
    			'product_id' => $this->integer(11),
    			'company_id' => $this->integer(11),
    			'user_id' => $this->integer(11),
    			'task_id' => $this->integer(11),
    			'warehouse_id' => $this->integer(11)
    	], $tableOptions);
    	 
    	$this->addForeignKey('fk_alert_alert_type', 'alert', 'alert_type_id', 'alert_type', 'id');
    	$this->addForeignKey('fk_alert_user', 'alert', 'user_id', 'user', 'id');
    	$this->addForeignKey('fk_alert_user_deactivating', 'alert', 'user_deactivating_id', 'user', 'id');
    	
    	$this->addForeignKey('fk_alert_order_reference', 'alert', 'order_id', 'order', 'id');
    	$this->addForeignKey('fk_alert_product_reference', 'alert', 'product_id', 'product', 'id');
    	$this->addForeignKey('fk_alert_company_reference', 'alert', 'company_id', 'company', 'id');
    	$this->addForeignKey('fk_alert_user_reference', 'alert', 'user_id', 'user', 'id');
    	$this->addForeignKey('fk_alert_task_reference', 'alert', 'task_id', 'task', 'id');
    	$this->addForeignKey('fk_alert_warehouse_reference', 'alert', 'warehouse_id', 'warehouse', 'id');
    	
    	$this->alterColumn('alert', 'date_deactivation', 'TIMESTAMP NULL DEFAULT NULL');
    	$this->alterColumn('alert', 'date_creation', 'TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP');
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('alert');
        $this->dropTable('alert_type_translation');
        $this->dropTable('alert_type');
        $this->dropTable('task_data');
        $this->dropTable('task');
        $this->dropTable('task_type_reference');
        $this->dropTable('task_type_translation');
        $this->dropTable('task_type');
    }
}
