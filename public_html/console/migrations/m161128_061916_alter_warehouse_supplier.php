<?php

use yii\db\Schema;
use yii\db\Migration;

class m161128_061916_alter_warehouse_supplier extends Migration {

    public function up() {
		$this->addColumn('warehouse_supplier', 'contact_person', 'VARCHAR(256) NULL DEFAULT NULL AFTER name');
    }

    public function down() {
        $this->dropColumn('warehouse_supplier', 'contact_person');
    }

}
