<?php

use yii\db\Schema;
use yii\db\Migration;

class m160919_055045_alter_alert_history extends Migration {

    public function up() {
		$this->addColumn('alert_history', 'task_history_id', 'INT(11) NULL DEFAULT NULL AFTER task_id');
		$this->addForeignKey('fk_alert_history_task_history_reference', 'alert_history', 'task_history_id', 'task_history', 'id');
    }

    public function down() {
        $this->dropForeignKey('fk_alert_history_task_history_reference', 'alert_history');
        $this->dropColumn('alert_history', 'task_history_id');
    }
}
