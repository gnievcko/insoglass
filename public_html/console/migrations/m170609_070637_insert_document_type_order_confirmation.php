<?php

use yii\db\Schema;
use yii\db\Migration;
use common\models\ar\DocumentTypeSection;
use common\models\ar\DocumentType;
use common\models\ar\DocumentSection;
use yii\db\Query;

class m170609_070637_insert_document_type_order_confirmation extends Migration {

    public function safeUp() {
    	
    	$languageId = (new Query())->select(['id'])->from('language')->where(['symbol' => 'pl'])->scalar();
    	
    	$this->insert('document_section', ['symbol' => 'ORDER_CONFIRMATION_FOOTER',]);
    	$footerSectionId = $this->db->getLastInsertID();
    	
    	$this->insert('document_section', ['symbol' => 'ORDER_CONFIRMATION_BASIC_DATA',]);
    	$sectionId = $this->db->getLastInsertID();
    	
    	$this->insert('document_section', ['symbol' => 'ORDER_CONFIRMATION_PRODUCTS',]);
    	$productsSectionId = $this->db->getLastInsertID();
    	
    	$this->insert('document_section', ['symbol' => 'ORDER_CONFIRMATION_CUSTOM_DATA',]);
    	$customDataSectionId = $this->db->getLastInsertID();
    	
    	$headerSectionId = DocumentSection::find()->where(['symbol' => 'COMPANY_HEADER'])->one()->id;
    	
    	$this->insert('document_type', [
    			'symbol' => 'ORDER_CONFIRMATION',
    			'margin_top' => 15,
    			'margin_bottom' => 15,
    			'margin_left'=> 15,
    			'margin_right' => 15,
    			'format' => 'A4',
    			'orientation' => 'P',
				// Komentarz Kuby: Poniewaz ten dokument bedzie do specyficznych celow, to tutaj musimy go domyslnie wylaczyc
    			'is_for_order' => 0,
    	]);
    	
    	$documentTypeId = $this->db->getLastInsertId();
    	
    	$this->insert('document_type_section', ['document_type_id' => $documentTypeId, 'document_section_id' => $headerSectionId, 'sequence_number' => 0]);
    	$this->insert('document_type_section', ['document_type_id' => $documentTypeId, 'document_section_id' => $sectionId, 'sequence_number' => 1]);
    	$this->insert('document_type_section', ['document_type_id' => $documentTypeId, 'document_section_id' => $productsSectionId, 'sequence_number' => 3]);
    	$this->insert('document_type_section', ['document_type_id' => $documentTypeId, 'document_section_id' => $customDataSectionId, 'sequence_number' => 4]);
    	$this->insert('document_type_section', ['document_type_id' => $documentTypeId, 'document_section_id' => $footerSectionId, 'sequence_number' => 5]);
    	
    	$this->insert('document_type_translation', [
    			'document_type_id' => $documentTypeId,
    			'language_id' => $languageId,
    			'name' => 'Potwierdzenie zlecenia',
    	]);
    }

    public function safeDown() {
    	
    	$documentType1Id = DocumentType::find()->where(['symbol' => 'ORDER_CONFIRMATION'])->one()->id;
    	
    	DocumentTypeSection::deleteAll(['document_type_id' => $documentType1Id]);
    	DocumentType::deleteAll(['id' => $documentType1Id]);
    	DocumentSection::deleteAll(['symbol' => ['ORDER_CONFIRMATION_FOOTER']]);
    	DocumentSection::deleteAll(['symbol' => ['ORDER_CONFIRMATION_BASIC_DATA']]);
    	DocumentSection::deleteAll(['symbol' => ['ORDER_CONFIRMATION_PRODUCTS']]);
    	DocumentSection::deleteAll(['symbol' => ['ORDER_CONFIRMATION_CUSTOM_DATA']]);
    	
    	$this->delete('document_type_translation', ['document_type_id' => $documentType1Id]);

    }
}
