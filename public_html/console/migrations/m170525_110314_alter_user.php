<?php

use yii\db\Migration;

class m170525_110314_alter_user extends Migration {

    public function up() {
        $this->addColumn('user', 'can_logged',  $this->integer(1)->defaultValue(0)->notNull());
    }

    public function down() {
        $this->dropColumn('user', 'can_logged');
    }
}
