<?php

use yii\db\Schema;
use yii\db\Migration;

class m170116_135743_create_invoice_history extends Migration {

    public function up() {
    	$tableOptions = null;
    	if($this->db->driverName === 'mysql') {
    		$tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
    	}
    	 
    	$this->createTable('invoice_history', [
    			'id' => $this->primaryKey(11),
    			'document_type_id' => $this->integer(11)->notNull(),
    			'entity_id' => $this->integer(11)->notNull(),
    			'document_id' => $this->string(256)->notNull(),
    			'date_issue' => $this->date()->null(),
    			'number' => $this->string(256)->null(),
    			'is_last' => $this->boolean()->notNull()->defaultValue(1),
    			'date_creation' => $this->timestamp()->notNull(),
    	], $tableOptions);
    	
    	$this->addForeignKey('fk_invoice_history_document_type', 'invoice_history', 'document_type_id', 'document_type', 'id');
    	$this->addForeignKey('fk_invoice_history_entity', 'invoice_history', 'entity_id', 'entity', 'id');
    	$this->alterColumn('invoice_history', 'date_creation', 'TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP');
    }

    public function down() {
        $this->dropTable('invoice_history');
    }

}
