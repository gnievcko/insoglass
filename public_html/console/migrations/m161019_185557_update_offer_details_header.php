<?php

use common\documents\sections\SectionsMapper;
use yii\db\Migration;
use yii\db\Query;
use common\documents\DocumentType;

class m161019_185557_update_offer_details_header extends Migration {

    public function safeUp() {
        $this->insert('document_section', ['symbol' => SectionsMapper::COMPANY_HEADER_LOGOS_ONLY]);
        $headerSectionId = Yii::$app->db->lastInsertID;
        
        $this->update('document_type', [
            'document_section_header_id' => null
        ], [
            'symbol' => DocumentType::OFFER_DETAILS,
        ]);
        
        $offerDetailsId = $this->getOfferDetailsId();
            
        $this->update('document_type_section', [
            'sequence_number' => 2
        ], [
            'sequence_number' => 1,
            'document_type_id' => $offerDetailsId,
        ]);
        
        $this->update('document_type_section', [
            'sequence_number' => 1
        ], [
            'sequence_number' => 0,
            'document_type_id' => $offerDetailsId,
        ]);
        
        $this->insert('document_type_section', [
            'sequence_number' => 0,
            'document_type_id' => $offerDetailsId,
            'document_section_id' => $headerSectionId,
        ]);
    }

    public function safeDown() {
        $offerDetailsId = $this->getOfferDetailsId();
        
        $this->delete('document_type_section', [
            'document_type_id' => $offerDetailsId,
            'sequence_number' => 0,
        ]);
        
        $this->update('document_type_section', [
            'sequence_number' => 0
        ], [
            'sequence_number' => 1,
            'document_type_id' => $offerDetailsId,
        ]);
        
        $this->update('document_type_section', [
            'sequence_number' => 1
        ], [
            'sequence_number' => 2,
            'document_type_id' => $offerDetailsId,
        ]);
        
        $this->update('document_type', [
            'document_section_header_id' => (new Query())->select(['id'])
                ->from('document_section')
                ->where(['symbol' => SectionsMapper::COMPANY_HEADER])
                ->one()['id'],
        ], [
            'symbol' => DocumentType::OFFER_DETAILS,
        ]);
        
        $this->delete('document_section', ['symbol' => SectionsMapper::COMPANY_HEADER_LOGOS_ONLY]);
    }
    
    private function getOfferDetailsId() {
        return (new Query())->select(['id'])
                ->from('document_type')
                ->where(['symbol' => DocumentType::OFFER_DETAILS])
                ->one()['id'];
    }
}
