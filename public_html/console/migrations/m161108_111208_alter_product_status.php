<?php

use yii\db\Schema;
use yii\db\Migration;

class m161108_111208_alter_product_status extends Migration {

    public function up() {
		$this->update('product_status_translation', ['name' => 'Zwrot do dostawcy'], ['name' => 'Zwrot']);
    }

    public function down() {
    	$this->update('product_status_translation', ['name' => 'Zwrot'], ['name' => 'Zwrot do dostawcy']);
    }
}
