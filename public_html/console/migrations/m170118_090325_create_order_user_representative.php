<?php

use yii\db\Migration;
use yii\db\Query;

class m170118_090325_create_order_user_representative extends Migration {

    public function safeUp() {
		$tableOptions = null;
    	if($this->db->driverName === 'mysql') {
    		$tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
    	}
    	
    	$this->createTable('order_user_representative', [
    			'order_id' => $this->integer(11)->notNull(),
    			'user_representative_id' => $this->integer(11)->notNull(),
    	], $tableOptions);
    	
    	$this->addPrimaryKey('pk_order_user_representative', 'order_user_representative', ['order_id', 'user_representative_id']);
    	$this->addForeignKey('fk_order_user_representative_order', 'order_user_representative', 'order_id', 'order', 'id');
    	$this->addForeignKey('fk_order_user_representative_user', 'order_user_representative', 'user_representative_id', 'user', 'id');
    	
    	$representatives = (new Query())->select(['o.id', 'o.user_representative_id'])
    			->from('order o')
    			->where('o.user_representative_id IS NOT NULL')
    			->orderBy(['o.id' => SORT_ASC])
    			->all();
    	
    	if(!empty($representatives)) {
    		foreach($representatives as $rep) {
    			$this->insert('order_user_representative', [
    					'order_id' => intval($rep['id']), 'user_representative_id' => intval($rep['user_representative_id']),
    			]);
    		}
    	}
    	
    	$this->dropForeignKey('fk_order_user_representative', 'order');
    	$this->dropColumn('order', 'user_representative_id');
    }

    public function safeDown() {
        $this->addColumn('order', 'user_representative_id', 'INT(11) NULL DEFAULT NULL AFTER company_id');
        $this->addForeignKey('fk_order_user_representative', 'order', 'user_representative_id', 'user', 'id');
        
        $representatives = (new Query())->select(['our.order_id', 'our.user_representative_id'])
		        ->from('order_user_representative our')
		        ->orderBy(['our.order_id' => SORT_ASC])
		        ->all();
         
        if(!empty($representatives)) {
        	foreach($representatives as $rep) {
        		$this->update('order', ['user_representative_id' => intval($rep['user_representative_id'])], ['id' => intval($rep['order_id'])]);
        	}
        }
        
        $this->dropTable('order_user_representative');
    }

}
