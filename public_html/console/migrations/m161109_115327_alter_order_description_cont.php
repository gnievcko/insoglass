<?php

use yii\db\Schema;
use yii\db\Migration;

class m161109_115327_alter_order_description_cont extends Migration {

    public function up() {
		$this->addColumn('order', 'description_cont', 'TEXT NULL DEFAULT NULL AFTER description'); 
    }

    public function down() {
        $this->dropColumn('order', 'description_cont');
    }

}
