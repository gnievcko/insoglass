<?php

use yii\db\Schema;
use yii\db\Migration;

class m171016_120749_alter_offered_product_requirement extends Migration {

    public function safeUp() {
        $this->addColumn('offered_product_requirement', 'unit', 'VARCHAR(10) NULL DEFAULT NULL');
        $this->addColumn('offered_product_requirement', 'note', 'VARCHAR(1024) NULL DEFAULT NULL');
    }

    public function safeDown() {
        $this->dropColumn('offered_product_requirement', 'note');
        $this->dropColumn('offered_product_requirement', 'unit');
    }

}
