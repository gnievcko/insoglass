<?php

use yii\db\Schema;
use yii\db\Migration;
use common\models\ar\DocumentType;
use common\models\ar\DocumentSection;
use common\models\ar\DocumentTypeSection;
use common\documents\sections\SectionsMapper;

class m161121_094403_sales_result_report extends Migration {

    public function up() {
		$footerId = DocumentSection::find()->where(['symbol' => SectionsMapper::COMPANY_FOOTER])->one()->id;
    	$headerId = DocumentSection::find()->where(['symbol' => SectionsMapper::COMPANY_SHORT_HEADER])->one()->id;
    	$this->insert('document_type', [
    			'symbol' => 'SALES_RESULT_REPORT',
    			'document_section_footer_id' => $footerId,
    			'document_section_header_id' => $headerId,
    			'margin_top' => 50,
    			'margin_bottom' => 50,
    			'margin_left' => 15,
    			'margin_right' => 15,
    			'format' => 'A4',
    			'orientation' => 'P'
    	]);
    	 
    	$documentTypeId = $this->db->getLastInsertID();
    	$this->insert('document_section', [ 'symbol' => 'SALES_RESULT_REPORT']);
    	$documentSectionId = $this->db->getLastInsertID();
    	$this->insert('document_type_section', [
    			'document_type_id' => $documentTypeId,
    			'document_section_id' => $documentSectionId,
    			'sequence_number' => 1,
    	]);
    }

    public function down() {
        $documentTypeId = DocumentType::find()->where(['symbol' => 'SALES_RESULT_REPORT'])->one()->id;
		$documentSectionId = DocumentSection::find()->where(['symbol' => 'SALES_RESULT_REPORT'])->one()->id;
		DocumentTypeSection::deleteAll(['document_type_id' => $documentTypeId]);
		DocumentSection::deleteAll(['id'=> $documentSectionId]);
		DocumentType::deleteAll(['id' => $documentTypeId]);
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp() {
    }

    public function safeDown() {
    }
    */
}
