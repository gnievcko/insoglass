<?php

use yii\db\Schema;
use yii\db\Migration;

class m160718_094037_alter_file_temporary_storage extends Migration {

    public function up() {
		$this->renameColumn('file_temporary_storage', 'file_path', 'file_hash');
    	$this->alterColumn('user', 'date_creation', 'TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP');
    }

    public function down() {
		$this->renameColumn('file_temporary_storage', 'file_hash', 'file_path');
    }
}
