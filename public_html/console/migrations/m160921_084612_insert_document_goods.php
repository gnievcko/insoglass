<?php

use yii\db\Schema;
use yii\db\Migration;
use common\models\ar\DocumentTypeSection;
use common\models\ar\DocumentType;
use common\models\ar\DocumentSection;

class m160921_084612_insert_document_goods extends Migration {

    public function up() {
    	$headerSectionId = DocumentSection::find()->where(['symbol' => 'COMPANY_HEADER'])->one()->id;
    	$footerSectionId = DocumentSection::find()->where(['symbol' => 'COMPANY_FOOTER'])->one()->id;
    	
    	$this->insert('document_section', ['symbol' => 'GOODS_RECEIVED_TITLE']);
    	$goodsReceivedTitleId = $this->db->getLastInsertID();
    	$this->insert('document_section', ['symbol' => 'GOODS_RECEIVED_PRODUCTS']);
    	$goodsReceivedProductsId = $this->db->getLastInsertID();
    	$this->insert('document_section', ['symbol' => 'GOODS_ISSUED_TITLE']);
    	$goodsIssuedTitleId = $this->db->getLastInsertID();
    	$this->insert('document_section', ['symbol' => 'GOODS_ISSUED_PRODUCTS']);
    	$goodsIssuedProductsId = $this->db->getLastInsertID();
    	
    	$this->insert('document_type', [
    			'symbol' => 'GOODS_RECEIVED',
    			'document_section_header_id' => $headerSectionId,
    			'document_section_footer_id' => $footerSectionId,
    			'margin_top' => 70,
    			'margin_bottom' => 50,
    			'margin_left'=> 15,
    			'margin_right' => 15,
    			'format' => 'A4',
    			'orientation' => 'P',
    	]);
    	$documentTypeId = $this->db->getLastInsertId();
    	$this->insert('document_type_section', [
    			'document_type_id' => $documentTypeId,
				'document_section_id' => $goodsReceivedTitleId,
				'sequence_number' => 1,
		]);
		$this->insert('document_type_section', [
				'document_type_id' => $documentTypeId,
    			'document_section_id' => $goodsReceivedProductsId,
    			'sequence_number' => 2,
    	]);
    	 
		$this->insert('document_type', [
				'symbol' => 'GOODS_ISSUED',
				'document_section_header_id' => $headerSectionId,
				'document_section_footer_id' => $footerSectionId,
				'margin_top' => 70,
				'margin_bottom' => 50,
				'margin_left'=> 15,
				'margin_right' => 15,
				'format' => 'A4',
				'orientation' => 'P',
		]);
		$documentTypeId = $this->db->getLastInsertId();
		$this->insert('document_type_section', [
				'document_type_id' => $documentTypeId,
				'document_section_id' => $goodsIssuedTitleId,
				'sequence_number' => 1,
		]);
		$this->insert('document_type_section', [
				'document_type_id' => $documentTypeId,
				'document_section_id' => $goodsIssuedProductsId,
				'sequence_number' => 2,
		]);
    }

    public function down() {
    	$documentType1Id = DocumentType::find()->where(['symbol' => 'GOODS_RECEIVED'])->one()->id;
    	$documentType2Id = DocumentType::find()->where(['symbol' => 'GOODS_ISSUED'])->one()->id;
    	
        DocumentTypeSection::deleteAll(['document_type_id' => [$documentType1Id, $documentType2Id]]);
        DocumentType::deleteAll(['id' => [$documentType1Id, $documentType2Id]]);
        DocumentSection::deleteAll(['symbol' => ['GOODS_RECEIVED_PRODUCTS', 'GOODS_RECEIVED_TITLE', 
        		'GOODS_ISSUED_PRODUCTS', 'GOODS_ISSUED_TITLE']]);
    }
}
