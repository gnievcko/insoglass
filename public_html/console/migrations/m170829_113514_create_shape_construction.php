<?php

use yii\db\Schema;
use yii\db\Migration;

class m170829_113514_create_shape_construction extends Migration {

    public function up() {
        $tableOptions = null;
        if($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }
      
        $this->createTable('shape', [
            'id' => $this->primaryKey(11),
            'symbol' => $this->string(32)->notNull()->unique(),
        ], $tableOptions);
        
        $this->createTable('shape_translation', [
            'shape_id' => $this->integer(11),
            'language_id' => $this->integer(11),
            'name' => $this->string(256)->notNull(),
        ], $tableOptions);
        
        $this->addPrimaryKey('pk_shape_translation', 'shape_translation', ['shape_id', 'language_id']);
        $this->addForeignKey('fk_shape_translation_language', 'shape_translation', 'language_id', 'language', 'id');
        $this->addForeignKey('fk_shape_translation_shape', 'shape_translation', 'shape_id', 'shape', 'id');
        
        $this->createTable('construction', [
            'id' => $this->primaryKey(11),
            'symbol' => $this->string(32)->notNull()->unique(),
        ], $tableOptions);
        
        $this->createTable('construction_translation', [
            'construction_id' => $this->integer(11),
            'language_id' => $this->integer(11),
            'name' => $this->string(256)->notNull(),
        ], $tableOptions);
        
        $this->addPrimaryKey('pk_construction_translation', 'construction_translation', ['construction_id', 'language_id']);
        $this->addForeignKey('fk_construction_translation_language', 'construction_translation', 'language_id', 'language', 'id');
        $this->addForeignKey('fk_construction_translation_construction', 'construction_translation', 'construction_id', 'construction', 'id');

        $this->addColumn('order_offered_product', 'shape_id', $this->integer(11));
        $this->addColumn('order_offered_product', 'construction_id', $this->integer(11));
        
        $this->addForeignKey('fk_order_offered_product_construction', 'order_offered_product', 'construction_id', 'construction', 'id');
        $this->addForeignKey('fk_order_offered_product_shape', 'order_offered_product', 'shape_id', 'shape', 'id');
    }

    public function down() {
        $this->dropForeignKey('fk_order_offered_product_construction', 'order_offered_product');
        $this->dropForeignKey('fk_order_offered_product_shape', 'order_offered_product');
        $this->dropColumn('order_offered_product', 'shape_id');
        $this->dropColumn('order_offered_product', 'construction_id');   
        
        $this->dropForeignKey('fk_construction_translation_language', 'construction_translation');
        $this->dropForeignKey('fk_construction_translation_construction', 'construction_translation');
        $this->dropTable('construction_translation');
        $this->dropTable('construction');
                
        $this->dropForeignKey('fk_shape_translation_language', 'shape_translation');
        $this->dropForeignKey('fk_shape_translation_shape', 'shape_translation');
        $this->dropTable('shape_translation');
        $this->dropTable('shape');

    }

}
