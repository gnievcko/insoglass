<?php

use yii\db\Schema;
use yii\db\Migration;
use common\models\ar\DocumentType;
use common\models\ar\DocumentSection;
use common\documents\sections\SectionsMapper;

class m161211_095915_update_offer_details extends Migration {

    public function safeUp() {
    	$offerDetailsId = DocumentType::find()->where(['symbol' => \common\documents\DocumentType::OFFER_DETAILS])->one()->id;
    	$offerDetailsSummaryId = DocumentSection::find()->where(['symbol' => SectionsMapper::OFFER_DETAILS_SUMMARY])->one()->id;
    	
		$this->insert('document_section', ['symbol' => 'OFFER_DETAILS_CUSTOM_DATA']);
		$documentSectionId = $this->db->getLastInsertID();
		$this->update('document_type_section', ['sequence_number' => 4], ['document_type_id' => $offerDetailsId, 'document_section_id' => $offerDetailsSummaryId]);
		$this->insert('document_type_section', ['document_type_id' => $offerDetailsId, 'document_section_id' => $documentSectionId, 'sequence_number' => 3]);
    }

    public function safeDown() {
    	$offerDetailsId = DocumentType::find()->where(['symbol' => \common\documents\DocumentType::OFFER_DETAILS])->one()->id;
        $offerDetailsCustomDataId = DocumentSection::find()->where(['symbol' => 'OFFER_DETAILS_CUSTOM_DATA'])->one()->id;
        $offerDetailsSummaryId = DocumentSection::find()->where(['symbol' => SectionsMapper::OFFER_DETAILS_SUMMARY])->one()->id;
        
        $this->delete('document_type_section', ['document_type_id' => $offerDetailsId, 'document_section_id' => $offerDetailsCustomDataId]);
        $this->update('document_type_section', ['sequence_number' => 3], ['document_type_id' => $offerDetailsId, 'document_section_id' => $offerDetailsSummaryId]);
        $this->delete('document_section', ['id' => $offerDetailsCustomDataId]);
    }
}
