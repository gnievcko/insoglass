<?php

use yii\db\Schema;
use yii\db\Migration;

class m160812_092010_alter_warehouse_delivery extends Migration {

    public function up() {
        $this->addColumn('warehouse_delivery', 'is_active', 'BOOLEAN NOT NULL DEFAULT 1');
    }

    public function down() {
        $this->dropColumn('warehouse_delivery', 'is_active');
    }
}
