<?php

use yii\db\Schema;
use yii\db\Migration;

class m161005_075657_alter_user extends Migration {

    public function up() {
		$this->alterColumn('user', 'date_employment', 'TIMESTAMP NULL DEFAULT NULL AFTER is_active');
		$this->addColumn('company', 'note', 'VARCHAR(2048) NULL DEFAULT NULL AFTER address_postal_id');
		$this->alterColumn('user', 'email', 'VARCHAR(64) NOT NULL COLLATE "utf8_bin"');
    }

    public function down() {
    	$this->alterColumn('user', 'email', 'VARCHAR(64) NOT NULL COLLATE "utf8_unicode_ci"');
		$this->dropColumn('company', 'note');
    	$this->alterColumn('user', 'date_employment', 'TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP AFTER is_active');
    }

}
