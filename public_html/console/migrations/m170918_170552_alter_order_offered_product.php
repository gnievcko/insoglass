<?php

use yii\db\Schema;
use yii\db\Migration;
use common\models\ar\OrderOfferedProduct;

class m170918_170552_alter_order_offered_product extends Migration {

    public function safeUp() {
        $model = new OrderOfferedProduct();
        if(!$model->hasAttribute('remarks')) {
            $this->renameColumn('order_offered_product', 'remark', 'remarks');
        }
        if(!$model->hasAttribute('remarks2')) {
            $this->renameColumn('order_offered_product', 'remark2', 'remarks2');
        }
    }

    public function safeDown() {
        
    }
}
