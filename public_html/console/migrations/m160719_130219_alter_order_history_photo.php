<?php

use yii\db\Schema;
use yii\db\Migration;

class m160719_130219_alter_order_history_photo extends Migration {

    public function up() {
        $this->addColumn('order_history_photo', 'url_thumbnail', $this->string(256)->notNull());
    }

    public function down() {
        $this->dropColumn('order_history_photo', 'url_thumbnail');
    }
}
