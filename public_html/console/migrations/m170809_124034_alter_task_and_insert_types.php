<?php

use yii\db\Schema;
use yii\db\Migration;
use yii\db\Query;
use common\models\ar\TaskTypeTranslation;
use common\models\ar\TaskType;
use common\models\ar\OrderPriorityTranslation;
use common\models\ar\OrderPriority;

class m170809_124034_alter_task_and_insert_types extends Migration {

    public function safeUp() {
        $languageId = (new Query())->select(['id'])->from('language')->where(['symbol' => 'pl'])->scalar();
        
        $this->insert('order_priority', [
            'symbol' => 'medium',
            'priority' => 20,
        ]);
        $orderPriorityId = $this->db->getLastInsertID();
        $this->insert('order_priority_translation', [
            'order_priority_id' => $orderPriorityId,
            'language_id' => $languageId,
            'name' => 'Normalny',
        ]);
        
        $this->insert('order_priority', [
            'symbol' => 'high',
            'priority' => 30,
        ]);
        $orderPriorityId = $this->db->getLastInsertID();
        $this->insert('order_priority_translation', [
            'order_priority_id' => $orderPriorityId,
            'language_id' => $languageId,
            'name' => 'Wysoki',
        ]);
        
        $this->addColumn('task_type', 'is_visible', 'TINYINT(1) NULL DEFAULT NULL');
        $this->update('task_type', ['is_visible' => 1]);
        $this->alterColumn('task_type', 'is_visible', 'TINYINT(1) NOT NULL');
        
        $this->insert('task_type', [
            'symbol' => 'production',
            'priority' => 40,
            'is_visible' => 0,
        ]);
        $taskTypeId = $this->db->getLastInsertID();
        $this->insert('task_type_translation', [
            'task_type_id' => $taskTypeId,
            'language_id' => $languageId,
            'name' => 'Zadanie produkcyjne',
        ]);
    }

    public function safeDown() {
        $taskTypeIds = array_column((new Query())->select(['tt.id'])->from('task_type tt')->where(['tt.symbol' => 'production'])->all(), 'id');
        TaskTypeTranslation::deleteAll(['task_type_id' => $taskTypeIds]);
        TaskType::deleteAll(['id' => $taskTypeIds]);
        
        $this->dropColumn('task_type', 'is_visible');
        
        $orderPriorityIds = array_column((new Query())->select(['op.id'])->from('order_priority op')->where(['op.symbol' => ['medium', 'high']])->all(), 'id');
        OrderPriorityTranslation::deleteAll(['order_priority_id' => $orderPriorityIds]);
        OrderPriority::deleteAll(['id' => $orderPriorityIds]);
    }
}
