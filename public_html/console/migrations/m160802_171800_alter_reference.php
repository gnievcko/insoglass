<?php

use yii\db\Schema;
use yii\db\Migration;
use common\models\ar\ProductStatusReference;
use common\models\ar\Language;

class m160802_171800_alter_reference extends Migration {

    public function up() {
    	$tableOptions = null;
    	if($this->db->driverName === 'mysql') {
    		$tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
    	}
    	
		$this->addColumn('product_status_reference', 'is_required', 'TINYINT(1) NOT NULL DEFAULT 1');
		$this->addColumn('task_type_reference', 'is_required', 'TINYINT(1) NOT NULL DEFAULT 1');
		
		$this->createTable('product_status_reference_translation', [
				'reference_id' => $this->integer(11),
				'language_id' => $this->integer(11),
				'name' => $this->string(512)->notNull(),
		], $tableOptions);
		
		$this->addPrimaryKey('pk_product_status_reference_translation', 'product_status_reference_translation', ['reference_id', 'language_id']);
		$this->addForeignKey('fk_product_status_reference_translation_reference', 'product_status_reference_translation', 'reference_id', 'product_status_reference', 'id');
		$this->addForeignKey('fk_product_status_reference_translation_language', 'product_status_reference_translation', 'language_id', 'language', 'id');
		
		$this->createTable('task_type_reference_translation', [
				'reference_id' => $this->integer(11),
				'language_id' => $this->integer(11),
				'name' => $this->string(512)->notNull(),
		], $tableOptions);
		
		$this->addPrimaryKey('pk_task_type_reference_translation', 'task_type_reference_translation', ['reference_id', 'language_id']);
		$this->addForeignKey('fk_task_type_reference_translation_reference', 'task_type_reference_translation', 'reference_id', 'task_type_reference', 'id');
		$this->addForeignKey('fk_task_type_reference_translation_language', 'task_type_reference_translation', 'language_id', 'language', 'id');
		
		$languageId = Language::find()->where(['symbol' => 'pl'])->one()->id;
		$id = ProductStatusReference::find()->where(['symbol' => 'orderReserved'])->one()->id;
		$this->insert('product_status_reference_translation', ['language_id' => $languageId, 'reference_id' => $id, 'name' => 'Zamówienie']);
		$id = ProductStatusReference::find()->where(['symbol' => 'userIssued'])->one()->id;
		$this->insert('product_status_reference_translation', ['language_id' => $languageId, 'reference_id' => $id, 'name' => 'Odbierający']);
    }

    public function down() {
        $this->dropTable('task_type_reference_translation');
        $this->dropTable('product_status_reference_translation');
        $this->dropColumn('task_type_reference', 'is_required');
        $this->dropColumn('product_status_reference', 'is_required');
    }
}
