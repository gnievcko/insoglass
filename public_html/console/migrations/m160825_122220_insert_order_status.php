<?php

use yii\db\Schema;
use yii\db\Migration;
use common\helpers\Utility;

use common\models\ar\OrderStatus;
use common\models\ar\Language;

class m160825_122220_insert_order_status extends Migration {

    public function up() {
    	$transaction = \Yii::$app->db->beginTransaction();

        $this->execute('SET foreign_key_checks = 0');
        $this->deleteStatuses();
    	$ids = [
    			Utility::ORDER_STATUS_NEW => 0,
    			Utility::ORDER_STATUS_SENT_TO_CLIENT => 1,
    			Utility::ORDER_STATUS_ACCEPTED_BY_CLIENT => 2,
    			Utility::ORDER_STATUS_REJECTED_BY_CLIENT => 3,
    			Utility::ORDER_STATUS_DEFERRED => 4,
    			Utility::ORDER_STATUS_IN_PROGRESS => 5,
    			Utility::ORDER_STATUS_SUSPENDED => 6,
    			Utility::ORDER_STATUS_COMPLETED => 7,
    			Utility::ORDER_STATUS_INVOICED => 8,
    			Utility::ORDER_STATUS_FINISHED => 9,
    			Utility::ORDER_STATUS_CANCELLED => 10,
    	];
    	
    	$statuses = [
    			[
    					'symbol' => Utility::ORDER_STATUS_NEW,
    					'translation' => \Yii::t('main', 'New'),
    					'transitions' => [
    							Utility::ORDER_STATUS_SENT_TO_CLIENT,
    							Utility::ORDER_STATUS_ACCEPTED_BY_CLIENT,
    							Utility::ORDER_STATUS_REJECTED_BY_CLIENT,
    							Utility::ORDER_STATUS_IN_PROGRESS,
    							Utility::ORDER_STATUS_COMPLETED,
    							Utility::ORDER_STATUS_INVOICED,
    							Utility::ORDER_STATUS_FINISHED,
    							Utility::ORDER_STATUS_CANCELLED,
    					],
    			],
    			[
    					'symbol' => Utility::ORDER_STATUS_SENT_TO_CLIENT,
    					'translation' => \Yii::t('main', 'Sent to client'),
    					'transitions' => [
    							Utility::ORDER_STATUS_ACCEPTED_BY_CLIENT,
    							Utility::ORDER_STATUS_REJECTED_BY_CLIENT,
    							Utility::ORDER_STATUS_DEFERRED,
    							Utility::ORDER_STATUS_IN_PROGRESS,
    							Utility::ORDER_STATUS_COMPLETED,
    							Utility::ORDER_STATUS_INVOICED,
    							Utility::ORDER_STATUS_FINISHED,
    							Utility::ORDER_STATUS_CANCELLED,
    					],
    			],
    			[
    					'symbol' => Utility::ORDER_STATUS_ACCEPTED_BY_CLIENT,
    					'translation' => \Yii::t('main', 'Accepted by client'),
    					'transitions' => [
    							Utility::ORDER_STATUS_DEFERRED,
    							Utility::ORDER_STATUS_IN_PROGRESS,
    							Utility::ORDER_STATUS_COMPLETED,
    							Utility::ORDER_STATUS_INVOICED,
    							Utility::ORDER_STATUS_FINISHED,
    							Utility::ORDER_STATUS_CANCELLED,
    					],
    			],
    			[
		    			'symbol' => Utility::ORDER_STATUS_REJECTED_BY_CLIENT,
		    			'translation' => \Yii::t('main', 'Rejected by client'),
    					
    			],
    			[
    					'symbol' => Utility::ORDER_STATUS_DEFERRED,
    					'translation' => \Yii::t('main', 'Deferred'),
    					'transitions' => [
    							Utility::ORDER_STATUS_ACCEPTED_BY_CLIENT,
    							Utility::ORDER_STATUS_REJECTED_BY_CLIENT,
    							Utility::ORDER_STATUS_IN_PROGRESS,
    							Utility::ORDER_STATUS_SUSPENDED,
    							Utility::ORDER_STATUS_FINISHED,
    							Utility::ORDER_STATUS_CANCELLED,
    					],
    			],
    			[
    					'symbol' => Utility::ORDER_STATUS_IN_PROGRESS,
    					'translation' => \Yii::t('main', 'In progress'),
    					'transitions' => [
    							Utility::ORDER_STATUS_DEFERRED,
    							Utility::ORDER_STATUS_SUSPENDED,
    							Utility::ORDER_STATUS_COMPLETED,
    							Utility::ORDER_STATUS_INVOICED,
    							Utility::ORDER_STATUS_FINISHED,
    							Utility::ORDER_STATUS_CANCELLED,
    					],
    			],
    			[
    					'symbol' => Utility::ORDER_STATUS_SUSPENDED,
    					'translation' => \Yii::t('main', 'Suspended'),
    					'transitions' => [
    							Utility::ORDER_STATUS_IN_PROGRESS,
    							Utility::ORDER_STATUS_INVOICED,
    							Utility::ORDER_STATUS_FINISHED,
    							Utility::ORDER_STATUS_CANCELLED,
    					],
    			],
    			[
    					'symbol' => Utility::ORDER_STATUS_COMPLETED,
    					'translation' => \Yii::t('main', 'Completed'),
    					'transitions' => [
    							Utility::ORDER_STATUS_IN_PROGRESS,
    							Utility::ORDER_STATUS_INVOICED,
    							Utility::ORDER_STATUS_FINISHED,
    							Utility::ORDER_STATUS_CANCELLED,
    					],
    			],
    			[
		    			'symbol' => Utility::ORDER_STATUS_INVOICED,
		    			'translation' => \Yii::t('main', 'Invoiced'),
    					'transitions' => [
    							Utility::ORDER_STATUS_FINISHED,
    							Utility::ORDER_STATUS_CANCELLED,
    					],
    			],
    			[
		    			'symbol' => Utility::ORDER_STATUS_FINISHED,
		    			'translation' => \Yii::t('main', 'Finished'),
    			],
    			[
    					'symbol' => Utility::ORDER_STATUS_CANCELLED,
    					'translation' => \Yii::t('main', 'Cancelled'),
    			],	
    	];
    	
    	$this->insert('order_status', [
		    	 'symbol' => $statuses[0]['symbol'],
		    	 'is_order' => true,
		    	 'is_visible' => true,
		    	 'is_visible_for_client' => false,
			   	 'is_terminal' => false,
    	]);
    	$statuses[0]['id'] = \Yii::$app->db->getLastInsertID();

    	$this->insert('order_status', [
    			'symbol' => $statuses[1]['symbol'],
    			'is_order' => false,
    			'is_visible' => true,
    			'is_visible_for_client' => false,
    			'is_terminal' => false,
    			]);
    	$statuses[1]['id'] = \Yii::$app->db->getLastInsertID();
    	
    	$this->insert('order_status', [
    			'symbol' => $statuses[2]['symbol'],
    			'is_order' => false,
    			'is_visible' => true,
    			'is_visible_for_client' => false,
    			'is_terminal' => false,
    	]);
    	$statuses[2]['id'] = \Yii::$app->db->getLastInsertID();
    	
		$this->insert('order_status', [
    			'symbol' => $statuses[3]['symbol'],
    			'is_order' => false,
    			'is_visible' => true,
    			'is_visible_for_client' => false,
    			'is_terminal' => true,
		]);
		$statuses[3]['id'] = \Yii::$app->db->getLastInsertID();
		
		$this->insert('order_status', [
				'symbol' => $statuses[4]['symbol'],
				'is_order' => false,
				'is_visible' => true,
				'is_visible_for_client' => false,
				'is_terminal' => false,
		]);
		$statuses[4]['id'] = \Yii::$app->db->getLastInsertID();
		
		$this->insert('order_status', [
				'symbol' => $statuses[5]['symbol'],
				'is_order' => false,
				'is_visible' => true,
				'is_visible_for_client' => false,
				'is_terminal' => false,
		]);
		$statuses[5]['id'] = \Yii::$app->db->getLastInsertID();
		
		$this->insert('order_status', [
				'symbol' => $statuses[6]['symbol'],
				'is_order' => false,
				'is_visible' => true,
				'is_visible_for_client' => false,
				'is_terminal' => false,
		]);
		$statuses[6]['id'] = \Yii::$app->db->getLastInsertID();
		
		$this->insert('order_status', [
				'symbol' => $statuses[7]['symbol'],
				'is_order' => false,
				'is_visible' => true,
				'is_visible_for_client' => false,
				'is_terminal' => true,
		]);
		$statuses[7]['id'] = \Yii::$app->db->getLastInsertID();
		
		$this->insert('order_status', [
				'symbol' => $statuses[8]['symbol'],
				'is_order' => false,
				'is_visible' => true,
				'is_visible_for_client' => false,
				'is_terminal' => false,
		]);
		$statuses[8]['id'] = \Yii::$app->db->getLastInsertID();
		
		$this->insert('order_status', [
				'symbol' => $statuses[9]['symbol'],
				'is_order' => false,
				'is_visible' => true,
				'is_visible_for_client' => false,
				'is_terminal' => true,
		]);
		$statuses[9]['id'] = \Yii::$app->db->getLastInsertID();
		
		$this->insert('order_status', [
				'symbol' => $statuses[10]['symbol'],
				'is_order' => false,
				'is_visible' => true,
				'is_visible_for_client' => false,
				'is_terminal' => true,
		]);
		$statuses[10]['id'] = \Yii::$app->db->getLastInsertID();
    	
		$languageId = Language::find()->where(['symbol' => \Yii::$app->language])->one()->id;
		
		foreach($statuses as $status) {
			$this->insert('order_status_translation', [
					'order_status_id' => $status['id'],
					'language_id' => $languageId,
					'name' => $status['translation'],
			]);
		}
		
		$a = [];
		foreach($statuses as $status) {
			if(!empty($status['transitions'])) {
				$t = $status['transitions'];
				$id = $status['id'];
				$a = array_filter($ids,
    				function ($key) use ($t) {
        				return in_array($key, $t);
   					 },ARRAY_FILTER_USE_KEY);
				
				\Yii::$app->db->createCommand()->batchInsert('order_status_transition',
					['order_status_predecessor_id', 'order_status_successor_id'],
					array_map(function($transition) use ($statuses, $id){
						return ['order_status_predecessor_id' => $id , 'order_status_successor_id' => $statuses[$transition]['id']];
					},  $a))->execute();
			}
		};

        $this->update('order_history', [
            'order_status_id' => (new \yii\db\Query())->select('id')->from('order_status')->where(['symbol' => Utility::ORDER_STATUS_NEW])->one()['id']
        ]);

        $this->execute('SET foreign_key_checks = 1');
		
    	$transaction->commit();
    }

    private function deleteStatuses() {
    	$symbols = [
    			Utility::ORDER_STATUS_NEW,
    			Utility::ORDER_STATUS_SENT_TO_CLIENT,
    			Utility::ORDER_STATUS_ACCEPTED_BY_CLIENT,
    			Utility::ORDER_STATUS_REJECTED_BY_CLIENT,
    			Utility::ORDER_STATUS_DEFERRED,
    			Utility::ORDER_STATUS_IN_PROGRESS,
    			Utility::ORDER_STATUS_SUSPENDED,
    			Utility::ORDER_STATUS_COMPLETED,
    			Utility::ORDER_STATUS_INVOICED,
    			Utility::ORDER_STATUS_FINISHED ,
    			Utility::ORDER_STATUS_CANCELLED,
    	];
    	
    	$ids = array_column(OrderStatus::find()->where(['symbol' => $symbols])->all(), 'id');
    	$this->delete('order_status_transition', ['order_status_predecessor_id' => $ids]);
    	$this->delete('order_status_translation', ['order_status_id' => $ids]);
    	$this->delete('order_status', ['id' => $ids]);
    }
	
    public function down() {
        return false;
    }

}
