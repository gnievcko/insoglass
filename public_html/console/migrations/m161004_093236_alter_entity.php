<?php

use yii\db\Schema;
use yii\db\Migration;

class m161004_093236_alter_entity extends Migration {

    public function up() {
		$this->alterColumn('entity', 'krs_number', 'VARCHAR(150) NOT NULL AFTER vat_identification_number');
		$this->alterColumn('entity', 'account_number', 'VARCHAR(150) NOT NULL AFTER fax');
		$this->alterColumn('entity', 'share_capital', 'DECIMAL(17,2) NULL DEFAULT NULL AFTER bank_name');
    }

    public function down() {
    	$this->alterColumn('entity', 'share_capital', 'DECIMAL(17,2) NOT NULL AFTER bank_name');
    	$this->alterColumn('entity', 'account_number', 'VARCHAR(32) NOT NULL AFTER fax');
    	$this->alterColumn('entity', 'krs_number', 'VARCHAR(32) NOT NULL AFTER vat_identification_number');
    	
    	Yii::$app->security->generateRandomString();
    }

}
