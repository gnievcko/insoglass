<?php

use yii\db\Schema;
use yii\db\Migration;
use common\models\ar\RoleTranslation;
use common\models\ar\Role;
use common\models\ar\Language;

class m160704_062111_init_advanced_tables_orders extends Migration {

    public function up() {
    	$tableOptions = null;
    	if($this->db->driverName === 'mysql') {
    		$tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
    	}
    	
    	$this->alterUsers($tableOptions);
    	$this->createOfferedProduct($tableOptions);	
    	$this->createOrder($tableOptions);
    	$this->createOrderStatus($tableOptions);
    	$this->createOrderAdditional($tableOptions);
    }

    public function down() {
     	// raczej nie bedzie potrzebne   
    }
    
    private function alterUsers($tableOptions) {
    	$this->createTable('company', [
    			'id' => $this->primaryKey(11),
    			'parent_company_id' => $this->integer(11),
    			'name' => $this->string(128)->notNull(),
    			'email' => $this->string(64),
    			'phone1' => $this->string(32),
    			'phone2' => $this->string(32),
    			'address_id' => $this->integer(11),
    			'address_postal_id' => $this->integer(11),
    			'language_id' => $this->integer(11),
    			'currency_id' => $this->integer(11),
    			'is_artificial' => $this->boolean()->notNull()->defaultValue(0),
    			'is_active' => $this->boolean()->notNull()->defaultValue(1),
    			'date_creation' => $this->timestamp()->notNull(),
    			'date_modification' => $this->timestamp()->defaultValue(null),
    	], $tableOptions);
    	
    	$this->addForeignKey('fk_company_parent', 'company', 'parent_company_id', 'company', 'id');
    	$this->addForeignKey('fk_company_language', 'company', 'language_id', 'language', 'id');
    	$this->addForeignKey('fk_company_currency', 'company', 'currency_id', 'currency', 'id');
    	$this->addForeignKey('fk_company_address', 'company', 'address_id', 'address', 'id');
    	$this->addForeignKey('fk_company_address_postal', 'company', 'address_postal_id', 'address', 'id');
    	$this->alterColumn('company', 'date_creation', 'TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP');
    	$this->alterColumn('company', 'date_modification', 'TIMESTAMP NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP');
    	
    	$this->dropColumn('user', 'phone');
    	$this->addColumn('user', 'phone1', 'VARCHAR(32) NULL DEFAULT NULL AFTER last_name');
    	$this->addColumn('user', 'phone2', 'VARCHAR(32) NULL DEFAULT NULL AFTER phone1');
    	$this->addColumn('user', 'note', 'VARCHAR(1024) NULL DEFAULT NULL AFTER address_postal_id');
    	$this->addColumn('user', 'last_login', 'TIMESTAMP NULL DEFAULT NULL AFTER is_active');
    	$this->addColumn('user', 'company_id', 'INT(11) NULL DEFAULT NULL AFTER id');
    	$this->addForeignKey('fk_user_company', 'user', 'company_id', 'company', 'id');
    	$this->alterColumn('user', 'email', 'VARCHAR(64) NOT NULL');
    	$this->dropForeignKey('fk_user_address', 'user');
    	$this->dropForeignKey('fk_user_address_postal', 'user');
    	$this->dropColumn('user', 'address_id');
    	$this->dropColumn('user', 'address_postal_id');
    	
    	/*$this->createTable('company_representative', [
    			'id' => $this->primaryKey(11),
    			'company_id' => $this->integer(11)->notNull(),
    			'email' => $this->string(64)->notNull()->unique(),
    			'password' => $this->string(64),
    			'first_name' => $this->string(32),
    			'last_name' => $this->string(32),
    			'url_photo' => $this->string(256),
    			'is_active' => $this->boolean()->notNull()->defaultValue(0),
    			'date_creation' => $this->timestamp()->notNull(),
    			'date_modification' => $this->timestamp(),
    	], $tableOptions);
    	
    	$this->createIndex('uk_company_representative_email', 'company_representative', 'email');
    	$this->addForeignKey('fk_company_representative_company', 'company_representative', 'company_id', 'company', 'id');
    	$this->alterColumn('company_representative', 'date_creation', 'TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP');
    	$this->alterColumn('company_representative', 'date_modification', 'TIMESTAMP NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP');*/
    	
    	$this->createTable('user_company_salesman', [
    			'user_id' => $this->integer(11),
    			'company_id' => $this->integer(11)
    	], $tableOptions);
    
    	$this->addPrimaryKey('pk_user_company_salesman', 'user_company_salesman', ['user_id', 'company_id']);
    	$this->addForeignKey('fk_user_company_salesman_user', 'user_company_salesman', 'user_id', 'user', 'id');
    	$this->addForeignKey('fk_user_company_salesman_company', 'user_company_salesman', 'company_id', 'company', 'id');
    	
    	$languageId = Language::find()->where('symbol = :symbol', [':symbol' => 'pl'])->one()->id;
    	
    	$this->insert('role', ['symbol' => 'salesman']);
    	$roleId = $this->db->getLastInsertID();
    	$this->insert('role_translation', [
    			'role_id' => $roleId,
    			'language_id' => $languageId,
    			'name' => 'Sprzedawca'
    	]);
    	
    	$this->insert('role', ['symbol' => 'storekeeper']);
    	$roleId = $this->db->getLastInsertID();
    	$this->insert('role_translation', [
    			'role_id' => $roleId,
    			'language_id' => $languageId,
    			'name' => 'Magazynier'
    	]);
    	
    	$this->insert('role', ['symbol' => 'main_storekeeper']);
    	$roleId = $this->db->getLastInsertID();
    	$this->insert('role_translation', [
    			'role_id' => $roleId,
    			'language_id' => $languageId,
    			'name' => 'Główny magazynier'
    	]);
    }
    
    private function createOfferedProduct($tableOptions) {
	    $this->createTable('offered_product_category', [
	    		'id' => $this->primaryKey(11),
	    		'symbol' => $this->string(32)->notNull()->unique(),
	    		'parent_category_id' => $this->integer(11)
	    ], $tableOptions);
	    
	    $this->addForeignKey('fk_offered_product_category_parent', 'offered_product_category', 'parent_category_id', 'offered_product_category', 'id');
	    
	    $this->createTable('offered_product_category_translation', [
	    		'offered_product_category_id' => $this->integer(11),
	    		'language_id' => $this->integer(11),
	    		'name' => $this->string(128)->notNull(),
	    		'description' => $this->string(1024)
	    ], $tableOptions);
	    
	    $this->addPrimaryKey('pk_offered_product_category_translation', 'offered_product_category_translation', ['offered_product_category_id', 'language_id']);
	    $this->addForeignKey('fk_offered_product_category_translation_language', 'offered_product_category_translation', 'language_id', 'language', 'id');
	    $this->addForeignKey('fk_offered_product_category_translation_category', 'offered_product_category_translation', 'offered_product_category_id', 'offered_product_category', 'id');
    
	    $this->createTable('time_unit', [
	    		'id' => $this->primaryKey(11),
	    		'symbol' => $this->string(32)->notNull()->unique(),
	    		'conversion_minute' => $this->decimal(10,2)->notNull()
	    ], $tableOptions);
	    
	    $this->createTable('time_unit_translation', [
	    		'time_unit_id' => $this->integer(11),
	    		'language_id' => $this->integer(11),
	    		'value' => $this->integer(11),
	    		'name' => $this->string(128)->notNull(),
	    ], $tableOptions);
	    
	    $this->addPrimaryKey('pk_time_unit_translation', 'time_unit_translation', ['time_unit_id', 'language_id', 'value']);
	    $this->addForeignKey('fk_time_unit_translation_language', 'time_unit_translation', 'language_id', 'language', 'id');
	    $this->addForeignKey('fk_time_unit_translation_time_unit', 'time_unit_translation', 'time_unit_id', 'time_unit', 'id');
	    
    	$this->createTable('offered_product', [
    			'id' => $this->primaryKey(11),
    			'symbol' => $this->string(32)->notNull()->unique(),
    			'is_active' => $this->boolean()->notNull(),
    			'date_creation' => $this->timestamp()->notNull(),
    	], $tableOptions);
    	
    	$this->alterColumn('offered_product', 'date_creation', 'TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP');
    	
    	$this->createTable('offered_product_translation', [
	    		'offered_product_id' => $this->integer(11),
	    		'language_id' => $this->integer(11),
	    		'name' => $this->string(512)->notNull(),
	    		'description' => $this->string(2048),
    			'price' => $this->decimal(10,2),
    			'currency_id' => $this->integer(11),
    			'expected_time' => $this->integer(10),
    			'time_unit_id' => $this->integer(11),
    			'date_creation' => $this->timestamp()->notNull(),
	    ], $tableOptions);
    	
    	$this->addPrimaryKey('pk_offered_product_translation', 'offered_product_translation', ['offered_product_id', 'language_id']);
    	$this->addForeignKey('fk_offered_product_translation_product', 'offered_product_translation', 'offered_product_id', 'offered_product', 'id');
    	$this->addForeignKey('fk_offered_product_translation_language', 'offered_product_translation', 'language_id', 'language', 'id');
    	$this->addForeignKey('fk_offered_product_translation_currency', 'offered_product_translation', 'currency_id', 'currency', 'id');
    	$this->addForeignKey('fk_offered_product_translation_time_unit', 'offered_product_translation', 'time_unit_id', 'time_unit', 'id');
    	$this->alterColumn('offered_product_translation', 'date_creation', 'TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP');
    	
    	$this->createTable('offered_product_category_set', [
    			'offered_product_id' => $this->integer(11),
    			'offered_product_category_id' => $this->integer(11)
    	], $tableOptions);
    
    	$this->addPrimaryKey('pk_offered_product_category_set', 'offered_product_category_set', ['offered_product_id', 'offered_product_category_id']);
    	$this->addForeignKey('fk_offered_product_category_set_product', 'offered_product_category_set', 'offered_product_id', 'offered_product', 'id');
    	$this->addForeignKey('fk_offered_product_category_set_category', 'offered_product_category_set', 'offered_product_category_id', 'offered_product_category', 'id');
    	
    	$this->createTable('offered_product_version', [
    			'id' => $this->primaryKey(11),
    			'offered_product_id' => $this->integer(11)->notNull(),
    			'symbol' => $this->string(32)->notNull()->unique(),
    			'priority' => $this->integer(5),
    			'is_active' => $this->boolean()->notNull(),
    			'date_creation' => $this->timestamp()->notNull(),
    	], $tableOptions);
    	
    	$this->addForeignKey('fk_offered_product_version_product', 'offered_product_version', 'offered_product_id', 'offered_product', 'id');
    	$this->alterColumn('offered_product_version', 'date_creation', 'TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP');
    	
    	$this->createTable('offered_product_version_translation', [
	    		'offered_product_version_id' => $this->integer(11),
	    		'language_id' => $this->integer(11),
	    		'name' => $this->string(128)->notNull(),
	    		'description' => $this->string(512),
	    ], $tableOptions);
    	
    	$this->addPrimaryKey('pk_offered_product_version_translation', 'offered_product_version_translation', ['offered_product_version_id', 'language_id']);
    	$this->addForeignKey('fk_offered_product_version_translation_version', 'offered_product_version_translation', 'offered_product_version_id', 'offered_product_version', 'id');
    	$this->addForeignKey('fk_offered_product_version_translation_language', 'offered_product_version_translation', 'language_id', 'language', 'id');
    	
    	$this->createTable('offered_product_photo', [
    			'id' => $this->primaryKey(11),
    			'offered_product_id' => $this->integer(11)->notNull(),
    			'url_photo' => $this->string(256)->notNull(),
    			'priority' => $this->integer(5),
    			'date_creation' => $this->timestamp()->notNull(),
    	], $tableOptions);
    	 
    	$this->addForeignKey('fk_offered_product_photo_product', 'offered_product_photo', 'offered_product_id', 'offered_product', 'id');
    	$this->alterColumn('offered_product_photo', 'date_creation', 'TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP');
    	 
    	$this->createTable('offered_product_photo_translation', [
    			'offered_product_photo_id' => $this->integer(11),
    			'language_id' => $this->integer(11),
    			'description' => $this->string(512),
    	], $tableOptions);
    	 
    	$this->addPrimaryKey('pk_offered_product_photo_translation', 'offered_product_photo_translation', ['offered_product_photo_id', 'language_id']);
    	$this->addForeignKey('fk_offered_product_photo_translation_version', 'offered_product_photo_translation', 'offered_product_photo_id', 'offered_product_photo', 'id');
    	$this->addForeignKey('fk_offered_product_photo_translation_language', 'offered_product_photo_translation', 'language_id', 'language', 'id');
    }
    
    private function createOrder($tableOptions) {
    	$this->createTable('order_group', [
    			'id' => $this->primaryKey(11),
    			'number' => $this->string(48)->unique(),
    			'date_creation' => $this->timestamp()->notNull(),
    	], $tableOptions);
    	
    	$this->alterColumn('order_group', 'date_creation', 'TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP');
    	
    	$this->createTable('order', [
    			'id' => $this->primaryKey(11),
    			'company_id' => $this->integer(11)->notNull(),
    			'user_representative_id' => $this->integer(11),
    			'order_group_id' => $this->integer(11)->notNull(),
    			'number' => $this->string(48)->unique()->notNull(),
    			'title' => $this->string(256)->notNull(),
    			'description' => $this->text()->notNull(),
    			'order_history_first_id' => $this->integer(11),
    			'order_history_last_id' => $this->integer(11),
    			'is_active' => $this->boolean()->notNull()->defaultValue(1),
    			'date_creation' => $this->timestamp()->notNull(),
    			'date_modification' => $this->timestamp()->defaultValue(null),
    	], $tableOptions);
    	
    	$this->addForeignKey('fk_order_company', 'order', 'company_id', 'company', 'id');
    	$this->addForeignKey('fk_order_user_representative', 'order', 'user_representative_id', 'user', 'id');
    	$this->addForeignKey('fk_order_order_group', 'order', 'order_group_id', 'order_group', 'id');
    	$this->alterColumn('order', 'date_creation', 'TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP');
    	$this->alterColumn('order', 'date_modification', 'TIMESTAMP NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP');
    	
    	$this->createTable('user_order_salesman', [
    			'user_id' => $this->integer(11),
    			'order_id' => $this->integer(11),
    			'is_main' => $this->boolean()->notNull()->defaultValue(1),
    	], $tableOptions);
    
    	$this->addPrimaryKey('pk_user_order_salesman', 'user_order_salesman', ['user_id', 'order_id']);
    	$this->addForeignKey('fk_user_order_salesman_user', 'user_order_salesman', 'user_id', 'user', 'id');
    	$this->addForeignKey('fk_user_order_salesman_company', 'user_order_salesman', 'order_id', 'order', 'id');    	
    }
    
    private function createOrderStatus($tableOptions) {
    	$this->createTable('order_status', [
    			'id' => $this->primaryKey(11),
    			'symbol' => $this->string(32)->notNull()->unique(),
    			'is_order' => $this->boolean()->notNull(),
    	], $tableOptions);
    	
    	$this->createTable('order_status_translation', [
    			'order_status_id' => $this->integer(11),
    			'language_id' => $this->integer(11),
    			'name' => $this->string(128)->notNull()
    	], $tableOptions);
    	 
    	$this->addPrimaryKey('pk_order_status_translation', 'order_status_translation', ['order_status_id', 'language_id']);
    	$this->addForeignKey('fk_order_status_translation_language', 'order_status_translation', 'language_id', 'language', 'id');
    	$this->addForeignKey('fk_order_status_translation_status', 'order_status_translation', 'order_status_id', 'order_status', 'id');
    	
    	$this->createTable('order_status_transition', [
    			'order_status_predecessor_id' => $this->integer(11),
    			'order_status_successor_id' => $this->integer(11),
    	], $tableOptions);
    	
    	$this->addPrimaryKey('pk_order_status_transition', 'order_status_transition', ['order_status_predecessor_id', 'order_status_successor_id']);
    	$this->addForeignKey('fk_order_status_transition_predecessor', 'order_status_transition', 'order_status_predecessor_id', 'order_status', 'id');
    	$this->addForeignKey('fk_order_status_transition_successor', 'order_status_transition', 'order_status_successor_id', 'order_status', 'id');
    	
    	$this->createTable('order_history', [
    			'id' => $this->primaryKey(11),
    			'order_id' => $this->integer(11)->notNull(),
    			'user_id' => $this->integer(11)->notNull(),
    			'order_status_id' => $this->integer(11)->notNull(),
    			'message' => $this->text()->notNull(),
    			'price' => $this->decimal(10,2),
    			'date_deadline' => $this->date(),
    			'is_client_entry' => $this->boolean()->notNull(),
    			'is_answer_required' => $this->boolean()->notNull()->defaultValue(0),
    			'date_creation' => $this->timestamp()->notNull(),
    	], $tableOptions);
    	
    	$this->addForeignKey('fk_order_history_order', 'order_history', 'order_id', 'order', 'id');
    	$this->addForeignKey('fk_order_history_user', 'order_history', 'user_id', 'user', 'id');
    	$this->addForeignKey('fk_order_history_order_status', 'order_history', 'order_status_id', 'order_status', 'id');
    	$this->alterColumn('order_history', 'date_creation', 'TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP');
    	
    	$this->addForeignKey('fk_order_order_history_first', 'order', 'order_history_first_id', 'order_history', 'id');
    	$this->addForeignKey('fk_order_order_history_last', 'order', 'order_history_last_id', 'order_history', 'id');    	
    }
    
    private function createOrderAdditional($tableOptions) {
    	$this->createTable('order_offered_product', [
    			'id' => $this->primaryKey(11),
    			'order_id' => $this->integer(11)->notNull(),
    			'offered_product_id' => $this->integer(11)->notNull(),
    			'order_history_id' => $this->integer(11),
    			'count' => $this->integer(10)->notNull(),
    			'description' => $this->string(1024),
    			'is_active' => $this->boolean()->notNull()->defaultValue(1),
    	], $tableOptions);
    	
    	$this->addForeignKey('fk_order_offered_product_order', 'order_offered_product', 'order_id', 'order', 'id');
    	$this->addForeignKey('fk_order_offered_product_product', 'order_offered_product', 'offered_product_id', 'offered_product', 'id');
    	$this->addForeignKey('fk_order_offered_product_history', 'order_offered_product', 'order_history_id', 'order_history', 'id');
    	
    	$this->createTable('order_attachment', [
    			'id' => $this->primaryKey(11),
    			'order_id' => $this->integer(11)->notNull(),
    			'order_history_id' => $this->integer(11),
    			'user_id' => $this->integer(11)->notNull(),
    			'url_file' => $this->string(256)->notNull(),
    			'description' => $this->string(512),
    			'date_creation' => $this->timestamp()->notNull(),
    	], $tableOptions);
    	
    	$this->addForeignKey('fk_order_attachment_order', 'order_attachment', 'order_id', 'order', 'id');
    	$this->addForeignKey('fk_order_attachment_order_history', 'order_attachment', 'order_history_id', 'order_history', 'id');
    	$this->addForeignKey('fk_order_attachment_user', 'order_attachment', 'user_id', 'user', 'id');
    	$this->alterColumn('order_attachment', 'date_creation', 'TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP');
    	
    	$this->createTable('order_history_photo', [
    			'id' => $this->primaryKey(11),
    			'order_history_id' => $this->integer(11)->notNull(),
    			'order_offered_product_id' => $this->integer(11),
    			'user_id' => $this->integer(11)->notNull(),
    			'url_photo' => $this->string(256)->notNull(),
    			'description' => $this->string(512),
    			'date_creation' => $this->timestamp()->notNull(),
    	], $tableOptions);
    	
    	$this->addForeignKey('fk_order_history_photo_order_history', 'order_history_photo', 'order_history_id', 'order_history', 'id');
    	$this->addForeignKey('fk_order_history_photo_order_product', 'order_history_photo', 'order_offered_product_id', 'order_offered_product', 'id');
    	$this->addForeignKey('fk_order_history_photo_user', 'order_history_photo', 'user_id', 'user', 'id');
    	$this->alterColumn('order_history_photo', 'date_creation', 'TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP');
    }
}
