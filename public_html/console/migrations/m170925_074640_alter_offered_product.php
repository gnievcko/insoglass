<?php

use yii\db\Schema;
use yii\db\Migration;

class m170925_074640_alter_offered_product extends Migration {

    public function up() {
        $this->dropForeignKey('fk_offered_product_muntin', 'offered_product');
        $this->dropColumn('offered_product', 'muntin_id');
        $this->dropForeignKey('fk_offered_product_construction', 'offered_product');
        $this->dropColumn('offered_product', 'construction_id');
        $this->dropColumn('offered_product', 'small_drillhole_count');
        $this->dropColumn('offered_product', 'big_drillhole_count');
        
        $this->dropForeignKey('fk_product_muntin', 'product');
        $this->dropColumn('product', 'muntin_id');
        $this->dropForeignKey('fkproduct_construction', 'product');
        $this->dropColumn('product', 'construction_id');
        $this->dropColumn('product', 'small_drillhole_count');
        $this->dropColumn('product', 'big_drillhole_count');
    }

    public function down() {
       
    }

}
