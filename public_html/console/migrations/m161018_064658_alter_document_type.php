<?php

use yii\db\Schema;
use yii\db\Migration;

class m161018_064658_alter_document_type extends Migration {

    public function safeUp() {
		$this->update('document_section', ['symbol' => 'CUSTOM_PROTOCOL'], ['symbol' => 'CUSTOM_TEXT']);
    }

    public function safeDown() {
        $this->update('document_section', ['symbol' => 'CUSTOM_TEXT'], ['symbol' => 'CUSTOM_PROTOCOL']);
    }
}
