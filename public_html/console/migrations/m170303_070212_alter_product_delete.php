<?php

use yii\db\Migration;

class m170303_070212_alter_product_delete extends Migration {

    public function up() {
		$this->addColumn('offered_product', 'date_deletion', 'TIMESTAMP NULL DEFAULT NULL AFTER is_active');
		$this->addColumn('product', 'date_deletion', 'TIMESTAMP NULL DEFAULT NULL AFTER is_active');
    }

    public function down() {
    	$this->dropColumn('product', 'date_deletion');
    	$this->dropColumn('offered_product', 'date_deletion');
    }

}
