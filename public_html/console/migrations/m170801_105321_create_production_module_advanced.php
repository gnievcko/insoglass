<?php

use yii\db\Schema;
use yii\db\Migration;
use yii\db\Query;
use common\models\ar\RoleTranslation;
use common\models\ar\Role;

class m170801_105321_create_production_module_advanced extends Migration {

    public function safeUp() {
    	$tableOptions = null;
    	if($this->db->driverName === 'mysql') {
    		$tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
    	}
    	
    	$this->dropTable('user_department');
    	$this->createTable('user_department', [
    			'user_id' => $this->integer(11),
    			'department_id' => $this->integer(11),
    	], $tableOptions);
    	 
    	$this->addPrimaryKey('pk_user_department', 'user_department', ['user_id', 'department_id']);
    	$this->addForeignKey('fk_user_department_user', 'user_department', 'user_id', 'user', 'id');
    	$this->addForeignKey('fk_user_department_department', 'user_department', 'department_id', 'department', 'id');
    	
    	$this->dropTable('department_activity');
    	$this->createTable('department_activity', [
    			'department_id' => $this->integer(11),
    			'activity_id' => $this->integer(11),
    	], $tableOptions);
    	
    	$this->addPrimaryKey('pk_department_activity', 'department_activity', ['department_id', 'activity_id']);
    	$this->addForeignKey('fk_department_activity_department', 'department_activity', 'department_id', 'department', 'id');
    	$this->addForeignKey('fk_department_activity_activity', 'department_activity', 'activity_id', 'activity', 'id');
    	
		$languageId = (new Query())->select(['id'])->from('language')->where(['symbol' => 'pl'])->scalar();
		
		$this->insert('role', ['symbol' => 'worker', 'is_visible' => false]);
		$roleId = $this->db->getLastInsertID();
		$this->insert('role_translation', [
				'role_id' => intval($roleId), 				
				'language_id' => intval($languageId),
				'name' => 'Pracownik produkcyjny',
		]);
		
		$this->insert('role', ['symbol' => 'qa_specialist', 'is_visible' => false]);
		$roleId = $this->db->getLastInsertID();
		$this->insert('role_translation', [
				'role_id' => intval($roleId),
				'language_id' => intval($languageId),
				'name' => 'Kontroler jakości',
		]);
		
		$this->createTable('production_path', [
				'id' => $this->primaryKey(11),
				'symbol' => $this->string(32)->notNull()->unique(),
		], $tableOptions);
		 
		$this->createTable('production_path_translation', [
				'production_path_id' => $this->integer(11),
				'language_id' => $this->integer(11),
				'name' => $this->string(256)->notNull(),
		], $tableOptions);
		
		$this->addPrimaryKey('pk_production_path_translation', 'production_path_translation', ['production_path_id', 'language_id']);
		$this->addForeignKey('fk_production_path_translation_language', 'production_path_translation', 'language_id', 'language', 'id');
		$this->addForeignKey('fk_production_path_translation_department', 'production_path_translation', 'production_path_id', 'production_path', 'id');
		 
		$this->createTable('production_path_step', [
				'id' => $this->primaryKey(11),
				'production_path_id' => $this->integer(11)->notNull(),
				'activity_id' => $this->integer(11)->notNull(),
				'step_required_ids' => $this->string(64),
				'step_successor_ids' => $this->string(64),
		], $tableOptions);
		
		$this->addForeignKey('fk_production_path_step_production_path', 'production_path_step', 'production_path_id', 'production_path', 'id');
		$this->addForeignKey('fk_production_path_step_activity', 'production_path_step', 'activity_id', 'activity', 'id');
		
		$this->createTable('product_attribute_type', [
    			'id' => $this->primaryKey(11),
    			'symbol' => $this->string(32)->notNull()->unique(),
				'variable_type' => $this->string(32)->notNull(),
    	], $tableOptions);
    	 
    	$this->createTable('product_attribute_type_translation', [
    			'product_attribute_type_id' => $this->integer(11),
    			'language_id' => $this->integer(11),
    			'name' => $this->string(128)->notNull(),
    	], $tableOptions);
    	 
    	$this->addPrimaryKey('pk_product_attribute_type_translation', 'product_attribute_type_translation', ['product_attribute_type_id', 'language_id']);
    	$this->addForeignKey('fk_product_attribute_type_translation_language', 'product_attribute_type_translation', 'language_id', 'language', 'id');
    	$this->addForeignKey('fk_product_attribute_type_translation_type', 'product_attribute_type_translation', 'product_attribute_type_id', 'product_attribute_type', 'id');
    	 
    	$this->createTable('offered_product_attribute', [
    			'offered_product_id' => $this->integer(11),
    			'product_attribute_type_id' => $this->integer(11),
    			'value' => $this->string(64)->notNull(),
    	], $tableOptions);
    	
    	$this->addPrimaryKey('pk_offered_product_attribute', 'offered_product_attribute', ['offered_product_id', 'product_attribute_type_id']);
    	$this->addForeignKey('fk_offered_product_attribute_product', 'offered_product_attribute', 'offered_product_id', 'offered_product', 'id');
    	$this->addForeignKey('fk_offered_product_attribute_attribute', 'offered_product_attribute', 'product_attribute_type_id', 'product_attribute_type', 'id');
    	    
    	$this->createTable('order_offered_product_attribute', [
    			'order_offered_product_id' => $this->integer(11),
    			'product_attribute_type_id' => $this->integer(11),
    			'value' => $this->string(64)->notNull(),
    	], $tableOptions);
    	 
    	$this->addPrimaryKey('pk_order_offered_product_attribute', 'order_offered_product_attribute', ['order_offered_product_id', 'product_attribute_type_id']);
    	$this->addForeignKey('fk_order_offered_product_attribute_product', 'order_offered_product_attribute', 'order_offered_product_id', 'order_offered_product', 'id');
    	$this->addForeignKey('fk_order_offered_product_attribute_attribute', 'order_offered_product_attribute', 'product_attribute_type_id', 'product_attribute_type', 'id');    	
    	
    	$this->createTable('activity_product_attribute', [
    			'activity_id' => $this->integer(11),
    			'product_attribute_type_id' => $this->integer(11),
    	], $tableOptions);
    	
    	$this->addPrimaryKey('pk_activity_product_attribute', 'activity_product_attribute', ['activity_id', 'product_attribute_type_id']);
    	$this->addForeignKey('fk_activity_product_attribute_product', 'activity_product_attribute', 'activity_id', 'activity', 'id');
    	$this->addForeignKey('fk_activity_product_attribute_attribute', 'activity_product_attribute', 'product_attribute_type_id', 'product_attribute_type', 'id');
    	
    	$this->addColumn('offered_product', 'attribute_value', 'VARCHAR(1024) NULL DEFAULT NULL AFTER date_deletion');
    	$this->addColumn('order_offered_product', 'attribute_value', 'VARCHAR(1024) NULL DEFAULT NULL AFTER unit');
    	$this->addColumn('offered_product', 'production_path_id', 'INT(11) NULL DEFAULT NULL AFTER attribute_value');
    	$this->addForeignKey('fk_offered_product_production_path', 'offered_product', 'production_path_id', 'production_path', 'id');
    	$this->addColumn('order_offered_product', 'production_path_id', 'INT(11) NULL DEFAULT NULL AFTER attribute_value');
    	$this->addForeignKey('fk_order_offered_product_production_path', 'order_offered_product', 'production_path_id', 'production_path', 'id');
    	
    	$this->createTable('offered_product_requirement', [
    			'id' => $this->primaryKey(11),
    			'offered_product_id' => $this->integer(11)->notNull(),
    			'product_required_id' => $this->integer(11)->notNull(),
    			'count' => $this->integer(10)->notNull(),
    	], $tableOptions);
    	
    	$this->addForeignKey('fk_offered_product_requirement_product', 'offered_product_requirement', 'offered_product_id', 'offered_product', 'id');
    	$this->addForeignKey('fk_offered_product_requirement_required', 'offered_product_requirement', 'product_required_id', 'product', 'id');
    	
    	$this->createTable('product_requirement', [
    			'id' => $this->primaryKey(11),
    			'product_id' => $this->integer(11)->notNull(),
    			'product_required_id' => $this->integer(11)->notNull(),
    			'count' => $this->integer(10)->notNull(),
    	], $tableOptions);
    	 
    	$this->addForeignKey('fk_product_requirement_product', 'product_requirement', 'product_id', 'product', 'id');
    	$this->addForeignKey('fk_product_requirement_required', 'product_requirement', 'product_required_id', 'product', 'id');
    	
    	$this->createTable('production_task', [
    			'id' => $this->primaryKey(11),
    			'task_id' => $this->integer(11)->notNull(),
    			'order_offered_product_id' => $this->integer(11)->notNull(),
    			'production_path_step_id' => $this->integer(11)->notNull(),
    			'is_available' => $this->boolean()->notNull()->defaultValue(false),
    			'total_count_made' => $this->integer(10)->notNull()->defaultValue(0),
    			'total_count_failed' => $this->integer(10)->notNull()->defaultValue(0),
    			'date_last_modification' => $this->timestamp()->notNull(),
    	], $tableOptions);
    	
    	$this->alterColumn('production_task', 'date_last_modification', 'TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP');
    	$this->addForeignKey('fk_production_task_task', 'production_task', 'task_id', 'task', 'id');
    	$this->addForeignKey('fk_production_task_order_offered_product', 'production_task', 'order_offered_product_id', 'order_offered_product', 'id');
    	$this->addForeignKey('fk_production_task_production_path_step', 'production_task', 'production_path_step_id', 'production_path_step', 'id');
    	
    	$this->createTable('production_task_history', [
    			'id' => $this->primaryKey(11),
    			'production_task_id' => $this->integer(11)->notNull(),
    			'user_id' => $this->integer(11)->notNull(),
    			'workstation_id' => $this->integer(11)->notNull(),
    			'count_made' => $this->integer(10)->notNull()->defaultValue(0),
    			'count_failed' => $this->integer(10)->notNull()->defaultValue(0),
    			'message' => $this->string(1024),
    			'date_start' => $this->timestamp()->defaultValue(null),
    			'date_stop' => $this->timestamp()->defaultValue(null),
    			'date_creation' => $this->timestamp()->notNull(),
    	], $tableOptions);
    	
    	$this->alterColumn('production_task_history', 'date_creation', 'TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP');
    	$this->alterColumn('production_task_history', 'date_start', 'TIMESTAMP NULL DEFAULT NULL AFTER message');
    	$this->alterColumn('production_task_history', 'date_stop', 'TIMESTAMP NULL DEFAULT NULL AFTER date_start');
    	$this->addForeignKey('fk_production_task_history_production_task', 'production_task_history', 'production_task_id', 'production_task', 'id');
    	$this->addForeignKey('fk_production_task_history_user', 'production_task_history', 'user_id', 'user', 'id');
    	$this->addForeignKey('fk_production_task_history_workstation', 'production_task', 'id', 'workstation', 'id');
    }

    public function safeDown() {
        
    	$this->dropTable('production_task_history');
    	$this->dropTable('production_task');
    	$this->dropTable('product_requirement');
    	$this->dropTable('offered_product_requirement');
    	
    	$this->dropForeignKey('fk_order_offered_product_production_path', 'order_offered_product');
    	$this->dropColumn('order_offered_product', 'production_path_id');
    	$this->dropForeignKey('fk_offered_product_production_path', 'offered_product');
    	$this->dropColumn('offered_product', 'production_path_id');
    	$this->dropColumn('order_offered_product', 'attribute_value');
    	$this->dropColumn('offered_product', 'attribute_value');
    	
    	$this->dropTable('activity_product_attribute');
    	$this->dropTable('order_offered_product_attribute');
    	$this->dropTable('offered_product_attribute');
    	$this->dropTable('product_attribute_type_translation');
    	$this->dropTable('product_attribute_type');
    	$this->dropTable('production_path_step');
    	$this->dropTable('production_path_translation');
    	$this->dropTable('production_path');
    	
    	$roleIds = array_column((new Query())->select(['r.id'])->from('role r')->where(['symbol' => ['qa_specialist', 'worker']])->all(), 'id');
    	RoleTranslation::deleteAll(['role_id' => $roleIds]);
    	Role::deleteAll(['id' => $roleIds]);
    	
    	$this->dropTable('department_activity');
    	$this->createTable('department_activity', [
    			'deparment_id' => $this->integer(11),
    			'activity_id' => $this->integer(11),
    	], $tableOptions);
    	
    	$this->addPrimaryKey('pk_department_activity', 'department_activity', ['deparment_id', 'activity_id']);
    	$this->addForeignKey('fk_department_activity_department', 'department_activity', 'deparment_id', 'department', 'id');
    	$this->addForeignKey('fk_department_activity_activity', 'department_activity', 'activity_id', 'activity', 'id');
    	
    	$this->dropTable('user_department');
    	$this->createTable('user_department', [
    			'user_id' => $this->integer(11),
    			'deparment_id' => $this->integer(11),
    	], $tableOptions);
    	
    	$this->addPrimaryKey('pk_user_deparment', 'user_department', ['user_id', 'deparment_id']);
    	$this->addForeignKey('fk_user_deparment_user', 'user_department', 'user_id', 'user', 'id');
    	$this->addForeignKey('fk_user_department_department', 'user_department', 'deparment_id', 'department', 'id');
    }

}
