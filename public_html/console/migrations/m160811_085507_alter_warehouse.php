<?php

use yii\db\Schema;
use yii\db\Migration;
use common\models\ar\Warehouse;

class m160811_085507_alter_warehouse extends Migration {

    public function up() {
		$this->dropForeignKey('fk_warehouse_supplier_warehouse', 'warehouse_supplier');
		$this->dropColumn('warehouse_supplier', 'warehouse_id');
		
		$this->addColumn('warehouse', 'index', 'VARCHAR(48) NULL DEFAULT NULL AFTER name');
		$this->addColumn('product', 'index', 'VARCHAR(48) NULL DEFAULT NULL AFTER symbol');
		
		$this->addColumn('product_translation', 'remarks', 'VARCHAR(1024) NULL DEFAULT NULL AFTER description');
		
		if(Warehouse::find()->count() == 0) {
			$this->insert('warehouse', [
					'name' => 'Magazyn główny',
					'is_active' => 1
			]);
		}
    }

    public function down() {
    	$this->dropColumn('product_translation', 'remarks');
		$this->dropColumn('product', 'index');
		$this->dropColumn('warehouse', 'index');
    	
    	$this->addColumn('warehouse_supplier', 'warehouse_id', 'INT(11) NULL DEFAULT NULL AFTER id');
		$this->addForeignKey('fk_warehouse_supplier_warehouse', 'warehouse_supplier', 'warehouse_id', 'warehouse', 'id');
    }
}
