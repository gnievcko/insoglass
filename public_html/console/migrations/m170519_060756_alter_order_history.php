<?php

use yii\db\Schema;
use yii\db\Migration;

class m170519_060756_alter_order_history extends Migration {

    public function up() {
    	$this->addColumn('order_history', 'modification',  $this->text());
    }

    public function down() {
    	$this->dropColumn('order_history', 'modification');
    }

}
