<?php

use yii\db\Schema;
use yii\db\Migration;

class m170531_060012_alter_user_date_employment extends Migration {

    public function up() {
		$this->alterColumn('user', 'date_employment', 'DATE NULL DEFAULT NULL AFTER is_active');
    }

    public function down() {
    	$this->alterColumn('user', 'date_employment', 'TIMESTAMP NULL DEFAULT NULL AFTER is_active');
    }
  
}
