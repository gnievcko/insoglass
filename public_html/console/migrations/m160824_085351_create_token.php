<?php

use yii\db\Schema;
use yii\db\Migration;

class m160824_085351_create_token extends Migration {
	private $tableName = 'token';
	
	public function up() {
		$tableOptions = null;
		if($this->db->driverName === 'mysql') {
			$tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
		}
		 
		$this->createTable($this->tableName, [
				'id' => $this->primaryKey(11),
				'user_id' => $this->integer(11)->notNull(),
				'token_type_id' => $this->integer(11)->notNull(),
				'date_expiration' => $this->timestamp()->notNull(),
				'token' => $this->string(50)->notNull(),
		], $tableOptions);
		
		$this->alterColumn($this->tableName, 'date_expiration', 'TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP');
		
		$this->addForeignKey('fk_token_toke_type', $this->tableName, 'token_type_id', 'token_type', 'id');
		$this->addForeignKey('fk_token_user', $this->tableName, 'user_id', 'user', 'id');
	}
	
	public function down() {
		$this->dropTable($this->tableName);
	}
	
	/*
	 // Use safeUp/safeDown to run migration code within a transaction
	 public function safeUp() {
	 }
	
	 public function safeDown() {
	 }
	 */
}
