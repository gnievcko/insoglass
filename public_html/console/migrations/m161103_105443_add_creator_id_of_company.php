<?php

use yii\db\Schema;
use yii\db\Migration;

class m161103_105443_add_creator_id_of_company extends Migration {
    
    public function up() {
        $this->addColumn('company', 'user_id', $this->integer(11)->null());
        $this->addForeignKey('fk_company_user', 'company', 'user_id', 'user', 'id');
    }

    public function down() {
        $this->dropForeignKey('fk_company_user', 'company');
        $this->dropIndex('fk_company_user', 'company');
        $this->dropColumn('company', 'user_id');
    }
}
