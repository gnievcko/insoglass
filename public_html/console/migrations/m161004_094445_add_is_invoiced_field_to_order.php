<?php

use yii\db\Schema;
use yii\db\Migration;

class m161004_094445_add_is_invoiced_field_to_order extends Migration {

    public function up() {
        $this->addColumn('order', 'is_invoiced', $this->boolean()->notNull()->defaultValue(0));
    }

    public function down() {
        $this->dropColumn('order', 'is_invoiced');
    }
}
