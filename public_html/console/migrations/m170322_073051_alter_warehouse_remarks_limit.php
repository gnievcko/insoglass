<?php

use yii\db\Migration;

class m170322_073051_alter_warehouse_remarks_limit extends Migration {

    public function safeUp() {
    	$this->alterColumn('warehouse_delivery', 'description', 'VARCHAR(2048) NULL DEFAULT NULL AFTER date_creation');
    	$this->alterColumn('warehouse_delivery_product', 'note', 'VARCHAR(1024) NULL DEFAULT NULL AFTER unit');
    	$this->alterColumn('warehouse_product_operation', 'description', 'VARCHAR(2048) NULL DEFAULT NULL AFTER user_confirming_id');
    	$this->alterColumn('warehouse_product_status_history', 'description', 'VARCHAR(2048) NULL DEFAULT NULL AFTER count');
    }

    public function safeDown() {
    	$this->alterColumn('warehouse_delivery', 'description', 'VARCHAR(256) NULL DEFAULT NULL AFTER date_creation');
    	$this->alterColumn('warehouse_delivery_product', 'note', 'VARCHAR(150) NULL DEFAULT NULL AFTER unit');
    	$this->alterColumn('warehouse_product_operation', 'description', 'VARCHAR(256) NULL DEFAULT NULL AFTER user_confirming_id');
    	$this->alterColumn('warehouse_product_status_history', 'description', 'VARCHAR(256) NULL DEFAULT NULL AFTER count');
    }
}
