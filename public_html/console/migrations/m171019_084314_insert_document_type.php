<?php

use yii\db\Schema;
use yii\db\Migration;
use common\models\ar\DocumentSection;
use common\models\ar\DocumentType;
use common\models\ar\DocumentTypeSection;
use common\models\ar\DocumentTypeTranslation;
use common\models\ar\Language;


class m171019_084314_insert_document_type extends Migration {

    public function safeUp() {
        $footerSectionId = DocumentSection::find()->where(['symbol' => 'COMPANY_FOOTER'])->one()->id;
        $headerSectionId = DocumentSection::find()->where(['symbol' => 'COMPANY_SHORT_HEADER'])->one()->id;
        $plId = Language::findOne(['symbol' => 'pl'])->id;
        
        $this->insert('document_type', [
            'symbol' => 'INTERMEDIATE_PRODUCTS',
            'document_section_header_id' => null,
            'document_section_footer_id' => $footerSectionId,
            'margin_top' => 15,
            'margin_bottom' => 50,
            'margin_left'=> 15,
            'margin_right' => 15,
            'format' => 'A4',
            'orientation' => 'P',
            'is_for_report' => 0,
        ]);
        
        $documentTypeId = $this->db->getLastInsertId();

        $this->insert('document_type_translation', [
            'document_type_id' => $documentTypeId,
            'language_id' => $plId,
            'name' => 'Raport prognozujący wykonalność zamówień',
        ]);
        
        $this->insert('document_section', [
            'symbol' => 'INTERMEDIATE_PRODUCTS'
        ]);
        
        $sectionId = $this->db->getLastInsertId();
        
        $this->insert('document_type_section', [
            'document_type_id' => $documentTypeId,
            'document_section_id' => $headerSectionId,
            'sequence_number' => 0,
        ]);
        
        $this->insert('document_type_section', [
            'document_type_id' => $documentTypeId,
            'document_section_id' => $sectionId,
            'sequence_number' => 1,
        ]);
        
    }

    public function down() {
        $documentType = DocumentType::findOne(['symbol' => 'INTERMEDIATE_PRODUCTS']);
        
        if(!empty($documentType)) {
            DocumentTypeTranslation::deleteAll(['document_type_id' => $documentType->id]);
            DocumentTypeSection::deleteAll(['document_type_id' => $documentType->id]);
            DocumentSection::deleteAll(['symbol' => 'INTERMEDIATE_PRODUCTS']);
            $documentType->delete();
        }
    }

}
