<?php

use yii\db\Schema;
use yii\db\Migration;

class m160819_125506_create_document_section extends Migration {
    private $tableName = 'document_section';

    public function up() {
    	$tableOptions = null;
    	if($this->db->driverName === 'mysql') {
    		$tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
    	}

    	$this->createTable($this->tableName, [
    			'id' => $this->primaryKey(11),
    			'symbol' => $this->string(30)->notNull()->unique(),
    			'description' => $this->text(),
    	], $tableOptions);
    }

    public function down() {
        $this->dropTable($this->tableName);
    }
}
