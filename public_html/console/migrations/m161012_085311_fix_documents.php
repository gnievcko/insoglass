<?php

use yii\db\Schema;
use yii\db\Migration;
use common\models\ar\DocumentType;
use common\models\ar\DocumentTypeSection;
use common\models\ar\DocumentSection;
use yii\db\Query;
class m161012_085311_fix_documents extends Migration {

    public function safeUp() {
        $section = DocumentSection::findOne(['symbol' => 'COMPANY_HEADER']);
        
        $this->add_header('GOODS_RECEIVED', $section, -1);
        $this->add_header('GOODS_ISSUED', $section, -1);
        
        $section2 = DocumentSection::findOne(['symbol' => 'COMPANY_SHORT_HEADER']);
        
        $this->add_header('OFFER_DETAILS', $section2, -1);
        
        $this->add_header('MALFUNCTION_PROTOCOL', $section2, -1);
        $this->add_header('FIREEQ_ACCEPTANCE_PROTOCOL', $section2, -1);
    }

    public function safeDown() {

    }
    
    private function add_header($symbol, $section, $number) {
        $id = (new Query())->select(['dt.id'])->from('document_type dt')->where(['symbol' => $symbol])->scalar();
        $this->update('document_type', ['document_section_header_id' => null, 'margin_top' => 15], ['id' => $id]);
        
        $documentTypeSection = new DocumentTypeSection();
        $documentTypeSection->document_type_id = $id;
        $documentTypeSection->document_section_id = $section->id;
        $documentTypeSection->sequence_number = $number;
        $documentTypeSection->insert();
    }
    
    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp() {
    }

    public function safeDown() {
    }
    */
}
