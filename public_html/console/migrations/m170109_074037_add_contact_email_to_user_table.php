<?php

use yii\db\Schema;
use yii\db\Migration;

class m170109_074037_add_contact_email_to_user_table extends Migration {

    public function up() {
        $this->addColumn('user', 'contact_email', $this->string(64)->null());
    }

    public function down() {
        $this->dropColumn('user', 'contact_email');
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp() {
    }

    public function safeDown() {
    }
    */
}
