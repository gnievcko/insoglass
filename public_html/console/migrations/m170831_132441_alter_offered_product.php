<?php

use yii\db\Schema;
use yii\db\Migration;

class m170831_132441_alter_offered_product extends Migration {

    public function up() {
        $this->addColumn('offered_product', 'height', $this->integer(11));
        $this->addColumn('offered_product', 'width', $this->integer(11));
        $this->addColumn('offered_product', 'shape_id', $this->integer(11));
        $this->addColumn('offered_product', 'construction_id', $this->integer(11));
        
        $this->addForeignKey('fk_offered_product_construction', 'offered_product', 'construction_id', 'construction', 'id');
        $this->addForeignKey('fk_offered_product_shape', 'offered_product', 'shape_id', 'shape', 'id');
        
        $this->addColumn('product', 'height', $this->integer(11));
        $this->addColumn('product', 'width', $this->integer(11));
        $this->addColumn('product', 'shape_id', $this->integer(11));
        $this->addColumn('product', 'construction_id', $this->integer(11));
        
        $this->addForeignKey('fkproduct_construction', 'product', 'construction_id', 'construction', 'id');
        $this->addForeignKey('fk_product_shape', 'product', 'shape_id', 'shape', 'id'); 
        
        $this->addColumn('offered_product', 'big_drillhole_count', $this->integer(11));
        $this->addColumn('offered_product', 'small_drillhole_count', $this->integer(11));
        $this->addColumn('product', 'big_drillhole_count', $this->integer(11));
        $this->addColumn('product', 'small_drillhole_count', $this->integer(11));
        
    }

    public function down() {
        $this->dropForeignKey('fk_offered_product_construction', 'offered_product');
        $this->dropForeignKey('fk_offered_product_shape', 'offered_product');
        $this->dropColumn('offered_product', 'shape_id');
        $this->dropColumn('offered_product', 'construction_id');
        $this->addColumn('offered_product', 'height');
        $this->addColumn('offered_product', 'width');
        
        $this->dropForeignKey('fk_product_construction', 'offered_product');
        $this->dropForeignKey('fk_product_shape', 'offered_product');
        $this->dropColumn('product', 'shape_id');
        $this->dropColumn('product', 'construction_id');
        $this->addColumn('product', 'height');
        $this->addColumn('product', 'width'); 
        
        $this->addColumn('offered_product', 'big_drillhole_count');
        $this->addColumn('offered_product', 'small_drillhole_count');
        $this->addColumn('product', 'big_drillhole_count');
        $this->addColumn('product', 'small_drillhole_count');
    }

}
