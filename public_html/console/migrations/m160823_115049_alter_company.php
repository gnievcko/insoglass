<?php

use yii\db\Schema;
use yii\db\Migration;

class m160823_115049_alter_company extends Migration {

    public function up() {
		$this->addColumn('company', 'fax1', 'VARCHAR(32) NULL DEFAULT NULL AFTER phone2');
		$this->addColumn('company', 'vat_identification_number', 'VARCHAR(32) NULL DEFAULT NULL AFTER fax1');
		$this->addColumn('company', 'taxpayer_identification_number', 'VARCHAR(32) NULL DEFAULT NULL AFTER vat_identification_number');
		$this->addColumn('company', 'krs_number', 'VARCHAR(32) NULL DEFAULT NULL AFTER taxpayer_identification_number');
    }

    public function down() {
		$this->dropColumn('company', 'krs_number');
		$this->dropColumn('company', 'taxpayer_identification_number');
		$this->dropColumn('company', 'vat_identification_number');
		$this->dropColumn('company', 'fax1');
    }

}
