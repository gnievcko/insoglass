<?php

use yii\db\Schema;
use yii\db\Migration;

class m160822_084252_create_document_type_section extends Migration {
    private $tableName = 'document_type_section';

    public function up() {
    	$tableOptions = null;
    	if($this->db->driverName === 'mysql') {
    		$tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
    	}

    	$this->createTable($this->tableName, [
    			'document_type_id' => $this->integer(11)->notNull(),
    			'document_section_id' => $this->integer(11)->notNull(),
    			'sequence_number' => $this->integer(11)->notNull(),
    	], $tableOptions);

	    $this->addPrimaryKey('pk_document_type_section', 'document_type_section', ['document_type_id', 'document_section_id']);

    	$this->addForeignKey('fk_document_type_section_document_type', 'document_type_section', 'document_type_id', 'document_type', 'id');
    	$this->addForeignKey('fk_document_type_section_document_section', 'document_type_section', 'document_section_id', 'document_section', 'id');

        $this->createIndex('uq_document_type_section_type_sequence_number', 'document_type_section', ['document_type_id', 'sequence_number'], true);
    }

    public function down() {
        $this->dropTable($this->tableName);
    }
}
