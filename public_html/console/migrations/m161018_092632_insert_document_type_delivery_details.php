<?php

use yii\db\Schema;
use yii\db\Migration;
use common\models\ar\DocumentTypeSection;
use common\models\ar\DocumentType;
use common\models\ar\DocumentSection;

class m161018_092632_insert_document_type_delivery_details extends Migration {


    public function up() {
    	$headerSectionId = DocumentSection::find()->where(['symbol' => 'COMPANY_HEADER'])->one()->id;
    	$footerSectionId = DocumentSection::find()->where(['symbol' => 'COMPANY_FOOTER'])->one()->id;
    	
    	$this->insert('document_section', ['symbol' => 'DELIVERY_DETAILS_TITLE']);
    	$deliveryDetailsTitleId = $this->db->getLastInsertID();
    	
        $goodsReceivedProducts = DocumentSection::findOne(['symbol' => 'GOODS_RECEIVED_PRODUCTS']);
                
    	$this->insert('document_type', [
    			'symbol' => 'DELIVERY_DETAILS',
    			'document_section_header_id' => $headerSectionId,
    			'document_section_footer_id' => $footerSectionId,
    			'margin_top' => 70,
    			'margin_bottom' => 50,
    			'margin_left'=> 15,
    			'margin_right' => 15,
    			'format' => 'A4',
    			'orientation' => 'P',
    	]);
        
    	$documentTypeId = $this->db->getLastInsertId();
        
    	$this->insert('document_type_section', [
    			'document_type_id' => $documentTypeId,
				'document_section_id' => $deliveryDetailsTitleId,
				'sequence_number' => 1,
		]);
		
        $this->insert('document_type_section', [
				'document_type_id' => $documentTypeId,
    			'document_section_id' => $goodsReceivedProducts->id,
    			'sequence_number' => 2,
    	]);
    }

    public function down() {
    	$documentType1Id = DocumentType::find()->where(['symbol' => 'DELIVERY_DETAILS'])->one()->id;
    	
        DocumentTypeSection::deleteAll(['document_type_id' => $documentType1Id]);
        DocumentType::deleteAll(['id' => $documentType1Id]);
        DocumentSection::deleteAll(['symbol' => ['DELIVERY_DETAILS_TITLE']]);
    }
    
}
