<?php

use yii\db\Schema;
use yii\db\Migration;
use common\models\ar\ProductStatusReference;
use common\models\ar\ProductStatus;
use common\helpers\Utility;
use common\models\ar\Language;
use common\models\ar\ProductStatusReferenceTranslation;

class m161123_100317_alter_insert_product_status_reference extends Migration {

    public function up() {
    	// dodatkowej tabelki slownikowej nie ma ze wzgledu na kwestie wydajnosciowe i niecheci
    	// do poszerzania i tak obciazonej bazy danych
		$this->addColumn('product_status_reference', 'field_type', 'VARCHAR(32) NULL DEFAULT NULL AFTER is_required');
		ProductStatusReference::updateAll(['field_type' => Utility::ADDITIONAL_FIELD_TYPE_SELECT2]);
		
		$issuingId = ProductStatus::find()->where(['symbol' => Utility::PRODUCT_STATUS_ISSUING])->one()->id;
		$utilizationId = ProductStatus::find()->where(['symbol' => Utility::PRODUCT_STATUS_UTILIZATION])->one()->id;
		$issuingReservationId = ProductStatus::find()->where(['symbol' => Utility::PRODUCT_STATUS_ISSUING_RESERVATION])->one()->id;
		$languageId = Language::find()->where(['symbol' => 'pl'])->one()->id;
		
		$this->update('product_status_reference', ['symbol' => 'orderIssued'], ['symbol' => 'orderReserved', 'product_status_id' => $issuingReservationId]);
		
		$this->insert('product_status_reference', [
				'product_status_id' => $issuingId,
				'symbol' => 'orderIssued',
				'name_table' => 'order',
				'name_column' => 'id',
				'name_expression' => 'CONCAT(order.title, " (", order.number, ")")',
				'url_details' => 'order/details/{id}',
				'is_required' => 0,
				'field_type' => Utility::ADDITIONAL_FIELD_TYPE_SELECT2,
		]);
		$referenceId = $this->db->getLastInsertID();

		$this->insert('product_status_reference_translation', [
				'reference_id' => $referenceId,
				'language_id' => $languageId,
				'name' => 'Zamówienie',
		]);
		
		$this->insert('product_status_reference', [
				'product_status_id' => $utilizationId,
				'symbol' => 'userUtilized',
				'name_table' => '',
				'name_column' => '',
				'name_expression' => '',
				'url_details' => null,
				'is_required' => 1,
				'field_type' => Utility::ADDITIONAL_FIELD_TYPE_TEXT_INPUT,
		]);
		$referenceId = $this->db->getLastInsertID();
		
		$this->insert('product_status_reference_translation', [
				'reference_id' => $referenceId,
				'language_id' => $languageId,
				'name' => 'Utylizujący',
		]);
		
		$this->alterColumn('warehouse_product_status_history_data', 'referenced_object_id', 'INT(11) NULL DEFAULT NULL');
		$this->addColumn('warehouse_product_status_history_data', 'custom_value', 'VARCHAR(2048) NULL DEFAULT NULL AFTER referenced_object_id');
    }

    public function down() {
    	$this->dropColumn('warehouse_product_status_history_data', 'custom_value');
    	$this->alterColumn('warehouse_product_status_history_data', 'referenced_object_id', 'INT(11) NOT NULL');
    	
    	$issuingId = ProductStatus::find()->where(['symbol' => Utility::PRODUCT_STATUS_ISSUING])->one()->id;
    	$utilizationId = ProductStatus::find()->where(['symbol' => Utility::PRODUCT_STATUS_UTILIZATION])->one()->id;
    	
    	$referenceId1 = ProductStatusReference::find()->where(['product_status_id' => $issuingId, 'symbol' => 'orderIssued'])->one()->id;
    	$referenceId2 = ProductStatusReference::find()->where(['product_status_id' => $utilizationId, 'symbol' => 'userUtilized'])->one()->id;
    	
    	ProductStatusReferenceTranslation::deleteAll(['reference_id' => [$referenceId1, $referenceId2]]);
    	ProductStatusReference::deleteAll(['id' => [$referenceId1, $referenceId2]]);
    	
        $this->dropColumn('product_status_reference', 'field_type');
    }
}
