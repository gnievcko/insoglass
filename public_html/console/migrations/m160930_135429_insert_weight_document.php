<?php

use yii\db\Schema;
use yii\db\Migration;
use common\models\ar\DocumentSection;
use common\models\ar\DocumentType;
use common\models\ar\DocumentTypeSection;

class m160930_135429_insert_weight_document extends Migration {

    public function up() {
        $headerSectionId = DocumentSection::find()->where(['symbol' => 'COMPANY_SHORT_HEADER'])->one()->id;
        $footerSectionId = DocumentSection::find()->where(['symbol' => 'COMPANY_FOOTER'])->one()->id;

        $this->insert('document_section', ['symbol' => 'WEIGHT_PROTOCOL']);
        $weightId = $this->db->getLastInsertID();

        $this->insert('document_type', [
            'symbol' => 'WEIGHT_PROTOCOL',
            'document_section_header_id' => null,
            'document_section_footer_id' => $footerSectionId,
            'margin_top' => 15,
            'margin_bottom' => 50,
            'margin_left' => 15,
            'margin_right' => 15,
            'format' => 'A4',
            'orientation' => 'P',
        ]);
        
        $documentTypeId = $this->db->getLastInsertId();
        $this->insert('document_type_section', [
            'document_type_id' => $documentTypeId,
            'document_section_id' => $headerSectionId,
            'sequence_number' => 1,
        ]);
        $this->insert('document_type_section', [
            'document_type_id' => $documentTypeId,
            'document_section_id' => $weightId,
            'sequence_number' => 2,
        ]);
    }

    public function down() {
        $documentType1Id = DocumentType::find()->where(['symbol' => 'WEIGHT_PROTOCOL'])->one()->id;


        DocumentTypeSection::deleteAll(['document_type_id' => [$documentType1Id]]);
        DocumentType::deleteAll(['id' => [$documentType1Id]]);
        DocumentSection::deleteAll(['symbol' => ['WEIGHT_PROTOCOL']]);
    }

}
