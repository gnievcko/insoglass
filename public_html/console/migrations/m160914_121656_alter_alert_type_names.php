<?php

use yii\db\Schema;
use yii\db\Migration;
use common\models\ar\AlertType;

class m160914_121656_alter_alert_type_names extends Migration {

    public function up() {
		$this->update('alert_type_translation', ['name' => 'Zbliża się termin realizacji zamówienia'], 
				'alert_type_id = :id', [':id' => AlertType::find()->where(['symbol' => 'order_deadline_warning'])->one()->id]);
		
		$this->update('alert_type_translation', ['name' => 'Nadszedł termin realizacji zamówienia'],
				'alert_type_id = :id', [':id' => AlertType::find()->where(['symbol' => 'order_deadline_problem'])->one()->id]);
		
		$this->update('alert_type_translation', ['name' => 'Zbliża się termin realizacji zadania'],
				'alert_type_id = :id', [':id' => AlertType::find()->where(['symbol' => 'task_deadline_warning'])->one()->id]);
		
		$this->update('alert_type_translation', ['name' => 'Nadszedł termin realizacji zadania'],
				'alert_type_id = :id', [':id' => AlertType::find()->where(['symbol' => 'task_deadline_problem'])->one()->id]);
    }

    public function down() {
        // nie ma potrzeby
    }
}
