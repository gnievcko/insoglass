<?php

use yii\db\Schema;
use yii\db\Migration;

class m160915_063730_alert_and_task_history extends Migration {

    public function up() {
    	$tableOptions = null;
    	if($this->db->driverName === 'mysql') {
    		$tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
    	}
    	
    	$this->createTable('alert_history', [
    			'id' => $this->primaryKey(11),
    			'alert_type_id' => $this->integer(11)->notNull(),
    			'user_id' => $this->integer(11)->notNull(),
    			'message' => $this->string(2048)->notNull(),
    			'user_deactivating_id' => $this->integer(11),
    			'date_deactivation' => $this->timestamp()->defaultValue(null),
    			'date_creation' => $this->timestamp()->defaultValue(null),
    			'order_id' => $this->integer(11),
    			'product_id' => $this->integer(11),
    			'company_id' => $this->integer(11),
    			'task_id' => $this->integer(11),
    			'warehouse_id' => $this->integer(11)
    	], $tableOptions);
    	
    	$this->addForeignKey('fk_alert_history_alert_type', 'alert_history', 'alert_type_id', 'alert_type', 'id');
    	$this->addForeignKey('fk_alert_history_user', 'alert_history', 'user_id', 'user', 'id');
    	$this->addForeignKey('fk_alert_history_user_deactivating', 'alert_history', 'user_deactivating_id', 'user', 'id');
    	 
    	$this->addForeignKey('fk_alert_history_order_reference', 'alert_history', 'order_id', 'order', 'id');
    	$this->addForeignKey('fk_alert_history_product_reference', 'alert_history', 'product_id', 'product', 'id');
    	$this->addForeignKey('fk_alert_history_company_reference', 'alert_history', 'company_id', 'company', 'id');
    	$this->addForeignKey('fk_alert_history_task_reference', 'alert_history', 'task_id', 'task', 'id');
    	$this->addForeignKey('fk_alert_history_warehouse_reference', 'alert_history', 'warehouse_id', 'warehouse', 'id');
    	 
    	$this->alterColumn('alert_history', 'date_deactivation', 'TIMESTAMP NULL DEFAULT NULL');
    	$this->alterColumn('alert_history', 'date_creation', 'TIMESTAMP NULL DEFAULT NULL');
    	
    	$this->dropForeignKey('fk_alert_user_deactivating', 'alert');
    	$this->dropColumn('alert', 'user_deactivating_id');
    	$this->dropColumn('alert', 'date_deactivation');
    	
    	$this->createTable('task_history', [
    			'id' => $this->primaryKey(11),
    			'task_type_id' => $this->integer(11)->notNull(),
    			'task_priority_id' => $this->integer(11)->notNull(),
    			'user_id' => $this->integer(11)->notNull(),
    			'title' => $this->string(128)->notNull(),
    			'message' => $this->string(2048)->notNull(),
    			'user_notified_id' => $this->integer(11),
    			'date_reminder' => $this->timestamp()->defaultValue(null),
    			'date_deadline' => $this->timestamp()->defaultValue(null),
    			'is_complete' => $this->boolean()->notNull()->defaultValue(0),
    			'user_completing_id' => $this->integer(11),
    			'date_completion' => $this->timestamp()->defaultValue(null),
    			'date_creation' => $this->timestamp()->defaultValue(null),
    			'date_history' => $this->timestamp()->defaultValue(null),
    	], $tableOptions);
    	 
    	$this->addForeignKey('fk_task_history_task_type', 'task_history', 'task_type_id', 'task_type', 'id');
    	$this->addForeignKey('fk_task_history_task_priority', 'task_history', 'task_priority_id', 'task_priority', 'id');
    	$this->addForeignKey('fk_task_history_user', 'task_history', 'user_id', 'user', 'id');
    	$this->addForeignKey('fk_task_history_user_notified', 'task_history', 'user_notified_id', 'user', 'id');
    	$this->addForeignKey('fk_task_history_user_deactivating', 'task_history', 'user_completing_id', 'user', 'id');
    	$this->alterColumn('task_history', 'date_deadline', 'TIMESTAMP NULL DEFAULT NULL');
    	$this->alterColumn('task_history', 'date_completion', 'TIMESTAMP NULL DEFAULT NULL');
    	$this->alterColumn('task_history', 'date_creation', 'TIMESTAMP NULL DEFAULT NULL');
    	$this->alterColumn('task_history', 'date_history', 'TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP');
    	 
    	$this->createTable('task_data_history', [
    			'task_history_id' => $this->integer(11),
    			'task_type_reference_id' => $this->integer(11),
    			'referenced_object_id' => $this->integer(11)->notNull(),
    	], $tableOptions);
    	 
    	$this->addPrimaryKey('pk_task_data_history', 'task_data_history', ['task_type_reference_id', 'task_history_id']);
    	$this->addForeignKey('fk_task_data_history_task', 'task_data_history', 'task_history_id', 'task_history', 'id');
    	$this->addForeignKey('fk_task_data_history_reference', 'task_data_history', 'task_type_reference_id', 'task_type_reference', 'id');    	 
    }

    public function down() {
        $this->dropTable('task_data_history');
        $this->dropTable('task_history');
        
        $this->addColumn('alert', 'user_deactivating_id', 'INT(11) NULL DEFAULT NULL AFTER is_active');
        $this->addColumn('alert', 'date_deactivation', 'TIMESTAMP NULL DEFAULT NULL AFTER user_deactivating_id');
        $this->addForeignKey('fk_alert_user_deactivating', 'alert', 'user_deactivating_id', 'user', 'id');
        
        $this->dropTable('alert_history');
    }

}
