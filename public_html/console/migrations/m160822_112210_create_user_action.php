<?php

use yii\db\Schema;
use yii\db\Migration;

class m160822_112210_create_user_action extends Migration {

    public function up() {
    	if($this->db->driverName === 'mysql') {
    		$tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
    	}
   	
    	$this->createTable('user_action', [
    			'id' => $this->primaryKey(11),
    			'user_id' => $this->integer(11)->notNull(),
    			'date_action' => $this->timestamp()->notNull(),
    	], $tableOptions);
    	
    	$this->addForeignKey('fk_user_action_user', 'user_action', 'user_id', 'user', 'id');
   	
    	$this->dropColumn('user', 'last_login');
    }

    public function down() {
        $this->dropForeignKey('fk_user_action_user', 'user_action');
		$this->dropTable('alert_priority_translation');
		$this->addColumn('user', 'last_login', $this->timestamp->defaultValue(null));

    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp() {
    }

    public function safeDown() {
    }
    */
}
