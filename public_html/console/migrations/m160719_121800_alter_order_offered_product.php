<?php

use yii\db\Schema;
use yii\db\Migration;

class m160719_121800_alter_order_offered_product extends Migration {

    public function up() {
        $this->addColumn('order_offered_product', 'warehouse_product_id', $this->integer(11));
        $this->alterColumn('order_offered_product', 'warehouse_product_id', 'integer(11) NULL DEFAULT NULL');

    	$this->addForeignKey('fk_order_offered_product_warehouse_product', 'order_offered_product', 'warehouse_product_id', 'warehouse_product', 'id');
        $this->alterColumn('order_offered_product', 'offered_product_id', 'integer(11) NULL DEFAULT NULL');
    }

    public function down() {
        $this->alterColumn('order_offered_product', 'offered_product_id', 'integer(11) NOT NULL');
        $this->dropForeignKey('fk_order_offered_product_warehouse_product', 'order_offered_product');
        $this->dropColumn('order_offered_product', 'warehouse_product_id');
    }
}
