<?php

use yii\db\Schema;
use yii\db\Migration;

class m160811_073528_alter_warehouse_delivery extends Migration {

    public function up() {
        $this->addColumn('warehouse_delivery', 'description', 'string(256) NULL');
    }

    public function down() {
        $this->dropColumn('warehouse_delivery', 'description');
    }
}
