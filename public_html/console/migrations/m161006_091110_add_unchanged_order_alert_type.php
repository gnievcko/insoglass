<?php

use yii\db\Migration;
use common\alerts\AlertConstants;
use yii\db\Query;

class m161006_091110_add_unchanged_order_alert_type extends Migration {

    public function safeUp() {
        $this->insert('alert_type', [
            'symbol' => AlertConstants::ALERT_TYPE_NEW_ORDER_STATUS_UNCHANGED,
            'alert_priority_id' => (new Query())->select(['id'])
                ->from('alert_priority')
                ->where(['symbol' => AlertConstants::ALERT_PRIORITY_WARNING])
                ->one()['id'],
        ]);
        
        $alertTypeId = Yii::$app->db->lastInsertID;
        $this->insert('alert_type_translation', [
            'alert_type_id' => $alertTypeId,
            'name' => 'Zamówienie nie zostało zmodyfikowane w ciągu 24 godzin od akceptacji przez klienta',
            'language_id' => (new Query())->select(['id'])
                ->from('language')
                ->where(['symbol' => 'pl'])
                ->one()['id'],
        ]);
    }

    public function safeDown() {
        $alertTypeId = (new Query())->select(['id'])
            ->from('alert_type')
            ->where(['symbol' => AlertConstants::ALERT_TYPE_NEW_ORDER_STATUS_UNCHANGED])
            ->one()['id'];
        
        $this->delete('alert_type_translation', ['alert_type_id' => $alertTypeId]);
        $this->delete('alert_type', ['id' => $alertTypeId]);
    }
}
