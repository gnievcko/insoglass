<?php

use yii\db\Schema;
use yii\db\Migration;

class m161103_212311_alter_order_optional_description extends Migration {

    public function up() {
        $this->alterColumn('order', 'description', $this->text()->null()->defaultValue(null));
    }

    public function down() {
        $this->alterColumn('order', 'description', $this->text()->notNull());
    }
}
