<?php

use yii\db\Schema;
use yii\db\Migration;
use common\models\ar\Entity;
use common\models\ar\NumberTemplateStorage;
use common\models\ar\NumberTemplate;
use yii\db\Query;

class m170116_090953_alter_entity_number_template_storage extends Migration {

    public function up() {
		$entityId = (new Query())->select(['e.id'])->from('entity e')->where(['e.is_active' => true])->orderBy(['id' => SORT_ASC])->limit(1)->scalar();
		$otherEntityIds = array_column((new Query())->select(['e.id'])->from('entity e')->where(['<>', 'id', $entityId])->orderBy(['id' => SORT_ASC])->all(), 'id');
		
		$this->dropForeignKey('fk_order_entity', 'order');
		$this->update('order', ['entity_id' => intval($entityId)], ['entity_id' => null]);
		$this->alterColumn('order', 'entity_id', 'INT(11) NOT NULL DEFAULT 1');
		$this->addForeignKey('fk_order_entity', 'order', 'entity_id', 'entity', 'id');
		
		$this->addColumn('number_template', 'entity_id', 'INT(11) NULL DEFAULT NULL AFTER symbol');
		$this->update('number_template', ['entity_id' => $entityId], ['symbol' => ['invoice', 'proforma_invoice', 'advance_invoice']]);
		$this->dropIndex('symbol', 'number_template');
		$this->execute('ALTER TABLE number_template ADD UNIQUE INDEX uk_number_template_symbol_entity (symbol, entity_id)');
		
		if(!empty($otherEntityIds)) {
			foreach($otherEntityIds as $id) {
				$this->insert('number_template', [
						'symbol' => 'invoice',
						'entity_id' => $id,
						'separator' => '/',
						'template' => '{noInYear:inc}/{year}',
						'is_active' => 1,
				]);				
				$numberTemplateId = $this->db->getLastInsertID();
				$nts = NumberTemplateStorage::find()->joinWith('numberTemplate nt')->where(['nt.symbol' => 'invoice', 'entity_id' => $entityId])->one();				
				$this->insert('number_template_storage', [
						'number_template_id' => $numberTemplateId,
						'current_number' => $nts->current_number,
				]);
				
				$this->insert('number_template', [
						'symbol' => 'proforma_invoice',
						'entity_id' => $id,
						'separator' => '/',
						'template' => '{noInYear:inc}/PRO/{year}',
						'is_active' => 1,
				]);
				$numberTemplateId = $this->db->getLastInsertID();
				$nts = NumberTemplateStorage::find()->joinWith('numberTemplate nt')->where(['nt.symbol' => 'proforma_invoice', 'entity_id' => $entityId])->one();
				$this->insert('number_template_storage', [
						'number_template_id' => $numberTemplateId,
						'current_number' => $nts->current_number,
				]);
				
				$this->insert('number_template', [
						'symbol' => 'advance_invoice',
						'entity_id' => $id,
						'separator' => '/',
						'template' => '{noInYear:inc}/ZAL/{year}',
						'is_active' => 1,
				]);
				$numberTemplateId = $this->db->getLastInsertID();
				$nts = NumberTemplateStorage::find()->joinWith('numberTemplate nt')->where(['nt.symbol' => 'advance_invoice', 'entity_id' => $entityId])->one();
				$this->insert('number_template_storage', [
						'number_template_id' => $numberTemplateId,
						'current_number' => $nts->current_number,
				]);
			}
		}
		
		if(!empty($entityId)) {
			$this->addForeignKey('fk_number_template_entity', 'number_template', 'entity_id', 'entity', 'id');
		}
    }

    public function down() {
    	$entityId = Entity::find()->where(['is_active' => 1])->orderBy(['id' => SORT_ASC])->limit(1)->one()->id;
    	$ntIds = NumberTemplate::find()->where(['<>', 'entity_id', $entityId])->andWhere('entity_id IS NOT NULL')->select(['id'])->column();
    	
    	if(!empty($ntIds)) {
    		foreach($ntIds as $id) {
    			NumberTemplateStorage::deleteAll(['number_template_id' => $id]);
    			NumberTemplate::deleteAll(['id' => $id]);
    		}
    	}
    	
    	$this->dropIndex('uk_number_template_symbol_entity', 'number_template');
    	$this->execute('ALTER TABLE number_template ADD UNIQUE INDEX symbol (symbol)');
    	
    	$this->dropForeignKey('fk_number_template_entity', 'number_template');
    	$this->dropColumn('number_template', 'entity_id');
    	
    	$this->dropForeignKey('fk_order_entity', 'order');
    	$this->alterColumn('order', 'entity_id', 'INT(11) NULL DEFAULT 1');
		$this->addForeignKey('fk_order_entity', 'order', 'entity_id', 'entity', 'id');
    	
    }
}
