<?php

use yii\db\Schema;
use yii\db\Migration;

class m161205_122052_create_configuration extends Migration {

    public function up() {
    	$tableOptions = null;
    	if($this->db->driverName === 'mysql') {
    		$tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
    	}
    	 
    	$this->createTable('configuration', [
    			'id' => $this->primaryKey(11),
    			'path_mysqldump' => $this->string(1024),
    			'path_mongodbdump' => $this->string(1024),
    			'path_dbbackups' => $this->string(1024),
    	], $tableOptions);
    	
    	$this->insert('configuration', [
    			'path_mysqldump' => 'mysqldump',
    			'path_mongodbdump' => 'mongodump',
    			'path_dbbackups' => '~/db_backups',
    	]);
    }

    public function down() {
        $this->dropTable('configuration');
    }

}
