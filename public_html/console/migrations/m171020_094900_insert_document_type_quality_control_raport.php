<?php

use yii\db\Schema;
use yii\db\Migration;
use common\models\ar\DocumentTypeSection;
use common\models\ar\DocumentType;
use common\models\ar\DocumentSection;
use yii\db\Query;

class m171020_094900_insert_document_type_quality_control_raport extends Migration {
    public function safeUp()
    {
        $languageId = (new Query())->select(['id'])->from('language')->where(['symbol' => 'pl'])->scalar();

        $this->insert('document_section', ['symbol' => 'QUALITY_CONTROL_SUMMARY',]);
        $summarySectionId = $this->db->getLastInsertID();

        $this->insert('document_section', ['symbol' => 'QUALITY_CONTROL_DETAILS',]);
        $detailsSectionId = $this->db->getLastInsertID();

        $headerSectionId = DocumentSection::find()->where(['symbol' => 'COMPANY_HEADER'])->one()->id;
        $footerSectionId = DocumentSection::find()->where(['symbol' => 'COMPANY_FOOTER'])->one()->id;

        $this->insert('document_type', [
            'symbol' => 'QUALITY_CONTROL',
            'margin_top' => 15,
            'margin_bottom' => 50,
            'margin_left' => 15,
            'margin_right' => 15,
            'format' => 'A4',
            'orientation' => 'P',
            'document_section_footer_id' => $footerSectionId,
            'document_section_header_id' => $headerSectionId,
            'is_for_report' => 0,
        ]);

        $documentTypeId = $this->db->getLastInsertId();

        $this->insert('document_type_section', ['document_type_id' => $documentTypeId, 'document_section_id' => $headerSectionId, 'sequence_number' => 0]);
        $this->insert('document_type_section', ['document_type_id' => $documentTypeId, 'document_section_id' => $summarySectionId, 'sequence_number' => 1]);
        $this->insert('document_type_section', ['document_type_id' => $documentTypeId, 'document_section_id' => $detailsSectionId, 'sequence_number' => 2]);

        $this->insert('document_type_translation', [
            'document_type_id' => $documentTypeId,
            'language_id' => $languageId,
            'name' => 'Kontrola jakości',
        ]);
    }

    public function safeDown()
    {
        $documentType1Id = DocumentType::find()->where(['symbol' => 'QUALITY_CONTROL'])->one()->id;
        $this->delete('document_type_translation', ['document_type_id' => $documentType1Id]);

        DocumentTypeSection::deleteAll(['document_type_id' => $documentType1Id]);
        DocumentType::deleteAll(['id' => $documentType1Id]);
        DocumentSection::deleteAll(['symbol' => ['QUALITY_CONTROL_SUMMARY']]);
        DocumentSection::deleteAll(['symbol' => ['QUALITY_CONTROL_DETAILS']]);
    }
}
