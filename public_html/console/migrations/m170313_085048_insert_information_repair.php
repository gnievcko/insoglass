<?php

use yii\db\Migration;
use common\models\ar\Language;
use common\models\ar\InformationPage;

class m170313_085048_insert_information_repair extends Migration {

    public function safeUp() {
    	$languageId = Language::find()->where(['symbol' => 'pl'])->one()->id;
    	 
    	$this->insert('information_page', [
    			'symbol' => 'default_repair_additional_text',
    			'is_active' => 1,
    	]);
    	$informationPageId = $this->db->getLastInsertID();
    	 
    	$this->insert('information_page_translation', [
    			'information_page_id' => $informationPageId,
    			'language_id' => $languageId,
    			'title' => 'Domyślny dodatkowy tekst na protokole naprawy',
    			'content' => '',
    			'is_active' => 1,
    	]);
    }

    public function safeDown() {
    	$ids = InformationPage::find()->where(['symbol' => ['default_repair_additional_text']])->select(['id'])->column();
    	 
    	$this->delete('information_page_translation', ['information_page_id' => $ids]);
    	$this->delete('information_page', ['id' => $ids]);
    }
}
