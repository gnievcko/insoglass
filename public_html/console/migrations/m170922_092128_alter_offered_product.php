<?php

use yii\db\Schema;
use yii\db\Migration;

class m170922_092128_alter_offered_product extends Migration {

    public function up() {
        $this->addColumn('offered_product', 'muntin_id', $this->integer(11));
        $this->addForeignKey('fk_offered_product_muntin', 'offered_product', 'muntin_id', 'muntin', 'id');
        
        $this->addColumn('product', 'muntin_id', $this->integer(11));
        $this->addForeignKey('fk_product_muntin', 'product', 'muntin_id', 'muntin', 'id');
    }

    public function down() {
        $this->dropForeignKey('fk_offered_product_muntin', 'offered_product');
        $this->dropColumn('offered_product', 'muntin_id');
        
        $this->dropForeignKey('fk_product_muntin', 'product');
        $this->dropColumn('product', 'muntin_id');
    }

}
