<?php

use yii\db\Schema;
use yii\db\Migration;

class m170511_085655_alter_order extends Migration {

    public function up() {
        $this->addColumn('order', 'user_responsible_id', $this->integer(11));
        
        $this->addForeignKey('fk_order_user_responsilbe', 'order', 'user_responsible_id', 'user', 'id');
    }

    public function down() {
        $this->dropColumn('order', 'user_responsible_id');
        $this->dropForeignKey('fk_order_user_responsilbe', 'order');
    }
}
