<?php

use yii\db\Migration;
use yii\db\Query;

class m170313_100941_insert_document_section_service_fdafas extends Migration {

	public function safeUp() {
    	$documentTypeId = (new Query())->select(['dt.id'])->from('document_type dt')->where(['dt.symbol' => 'SERVICE_PROTOCOL_FDAFAS'])->scalar();
    	$serviceProtocolId = (new Query())->select(['ds.id'])->from('document_section ds')->where(['ds.symbol' => 'SERVICE_PROTOCOL'])->scalar();
    	 
    	$this->insert('document_section', ['symbol' => 'SERVICE_PROTOCOL_FDAFAS',]);
    	$sectionId = $this->db->getLastInsertID();
    	$this->delete('document_type_section', ['document_type_id' => $documentTypeId, 'document_section_id' => $serviceProtocolId]);
    	$this->insert('document_type_section', ['document_type_id' => $documentTypeId, 'document_section_id' => $sectionId, 'sequence_number' => 2]);
    	
    	$languageId = (new Query())->select(['l.id'])->from('language l')->where(['l.symbol' => 'pl'])->scalar();
    	 
    	$this->insert('information_page', [
    			'symbol' => 'default_service_fdafas_tables',
    			'is_active' => 1,
    	]);
    	$informationPageId = $this->db->getLastInsertID();
    	 
    	$this->insert('information_page_translation', [
    			'information_page_id' => $informationPageId,
    			'language_id' => $languageId,
    			'title' => 'Domyślne produkty w protokole serwisowym systemu sygnalizacji pożaru',
    			'content' => '',
    			'is_active' => 1,
    	]);
    }

    public function safeDown() {
    	$ids = (new Query())->select(['ip.id'])->where(['symbol' => ['default_service_fdafas_tables']])->scalar();
    	 
    	$this->delete('information_page_translation', ['information_page_id' => $ids]);
    	$this->delete('information_page', ['id' => $ids]);
    	
       	$documentTypeId = (new Query())->select(['dt.id'])->from('document_type dt')->where(['dt.symbol' => 'SERVICE_PROTOCOL_FDAFAS'])->scalar();
    	$serviceProtocolId = (new Query())->select(['ds.id'])->from('document_section ds')->where(['ds.symbol' => 'SERVICE_PROTOCOL'])->scalar();
    	$serviceProtocolFdafasId = (new Query())->select(['ds.id'])->from('document_section ds')->where(['ds.symbol' => 'SERVICE_PROTOCOL_FDAFAS'])->scalar();
        
        $this->delete('document_type_section', ['document_type_id' => $documentTypeId, 'document_section_id' => $serviceProtocolFdafasId]);
    	$this->insert('document_type_section', ['document_type_id' => $documentTypeId, 'document_section_id' => $serviceProtocolId, 'sequence_number' => 2]);
    	$this->delete('document_section', ['id' => $serviceProtocolFdafasId]);
    }
}
