<?php

use yii\db\Schema;
use yii\db\Migration;
use common\helpers\Utility;
use common\models\ar\Language;
use common\models\ar\Role;
use common\models\ar\RoleTranslation;
use common\documents\sections\SectionsMapper;
use common\models\ar\DocumentSection;
use common\models\ar\DocumentType;
use common\models\ar\DocumentTypeSection;

class m161017_083045_insert_role extends Migration {

    public function safeUp() {

    	$this->insert('role', [
    			'symbol' => Utility::ROLE_REPAIRER,
    			'is_visible' => 1,
    	]);
    	$roleId = $this->db->getLastInsertID();
    	$languageId = Language::find()->where(['symbol' => 'pl'])->one()->id;
    	
    	$this->insert('role_translation',  [
    			'role_id' => $roleId,
    			'language_id' => $languageId,
    			'name' => 'Serwisant',
    	]);
    	  	
    	$this->addColumn('order', 'duration_time', $this->string(64)->defaultValue(null));
    	$this->addColumn('order', 'execution_time', $this->string(64)->defaultValue(null));
    	$this->addColumn('order', 'payment_terms', $this->string(64)->defaultValue(null));


    	
    	$this->addColumn('company', 'contract_type_id', $this->integer(11)->defaultValue(null));
    	$this->createTable('contract_type', [
    			'id' => $this->primaryKey(11),
    			'symbol' => $this->string(32)->notNull(),
    	]);
    	
    	$this->createTable('contract_type_translation', [
    			'language_id' => $this->integer(11),
    			'contract_type_id' => $this->integer(11),
    			'name' => $this->string(32)->notNull(),
    	]);
    	
    	$this->addForeignKey('fk_company_contract_type', 'company', 'contract_type_id', 'contract_type', 'id');
    	$this->addForeignKey('fk_contract_type_translation_contract_type', 'contract_type_translation', 'contract_type_id', 'contract_type', 'id');
    	$this->addForeignKey('fk_contract_type_translation_language', 'contract_type_translation', 'language_id', 'language', 'id');
    	
    	$languageId = Language::find()->where(['symbol' => 'pl'])->one()->id;
    	$this->insert('contract_type', ['symbol' => Utility::CONTRACT_TYPE_ORDER]);
    	$contractTypeId = Yii::$app->db->lastInsertID;

    	$this->insert('contract_type_translation', [
    			'language_id' => $languageId,
    			'contract_type_id' => $contractTypeId,
    			'name' => 'Zlecenie',
    	]);
    	$this->insert('contract_type', ['symbol' => Utility::CONTRACT_TYPE_CONTRACT]);
    	$contractTypeId = Yii::$app->db->lastInsertID;
    	
    	$this->insert('contract_type_translation', [
    			'language_id' => $languageId,
    			'contract_type_id' => $contractTypeId,
    			'name' => 'Umowa',
    	]);
    	
    	
    	$headerSectionId = DocumentSection::find()->where(['symbol' => 'COMPANY_SHORT_HEADER'])->one()->id;
    	$footerSectionId = DocumentSection::find()->where(['symbol' => 'COMPANY_FOOTER'])->one()->id;
    	
    	$this->insert('document_section', ['symbol' => SectionsMapper::ORDER_REGISTER]);
    	$documentSectionId = $this->db->getLastInsertID();
    	
    	$this->insert('document_type', [
    			'symbol' => SectionsMapper::ORDER_REGISTER,
    			'document_section_header_id' => $headerSectionId,
    			'document_section_footer_id' => $footerSectionId,
    			'margin_top' => 50,
    			'margin_bottom' => 50,
    			'margin_left' => 15,
    			'margin_right' => 15,
    			'format' => 'A4',
    			'orientation' => 'P',
    	]);
    	
    	$documentTypeId = $this->db->getLastInsertId();
    	$this->insert('document_type_section', [
    			'document_type_id' => $documentTypeId,
    			'document_section_id' => $documentSectionId,
    			'sequence_number' => 1,
    	]);
    	
    }

    public function safeDown() {
		$role = Role::findOne(['symbol' => Utility::ROLE_REPAIRER]); 
    	RoleTranslation::deleteAll(['role_id' => $role->id]);
    	$role->delete();	
    	
    	$this->dropColumn('order', 'duration_time');
    	$this->dropColumn('order', 'execution_time');
    	$this->dropColumn('order', 'payment_terms');	
	
    	$this->dropForeignKey('fk_company_contract_type', 'company');
    	$this->dropForeignKey('fk_contract_type_translation_contract_type', 'contract_type_translation');
    	$this->addForeignKey('fk_contract_type_translation_language', 'contract_type_translation');
    	
    	$this->dropTable('contract_type_translation');
    	$this->dropTable('contract_type');

    	$this->dropColumn('company', 'contract_type_id');
    			
    	$documentTypeId = DocumentType::find()->where(['symbol' => SectionsMapper::ORDER_REGISTER])->one()->id;
    	
    	DocumentTypeSection::deleteAll(['document_type_id' => [$documentTypeId]]);
    	DocumentType::deleteAll(['id' => [$documentTypeId]]);
    	DocumentSection::deleteAll(['symbol' => [SectionsMapper::ORDER_REGISTER]]);  	
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp() {
    }

    public function safeDown() {
    }
    */
}
