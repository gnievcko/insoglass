<?php

use yii\db\Schema;
use yii\db\Migration;
use common\models\ar\ProductStatus;
use common\helpers\Utility;

class m160817_065400_alter_product_status extends Migration {

    public function up() {
    	$this->addColumn('product_status', 'is_positive', 'BOOLEAN NOT NULL DEFAULT 0');
    	ProductStatus::updateAll(['is_positive' => 0]);
    	ProductStatus::updateAll(['is_positive' => 1], ['symbol' => Utility::PRODUCT_STATUS_ADDITION]);
    }

    public function down() {
        $this->dropColumn('product_status', 'is_positive');
    }

}
