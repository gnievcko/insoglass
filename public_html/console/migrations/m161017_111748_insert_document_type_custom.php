<?php

use yii\db\Schema;
use yii\db\Migration;
use common\models\ar\DocumentType;
use common\models\ar\DocumentSection;
use common\models\ar\DocumentTypeSection;
use common\documents\sections\SectionsMapper;

class m161017_111748_insert_document_type_custom extends Migration {

    public function safeUp() {
    	$footerId = DocumentSection::find()->where(['symbol' => SectionsMapper::COMPANY_FOOTER])->one()->id;
    	$headerId = DocumentSection::find()->where(['symbol' => SectionsMapper::COMPANY_SHORT_HEADER])->one()->id;
    	
    	$this->insert('document_type', [
    			'symbol' => 'CUSTOM_PROTOCOL',
    			'document_section_footer_id' => $footerId,
    			'margin_top' => 15,
    			'margin_bottom' => 50,
    			'margin_left' => 15,
    			'margin_right' => 15,
    			'format' => 'A4',
    			'orientation' => 'P'
    	]);
    	$documentTypeId = $this->db->getLastInsertID();
    	
    	
    	$this->insert('document_section', [ 'symbol' => 'TITLE_WITH_DATE_EDITABLE']);
    	$documentSectionId1 = $this->db->getLastInsertID();
    	$this->insert('document_section', [ 'symbol' => 'CUSTOM_TEXT']);
    	$documentSectionId2 = $this->db->getLastInsertID();
    	
    	$this->insert('document_type_section', [
    			'document_type_id' => $documentTypeId,
    			'document_section_id' => $headerId,
    			'sequence_number' => 1,
    	]);
    	
    	$this->insert('document_type_section', [
    			'document_type_id' => $documentTypeId,
    			'document_section_id' => $documentSectionId1,
    			'sequence_number' => 2,
    	]);
    	
    	$this->insert('document_type_section', [
    			'document_type_id' => $documentTypeId,
    			'document_section_id' => $documentSectionId2,
    			'sequence_number' => 3,
    	]);
    }

    public function safeDown() {
       	$documentTypeId = DocumentType::find()->where(['symbol' => 'CUSTOM_PROTOCOL'])->one()->id;
       	$documentSectionId1 = DocumentSection::find()->where(['symbol' => 'TITLE_WITH_DATE_EDITABLE'])->one()->id;
       	$documentSectionId2 = DocumentSection::find()->where(['symbol' => 'CUSTOM_TEXT'])->one()->id;
       	
       	DocumentTypeSection::deleteAll(['document_type_id' => $documentTypeId]);
       	DocumentSection::deleteAll(['in', 'id', [$documentSectionId1, $documentSectionId2]]);
       	DocumentType::deleteAll(['id' => $documentTypeId]);
    }
}
