<?php

use yii\db\Schema;
use yii\db\Migration;

class m160808_050152_alter_task extends Migration {

    public function up() {
		$this->addColumn('task', 'title', 'VARCHAR(128) NOT NULL AFTER user_id');
    }

    public function down() {
		$this->dropColumn('task', 'title');
    }

}
