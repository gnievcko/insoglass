<?php

use yii\db\Schema;
use yii\db\Migration;
use yii\db\Query;

class m161014_070933_alter_order extends Migration {

    public function up() {   	
    	$entityId = (new Query())
    			->select(['id'])
    			->from('entity')
    			->orderBy('id')
    			->one();
    	
    	if(!empty($entityId)) {
    		$entityId = $entityId['id'];
    	}
    	else {
    		$entityId = 1;
    	}
    		
    	$this->addColumn('`order`', 'entity_id', 'INT(11) AFTER user_representative_id');
    	$this->addForeignKey('fk_order_entity', 'order', 'entity_id', 'entity', 'id');

    	$this->createTable('application_cache', [
    			'id' => $this->primaryKey(11),
    			'date_last_invoice' => $this->timestamp()->defaultValue(null),
    	]);
    	
    	$this->insert('application_cache', ['date_last_invoice' => '1989-12-13 10:00:00']);
    }

    public function down() {
    	$this->dropForeignKey('fk_order_entity', 'order');
        $this->dropColumn('order', 'entity_id');
        $this->dropTable('application_cache');
        
   
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp() {
    }

    public function safeDown() {
    }
    */
}
