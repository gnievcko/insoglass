<?php

use yii\db\Schema;
use yii\db\Migration;
use common\models\ar\DocumentTypeSection;
use common\models\ar\DocumentType;
use common\models\ar\DocumentSection;

class m161110_093601_insert_document_type_section extends Migration {

    public function up() {
    	$footerSectionId = DocumentSection::find()->where(['symbol' => 'COMPANY_FOOTER'])->one()->id;
        $headerSectionId = DocumentSection::find()->where(['symbol' => 'COMPANY_SHORT_HEADER'])->one()->id;
        
        
        $this->insert('document_section', ['symbol' => 'LOST_CLIENT']);
    	$lostClientTitleId = $this->db->getLastInsertID();
        
        $this->insert('document_type', [
    			'symbol' => 'LOST_CLIENT',
    			'document_section_header_id' => null,
    			'document_section_footer_id' => $footerSectionId,
    			'margin_top' => 15,
    			'margin_bottom' => 50,
    			'margin_left'=> 15,
    			'margin_right' => 15,
    			'format' => 'A4',
    			'orientation' => 'P',
    	]);
        
        $documentTypeId = $this->db->getLastInsertId();
        
        $this->insert('document_type_section', [
    			'document_type_id' => $documentTypeId,
				'document_section_id' => $headerSectionId,
				'sequence_number' => 1,
		]);
        
        $this->insert('document_type_section', [
    			'document_type_id' => $documentTypeId,
				'document_section_id' => $lostClientTitleId,
				'sequence_number' => 2,
		]);
    }

    public function down() {

    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp() {
    }

    public function safeDown() {
    }
    */
}
