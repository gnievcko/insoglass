<?php

use yii\db\Schema;
use yii\db\Migration;

class m161104_105428_product_count_allow_null extends Migration {

    public function up() {
        $this->alterColumn('order_offered_product', 'count', $this->integer(10)->null());
    }

    public function down() {
        $this->alterColumn('order_offered_product', 'count', $this->integer(10)->notNull());
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp() {
    }

    public function safeDown() {
    }
    */
}
