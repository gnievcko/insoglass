<?php

use yii\db\Schema;
use yii\db\Migration;

class m170824_062612_alter_order extends Migration {

    public function up() {
        $this->addColumn('order', 'is_tasks_generated', $this->boolean()->defaultValue(false));
    }

    public function down() {
        $this->dropColumn('order', 'is_tasks_generated');
    }

}
