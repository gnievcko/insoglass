<?php

use yii\db\Schema;
use yii\db\Migration;
use common\helpers\Utility;
use common\models\ar\Language;

class m161018_075546_create_order_type extends Migration {

    public function up() {
    	$this->addColumn('order', 'order_type_id', $this->integer(11)->defaultValue(null));
    	$this->createTable('order_type', [
    			'id' => $this->primaryKey(11),
    			'symbol' => $this->string(32)->notNull(),
    	]);
    	 
    	$this->createTable('order_type_translation', [
    			'language_id' => $this->integer(11),
    			'order_type_id' => $this->integer(11),
    			'name' => $this->string(32)->notNull(),
    	]);
    	 
    	$this->addForeignKey('fk_order_order_type', 'order', 'order_type_id', 'order_type', 'id');
    	$this->addForeignKey('fk_order_type_translation_order_type', 'order_type_translation', 'order_type_id', 'order_type', 'id');
    	$this->addForeignKey('fk_order_type_translation_language', 'order_type_translation', 'language_id', 'language', 'id');
    	
    	$languageId = Language::find()->where(['symbol' => 'pl'])->one()->id;
    	$this->insert('order_type', ['symbol' => Utility::ORDER_TYPE_MALFUNCTION_FIXING]);
    	$orderTypeId = Yii::$app->db->lastInsertID;
    	
    	$this->insert('contract_type_translation', [
    			'language_id' => 1,
    			'contract_type_id' => 1,
    			'name' => 'Usuniecie awarii',
    	]);
    	
    }

    public function down() {
    	$this->dropForeignKey('fk_order_order_type', 'order');
    	$this->dropForeignKey('fk_order_type_translation_order_type', 'order_type_translation');
    	$this->dropForeignKey('fk_order_type_translation_language', 'order_type_translation');
    	 
    	$this->dropTable('order_type_translation');
    	$this->dropTable('order_type');
    	
    	$this->dropColumn('order', 'order_type_id');
    	
    	
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp() {
    }

    public function safeDown() {
    }
    */
}
