<?php

use yii\db\Schema;
use yii\db\Migration;

class m160808_120614_alter_task extends Migration {

    public function up() {
		$this->renameColumn('task', 'date_task_execution', 'date_reminder');
    }

    public function down() {
        $this->renameColumn('task', 'date_reminder', 'date_task_execution');
    }
}
