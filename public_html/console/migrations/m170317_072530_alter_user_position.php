<?php

use yii\db\Migration;

class m170317_072530_alter_user_position extends Migration {

    public function safeUp() {
		$this->addColumn('user', 'position', 'VARCHAR(256) NULL DEFAULT NULL AFTER currency_id');
    }

    public function safeDown() {
        $this->dropColumn('user', 'position');
    }

}
