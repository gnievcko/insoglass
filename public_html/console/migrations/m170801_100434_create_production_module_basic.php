<?php

use yii\db\Schema;
use yii\db\Migration;

class m170801_100434_create_production_module_basic extends Migration {

    public function safeUp() {
    	$tableOptions = null;
    	if($this->db->driverName === 'mysql') {
    		$tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
    	}
    	
    	$this->createTable('department_type', [
    			'id' => $this->primaryKey(11),
    			'symbol' => $this->string(32)->notNull()->unique(),
    	], $tableOptions);
    	 
    	$this->createTable('department_type_translation', [
    			'department_type_id' => $this->integer(11),
    			'language_id' => $this->integer(11),
    			'name' => $this->string(128)->notNull(),
    	], $tableOptions);
    	 
    	$this->addPrimaryKey('pk_department_type_translation', 'department_type_translation', ['department_type_id', 'language_id']);
    	$this->addForeignKey('fk_department_type_translation_language', 'department_type_translation', 'language_id', 'language', 'id');
    	$this->addForeignKey('fk_department_type_translation_type', 'department_type_translation', 'department_type_id', 'department_type', 'id');
    	 
    	$this->createTable('department', [
    			'id' => $this->primaryKey(11),
    			'department_type_id' => $this->integer(11)->notNull(),
    			'symbol' => $this->string(32)->notNull()->unique(),
    	], $tableOptions);
    	
    	$this->addForeignKey('fk_department_department_type', 'department', 'department_type_id', 'department_type', 'id');
    	
    	$this->createTable('department_translation', [
    			'department_id' => $this->integer(11),
    			'language_id' => $this->integer(11),
    			'name' => $this->string(256)->notNull(),
    	], $tableOptions);
    	
    	$this->addPrimaryKey('pk_department_translation', 'department_translation', ['department_id', 'language_id']);
    	$this->addForeignKey('fk_department_translation_language', 'department_translation', 'language_id', 'language', 'id');
    	$this->addForeignKey('fk_department_translation_department', 'department_translation', 'department_id', 'department', 'id');
    	
    	$this->createTable('user_department', [
    			'user_id' => $this->integer(11),
    			'deparment_id' => $this->integer(11),
    	], $tableOptions);
    	
    	$this->addPrimaryKey('pk_user_deparment', 'user_department', ['user_id', 'deparment_id']);
    	$this->addForeignKey('fk_user_deparment_user', 'user_department', 'user_id', 'user', 'id');
    	$this->addForeignKey('fk_user_department_department', 'user_department', 'deparment_id', 'department', 'id');
    	
    	$this->createTable('workstation', [
    			'id' => $this->primaryKey(11),
    			'department_id' => $this->integer(11)->notNull(),
    			'symbol' => $this->string(32)->notNull()->unique(),
    	], $tableOptions);
    	 
    	$this->addForeignKey('fk_workstation_department', 'workstation', 'department_id', 'department', 'id');
    	 
    	$this->createTable('workstation_translation', [
    			'workstation_id' => $this->integer(11),
    			'language_id' => $this->integer(11),
    			'name' => $this->string(128)->notNull(),
    	], $tableOptions);
    	 
    	$this->addPrimaryKey('pk_workstation_translation', 'workstation_translation', ['workstation_id', 'language_id']);
    	$this->addForeignKey('fk_workstation_translation_language', 'workstation_translation', 'language_id', 'language', 'id');
    	$this->addForeignKey('fk_workstation_translation_workstation', 'workstation_translation', 'workstation_id', 'workstation', 'id');
    	 
    	$this->createTable('user_workstation', [
    			'user_id' => $this->integer(11),
    			'workstation_id' => $this->integer(11),
    	], $tableOptions);
    	 
    	$this->addPrimaryKey('pk_user_workstation', 'user_workstation', ['user_id', 'workstation_id']);
    	$this->addForeignKey('fk_user_workstation_user', 'user_workstation', 'user_id', 'user', 'id');
    	$this->addForeignKey('fk_user_workstation_workstation', 'user_workstation', 'workstation_id', 'workstation', 'id');
    	 
    	$this->createTable('activity', [
    			'id' => $this->primaryKey(11),
    			'symbol' => $this->string(32)->notNull()->unique(),
    			'effort_factor' => $this->integer(7),
    	], $tableOptions);
    	 
    	$this->createTable('activity_translation', [
    			'activity_id' => $this->integer(11),
    			'language_id' => $this->integer(11),
    			'name' => $this->string(256)->notNull(),
    	], $tableOptions);
    	 
    	$this->addPrimaryKey('pk_activity_translation', 'activity_translation', ['activity_id', 'language_id']);
    	$this->addForeignKey('fk_activity_translation_language', 'activity_translation', 'language_id', 'language', 'id');
    	$this->addForeignKey('fk_activity_translation_activity', 'activity_translation', 'activity_id', 'activity', 'id');
    	 
    	$this->createTable('department_activity', [
    			'deparment_id' => $this->integer(11),
    			'activity_id' => $this->integer(11),
    	], $tableOptions);
    	 
    	$this->addPrimaryKey('pk_department_activity', 'department_activity', ['deparment_id', 'activity_id']);
    	$this->addForeignKey('fk_department_activity_department', 'department_activity', 'deparment_id', 'department', 'id');
    	$this->addForeignKey('fk_department_activity_activity', 'department_activity', 'activity_id', 'activity', 'id');
    }

    public function safeDown() {
        $this->dropTable('department_activity');
        $this->dropTable('activity_translation');
        $this->dropTable('activity');
        $this->dropTable('user_workstation');
        $this->dropTable('workstation_translation');
        $this->dropTable('workstation');
        $this->dropTable('user_department');
        $this->dropTable('department_translation');
        $this->dropTable('department');
        $this->dropTable('department_type_translation');
        $this->dropTable('department_type');
    }
}
