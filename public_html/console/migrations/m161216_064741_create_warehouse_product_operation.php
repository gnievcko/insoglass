<?php

use yii\db\Migration;
use yii\db\Expression;
use common\models\aq\WarehouseQuery;
use yii\db\Query;

class m161216_064741_create_warehouse_product_operation extends Migration {

    public function up() {
    	$tableOptions = null;
    	if($this->db->driverName === 'mysql') {
    		$tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
    	}
    	
    	$warehouseId = WarehouseQuery::getDefaultWarehouse()->one()->id;
    	
    	$this->createTable('warehouse_product_operation', [
    			'id' => $this->primaryKey(11),
    			'warehouse_id' => $this->integer(11)->notNull(),
    			'product_status_id' => $this->integer(11)->notNull(),
    			'user_confirming_id' => $this->integer(11)->notNull(),
    			'description' => $this->string(256),
    			'date_creation' => $this->timestamp()->notNull(),
    			'number' => $this->string(48),
    			'is_accounted' => $this->boolean(),
    	], $tableOptions);
    	    	
    	$this->addForeignKey('fk_warehouse_product_operation_warehouse', 'warehouse_product_operation', 'warehouse_id', 'warehouse', 'id');
    	$this->addForeignKey('fk_warehouse_product_operation_product_status', 'warehouse_product_operation', 'product_status_id', 'product_status', 'id');
    	$this->addForeignKey('fk_warehouse_product_operation_user_confirming', 'warehouse_product_operation', 'user_confirming_id', 'user', 'id');
    	$this->alterColumn('warehouse_product_operation', 'date_creation', 'TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP');
    	
    	$this->addColumn('warehouse_product_status_history', 'operation_id', 'INT(11) NULL DEFAULT NULL AFTER id');
    	
		$histories = (new Query())->select(['product_status_id', 'user_confirming_id', 'date_creation', 'description', new Expression('GROUP_CONCAT(id) as ids')])
				->from('warehouse_product_status_history')
				->groupBy(['product_status_id', 'user_confirming_id', 'date_creation', 'description'])
				->all();
		
		if(!empty($histories)) {
			foreach($histories as $history) {
				$this->insert('warehouse_product_operation', [
						'warehouse_id' => $warehouseId,
						'product_status_id' => $history['product_status_id'],
						'user_confirming_id' => $history['user_confirming_id'],
						'date_creation' => $history['date_creation'],
						'description' => $history['description'],
				]);
				$operationId = $this->db->getLastInsertID();
				
				$this->update('warehouse_product_status_history', ['operation_id' => $operationId], ['id' => explode(',', $history['ids'])]);
			}
		}
    	
		$this->alterColumn('warehouse_product_status_history', 'operation_id', 'INT(11) NOT NULL AFTER id');
		$this->addForeignKey('fk_warehouse_product_status_history_operation', 'warehouse_product_status_history', 'operation_id', 'warehouse_product_operation', 'id');
    	
    	$this->addColumn('warehouse_delivery', 'operation_id', 'INT(11) NULL DEFAULT NULL');
    	$this->addForeignKey('fk_warehouse_delivery_operation', 'warehouse_delivery', 'operation_id', 'warehouse_product_operation', 'id');
    }

    public function down() {
    	$this->dropForeignKey('fk_warehouse_delivery_operation', 'warehouse_delivery');
        $this->dropColumn('warehouse_delivery', 'operation_id');
        
        $this->dropForeignKey('fk_warehouse_product_status_history_operation', 'warehouse_product_status_history');
        $this->dropColumn('warehouse_product_status_history', 'operation_id');
        
        $this->dropTable('warehouse_product_operation');
    }

}
