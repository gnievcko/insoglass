<?php

use yii\db\Schema;
use yii\db\Migration;

class m170119_120331_alter_order extends Migration {

    public function safeUp() {
		$this->addColumn('order', 'description_note', 'TEXT NULL DEFAULT NULL AFTER description');
    }

    public function safeDown() {
        $this->dropColumn('order', 'description_note', 'TEXT NULL DEFAULT NULL');        
    }
}
