<?php

use yii\db\Migration;

class m160930_101605_alter_warehouse_product extends Migration {

    public function up() {
		$this->addColumn('warehouse_product', 'stillage', $this->string(20));
		$this->addColumn('warehouse_product', 'shelf', $this->string(20));
    }

    public function down() {
		$this->dropColumn('warehouse_product', 'shelf');
		$this->dropColumn('warehouse_product', 'stillage');
    }
}
