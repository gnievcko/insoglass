<?php
use yii\db\Schema;
use yii\db\Migration;
use common\models\ar\DocumentSection;
use common\models\ar\DocumentType;
use common\models\ar\DocumentTypeSection;

class m161003_065042_insert_repair_protocol_document extends Migration {

    public function up() {
        $headerSectionId = DocumentSection::find()->where(['symbol' => 'COMPANY_SHORT_HEADER'])->one()->id;
        $footerSectionId = DocumentSection::find()->where(['symbol' => 'COMPANY_FOOTER'])->one()->id;

        $this->insert('document_section', ['symbol' => 'REPAIR_PROTOCOL']);
        $repairId = $this->db->getLastInsertID();

        $this->insert('document_type', [
            'symbol' => 'REPAIR_PROTOCOL',
            'document_section_header_id' => null,
            'document_section_footer_id' => $footerSectionId,
            'margin_top' => 15,
            'margin_bottom' => 50,
            'margin_left' => 15,
            'margin_right' => 15,
            'format' => 'A4',
            'orientation' => 'P',
        ]);
        
        $documentTypeId = $this->db->getLastInsertId();
        $this->insert('document_type_section', [
            'document_type_id' => $documentTypeId,
            'document_section_id' => $headerSectionId,
            'sequence_number' => 1,
        ]);
        $this->insert('document_type_section', [
            'document_type_id' => $documentTypeId,
            'document_section_id' => $repairId,
            'sequence_number' => 2,
        ]);
    }

    public function down() {
        $documentType1Id = DocumentType::find()->where(['symbol' => 'REPAIR_PROTOCOL'])->one()->id;

        DocumentTypeSection::deleteAll(['document_type_id' => [$documentType1Id]]);
        DocumentType::deleteAll(['id' => [$documentType1Id]]);
        DocumentSection::deleteAll(['symbol' => ['REPAIR_PROTOCOL']]);
    }

}

