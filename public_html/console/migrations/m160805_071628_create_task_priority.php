<?php

use yii\db\Schema;
use yii\db\Migration;
use common\models\ar\Language;

class m160805_071628_create_task_priority extends Migration {

    public function up() {
    	$tableOptions = null;
    	if($this->db->driverName === 'mysql') {
    		$tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
    	}
    	 
    	$this->createTable('task_priority', [
    			'id' => $this->primaryKey(11),
    			'symbol' => $this->string(32)->notNull()->unique(),
    			'priority' => $this->integer(3)->notNull(),
    	], $tableOptions);
    	 
    	$this->createTable('task_priority_translation', [
    			'task_priority_id' => $this->integer(11),
    			'language_id' => $this->integer(11),
    			'name' => $this->string(256)->notNull(),
    	], $tableOptions);
    	
    	$this->addPrimaryKey('pk_task_priority_translation', 'task_priority_translation', ['task_priority_id', 'language_id']);
    	$this->addForeignKey('fk_task_priority_translation_language', 'task_priority_translation', 'language_id', 'language', 'id');
    	$this->addForeignKey('fk_task_priority_translation_priority', 'task_priority_translation', 'task_priority_id', 'task_priority', 'id');
    	 
    	$this->addColumn('task', 'task_priority_id', 'INT(11) NOT NULL AFTER task_type_id');
    	$this->addForeignKey('fk_task_task_priority', 'task', 'task_priority_id', 'task_priority', 'id');
    	
    	$languageId = Language::find()->where(['symbol' => 'pl'])->one()->id;
    	$this->insert('task_priority', ['symbol' => 'low', 'priority' => 10]);
    	$this->insert('task_priority_translation', ['task_priority_id' => $this->db->getLastInsertID(), 'language_id' => $languageId, 'name' => 'Niski']);
    	$this->insert('task_priority', ['symbol' => 'medium', 'priority' => 20]);
    	$this->insert('task_priority_translation', ['task_priority_id' => $this->db->getLastInsertID(), 'language_id' => $languageId, 'name' => 'Średni']);
    	$this->insert('task_priority', ['symbol' => 'high', 'priority' => 30]);
    	$this->insert('task_priority_translation', ['task_priority_id' => $this->db->getLastInsertID(), 'language_id' => $languageId, 'name' => 'Wysoki']);
    }

    public function down() {
		$this->dropForeignKey('fk_task_task_priority', 'task');
		$this->dropColumn('task', 'task_priority_id');
		$this->dropTable('task_priority_translation');
		$this->dropTable('task_priority');
    }

}
