<?php

use yii\db\Migration;
use yii\db\Query;

class m170223_101638_insert_document_type_sap extends Migration {

    public function safeUp() {
    	$languageId = (new Query())->select(['id'])->from('language')->where(['symbol' => 'pl'])->scalar();
    	$footerId = (new Query())->select(['id'])->from('document_section')->where(['symbol' => 'COMPANY_FOOTER'])->scalar();
    	
    	$this->insert('document_type', [
    			'symbol' => 'SERVICE_PROTOCOL_FDAFAS',
    			'document_section_footer_id' => $footerId,
    			'margin_top' => 15,
    			'margin_bottom' => 50,
    			'margin_left' => 15,
    			'margin_right' => 15,
    			'is_for_order' => 1,
    	]);
    	$documentTypeId = $this->db->getLastInsertID();
    	$this->insert('document_type_translation', [
    			'document_type_id' => $documentTypeId,
    			'language_id' => $languageId,
    			'name' => 'Protokół serwisowy systemu sygnalizacji pożaru',
		]);
    	
    	$documentSections = (new Query())->select(['dts.document_section_id', 'dts.sequence_number'])
    			->from('document_type dt')
    			->join('INNER JOIN', 'document_type_section dts', 'dt.id = dts.document_type_id')
    			->where(['dt.symbol' => 'SERVICE_PROTOCOL'])
    			->all();
    	
    	foreach($documentSections as $ds) {
    		$this->insert('document_type_section', [
    				'document_type_id' => $documentTypeId,
    				'document_section_id' => $ds['document_section_id'],
    				'sequence_number' => $ds['sequence_number'],
    		]);
    	}
    }

    public function safeDown() {
        $typeId = (new Query())->select(['id'])->from('document_type')->where(['symbol' => 'SERVICE_PROTOCOL_FDAFAS'])->scalar();
        
        $this->delete('document_type_section', ['document_type_id' => $typeId]);
        $this->delete('document_type_translation', ['document_type_id' => $typeId]);
        $this->delete('document_type', ['id' => $typeId]);
    }
}
