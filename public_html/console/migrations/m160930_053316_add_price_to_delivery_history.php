<?php

use yii\db\Migration;

class m160930_053316_add_price_to_delivery_history extends Migration {

	public function up() {
		$this->addColumn('warehouse_product_status_history', 'price_unit', $this->decimal(12, 2)->null());
		$this->addColumn('warehouse_product_status_history', 'price_group', $this->decimal(12, 2)->null());
		$this->addColumn('warehouse_product_status_history', 'currency_id', $this->integer(11));
		
		$this->addForeignKey('fk_warehouse_product_status_history_currency', 
				'warehouse_product_status_history', 'currency_id', 'currency', 'id');
	}
	
	public function down() {
		$this->dropForeignKey('fk_warehouse_product_status_history_currency', 'warehouse_product_status_history');
		$this->dropIndex('fk_warehouse_product_status_history_currency', 'warehouse_product_status_history');
		
		$this->dropColumn('warehouse_product_status_history', 'currency_id');
		$this->dropColumn('warehouse_product_status_history', 'price_unit');
		$this->dropColumn('warehouse_product_status_history', 'price_group');
	}
}
