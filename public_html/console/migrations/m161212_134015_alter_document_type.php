<?php

use yii\db\Schema;
use yii\db\Migration;
use common\models\ar\Language;
use common\models\ar\DocumentType;

class m161212_134015_alter_document_type extends Migration {

    public function up() {
		$this->addColumn('document_type', 'is_for_order', 'TINYINT(1) NOT NULL DEFAULT 0');
		$this->addColumn('document_type', 'is_for_warehouse', 'TINYINT(1) NOT NULL DEFAULT 0');
		$this->addColumn('document_type', 'is_for_report', 'TINYINT(1) NOT NULL DEFAULT 0');
		
		$this->update('document_type', ['is_for_order' => 1], ['symbol' => [
				'INVOICE', 'ACCEPTANCE_PROTOCOL', 'OFFER_DETAILS', 'MALFUNCTION_PROTOCOL', 'FIREEQ_ACCEPTANCE_PROTOCOL', 
				'LIGHT_MEASUREMENT', 'SERVICE_PROTOCOL', 'WEIGHT_PROTOCOL', 'REPAIR_PROTOCOL', 'CUSTOM_PROTOCOL'
		]]);
		
		$this->update('document_type', ['is_for_warehouse' => 1], ['symbol' => [
				'GOODS_RECEIVED', 'GOODS_ISSUED', 'DELIVERY_DETAILS', 
		]]);
		
		$this->update('document_type', ['is_for_report' => 1], ['symbol' => [
				'REPORT_ORDER', 'REPORT_UTILIZATION', 'ORDER_REGISTER', 'TASK_REPORT', 'GOODS_ISSUED_REPORT', 
				'LOST_CLIENT', 'LIST_OF_ITEMS', 'GOODS_DELIVERED_REPORT', 'SALES_RESULT_REPORT'
		]]);
		
		$languageId = Language::find()->where(['symbol' => 'pl'])->one()->id;
		
		$this->insert('document_type_translation', [
				'document_type_id' => DocumentType::find()->where(['symbol' => 'ORDER_REGISTER'])->one()->id,
				'language_id' => $languageId,
				'name' => 'Rejestr zleceń',
		]);
		
		$this->insert('document_type_translation', [
				'document_type_id' => DocumentType::find()->where(['symbol' => 'TASK_REPORT'])->one()->id,
				'language_id' => $languageId,
				'name' => 'Raport z zadań',
		]);
		
		$this->insert('document_type_translation', [
				'document_type_id' => DocumentType::find()->where(['symbol' => 'GOODS_ISSUED_REPORT'])->one()->id,
				'language_id' => $languageId,
				'name' => 'Raport z wydań',
		]);
		
		$this->insert('document_type_translation', [
				'document_type_id' => DocumentType::find()->where(['symbol' => 'GOODS_DELIVERED_REPORT'])->one()->id,
				'language_id' => $languageId,
				'name' => 'Raport z dostaw',
		]);
		
		$this->insert('document_type_translation', [
				'document_type_id' => DocumentType::find()->where(['symbol' => 'LOST_CLIENT'])->one()->id,
				'language_id' => $languageId,
				'name' => 'Raport z utraconych klientów',
		]);
		
		$this->insert('document_type_translation', [
				'document_type_id' => DocumentType::find()->where(['symbol' => 'LIST_OF_ITEMS'])->one()->id,
				'language_id' => $languageId,
				'name' => 'Spis z natury',
		]);
		
		$this->insert('document_type_translation', [
				'document_type_id' => DocumentType::find()->where(['symbol' => 'SALES_RESULT_REPORT'])->one()->id,
				'language_id' => $languageId,
				'name' => 'Wyniki sprzedaży',
		]);
    }

    public function down() {
		$this->dropColumn('document_type', 'is_for_order');
		$this->dropColumn('document_type', 'is_for_warehouse');
		$this->dropColumn('document_type', 'is_for_report');
    }
}
