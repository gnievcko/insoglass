<?php

use yii\db\Migration;

class m170220_132313_update_task_reference extends Migration {

    public function safeUp() {
		$this->update('task_type_reference', ['url_details' => 'client/details/{id}'], ['name_table' => 'company']);
    }

    public function safeDown() {
        
    }
}
