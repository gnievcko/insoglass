<?php

use yii\db\Schema;
use yii\db\Migration;
use common\helpers\Utility;
use yii\db\Query;
class m160824_112024_insert_email_template extends Migration {
	
	public function up() {
		
		$this->insert('email_template', [
				'email_type_id' => (new Query())->select(['id'])->from('email_type')->where(['symbol' => Utility::EMAIL_TYPE_RESET_PASSWORD])->one()['id'],
				'language_id' => (new Query())->select(['id'])->from('language')->where(['symbol' => 'pl'])->one()['id'],
				'subject' => 'Odzyskiwanie hasła',
				'content_html' => '<html>'
						.'<head>'
						.'<title></title>'
						.'</head>'
						.'<body>'
						.'<h2>Witaj {firstname} {lastname}</h2>'
						.'Aby ustawic nowe haslo prosze otworzyc ponizszy link:<br />'
						.'{link_with_token}<br />'
						.'<br />'
						.'&nbsp;'
						.'<hr /> <em><span style="font-size:12px;">Ten email zostal wyslany automatycznie, prosze na niego nie odpowiadac.</span></em></body>'
						.'</html>',
				'content_text' => 'Witaj {firstname} {lastname}'
						.''
						.'Aby ustawic nowe haslo prosze otworzyc ponizszy link:'
						.'{link_with_token}'
						.''
						.'Ten email zostal wyslany automatycznie, prosze na niego nie odpowiadac.',
		]);
	}
	
	public function down() {
		$this->delete('email_type', [
				'email_type_id' => (new Query())->select(['id'])->from('email_type')->where(['symbol' => Utility::EMAIL_TYPE_RESET_PASSWORD])->one()['id'],
				'language_id' => (new Query())->select(['id'])->from('language')->where(['symbol' => 'pl'])->one()['id'],
		]);
	}

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp() {
    }

    public function safeDown() {
    }
    */
}
