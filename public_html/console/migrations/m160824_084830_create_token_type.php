<?php

use yii\db\Schema;
use yii\db\Migration;
use common\helpers\Utility;

class m160824_084830_create_token_type extends Migration {
	private $tableName = 'token_type';
	
    public function up() {
    	$tableOptions = null;
    	if($this->db->driverName === 'mysql') {
    		$tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
    	}
    	
    	$this->createTable($this->tableName, [
    			'id' => $this->primaryKey(11),
    			'symbol' => $this->string(50)->notNull(),
    	], $tableOptions);
    	$this->insert($this->tableName, ['symbol' => Utility::TOKEN_TYPE_RESET_PASSWORD]);
    }

    public function down() {
        $this->dropTable($this->tableName);
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp() {
    }

    public function safeDown() {
    }
    */
}
