<?php

use yii\db\Schema;
use yii\db\Migration;
use common\helpers\Utility;
use console\controllers\RbacController;

class m160701_161458_init_basic_tables_data extends Migration {

    public function up() {
    	$this->insert('language', [
    			'id' => 1,
    			'symbol' => 'pl',
    			'name' => 'Polski',
    			'is_active' => true
    	]);
    	$languageId = $this->db->getLastInsertID();
    	 
    	$this->insert('currency', [
    			'id' => 1,
    			'symbol' => 'PLN',
    			'short_symbol' => 'zł',
    			'conversion_value' => 1.0
    	]);
    	$currencyId = $this->db->getLastInsertID();
    	 
    	$this->insert('role', [
    			'id' => 1,
    			'symbol' => 'admin'
    	]);
    	
    	$this->insert('role_translation', [
    			'role_id' => 1,
    			'language_id' => 1,
    			'name' => 'Administrator'
    	]);
    	
    	$this->insert('role', [
    			'id' => 2,
    			'symbol' => 'client'
    	]);
    	
    	$this->insert('role_translation', [
    			'role_id' => 2,
    			'language_id' => 1,
    			'name' => 'Klient'
    	]);
    	 
    	$this->insert('email_config', [
    			'id' => 1,
    			'email' => 'testmail@mindseater.com',
    			'password' => 'tmME2014**',
    			'port' => 25,
    			'protocol' => 'smtp',
    			'host' => 'mail.mindseater.com',
    			'starttls' => false,
    			'smtp_auth' => true,
    			'noreply_email' => 'noreply@mindseater.com'
    	]);
    	 
    	$this->insert('country', [
    			'id' => 1,
    			'symbol' => 'PL',
    			'priority' => 1
    	]);
    	 
    	$this->insert('country_translation', [
    			'country_id' => 1,
    			'language_id' => $languageId,
    			'name' => 'Polska'
    	]);
    	 
    	$this->insert('province', [
    			'id' => 1,
    			'symbol' => 'DS',
    			'name' => 'Dolnośląskie',
    			'country_id' => 1
    	]);
    	 
    	$this->insert('province', [
    			'id' => 2,
    			'symbol' => 'KP',
    			'name' => 'Kujawsko-Pomorskie',
    			'country_id' => 1
    	]);
    	 
    	$this->insert('province', [
    			'id' => 3,
    			'symbol' => 'LB',
    			'name' => 'Lubuskie',
    			'country_id' => 1
    	]);
    	 
    	$this->insert('province', [
    			'id' => 4,
    			'symbol' => 'LD',
    			'name' => 'Łódzkie',
    			'country_id' => 1
    	]);
    	 
    	$this->insert('province', [
    			'id' => 5,
    			'symbol' => 'LU',
    			'name' => 'Lubelskie',
    			'country_id' => 1
    	]);
    	 
    	$this->insert('province', [
    			'id' => 6,
    			'symbol' => 'MA',
    			'name' => 'Małopolskie',
    			'country_id' => 1
    	]);
    	 
    	$this->insert('province', [
    			'id' => 7,
    			'symbol' => 'MZ',
    			'name' => 'Mazowieckie',
    			'country_id' => 1
    	]);
    	 
    	$this->insert('province', [
    			'id' => 8,
    			'symbol' => 'OP',
    			'name' => 'Opolskie',
    			'country_id' => 1
    	]);
    	 
    	$this->insert('province', [
    			'id' => 9,
    			'symbol' => 'PD',
    			'name' => 'Podlaskie',
    			'country_id' => 1
    	]);
    	 
    	$this->insert('province', [
    			'id' => 10,
    			'symbol' => 'PK',
    			'name' => 'Podkarpackie',
    			'country_id' => 1
    	]);
    	 
    	$this->insert('province', [
    			'id' => 11,
    			'symbol' => 'PM',
    			'name' => 'Pomorskie',
    			'country_id' => 1
    	]);
    	 
    	$this->insert('province', [
    			'id' => 12,
    			'symbol' => 'SK',
    			'name' => 'Świętokrzyskie',
    			'country_id' => 1
    	]);
    	 
    	$this->insert('province', [
    			'id' => 13,
    			'symbol' => 'SL',
    			'name' => 'Śląskie',
    			'country_id' => 1
    	]);
    	 
    	$this->insert('province', [
    			'id' => 14,
    			'symbol' => 'WM',
    			'name' => 'Warmińsko-Mazurskie',
    			'country_id' => 1
    	]);
    	 
    	$this->insert('province', [
    			'id' => 15,
    			'symbol' => 'WP',
    			'name' => 'Wielkopolskie',
    			'country_id' => 1
    	]);
    	 
    	$this->insert('province', [
    			'id' => 16,
    			'symbol' => 'ZP',
    			'name' => 'Zachodniopomorskie',
    			'country_id' => 1
    	]);
    	
    	$this->insert('user', [
    			'id' => 1,
    			'email' => 'admin@manman.com',
    			'password' => Yii::$app->getSecurity()->generatePasswordHash('1qazxsw2'),
    			'first_name' => 'Administrator',
    			'last_name' => 'systemu',
    			'language_id' => $languageId,
    			'currency_id' => $currencyId,
    			'is_active' => 1
    	]);
    	 
    	$this->insert('user_role', [
    			'user_id' => 1,
    			'role_id' => 1
    	]);
    	 
    	$auth = Yii::$app->authManager;
    	 
    	if($auth->getPermission(Utility::ROLE_ADMIN) === null) {
    		$rbac = new RbacController('rbac', 'console');
    		$rbac->actionInit();
    	}
    	 
    	$auth->revokeAll(1);
    	$auth->assign($auth->getRole(Utility::ROLE_ADMIN.'_role'), 1);
    }

    public function down() {
        $this->delete('user_role', ['user_id' => 1, 'role_id' => 1]);        
        $this->delete('user', ['id' => 1]);        
        
        $auth = Yii::$app->authManager;
        $auth->revokeAll(1);
        
        $this->delete('province');
        $this->delete('country_translation');
        $this->delete('country');
        $this->delete('email_config');
        $this->delete('role_translation');
        $this->delete('role');
        $this->delete('currency');
        $this->delete('language');
    }
}
