<?php

use yii\db\Schema;
use yii\db\Migration;

class m170918_130400_alter_order_offered_product extends Migration {

    public function up() {
        $this->addColumn('order_offered_product', 'remarks', $this->string(512));
        $this->addColumn('order_offered_product', 'remarks2', $this->string(512));
    }

    public function down() {
        $this->dropColumn('order_offered_product', 'remarks');
        $this->dropColumn('order_offered_product', 'remarks2');
    }
}
