<?php

use yii\db\Schema;
use yii\db\Migration;

class m160823_095850_alter_email_type extends Migration {

    public function up() {
		$this->addColumn('email_type', 'tags', 'VARCHAR(128) NULL DEFAULT NULL AFTER symbol');
    }

    public function down() {
        $this->dropColumn('email_type', 'tags');
    }
}
