<?php

use yii\db\Schema;
use yii\db\Migration;

class m161003_121633_alter_order_history extends Migration {

    public function up() {
        $this->execute('ALTER TABLE order_history add date_shipping DATE NULL DEFAULT NULL');
    }

    public function down() {
        $this->dropColumn('order_history', 'date_shipping');
    }

}
