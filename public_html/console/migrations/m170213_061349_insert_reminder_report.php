<?php

use yii\db\Schema;
use yii\db\Migration;
use yii\db\Query;

class m170213_061349_insert_reminder_report extends Migration {

    public function safeUp() {
    	$languageId = (new Query())->select(['id'])->from('language')->where(['symbol' => 'pl'])->scalar();
    	$footerId = (new Query())->select(['id'])->from('document_section')->where(['symbol' => 'COMPANY_FOOTER'])->scalar();
    	$companyShortHeaderId = (new Query())->select(['id'])->from('document_section')->where(['symbol' => 'COMPANY_SHORT_HEADER'])->scalar();    	
    	 
    	$this->insert('document_type', [
    			'symbol' => 'REMINDER_REPORT',
    			'document_section_footer_id' => $footerId,
    			'margin_top' => 15,
    			'margin_bottom' => 50,
    			'margin_left' => 15,
    			'margin_right' => 15,
    			'is_for_report' => 1,
    	]);
    	$documentTypeId = $this->db->getLastInsertID();
    	$this->insert('document_type_translation', [
    			'document_type_id' => $documentTypeId,
    			'language_id' => $languageId,
    			'name' => 'Raport przypomnień',
    	]);
    	$this->insert('document_section', ['symbol' => 'REMINDER_REPORT',]);
    	$sectionId = $this->db->getLastInsertID();
    	$this->insert('document_type_section', ['document_type_id' => $documentTypeId, 'document_section_id' => $companyShortHeaderId, 'sequence_number' => 0]);
    	$this->insert('document_type_section', ['document_type_id' => $documentTypeId, 'document_section_id' => $sectionId, 'sequence_number' => 1]);
    }

    public function safeDown() {
    	$sectionId = (new Query())->select(['id'])->from('document_section')->where(['symbol' => 'REMINDER_REPORT'])->scalar();
    	$typeId = (new Query())->select(['id'])->from('document_type')->where(['symbol' => 'REMINDER_REPORT'])->scalar();
    	
    	$this->delete('document_type_section', ['document_type_id' => $typeId]);
    	$this->delete('document_section', ['id' => $sectionId]);
    	$this->delete('document_type_translation', ['document_type_id' => $typeId]);
    	$this->delete('document_type', ['id' => $typeId]);
    }
}
