<?php

use yii\db\Schema;
use yii\db\Migration;

class m161207_105711_alter_order_history extends Migration {

    public function up() {
    	$tableOptions = null;
    	if($this->db->driverName === 'mysql') {
    		$tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
    	}
    	
    	$this->createTable('order_custom_data', [
    			'id' => $this->primaryKey(11),
    			'column_count' => $this->integer(2)->notNull(),
    			'content' => $this->string()->notNull(),
    	], $tableOptions);
    	
    	$this->alterColumn('order_custom_data', 'content', 'MEDIUMTEXT NOT NULL');
    	
    	$this->addColumn('order_history', 'order_custom_data_id', 'INT(11) NULL DEFAULT NULL AFTER is_answer_required');
    	$this->addForeignKey('fk_order_history_order_custom_data', 'order_history', 'order_custom_data_id', 'order_custom_data', 'id');
    }

    public function down() {
		$this->dropForeignKey('fk_order_history_order_custom_data', 'order_history');
		$this->dropColumn('order_history', 'order_custom_data_id');
		
		$this->dropTable('order_custom_data');
    }

}
