<?php

use yii\db\Schema;
use yii\db\Migration;

class m160929_063553_create_number_template extends Migration {

    public function up() {
    	if($this->db->driverName === 'mysql') {
    		$tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
    	}
    	
    	$this->createTable('number_template', [
    			'id' => $this->primaryKey(11),
    			'symbol' => $this->string(64)->notNull()->unique(),
    			'separator' => $this->string(10)->notNull(),
    			'template' => $this->string(255)->notNull(),
    			'is_active' => $this->boolean()->notNull()->defaultValue(1),
    	], $tableOptions);
    	
    	$this->createTable('number_template_storage', [
    			'id' => $this->primaryKey(11),
    			'number_template_id' => $this->integer(11)->notNull()->unique(),
    			'current_number' => $this->string(255),
    	], $tableOptions);
    	
    	$this->addForeignKey('fk_number_template_storage_number_template', 'number_template_storage', 'number_template_id', 'number_template', 'id');
    	
    	$this->insert('number_template', ['symbol' => 'order', 'separator' => '/', 'template' => '{noInYear:inc}/{initials}/{year}']);
    	$this->insert('number_template_storage', ['number_template_id' => $this->db->getLastInsertID()]);
    	
    	$this->insert('number_template', ['symbol' => 'goods_received', 'separator' => '/', 'template' => '{noInYear:inc}/{initials}/{year}']);
    	$this->insert('number_template_storage', ['number_template_id' => $this->db->getLastInsertID()]);
    	
    	$this->insert('number_template', ['symbol' => 'goods_issue', 'separator' => '/', 'template' => '{noInYear:inc}/{initials}/{year}']);
    	$this->insert('number_template_storage', ['number_template_id' => $this->db->getLastInsertID()]);
    }

    public function down() {
        $this->droptable('number_template_storage');
        $this->dropTable('number_template');
    }
}
