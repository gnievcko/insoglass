<?php

use yii\db\Schema;
use yii\db\Migration;

class m171103_082349_alter_attribute_client extends Migration {

    public function up() {
        $this->addColumn('product_attribute_type', 'is_visible_for_client', 'TINYINT(1) NOT NULL DEFAULT 1');
    }

    public function down() {
        $this->dropColumn('product_attribute_type', 'is_visible_for_client');
    }

}
