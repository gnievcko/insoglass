<?php

use yii\db\Schema;
use yii\db\Migration;
use common\models\ar\DocumentSection;
use common\models\ar\DocumentType;
use common\models\ar\DocumentTypeSection;

class m161006_125137_insert_section_type extends Migration {

    public function up() {
        
        $this->insertSection('GOODS_RECEIVED_FOOTER', 'GOODS_RECEIVED', 3);
        $this->insertSection('GOODS_ISSUED_FOOTER', 'GOODS_ISSUED', 3);

    }

    public function down() {
        
        $section1 = DocumentSection::findOne(['symbol' => 'GOODS_RECEIVED_FOOTER']);
        $section2 = DocumentSection::findOne(['symbol' => 'GOODS_ISSUED_FOOTER']);
        
        foreach($section1->getDocumentTypeSections()->all() as $row) {
                $row->delete();
        }
         foreach( $section2->getDocumentTypeSections()->all() as $row) {
                $row->delete();
        }
       
        
        $section1->delete();
        $section2->delete();
               
    }
    private function insertSection($sectionName, $documentName, $number) {
        $section = new DocumentSection();
        $section->symbol = $sectionName;
        $section->insert();

        $document1 = DocumentType::findOne(['symbol' => $documentName]);
        $documentTypeSection = new DocumentTypeSection();
        $documentTypeSection->document_type_id = $document1->id;
        $documentTypeSection->document_section_id = $section->id;
        $documentTypeSection->sequence_number = $number;

        $documentTypeSection->insert();
    }
    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp() {
    }

    public function safeDown() {
    }
    */
}
