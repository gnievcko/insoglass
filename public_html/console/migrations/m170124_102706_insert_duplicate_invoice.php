<?php

use yii\db\Schema;
use yii\db\Migration;
use yii\db\Query;

class m170124_102706_insert_duplicate_invoice extends Migration {

    public function safeUp() {
		$languageId = (new Query())->select(['id'])->from('language')->where(['symbol' => 'pl'])->scalar();
		$footerId = (new Query())->select(['id'])->from('document_section')->where(['symbol' => 'COMPANY_FOOTER'])->scalar();
		$companyHeaderId = (new Query())->select(['id'])->from('document_section')->where(['symbol' => 'COMPANY_HEADER'])->scalar();
		$invoiceProductsId = (new Query())->select(['id'])->from('document_section')->where(['symbol' => 'INVOICE_PRODUCTS'])->scalar();
		$invoicePaymentId = (new Query())->select(['id'])->from('document_section')->where(['symbol' => 'INVOICE_PAYMENT'])->scalar();
    	
    	$this->insert('document_type', [
    			'symbol' => 'DUPLICATE_INVOICE',
    			'document_section_footer_id' => $footerId,
    			'margin_top' => 15,
    			'margin_bottom' => 50,
    			'margin_left' => 15,
    			'margin_right' => 15,
    			'is_for_order' => 1,
    	]);
    	$documentTypeId = $this->db->getLastInsertID();    	
    	$this->insert('document_type_translation', [
    			'document_type_id' => $documentTypeId,
    			'language_id' => $languageId,
    			'name' => 'Duplikat faktury',
    	]);
    	$this->insert('document_section', ['symbol' => 'DUPLICATE_INVOICE_TITLE',]);
    	$sectionId = $this->db->getLastInsertID();
    	$this->insert('document_type_section', ['document_type_id' => $documentTypeId, 'document_section_id' => $companyHeaderId, 'sequence_number' => 0]);
    	$this->insert('document_type_section', ['document_type_id' => $documentTypeId, 'document_section_id' => $sectionId, 'sequence_number' => 1]);
    	$this->insert('document_type_section', ['document_type_id' => $documentTypeId, 'document_section_id' => $invoiceProductsId , 'sequence_number' => 2]);
    	$this->insert('document_type_section', ['document_type_id' => $documentTypeId, 'document_section_id' => $invoicePaymentId, 'sequence_number' => 3]);
    }

    public function safeDown() {
       	$sectionId = (new Query())->select(['id'])->from('document_section')->where(['symbol' => 'DUPLICATE_INVOICE_TITLE'])->scalar();
        $typeId = (new Query())->select(['id'])->from('document_type')->where(['symbol' => 'DUPLICATE_INVOICE'])->scalar();
        
        $this->delete('document_type_section', ['document_type_id' => $typeId]);
        $this->delete('document_section', ['id' => $sectionId]);
        $this->delete('document_type_translation', ['document_type_id' => $typeId]);
        $this->delete('document_type', ['id' => $typeId]);
    }
}
