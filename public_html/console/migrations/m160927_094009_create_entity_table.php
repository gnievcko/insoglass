<?php

use yii\db\Migration;

/**
 * Handles the creation for table `entity`.
 */
class m160927_094009_create_entity_table extends Migration
{
    public function up()
    {
        $this->createTable('entity', [
            	'id' => $this->primaryKey(),
        		'name' => $this->string(255)->notNull()->unique(),
        		'address_id' => $this->integer(11)->notNull(),
        		'taxpayer_identification_number' => $this->string(32)->notNull(),
        		'vat_identification_number' => $this->string(32)->notNull(),
        		'krs_number' => $this->string(32)->notNull(),
        		'email' => $this->string(64)->notNull(),
        		'phone' => $this->string(32)->notNull(),
        		'fax' => $this->string(32)->null(),
        		'account_number' => $this->string(32)->notNull(),
        		'bank_name' => $this->string(128)->notNull(),
        		'share_capital' => $this->decimal(17, 2)->notNull(),
        		'website_address' => $this->string(255)->null(),
        		'is_active' => $this->boolean()->notNull(),
        ]);
        $this->addForeignKey('fk_entity_address', 'entity', 'address_id', 'address', 'id');
    }

    public function down()
    {
    	$this->dropForeignKey('fk_entity_address', 'entity');
    	$this->dropIndex('fk_entity_address', 'entity');
        $this->dropTable('entity');
    }
}
