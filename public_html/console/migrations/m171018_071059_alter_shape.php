<?php

use yii\db\Schema;
use yii\db\Migration;

class m171018_071059_alter_shape extends Migration {

    public function up() {
        $this->addColumn('shape', 'identifier', $this->string(64));
    }

    public function down() {
        $this->dropColumn('shape', 'identifier');
    }

}
