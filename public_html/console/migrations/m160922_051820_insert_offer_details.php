<?php

use yii\db\Schema;
use yii\db\Migration;
use common\documents\DocumentType;
use common\documents\sections\SectionsMapper;
use yii\db\Query;

class m160922_051820_insert_offer_details extends Migration {

    public function safeUp() {
        $headerId = (new Query())->select(['id'])->from('document_section')->where(['symbol' => SectionsMapper::COMPANY_SHORT_HEADER])->one()['id'];
        $footerId = (new Query())->select(['id'])->from('document_section')->where(['symbol' => SectionsMapper::COMPANY_FOOTER])->one()['id'];

        $this->insert('document_type', [
	            'symbol' => DocumentType::OFFER_DETAILS, 
	            'document_section_header_id' => $headerId, 
	            'document_section_footer_id' => $footerId,
	            'margin_top' => 100,
	            'margin_bottom' => 50, 
	            'margin_left' => 15, 
	            'margin_right' => 15,
        ]);
        $documentTypeId = \Yii::$app->db->getLastInsertID();

    	$this->insert('document_section', ['symbol' => SectionsMapper::OFFER_DETAILS_BASIC_DATA]);
        $basicDataId = Yii::$app->db->getLastInsertID();
    	$this->insert('document_type_section', ['document_type_id' => $documentTypeId, 'document_section_id' => $basicDataId, 'sequence_number' => 0]);

    	$this->insert('document_section', ['symbol' => SectionsMapper::OFFER_DETAILS_PRODUCTS]);
        $offerProductsId = Yii::$app->db->getLastInsertID();
    	$this->insert('document_type_section', ['document_type_id' => $documentTypeId, 'document_section_id' => $offerProductsId, 'sequence_number' => 1]);
    }

    public function safeDown() {
        $documentTypeId = (new Query())->select(['id'])->from('document_type')->where(['symbol' => DocumentType::OFFER_DETAILS])->one()['id'];

        $this->delete('document_type_section', ['document_type_id' => $documentTypeId]);
        $this->delete('document_section', ['symbol' => [SectionsMapper::OFFER_DETAILS_BASIC_DATA, SectionsMapper::OFFER_DETAILS_PRODUCTS]]);
        $this->delete('document_type', ['symbol' => DocumentType::OFFER_DETAILS]);
    }
}
