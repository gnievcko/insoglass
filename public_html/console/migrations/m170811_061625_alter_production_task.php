<?php

use yii\db\Schema;
use yii\db\Migration;

class m170811_061625_alter_production_task extends Migration {

    public function safeUp() {
        $this->dropForeignKey('fk_production_task_history_workstation', 'production_task');
        $this->addForeignKey('fk_production_task_history_workstation', 'production_task_history', 'workstation_id', 'workstation', 'id');
    }

    public function safeDown() {
        $this->dropForeignKey('fk_production_task_history_workstation', 'production_task_history');
    }
}
