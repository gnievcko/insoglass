<?php

use yii\db\Schema;
use yii\db\Migration;

class m161206_055840_alter_order_configuration extends Migration {

    public function up() {
		$this->addColumn('configuration', 'days_dbbackups_expiration', 'INT(5) NULL DEFAULT NULL');
		$this->update('configuration', ['days_dbbackups_expiration' => 21]);
		
		$this->addColumn('order', 'is_deleted', 'TINYINT(1) NULL DEFAULT NULL');
		$this->update('order', ['is_deleted' => 0]);
		$this->alterColumn('order', 'is_deleted', 'TINYINT(1) NOT NULL DEFAULT 0');
		
		$this->addColumn('order', 'user_deleting_id', 'INT(11) NULL DEFAULT NULL');
		$this->addForeignKey('fk_order_user_deleting', 'order', 'user_deleting_id', 'user', 'id');
    }

    public function down() {
    	$this->dropForeignKey('fk_order_user_deleting', 'order');
    	$this->dropColumn('order', 'user_deleting_id');
    	
    	$this->dropColumn('order', 'is_deleted');
        $this->dropColumn('configuration', 'days_dbbackups_expiration');
    }

}
