<?php

use yii\db\Schema;
use yii\db\Migration;

class m160704_123101_init_advanced_tables_storehouse extends Migration {

    public function up() {
    	$tableOptions = null;
    	if($this->db->driverName === 'mysql') {
    		$tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
    	}
    	
    	$this->createProduct($tableOptions);
    	$this->createWarehouse($tableOptions);
    	$this->createWarehouseDelivery($tableOptions);
    }

    public function down() {
        // raczej nie bedzie potrzebne
    }
    
    private function createProduct($tableOptions) {
    	$this->createTable('product_category', [
    			'id' => $this->primaryKey(11),
    			'symbol' => $this->string(32)->notNull()->unique(),
    			'parent_category_id' => $this->integer(11)
    	], $tableOptions);
    	 
    	$this->addForeignKey('fk_product_category_parent', 'product_category', 'parent_category_id', 'product_category', 'id');
    	 
    	$this->createTable('product_category_translation', [
    			'product_category_id' => $this->integer(11),
    			'language_id' => $this->integer(11),
    			'name' => $this->string(128)->notNull(),
    			'description' => $this->string(1024)
    	], $tableOptions);
    	 
    	$this->addPrimaryKey('pk_product_category_translation', 'product_category_translation', ['product_category_id', 'language_id']);
    	$this->addForeignKey('fk_product_category_translation_language', 'product_category_translation', 'language_id', 'language', 'id');
    	$this->addForeignKey('fk_product_category_translation_category', 'product_category_translation', 'product_category_id', 'product_category', 'id');
    	    	 
    	$this->createTable('product', [
    			'id' => $this->primaryKey(11),
    			'symbol' => $this->string(32)->notNull()->unique(),
    			'count_minimal' => $this->integer(10),
    			'is_active' => $this->boolean()->notNull(),
    			'date_creation' => $this->timestamp()->notNull(),
    	], $tableOptions);
    	 
    	$this->alterColumn('product', 'date_creation', 'TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP');
    	 
    	$this->createTable('product_translation', [
    			'product_id' => $this->integer(11),
    			'language_id' => $this->integer(11),
    			'name' => $this->string(512)->notNull(),
    			'description' => $this->string(2048),
    			'price_default' => $this->decimal(10,2),
    			'currency_id' => $this->integer(11)
    	], $tableOptions);
    	 
    	$this->addPrimaryKey('pk_product_translation', 'product_translation', ['product_id', 'language_id']);
    	$this->addForeignKey('fk_product_translation_product', 'product_translation', 'product_id', 'product', 'id');
    	$this->addForeignKey('fk_product_translation_language', 'product_translation', 'language_id', 'language', 'id');
    	$this->addForeignKey('fk_product_translation_currency', 'product_translation', 'currency_id', 'currency', 'id');
    	 
    	$this->createTable('product_category_set', [
    			'product_id' => $this->integer(11),
    			'product_category_id' => $this->integer(11)
    	], $tableOptions);
    	
    	$this->addPrimaryKey('pk_product_category_set', 'product_category_set', ['product_id', 'product_category_id']);
    	$this->addForeignKey('fk_product_category_set_product', 'product_category_set', 'product_id', 'product', 'id');
    	$this->addForeignKey('fk_product_category_set_category', 'product_category_set', 'product_category_id', 'product_category', 'id');
    	 
    	$this->createTable('product_version', [
    			'id' => $this->primaryKey(11),
    			'product_id' => $this->integer(11)->notNull(),
    			'symbol' => $this->string(32)->notNull()->unique(),
    			'priority' => $this->integer(5),
    			'is_active' => $this->boolean()->notNull(),
    			'date_creation' => $this->timestamp()->notNull(),
    	], $tableOptions);
    	 
    	$this->addForeignKey('fk_product_version_product', 'product_version', 'product_id', 'product', 'id');
    	$this->alterColumn('product_version', 'date_creation', 'TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP');
    	 
    	$this->createTable('product_version_translation', [
    			'product_version_id' => $this->integer(11),
    			'language_id' => $this->integer(11),
    			'name' => $this->string(128)->notNull(),
    			'description' => $this->string(512),
    	], $tableOptions);
    	 
    	$this->addPrimaryKey('pk_product_version_translation', 'product_version_translation', ['product_version_id', 'language_id']);
    	$this->addForeignKey('fk_product_version_translation_version', 'product_version_translation', 'product_version_id', 'product_version', 'id');
    	$this->addForeignKey('fk_product_version_translation_language', 'product_version_translation', 'language_id', 'language', 'id');
    	 
    	$this->createTable('product_photo', [
    			'id' => $this->primaryKey(11),
    			'product_id' => $this->integer(11)->notNull(),
    			'url_photo' => $this->string(256)->notNull(),
    			'priority' => $this->integer(5),
    			'date_creation' => $this->timestamp()->notNull(),
    	], $tableOptions);
    	
    	$this->addForeignKey('fk_product_photo_product', 'product_photo', 'product_id', 'product', 'id');
    	$this->alterColumn('product_photo', 'date_creation', 'TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP');
    	
    	$this->createTable('product_photo_translation', [
    			'product_photo_id' => $this->integer(11),
    			'language_id' => $this->integer(11),
    			'description' => $this->string(512),
    	], $tableOptions);
    	
    	$this->addPrimaryKey('pk_product_photo_translation', 'product_photo_translation', ['product_photo_id', 'language_id']);
    	$this->addForeignKey('fk_product_photo_translation_photo', 'product_photo_translation', 'product_photo_id', 'product_photo', 'id');
    	$this->addForeignKey('fk_product_photo_translation_language', 'product_photo_translation', 'language_id', 'language', 'id');
    	
    	$this->createTable('product_alias', [
    			'id' => $this->primaryKey(11),
    			'product_id' => $this->integer(11)->notNull(),
    			'language_id' => $this->integer(11)->notNull(),
    			'name' => $this->string(512)->notNull(),
    	], $tableOptions);
    	
    	$this->addForeignKey('fk_product_alias_product', 'product_alias', 'product_id', 'product', 'id');
    	$this->addForeignKey('fk_product_alias_language', 'product_alias', 'language_id', 'language', 'id');
    }
    
    private function createWarehouse($tableOptions) {
    	$this->createTable('warehouse', [
    			'id' => $this->primaryKey(11),
    			'name' => $this->string(256)->notNull(),
    			'parent_warehouse_id' => $this->integer(11),
    			'address_id' => $this->integer(11),
    			'is_active' => $this->boolean()->notNull()->defaultValue(1),
    			'date_creation' => $this->timestamp()->notNull(),
    	], $tableOptions);
    	
    	$this->addForeignKey('fk_warehouse_parent', 'warehouse', 'parent_warehouse_id', 'warehouse', 'id');
    	$this->addForeignKey('fk_warehouse_address', 'warehouse', 'address_id', 'address', 'id');
    	$this->alterColumn('warehouse', 'date_creation', 'TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP');
    	
    	$this->createTable('user_warehouse', [
    			'user_id' => $this->integer(11),
    			'warehouse_id' => $this->integer(11)
    	], $tableOptions); 	 
    	
    	$this->addPrimaryKey('pk_user_warehouse', 'user_warehouse', ['user_id', 'warehouse_id']);
    	$this->addForeignKey('fk_user_warehouse_user', 'user_warehouse', 'user_id', 'user', 'id');
    	$this->addForeignKey('fk_user_warehouse_warehouse', 'user_warehouse', 'warehouse_id', 'warehouse', 'id');
    }
    
    private function createWarehouseDelivery($tableOptions) {
    	$this->createTable('warehouse_supplier', [
    			'id' => $this->primaryKey(11),
    			'warehouse_id' => $this->integer(11),
    			'name' => $this->string(256)->notNull(),
    			'is_active' => $this->boolean()->notNull()->defaultValue(1),
    			'date_creation' => $this->timestamp()->notNull(),
    	], $tableOptions);
    			 
    	$this->addForeignKey('fk_warehouse_supplier_warehouse', 'warehouse_supplier', 'warehouse_id', 'warehouse', 'id');
    	$this->alterColumn('warehouse_supplier', 'date_creation', 'TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP');
    		
    	$this->createTable('warehouse_delivery', [
    			'id' => $this->primaryKey(11),
    			'warehouse_id' => $this->integer(11)->notNull(),
    			'warehouse_supplier_id' => $this->integer(11),
    			'number' => $this->string(48)->notNull(),
    			'user_confirming_id' => $this->integer(11)->notNull(),
    			'is_artificial' => $this->boolean()->notNull()->defaultValue(0),
    			'date_delivery' => $this->timestamp()->defaultValue(null),
    			'date_creation' => $this->timestamp()->defaultValue(null),
    	], $tableOptions);
    	
    	$this->addForeignKey('fk_warehouse_delivery_warehouse', 'warehouse_delivery', 'warehouse_id', 'warehouse', 'id');
    	$this->addForeignKey('fk_warehouse_delivery_warehouse_supplier', 'warehouse_delivery', 'warehouse_supplier_id', 'warehouse_supplier', 'id');
    	$this->addForeignKey('fk_warehouse_delivery_user', 'warehouse_delivery', 'user_confirming_id', 'user', 'id');
    	$this->alterColumn('warehouse_delivery', 'date_creation', 'TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP');
    	$this->alterColumn('warehouse_delivery', 'date_delivery', 'TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP');
    	
    	$this->createTable('warehouse_delivery_product', [
    			'id' => $this->primaryKey(11),
    			'warehouse_delivery_id' => $this->integer(11)->notNull(),
    			'product_id' => $this->integer(11)->notNull(),
    			'count' => $this->integer(10)->notNull(),
    			'price_unit' => $this->decimal(12,2),
    			'price_group' => $this->decimal(12,2),
    			'currency_id' => $this->integer(11),
    	], $tableOptions);
    	
    	$this->addForeignKey('fk_warehouse_delivery_product_delivery', 'warehouse_delivery_product', 'warehouse_delivery_id', 'warehouse_delivery', 'id');
    	$this->addForeignKey('fk_warehouse_delivery_product_product', 'warehouse_delivery_product', 'product_id', 'product', 'id');
    	$this->addForeignKey('fk_warehouse_delivery_product_currency', 'warehouse_delivery_product', 'currency_id', 'currency', 'id');
    	
    	$this->createTable('warehouse_product', [
    			'id' => $this->primaryKey(11),
    			'warehouse_id' => $this->integer(11)->notNull(),
    			'product_id' => $this->integer(11)->notNull(),
    			'count' => $this->integer(10)->notNull(),
    			'date_modification' => $this->timestamp()->notNull(),
    	], $tableOptions);
    	
    	$this->execute('ALTER TABLE warehouse_product ADD UNIQUE INDEX uk_warehouse_product_warehouse_product (warehouse_id, product_id)');
    	$this->addForeignKey('fk_warehouse_product_warehouse', 'warehouse_product', 'warehouse_id', 'warehouse', 'id');
    	$this->addForeignKey('fk_warehouse_product_product', 'warehouse_product', 'product_id', 'product', 'id');
    	
    	$this->createTable('product_status', [
    			'id' => $this->primaryKey(11),
    			'symbol' => $this->string(32)->notNull()->unique(),
    	], $tableOptions);
    	 
    	$this->createTable('product_status_translation', [
    			'product_status_id' => $this->integer(11),
    			'language_id' => $this->integer(11),
    			'name' => $this->string(128)->notNull(),
    	], $tableOptions);
    	
    	$this->addPrimaryKey('pk_product_status_translation', 'product_status_translation', ['product_status_id', 'language_id']);
    	$this->addForeignKey('fk_product_status_translation_language', 'product_status_translation', 'language_id', 'language', 'id');
    	$this->addForeignKey('fk_product_status_translation_status', 'product_status_translation', 'product_status_id', 'product_status', 'id');
    	 
    	$this->createTable('product_status_reference', [
    			'id' => $this->primaryKey(11),
    			'product_status_id' => $this->integer(11)->notNull(),
    			'symbol' => $this->string(64)->notNull(),
    			'name_table' => $this->string(64)->notNull(),
    			'name_column' => $this->string(64)->notNull(),
    	], $tableOptions);
    	
    	$this->addForeignKey('fk_product_status_reference_status', 'product_status_reference', 'product_status_id', 'product_status', 'id');
    	
    	$this->createTable('warehouse_product_status_history', [
    			'id' => $this->primaryKey(11),
    			'warehouse_product_id' => $this->integer(11)->notNull(),
    			'product_status_id' => $this->integer(11)->notNull(),
    			'user_confirming_id' => $this->integer(11)->notNull(),
    			'count' => $this->integer(10)->notNull(),
    			'description' => $this->string(256),
    			'date_creation' => $this->timestamp()->notNull(),
    	], $tableOptions);
    	
    	$this->addForeignKey('fk_warehouse_product_status_history_product', 'warehouse_product_status_history', 'warehouse_product_id', 'warehouse_product', 'id');
    	$this->addForeignKey('fk_warehouse_product_status_history_status', 'warehouse_product_status_history', 'product_status_id', 'product_status', 'id');
    	$this->addForeignKey('fk_warehouse_product_status_history_user', 'warehouse_product_status_history', 'user_confirming_id', 'user', 'id');
    	$this->alterColumn('warehouse_product_status_history', 'date_creation', 'TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP');
    	
    	$this->createTable('warehouse_product_status_history_data', [
    			'history_id' => $this->integer(11),
    			'product_status_reference_id' => $this->integer(11),
    			'referenced_object_id' => $this->integer(11)->notNull(),
    	], $tableOptions);
    	
    	$this->addPrimaryKey('pk_warehouse_product_status_history_data', 'warehouse_product_status_history_data', ['product_status_reference_id', 'history_id']);
    	$this->addForeignKey('fk_warehouse_product_status_history_data_history', 'warehouse_product_status_history_data', 'history_id', 'warehouse_product_status_history', 'id');
    	$this->addForeignKey('fk_warehouse_product_status_history_data_reference', 'warehouse_product_status_history_data', 'product_status_reference_id', 'product_status_reference', 'id');
    }
}
