<?php

use yii\db\Schema;
use yii\db\Migration;
use common\helpers\Utility;

class m161017_133317_alter_order_status extends Migration {

    public function safeUp() {
		$this->addColumn('order_status', 'is_positive', $this->boolean()->notNull());
		$this->update('order_status', ['is_positive' => 1], ['symbol' => [Utility::ORDER_STATUS_COMPLETED, Utility::ORDER_STATUS_FINISHED]]);
    }

    public function safeDown() {
		$this->dropColumn('order_status', 'is_positive');
    }

}
