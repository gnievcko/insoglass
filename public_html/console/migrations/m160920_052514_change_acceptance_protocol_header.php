<?php

use yii\db\Schema;
use yii\db\Migration;
use common\documents\DocumentType;
use common\documents\sections\SectionsMapper;
use yii\db\Query;

class m160920_052514_change_acceptance_protocol_header extends Migration {

    public function safeUp() {
    	$this->insert('document_section', ['symbol' => SectionsMapper::COMPANY_SHORT_HEADER]);
        $shortHeaderId = Yii::$app->db->getLastInsertID();

        Yii::$app->db->createCommand('
            UPDATE document_type_section SET sequence_number = sequence_number + 1 WHERE sequence_number = 1 AND document_type_id = (
                SELECT id FROM document_type WHERE symbol = :acceptanceProtocolSymbol
            )
        ')
        ->bindValue(':acceptanceProtocolSymbol', DocumentType::ACCEPTANCE_PROTOCOL)
        ->execute();

        Yii::$app->db->createCommand('
            UPDATE document_type_section SET sequence_number = sequence_number + 1 WHERE sequence_number = 0 AND document_type_id = (
                SELECT id FROM document_type WHERE symbol = :acceptanceProtocolSymbol
            )
        ')
        ->bindValue(':acceptanceProtocolSymbol', DocumentType::ACCEPTANCE_PROTOCOL)
        ->execute();

        $acceptanceProtocolId = (new Query())->select(['id'])->from('document_type')->where(['symbol' => DocumentType::ACCEPTANCE_PROTOCOL])->one()['id'];

        $this->insert('document_type_section', [
            'document_type_id' => $acceptanceProtocolId, 
            'document_section_id' => $shortHeaderId, 
            'sequence_number' => 0,
        ]);

        $this->update('document_type', ['document_section_header_id' => null, 'margin_top' => 15], ['id' => $acceptanceProtocolId]);
    }

    public function safeDown() {
        $acceptanceProtocolId = (new Query())->select(['id'])->from('document_type')->where(['symbol' => DocumentType::ACCEPTANCE_PROTOCOL])->one()['id'];
        $companyShortHeaderId = (new Query())->select(['id'])->from('document_section')->where(['symbol' => SectionsMapper::COMPANY_SHORT_HEADER])->one()['id'];

        $this->update('document_type', [
            'document_section_header_id' => (new Query())->select(['id'])->from('document_section')->where(['symbol' => SectionsMapper::COMPANY_HEADER])->one()['id'],
            'margin_top' => 100,
        ], ['id' => $acceptanceProtocolId]);

        $this->delete('document_type_section', [
            'document_type_id' => $acceptanceProtocolId,
            'document_section_id' => $companyShortHeaderId,
        ]);

        Yii::$app->db->createCommand('
            UPDATE document_type_section SET sequence_number = sequence_number - 1 WHERE sequence_number = 1 AND document_type_id = (
                SELECT id FROM document_type WHERE symbol = :acceptanceProtocolSymbol
            )
        ')
        ->bindValue(':acceptanceProtocolSymbol', DocumentType::ACCEPTANCE_PROTOCOL)
        ->execute();

        Yii::$app->db->createCommand('
            UPDATE document_type_section SET sequence_number = sequence_number - 1 WHERE sequence_number = 2 AND document_type_id = (
                SELECT id FROM document_type WHERE symbol = :acceptanceProtocolSymbol
            )
        ')
        ->bindValue(':acceptanceProtocolSymbol', DocumentType::ACCEPTANCE_PROTOCOL)
        ->execute();

        $this->delete('document_section', ['id' => $companyShortHeaderId]);
    }
}
