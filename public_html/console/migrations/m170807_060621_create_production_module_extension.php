<?php

use yii\db\Schema;
use yii\db\Migration;
use yii\db\Query;
use common\models\ar\RoleTranslation;
use common\models\ar\Role;

class m170807_060621_create_production_module_extension extends Migration {

    public function safeUp() {
    	$tableOptions = null;
    	if($this->db->driverName === 'mysql') {
    		$tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
    	}
    	
    	$languageId = (new Query())->select(['id'])->from('language')->where(['symbol' => 'pl'])->scalar();
    	
    	$this->insert('role', ['symbol' => 'main_worker', 'is_visible' => false]);
    	$roleId = $this->db->getLastInsertID();
    	$this->insert('role_translation', [
    			'role_id' => intval($roleId),
    			'language_id' => intval($languageId),
    			'name' => 'Kierownik produkcji',
    	]);
    	
    	$this->addColumn('order_offered_product', 'width', 'INT(6) NULL DEFAULT NULL AFTER production_path_id');
    	$this->addColumn('order_offered_product', 'height', 'INT(6) NULL DEFAULT NULL AFTER width');
    	
    	$this->createTable('order_offered_product_attachment', [
    			'id' => $this->primaryKey(11),
    			'order_offered_product_id' => $this->integer(11)->notNull(),
    			'user_id' => $this->integer(11)->notNull(),
    			'url_file' => $this->string(256)->notNull(),
    			'name' => $this->string(256)->notNull(),
    			'description' => $this->string(512),
    			'date_creation' => $this->timestamp()->notNull(),
    	], $tableOptions);
    	 
    	$this->addForeignKey('fk_order_offered_product_attachment_product', 'order_offered_product_attachment', 'order_offered_product_id', 'order_offered_product', 'id');
    	$this->addForeignKey('fk_order_offered_product_attachment_user', 'order_offered_product_attachment', 'user_id', 'user', 'id');
    	$this->alterColumn('order_offered_product_attachment', 'date_creation', 'TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP'); 
    	
    	$this->createTable('product_attribute_group', [
    			'id' => $this->primaryKey(11),
    			'symbol' => $this->string(32)->notNull()->unique(),
    	], $tableOptions);
    	
    	$this->createTable('product_attribute_group_translation', [
    			'product_attribute_group_id' => $this->integer(11),
    			'language_id' => $this->integer(11),
    			'name' => $this->string(128)->notNull(),
    	], $tableOptions);
    	
    	$this->addPrimaryKey('pk_product_attribute_group_translation', 'product_attribute_group_translation', ['product_attribute_group_id', 'language_id']);
    	$this->addForeignKey('fk_product_attribute_group_translation_language', 'product_attribute_group_translation', 'language_id', 'language', 'id');
    	$this->addForeignKey('fk_product_attribute_group_translation_group', 'product_attribute_group_translation', 'product_attribute_group_id', 'product_attribute_group', 'id');
    	
    	$this->addColumn('product_attribute_type', 'product_attribute_group_id', 'INT(11) NULL DEFAULT NULL AFTER id');
    	$this->addForeignKey('fk_product_attribute_type_group', 'product_attribute_type', 'product_attribute_group_id', 'product_attribute_group', 'id');
    	$this->addColumn('product_attribute_type', 'is_visible', 'TINYINT(1) NOT NULL DEFAULT 1');
    	
    	$this->addColumn('activity', 'is_node', 'TINYINT(1) NOT NULL DEFAULT 0');
    	
    	$this->createTable('order_priority', [
    			'id' => $this->primaryKey(11),
    			'symbol' => $this->string(32)->notNull()->unique(),
    			'priority' => $this->integer(3)->notNull(),
    	], $tableOptions);
    	 
    	$this->createTable('order_priority_translation', [
    			'order_priority_id' => $this->integer(11),
    			'language_id' => $this->integer(11),
    			'name' => $this->string(128)->notNull(),
    	], $tableOptions);
    	 
    	$this->addPrimaryKey('pk_order_priority_translation', 'order_priority_translation', ['order_priority_id', 'language_id']);
    	$this->addForeignKey('fk_order_priority_translation_language', 'order_priority_translation', 'language_id', 'language', 'id');
    	$this->addForeignKey('fk_order_priority_translation_priority', 'order_priority_translation', 'order_priority_id', 'order_priority', 'id');
    	
    	$this->addColumn('order', 'order_priority_id', 'INT(11) NULL DEFAULT NULL');
    	$this->addForeignKey('fk_order_order_priority', 'order', 'order_priority_id', 'order_priority', 'id');
    	
    	$this->addColumn('production_path_step', 'is_requirement_type_or', 'TINYINT(1) NOT NULL AFTER activity_id');
    	
    	$this->addColumn('user_department', 'is_lead', 'TINYINT(1) NOT NULL DEFAULT 0');
    	
    	$this->createTable('production_path_step_attribute', [
    			'production_path_step_id' => $this->integer(11),
    			'product_attribute_type_id' => $this->integer(11),
    	], $tableOptions);
    	 
    	$this->addPrimaryKey('pk_production_path_step_attribute', 'production_path_step_attribute', ['production_path_step_id', 'product_attribute_type_id']);
    	$this->addForeignKey('fk_production_path_step_attribute_step', 'production_path_step_attribute', 'production_path_step_id', 'production_path_step', 'id');
    	$this->addForeignKey('fk_production_path_step_attribute_attribute', 'production_path_step_attribute', 'product_attribute_type_id', 'product_attribute_type', 'id');
    	
    }

    public function safeDown() {
        $this->dropTable('production_path_step_attribute');
        $this->dropColumn('user_department', 'is_lead');
        $this->dropColumn('production_path_step', 'is_requirement_type_or');
        $this->dropForeignKey('fk_order_order_priority', 'order');
        $this->dropColumn('order', 'order_priority_id');
        $this->dropTable('order_priority_translation');
        $this->dropTable('order_priority');
        
        $this->dropColumn('activity', 'is_node');
        $this->dropColumn('product_attribute_type', 'is_visible');
        $this->dropForeignKey('fk_product_attribute_type_group', 'product_attribute_type');
        $this->dropColumn('product_attribute_type', 'product_attribute_group_id');
        
        $this->dropTable('product_attribute_group_translation');
        $this->dropTable('product_attribute_group');
        $this->dropTable('order_offered_product_attachment');
        
        $this->dropColumn('order_offered_product', 'width');
        $this->dropColumn('order_offered_product', 'height');
        
        $roleIds = array_column((new Query())->select(['r.id'])->from('role r')->where(['symbol' => ['main_worker']])->all(), 'id');
        RoleTranslation::deleteAll(['role_id' => $roleIds]);
        Role::deleteAll(['id' => $roleIds]);
    }
}
