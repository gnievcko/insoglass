<?php

use yii\db\Migration;
use common\alerts\AlertConstants;

class m161006_174431_add_enable_flag_to_alerts extends Migration {

    public function up() {
        $this->addColumn('alert_type', 'is_enabled', $this->boolean()->notNull()->defaultValue(true));
        $this->update('alert_type', ['is_enabled' => false], ['symbol' => AlertConstants::ALERT_TYPE_NEW_ORDER_STATUS_UNCHANGED]);
    }

    public function down() {
        $this->dropColumn('alert_type', 'is_enabled');
    }
}
