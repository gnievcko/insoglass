<?php

use yii\db\Schema;
use yii\db\Migration;

class m160826_103102_alter_order extends Migration {

    public function up() {
    	$this->addColumn('order', 'is_template', 'BOOLEAN NOT NULL DEFAULT 0 AFTER is_active');
    }

    public function down() {
        $this->dropColumn('order', 'is_template');
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp() {
    }

    public function safeDown() {
    }
    */
}
