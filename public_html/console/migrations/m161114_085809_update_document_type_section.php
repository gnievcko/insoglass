<?php

use yii\db\Schema;
use yii\db\Migration;

class m161114_085809_update_document_type_section extends Migration {

    public function safeUp() {
    	$this->update('document_type', ['symbol' => 'LIST_OF_ITEMS'], ['symbol' => 'LIST_OF_NATURE']);
        $this->update('document_section', ['symbol' => 'LIST_OF_ITEMS'], ['symbol' => 'LIST_OF_NATURE']);
    }

    public function safeDown() {
    	
    }
}
