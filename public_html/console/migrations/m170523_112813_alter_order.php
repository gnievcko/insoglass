<?php

use yii\db\Schema;
use yii\db\Migration;

class m170523_112813_alter_order extends Migration {

	public function up() {
		$this->addColumn('order', 'parent_order_id', $this->integer(11));

		$this->addForeignKey('fk_parent_order_ordr', 'order', 'parent_order_id', 'order', 'id');
	}
    public function down() {

    }

}
