<?php

use yii\db\Schema;
use yii\db\Migration;
use common\models\ar\Language;
use common\models\ar\TaskType;
use common\models\ar\TaskTypeReference;
use common\models\ar\TaskTypeReferenceTranslation;
use common\models\ar\TaskTypeTranslation;

class m160915_103958_insert_task_type extends Migration {

    public function up() {
    	$languageId = Language::find()->where(['symbol' => 'pl'])->one()->id;
    	
    	$this->insert('task_type', ['symbol' => 'client_contact']);
    	$taskTypeId = $this->db->getLastInsertID();
    	$this->insert('task_type_translation', [
    			'task_type_id' => $taskTypeId,
    			'language_id' => $languageId,
    			'name' => 'Kontakt z klientem',
    	]);
    	$this->insert('task_type_reference', [
    			'task_type_id' => $taskTypeId,
    			'symbol' => 'company_data',
    			'name_table' => 'company',
    			'name_column' => 'id',
    			'name_expression' => 'company.name',
    			'url_details' => 'company/details/{id}',
    			'is_required' => 1,
    	]);
    	$this->insert('task_type_reference_translation', [
    			'reference_id' => $this->db->getLastInsertID(),
    			'language_id' => $languageId,
    			'name' => 'Dotyczy klienta',
    	]);
    	
    	$this->insert('task_type', ['symbol' => 'other']);
    	$this->insert('task_type_translation', [
    			'task_type_id' => $this->db->getLastInsertID(),
    			'language_id' => $languageId,
    			'name' => 'Inne',
    	]);
    }

    public function down() {
        $taskType1Id = TaskType::find()->where(['symbol' => 'client_contact'])->one()->id;
        $taskTypeReference1Id = TaskTypeReference::find()->where(['task_type_id' => $taskType1Id])->one()->id;
        $taskType2Id = TaskType::find()->where(['symbol' => 'other'])->one()->id;
        
        TaskTypeReferenceTranslation::deleteAll(['reference_id' => $taskTypeReference1Id]);
        TaskTypeReference::deleteAll(['id' => $taskTypeReference1Id]);
        TaskTypeTranslation::deleteAll(['task_type_id' => [$taskType1Id, $taskType2Id]]);
        TaskType::deleteAll(['id' => [$taskType1Id, $taskType2Id]]);
    }

}
