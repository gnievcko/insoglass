<?php

use yii\db\Schema;
use yii\db\Migration;
use yii\db\Query;
use common\documents\DocumentType;
use common\documents\sections\SectionsMapper;

class m171023_115753_alter_document_type_headers extends Migration {

    public function safeUp() {
        $reportProductionId = (new Query())->select(['dt.id'])->from('document_type dt')->where(['dt.symbol' => DocumentType::PRODUCTION_PROGRESS])->scalar();
        $reportQualityId = (new Query())->select(['dt.id'])->from('document_type dt')->where(['dt.symbol' => DocumentType::QUALITY_CONTROL])->scalar();
        $companyHeaderId = (new Query())->select(['ds.id'])->from('document_section ds')->where(['ds.symbol' => SectionsMapper::COMPANY_HEADER])->scalar();
        $companyShortHeaderId = (new Query())->select(['ds.id'])->from('document_section ds')->where(['ds.symbol' => SectionsMapper::COMPANY_SHORT_HEADER])->scalar();
        
        // TODO poprawić
        $this->update('document_type_section', ['document_section_id' => $companyShortHeaderId], [
            'document_type_id' => [$reportProductionId, $reportQualityId],
            'document_section_id' => $companyHeaderId,
        ]);
    }

    public function safeDown() {
        $reportProductionId = (new Query())->select(['dt.id'])->from('document_type dt')->where(['dt.symbol' => DocumentType::PRODUCTION_PROGRESS])->scalar();
        $reportQualityId = (new Query())->select(['dt.id'])->from('document_type dt')->where(['dt.symbol' => DocumentType::QUALITY_CONTROL])->scalar();
        $companyHeaderId = (new Query())->select(['ds.id'])->from('document_section ds')->where(['ds.symbol' => SectionsMapper::COMPANY_HEADER])->scalar();
        $companyShortHeaderId = (new Query())->select(['ds.id'])->from('document_section ds')->where(['ds.symbol' => SectionsMapper::COMPANY_SHORT_HEADER])->scalar();
        
        $this->update('document_type_section', ['document_section_id' => $companyHeaderId], [
            'document_type_id' => [$reportProductionId, $reportQualityId],
            'document_section_id' => $companyShortHeaderId,
        ]);
    }
}
