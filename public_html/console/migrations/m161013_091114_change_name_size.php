<?php

use yii\db\Schema;
use yii\db\Migration;

class m161013_091114_change_name_size extends Migration {

    public function up() {
        $this->alterColumn('product_translation', 'name', 'VARCHAR(2048) NOT NULL');
        $this->alterColumn('offered_product_translation', 'name', 'VARCHAR(2048) NOT NULL');
    }

    public function down() {
        $this->alterColumn('product_translation', 'name', 'VARCHAR(512) NOT NULL');
        $this->alterColumn('offered_product_translation', 'name', 'VARCHAR(512) NOT NULL');
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp() {
    }

    public function safeDown() {
    }
    */
}
