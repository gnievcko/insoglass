<?php

use yii\db\Schema;
use yii\db\Migration;
use yii\db\Query;

class m170126_063339_update_returns_from_issues extends Migration {

    public function safeUp() {
		$returnsFromIssues = (new Query())->select(['wpsh.id', 'wpo.number'])
				->from('warehouse_product_status_history wpsh')
				->join('INNER JOIN', 'warehouse_delivery wd', 'wpsh.warehouse_delivery_id = wd.id')
				->join('INNER JOIN', 'warehouse_product_operation wpo', 'wd.operation_id = wpo.id')
				->join('INNER JOIN', 'product_status ps', 'wpo.product_status_id = ps.id')
				->where(['ps.symbol' => ['issuing', 'issuing_reservation']])
				->orderBy(['wpsh.id' => SORT_ASC])
				->all();
		
		if(!empty($returnsFromIssues)) {
			foreach($returnsFromIssues as $rfi) {
				$this->update('warehouse_product_status_history', 
						['description' => \Yii::t('documents', 'Accounting of goods issued no. {number}', ['number' => $rfi['number']])],
						['id' => $rfi['id']]
				);
			}
		}
    }

    public function safeDown() {
        
    }

}
