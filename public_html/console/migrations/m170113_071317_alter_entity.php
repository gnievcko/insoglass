<?php

use yii\db\Schema;
use yii\db\Migration;
use common\models\ar\Entity;

class m170113_071317_alter_entity extends Migration {

    public function up() {
		$this->addColumn('entity', 'url_logo', 'VARCHAR(256) NULL DEFAULT NULL AFTER website_address');
		$this->addColumn('entity', 'is_certified', 'TINYINT(1) NULL DEFAULT NULL AFTER url_logo');
		$this->addColumn('entity', 'is_footer', 'TINYINT(1) NULL DEFAULT NULL AFTER is_certified');
		
		$entityDefault1 = Entity::find()->where(['vat_identification_number' => '9950196844'])->one();
		$entityDefault2 = Entity::find()->where(['vat_identification_number' => '7881116863'])->one();
		
		if(!empty($entityDefault1)) {
			$this->update('entity', [
					'url_logo' => 'logo_matbud.jpg',
					'is_certified' => 1,
					'is_footer' => 1,
			], ['id' => $entityDefault1->id]);
		}
		if(!empty($entityDefault2)) {
			$this->update('entity', [
					'url_logo' => 'logo_matbud_oddymianie.jpg',
					'is_certified' => 0,
					'is_footer' => 0,
			], ['id' => $entityDefault2->id]);
		}
    }

    public function down() {
    	$this->dropColumn('entity', 'is_footer');
    	$this->dropColumn('entity', 'is_certified');
    	$this->dropColumn('entity', 'url_logo');
    }

}
