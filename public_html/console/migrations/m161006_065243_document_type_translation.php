<?php

use yii\db\Migration;
use common\models\ar\DocumentType;
use common\models\ar\Language;

class m161006_065243_document_type_translation extends Migration {

    public function up() {
        $this->createDocumentTypeTranslation();
        $array = [
            'INVOICE' => 'Faktura',
            'REPORT_ORDER' => 'Raport - zamówienie',
            'REPORT_UTILIZATION' => 'Raport - utylizacje',
            'ACCEPTANCE_PROTOCOL' => 'Protokół odbioru robót',
            'GOODS_RECEIVED' => 'Przyjęcie zewnętrzne (PZ)',
            'GOODS_ISSUED' => 'Wydanie zewnętrzne (WZ)',
            'OFFER_DETAILS' => 'Szczegóły oferty',
            'LIGHT_MEASUREMENT' => 'Protokół z pomiaru natężenia oświetlenia',
            'SERVICE_PROTOCOL' => 'Protokół przeglądu serwisowego',
            'WEIGHT_PROTOCOL' => 'Protokół z przeważania ładunków CO2',
            'REPAIR_PROTOCOL' => 'Protokół z naprawy',
            'FIREEQ_ACCEPTANCE_PROTOCOL' => 'Protokół z oględzin',
            'MALFUNCTION_PROTOCOL' => 'Protokół z awarii',
        ];
        
        $languageId = Language::find()->where(['symbol' => 'pl'])->one()->id;
        
        foreach($array as $key => $translationText) {
            
            $documentType = DocumentType::find()->where(['symbol' => $key])->one()->id;
            $translation = new \common\models\ar\DocumentTypeTranslation();
            $translation->language_id = $languageId;
            $translation->document_type_id = $documentType;
            $translation->name = $translationText;
            $translation->insert();
        }
    }

    public function down() {
        $this->dropTable('document_type_translation');
    }

    private function createDocumentTypeTranslation() {
        $this->createTable('document_type_translation', [
            'document_type_id' => $this->integer(11),
            'language_id' => $this->integer(11),
            'name' => $this->string(128)->notNull(),
                ]);

        $this->addPrimaryKey('pk_document_type_translation', 'document_type_translation', array('document_type_id', 'language_id'));
        $this->addForeignKey('fk_document_type_translation_language', 'document_type_translation', 'language_id', 'language', 'id');
        $this->addForeignKey('fk_document_type_id_translation', 'document_type_translation', 'document_type_id', 'document_type', 'id');
    }

}
