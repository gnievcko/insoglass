<?php

use yii\db\Schema;
use yii\db\Migration;
use common\models\ar\Language;
use common\models\ar\TaskType;
use common\models\ar\TaskTypeReference;
use common\models\ar\TaskTypeReferenceTranslation;
use common\models\ar\TaskTypeTranslation;

class m161003_100315_insert_tast_type_order extends Migration {

    public function up() {
        $languageId = Language::find()->where(['symbol' => 'pl'])->one()->id;

        $this->insert('task_type', ['symbol' => 'associated_with_order']);
        $taskTypeId = $this->db->getLastInsertID();
        
        $this->insert('task_type_translation', [
            'task_type_id' => $taskTypeId,
            'language_id' => $languageId,
            'name' => 'Zadanie dotyczące zamówienia',
        ]);
        
        $this->insert('task_type_reference', [
            'task_type_id' => $taskTypeId,
            'symbol' => 'order_data',
            'name_table' => 'order',
            'name_column' => 'id',
            'name_expression' => 'order.title',
            'url_details' => 'order/details/{id}',
            'is_required' => 1,
        ]);
        
        $this->insert('task_type_reference_translation', [
            'reference_id' => $this->db->getLastInsertID(),
            'language_id' => $languageId,
            'name' => 'Dotyczy zamówienia',
        ]);

    }

    public function down() {
        $taskType1Id = TaskType::find()->where(['symbol' => 'associated_with_order'])->one()->id;
        $taskTypeReference1Id = TaskTypeReference::find()->where(['task_type_id' => $taskType1Id])->one()->id;

        TaskTypeReferenceTranslation::deleteAll(['reference_id' => $taskTypeReference1Id]);
        TaskTypeReference::deleteAll(['id' => $taskTypeReference1Id]);
        TaskTypeTranslation::deleteAll(['task_type_id' => [$taskType1Id]]);
        TaskType::deleteAll(['id' => [$taskType1Id]]);
        
        
    }

}
