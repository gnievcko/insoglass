<?php

use yii\db\Schema;
use yii\db\Migration;

class m160809_104229_alter_task extends Migration {

    public function up() {
		$this->alterColumn('task', 'is_active', 'TINYINT(1) NOT NULL DEFAULT 1 AFTER date_deactivation');
		$this->renameColumn('task', 'date_deactivation', 'date_completion');
		
		$this->dropForeignKey('fk_task_user_deactivating', 'task');
		$this->renameColumn('task', 'user_deactivating_id', 'user_completing_id');
		$this->addForeignKey('fk_task_user_completing', 'task', 'user_completing_id', 'user', 'id');
		
		$this->addColumn('task', 'is_complete', 'TINYINT(1) NOT NULL DEFAULT 0 AFTER date_deadline');
    }

    public function down() {
    	$this->dropColumn('task', 'is_complete');
    	$this->dropForeignKey('fk_task_user_completing', 'task');
    	$this->renameColumn('task', 'user_completing_id', 'user_deactivating_id');
    	$this->addForeignKey('fk_task_user_deactivating', 'task', 'user_deactivating_id', 'user', 'id');
    	$this->renameColumn('task', 'date_completion', 'date_deactivation');
    	$this->alterColumn('task', 'is_active', 'TINYINT(1) NOT NULL DEFAULT 1 AFTER date_deadline');
    }
}
