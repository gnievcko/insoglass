<?php

use yii\db\Schema;
use yii\db\Migration;

class m161018_124314_update_product_status_reference extends Migration {

    public function safeUp() {
    	$this->update('product_status_reference', ['name_expression' => 'CONCAT(order.title, " (", order.number, ")")'], 
    			['name_table' => 'order']);
    }

    public function safeDown() {
    	$this->update('product_status_reference', ['name_expression' => 'order.title'],
    			['name_table' => 'order']);
    }
    
}
