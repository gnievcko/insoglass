<?php

use yii\db\Schema;
use yii\db\Migration;

class m170814_053118_alter_production_path_step extends Migration {

    public function safeUp() {
        $this->addColumn('production_path_step', 'attribute_needed_ids', 'VARCHAR(64) NULL DEFAULT NULL');
        $this->addColumn('production_path_step', 'attribute_forbidden_ids', 'VARCHAR(64) NULL DEFAULT NULL');
        $this->addColumn('product_attribute_type', 'attribute_checked_ids', 'VARCHAR(64) NULL DEFAULT NULL AFTER variable_type');
        $this->addColumn('product_attribute_type', 'attribute_blocked_ids', 'VARCHAR(64) NULL DEFAULT NULL AFTER attribute_checked_ids');
    }

    public function safeDown() {
        $this->dropColumn('product_attribute_type', 'attribute_blocked_ids', 'VARCHAR(64) NULL DEFAULT NULL');
        $this->dropColumn('product_attribute_type', 'attribute_checked_ids', 'VARCHAR(64) NULL DEFAULT NULL');
        $this->dropColumn('production_path_step', 'attribute_forbidden_ids');
        $this->dropColumn('production_path_step', 'attribute_needed_ids');
    }

}
