<?php

use yii\db\Schema;
use yii\db\Migration;
use common\models\ar\Language;

class m160812_074552_create_alert_priority extends Migration {

    public function up() {
    	$tableOptions = null;
    	if($this->db->driverName === 'mysql') {
    		$tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
    	}

    	$this->createTable('alert_priority', [
    			'id' => $this->primaryKey(11),
    			'symbol' => $this->string(32)->notNull()->unique(),
    			'priority' => $this->integer(3)->notNull(),
    	], $tableOptions);
    	
    	$this->createTable('alert_priority_translation', [
    			'alert_priority_id' => $this->integer(11),
    			'language_id' => $this->integer(11),
    			'name' => $this->string(256)->notNull(),
    	], $tableOptions);
    	 
    	$this->addPrimaryKey('pk_alert_priority_translation', 'alert_priority_translation', ['alert_priority_id', 'language_id']);
    	$this->addForeignKey('fk_alert_priority_translation_language', 'alert_priority_translation', 'language_id', 'language', 'id');
    	$this->addForeignKey('fk_alert_priority_translation_priority', 'alert_priority_translation', 'alert_priority_id', 'alert_priority', 'id');
    	
    	$this->addColumn('alert', 'alert_priority_id', 'INT(11) NOT NULL AFTER alert_type_id');
    	$this->addForeignKey('fk_alert_alert_priority', 'alert', 'alert_priority_id', 'alert_priority', 'id');

    	$languageId = Language::find()->where(['symbol' => 'pl'])->one()->id;
    	$this->insert('alert_priority', ['symbol' => 'low', 'priority' => 10]);
    	$this->insert('alert_priority_translation', ['alert_priority_id' => $this->db->getLastInsertID(), 'language_id' => $languageId, 'name' => 'Niski']);
    	$this->insert('alert_priority', ['symbol' => 'medium', 'priority' => 20]);
    	$this->insert('alert_priority_translation', ['alert_priority_id' => $this->db->getLastInsertID(), 'language_id' => $languageId, 'name' => 'Średni']);
    	$this->insert('alert_priority', ['symbol' => 'high', 'priority' => 30]);
    	$this->insert('alert_priority_translation', ['alert_priority_id' => $this->db->getLastInsertID(), 'language_id' => $languageId, 'name' => 'Wysoki']);
    }

    public function down() {
        $this->dropForeignKey('fk_alert_alert_priority', 'alert');
		$this->dropColumn('alert', 'alert_priority_id');
		$this->dropTable('alert_priority_translation');
		$this->dropTable('alert_priority');

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp() {
    }

    public function safeDown() {
    }
    */
}
