<?php

use yii\db\Schema;
use yii\db\Migration;

class m171017_120257_create_qa_module extends Migration {

    public function safeUp() {
        $tableOptions = null;
        if($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }
        
        $this->createTable('qa_factor_type', [
            'id' => $this->primaryKey(11),
            'symbol' => $this->string(32)->notNull()->unique(),
            'variable_type' => $this->string(32)->notNull(),
            'priority' => $this->integer(5)->notNull(),
            'is_visible' => $this->boolean()->notNull()->defaultValue(0),
            'is_required' => $this->boolean()->notNull()->defaultValue(0),
        ], $tableOptions);
        
        $this->createTable('qa_factor_type_translation', [
            'qa_factor_type_id' => $this->integer(11),
            'language_id' => $this->integer(11),
            'name' => $this->string(256)->notNull(),
        ], $tableOptions);
        
        $this->addPrimaryKey('pk_qa_factor_type_translation', 'qa_factor_type_translation', ['qa_factor_type_id', 'language_id']);
        $this->addForeignKey('fk_qa_factor_type_translation_language', 'qa_factor_type_translation', 'language_id', 'language', 'id');
        $this->addForeignKey('fk_qa_factor_type_translation_shape', 'qa_factor_type_translation', 'qa_factor_type_id', 'qa_factor_type', 'id');
        
        $this->addColumn('activity', 'is_qa_required', 'TINYINT(1) NOT NULL DEFAULT 0');
        
        $this->createTable('production_task_qa_factor', [
            'id' => $this->primaryKey(11),
            'production_task_id' => $this->integer(11)->notNull(),
            'qa_factor_type_id' => $this->integer(11)->notNull(),
            'value' => $this->string(64)->notNull(),
            'user_id' => $this->integer(11)->notNull(),
            'date_creation' => $this->timestamp()->notNull(),
        ], $tableOptions);
        
        $this->alterColumn('production_task_qa_factor', 'date_creation', 'TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP');
        $this->addForeignKey('fk_production_task_qa_factor_task', 'production_task_qa_factor', 'production_task_id', 'production_task', 'id');
        $this->addForeignKey('fk_production_task_qa_factor_factor', 'production_task_qa_factor', 'qa_factor_type_id', 'qa_factor_type', 'id');
        $this->addForeignKey('fk_production_task_qa_factor_user', 'production_task_qa_factor', 'user_id', 'user', 'id');
    }

    public function safeDown() {
        $this->dropTable('production_task_qa_factor');
        $this->dropColumn('activity', 'is_qa_required');
        $this->dropTable('qa_factor_type_translation');
        $this->dropTable('qa_factor_type');
    }

}
