<?php

use yii\db\Schema;
use yii\db\Migration;
use common\models\ar\Province;

class m161107_121455_create_province_zip_code_role extends Migration {

    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp() {
    	$this->createTable('province_zip_code_rule', [
    			'id' => $this->primaryKey(11),
    			'province_id' => $this->integer(11),
    			'zip_code' => $this->string(16)->notNull(),
    	]);
    	 
    	$this->addForeignKey('fk_province_zip_code_rule_province', 'province_zip_code_rule', 'province_id', 'province', 'id');
    	 
    	$provinces = Province::find()->all();
    	foreach($provinces as $province) {
    		$province['name'] = mb_strtolower($province['name'], 'UTF-8');
    	}
    	  	 
    	$string = file_get_contents(realpath(dirname(__FILE__)).DIRECTORY_SEPARATOR.'postcodes.json');
    	$json = json_decode($string, true);
    	
    	foreach($json as $zipCode => $values) {
    		foreach($values as $provinceName) {
    			$this->insert('province_zip_code_rule', [
    					'province_id' => self::findProvinceIdByName($provinces, $provinceName),
    					'zip_code' => $zipCode,
    			]);
    		}
    	}
    	
    }

    public function safeDown() {
    	$this->dropForeignKey('fk_province_zip_code_rule_province', 'province_zip_code_rule');
    	$this->dropTable('province_zip_code_rule');
    }
    
    public function findProvinceIdByName($provinces, $name) {
    	foreach($provinces as $province) {
    		if($province['name'] == $name) {
    			return $province['id'];
    		}
    	}	
    }

}
