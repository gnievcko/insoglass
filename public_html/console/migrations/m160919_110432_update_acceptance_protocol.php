<?php

use yii\db\Schema;
use yii\db\Migration;
use common\documents\DocumentType;

class m160919_110432_update_acceptance_protocol extends Migration {

    public function safeUp() {
        $this->update('document_type', ['margin_top' => 100, 'margin_bottom' => 50, 'margin_left' => 15, 'margin_right' => 15], ['symbol' => DocumentType::ACCEPTANCE_PROTOCOL]);
    }

    public function safeDown() {
        $this->update('document_type', ['margin_top' => 0, 'margin_bottom' => 0, 'margin_left' => 0, 'margin_right' => 0], ['symbol' => DocumentType::ACCEPTANCE_PROTOCOL]);
    }
}
