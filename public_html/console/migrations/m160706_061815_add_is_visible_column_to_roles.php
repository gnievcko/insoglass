<?php

use yii\db\Migration;

/**
 * Handles adding is_visible_column to table `roles`.
 */
class m160706_061815_add_is_visible_column_to_roles extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
    	$this->addColumn('role', 'is_visible', 'BOOLEAN NOT NULL DEFAULT 1');
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropColumn('role', 'is_visible');
    }
}
