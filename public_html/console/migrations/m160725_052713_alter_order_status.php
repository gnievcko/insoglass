<?php

use yii\db\Schema;
use yii\db\Migration;

class m160725_052713_alter_order_status extends Migration {

    public function up() {
        $this->addColumn('order_status', 'is_terminal', $this->boolean()->notNull());
    }

    public function down() {
        $this->dropColumn('order_status', 'is_terminal');
    }
}
