<?php

use yii\db\Schema;
use yii\db\Migration;
use common\helpers\Utility;
use common\models\ar\Language;

use common\documents\DocumentType;
use common\documents\sections\SectionsMapper;
use common\models\ar\DocumentSection;

class m160928_115108_insert_documents_config extends Migration {

    public function up() {
    	$transaction = \Yii::$app->db->beginTransaction();
    		 
    	$headerSectionId = DocumentSection::find()->where(['symbol' =>  SectionsMapper::COMPANY_SHORT_HEADER])->one()->id;
    	$footerSectionId = DocumentSection::find()->where(['symbol' =>  SectionsMapper::COMPANY_FOOTER])->one()->id;

    	$this->insert('document_section', ['symbol' => SectionsMapper::REPORT_UTILIZATION]);
    	$reportId = $this->db->getLastInsertID();

    	$this->insert('document_type', [
    			'symbol' => SectionsMapper::REPORT_UTILIZATION,
    			'document_section_header_id' => $headerSectionId,
    			'document_section_footer_id' => $footerSectionId,
    			'margin_top' => 70,
    			'margin_bottom' => 50,
    			'margin_left'=> 15,
    			'margin_right' => 15,
    			'format' => 'A4',
    			'orientation' => 'P',
    	]);

    	$documentTypeId = $this->db->getLastInsertId();

    	$this->insert('document_type_section', [
    			'document_type_id' => $documentTypeId,
    			'document_section_id' => $reportId,
    			'sequence_number' => 1,
    	]);
    	
    	$this->insert('document_section', ['symbol' => SectionsMapper::MALFUNCTION_PROTOCOL]);
    	$reportId = $this->db->getLastInsertID();
    	
    	$this->insert('document_type', [
    			'symbol' => SectionsMapper::MALFUNCTION_PROTOCOL,
    			'document_section_header_id' => $headerSectionId,
    			'document_section_footer_id' => $footerSectionId,
    			'margin_top' => 70,
    			'margin_bottom' => 50,
    			'margin_left'=> 15,
    			'margin_right' => 15,
    			'format' => 'A4',
    			'orientation' => 'P',
    	]);
    	
    	$documentTypeId = $this->db->getLastInsertId();
    	
    	$this->insert('document_type_section', [
    			'document_type_id' => $documentTypeId,
    			'document_section_id' => $reportId,
    			'sequence_number' => 1,
    	]);
    	
    	$this->insert('document_section', ['symbol' => SectionsMapper::FIREEQ_ACCEPTANCE_PROTOCOL]);
    	$reportId = $this->db->getLastInsertID();
    	 
    	$this->insert('document_type', [
    			'symbol' => SectionsMapper::FIREEQ_ACCEPTANCE_PROTOCOL,
    			'document_section_header_id' => $headerSectionId,
    			'document_section_footer_id' => $footerSectionId,
    			'margin_top' => 70,
    			'margin_bottom' => 50,
    			'margin_left'=> 15,
    			'margin_right' => 15,
    			'format' => 'A4',
    			'orientation' => 'P',
    	]);
    	 
    	$documentTypeId = $this->db->getLastInsertId();
    	 
    	$this->insert('document_type_section', [
    			'document_type_id' => $documentTypeId,
    			'document_section_id' => $reportId,
    			'sequence_number' => 1,
    	]);
    	
    	$this->addColumn('user', 'pesel', 'VARCHAR(20) DEFAULT NULL AFTER phone2');
    	$this->addColumn('user', 'id_number', 'VARCHAR(20) DEFAULT NULL AFTER pesel');
    	$this->addColumn('user', 'address_id', 'INT(11) DEFAULT NULL AFTER id_number'); 
    	$this->addColumn('user', 'date_employment', 'TIMESTAMP AFTER is_active');
    	
    	$this->addForeignKey('fk_user_address', 'user', 'address_id', 'address', 'id');
    	
    	$transaction->commit();
    }


    public function down() {
    	$documentType1Id = DocumentType::find()->where(['symbol' => SectionsMapper::MALFUNCTION_PROTOCOL])->one()->id;
    	$documentType2Id = DocumentType::find()->where(['symbol' => SectionsMapper::FIREEQ_ACCEPTANCE_PROTOCOL])->one()->id;
    	$documentType3Id = DocumentType::find()->where(['symbol' => SectionsMapper::REPORT_UTILIZATION])->one()->id;
    	 
    	DocumentTypeSection::deleteAll(['document_type_id' => [$documentType1Id, $documentType2Id, $documentType3Id]]);
    	DocumentType::deleteAll(['id' => [$documentType1Id, $documentType2Id, $documentType3Id]]);
    	DocumentSection::deleteAll(['symbol' => [SectionsMapper::REPORT_UTILIZATION, SectionsMapper::FIREEQ_ACCEPTANCE_PROTOCOL,
    			SectionsMapper::MALFUNCTION_PROTOCOL]]);
    	
        $this->dropColumn('user', 'pesel');
    	$this->dropColumn('user', 'id_number');
    	$this->dropColumn('user', 'address_id');
    	$this->dropColumn('user', 'date_employment');
    	$this->addForeignKey('fk_user_address', 'user');
   

    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp() {
    }

    public function safeDown() {
    }
    */
}
