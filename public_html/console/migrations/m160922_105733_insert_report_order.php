<?php

use common\documents\DocumentType;
use common\models\ar\DocumentSection;
use common\models\ar\DocumentTypeSection;
use yii\db\Schema;
use yii\db\Migration;

class m160922_105733_insert_report_order extends Migration {

    public function up() {

    	$headerSectionId = DocumentSection::find()->where(['symbol' => 'COMPANY_SHORT_HEADER'])->one()->id;
    	$footerSectionId = DocumentSection::find()->where(['symbol' => 'COMPANY_FOOTER'])->one()->id;

    	$this->insert('document_section', ['symbol' => 'REPORT_ORDER']);
    	$orderReportId = $this->db->getLastInsertID();

    	$this->insert('document_type', [
    			'symbol' => 'REPORT_ORDER',
    			'document_section_header_id' => $headerSectionId,
    			'document_section_footer_id' => $footerSectionId,
    			'margin_top' => 70,
    			'margin_bottom' => 50,
    			'margin_left'=> 15,
    			'margin_right' => 15,
    			'format' => 'A4',
    			'orientation' => 'P',
    	]);

    	$documentTypeId = $this->db->getLastInsertId();

    	$this->insert('document_type_section', [
    			'document_type_id' => $documentTypeId,
    			'document_section_id' => $orderReportId,
    			'sequence_number' => 1,
    	]);

    }

    public function down() {
       	$documentTypeId = DocumentType::find()->where(['symbol' => 'REPORT_ORDER'])->one()->id;

        DocumentTypeSection::delete(['document_type_id' => $documentTypeId, $documentType2Id]);
        DocumentType::delete(['id' => $documentTypeId]);
        DocumentSection::delete(['symbol' => 'REPORT_ORDER']);

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp() {
    }

    public function safeDown() {
    }
    */
}
