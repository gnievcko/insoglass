<?php

use yii\db\Schema;
use yii\db\Migration;
use common\helpers\Utility;

class m160824_102645_insert_email_type extends Migration {

    public function up() {
    	$this->insert('email_type', [
    			'symbol' => Utility::EMAIL_TYPE_RESET_PASSWORD,
    			'is_active' => true,
    			'tags' => '{link_with_token},{firstname},{lastname}',
    	]);
    }

    public function down() {
    	$this->delete('email_type', [
    			'symbol' => Utility::EMAIL_TYPE_RESET_PASSWORD,
    	]);
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp() {
    }

    public function safeDown() {
    }
    */
}
