<?php

use yii\db\Migration;

class m170306_061449_alter_order_artificial extends Migration {

    public function safeUp() {
		$this->addColumn('order', 'is_artificial', 'TINYINT(1) NOT NULL DEFAULT 0');
		$this->update('order', ['is_artificial' => true], 'number LIKE "ZF/%"');
    }

    public function safeDown() {
        $this->dropColumn('order', 'is_artificial');
    }

}
