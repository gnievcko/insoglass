<?php

use yii\db\Schema;
use yii\db\Migration;

class m160929_100603_alter_user extends Migration {

    public function up() {

    	$this->addColumn('user', 'phone3', 'VARCHAR(32) DEFAULT NULL AFTER phone2');
    	$this->addColumn('user_action', 'ip_address', 'VARCHAR(46) AFTER date_action');
    	
    	$this->addColumn('warehouse_supplier', 'phone', 'VARCHAR(32) DEFAULT NULL AFTER name');
    	$this->addColumn('warehouse_supplier', 'email', 'VARCHAR(64) DEFAULT NULL AFTER phone');
    	$this->addColumn('warehouse_supplier', 'address_id', 'INT(11) DEFAULT NULL AFTER email');

    	$this->addForeignKey('fk_warehouse_supplier_address', 'warehouse_supplier', 'address_id', 'address', 'id');
    }

    public function down() {
    	$this->dropColumn('user', 'phone3');
    	$this->dropColumn('user_action', 'ip_address');
    	
    	$this->dropColumn('warehouse_supplier', 'phone');
    	$this->dropColumn('warehouse_supplier', 'email');
    	$this->dropColumn('warehouse_supplier', 'address_id');
    	
    	$this->dropForeignKey('fk_warehouse_supplier_address', 'warehouse_supplier');
    	
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp() {
    }

    public function safeDown() {
    }
    */
}
