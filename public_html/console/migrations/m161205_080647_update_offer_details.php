<?php

use yii\db\Schema;
use yii\db\Migration;
use common\documents\DocumentType;

class m161205_080647_update_offer_details extends Migration {

    public function up() {
		$this->update('document_type', ['margin_bottom' => 25], ['symbol' => DocumentType::OFFER_DETAILS]);
    }

    public function down() {
    	$this->update('document_type', ['margin_bottom' => 50], ['symbol' => DocumentType::OFFER_DETAILS]);
    }

}
