<?php

use yii\db\Schema;
use yii\db\Migration;

class m171019_090959_alter_production_task extends Migration {

    public function safeUp() {
        $this->addColumn('production_task', 'qa_factor_value', 'TEXT NULL DEFAULT NULL AFTER total_count_failed');
    }

    public function safeDown() {
        $this->dropColumn('production_task', 'qa_factor_value');
    }

}
