<?php

use yii\db\Schema;
use yii\db\Migration;
use common\models\ar\Language;
use common\models\ar\DocumentType;
use common\models\ar\DocumentTypeTranslation;

class m161108_140639_insert_documents_translation extends Migration {

    private $array = [
            'DELIVERY_DETAILS' => 'Szczegóły dostawy',
            'CUSTOM_PROTOCOL' => 'Dowolny protokół'
        ];
    
    public function up() {

        $languageId = Language::find()->where(['symbol' => 'pl'])->one()->id;

        foreach ($this->array as $key => $translationText) {

            $documentType = DocumentType::find()->where(['symbol' => $key])->one()->id;
            $translation = new DocumentTypeTranslation();
            $translation->language_id = $languageId;
            $translation->document_type_id = $documentType;
            $translation->name = $translationText;
            $translation->insert();
        }
    }

    public function down() {
        
        $languageId = Language::find()->where(['symbol' => 'pl'])->one()->id;
        
        foreach ($this->array as $key => $translationText) {
            $documentType = DocumentType::find()->where(['symbol' => $key])->one()->id;
            $translation = DocumentTypeTranslation::find()->where(['language_id' => $languageId, 'document_type_id' => $documentType])->one();
            $translation->delete();
        }
    }

    /*
      // Use safeUp/safeDown to run migration code within a transaction
      public function safeUp() {
      }

      public function safeDown() {
      }
     */
}
