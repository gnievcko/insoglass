<?php

use yii\db\Schema;
use yii\db\Migration;

class m161128_095909_create_user_attachment extends Migration {

public function up() {
    	$tableOptions = null;
    	if($this->db->driverName === 'mysql') {
    		$tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
    	}
    	
    	$this->createTable('user_attachment', [
    			'id' => $this->primaryKey(11),
    			'user_attached_id' => $this->integer(11)->notNull(),
    			'user_id' => $this->integer(11)->notNull(),
    			'url_file' => $this->string(256)->notNull(),
    			'name' => $this->string(256)->notNull(),
    			'description' => $this->string(512),
    			'date_creation' => $this->timestamp()->notNull(),
    	], $tableOptions);
    	 
    	$this->addForeignKey('fk_user_attachment_user_attached', 'user_attachment', 'user_attached_id', 'user', 'id');
    	$this->addForeignKey('fk_user_attachment_user', 'user_attachment', 'user_id', 'user', 'id');
    	$this->alterColumn('user_attachment', 'date_creation', 'TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP');    	
    }

    public function down() {
        $this->dropTable('user_attachment');
    }
}
