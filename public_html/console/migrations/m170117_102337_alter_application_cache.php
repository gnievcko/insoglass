<?php

use yii\db\Schema;
use yii\db\Migration;
use common\models\ar\Entity;
use common\models\ar\ApplicationCache;
use yii\db\Query;

class m170117_102337_alter_application_cache extends Migration {

    public function up() {
    	$entityId = (new Query())->select(['e.id'])->from('entity e')->where(['e.is_active' => true])->orderBy(['id' => SORT_ASC])->limit(1)->scalar();
    	$otherEntityIds = array_column((new Query())->select(['e.id'])->from('entity e')->where(['<>', 'id', $entityId])->orderBy(['id' => SORT_ASC])->all(), 'id');    	
    	
		$this->addColumn('application_cache', 'entity_id', 'INT(11) NULL DEFAULT NULL');
		$this->addForeignKey('fk_application_cache_entity', 'application_cache', 'entity_id', 'entity', 'id');
		if(!empty($entityId)) {
			$this->update('application_cache', ['entity_id' => intval($entityId)]);
		}
		
		if(!empty($otherEntityIds)) {
			foreach($otherEntityIds as $id) {
				$this->insert('application_cache', [
						'date_last_invoice' => null,
						'entity_id' => $id,
				]);		
			}
		}
    }

    public function down() {
    	$entityId = Entity::find()->where(['is_active' => 1])->orderBy(['id' => SORT_ASC])->limit(1)->one()->id;
    	ApplicationCache::deleteAll(['and', ['<>', 'entity_id', $entityId], 'entity_id IS NOT NULL']);
    	$this->dropForeignKey('fk_application_cache_entity', 'application_cache');
    	$this->dropColumn('application_cache', 'entity_id');
    }
}
