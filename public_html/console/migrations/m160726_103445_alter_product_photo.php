<?php

use yii\db\Schema;
use yii\db\Migration;
use common\models\ar\OfferedProductPhoto;
use common\models\ar\ProductPhoto;

class m160726_103445_alter_product_photo extends Migration {

	public function up() {
        $this->addColumn('offered_product_photo', 'url_thumbnail', 'VARCHAR(256) NOT NULL AFTER url_photo');
        $this->addColumn('product_photo', 'url_thumbnail', 'VARCHAR(256) NOT NULL AFTER url_photo');
        
        OfferedProductPhoto::updateAll(['url_thumbnail' => '']);
        ProductPhoto::updateAll(['url_thumbnail' => '']);
    }

    public function down() {
    	$this->dropColumn('product_photo', 'url_thumbnail');
        $this->dropColumn('offered_product_photo', 'url_thumbnail');
    }
}
