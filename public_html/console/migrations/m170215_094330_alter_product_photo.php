<?php

use yii\db\Schema;
use yii\db\Migration;
use yii\db\Query;

class m170215_094330_alter_product_photo extends Migration {

    public function up() {
		$this->addColumn('product_photo', 'description', 'VARCHAR(512) NULL DEFAULT NULL');
		$this->addColumn('offered_product_photo', 'description', 'VARCHAR(512) NULL DEFAULT NULL');
    }

    public function down() {
        $languageId = (new Query())->select(['l.id'])->from('language l')->where(['l.symbol' => 'pl'])->scalar();
    	$photos = (new Query())->select(['p.id', 'p.description'])->from('product_photo p')->all();
        
    	if(!empty($photos)) {
    		foreach($photos as $photo) {
    			$this->insert('product_photo_translation', [
    					'product_photo_id' => $photo['id'],
    					'language_id' => $languageId,
    					'description' => $photo['description'],
    			]);
    		}
    	}
    	
    	$photos = (new Query())->select(['p.id', 'p.description'])->from('offered_product_photo p')->all();
    	
    	if(!empty($photos)) {
    		foreach($photos as $photo) {
    			$this->insert('offered_product_photo_translation', [
    					'offered_product_photo_id' => $photo['id'],
    					'language_id' => $languageId,
    					'description' => $photo['description'],
    			]);
    		}
    	}
    	
    	$this->dropColumn('offered_product_photo', 'description');
    	$this->dropColumn('product_photo', 'description');
    }

}
