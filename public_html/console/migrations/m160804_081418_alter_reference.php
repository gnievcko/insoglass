<?php

use yii\db\Schema;
use yii\db\Migration;
use common\models\ar\ProductStatusReference;

class m160804_081418_alter_reference extends Migration {

    public function up() {
		$this->addColumn('product_status_reference', 'name_expression', 'VARCHAR(512) NULL DEFAULT NULL AFTER name_column');
		$this->addColumn('task_type_reference', 'name_expression', 'VARCHAR(256) NOT NULL AFTER name_column');
		$this->addColumn('product_status_reference', 'url_details', 'VARCHAR(512) NULL DEFAULT NULL AFTER name_expression');
		$this->addColumn('task_type_reference', 'url_details', 'VARCHAR(256) NULL DEFAULT NULL AFTER name_expression');
		
		ProductStatusReference::updateAll(['name_expression' => 'order.title', 'url_details' => 'order/details/{id}'], ['name_table' => 'order']);
		ProductStatusReference::updateAll(['name_expression' => 'CONCAT(COALESCE(user.first_name, ""), "|", COALESCE(user.last_name, ""), "|", user.email)', 'url_details' => 'employee/details/{id}'], ['name_table' => 'user']);
		
		$this->alterColumn('product_status_reference', 'name_expression', 'VARCHAR(512) NOT NULL AFTER name_column');	
    }

    public function down() {
    	$this->dropColumn('task_type_reference', 'url_details');
		$this->dropColumn('product_status_reference', 'url_details');
		$this->dropColumn('task_type_reference', 'name_expression');
		$this->dropColumn('product_status_reference', 'name_expression');
    }

}
