<?php

use yii\db\Schema;
use yii\db\Migration;
use common\models\ar\WarehouseProduct;
use common\models\ar\ProductStatus;
use common\models\ar\ProductStatusReference;
use common\models\ar\Language;
use common\models\ar\ProductStatusReferenceTranslation;
use common\models\ar\ProductStatusTranslation;
use yii\db\Expression;

class m160927_065923_alter_reservations extends Migration {

    public function up() {
		$this->addColumn('warehouse_product', 'count_available', 'INT(10) NOT NULL AFTER count');
		WarehouseProduct::updateAll(['count_available' => new Expression('count')]);
		
		$languageId = Language::find()->where(['symbol' => 'pl'])->one()->id;
		
		$this->insert('product_status', [
				'symbol' => 'undo_reservation',
				'is_active' => 1,
				'is_positive' => 1,
		]);
		$undoReservationId = $this->db->getLastInsertID();
		
		$this->insert('product_status_translation', [
				'product_status_id' => $undoReservationId,
				'language_id' => $languageId,
				'name' => 'Cofnięcie rezerwacji',
		]);
		$this->insert('product_status_reference', [
				'product_status_id' => $undoReservationId,
				'symbol' => 'orderReserved',
				'name_table' => 'order',
				'name_column' => 'id',
				'name_expression' => 'order.title',
				'url_details' => 'order/details/{id}',
				'is_required' => 1,
		]);
		$this->insert('product_status_reference_translation', [
				'reference_id' => $this->db->getLastInsertID(),
				'language_id' => $languageId,
				'name' => 'Zamówienie',
		]);
		
		$this->insert('product_status', [
				'symbol' => 'issuing_reservation',
				'is_active' => 1,
				'is_positive' => 0,
		]);
		$issuingReservationId = $this->db->getLastInsertID();
		
		$this->insert('product_status_translation', [
				'product_status_id' => $issuingReservationId,
				'language_id' => $languageId,
				'name' => 'Wydanie w ramach rezerwacji',
		]);
		
		$this->insert('product_status_reference', [
				'product_status_id' => $issuingReservationId,
				'symbol' => 'orderReserved',
				'name_table' => 'order',
				'name_column' => 'id',
				'name_expression' => 'order.title',
				'url_details' => 'order/details/{id}',
				'is_required' => 1,
		]);
		$this->insert('product_status_reference_translation', [
				'reference_id' => $this->db->getLastInsertID(),
				'language_id' => $languageId,
				'name' => 'Zamówienie',
		]);
    }

    public function down() {
    	$undoReservationId = ProductStatus::find()->where(['symbol' => 'undo_reservation'])->one()->id;
    	$undoReservationReferences = ProductStatusReference::find()->where(['product_status_id' => $undoReservationId])->all();
    	$issuingReservationId = ProductStatus::find()->where(['symbol' => 'issuing_reservation'])->one()->id;
    	$issuingReservationReferences = ProductStatusReference::find()->where(['product_status_id' => $issuingReservationId])->all();
    	
    	ProductStatusReferenceTranslation::deleteAll(['reference_id' => array_merge(array_column($undoReservationReferences, 'id'), array_column($issuingReservationReferences, 'id'))]);
    	ProductStatusReference::deleteAll(['id' => array_merge(array_column($undoReservationReferences, 'id'), array_column($issuingReservationReferences, 'id'))]);
    	ProductStatusTranslation::deleteAll(['product_status_id' => [$undoReservationId, $issuingReservationId]]);
    	ProductStatus::deleteAll(['id' => [$undoReservationId, $issuingReservationId]]);
    	
        $this->dropColumn('warehouse_product', 'count_available');
    }
}
