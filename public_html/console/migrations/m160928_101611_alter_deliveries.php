<?php

use yii\db\Migration;

class m160928_101611_alter_deliveries extends Migration {

    public function up() {
		$this->addColumn('warehouse_delivery', 'is_confirmed', $this->boolean()->notNull());
		$this->addColumn('warehouse_product', 'count_ordered', 'INT(10) NOT NULL DEFAULT 0 AFTER count_available');
    }

    public function down() {
        $this->dropColumn('warehouse_product', 'count_ordered');
        $this->dropColumn('warehouse_delivery', 'is_confirmed');
    }
}
