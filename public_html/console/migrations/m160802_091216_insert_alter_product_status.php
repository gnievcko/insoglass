<?php

use yii\db\Migration;
use common\models\ar\ProductStatusReference;
use common\models\ar\ProductStatusTranslation;
use common\models\ar\ProductStatus;
use common\models\ar\Language;

class m160802_091216_insert_alter_product_status extends Migration {

    public function up() {    	
    	$this->addColumn('product_status', 'is_active', 'TINYINT(1) NOT NULL DEFAULT 1 AFTER symbol');
		
    	$languageId = Language::find()->where(['symbol' => 'pl'])->one()->id;
    	
		$this->insert('product_status', ['symbol' => 'addition']);
		$productStatusId = $this->db->getLastInsertID();
		$this->insert('product_status_translation', [
				'product_status_id' => $productStatusId,
				'language_id' => $languageId,
				'name' => 'Dodanie',
		]);
		
		$this->insert('product_status', ['symbol' => 'reservation']);
		$productStatusId = $this->db->getLastInsertID();
		$this->insert('product_status_translation', [
				'product_status_id' => $productStatusId,
				'language_id' => $languageId,
				'name' => 'Rezerwacja',
		]);
		$this->insert('product_status_reference', [
				'product_status_id' => $productStatusId,
				'symbol' => 'orderReserved',
				'name_table' => 'order',
				'name_column' => 'id',
		]);
		
		$this->insert('product_status', ['symbol' => 'issuing']);
		$productStatusId = $this->db->getLastInsertID();
		$this->insert('product_status_translation', [
				'product_status_id' => $productStatusId,
				'language_id' => $languageId,
				'name' => 'Wydanie',
		]);
		$this->insert('product_status_reference', [
				'product_status_id' => $productStatusId,
				'symbol' => 'userIssued',
				'name_table' => 'user',
				'name_column' => 'id',
		]);
		
		$this->insert('product_status', ['symbol' => 'return']);
		$productStatusId = $this->db->getLastInsertID();
		$this->insert('product_status_translation', [
				'product_status_id' => $productStatusId,
				'language_id' => $languageId,
				'name' => 'Zwrot',
		]);
		
		$this->insert('product_status', ['symbol' => 'utilization']);
		$productStatusId = $this->db->getLastInsertID();
		$this->insert('product_status_translation', [
				'product_status_id' => $productStatusId,
				'language_id' => $languageId,
				'name' => 'Utylizacja',
		]);
    }

    public function down() {
    	ProductStatusReference::deleteAll();
    	ProductStatusTranslation::deleteAll();
    	ProductStatus::deleteAll();
    	
        $this->dropColumn('product_status', 'is_active');
    }
}
