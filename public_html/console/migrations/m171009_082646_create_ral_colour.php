<?php

use yii\db\Schema;
use yii\db\Migration;

class m171009_082646_create_ral_colour extends Migration {

    public function up() {
        $tableOptions = null;
        if($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }
        
        $this->createTable('ral_colour_group', [
            'id' => $this->primaryKey(11),
            'symbol' => $this->string(32)->notNull()->unique(),
            'index' => $this->integer(11)->notNull(),
            'rgb_hash' => $this->string(7)->notNull(),
        ], $tableOptions);
        
        
        $this->createTable('ral_colour_group_translation', [
            'ral_colour_group_id' => $this->integer(11),
            'language_id' => $this->integer(11),
            'name' => $this->string(128)->notNull(),
        ], $tableOptions);
        
        $this->addPrimaryKey('pk_ral_colour_group_translation', 'ral_colour_group_translation', array('ral_colour_group_id', 'language_id'));
        $this->addForeignKey('fk_ral_colour_group_language', 'ral_colour_group_translation', 'language_id', 'language', 'id');
        $this->addForeignKey('fk_ral_colour_group_translation_ral_colour_group', 'ral_colour_group_translation', 'ral_colour_group_id', 'ral_colour_group', 'id');
        
        
        
        $this->createTable('ral_colour', [
            'id' => $this->primaryKey(11),
            'ral_colour_group_id' => $this->integer(11),
            'symbol' => $this->string(32)->notNull()->unique(),
            'rgb_hash' => $this->string(7)->notNull(),
            'is_colour_reversed' => $this->smallInteger(1)->defaultValue(0),
        ], $tableOptions);
        
        $this->addForeignKey('fk_ral_colour_ral_colour_group', 'ral_colour', 'ral_colour_group_id', 'ral_colour_group', 'id');
        
        
        $this->createTable('ral_colour_translation', [
            'ral_colour_id' => $this->integer(11),
            'language_id' => $this->integer(11),
            'name' => $this->string(128)->notNull(),
        ], $tableOptions);
        
        $this->addPrimaryKey('pk_ral_colour_translation', 'ral_colour_translation', array('ral_colour_id', 'language_id'));
        $this->addForeignKey('fk_ral_colour_language', 'ral_colour_translation', 'language_id', 'language', 'id');
        $this->addForeignKey('fk_ral_colour_translation_ral_colour', 'ral_colour_translation', 'ral_colour_id', 'ral_colour', 'id');
        
        $this->addColumn('order_offered_product', 'ral_colour_id', $this->integer(11));
        $this->addForeignKey('fk_order_offered_product_ral_colour', 'order_offered_product', 'ral_colour_id', 'ral_colour', 'id');
    }

    public function down() {
        $this->dropForeignKey('fk_ral_colour_translation_ral_colour', 'ral_colour_translation');
        $this->dropForeignKey('fk_ral_colour_language', 'ral_colour_translation');
        $this->dropPrimaryKey('pk_ral_colour_translation', 'ral_colour_translation');
        $this->dropTable('ral_colour_translation');
       
        $this->dropForeignKey('fk_ral_colour_ral_colour_group', 'ral_colour');
        $this->dropTable('ral_colour');
        
        $this->dropForeignKey('fk_ral_colour_group_translation_ral_colour_group', 'ral_colour_group_translation');
        $this->dropForeignKey('fk_ral_colour_group_language', 'ral_colour_group_translation');
        $this->dropPrimaryKey('pk_ral_colour_group_translation', 'ral_colour_group_translation');
        $this->dropTable('ral_colour_group_translation');

        $this->dropTable('ral_colour_group');
        
    }

}
