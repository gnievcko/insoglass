<?php

use yii\db\Schema;
use yii\db\Migration;

class m160811_075745_alter_warehouse_delivery_product extends Migration {

    public function up() {
        $this->addColumn('warehouse_delivery_product', 'note', 'string(150) NULL');
    }

    public function down() {
        $this->dropColumn('warehouse_delivery_product', 'note');
    }
}
