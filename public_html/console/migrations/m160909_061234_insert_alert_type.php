<?php

use yii\db\Schema;
use yii\db\Migration;
use common\models\ar\AlertPriorityTranslation;
use common\models\ar\AlertPriority;
use common\models\ar\AlertTypeTranslation;
use common\models\ar\AlertType;
use common\models\ar\Language;

class m160909_061234_insert_alert_type extends Migration {

    public function up() {
		AlertPriorityTranslation::deleteAll();
		AlertPriority::deleteAll();
		
		$this->dropForeignKey('fk_alert_alert_priority', 'alert');
		$this->dropColumn('alert', 'alert_priority_id');
		$this->addColumn('alert_type', 'alert_priority_id', 'INT(11) NOT NULL AFTER symbol');
		$this->addForeignKey('fk_alert_type_alert_priority', 'alert_type', 'alert_priority_id', 'alert_priority', 'id');
		
		$languageId = Language::find()->where(['symbol' => 'pl'])->one()->id;
    	$this->insert('alert_priority', ['symbol' => 'warning', 'priority' => 10]);
    	$warningPriority = $this->db->getLastInsertID();
    	$this->insert('alert_priority_translation', ['alert_priority_id' => $warningPriority, 'language_id' => $languageId, 'name' => 'Ostrzeżenie']);
    	$this->insert('alert_priority', ['symbol' => 'problem', 'priority' => 20]);
    	$problemPriority = $this->db->getLastInsertID();
    	$this->insert('alert_priority_translation', ['alert_priority_id' => $problemPriority, 'language_id' => $languageId, 'name' => 'Problem']);
    	
    	$this->insert('alert_type', ['symbol' => 'order_deadline_warning', 'alert_priority_id' => $warningPriority]);
    	$this->insert('alert_type_translation', ['alert_type_id' => $this->db->getLastInsertID(), 'language_id' => $languageId, 'name' => 'Zbliża się termin realizacji']);
    	$this->insert('alert_type', ['symbol' => 'order_deadline_problem', 'alert_priority_id' => $problemPriority]);
    	$this->insert('alert_type_translation', ['alert_type_id' => $this->db->getLastInsertID(), 'language_id' => $languageId, 'name' => 'Nadszedł termin realizacji']);
    	$this->insert('alert_type', ['symbol' => 'task_deadline_warning', 'alert_priority_id' => $warningPriority]);
    	$this->insert('alert_type_translation', ['alert_type_id' => $this->db->getLastInsertID(), 'language_id' => $languageId, 'name' => 'Zbliża się termin realizacji']);
    	$this->insert('alert_type', ['symbol' => 'task_deadline_problem', 'alert_priority_id' => $problemPriority]);
    	$this->insert('alert_type_translation', ['alert_type_id' => $this->db->getLastInsertID(), 'language_id' => $languageId, 'name' => 'Nadszedł termin realizacji']);
    	$this->insert('alert_type', ['symbol' => 'product_count_below_minimal', 'alert_priority_id' => $problemPriority]);
    	$this->insert('alert_type_translation', ['alert_type_id' => $this->db->getLastInsertID(), 'language_id' => $languageId, 'name' => 'Stan produktu poniżej minimalnego']);
    }

    public function down() {
    	AlertTypeTranslation::deleteAll();
    	AlertType::deleteAll();    	
    	AlertPriorityTranslation::deleteAll();
    	AlertPriority::deleteAll();
    	
    	$this->dropForeignKey('fk_alert_type_alert_priority', 'alert_type');
    	$this->dropColumn('alert_type', 'alert_priority_id');
    	$this->addColumn('alert', 'alert_priority_id', 'INT(11) NOT NULL AFTER alert_type_id');
    	$this->addForeignKey('fk_alert_alert_priority', 'alert', 'alert_priority_id', 'alert_priority', 'id');    	
    	
        $languageId = Language::find()->where(['symbol' => 'pl'])->one()->id;
    	$this->insert('alert_priority', ['symbol' => 'low', 'priority' => 10]);
    	$this->insert('alert_priority_translation', ['alert_priority_id' => $this->db->getLastInsertID(), 'language_id' => $languageId, 'name' => 'Niski']);
    	$this->insert('alert_priority', ['symbol' => 'medium', 'priority' => 20]);
    	$this->insert('alert_priority_translation', ['alert_priority_id' => $this->db->getLastInsertID(), 'language_id' => $languageId, 'name' => 'Średni']);
    	$this->insert('alert_priority', ['symbol' => 'high', 'priority' => 30]);
    	$this->insert('alert_priority_translation', ['alert_priority_id' => $this->db->getLastInsertID(), 'language_id' => $languageId, 'name' => 'Wysoki']);
    }
}
