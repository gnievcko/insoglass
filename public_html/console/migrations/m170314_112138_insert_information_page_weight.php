<?php

use yii\db\Migration;
use yii\db\Query;

class m170314_112138_insert_information_page_weight extends Migration {

    public function safeUp() {
    	$languageId = (new Query())->select(['l.id'])->from('language l')->where(['l.symbol' => 'pl'])->one()['id'];
    	
    	$this->insert('information_page', [
    			'symbol' => 'default_weight_tables',
    			'is_active' => 1,
    	]);
    	$informationPageId = $this->db->getLastInsertID();
    	 
    	$this->insert('information_page_translation', [
    			'information_page_id' => $informationPageId,
    			'language_id' => $languageId,
    			'title' => 'Domyślne pozycje na protokole przeważania ładunków CO2',
    			'content' => '',
    			'is_active' => 1,
    	]);
    	
    	$this->insert('information_page', [
    			'symbol' => 'default_weight_additional_text',
    			'is_active' => 1,
    	]);
    	$informationPageId = $this->db->getLastInsertID();
    	
    	$this->insert('information_page_translation', [
    			'information_page_id' => $informationPageId,
    			'language_id' => $languageId,
    			'title' => 'Domyślny dodatkowy tekst na protokole przeważania ładunków CO2',
    			'content' => '',
    			'is_active' => 1,
    	]);
    	
    	$this->insert('information_page', [
    			'symbol' => 'default_weight_key',
    			'is_active' => 1,
    	]);
    	$informationPageId = $this->db->getLastInsertID();
    	 
    	$this->insert('information_page_translation', [
    			'information_page_id' => $informationPageId,
    			'language_id' => $languageId,
    			'title' => 'Domyślna legenda na protokole przeważania ładunków CO2',
    			'content' => '',
    			'is_active' => 1,
    	]);
    }

    public function safeDown() {
        $ids = (new Query())->select(['ip.id'])->from('information_page ip')->where(['ip.symbol' => ['default_weight_tables', 'default_weight_additional_text', 'default_weight_key']])->column();
    	 
    	$this->delete('information_page_translation', ['information_page_id' => $ids]);
    	$this->delete('information_page', ['id' => $ids]);
    }

}
