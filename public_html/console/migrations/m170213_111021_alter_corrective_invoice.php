<?php

use yii\db\Migration;
use yii\db\Query;
use common\models\ar\NumberTemplate;
use common\models\ar\NumberTemplateStorage;

class m170213_111021_alter_corrective_invoice extends Migration {

    public function safeUp() {
    	$entityIds = array_column((new Query())->select(['e.id'])->from('entity e')->orderBy(['id' => SORT_ASC])->all(), 'id');
    	
    	foreach($entityIds as $entityId) {
	    	$this->insert('number_template', [
	    			'symbol' => 'corrective_invoice',
	    			'separator' => '/',
	    			'template' => '{noInYear:inc}/{year}',
	    			'entity_id' => $entityId,
	    			'is_active' => 1,
	    	]);
	    	$templateId = $this->db->getLastInsertId();
	    	
	    	$this->insert('number_template_storage', [
	    			'number_template_id' => $templateId,
	    			'current_number' => NULL,
	    	]);
    	}
    	
    	$documentTypeId = (new Query())->select(['dt.id'])->from('document_type dt')->where(['dt.symbol' => 'CORRECTIVE_INVOICE'])->scalar();
    	$invoiceProductsId = (new Query())->select(['ds.id'])->from('document_section ds')->where(['ds.symbol' => 'INVOICE_PRODUCTS'])->scalar();
    	$invoicePaymentId = (new Query())->select(['ds.id'])->from('document_section ds')->where(['ds.symbol' => 'CORRECTIVE_INVOICE_PAYMENT'])->scalar();
    	
    	$this->insert('document_section', ['symbol' => 'INVOICE_PRODUCTS_PRECORRECTED']);
    	$sectionId = $this->db->getLastInsertID();
    	
    	$this->update('document_type_section', ['sequence_number' => 4], ['document_type_id' => $documentTypeId, 'document_section_id' => $invoicePaymentId]);
    	$this->update('document_type_section', ['sequence_number' => 3], ['document_type_id' => $documentTypeId, 'document_section_id' => $invoiceProductsId]);
    	$this->insert('document_type_section', ['document_type_id' => $documentTypeId, 'document_section_id' => $sectionId, 'sequence_number' => 2]);
    }

    public function safeDown() {
    	$sectionId = (new Query())->select(['ds.id'])->from('document_section ds')->where(['ds.symbol' => 'INVOICE_PRODUCTS_PRECORRECTED'])->scalar();
    	$this->delete('document_type_section', ['document_type_id' => $sectionId]);
    	$this->delete('document_section', ['id' => $sectionId]);
    	
    	$this->update('document_type_section', ['sequence_number' => 2], ['document_type_id' => $documentTypeId, 'document_section_id' => $invoiceProductsId]);
    	$this->update('document_type_section', ['sequence_number' => 3], ['document_type_id' => $documentTypeId, 'document_section_id' => $invoicePaymentId]);
    	
    	$documentTypeId = (new Query())->select(['dt.id'])->from('document_type dt')->where(['dt.symbol' => 'CORRECTIVE_INVOICE'])->scalar();
    	$invoiceProductsId = (new Query())->select(['ds.id'])->from('document_section ds')->where(['ds.symbol' => 'INVOICE_PRODUCTS'])->scalar();
    	$invoicePaymentId = (new Query())->select(['ds.id'])->from('document_section ds')->where(['ds.symbol' => 'CORRECTIVE_INVOICE_PAYMENT'])->scalar();
    	
    	
    	
        $numberTemplateIds = array_column(NumberTemplate::find()->where(['symbol' => 'corrective_invoice'])->all(), 'id');
    	NumberTemplateStorage::deleteAll(['number_template_id' => $numberTemplateIds]);
    	NumberTemplate::deleteAll(['id' => $numberTemplateIds]);  
    }

}
