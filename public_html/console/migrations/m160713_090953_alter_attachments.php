<?php

use yii\db\Schema;
use yii\db\Migration;

class m160713_090953_alter_attachments extends Migration {

    public function up() {
		$this->addColumn('order_attachment', 'name', 'VARCHAR(256) NOT NULL AFTER url_file');
    }

    public function down() {
        $this->dropColumn('order_attachment', 'name');
    }

}
