<?php

use yii\db\Schema;
use yii\db\Migration;

class m170511_063636_alter_order extends Migration {

    public function up() {
        $this->addColumn('order', 'number_for_client', 'VARCHAR(48) NULL DEFAULT NULL AFTER number');
    }

    public function down() {
        $this->dropColumn('order', 'number_for_client');
    }

}
