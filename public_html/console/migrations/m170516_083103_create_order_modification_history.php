<?php

use yii\db\Schema;
use yii\db\Migration;

class m170516_083103_create_order_modification_history extends Migration {

	public function up() {	
		$tableOptions = null;
		if($this->db->driverName === 'mysql') {
			$tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
		}
		
		if($this->db->getTableSchema('order_modification_history', true) === null) {
    		$this->createTable('order_modification_history', [
    				'id' => $this->primaryKey(11),
    				'order_id' => $this->integer(11),
    				'modification' => $this->text()->notNull(),
    		], $tableOptions);
    		
    		$this->addForeignKey('fk_order_modification_history_order', 'order_modification_history', 'order_id', 'order', 'id');
		}
		
	}
	
	public function down() {
		$this->dropTable('order_modification_history');
	}

}
