<?php

use yii\db\Schema;
use yii\db\Migration;

class m160823_123528_create_user_order_salesman_history extends Migration {

    public function up() {
    	if($this->db->driverName === 'mysql') {
    		$tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
    	}
    	 
    	$this->createTable('user_order_salesman_history', [
    			'id' => $this->primaryKey(11),
    			'user_id' => $this->integer(11)->notNull(),
    			'date' => $this->timestamp()->notNull(),
    			'history_id' => $this->integer(11)->notNull(),
    			'is_added' => $this->boolean()->notNull(),
    	], $tableOptions);
    	
    	$this->addForeignKey('fk_user_order_salesman_history_user', 'user_order_salesman_history', 'user_id', 'user', 'id'); 
    	$this->addForeignKey('fk_user_order_salesman_history_order_history', 'user_order_salesman_history', 'history_id', 'order_history', 'id');
    }

    public function down() {
        $this->dropForeignKey('fk_user_order_salesman_history_user', 'user_order_salesman_history');
    	$this->dropForeignKey('fk_user_order_salesman_history_order_history', 'user_order_salesman_history');
    	
    	$this->dropTable('user_order_salesman_history');
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp() {
    }

    public function safeDown() {
    }
    */
}
