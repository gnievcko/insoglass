<?php

use yii\db\Schema;
use yii\db\Migration;

class m170712_102401_alter_province extends Migration {

    public function safeUp() {
        $this->addColumn('province', 'is_artificial', 'TINYINT(1) NOT NULL DEFAULT 0');
    }

    public function safeDown() {
        $this->dropColumn('province', 'is_artificial');
    }
}
