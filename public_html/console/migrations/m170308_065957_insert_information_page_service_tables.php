<?php

use yii\db\Migration;
use common\models\ar\Language;
use common\models\ar\InformationPage;

class m170308_065957_insert_information_page_service_tables extends Migration {

    public function safeUp() {
    	$languageId = Language::find()->where(['symbol' => 'pl'])->one()->id;
    	
    	$this->insert('information_page', [
    			'symbol' => 'default_service_tables',
    			'is_active' => 1,
    	]);
    	$informationPageId = $this->db->getLastInsertID();
    	
    	$this->insert('information_page_translation', [
    			'information_page_id' => $informationPageId,
    			'language_id' => $languageId,
    			'title' => 'Domyślne produkty w protokole serwisowym',
    			'content' => '',
    			'is_active' => 1,
    	]);
    }

    public function safeDown() {
        $ids = InformationPage::find()->where(['symbol' => ['default_service_tables']])->select(['id'])->column();
    	
    	$this->delete('information_page_translation', ['information_page_id' => $ids]);
    	$this->delete('information_page', ['id' => $ids]);
    }

}
