<?php

use yii\db\Migration;
use common\helpers\Utility;

class m161018_062208_insert_invoice_number_template extends Migration {

    public function safeUp() {
        $this->insert('number_template', [
            'symbol' => Utility::NUMBER_TEMPLATE_INVOICE,
            'separator' => '/',
            'template' => '{noInYear:inc}/{year}',
            'is_active' => 1
        ]);
        
        $numberTemplateId = Yii::$app->db->lastInsertID;
        $this->insert('number_template_storage', [
            'number_template_id' => $numberTemplateId,
        ]);
    }

    public function safeDown() {
        $this->delete('number_template_storage', (new \yii\db\Query)->select(['id'])
            ->from('number_template')->where(['symbol' => Utility::NUMBER_TEMPLATE_INVOICE])->one()['id']);
        $this->delete('number_template', ['symbol' => Utility::NUMBER_TEMPLATE_INVOICE]);
    }
}