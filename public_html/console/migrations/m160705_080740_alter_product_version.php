<?php

use yii\db\Schema;
use yii\db\Migration;

class m160705_080740_alter_product_version extends Migration {

    public function up() {
    	$this->addColumn('offered_product', 'parent_offered_product_id', 'INT(11) NULL DEFAULT NULL AFTER symbol');
    	$this->addColumn('offered_product', 'priority', 'INT(5) NULL DEFAULT NULL AFTER parent_offered_product_id');
    	$this->addForeignKey('fk_offered_product_parent', 'offered_product', 'parent_offered_product_id', 'offered_product', 'id');
    	$this->dropTable('offered_product_version_translation');
    	$this->dropTable('offered_product_version');
    	
    	$this->addColumn('product', 'parent_product_id', 'INT(11) NULL DEFAULT NULL AFTER symbol');
    	$this->addColumn('product', 'priority', 'INT(5) NULL DEFAULT NULL AFTER parent_product_id');
    	$this->addForeignKey('fk_product_parent', 'product', 'parent_product_id', 'product', 'id');
    	$this->dropTable('product_version_translation');
    	$this->dropTable('product_version');
    	
    	$this->addColumn('order_status', 'is_visible', 'TINYINT(1) NOT NULL');
    	$this->addColumn('order_status', 'is_visible_for_client', 'TINYINT(1) NOT NULL');
    	$this->addColumn('order_status_translation', 'name_client', 'VARCHAR(128) NULL DEFAULT NULL');
    	$this->alterColumn('order_history', 'message', 'TEXT NULL DEFAULT NULL');
    }

    public function down() {
        
    }
}
