<?php

use yii\db\Schema;
use yii\db\Migration;

class m160707_100616_alter_email_type extends Migration {

    public function up() {
		$this->dropForeignKey('fk_email_type_email_config', 'email_type');
		$this->dropColumn('email_type', 'email_config_id');
		$this->renameColumn('email_type', 'is_enabled', 'is_active');
    }

    public function down() {
    	$this->renameColumn('email_type', 'is_active', 'is_enabled');
        $this->addColumn('email_type', 'email_config_id', 'INT(11) NULL AFTER id');
        $this->addForeignKey('fk_email_type_email_config', 'email_type', 'email_config_id', 'email_config', 'id');
    }
}
