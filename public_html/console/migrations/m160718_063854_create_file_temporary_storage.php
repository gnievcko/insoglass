<?php

use yii\db\Migration;

/**
 * Handles the creation for table `file_temporary_storage`.
 */
class m160718_063854_create_file_temporary_storage extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('file_temporary_storage', [
            'id' => $this->bigPrimaryKey (16),
            'file_path' => $this->string(255)->notNull()->unique(),
            'date_creation' => $this->timestamp()->notNull(),
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('file_temporary_storage');
    }
}
