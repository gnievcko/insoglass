<?php

use yii\db\Migration;
use common\models\ar\Language;
use common\models\ar\InformationPage;

class m170119_124753_insert_information_page_default_text extends Migration {

    public function safeUp() {
    	$languageId = Language::find()->where(['symbol' => 'pl'])->one()->id;
    	
    	$this->insert('information_page', [
    			'symbol' => 'default_description',
    			'is_active' => 1,
    	]);
    	$informationPageId = $this->db->getLastInsertID();
    	
    	$this->insert('information_page_translation', [
    			'information_page_id' => $informationPageId,
    			'language_id' => $languageId,
    			'title' => 'Domyślny opis',
    			'content' => '',
    			'is_active' => 1,
    	]);
    	
    	$this->insert('information_page', [
    			'symbol' => 'default_description_note',
    			'is_active' => 1,
    	]);
    	$informationPageId = $this->db->getLastInsertID();
    	 
    	$this->insert('information_page_translation', [
    			'information_page_id' => $informationPageId,
    			'language_id' => $languageId,
    			'title' => 'Domyślny opis bezpośrednio pod tabelką',
    			'content' => '',
    			'is_active' => 1,
    	]);
    	
    	$this->insert('information_page', [
    			'symbol' => 'default_description_cont',
    			'is_active' => 1,
    	]);
    	$informationPageId = $this->db->getLastInsertID();
    	
    	$this->insert('information_page_translation', [
    			'information_page_id' => $informationPageId,
    			'language_id' => $languageId,
    			'title' => 'Domyślne uwagi',
    			'content' => '',
    			'is_active' => 1,
    	]);
    }

    public function safeDown() {
        $ids = InformationPage::find()->where(['symbol' => ['default_description', 'default_description_note', 'default_description_cont']])->select(['id'])->column();
        
        $this->delete('information_page_translation', ['information_page_id' => $ids]);
        $this->delete('information_page', ['id' => $ids]);
    }
}
