<?php

use yii\db\Migration;
use common\models\ar\NumberTemplate;
use common\models\ar\NumberTemplateStorage;

class m170111_060950_insert_number_template_advance_invoice extends Migration {

    public function safeUp() {
    	$this->insert('number_template', [
    			'symbol' => 'advance_invoice',
    			'separator' => '/',
    			'template' => '{noInYear:inc}/ZAL/{year}',
    			'is_active' => 1,
    	]);
    	$templateId = $this->db->getLastInsertId();
    	 
    	$this->insert('number_template_storage', [
    			'number_template_id' => $templateId,
    			'current_number' => NULL,
    	]);
    }

    public function safeDown() {
    	$numberTemplateId = NumberTemplate::find()->where(['symbol' => 'advance_invoice'])->one()->id;
    	NumberTemplateStorage::deleteAll(['number_template_id' => $numberTemplateId]);
    	NumberTemplate::deleteAll(['id' => $numberTemplateId]);    	
    }

}
