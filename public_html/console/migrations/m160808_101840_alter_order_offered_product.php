<?php

use yii\db\Schema;
use yii\db\Migration;

class m160808_101840_alter_order_offered_product extends Migration {

    public function up() {
        $this->dropForeignKey('fk_order_offered_product_product', 'order_offered_product');
        $this->dropForeignKey('fk_order_offered_product_warehouse_product', 'order_offered_product');
        $this->dropIndex('fk_order_offered_product_product', 'order_offered_product');
        $this->dropIndex('fk_order_offered_product_warehouse_product', 'order_offered_product');

        $this->renameColumn('order_offered_product', 'warehouse_product_id', 'product_id');

    	$this->addForeignKey('fk_order_offered_product_product', 'order_offered_product', 'product_id', 'product', 'id');
    	$this->addForeignKey('fk_order_offered_product_offered_product', 'order_offered_product', 'offered_product_id', 'offered_product', 'id');

    }

    public function down() {
        $this->dropForeignKey('fk_order_offered_product_offered_product', 'order_offered_product');
        $this->dropForeignKey('fk_order_offered_product_product', 'order_offered_product');
        $this->dropIndex('fk_order_offered_product_offered_product', 'order_offered_product');
        $this->dropIndex('fk_order_offered_product_product', 'order_offered_product');

        $this->renameColumn('order_offered_product', 'product_id', 'warehouse_product_id');

    	$this->addForeignKey('fk_order_offered_product_warehouse_product', 'order_offered_product', 'warehouse_product_id', 'warehouse_product', 'id');
    	$this->addForeignKey('fk_order_offered_product_product', 'order_offered_product', 'offered_product_id', 'offered_product', 'id');
    }
}
