<?php

use yii\db\Migration;
use common\models\ar\WarehouseProductStatusHistory;
use common\models\ar\ProductStatus;
use common\helpers\Utility;
use yii\db\Query;

class m161202_100037_alter_warehouse_product extends Migration {

    public function up() {
    	$statusId = ProductStatus::find()->where(['symbol' => Utility::PRODUCT_STATUS_ADDITION])->one()->id;
    	
		$this->addColumn('warehouse_product_status_history', 'warehouse_delivery_id', 'INT(11) NULL DEFAULT NULL');
		$this->addForeignKey('fk_warehouse_product_status_history_delivery', 'warehouse_product_status_history', 'warehouse_delivery_id', 'warehouse_delivery', 'id');
		
		$entries = WarehouseProductStatusHistory::find()->where(['product_status_id' => $statusId])->all();
		foreach($entries as $entry) {
			$deliveryId = (new Query())->select(['wd.id'])
					->from('warehouse_delivery wd')
	    			->join('INNER JOIN', 'warehouse_delivery_product wdp', 'wd.id = wdp.warehouse_delivery_id')
	    			->Where(['wdp.product_id' => $entry->warehouseProduct->product_id])
	    			->andWhere(['wdp.count' => $entry->count])
	    			->andWhere('DATE_FORMAT(wd.date_creation, "%Y-%m-%d %H:%i") = DATE_FORMAT("'.$entry->date_creation.'", "%Y-%m-%d %H:%i")')
	    			->andWhere('TIMESTAMPDIFF(SECOND, wd.date_creation, "'.$entry->date_creation.'") <= 10')
	    			->limit(1)
					->scalar();
			
			if(!empty($deliveryId)) {
				$this->update('warehouse_product_status_history', ['warehouse_delivery_id' => $deliveryId], ['id' => $entry->id]);		
			}
		}
    }

    public function down() {
    	$this->dropForeignKey('fk_warehouse_product_status_history_delivery', 'warehouse_delivery_id');
        $this->dropColumn('warehouse_product_status_history', 'warehouse_delivery_id');
    }
}
