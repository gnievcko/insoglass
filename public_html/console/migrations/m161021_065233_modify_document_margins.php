<?php

use yii\db\Schema;
use yii\db\Migration;

class m161021_065233_modify_document_margins extends Migration {

    public function safeUp() {
    	$this->update('document_type', ['margin_top' => 50], ['in', 'symbol', 
    			['REPORT_ORDER', 'REPORT_UTILIZATION']    			
    	]);
    }

    public function safeDown() {
    	$this->update('document_type', ['margin_top' => 70], ['in', 'symbol',
    			['REPORT_ORDER', 'REPORT_UTILIZATION']
    	]);
    }

}
