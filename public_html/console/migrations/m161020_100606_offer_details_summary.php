<?php

use yii\db\Query;
use yii\db\Migration;
use common\documents\sections\SectionsMapper;
use common\documents\DocumentType;

class m161020_100606_offer_details_summary extends Migration {

    public function safeUp() {
        $offerDetailsId = $this->getOfferDetailsId();
        
        $this->delete('document_type_section', [
            'sequence_number' => -1,
            'document_type_id' => $offerDetailsId,
        ]);
        
        $this->insert('document_section', [
            'symbol' => SectionsMapper::OFFER_DETAILS_SUMMARY
        ]);
        
        $summaryId = Yii::$app->db->lastInsertID;
        $this->insert('document_type_section', [
            'document_type_id' => $offerDetailsId,
            'document_section_id' => $summaryId,
            'sequence_number' => 3,
        ]);
    }

    public function safeDown() {
        $offerDetailsId = $this->getOfferDetailsId();
        $this->delete('document_type_section', [
            'document_type_id' => $offerDetailsId,
            'sequence_number' => 3,
        ]);
        
        $this->delete('document_section', [
            'symbol' => SectionsMapper::OFFER_DETAILS_SUMMARY,
        ]);
        
        $this->insert('document_type_section', [
            'sequence_number' => -1,
            'document_type_id' => $offerDetailsId,
            'document_section_id' => $this->getShortHeaderId(),
        ]);
    }
    
    private function getOfferDetailsId() {
        return (new Query())->select(['id'])
            ->from('document_type')
            ->where(['symbol' => DocumentType::OFFER_DETAILS])
            ->one()['id'];
    }
    
    private function getShortHeaderId() {
        return (new Query())->select(['id'])
            ->from('document_section')
            ->where(['symbol' => SectionsMapper::COMPANY_SHORT_HEADER])
            ->one()['id'];
    }
}
