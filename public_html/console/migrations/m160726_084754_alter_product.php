<?php

use yii\db\Schema;
use yii\db\Migration;
use common\models\ar\OfferedProduct;
use common\models\ar\Product;

class m160726_084754_alter_product extends Migration {

    public function up() {
		$this->addColumn('offered_product', 'user_id', 'INT(11) NOT NULL AFTER priority');
		$this->addColumn('product', 'user_id', 'INT(11) NOT NULL AFTER count_minimal');
		
		OfferedProduct::updateAll(['user_id' => 1]);
		Product::updateAll(['user_id' => 1]);
		
		$this->addForeignKey('fk_offered_product_user', 'offered_product', 'user_id', 'user', 'id');
		$this->addForeignKey('fk_product_user', 'product', 'user_id', 'user', 'id');
    }

    public function down() {
		$this->dropForeignKey('fk_product_user', 'product');
		$this->dropForeignKey('fk_offered_product_user', 'offered_product');
		
		$this->dropColumn('product', 'user_id');
		$this->dropColumn('offered_product', 'user_id');
    }
}
