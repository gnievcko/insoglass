<?php

use yii\db\Schema;
use yii\db\Migration;

class m171006_103055_alter_production_path extends Migration {

    public function up() {
        $this->addColumn('production_path', 'is_active', $this->integer(1)->notNull()->defaultValue(1));
    }

    public function down() {
        $this->dropColumn('production_path', 'is_active');
    }
}
