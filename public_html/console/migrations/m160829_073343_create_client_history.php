<?php

use yii\db\Schema;
use yii\db\Migration;

class m160829_073343_create_client_history extends Migration {

    public function up() {
    	$transaction = \Yii::$app->db->beginTransaction();
    	
    	$tableOptions = null;
    	if($this->db->driverName === 'mysql') {
    		$tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
    	}
    		
    	$this->createTable('client_modification_history', [
    			'id' => $this->primaryKey(11),
    			'modification' => $this->text()->notNull(),
    	], $tableOptions);
    	
    	$this->createTable('user_modification_history', [
    			'id' => $this->primaryKey(11),
    			'modification' => $this->text()->notNull(),
    	], $tableOptions);
    	
    	$transaction->commit();
    }

    public function down() {
        $this->dropTable('client_modification_history');
        $this->dropTable('user_modification_history');
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp() {
    }

    public function safeDown() {
    }
    */
}
