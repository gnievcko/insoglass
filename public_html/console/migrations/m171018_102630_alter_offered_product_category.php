<?php

use yii\db\Schema;
use yii\db\Migration;

class m171018_102630_alter_offered_product_category extends Migration {
    public function safeUp() {
        $this->addColumn('offered_product_category', 'effort_factor', 'INT(7) NULL DEFAULT NULL AFTER parent_category_id');
    }

    public function safeDown() {
        $this->dropColumn('offered_product_category', 'effort_factor');
    }
}
