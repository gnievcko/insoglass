<?php

use yii\db\Schema;
use yii\db\Migration;

class m171018_122500_alter_product_attribute_type_translation extends Migration {
    public function safeUp() {
        $this->addColumn('product_attribute_type_translation', 'hint', 'TEXT NULL DEFAULT NULL');
    }

    public function safeDown() {
        $this->dropColumn('product_attribute_type_translation', 'hint');
    }
}
