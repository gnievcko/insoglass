<?php

use yii\db\Schema;
use yii\db\Migration;

class m170915_095945_create_muntin extends Migration {

    public function up() {
        $tableOptions = null;
        if($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }
        
        $this->createTable('muntin', [
            'id' => $this->primaryKey(11),
            'symbol' => $this->string(32)->notNull()->unique(),
        ], $tableOptions);
        
        $this->createTable('muntin_translation', [
            'muntin_id' => $this->integer(11),
            'language_id' => $this->integer(11),
            'name' => $this->string(256)->notNull(),
        ], $tableOptions);
        
        $this->addPrimaryKey('pk_muntin_translation', 'muntin_translation', ['muntin_id', 'language_id']);
        $this->addForeignKey('fk_muntin_translation_language', 'muntin_translation', 'language_id', 'language', 'id');
        $this->addForeignKey('fk_muntin_translation_muntin', 'muntin_translation', 'muntin_id', 'muntin', 'id');      

        $this->addColumn('order_offered_product', 'muntin_id', $this->integer(11));
        $this->addForeignKey('tk_order_offered_product_muntin', 'order_offered_product', 'muntin_id', 'muntin', 'id');
    }

    public function down() {
        $this->dropForeignKey('tk_order_offered_product_muntin', 'order_offered_product');
        $this->dropColumn('order_offered_product', 'muntin_id');
        $this->dropForeignKey('fk_muntin_translation_language', 'muntin_translation');
        $this->dropForeignKey('fk_muntin_translation_muntin', 'muntin_translation');
        $this->dropTable('muntin_translation');
        $this->dropTable('muntin');     
    }

}
