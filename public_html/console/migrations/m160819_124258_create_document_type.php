<?php

use yii\db\Schema;
use yii\db\Migration;

class m160819_124258_create_document_type extends Migration {

    private $tableName = 'document_type';

    public function up() {
    	$tableOptions = null;
    	if($this->db->driverName === 'mysql') {
    		$tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
    	}

    	$this->createTable($this->tableName, [
    			'id' => $this->primaryKey(11),
    			'symbol' => $this->string(30)->notNull()->unique(),
    			'description' => $this->text(),
    	], $tableOptions);
    }

    public function down() {
        $this->dropTable($this->tableName);
    }
}
