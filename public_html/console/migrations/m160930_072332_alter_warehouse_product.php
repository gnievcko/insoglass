<?php

use yii\db\Schema;
use yii\db\Migration;

class m160930_072332_alter_warehouse_product extends Migration {

    public function up() {
  		$this->addColumn('product', 'warehouse_supplier_id', 'INT(11) DEFAULT NULL AFTER `index`');
    	$this->addForeignKey('fk_product_warehouse_supplier', 'product', 'warehouse_supplier_id', 'warehouse_supplier', 'id');  	
    }

    public function down() {
    	$this->dropForeignKey('fk_product_warehouse_supplier');
       	$this->dropColumn('product', 'warehouse_supplier_id');
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp() {
    }

    public function safeDown() {
    }
    */
}
