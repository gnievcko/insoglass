<?php

use yii\db\Schema;
use yii\db\Migration;

class m160829_061657_alter_document_type extends Migration {

    public function up() {
    	$this->addColumn('document_type', 'margin_top', 'INT(11) NULL DEFAULT 0');
    	$this->addColumn('document_type', 'margin_bottom', 'INT(11) NULL DEFAULT 0');
    	$this->addColumn('document_type', 'margin_left', 'INT(11) NULL DEFAULT 0');
    	$this->addColumn('document_type', 'margin_right', 'INT(11) NULL DEFAULT 0');
    	$this->addColumn('document_type', 'format', 'VARCHAR(16) NULL DEFAULT "A4"');
    	$this->addColumn('document_type', 'orientation', 'VARCHAR(16) NULL DEFAULT "P"');

        $this->update('document_type', ['margin_top' => 100, 'margin_bottom' => '50', 'margin_right' => 15, 'margin_left' => 15]);
    }

    public function down() {
    	$this->dropColumn('document_type', 'margin_top');
    	$this->dropColumn('document_type', 'margin_bottom');
    	$this->dropColumn('document_type', 'margin_left');
    	$this->dropColumn('document_type', 'margin_right');
    	$this->dropColumn('document_type', 'format');
    	$this->dropColumn('document_type', 'orientation');
    }
}
