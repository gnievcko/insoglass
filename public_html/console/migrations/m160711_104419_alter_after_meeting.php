<?php

use yii\db\Schema;
use yii\db\Migration;
use common\models\ar\Language;
use common\models\ar\Company;
use common\models\ar\Role;

class m160711_104419_alter_after_meeting extends Migration {

    public function up() {
    	$tableOptions = null;
    	if($this->db->driverName === 'mysql') {
    		$tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
    	}
		
		$this->createTable('company_group', [
				'id' => $this->primaryKey(11),
				'symbol' => $this->string(32)->notNull()->unique(),
				'is_predefined' => $this->boolean()->notNull()->defaultValue(0),
		], $tableOptions);
		
		$this->createTable('company_group_translation', [
				'company_group_id' => $this->integer(11),
				'language_id' => $this->integer(11),
				'name' => $this->string(256)->notNull(),
		], $tableOptions);
		 
		$this->addPrimaryKey('pk_company_group_translation', 'company_group_translation', ['company_group_id', 'language_id']);
		$this->addForeignKey('fk_company_group_translation_language', 'company_group_translation', 'language_id', 'language', 'id');
		$this->addForeignKey('fk_company_group_translation_group', 'company_group_translation', 'company_group_id', 'company_group', 'id');
		
		$this->createTable('company_group_set', [
				'company_id' => $this->integer(11),
				'company_group_id' => $this->integer(11),
		], $tableOptions);
		
		$this->addPrimaryKey('pk_company_group_set', 'company_group_set', ['company_id', 'company_group_id']);
		$this->addForeignKey('fk_company_group_set_company', 'company_group_set', 'company_id', 'company', 'id');
		$this->addForeignKey('fk_company_group_set_company_group', 'company_group_set', 'company_group_id', 'company_group', 'id');
		
		$languageId = Language::find()->where('symbol = :symbol', [':symbol' => 'pl'])->one()->id;
		$this->insert('company_group', ['id' => 1, 'symbol' => 'all_default_group', 'is_predefined' => 1]);		
		$this->insert('company_group_translation', ['company_group_id' => 1, 'language_id' => 1, 'name' => 'Wszystkie firmy']);
		$companies = Company::find()->all();
		foreach($companies as $company) {
			$this->insert('company_group', ['symbol' => Yii::$app->security->generateRandomString(16), 'is_predefined' => 1]);
			$companyGroupId = $this->db->getLastInsertID();
			$this->insert('company_group_translation', ['company_group_id' => $companyGroupId, 'language_id' => 1, 'name' => $company->name]);
			
			$this->insert('company_group_set', ['company_id' => $company->id, 'company_group_id' => 1]);
			$this->insert('company_group_set', ['company_id' => $company->id, 'company_group_id' => $companyGroupId]);
		}
		
		$this->dropTable('user_company_salesman');
		$this->createTable('user_company_salesman', [
				'user_id' => $this->integer(11),
				'company_group_id' => $this->integer(11)
		], $tableOptions);
		
		$this->addPrimaryKey('pk_user_company_salesman', 'user_company_salesman', ['user_id', 'company_group_id']);
		$this->addForeignKey('fk_user_company_salesman_user', 'user_company_salesman', 'user_id', 'user', 'id');
		$this->addForeignKey('fk_user_company_salesman_company_group', 'user_company_salesman', 'company_group_id', 'company_group', 'id');
		
		$this->insert('role', ['symbol' => 'main_salesman']);
		$roleId = $this->db->getLastInsertID();
		$this->insert('role_translation', [
				'role_id' => $roleId,
				'language_id' => $languageId,
				'name' => 'Kierownik sprzedaży'
		]);
		
		$this->dropColumn('order_history', 'price');
		$this->addColumn('order_offered_product', 'price', 'DECIMAL(10,2) NULL DEFAULT NULL AFTER count');
		$this->addColumn('order_offered_product', 'currency_id', 'INT(11) NULL DEFAULT NULL AFTER price');
		$this->addForeignKey('fk_order_offered_product_currency', 'order_offered_product', 'currency_id', 'currency', 'id');
		$this->addColumn('offered_product', 'is_artificial', 'TINYINT(1) NOT NULL DEFAULT 0 AFTER priority');
		
		$this->addColumn('product', 'is_visible_for_client', 'TINYINT(1) NOT NULL DEFAULT 0 AFTER count_minimal');
		
		$this->addColumn('order', 'user_id', 'INT(11) NOT NULL AFTER order_history_last_id');
		$this->addForeignKey('fk_order_user', 'order', 'user_id', 'user', 'id');
    }

    public function down() {
    	$tableOptions = null;
    	if($this->db->driverName === 'mysql') {
    		$tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
    	}
    	
    	$this->dropForeignKey('fk_order_user', 'order');
        $this->dropColumn('order', 'user_id');
        $this->dropColumn('product', 'is_visible_for_client');
        $this->dropColumn('offered_product', 'is_artificial');
        $this->dropForeignKey('fk_order_offered_product_currency', 'order_offered_product');
        $this->dropColumn('order_offered_product', 'currency_id');
        $this->dropColumn('order_offered_product', 'price');
        $this->addColumn('order_history', 'price', 'DECIMAL(10,2) NULL DEFAULT NULL AFTER message');
        
        $roleId = Role::find()->where('symbol = :symbol', [':symbol' => 'main_salesman'])->one()->id;
        $this->delete('role_translation', 'role_id = :roleId', [':roleId' => $roleId]);
        $this->delete('role', 'id = :id', [':id' => $roleId]);
        
        $this->dropTable('user_company_salesman');
        $this->createTable('user_company_salesman', [
        		'user_id' => $this->integer(11),
        		'company_id' => $this->integer(11)
        ], $tableOptions);
        
        $this->addPrimaryKey('pk_user_company_salesman', 'user_company_salesman', ['user_id', 'company_id']);
        $this->addForeignKey('fk_user_company_salesman_user', 'user_company_salesman', 'user_id', 'user', 'id');
        $this->addForeignKey('fk_user_company_salesman_company', 'user_company_salesman', 'company_id', 'company', 'id');
         
        $this->dropTable('company_group_set');
        $this->dropTable('company_group_translation');
        $this->dropTable('company_group');
    }
}
