<?php

use yii\db\Schema;
use yii\db\Migration;

class m160825_073134_alter_order_history extends Migration {

    public function up() {
    	$this->addColumn('order_history', 'date_reminder', 'DATE NULL DEFAULT NULL AFTER date_deadline');
    }

    public function down() {
        $this->dropColumn('order_history', 'date_reminder');
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp() {
    }

    public function safeDown() {
    }
    */
}
