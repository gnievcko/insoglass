<?php

use yii\db\Schema;
use yii\db\Migration;

class m170809_051713_alter_production_task extends Migration {

    public function safeUp() {
        $this->addColumn('production_task', 'task_required_ids', 'VARCHAR(64) NULL DEFAULT NULL AFTER production_path_step_id');
        $this->addColumn('production_task', 'task_successor_ids', 'VARCHAR(64) NULL DEFAULT NULL AFTER task_required_ids');
        $this->addColumn('production_path', 'topological_order', 'VARCHAR(1024) NULL DEFAULT NULL');
        $this->addColumn('production_path', 'is_default', 'TINYINT(1) NOT NULL DEFAULT 0');
    }

    public function safeDown() {
        $this->dropColumn('production_path', 'is_default');
        $this->dropColumn('production_path', 'topological_order');
        $this->dropColumn('production_task', 'task_successor_ids');
        $this->dropColumn('production_task', 'task_required_ids');
    }

}
