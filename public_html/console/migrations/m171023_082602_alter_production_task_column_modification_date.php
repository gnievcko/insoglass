<?php

use yii\db\Schema;
use yii\db\Migration;
use yii\db\Query;

class m171023_082602_alter_production_task_column_modification_date extends Migration {

    public function up() {
        $this->alterColumn('production_task','date_last_modification','TIMESTAMP on update CURRENT_TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP');

        //Update wrong dates

        $latest = (new Query())
            ->select(['MAX(date_creation) as date','production_task_id'])
            ->from('production_task_history')
            ->groupBy('production_task_id')
            ->all();

        foreach($latest as $lastModification){
            Yii::$app->db->createCommand()
                ->update('production_task', ['date_last_modification'=>$lastModification['date']], ['id'=>$lastModification['production_task_id']])
                ->execute();
        }
    }

    public function down() {
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp() {
    }

    public function safeDown() {
    }
    */
}
