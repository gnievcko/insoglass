<?php

use yii\db\Schema;
use yii\db\Migration;
use common\models\ar\DocumentSection;
use common\models\ar\Language;
use common\models\ar\NumberTemplate;
use common\models\ar\NumberTemplateStorage;
use common\models\ar\DocumentType;
use common\models\ar\DocumentTypeSection;
use common\models\ar\DocumentTypeTranslation;

class m170104_101101_insert_invoice_types extends Migration {

    public function safeUp() {
    	$languageId = Language::find()->where(['symbol' => 'pl'])->one()->id;
    	$footerId = DocumentSection::find()->where(['symbol' => 'company_footer'])->one()->id;
    	$companyHeaderId = DocumentSection::find()->where(['symbol' => 'COMPANY_HEADER'])->one()->id;
    	$invoiceProductsId = DocumentSection::find()->where(['symbol' => 'INVOICE_PRODUCTS'])->one()->id;
    	$invoicePaymentId = DocumentSection::find()->where(['symbol' => 'INVOICE_PAYMENT'])->one()->id;
    	
    	$this->insert('document_type', [
    			'symbol' => 'PROFORMA_INVOICE',
    			'document_section_footer_id' => $footerId,
    			'margin_top' => 15,
    			'margin_bottom' => 50,
    			'margin_left' => 15,
    			'margin_right' => 15,
    			'is_for_order' => 1,
    	]);
    	$documentTypeId = $this->db->getLastInsertID();    	
    	$this->insert('document_type_translation', [
    			'document_type_id' => $documentTypeId,
    			'language_id' => $languageId,
    			'name' => 'Faktura pro forma',
    	]);
    	$this->insert('document_section', ['symbol' => 'PROFORMA_INVOICE_TITLE',]);
    	$sectionId = $this->db->getLastInsertID();
    	$this->insert('document_type_section', ['document_type_id' => $documentTypeId, 'document_section_id' => $companyHeaderId, 'sequence_number' => 0]);
    	$this->insert('document_type_section', ['document_type_id' => $documentTypeId, 'document_section_id' => $sectionId, 'sequence_number' => 1]);
    	$this->insert('document_type_section', ['document_type_id' => $documentTypeId, 'document_section_id' => $invoiceProductsId , 'sequence_number' => 2]);
    	$this->insert('document_type_section', ['document_type_id' => $documentTypeId, 'document_section_id' => $invoicePaymentId, 'sequence_number' => 3]);
    	
    	$this->insert('document_type', [
    			'symbol' => 'CORRECTIVE_INVOICE',
    			'document_section_footer_id' => $footerId,
    			'margin_top' => 15,
    			'margin_bottom' => 50,
    			'margin_left' => 15,
    			'margin_right' => 15,
    			'is_for_order' => 1,
    	]);
    	$documentTypeId = $this->db->getLastInsertID();
    	$this->insert('document_type_translation', [
    			'document_type_id' => $documentTypeId,
    			'language_id' => $languageId,
    			'name' => 'Faktura korygująca',
    	]);
    	$this->insert('document_section', ['symbol' => 'CORRECTIVE_INVOICE_TITLE',]);
    	$sectionId = $this->db->getLastInsertID();
    	$this->insert('document_type_section', ['document_type_id' => $documentTypeId, 'document_section_id' => $companyHeaderId, 'sequence_number' => 0]);
    	$this->insert('document_type_section', ['document_type_id' => $documentTypeId, 'document_section_id' => $sectionId, 'sequence_number' => 1]);
    	$this->insert('document_type_section', ['document_type_id' => $documentTypeId, 'document_section_id' => $invoiceProductsId , 'sequence_number' => 2]);
    	$this->insert('document_type_section', ['document_type_id' => $documentTypeId, 'document_section_id' => $invoicePaymentId, 'sequence_number' => 3]);
    	
    	$this->insert('document_type', [
    			'symbol' => 'ADVANCE_INVOICE',
    			'document_section_footer_id' => $footerId,
    			'margin_top' => 15,
    			'margin_bottom' => 50,
    			'margin_left' => 15,
    			'margin_right' => 15,
    			'is_for_order' => 1,
    	]);
    	$documentTypeId = $this->db->getLastInsertID();
    	$this->insert('document_type_translation', [
    			'document_type_id' => $documentTypeId,
    			'language_id' => $languageId,
    			'name' => 'Faktura zaliczkowa',
    	]);
    	$this->insert('document_section', ['symbol' => 'ADVANCE_INVOICE_TITLE',]);
    	$sectionId = $this->db->getLastInsertID();
    	$this->insert('document_type_section', ['document_type_id' => $documentTypeId, 'document_section_id' => $companyHeaderId, 'sequence_number' => 0]);
    	$this->insert('document_type_section', ['document_type_id' => $documentTypeId, 'document_section_id' => $sectionId, 'sequence_number' => 1]);
    	$this->insert('document_type_section', ['document_type_id' => $documentTypeId, 'document_section_id' => $invoiceProductsId , 'sequence_number' => 2]);
    	$this->insert('document_type_section', ['document_type_id' => $documentTypeId, 'document_section_id' => $invoicePaymentId, 'sequence_number' => 3]);
    	
    	$this->insert('number_template', [
				'symbol' => 'proforma_invoice',
		    	'separator' => '/',
		    	'template' => '{noInYear:inc}/PRO/{year}',
				'is_active' => 1,
		]);
		$templateId = $this->db->getLastInsertId();
		    	
		$this->insert('number_template_storage', [
				'number_template_id' => $templateId,
				'current_number' => NULL,	
		]);
    }

    public function safeDown() {
        $numberTemplateId = NumberTemplate::find()->where(['symbol' => 'proforma_invoice'])->one()->id;
        NumberTemplateStorage::deleteAll(['number_template_id' => $numberTemplateId]);
        NumberTemplate::deleteAll(['id' => $numberTemplateId]);
        
        $sectionIds = array_column(DocumentSection::find()->where(['symbol' => ['PROFORMA_INVOICE_TITLE', 'CORRECTIVE_INVOICE_TITLE', 'ADVANCE_INVOICE_TITLE']])->asArray()->all(), 'id');
        $typeIds = array_column(DocumentType::find()->where(['symbol' => ['PROFORMA_INVOICE', 'CORRECTIVE_INVOICE', 'ADVANCE_INVOICE']])->asArray()->all(), 'id');
        
        DocumentTypeSection::deleteAll(['document_type_id' => $typeIds]);
        DocumentSection::deleteAll(['id' => $sectionIds]);
        DocumentTypeTranslation::deleteAll(['document_type_id' => $typeIds]);
        DocumentType::deleteAll(['id' => $typeIds]);
    }
}
