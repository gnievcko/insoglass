<?php

use yii\db\Migration;

class m170320_121925_alter_product_unit extends Migration {

    public function safeUp() {
		$this->addColumn('offered_product_translation', 'unit', 'VARCHAR(10) NULL DEFAULT NULL AFTER price');
		$this->addColumn('product_translation', 'unit_default', 'VARCHAR(10) NULL DEFAULT NULL AFTER price_default');
		$this->addColumn('warehouse_delivery_product', 'unit', 'VARCHAR(10) NULL DEFAULT NULL AFTER currency_id');
		$this->addColumn('warehouse_product_status_history', 'unit', 'VARCHAR(10) NULL DEFAULT NULL');
    }

    public function safeDown() {
    	$this->dropColumn('warehouse_product_status_history', 'unit');
    	$this->dropColumn('warehouse_delivery_product', 'unit');
        $this->dropColumn('product_translation', 'unit_default');
        $this->dropColumn('offered_product_translation', 'unit');
    }
}
