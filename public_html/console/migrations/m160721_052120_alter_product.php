<?php

use yii\db\Schema;
use yii\db\Migration;

class m160721_052120_alter_product extends Migration {

    public function up() {
		$this->addColumn('product', 'is_available', 'TINYINT(1) NOT NULL AFTER is_visible_for_client');
		$this->addColumn('offered_product', 'is_available', 'TINYINT(1) NOT NULL AFTER is_artificial');
    }

    public function down() {
        $this->dropColumn('offered_product', 'is_available');
        $this->dropColumn('product', 'is_available');
    }

}
