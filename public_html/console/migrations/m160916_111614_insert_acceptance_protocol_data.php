<?php

use yii\db\Schema;
use yii\db\Migration;
use common\documents\DocumentType;
use common\documents\sections\SectionsMapper;
use yii\db\Query;

class m160916_111614_insert_acceptance_protocol_data extends Migration {

    public function safeUp() {
        $headerId = (new Query())->select(['id'])->from('document_section')->where(['symbol' => SectionsMapper::COMPANY_HEADER])->one()['id'];
        $footerId = (new Query())->select(['id'])->from('document_section')->where(['symbol' => SectionsMapper::COMPANY_FOOTER])->one()['id'];

    	$this->insert('document_type', ['symbol' => DocumentType::ACCEPTANCE_PROTOCOL, 'document_section_header_id' => $headerId, 'document_section_footer_id' => $footerId]);
        $documentTypeId = \Yii::$app->db->getLastInsertID();

    	$this->insert('document_section', ['symbol' => SectionsMapper::TITLE_WITH_DATE]);
        $titleTypeId = Yii::$app->db->getLastInsertID();
    	$this->insert('document_type_section', ['document_type_id' => $documentTypeId, 'document_section_id' => $titleTypeId, 'sequence_number' => 0]);

    	$this->insert('document_section', ['symbol' => SectionsMapper::ACCEPTANCE_PROTOCOL_CONTENT]);
        $protocolContentTypeId = Yii::$app->db->getLastInsertID();
    	$this->insert('document_type_section', ['document_type_id' => $documentTypeId, 'document_section_id' => $protocolContentTypeId, 'sequence_number' => 1]);
    }

    public function safeDown() {
        $documentTypeId = (new Query())->select(['id'])->from('document_type')->where(['symbol' => DocumentType::ACCEPTANCE_PROTOCOL])->one()['id'];

        $this->delete('document_type_section', ['document_type_id' => $documentTypeId]);
        $this->delete('document_section', ['symbol' => [SectionsMapper::TITLE_WITH_DATE, SectionsMapper::ACCEPTANCE_PROTOCOL_CONTENT]]);
        $this->delete('document_type', ['symbol' => DocumentType::ACCEPTANCE_PROTOCOL]);
    }
}
