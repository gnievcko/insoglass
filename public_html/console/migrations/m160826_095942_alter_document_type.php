<?php

use yii\db\Schema;
use yii\db\Migration;
use yii\db\Query;
use common\documents\sections\SectionsMapper;
use common\documents\DocumentType;

class m160826_095942_alter_document_type extends Migration {

    public function up() {
    	$this->addColumn('document_type', 'document_section_header_id', 'INT(11) NULL DEFAULT NULL');
    	$this->addColumn('document_type', 'document_section_footer_id', 'INT(11) NULL DEFAULT NULL');

    	$this->addForeignKey('fk_document_type_header', 'document_type', 'document_section_header_id', 'document_section', 'id');
    	$this->addForeignKey('fk_document_type_footer', 'document_type', 'document_section_footer_id', 'document_section', 'id');

        $headerId = (new Query())->select(['id'])->from('document_section')->where(['symbol' => SectionsMapper::COMPANY_HEADER])->one()['id'];
        $footerId = (new Query())->select(['id'])->from('document_section')->where(['symbol' => SectionsMapper::COMPANY_FOOTER])->one()['id'];

        $this->update('document_type', [
            'document_section_header_id' => $headerId,
            'document_section_footer_id' => $footerId,
        ]);

        $this->delete('document_type_section', ['document_section_id' => $headerId]);
        $this->delete('document_type_section', ['document_section_id' => $footerId]);
    }

    public function down() {
        $documentTypeId = (new Query())->select(['id'])->from('document_type')->where(['symbol' => DocumentType::INVOICE])->one()['id'];
        $headerId = (new Query())->select(['id'])->from('document_section')->where(['symbol' => SectionsMapper::COMPANY_HEADER])->one()['id'];
        $footerId = (new Query())->select(['id'])->from('document_section')->where(['symbol' => SectionsMapper::COMPANY_FOOTER])->one()['id'];

        $this->insert('document_type_section', ['document_section_id' => $headerId, 'document_type_id' => $documentTypeId, 'sequence_number' => 0]);
        $this->insert('document_type_section', ['document_section_id' => $footerId, 'document_type_id' => $documentTypeId, 'sequence_number' => 4]);

        $this->update('document_type', [
            'document_section_header_id' => null,
            'document_section_footer_id' => null,
        ]);

    	$this->dropForeignKey('fk_document_type_header', 'document_type');
    	$this->dropForeignKey('fk_document_type_footer', 'document_type');

        $this->dropColumn('document_type', 'document_section_footer_id');
        $this->dropColumn('document_type', 'document_section_header_id');
    }
}
