<?php

use yii\db\Schema;
use yii\db\Migration;

class m160704_152528_init_common_tables extends Migration {

    public function up() {
    	$tableOptions = null;
    	if($this->db->driverName === 'mysql') {
    		$tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
    	}
    	
    	$this->createTable('banner', [
    			'id' => $this->primaryKey(11),
    			'symbol' => $this->string(32)->notNull()->unique(),
    	], $tableOptions);
    	
    	$this->createTable('banner_translation', [
    			'banner_id' => $this->integer(11),
    			'language_id' => $this->integer(11),
    			'url_photo' => $this->string(256)->notNull(),
    			'description' => $this->string(512),
    	], $tableOptions);
    	 
    	$this->addPrimaryKey('pk_banner_translation', 'banner_translation', ['banner_id', 'language_id']);
    	$this->addForeignKey('fk_banner_translation_language', 'banner_translation', 'language_id', 'language', 'id');
    	$this->addForeignKey('fk_banner_translation_banner', 'banner_translation', 'banner_id', 'banner', 'id');
    	
    	$this->createTable('slider', [
    			'id' => $this->primaryKey(11),
    			'name' => $this->string(50)->notNull()->unique(),
    	], $tableOptions);
    	
    	$this->createTable('slider_banner', [
    			'slider_id' => $this->integer(11),
    			'banner_id' => $this->integer(11),
    			'priority' => $this->integer(3)->notNull(),
    	], $tableOptions);
    	 
    	$this->addPrimaryKey('pk_slider_banner', 'slider_banner', ['slider_id', 'banner_id']);
    	$this->addForeignKey('fk_slider_banner_slider', 'slider_banner', 'slider_id', 'slider', 'id');
    	$this->addForeignKey('fk_slider_banner_banner', 'slider_banner', 'banner_id', 'banner', 'id');
    	
    	$this->createTable('information_page', [
    			'id' => $this->primaryKey(11),
    			'symbol' => $this->string(32)->notNull()->unique(),
    			'priority' => $this->integer(10),
    			'is_active' => $this->boolean()->notNull()->defaultValue(1),
    			'date_creation' => $this->timestamp()->notNull()
    	], $tableOptions);
    	
    	$this->alterColumn('information_page', 'date_creation', 'TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP');
    	
    	$this->createTable('information_page_translation', [
    			'information_page_id' => $this->integer(11),
    			'language_id' => $this->integer(11),
    			'title' => $this->string(255)->notNull(),
    			'content' => $this->text(),
    			'keyword' => $this->string(255),
    			'description' => $this->string(255),
    			'is_active' => $this->boolean()->notNull()->defaultValue(1)
    	], $tableOptions);
    	
    	$this->addPrimaryKey('pk_information_page_translation', 'information_page_translation', ['information_page_id', 'language_id']);
    	$this->addForeignKey('fk_information_page_translation_page', 'information_page_translation', 'information_page_id', 'information_page', 'id');
    	$this->addForeignKey('fk_information_page_translation_language', 'information_page_translation', 'language_id', 'language', 'id');
    }

    public function down() {
		// raczej nie potrzebna        
    }

}
