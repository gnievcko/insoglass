<?php

use yii\db\Schema;
use yii\db\Migration;

class m171031_072929_create_order_visualisation_photo extends Migration {

    public function up() {
        $tableOptions = null;
        if($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('order_visualisation_photo', [
            'id' => $this->primaryKey(11),
            'order_offered_product_id' => $this->integer(11),
            'url_photo' => $this->string(256)->notNull(),
            'height' => $this->string(5)->notNull(),
            'width' => $this->string(5)->notNull(),
            'top' => $this->string(5)->notNull(),
            'left' => $this->string(5)->notNull(),
            'date_creation' => $this->timestamp()->notNull(),
                ], $tableOptions);

        $this->addForeignKey('fk_order_visualisation_photo_order_product', 'order_visualisation_photo', 'order_offered_product_id', 'order_offered_product', 'id');
        $this->alterColumn('order_visualisation_photo', 'date_creation', 'TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP');
    }

    public function down() {
        echo "m171031_072929_create_order_visualisation_photo cannot be reverted.\n";

        return false;
    }

    /*
      // Use safeUp/safeDown to run migration code within a transaction
      public function safeUp() {
      }

      public function safeDown() {
      }
     */
}
