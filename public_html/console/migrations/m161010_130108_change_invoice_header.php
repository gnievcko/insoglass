<?php

use yii\db\Migration;
use yii\db\Query;

class m161010_130108_change_invoice_header extends Migration {

    public function safeUp() {
        $this->update('document_type', ['document_section_header_id' => null, 'margin_top' => 15], ['symbol' => 'INVOICE']);
        
        $id = (new Query())->select(['dt.id'])->from('document_type dt')->where(['symbol' => 'INVOICE'])->scalar();
        $this->insert('document_type_section', [
        		'document_type_id' => $id,
        		'document_section_id' => common\models\ar\DocumentSection::findOne(['symbol' => 'COMPANY_HEADER'])->id,
        		'sequence_number' => 0,
        ]);
    }

    public function safeDown() {
        
    }

}
