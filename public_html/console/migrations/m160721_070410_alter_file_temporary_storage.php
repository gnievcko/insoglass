<?php

use yii\db\Schema;
use yii\db\Migration;

class m160721_070410_alter_file_temporary_storage extends Migration {

    public function up() {
        $this->addColumn('file_temporary_storage', 'original_file_name', $this->string(255)->notNull());
    }

    public function down() {
        $this->dropColumn('file_temporary_storage', 'original_file_name');
    }
}
