<?php

use yii\db\Schema;
use yii\db\Migration;
use common\models\ar\Language;

class m161020_065851_alter_contract_type_translation extends Migration {

    public function safeUp() {
    	$this->addColumn('contract_type', 'is_active', 'TINYINT(1) NOT NULL');
    	$this->update('contract_type', ['is_active' => 1]);
    	$this->insert('contract_type', ['symbol' => 'malfunction_request', 'is_active' => 0]);
    	
    	$this->update('contract_type_translation', ['contract_type_id' => $this->db->getLastInsertID()], ['name' => 'Usuniecie awarii']);
    	$this->update('contract_type_translation', ['name' => 'Usunięcie awarii'], ['name' => 'Usuniecie awarii']);
      
        $this->dropForeignKey('fk_contract_type_translation_contract_type', 'contract_type_translation');
        $this->dropIndex('fk_contract_type_translation_contract_type', 'contract_type_translation');
        $this->dropForeignKey('fk_contract_type_translation_language', 'contract_type_translation');
        $this->dropIndex('fk_contract_type_translation_language', 'contract_type_translation');
    	
        $this->addPrimaryKey('pk_contract_type_translation', 'contract_type_translation', ['contract_type_id', 'language_id']);
    	
        $this->addForeignKey('fk_contract_type_translation_contract_type', 'contract_type_translation', 'contract_type_id', 'contract_type', 'id');
        $this->addForeignKey('fk_contract_type_translation_language', 'contract_type_translation', 'language_id', 'language', 'id');
        
    	$languageId = Language::find()->where(['symbol' => 'pl'])->one()->id;
    	
    	$this->delete('order_type_translation');
        
        $this->dropForeignKey('fk_order_type_translation_language', 'order_type_translation');
        $this->dropIndex('fk_order_type_translation_language', 'order_type_translation');
        $this->dropForeignKey('fk_order_type_translation_order_type', 'order_type_translation');
        $this->dropIndex('fk_order_type_translation_order_type', 'order_type_translation');
    	
        $this->addPrimaryKey('pk_order_type_translation', 'order_type_translation', ['order_type_id', 'language_id']);
    	
        $this->addForeignKey('fk_order_type_translation_language', 'order_type_translation', 'language_id', 'language', 'id');
        $this->addForeignKey('fk_order_type_translation_order_type', 'order_type_translation', 'order_type_id', 'order_type', 'id');
        
        $this->alterColumn('order_type_translation', 'name', 'VARCHAR(128) NOT NULL');
    	$this->delete('order_type');
    	
    	$this->insert('order_type', ['symbol' => 'malfunction_fixing']);
    	$this->insert('order_type_translation', ['language_id' => $languageId, 'order_type_id' => $this->db->getLastInsertID(), 
    			'name' => 'Usunięcie awarii',
    	]);
    	
    	$this->insert('order_type', ['symbol' => 'fire_gate_repair']);
    	$this->insert('order_type_translation', ['language_id' => $languageId, 'order_type_id' => $this->db->getLastInsertID(),
    			'name' => 'Naprawa bramy ppoż.',
    	]);
    	
    	$this->insert('order_type', ['symbol' => 'item_delivery']);
    	$this->insert('order_type_translation', ['language_id' => $languageId, 'order_type_id' => $this->db->getLastInsertID(),
    			'name' => 'Dostawa elementów.',
    	]);
    	
    	$this->insert('order_type', ['symbol' => 'item_replace']);
    	$this->insert('order_type_translation', ['language_id' => $languageId, 'order_type_id' => $this->db->getLastInsertID(),
    			'name' => 'Wymiana elementów systemu oddymiania; naprawa grodzi ppoż.',
    	]);
    	
    	$this->insert('order_type', ['symbol' => 'service']);
    	$this->insert('order_type_translation', ['language_id' => $languageId, 'order_type_id' => $this->db->getLastInsertID(),
    			'name' => 'Przegląd serwisowy systemu oddymiania i grodzi ppoż.',
    	]);
    }

    public function safeDown() {
        // tutaj nie bedzie downa, bo ta migracja poprawia bledna tabelke
    }
}
