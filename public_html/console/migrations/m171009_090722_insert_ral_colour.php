<?php

use yii\db\Schema;
use yii\db\Migration;
use common\models\ar\Language;

class m171009_090722_insert_ral_colour extends Migration {
   
    public function safeUp() {
        $plId = Language::findOne(['symbol' => 'pl'])->id;
        $en = Language::findOne(['symbol' => 'en']);
        
        if(empty($en)) {
            $en = new Language();
            $en->symbol = 'en';
            $en->name = 'English';
            $en->is_active = 1;
            $en->save;
        }
        $enId = $en->id;
        
        $groups = [
            'yellow' => ['position' => 1000,'rgb' => '#d7b327', 'en' => 'Yellow hues', 'pl' => 'Odcienie żółtego'],
            'orange' => ['position' => 2000,'rgb' => '#f38f28', 'en' => 'Orange hues', 'pl' => 'Odcienie pomarańczowego'],
            'red' => ['position' => 3000,'rgb' => '#d64818', 'en' => 'Red hues', 'pl' => 'Odcienie czerwonego'],
            'violet' => ['position' => 4000,'rgb' => '#8b1892', 'en' => 'Violet hues', 'pl' => 'Odcienie fioletowego'],
            'blue' => ['position' => 5000,'rgb' => '#0d2791', 'en' => 'Blue hues', 'pl' => 'Odcienie niebieskiego'],
            'green' => ['position' => 6000,'rgb' => '#1e7f16', 'en' => 'Green hues', 'pl' => 'Odcienie zielonego'],
            'grey' => ['position' => 7000,'rgb' => '#979797', 'en' => 'Grey hues', 'pl' => 'Odcienie szarego'],
            'brown' => ['position' => 8000,'rgb' => '#6c4a11', 'en' => 'Brown hues', 'pl' => 'Odcienie brązowego'],
            'white_black' => ['position' => 9000,'rgb' => '#000000', 'en' => 'White & black hues', 'pl' => 'Odcienie białego i czarnego'],
        ];
        
        $colours = [
            'yellow' => [
                ['symbol' => 'RAL 1000', 'rgb' => '#cccc99', 'isReversed' => 1],
                ['symbol' => 'RAL 1001', 'rgb' => '#d2aa5a', 'isReversed' => 1],
                ['symbol' => 'RAL 1002', 'rgb' => '#d0a818', 'isReversed' => 1],
                ['symbol' => 'RAL 1003', 'rgb' => '#ffcc00', 'isReversed' => 1],
                ['symbol' => 'RAL 1004', 'rgb' => '#e0b000', 'isReversed' => 1],
                ['symbol' => 'RAL 1005', 'rgb' => '#d1a40f', 'isReversed' => 1],
                ['symbol' => 'RAL 1006', 'rgb' => '#e3a529', 'isReversed' => 1],
                ['symbol' => 'RAL 1007', 'rgb' => '#dd9f23', 'isReversed' => 1],
                ['symbol' => 'RAL 1011', 'rgb' => '#ac7e24', 'isReversed' => 1],
                ['symbol' => 'RAL 1012', 'rgb' => '#d8b400', 'isReversed' => 1],
                ['symbol' => 'RAL 1013', 'rgb' => '#eae9c2', 'isReversed' => 1],
                ['symbol' => 'RAL 1014', 'rgb' => '#d3c378', 'isReversed' => 1],
                ['symbol' => 'RAL 1015', 'rgb' => '#f5e1aa', 'isReversed' => 1],
                ['symbol' => 'RAL 1016', 'rgb' => '#ffff00', 'isReversed' => 1],
                ['symbol' => 'RAL 1017', 'rgb' => '#f4c431', 'isReversed' => 1],
                ['symbol' => 'RAL 1018', 'rgb' => '#fbe22b', 'isReversed' => 1],
                ['symbol' => 'RAL 1019', 'rgb' => '#a68a3c', 'isReversed' => 1],
                ['symbol' => 'RAL 1021', 'rgb' => '#ffcd06', 'isReversed' => 1],
                ['symbol' => 'RAL 1023', 'rgb' => '#eec61b', 'isReversed' => 1],
                ['symbol' => 'RAL 1024', 'rgb' => '#b29504', 'isReversed' => 1],
                ['symbol' => 'RAL 1028', 'rgb' => '#fd9e00', 'isReversed' => 1],
                ['symbol' => 'RAL 1032', 'rgb' => '#d0a300', 'isReversed' => 1],
                ['symbol' => 'RAL 1033', 'rgb' => '#ff9a35', 'isReversed' => 1],
                ['symbol' => 'RAL 1020', 'rgb' => '#a0956d', 'isReversed' => 1],
                ['symbol' => 'RAL 1026', 'rgb' => '#effb00', 'isReversed' => 1],
                ['symbol' => 'RAL 1027', 'rgb' => '#a68631', 'isReversed' => 1],
                ['symbol' => 'RAL 1034', 'rgb' => '#e7a35f', 'isReversed' => 1],
                ['symbol' => 'RAL 1035', 'rgb' => '#88826e', 'isReversed' => 1],
                ['symbol' => 'RAL 1036', 'rgb' => '#867045'],
                ['symbol' => 'RAL 1037', 'rgb' => '#ea9829', 'isReversed' => 1],
            ],
            
            'orange' => [
                ['symbol' => 'RAL 2000', 'rgb' => '#e29600', 'isReversed' => 1],
                ['symbol' => 'RAL 2001', 'rgb' => '#a21d00'],
                ['symbol' => 'RAL 2002', 'rgb' => '#d21500'],
                ['symbol' => 'RAL 2004', 'rgb' => '#d47a04', 'isReversed' => 1],
                ['symbol' => 'RAL 2009', 'rgb' => '#d95c30'],
                ['symbol' => 'RAL 2003', 'rgb' => '#f07f41'],
                ['symbol' => 'RAL 2005', 'rgb' => '#ff1300'],
                ['symbol' => 'RAL 2007', 'rgb' => '#ff9500', 'isReversed' => 1],
                ['symbol' => 'RAL 2008', 'rgb' => '#d68212', 'isReversed' => 1],
                ['symbol' => 'RAL 2010', 'rgb' => '#cd643c'],
                ['symbol' => 'RAL 2011', 'rgb' => '#df7736', 'isReversed' => 1],
                ['symbol' => 'RAL 2012', 'rgb' => '#d26a56', 'isReversed' => 1],
                ['symbol' => 'RAL 2013', 'rgb' => '#964726'],
            ],
            
            'red' => [
                ['symbol' => 'RAL 3000', 'rgb' => '#b00900'],
                ['symbol' => 'RAL 3001', 'rgb' => '#8e0700'],
                ['symbol' => 'RAL 3002', 'rgb' => '#970700'],
                ['symbol' => 'RAL 3003', 'rgb' => '#8e0700'],
                ['symbol' => 'RAL 3004', 'rgb' => '#771f0b'],
                ['symbol' => 'RAL 3005', 'rgb' => '#611200'],
                ['symbol' => 'RAL 3007', 'rgb' => '#e29600'],
                ['symbol' => 'RAL 3009', 'rgb' => '#641706'],
                ['symbol' => 'RAL 3011', 'rgb' => '#993333'],
                ['symbol' => 'RAL 3012', 'rgb' => '#bd7157'],
                ['symbol' => 'RAL 3013', 'rgb' => '#8c0700'],
                ['symbol' => 'RAL 3014', 'rgb' => '#d37378', 'isReversed' => 1],
                ['symbol' => 'RAL 3015', 'rgb' => '#e0a5ac', 'isReversed' => 1],
                ['symbol' => 'RAL 3016', 'rgb' => '#9d0c00'],
                ['symbol' => 'RAL 3020', 'rgb' => '#d20b00'],
                ['symbol' => 'RAL 3022', 'rgb' => '#d5462d'],
                ['symbol' => 'RAL 3027', 'rgb' => '#bf0000'],
                ['symbol' => 'RAL 3031', 'rgb' => '#9e110e'],
                ['symbol' => 'RAL 3017', 'rgb' => '#ca5d66'],
                ['symbol' => 'RAL 3018', 'rgb' => '#c64e59'],
                ['symbol' => 'RAL 3024', 'rgb' => '#f70000'],
                ['symbol' => 'RAL 3026', 'rgb' => '#ff0000'],             
                ['symbol' => 'RAL 3028', 'rgb' => '#cb3234'],
                ['symbol' => 'RAL 3032', 'rgb' => '#791925'],
                ['symbol' => 'RAL 3033', 'rgb' => '#b0423b'], 
            ],
            
            'violet' => [
                ['symbol' => 'RAL 4002', 'rgb' => '#7c2527'],
                ['symbol' => 'RAL 4003', 'rgb' => '#d23e58'],
                ['symbol' => 'RAL 4004', 'rgb' => '#710b19'],
                ['symbol' => 'RAL 4005', 'rgb' => '#75406d'],
                ['symbol' => 'RAL 4001', 'rgb' => '#886a8b'],
                ['symbol' => 'RAL 4006', 'rgb' => '#96467e'],
                ['symbol' => 'RAL 4007', 'rgb' => '#574050'],
                ['symbol' => 'RAL 4008', 'rgb' => '#8c568a'],
                ['symbol' => 'RAL 4009', 'rgb' => '#a18d99'],
                ['symbol' => 'RAL 4010', 'rgb' => '#be4e80'],
                ['symbol' => 'RAL 4011', 'rgb' => '#836695'],
                ['symbol' => 'RAL 4012', 'rgb' => '#706a82'],   
            ],
            
            'blue' => [
                ['symbol' => 'RAL 5000', 'rgb' => '#374b6f'],
                ['symbol' => 'RAL 5001', 'rgb' => '#1e4663'],
                ['symbol' => 'RAL 5002', 'rgb' => '#2a2b7b'],
                ['symbol' => 'RAL 5003', 'rgb' => '#293655'],
                ['symbol' => 'RAL 5004', 'rgb' => '#1c1e29'],
                ['symbol' => 'RAL 5005', 'rgb' => '#144788'],
                ['symbol' => 'RAL 5007', 'rgb' => '#40668c'],
                ['symbol' => 'RAL 5008', 'rgb' => '#303b47'],
                ['symbol' => 'RAL 5009', 'rgb' => '#293655'],
                ['symbol' => 'RAL 5010', 'rgb' => '#1c1e29'],
                ['symbol' => 'RAL 5011', 'rgb' => '#0f1114'],
                ['symbol' => 'RAL 5012', 'rgb' => '#4a6e8c'],
                ['symbol' => 'RAL 5013', 'rgb' => '#222c52'],
                ['symbol' => 'RAL 5014', 'rgb' => '#686e7a'],
                ['symbol' => 'RAL 5015', 'rgb' => '#2851ae'],
                ['symbol' => 'RAL 5017', 'rgb' => '#003e91'],
                ['symbol' => 'RAL 5018', 'rgb' => '#007b6c'],
                ['symbol' => 'RAL 5019', 'rgb' => '#24439f'],
                ['symbol' => 'RAL 5020', 'rgb' => '#003a3c'],
                ['symbol' => 'RAL 5021', 'rgb' => '#006661'],
                ['symbol' => 'RAL 5022', 'rgb' => '#00004a'],
                ['symbol' => 'RAL 5023', 'rgb' => '#516179'],
                ['symbol' => 'RAL 5024', 'rgb' => '#5e718e'],
                ['symbol' => 'RAL 5025', 'rgb' => '#265a6e'],
                ['symbol' => 'RAL 5026', 'rgb' => '#192652'],   
            ],
            
            'green' => [
                ['symbol' => 'RAL 6000', 'rgb' => '#317561'],
                ['symbol' => 'RAL 6001', 'rgb' => '#216f2e'],
                ['symbol' => 'RAL 6002', 'rgb' => '#1d5f28'],
                ['symbol' => 'RAL 6003', 'rgb' => '#4a563d'],
                ['symbol' => 'RAL 6004', 'rgb' => '#0d4142'],
                ['symbol' => 'RAL 6005', 'rgb' => '#0e4235'],
                ['symbol' => 'RAL 6006', 'rgb' => '#3f423a'],
                ['symbol' => 'RAL 6007', 'rgb' => '#273323'],
                ['symbol' => 'RAL 6009', 'rgb' => '#25382e'],
                ['symbol' => 'RAL 6010', 'rgb' => '#3d743a'],
                ['symbol' => 'RAL 6011', 'rgb' => '#71825e'],
                ['symbol' => 'RAL 6012', 'rgb' => '#303f3c'],
                ['symbol' => 'RAL 6013', 'rgb' => '#787b59'],
                ['symbol' => 'RAL 6014', 'rgb' => '#434236'],
                ['symbol' => 'RAL 6015', 'rgb' => '#3c3f39'],
                ['symbol' => 'RAL 6016', 'rgb' => '#016951'],
                ['symbol' => 'RAL 6017', 'rgb' => '#458540'],
                ['symbol' => 'RAL 6018', 'rgb' => '#47a33e'],
                ['symbol' => 'RAL 6019', 'rgb' => '#acd0aa', 'isReversed' => 1],
                ['symbol' => 'RAL 6020', 'rgb' => '#344632'],
                ['symbol' => 'RAL 6021', 'rgb' => '#718154'],
                ['symbol' => 'RAL 6022', 'rgb' => '#143400'],
                ['symbol' => 'RAL 6024', 'rgb' => '#297d2f'],
                ['symbol' => 'RAL 6025', 'rgb' => '#385924'],
                ['symbol' => 'RAL 6026', 'rgb' => '#005c51'],
                ['symbol' => 'RAL 6027', 'rgb' => '#5b9b94', 'isReversed' => 1],
                ['symbol' => 'RAL 6028', 'rgb' => '#2c5445'],
                ['symbol' => 'RAL 6029', 'rgb' => '#007142'],
                ['symbol' => 'RAL 6033', 'rgb' => '#5c9a85', 'isReversed' => 1],
                ['symbol' => 'RAL 6034', 'rgb' => '#82b2ac', 'isReversed' => 1],
                ['symbol' => 'RAL 6008', 'rgb' => '#454640'],
                ['symbol' => 'RAL 6032', 'rgb' => '#31865c'],
                ['symbol' => 'RAL 6035', 'rgb' => '#0a4e29'],
                ['symbol' => 'RAL 6036', 'rgb' => '#085c4e'],
                ['symbol' => 'RAL 6037', 'rgb' => '#008f39'],
                ['symbol' => 'RAL 6038', 'rgb' => '#00bb2d'],
            ],
            
            'grey' => [
                ['symbol' => 'RAL 7000', 'rgb' => '#777874'],
                ['symbol' => 'RAL 7001', 'rgb' => '#8a9597', 'isReversed' => 1],
                ['symbol' => 'RAL 7002', 'rgb' => '#807e67'],
                ['symbol' => 'RAL 7003', 'rgb' => '#797a6c'],
                ['symbol' => 'RAL 7004', 'rgb' => '#969992', 'isReversed' => 1],
                ['symbol' => 'RAL 7005', 'rgb' => '#6a706e'],
                ['symbol' => 'RAL 7006', 'rgb' => '#746e60'],
                ['symbol' => 'RAL 7009', 'rgb' => '#5a6158'],
                ['symbol' => 'RAL 7010', 'rgb' => '#565c56'],
                ['symbol' => 'RAL 7011', 'rgb' => '#545c60'],
                ['symbol' => 'RAL 7012', 'rgb' => '#586062'],
                ['symbol' => 'RAL 7013', 'rgb' => '#545447'],
                ['symbol' => 'RAL 7015', 'rgb' => '#50555b'],
                ['symbol' => 'RAL 7016', 'rgb' => '#363e42'],
                ['symbol' => 'RAL 7021', 'rgb' => '#161516'],
                ['symbol' => 'RAL 7022', 'rgb' => '#433d35'],
                ['symbol' => 'RAL 7023', 'rgb' => '#808378', 'isReversed' => 1],
                ['symbol' => 'RAL 7024', 'rgb' => '#46494f'],
                ['symbol' => 'RAL 7026', 'rgb' => '#364346'],
                ['symbol' => 'RAL 7030', 'rgb' => '#929287', 'isReversed' => 1],
                ['symbol' => 'RAL 7031', 'rgb' => '#5c686f'],
                ['symbol' => 'RAL 7032', 'rgb' => '#b8b8a7', 'isReversed' => 1],
                ['symbol' => 'RAL 7033', 'rgb' => '#535c4e'],
                ['symbol' => 'RAL 7034', 'rgb' => '#929075', 'isReversed' => 1],
                ['symbol' => 'RAL 7035', 'rgb' => '#d9d9cf', 'isReversed' => 1],
                ['symbol' => 'RAL 7036', 'rgb' => '#999596', 'isReversed' => 1],
                ['symbol' => 'RAL 7037', 'rgb' => '#7b7e7d'],
                ['symbol' => 'RAL 7038', 'rgb' => '#b3b7af', 'isReversed' => 1],
                ['symbol' => 'RAL 7039', 'rgb' => '#6a685e'],
                ['symbol' => 'RAL 7040', 'rgb' => '#9ca2a5', 'isReversed' => 1],  
                ['symbol' => 'RAL 7042', 'rgb' => '#8e9698', 'isReversed' => 1],
                ['symbol' => 'RAL 7043', 'rgb' => '#3f4444'],
                ['symbol' => 'RAL 7044', 'rgb' => '#bcbcb1', 'isReversed' => 1],
                ['symbol' => 'RAL 7046', 'rgb' => '#77828c'],
                ['symbol' => 'RAL 7008', 'rgb' => '#786a4e'],
                ['symbol' => 'RAL 7045', 'rgb' => '#93989c', 'isReversed' => 1],
                ['symbol' => 'RAL 7047', 'rgb' => '#c9cbcb', 'isReversed' => 1],
                ['symbol' => 'RAL 7048', 'rgb' => '#858274', 'isReversed' => 1],
            ],
            
            'brown' => [
                ['symbol' => 'RAL 8000', 'rgb' => '#816927'],
                ['symbol' => 'RAL 8001', 'rgb' => '#926b00'],
                ['symbol' => 'RAL 8002', 'rgb' => '#65270a'],
                ['symbol' => 'RAL 8003', 'rgb' => '#7f532e'],
                ['symbol' => 'RAL 8004', 'rgb' => '#8c290d'],
                ['symbol' => 'RAL 8007', 'rgb' => '#562707'],
                ['symbol' => 'RAL 8008', 'rgb' => '#6e4e27'],
                ['symbol' => 'RAL 8011', 'rgb' => '#3d1c05'],
                ['symbol' => 'RAL 8012', 'rgb' => '#663730'],
                ['symbol' => 'RAL 8014', 'rgb' => '#48382c'],
                ['symbol' => 'RAL 8015', 'rgb' => '#623933'],
                ['symbol' => 'RAL 8016', 'rgb' => '#4b2e25'],
                ['symbol' => 'RAL 8017', 'rgb' => '#330f04'],
                ['symbol' => 'RAL 8019', 'rgb' => '#231f1d'],
                ['symbol' => 'RAL 8022', 'rgb' => '#201e1f'],
                ['symbol' => 'RAL 8023', 'rgb' => '#a55d2e'],
                ['symbol' => 'RAL 8024', 'rgb' => '#78543b'],
                ['symbol' => 'RAL 8025', 'rgb' => '#745b48'],
                ['symbol' => 'RAL 8028', 'rgb' => '#4d3a2a'],
                ['symbol' => 'RAL 8029', 'rgb' => '#7e3f27'],  
            ],
            
            'white_black' => [
                ['symbol' => 'RAL 9001', 'rgb' => '#eeeadb', 'isReversed' => 1],
                ['symbol' => 'RAL 9002', 'rgb' => '#dcddd3', 'isReversed' => 1],
                ['symbol' => 'RAL 9003', 'rgb' => '#f3f7f3', 'isReversed' => 1],
                ['symbol' => 'RAL 9005', 'rgb' => '#09090c'],
                ['symbol' => 'RAL 9010', 'rgb' => '#f6f8ee', 'isReversed' => 1],
                ['symbol' => 'RAL 9011', 'rgb' => '#282b2e'],
                ['symbol' => 'RAL 9016', 'rgb' => '#f6faf4', 'isReversed' => 1],
                ['symbol' => 'RAL 9018', 'rgb' => '#ced2cc', 'isReversed' => 1],
                ['symbol' => 'RAL 9004', 'rgb' => '#414446'],
                ['symbol' => 'RAL 9006', 'rgb' => '#a2a6a4', 'isReversed' => 1],
                ['symbol' => 'RAL 9007', 'rgb' => '#8e8e8b', 'isReversed' => 1],
                ['symbol' => 'RAL 9017', 'rgb' => '#3f403f'],
                ['symbol' => 'RAL 9022', 'rgb' => '#9d9f9f', 'isReversed' => 1],
                ['symbol' => 'RAL 9023', 'rgb' => '#87898b', 'isReversed' => 1],
            ],
            
        ];

        foreach($groups as $symbol => &$group) {
            $this->insert('ral_colour_group', [
                'symbol' => $symbol,
                'index' => $group['position'],
                'rgb_hash' => $group['rgb'],
            ]);
            
            $group['dbId'] = Yii::$app->db->getLastInsertID();
            
            $this->insert('ral_colour_group_translation', [
                'ral_colour_group_id' => $group['dbId'],
                'language_id' => $enId,
                'name' => $group['en'],
            ]);
            
            $this->insert('ral_colour_group_translation', [
                'ral_colour_group_id' => $group['dbId'],
                'language_id' => $plId,
                'name' => $group['pl'],
            ]);
            
            foreach($colours[$symbol] as $colour) {
                $this->insert('ral_colour', [
                    'ral_colour_group_id' => $group['dbId'],
                    'symbol' => $colour['symbol'],
                    'rgb_hash'=> $colour['rgb'],
                    'is_colour_reversed' => !empty($colour['isReversed']) ? $colour['isReversed'] : 0,
                ]);
            }
        }
    }

    public function safeDown() {
        $this->delete('ral_colour');
        $this->delete('ral_colour_group_translation');
        $this->delete('ral_colour_group');
        
    }
}
