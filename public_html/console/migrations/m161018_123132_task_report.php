<?php

use yii\db\Schema;
use yii\db\Migration;
use common\documents\sections\SectionsMapper;
use common\models\ar\DocumentSection;
use common\models\ar\DocumentType;
use common\models\ar\DocumentTypeSection;
use common\helpers\Utility;
use common\models\ar\Language;

class m161018_123132_task_report extends Migration {

    public function up() {
    	$headerSectionId = DocumentSection::find()->where(['symbol' => 'COMPANY_SHORT_HEADER'])->one()->id;
    	$footerSectionId = DocumentSection::find()->where(['symbol' => 'COMPANY_FOOTER'])->one()->id;
    	 
    	$this->insert('document_section', ['symbol' => SectionsMapper::TASK_REPORT]);
    	$documentSectionId = $this->db->getLastInsertID();
    	 
    	$this->insert('document_type', [
    			'symbol' => SectionsMapper::TASK_REPORT,
    			'document_section_header_id' => $headerSectionId,
    			'document_section_footer_id' => $footerSectionId,
    			'margin_top' => 50,
    			'margin_bottom' => 50,
    			'margin_left' => 15,
    			'margin_right' => 15,
    			'format' => 'A4',
    			'orientation' => 'P',
    	]);
    	 
    	$documentTypeId = $this->db->getLastInsertId();
    	$this->insert('document_type_section', [
    			'document_type_id' => $documentTypeId,
    			'document_section_id' => $documentSectionId,
    			'sequence_number' => 1,
    	]);
    }

    public function down() {
        $documentTypeId = DocumentType::find()->where(['symbol' => SectionsMapper::TASK_REPORT])->one()->id;
    	
    	DocumentTypeSection::deleteAll(['document_type_id' => [$documentTypeId]]);
    	DocumentType::deleteAll(['id' => [$documentTypeId]]);
    	DocumentSection::deleteAll(['symbol' => [SectionsMapper::TASK_REPORT]]);  	
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp() {
    }

    public function safeDown() {
    }
    */
}
