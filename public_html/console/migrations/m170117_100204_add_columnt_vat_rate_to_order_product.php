<?php

use yii\db\Schema;
use yii\db\Migration;

class m170117_100204_add_columnt_vat_rate_to_order_product extends Migration {

    public function up() {
        $this->addColumn('order_offered_product', 'vat_rate', $this->string(10));
    }

    public function down() {
        $this->dropColumn('order_offered_product', 'vat_rate');
    }

    /*
      // Use safeUp/safeDown to run migration code within a transaction
      public function safeUp() {
      }

      public function safeDown() {
      }
     */
}
