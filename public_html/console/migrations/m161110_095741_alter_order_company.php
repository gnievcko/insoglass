<?php

use yii\db\Schema;
use yii\db\Migration;

class m161110_095741_alter_order_company extends Migration {

    public function up() {
        $this->dropForeignKey('fk_company_contract_type', 'company');
		$this->dropColumn('company', 'contract_type_id');
        
        $this->addColumn('order', 'contract_type_id', 'INT(11) NULL');
        $this->addForeignKey('fk_order_contract_type', 'order', 'contract_type_id', 'contract_type', 'id');
    }

    public function down() {
    }
}
