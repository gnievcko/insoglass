<?php

use yii\db\Schema;
use yii\db\Migration;

class m170117_122817_add_column_unit_to_order_product extends Migration {

    public function up() {
        $this->addColumn('order_offered_product', 'unit', $this->string(10));
    }

    public function down() {
        $this->dropColumn('order_offered_product', 'unit');
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp() {
    }

    public function safeDown() {
    }
    */
}
