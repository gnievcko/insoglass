<?php

use yii\db\Schema;
use yii\db\Migration;

class m160701_153832_init_basic_tables extends Migration {

    public function up() {
    	$tableOptions = null;
    	if($this->db->driverName === 'mysql') {
    		$tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
    	}
    	
    	$this->createLanguage($tableOptions);
    	$this->createCurrency($tableOptions);
    	$this->createRole($tableOptions);
    	$this->createRoleTranslation($tableOptions);
    	$this->createEmailConfig($tableOptions);
    	$this->createEmailType($tableOptions);
    	$this->createEmailTemplate($tableOptions);
    	$this->createCountry($tableOptions);
    	$this->createCountryTranslation($tableOptions);
    	$this->createProvince($tableOptions);
    	$this->createCity($tableOptions);
    	$this->createAddress($tableOptions);
    	$this->createUser($tableOptions);
    	$this->createUserRole($tableOptions);
    }

	public function down() {
		$this->dropTable('user_role');
    	$this->dropTable('user');
    	$this->dropTable('address');
    	$this->dropTable('city');
    	$this->dropTable('province');
    	$this->dropTable('country_translation');
    	$this->dropTable('country');
    	$this->dropTable('email_template');
    	$this->dropTable('email_type');
    	$this->dropTable('email_config');
    	$this->dropTable('role_translation');
    	$this->dropTable('role');
    	$this->dropTable('currency');
    	$this->dropTable('language');
    }

    private function createLanguage($tableOptions) {
    	$this->createTable('language', [
    			'id' => $this->primaryKey(11),
    			'symbol' => $this->string(5)->notNull()->unique(),
    			'name' => $this->string(32)->notNull(),
    			'is_active' => $this->boolean()->notNull()
    	], $tableOptions);
    }
    
    private function createCurrency($tableOptions) {
    	$this->createTable('currency', [
    			'id' => $this->primaryKey(11),
    			'symbol' => $this->string(10)->notNull()->unique(),
    			'short_symbol' => $this->string(10)->unique(),
    			'conversion_value' => $this->decimal(8,4)->notNull()
    	], $tableOptions);
    }
    
    private function createRole($tableOptions) {
    	$this->createTable('role', [
    			'id' => $this->primaryKey(11),
    			'symbol' => $this->string(32)->notNull()->unique()
    	], $tableOptions);
    }
    
    private function createRoleTranslation($tableOptions) {
    	$this->createTable('role_translation', [
    			'role_id' => $this->integer(11),
    			'language_id' => $this->integer(11),
    			'name' => $this->string(128)->notNull(),
    	], $tableOptions);
    
    	$this->addPrimaryKey('pk_role_translation', 'role_translation', array('role_id', 'language_id'));
    	$this->addForeignKey('fk_role_translation_language', 'role_translation', 'language_id', 'language', 'id');
    	$this->addForeignKey('fk_role_translation_role', 'role_translation', 'role_id', 'role', 'id');
    }
    
    private function createEmailConfig($tableOptions) {
    	$this->createTable('email_config', [
    			'id' => $this->primaryKey(11),
    			'email' => $this->string(64)->notNull(),
    			'password' => $this->string(32)->notNull(),
    			'port' => $this->integer(5)->notNull(),
    			'protocol' => $this->string(32)->notNull(),
    			'host' => $this->string(64)->notNull(),
    			'starttls' => $this->boolean()->notNull()->defaultValue(0),
    			'smtp_auth' => $this->boolean()->notNull()->defaultValue(1),
    			'noreply_email' => $this->string(64)
    	], $tableOptions);
    
    	$this->createIndex('uk_email_config_email_host', 'email_config', array('email', 'host'), true);
    }
    
    private function createEmailType($tableOptions) {
    	$this->createTable('email_type', [
    			'id' => $this->primaryKey(11),
    			'email_config_id' => $this->integer(11)->notNull(),
    			'symbol' => $this->string(32)->notNull()->unique(),
    			'is_archived' => $this->boolean()->notNull()->defaultValue(0),
    			'is_enabled' => $this->boolean()->notNull(),
    	], $tableOptions);
    
    	$this->addForeignKey('fk_email_type_email_config', 'email_type', 'email_config_id', 'email_config', 'id');
    }

    private function createEmailTemplate($tableOptions) {
    	$this->createTable('email_template', [
    			'email_type_id' => $this->integer(11),
    			'language_id' => $this->integer(11),
    			'subject' => $this->string(256)->notNull(),
    			'content_html' => $this->text(),
    			'content_text' => $this->text()
    	], $tableOptions);
    
    	$this->addPrimaryKey('pk_email_template', 'email_template', array('email_type_id', 'language_id'));
    	$this->addForeignKey('fk_email_template_language', 'email_template', 'language_id', 'language', 'id');
    	$this->addForeignKey('fk_email_template_email_type', 'email_template', 'email_type_id', 'email_type', 'id');
    	
    	$this->alterColumn('email_template', 'content_html', 'mediumtext NOT NULL');
    }
    
    private function createCountry($tableOptions) {
    	$this->createTable('country', [
    			'id' => $this->primaryKey(11),
    			'symbol' => $this->string(5)->notNull()->unique(),
    			'priority' => $this->integer(11)->notNull()
    	], $tableOptions);
    }
    
    private function createCountryTranslation($tableOptions) {
    	$this->createTable('country_translation', [
    			'country_id' => $this->integer(11),
    			'language_id' => $this->integer(11),
    			'name' => $this->string(128)->notNull(),
    	], $tableOptions);
    
    	$this->addPrimaryKey('pk_country_translation', 'country_translation', array('country_id', 'language_id'));
    	$this->addForeignKey('fk_country_translation_language', 'country_translation', 'language_id', 'language', 'id');
    	$this->addForeignKey('fk_country_translation_country', 'country_translation', 'country_id', 'country', 'id');
    }
    
    private function createProvince($tableOptions) {
    	$this->createTable('province', [
    			'id' => $this->primaryKey(11),
    			'country_id' => $this->integer(11)->notNull(),
    			'symbol' => $this->string(5)->notNull(),
    			'name' => $this->string(64)->notNull()    			
    	], $tableOptions);
    
    	$this->addForeignKey('fk_province_country', 'province', 'country_id', 'country', 'id');
    	$this->createIndex('uk_province_symbol_country_id', 'province', ['symbol', 'country_id']);
    }
    
    private function createCity($tableOptions) {
    	$this->createTable('city', [
    			'id' => $this->primaryKey(11),
    			'province_id' => $this->integer(11),
    			'name' => $this->string(64)->notNull(),
    			'zip_code' => $this->string(10)->notNull(),
    	], $tableOptions);
    
    	$this->addForeignKey('fk_city_province', 'city', 'province_id', 'province', 'id');
    }
    
    private function createAddress($tableOptions) {
    	$this->createTable('address', [
    			'id' => $this->primaryKey(11),
    			'city_id' => $this->integer(11)->notNull(),
    			'main' => $this->string(64)->notNull(),
    			'complement' => $this->string(64)
    	], $tableOptions);
    
    	$this->addForeignKey('fk_address_city', 'address', 'city_id', 'city', 'id');
    }
    
    private function createUser($tableOptions) {
    	$this->createTable('user', [
    			'id' => $this->primaryKey(11),
    			'email' => $this->string(64)->unique(),
    			'password' => $this->string(64),
    			'first_name' => $this->string(32),
    			'last_name' => $this->string(32),
    			'phone' => $this->string(32),
    			'url_photo' => $this->string(256),
    			'address_id' => $this->integer(11),
    			'address_postal_id' => $this->integer(11),
    			'language_id' => $this->integer(11),
    			'currency_id' => $this->integer(11),
    			'is_active' => $this->boolean()->notNull()->defaultValue(0),
    			'date_creation' => $this->timestamp()->notNull(),
    			'date_modification' => $this->timestamp()->defaultValue(null),
    	], $tableOptions);
    
    	$this->createIndex('uk_user_email', 'user', 'email');
    	$this->addForeignKey('fk_user_language', 'user', 'language_id', 'language', 'id');
    	$this->addForeignKey('fk_user_currency', 'user', 'currency_id', 'currency', 'id');
    	$this->addForeignKey('fk_user_address', 'user', 'address_id', 'address', 'id');
    	$this->addForeignKey('fk_user_address_postal', 'user', 'address_postal_id', 'address', 'id');
    	
    	$this->alterColumn('user', 'date_creation', 'TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP');
    	$this->alterColumn('user', 'date_modification', 'TIMESTAMP NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP');
    }
    
    private function createUserRole($tableOptions) {
    	$this->createTable('user_role', [
    			'user_id' => $this->integer(11),
    			'role_id' => $this->integer(11)
    	], $tableOptions);
    
    	$this->addPrimaryKey('pk_user_role', 'user_role', array('user_id', 'role_id'));
    	$this->addForeignKey('fk_user_role_user', 'user_role', 'user_id', 'user', 'id');
    	$this->addForeignKey('fk_user_role_role', 'user_role', 'role_id', 'role', 'id');
    }
}
