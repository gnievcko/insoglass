<?php

use yii\db\Query;
use yii\db\Schema;
use yii\db\Migration;
use common\documents\DocumentType;
use common\documents\sections\SectionsMapper;

class m160823_094753_insert_invoice_data extends Migration {

    public function up() {
        $transaction = \Yii::$app->db->beginTransaction();

    	$this->insert('document_type', ['symbol' => DocumentType::INVOICE]);
        $invoiceTypeId = \Yii::$app->db->getLastInsertID();

    	$this->insert('document_section', ['symbol' => SectionsMapper::COMPANY_HEADER]);
        $companyHeaderTypeId = Yii::$app->db->getLastInsertID();
    	$this->insert('document_type_section', ['document_type_id' => $invoiceTypeId, 'document_section_id' => $companyHeaderTypeId, 'sequence_number' => 0]);

    	$this->insert('document_section', ['symbol' => SectionsMapper::INVOICE_TITLE]);
        $invoiceTitleTypeId = Yii::$app->db->getLastInsertID();
    	$this->insert('document_type_section', ['document_type_id' => $invoiceTypeId, 'document_section_id' => $invoiceTitleTypeId, 'sequence_number' => 1]);

    	$this->insert('document_section', ['symbol' => SectionsMapper::INVOICE_PRODUCTS]);
        $invoiceProductsTypeId = Yii::$app->db->getLastInsertID();
    	$this->insert('document_type_section', ['document_type_id' => $invoiceTypeId, 'document_section_id' => $invoiceProductsTypeId, 'sequence_number' => 2]);

        $transaction->commit();
    }

    public function down() {
        $transaction = Yii::$app->db->beginTransaction();

        $this->delete('document_type_section');
    	$this->delete('document_section');
    	$this->delete('document_type');

        $transaction->commit();
    }
}
