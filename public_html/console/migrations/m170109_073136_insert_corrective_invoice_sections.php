<?php

use yii\db\Migration;
use common\models\ar\DocumentType;
use common\models\ar\DocumentSection;
use common\models\ar\DocumentTypeSection;

class m170109_073136_insert_corrective_invoice_sections extends Migration {

    public function up() {
		$documentTypeId = DocumentType::find()->where(['symbol' => 'CORRECTIVE_INVOICE'])->one()->id;
		$invoicePaymentId = DocumentSection::find()->where(['symbol' => 'INVOICE_PAYMENT'])->one()->id;
		
		$this->insert('document_section', [
				'symbol' => 'CORRECTIVE_INVOICE_PAYMENT',
		]);
		$documentSectionId = $this->db->getLastInsertID();
		
		$this->update('document_type_section', ['document_section_id' => $documentSectionId], 
				['document_section_id' => $invoicePaymentId, 'document_type_id' => $documentTypeId]
		);
    }

    public function down() {
    	$documentTypeId = DocumentType::find()->where(['symbol' => 'CORRECTIVE_INVOICE'])->one()->id;
        $id2 = DocumentSection::find()->where(['symbol' => 'CORRECTIVE_INVOICE_PAYMENT'])->one()->id;
        $invoicePaymentId = DocumentSection::find()->where(['symbol' => 'INVOICE_PAYMENT'])->one()->id;
        
        $this->update('document_type_section', ['document_section_id' => $invoicePaymentId], ['document_section_id' => $id2, 'document_type_id' => $documentTypeId]);
        
        DocumentTypeSection::deleteAll(['document_section_id' => [$id2]]);
        DocumentSection::deleteAll(['id' => [$id2]]);
    }
}
