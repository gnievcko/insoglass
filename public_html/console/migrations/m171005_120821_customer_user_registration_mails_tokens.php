<?php

use common\models\ar\Language;
use yii\db\Migration;
use yii\db\Query;

class m171005_120821_customer_user_registration_mails_tokens extends Migration {

    public function safeUp() {
        $emailTypes = [
            ['symbol' => 'email_customer_new_user', 'tags' => '{activation_link},{app_name},{team_name}'],
            ['symbol' => 'email_customer_new_user_new_comp', 'tags' => '{full_name},{email},{company_name},{VAT_ID},{team_name}'],
            ['symbol' => 'email_customer_new_user_existing', 'tags' => '{full_name},{email},{company_name},{VAT_ID},{team_name}'],
            ['symbol' => 'email_customer_new_activation_li', 'tags' => '{activation_link},{app_name},{team_name}'],
        ];
        foreach ($emailTypes as &$emailType) {
            $this->insert('email_type', ['symbol' => $emailType['symbol'], 'tags' => $emailType['tags'], 'is_archived' => 0, 'is_active' => 1]);
            $emailType['id'] = $this->db->getLastInsertID();
        }
        unset($emailType);

        $emailTemplates = [
            ['email_type_symbol' => 'email_customer_new_user', 'subject' => 'Aktywacja konta', 'content_html' => '<html><head><title></title></head><body><p>Witaj!</p><p>Twoje konto zostało zarejestrowane w systemie {app_name}. Przed zalogowaniem należy aktywować konto, klikając w poniższy link:<br><a href="{activation_link}">{activation_link}</a></p><p>Pozdrawiamy,<br>zespół {team_name}</p><p><span style="font-size: 75%;">Ten email został wygenerowany automatycznie, prosimy na niego nie odpowiadać.</span></p></body></html>', 'content_text' => "Witaj!\n\nTwoje konto zostało zarejestrowane w systemie {app_name}. Przed zalogowaniem należy aktywować konto, klikając w poniższy link:\n{activation_link}\n\nPozdrawiamy,\nzespół {team_name}\n\nTen email został wygenerowany automatycznie, prosimy na niego nie odpowiadać."],
            ['email_type_symbol' => 'email_customer_new_user_new_comp', 'subject' => 'Nowy użytkownik (nowa firma)', 'content_html' => '<html><head><title></title></head><body><p>Witaj!</p><p>Nowy użytkownik zgłosił się do firmy. Nie będzie mógł się zalogować, dopóki nie zostaną zweryfikowane jego dane, a możliwość logowania nie zostanie włączona.</p><p>Dane użytkownika:<br>Imię i nazwisko: {full_name}<br>Adres email: {email}</p><p>Dane firmy:<br>Nazwa: {company_name}<br>NIP: {VAT_ID}</p><p>Pozdrawiamy,<br>zespół {team_name}</p><p><span style="font-size: 75%;">Ten email został wygenerowany automatycznie, prosimy na niego nie odpowiadać.</span></p></body></html>', 'content_text' => "Witaj!\n\nNowy użytkownik zgłosił się do firmy. Nie będzie mógł się zalogować, dopóki nie zostaną zweryfikowane jego dane, a możliwość logowania nie zostanie włączona.\n\nDane użytkownika:\nImię i nazwisko: {full_name}\nAdres email: {email}\n\nDane firmy:\nNazwa: {company_name}\nNIP: {VAT_ID}\n\nPozdrawiamy,\nzespół {team_name}\n\nTen email został wygenerowany automatycznie, prosimy na niego nie odpowiadać."],
            ['email_type_symbol' => 'email_customer_new_user_existing', 'subject' => 'Nowy użytkownik (istniejąca firma)', 'content_html' => '<html><head><title></title></head><body><p>Witaj!</p><p>Nowy użytkownik zgłosił się do firmy. Nie będzie mógł się zalogować, dopóki nie zostaną zweryfikowane jego dane, a możliwość logowania nie zostanie włączona.</p><p>Dane użytkownika:<br>Imię i nazwisko: {full_name}<br>Adres email: {email}</p><p>Dane firmy:<br>Nazwa: {company_name}<br>NIP: {VAT_ID}</p><p>Pozdrawiamy,<br>zespół {team_name}</p><p><span style="font-size: 75%;">Ten email został wygenerowany automatycznie, prosimy na niego nie odpowiadać.</span></p></body></html>', 'content_text' => "Witaj!\n\nNowy użytkownik zgłosił się do firmy. Nie będzie mógł się zalogować, dopóki nie zostaną zweryfikowane jego dane, a możliwość logowania nie zostanie włączona.\n\nDane użytkownika:\nImię i nazwisko: {full_name}\nAdres email: {email}\n\nDane firmy:\nNazwa: {company_name}\nNIP: {VAT_ID}\n\nPozdrawiamy,\nzespół {team_name}\n\nTen email został wygenerowany automatycznie, prosimy na niego nie odpowiadać."],
            ['email_type_symbol' => 'email_customer_new_activation_li', 'subject' => 'Aktywacja konta (nowy link aktywacyjny)', 'content_html' => '<html><head><title></title></head><body><p>Witaj!</p><p>Twoje konto zostało zarejestrowane w systemie {app_name}. Przed zalogowaniem należy aktywować konto, klikając w poniższy link:<br><a href="{activation_link}">{activation_link}</a></p><p>Pozdrawiamy,<br>zespół {team_name}</p><p><span style="font-size: 75%;">Ten email został wygenerowany automatycznie, prosimy na niego nie odpowiadać.</span></p></body></html>', 'content_text' => "Witaj!\n\nTwoje konto zostało zarejestrowane w systemie {app_name}. Przed zalogowaniem należy aktywować konto, klikając w poniższy link:\n{activation_link}\n\nPozdrawiamy,\nzespół {team_name}\n\nTen email został wygenerowany automatycznie, prosimy na niego nie odpowiadać."],
        ];
        foreach ($emailTemplates as $emailTemplate) {
            $emailTypeID = array_search($emailTemplate['email_type_symbol'], array_column($emailTypes, 'symbol', 'id'));
            // $languageID = Language::findOne(['symbol' => 'pl'])->id;
            $languageID = (new Query())->select('id')->from('language')->where(['symbol' => 'pl'])->scalar();
            $this->insert('email_template', ['email_type_id' => $emailTypeID, 'language_id' => $languageID, 'subject' => $emailTemplate['subject'], 'content_html' => $emailTemplate['content_html'], 'content_text' => $emailTemplate['content_text']]);
        }

        $this->insert('token_type', ['symbol' => 'customer_activate_account']);
    }

    public function safeDown() {
        echo "m171005_120821_customer_user_registration_mails_tokens cannot be reverted.\n";

        return false;
    }
}
