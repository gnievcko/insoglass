<?php

use yii\db\Schema;
use yii\db\Migration;
use yii\db\Query;
use common\models\aq\WarehouseQuery;

class m170223_124134_alter_warehouse_product_last_price extends Migration {

    public function safeUp() {
		$this->addColumn('warehouse_product', 'price_unit', 'DECIMAL(12,2) NULL DEFAULT NULL');
		$this->addColumn('warehouse_product', 'price_group', 'DECIMAL(12,2) NULL DEFAULT NULL');
		$this->addColumn('warehouse_product', 'currency_id', 'INT(11) NULL DEFAULT NULL');
		$this->addForeignKey('fk_warehouse_product_currency', 'warehouse_product', 'currency_id', 'currency', 'id');
		
		$warehouse = WarehouseQuery::getDefaultWarehouse()->one();
		
		$subquery = (new Query())->select(['wdp1.id'])
				->from('warehouse_delivery_product wdp1')
				->join('INNER JOIN', 'warehouse_delivery wd1', 'wdp1.warehouse_delivery_id = wd1.id')
				->where(['wd1.is_active' => true])
				->andWhere(['wd1.is_confirmed' => true])
				->andWhere(['wd1.is_artificial' => false])
				->andWhere(['wd1.operation_id' => null])
				->andWhere('wdp1.product_id = wdp.product_id')
				->orderBy(['wd1.date_creation' => SORT_DESC])
				->limit(1);
		
		$lastDeliveries = (new Query())->select(['wdp.product_id', 'wdp.price_unit', 'wdp.price_group', 'wdp.currency_id'])
				->from('warehouse_delivery_product wdp')
				->join('INNER JOIN', 'warehouse_delivery wd', 'wdp.warehouse_delivery_id = wd.id')
				->where(['wd.is_active' => true])
				->andWhere(['wd.is_confirmed' => true])
				->andWhere(['wd.is_artificial' => false])
				->andWhere(['wd.operation_id' => null])
				->andWhere(['=', 'wdp.id', $subquery])
				->all();
		
			
		foreach($lastDeliveries as $del) {
			$this->update('warehouse_product', [
							'price_unit' => $del['price_unit'], 
							'price_group' => $del['price_group'],
							'currency_id' => $del['currency_id']
					], [
							'product_id' => $del['product_id'],
							'warehouse_id' => $warehouse->id,
					]);
		}
    }

    public function safeDown() {
		$this->dropForeignKey('fk_warehouse_product_currency', 'warehouse_product');
		$this->dropColumn('warehouse_product', 'currency_id');
		$this->dropColumn('warehouse_product', 'price_group');
		$this->dropColumn('warehouse_product', 'price_unit');
    }
}
