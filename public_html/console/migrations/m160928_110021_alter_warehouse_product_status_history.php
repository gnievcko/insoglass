<?php

use yii\db\Schema;
use yii\db\Migration;

class m160928_110021_alter_warehouse_product_status_history extends Migration {

    public function up() {
		$this->addColumn('warehouse_product_status_history', 'history_blocked_id', 'INT(11) NULL DEFAULT NULL AFTER description');
		$this->addForeignKey('fk_warehouse_product_status_history_history', 'warehouse_product_status_history', 'history_blocked_id', 'warehouse_product_status_history', 'id');
    }

    public function down() {
        $this->dropForeignKey('fk_warehouse_product_status_history_history', 'warehouse_product_status_history');
        $this->dropColumn('warehouse_product_status_history', 'history_blocked_id');
    }
}
