<?php

use yii\db\Query;
use yii\db\Schema;
use yii\db\Migration;
use common\documents\DocumentType;
use common\documents\sections\SectionsMapper;

class m160824_104117_append_invoice_sections extends Migration {

    public function up() {
        $transaction = \Yii::$app->db->beginTransaction();

        $invoiceTypeId = (new Query())->select(['id'])->from('document_type')->where(['symbol' => DocumentType::INVOICE])->one()['id'];

    	$this->insert('document_section', ['symbol' => SectionsMapper::INVOICE_PAYMENT]);
        $invoicePaymentTypeId = Yii::$app->db->getLastInsertID();
    	$this->insert('document_type_section', ['document_type_id' => $invoiceTypeId, 'document_section_id' => $invoicePaymentTypeId, 'sequence_number' => 3]);

    	$this->insert('document_section', ['symbol' => SectionsMapper::COMPANY_FOOTER]);
        $companyFooterTypeId = Yii::$app->db->getLastInsertID();
    	$this->insert('document_type_section', ['document_type_id' => $invoiceTypeId, 'document_section_id' => $companyFooterTypeId, 'sequence_number' => 4]);

        $transaction->commit();
    }

    public function down() {
        $transaction = Yii::$app->db->beginTransaction();

        $invoiceTypeId = (new Query())->select(['id'])->from('document_type')->where(['symbol' => DocumentType::INVOICE])->one()['id'];
        $invoicePaymentTypeId = (new Query())->select(['id'])->from('document_section')->where(['symbol' => SectionsMapper::INVOICE_PAYMENT])->one()['id'];
        $companyFooterTypeId = (new Query())->select(['id'])->from('document_section')->where(['symbol' => SectionsMapper::COMPANY_FOOTER])->one()['id'];

        $this->delete('document_type_section', ['document_type_id' => $invoiceTypeId, 'document_section_id' => $invoicePaymentTypeId]);
        $this->delete('document_type_section', ['document_type_id' => $invoiceTypeId, 'document_section_id' => $companyFooterTypeId]);

    	$this->delete('document_section', ['id' => $invoicePaymentTypeId]);
    	$this->delete('document_section', ['id' => $companyFooterTypeId]);

        $transaction->commit();
    }
}
