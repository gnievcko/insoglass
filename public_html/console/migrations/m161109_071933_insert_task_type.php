<?php

use yii\db\Schema;
use yii\db\Migration;
use common\models\ar\Language;
use common\models\ar\TaskType;
use common\models\ar\TaskTypeReference;
use common\models\ar\TaskTypeReferenceTranslation;
use common\models\ar\TaskTypeTranslation;

class m161109_071933_insert_task_type extends Migration {

    public function up() {
    	$languageId = Language::find()->where(['symbol' => 'pl'])->one()->id;
    	
    	$this->addColumn('task_type', 'priority', 'INT(3) NULL DEFAULT NULL');
    	$this->update('task_type', ['priority' => 1], ['symbol' => 'client_contact']);
    	$this->update('task_type', ['priority' => 2], ['symbol' => 'associated_with_order']);
    	$this->update('task_type', ['priority' => 100], ['symbol' => 'other']);
    	$this->alterColumn('task_type', 'priority', 'INT(3) NOT NULL');
		
		$this->insert('task_type', [
				'symbol' => 'order_product',
				'priority' => 3
		]);
		$taskTypeId = $this->db->getLastInsertID();
		
		$this->insert('task_type_translation', [
				'task_type_id' => $taskTypeId,
				'language_id' => $languageId,
				'name' => 'Zamówienie produktu',
		]);
		
		$this->insert('task_type_reference', [
				'task_type_id' => $taskTypeId,
				'symbol' => 'product_data',
				'name_table' => 'product',
				'name_column' => 'id',
				'name_expression' => 'product.symbol',
				'url_details' => 'product/details/{id}',
				'is_required' => 1,
		]);
		$referenceId = $this->db->getLastInsertID();
		
		$this->insert('task_type_reference_translation', [
				'reference_id' => $referenceId,
				'language_id' => $languageId,
				'name' => 'Dotyczy produktu',
		]);
    }

    public function down() {
        $taskTypeId = TaskType::find()->where(['symbol' => 'order_product'])->one()->id;
        $referenceId = TaskTypeReference::find()->where(['task_type_id' => $taskTypeId])->one()->id;
        
        TaskTypeReferenceTranslation::deleteAll(['reference_id' => $referenceId]);
        TaskTypeReference::deleteAll(['id' => $referenceId]);
        TaskTypeTranslation::deleteAll(['task_type_id' => $taskTypeId]);
        TaskType::deleteAll(['id' => $taskTypeId]);
        
        $this->dropColumn('task_type', 'priority');
    }
}
