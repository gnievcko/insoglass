<?php

namespace console\components;

use common\components\MessageTrait;

/**
 * @inheritdoc
 */
class Controller extends \yii\console\Controller {
	
	use MessageTrait;
	
}
