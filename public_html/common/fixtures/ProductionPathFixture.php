<?php
namespace common\fixtures;

use yii\test\ActiveFixture;

class ProductionPathFixture extends ActiveFixture {
	
    public $modelClass = 'common\models\ar\ProductionPath';
    public $dataFile = 'common\tests\_data\production-path.php';
}
