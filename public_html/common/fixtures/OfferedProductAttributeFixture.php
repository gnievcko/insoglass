<?php
namespace common\fixtures;

use yii\test\ActiveFixture;

class OfferedProductAttributeFixture extends ActiveFixture {
	
    public $modelClass = 'common\models\ar\OfferedProductAttribute';
    public $dataFile = 'common\tests\_data\offered-product-attribute.php';
}
