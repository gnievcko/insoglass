<?php
namespace common\fixtures;

use yii\test\ActiveFixture;

class DepartmentTypeFixture extends ActiveFixture {
	
    public $modelClass = 'common\models\ar\DepartmentType';
    public $dataFile = 'common\tests\_data\department-type.php';
}
