<?php
namespace common\fixtures;

use yii\test\ActiveFixture;

class DepartmentActivityFixture extends ActiveFixture {
	
    public $modelClass = 'common\models\ar\DepartmentActivity';
    public $dataFile = 'common\tests\_data\department-activity.php';
}