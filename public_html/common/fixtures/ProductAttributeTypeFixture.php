<?php
namespace common\fixtures;

use yii\test\ActiveFixture;

class ProductAttributeTypeFixture extends ActiveFixture {
	
    public $modelClass = 'common\models\ar\ProductAttributeType';
    public $dataFile = 'common\tests\_data\product-attribute-type.php';
}
