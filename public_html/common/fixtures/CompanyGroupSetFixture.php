<?php
namespace common\fixtures;

use yii\test\ActiveFixture;

class CompanyGroupSetFixture extends ActiveFixture {
	
    public $modelClass = 'common\models\ar\CompanyGroupSet';
    public $dataFile = 'common\tests\_data\company-group-set.php';
}