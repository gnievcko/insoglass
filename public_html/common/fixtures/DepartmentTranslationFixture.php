<?php
namespace common\fixtures;

use yii\test\ActiveFixture;

class DepartmentTranslationFixture extends ActiveFixture {
	
    public $modelClass = 'common\models\ar\DepartmentTranslation';
    public $dataFile = 'common\tests\_data\department-translation.php';
}