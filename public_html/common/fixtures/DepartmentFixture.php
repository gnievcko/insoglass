<?php
namespace common\fixtures;

use yii\test\ActiveFixture;

class DepartmentFixture extends ActiveFixture {
	
    public $modelClass = 'common\models\ar\Department';
    public $dataFile = 'common\tests\_data\department.php';
}