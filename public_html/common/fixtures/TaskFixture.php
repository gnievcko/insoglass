<?php
namespace common\fixtures;

use yii\test\ActiveFixture;

class TaskFixture extends ActiveFixture {
	
    public $modelClass = 'common\models\ar\Task';
    public $dataFile = 'common\tests\_data\task.php';
}