<?php
namespace common\fixtures;

use yii\test\ActiveFixture;

class UserWorkstationFixture extends ActiveFixture {
	
    public $modelClass = 'common\models\ar\UserWorkstation';
    public $dataFile = 'common\tests\_data\user-workstation.php';
}