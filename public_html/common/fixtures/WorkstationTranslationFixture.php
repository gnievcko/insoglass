<?php
namespace common\fixtures;

use yii\test\ActiveFixture;

class WorkstationTranslationFixture extends ActiveFixture {
	
    public $modelClass = 'common\models\ar\WorkstationTranslation';
    public $dataFile = 'common\tests\_data\workstation-translation.php';
}