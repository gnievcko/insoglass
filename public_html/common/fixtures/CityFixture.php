<?php
namespace common\fixtures;

use yii\test\ActiveFixture;

class CityFixture extends ActiveFixture {
	
    public $modelClass = 'common\models\ar\City';
    public $dataFile = 'common\tests\_data\city.php';
}