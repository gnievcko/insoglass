<?php
namespace common\fixtures;

use yii\test\ActiveFixture;

class CompanyFixture extends ActiveFixture {
	
    public $modelClass = 'common\models\ar\Company';
    public $dataFile = 'common\tests\_data\company.php';
}