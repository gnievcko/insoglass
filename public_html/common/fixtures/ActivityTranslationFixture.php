<?php
namespace common\fixtures;

use yii\test\ActiveFixture;

class ActivityTranslationFixture extends ActiveFixture {
	
    public $modelClass = 'common\models\ar\ActivityTranslation';
    public $dataFile = 'common\tests\_data\activity-translation.php';
}