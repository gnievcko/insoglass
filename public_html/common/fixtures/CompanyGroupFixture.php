<?php
namespace common\fixtures;

use yii\test\ActiveFixture;

class CompanyGroupFixture extends ActiveFixture {
	
    public $modelClass = 'common\models\ar\CompanyGroup';
    public $dataFile = 'common\tests\_data\company-group.php';
}