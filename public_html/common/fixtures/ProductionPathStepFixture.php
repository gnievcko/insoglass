<?php
namespace common\fixtures;

use yii\test\ActiveFixture;

class ProductionPathStepFixture extends ActiveFixture {
	
    public $modelClass = 'common\models\ar\ProductionPathStep';
    public $dataFile = 'common\tests\_data\production-path-step.php';
}