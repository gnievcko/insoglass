<?php
namespace common\fixtures;

use yii\test\ActiveFixture;

class ProductAttributeGroupTranslationFixture extends ActiveFixture {
	
    public $modelClass = 'common\models\ar\ProductAttributeGroupTranslation';
    public $dataFile = 'common\tests\_data\product-attribute-group-translation.php';
}
