<?php
namespace common\fixtures;

use yii\test\ActiveFixture;

class ProductionTaskFixture extends ActiveFixture {
	
    public $modelClass = 'common\models\ar\ProductionTask';
    public $dataFile = 'common\tests\_data\production-task.php';
}