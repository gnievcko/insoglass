<?php
namespace common\fixtures;

use yii\test\ActiveFixture;

class OfferedProductTranslationFixture extends ActiveFixture {
	
    public $modelClass = 'common\models\ar\OfferedProductTranslation';
    public $dataFile = 'common\tests\_data\offered-product-translation.php';
}
