<?php
namespace common\fixtures;

use yii\test\ActiveFixture;

class ProductAttributeTypeTranslationFixture extends ActiveFixture {
	
    public $modelClass = 'common\models\ar\ProductAttributeTypeTranslation';
    public $dataFile = 'common\tests\_data\product-attribute-type-translation.php';
}
