<?php
namespace common\fixtures;

use yii\test\ActiveFixture;

class CompanyGroupTranslationFixture extends ActiveFixture {
	
    public $modelClass = 'common\models\ar\CompanyGroupTranslation';
    public $dataFile = 'common\tests\_data\company-group-translation.php';
}