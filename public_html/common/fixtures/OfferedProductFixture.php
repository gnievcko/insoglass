<?php
namespace common\fixtures;

use yii\test\ActiveFixture;

class OfferedProductFixture extends ActiveFixture {
	
    public $modelClass = 'common\models\ar\OfferedProduct';
    public $dataFile = 'common\tests\_data\offered-product.php';
}
