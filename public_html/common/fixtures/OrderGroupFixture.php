<?php
namespace common\fixtures;

use yii\test\ActiveFixture;

class OrderGroupFixture extends ActiveFixture {
	
    public $modelClass = 'common\models\ar\OrderGroup';
    public $dataFile = 'common\tests\_data\order-group.php';
}