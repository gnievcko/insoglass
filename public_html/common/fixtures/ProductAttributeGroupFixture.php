<?php
namespace common\fixtures;

use yii\test\ActiveFixture;

class ProductAttributeGroupFixture extends ActiveFixture {
	
    public $modelClass = 'common\models\ar\ProductAttributeGroup';
    public $dataFile = 'common\tests\_data\product-attribute-group.php';
}
