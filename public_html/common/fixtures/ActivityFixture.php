<?php
namespace common\fixtures;

use yii\test\ActiveFixture;

class ActivityFixture extends ActiveFixture {
	
    public $modelClass = 'common\models\ar\Activity';
    public $dataFile = 'common\tests\_data\activity.php';
}