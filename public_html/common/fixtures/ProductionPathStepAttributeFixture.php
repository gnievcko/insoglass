<?php
namespace common\fixtures;

use yii\test\ActiveFixture;

class ProductionPathStepAttributeFixture extends ActiveFixture {
	
    public $modelClass = 'common\models\ar\ProductionPathStepAttribute';
    public $dataFile = 'common\tests\_data\production-path-step-attribute.php';
}