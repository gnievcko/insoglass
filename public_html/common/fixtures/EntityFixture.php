<?php
namespace common\fixtures;

use yii\test\ActiveFixture;

class EntityFixture extends ActiveFixture {
	
    public $modelClass = 'common\models\ar\Entity';
    public $dataFile = 'common\tests\_data\entity.php';
}