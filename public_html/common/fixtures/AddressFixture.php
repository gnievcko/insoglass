<?php
namespace common\fixtures;

use yii\test\ActiveFixture;

class AddressFixture extends ActiveFixture {
	
    public $modelClass = 'common\models\ar\Address';
    public $dataFile = 'common\tests\_data\address.php';
}