<?php
namespace common\fixtures;

use yii\test\ActiveFixture;

class WorkstationFixture extends ActiveFixture {
	
    public $modelClass = 'common\models\ar\Workstation';
    public $dataFile = 'common\tests\_data\workstation.php';
}