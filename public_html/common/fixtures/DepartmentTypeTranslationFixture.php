<?php
namespace common\fixtures;

use yii\test\ActiveFixture;

class DepartmentTypeTranslationFixture extends ActiveFixture {
	
    public $modelClass = 'common\models\ar\DepartmentTypeTranslation';
    public $dataFile = 'common\tests\_data\department-type-translation.php';
}
