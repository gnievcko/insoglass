<?php
return [
	    'implementingCompany' => 'MindsEater Sp. z o.o.',
	    'maintenance' => false,
	    'version' => 'v1.0',
	    'emailAvailableTags' => [
			'{firstname}',
	    	'{lastname}',
			'{email}',
			'{password}',
			'{link}',
			'{name}',
			'{body}',
	    	'{link_with_token}',
	        '{activation_link}',
	        '{app_name}',
	        '{team_name}',
	        '{full_name}',
	        '{company_name}',
	        '{VAT_ID}',
	    ],
	    'owner' => [
	    		'name' => 'INSOGLAS Andrzej i Karol Woźniak sp.j.',
	    		'address' => 'ul. Kamienna 6, 62-081 Wysogotowo',
	    		'email' => 'insoglas@insoglas.pl',
	    		'phone' => 'tel. 61 816 29 19',
	    		'vatin' => '//TODO vatin',
	    		'regon' => '630321860',
                'vatId' => '781-10-08-571',
	    		'bank' => '',
                'logo' => '@web/images/logo_insoglas.jpg',
                'certificateLogo' => '',
                'swift' => '',
                'bankAddress' => '',
                'nationalCourtRegister' => '0000109108',
                'courtAddress' => '',
                'shareCapital' => '',
	    ],
	    'identityDuration' => 0,
	    'user.passwordResetTokenExpire' => 3600, // 3600 = 1h
        'customerTimeForAccountActivation' => 259200, // 25900 secs = 3 days
        'maxDimensionShort' => 2550, //[mm]
        'maxDimensionLong' =>  3210 //[mm]
];
