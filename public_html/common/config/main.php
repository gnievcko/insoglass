<?php

return [
	    'vendorPath' => dirname(dirname(__DIR__)) . '/vendor',
		'modules' => [
				'datecontrol' =>  [
						'class' => '\kartik\datecontrol\Module'
				]
		],
	    'components' => [
		        'cache' => [
		            	'class' => 'yii\caching\FileCache',
		        ],
		        'i18n' => [
				        'translations' => [
						        'main' => [
								        'class' => 'yii\i18n\PhpMessageSource',
								        'basePath' => '@common/messages'
						        ],
						        'web' => [
								        'class' => 'yii\i18n\PhpMessageSource',
								        'basePath' => '@common/messages'
						        ],
						        'documents' => [
								        'class' => 'yii\i18n\PhpMessageSource',
								        'basePath' => '@common/messages'
						        ],
                                'documents_orders' => [
								        'class' => 'yii\i18n\PhpMessageSource',
								        'basePath' => '@common/messages'
						        ],
				        ],
		        ],
		        'authManager' => [
		        		'class' => 'yii\rbac\PhpManager',
		        		'assignmentFile' => '@common/rbac/assignments.php',
		        		'itemFile' => '@common/rbac/items.php',
		        		'ruleFile' => '@common/rbac/rules.php'
		        ],
		        /* 'assetManager' => [
		        		'appendTimestamp' => true,
		        ], */
	    ],
	    'language' => 'pl',
	    'sourceLanguage' => 'en',
		'timeZone' => 'Europe/Berlin',
];
