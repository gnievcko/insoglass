<?php
use kartik\mpdf\Pdf;

return [
	    'components' => [
		        'db' => [
			            'class' => 'yii\db\Connection',
			            'dsn' => 'mysql:host=localhost;dbname=systemin_prod',
			            'username' => 'systemin_prod',
			            'password' => 'gxUA8CVjuaA2QvE7',
			            'charset' => 'utf8',
		        ],
                'mongodb' => [
			            'class' => 'yii\mongodb\Connection',
			            'dsn' => 'mongodb://10.252.100.15:27017/insoglas',
		        ],
		        'mailer' => [
			            'class' => 'yii\swiftmailer\Mailer',
			            'viewPath' => '@common/mail',
		        ],
	    ],
];
