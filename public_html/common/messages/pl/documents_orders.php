<?php

return [
    	'No.' => 'L.dz.',
    	'From' => 'Od',
    	'To' => 'Do',
    	'Refers to' => 'Dotyczy',
    	'Price per unit' => 'Cena jedn.',
    	'Total price' => 'Łączna wartość',
    	'Count' => 'Ilość',
    	'Deadline' => 'Termin realizacji',
    	'Terms of payment' => 'Warunki płatności',
    	'Validity date' => 'Ważność oferty',
    	'Additional information' => 'Informacje dodatkowe',
		'The table with products can be displayed in another way in a resulting PDF file, regarding available data. If sum of cost is equal to 0, the summary row will not be visible.' => 'Tabela z produktami może być wyświetlana w inny sposób w wynikowym pliku PDF, zależnie od dostępnych danych. Jeśli suma kosztów wyniesie 0, wiersz z podsumowaniem nie będzie widoczny.', 
];