<?php
return [
    'client_role' => [
        'type' => 1,
        'children' => [
            'client',
        ],
    ],
    'salesman_role' => [
        'type' => 1,
        'children' => [
            'salesman',
        ],
    ],
    'repairer_role' => [
        'type' => 1,
        'children' => [
            'repairer',
        ],
    ],
    'main_salesman_role' => [
        'type' => 1,
        'children' => [
            'salesman_role',
            'main_salesman',
        ],
    ],
    'storekeeper_role' => [
        'type' => 1,
        'children' => [
            'storekeeper',
        ],
    ],
    'main_storekeeper_role' => [
        'type' => 1,
        'children' => [
            'storekeeper_role',
            'main_storekeeper',
        ],
    ],
    'worker_role' => [
        'type' => 1,
        'children' => [
            'worker',
        ],
    ],
    'qa_specialist_role' => [
        'type' => 1,
        'children' => [
            'qa_specialist',
        ],
    ],
    'main_worker_role' => [
        'type' => 1,
        'children' => [
            'worker_role',
            'qa_specialist_role',
            'main_worker',
        ],
    ],
    'admin_role' => [
        'type' => 1,
        'children' => [
            'client_role',
            'main_salesman_role',
            'main_storekeeper_role',
            'main_worker_role',
            'repairer_role',
            'admin',
        ],
    ],
    'client' => [
        'type' => 2,
    ],
    'salesman' => [
        'type' => 2,
    ],
    'repairer' => [
        'type' => 2,
    ],
    'main_salesman' => [
        'type' => 2,
    ],
    'storekeeper' => [
        'type' => 2,
    ],
    'main_storekeeper' => [
        'type' => 2,
    ],
    'worker' => [
        'type' => 2,
    ],
    'qa_specialist' => [
        'type' => 2,
    ],
    'main_worker' => [
        'type' => 2,
    ],
    'admin' => [
        'type' => 2,
    ],
];
