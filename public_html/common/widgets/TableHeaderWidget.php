<?php
namespace common\widgets;

use yii\base\Widget;
use Yii;

class TableHeaderWidget extends Widget
{
	const TABLECLASS = 'table table-bordered grid-table';
	public $listForm;
	public $fields;
	public $action;
	public $iconCount;
	public $actionColumnClass;
	
	public function init()
	{
		parent::init();
		if ($this->fields === null && $this->listForm === null) {
			throw new Exception('Not data');
		}
		if($this->action === null) {
			$this->action = true;
		}
	
		$iconCount = 3;
	}

	public function run()
	{
		$string ='';
		$string .='<thead>';
		$string .='<tr>';
		
		if(empty($this->actionColumnClass)) {
		    $this->actionColumnClass = '';
		}

		foreach($this->fields as $field) {
			if(!isset($field['isSortable'])) {
				$field['isSortable'] = true;
			}
			if(!isset($field['isHidden'])) {
				$field['isHidden'] = false;
			}
			if($field['isHidden']) {
				continue;
			}
			$isSortedByThisField = ($field['name'] == $this->listForm->sortField);
			$sortDirAfterClick = $isSortedByThisField ? (($this->listForm->sortDir == 'asc') ? 'desc' : 'asc') : 'asc';

			$style = (isset($field['style']) && !empty($field['style'])) ? 'style="'.$field['style'].'"' : '';
			
			$string .='<th '.(!empty($style) ? $style : '').' class="'. ($field['isSortable'] ? "dynamic-table-sort" : "" ).' " data-sort-field="'.$field['name'].'" data-sort-dir="'.$sortDirAfterClick.'">';
			if($field['isSortable']) $string .='<span>'.$field['label'].'</span>';
			else $string .= $field['label'];
		
			$spanClass = $isSortedByThisField ? (($sortDirAfterClick == 'asc') ? 'base-icon caret-icon-down table-caret' : 'base-icon caret-icon table-caret') : 'base-icon caret-icon table-caret';
			if($field['isSortable']) {
				$string .=' <div class="'.$spanClass.'"></div>';
			}
			$string .='</th>';
			                    
		}
		if($this->action == true) {
			$actionStyle = 'style="width: 120px"';
			try {
				if($this->iconCount > 3) {
					$actionStyle = 'style="width: 150px"';
				}
			}
			catch(\Exception $e) { 
				$actionStyle = '';
			}
			
			$string .='<th '.$actionStyle.' class="tableAction' . $this->actionColumnClass .'">'.Yii::t('web', 'Actions').'</th>';
		}
		$string .=' </tr>';
		$string .='</thead>';
		return $string;
	}
	
}