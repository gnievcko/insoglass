<?php
namespace common\tests\unit\models\ar;

use common\models\ar\Company;
use common\models\ar\User;
use common\fixtures\CompanyFixture;
use common\fixtures\UserFixture;

class CompanyRepresentativeTest extends \Codeception\Test\Unit {
    /**
     * @var \UnitTester
     */
    protected $tester;

    public function _before()
    {
        $this->tester->haveFixtures([
            'company' => [
                'class' => CompanyFixture::className(),
            ],
            'user' => [
                'class' => UserFixture::className(),  
            ],
        ]);
    }

    protected function _after() {
    }

    public function testCorrectCreate()
    {
		$company = Company::find()->where(['name' => 'MindsEater'])->one();
	
        $model = new User();
		$model->company_id = $company->id;
		$model->email = 'cokolwiek@mindseater.com';
		$model->is_active = true;
		
		try {
			$model->save();
		} 
		catch(\Exception $e) {
			$this->fail($e->getMessage());
		}

        expect($model)->isInstanceOf('common\models\ar\User');

        expect($model->email)->equals('cokolwiek@mindseater.com');
		expect($model->is_active)->equals(true);
    }
	
	public function testIncorrectCreate()
    {
		$company = Company::find()->where(['name' => 'MindsEater'])->one();
	
        $model = new User();
		$model->company_id = $company->id;
		$model->email = 'cokolwiek@mindseater.com';
		$model->is_active = true;
		$model->save();
		
		$model2 = new User();
		$model2->company_id = $company->id;
		$model2->email = 'cokolwiek@mindseater.com';
		$model2->is_active = true;
		try {
			$model2->save();
			$this->fail('Duplikat');
		} 
		catch(\Exception $e) { 
			$this->assertTrue(true);
		}
    }
}