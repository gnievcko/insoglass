<?php
namespace common\tests\unit\models\ar;

use common\models\ar\Company;

class CompanyTest extends \Codeception\Test\Unit {
    /**
     * @var \UnitTester
     */
    protected $tester;

    protected function _before() {
    }

    protected function _after() {
    }

    // tests
    public function testValidate() {
		$company = new Company();
		
		$company->name = null;
		$this->assertFalse($company->validate(['name']));
		
		$company->name = 'Washing Machines LLC';
		$this->assertTrue($company->validate(['name']));	
    }
}