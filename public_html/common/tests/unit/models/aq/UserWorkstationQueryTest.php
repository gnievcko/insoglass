<?php
namespace common\tests\unit\models\aq;

use common\fixtures\DepartmentTypeFixture;
use common\fixtures\DepartmentTypeTranslationFixture;
use common\fixtures\DepartmentFixture;
use common\fixtures\DepartmentTranslationFixture;
use common\fixtures\WorkstationFixture;
use common\fixtures\WorkstationTranslationFixture;
use common\models\aq\WorkstationQuery;
use common\fixtures\UserWorkstationFixture;
use common\models\aq\UserWorkstationQuery;

class UserWorkstationQueryTest extends \Codeception\Test\Unit {
    /**
     * @var \UnitTester
     */
    protected $tester;

    protected function _before() {
        $this->tester->haveFixtures([
            'departmentType' => [
                'class' => DepartmentTypeFixture::className(),
            ],
            'departmentTypeTranslation' => [
                'class' => DepartmentTypeTranslationFixture::className(),
            ],
            'department' => [
                'class' => DepartmentFixture::className(),
            ],
            'departmentTranslation' => [
                'class' => DepartmentTranslationFixture::className(),
            ],
            'workstation' => [
                'class' => WorkstationFixture::className(),  
            ],
            'workstationTranslation' => [
                'class' => WorkstationTranslationFixture::className(),
            ],
            'userWorkstation' => [
                'class' => UserWorkstationFixture::className(),  
            ],
        ]);
    }
    
    public function testGetUsersByWorkstationIdForWorkstationWithWorkers() {
        $actual = UserWorkstationQuery::getWorkersByWorkstationId(1);
        
        $this->assertEquals(2, count($actual), 'Worker count is incorrect');
        $this->assertEquals(3, $actual[0]['id'], 'Worker 0 id is incorrect');
        $this->assertEquals(4, $actual[1]['id'], 'Worker 1 id is incorrect');
    }
    
    public function testGetUsersByWorkstationIdForWorkstationWithoutWorkers() {
        $actual = UserWorkstationQuery::getWorkersByWorkstationId(2);
    
        $this->assertEquals(0, count($actual), 'Worker count is incorrect');
    }
}