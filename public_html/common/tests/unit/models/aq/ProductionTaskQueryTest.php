<?php
namespace common\tests\unit\models\aq;

use Yii;
use common\fixtures\DepartmentTypeFixture;
use common\fixtures\DepartmentTypeTranslationFixture;
use common\fixtures\DepartmentFixture;
use common\fixtures\DepartmentTranslationFixture;
use common\fixtures\DepartmentActivityFixture;
use common\fixtures\ActivityTranslationFixture;
use common\fixtures\ActivityFixture;
use common\fixtures\OfferedProductTranslationFixture;
use common\fixtures\OfferedProductFixture;
use common\models\aq\ProductionTaskQuery;
use common\tests\_support\TestHelper;
use common\tests\_support\ProjectHelper;
use common\helpers\Utility;
use common\models\aq\TaskPriorityQuery;
use common\models\aq\TaskTypeQuery;
use common\models\ar\User;
use common\models\ar\Task;
use common\models\ar\ProductionTask;
use common\fixtures\TaskFixture;
use common\fixtures\ProductionTaskFixture;
use yii\db\Query;
use common\models\ar\OrderPriority;

class ProductionTaskQueryTest extends \Codeception\Test\Unit {
    /**
     * @var \UnitTester
     */
    protected $tester;
    protected $helper;
    protected $projectHelper;

    protected function _before() {
        $this->tester->haveFixtures([
            'departmentType' => [
                'class' => DepartmentTypeFixture::className(),
            ],
            'departmentTypeTranslation' => [
                'class' => DepartmentTypeTranslationFixture::className(),
            ],
            'department' => [
                'class' => DepartmentFixture::className(),
            ],
            'departmentTranslation' => [
                'class' => DepartmentTranslationFixture::className(),
            ],
            'offeredProduct' => [
                'class' => OfferedProductFixture::className(),
            ],
            'offeredProductTranslation' => [
                'class' => OfferedProductTranslationFixture::className(),
            ],
            'activity' => [
                'class' => ActivityFixture::className(),
            ],
            'activityTranslation' => [
                'class' => ActivityTranslationFixture::className(),
            ],
            'departmentActivity' => [
                'class' => DepartmentActivityFixture::className(),
            ],
            'task' => [
                'class' => TaskFixture::className(),  
            ],
            'productionTask' => [
                'class' => ProductionTaskFixture::className(),  
            ],
        ]);
        
        $this->helper = new TestHelper();
        $this->projectHelper = new ProjectHelper();
    }
    
    public function testGetOngoingProductedProductsAtStart() {
        $user = User::findOne(2);
        Yii::$app->user->login($user);
        
        $orders = $this->generateOrders();
        $this->generateTasksForSimpleGeneralPath($orders);

        $actual = ProductionTaskQuery::getOngoingProductedProducts()->all();
        
        try {
            $normalOrderPriorityName = OrderPriority::find()->where(['symbol' => Utility::ORDER_PRIORITY_MEDIUM])->one()->getTranslation()->name;
            
            $this->assertEquals(5, count($actual), 'Count is incorrect');
            foreach($orders as $order) {
            	foreach($order->orderOfferedProducts as $oop) {
            		$this->assertContains([
            			'id' => $order->id,
            			'customerName' => $order->company->name,
            			'orderNumber' => $order->number,
            			'productCode' => $oop->offeredProduct->symbol,
            			'deadline' => null,
            			'completedCount' => '0/'.$oop->count,
            			'priority' => $normalOrderPriorityName,
            			'orderOfferedProductId' => $oop->id
            		], $actual, 'One of expected entries cannot be found');
            	}
            }
        }
        catch(\Exception $e) {
            $this->fail($e->getMessage());
        }
    }
    
    public function testGetOngoingProductedProductsForPathWithManyStartingPoints() {
        $user = User::findOne(2);
        Yii::$app->user->login($user);
    
        $orders = $this->generateOrders();
        $this->generateTasksForPathWithManyStartingPoints($orders);
    
        $actual = ProductionTaskQuery::getOngoingProductedProducts()->all();
    
        try {
            $normalOrderPriorityName = OrderPriority::find()->where(['symbol' => Utility::ORDER_PRIORITY_MEDIUM])->one()->getTranslation()->name;
            
            $this->assertEquals(5, count($actual), 'Count is incorrect');
            foreach($orders as $order) {
                foreach($order->orderOfferedProducts as $oop) {
                    $this->assertContains([
                        'id' => $order->id,
                        'customerName' => $order->company->name,
                        'orderNumber' => $order->number,
                        'productCode' => $oop->offeredProduct->symbol,
                        'deadline' => null,
                        'completedCount' => '0/'.$oop->count,
                        'priority' => $normalOrderPriorityName,
                        'orderOfferedProductId' => $oop->id
                    ], $actual, 'One of expected entries cannot be found');
                }
            }
        }
        catch(\Exception $e) {
            $this->fail($e->getMessage());
        }
    }
    
    public function testGetOngoingProductedProductsForPathWithManyEndPoints() {
        $user = User::findOne(2);
        Yii::$app->user->login($user);
    
        $orders = $this->generateOrders();
        $this->generateTasksForPathWithManyEndPoints($orders);
    
        $actual = ProductionTaskQuery::getOngoingProductedProducts()->all();
    
        try {
            $normalOrderPriorityName = OrderPriority::find()->where(['symbol' => Utility::ORDER_PRIORITY_MEDIUM])->one()->getTranslation()->name;
            
            $this->assertEquals(5, count($actual), 'Count is incorrect');
            foreach($orders as $order) {
                foreach($order->orderOfferedProducts as $oop) {
                    $this->assertContains([
                        'id' => $order->id,
                        'customerName' => $order->company->name,
                        'orderNumber' => $order->number,
                        'productCode' => $oop->offeredProduct->symbol,
                        'deadline' => null,
                        'completedCount' => '0/'.$oop->count,
                        'priority' => $normalOrderPriorityName,
                        'orderOfferedProductId' => $oop->id
                    ], $actual, 'One of expected entries cannot be found');
                }
            }
        }
        catch(\Exception $e) {
            $this->fail($e->getMessage());
        }
    }
    
    public function testGetOngoingProductedProductsWithOneCompleted() {
        $user = User::findOne(2);
        Yii::$app->user->login($user);
    
        $orders = $this->generateOrders();
        $this->generateTasksForSimpleGeneralPath($orders);
        
        $task = $orders[0]->orderOfferedProducts[0]->productionTasks[0]->task;
        $task->is_complete = 1;
        $task->save();
        $productionTask = $orders[0]->orderOfferedProducts[0]->productionTasks[0];
        $productionTask->total_count_made = $orders[0]->orderOfferedProducts[0]->count;
        $productionTask->save();
        $productionTask = $orders[0]->orderOfferedProducts[0]->productionTasks[1];
        $productionTask->is_available = 1;
        $productionTask->save();
    
        $actual = ProductionTaskQuery::getOngoingProductedProducts()->all();
    
        try {
            $normalOrderPriorityName = OrderPriority::find()->where(['symbol' => Utility::ORDER_PRIORITY_MEDIUM])->one()->getTranslation()->name;
    
            $this->assertEquals(5, count($actual), 'Count is incorrect');
            foreach($orders as $order) {
                foreach($order->orderOfferedProducts as $oop) {
                    $this->assertContains([
                        'id' => $order->id,
                        'customerName' => $order->company->name,
                        'orderNumber' => $order->number,
                        'productCode' => $oop->offeredProduct->symbol,
                        'deadline' => null,
                        'completedCount' => '0/'.$oop->count,
                        'priority' => $normalOrderPriorityName,
                        'orderOfferedProductId' => $oop->id
                    ], $actual, 'One of expected entries cannot be found');
                }
            }
        }
        catch(\Exception $e) {
            $this->fail($e->getMessage());
        }
    }
    
    public function testGetOngoingProductedProductsWithOneIncompleted() {
        $user = User::findOne(2);
        Yii::$app->user->login($user);
    
        $orders = $this->generateOrders();
        $this->generateTasksForSimpleGeneralPath($orders);
    
        $task = $orders[0]->orderOfferedProducts[0]->productionTasks[0]->task;
        $task->is_complete = 1;
        $task->save();
        $productionTask = $orders[0]->orderOfferedProducts[0]->productionTasks[0];
        $productionTask->total_count_made = $orders[0]->orderOfferedProducts[0]->count - 1;
        $productionTask->save();
        $productionTask = $orders[0]->orderOfferedProducts[0]->productionTasks[1];
        $productionTask->is_available = 1;
        $productionTask->save();
    
        $actual = ProductionTaskQuery::getOngoingProductedProducts()->all();
    
        try {
            $normalOrderPriorityName = OrderPriority::find()->where(['symbol' => Utility::ORDER_PRIORITY_MEDIUM])->one()->getTranslation()->name;
    
            $this->assertEquals(5, count($actual), 'Count is incorrect');
            foreach($orders as $order) {
                foreach($order->orderOfferedProducts as $oop) {
                    $this->assertContains([
                        'id' => $order->id,
                        'customerName' => $order->company->name,
                        'orderNumber' => $order->number,
                        'productCode' => $oop->offeredProduct->symbol,
                        'deadline' => null,
                        'completedCount' => '0/'.$oop->count,
                        'priority' => $normalOrderPriorityName,
                        'orderOfferedProductId' => $oop->id
                    ], $actual, 'One of expected entries cannot be found');
                }
            }
        }
        catch(\Exception $e) {
            $this->fail($e->getMessage());
        }
    }
    
    public function testGetOngoingProductedProductsWithOneAlmostFullyCompletedForSortingByCompletedCount() {
        $user = User::findOne(2);
        Yii::$app->user->login($user);
    
        $orders = $this->generateOrders();
        $this->generateTasksForSimpleGeneralPath($orders);
    
        $task = $orders[0]->orderOfferedProducts[0]->productionTasks[0]->task;
        $task->is_complete = 1;
        $task->save();
        $task = $orders[0]->orderOfferedProducts[0]->productionTasks[1]->task;
        $task->is_complete = 1;
        $task->save();
        $productionTask = $orders[0]->orderOfferedProducts[0]->productionTasks[0];
        $productionTask->total_count_made = $orders[0]->orderOfferedProducts[0]->count;
        $productionTask->save();
        $productionTask = $orders[0]->orderOfferedProducts[0]->productionTasks[1];
        $productionTask->total_count_made = $orders[0]->orderOfferedProducts[0]->count;
        $productionTask->is_available = 1;
        $productionTask->save();
        $productionTask = $orders[0]->orderOfferedProducts[0]->productionTasks[2];
        $productionTask->total_count_made = $orders[0]->orderOfferedProducts[0]->count - 1;
        $productionTask->is_available = 1;
        $productionTask->save();
        
        $concernedProductId = $orders[0]->orderOfferedProducts[0]->id;
    
        $actual = ProductionTaskQuery::getOngoingProductedProducts([
                    'sortField' => 'completedCount',
                    'sortDir' => SORT_ASC,
                ])->all();
    
        try {
            $normalOrderPriorityName = OrderPriority::find()->where(['symbol' => Utility::ORDER_PRIORITY_MEDIUM])->one()->getTranslation()->name;
    
            $this->assertEquals(5, count($actual), 'Count is incorrect');
            foreach($orders as $order) {
                foreach($order->orderOfferedProducts as $oop) {
                    $this->assertContains([
                        'id' => $order->id,
                        'customerName' => $order->company->name,
                        'orderNumber' => $order->number,
                        'productCode' => $oop->offeredProduct->symbol,
                        'deadline' => null,
                        'completedCount' => $oop->id == $concernedProductId ? '1/'.$oop->count : '0/'.$oop->count,
                        'priority' => $normalOrderPriorityName,
                        'orderOfferedProductId' => $oop->id
                    ], $actual, 'One of expected entries cannot be found');
                }
            }
        }
        catch(\Exception $e) {
            $this->fail($e->getMessage());
        }
    }
    
    private function generateOrders() {
        $order1 = $this->projectHelper->generateOrder(Utility::ORDER_STATUS_IN_PROGRESS);
        $order1 = $this->projectHelper->assignOfferedProduct($order1, 1);
        $order1 = $this->projectHelper->assignOfferedProduct($order1, 3);
        $order2 = $this->projectHelper->generateOrder(Utility::ORDER_STATUS_IN_PROGRESS);
        $order2 = $this->projectHelper->assignOfferedProduct($order2, 1);
        $order2 = $this->projectHelper->assignOfferedProduct($order2, 4);
        $order2 = $this->projectHelper->assignOfferedProduct($order2, 5);
        
        return [$order1, $order2];
    }
    
    private function generateTasksForSimpleGeneralPath($orders) {
        $productionPathStepIds = [1, 7, 14];
        $taskTypeId = TaskTypeQuery::getBySymbol(Utility::TASK_TYPE_PRODUCTION);
        $taskPriorityIds = \yii\helpers\ArrayHelper::map(
                    TaskPriorityQuery::getBySymbol([Utility::TASK_PRIORITY_MEDIUM, Utility::TASK_PRIORITY_HIGH]),
                'symbol', 'id');
                 
        for($o = 0; $o < count($orders); ++$o) {
            for($p = 0; $p < count($orders[$o]->orderOfferedProducts); ++$p) {
                $previousTaskId = (new Query())->select(['MAX(id)'])->from('production_task')->scalar();
                for($i = 0; $i < count($productionPathStepIds); ++$i) {
                    $task = new Task();
                    $task->task_type_id = $taskTypeId['id'];
                    $task->task_priority_id = $taskPriorityIds[Utility::TASK_PRIORITY_MEDIUM];
                    $task->user_id = Yii::$app->user->id;
                    $task->title = Yii::t('main', 'Production task');
                    $task->message = 'abc1';
                    if(!$task->save()) {
                        throw new \Exception('Task cannot be saved: '.print_r($task->getErrors(), true));
                    }

                    $prodTask = new ProductionTask();
                    $prodTask->task_id = $task->id;
                    $prodTask->order_offered_product_id = $orders[$o]->orderOfferedProducts[$p]->id;
                    $prodTask->production_path_step_id = $productionPathStepIds[$i];
                    $prodTask->task_required_ids = ($i == 0) ? null : (string)$previousTaskId;
                    $prodTask->task_successor_ids = ($i == count($productionPathStepIds) - 1) ? null : (string)($previousTaskId + 2);
                    $prodTask->is_available = ($i == 0) ? 1 : 0;
                    if(!$prodTask->save()) {
                        throw new \Exception('Production task cannot be saved: '.print_r($prodTask->getErrors(), true));
                    }

                    $previousTaskId = $prodTask->id;
                }
            }
        }
    }
    
    private function generateTasksForPathWithManyStartingPoints($orders) {
        $productionPathStepIds = [1, 7, 14];
        $taskTypeId = TaskTypeQuery::getBySymbol(Utility::TASK_TYPE_PRODUCTION);
        $taskPriorityIds = \yii\helpers\ArrayHelper::map(
                    TaskPriorityQuery::getBySymbol([Utility::TASK_PRIORITY_MEDIUM, Utility::TASK_PRIORITY_HIGH]),
                'symbol', 'id');
                 
        for($o = 0; $o < count($orders); ++$o) {
            for($p = 0; $p < count($orders[$o]->orderOfferedProducts); ++$p) {

                $previousTaskId = (new Query())->select(['MAX(id)'])->from('production_task')->scalar();
                $lastTaskId = $previousTaskId + 3;
                for($i = 0; $i < count($productionPathStepIds); ++$i) {
                    $task = new Task();
                    $task->task_type_id = $taskTypeId['id'];
                    $task->task_priority_id = $taskPriorityIds[Utility::TASK_PRIORITY_MEDIUM];
                    $task->user_id = Yii::$app->user->id;
                    $task->title = Yii::t('main', 'Production task');
                    $task->message = 'abc1';
                    if(!$task->save()) {
                        throw new \Exception('Task cannot be saved: '.print_r($task->getErrors(), true));
                    }

                    $prodTask = new ProductionTask();
                    $prodTask->task_id = $task->id;
                    $prodTask->order_offered_product_id = $orders[$o]->orderOfferedProducts[$p]->id;
                    $prodTask->production_path_step_id = $productionPathStepIds[$i];
                    $prodTask->task_required_ids = ($i <= 1) ? null : ($previousTaskId - 1).','.$previousTaskId;
                    $prodTask->task_successor_ids = ($i == count($productionPathStepIds) - 1) ? null : (string)$lastTaskId;
                    $prodTask->is_available = ($i == 0) ? 1 : 0;
                    if(!$prodTask->save()) {
                        throw new \Exception('Production task cannot be saved: '.print_r($prodTask->getErrors(), true));
                    }

                    $previousTaskId = $prodTask->id;
                }
            }
        }
    }
    
    private function generateTasksForPathWithManyEndPoints($orders) {
        $productionPathStepIds = [1, 7, 14];
        $taskTypeId = TaskTypeQuery::getBySymbol(Utility::TASK_TYPE_PRODUCTION);
        $taskPriorityIds = \yii\helpers\ArrayHelper::map(
                    TaskPriorityQuery::getBySymbol([Utility::TASK_PRIORITY_MEDIUM, Utility::TASK_PRIORITY_HIGH]),
                'symbol', 'id');
                 
        for($o = 0; $o < count($orders); ++$o) {
            for($p = 0; $p < count($orders[$o]->orderOfferedProducts); ++$p) {

                $previousTaskId = (new Query())->select(['MAX(id)'])->from('production_task')->scalar();
                for($i = 0; $i < count($productionPathStepIds); ++$i) {
                    $task = new Task();
                    $task->task_type_id = $taskTypeId['id'];
                    $task->task_priority_id = $taskPriorityIds[Utility::TASK_PRIORITY_MEDIUM];
                    $task->user_id = Yii::$app->user->id;
                    $task->title = Yii::t('main', 'Production task');
                    $task->message = 'abc1';
                    if(!$task->save()) {
                        throw new \Exception('Task cannot be saved: '.print_r($task->getErrors(), true));
                    }

                    $prodTask = new ProductionTask();
                    $prodTask->task_id = $task->id;
                    $prodTask->order_offered_product_id = $orders[$o]->orderOfferedProducts[$p]->id;
                    $prodTask->production_path_step_id = $productionPathStepIds[$i];
                    $prodTask->task_required_ids = ($i == 0) ? null : (string)$previousTaskId;
                    $prodTask->task_successor_ids = ($i > 0) ? null : ($previousTaskId + 2).','.($previousTaskId + 3);
                    $prodTask->is_available = ($i == 0) ? 1 : 0;
                    if(!$prodTask->save()) {
                        throw new \Exception('Production task cannot be saved: '.print_r($prodTask->getErrors(), true));
                    }

                    if($i == 0) {
                        $previousTaskId = $prodTask->id;
                    }
                }
            }
        }
    }
}