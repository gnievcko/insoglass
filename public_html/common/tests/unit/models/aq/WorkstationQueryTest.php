<?php
namespace common\tests\unit\models\aq;

use common\fixtures\DepartmentTypeFixture;
use common\fixtures\DepartmentTypeTranslationFixture;
use common\fixtures\DepartmentFixture;
use common\fixtures\DepartmentTranslationFixture;
use common\fixtures\WorkstationFixture;
use common\fixtures\WorkstationTranslationFixture;
use common\models\aq\WorkstationQuery;

class WorkstationQueryTest extends \Codeception\Test\Unit {
    /**
     * @var \UnitTester
     */
    protected $tester;

    protected function _before() {
        $this->tester->haveFixtures([
            'departmentType' => [
                'class' => DepartmentTypeFixture::className(),
            ],
            'departmentTypeTranslation' => [
                'class' => DepartmentTypeTranslationFixture::className(),
            ],
            'department' => [
                'class' => DepartmentFixture::className(),
            ],
            'departmentTranslation' => [
                'class' => DepartmentTranslationFixture::className(),
            ],
            'workstation' => [
                'class' => WorkstationFixture::className(),  
            ],
            'workstationTranslation' => [
                'class' => WorkstationTranslationFixture::className(),
            ]
        ]);
    }
    
    public function testGetActiveWorkstationQueryForFilterByName() {
        $params = [
            'name' => 'bra',
        ];
        
        $query = WorkstationQuery::getActiveWorkstationQuery($params);
        $actual = $query->all();
        
        $this->assertEquals(1, count($actual), 'Workstation count is incorrect');
        $this->assertEquals(1, $actual[0]['id'], 'Workstation 0 id is incorrect');
        $this->assertEquals('Machine 1', $actual[0]['symbol'], 'Workstation 0 symbol is incorrect');
        $this->assertEquals(1, $actual[0]['departmentId'], 'Workstation 0 departmentId is incorrect');
        $this->assertEquals('Obrabiarka', $actual[0]['name'], 'Workstation 0 name is incorrect');
        $this->assertEquals('Dział kontroli jakości', $actual[0]['departmentName'], 'Workstation 0 departmentName is incorrect');
    }
    
    public function testGetActiveWorkstationQueryForFilterByDepartmentId() {
        $params = [
            'departmentId' => [ 1 ],
        ];
    
        $query = WorkstationQuery::getActiveWorkstationQuery($params);
        $actual = $query->all();
    
        $this->assertEquals(1, count($actual), 'Workstation count is incorrect');
        $this->assertEquals(1, $actual[0]['id'], 'Workstation 0 id is incorrect');
        $this->assertEquals('Machine 1', $actual[0]['symbol'], 'Workstation 0 symbol is incorrect');
        $this->assertEquals(1, $actual[0]['departmentId'], 'Workstation 0 departmentId is incorrect');
        $this->assertEquals('Obrabiarka', $actual[0]['name'], 'Workstation 0 name is incorrect');
        $this->assertEquals('Dział kontroli jakości', $actual[0]['departmentName'], 'Workstation 0 departmentName is incorrect');
    }
    
    public function testGetActiveWorkstationQueryForSortByDepartmentNameAsc() {
        $params = [
            'sortDir' => 'asc',
            'sortField' => 'departmentName',
        ];
    
        $query = WorkstationQuery::getActiveWorkstationQuery($params);
        $actual = $query->all();
    
        $this->assertEquals(3, count($actual), 'Workstation count is incorrect');
        
        $this->assertEquals(2, $actual[0]['id'], 'Workstation 0 id is incorrect');
        $this->assertEquals('Machine 2', $actual[0]['symbol'], 'Workstation 0 symbol is incorrect');
        $this->assertEquals(2, $actual[0]['departmentId'], 'Workstation 0 departmentId is incorrect');
        $this->assertEquals('Frezarka', $actual[0]['name'], 'Workstation 0 name is incorrect');
        $this->assertEquals('Dział graficzny', $actual[0]['departmentName'], 'Workstation 0 departmentName is incorrect');
        
        $this->assertEquals(1, $actual[1]['id'], 'Workstation 1 id is incorrect');
        $this->assertEquals('Machine 1', $actual[1]['symbol'], 'Workstation 1 symbol is incorrect');
        $this->assertEquals(1, $actual[1]['departmentId'], 'Workstation 1 departmentId is incorrect');
        $this->assertEquals('Obrabiarka', $actual[1]['name'], 'Workstation 1 name is incorrect');
        $this->assertEquals('Dział kontroli jakości', $actual[1]['departmentName'], 'Workstation 1 departmentName is incorrect');
        
        $this->assertEquals(3, $actual[2]['id'], 'Workstation 2 id is incorrect');
        $this->assertEquals('Machine 3', $actual[2]['symbol'], 'Workstation 2 symbol is incorrect');
        $this->assertEquals(3, $actual[2]['departmentId'], 'Workstation 2 departmentId is incorrect');
        $this->assertEquals('Nożyce mechaniczne', $actual[2]['name'], 'Workstation 2 name is incorrect');
        $this->assertEquals('Szefowie', $actual[2]['departmentName'], 'Workstation 2 departmentName is incorrect');
    }
}