<?php
namespace common\tests\_support;

use common\models\ar\Order;
use common\helpers\RandomSymbolGenerator;
use common\models\ar\OrderHistory;
use common\models\aq\OrderStatusQuery;
use common\models\ar\OrderOfferedProduct;
use common\models\ar\OrderOfferedProductAttribute;
use yii\helpers\Json;

class ProjectHelper {
    
    public function generateOrder($status) {
        $order = new Order();
        $order->company_id = 1;
        $order->entity_id = 1;
        $order->order_group_id = 1;
        $order->number = RandomSymbolGenerator::generate(Order::className(), 'number');
        $order->title = 'Zamówienie';
        $order->user_id = 2;
        $order->is_active = 1;
        if(!$order->save()) {
            $this->fail('Order cannot be generated: '.print_r($order->getErrors(), true));
        }
    
        $orderHistory = new OrderHistory();
        $orderHistory->order_id = $order->id;
        $orderHistory->user_id = 2;
        $orderHistory->order_status_id = OrderStatusQuery::getBySymbol($status)['id'];
        $orderHistory->is_client_entry = 0;
        if(!$orderHistory->save()) {
            $this->fail('OrderHistory cannot be generated: '.print_r($orderHistory->getErrors(), true));
        }
    
        $order->order_history_last_id = $orderHistory->id;
        if(!$order->save()) {
            $this->fail('Order2 cannot be generated: '.print_r($order->getErrors(), true));
        }
    
        return $order;
    }
    
    public function assignOfferedProduct($order, $offeredProductId, $attributeValue = [], $productionPathId = null) {
        $oop = new OrderOfferedProduct();
        $oop->order_id = $order->id;
        $oop->offered_product_id = $offeredProductId;
        $oop->order_history_id = $order->orderHistoryLast->id;
        $oop->is_active = 1;
        $oop->count = 2;
        $oop->production_path_id = $productionPathId;
        if(!$oop->save()) {
            $this->fail('Problem with saving order offered product: '.print_r($oop->getErrors(), true));
        }
    
        if(!empty($attributeValue)) {
            foreach($attributeValue as $attribute) {
                $oopa = new OrderOfferedProductAttribute();
                $oopa->order_offered_product_id = $oop->id;
                $oopa->product_attribute_type_id = $attribute['id'];
                $oopa->value = 'true';
                if(!$oopa->save()) {
                    $this->fail('Problem with saving attributes: '.print_r($oopa->getErrors(), true));
                }
            }
    
            $oop->attribute_value = Json::encode($attributeValue);
            if(!$oop->save()) {
                $this->fail('Problem with saving order offered product with attributes: '.print_r($oop->getErrors(), true));
            }
        }
    
        return $order;
    }
    
    public function getLastOrder() {
        return Order::find()->orderBy(['id' => SORT_DESC])->limit(1)->one();
    }
}