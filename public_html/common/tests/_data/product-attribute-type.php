<?php

return [
    [
        'id' => 1,
        'product_attribute_group_id' => 1,
    	'symbol' => 'feature01',
        'variable_type' => 'bool',
        'is_visible' => 1,
    ],
    [
        'id' => 2,
        'product_attribute_group_id' => 1,
        'symbol' => 'feature02',
        'variable_type' => 'bool',
        'is_visible' => 1,
    ],
    [
        'id' => 3,
        'product_attribute_group_id' => 1,
        'symbol' => 'feature03',
        'variable_type' => 'bool',
        'is_visible' => 1,
    ],
    [
        'id' => 4,
        'product_attribute_group_id' => 1,
        'symbol' => 'feature04',
        'variable_type' => 'bool',
        'is_visible' => 1,
    ],
    [
        'id' => 5,
        'symbol' => 'feature05',
        'variable_type' => 'bool',
        'is_visible' => 1,
    ],
    [
        'id' => 6,
        'symbol' => 'feature06',
        'variable_type' => 'bool',
        'is_visible' => 0,
    ],
    [
        'id' => 7,
        'product_attribute_group_id' => 2,
        'symbol' => 'feature07',
        'variable_type' => 'bool',
        'is_visible' => 1,
    ],
    [
        'id' => 8,
        'product_attribute_group_id' => 2,
        'symbol' => 'feature08',
        'variable_type' => 'bool',
        'is_visible' => 1,
    ],
    [
        'id' => 9,
        'product_attribute_group_id' => 2,
        'symbol' => 'feature09',
        'variable_type' => 'bool',
        'is_visible' => 0,
    ],
    [
        'id' => 10,
        'product_attribute_group_id' => 2,
        'symbol' => 'feature10',
        'variable_type' => 'bool',
        'is_visible' => 1,
    ],
    [
        'id' => 11,
        'product_attribute_group_id' => 2,
        'symbol' => 'feature11',
        'variable_type' => 'int',
        'is_visible' => 1,
    ],
];
