<?php

return [
    [
        'workstation_id' => 1,
        'language_id' => 1,
        'name' => 'Obrabiarka',
    ],
    [
        'workstation_id' => 2,
        'language_id' => 1,
        'name' => 'Frezarka',
    ],
    [
        'workstation_id' => 3,
        'language_id' => 1,
        'name' => 'Nożyce mechaniczne',
    ],
];
