<?php

return [
    [
        'activity_id' => 1,
        'language_id' => 1,
    	'name' => 'Czynność A',
    ],
    [
        'activity_id' => 2,
        'language_id' => 1,
    	'name' => 'Czynność B',
    ],
    [
        'activity_id' => 3,
        'language_id' => 1,
        'name' => 'Czynność C',
    ],
    [
        'activity_id' => 4,
        'language_id' => 1,
        'name' => 'Czynność D',
    ],
    [
        'activity_id' => 5,
        'language_id' => 1,
        'name' => 'Czynność E',
    ],
    [
        'activity_id' => 6,
        'language_id' => 1,
        'name' => 'Czynność F',
    ],
    [
        'activity_id' => 7,
        'language_id' => 1,
        'name' => 'Czynność G',
    ],
    [
        'activity_id' => 8,
        'language_id' => 1,
        'name' => 'Czynność H',
    ],
    [
        'activity_id' => 9,
        'language_id' => 1,
        'name' => 'Czynność I',
    ],
    [
        'activity_id' => 10,
        'language_id' => 1,
        'name' => 'Czynność J',
    ],
    [
        'activity_id' => 11,
        'language_id' => 1,
        'name' => 'Czynność K',
    ],
    [
        'activity_id' => 12,
        'language_id' => 1,
        'name' => 'Czynność M',
    ],
    [
        'activity_id' => 13,
        'language_id' => 1,
        'name' => 'Czynność L',
    ],
    [
        'activity_id' => 14,
        'language_id' => 1,
        'name' => 'Czynność N',
    ],
    [
        'activity_id' => 15,
        'language_id' => 1,
        'name' => 'Węzeł',
    ],
];
