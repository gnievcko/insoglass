<?php

return [
    [
        'offered_product_id' => 1,
        'language_id' => 1,
        'name' => 'Przykładowy 1',
    ],
    [
        'offered_product_id' => 2,
        'language_id' => 1,
        'name' => 'Przykładowy 2 - z atrybutami',
    ],
    [
        'offered_product_id' => 3,
        'language_id' => 1,
        'name' => 'Przykładowy 3 - ze ścieżką',
    ],
    [
        'offered_product_id' => 4,
        'language_id' => 1,
        'name' => 'Przykładowy 4 - ze ścieżką z wymaganymi i blokowanymi krokami',
    ],
    [
        'offered_product_id' => 5,
        'language_id' => 1,
        'name' => 'Przykładowy 5 - z wieloma punktami początkowymi',
    ],
    [
        'offered_product_id' => 6,
        'language_id' => 1,
        'name' => 'Przykładowy 6 - z wieloma punktami końcowymi',
    ],
];