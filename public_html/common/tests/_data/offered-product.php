<?php

return [
    [
        'id' => 1,
        'symbol' => 'example001',
        'user_id' => 1,
        'is_available' => 1,
        'is_active' => 1,
    ],
    [
        'id' => 2,
        'symbol' => 'example002',
        'user_id' => 1,
        'is_available' => 1,
        'is_active' => 1,
        'attribute_value' => '[{"id":2,"value":"true"},{"id":5,"value":"true"},{"id":6,"value":"true"},{"id":10,"value":"true"}]',
    ],
    [
        'id' => 3,
        'symbol' => 'example003',
        'user_id' => 1,
        'is_available' => 1,
        'is_active' => 1,
        'production_path_id' => 3,
    ],
    [
        'id' => 4,
        'symbol' => 'example004',
        'user_id' => 1,
        'is_available' => 1,
        'is_active' => 1,
        'production_path_id' => 5,
    ],
    [
        'id' => 5,
        'symbol' => 'example005',
        'user_id' => 1,
        'is_available' => 1,
        'is_active' => 1,
        'production_path_id' => 6,
    ],
    [
        'id' => 6,
        'symbol' => 'example006',
        'user_id' => 1,
        'is_available' => 1,
        'is_active' => 1,
        'production_path_id' => 7,
    ],
];