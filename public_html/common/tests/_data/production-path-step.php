<?php
return [ 
    [ 
        'id' => 1,
        'production_path_id' => 1,
        'activity_id' => 1,
        'is_requirement_type_or' => 0,
        'step_required_ids' => null,
        'step_successor_ids' => '2' 
    ],
    [ 
        'id' => 2,
        'production_path_id' => 1,
        'activity_id' => 2,
        'is_requirement_type_or' => 0,
        'step_required_ids' => '1',
        'step_successor_ids' => '3' 
    ],
    [ 
        'id' => 3,
        'production_path_id' => 1,
        'activity_id' => 3,
        'is_requirement_type_or' => 0,
        'step_required_ids' => '2',
        'step_successor_ids' => '4,5,6' 
    ],
    [ 
        'id' => 4,
        'production_path_id' => 1,
        'activity_id' => 4,
        'is_requirement_type_or' => 0,
        'step_required_ids' => '3',
        'step_successor_ids' => '7' 
    ],
    [ 
        'id' => 5,
        'production_path_id' => 1,
        'activity_id' => 5,
        'is_requirement_type_or' => 0,
        'step_required_ids' => '3',
        'step_successor_ids' => '7' 
    ],
    [ 
        'id' => 6,
        'production_path_id' => 1,
        'activity_id' => 6,
        'is_requirement_type_or' => 0,
        'step_required_ids' => '3',
        'step_successor_ids' => '7' 
    ],
    [ 
        'id' => 7,
        'production_path_id' => 1,
        'activity_id' => 7,
        'is_requirement_type_or' => 0,
        'step_required_ids' => '4,5,6',
        'step_successor_ids' => '8,9' 
    ],
    [ 
        'id' => 8,
        'production_path_id' => 1,
        'activity_id' => 8,
        'is_requirement_type_or' => 0,
        'step_required_ids' => '7',
        'step_successor_ids' => '11' 
    ],
    [ 
        'id' => 9,
        'production_path_id' => 1,
        'activity_id' => 9,
        'is_requirement_type_or' => 0,
        'step_required_ids' => '7',
        'step_successor_ids' => '10' 
    ],
    [ 
        'id' => 10,
        'production_path_id' => 1,
        'activity_id' => 10,
        'is_requirement_type_or' => 0,
        'step_required_ids' => '9',
        'step_successor_ids' => '11' 
    ],
    [ 
        'id' => 11,
        'production_path_id' => 1,
        'activity_id' => 11,
        'is_requirement_type_or' => 0,
        'step_required_ids' => '8,10',
        'step_successor_ids' => '12,13' 
    ],
    [ 
        'id' => 12,
        'production_path_id' => 1,
        'activity_id' => 12,
        'is_requirement_type_or' => 0,
        'step_required_ids' => '11',
        'step_successor_ids' => '13' 
    ],
    [ 
        'id' => 13,
        'production_path_id' => 1,
        'activity_id' => 13,
        'is_requirement_type_or' => 0,
        'step_required_ids' => '11,12',
        'step_successor_ids' => '14' 
    ],
    [ 
        'id' => 14,
        'production_path_id' => 1,
        'activity_id' => 14,
        'is_requirement_type_or' => 0,
        'step_required_ids' => '13',
        'step_successor_ids' => null 
    ],
    
    [ 
        'id' => 15,
        'production_path_id' => 2,
        'activity_id' => 1,
        'is_requirement_type_or' => 0,
        'step_required_ids' => null,
        'step_successor_ids' => '16,17' 
    ],
    [ 
        'id' => 16,
        'production_path_id' => 2,
        'activity_id' => 2,
        'is_requirement_type_or' => 0,
        'step_required_ids' => '15',
        'step_successor_ids' => '18' 
    ],
    [ 
        'id' => 17,
        'production_path_id' => 2,
        'activity_id' => 3,
        'is_requirement_type_or' => 0,
        'step_required_ids' => '15',
        'step_successor_ids' => '18' 
    ],
    [ 
        'id' => 18,
        'production_path_id' => 2,
        'activity_id' => 4,
        'is_requirement_type_or' => 0,
        'step_required_ids' => '16,17',
        'step_successor_ids' => null 
    ],
    
    [ 
        'id' => 19,
        'production_path_id' => 3,
        'activity_id' => 1,
        'is_requirement_type_or' => 0,
        'step_required_ids' => null,
        'step_successor_ids' => '20,22' 
    ],
    [ 
        'id' => 20,
        'production_path_id' => 3,
        'activity_id' => 2,
        'is_requirement_type_or' => 0,
        'step_required_ids' => '19',
        'step_successor_ids' => '21' 
    ],
    [ 
        'id' => 21,
        'production_path_id' => 3,
        'activity_id' => 3,
        'is_requirement_type_or' => 0,
        'step_required_ids' => '20',
        'step_successor_ids' => '22' 
    ],
    [ 
        'id' => 22,
        'production_path_id' => 3,
        'activity_id' => 4,
        'is_requirement_type_or' => 0,
        'step_required_ids' => '19,21',
        'step_successor_ids' => null 
    ],
    
    [ 
        'id' => 23,
        'production_path_id' => 4,
        'activity_id' => 1,
        'is_requirement_type_or' => 0,
        'step_required_ids' => null,
        'step_successor_ids' => '24' 
    ],
    [ 
        'id' => 24,
        'production_path_id' => 4,
        'activity_id' => 2,
        'is_requirement_type_or' => 0,
        'step_required_ids' => '23',
        'step_successor_ids' => '25,27' 
    ],
    [ 
        'id' => 25,
        'production_path_id' => 4,
        'activity_id' => 3,
        'is_requirement_type_or' => 0,
        'step_required_ids' => '24',
        'step_successor_ids' => '26' 
    ],
    [ 
        'id' => 26,
        'production_path_id' => 4,
        'activity_id' => 4,
        'is_requirement_type_or' => 0,
        'step_required_ids' => '25',
        'step_successor_ids' => '29' 
    ],
    [ 
        'id' => 27,
        'production_path_id' => 4,
        'activity_id' => 5,
        'is_requirement_type_or' => 0,
        'step_required_ids' => '24',
        'step_successor_ids' => '28' 
    ],
    [ 
        'id' => 28,
        'production_path_id' => 4,
        'activity_id' => 6,
        'is_requirement_type_or' => 0,
        'step_required_ids' => '27',
        'step_successor_ids' => '29' 
    ],
    [ 
        'id' => 29,
        'production_path_id' => 4,
        'activity_id' => 7,
        'is_requirement_type_or' => 0,
        'step_required_ids' => '26,28',
        'step_successor_ids' => null 
    ],
    
    [ 
        'id' => 30,
        'production_path_id' => 5,
        'activity_id' => 1,
        'is_requirement_type_or' => 0,
        'step_required_ids' => null,
        'step_successor_ids' => '31,32',
        'attribute_needed_ids' => null,
        'attribute_forbidden_ids' => null 
    ],
    [ 
        'id' => 31,
        'production_path_id' => 5,
        'activity_id' => 2,
        'is_requirement_type_or' => 0,
        'step_required_ids' => '30',
        'step_successor_ids' => '33',
        'attribute_needed_ids' => null,
        'attribute_forbidden_ids' => '2' 
    ],
    [ 
        'id' => 32,
        'production_path_id' => 5,
        'activity_id' => 3,
        'is_requirement_type_or' => 0,
        'step_required_ids' => '30',
        'step_successor_ids' => '33',
        'attribute_needed_ids' => '2,3',
        'attribute_forbidden_ids' => null 
    ],
    [ 
        'id' => 33,
        'production_path_id' => 5,
        'activity_id' => 4,
        'is_requirement_type_or' => 0,
        'step_required_ids' => '31,32',
        'step_successor_ids' => '34',
        'attribute_needed_ids' => null,
        'attribute_forbidden_ids' => null 
    ],
    [ 
        'id' => 34,
        'production_path_id' => 5,
        'activity_id' => 5,
        'is_requirement_type_or' => 0,
        'step_required_ids' => '33',
        'step_successor_ids' => null,
        'attribute_needed_ids' => null,
        'attribute_forbidden_ids' => '3' 
    ],
    
    [ 
        'id' => 35,
        'production_path_id' => 6,
        'activity_id' => 1,
        'is_requirement_type_or' => 0,
        'step_required_ids' => null,
        'step_successor_ids' => '38' 
    ],
    [ 
        'id' => 36,
        'production_path_id' => 6,
        'activity_id' => 2,
        'is_requirement_type_or' => 0,
        'step_required_ids' => null,
        'step_successor_ids' => '38' 
    ],
    [ 
        'id' => 37,
        'production_path_id' => 6,
        'activity_id' => 3,
        'is_requirement_type_or' => 0,
        'step_required_ids' => null,
        'step_successor_ids' => '41' 
    ],
    [ 
        'id' => 38,
        'production_path_id' => 6,
        'activity_id' => 15,
        'is_requirement_type_or' => 0,
        'step_required_ids' => '35,36',
        'step_successor_ids' => '39,40' 
    ],
    [ 
        'id' => 39,
        'production_path_id' => 6,
        'activity_id' => 4,
        'is_requirement_type_or' => 0,
        'step_required_ids' => '38',
        'step_successor_ids' => '41' 
    ],
    [ 
        'id' => 40,
        'production_path_id' => 6,
        'activity_id' => 5,
        'is_requirement_type_or' => 0,
        'step_required_ids' => '38',
        'step_successor_ids' => '41' 
    ],
    [ 
        'id' => 41,
        'production_path_id' => 6,
        'activity_id' => 6,
        'is_requirement_type_or' => 0,
        'step_required_ids' => '37,39,40',
        'step_successor_ids' => null 
    ], 
    [
        'id' => 42,
        'production_path_id' => 7,
        'activity_id' => 1,
        'is_requirement_type_or' => 0,
        'step_required_ids' => null,
        'step_successor_ids' => '43',
    ],
    [
        'id' => 43,
        'production_path_id' => 7,
        'activity_id' => 2,
        'is_requirement_type_or' => 0,
        'step_required_ids' => '42',
        'step_successor_ids' => '44,45',
    ],
    [
        'id' => 44,
        'production_path_id' => 7,
        'activity_id' => 3,
        'is_requirement_type_or' => 0,
        'step_required_ids' => '43',
        'step_successor_ids' => null,
    ],
    [
        'id' => 45,
        'production_path_id' => 7,
        'activity_id' => 4,
        'is_requirement_type_or' => 0,
        'step_required_ids' => '43',
        'step_successor_ids' => null,
    ],
];
