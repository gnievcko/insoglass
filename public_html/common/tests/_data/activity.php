<?php

return [
    [
        'id' => 1,
    	'symbol' => 'A',
        'is_node' => '0',
    ],
    [
        'id' => 2,
    	'symbol' => 'B',
        'is_node' => '0',
    ],
    [
        'id' => 3,
        'symbol' => 'C',
        'is_node' => '0',
    ],
    [
        'id' => 4,
        'symbol' => 'D',
        'is_node' => '0',
    ],
    [
        'id' => 5,
        'symbol' => 'E',
        'is_node' => '0',
    ],
    [
        'id' => 6,
        'symbol' => 'F',
        'is_node' => '0',
    ],
    [
        'id' => 7,
        'symbol' => 'G',
        'is_node' => '0',
    ],
    [
        'id' => 8,
        'symbol' => 'H',
        'is_node' => '0',
    ],
    [
        'id' => 9,
        'symbol' => 'I',
        'is_node' => '0',
    ],
    [
        'id' => 10,
        'symbol' => 'J',
        'is_node' => '0',
    ],
    [
        'id' => 11,
        'symbol' => 'K',
        'is_node' => '0',
    ],
    [
        'id' => 12,
        'symbol' => 'M',
        'is_node' => '0',
    ],
    [
        'id' => 13,
        'symbol' => 'L',
        'is_node' => '0',
    ],
    [
        'id' => 14,
        'symbol' => 'N',
        'is_node' => '0',
    ],
    [
        'id' => 15,
        'symbol' => 'node',
        'is_node' => 1,
    ],
];
