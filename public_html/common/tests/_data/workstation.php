<?php

return [
    [
        'id' => 1,
    	'department_id' => 1,
    	'symbol' => 'Machine 1',
    ],
    [
        'id' => 2,
        'department_id' => 2,
        'symbol' => 'Machine 2',
    ],
    [
        'id' => 3,
        'department_id' => 3,
        'symbol' => 'Machine 3',
    ],
];
