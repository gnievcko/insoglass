<?php

return [
    [
        'id' => 2,
        'symbol' => 'group01',
        'is_predefined' => 0,
    ],
    [
        'id' => 3,
        'symbol' => 'group02woTrans',
        'is_predefined' => 0,
    ],
    [
        'id' => 4,
        'symbol' => 'group03withClients',
        'is_predefined' => 0,
    ],
];