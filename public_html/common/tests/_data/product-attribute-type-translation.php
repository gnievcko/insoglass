<?php

return [
    [
        'product_attribute_type_id' => 1,
        'language_id' => 1,
        'name' => 'Programowanie',
    ],
    [
        'product_attribute_type_id' => 2,
        'language_id' => 1,
        'name' => 'Projektowanie',
    ],
    [
        'product_attribute_type_id' => 3,
        'language_id' => 1,
        'name' => 'Weryfikacja',
    ],
    [
        'product_attribute_type_id' => 4,
        'language_id' => 1,
        'name' => 'Walidacja',
    ],
    [
        'product_attribute_type_id' => 5,
        'language_id' => 1,
        'name' => 'Złożenie wniosku',
    ],
    [
        'product_attribute_type_id' => 6,
        'language_id' => 1,
        'name' => 'Sprzątanie',
    ],
    [
        'product_attribute_type_id' => 7,
        'language_id' => 1,
        'name' => 'Marketing',
    ],
    [
        'product_attribute_type_id' => 8,
        'language_id' => 1,
        'name' => 'Obsługa serwisów',
    ],
    [
        'product_attribute_type_id' => 9,
        'language_id' => 1,
        'name' => 'Obsługa klienta',
    ],
    [
        'product_attribute_type_id' => 10,
        'language_id' => 1,
        'name' => 'SEO',
    ],
    [
        'product_attribute_type_id' => 11,
        'language_id' => 1,
        'name' => 'Tłumaczenie',
    ],
];