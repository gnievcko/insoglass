<?php

return [
    [
        'id' => 1,
    	'symbol' => 'general',
        'topological_order' => '1,2,3,4,5,6,7,8,9,10,11,12,13,14',
        'is_default' => 1,
    ],
    [
        'id' => 2,
        'symbol' => 'only_parallel',
        'topological_order' => '15,16,17,18',
        'is_default' => 0,
    ],
    [
        'id' => 3,
        'symbol' => 'parallel_long_side',
        'topological_order' => '19,20,21,22',
        'is_default' => 0,
    ],
    [
        'id' => 4,
        'symbol' => 'parallel_double_series_pairs',
        'topological_order' => '23,24,25,27,26,28,29',
        'is_default' => 0,
    ],
    [
        'id' => 5,
        'symbol' => 'attribute_dependencies',
        'topological_order' => '30,31,32,33,34',
        'is_default' => 0,
    ],
    [
        'id' => 6,
        'symbol' => 'many_starting_points',
        'topological_order' => '35,36,37,38,39,40,41',
        'is_default' => 0,
    ],
    [
        'id' => 7,
        'symbol' => 'many_end_points',
        'topological_order' => '42,43,44,45',
        'is_default' => 0,
    ],
];
