<?php

return [
    [
        'id' => 2,
        'email' => 'jakub.rojek@mindseater.com',
        'is_active' => 1,
    ],    
    [
        'id' => 3,
        'email' => 'worker1@mindseater.com',
        'is_active' => 1,
    ],  
    [
        'id' => 4,
        'email' => 'worker2@mindseater.com',
        'is_active' => 1,
    ],
];