<?php
return [ 
    [ 
        'production_path_step_id' => 2,
        'product_attribute_type_id' => 1 
    ],
    [ 
        'production_path_step_id' => 3,
        'product_attribute_type_id' => 2 
    ],
    [ 
        'production_path_step_id' => 4,
        'product_attribute_type_id' => 3 
    ],
    [ 
        'production_path_step_id' => 5,
        'product_attribute_type_id' => 4 
    ],
    [ 
        'production_path_step_id' => 6,
        'product_attribute_type_id' => 5 
    ],
    [ 
        'production_path_step_id' => 8,
        'product_attribute_type_id' => 6 
    ],
    [ 
        'production_path_step_id' => 9,
        'product_attribute_type_id' => 7 
    ],
    [ 
        'production_path_step_id' => 10,
        'product_attribute_type_id' => 7 
    ],
    [ 
        'production_path_step_id' => 11,
        'product_attribute_type_id' => 1 
    ],
    [ 
        'production_path_step_id' => 11,
        'product_attribute_type_id' => 8 
    ],
    [ 
        'production_path_step_id' => 12,
        'product_attribute_type_id' => 9 
    ],
    [ 
        'production_path_step_id' => 13,
        'product_attribute_type_id' => 10 
    ],
    
    [
        'production_path_step_id' => 25,
        'product_attribute_type_id' => 1
    ],
    [
        'production_path_step_id' => 26,
        'product_attribute_type_id' => 2
    ],
    [
        'production_path_step_id' => 27,
        'product_attribute_type_id' => 3
    ],
    [
        'production_path_step_id' => 28,
        'product_attribute_type_id' => 4
    ],
    [
        'production_path_step_id' => 31,
        'product_attribute_type_id' => 1,
    ],
    [
        'production_path_step_id' => 31,
        'product_attribute_type_id' => 3,
    ],
    [
        'production_path_step_id' => 32,
        'product_attribute_type_id' => 2,
    ],
    [
        'production_path_step_id' => 32,
        'product_attribute_type_id' => 3,
    ],
    [
        'production_path_step_id' => 33,
        'product_attribute_type_id' => 3,
    ],
    [
        'production_path_step_id' => 34,
        'product_attribute_type_id' => 2,
    ],
    [
        'production_path_step_id' => 45,
        'product_attribute_type_id' => 1,
    ],
];