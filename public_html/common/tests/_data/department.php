<?php

return [
    [
        'id' => 1,
    	'department_type_id' => 1,
    	'symbol' => 'QA Team',
    ],
    [
        'id' => 2,
        'department_type_id' => 1,
        'symbol' => 'Art',
    ],
    [
        'id' => 3,
        'department_type_id' => 1,
        'symbol' => 'Chiefs',
    ],
];
