<?php

return [
    [
        'id' => 1,
        'name' => 'Client',
        'address_id' => 1,
        'taxpayer_identification_number' => '',
        'vat_identification_number' => '',
        'krs_number' => '',
        'email' => '',
        'phone' => '',
        'account_number' => '',
        'bank_name' => '',
        'is_active' => 1,
    ],
];
