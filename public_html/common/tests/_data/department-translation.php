<?php

return [
    [
        'department_id' => 1,
    	'language_id' => 1,
    	'name' => 'Dział kontroli jakości',
    ],
    [
        'department_id' => 2,
        'language_id' => 1,
        'name' => 'Dział graficzny',
    ],
    [
        'department_id' => 3,
        'language_id' => 1,
        'name' => 'Szefowie',
    ],
];
