<?php

namespace common\helpers;

class Utility {

    const ROLE_ADMIN = 'admin';
    const ROLE_CLIENT = 'client';
    const ROLE_SALESMAN = 'salesman';
    const ROLE_STOREKEEPER = 'storekeeper';
    const ROLE_MAIN_STOREKEEPER = 'main_storekeeper';
    const ROLE_MAIN_SALESMAN = 'main_salesman';
    const ROLE_REPAIRER = 'repairer';
    const ROLE_WORKER = 'worker';
    const ROLE_QA_SPECIALIST = 'qa_specialist';
    const ROLE_MAIN_WORKER = 'main_worker';
    const ROLE_CONTACT_PERSON = 'contact_person';
    const ROLE_ARTIST = 'artist';
    const ROLE_PRODUCER = 'producer';
    
    const TOKEN_TYPE_RESET_PASSWORD = 'reset_password';
    const TOKEN_TYPE_CUSTOMER_ACTIVATE_ACCOUNT = 'customer_activate_account';
    
    const EMAIL_TYPE_WELCOME_WITH_PASSWORD = 'email_welcome_with_password';
    const EMAIL_TYPE_WELCOME_VERIFICATION = 'email_welcome_verification';
    const EMAIL_TYPE_RESET_PASSWORD = 'email_reset_password';
    const EMAIL_TYPE_CONTACT = 'email_contact';
    const EMAIL_TYPE_CUSTOMER_NEW_USER = 'email_customer_new_user';
    const EMAIL_TYPE_CUSTOMER_NEW_USER_NEW_COMPANY = 'email_customer_new_user_new_comp';
    const EMAIL_TYPE_CUSTOMER_NEW_USER_EXISTING_COMPANY = 'email_customer_new_user_existing';
    const EMAIL_TYPE_CUSTOMER_NEW_ACTIVATION_LINK = 'email_customer_new_activation_li';
    
    const ONE_DAY = 86400;
    const SUGGESTIONS_LIMIT = 3;
    const MAX_DESCRIPTION_LENGTH = 80;
    const SLIDER_DEFAULT = 'default';
    
    const COMPANY_GROUP_DEFAULT = 'all_default_group';
    
    const CURRENCY_PLN = 'PLN';
    
    const PRODUCT_TYPE_PRODUCT = 'product';
    const PRODUCT_TYPE_OFFERED_PRODUCT = 'offered-product';
    
    const PRODUCT_STATUS_ADDITION = 'addition';
    const PRODUCT_STATUS_RESERVATION = 'reservation';
    const PRODUCT_STATUS_ISSUING = 'issuing';
    const PRODUCT_STATUS_RETURN = 'return';
    const PRODUCT_STATUS_UTILIZATION = 'utilization';
    const PRODUCT_STATUS_UNDO_RESERVATION = 'undo_reservation';
    const PRODUCT_STATUS_ISSUING_RESERVATION = 'issuing_reservation';
    
    const ORDER_STATUS_NEW = 'new';
    const ORDER_STATUS_SENT_TO_CLIENT = 'sent_to_client';
    const ORDER_STATUS_ACCEPTED_BY_CLIENT = 'accepted_by_client';
    const ORDER_STATUS_REJECTED_BY_CLIENT = 'rejected_by_client';
    const ORDER_STATUS_DEFERRED = 'deferred';
    const ORDER_STATUS_IN_PROGRESS = 'in_progress';
    const ORDER_STATUS_SUSPENDED = 'suspended';
    const ORDER_STATUS_COMPLETED = 'completed';
    const ORDER_STATUS_INVOICED = 'invoiced';
    const ORDER_STATUS_FINISHED = 'finished';
    const ORDER_STATUS_CANCELLED = 'cancelled';
    
    const ORDER_TYPE_ALL = 'all';
    const ORDER_TYPE_OPEN = 'open';
    const ORDER_TYPE_ARCHIVAL = 'archival';
    const ORDER_TYPE_TEMPLATE = 'template';
    
    const CLIENT_REPRESENTATIVE_ALL = 'all';
    const CLIENT_REPRESENTATIVE_LOG_PERMISSION_ONLY = 'logPermissionOnly';
    const CLIENT_REPRESENTATIVE_LOG_PROHIBITION_ONLY = 'logProhibitionOnly';
    
    const ACTION_CREATE = 'create';
    const ACTION_EDIT = 'edit';
    const ACTION_DELETE = 'delete';
    
    const NUMBER_TEMPLATE_ORDER = 'order';
    const NUMBER_TEMPLATE_GOODS_RECEIVED = 'goods_received';
    const NUMBER_TEMPLATE_GOODS_ISSUE = 'goods_issue';
    const NUMBER_TEMPLATE_INVOICE = 'invoice';
    const NUMBER_TEMPLATE_PROFORMA_INVOICE = 'proforma_invoice';
    const NUMBER_TEMPLATE_ADVANCE_INVOICE = 'advance_invoice';
    const NUMBER_TEMPLATE_CORRECTIVE_INVOICE = 'corrective_invoice';
    
    const TASK_TYPE_ASSOCIATED_WITH_ORDER = 'associated_with_order';
    const TASK_TYPE_CLIENT_CONTACT = 'client_contact';
    const TASK_TYPE_ORDER_PRODUCT = 'order_product';
    const TASK_TYPE_OTHER = 'other';
    const TASK_TYPE_PRODUCTION = 'production';
    
    const TASK_PRIORITY_HIGH = 'high';
    const TASK_PRIORITY_MEDIUM = 'medium';
    const TASK_PRIORITY_LOW = 'low';
    
    const PARENT_COMPANY = 'parent_company';
    const CHILD_COMPANY = 'child_company';
    
    const CONTRACT_TYPE_CONTRACT = 'contract';
    const CONTRACT_TYPE_ORDER = 'order';
    const CONTRACT_TYPE_MALFUNCTION_REQUEST = 'malfunction_request';
    
    const ORDER_TYPE_MALFUNCTION_FIXING = 'malfunction_fixing';
    const ORDER_TYPE_FIRE_GATE_REPAIR = 'fire_gate_repair';
    const ORDER_TYPE_ITEM_DELIVERY = 'item_delivery';
    const ORDER_TYPE_ITEM_REPLACE = 'item_replace';
    const ORDER_TYPE_SERVICE = 'service';
    
    const ADDITIONAL_FIELD_TYPE_SELECT2 = 'select2';
    const ADDITIONAL_FIELD_TYPE_TEXT_INPUT = 'textInput';
    
    const INFORMATION_PAGE_DEFAULT_DESCRIPTION = 'default_description';
    const INFORMATION_PAGE_DEFAULT_DESCRIPTION_NOTE = 'default_description_note';
    const INFORMATION_PAGE_DEFAULT_DESCRIPTION_CONT = 'default_description_cont';
    const INFORMATION_PAGE_DEFAULT_SERVICE_ADDITIONAL_TEXT = 'default_service_additional_text';
    const INFORMATION_PAGE_DEFAULT_SERVICE_TABLES = 'default_service_tables';
    const INFORMATION_PAGE_DEFAULT_REPAIR_ADDITIONAL_TEXT = 'default_repair_additional_text';
    const INFORMATION_PAGE_DEFAULT_SERVICE_FDAFAS_TABLES = 'default_service_fdafas_tables';
    const INFORMATION_PAGE_DEFAULT_WEIGHT_TABLES = 'default_weight_tables';
    const INFORMATION_PAGE_DEFAULT_WEIGHT_ADDITIONAL_TEXT = 'default_weight_additional_text';
    const INFORMATION_PAGE_DEFAULT_WEIGHT_KEY = 'default_weight_key';
    
    const PROTOCOL_COPY_TABLE_WITHOUT_MODIFICATION = 0;
    const PROTOCOL_COPY_TABLE_WITH_ADDITION_ONE_COLUMN = 1;
    const PROTOCOL_COPY_TABLE_WITH_REMOVING_ONE_COLUMN = 2;
    
    const ORDER_PRIORITY_MEDIUM = 'medium';
    const ORDER_PRIORITY_HIGH = 'high';
    const DATE_DEADLINE = 'date_deadline';
    const DATE_SHIPPING = 'date_shipping';

    const PRODUCT_ATTRIBUTE_TYPE_VARIABLE_TYPE_INT = 'int';
    const PRODUCT_ATTRIBUTE_TYPE_VARIABLE_TYPE_BOOL = 'bool';
    
    const QA_FACTOR_TYPE_VARIABLE_TYPE_RANGE_15 = 'range_1-5';
    const QA_FACTOR_TYPE_VARIABLE_TYPE_TEXT = 'text';

    const RECTANGLE = 'Rectangle';
    const TRIANGLE = 'Triangle';
    const RIGHT_RIGHT_TRAPEZE = 'RightRightTrapeze';
    const TOP_RIGHT_TRAPEZE = 'TopRightTrapeze';
    const TRIPLE_RIGHT_PENTAGON = 'TripleRightPentagon';
    const ISOSCELES_TRIANGLE = 'IsoscelesTriangle';
    const TRAPEZE = 'Trapeze';
    const CUSTOM_TRIANGLE = 'CustomTriangle';
    const RHOMBUS = 'Rhombus';
    const TRAPEZOID_LEANING = 'TrapezoidLeaning';
    const QUADRANGLE = 'Quadrangle';
    const RIGHT_QUADRANGLE_WARPED = 'RightQuadrangleWarped';
    const RIGHT_QUADRANGLE = 'RightQuadrangle';
    const RIGHT_QUADRANGLE_FLATTENED = 'RightQuadrangleFlattened';
    const DOUBLE_RIGHT_PENTAGON = 'DoubleRightPentagon';
    const QUADRANGLE_FLATTENED = 'QuadrangleFlattened';
    const TRIPLE_IRREGULAR_QUADRANGLE = 'TripleIrregularQuadrangle';
    const QUADRANGLE_WARPED = 'QuadrangleWarped';
    const DOUBLE_PITCH_PENTAGON = 'DoublePitchPentagon';
    const SINGLE_RIGHT_PENTAGON = 'SingleRightPentagon';
    const SINGLE_RIGHT_PENTAGON_WARPED = 'SingleRightPentagonWarped';
    const DOUPLE_RIGHT_HEXAGON = 'DoupleRightHexagon';
    const QUADRANGLE_WARPED_IN_LEFT = 'QuadrangleWarpedInLeft';
    const SYMMETRICAL_PENTAGON = 'SymmetricalPentagon';
    const DOUBLE_RIGHT_HEXAGON_WRAPED = 'DoubleRightHexagonWraped';
    const SYMMETRICAL_ONE_AXIS_HEXAGON = 'SymmetricalOneAxisHexagon';
    const SYMMETRICAL_ONE_AXIS_OCTAGON = 'SymmetricalOneAxisOctagon';
    const CIRCLE = 'Circle';
    const ELLIPSE = 'Ellipse';
    const SLICE_CIRCLE = 'SliceCircle';
    const SLICE_RING = 'SliceRing';
    const IRREGULAR_SLICE_CIRCLE = 'IrregularSliceCircle';
    const SYMMETRICAL_FRAME_WITH_CIRCULAR_ARC = 'SymmetricalFrameWithCircularArc';
    const FRAME_WITH_CIRCULAR_ARC = 'FrameWithCircularArc';
    const INDEFINITE_SHAPE = 'IndefiniteShape';

    public static function getShapes() {
        return [
            self::RECTANGLE,
            self::TRIANGLE,
            self::RIGHT_RIGHT_TRAPEZE,
            self::TOP_RIGHT_TRAPEZE,
            self::TRIPLE_RIGHT_PENTAGON,
            self::ISOSCELES_TRIANGLE,
            self::TRAPEZE,
            self::CUSTOM_TRIANGLE,
            self::RHOMBUS,
            self::TRAPEZOID_LEANING,
            self::QUADRANGLE,
            self::RIGHT_QUADRANGLE_WARPED,
            self::RIGHT_QUADRANGLE,
            self::RIGHT_QUADRANGLE_FLATTENED,
            self::DOUBLE_RIGHT_PENTAGON,
            self::QUADRANGLE_FLATTENED,
            self::TRIPLE_IRREGULAR_QUADRANGLE,
            self::QUADRANGLE_WARPED,
            self::DOUBLE_PITCH_PENTAGON,
            self::SINGLE_RIGHT_PENTAGON,
            self::SINGLE_RIGHT_PENTAGON_WARPED,
            self::DOUPLE_RIGHT_HEXAGON,
            self::QUADRANGLE_WARPED_IN_LEFT,
            self::SYMMETRICAL_PENTAGON,
            self::DOUBLE_RIGHT_HEXAGON_WRAPED,
            self::SYMMETRICAL_ONE_AXIS_HEXAGON,
            self::SYMMETRICAL_ONE_AXIS_OCTAGON,
            self::CIRCLE,
            self::ELLIPSE,
            self::SLICE_CIRCLE,
            self::SLICE_RING,
            self::IRREGULAR_SLICE_CIRCLE,
            self::SYMMETRICAL_FRAME_WITH_CIRCULAR_ARC,
            self::FRAME_WITH_CIRCULAR_ARC,
            self::INDEFINITE_SHAPE,
        ];
    }

}

?>
