<?php
namespace common\helpers;

class ArrayHelper {
	
	public static function searchArrayForKey($array, $key) {
		$value = null;
		if(!empty($array)) {
			foreach($array as $k => $v) {
				if(is_array($v)) {
					$result = self::searchArrayForKey($v, $key);
					if(!empty($result)) {
						return $result;
					}
				}
				elseif($k == $key) {
					$value = $v;
				}
			}
		}
		 
		return $value;
	}
	
	public static function searchAssociativeArrayForValue($array, $key, $value) {
		if(empty($array)) {
			return null;
		}
		
		$indices = [];
		for($i = 0; $i < count($array); ++$i) {
			foreach($array[$i] as $k => $v) {
				if($k == $key && $v == $value) {
					$indices[] = $i;
				}
			}	
		}
		
		return count($indices) == 1 ? $indices[0] : (empty($indices) ? null : $indices);
	}
	
	public static function isArrayEmptyTotal($array) {
		if(empty($array)) {
			return true;
		}
		elseif(count($array) > 1 && is_array($array[0])) {
			return false;
		}
		elseif(!is_array($array)) {
			return empty($array);
		}
		
		$isEmpty = true;
		foreach($array as $k => $v) {
			if(!empty($v) && !self::isArrayEmptyTotal($v)) {
				$isEmpty = false;
				break;
			}
		}
			
		return $isEmpty;
	}
	
	public static function transformToAssociativeArray($array, $column) {
	    return array_combine(array_column($array, $column), $array);
	}
}