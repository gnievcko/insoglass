<?php
namespace common\helpers;

use Yii;
use common\models\ar\NumberTemplate;
use common\models\ar\User;
use common\models\ar\NumberTemplateStorage;
use common\models\aq\NumberTemplateStorageQuery;

class NumberTemplateHelper {
	
	public static function getNextNumber($symbol, $entityId = null) {
		$query = NumberTemplate::find()->where(['symbol' => $symbol]);
		if(!empty($entityId)) {
			$query->andWhere(['entity_id' => intval($entityId)]);
		}
		$numberTemplate = $query->one();;
		
		if(empty($numberTemplate)) {
			return null;
		}
		
		return self::constructNextNumber([
				'current_number' => $numberTemplate->numberTemplateStorage->current_number,
				'separator' => $numberTemplate->separator,
				'template' => $numberTemplate->template,
		]);
	}
	
	private static function constructNextNumber($numberTemplate, $isDecremented = false) {
		$isFresh = empty($numberTemplate['current_number']);
		$currentData = !$isFresh ? self::mapVariables($numberTemplate['current_number'], 
				$numberTemplate) : null;
		
		$parts = explode($numberTemplate['separator'], $numberTemplate['template']);
		$dt = new \DateTime();
		$user = User::findOne(Yii::$app->user->id);
		
		$result = [];		
		$step = $isDecremented ? -1 : 1;
		
		foreach($parts as $part) {
			switch($part) {
				case '{noInYear:inc}':
						if(!empty($currentData) && $currentData['{year}'] == $dt->format('Y')) {
							$result['{noInYear:inc}'] = intval($currentData['{noInYear:inc}']) + $step;
							if(intval($result['{noInYear:inc}']) <= 0) {
								$result['{noInYear:inc}'] = 0;
							}
						}
						else {
							$result['{noInYear:inc}'] = '1';
						}
						break;
				case '{initials}':
						if(!empty($user->first_name) && !empty($user->last_name)) {
							$result['{initials}'] = mb_strtoupper(mb_substr($user->first_name, 0, 1, 'UTF-8'), 'UTF-8')
									.mb_strtoupper(mb_substr($user->last_name, 0, 1, 'UTF-8'), 'UTF-8');
						}
						else {
							$result['{initials}'] = mb_strtoupper($user->email, 'UTF-8');
						}
						break;
				case '{year}':
						$result['{year}'] = $dt->format('Y');
						break;
				default:
						$result[$part] = $part;
						break;
			}
		}
		
		return self::constructNumber($result, $numberTemplate);
	}
	
	private static function mapVariables($currentNumber, $numberTemplate) {
		$partsCurrent = explode($numberTemplate['separator'], $currentNumber);
		$partsTemplate = explode($numberTemplate['separator'], $numberTemplate['template']);
		
		if(count($partsCurrent) != count($partsTemplate)) {
			return null;
		}
		
		$result = [];
		for($i = 0; $i < count($partsCurrent); ++$i) {
			$result[$partsTemplate[$i]] = $partsCurrent[$i]; 
		}
		
		return $result;
	}
	
	private static function constructNumber($data, $numberTemplate) {
		$partsTemplate = explode($numberTemplate['separator'], $numberTemplate['template']);
		$result = [];
		
		foreach($partsTemplate as $part) {
			$result[] = $data[$part];
		}
		
		return implode($numberTemplate['separator'], $result);
	}
	
	public static function getAndSaveNextNumber($number, $symbol, $entityId = null) {    
        $condition = !empty($entityId) ? ' AND nt.entity_id = :entityId ' : '';
        
        $query = Yii::$app->db->createCommand(''
            . 'SELECT * '
            . 'FROM number_template_storage nts '
        	. 'JOIN number_template nt ON nts.number_template_id = nt.id '
            . 'WHERE nt.symbol = :symbol '.$condition.' '
            . 'FOR UPDATE')
            ->bindParam(':symbol', $symbol);
        
        if(!empty($entityId)) {
        	$query->bindParam(':entityId', $entityId);
        }
        
        $lockedRow = $query->queryOne();
        
		//$currentNumber = self::getNextNumber($symbol, $entityId);
		$currentNumber = self::constructNextNumber($lockedRow);
		
		/*$query2 = NumberTemplateStorage::find()->alias('nts')->joinWith('numberTemplate nt')->where(['nt.symbol' => $symbol]);
		if(!empty($entityId)) {
			$query2->andWhere(['nt.entity_id' => intval($entityId)]);
		}
		$query2 = NumberTemplateStorage::findOne($lockedRow['id']);
		$storage = $query2->one();*/
		
		//$storage = NumberTemplateStorage::findOne($lockedRow['id']);
		//$storage->current_number = $currentNumber;
		//$storage->save();
		NumberTemplateStorageQuery::updateCurrentNumberById($lockedRow['id'], $currentNumber);
      
		return [
				'number' => $currentNumber, 
				'isDifferent' => $currentNumber != $number,
		];
	}
	
	public static function undoNumber($symbol, $entityId = null) {
		$query = NumberTemplate::find()->where(['symbol' => $symbol]);
		if(!empty($entityId)) {
			$query->andWhere(['entity_id' => intval($entityId)]);
		}
		
		$numberTemplate = $query->one();
		if(empty($numberTemplate)) {
			return null;
		}
		
		$currentNumber = self::constructNextNumber([
				'current_number' => $numberTemplate->numberTemplateStorage->current_number,
				'separator' => $numberTemplate->separator,
				'template' => $numberTemplate->template,
		], true);
		
		$query2 = NumberTemplateStorage::find()->alias('nts')->joinWith('numberTemplate nt')->where(['nt.symbol' => $symbol]);
		if(!empty($entityId)) {
			$query2->andWhere(['nt.entity_id' => intval($entityId)]);
		}
				
		$storage = $query2->one();
		$storage->current_number = $currentNumber;
		$storage->save();
		
		return $currentNumber;
	}
}
