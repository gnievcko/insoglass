<?php

namespace common\helpers;

class RandomSymbolGenerator {

    public static function generate($modelClass, $propertyName = 'symbol', $symbolLength = 16) {
		$symbol = \Yii::$app->security->generateRandomString($symbolLength);

		while($modelClass::find()->where("{$propertyName} = :symbol", [':symbol' => $symbol])->count() > 0) {
			$symbol = \Yii::$app->security->generateRandomString($symbolLength);
		}

        return $symbol;
    }
}
