<?php
namespace common\helpers;

use Yii;
use common\models\ar\Currency;

class StringHelper {

	public static function boolTranslation($value, $strict = true) {
		if($strict) {
			if($value === 1) {
				return \Yii::t('main', 'Yes');
			}
			elseif($value === 0) {
				return \Yii::t('main', 'No');
			}
			return null;
		}
		else {
			if($value == 1) {
				return \Yii::t('main', 'Yes');
			}
			elseif($value == 0) {
				return \Yii::t('main', 'No');
			}
			return null;
		}
	}

	public static function translateOrderToOffer($category, $text, $params = []) {
		if(Yii::$app->params['isOrdersCalledOffers']) {
			switch ($text) {
				case 'Orders':
					return Yii::t($category, 'Offers');
					break;
				case 'Order':
					return Yii::t($category, 'Offer');
					break;
				case 'Order status':
					return Yii::t($category, 'Offer status');
					break;
				case 'Order name':
					return Yii::t($category, 'Offer name');
					break;
				case 'Order number':
					return Yii::t($category, 'Offer number');
					break;
				case 'An order does not exist or you do not have access rights.':
					return Yii::t($category, 'An offer does not exist or you do not have access rights.');
					break;
				case 'List of orders':
					return Yii::t($category, 'List of offers');
					break;
				case 'Update order status':
					return Yii::t($category, 'Update offer status');
					break;
				case 'Order details':
					return Yii::t($category, 'Offer details');
					break;
				case 'Order completion time':
					return Yii::t($category, 'Offer completion time');
					break;
				case 'Order products':
					return Yii::t($category, 'Offer products');
					break;
				case 'Order description':
					return Yii::t($category, 'Offer description');
					break;
				case 'Order history':
					return Yii::t($category, 'Offer history');
					break;
				case 'Create order':
					return Yii::t($category, 'Create offer');
					break;
				case 'Order costs':
					return Yii::t($category, 'Offer costs');
					break;
				case 'Order statuses':
					return Yii::t($category, 'Offer statuses');
					break;
				case 'Orders statuses':
					return Yii::t($category, 'Offers statuses');
					break;
				case 'Create order status':
					return Yii::t($category, 'Create offer status');
					break;
				case 'Update order status':
					return Yii::t($category, 'Update offer status');
					break;
				case 'Order status translation':
					return Yii::t($category, 'Offer status translation');
					break;
				case 'Transitions between statuses orders':
					return Yii::t($category, 'Transitions between statuses offers');
					break;
				case 'Transition between statuses orders':
					return Yii::t($category, 'Transition between statuses offers');
					break;
				case 'The chosen order status-language pair has a translation already.':
					return Yii::t($category, 'The chosen offer status-language pair has a translation already.');
					break;
				case 'Order status transitions':
					return Yii::t($category, 'Offer status transitions');
					break;
				case 'Is order':
					return Yii::t($category, 'Is offer');
					break;
				case 'Add order':
					return Yii::t($category, 'Add offer');
				case 'Select an order...':
					return Yii::t($category, 'Select an offer...');
					break;
				case 'History of salesmen assignment to orders':
					return Yii::t($category, 'History of salesmen assignment to offers');
					break;
				case 'History of salesman assignment to order':
					return Yii::t($category, 'History of salesman assignment to offer');
					break;
				case 'Order update id':
					return Yii::t($category, 'Offer update id');
					break;
				case 'Create a template based on this order':
					return Yii::t($category, 'Create a template based on this offer');
					break;
				case 'Reserved order products':
					return Yii::t($category, 'Reserved offer products');
					break;
				case 'Order type':
					return Yii::t($category, 'Offer type');
					break;
				case 'Edit order':
					return Yii::t($category, 'Edit offer');
					break;
				case 'Concerns order':
					return Yii::t($category, 'Concerns offer');
				case 'Order has been updated successfully.':
					return Yii::t($category, 'Offer has been updated successfully.');
					break;
				case 'Order data cannot be saved. Check provided values.':
					return Yii::t($category, 'Offer data cannot be saved. Check provided values.');
					break;
				case 'Order has been added successfully.':
					return Yii::t($category, 'Offer has been added successfully.');
					break;
				case 'Order is inactive.':
					return Yii::t($category, 'Offer is inactive.');
					break;
				case 'Order value':
					return Yii::t($category, 'Offer value');
					break;
				case 'Number of open orders':
					return Yii::t($category, 'Number of open offers');
					break;
				case 'Number of closed orders':
					return Yii::t($category, 'Number of closed offers');
					break;
				case 'Number of ongoing orders':
					return Yii::t($category, 'Number of ongoing offers');
					break;
				case 'Number of handled orders':
					return Yii::t($category, 'Number of handled offers');
					break;
				case 'Orders by status':
					return Yii::t($category, 'Offers by status');
					break;
				case 'Orders by client':
					return Yii::t($category, 'Offers by client');
					break;
				case 'Orders by author':
					return Yii::t($category, 'Offers by author');
					break;
				case 'Orders by employee updating':
					return Yii::t($category, 'Offers by employee updating');
					break;
				case 'Upcoming orders':
					return Yii::t($category, 'Upcoming offers');
					break;
				case 'REPORT - ORDERS':
					return Yii::t($category, 'REPORT - OFFERS');
					break;
				case 'Current orders':
					return Yii::t($category, 'Current offers');
					break;
				case 'Additional costs of order':
					return Yii::t($category, 'Additional costs of offer');
					break;
				case 'Report on orders':
					return Yii::t($category, 'Report on offers');
				case 'Number of the order would be {number} because of concurrent operations.':
					return Yii::t($category, 'Number of the offer would be {number} because of concurrent operations.');
                case 'Update order':
                    return Yii::t($category, 'Update offer');
                case 'History order status':
                    return Yii::t($category, 'History offer status');
                case 'Number of orders':
                	return Yii::t($category, 'Number of offers');
                case 'Orders deadlines':
                	return Yii::t($category, 'Offers deadlines');
                case 'Completed orders':
                	return Yii::t($category, 'Completed offers');
				case 'Order renewing':
					return Yii::t($category, 'Offer renewing');
				case 'An order renewing is required':
					return Yii::t($category, 'An offer renewing is required');
				case 'An order renewing alert has been created successfully':
					return Yii::t($category, 'An offer renewing alert has been created successfully');
				case 'Cannot create an order renewing alert':
					return Yii::t($category, 'Cannot create an offer alert');
				case 'You can set an alert in the form of a task so the system can remind you about the possibility of renewing the order.':
					return Yii::t($category, 'You can set an alert in the form of a task so the system can remind you about the possibility of renewing the offer.');
                case 'Order register':
                	return Yii::t($category, 'Offer register');
                case 'Order date':
                	return Yii::t($category, 'Offer date');
                case 'Choose type of the order':
                	return Yii::t($category, 'Choose type of the offer');
                case 'Register of orders within a selected period.':
                	return Yii::t($category, 'Register of offers within a selected period.');
                case 'Orders for clients':
                	return Yii::t($category, 'Offers for clients');
                case 'Are you sure you want to delete this order?<br/><strong>It would be not available as well as its related specific data.</strong>':
                	return Yii::t($category, 'Are you sure you want to delete this offer?<br/><strong>It would be not available as well as its related specific data.</strong>');
               	case 'The order has been deleted.':
               		return Yii::t($category, 'The offer has been deleted.');
               	case 'The order cannot be deleted. Please contact the administrator.':
               		return Yii::t($category, 'The offer cannot be deleted. Please contact the administrator.');
               	case 'Are you sure you want to generate this invoice?<br/>The invoice for this order was generated at {date}.':
               		return Yii::t($category, 'Are you sure you want to generate this invoice?<br/>The invoice for this offer was generated at {date}.', $params);
               	case 'Copy order':
               		return Yii::t($category, 'Copy offer');
               	case 'Order types':
               		return Yii::t($category, 'Offer types');
               	case 'List of order types':
               		return Yii::t($category, 'List of offer types');
               	case 'Add order type':
               		return Yii::t($category, 'Add offer type');
               	case 'Order type details':
               		return Yii::t($category, 'Offer type details');
               	case 'Order type has been added successfully.':
               		return Yii::t($category, 'Offer type has been added successfully.');
               	case 'Order type cannot be saved. Check provided values.':
               		return Yii::t($category, 'Offer type cannot be saved. Check provided values.');
               	case 'Order type has been updated successfully.':
               		return Yii::t($category, 'Offer type has been updated successfully.');
               	case 'Update order type':
               		return Yii::t($category, 'Update offer type');
               	case 'Report with reminders to orders\' renewal':
               		return Yii::t($category, 'Report with reminders to offers\' renewal');
               	case 'Included order types':
               		return Yii::t($category, 'Included offer types');
               	case 'Order type has been deleted.':
               		return Yii::t($category, 'Offer type has been deleted.');
               	case 'Order type cannot be deleted. Please contact the administrator.':
               		return Yii::t($category, 'Offer type cannot be deleted. Please contact the administrator.');
               	case 'Search by order':
               		return Yii::t($category, 'Search by offer');
               	case 'Create suborder':
               		return Yii::t($category, 'Create suboffer');
               	case 'Suborder':
               		return Yii::t($category, 'Suboffer');
               	case 'Suborders':
               		return Yii::t($category, 'Suboffers');
               	case 'Choose parent order' :
               		return Yii::t($category, 'Choose parent offer');
               	case 'Parent order' :
               		return Yii::t($category, 'Parent offer');
               	case 'Tasks for this order':
               		return Yii::t($category, 'Tasks for this offer');
               	case 'No orders to view' :
               	    return Yii::t($category, 'No offers to view');
                case 'Update order' :
                    return Yii::t($category, 'Update offer');
                case 'You can change priority of this product\'s production process. If you do, it affects on the whole order, not only this particular product.':
                    return Yii::t($category, 'You can change priority of this product\'s production process. If you do, it affects on the whole offer, not only this particular product.');
                case 'Change priority of the whole order':
                    return Yii::t($category, 'Change priority of the whole offer');
                case 'Products in order':
                    return Yii::t($category, 'Products in offer');
                default:
					return Yii::t($category, $text);
			}
		}
		else {
			return Yii::t($category, $text);
		}
	}

	public static function getFormattedDateFromTimestamp($date, $withTime = false) {
		if(empty($date)) {
			return $date;
		}

		if($withTime) {
			return date('d.m.Y H:i:s', $date);
		}
		else {
			return date('d.m.Y', $date);
		}
	}

	public static function getFormattedDateFromDate($date, $withTime = false) {
		if(empty($date)) {
			return $date;
		}

		$dt = new \DateTime($date);
		if($withTime) {
			return $dt->format('d.m.Y H:i:s');
		}
		else {
			return $dt->format('d.m.Y');
		}
	}

	public static function getFormattedCost($cost, $withDefaultValue = false) {
		$str = number_format(floatval($cost), 2, ',', ' ');
		
		if($withDefaultValue) {
			$currency = Currency::find()->where(['symbol' => Utility::CURRENCY_PLN])->one();
			$str .= ' '.$currency->short_symbol;
		}
		
		return $str;
	}

	public static function mb_lcfirst($str) {
		$firstLetter = mb_substr($str, 0, 1);
		return mb_strtolower($firstLetter) . mb_substr($str, 1);
	}
	
	public static function getCurrentDateAsText() {
	    $now = new \DateTime();
	    $day = $now->format('j');
	    $month = mb_strtolower(self::getMonthName($now->format('n')), 'UTF-8');
	    $year = $now->format('Y');
	    return $day.' '.$month.' '.$year;
	}
	
	private static function getMonthName($monthIndex) {
	    $monthNames = [
	        '',
	        Yii::t('main', 'January'),
	        Yii::t('main', 'February'),
	        Yii::t('main', 'March'),
	        Yii::t('main', 'April'),
	        Yii::t('main', 'May'),
	        Yii::t('main', 'June'),
	        Yii::t('main', 'July'),
	        Yii::t('main', 'August'),
	        Yii::t('main', 'September'),
	        Yii::t('main', 'October'),
	        Yii::t('main', 'November'),
	        Yii::t('main', 'December'),
	    ];
	
	    return $monthNames[$monthIndex];
	}
}
