<?php
namespace common\helpers;

return [
    'normal' => [
	        'min' => 6,
	        'upper' => 0,
	        'lower' => 0,
	        'digit' => 0,
	        'special' => 0,
	        'hasUser' => false,
	        'hasEmail' => false
    ],
];
