<?php
namespace common\helpers;

use Yii;

class RoleHelper {
	
	public static function getSalesmanRoles() {
		return [
			Utility::ROLE_SALESMAN => Utility::ROLE_SALESMAN,
			Utility::ROLE_MAIN_SALESMAN => Utility::ROLE_MAIN_SALESMAN,
		];
	}
	
	public static function getStorekeeperRoles() {
		return [
			Utility::ROLE_STOREKEEPER => Utility::ROLE_STOREKEEPER,
			Utility::ROLE_MAIN_STOREKEEPER => Utility::ROLE_MAIN_STOREKEEPER,
		];
	}
	
	public static function getProductionRoles() {
	    return [
            Utility::ROLE_WORKER => Utility::ROLE_WORKER,
            Utility::ROLE_QA_SPECIALIST => Utility::ROLE_QA_SPECIALIST,
	        Utility::ROLE_MAIN_WORKER => Utility::ROLE_MAIN_WORKER,
	    ];
	}
	
	public static function getEmployeeRoles() {
		return [
			Utility::ROLE_ADMIN => Utility::ROLE_ADMIN,
			Utility::ROLE_STOREKEEPER => Utility::ROLE_STOREKEEPER,
			Utility::ROLE_MAIN_STOREKEEPER => Utility::ROLE_MAIN_STOREKEEPER,
			Utility::ROLE_SALESMAN => Utility::ROLE_SALESMAN,
			Utility::ROLE_MAIN_SALESMAN => Utility::ROLE_MAIN_SALESMAN,
			Utility::ROLE_REPAIRER => Utility::ROLE_REPAIRER,
		    Utility::ROLE_WORKER => Utility::ROLE_WORKER,
		    Utility::ROLE_QA_SPECIALIST => Utility::ROLE_QA_SPECIALIST,
		    Utility::ROLE_MAIN_WORKER => Utility::ROLE_MAIN_WORKER,
		];
	}
	
	public static function getMyUnderlingEmployeeRoles() {
		$roleArray = null;
		if(Yii::$app->user->can(Utility::ROLE_ADMIN)) {
			$roleArray = RoleHelper::getEmployeeRoles();
		}
		else if(Yii::$app->user->can(Utility::ROLE_MAIN_STOREKEEPER) && Yii::$app->user->can(Utility::ROLE_MAIN_SALESMAN) &&
				!(Yii::$app->user->can(Utility::ROLE_ADMIN))) {
			$roleArray = array_merge(RoleHelper::getStorekeeperRoles(), RoleHelper::getSalesmanRoles());
		}
		else if(Yii::$app->user->can(Utility::ROLE_MAIN_STOREKEEPER)) {
			$roleArray = RoleHelper::getStorekeeperRoles();
		}
		else if(Yii::$app->user->can(Utility::ROLE_MAIN_SALESMAN)) {
			$roleArray = RoleHelper::getSalesmanRoles();
		}
		else if(Yii::$app->user->can(Utility::ROLE_MAIN_WORKER)) {
		    $roleArray = RoleHelper::getProductionRoles();
		}
		return $roleArray;	
	}		
}