<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace common\helpers;

use Yii;
use yii\helpers\Html;

/**
 * Description of StrengthValidator
 *
 * @author Bartek
 */
class StrengthValidator extends \kartik\password\StrengthValidator {

    public $specialLetter = true;
    public $specialLetterError;

    const RULE_SPECIAL_LETTER = 'specialLetter';

    public function __construct($config = array()) {
        parent::$_rules = parent::$_rules + [
            self::RULE_SPECIAL_LETTER => ['bool' => true]
        ];
        parent::__construct($config);
    }

    protected static function getRuleMessage($rule) {
        $result = parent::getRuleMessage($rule);
        if (empty($result)) {
            switch ($rule) {
                case self::RULE_SPECIAL_LETTER:
                    return Yii::t('main', '{attribute} cannot contain any special letter');
            }
        }

        return $result;
    }

    protected function performValidation($params = array()) {
        $data = parent::performValidation($params);
        if (empty($data)) {
            foreach (parent::$_rules as $rule => $setup) {
                if ($rule === self::RULE_SPECIAL_LETTER && $this->specialLetter === false) {
                    $this->validateSpecialLetter($params, $rule);
                }
            }
        }
        return $data;
    }

    protected function getData($params = []) {
        /** @var Model $model */
        $model = $attribute = $value = $label = null;
        extract($params);
        $hasModel = $model !== null;
        if ($hasModel) {
            $value = Html::getAttributeValue($model, $attribute);
            if (!is_string($value)) {
                $this->addError($model, $attribute, $this->message);
                return null;
            }
            $label = $model->getAttributeLabel($attribute);
            $username = !$this->hasUser ? '' : (isset($this->usernameValue) ? $this->usernameValue :
                    Html::getAttributeValue($model, $this->userAttribute));
        } else {
            if (!is_string($value)) {
                return [$this->message, []];
            }
            $username = !$this->hasUser ? '' : $this->usernameValue;
        }
        $temp = [];

        return [$model, $attribute, $value, $label, $hasModel];
    }

    protected function validateSpecialLetter($params = [], $rule) {
        list($model, $attribute, $value, $label, $hasModel) = $this->getData($params);
        if (preg_match('/[^\x20-\x7f]/', $value) === 1) {
            $param = "{$rule}Error";
            if ($hasModel) {
                $this->addError($model, $attribute, $this->$param, ['attribute' => $label]);
            } else {
                return [$this->$param, []];
            }
        }

        return null;
    }

}
