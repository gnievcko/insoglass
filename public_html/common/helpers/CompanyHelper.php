<?php
namespace common\helpers;

use Yii;
use common\models\ar\CompanyGroup;
use common\models\ar\CompanyGroupSet;
use common\models\ar\CompanyGroupTranslation;
use common\models\ar\Language;

class CompanyHelper {
	
	public static function addNewCompanyGroup($company) {
		$companyGroupSet = new CompanyGroupSet();
		$companyGroupSet->company_id = $company->id;
		$companyGroupSet->company_group_id = CompanyGroup::find()->where('symbol = :symbol', [':symbol' => Utility::COMPANY_GROUP_DEFAULT])->one()->id;
		$companyGroupSet->save();
	
        $hash = RandomSymbolGenerator::generate(CompanyGroup::class);
		 
		$companyGroup = new CompanyGroup();
		$companyGroup->is_predefined = 1;
		$companyGroup->symbol = $hash;
		$companyGroup->save();
		 
		$companyGroupTranslation = new CompanyGroupTranslation();
		$companyGroupTranslation->company_group_id = $companyGroup->id;
		$companyGroupTranslation->language_id = Language::find()->where('symbol = :symbol', [':symbol' => Yii::$app->language])->one()->id;
		$companyGroupTranslation->name = $company->name;
		$companyGroupTranslation->save();
		 
		$companyGroupSetSingle = new CompanyGroupSet();
		$companyGroupSetSingle->company_id = $company->id;
		$companyGroupSetSingle->company_group_id = $companyGroup->id;
		$companyGroupSetSingle->save();
		
		return $companyGroup->id;
	}
	
	public static function updateCompanyGroup($company) {
		$groups = $company->companyGroupSets;
		if(!empty($groups)) {
			$defaultGroupId = CompanyGroup::find()->where('symbol = :symbol', [':symbol' => Utility::COMPANY_GROUP_DEFAULT])->one()->id;
	
			foreach($groups as $groupSet) {
				if(!empty($groupSet->companyGroup->is_predefined) && $groupSet->company_group_id != $defaultGroupId) {
					$translation = CompanyGroup::findOne($groupSet->company_group_id)->getTranslation();
					if(!empty($translation) && $translation->name != $company->name) {
						$translation->name = $company->name;
						$translation->save();
					}
				}
			}
		}
	}
	
	public static function deleteCompanyGroups($company) {
		$groups = $company->companyGroupSets;
		if(!empty($groups)) {
			$defaultGroupId = CompanyGroup::find()->where('symbol = :symbol', [':symbol' => Utility::COMPANY_GROUP_DEFAULT])->one()->id;
	
			for($i = 0; $i < count($groups); ++$i) {
				$groupSet = $groups[$i];
				$companyGroupId = $groupSet->company_group_id;
				$isPredefined = $groupSet->companyGroup->is_predefined;
				$groupSet->delete();
				 
				if(!empty($isPredefined) && $companyGroupId != $defaultGroupId) {
					CompanyGroupTranslation::deleteAll(['company_group_id' => $companyGroupId]);
					CompanyGroup::deleteAll(['id' => $companyGroupId]);
				}
			}
		}
	}
	
}
