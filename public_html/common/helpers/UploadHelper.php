<?php
namespace common\helpers;

use Yii;
use yii\helpers\Url;

class UploadHelper {

	const UPLOAD_DIRECTORY = 'uploads';

	public static function getUploadPath() {
		$directory = dirname(dirname(\Yii::getAlias('@webroot'))).DIRECTORY_SEPARATOR.self::UPLOAD_DIRECTORY;
		
		if(!file_exists($directory)) {
			@mkdir($directory);
		}
		
		return $directory;
	}
	
	public static function generatePath($extension) {
		$uploadPath = self::getUploadPath();
		
		do {
			$name = \Yii::$app->getSecurity()->generateRandomString(24) . '.' . $extension;
			$fullName = $uploadPath . DIRECTORY_SEPARATOR . $name;
		} while(file_exists($fullName));
		
		return ['name' => $name, 'fullName' => $fullName];
	}

    public static function saveTemporaryFile($uploadedFile) {
        $filePath = self::generatePath($uploadedFile->extension);
        $uploadedFile->saveAs($filePath['fullName']);

        return $filePath;
    }

    public static function storeInformationAboutUploadedFile($fileHash, $originalName) {
        Yii::$app->db->createCommand()->insert('file_temporary_storage', [
            'file_hash' => $fileHash,
            'original_file_name' => $originalName,
        ])->execute();
            
        $uploadDetailsId = Yii::$app->db->getLastInsertID();
        $uploadDetails = (new \yii\db\Query())->select(['id', 'file_hash', 'date_creation', 'original_file_name'])
            ->from('file_temporary_storage')
            ->where(['=', 'id', $uploadDetailsId])
            ->one();

        return $uploadDetails;
    }

    public static function copy($sourceFileHash) {
        $sourceFileAbsolutePath = self::getAbsolutePath($sourceFileHash);
        $sourceFileExtension = pathinfo($sourceFileAbsolutePath, PATHINFO_EXTENSION);

        $copyPath = self::generatePath($sourceFileExtension);
        copy($sourceFileAbsolutePath, $copyPath['fullName']);

        return $copyPath['name'];
    }

    public static function removeByUrl($fileUrl) {
        $fileName = self::getFileNameByUrl($fileUrl);

        $absolutePathToFile = rtrim(self::getUploadPath(), '/').'/'.$fileName;

        if(file_exists($absolutePathToFile)) {
            unlink($absolutePathToFile);
        }
    }

    public static function getFileNameByUrl($url) {
        $patternMatches = [];
        preg_match('|/index\?f=(.+)$|', $url, $patternMatches);

        return end($patternMatches);
    }

    public static function getAbsolutePath($fileHash) {
        return rtrim(self::getUploadPath(), '/').'/'.$fileHash;
    }
    
    public static function deleteAttachment($url, $attachment) {
    	try {
    		self::removeByUrl($url);
    		$attachment->delete();
    		
    		if(!Yii::$app->request->isAjax) {
    			Yii::$app->getSession()->setFlash('success', Yii::t('web', 'The attachment has been deleted.'));
    		}
    	} catch (\Exception $e) {
    		Yii::error(print_r($e->getMessage(), true));
    		if(!Yii::$app->request->isAjax) {
    			Yii::$app->getSession()->setFlash('error', Yii::t('web', 'The attachment cannot be deleted. Please contact the administrator.'));
    		}
    	}
    }
    
    public static function getNormalPath($url, $originalFileName) {
        $fileName = self::getFileNameByUrl($url);
        return Url::to(['storage/file', 'name' => $originalFileName, 'f' => $fileName]);
    }
}
