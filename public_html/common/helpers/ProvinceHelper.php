<?php

namespace common\helpers;

use Yii;
use common\models\ar\Province;
use common\models\ar\Language;
use yii\helpers\ArrayHelper;

class ProvinceHelper {
    
    public static function getListOfNonArtificialProvinces() {
        return ArrayHelper::map(
            Province::find()->select(['province.id as id', 'ct.name as symbol', 'province.name as name'])
            ->joinWith(['country c' => function($q) {
                $q->joinWith('countryTranslations ct');
                $q->where(['ct.language_id' => Language::find()->where(['symbol' => Yii::$app->language])->one()->id]);
            }])->where(['province.is_artificial' => 0])->orderBy('name')->all(), 'id', function($item) {
                return $item->name . ' (' . $item->symbol . ')';
            }
        );
    }
}
