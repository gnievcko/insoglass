<?php
namespace common\helpers;

use Yii;
use yii\helpers\Url;

class ImageHelper {

    const THUMBNAIL_SIZE = 100;

	public static function generateThumbnail($filePath, $extenstion) {
        $image = new \Imagick($filePath);
        self::scaleThumbnail($image);
        self::cropThumbnail($image);

        $croppedImagePath = UploadHelper::generatePath($extenstion);
        $image->writeImage($croppedImagePath['fullName']);

        return $croppedImagePath;
	}

    private static function scaleThumbnail($image) {
        $width = $image->getImageWidth();
        $height = $image->getImageHeight();

        $smallerEdgeLenght = ($width < $height) ? $width : $height;
        if($smallerEdgeLenght > self::THUMBNAIL_SIZE) {
            $scaleRatio = self::THUMBNAIL_SIZE / $smallerEdgeLenght;
            $image->scaleImage($width*$scaleRatio, $height*$scaleRatio);
        }
    }

    private static function cropThumbnail($image) {
        $width = $image->getImageWidth();
        $height = $image->getImageHeight();

        $newWidth = ($width > self::THUMBNAIL_SIZE) ? self::THUMBNAIL_SIZE : $width;
        $newHeight = ($height > self::THUMBNAIL_SIZE) ? self::THUMBNAIL_SIZE : $height;
        $x = ($width - $newWidth)/2;
        $y = ($height - $newHeight)/2;

        $image->cropImage($newWidth, $newHeight, $x, $y);
    }
}
