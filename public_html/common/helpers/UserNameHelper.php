<?php

namespace common\helpers;

class UserNameHelper {

    public static function generateDisplayedName($user) {
        return empty($user['first_name']) ? $user['last_name'] : "{$user['first_name']} {$user['last_name']}";
    }
}
