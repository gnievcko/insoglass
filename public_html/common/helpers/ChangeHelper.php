<?php
namespace common\helpers;

use Yii;
use yii\helpers\BaseJson;
use common\helpers\StringHelper;
use common\helpers\Utility;
use common\models\ar\ClientModificationHistory;
use common\models\ar\UserModificationHistory;

class ChangeHelper {
	public $result;
	
	public function __construct($record , $action, $except = []) {
			$this->findChanges($record, $action, $except);
	}
	
	
	public function findChanges($record, $action, $except) {
		
		$this->result = [
				'user id' => Yii::$app->user->id,
				'date' => StringHelper::getFormattedDateFromTimestamp(time(), true),
				'action' => $action,
				'target id' => null,
		];
		
		if($action == Utility::ACTION_EDIT) {
			$fieldsTracked = array_keys($record->getAttributes(null, $except));
			$changedAttributes = $record->getDirtyAttributes($fieldsTracked);
			$changedKeys = array_keys($changedAttributes);
			
			$old = null;
			foreach($changedKeys as $r) {
				$old[] = $record->getOldAttribute($r);
			}
			
			$resultData = null;
			
			for($i = 0; $i < count($changedKeys); $i++) {
				if($changedKeys[$i] != 'password') {
					if(!empty($old[$i]) || !empty($changedAttributes[$changedKeys[$i]])) {
						if($old[$i] != $changedAttributes[$changedKeys[$i]]) {
							$resultData[$changedKeys[$i]] = [
									'old' => $old[$i],
									'new' => $changedAttributes[$changedKeys[$i]],
							] ;
						}
					}
				}
				else {
					$resultData[$changedKeys[$i]] = [
							'old' => Yii::t('main', 'Cannot display for security reasons'),
							'new' => Yii::t('main', 'Cannot display for security reasons'),
					] ;
					
				}
			}	
			$this->result['fields changed'] =  $resultData;
		}

	}
	
	public function addAttribute($name, $old, $new) {
		if(is_array($old) && is_array($new)) {
			if(empty(array_diff($old, $new)) && empty(array_diff($new, $old))) {
				return false;
			}
			
		}
		else {
			if($old == $new || (empty($old) && empty($new))) {
				return false;
			}
		}

		$this->result['fields changed'][$name] = [
				'old' => $old,
				'new' => $new,
		];
	}
	
	public function saveChanges($id = null, $historyModel) {
		$this->result['target id'] = $id;
		if(!empty($this->result)) {
			$historyModel->modification = BaseJson::encode($this->result);
			$historyModel->save();
		}
	}
	
}