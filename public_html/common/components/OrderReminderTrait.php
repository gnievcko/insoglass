<?php

namespace common\components;

use Yii;
use common\models\ar\Task;
use common\models\ar\TaskData;
use common\models\ar\TaskPriority;
use common\models\ar\TaskType;
use common\models\ar\TaskTypeReference;
use common\helpers\StringHelper;
use common\helpers\Utility;

// TODO: Zabezpieczyc ten formularz przed bledami, w tym dotyczacymi braku odpowiednich statusow w bazie danych
trait OrderReminderTrait {
	
	public $period;
	public $orderId;
	public $dateSpecific;
	
	public $optionNever = 'never';
	public $option2Months = '2months';
	public $option5Months = '5months';
	public $option11Months = '11months';
	public $optionCustom = 'custom';
	
	public function init() {
		$this->dateSpecific = (new \DateTime())->format('Y-m-d');
	}
	
	public function getTimeOptions() {
		return [
				$this->optionNever => Yii::t('web', 'Never'),
				$this->option2Months => Yii::t('web', '2 months'),
				$this->option5Months => Yii::t('web', '5 months'),
				$this->option11Months => Yii::t('web', '11 months'),
				$this->optionCustom => Yii::t('web', 'Specific date'),
		];
	}
	
	public function attributeLabels() {
		return [
				'period' => Yii::t('web', 'Remind in:'),
				'dateSpecific' => Yii::t('web', 'Specific date'),
		];
	}
	
	public function rules() {
		return [
				[['period', 'orderId'], 'required'],
				[['period'], 'in', 'range' => array_keys($this->getTimeOptions()), 'message' => Yii::t('web', 'Wrong value for alert period')],
				[['dateSpecific'], 'safe'],
		];
	}
	
	public function save($dateReference = null) {
		if(!$this->validate()) {
			throw new \Exception('OrderReminderTrait validation error: ' . print_r($this->getErrors(), true));
		}
	
		switch($this->period) {
			case $this->option2Months:
				$interval = '+2 months';
				break;
			case $this->option5Months:
				$interval = '+5 months';
				break;
			case $this->option11Months:
				$interval = '+11 months';
				break;
			case $this->optionCustom:
				$interval = '';
				break;
			default:
				return;
		}
	
		$initialDate = !empty($dateReference) ? $dateReference : new \DateTime();
		$date = !empty($interval) ? $initialDate->modify($interval) : (new \DateTime($this->dateSpecific));
	
		$task = new Task();
		$task->task_type_id = TaskType::find()->select(['id'])->where(['symbol' => Utility::TASK_TYPE_ASSOCIATED_WITH_ORDER])->scalar();
		$task->user_id = Yii::$app->user->id;
		$task->title = StringHelper::translateOrderToOffer('main', 'Order renewing');
		$task->message = StringHelper::translateOrderToOffer('main', 'An order renewing is required');
		$task->date_deadline = $date->format('Y-m-d');
		$task->date_reminder = $date->modify('-14 days')->format('Y-m-d');
		$task->task_priority_id = TaskPriority::find()->select(['id'])->where(['symbol' => Utility::TASK_PRIORITY_MEDIUM])->scalar();
		$task->is_active = 1;
		$task->is_complete = 0;
	
		if(!$task->save()) {
			throw new \Exception('Error during saving the task.');
		}
	
		$taskData = new TaskData();
		$taskData->task_id = $task->id;
		$taskData->task_type_reference_id = TaskTypeReference::find()->select(['id'])->where([
				'task_type_id' => $task->task_type_id,
				'name_table' => 'order',
		])->scalar();
		$taskData->referenced_object_id = $this->orderId;
	
		if (!$taskData->save()) {
			throw new \Exception('Error during saving the task data.');
		}
	}
}