<?php
namespace common\components;

class ActiveForm extends \yii\widgets\ActiveForm {

	public $fieldClass = 'common\components\ActiveField';

}
