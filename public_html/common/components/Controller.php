<?php
namespace common\components;

use Yii;
use yii\helpers\Html;
use yii\db\Expression;

/**
 * @inheritdoc
 */
class Controller extends \yii\web\Controller {
	
	use MessageTrait;
	
	public $withoutAssets = false;
	
	/*public function injectBanners(&$content, $withUpdateCounters = true, $bannerImageOptions = null) {
		while(strpos($content, '{banner}') !== false) {
			$banner = BannerTranslation::find()->joinWith('language l', true, 'JOIN')->where('l.symbol = :language', [':language' => Yii::$app->language])->orderBy(new Expression('RAND()'))->one();
				
			if(!empty($banner)) {
				$content = str_replace('{banner}', Html::img($banner->url_photo, $bannerImageOptions ?: ['style' => 'max-width: 100%; width: 630px;', 'alt' => empty($banner->description) ? 'Footer' : $banner->description]), $content);
	
				if($withUpdateCounters) {
					$bannerRecord = $banner->banner;
					$bannerRecord->count_uses = new Expression('count_uses + 1');
					$bannerRecord->save(false);
				}
			}
			else {
				$content = strtr($content, ['{banner}' => $withUpdateCounters ? '' : 'No banners in DB (message only in test mode)']);
				return;
			}
		}
	
		while(preg_match('/\{banner\.(?P<symbol>[\w\-_]+)\}/', $content, $matches)) {
			$banner = BannerTranslation::find()->joinWith('language l', true, 'JOIN')->joinWith('banner b', true, 'JOIN')->where('l.symbol = :language AND b.symbol = :banner', [':language' => Yii::$app->language, ':banner' => $matches['symbol']])->one();
	
			if(!empty($banner)) {
				$content = str_replace('{banner.'.$matches['symbol'].'}', Html::img($banner->url_photo, $bannerImageOptions ?: ['style' => 'max-width: 100%; width: 630px;', 'alt' => empty($banner->description) ? 'Footer' : $banner->description]), $content);
	
				if($withUpdateCounters) {
					$bannerRecord = $banner->banner;
					$bannerRecord->count_uses = new Expression('count_uses + 1');
					$bannerRecord->save(false);
				}
			}
			else {
				$content = strtr($content, ['{banner.'.$matches['symbol'].'}' => $withUpdateCounters ? '' : 'No banners in DB (message only in test mode)']);
				return;
			}
		}
	}*/
	
}
