<?php

namespace common\components;

use Yii;
use yii\helpers\Html;
use yii\base\Model;

class ActiveField extends \yii\widgets\ActiveField {

	private $_inputId;
	private $_skipLabelFor = false;
	
	private function togglePlaceholderLabel(&$options) {
		if(Yii::$app->params['isPlaceholderInputLabel']) {
			$this->label(false);
		} else {
			if(!empty($options['placeholder'])) {
				$this->label($options['placeholder']);
				$options['placeholder'] = null;
			} else if(!empty($options['placeHolder'])) {
				$this->label($options['placeHolder']);
				$options['placeHolder'] = null;
			}

		}
	}
	
	public function textInput($options = []) {
		$this->togglePlaceholderLabel($options);
		parent::textInput($options);
		return $this;
	}
	
	public function hiddenInput($options = []) {
		$this->togglePlaceholderLabel($options);
		parent::hiddenInput($options);
		
		return $this;
	}
	
	public function passwordInput($options = []) {
		$this->togglePlaceholderLabel($options);
		parent::passwordInput($options);

		return $this;
	}
	
	public function textarea($options = []) {
		$this->togglePlaceholderLabel($options);
		parent::textarea($options);
		return $this;
	}

	public function dropDownList($items, $options = []) {
		$this->togglePlaceholderLabel($options);
		parent::dropDownList($items, $options);
		
		return $this;
	}
	
	public function listBox($items, $options = []) {
		$this->togglePlaceholderLabel($options);
		parent::listBox($items, $options);
		
		return $this;
	}
	
	public function checkboxList($items, $options = []) {
		$this->togglePlaceholderLabel($options);
		parent::checkboxList($items, $options);
		
		return $this;
	}
	
	public function radioList($items, $options = []) {
		$this->togglePlaceholderLabel($options);
		parent::radioList($items, $options);
		
		return $this;
	}
	
	public function widget($class, $config = [])
	{
		$this->togglePlaceholderLabel($config);
		parent::widget($class, $config);
		
		return $this;
	}
	
	
	/*
	 public function fileInput($options = [])
	 {
	 // https://github.com/yiisoft/yii2/pull/795
	 if ($this->inputOptions !== ['class' => 'form-control']) {
	 $options = array_merge($this->inputOptions, $options);
	 }
	 // https://github.com/yiisoft/yii2/issues/8779
	 if (!isset($this->form->options['enctype'])) {
	 $this->form->options['enctype'] = 'multipart/form-data';
	 }
	 $this->addAriaAttributes($options);
	 $this->adjustLabelFor($options);
	 $this->parts['{input}'] = Html::activeFileInput($this->model, $this->attribute, $options);
	 
	 return $this;
	 }
	 
	 public function radio($options = [], $enclosedByLabel = true)
	 {
	 if ($enclosedByLabel) {
	 $this->parts['{input}'] = Html::activeRadio($this->model, $this->attribute, $options);
	 $this->parts['{label}'] = '';
	 } else {
	 if (isset($options['label']) && !isset($this->parts['{label}'])) {
	 $this->parts['{label}'] = $options['label'];
	 if (!empty($options['labelOptions'])) {
	 $this->labelOptions = $options['labelOptions'];
	 }
	 }
	 unset($options['labelOptions']);
	 $options['label'] = null;
	 $this->parts['{input}'] = Html::activeRadio($this->model, $this->attribute, $options);
	 }
	 $this->addAriaAttributes($options);
	 $this->adjustLabelFor($options);
	 
	 return $this;
	 }
	 
	 public function checkbox($options = [], $enclosedByLabel = true)
	 {
	 if ($enclosedByLabel) {
	 $this->parts['{input}'] = Html::activeCheckbox($this->model, $this->attribute, $options);
	 $this->parts['{label}'] = '';
	 } else {
	 if (isset($options['label']) && !isset($this->parts['{label}'])) {
	 $this->parts['{label}'] = $options['label'];
	 if (!empty($options['labelOptions'])) {
	 $this->labelOptions = $options['labelOptions'];
	 }
	 }
	 unset($options['labelOptions']);
	 $options['label'] = null;
	 $this->parts['{input}'] = Html::activeCheckbox($this->model, $this->attribute, $options);
	 }
	 $this->addAriaAttributes($options);
	 $this->adjustLabelFor($options);
	 
	 return $this;
	 }
	 */	
	
}
