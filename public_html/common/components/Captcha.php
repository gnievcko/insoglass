<?php

namespace common\components;

use yii\captcha\CaptchaAsset;
use Yii;
use yii\base\InvalidConfigException;
use yii\helpers\Url;
use yii\helpers\Html;
use yii\helpers\Json;
use yii\widgets\InputWidget;

/**
 * @inherited
 */
class Captcha extends \yii\captcha\Captcha {

	public $refreshId = 'captcha-';
	
	public function init() {		
		$this->captchaAction = Yii::$app->controller->id.'/captcha';
		
		parent::init();		
	}
	
    /**
     * Renders the widget.
     */
    public function run() {
    	$this->refreshId = uniqid('captcha-');
        $this->registerClientScript();
        if($this->hasModel()) {
            $input = Html::activeTextInput($this->model, $this->attribute, $this->options);
        } 
        else {
            $input = Html::textInput($this->name, $this->value, $this->options);
        }
        
        $route = $this->captchaAction;
        if(is_array($route)) {
            $route['v'] = uniqid();
        } 
        else {
            $route = [$route, 'v' => uniqid()];
        }
        
        $image = Html::img($route, $this->imageOptions);
        
        $refresh = Html::a('', '#', [ 'class' => 'glyphicon glyphicon-refresh', 'id' => $this->refreshId ]);
        
        echo strtr($this->template, [
	            '{input}' => $input,
	            '{image}' => $image,
	        	'{refresh}' => $refresh
        ]);
    }

    /**
     * Registers the needed JavaScript.
     */
    public function registerClientScript() {
        $options = $this->getClientOptions();
        $options = empty($options) ? '' : Json::htmlEncode($options);
        $id = $this->imageOptions['id'];
        $view = $this->getView();
        CaptchaAsset::register($view);
        $view->registerJs("jQuery('#$id').yiiCaptcha($options); jQuery('#$this->refreshId').click(function(e) { e.preventDefault(); $(this).prev().trigger('click'); });");
    }
}
