<?php
use yii\helpers\Html;
use kartik\datecontrol\DateControl;
use yii\web\View;
?>

<div class="order-reminder row">
	<div class="col-sm-7">
		<?php
			echo $form->field($model, 'period')->radioList($model->getTimeOptions());
			echo Html::activeHiddenInput($model, 'orderId', ['value' => $order['id']]);
		?>
	</div>
	<div class="col-sm-3 date-specific-select">
		<?php 
			echo $form->field($model, 'dateSpecific')->widget(DateControl::classname(), [
	                'language' => \Yii::$app->language,
	                'type' => DateControl::FORMAT_DATE,
	                'ajaxConversion' => false,
	                'displayFormat' => 'php:d.m.Y',
	                'saveFormat' => 'php:Y-m-d',
	                'options' => [
		                   	'pluginOptions' => [
		                       		'autoclose' => true
		                	]
	        		]
			])->label(false);
		?>
	</div>
</div>

<?php 
	$this->registerJs('
				$("input[name*=\"[period]\"]").change(function() {
		            if($("input[name*=\"[period]\"]:checked").val() == "custom") {
		            	$(".date-specific-select").css("display", "block");
		            }
					else {
						$(".date-specific-select").css("display", "none");
					}
				});
			', View::POS_END);
?>