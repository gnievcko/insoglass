<?php

namespace common\components\validators;

use yii\validators\Validator;

class RequiredIfFormEmptyValidator extends Validator
{
    public function validateAttribute($model, $attribute)
    {
        if(!$this->areOtherFieldsEmpty($model, $attribute) && empty($model->$attribute)) {
            $this->addError($model, $attribute, \Yii::t('web', '{attribute} field must not be empty', ['attribute' => $model->getAttributeLabel($attribute)]));
        } 
    }

    private function areOtherFieldsEmpty($model, $attributeToExclude) {
        return array_reduce($model->attributes(), function($isFormEmpty, $attribute) use($model, $attributeToExclude) {
            return $isFormEmpty && (($attribute === $attributeToExclude) || empty($model->$attribute));
        }, true);
    }

    public function isEmpty($value) {
        return false;
    }
}
