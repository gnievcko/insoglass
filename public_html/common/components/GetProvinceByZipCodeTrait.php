<?php
namespace common\components;
use common\models\ar\ProvinceZipCodeRule;

use Yii;

trait GetProvinceByZipCodeTrait {
	public function actionGetProvinceByZipCode() {
		$zipCode = Yii::$app->request->get('zipCode');
		$zipCodeProvince = ProvinceZipCodeRule::findOne(['zip_code' => $zipCode]);
		return !empty($zipCodeProvince) ? $zipCodeProvince->province->id : null;
	}
}