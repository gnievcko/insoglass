<?php

namespace common\components;

use Yii;
use common\models\ar\EmailConfig;

class MessageProvider {

	const MODE_PHONE = 1;
	const MODE_EMAIL = 2;
	const MODE_ALL = 3;

	public function send($user, $smsSymbol, $emailSymbol, $mode = null, $smsOptions = [], $emailOptions = [], $withAlert = true, $attachments = []) {
		$mode = $mode ?: self::MODE_ALL;
		$sent = false;
		
		if(!empty($user->email) && in_array($mode, [self::MODE_EMAIL, self::MODE_ALL])) {
			/* @var frontend\components\Controller|backend\components\Controller $controller */
			$controller = Yii::$app->controller;
			$sent = (EmailHelper::STATUS_OK === $controller->sendEmailMessage($user->email, $emailSymbol, $emailOptions, $withAlert, $attachments)) || $sent;
		}
		
		return $sent;
	}

	public function sendStaticEmail($from, $fromName, $to, $subject, $body, $textMode = true, $emailConfigId = null) {
		$config = EmailConfig::findOne($emailConfigId);

		ini_set('max_execution_time', 120);
		if(empty($config)) {
			return false;
		}

		/* @var yii\swiftmailer\Mailer $mailer */
		$mailer = Yii::$app->mailer;

		$transport = \Swift_SmtpTransport::newInstance($config->host, $config->port, $config->starttls ? 'ssl' : null)
				->setUsername($config->email)
				->setPassword($config->password);

		$mailer->setTransport($transport);

		$message = $mailer->compose()
				->setFrom($config->noreply_email)
				->setTo([ $to ])
				->setReplyTo([$from => $fromName])
				->setSubject($subject);

		if($textMode) {
			$message->setTextBody($body);
		}
		else {
			$message->setHtmlBody($body);
			$message->setTextBody('Twój klient pocztowy ma wyłączoną możliwość wyświetlania wiadomości w trybie HTML lub jej nie obsługuje. Prosimy włączyć tryb HTML lub zmienić klienta na obsługującego ten tryb.');
		}

		if(isset(Yii::$app->params['archiveEmail'])) {
			$message->setBcc(Yii::$app->params['archiveEmail']);
		}

		return $message->send();
	}

}
?>