<?php

namespace common\alerts;

use Yii;
use common\models\ar\Alert;
use common\models\ar\AlertHistory;
use yii\db\Expression;

abstract class AlertHandler {
	
	abstract public static function add($params);
	abstract protected static function findCreatedAlert($params);
	abstract public static function check($params);
	
	public static function complete($id) {
		$transaction = Yii::$app->db->beginTransaction();
		try {
			$alert = Alert::findOne($id);
			
			$alertHistory = new AlertHistory();
			$alertHistory->alert_type_id = $alert->alert_type_id;
			$alertHistory->user_id = $alert->user_id;
			$alertHistory->user_deactivating_id = Yii::$app->user->id;
			$alertHistory->message = $alert->message;
			$alertHistory->date_deactivation = new Expression('NOW()');
			$alertHistory->date_creation = $alert->date_creation;
			$alertHistory->order_id = $alert->order_id;
			$alertHistory->product_id = $alert->product_id;
			$alertHistory->company_id = $alert->company_id;
			$alertHistory->task_id = $alert->task_id;
			$alertHistory->warehouse_id = $alert->warehouse_id;
			$alertHistory->save();
			
			$alert->delete();
			
			$transaction->commit();
		}
		catch(\Exception $e) {
			$transaction->rollBack();
			Yii::error('System cannot complete alert with id = '.$id);
			return false;
		}
		
		return true;
	}	
}
