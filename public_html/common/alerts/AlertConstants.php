<?php

namespace common\alerts;

class AlertConstants {
	
	const ALERT_PRIORITY_WARNING = 'warning';
	const ALERT_PRIORITY_PROBLEM = 'problem';
	
	const ALERT_TYPE_ORDER_DEADLINE_WARNING = 'order_deadline_warning';
	const ALERT_TYPE_ORDER_DEADLINE_PROBLEM = 'order_deadline_problem';
	const ALERT_TYPE_TASK_DEADLINE_WARNING = 'task_deadline_warning';
	const ALERT_TYPE_TASK_DEADLINE_PROBLEM = 'task_deadline_problem';
	const ALERT_TYPE_PROBLEM_COUNT_BELOW_MINIMAL = 'product_count_below_minimal';
    const ALERT_TYPE_NEW_ORDER_STATUS_UNCHANGED = 'new_order_status_unchanged';
}