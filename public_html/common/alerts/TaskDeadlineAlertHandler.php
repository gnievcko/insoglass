<?php

namespace common\alerts;

use Yii;
use common\models\ar\Alert;
use common\models\ar\AlertType;
use common\models\ar\Task;

class TaskDeadlineAlertHandler extends AlertHandler {
	
	public static function add($params) {
		if(!isset($params['taskId']) || !isset($params['alertTypeSymbol'])) {
			return null;
		}
		
		try {
			$alert = self::findCreatedAlert($params);
			if(empty($alert)) {
				$task = Task::findOne($params['taskId']);
				$alertType = AlertType::find()->where(['symbol' => $params['alertTypeSymbol']])->one();
				$alertTypeTranslation = $alertType->getTranslation();
				
				if($alertType->symbol == AlertConstants::ALERT_TYPE_TASK_DEADLINE_PROBLEM) {
					$warningAlert = self::findCreatedAlert([
							'taskId' => $params['taskId'], 
							'alertTypeSymbol' => AlertConstants::ALERT_TYPE_TASK_DEADLINE_WARNING					
					]);
					
					if(!empty($warningAlert)) {
						self::complete($warningAlert->id);
					}
				}
				
				$alert = new Alert();
				$alert->alert_type_id = $alertType->id;
				$alert->user_id = !empty($task->user_notified_id) ? $task->user_notified_id : $task->user_id;
				$alert->message = !empty($alertTypeTranslation) ? $alertTypeTranslation->name : $task->title;
				$alert->is_active = 1;
				$alert->task_id = $task->id;
				$alert->save();
			}
		}
		catch(\Exception $e) {
			Yii::error('Error during adding task deadline alert for ('.$params['taskId'].', '.$params['alertTypeSymbol'].'): '.$e->getMessage());
			return null;
		}
		
		return $alert;
	}
	
	protected static function findCreatedAlert($params) {
		$query = Alert::find()->alias('a')->joinWith('alertType at')->where([
				'a.task_id' => $params['taskId'],
				'a.is_active' => true,
		]);
		
		if(isset($params['alertTypeSymbol'])) {
			$query->andWhere(['at.symbol' => $params['alertTypeSymbol']]);
			return $query->one();
		}
		else {
			$query->andWhere(['at.symbol' => [AlertConstants::ALERT_TYPE_TASK_DEADLINE_WARNING, AlertConstants::ALERT_TYPE_TASK_DEADLINE_PROBLEM]]);
			return $query->all();
		}
	}
	
	public static function check($params) {
		if(!isset($params['taskId'])) {
			return false;
		}
		
		$alerts = self::findCreatedAlert(['taskId' => $params['taskId']]);
		if(empty($alerts)) {
			return false;		
		}
		
		$task = Task::findOne($params['taskId']);
		if(empty($task)) {
			return false;
		}
		
		if(!empty($task->is_complete) || (
					isset($params['forceIfIncomplete']) && !empty($params['forceIfIncomplete'])
				)) {
			foreach($alerts as $alert) {
				self::complete($alert->id);
			}
		}
	}
}
