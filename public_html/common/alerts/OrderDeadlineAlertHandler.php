<?php

namespace common\alerts;

use Yii;
use common\models\ar\Alert;
use common\models\ar\AlertType;
use common\models\ar\Order;
use common\models\aq\OrderHistoryQuery;
use common\helpers\Utility;

class OrderDeadlineAlertHandler extends AlertHandler {
	
	public static function add($params) {
		if(!isset($params['orderId']) || !isset($params['alertTypeSymbol'])) {
			return null;
		}
		
		try {
			$alert = self::findCreatedAlert($params);
			if(empty($alert)) {
				$order = Order::findOne($params['orderId']);
				$alertType = AlertType::find()->where(['symbol' => $params['alertTypeSymbol']])->one();
				$alertTypeTranslation = $alertType->getTranslation();
				
				if($alertType->symbol == AlertConstants::ALERT_TYPE_ORDER_DEADLINE_PROBLEM) {
					$warningAlert = self::findCreatedAlert([
							'orderId' => $params['orderId'], 
							'alertTypeSymbol' => AlertConstants::ALERT_TYPE_ORDER_DEADLINE_WARNING					
					]);
					
					if(!empty($warningAlert)) {
						self::complete($warningAlert->id);
					}
				}
				
				$alert = new Alert();
				$alert->alert_type_id = $alertType->id;
				$alert->user_id = $order->user_id;
				$alert->message = !empty($alertTypeTranslation) ? $alertTypeTranslation->name : $task->title;
				$alert->is_active = 1;
				$alert->order_id = $order->id;
				$alert->save();
			}
		}
		catch(\Exception $e) {
			Yii::error('Error during adding order deadline alert for ('.$params['orderId'].', '.$params['alertTypeSymbol'].'): '.$e->getMessage());
			return null;
		}
		
		return $alert;
	}
	
	protected static function findCreatedAlert($params) {
		$query = Alert::find()->alias('a')->joinWith('alertType at')->where([
				'a.order_id' => $params['orderId'],
				'a.is_active' => true,
		]);
		
		if(isset($params['alertTypeSymbol'])) {
			$query->andWhere(['at.symbol' => $params['alertTypeSymbol']]);
			return $query->one();
		}
		else {
			$query->andWhere(['at.symbol' => [AlertConstants::ALERT_TYPE_ORDER_DEADLINE_WARNING, AlertConstants::ALERT_TYPE_ORDER_DEADLINE_PROBLEM]]);
			return $query->all();
		}
	}
	
	public static function check($params) {
		if(!isset($params['orderId'])) {
			return false;
		}
		
		$alerts = self::findCreatedAlert(['orderId' => $params['orderId']]);
		if(empty($alerts)) {
			return false;
		}
		
		$entries = OrderHistoryQuery::getOrderEntries($params['orderId']);
		if(count($entries) < 2) {
			return false;
		}
		
		if(count($entries) > 2) {
			$entries = [$entries[0], $entries[1]];
		}
		
		$order = Order::findOne($params['orderId']);
		$endAlerts = false;
		
		if(empty($order->is_active) || !empty($order->is_deleted)) {
			$endAlerts = true;
		}
		elseif($entries[0]['statusSymbol'] != $entries[1]['statusSymbol'] ||
				$entries[0]['dateReminder'] != $entries[1]['dateReminder'] ||
				$entries[0]['dateDeadline'] != $entries[1]['dateDeadline']
		) {
			if(in_array($entries[0]['statusSymbol'], [
					Utility::ORDER_STATUS_ACCEPTED_BY_CLIENT, 
					Utility::ORDER_STATUS_DEFERRED, 
					Utility::ORDER_STATUS_IN_PROGRESS,
					Utility::ORDER_STATUS_COMPLETED,
					Utility::ORDER_STATUS_INVOICED,
			])) {
				$endAlerts = true;
			}
			elseif(in_array($entries[0]['statusSymbol'], [
					Utility::ORDER_STATUS_SUSPENDED,
			])) {
				$dateReminder = new \DateTime($entries[1]['dateReminder'] ?: '');
				$dateDeadline = new \DateTime($entries[1]['dateDeadline'] ?: '');
				$now = new \DateTime();
					
				if($now <= $dateReminder && $now <= $dateDeadline) {
					$endAlerts = true;
				}
			}
		}
		
		if($endAlerts) {
			foreach($alerts as $alert) {
				self::complete($alert->id);
			}
		}
	}
}
