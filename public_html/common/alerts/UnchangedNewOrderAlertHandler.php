<?php

namespace common\alerts;

use common\models\ar\Alert;
use common\models\ar\AlertType;
use common\alerts\AlertConstants;
use common\helpers\StringHelper;
use common\models\ar\Order;

class UnchangedNewOrderAlertHandler extends AlertHandler {
    
    public static function add($params) {
        $orderId = isset($params['orderId']) ? $params['orderId'] : null;
		if($orderId === null || !empty(self::findCreatedAlert($params))) {
            return null;
        }
        
        $alertType = AlertType::find()->where([
            'symbol' => AlertConstants::ALERT_TYPE_NEW_ORDER_STATUS_UNCHANGED
        ])->one();
        
        $alert = new Alert();
        $alert->alert_type_id = $alertType->id;
        $alert->is_active = 1;
        $alert->order_id = $orderId;
        $alert->message = StringHelper::translateOrderToOffer('web', 'Status of order accepted by client has not changed');
        $alert->save();
        
        return $alert;
	}

    protected static function findCreatedAlert($params) {
        return Alert::find()->alias('a')->joinWith('alertType at')->where([
				'a.order_id' => $params['orderId'],
				'a.is_active' => true,
                'at.symbol' => AlertConstants::ALERT_TYPE_NEW_ORDER_STATUS_UNCHANGED,
		])->one();
    }

    public static function check($params) {
        if(empty($params['orderId'])) {
			return false;
		}
        
		$alert = self::findCreatedAlert(['orderId' => $params['orderId']]);
		if(empty($alert)) {
			return false;
		}
		
		$order = Order::findOne($params['orderId']);
		if(!$order->is_active || self::orderStatusHasChanged($alert->order_id)) {
			self::complete($alert->id);
		}
    }
    
    private static function orderStatusHasChanged($orderId) {
        return !empty((new \yii\db\Query())->select(['*'])
            ->from('order o')
            ->innerJoin('order_history oh', 'o.order_history_last_id = oh.id')
            ->innerJoin('order_status os', 'os.id = oh.order_status_id')
            ->andWhere(['order_id' => $orderId])
            ->andWhere(['!=', 'os.symbol', \common\helpers\Utility::ORDER_STATUS_ACCEPTED_BY_CLIENT])
            ->all());
    }
}
