<?php

namespace common\alerts;

use Yii;
use common\models\ar\Alert;
use common\models\ar\AlertType;
use common\models\aq\UserQuery;
use common\models\aq\WarehouseProductQuery;

class ProductMinimalCountAlertHandler extends AlertHandler {
	
	public static function add($params) {
		if(!isset($params['productId'])) {
			return null;
		}
		
		try {
			$alert = self::findCreatedAlert($params);
			if(empty($alert)) {
				$alertType = AlertType::find()->where(['symbol' => AlertConstants::ALERT_TYPE_PROBLEM_COUNT_BELOW_MINIMAL])->one();
				$alertTypeTranslation = $alertType->getTranslation();
				
				$mainStorekeepers = UserQuery::getActiveMainStorekeepers();
				$userId = null;
				if(!empty($mainStorekeepers)) {
					$userId = $mainStorekeepers[0]['id'];
				}
				elseif(isset($params['userId'])) {
					$userId = $params['userId'];
				}

				$alert = new Alert();
				$alert->alert_type_id = $alertType->id;
				$alert->message = !empty($alertTypeTranslation) ? $alertTypeTranslation->name : $task->title;
				$alert->is_active = 1;
				$alert->product_id = $params['productId'];
				$alert->save();
			}
		}
		catch(\Exception $e) {
			Yii::error('Error during adding product minimal count alert for ('.$params['productId'].', '.($params['userId'] ?: 'null').'): '.$e->getMessage());
			return null;
		}
		
		return $alert;
	}
	
	protected static function findCreatedAlert($params) {
		$query = Alert::find()->alias('a')->joinWith('alertType at')->where([
				'a.product_id' => $params['productId'],
				'a.is_active' => true,
				'at.symbol' => AlertConstants::ALERT_TYPE_PROBLEM_COUNT_BELOW_MINIMAL,
				
		]);
		
		return $query->one();
	}
	
	public static function check($params) {
		if(!isset($params['productId'])) {
			return false;
		}
		
		$alert = self::findCreatedAlert(['productId' => $params['productId']]);
		if(empty($alert)) {
			return false;		
		}
		
		$result = WarehouseProductQuery::getCountByProductId($params['productId']);
		if(!empty($result) && (empty($result['countMinimal']) || $result['count'] >= $result['countMinimal'])) {
			self::complete($alert->id);
		}
	}
}
