<?php 

namespace common\bundles;

use yii\web\AssetBundle;

class EnhancedDialogAsset extends AssetBundle {
	
	/**
	 * @inheritdoc
	 */
	public $jsOptions = ['position' => \yii\web\View::POS_END];
	
	public $publishOptions = ['forceCopy' => true];
	
	/**
	 * @inheritdoc
	 */
	public $depends = [
			'yii\web\YiiAsset',
			'kartik\dialog\DialogBootstrapAsset'
			];
	
	/**
	 * @inheritdoc
	 */
	public $js = [
			'js/enhanced-dialog.js'
			];
	
	/**
     * @inheritdoc
     */
    public function init() {
        $this->sourcePath = __DIR__ . '/assets';
        parent::init();
    }
    
    /**
     * Registers this asset bundle with a view. And registering i18n for BootstrapDialog
     * @param \yii\web\View $view the view to be registered with
     * @return static the registered asset bundle instance
     */
    public static function register($view, $title = null) {
    	if(empty($title)) {
    		$title = \Yii::t('web', 'Information');
    	}
    	
    	$view->registerJs("
    			BootstrapDialog.DEFAULT_TEXTS[BootstrapDialog.TYPE_DEFAULT] = '".$title."';
		        BootstrapDialog.DEFAULT_TEXTS[BootstrapDialog.TYPE_INFO] = '".$title."';
		        BootstrapDialog.DEFAULT_TEXTS[BootstrapDialog.TYPE_PRIMARY] = '".$title."';
		        BootstrapDialog.DEFAULT_TEXTS[BootstrapDialog.TYPE_SUCCESS] = '".$title."';
		        BootstrapDialog.DEFAULT_TEXTS[BootstrapDialog.TYPE_WARNING] = '".$title."';
		        BootstrapDialog.DEFAULT_TEXTS[BootstrapDialog.TYPE_DANGER] = '".$title."';
		        BootstrapDialog.DEFAULT_TEXTS['OK'] = '".\Yii::t('web', 'Yes')."';
		        BootstrapDialog.DEFAULT_TEXTS['CANCEL'] = '".\Yii::t('web', 'Cancel')."';
		        BootstrapDialog.DEFAULT_TEXTS['CONFIRM'] = '".\Yii::t('web', 'Confirmation')."';
    			", \yii\web\View::POS_END);
    	
    	return parent::register($view);
    }
}