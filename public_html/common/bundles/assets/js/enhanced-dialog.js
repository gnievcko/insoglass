yii.confirm = function(message, okCallback, cancelCallback) {
	BootstrapDialog.confirm({
        message: message,
        type: BootstrapDialog.TYPE_DANGER,
        draggable: true, // <-- Default value is false
        callback: function(result) {
            if(result) {
            	!okCallback || okCallback();
            }
            else {
                !cancelCallback || cancelCallback();
            }
        }});
};
