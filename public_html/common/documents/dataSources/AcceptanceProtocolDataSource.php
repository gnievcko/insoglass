<?php 
namespace common\documents\dataSources;

use common\documents\sections\AcceptanceProtocolContent;
use common\models\ar\Order;

class AcceptanceProtocolDataSource implements DataSource {

    private $contentSectionClass;
    private $orderId;

    public function __construct($orderId) {
        $this->contentSectionClass = (new AcceptanceProtocolContent())->formName();
        $this->orderId = $orderId;
    }

    public function getInitialData() {
    	$companyParentText = '';
    	$companyObjectText = '';
    
    	if(!empty($this->orderId)) {
	    	$companyParent = Order::findOne($this->orderId)->company;
	    	$companyObject = null;
	    	if(!empty($companyParent->parentCompany)) {
	    		$companyObject = $companyParent;
	    		$companyParent = $companyParent->parentCompany;
	    	}
	    	
	    	$companyParentText = $companyParent->name;
	    	if(!empty($companyParent->address)) {
	    		$companyParentText .= ', '.$companyParent->address->getFullAddress();
	    	}
	    	if(!empty($companyObject)) {
	    		$companyObjectText = $companyObject->name;
	    		if(!empty($companyObject->address)) {
	    			$companyObjectText .= ', '.$companyObject->address->getFullAddress();
	    		}
	    	}
    	}
    
        return [
            $this->contentSectionClass => [
            		'orderingParty' => $companyParentText,
            		'object' => $companyObjectText,
            ]
        ];
    }
    
    public function getNumber() {
    	return null;
    }
}