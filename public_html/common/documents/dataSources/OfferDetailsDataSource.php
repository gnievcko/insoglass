<?php

namespace common\documents\dataSources;

use Yii;
use common\repositories\OfferDetailsRepository;
use common\documents\sections\OfferDetailsBasicData;
use common\documents\forms\OfferDetailsProductForm;
use Mindseater\PHPUtils\Dates\Formatter;
use frontend\helpers\UserHelper;
use common\documents\sections\OfferDetailsSummary;
use common\models\aq\OrderQuery;
use common\documents\sections\OfferDetailsCustomData;
use common\models\ar\Order;
use yii\helpers\Json;
use common\models\ar\Entity;
use common\models\aq\OrderUserRepresentativeQuery;

class OfferDetailsDataSource implements DataSource {

    private $repository;
    private $basicDataSectionClass;
    private $productsClass;
    private $customDataClass;
    private $summaryClass;
    private $orderId;
    private $details;

    public function __construct(OfferDetailsRepository $repository, $orderId) {
        $this->repository = $repository;
        $this->basicDataSectionClass = (new OfferDetailsBasicData())->formName();
        $this->productsClass = (new OfferDetailsProductForm())->formName();
        $this->summaryClass = (new OfferDetailsSummary())->formName();
        $this->customDataClass = (new OfferDetailsCustomData())->formName();
        $this->orderId = $orderId;
        $this->details = $this->repository->getDetails($this->orderId);
    }

    public function getInitialData() {
        return [
            	$this->basicDataSectionClass => $this->loadBasicData($this->orderId),
            	$this->productsClass => $this->repository->getProducts($this->orderId),
        		$this->summaryClass => $this->loadSummaryData(),
        		$this->customDataClass => $this->loadCustomData(),
        ];
    }

    private function loadBasicData() {
        $orderDetails = $this->getDetails(); 
        $salespeople = $this->getSalespeople();
        $orderDetails['orderId'] = $orderDetails['id'];
        return $orderDetails + ['salespeople' => $salespeople, 'remarks' => $orderDetails['description']];
    }

    private function getDetails() {
        $details = $this->details;
        $representatives = OrderUserRepresentativeQuery::getUserRepresentativesByOrderId($this->orderId);
        $this->convertDatetimesToDates($details, ['dateLastModification', 'dateCreation']);
        
        $details['contactPeople'] = '';
        $details['contactPerson'] = '';
        $details['clientPhone'] = '';
        $details['clientEmail'] = '';
        
        if(!empty($representatives)) {
        	foreach($representatives as $rep) {
        		$details['contactPeople'] .= UserHelper::getPrettyUserName($rep['email'], $rep['firstName'], $rep['lastName']);
        		
        		if(!empty($rep['contactEmail'] || (!empty($rep['email']) && !strstr($rep['email'], '@domdozm.net') && !strstr($rep['email'], '@osobakontaktowa.pl')))) {
        			$details['contactPeople'] .= ', '.(!empty($rep['contactEmail']) ? $rep['contactEmail'] : $rep['email']);
        		}
        		
        		$phone = implode(', ', array_filter([$rep['phone'], $rep['phone2']], function($phone) {
        			return !empty($phone);
        		}));
        		if(!empty($phone)) {
        			$details['contactPeople'] .= ', '.Yii::t('web', 'ph.').' '.$phone;
        		}
        		
        		$details['contactPeople'] .= "\n";
        	}
        }
        
        $details['clientCompany'] = $details['companyName'];
        $details['authorSalesman'] = UserHelper::getPrettyUserName($details['email'], $details['firstName'], $details['lastName']);
        $details['authorEmail'] = $details['email'];
        $details['authorPhone'] = implode(', ', 
            	array_filter([$details['userPhone1'], $details['userPhone2']], function($phone) {
                return !empty($phone);
        }));
        $details['authorCompany'] = $details['entity'];
        if(!empty($details['parentCompanyName'])) {
        	$details['clientCompanyParent'] = $details['parentCompanyName'];
        }
        if(!empty($details['position'])) {
        	$details['authorPosition'] = $details['position'];
        }
        
        $entity = Entity::findOne($details['entityId']);
        if(!empty($entity) && !empty($entity->address)) {
        	$details['authorAddress'] = $entity->address->main.', '.$entity->address->city->zip_code.' '.$entity->address->city->name;
        }
        
        if(!empty($details['companyAddress'])) {
        	$details['clientAddress'] = $details['companyAddress'].', '.$details['cityZipCode'].' '.$details['cityName'];
        }
        
        if(!empty($details['companyPhone1'])) {
        	$details['clientPhone'] = $details['companyPhone1'];
        	if(!empty($details['companyPhone2'])) {
        		$details['clientPhone'] .= ', '.$details['companyPhone2'];
        	}
        }
        
        return $details;
    }

    private function convertDatetimesToDates(&$details, $datetimesKeys) {
        $dateFormatter = new Formatter();
        $datetimeFormat = 'Y-m-d H:i:s';
        $dateFormat = 'Y-m-d';

        foreach($datetimesKeys as $datetimeKey) {
            $details[$datetimeKey] = $dateFormatter->from($details[$datetimeKey], $datetimeFormat)->format($dateFormat)->getDate();
        }
    }

    private function getSalespeople() {
        $salespeopleNames = array_map(function($salesman) {
            return UserHelper::getPrettyUserName($salesman['firstName'], $salesman['lastName'], $salesman['email']);
        }, $this->repository->getSalespeople($this->orderId));

        return implode(', ', $salespeopleNames);
    }
    
    private function loadSummaryData() {
    	return [
    			'note' => $this->details['descriptionNote'],
    			'remarks' => $this->details['descriptionCont'],
    			'deadline' => $this->details['execution_time'], 
    			'termsOfPayment' => $this->details['payment_terms'], 
    			'validityDate' => $this->details['duration_time'],
    	];
    }
    
    private function loadCustomData() {
    	$customData = Order::findOne($this->orderId)->orderHistoryLast->orderCustomData;
    	$result = [];
    	$result['isVisible'] = !empty($customData) ? 1 : 0;
    	$result['columnCount'] = !empty($customData) ? $customData->column_count : 2;
    	$result['tables'] = !empty($customData) ? Json::decode($customData->content) : [];    	
    	
    	return $result;
    }
    
    public function getNumber() {
    	return !empty($this->details) ? $this->details['number'] : null;
    }
}
