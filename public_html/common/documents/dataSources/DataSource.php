<?php

namespace common\documents\dataSources;

interface DataSource {

    function getInitialData();
    
    function getNumber();
}
