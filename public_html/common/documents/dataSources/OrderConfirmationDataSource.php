<?php
namespace common\documents\dataSources;

use Yii;
use common\repositories\OfferDetailsRepository;
use common\models\ar\Entity;
use common\models\aq\OrderUserRepresentativeQuery;
use Mindseater\PHPUtils\Dates\Formatter;
use frontend\helpers\UserHelper;
use common\documents\forms\OrderConfirmationProductForm;
use common\documents\sections\OrderConfirmationBasicData;
use common\documents\sections\OrderConfirmationCustomData;
use common\models\ar\Order;

class OrderConfirmationDataSource implements DataSource {
	
	private $repository;
	private $basicDataSectionClass;
	private $productsClass;
	private $customDataSectionClass;
	private $orderId;
	private $details;
	
	public function __construct(OfferDetailsRepository $repository, $orderId) {
		$this->repository = $repository;
		$this->basicDataSectionClass = (new OrderConfirmationBasicData())->formName();
		$this->productsClass = (new OrderConfirmationProductForm())->formName();
		$this->customDataSectionClass = (new OrderConfirmationCustomData())->formName();

		$this->orderId = $orderId;
		$this->details = $this->repository->getDetails($this->orderId);
	}
	
	public function getInitialData() {
		return [
				$this->basicDataSectionClass => $this->loadBasicData(),
				$this->productsClass => $this->repository->getProducts($this->orderId),
				$this->customDataSectionClass => $this->loadCustomData(),
		];
	}
	
	private function loadBasicData() {
		$orderDetails = $this->getDetails();
		$salespeople = $this->getSalespeople();
		$orderDetails['orderId'] = $orderDetails['id'];
		return $orderDetails + ['salespeople' => $salespeople, 'remarks' => $orderDetails['description']];
	}
	
	private function loadCustomData() {
		$config = Yii::$app->params;
		
		$companyBankAccount = $config['owner']['bank'];
		$companySwift = $config['owner']['swift'];
		$companyBank = $config['owner']['bankAddress'];
		
		if(!empty($this->orderId)) {
			
			$order = Order::findOne($this->orderId);
			if(!empty($order)) {
				$entity = $order->entity;
				if(empty($entity)) {
					$entity = EntityQuery::getDefaultEntity();
				}
				
				if(!empty($entity)) {
					$accountParts = explode(', SWIFT: ', $entity->account_number);
					$companyBankAccount = $accountParts[0];
					$companySwift = (count($accountParts) > 1 ? $accountParts[1] : '');
					$companyBank = $entity->bank_name;
				}
			}
		}
		return ['companyBankAccount' => $companyBankAccount, 'companySwift' => $companySwift, 'companyBank' => $companyBank];
	}
	
	private function getDetails() {
		$details = $this->details;
		$representatives = OrderUserRepresentativeQuery::getUserRepresentativesByOrderId($this->orderId);
		$this->convertDatetimesToDates($details, ['dateLastModification', 'dateCreation']);
		
		$details['contactPeople'] = '';
		$details['contactPerson'] = '';
		$details['clientPhone'] = '';
		$details['clientEmail'] = '';
		
		if(!empty($representatives)) {
			foreach($representatives as $rep) {
				$details['contactPeople'] .= UserHelper::getPrettyUserName($rep['email'], $rep['firstName'], $rep['lastName'])."\n";
			}
		}
		
		$details['clientCompany'] = $details['companyName'];

		if(!empty($details['companyFax'])){
			$details['clientFax'] = $details['companyFax'];
		}
		
		if(!empty($details['companyAddress'])) {
			$details['clientAddress'] = $details['companyAddress'].', '.$details['cityZipCode'].' '.$details['cityName'];
			$details['deliveryAddress'] = $details['clientAddress'];
		}
		
		if(!empty($details['companyPhone1'])) {
			$details['clientPhone'] = $details['companyPhone1'];
			if(!empty($details['companyPhone2'])) {
				$details['clientPhone'] .= ', '.$details['companyPhone2'];
			}
		}
		
		$entity = Entity::findOne($details['entityId']);
		if(!empty($entity) && !empty($entity->fax)) {
			$details['authorFax'] = $entity->fax;
		}
		return $details;
	}
	
	private function convertDatetimesToDates(&$details, $datetimesKeys) {
		$dateFormatter = new Formatter();
		$datetimeFormat = 'Y-m-d H:i:s';
		$dateFormat = 'Y-m-d';
		
		foreach($datetimesKeys as $datetimeKey) {
			$details[$datetimeKey] = $dateFormatter->from($details[$datetimeKey], $datetimeFormat)->format($dateFormat)->getDate();
		}
	}
	
	private function getSalespeople() {
		$salespeopleNames = array_map(function($salesman) {
			return UserHelper::getPrettyUserName($salesman['firstName'], $salesman['lastName'], $salesman['email']);
		}, $this->repository->getSalespeople($this->orderId));
			
			return implode(', ', $salespeopleNames);
	}
	
	public function getNumber() {
		return !empty($this->details) ? $this->details['number'] : null;
	}
}