<?php

namespace common\documents;

use Yii;
use common\components\Controller;
use common\documents\html\Renderer as HtmlRenderer;
use common\documents\pdf\Renderer as PdfRenderer;
use yii\web\Response;
use common\documents\dataSources\DataSource;
use common\documents\DocumentsGenerator;
use frontend\helpers\DocumentHelper;

abstract class DocumentController extends Controller {
	
    protected $save = true;
    protected $renderPdf = false;
    protected $orderId;
    protected $title = '';
    protected $edit = true;

    protected function createAndView($documentType, DataSource $dataSource = null) {
        $request = Yii::$app->request;
        $document = $this->getDocument($documentType, $dataSource);
        
        $filename = null;
        if(!empty($dataSource)) {
			$number = $dataSource->getNumber();
        	$filename = (!empty($number) ? $number : $this->title);
        }
        
		if($request->isPost && $request->isAjax) {
			Yii::$app->response->format = Response::FORMAT_JSON;
			return $document->performActiveValidation();
		}
		if($request->isPost || $this->renderPdf) {
		$renderer = new PdfRenderer([
                        'options' => [
                            'title' => $this->title
                            ]
                ], $filename
            );
            if($this->save) {
                DocumentsGenerator::save($document);
            }
            return $this->renderContent($renderer->render($document));
        } else {
            $renderer = new HtmlRenderer();
            return $this->render('@common/documents/html/views/document_base', [
                'documentView' => $renderer->render($document),
                'documents' => DocumentHelper::getDocuments($this->orderId, $documentType),
                'orderId' => $this->orderId,
            ]);
        }
		
	}
    
    protected function getDocument($documentType, DataSource $dataSource = null) {
        $request = Yii::$app->request;
        $documentId = $request->get('documentId');
        $historyId = $request->get('historyId');
        
        if(!empty($documentId) && !empty($historyId) && $this->orderId == 0) {
            $document = DocumentsGenerator::reconstruct($documentId, $historyId);
            $this->renderPdf = true;
            $this->save = false;
        } else {
            
            if(!empty($documentId) && !empty($historyId)) {
                $document = DocumentsGenerator::reconstruct($documentId, $historyId, $this->edit);
                $dataSource = null;
            } else {
                $document = DocumentsGenerator::generate($documentType);
            }
            $document->makeEditable();

            $request = Yii::$app->request;

            $document->loadInput($request->isPost 
                    ? $request->post() 
                    : ($dataSource === null ? [] : $dataSource->getInitialData()));

            $this->renderPdf = $this->renderPdf || false;
            $this->save = $this->save && true;
        }
        
        return $document;
    }
}