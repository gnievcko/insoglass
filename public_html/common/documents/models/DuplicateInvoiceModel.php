<?php
namespace common\documents\models;

use Yii;
use common\documents\sections\InvoiceTitle;
use common\documents\sections\InvoicePayment;
use common\documents\sections\InvoiceProducts;

class DuplicateInvoiceModel {
	
	private $rawDataInput;
	private $documentData;
	
	public function __construct($rawDataInput, $documentData) {
		$this->rawDataInput = $rawDataInput;
		$this->documentData = $documentData;
	}
	
	public function prepareInputData($isEdit = false) {		
		$invoiceTitleFormName = (new InvoiceTitle())->formName();
		$invoiceProductsFormName = (new InvoiceProducts())->formName();
		$invoicePaymentFormName = (new InvoicePayment())->formName();
		
		$dataInput = $this->rawDataInput;
		
		$number = $this->documentData['history'][0]['sections'][1]['number'];
		$dataInput[$invoiceTitleFormName]['dateOfIssue'] = $this->documentData['history'][0]['sections'][1]['dateOfIssue'];
		$dataInput[$invoiceTitleFormName]['dateOfSale'] = $this->documentData['history'][0]['sections'][1]['dateOfSale'];
		$dataInput[$invoiceTitleFormName]['sellerName'] = $this->documentData['history'][0]['sections'][1]['sellerName'];
		$dataInput[$invoiceTitleFormName]['sellerAddress'] = $this->documentData['history'][0]['sections'][1]['sellerAddress'];
		$dataInput[$invoiceTitleFormName]['sellerVatId'] = $this->documentData['history'][0]['sections'][1]['sellerVatId'];
		$dataInput[$invoiceTitleFormName]['buyerName'] = $this->documentData['history'][0]['sections'][1]['buyerName'];
		$dataInput[$invoiceTitleFormName]['buyerAddress'] = $this->documentData['history'][0]['sections'][1]['buyerAddress'];
		$dataInput[$invoiceTitleFormName]['buyerVatId'] = $this->documentData['history'][0]['sections'][1]['buyerVatId'];
		$dataInput[$invoiceTitleFormName]['isCopy'] = $this->documentData['history'][0]['sections'][1]['isCopy'];
		$dataInput[$invoiceTitleFormName]['orderId'] = $this->documentData['history'][0]['sections'][1]['orderId'];
		$dataInput[$invoiceTitleFormName]['companyId'] = $this->documentData['history'][0]['sections'][1]['companyId'];
		$dataInput[$invoiceTitleFormName]['referencedInvoiceId'] = $this->documentData['_id'];
		$dataInput[$invoiceTitleFormName]['referencedInvoiceHistoryId'] = $this->documentData['history'][0]['_id'];
		$dataInput[$invoiceTitleFormName]['referencedInvoiceType'] = $this->documentData['type'];
		$dataInput[$invoiceTitleFormName]['referencedInvoiceNumber'] = $number;
		$dataInput[$invoiceTitleFormName]['number'] = $number;
		
		if(!empty($this->documentData['history'][0]['sections'][1]['dateOfIssueDuplicate'])) {
			$dataInput[$invoiceTitleFormName]['dateOfIssueDuplicate'] = $this->documentData['history'][0]['sections'][1]['dateOfIssueDuplicate'];
		}
		
		if(isset($this->documentData['history'][0]['sections'][1]['contractTypeId']) && Yii::$app->params['isContractTypeVisible']) {
			$dataInput[$invoiceTitleFormName]['contractTypeId'] = $this->documentData['history'][0]['sections'][1]['contractTypeId'];
		}
		else {
			$dataInput[$invoiceTitleFormName]['contractTypeId'] = null;
		}
		
		if(isset($this->documentData['history'][0]['sections'][1]['orderTypeIds']) && Yii::$app->params['isOrderTypeVisible']) {
			$dataInput[$invoiceTitleFormName]['orderTypeIds'] = $this->documentData['history'][0]['sections'][1]['orderTypeIds'];
		}
		else {
			$dataInput[$invoiceTitleFormName]['orderTypeIds'] = null;
		}	
		
		$dataInput[$invoiceProductsFormName] = $this->documentData['history'][0]['sections'][2];
		
		$dataInput[$invoicePaymentFormName]['paymentMethod'] = $this->documentData['history'][0]['sections'][3]['paymentMethod'];
    	$dataInput[$invoicePaymentFormName]['paymentDate'] = $this->documentData['history'][0]['sections'][3]['paymentDate'];
    	$dataInput[$invoicePaymentFormName]['remarks'] = $this->documentData['history'][0]['sections'][3]['remarks'];    			
    	
		return $dataInput;
	}
}
