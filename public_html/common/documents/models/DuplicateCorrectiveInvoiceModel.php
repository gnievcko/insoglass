<?php
namespace common\documents\models;

use Yii;
use common\documents\sections\InvoiceTitle;
use common\documents\sections\InvoiceProducts;
use common\documents\sections\InvoiceProductsPrecorrected;
use common\documents\sections\CorrectiveInvoicePayment;
use common\helpers\StringHelper;

class DuplicateCorrectiveInvoiceModel {
	
	private $rawDataInput;
	private $documentData;
	
	public function __construct($rawDataInput, $documentData) {
		$this->rawDataInput = $rawDataInput;
		$this->documentData = $documentData;
	}
	
	public function prepareInputData($isEdit = false) {		
		$invoiceTitleFormName = (new InvoiceTitle())->formName();
		$invoiceProductsPrecorrectedFormName = (new InvoiceProductsPrecorrected())->formName();
		$invoiceProductsFormName = (new InvoiceProducts())->formName();
		$invoicePaymentFormName = (new CorrectiveInvoicePayment())->formName();
		
		$dataInput = $this->rawDataInput;
		
		$number = $this->documentData['history'][0]['sections'][1]['number'];
		$dataInput[$invoiceTitleFormName]['dateOfIssue'] = $this->documentData['history'][0]['sections'][1]['dateOfIssue'];
		$dataInput[$invoiceTitleFormName]['dateOfSale'] = $this->documentData['history'][0]['sections'][1]['dateOfSale'];
		$dataInput[$invoiceTitleFormName]['sellerName'] = $this->documentData['history'][0]['sections'][1]['sellerName'];
		$dataInput[$invoiceTitleFormName]['sellerAddress'] = $this->documentData['history'][0]['sections'][1]['sellerAddress'];
		$dataInput[$invoiceTitleFormName]['sellerVatId'] = $this->documentData['history'][0]['sections'][1]['sellerVatId'];
		$dataInput[$invoiceTitleFormName]['buyerName'] = $this->documentData['history'][0]['sections'][1]['buyerName'];
		$dataInput[$invoiceTitleFormName]['buyerAddress'] = $this->documentData['history'][0]['sections'][1]['buyerAddress'];
		$dataInput[$invoiceTitleFormName]['buyerVatId'] = $this->documentData['history'][0]['sections'][1]['buyerVatId'];
		$dataInput[$invoiceTitleFormName]['isCopy'] = $this->documentData['history'][0]['sections'][1]['isCopy'];
		$dataInput[$invoiceTitleFormName]['orderId'] = $this->documentData['history'][0]['sections'][1]['orderId'];
		$dataInput[$invoiceTitleFormName]['companyId'] = $this->documentData['history'][0]['sections'][1]['companyId'];
		$dataInput[$invoiceTitleFormName]['referencedCorrInvoiceId'] = $this->documentData['_id'];
		$dataInput[$invoiceTitleFormName]['referencedCorrInvoiceHistoryId'] = $this->documentData['history'][0]['_id'];
		$dataInput[$invoiceTitleFormName]['referencedCorrInvoiceType'] = $this->documentData['type'];
		$dataInput[$invoiceTitleFormName]['referencedCorrInvoiceNumber'] = $number;
		$dataInput[$invoiceTitleFormName]['referencedInvoiceId'] = $this->documentData['history'][0]['sections'][1]['referencedInvoiceId'];
		$dataInput[$invoiceTitleFormName]['referencedInvoiceHistoryId'] = $this->documentData['history'][0]['sections'][1]['referencedInvoiceHistoryId'];
		$dataInput[$invoiceTitleFormName]['referencedInvoiceNumber'] = $this->documentData['history'][0]['sections'][1]['referencedInvoiceNumber'];
		$dataInput[$invoiceTitleFormName]['referencedInvoiceDateOfIssue'] = $this->documentData['history'][0]['sections'][1]['referencedInvoiceDateOfIssue'];
		$dataInput[$invoiceTitleFormName]['number'] = $number;
		if($isEdit) {
			$dataInput[$invoiceTitleFormName]['number'] = $number;
			$dataInput[$invoiceTitleFormName]['referencedInvoiceNumber'] = $this->documentData['history'][0]['sections'][1]['referencedInvoiceNumber'];
		}
		
		$dataInput[$invoiceTitleFormName]['referencedInvoiceDateOfIssue'] = StringHelper::getFormattedDateFromDate($dataInput[$invoiceTitleFormName]['referencedInvoiceDateOfIssue']);
		
		if(!empty($this->documentData['history'][0]['sections'][1]['dateOfIssueDuplicate'])) {
			$dataInput[$invoiceTitleFormName]['dateOfIssueDuplicate'] = $this->documentData['history'][0]['sections'][1]['dateOfIssueDuplicate'];
		}
		
		if(isset($this->documentData['history'][0]['sections'][1]['contractTypeId']) && Yii::$app->params['isContractTypeVisible']) {
			$dataInput[$invoiceTitleFormName]['contractTypeId'] = $this->documentData['history'][0]['sections'][1]['contractTypeId'];
		}
		else {
			$dataInput[$invoiceTitleFormName]['contractTypeId'] = null;
		}
		
		if(isset($this->documentData['history'][0]['sections'][1]['orderTypeIds']) && Yii::$app->params['isOrderTypeVisible']) {
			$dataInput[$invoiceTitleFormName]['orderTypeIds'] = $this->documentData['history'][0]['sections'][1]['orderTypeIds'];
		}
		else {
			$dataInput[$invoiceTitleFormName]['orderTypeIds'] = null;
		}	
		
		/*if($isEdit) {
			$dataInput[$invoiceTitleFormName]['dateOfIssue'] = $this->documentData['history'][0]['sections'][1]['dateOfIssue'];
			$dataInput[$invoiceTitleFormName]['referencedInvoiceDateOfIssue'] = '';
			if(isset($this->documentData['history'][0]['sections'][1]['referencedInvoiceDateOfIssue'])) {
				$dataInput[$invoiceTitleFormName]['referencedInvoiceDateOfIssue'] = $this->documentData['history'][0]['sections'][1]['referencedInvoiceDateOfIssue'];
			}
		}
		else {
			$dataInput[$invoiceTitleFormName]['dateOfIssue'] = null;
			$dataInput[$invoiceTitleFormName]['referencedInvoiceDateOfIssue'] = $this->documentData['history'][0]['sections'][1]['dateOfIssue'];
		}*/
		
		$dataInput[$invoiceTitleFormName]['dateOfIssue'] = $this->documentData['history'][0]['sections'][1]['dateOfIssue'];
		
		$dataInput[$invoiceProductsPrecorrectedFormName] = $this->documentData['history'][0]['sections'][2];
		$dataInput[$invoiceProductsFormName] = $this->documentData['history'][0]['sections'][3];
		
		$dataInput[$invoicePaymentFormName]['paymentMethod'] = $this->documentData['history'][0]['sections'][4]['paymentMethod'];
    	$dataInput[$invoicePaymentFormName]['paymentDate'] = $this->documentData['history'][0]['sections'][4]['paymentDate'];
    	$dataInput[$invoicePaymentFormName]['remarks'] = $this->documentData['history'][0]['sections'][4]['remarks'];
    	$dataInput[$invoicePaymentFormName]['modifyReason'] = $this->documentData['history'][0]['sections'][4]['modifyReason'];
    	$dataInput[$invoicePaymentFormName]['priceDifference'] = $this->documentData['history'][0]['sections'][4]['priceDifference'];		    	
    	
		return $dataInput;
	}
}
