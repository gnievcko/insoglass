<?php
namespace common\documents\models;

use Yii;
use common\documents\sections\InvoiceTitle;
use common\documents\sections\InvoicePayment;
use common\documents\sections\InvoiceProducts;

class AdvanceInvoiceModel {
	
	private $rawDataInput;
	private $documentData;
	
	public function __construct($rawDataInput, $documentData) {
		$this->rawDataInput = $rawDataInput;
		$this->documentData = $documentData;
	}
	
	public function prepareInputData($isEdit = false) {		
		$invoiceTitleFormName = (new InvoiceTitle())->formName();
		$invoiceProductsFormName = (new InvoiceProducts())->formName();
		$invoicePaymentFormName = (new InvoicePayment())->formName();
		
		$dataInput = $this->rawDataInput;
		
		$number = $this->documentData['history'][0]['sections'][1]['number'];
		$dataInput[$invoiceTitleFormName]['dateOfSale'] = $this->documentData['history'][0]['sections'][1]['dateOfSale'];
		$dataInput[$invoiceTitleFormName]['sellerName'] = $this->documentData['history'][0]['sections'][1]['sellerName'];
		$dataInput[$invoiceTitleFormName]['sellerAddress'] = $this->documentData['history'][0]['sections'][1]['sellerAddress'];
		$dataInput[$invoiceTitleFormName]['sellerVatId'] = $this->documentData['history'][0]['sections'][1]['sellerVatId'];
		$dataInput[$invoiceTitleFormName]['buyerName'] = $this->documentData['history'][0]['sections'][1]['buyerName'];
		$dataInput[$invoiceTitleFormName]['buyerAddress'] = $this->documentData['history'][0]['sections'][1]['buyerAddress'];
		$dataInput[$invoiceTitleFormName]['buyerVatId'] = $this->documentData['history'][0]['sections'][1]['buyerVatId'];
		$dataInput[$invoiceTitleFormName]['isCopy'] = $this->documentData['history'][0]['sections'][1]['isCopy'];
		$dataInput[$invoiceTitleFormName]['orderId'] = $this->documentData['history'][0]['sections'][1]['orderId'];
		$dataInput[$invoiceTitleFormName]['companyId'] = $this->documentData['history'][0]['sections'][1]['companyId'];
		$dataInput[$invoiceTitleFormName]['referencedInvoiceId'] = $this->documentData['_id'];
		$dataInput[$invoiceTitleFormName]['referencedInvoiceHistoryId'] = $this->documentData['history'][0]['_id'];
		if($isEdit) {
			$dataInput[$invoiceTitleFormName]['number'] = $number;
		}
		
		if(isset($this->documentData['history'][0]['sections'][1]['contractTypeId']) && Yii::$app->params['isContractTypeVisible']) {
			$dataInput[$invoiceTitleFormName]['contractTypeId'] = $this->documentData['history'][0]['sections'][1]['contractTypeId'];
		}
		else {
			$dataInput[$invoiceTitleFormName]['contractTypeId'] = null;
		}
		
		if(isset($this->documentData['history'][0]['sections'][1]['orderTypeId']) && Yii::$app->params['isOrderTypeVisible']) {
			$dataInput[$invoiceTitleFormName]['orderTypeId'] = $this->documentData['history'][0]['sections'][1]['orderTypeId'];
		}
		else {
			$dataInput[$invoiceTitleFormName]['orderTypeId'] = null;
		}
		
		if($isEdit) {
			$dataInput[$invoiceTitleFormName]['dateOfIssue'] = $this->documentData['history'][0]['sections'][1]['dateOfIssue'];
		}
		else {
			$dataInput[$invoiceTitleFormName]['dateOfIssue'] = null;
		}
		
		$dataInput[$invoiceProductsFormName] = $this->documentData['history'][0]['sections'][2];
		
		$dataInput[$invoicePaymentFormName]['paymentMethod'] = $this->documentData['history'][0]['sections'][3]['paymentMethod'];
    	$dataInput[$invoicePaymentFormName]['paymentDate'] = $this->documentData['history'][0]['sections'][3]['paymentDate'];
    	$dataInput[$invoicePaymentFormName]['remarks'] = $this->documentData['history'][0]['sections'][3]['remarks'];
    	
    	if(isset($this->documentData['history'][0]['sections'][3]['dataBefore'])) {
    		$dataInput[$invoicePaymentFormName]['dataBefore'] = $this->documentData['history'][0]['sections'][3]['dataBefore'];
    	}
    	else {
    		$dataInput[$invoicePaymentFormName]['dataBefore'] = null;
    	}
    	
    	if(isset($this->documentData['history'][0]['sections'][3]['dataNow'])) {
    		$dataInput[$invoicePaymentFormName]['dataNow'] = $this->documentData['history'][0]['sections'][3]['dataNow'];
    	}
    	else {
    		$dataInput[$invoicePaymentFormName]['dataNow'] = null;
    	}
		
		return $dataInput;
	}
}
