<?php
namespace common\documents\models;

use Yii;
use common\documents\sections\InvoiceTitle;
use common\documents\sections\CorrectiveInvoicePayment;
use common\documents\sections\InvoiceProducts;
use common\documents\sections\InvoiceProductsPrecorrected;
use common\helpers\StringHelper;

class CorrectiveInvoiceModel {
	
	private $rawDataInput;
	private $documentData;
	
	public function __construct($rawDataInput, $documentData) {
		$this->rawDataInput = $rawDataInput;
		$this->documentData = $documentData;
	}
	
	public function prepareInputData($isEdit = false) {		
		$invoiceTitleFormName = (new InvoiceTitle())->formName();
		$invoiceProductsPrecorrectedFormName = (new InvoiceProductsPrecorrected())->formName();
		$invoiceProductsFormName = (new InvoiceProducts())->formName();
		$invoicePaymentFormName = (new CorrectiveInvoicePayment())->formName();
		
		$dataInput = $this->rawDataInput;
		
		$number = $this->documentData['history'][0]['sections'][1]['number'];		
		//$dataInput[$invoiceTitleFormName]['number'] = $number;
		$dataInput[$invoiceTitleFormName]['dateOfSale'] = $this->documentData['history'][0]['sections'][1]['dateOfSale'];
		$dataInput[$invoiceTitleFormName]['sellerName'] = $this->documentData['history'][0]['sections'][1]['sellerName'];
		$dataInput[$invoiceTitleFormName]['sellerAddress'] = $this->documentData['history'][0]['sections'][1]['sellerAddress'];
		$dataInput[$invoiceTitleFormName]['sellerVatId'] = $this->documentData['history'][0]['sections'][1]['sellerVatId'];
		$dataInput[$invoiceTitleFormName]['buyerName'] = $this->documentData['history'][0]['sections'][1]['buyerName'];
		$dataInput[$invoiceTitleFormName]['buyerAddress'] = $this->documentData['history'][0]['sections'][1]['buyerAddress'];
		$dataInput[$invoiceTitleFormName]['buyerVatId'] = $this->documentData['history'][0]['sections'][1]['buyerVatId'];
		$dataInput[$invoiceTitleFormName]['isCopy'] = $this->documentData['history'][0]['sections'][1]['isCopy'];
		$dataInput[$invoiceTitleFormName]['orderId'] = $this->documentData['history'][0]['sections'][1]['orderId'];
		$dataInput[$invoiceTitleFormName]['companyId'] = $this->documentData['history'][0]['sections'][1]['companyId'];
		$dataInput[$invoiceTitleFormName]['referencedInvoiceId'] = $this->documentData['_id'];
		$dataInput[$invoiceTitleFormName]['referencedInvoiceHistoryId'] = $this->documentData['history'][0]['_id'];
		$dataInput[$invoiceTitleFormName]['referencedInvoiceNumber'] = $number;
		if($isEdit) {
			$dataInput[$invoiceTitleFormName]['number'] = $number;
			$dataInput[$invoiceTitleFormName]['referencedInvoiceNumber'] = $this->documentData['history'][0]['sections'][1]['referencedInvoiceNumber'];
		}
		
		if(isset($this->documentData['history'][0]['sections'][1]['contractTypeId']) && Yii::$app->params['isContractTypeVisible']) {
			$dataInput[$invoiceTitleFormName]['contractTypeId'] = $this->documentData['history'][0]['sections'][1]['contractTypeId'];
		}
		else {
			$dataInput[$invoiceTitleFormName]['contractTypeId'] = null;
		}
		
		if(isset($this->documentData['history'][0]['sections'][1]['orderTypeId']) && Yii::$app->params['isOrderTypeVisible']) {
			$dataInput[$invoiceTitleFormName]['orderTypeId'] = $this->documentData['history'][0]['sections'][1]['orderTypeId'];
		}
		else {
			$dataInput[$invoiceTitleFormName]['orderTypeId'] = null;
		}
		
		if($isEdit) {
			$dataInput[$invoiceTitleFormName]['dateOfIssue'] = $this->documentData['history'][0]['sections'][1]['dateOfIssue'];
			$dataInput[$invoiceTitleFormName]['referencedInvoiceDateOfIssue'] = '';
			if(isset($this->documentData['history'][0]['sections'][1]['referencedInvoiceDateOfIssue'])) {
				$dataInput[$invoiceTitleFormName]['referencedInvoiceDateOfIssue'] = $this->documentData['history'][0]['sections'][1]['referencedInvoiceDateOfIssue'];
			}
		}
		else {
			$dataInput[$invoiceTitleFormName]['dateOfIssue'] = null;
			$dataInput[$invoiceTitleFormName]['referencedInvoiceDateOfIssue'] = $this->documentData['history'][0]['sections'][1]['dateOfIssue'];
		}
		
		$dataInput[$invoiceTitleFormName]['referencedInvoiceDateOfIssue'] = StringHelper::getFormattedDateFromDate($dataInput[$invoiceTitleFormName]['referencedInvoiceDateOfIssue']);
		
		$productsPrecorrectedSectionIndex = count($this->documentData['history'][0]['sections']) > 4 ? 2 : null; 
		$productsSectionIndex = count($this->documentData['history'][0]['sections']) > 4 ? 3 : 2;
		
		$dataInput[$invoiceProductsFormName] = $this->documentData['history'][0]['sections'][$productsSectionIndex];
		if(!empty($productsPrecorrectedSectionIndex)) {
			$dataInput[$invoiceProductsPrecorrectedFormName] = $this->documentData['history'][0]['sections'][$productsPrecorrectedSectionIndex];
		}
		elseif(empty(Yii::$app->request->isPost) && empty(Yii::$app->request->isAjax) && empty(Yii::$app->request->get('edit', 0))) {
			$dataInput[$invoiceProductsPrecorrectedFormName] = $dataInput[$invoiceProductsFormName];
		}
		
		$paymentSectionIndex = count($this->documentData['history'][0]['sections']) > 4 ? 4 : 3;
		
		$dataInput[$invoicePaymentFormName]['paymentMethod'] = $this->documentData['history'][0]['sections'][$paymentSectionIndex]['paymentMethod'];
    	$dataInput[$invoicePaymentFormName]['paymentDate'] = $this->documentData['history'][0]['sections'][$paymentSectionIndex]['paymentDate'];
    	$dataInput[$invoicePaymentFormName]['remarks'] = $this->documentData['history'][0]['sections'][$paymentSectionIndex]['remarks'];
    	
    	if(isset($this->documentData['history'][0]['sections'][$paymentSectionIndex]['dataBefore'])) {
    		$dataInput[$invoicePaymentFormName]['modifyReason'] = $this->documentData['history'][0]['sections'][$paymentSectionIndex]['dataBefore'];
    	}
    	elseif(isset($this->documentData['history'][0]['sections'][$paymentSectionIndex]['modifyReason'])) {
    		$dataInput[$invoicePaymentFormName]['modifyReason'] = $this->documentData['history'][0]['sections'][$paymentSectionIndex]['modifyReason'];
    	}
    	
    	if(isset($this->documentData['history'][0]['sections'][$paymentSectionIndex]['priceDifference'])) {
    		$dataInput[$invoicePaymentFormName]['priceDifference'] = $this->documentData['history'][0]['sections'][$paymentSectionIndex]['priceDifference'];
    	}
    	else {
    		$dataInput[$invoicePaymentFormName]['priceDifference'] = StringHelper::getFormattedCost(0, true);
    	}
		
		return $dataInput;
	}
}
