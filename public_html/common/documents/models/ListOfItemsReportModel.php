<?php
namespace common\documents\models;

use common\models\aq\WarehouseProductQuery;
use common\models\aq\WarehouseProductStatusHistoryQuery;
use common\helpers\ArrayHelper;

class ListOfItemsReportModel {
	
	public function prepareProductsByDateRange($dateFrom, $dateTo, $categoryIds) {
		$currentProducts = WarehouseProductQuery::getProducts($categoryIds);
		$dateNow = (new \DateTime())->format('Y-m-d');
		
		$data = [];
		$data['from'] = [];
		$data['to'] = $currentProducts;
		
		if($dateTo != $dateNow) {
			$tempDateTo = new \DateTime($dateTo);
			// jesli chcemy tylko dla jednego dnia w przeszlosci, to prawdopodobnie interesuje nas stan na koniec dnia
			// trzeba wiec ominac operacje w dniu dateTo
			if($dateFrom == $dateTo) {
				$tempDateTo->add(new \DateInterval('P1D'));
			}
			
			$balanceTo = WarehouseProductStatusHistoryQuery::getBalanceFromDateRange($tempDateTo->format('Y-m-d'), $dateNow);
			foreach($balanceTo as $row) {
				$index = ArrayHelper::searchAssociativeArrayForValue($data['to'], 'id', $row['productId']);
				if(!is_null($index)) {
					$sign = !empty($row['isPositive']) ? -1 : 1;
					$data['to'][$index]['count'] += ($sign * $row['count']);
				}
			}
		}
		
		if($dateFrom != $dateTo) {
			$data['from'] = $data['to'];
			$intDateTo = new \DateTime($dateTo);
			$intDateTo->sub(new \DateInterval('P1D'));
			
			$balanceFrom = WarehouseProductStatusHistoryQuery::getBalanceFromDateRange($dateFrom, $intDateTo->format('Y-m-d'));
			foreach($balanceFrom as $row) {
				$index = ArrayHelper::searchAssociativeArrayForValue($data['from'], 'id', $row['productId']);
				if(!is_null($index)) {
					$sign = !empty($row['isPositive']) ? -1 : 1;
					$data['from'][$index]['count'] += ($sign * $row['count']);
				}
			}
		}
		
		return $data;
	}
}
