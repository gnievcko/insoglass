<?php

namespace common\documents\pdf;

use Yii;
use kartik\mpdf\Pdf;
use yii\helpers\ArrayHelper;
use common\documents\Document;
use common\documents\Renderer as DocumentRenderer;
use common\documents\html\Renderer as HtmlRenderer;

class Renderer implements DocumentRenderer {

    private $pdf;
    private $htmlRenderer;
    private $config;
    private $filename;

    public function __construct(array $config = [], $filename = null) {
        $this->htmlRenderer = new HtmlRenderer();
        $this->config = $config;
        $this->config['cssFile'] = ArrayHelper::getValue($config, 'cssFile', __DIR__.'/style.css');
        $this->filename = $filename;
    }

    public function render(Document $document) {
        $documentWasEditable = $document->isEditable();
        $document->makeReadOnly();

        //TODO Artur o co chodzi?
        //$renderedDocument = $this->renderToPdf($document);

        // if($documentWasEditable) {
        //    $document->makeEditable();
        //}

        return $this->renderToPdf($document);
    }

    private function renderToPdf(Document $document) {
        $this->setIfNotSet('orientation', $document->orientation);
        $pdf = new Pdf($this->config);
        $mpdf = $pdf->api;
        $mpdf->mPDF('utf-8', $document->format, '', '', $document->marginLeft, $document->marginRight, $document->marginTop, $document->marginBottom); 
        //$mpdf = new \mPDF('utf-8', $document->format, '', '', $document->marginLeft, $document->marginRight, $document->marginTop, $document->marginBottom);
        
        $mpdf->SetHTMLHeader(Yii::$app->controller->renderFile(__DIR__.'/views/header.php', ['document' => $document]));
        $pdf->content = Yii::$app->controller->renderFile(__DIR__.'/views/body.php', ['document' => $document]);
        $mpdf->SetHTMLFooter(Yii::$app->controller->renderFile(__DIR__.'/views/footer.php', ['document' => $document]));
        
        if(!empty($this->filename)) {
        	$pdf->filename = $this->filename.'.pdf';
        }
        
        $output = $pdf->render();
        return $output;
    }

    private function setIfNotSet($configParam, $value) {
        $this->config[$configParam] = isset($this->config[$configParam]) ? $this->config[$configParam] : $value;
    }
}
