<?php
    $title = Yii::t('documents', 'Report of progress and losses during production');
    $this->title = $title.' - '.Yii::$app->name;
?>

<div class="text-center"><h2><i><?= mb_strtoupper($title, 'UTF-8') ?></i></h2></div>

<div class="text-center">
	<?= Yii::t('web', 'For period') . ': ' . $section->dateFrom . ' - ' .$section->dateTo ?>
</div>

<div class="text-center"><?= Yii::t('web', 'Generated for') . ': ' . $section->user ?></div>

<div class="text-center"><?= Yii::t('web', 'Date of generation') . ': ' . $section->dateGenerated ?></div>



