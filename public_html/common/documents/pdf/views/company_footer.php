<?php use yii\helpers\Html;
if($section->isVisible) { ?>
	<div class="text-center">
		<?= Html::img(Yii::$app->getUrlManager()->getBaseUrl().'/images/ue_report.jpg', ['style' => 'width: 80%; margin-bottom: 8px']) ?>
	</div>
<?php } ?>
