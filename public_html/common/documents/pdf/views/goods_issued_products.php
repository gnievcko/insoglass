<?php 
    use yii\web\View;
    use yii\helpers\Html;
    use yii\helpers\Url;

    $edit = $section->isEditable();

    $this->registerJsFile('@web/js/documents/goods-issued-products.js?'.uniqid(), ['depends' => [\yii\web\JqueryAsset::className()]]);
?>

<div class="goods-issued-products" >
    <?php if($edit): ?>
        <span class="add-goods-issued-product-button">
            <span class="glyphicon glyphicon-plus-sign"></span>
            <span><?= Yii::t('web', 'Add product') ?></span>
        </span>
    <?php endif; ?>
    
    <table class="table-protocol" border="1">
        <thead>
            <tr>
            	<th>
            		<?= Yii::t('documents', 'No.') ?>
            	</th>
                <th>
                    <?= $section->getAttributeLabel('name') ?>
                </th>
                <th>
                    <?= $section->getAttributeLabel('location') ?>
                </th>
                <th>
                    <?= $section->getAttributeLabel('count') ?>
                </th>
                <th>
                	<?= $section->getAttributeLabel('unit') ?>
                </th>
                <?php if($edit): ?>
                    <th></th>
                <?php endif; ?>
            </tr>
        </thead>
        <tbody>
            <?= Yii::$app->controller->renderFile(__DIR__.'/goods-issued-products/table-body.php', ['section' => $section] + (isset($form) ? ['form' => $form] : [])) ?>
        </tbody>
    </table>
    <div class="ajax-loader">
        <?php //echo Html::img('@web/images/ajax-loader-big-circle.gif') ?>
    </div>

    <?php if($edit): ?>
        <span class="add-goods-issued-product-button">
            <span class="glyphicon glyphicon-plus-sign"></span>
            <span><?= Yii::t('web', 'Add product') ?></span>
        </span>
    <?php endif; ?>

</div>

<?php
    $this->registerJs('
        (function() {
            initializeGoodsIssuedProductsTables(
                "goods-issued-products", 
                {
                    productRowUrl: "'.Url::to(['document/get-goods-issued-product-row']).'",
                    edit: '.(int)$edit.'
                }
            );
        })()
    ', View::POS_END);
?>
