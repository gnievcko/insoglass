<?php 
    use yii\web\View;
    use yii\helpers\Html;
    use yii\helpers\Url;

    $edit = false;

    $this->registerJsFile('@web/js/documents/invoice-products.js?'.uniqid(), ['depends' => [\yii\web\JqueryAsset::className()]]);
?>

<div class="invoice-products">
	<?php 
        echo Html::activeHiddenInput($section, 'currencyId')
    ?>
    <h3 style="margin-top: 15px"><?= Yii::t('documents', 'Data before correction') ?></h3>
    <table class="table-protocol" border="1" style="margin: 10px 0px">
        <thead>
            <tr>
                <th style="width: 20px">
                    <?= $section->getAttributeLabel('no') ?>
                </th>
                <th style="width: 150px">
                    <?= $section->getAttributeLabel('name') ?>
                </th>
                <th style="width: 30px">
                    <?= $section->getAttributeLabel('count') ?>
                </th>
                <th style="width: 30px">
                    <?= $section->getAttributeLabel('unit') ?>
                </th>
                <th style="width: 30px">
                	<?= $section->getAttributeLabel('category') ?>
                </th>
                <th style="width: 90px">
                    <?= $section->getAttributeLabel('netPrice') ?>
                </th>
                <th style="width: 90px">
                    <?= $section->getAttributeLabel('netValue') ?>
                </th>
                <th style="width: 30px">
                    <?= $section->getAttributeLabel('vatRate') ?>
                </th>
                <th style="width: 80px">
                    <?= $section->getAttributeLabel('vatAmount') ?>
                </th>
                <th style="width: 100px">
                    <?= $section->getAttributeLabel('grossValue') ?>
                </th>
                <?php if($edit): ?>
                    <th></th>
                <?php endif; ?>
            </tr>
        </thead>
        <tbody>
            <?= Yii::$app->controller->renderFile(__DIR__.'/invoice-products/table-body.php', ['section' => $section, 'forceStatic' => true] + (isset($form) ? ['form' => $form] : [])) ?>
        </tbody>
    </table>
    <div style="font-size: 12px">
	    <div>
	        <?= $section->getAttributeLabel('amountVerbally').': '.$section->amountVerbally ?>
	    </div>
    </div>
</div>

<?php
    $this->registerJs('
        (function() {
            initializeInvoiceProductsTables(
                "invoice-products", 
                {
                    calculationsUrl: "'.Url::to(['document/calculate-invoice-amounts']).'",
                    productRowUrl: "'.Url::to(['document/get-invoice-product-row']).'",
                    edit: '.(int)$edit.',
                    initialCurrencySymbol: "'.$section->getCurrencySymbol().'"
                }
            );
        })()
    ', View::POS_END);
?>
