<?php

use kartik\datecontrol\DateControl;
use Mindseater\PHPUtils\Dates\Formatter;

$edit = $section->isEditable();
$dateFormatter = new Formatter();
?>
<br/>
<div class="title-with-subtitle text-center">
    <h2 style="margin-top:25px"><?= mb_strtoupper(Yii::t('documents', 'Work acceptance protocol'), 'utf-8') ?></h2>
    <h3 style="margin-bottom: 25px;">
          <?= mb_strtolower($section->getAttributeLabel('date'), 'utf-8') ?>:
          <?= //!empty($section->date) ? $dateFormatter->from($section->date, 'Y-m-d')->format('d.m.Y')->getDate() : '..........' 
				!empty($section->date) ? $section->date : '..........' 
			?>
    </h3>
</div>
