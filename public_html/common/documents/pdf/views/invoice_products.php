<?php 
    use yii\web\View;
    use yii\helpers\Html;
    use yii\helpers\Url;

    $edit = $section->isEditable();

    $this->registerJsFile('@web/js/documents/invoice-products.js?'.uniqid(), ['depends' => [\yii\web\JqueryAsset::className()]]);
?>

<div class="invoice-products">
	<?php 
		$isCorrective = strpos(Yii::$app->requestedRoute, 'corrective-invoice') !== false;
		if($isCorrective) { ?>
			<h3 style="margin-top: 15px"><?= Yii::t('documents', 'Data after correction') ?></h3>
		<?php } ?>
    <?php 
        echo $edit ? $form->field($section, 'currencyId')->dropDownList($section->getCurrencies(), ['class' => 'currency-select form-control']) 
                   : Html::activeHiddenInput($section, 'currencyId')
    ?>
    <?php if($edit): ?>
        <span class="add-invoice-product-button">
            <span class="glyphicon glyphicon-plus-sign"></span>
            <span><?= Yii::t('web', 'Add product') ?></span>
        </span>
    <?php endif; ?>
    <table class="table-protocol" border="1" style="margin: <?= $isCorrective ? '10px' : '25px' ?> 0px">
        <thead>
            <tr>
                <th style="width: 20px">
                    <?= $section->getAttributeLabel('no') ?>
                </th>
                <th style="width: 150px">
                    <?= $section->getAttributeLabel('name') ?>
                </th>
                <th style="width: 30px">
                    <?= $section->getAttributeLabel('count') ?>
                </th>
                <th style="width: 30px">
                    <?= $section->getAttributeLabel('unit') ?>
                </th>
                <th style="width: 30px">
                	<?= $section->getAttributeLabel('category') ?>
                </th>
                <th style="width: 90px">
                    <?= $section->getAttributeLabel('netPrice') ?>
                </th>
                <th style="width: 90px">
                    <?= $section->getAttributeLabel('netValue') ?>
                </th>
                <th style="width: 30px">
                    <?= $section->getAttributeLabel('vatRate') ?>
                </th>
                <th style="width: 80px">
                    <?= $section->getAttributeLabel('vatAmount') ?>
                </th>
                <th style="width: 100px">
                    <?= $section->getAttributeLabel('grossValue') ?>
                </th>
                <?php if($edit): ?>
                    <th></th>
                <?php endif; ?>
            </tr>
        </thead>
        <tbody>
            <?= Yii::$app->controller->renderFile(__DIR__.'/invoice-products/table-body.php', ['section' => $section] + (isset($form) ? ['form' => $form] : [])) ?>
        </tbody>
    </table>
    <div style="font-size: 12px">
	    <?php if($edit): ?>
	        <div class="ajax-loader">
	            <?= Html::img('@web/images/ajax-loader-big-circle.gif') ?>
	        </div>
	
	        <div class="row">
	            <div class="col-sm-6">
	                <span class="add-invoice-product-button">
	                    <span class="glyphicon glyphicon-plus-sign"></span>
	                    <span><?= Yii::t('web', 'Add product') ?></span>
	                </span>  
	            </div>
	            <div class="col-sm-6 text-right">
	                <span class="remove-invoice-products-button">
	                    <span class="glyphicon glyphicon-minus-sign"></span>
	                    <span><?= Yii::t('web', 'Remove all products') ?></span>
	                </span>
	            </div>
	        </div>
	    <?php endif; ?>
	
	    <div>
	        <?= $edit ? $form->field($section, 'amountVerbally') : $section->getAttributeLabel('amountVerbally').': '.$section->amountVerbally ?>
	    </div>
    </div>
</div>

<?php
    $this->registerJs('
        (function() {
            initializeInvoiceProductsTables(
                "invoice-products", 
                {
                    calculationsUrl: "'.Url::to(['document/calculate-invoice-amounts']).'",
                    productRowUrl: "'.Url::to(['document/get-invoice-product-row']).'",
                    edit: '.(int)$edit.',
                    initialCurrencySymbol: "'.$section->getCurrencySymbol().'"
                }
            );
        })()
    ', View::POS_END);
?>
