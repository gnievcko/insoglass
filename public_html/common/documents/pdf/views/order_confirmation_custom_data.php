<?php 	    	
	$prevLanguage = Yii::$app->language;
	Yii::$app->language = $section->getLanguage();
?>
<div style="font-size: 11px; font-weight: bold;margin-top:15px;">
	<?php if(!empty($section->prepaymentAmount)):?>
	<div>
		<?= strtoupper(\Yii::t('documents', 'Prepayment')) ?>
	    <div style="width:60%">
	    	<?= $section->getAttributeLabel('prepaymentAmount') .' : '. $section->prepaymentAmount ?>
	    </div>
	</div>
	<?php endif;?>
	<?php if(!empty($section->companyBank) || !empty($section->companyBankAccount)):?>
	<div class="col-xs-12 row">
	    <div style="width:60%">
	    	<?= \Yii::t('documents', 'Bank account number') .' : '. $section->companyBank .' '. $section->companyBankAccount ?>
	    </div>
	</div>
	<?php endif;?>
	<?php if(!empty($section->companySwift)):?>
	<div>
	    <div style="width:60%">
	    	<?= $section->getAttributeLabel('companySwift') .' : '. $section->companySwift ?>
	    </div>
	</div>
	<?php endif;?>
</div>
<?php	Yii::$app->language = $prevLanguage; ?>
