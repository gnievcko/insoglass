<?php
    use yii\helpers\Html;
    use Mindseater\PHPUtils\Lists\AlphabeticalEnumerator;
    use Mindseater\PHPUtils\Dates\Formatter;
    use Mindseater\PHPUtils\Arrays\ValuesReplacer;
    use Mindseater\PHPUtils\Text\StringSequenceGenerator;

    $dotsGenerator = new StringSequenceGenerator('.');

?>
<br/>
<div class="acceptance-protocol-section">
    <div>1. <?= $section->getAttributeLabel('orderingParty') ?>:</div>
    <div class="indentation"><b><?= $section->orderingParty ?></b></div>
</div>
<br/>
<div class="acceptance-protocol-section">
    <div>2. <?= $section->getAttributeLabel('object') ?>:</div>
    <div class="indentation"><b><?= $section->object ?></b></div>
</div>
<br/>
<div class="acceptance-protocol-section">
    <div>3. <?= $section->getAttributeLabel('orderDescription') ?>:</div>
    <div class="indentation"><b><?= nl2br($section->orderDescription) ?></b></div>
</div>
<br/>
<div class="acceptance-protocol-section">
    <div>4. <?= $section->getAttributeLabel('works') ?>:</div>
    <div class="indentation"><?= nl2br($section->works) ?></div>
</div>
<br/>
<div class="acceptance-protocol-section">
    <div>5. <?= $section->getAttributeLabel('executor') ?>:</div>
    <div class="indentation"><?= $section->executor ?></div>
</div>
<br/>
<?php 
    $enumerator = new AlphabeticalEnumerator(') '); 
    $dotsGenerator = new StringSequenceGenerator('.');
    $numberOfDotsForNames = 50;
    $numberOfDotsForText = 486;
?>

<div class="acceptance-protocol-section" style="margin-top: 35px; margin-bottom: 35px">
    <div>6. <?= Yii::t('documents', 'Supervising board consisting of') ?>:</div>
    <div style="margin-top: 10px" class="indentation">
        <div style="float: left; width: 50%">
            <div class="bold">
                <?= Yii::t('documents', 'Ordering party') ?>
            </div>
            <?php $filteredNames = (new ValuesReplacer($section->orderingPartySupervisingBoard))->replaceEmpty($dotsGenerator->repeat($numberOfDotsForNames)) ?>
            <?php foreach($enumerator->enumerate($filteredNames) as $orderingPartyListItem): ?>
                <div style="margin-top:10px">
                    <?= $orderingPartyListItem ?>
                </div>
            <?php endforeach ?>
        </div>
        <div style="float: right; width: 50%">
            <div class="bold">
                <?= Yii::t('documents', 'Executor') ?>
            </div>
            <?php $filteredNames = (new ValuesReplacer($section->executorSupervisingBoard))->replaceEmpty($dotsGenerator->repeat($numberOfDotsForNames)) ?>
            <?php foreach($enumerator->enumerate($filteredNames) as $executorListItem): ?>
                <div style="margin-top:10px">
                    <?= $executorListItem ?>
                </div>
            <?php endforeach ?>
        </div>
    </div>
</div>
<br/><br/>
<?php 
    $datesFormatter = new Formatter();
    $originalDatesFormat = 'Y-m-d';
    $datesFormatToDisplay = 'd.m.Y';
?>
<div class="acceptance-protocol-section" style="margin-top: 10px; margin-bottom: 20px">
    <?= Yii::t('documents', 'Works began at') ?> <?= !empty($section->worksBeginDate) ? $datesFormatter->from($section->worksBeginDate, $originalDatesFormat)->format($datesFormatToDisplay)->getDate() : '...........................' ?>, 
    <?= mb_strtolower(Yii::t('documents', 'Finished at'), 'utf-8') ?> 
    <?= !empty($section->worksEndDate) ? $datesFormatter->from($section->worksEndDate, $originalDatesFormat)->format($datesFormatToDisplay)->getDate() : '...........................' ?>
</div><br/>
<div class="acceptance-protocol-section">
    <?= Yii::t('documents', 'Supervising board consisting of the members mentioned above has revieved the tasks issued and stated that the tasks had been executed') ?>
    <?= Html::tag($section->wasExecutedInComplianceWithOrder ? 'span' : /*'strike'*/ 'span', mb_strtolower(Yii::t('documents', 'In compliance'), 'utf-8')) ?>
    /
    <?= Html::tag($section->wasExecutedInComplianceWithOrder ? /*'strike'*/ 'span' : 'span', mb_strtolower(Yii::t('documents', 'Not in compliance'), 'utf-8')) ?>
    <?= mb_strtolower(Yii::t('documents', 'With the order'), 'utf-8') ?>, 
    <?= Html::tag($section->hasFlaws ? /*'strike'*/ 'span' : 'span', mb_strtolower(Yii::t('documents', 'Without flaws'), 'utf-8')) ?>
    /
    <?= Html::tag($section->hasFlaws ? 'span' : /*'strike'*/ 'span', mb_strtolower(Yii::t('documents', 'With flaws'), 'utf-8')) ?>
</div>
<div>
	<?= $section->flawsDescription ? nl2br($section->flawsDescription) : $dotsGenerator->repeat($numberOfDotsForText) ?>
</div>
<div class="acceptance-protocol-section" style="margin-top: 10px">
    <?= Yii::t('documents', 'Contractor is oblidged to fix the flaws by') ?>:
    <?= empty($section->flawsRemovalDate) ? $dotsGenerator->repeat($numberOfDotsForNames) : $datesFormatter->from($section->flawsRemovalDate, $originalDatesFormat)->format($datesFormatToDisplay)->getDate() ?>
</div>

<div class="acceptance-protocol-section" style="margin-top: 20px">
    <div>7. <?= $section->getAttributeLabel('remarks') ?>:</div>
    <div class="remarks">
        <?= $section->remarks ? nl2br($section->remarks) :  $dotsGenerator->repeat(1014) ?>
    </div>
</div>

<?= $this->renderFile(__DIR__.'/acceptance-protocol/signatures.php'); ?>
