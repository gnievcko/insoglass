<?php 
$editMode = $section->isEditable();
    
$this->title = Yii::t('documents', 'Proforma invoice').' - '.Yii::$app->name;
?>

<div class="text-center" style="margin: 30px 0px">
    <div>
        <h3 style="margin: 0px"><?= $editMode ? $form->field($section, 'number')->textInput(['readonly' => true]) : $section->getAttributeLabel('number').': '.$section->number ?></h3> 
    </div>
</div>

<?php
include('invoice-title/invoice-title-content.php');
?>
