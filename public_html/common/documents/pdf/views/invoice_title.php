<?php 
use yii\helpers\Html;
$editMode = $section->isEditable();
    
$this->title = Yii::t('documents', 'Invoice').' - '.Yii::$app->name;
?>

<div class="text-center" style="margin: 30px 0px">
    <div>
        <h3 style="margin: 0px"><?= $editMode ? $form->field($section, 'number')->textInput(['readonly' => true]) : $section->getAttributeLabel('number').': '.$section->number ?></h3> 
    </div>
    <div>
        <h4 style="margin: 0px">(<?= 
            $editMode 
                ? $form->field($section, 'isCopy')->radioList([
                    0 => Yii::t('documents', 'Original'), 
                    1 => Yii::t('documents', 'Copy')])->label(false)
                : Html::tag($section->isCopy ? 's' : 'span', Yii::t('documents', 'Original')).' / '.Html::tag($section->isCopy ? 'span' : 's', Yii::t('documents', 'Copy'))
        ?>)</h4>
    </div>
</div>

<?php
include('invoice-title/invoice-title-content.php');
?>
