<?php
$edit = $section->isEditable();
    
$this->title = Yii::t('documents', 'Custom protocol').' - '.Yii::$app->name;
?>

 <div class="text-center">
	<div style="margin: 25px 0px">
        <h2 class="upper" style="margin-bottom: 0px"><i><?= $edit ? $form->field($section, 'title')->textInput()->label(false) : $section->title ?></i></h2> 
        <h3 class="mt-0">
            <i><?= $section->getAttributeLabel('date') ?></i>
        	<?= !empty($section->date) ? $section->date : '..........' ?>
		</h3>
	</div>
</div>