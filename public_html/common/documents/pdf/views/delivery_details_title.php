<?php 
use yii\helpers\Html;
use kartik\datecontrol\DateControl;
use kartik\datecontrol\Module;
    
$editMode = $section->isEditable();
    
$this->title = \Yii::t('web', 'Delivery details').' - '.\Yii::$app->name;
?>

<div class="text-center">
    <div>
        <h2><i><?= \Yii::t('web', 'Delivery details') ?> / Delivery details</i></h2> 
    </div>
</div>

<table class="goods-issued-list">
	<tr>
		<th><?= $section->getAttributeLabel('number') ?></th>
		<td><?= $editMode ? $form->field($section, 'number')->textInput(['readonly' => true])->label(false) : $section->number ?></td>
	</tr>
	<tr>
		<th><?= $section->getAttributeLabel('warehouseName') ?></th>
		<td><?= $editMode ? $form->field($section, 'warehouseName')->textInput()->label(false) : $section->warehouseName ?></td>
	</tr>
	<tr>
		<th><?= $section->getAttributeLabel('supplierName') ?></th>
		<td><?= $editMode ? $form->field($section, 'supplierName')->textInput()->label(false) : $section->supplierName ?></td>
	</tr>
	<tr>
		<th><?= $section->getAttributeLabel('userOrderingName') ?></th>
		<td><?= $editMode ? $form->field($section, 'userOrderingName')->textInput()->label(false) : $section->userOrderingName?></td>
	</tr>
	<tr>
		<th><?= $section->getAttributeLabel('dateOrder') ?></th>
		<td><?=
			$editMode 
				? $form->field($section, 'dateOrder')->widget(DateControl::classname(), [
	                    'language' => \Yii::$app->language,
	                    'type' => DateControl::FORMAT_DATETIME,
	                    'ajaxConversion' => false,
	                    'displayFormat' => 'php:d.m.Y H:i',
						'saveFormat' => 'php:d.m.Y H:i',
	                    'options' => [
		                        'pluginOptions' => [
		                            	'autoclose' => true
		                        ]
	                    ]
	                ])->label(false)
            	: $section->dateOrder
		?></td>
	</tr>
    <tr>
    	<th><?= $section->getAttributeLabel('description') ?></th>
    	<td><?= $editMode ? $form->field($section, 'description')->textarea()->label(false) : ($section->description ?: '-') ?></td>
    </tr>
</table>
