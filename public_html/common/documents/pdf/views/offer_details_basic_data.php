<?php
    $this->title = Yii::t('documents', 'Offer details protocol').' - '.Yii::$app->name;
?>

<h2 class="margin-0"><?= \common\helpers\StringHelper::translateOrderToOffer('main', 'Order') ?></h2>
<div class="row offer-number">
    <div class="col-sm-6">
        <span><?= Yii::t('documents_orders', 'No.') ?></span>
        <span><?= $section->number ?></span>
    </div>
    <div class="col-sm-6 text-right">
        <?= $section->dateCreation ?>
    </div>
</div>

<div class="offer-details-bordered-title">
   <?= Yii::t('documents', 'To Mr/Mrs')?>:
</div>
<div>
	<div class="col-sm-12">
		<?= nl2br($section->contactPeople) ?>
	</div>
	<?php if(!empty($section->contactPerson)) { ?>
	    <div>
	        <?= $section->contactPerson ?>
	    </div>
    <?php } ?>
    <?php if(!empty($section->clientCompanyParent)) { ?>
    	<div>
	        <?= $section->clientCompanyParent ?>
	    </div>
    <?php } ?>
    <div>
        <?= $section->clientCompany ?>
    </div>
    <div>
        <?= $section->clientAddress ?>
    </div>
    <?php if(!empty($section->clientPhone)) { ?>
        <div>
            <span><?= $section->getAttributeLabel('clientPhone') ?>:</span>
            <?= $section->clientPhone ?>
        </div>
    <?php } 
    if(!empty($section->clientEmail)) { ?>
        <div>
            <span><?= $section->getAttributeLabel('clientEmail') ?>:</span>
            <?= $section->clientEmail ?>
        </div>
    <?php } ?>
</div>

<div class="offer-details-bordered-title">
    <?= Yii::t('documents_orders', 'From')?>:
</div>
<div>
    <div>
        <?= $section->authorCompany ?>
    </div>
    <div>
        <?= $section->authorAddress ?>
    </div>
    <div>
        <?= $section->authorSalesman ?>
    </div>
    <?php if(!empty($section->authorPosition)) { ?>
    	<?= $section->authorPosition ?>
    <?php } ?>
    <div>
        <?= $section->authorPhone ?>
    </div>
    <div>
        <?= $section->authorFax ?>
    </div>
    <div>
        <?= $section->authorEmail ?>
    </div>
</div>
<?php if (!empty($section->getAttributeLabel('title')) && !empty($section->title)) { ?>
<div class="offer-details-bordered-title">
    <span>
        <?= $section->getAttributeLabel('title').':' ?>
    </span>
    <span>
        <?= $section->title ?>
    </span>
</div>
<?php } ?>

<?php if (!empty($section->description)) { ?>
	<div class="offer-details-bordered-title" style="margin: 20px 0px">
	    <?= nl2br($section->description) ?>
	</div>
<?php }?>

