<?php 
use common\helpers\Utility;
$this->title = $section->getAttributeLabel('report-orders').' - '.Yii::$app->name;
?>

<div class="text-center"><h2><i><?= $section->getAttributeLabel('report-goods-issued') ?></i></h2></div>

<div class="text-center">
	<?= $section->getAttributeLabel('forPeriod') . ': ' .  
	$section->dateFrom . ' - ' .
	$section->dateTo ?>
</div>

<div class="text-center"><?= $section->getAttributeLabel('generatedFor') . ': ' . $section->user ?></div>

<div class="text-center"><?= $section->getAttributeLabel('generationDate') . ': ' . $section->dateGenerated ?></div>

<br><br>

<?php if(!empty($section->data)) {?>
	<?php foreach($section->data as $row) {?>
	
		<div>
			<div style="float: left;width:200px"><?= $section->getAttributeLabel('userConfirming') . ':' ?></div>
			<div style="float: left;width:200px;"><?= $row['header']['userConfirmingFirstName'] . ' ' . $row['header']['userConfirmingLastName'] ?> </div>
		</div>
		<div>
			<div style="float: left;width:200px"><?= $section->getAttributeLabel('issuingType') . ':' ?></div>
			<div style="float: left;width:200px;"><?= $row['header']['productStatusName']?> </div>
		</div>
		<?php if($row['header']['productStatusSymbol'] == Utility::PRODUCT_STATUS_ISSUING) { ?>
			<div>
				<div style="float: left;width:200px"><?= $section->getAttributeLabel('issuer') . ':' ?></div>
				<div style="float: left;width:200px;"><?= $row['header']['issuerFirstName'] . ' ' . $row['header']['issuerLastName'] ?> </div>
			</div>
		<?php } else {?>
			<div>
				<div style="float: left;width:200px"><?= $section->getAttributeLabel('orderTitle') . ':' ?></div>
				<div style="float: left;width:200px;"><?= $row['header']['orderTitle'] . '(' . $row['header']['orderNumber'] . ')' ?> </div>
			</div>
		<?php }?>
		<div>
			<div style="float: left;width:200px"><?= $section->getAttributeLabel('date') . ':' ?></div>
			<div style="float: left;width:200px;"><?= $row['header']['dateOperation'] ?> </div>
		</div>
		<div>
			<div style="float: left;width:200px"><?= $section->getAttributeLabel('warehouse') . ':' ?></div>
			<div style="float: left;width:200px;"><?= $row['header']['warehouseName'] ?> </div>
		</div>
	
		<table class="table-protocol-detail" border="1">	  	  	
		  	<tr>
		  		<th width="70%"><?= $section->getAttributeLabel('productName') ?></th>
			    <th width="30%"><?= $section->getAttributeLabel('count') ?></th>
		  	</tr>
		  	<?php foreach($row['products'] as $data) {?>
		  		<tr>
		    		<td><?= $data['name'] ?></td>
		    		<td><?= $data['count']?></td>
		  		</tr>
		  	<?php }?>
		</table>
		<br>
	<?php }	?>
<?php }
else {
	echo $section->getAttributeLabel('none');
}?>


