<?php 
    use common\helpers\StringHelper;
?>

<div class="offer-details-products">
	<?php if(!empty($section->products)) { 
			$missedColumns = [];
			$atLeastOneCount = false;
			$atLeastOnePriceTotal = false;
			foreach($section->products as $prefix => $product) {
				if(!empty($product->count)) {
					$atLeastOneCount = true;
				}
				if(!empty(floatval($product->totalPrice))) {
					$atLeastOnePriceTotal = true;
				}
			}
			
			if(!$atLeastOneCount) {
				$missedColumns[] = 'count';
			}
			if(!$atLeastOnePriceTotal) {
				$missedColumns[] = 'totalPrice';
			}
		?>
	    <table class="table-protocol-detail" border="1">
	        <thead>
	            <tr>
	                <th>
	                    <?= Yii::t('documents', 'No.') ?>
	                </th>
	                <th>
	                    <?= $section->getAttributeLabel('assortment') ?>
	                </th>
	                <th>
	                    <?= $section->getAttributeLabel('priceUnit') ?>
	                </th>
	                <?php if(!in_array('count', $missedColumns)) { ?>
	                	<th>
		                    <?= $section->getAttributeLabel('unit') ?>
		                </th>
		                <th>
		                    <?= $section->getAttributeLabel('count') ?>
		                </th>		                
	                <?php }
	                if(!in_array('totalPrice', $missedColumns)) { ?>
		                <th>
		                    <?= $section->getAttributeLabel('priceTotal') ?>
		                    [<span><?= $section->currency->symbol ?></span>]
		                </th>
	                <?php } ?>
	            </tr>
	        </thead>
	        <tbody>
	            <?php $index = 0; 
	            	foreach($section->products as $prefix => $product): ?>
	                <?= Yii::$app->controller->renderFile(__DIR__.'/offer-details-products/product-row.php', [
	                		'product' => $product, 
	                		'prefix' => $index++,
	                		'missedColumns' => $missedColumns,
	                ]) ?>
	            <?php endforeach ?>
	        </tbody>
	        <?php if(!empty(floatval($section->summaryCost))) { ?>
		        <tfoot>
		            <tr>
		                <td></td>
		                <td></td>
		                <td></td>
		                <td></td>
		                <td><?= Yii::t('web', 'Summary') ?></td>
		                <td class="summary-cost text-center"><?= StringHelper::getFormattedCost($section->summaryCost).' '.$section->currency->symbol ?></td>
		            </tr>
		        </tfoot>
	        <?php } ?>
	    </table>
    <?php } ?>
</div>