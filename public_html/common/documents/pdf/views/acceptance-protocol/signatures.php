<?php
    use Mindseater\PHPUtils\Text\StringSequenceGenerator;
?>

<div class="text-center">
    <h4><?= mb_strtoupper(Yii::t('documents', 'Supervising board signatures'), 'utf-8') ?>:</h4>
</div>

<?php 
    $dotsGenerator = new StringSequenceGenerator('.');
    $numberOfDots = 50;
?>
<div>
    <div style="float: left; width: 50%">
        <div class="bold" >
            <?= Yii::t('documents', 'Ordering party') ?>
        </div>
        <div style="margin-top:10px">
            a) <?= $dotsGenerator->repeat($numberOfDots) ?>
        </div>
        <div style="margin-top:10px">
            b) <?= $dotsGenerator->repeat($numberOfDots) ?>
        </div>
    </div>
    <div style="float: left; width: 50%">
        <div class="bold">
            <?= Yii::t('documents', 'Executor') ?>
        </div>
        <div style="margin-top:10px">
            a) <?= $dotsGenerator->repeat($numberOfDots) ?>
        </div>
        <div style="margin-top:10px">
            b) <?= $dotsGenerator->repeat($numberOfDots) ?>
        </div>
    </div>
</div>
