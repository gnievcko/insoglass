<?php
    use yii\helpers\Html;
    use common\helpers\StringHelper;
?>

<tr class="document-table-row">
	<?php if(!$edit && !$formModel->hasIndex()) echo '<td>'.($i).'</td>'; ?>
	<?php foreach ($formModel->getAttributes() as $key => $value) : ?>
		<td><?= $edit ? $form->field($formModel, "[{$idx}]". $key)->textInput()->label(false) : ($row[$key] ?: '') ?></td>
	<?php endforeach; ?>

    <?php if($edit): ?>
        <td>
            <?= Html::img('@web/images/placeholders/garbage.png', ['class' => 'remove-row-btn']) ?>
        </td>
    <?php endif; ?>
</tr>
