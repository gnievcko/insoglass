<?php

use common\helpers\ArrayHelper;

$edit = $section->isEditable();
?>
<?php if(!ArrayHelper::isArrayEmptyTotal($rows)) { ?>
	<table class="table-protocol" border="1">
	    <thead>
	        <tr>
	            <?php if(!$edit && !$formModel->hasIndex()) echo '<th>'.Yii::t('documents', 'Id').'</th>'; ?>
	            <?php foreach($formModel->getAttributes() as $name => $val) : ?>
	                <th>
	                    <?= $formModel->getAttributeLabel($name) ?>
	                </th>
	            <?php endforeach; ?>
	            <?php if($edit): ?>
	                <th></th>
	            <?php endif; ?>
	        </tr>
	    </thead>
	    <tbody>
	        <?php
	        $i = 1;
	        foreach($rows as $idx => $row):
	              $formModel->load(["{$formModel->formName()}" => $row]);
	            ?>
	            <?=
	          
	            Yii::$app->controller->renderFile(__DIR__.'/row.php', [
	                'idx' => $idx,
	                'i' => $i,
	                'row' => $row,
	                'edit' => $edit,
	                'formModel' => $formModel
	                    ] + (isset($form) ? ['form' => $form] : []));
	            $i++;
	            ?>
	        <?php endforeach; ?>
	    </tbody>
	</table>
<?php } ?>
