<?php

use yii\web\View;
use yii\helpers\Html;

$this->registerJsFile('@web/js/documents/tables.js?'.uniqid(), ['depends' => [ \yii\jui\JuiAsset::className()]]);
?>
<div class="document-tables-<?= $name ?>" >

    <?= Yii::$app->controller->renderFile(__DIR__.'/table-body.php', ['section' => $section, 'rows' => $rows, 'formModel' => $formModel] + (isset($form) ? ['form' => $form] : [])) ?>

    <?php if($edit): ?>
        <div class="ajax-loader">
            <?php echo Html::img('@web/images/ajax-loader.gif') ?>
        </div>

        <span class="add-row-button btn btn-link">
            <span class="glyphicon glyphicon-plus-sign"></span>
            <span><?= Yii::t('web', 'Add row') ?></span>
        </span>
    <?php endif; ?>

</div>
<?php
$this->registerJs('
        (function() {
            initializeTables(
                "document-tables-'.$name.'",
                {
                    rowUrl: "'.$url.'",
                    edit: '.(int) $edit.'
                }
            );
        })();
    ', View::POS_END);
?>
