<?php

use common\helpers\StringHelper;
use frontend\helpers\UserHelper;
use common\helpers\Utility;
use yii\helpers\ArrayHelper;
$this->title = $section->getAttributeLabel('lostClientReport').' - '.Yii::$app->name;
?>

<div class="text-center"><h2><i><?= $section->getAttributeLabel('lostClientReport') ?></i></h2></div>

<div class="text-center">
    <?=
    $section->getAttributeLabel('forFirstPeriod') . ': ' .
    $section->dateFrom . ' - ' .
    $section->dateTo
    ?>
</div>

<div class="text-center">
    <?=
    $section->getAttributeLabel('forSecondPeriod') . ': ' .
    $section->compareDateFrom . ' - ' .
    $section->compareDateTo
    ?>
</div>

<div class="text-center"><?= $section->getAttributeLabel('generatedFor') . ': ' . $section->user ?></div>

<div class="text-center"><?= $section->getAttributeLabel('generationDate') . ': ' . $section->dateGenerated ?></div>

<br>
<div class="table-responsive">
    <table class="table-protocol-detail" border="1">
        <tr>
            <th><?= $section->getAttributeLabel('Id') ?></th>
            <th><?= $section->getAttributeLabel('company') ?></th>
            <th><?= $section->getAttributeLabel('numberOfInvoice') ?></th>
            <th><?= $section->getAttributeLabel('totalPrice') ?></th>
        </tr>
        <?php if (!empty($section->tableData)) { ?>
            <?php
            foreach ($section->tableData as $row) {
                $lp++;
                ?>
                <tr>
                    <td><?= $lp ?>.</td>
                    <td><?= $row['name'] ?></td>
                    <td><?= $row['numberOfInvoices'] ?></td>
                    <td><?= number_format($row['price'], 2, ',', '') . ' ' . $row[currency]?></td>
                </tr>
                <?php
            }
            ?>

        <?php } else {
            ?>
            <tr>
                <td>1.</td>
                <td>-</td>
                <td>-</td>
                <td>-</td>
            </tr>
        <?php }
        ?>
    </table>
</div>

