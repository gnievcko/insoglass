<?php 
    use yii\helpers\Html;
?>

<div class="full-width">
    <div class="half-width pull-left"> 
        <?= Html::img($section->companyLogo, ['style' => 'width: 200px']) ?>
        <div style="margin-top: 7px; font-size: 12px">
		    <strong><?= $section->companyFullName ?></strong><br />
		    <span><?= $section->companyAddress ?></span><br />
		    <span><?= $section->getAttributeLabel('companyVatId') ?>: <?= $section->companyVatId ?></span><br />
		    <span><?= $section->getAttributeLabel('companyBankAccount') ?>: <?= $section->companyBankAccount ?></span><br />
		    <?php if(!empty($section->companySwift)) { ?>
		    	<span><?= $section->getAttributeLabel('companySwift') ?>: <?= $section->companySwift ?></span><br />
		    <?php } ?>
		    <span><?= $section->companyBank ?></span>
		</div>
    </div>
    <div class="half-width pull-right"> 
    	<?php if(!empty($section->companyCertificateLogo)) {
        	echo Html::img($section->companyCertificateLogo, ['style' => 'height: 100px; float: right']);
    	} ?>
    </div>
</div>


