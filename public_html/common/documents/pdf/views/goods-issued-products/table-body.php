<?php
    use common\helpers\StringHelper;

    $edit = $section->isEditable();
?>

<?php
	$index = 1;
	foreach($section->products as $idx => $product): ?>
    <?= 
        Yii::$app->controller->renderFile(__DIR__.'/product-row.php', [
        		'index' => $index++,
            	'idx' => $idx, 
            	'product' => $product, 
            	'edit' => $edit, 
        ] + (isset($form) ? ['form' => $form] : [])) 
    ?>
<?php endforeach; 

//$summary = $section->calculateSummary();
?>
