<?php
    use yii\helpers\Html;
    use common\helpers\StringHelper;

    $recalculateInputsOptions = ['class' => 'recalculates form-control'];
    $numericFieldsCommonOptions = ['type' => 'number'];
?>

<tr class="goods-issued-product">
	<td><?= $index ?></td>
    <td><?= $edit ? $form->field($product, "[{$idx}]name")->textInput()->label(false) : $product->name ?></td>
    <td><?= $edit ? $form->field($product, "[{$idx}]location")->textInput()->label(false) : ($product->location ?: '-') ?></td>
    <td><?= $edit ? $form->field($product, "[{$idx}]count")->textInput($recalculateInputsOptions + $numericFieldsCommonOptions)->label(false) : $product->count ?></td>
    <td><?= $edit ? $form->field($product, "[{$idx}]unit")->textInput()->label(false) : ($product->unit ?: '-') ?></td>
    <?php if($edit): ?>
        <td>
            <?= Html::img('@web/images/placeholders/garbage.png', ['class' => 'remove-goods-issued-product-btn']) ?>
        </td>
    <?php endif; ?>
</tr>
