<div class="table-responsive">
    <table class="table-protocol-detail" border="1">
        <tr>
            <th><?= $section->getAttributeLabel('Id') ?></th>
            <th><?= $section->getAttributeLabel('name') ?></th>
            <th><?= $section->getAttributeLabel('M.u.') ?></th>
            <th><?= $section->getAttributeLabel('amount') ?></th>
            <th><?= $section->getAttributeLabel('price') ?></th>
            <th><?= $section->getAttributeLabel('value') ?></th>
            <th><?= $section->getAttributeLabel('location') ?></th>
        </tr>
        
        <?php foreach($data as $row) {
			$lp++; ?>
            <tr>
           		<td><?= $lp ?>.</td>
                <td><?= $row['parentName'] != null ? $row['parentName'] . ' (' . $row['name'] . ')' : $row['name'] ?></td>
                <td><?= $row['unit_default'] != null ? $row['unit_default'] : $section->getAttributeLabel('pc.') ?></td>
                <td><?= $row['count'] ?></td>
                <td><?= $row['price_default'] != null ?  number_format($row['price_default'], 2, ',', '') . ' ' . $row['short_symbol'] : '-'?></td>
                <td><?= $row['price_default'] != null ?  number_format($row['count'] * $row['price_default'], 2, ',', '') . ' ' . $row['short_symbol'] : '-'?></td>
                <td><?= !empty($row['stillage']) && !empty($row['shelf']) ? $row['stillage'].'/'.$row['shelf'] : '-' ?></td>
             </tr>
		<?php } ?>
    </table>
</div>