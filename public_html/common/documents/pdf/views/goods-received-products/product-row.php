<?php
    use yii\helpers\Html;
    use common\helpers\StringHelper;

    $recalculateInputsOptions = ['class' => 'recalculates form-control'];
    $numericFieldsCommonOptions = ['type' => 'number'];
?>

<tr class="goods-received-product">
    <td><?= $edit ? $form->field($product, "[{$idx}]index")->textInput()->label(false) : ($product->index ?: '-') ?></td>
    <td><?= $edit ? $form->field($product, "[{$idx}]name")->textInput()->label(false) : $product->name ?></td>
    <td><?= $edit ? $form->field($product, "[{$idx}]count")->textInput($recalculateInputsOptions + $numericFieldsCommonOptions)->label(false) : $product->count ?></td>
    <td><?= $edit 
    		? $form->field($product, "[{$idx}]price")->textInput($recalculateInputsOptions + $numericFieldsCommonOptions + ['step' => '0.01'])->label(false) 
            : StringHelper::getFormattedCost($product->price).' '.$product->currencySymbol
	?></td>
    <td><?= $edit ? $form->field($product, "[{$idx}]unit")->textInput()->label(false) : ($product->unit ?: '-') ?></td>
	<td><?= $edit ? $form->field($product, "[{$idx}]note")->textInput()->label(false) : $product->note ?></td>
    <td><?= $edit  
    		? $form->field($product, "[{$idx}]costSum")->textInput()->label(false)
    		: StringHelper::getFormattedCost($product->costSum).' '.$product->currencySymbol
    ?></td>
    <?php if($edit): ?>
        <td>
            <?= Html::img('@web/images/placeholders/garbage.png', ['class' => 'remove-goods-received-product-btn']) ?>
        </td>
    <?php 
    	endif; 
    	
    	//echo $form->field($product, "[{$idx}]price")
    ?>
</tr>
