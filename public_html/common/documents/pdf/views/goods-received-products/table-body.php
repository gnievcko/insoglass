<?php
    use common\helpers\StringHelper;

    $edit = $section->isEditable();
?>

<?php foreach($section->products as $idx => $product): ?>
    <?= 
        Yii::$app->controller->renderFile(__DIR__.'/product-row.php', [
            'idx' => $idx, 
            'product' => $product, 
            'edit' => $edit, 
        ] + (isset($form) ? ['form' => $form] : [])) 
    ?>
<?php endforeach; 

$summary = $section->calculateSummary();
?>

<tr>
	<td class="bold"><?= Yii::t('documents', 'Total'); ?></td>
	<td></td>
	<td class="bold"><?= $summary['count'] ?></td>
	<td></td>
	<td></td>
	<td></td>
	<td class="bold"><?= StringHelper::getFormattedCost($summary['price']).' '.$summary['currency']; ?></td>
</tr>