<?php
use common\helpers\StringHelper;
$this->title = $section->getAttributeLabel('reportName').' - '.Yii::$app->name;

?>

<div class="text-center"><h2><i><?= $section->getAttributeLabel('reportName') ?></i></h2></div>

<div class="text-center">
	<?= $section->getAttributeLabel('forPeriod') . ': ' .  
	$section->dateFrom . ' - ' .
	$section->dateTo ?>
</div>

<div class="text-center"><?= $section->getAttributeLabel('generatedFor') . ': ' . $section->user ?></div>

<div class="text-center"><?= $section->getAttributeLabel('generationDate') . ': ' . $section->dateGenerated ?></div>

<br><br>

<h3><?= $section->getAttributeLabel('demandOnIntermediateProductsAccordingToOrder') ?></h3>

<?php if(!empty($section->countByOrder)) {?>
    
    <?php foreach($section->countByOrder as $orderNumber => $order) {?>
    	<?php if(!empty($order[0])) {
    	    echo $order[0]['orderNumber'] . ', ' . $order[0]['orderTitle'] . ', ' . mb_strtolower(Yii::t('web', 'Deadline'), 'UTF-8').': ' . StringHelper::getFormattedDateFromDate($order[0]['deadline']);
    	} ?>
    	<br>
    	<table class="table-protocol-detail" border="1">
			<tr>
    			<th><?= $section->getAttributeLabel('product') ?></th>
				<th><?= $section->getAttributeLabel('productCount') ?></th>
				<th><?= $section->getAttributeLabel('intermediateProduct') ?></th>
				<th><?= $section->getAttributeLabel('intermediateProductRequiredCount') ?></th>
				<th><?= $section->getAttributeLabel('intermediateProductsInWarehouses') ?></th>
				<th><?= $section->getAttributeLabel('isThereEnoughIntermediateProductsInWarehouses') ?></th>
			</tr>
			<?php foreach($order as $product) {?>
				<tr>
            		<td><?= $product['productName'] ?></td>
            		<td><?= $product['productRequiredCount'] ?></td>
            		<td><?= $product['intermediateProductName'] ?></td>
            		<td><?= $product['intermediateProductRequired'] ?></td>
            		<td><?= $product['intermediateCountAvailable'] ?></td>
            		<td><?= $product['intermediateProductRequired'] <= $product['intermediateCountAvailable'] ? Yii::t('main', 'Yes') : Yii::t('main', 'No')?></td>
            	</tr>
			<?php }?>
		</table>
		<br>
    <?php }?>	
<?php } else {
	echo $section->getAttributeLabel('none');
}?>

<h3><?= $section->getAttributeLabel('demandOnIntermediateProductsAccordingToAllOrders') ?></h3>
<?php if(!empty($section->countByOrder)) {?>
    <table class="table-protocol-detail" border="1">
		<tr>
    		<th><?= $section->getAttributeLabel('intermediateProduct') ?></th>
    		<th><?= $section->getAttributeLabel('intermediateProductRequiredCount') ?></th>
    		<th><?= $section->getAttributeLabel('intermediateProductsInWarehouses') ?></th>
    		<th><?= $section->getAttributeLabel('isThereEnoughIntermediateProductsInWarehouses') ?></th>
    		<th><?= $section->getAttributeLabel('numberToBuy') ?></th>
		</tr>
		<?php foreach($section->countByIntermediateProduct as $product) {?>
			<tr>
        		<td><?= $product['intermediateProductName'] ?></td>
        		<td><?= $product['intermediateProductRequired'] ?></td>
        		<td><?= $product['intermediateCountAvailable'] ?></td>
        		<td><?= $product['intermediateProductRequired'] <= $product['intermediateCountAvailable'] ? Yii::t('main', 'Yes') : Yii::t('main', 'No') ?></td>
        		<td><?= max($product['intermediateProductRequired'] - $product['intermediateCountAvailable'], 0)?></td>
        	</tr>
		<?php } ?>
	</table>	
<?php } else {
	echo $section->getAttributeLabel('none');
}?>