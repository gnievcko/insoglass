<?php
    use yii\bootstrap\Html;
    use common\helpers\StringHelper;
    $option = ['class' => 'form-control xSmallTextarea', 'placeHolder' => Yii::t('documents', 'Write value')];
 ?>

<tr class="document-table-row">
    <?php
    foreach($table['theads'] as $key => $th) {
        echo '<td>'.($row[$key] ?: '-').'</td>';
    }
    ?>
</tr>
