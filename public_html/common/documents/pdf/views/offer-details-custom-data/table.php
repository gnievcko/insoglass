<?php
use yii\helpers\Url;

$uniq = uniqid();
?>
<div class="document-tables-<?= $uniq ?> document-table" >
    <?= Yii::$app->controller->renderFile(__DIR__.'/table-body.php', ['table' => $table, 'uniq' => $uniq, 'edit' => $edit]) ?>
</div>
<?php
$script = '
        (function() {
            initializeTables(
                "document-tables-'.$uniq.'",
                {
                    rowUrl: "'.Url::to(['protocol/get-row']).'",
                    uniq: "'.$uniq.'",
                    edit: '.(int)$edit.',
                    type: "",
                    count: '.count($table['theads']).',
    				tableViewsDir: "'.'@common/documents/pdf/views/offer-details-custom-data'.'",
        			startIdx: '.(isset($table['rows']) ? count($table['rows']) : 0).'
                }
            );
        })()
    ';
if(isset($showScript) && $showScript) {
    echo '<script>'.$script.'</script>';
} else {
    $this->registerJs($script);
}
?>
