<?php

use common\documents\sections\OfferDetailsCustomData;
use frontend\models\OrderTableForm;

$option = ['class' => 'form-control xSmallTextarea', 'placeHolder' => Yii::t('documents', 'Write thead name')];
?> 

<div class="offer-details-products">
    <table class="table-protocol-detail" border="1">
        <thead>
            <tr>
                <?php 
                $i = 0;
                foreach($table['theads'] as $key => $th) {
                	$style = ($i == 0 ? 'width: 7%' : ($i == 1 ? 'width: 29%' : ''));
                	if(!empty($style)) {
                		$style = ' style="'.$style.'"';
                	}
					echo '<th'.$style.'>'.($table['theads'][$key] ?: '-').'</td>';
					$i++;
                }
                ?>
            </tr>
        </thead>
        <tbody>
            <?php foreach($table['rows'] as $idx => $row): ?>
                <?=
                Yii::$app->controller->renderFile(__DIR__.'/row.php', [
                    'table' => $table,
                    'idx' => $idx,
                    'row' => $row,
                    'edit' => $edit,
                    'uniq' => $uniq,
                    'formModel' => new OfferDetailsCustomData(),
                ]);
                ?>
            <?php endforeach; ?>
        </tbody>
        <tfoot>
        	<tr>
	        	<?php for($i = 0; $i < count($table['theads']) - 2; ++$i) { ?>
	        		<td></td>
	        	<?php } ?>
	        	<td><?= Yii::t('main', 'Total') ?></td>
	        	<td><span name="tables[<?= $uniq ?>][summary]"><?= OrderTableForm::calculateTotalPriceStatic($table, false) ?></span></td> 
        	</tr>
        </tfoot>
    </table>
</div>