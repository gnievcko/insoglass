<?php

use yii\helpers\Url;
use Mindseater\PHPUtils\Text\StringSequenceGenerator;

$dotsGenerator = new StringSequenceGenerator('.');

$edit = $section->isEditable();
$this->title = \Yii::t('documents', 'Repair protocol').' - '.Yii::$app->name;
?>
<div class="light_measurement">
    <div class="text-center">
        <div style="margin: 25px 0px">
            <h2 class="upper" style="margin-bottom: 0px"><i><?= \Yii::t('documents', 'Repair protocol') ?></i></h2> 
            <h3 class="mt-0"><i><?= $section->getAttributeLabel('date').(!empty($section->date) ? $section->date : $dotsGenerator->repeat(10)) ?></i></h3>
                <?= $edit ? $form->field($section, 'date')->textInput()->label(false) : '' ?>
        </div>
    </div>

    <div class=""><?= $section->getAttributeLabel('object') ?> <strong><?= !$edit ? $section->object : '' ?></strong></div>
    <div class=""><?= $edit ? $form->field($section, 'object')->textInput()->label(false) : '' ?></div>

       <div><?= $section->getAttributeLabel('repairer').':' ?> <?= !empty($section->repairer) ? $section->repairer : $dotsGenerator->repeat(50) ?></div>
    <div class=""><?= $edit ? $form->field($section, 'repairer')->textInput()->label(false) : '' ?></div>
    <div class="mt-20"><strong><?= ($edit || !empty($section->description)) ? $section->getAttributeLabel('description').':' : '' ?></strong></div>
    <div class=""><?= $edit ? $form->field($section, 'description')->textArea()->label(false) : $section->description ?></div>

    <?php
    if($edit || count($section->rows)) {
        echo '<div style="font-size: 16px; margin: 20px 0px">'. Yii::t('documents','Repair list') .': ' . ( $edit ? $form->field($section, 'tableName')->textInput() : '<strong>'.$section->tableName.'</strong>') .'</div>';
        echo $this->renderFile('@common/documents/pdf/views/repair-table/table.php', [
            'section' => $section,
            'edit' => $edit,
            'form' => $form,
            'url' => Url::to(['protocol/get-row-repair'])
        ]);
    }
    ?>
	<br/>
    <?php
    if($edit || count($section->rowsReplacements)) {
        echo '<div class="col-sm-12">'.( $edit ? $form->field($section, 'tableNameReplacements')->textInput() : '<strong>'.$section->tableNameReplacements.'</strong>') .'</div>';
        echo $this->renderFile('@common/documents/pdf/views/single-table/table.php', ['section' => $section, 'name' =>'replacements', 'rows' => $section->rowsReplacements, 'edit' => $edit, 'formModel' => $section->formReplacements, 'form' => $form, 'url' => Url::to(['protocol/get-row-repair-replacements'])]);
    }
    ?>

    <?php
    if($edit || count($section->rowsTests)) {
        echo '<div class="col-sm-12" style="margin-top: 20px">'.( $edit ? $form->field($section, 'tableNameTests')->textInput() : '<strong>'.$section->tableNameTests.'</strong>') .'</div>';
        echo $this->renderFile('@common/documents/pdf/views/single-table/table.php', ['section' => $section, 'name' =>'tests', 'rows' => $section->rowsTests, 'edit' => $edit, 'formModel' => $section->formTests, 'form' => $form, 'url' => Url::to(['protocol/get-row-repair-tests'])]);
    }
    ?>
    <div class="col-sm-12">
        <?= $edit ? $form->field($section, 'text')->textArea()->label(false) : nl2br($section->text) ?>
    </div>

    <div><strong><?= Yii::t('documents', 'Remarks') ?>:</strong></div>

    <div style="white-space: pre-line">
        <?= !empty($section->remarks) ? nl2br($section->remarks) : $dotsGenerator->repeat(1014) ?>
    </div>
    
    <div class="text-center signature" style="margin-top: 20px;">
        <div class="col-sm-6" style="display: inline-block;">
            <div style="margin-bottom: 30px"><b><?= Yii::t('documents', 'CREATED BY') ?></b></div>
            <div>....................................................</div>
            <div style="font-size: 12px"><?= '('.(Yii::t('documents', 'Repairer - Contractor')).')' ?></div>
        </div>
        <div class="col-sm-6" style="display: inline-block;">
            <div style="margin-bottom: 30px"><b><?= Yii::t('documents', 'ACCEPTED BY') ?></b></div>
            <div>....................................................</div>
            <div style="font-size: 12px"><?= '('.(Yii::t('documents', 'User')).')' ?></div>
        </div>
    </div>
</div>
