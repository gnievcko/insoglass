<div class="text-center">
        <div style="margin: 25px 0px">
            <h2 class="upper" style="margin-bottom: 0px"><i><?= $section->getAttributeLabel('title')?></i></h2> 
            <h3 class="mt-0"><i><?= $section->getAttributeLabel('from') . '  ' . (!empty($section->dateOperation) ? $section->dateOperation : '..........') ?></i></h3>
                <?= $edit ? $form->field($section, 'date')->textInput()->label(false) : '' ?>
        </div>
    </div>

<div style="margin-bottom: 20px"><?= $section->getAttributeLabel('onObject') . ' <strong>' . $section->companyName . ' ' . $section->companyAddress . '</strong>'?> </div>

<?php
	$id = 1; 
	
	if(!empty($section->rows)) {
	foreach($section->rows as $rowForm) {
		echo Yii::$app->controller->renderFile(__DIR__.'/fireeq-acceptance-protocol/product-row.php',[
				'editMode' => $editMode,
				'text' => $rowForm['text'],
				'number' => $rowForm['number'],
				'id' => $id,
		]);
		$id++;
	}
}
	
?>

<div class="text-center" style="margin-top: 30px">
        <div class="col-sm-6">
            <div style="margin-bottom: 30px"><b><?= Yii::t('documents', 'CREATED BY') ?></b></div>
            <div>....................................................</div>
            <div style="font-size: 12px"><?= '('.(Yii::t('documents', 'Repairer - Contractor')).')' ?></div>
        </div>
        <div class="col-sm-6">
            <div style="margin-bottom: 30px"><b><?= Yii::t('documents', 'ACCEPTED BY') ?></b></div>
            <div>....................................................</div>
            <div style="font-size: 12px"><?= '('.(Yii::t('documents', 'User')).')' ?></div>
        </div>
    </div>