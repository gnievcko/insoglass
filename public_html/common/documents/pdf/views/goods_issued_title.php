<?php
use yii\helpers\Html;
use kartik\datecontrol\DateControl;
use kartik\datecontrol\Module;

$editMode = $section->isEditable();

$this->title = \Yii::t('documents', 'Goods Issue Note (GIN)').' - '.\Yii::$app->name;
?>

<div class="text-center">
    <div>
        <h2><?= \Yii::t('documents', 'Goods Issued no. {number}', ['number' => $section->number]) ?></h2> 
    </div>
</div>

<table class="goods-issued-list">
	<tr>
		<th><?= $section->getAttributeLabel('warehouseName') ?>:</th>
		<td><?= $editMode ? $form->field($section, 'warehouseName')->textInput()->label(false) : $section->warehouseName ?></td>
	</tr>
	<tr>
		<th><?= $section->getAttributeLabel('userReceivingName') ?>:</th>
		<td><?= $editMode ? $form->field($section, 'userReceivingName')->textInput()->label(false) : $section->userReceivingName ?></td>
	</tr>
	<tr>
		<th><?= $section->getAttributeLabel('userConfirmingName') ?>:</th>
		<td><?= $editMode ? $form->field($section, 'userConfirmingName')->textInput()->label(false) : $section->userConfirmingName ?></td>
	</tr>
	<tr>
		<th><?= $section->getAttributeLabel('dateOperation') ?>:</th>
		<td><?=
			$editMode 
				? $form->field($section, 'dateOperation')->widget(DateControl::classname(), [
	                    'language' => \Yii::$app->language,
	                    'type' => DateControl::FORMAT_DATETIME,
	                    'ajaxConversion' => false,
	                    'displayFormat' => 'php:d.m.Y H:i',
						'saveFormat' => 'php:d.m.Y H:i',
	                    'options' => [
		                        'pluginOptions' => [
		                            	'autoclose' => true
		                        ]
	                    ]
	                ])->label(false)
            	: $section->dateOperation 
		?></td>
	</tr>
	<tr>
		<th><?= $section->getAttributeLabel('dateIssue') ?>:</th>
		<td><?=
			$editMode 
				? $form->field($section, 'dateIssue')->widget(DateControl::classname(), [
	                    'language' => \Yii::$app->language,
	                    'type' => DateControl::FORMAT_DATETIME,
	                    'ajaxConversion' => false,
	                    'displayFormat' => 'php:d.m.Y H:i',
						'saveFormat' => 'php:d.m.Y H:i',
	                    'options' => [
		                        'pluginOptions' => [
		                            	'autoclose' => true
		                        ]
	                    ]
	                ])->label(false)
            	: $section->dateIssue 
		?></td>
	</tr>
	<tr>
    	<th style="vertical-align: top"><?= $section->getAttributeLabel('goesTo') ?>:</th>
    	<td style="font-weight: normal"><?= $editMode ? $form->field($section, 'goesTo')->textarea()->label(false) : (!empty($section->goesTo) ? nl2br($section->goesTo) : '-') ?></td>
    </tr>
    <tr>
    	<th style="vertical-align: top"><?= $section->getAttributeLabel('status') ?>:</th>
    	<td style="font-weight: normal"><?= $editMode ? $form->field($section, 'status')->radioList([
				0 => Yii::t('documents', 'Not accounted'), 
				1 => Yii::t('documents', 'Accounted')
		])->label(false) : (!empty($section->status) ? Yii::t('documents', 'Accounted') : Yii::t('documents', 'Not accounted')) ?></td>
    </tr>
    <tr>
    	<th style="vertical-align: top"><?= $section->getAttributeLabel('description') ?>:</th>
    	<td style="font-weight: normal"><?= $editMode ? $form->field($section, 'description')->textarea()->label(false) : (!empty($section->description) ? nl2br($section->description) : '-') ?></td>
    </tr>
</table>
