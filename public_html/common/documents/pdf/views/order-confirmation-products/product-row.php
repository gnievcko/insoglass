<?php
use common\helpers\StringHelper;

$netValue = bcmul($product->price, $product->count, 2);
$vatAmount = bcmul($netValue, $product->vatRate, 2);
$grossValue = bcadd($netValue, $vatAmount, 2);

?>
<tr>
<td><?= $prefix + 1 ?></td>
  <td><?= $product->name ?></td>
<td><?= $product->count.' '.$product->getUnitTranslation($language)?> </td>

<td><?= $product->price ?></td>
<td class="gross-price-value"><?= StringHelper::getFormattedCost($grossValue)?></td>
</tr>
