<?php
use common\helpers\StringHelper;

$vats = [ 
		'' => Yii::t ( 'documents', 'Vat-free' ),
		'NP' => Yii::t ( 'documents', 'Not applicable' ),
		'0.23' => '23 %',
		'0.08' => '8 %',
		'0' => '0 %' 
];

$vatRatesSummary = [ ];
$vatRatesSummarySum = [ 
		'netto' => 0,
		'vatAmount' => 0 
];
foreach ( $products as $prefix => $product ) {
	
	$netValue = bcmul ( $product ['price'], $product ['count'], 2 );
	
	$vatRate = ( string ) $product->vatRate;
	$vatRatesSummary [$vatRate] ['netto'] = bcadd ( isset ( $vatRatesSummary [$vatRate] ['netto'] ) ? $vatRatesSummary [$vatRate] ['netto'] : 0, $netValue, 2 );
	
	$vatAmount = bcmul ( $netValue, $product->vatRate, 2 );
	$vatRatesSummary [$vatRate] ['vatAmount'] = bcadd ( isset ( $vatRatesSummary [$vatRate] ['vatAmount'] ) ? $vatRatesSummary [$vatRate] ['vatAmount'] : 0, $vatAmount, 2 );
	
	$vatRatesSummarySum ['netto'] = bcadd ( $vatRatesSummarySum ['netto'], $netValue, 2 );
	$vatRatesSummarySum ['vatAmount'] = bcadd ( $vatRatesSummarySum ['vatAmount'], $vatAmount, 2 );
}
$vatRatesSummarySum ['brutto'] = bcadd ( $vatRatesSummarySum ['vatAmount'], $vatRatesSummarySum ['netto'], 2 );
?>

<?php if($vatRatesSummarySum ['brutto'] != 0):?>
<table style="width:100%; text-align:center; font-size: 11px;">
	<thead>
		<tr style="background-color: LightGray;">
			<th><?= Yii::t('documents', 'VAT rate')?></th>
			<th><?= Yii::t('documents', 'Excluding VAT')?></th>
			<th><?= Yii::t('documents', 'VAT')?></th>
			<th><?= Yii::t('documents', 'Gross')?></th>
		</tr>
	</thead>
	<tbody>
		<tr>
			<td style="text-align: left"><?= Yii::t('documents', 'Total').":" ?></td>
			<td><?= StringHelper::getFormattedCost($vatRatesSummarySum['netto']) ?></td>
			<td><?= StringHelper::getFormattedCost($vatRatesSummarySum['vatAmount']) ?></td>
			<td><?= StringHelper::getFormattedCost($vatRatesSummarySum ['brutto']) .' '. $currencySymbol  ?></td>
		</tr>
		<?php foreach($vatRatesSummary as $key => $value): ?>
		<tr>
			<td><?=$vats [$key];?></td>
			<td><?= StringHelper::getFormattedCost($value['netto']) ?></td>
			<td><?= StringHelper::getFormattedCost($value['vatAmount']) ?></td>
			<td><?= StringHelper::getFormattedCost(bcadd($value['netto'], $value['vatAmount'], 2)) ?></td>
		</tr>
		<?php endforeach; ?>
	</tbody>
</table>
<?php endif;?>
