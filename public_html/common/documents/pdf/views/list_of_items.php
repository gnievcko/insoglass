<?php

use common\helpers\StringHelper;

$this->title = $section->getAttributeLabel('listOfItemsReport').' - '.Yii::$app->name;
?>

<div class="text-center"><h2><i><?= $section->getAttributeLabel('listOfItemsReport') ?></i></h2></div>

<div class="text-center"><?= $section->getAttributeLabel('generatedFor') . ': ' . $section->user ?></div>

<div class="text-center"><?= $section->getAttributeLabel('generationDate') . ': ' . $section->dateGenerated ?></div>

<br/>
<?php if(!empty($section->categoryNames)) { ?>
	<div class="text-center"><?= StringHelper::translateOrderToOffer('web', 'Included product categories').':' ?></div>
	<?php foreach($section->categoryNames as $name) { ?>
		<div class="text-center"><?= $name ?></div>
	<?php } ?>
<?php } ?>
<br>

<?php if(!empty($section->tableData)) { ?>
	<?php if(!empty($section->tableData['from'])) { ?>
		<h4><?= Yii::t('documents', 'Situation as of').': '.$section->dateFrom.' ('.mb_strtolower(Yii::t('documents', 'Start of the day'), 'UTF-8').')'; ?></h4>
		<?= Yii::$app->controller->renderFile(__DIR__.'/list-of-items/table-data.php', ['section' => $section, 'data' => $section->tableData['from']]) ?>	
	<?php } ?>

	<h4><?= Yii::t('documents', 'Situation as of').': '.$section->dateTo.' ('.mb_strtolower(Yii::t('documents', 'End of the day'), 'UTF-8').')'; ?></h4>
	<?= Yii::$app->controller->renderFile(__DIR__.'/list-of-items/table-data.php', ['section' => $section, 'data' => $section->tableData['to']]) ?>
<?php }
else { 
	echo Yii::t('web', 'None');	
} ?>
