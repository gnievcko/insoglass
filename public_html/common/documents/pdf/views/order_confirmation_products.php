<?php
	$prevLanguage = Yii::$app->language;
	Yii::$app->language = $section->getLanguage();
?>
<div style="font-size: 11px;">
    <table style="width:100%; text-align:center; font-size: 11px;">
        <thead>
            <tr style="background-color: LightGray;">
                <th class="product-no">
                    <?= Yii::t('documents', 'No.') ?>
                </th>
                <th style="width: 250px">
                    <?= $section->getAttributeLabel('name') ?>
                </th>
                <th style="min-width: 110px">
                    <?= $section->getAttributeLabel('count') ?>
                </th>
                <th style="min-width: 150px">
                    <?= $section->getAttributeLabel('netPrice') ?>
                </th>
                <th style="width: 140px">
                    <?= $section->getAttributeLabel('grossValue') ?>
                </th>
            </tr>
        </thead>
        <tbody>
            <?php 
            $i = 0;
            foreach($section->products as $prefix => $product): ?>
                <?= Yii::$app->controller->renderFile(__DIR__.'/order-confirmation-products/product-row.php', [
                		'product' => $product, 
                		'prefix' => $i,
                		'currencySymbol' => $section->getCurrencySymbol(),
                		'language' => $section->getLanguage(),
                ]) ?>
            <?php 
            $i++;
            endforeach ?>
        </tbody>
    </table>
	<?php if($i > 0):?>
	<div style="text-align: center;width:60%;float:right;margin-top:30px;">
				<?=
				Yii::$app->controller->renderFile(__DIR__.'/order-confirmation-products/products-summary-body.php', [
						'products' => $section->products,
						'currencySymbol' => $section->getCurrencySymbol(),
				])
				?>
	</div>
	<?php endif;?>
	<div style="clear:both"></div>
</div>

<?php	Yii::$app->language = $prevLanguage; ?>