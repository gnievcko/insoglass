<?php 
use kartik\datecontrol\DateControl;
use Mindseater\PHPUtils\Dates\Formatter;
?>

<div style="font-size: 12px">
	<div>
	    <?= 
	        $editMode 
	            ? $form->field($section, 'dateOfIssue')->widget(DateControl::classname(), [
	                    'language' => \Yii::$app->language,
	                    'type' => DateControl::FORMAT_DATE,
	                    'ajaxConversion' => false,
	                    'displayFormat' => 'php:d.m.Y',
	                    'saveFormat' => 'php:Y-m-d',
	                    'options' => [
	                        'pluginOptions' => [
	                            'autoclose' => true
	                        ]
	                    ]
	                ])
	            : $section->getAttributeLabel('dateOfIssue').': '. (!empty($section->dateOfIssue) ? (new Formatter())->from($section->dateOfIssue, 'Y-m-d')->format('d.m.Y')->getDate() : '')
	    ?>
	    <?= 
	        $editMode 
	            ? $form->field($section, 'dateOfSale')->widget(DateControl::classname(), [
	                    'language' => \Yii::$app->language,
	                    'type' => DateControl::FORMAT_DATE,
	                    'ajaxConversion' => false,
	                    'displayFormat' => 'php:d.m.Y',
	                    'saveFormat' => 'php:Y-m-d',
	                    'options' => [
	                        'pluginOptions' => [
	                            'autoclose' => true
	                        ]
	                    ]
	                ]) 
	            : '<br/>'.$section->getAttributeLabel('dateOfSale').': '. (!empty($section->dateOfSale) ? (new Formatter())->from($section->dateOfSale, 'Y-m-d')->format('d.m.Y')->getDate() : '').'<br/><br/>'
	    ?>
	    
	</div>
	
	<div class="row">
	    <div class="col-sm-6">
	        <span><b><?= Yii::t('documents', 'Seller') ?></b></span><br />
	        <span><?= $section->sellerName ?></span><br />
	        <span><?= $section->sellerAddress ?></span><br />
	        <span><?= Yii::t('documents', 'VAT ID') ?>: <?= $section->sellerVatId ?></span>
	    </div>
	    <div class="col-sm-6">
	        <span><b><?= Yii::t('documents', 'Buyer') ?></b></span><br />
	        <span>
	            <?= $editMode ? $form->field($section, 'buyerName')->textInput(['placeholder' => $section->getAttributeLabel('buyerName')])->label(false) : $section->buyerName ?>
	        </span><br />
	        <span>
	            <?= 
	                $editMode 
	                    ? $form->field($section, 'buyerAddress')->textInput(['placeholder' => $section->getAttributeLabel('buyerAddress')])->label(false) 
	                    : $section->buyerAddress 
	            ?>
	        </span><br />
	        <span>
	            <?= $editMode ? $form->field($section, 'buyerVatId') : Yii::t('documents', 'VAT ID').': '.$section->buyerVatId ?>
	        </span>
	    </div>
	</div>
</div>