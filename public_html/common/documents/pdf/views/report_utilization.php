<?php 
use common\helpers\StringHelper;
$this->title = $section->getAttributeLabel('report-utilizations').' - '.Yii::$app->name;
?>

<div class="text-center"><h2><i><?= $section->getAttributeLabel('report-utilizations') ?></i></h2></div>

<div class="text-center">
	<?= $section->getAttributeLabel('forPeriod') . ': ' .  
	$section->dateFrom . ' - ' .
	$section->dateTo ?>
</div>

<div class="text-center"><?= $section->getAttributeLabel('generatedFor') . ': ' . $section->user ?></div>

<div class="text-center"><?= $section->getAttributeLabel('generationDate') . ': ' . $section->dateGenerated ?></div>

<br><br>

<h4><?= $section->getAttributeLabel('utilizationsByProduct') ?></h4>

<?php if(!empty($section->countByProduct)) {?>
	
		<table class="table-protocol-detail" border="1">
			<tr>
		    	<th><?= $section->getAttributeLabel('product') ?></th>
		    	<th><?= $section->getAttributeLabel('count') ?></th>
		  	</tr>
			<?php foreach($section->countByProduct as $row) {?>
		  		<tr>
		    		<td><?= $row['name'] ?></td>
		    		<td><?= $row['count'] ?></td>
		  		</tr>
		  	<?php }?>
		</table>
	
<?php }
else {
	echo $section->getAttributeLabel('none');
}?>

<h4><?= $section->getAttributeLabel('utilizationOperations') ?></h4>

<?php if(!empty($section->history)) {?>
	
		<table class="table-protocol-detail" border="1">
		  	<tr>
			    <th><?= $section->getAttributeLabel('product') ?></th>
			    <th><?= $section->getAttributeLabel('count') ?></th>
			    <th><?= $section->getAttributeLabel('user') ?></th>
			    <th><?= $section->getAttributeLabel('userReceiving') ?></th>
			    <th><?= $section->getAttributeLabel('description') ?></th>
			    <th><?= $section->getAttributeLabel('dateCreation') ?></th>
		  	</tr>
			<?php foreach($section->history as $row) {?>
		  		<tr>
		    		<td width="20%"><?= $row['name'] ?></td>
		    		<td align="center" width="10%"><?= $row['count']?></td>
		    		<td width="20%"><?= $row['user'] ?></td>
		    		<td width="20%"><?= $row['userReceiving'] ?></td>
		    		<td width="15%"><?= $row['description'] ?></td>
		    		<td width="15%"><?= $row['date_creation'] ?></td>
		  		</tr>
		  	<?php }?>
		</table>
	
<?php }
else {
	echo $section->getAttributeLabel('none');
}?>