<?php 
use common\helpers\Utility;
use frontend\helpers\UserHelper;
$this->title = $section->getAttributeLabel('report-orders').' - '.Yii::$app->name;
?>

<div class="text-center"><h2><i><?= $section->getAttributeLabel('report-sales-result') ?></i></h2></div>

<div class="text-center">
	<?= $section->getAttributeLabel('forPeriod') . ': ' .  
	$section->dateFrom . ' - ' .
	$section->dateTo ?>
</div>

<div class="text-center"><?= $section->getAttributeLabel('generatedFor') . ': ' . $section->user ?></div>

<div class="text-center"><?= $section->getAttributeLabel('generationDate') . ': ' . $section->dateGenerated ?></div>

<br><br>

<table class="table-protocol-detail" border="1">
	<tr>
		<th width="40%"><?= $section->getAttributeLabel('name') ?></th>
		<th width="20%"><?= $section->getAttributeLabel('newClients')  ?></th>
		<th width="20%"><?= $section->getAttributeLabel('ordersForClients') ?></th>
		<th width="20%"><?= $section->getAttributeLabel('invoicedClients')?>
	</tr>
	<?php foreach($section->data as $row) {?>
	<tr>
		<td><?= UserHelper::getPrettyUserName(null, $row['first_name'], $row['last_name'])?></td>
		<td><?= $row['newClientsCount']?></td>
		<td><?= $row['orderCount']?></td>
		<td><?= $row['invoicedOrdersCount']?></td>
	</tr>
	<?php }?>
</table>