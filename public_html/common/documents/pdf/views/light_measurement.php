<?php
use yii\helpers\Url;
use Mindseater\PHPUtils\Text\StringSequenceGenerator;

$dotsGenerator = new StringSequenceGenerator('.');

$edit = $section->isEditable();
$this->title = \Yii::t('documents', 'Protocol of light intensity measurement').' - '.Yii::$app->name;
?>
<div class="light_measurement">
    <div class="text-center">
        <div style="margin: 25px 0px">
            <h2 class="upper" style="margin-bottom: 0px"><i><?= \Yii::t('documents', 'Protocol of light intensity measurement') ?></i></h2> 
            <h3 class="mt-0"><i><?= $section->getAttributeLabel('date').(!empty($section->date) ? $section->date : $dotsGenerator->repeat(10)) ?></i></h3>
                <?= $edit ? $form->field($section, 'date')->textInput()->label(false) : '' ?>
        </div>
    </div>
    

    

    <div><?= $section->getAttributeLabel('object') ?><strong> <?= $section->object ?></strong></div>
    <div><?= $edit ? $form->field($section, 'object')->textInput()->label(false) : '' ?></div>

    <div><?= $section->getAttributeLabel('repairer').':' ?> <?= !empty($section->repairer) ? $section->repairer : $dotsGenerator->repeat(50) ?></div>
    <div><?= $edit ? $form->field($section, 'repairer')->textInput()->label(false) : '' ?></div>
    <br/>
    <div><?= ($edit || !empty($section->measurementTool)) ? $section->getAttributeLabel('measurementTool').':' : '' ?><strong> <?= !$edit ? $section->measurementTool : '' ?></strong></div>
    <div><?= $edit ? $form->field($section, 'measurementTool')->textInput()->label(false) : '' ?></div>

 

    <?php
    if($edit || count($section->rows)) {
        echo $this->renderFile('@common/documents/pdf/views/single-table/table.php', ['section' => $section, 'name' => 'light', 'rows' => $section->rows, 'formModel' => $section->form, 'edit' => $edit, 'form' => $form, 'url' => Url::to(['protocol/get-row-light'])]);
    }
    ?>
    <div class="">
        <?php echo nl2br($section->preText) ?>
    </div>
    <div style="margin-top:15px">
    	<div class="title" style="margin-bottom: 5px;"><?php echo Yii::t('documents', 'Calculations')?></div>
    	<div class="">
    		<div style="text-transform: uppercase"><?= !$edit ? $section->getAttributeLabel('lightIntensityFormula').':' : '' ?></div>
    		<div style="font-weight:lighter"> <?php echo nl2br($section->lightIntensityFormula) ?></div>
    		<div style="text-transform: uppercase;margin-top:15px"><?= !$edit ? $section->getAttributeLabel('lightUniformityFormula').':' : '' ?></div>
    		<div><?= $edit ? $form->field($section, 'lightUniformityFormula')->textArea() : nl2br($section->lightUniformityFormula) ?></div>
    	</div>
    </div>
    
    <div style="margin-top:15px">
    	<?php echo (!empty($section->time) || !empty($section->testDate) || !empty($section->measurementDate)) ? '<div class="title">'.Yii::t('documents', 'Time measurement').'</div>' : '' ?>
    	<div class="">
    		<div><?= ($edit || !empty($section->time)) ? $section->getAttributeLabel('time').':' : '' ?><strong> <?= !$edit ? $section->time : '' ?></strong></div>
		    <div><?= $edit ? $form->field($section, 'time')->textInput()->label(false) : '' ?></div>
		    <div><?= ($edit || !empty($section->testDate)) ? $section->getAttributeLabel('testDate').':' : '' ?> <strong><?= !$edit ? $section->testDate : '' ?></strong></div>
		    <div><?= $edit ? $form->field($section, 'testDate')->textInput()->label(false) : '' ?></div>
		    <div><?= ($edit || !empty($section->measurementDate)) ? $section->getAttributeLabel('measurementDate').':' : '' ?><strong> <?= !$edit ? $section->measurementDate : '' ?></strong></div>
		    <div><?= $edit ? $form->field($section, 'measurementDate')->textInput()->label(false) : '' ?></div>
    	</div>
    </div>
    
    <?php  echo '<div class="title" style="margin-top:25px">'.Yii::t('documents', 'Remarks').':</div>';?>
    
    <div style="white-space: pre-line;">
        <?= !empty($section->text) ? nl2br($section->text) : $dotsGenerator->repeat(1014) ?>
    </div>

    <div class="text-center" style="margin-top: 40px">
        <div class="col-sm-6" >
            <div style="margin-bottom: 30px"><b><?= Yii::t('documents', 'CREATED BY') ?></b></div>
            <div>....................................................</div>
            <div style="font-size: 12px"><?= '('.(Yii::t('documents', 'Repairer - Contractor')).')' ?></div>
        </div>
        <div class="col-sm-6">
            <div style="margin-bottom: 30px"><b><?= Yii::t('documents', 'ACCEPTED BY') ?></b></div>
            <div>....................................................</div>
            <div style="font-size: 12px"><?= '('.(Yii::t('documents', 'User')).')' ?></div>
        </div>
    </div>
</div>