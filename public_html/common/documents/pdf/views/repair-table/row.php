<div class="margin-0">
	<?php
		if(!empty($row)) {
			$formModel['place'] = $row['place'];
			$formModel['description'] = $row['description'];
		}
	?>

	<div>
		<?= $edit ? $form->field($formModel, "[{$idx}]".'place')
				->textInput(['placeHolder' => $formModel->getAttributeLabel('place')])
				->label(false) : '<strong>'.($row['place'] ?: '').'</strong>' ?>
	</div>
			
	<div style="margin-bottom: 10px">
		<?= $edit ? $form->field($formModel, "[{$idx}]".'description')
				->textArea(['placeHolder' => $formModel->getAttributeLabel('description')])
				->label(false) : (nl2br($row['description']) ?: '') ?>
	</div>
</div>