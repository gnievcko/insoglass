<?php
use yii\web\View;

$this->registerJsFile('@web/js/documents/repair.js?'.uniqid(), ['depends' => [\yii\jui\JuiAsset::className()]]);
$edit = $section->isEditable();
?>
<div class="document-tables" >
    <div >
        <?php
        $i = 1;
        foreach($section->rows as $idx => $row):
            ?>
            <?=
            Yii::$app->controller->renderFile(__DIR__.'/row.php', [
                'idx' => $idx,
                'i' => $i,
                'row' => $row,
                'edit' => $edit,
                'formModel' => $section->form
                    ] + (isset($form) ? ['form' => $form] : []));
            $i++;
            ?>
        <?php endforeach; ?>
    </div>
</div>
<?php
$this->registerJs('
        (function() {
            repair(
                "document-tables",
                {
                    rowUrl: "'.$url.'",
                    edit: '.(int) $edit.'
                }
            );
        })();
    ', View::POS_END);
?>
