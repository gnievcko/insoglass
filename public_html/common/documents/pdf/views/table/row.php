<?php
    use yii\bootstrap\Html;
    use common\helpers\StringHelper;
    $option = ['class' => 'form-control', 'placeHolder' => Yii::t('documents', 'Write value')];
 ?>

<tr class="document-table-row">
    <?php
    foreach($table['theads'] as $key => $th) {
        echo '<td>'.($edit ? Html::input('text', 'tables['.$uniq.'][rows]['.$idx.']['.$key.']', !empty($row[$key]) ? nl2br($row[$key]) : '', $option) : (!empty($row[$key]) ? nl2br($row[$key]) : '')).'</td>';
    }
    ?>
    
</tr>
