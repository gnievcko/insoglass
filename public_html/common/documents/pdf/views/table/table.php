<?php
use yii\helpers\Url;

$this->registerJsFile('@web/js/documents/tables.js?'.uniqid(), ['depends' => [\yii\jui\JuiAsset::className()]]);

$uniq = uniqid();
?>
<div class="document-tables-<?= $uniq ?> document-table" >
    
    <?= Yii::$app->controller->renderFile(__DIR__.'/table-body.php', ['table' => $table, 'uniq' => $uniq, 'edit' => $edit]) ?>

</div>
<?php
$script = '
        (function() {
            initializeTables(
                "document-tables-'.$uniq.'",
                {
                    rowUrl: "'.Url::to(['protocol/get-row']).'",
                    uniq: "'.$uniq.'",
                    edit: '.(int) $edit.',
                    type: "",
                    count: '.count($table['theads']).',
                }
            );
        })()
    ';
if(isset($showScript) && $showScript) {
    echo '<script>'.$script.'</script>';
} else {
    $this->registerJs($script);
}
?>
