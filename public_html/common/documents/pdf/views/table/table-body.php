<?php

use common\helpers\StringHelper;
use common\documents\forms\LightMeasurementForm;
use yii\bootstrap\Html;
use common\documents\sections\ServiceProtocol;
use common\helpers\ArrayHelper;

$option = ['class' => 'form-control', 'placeHolder' => Yii::t('documents', 'Write thead name')];
?>
<?php if($edit): ?>
    <div class="col-sm-11">
        <?= Yii::t('documents', 'Table name'); ?>
        <?= Html::input('text', 'tables['.$uniq.'][name]', !empty($table['name']) ? $table['name'] : '', ['class' => 'form-control']) ?>
    </div>
<?php endif; ?>
<?php if(!$edit) : ?>
    <div style="font-size: 14px; margin: 10px 0px; font-weight: bold"><?= $table['name'] ?></div>
<?php endif; ?>

<?php if(!ArrayHelper::isArrayEmptyTotal($table['theads']) || !ArrayHelper::isArrayEmptyTotal($table['rows'])) { ?>
	<table border="1" class="table-protocol">
	    <thead>
	        <tr>
	            <?php $i = 0;
	            $lastColumn = count($table['theads']) - 1;
	            foreach($table['theads'] as $key => $th) {
	            	$style = '';
	            	if($i == 0) {
	            		$style = ' style="width: 65px"';
	            	}
	            	elseif($i == $lastColumn) {
	            		$style = ' style="width: 120px"';	
	            	}
					echo '<th'.$style.'>'.($edit ? Html::input('text', 'tables['.$uniq.'][theads]['.$key.']', (isset($table['theads'][$key]) ? $table['theads'][$key] : ''), $option) : ($table['theads'][$key] ?: '')).'</td>';
					$i++;
	            }
	            ?>
	            <?php if($edit): ?>
	                <th></th>
	            <?php endif; ?>
	        </tr>
	    </thead>
	    <tbody>
	        <?php foreach($table['rows'] as $idx => $row): ?>
	            <?=
	            Yii::$app->controller->renderFile(__DIR__.'/row.php', [
	                'table' => $table,
	                'idx' => $idx,
	                'row' => $row,
	                'edit' => $edit,
	                'uniq' => $uniq,
	                'formModel' => new ServiceProtocol(),
	            ]);
	            ?>
	        <?php endforeach; ?>
	    </tbody>
	</table>
<?php } ?>
