<?php
use yii\helpers\Url;
use yii\web\View;
use Mindseater\PHPUtils\Text\StringSequenceGenerator;

$dotsGenerator = new StringSequenceGenerator('.');
$edit = $section->isEditable();
$this->title = \Yii::t('documents', 'Weighting CO2 protocol').' - '.Yii::$app->name;
$this->registerJsFile('@web/js/documents/addTable.js?'.uniqid(), ['depends' => [\yii\web\JqueryAsset::className()]]);
?>

<div class="light_measurement">
    <div class="text-center">
        <div style="margin: 25px 0px">
            <h2 class="upper" style="margin-bottom: 0px"><i><?= \Yii::t('documents', 'Weighting CO2 protocol') ?></i></h2> 
            <h3 class="mt-0"><i><?= $section->getAttributeLabel('date').(!empty($section->date) ? $section->date : $dotsGenerator->repeat(10)) ?></i></h3>
                <?= $edit ? $form->field($section, 'date')->textInput()->label(false) : '' ?>
        </div>
    </div>

    <div><?= $section->getAttributeLabel('object') ?><strong> <?= !$edit ? $section->object : '' ?></strong></div>
    <div><?= $edit ? $form->field($section, 'object')->textInput()->label(false) : '' ?></div>
    <div><?= $section->getAttributeLabel('repairer').':'  ?> <?= !empty($section->repairer) ? $section->repairer : $dotsGenerator->repeat(50) ?></div>
    <div><?= $edit ? $form->field($section, 'repairer')->textInput()->label(false) : '' ?></div>
    <br/>
    <div><?= ($edit || !empty($section->measurementTool)) ? $section->getAttributeLabel('measurementTool').':' : '' ?> <strong><?= !$edit ? $section->measurementTool : '' ?></strong></div>
    <div><?= $edit ? $form->field($section, 'measurementTool')->textInput()->label(false) : '' ?></div>
    <div class="weight-tables col-sm-12">
        <?php

        if(empty($section->tables)) {
            $section->tables = [['rows' => [[]]]];
        }
        foreach($section->tables as $table) {
            if(count($table['rows']) > 0) {
                echo $this->renderFile('@common/documents/pdf/views/table/table.php', ['table' => $table, 'edit' => $edit]);
            }
        }
        ?>
    </div>
    <?php
    $this->registerJs('
	            initializeAddTable(
	                "weight-tables",
	                {
	                    rowUrl: "'.Url::to(['protocol/get-table']).'",
    					edit: "'.$edit.'",
                        sectionClass: "weight-protocol",
	                }
	            );
	        
	    ', View::POS_END);
    ?>
    
    <div style="margin: 20px 0px">
        <?= $edit ? $form->field($section, 'text')->textArea()->label(false) :  nl2br($section->text) ?>
    </div>

    <div><strong><?= Yii::t('documents', 'Remarks') ?>:</strong></div>

    <div style="white-space: pre-line;">
        <?= !empty($section->remarks) ? nl2br($section->remarks) : $dotsGenerator->repeat(1014) ?>
    </div>

	<?php if(!empty($section->key)) { ?> 
		<div style="margin-top: 20px">
    		<?= $section->key; ?>
    	</div>
    <?php } ?>

    <div class="text-center" style="margin-top: 20px">
        <div class="col-sm-6">
            <div style="margin-bottom: 30px"><b><?= Yii::t('documents', 'CREATED BY') ?></b></div>
            <div>....................................................</div>
            <div style="font-size: 12px"><?= '('.(Yii::t('documents', 'Repairer - Contractor')).')' ?></div>
        </div>
        <div class="col-sm-6">
            <div style="margin-bottom: 30px"><b><?= Yii::t('documents', 'ACCEPTED BY') ?></b></div>
            <div>....................................................</div>
            <div style="font-size: 12px"><?= '('.(Yii::t('documents', 'User')).')' ?></div>
        </div>
    </div>
</div>