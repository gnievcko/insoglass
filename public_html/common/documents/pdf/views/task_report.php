<?php 
use common\helpers\StringHelper;
use frontend\helpers\UserHelper;
use common\helpers\Utility;
use yii\helpers\ArrayHelper;
$this->title = $section->getAttributeLabel('taskReport').' - '.Yii::$app->name;
?>

<div class="text-center"><h2><i><?= $section->getAttributeLabel('taskReport') ?></i></h2></div>

<div class="text-center">
	<?= $section->getAttributeLabel('forPeriod') . ': ' .  
	$section->dateFrom . ' - ' .
	$section->dateTo ?>
</div>

<div class="text-center"><?= $section->getAttributeLabel('generatedFor') . ': ' . $section->user ?></div>

<div class="text-center"><?= $section->getAttributeLabel('generationDate') . ': ' . $section->dateGenerated ?></div>

<br>

<?php 
$user = null;
if(!empty($section->tableData)) {
	for($i=0; $i<=1; $i++) { ?>
		<h4><?= $i == 0 ? $section->getAttributeLabel('ongoingTasks') : $section->getAttributeLabel('completedTasks') ?></h4>
		<?php if(in_array($i, ArrayHelper::getColumn($section->tableData, 'is_complete'))) { 
			$lp = 1; ?>
			<div class="table-responsive">
				<table class="table-protocol-detail" border="1">
					<tr>
				    	<th><?= $section->getAttributeLabel('Id') ?></th>
				    	<th><?= $section->getAttributeLabel('title') ?></th>
				    	<th><?= $section->getAttributeLabel('taskType') ?></th>
				    	<th><?= $section->getAttributeLabel('taskPriority') ?></th>
				    	<th><?= $section->getAttributeLabel('user') ?></th>
				    	<th><?= $section->getAttributeLabel('dateReminder') ?></th>
				    	<th><?= $section->getAttributeLabel('dateDeadline') ?></th>
				    	<th><?= $section->getAttributeLabel('message') ?></th>
				  	</tr>
					<?php foreach($section->tableData as $row) {
							if($row['is_complete'] == $i) { 
							if(!empty($row['notified_first_name']) && !empty($row['notified_last_name'])) {
								$user = UserHelper::getPrettyUserName($row['notified_email'], $row['notified_first_name'], $row['notified_last_name']);
							}
							else {
								$user = UserHelper::getPrettyUserName($row['email'], $row['first_name'], $row['last_name']);
							}	
						?>
					  		<tr>
					  			<td><?= $lp ?></td>
					    		<td><?= $row['title']?></td>
					    		<td><?= $row['task_type'] ?></td>
					    		<td><?= $row['task_priority'] ?></td>
					    		<td><?= $user ?></td>
					    		<td><?= StringHelper::getFormattedDateFromDate($row['date_reminder']) ?></td>
					    		<td><?= StringHelper::getFormattedDateFromDate($row['date_deadline']) ?></td>
					    		<td><?= $row['message'] ?></td>
					  		</tr>
					  	<?php
							$lp++;
						}
					 }?>
				</table>
			</div>
			<?php }
			else {
				echo $section->getAttributeLabel('none');
			}
		}
	}
	else {
		echo '<h4>' .$section->getAttributeLabel('ongoingTasks') .'</h4>';
		echo $section->getAttributeLabel('none');
		echo '<h4>' .$section->getAttributeLabel('completedTasks') .'</h4>';
		echo $section->getAttributeLabel('none');
	}?>