<?php
    foreach($document->getSectionsSequence() as $section) {
        $dedicatedPdfView = __DIR__ . '/' . strtolower($section->getType()).'.php';
        $htmlView = __DIR__.'/../../html/views/'.strtolower($section->getType()).'.php';
        echo Yii::$app->controller->renderFile(file_exists($dedicatedPdfView) ? $dedicatedPdfView : $htmlView, ['section' => $section]);
    }

?>
