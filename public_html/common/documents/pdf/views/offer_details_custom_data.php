<?php
use yii\helpers\Url;
use yii\web\View;
use common\documents\sections\OfferDetailsCustomData;
	
$edit = $section->isEditable();
$this->registerJsFile('@web/js/documents/addTable.js?'.uniqid(), ['depends' => [\yii\web\JqueryAsset::className()]]);
$this->registerJsFile('@web/js/documents/tables.js?'.uniqid(), ['depends' => [\yii\jui\JuiAsset::className()]]);

?>

<div class="order-table-form">
	<?php if(Yii::$app->params['isOrderCustomDataVisible']) { ?>
		<div class="custom-tables col-sm-12"><?php 
			if(empty($section->tables)) {
				$section->tables = OfferDetailsCustomData::EMPTY_DATA;
			}
			
			if(!OfferDetailsCustomData::isCustomDataEmpty($section->tables)) {
				echo '<h4>'.Yii::t('web', 'Additional data').'</h4>';
				foreach($section->tables as $table) {
					if(count($table['rows']) > 0) {
						echo $this->renderFile('@common/documents/pdf/views/offer-details-custom-data/table.php', ['table' => $table, 'edit' => $edit]);
					}
				}
			}
		?></div>
	<?php } ?>
</div>

<?php
    $this->registerJs('
				initializeAddTable("custom-tables", {
						rowUrl: "' . Url::to(['protocol/get-table']) . '",
	    				edit: "true",
	                    sectionClass: "order-table-form",
    					limitToSingle: true,
    					tableViewsDir: "'.'@common/documents/pdf/views/offer-details-custom-data'.'",
		        });
	        ', View::POS_END);
?>