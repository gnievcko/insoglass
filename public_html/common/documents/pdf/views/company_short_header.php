<?php 
    use yii\helpers\Html;
?>

<div class="row">
    <div class="col-sm-6"> 
        <?= Html::img($section->companyLogo, ['style' => 'width: 200px']) ?>
        <div style="margin-top: 7px; font-size: 12px">
	        <strong><?= $section->companyFullName ?></strong><br />
	        <span><?= $section->companyAddress ?></span><br />
	    </div>
    </div>
    <div class="col-sm-6 text-right"> 
    	<?php if(!empty($section->companyCertificateLogo)) {
        	echo Html::img($section->companyCertificateLogo, ['style' => 'height: 100px']);
    	} ?>
    </div>
</div>
