<?php 
use common\helpers\Utility;
use common\helpers\StringHelper;
$this->title = $section->getAttributeLabel('report-orders').' - '.Yii::$app->name;
?>

<div class="text-center"><h2><i><?= $section->getAttributeLabel('report-goods-delivered') ?></i></h2></div>

<div class="text-center">
	<?= $section->getAttributeLabel('forPeriod') . ': ' .  
	$section->dateFrom . ' - ' .
	$section->dateTo ?>
</div>

<div class="text-center"><?= $section->getAttributeLabel('generatedFor') . ': ' . $section->user ?></div>

<div class="text-center"><?= $section->getAttributeLabel('generationDate') . ': ' . $section->dateGenerated ?></div>

<br><br>

<?php if(!empty($section->data)) {
	$deliveryNumber = null;
	?>
	<?php foreach($section->data as $row) {?>
		<?php if($row['deliveryNumber'] != $deliveryNumber) {?>
			<?php if($deliveryNumber != null) { ?>
				</table>
				<br>
			<?php }?>
			<div>
				<div style="float: left;width:200px"><?= $section->getAttributeLabel('deliveryNumber') . ':' ?></div>
				<div style="float: left;width:200px;"><?= $row['deliveryNumber'] ?></div>
			</div>
			<div>
				<div style="float: left;width:200px"><?= $section->getAttributeLabel('warehouseSupplier') . ':' ?></div>
				<div style="float: left;width:200px;"><?= $row['warehouseSupplierName'] ?></div>
			</div>
			<div>
				<div style="float: left;width:200px"><?= $section->getAttributeLabel('userConfirming') . ':' ?></div>
				<div style="float: left;width:200px;"><?= $row['userConfirmingFirstName'] . ' ' . $row['userConfirmingLastName'] ?> </div>
			</div>
			<div>
				<div style="float: left;width:200px"><?= $section->getAttributeLabel('dateDelivery') . ':' ?></div>
				<div style="float: left;width:200px;"><?= StringHelper::getFormattedDateFromDate($row['dateDelivery'], true) ?> </div>
			</div>
			<?php if(!empty($row['deliveryDescription'])) { ?>
			<div>
				<div style="float: left;width:200px"><?= $section->getAttributeLabel('description') . ':' ?></div>
				<div style="float: left;width:200px;"><?= $row['deliveryDescription'] ?> </div>
			</div>	
			<?php }?>
			<div>
				<div style="float: left;width:200px"><?= $section->getAttributeLabel('totalPrice') . ':' ?></div>
				<div style="float: left;width:200px;">
					<?php echo!empty($row['totalPrice']) ? StringHelper::getFormattedCost($row['totalPrice']) : 0;
					echo ' ' .  $row['currencySymbol'] ?> 
				</div>
			</div>
			
			
			<?php if(Yii::$app->params['isMoreWarehousesAvailable']) {?>
				<div>
					<div style="float: left;width:200px"><?= $section->getAttributeLabel('date') . ':' ?></div>
					<div style="float: left;width:200px;"><?= $row['header']['dateOperation'] ?> </div>
				</div>
			<?php }?>	
		<?php }?>
		<?php if($row['deliveryNumber'] != $deliveryNumber) {?>	
			<table class="table-protocol-detail" border="1">	  	  	
		  		<tr>
		  			<th width="30%"><?= $section->getAttributeLabel('productName') ?></th>
			    	<th width="15%"><?= $section->getAttributeLabel('count') . '/' . $section->getAttributeLabel('sets')  ?></th>
			    	<th width="20%"><?= $section->getAttributeLabel('unitPrice') ?></th>
			    	<th width="35%"><?= $section->getAttributeLabel('note')?>
		  		</tr>
		  	<?php $deliveryNumber = $row['deliveryNumber'] ?>
		 <?php }?>
		  	<tr>
		    	<td><?= !empty($row['parentName']) ? $row['parentName'] . ' (' . $row['productName'] . ')' : $row['productName'] ?></td>
		    	<td><?= $row['deliveryCount']?></td>
		    	<td><?php  if(!empty($row['deliveryPriceUnit'])) {
		    		echo StringHelper::getFormattedCost($row['deliveryPriceUnit']);
		    	}
		    	else {
		    		echo StringHelper::getFormattedCost($row['deliveryPriceGroup']);
		    	}
		    	echo ' ' . $row['currencySymbol']?></td>
		    	<td><?= $row['productNote']?></td>
		  	</tr>
	<?php }	?>
	</table>
<?php }
else {
	echo $section->getAttributeLabel('none');
}?>

