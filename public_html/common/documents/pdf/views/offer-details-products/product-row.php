<?php
use common\helpers\StringHelper;
?>
<tr class="offer-details-product">
    <td><?= $prefix + 1 ?></td>
    <td><?= $product->name ?></td>
    <td><?= StringHelper::getFormattedCost($product->price) ?></td>
    <?php if(!in_array('count', $missedColumns)) { ?>
    	<td><?= $product->unit ?></td>
    	<td><?= $product->count ?></td>
    <?php }
	if(!in_array('totalPrice', $missedColumns)) { ?>
	    <td class="offer-details-product-total-price">
	        <?= StringHelper::getFormattedCost($product->totalPrice) ?>
	    </td>
    <?php } ?>
</tr>
