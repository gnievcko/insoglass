<?php
    use common\helpers\StringHelper;

    $edit = $section->isEditable();
    $currencySymbol = $section->getCurrencySymbol();
?>

<?php foreach($section->products as $idx => $product): ?>
    <?= 
        Yii::$app->controller->renderFile(__DIR__.'/product-row.php', [
            'idx' => $idx, 
            'product' => $product, 
            'edit' => $edit, 
            'currencySymbol' => $currencySymbol,
        ] + (isset($form) ? ['form' => $form] : [])) 
    ?>
<?php endforeach; ?>
<?php 
    $summary = $section->calculateSummary();
    $total = $summary['total'];
    foreach($summary['vats'] as $vat): 
?>
    <tr>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td style="text-align: right">
            <?= StringHelper::getFormattedCost($vat['netValue']).' '.$currencySymbol ?>
        </td>
        <td><?= $vat['vatLabel'] ?></td>
        <td style="text-align: right">
            <?= StringHelper::getFormattedCost($vat['vatAmount']).' '.$currencySymbol ?>
        </td>
        <td <?php if($edit) echo 'colspan="2"' ?> style="text-align: right">
            <?= StringHelper::getFormattedCost($vat['grossValue']).' '.$currencySymbol ?>
        </td>
    </tr>
<?php endforeach; ?>
<tr class="last-row">
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td colspan="2" style="text-align: right"><?= Yii::t('documents', 'Total') ?>:</td>
    <td style="text-align: right">
        <?= StringHelper::getFormattedCost($total['netValue']).' '.$currencySymbol ?>
    </td>
    <td> - </td>
    <td style="text-align: right">
        <?= StringHelper::getFormattedCost($total['vatAmount']).' '.$currencySymbol ?>
    </td>
    <td <?php if($edit) echo 'colspan="2"' ?> style="text-align: right">
        <?= StringHelper::getFormattedCost($total['grossValue']).' '.$currencySymbol ?>
    </td>
</tr>
