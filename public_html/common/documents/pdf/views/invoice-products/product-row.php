<?php
    use yii\helpers\Html;
    use common\helpers\StringHelper;

    $netValue = bcmul($product->price, $product->count, 2);
    $vatAmount = bcmul($netValue, $product->vatRate, 2);
    $grossValue = bcadd($netValue, $vatAmount, 2);

    $recalculateInputsOptions = ['class' => 'recalculates form-control'];
    $numericFieldsCommonOptions = ['type' => 'number'];
    $areaOptions = ['class' => 'form-control smallTexarea invoice-product-name'];
?>

<tr class="invoice-product">
    <td style="text-align: left"><?= $idx + 1 ?></td>
    <td style="text-align: left"><?= $edit ? $form->field($product, "[{$idx}]name")->textArea($areaOptions)->label(false) : $product->name ?></td>
    <td style="text-align: right"><?= $edit ? $form->field($product, "[{$idx}]count")->textInput($recalculateInputsOptions + $numericFieldsCommonOptions)->label(false) : $product->count ?></td>
    <td style="text-align: right"><?= $edit ? $form->field($product, "[{$idx}]unit")->label(false) : $product->unit ?></td>
    <td style="text-align: right"><?= $edit ? $form->field($product, "[{$idx}]category")->label(false) : $product->category ?></td>
    <td style="text-align: right">
        <?= 
            $edit 
                ? $form->field($product, "[{$idx}]price")->textInput($recalculateInputsOptions + $numericFieldsCommonOptions + ['step' => '0.01'])->label(false) 
                : StringHelper::getFormattedCost($product->price).' '.$currencySymbol 
        ?>
    </td>
    <td style="text-align: right"><?= StringHelper::getFormattedCost($netValue).' '.$currencySymbol ?></td>
    <td><?= $edit 
                ? ($form->field($product, "[{$idx}]vatRate")->dropDownList($product->getVatRates(), $recalculateInputsOptions)->label(false))
                : (
                        ($product->vatRate === '') ? Yii::t('documents', 'Vat-free') :
                        (($product->vatRate === 'NP') ? Yii::t('documents', 'Not applicable') : bcmul($product->vatRate, 100, 0).' %')
                        
                  )
        ?>
    </td>

    <td style="text-align: right"><?= StringHelper::getFormattedCost($vatAmount).' '.$currencySymbol ?></td>
    <td style="text-align: right"><?= StringHelper::getFormattedCost($grossValue).' '.$currencySymbol ?></td>
    <?php if($edit): ?>
        <td>
            <?= Html::img('@web/images/placeholders/garbage.png', ['class' => 'remove-invoice-product-btn']) ?>
        </td>
    <?php endif; ?>
</tr>
