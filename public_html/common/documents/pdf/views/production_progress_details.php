<?php
use common\helpers\StringHelper;
?>

<div class="text-center" style="margin: 30px 0 100px">
    <div class="row">
        <div class="col-sm-12">
            <table class="table-protocol-detail" border="1">
                <thead>
                    <tr>
                        <th><?= \Yii::t('main', 'No.'); ?></th>
                        <th><?= StringHelper::translateOrderToOffer('main','Order'); ?></th>
                        <th><?= \Yii::t('main', 'Product'); ?></th>
                        <th><?= \Yii::t('main', 'Created quantity'); ?></th>
                        <th><?= \Yii::t('main', 'Damaged'); ?></th>
                        <th><?= \Yii::t('main', 'Production step'); ?></th>
                    </tr>
                </thead>
                <tbody>
                <?php $index = 1; ?>
                <?php foreach($section->tasks as $task) { ?>
                    <tr>
                        <td>
                            <?= $index++ ?>
                        </td>
                        <td>
                            <?= $task['companyName'].': '.$task['orderNumber'] ?>
                        </td>
                        <td>
                            <?= $task['productCode'] ?>
                        </td>
                        <td style="text-align: center">
                            <?= $task['completedCount'] ?>
                        </td>
                        <td style="text-align: center; <?= ($task['failedCount'] > 0 ? 'color: red; font-weight: bold; ':'' ) ?>">
                            <?= $task['failedCount'] ?>
                        </td>
                        <td style="text-align: center">
                            <?= $task['activityName'] ?>
                        </td>
                    </tr>
                <?php } ?>
                </tbody>
            </table>
        </div>
    </div>
</div>