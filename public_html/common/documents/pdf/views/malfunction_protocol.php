<?php
use Mindseater\PHPUtils\Text\StringSequenceGenerator;

$dotsGenerator = new StringSequenceGenerator('.');

$shortDots = '.........................';
$longDots = '   ..................................................................................................................................................................
		   ..................................................................................................................................................................
		   ..................................................................................................................................................................';
?>
 <div class="text-center">
        <div style="margin: 25px 0px">
            <h2 class="upper" style="margin-bottom: 0px"><i><?= $section->getAttributeLabel('title')?></i></h2> 
            <h3 class="mt-0"><i><?= $section->getAttributeLabel('from') . '  ' . (!empty($section->dateOperation) ? $section->dateOperation : $dotsGenerator->repeat(10)) ?></i></h3>
                <?= $edit ? $form->field($section, 'date')->textInput()->label(false) : '' ?>
        </div>
    </div>

<div style="margin-bottom: 20px"><?= $section->getAttributeLabel('onObject') . ' <strong>' . $section->companyName . ' ' . $section->companyAddress?></strong> </div>

<div style="font-size: 12px">
	<div style="margin-bottom: 10px"><strong><?= $section->getAttributeLabel('serviceArrivalConfirmed') ?></strong></div>
	<div style="margin-bottom: 5px"><strong><?= $section->getAttributeLabel('followingTasksCompleted') ?></strong></div>
	
	<div style="margin-bottom: 5px"><?= $section->getAttributeLabel('reasonHoursLabel') . '  ' ?><?= !empty($section->reasonHours) ? $section->reasonHours : $shortDots ?> </div>
	<div style="margin-bottom: 5px"><?= $section->getAttributeLabel('repairHoursLabel') . '  ' ?><?= !empty($section->repairHours) ? $section->repairHours : $shortDots ?> </div>
</div>

<div class="mt-20"><strong><?= $section->getAttributeLabel('firstHeader')?>:</strong></div>
<div style="margin-top: 5px"><?= !empty($section->textArea1) ?  nl2br($section->textArea1) :  nl2br($longDots) ?></div>
<br>

<div><strong><?= $section->getAttributeLabel('secondHeader')?>:</strong></div>
<div style="margin-top: 5px"><?= !empty($section->textArea2) ? nl2br($section->textArea2) :  nl2br($longDots) ?></div>
<br>

<div><strong><?= $section->getAttributeLabel('thirdHeader')?>:</strong></div>
<div style="margin-top: 5px"><?= !empty($section->textArea3) ? nl2br($section->textArea3) :  nl2br($longDots) ?></div>
<br>

<div><?= $section->getAttributeLabel('fourthHeader')?></div>
<div class="mt-20"><strong><?= $section->getAttributeLabel('remarks')?>:</strong></div>
<div style="margin-top: 5px"><?= !empty($section->textArea4) ? nl2br($section->textArea4) :  $dotsGenerator->repeat(1014) ?></div>
<br>

<div><strong><?= $section->getAttributeLabel('repairer')?>:</strong></div>
<div style="margin-top: 5px"><?= !empty($section->repairer) ? nl2br($section->repairer) :  nl2br($shortDots) ?></div>
<br/>
<div class="text-center" style="margin-top: 30px">
    <div class="col-sm-6">
        <div style="margin-bottom: 30px"><b><?= Yii::t('documents', 'CREATED BY') ?></b></div>
        <div>....................................................</div>
        <div style="font-size: 12px"><?= '('.(Yii::t('documents', 'Repairer - Contractor')).')' ?></div>
    </div>
    <div class="col-sm-6">
        <div style="margin-bottom: 30px"><b><?= Yii::t('documents', 'ACCEPTED BY') ?></b></div>
        <div>....................................................</div>
        <div style="font-size: 12px"><?= '('.(Yii::t('documents', 'User')).')' ?></div>
    </div>
</div>