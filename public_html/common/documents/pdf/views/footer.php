<?php
    $footer = $document->getFooter();
    if($footer !== null) {
        $dedicatedPdfView = __DIR__.'/'.strtolower($footer->getType()).'.php';
        $htmlView = __DIR__.'/../../html/views/'.strtolower($footer->getType()).'.php';
        echo Yii::$app->controller->renderFile(file_exists($dedicatedPdfView) ? $dedicatedPdfView : $htmlView, ['section' => $footer]);
    }
