<?php
	$prevLanguage = Yii::$app->language;
	Yii::$app->language = $section->getLanguage();
?>
<div class="text-center">
	<h3><?= \Yii::t('documents', 'Order confirmation') ?></h3>
	<div class="row">
		<?= $section->getAttributeLabel('number').' '.$section->number." ".strtolower($section->getAttributeLabel('dateCreation'))
		.' '.$section->dateCreation ?>
	</div>
</div>
<div>
	<h4><?= Yii::t('documents', 'Client details') ?></h4>
</div>

<div style="font-size: 12px;">	
	<div class="col-sm-6">
			<div>
				<?= $section->clientCompany ?>
			</div>
			<div>
				<?= $section->clientAddress ?>
			</div>
			<div>
				<?= $section->getAttributeLabel ( 'clientPhone' ).'.: '.$section->clientPhone ?>
			</div>
			<?php if(!empty($section->clientFax)): ?>
				<div>
					<?= $section->getAttributeLabel ( 'clientFax' ).': '.$section->clientFax ?>
				</div>
			<?php endif;?>
			<div>
				<?= $section->getAttributeLabel ( 'payment_terms' ).': '.$section->payment_terms ?>
			</div>
	</div>
	<div>
		<div><?= $section->getAttributeLabel ( 'contactPeople' ).':' ?></div>
		<div>
			<div style="float:right;width:58%;">
				<?php 
					$contactPeople = preg_split('/\r\n|[\r\n]/', $section->contactPeople);
					foreach ($contactPeople as $contactPerson) {
						echo $contactPerson;
						if(!empty($contactPerson)) {
					 		echo '<br/>';
					 	}
					}
				?>
			</div>
			
		</div>
		<?php if(!empty($section->shippingAndHandling)): ?>
			<div>
				<div style="float:left;width:40%;"><?= $section->getAttributeLabel ( 'shippingAndHandling' ) ?></div>
				<div style="float:left;width:60%;"><?= ': '.$section->shippingAndHandling?></div>
			</div>
		<?php endif;?>
		<div style="font-weight: bold;">
				<div style="float:left;width:40%;"><?= $section->getAttributeLabel ( 'shippingDate' ) ?></div>
				<div style="float:left;width:60%;"><?= ': '. $section->shippingDate?></div>
		</div>
		<div>
			<div style="float:left;width:40%;"><?= $section->getAttributeLabel ( 'deliveryAddress' ) ?></div>
			<div style="float:left;width:60%;"><?= ': '. $section->deliveryAddress?></div>
		</div>
	</div>
</div>
<div style="text-align: justify; font-size: 12px;margin-bottom:20px;">
	<div class="col-sm-6">
		<h5><?= $section->getAttributeLabel ( 'description' ) ?></h5>
			<?= $section->description ?>
	</div>
	<?php if(!empty($section->authorFax)): ?>
		<div class="col-sm-6" style="margin-top:20px;">
			<?= Yii::t('documents', 'Please send back signed copy of this document'	).'; '.$section->getAttributeLabel('authorFax').'.: '.$section->authorFax ?>
		</div>
	<?php endif;?>
</div>
<?php	Yii::$app->language = $prevLanguage; ?>