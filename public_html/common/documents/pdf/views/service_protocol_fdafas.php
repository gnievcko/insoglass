<?php
use Mindseater\PHPUtils\Text\StringSequenceGenerator;

$dotsGenerator = new StringSequenceGenerator('.');
$edit = $section->isEditable();
$this->registerJsFile('@web/js/documents/addTable.js?'.uniqid(), ['depends' => [\yii\web\JqueryAsset::className()]]);
$edit = $section->isEditable();
$this->title = \Yii::t('documents', 'Service protocol of fire alarm system').' - '.Yii::$app->name;
?>

<div class="service-protocol">
    
    <div class="text-center">
        <div style="margin: 25px 0px">
            <h2 class="upper" style="margin-bottom: 0px"><i><?= \Yii::t('documents', 'Service protocol of fire alarm system') ?></i></h2> 
            <h3 class="mt-0"><i><?= $section->getAttributeLabel('date').(!empty($section->date) ? $section->date : $dotsGenerator->repeat(10)) ?></i></h3>
                <?= $edit ? $form->field($section, 'date')->textInput()->label(false) : '' ?>
        </div>
    </div>
        
    <div><?= $section->getAttributeLabel('relatedTo').':' ?> <?= (!empty($section->relatedTo) ? $section->relatedTo : $dotsGenerator->repeat(70)) ?></div>
    <div><?= $edit ? $form->field($section, 'relatedTo')->textInput()->label(false) : '' ?></div>

    <div><?= ($edit || !empty($section->object)) ? $section->getAttributeLabel('object') : '' ?> <strong><?=$section->object ?></strong></div>
    <div><?= $edit ? $form->field($section, 'object')->textInput()->label(false) : '' ?></div>
    <div><?= $section->getAttributeLabel('repairer').':' ?> <?= !empty($section->repairer) ? $section->repairer : $dotsGenerator->repeat(50) ?></div>  
    <div><?= $edit ? $form->field($section, 'repairer')->textInput()->label(false) : '' ?></div>
    
    <div class="service-tables col-sm-12">
        <?php

        if(empty($section->tables)) {
            $section->tables = [['rows' => [[]]]];
        }
        foreach($section->tables as $table) {
            if(count($table['rows']) > 0) {
                
                echo $this->renderFile('@common/documents/pdf/views/table/table.php', ['table' => $table, 'edit' => $edit]);
            }
        }
        ?>
    </div>
    
    <div style="margin-bottom: 20px">
        <?= $edit ? $form->field($section, 'text')->textArea()->label(false) : nl2br($section->additionalText) ?>
    </div>
   
    <div><strong><?= Yii::t('documents', 'Remarks') ?>:</strong></div>

    <div style="white-space: pre-line;">
        <?= !empty($section->text) ? nl2br($section->text) : $dotsGenerator->repeat(1014) ?>
    </div>
    
    <div class="row text-center" style="margin-top: 20px">
        <div class="col-sm-6">
            <div style="margin-bottom: 30px"><b><?= Yii::t('documents', 'CREATED BY') ?></b></div>
            <div>....................................................</div>
            <div style="font-size: 12px"><?= '('.(Yii::t('documents', 'Repairer - Contractor')).')' ?></div>
        </div>
        <div class="col-sm-6">
            <div style="margin-bottom: 30px"><b><?= Yii::t('documents', 'ACCEPTED BY') ?></b></div>
            <div>....................................................</div>
            <div style="font-size: 12px"><?= '('.(Yii::t('documents', 'User')).')' ?></div>
        </div>
        
    </div>
</div>

