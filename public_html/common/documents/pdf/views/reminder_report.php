<?php 
$this->title = $section->getAttributeLabel('reminderReport').' - '.Yii::$app->name;
?>

<div class="text-center"><h2><i><?= $section->getAttributeLabel('reminderReport') ?></i></h2></div>

<div class="text-center">
	<?= $section->getAttributeLabel('forPeriod') . ': ' .  
	$section->dateFrom . ' - ' .
	$section->dateTo ?>
</div>

<div class="text-center"><?= $section->getAttributeLabel('generatedFor') . ': ' . $section->user ?></div>

<div class="text-center"><?= $section->getAttributeLabel('generationDate') . ': ' . $section->dateGenerated ?></div>

<br>

<?php 
$lp = 1;
if(!empty($section->data)) { ?>
	<div class="table-responsive">
		<table class="table-protocol-detail" border="1">
			<tr>
		    	<th><?= $section->getAttributeLabel('invoiceNumber') ?></th>
		    	<th><?= $section->getAttributeLabel('clientName') ?></th>
		    	<th><?= $section->getAttributeLabel('clientAddress') ?></th>
		    	<th><?= $section->getAttributeLabel('orderType') ?></th>
		    	<th><?= $section->getAttributeLabel('invoiceAuthor') ?></th>
		    	<th><?= $section->getAttributeLabel('invoicePrice') ?></th>
		  	</tr>
		  	
			<?php foreach($section->data as $row) { ?>			
			  	<tr>
			  		<td><?= $row['invoiceNumber'] ?></td>
			  		<td><?= $row['companyName'] ?></td>
			  		<td><?= $row['companyAddress'] ?></td>
			  		<td><?= $row['orderType'] ?></td>
			  		<td><?= $row['invoiceAuthor'] ?></td>
			  		<td><?= $row['invoicePrice'] ?></td>
			  	</tr>		  	
		  	<?php } ?>
		</table>
	</div>
<?php }
else {
	echo $section->getAttributeLabel('none');
} ?>
