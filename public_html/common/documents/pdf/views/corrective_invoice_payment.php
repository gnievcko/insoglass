<?php
use Mindseater\PHPUtils\Dates\Formatter;
use Mindseater\PHPUtils\Text\StringSequenceGenerator;

$edit = $section->isEditable();
$this->registerJsFile('@web/js/documents/invoiceHelper.js?'.uniqid(), ['depends' => [\yii\web\JqueryAsset::className()]]);
?>

<div class="row">
	<div class="col-sm-6">
	    <div class="row">
	        <div class="col-sm-6">
	            <?= '<b>'.$section->getAttributeLabel('paymentMethod').'</b>: '.$section->paymentMethod ?>
	        </div>
	    </div>
	    <div class="row">
	        <div class="col-sm-6">
	            <?= '<b>'.$section->getAttributeLabel('paymentDate').'</b>: '. (!empty($section->paymentDate) ? (new Formatter())->from($section->paymentDate, 'Y-m-d')->format('d.m.Y')->getDate() : '') ?>
	        </div>
	    </div>
	    <div>
	        <?= '<b>'.$section->getAttributeLabel('remarks').'</b>:<br/> '.nl2br($section->remarks) ?>
	    </div>
	</div>
	<div class="col-sm-6">
	    <div style="margin-bottom: 10px">
	        <?= '<b>'.$section->getAttributeLabel('modifyReason').'</b>:<br/> '.nl2br($section->modifyReason) ?>
	    </div>
	    <div>
	        <?= '<b>'.$section->getAttributeLabel('priceDifference').'</b>:<br/> '.$section->priceDifference ?>
	    </div>
    </div>
</div>
<br/>
<br/>

<?php 
    $dotsGenerator = new StringSequenceGenerator('.');
    $numberOfDots = 50;
?>

<div class="row text-center">
    <div class="col-sm-6">
        <?= Yii::t('documents', 'Signature of person authorized to receive an invoice') ?>
    </div> 
    <div class="col-sm-6">
        <?= Yii::t('documents', 'Signature of person authorized to issue an invoice') ?>
    </div> 
</div>
<div class="row text-center" style="margin-top: 15px">
    <div class="col-sm-6">
        <?= $dotsGenerator->repeat($numberOfDots) ?>
    </div> 
    <div class="col-sm-6">
        <?= $dotsGenerator->repeat($numberOfDots) ?>
    </div> 
</div>
