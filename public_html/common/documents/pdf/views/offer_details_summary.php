<?php if(Yii::$app->params['isOrderNoteVisible']): ?>
	<?php if(!empty($section->note)): ?>
	    <div class="note-section">
	        <?= '<b>'.nl2br($section->note).'</b>' ?>
	    </div>
	    <br/><br/>
	<?php endif; ?>
<?php endif; ?>
<?php if(Yii::$app->params['isOrderRemarksVisible']): ?>
	<?php if(!empty($section->remarks)): ?>
	    <div>
	        <span class="bold italic large-text"><?= $section->getAttributeLabel('remarks') ?>:</span>
	    </div>
	    <div class="remarks-section">
	        <?= nl2br($section->remarks) ?>
	    </div>
	    <br/><br/>
	<?php endif; ?>
<?php endif; ?>
<?php if(Yii::$app->params['isExecutionTimeVisible']): ?>
	<?php if(!empty($section->deadline)): ?>
	    <div>
	        <span>
	            <?= $section->getAttributeLabel('deadline').':' ?>
	        </span>
	        <span>
	            <?= $section->deadline ?>
	        </span>
	    </div>
	<?php endif; ?>
<?php endif; ?>
<?php if(Yii::$app->params['isPaymentTermsVisible']): ?>
	<?php if(!empty($section->termsOfPayment)): ?>
	    <div>
	        <span>
	            <?= $section->getAttributeLabel('termsOfPayment').':' ?>
	        </span>
	        <span>
	            <?= $section->termsOfPayment ?>
	        </span>
	    </div>
	<?php endif; ?>
<?php endif; ?>
<?php if(Yii::$app->params['isDurationTimeVisible']): ?>
	<?php if(!empty($section->validityDate)): ?>
	    <div>
	        <span>
	            <?= $section->getAttributeLabel('validityDate').':' ?>
	        </span>
	        <span>
	            <?= $section->validityDate ?>
	        </span>
	    </div>
	<?php endif; ?>
<?php endif; ?>