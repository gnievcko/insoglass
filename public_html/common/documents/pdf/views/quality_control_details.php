<?php
use common\helpers\StringHelper;
use yii\helpers\Json;
use common\helpers\ArrayHelper;
use yii\helpers\Html;
?>

<div style="margin: 30px 0 30px">
    <div class="row">
        <div class="col-sm-12">
        	<?php if(!empty($section->tasks)) { ?>
                <?php foreach($section->tasks as $order) { ?>
                	<?php if(!empty($order[0])) {
                	    echo $order[0]['orderNumber'] . ', ' . $order[0]['orderTitle'];
                	} ?>
                	<br/>
                	<table class="table-protocol-detail" border="1">
            			<tr>
                			<th><?= Yii::t('main', 'No.'); ?></th>
                            <th><?= Yii::t('main', 'Product'); ?></th>
                            <th><?= Yii::t('main', 'Created quantity'); ?></th>
                            <th><?= Yii::t('main', 'Control date'); ?></th>
                            <th><?= Yii::t('main', 'Control made by'); ?></th>
                            <th><?= Yii::t('main', 'Rate'); ?></th>
                            <th><?= Yii::t('main', 'Comment'); ?></th>
            			</tr>
            			<?php $index = 1;
                        foreach($order as $task) {
                            $qa = Json::decode($task['qa'], true); 
                            
                            // TODO: Kod dostosowany do prostej wersji - dla pełnej wersji musi być to napisane od nowa
                            $generalEvaluationIndex = ArrayHelper::searchAssociativeArrayForValue($qa['fields'], 'symbol', 'general_evaluation');
                            $commentIndex = ArrayHelper::searchAssociativeArrayForValue($qa['fields'], 'symbol', 'comment');
                            
                            $generalEvaluation = !is_null($generalEvaluationIndex) ? $qa['fields'][$generalEvaluationIndex] : null;
                            $comment = !is_null($commentIndex) ? $qa['fields'][$commentIndex] : null;
                            ?>
                            <tr>
                                <td style="text-align: center"><?= $index++ ?></td>
                                <td><?= $task['productName'] ?></td>
                                <td style="text-align: center"><?= $task['count'] ?></td>
                                <td><?= StringHelper::getFormattedDateFromDate($qa['dateCreation'], true) ?></td>
                                <td><?= Html::encode($qa['userName']) ?></td>
                                <td style="text-align: center"><?= $generalEvaluation['value'] ?></td>
                                <td><?= $comment['value'] ?></td>
                            </tr>
                    	<?php } ?>
            		</table>
            		<br/>
        		<?php } 
    		}
    		else {
    	       echo $section->getAttributeLabel('none');
            } ?>
        </div>
    </div>
</div>