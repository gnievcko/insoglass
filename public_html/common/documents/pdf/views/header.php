<?php
    $header = $document->getHeader();
    if($header !== null) {
        $dedicatedPdfView = __DIR__.'/'.strtolower($header->getType()).'.php';
        $htmlView = __DIR__.'/../../html/views/'.strtolower($header->getType()).'.php';
        echo Yii::$app->controller->renderFile(file_exists($dedicatedPdfView) ? $dedicatedPdfView : $htmlView, ['section' => $header]);
    }

