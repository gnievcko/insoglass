<?php

namespace common\documents\forms;

use Yii;
use yii\base\Model;
use common\documents\sections\SectionsMapper;

class RepairReplacementsProtocolForm extends Model implements HasIndexInterface {

    public $name;
    public $count;

    public function rules() {
        return [
                [['name', 'count'], 'safe'],
        ];
    }

    public function init() {
        
    }

    public function attributeLabels() {
        return [
            'name' => Yii::t('documents', 'Name'),
            'count' => Yii::t('documents', 'Count'),
        ];
    }

    public function getType() {
        return SectionsMapper::REPAIR_PROTOCOL;
    }

    public function hasIndex() {
        return false;
    }

}
