<?php
namespace common\documents\forms;

use Yii;
use yii\base\Model;

class OrderConfirmationProductForm extends Model {
	
	public $name;
	public $count;
	public $price;
	public $unit;
	public $vatRate;
	
	private $unitTranslationTable;
	
	public function rules() {
		return [
				[['name'], 'required'],
				[['count', 'price', 'unit'], 'safe'],
				['vatRate', 'in', 'range' => array_keys($this->getVatRates())],
		];
	}
	
	public function init() {
		$this->name = '';
		$this->count = null;
		$this->price = null;
		$this->vatRate = Yii::$app->params['defaultVatRate'];
		$this->unit = !empty(Yii::$app->params['defaultUnit']) ? Yii::t('documents', Yii::$app->params['defaultUnit']) : Yii::t('documents', 'pc.');
		
		$this->unitTranslationTable = ['en' => ['szt.' => 'unit']];
	}
	public function getVatRates() {
		return Yii::$app->params['vatRate'];
	}
	public function attributeLabels() {
		return [
				'name' => Yii::t('main', 'Name'),
				'count' => Yii::t('web', 'Count'),
				'price' => Yii::t('web', 'Price'),
				'vatRate' => Yii::t('documents', 'Vat rate'),
				'unit' => Yii::t('web', 'Count unit'),
				'category' => Yii::t('web', 'PKWiU'),
		];
	}
	
	public function getUnitTranslation($lang){
		$unit = $this->unitTranslationTable[$lang][$this->unit];
		return empty($unit) ? $this->unit : $unit;
	}
}