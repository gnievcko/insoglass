<?php

namespace common\documents\forms;

use Yii;
use yii\base\Model;
use common\helpers\StringHelper;
use common\models\aq\OrderTypeQuery;
use yii\helpers\ArrayHelper;

class OrderRegisterForm extends Model {

    public $dateFrom;
    public $dateTo;
    public $orderTypeIds;
    
    private $orderTypeOptions;

    public function init() {
        parent::init();
        $this->dateFrom = date('d.m.Y', time() - 365 * 24 * 60 * 60);
        $this->dateTo = date('d.m.Y', time() - 335 * 24 * 60 * 60);

        $this->orderTypeIds = [];
        
        $orderTypes = OrderTypeQuery::getOrderTypes();
        $this->orderTypeOptions = ArrayHelper::map($orderTypes, 'id', 'name');        
        $this->orderTypeIds = array_column($orderTypes, 'id');
    }

    public function attributeLabels() {
        return [
	            'dateFrom' => Yii::t('web', 'Date from'),
	            'dateTo' => Yii::t('web', 'Date to'),
	            'orderTypeIds' => StringHelper::translateOrderToOffer('web', 'Order types'),
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
        		[['dateFrom', 'dateTo'], 'required'],
        		[['orderTypeIds'], 'safe'],
        		['dateFrom', 'validateDates'],
        ];
    }
    
    public function validateDates() {
    	if(strtotime($this->dateTo) < strtotime($this->dateFrom)){
    		$this->addError('dateFrom', Yii::t('documents', 'Date "to" cannot be earlier than date "from"'));
    	}
    }

    public function getOrderTypeOptions() {
    	return $this->orderTypeOptions;
    }
}
