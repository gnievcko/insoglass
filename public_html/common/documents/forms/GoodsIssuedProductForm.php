<?php

namespace common\documents\forms;

use Yii;
use yii\base\Model;
use common\documents\sections\SectionsMapper;

class GoodsIssuedProductForm extends Model {
	
	public $location;
	public $name;
	public $count;
	public $parentName;
	public $shelf;
	public $stillage;
	public $unit;
	
	public function rules() {
		return [
				[['name', 'count'] , 'required'],
				[['location', 'parentName', 'shelf', 'stillage', 'unit'], 'safe'],
		];
	}
	
	public function init() {
		$this->name = '';
		$this->count = 0;
		$this->parentName = null;
		$this->location = '';
		$this->unit = !empty(Yii::$app->params['defaultUnit']) ? Yii::t('documents', Yii::$app->params['defaultUnit']) : Yii::t('documents', 'pc.');
	}
	
	public function attributeLabels() {
		return [
				'location' => Yii::t('web', 'Location'),
				'name' => Yii::t('main', 'Name'),
				'count' => Yii::t('web', 'Count'),
				'unit' => Yii::t('main', 'Unit'),
		];
	}
	
	public function getType() {
		return SectionsMapper::GOODS_ISSUED_PRODUCTS;
	}
}
