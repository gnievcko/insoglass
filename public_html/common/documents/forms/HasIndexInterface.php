<?php
namespace common\documents\forms;

interface HasIndexInterface {
	
	public function hasIndex();
	
}