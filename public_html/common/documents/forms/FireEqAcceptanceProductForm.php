<?php

namespace common\documents\forms;

use Yii;
use yii\base\Model;
use common\documents\sections\SectionsMapper;

class FireEqAcceptanceProductForm extends Model {

	public $number;
	public $text;

	public function rules() {
		return [
            [['number', 'text'], 'safe'],
		];
	}

	public function init() {
		$this->number = '';
		$this->text = '';
	}

	public function attributeLabels() {
		return [
				'hallNumber' => Yii::t('main', 'Hall number'),
		];
	}

	public function getType() {
		return SectionsMapper::FIREEQ_ACCEPTANCE_PRODUCTS;
	}
}
