<?php

namespace common\documents\forms;
use common\documents\sections\SectionsMapper;

class InvoiceProductPrecorrectedForm extends InvoiceProductForm {

    public function getType() {
        return SectionsMapper::INVOICE_PRODUCTS_PRECORRECTED;
    }

}
