<?php

namespace common\documents\forms;

use Yii;
use yii\base\Model;
use common\documents\sections\SectionsMapper;
use common\models\ar\Currency;
use common\helpers\Utility;

class GoodsReceivedProductForm extends Model {
	
	public $index;
	public $name;
	public $count;
	public $price;
	public $priceUnit;
	public $priceGroup;
	public $parentName;
	public $unit;
	public $currencySymbol;
	public $costSum;
	public $note;
	
	public function rules() {
		return [
				[['name', 'count', 'price', 'unit', 'currencySymbol', 'costSum'] , 'required'],
				[['index', 'priceUnit', 'priceGroup', 'parentName', 'note'], 'safe'],
		];
	}
	
	public function init() {
		$this->name = '';
		$this->count = 0;
		$this->price = '0.00';
		$this->priceUnit = null;
		$this->priceGroup = null;
		$this->parentName = null;
		$this->costSum = '0.00';
		$this->unit = !empty(Yii::$app->params['defaultUnit']) ? Yii::t('documents', Yii::$app->params['defaultUnit']) : Yii::t('documents', 'pc.');		
		$this->currencySymbol = Currency::find()->where(['symbol' => Utility::CURRENCY_PLN])->one()->short_symbol;
		$this->index = '';
	}
	
	public function attributeLabels() {
		return [
				'index' => Yii::t('main', 'Warehouse index'),
				'name' => Yii::t('main', 'Name'),
				'count' => Yii::t('web', 'Count'),
				'price' => Yii::t('web', 'Price'),
				'unit' => Yii::t('documents', 'Unit'),
				'costSum' => Yii::t('documents', 'Value'),
				'note' => Yii::t('main','Description'),
		];
	}
	
	public function getType() {
		return SectionsMapper::GOODS_RECEIVED_PRODUCTS;
	}
}
