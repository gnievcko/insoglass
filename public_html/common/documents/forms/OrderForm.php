<?php

namespace common\documents\forms;

use Yii;
use yii\base\Model;
use common\models\aq\OrderQuery;
use common\helpers\StringHelper;

class OrderForm extends Model {
	
	public $dateFrom;
	public $dateTo;
	public $user;
	public $dateGenerated;
	
	public $statistics; //[openOrderCount, closedOrderCount, ongoingOrderCount, handledOrderCount]
	
	public $orderCountByStatus;
	
	public $orderCountByClient;
	
	public $orderCountByAuthor;
	
	public $orderCountByEmployeeUpdating;
	
	public $upcommingOrders;

	/*
	public function rules() {
		return [
				[['name', 'count', 'price', 'unit'] , 'required'],
		];
	}
*/
	public function init() {
		/*
		$this->dateFrom = $dateFrom;
		$this->dateTo = $dateTo;
		$this->date = StringHelper::getFormattedDateFromTimestamp(time(), true);
		$this->user = Yii::$app->user->first_name . ' ' . Yii::$app->user->last_name;
		
		$this->statistics = OrderQuery::getOrderStatistics($this->dateFrom, $this->dateTo);
		$this->orderCountByStatus = OrderQuery::getOrderCountByStatus($this->dateFrom, $this->dateTo);
		$this->orderCountByClient = OrderQuery::getOrderCountByClient($this->dateTo);
		$this->orderCountByAuthor = OrderQuery::getOrderCountByAuthor($this->dateFrom, $this->dateTo);
		$this->orderCountByEmployeeUpdating = OrderQuery::getorderCountByEmployeeUpdating($this->dateFrom, $this->dateTo);
		*/
	}

	public function attributeLabels() {
		/*
		return [
				'name' => Yii::t('main', 'Name'),
				'count' => Yii::t('web', 'Count'),
				'price' => Yii::t('web', 'Price'),
				'vatRate' => Yii::t('documents', 'Vat rate'),
				'unit' => Yii::t('web', 'Count unit'),
		];
		*/
	}

	public function getType() {
		return SectionsMapper::INVOICE_PRODUCTS;
	}
}