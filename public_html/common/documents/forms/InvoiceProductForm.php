<?php

namespace common\documents\forms;

use Yii;
use yii\base\Model;

class InvoiceProductForm extends Model {

    public $name;
    public $count;
    public $price;
    public $unit;
    public $category;
    public $vatRate;

    public function rules() {
        return [
                [['name'], 'required'],
                [['count', 'price', 'category', 'unit'], 'safe'],
                ['vatRate', 'in', 'range' => array_keys($this->getVatRates())],
        ];
    }

    public function init() {
        $this->name = '';
        $this->count = null;
        $this->price = null;
        $this->vatRate = Yii::$app->params['defaultVatRate'];
        $this->unit = !empty(Yii::$app->params['defaultUnit']) ? Yii::t('documents', Yii::$app->params['defaultUnit']) : Yii::t('documents', 'pc.');
    }

    public function attributeLabels() {
        return [
                'name' => Yii::t('main', 'Name'),
                'count' => Yii::t('web', 'Count'),
                'price' => Yii::t('web', 'Price'),
                'vatRate' => Yii::t('documents', 'Vat rate'),
                'unit' => Yii::t('web', 'Count unit'),
        		'category' => Yii::t('web', 'PKWiU'),
        ];
    }

    public function getVatRates() {
        return Yii::$app->params['vatRate'];
    }

    public function getType() {
        return SectionsMapper::INVOICE_PRODUCTS;
    }

}
