<?php

namespace common\documents\forms;

use Yii;
use yii\base\Model;
use common\documents\sections\SectionsMapper;

class WeightProtocolForm extends Model implements HasIndexInterface {

    public $weight;
    public $manufacturer;
    public $place;
    public $number;
    public $year;
    public $result;

    public function rules() {
        return [
                [['weight', 'manufacturer', 'place', 'number', 'year', 'result'], 'safe'],
        ];
    }

    public function init() {
        
    }

    public function attributeLabels() {
        return [
            'weight' => Yii::t('documents', 'Net/gross weight'),
            'manufacturer' => Yii::t('documents', 'Manufacturer'),
            'place' => Yii::t('documents', 'Installation place'),
            'number' => Yii::t('documents', 'Number'),
            'year'  => Yii::t('documents', 'Year of production'),
            'result' => Yii::t('documents', 'Result of control/weight'),
        ];
    }

    public function getType() {
        return SectionsMapper::WEIGHT_PROTOCOL;
    }

    public function hasIndex() {
        return false;
    }

}
