<?php

namespace common\documents\forms;

use Yii;
use yii\base\Model;
use common\documents\sections\SectionsMapper;

class RepairProtocolForm extends Model {

    public $place;
    public $description;

    public function rules() {
        return [
                [['place', 'description'], 'safe'],
        ];
    }

    public function init() {
        
    }

    public function attributeLabels() {
        return [
            'place' => Yii::t('documents', 'Place'),
            'description' => Yii::t('documents', 'Description'),
        ];
    }

    public function getType() {
        return SectionsMapper::REPAIR_PROTOCOL;
    }

}
