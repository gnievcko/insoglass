<?php

namespace common\documents\forms;

use Yii;
use yii\base\Model;
use common\helpers\StringHelper;

class OfferDetailsProductForm extends Model {

    public $name;
    public $count;
    public $unit;
    public $price;
    public $totalPrice;

    public function rules() {
        return [
                [['name', 'count', 'price', 'totalPrice', 'unit'], 'safe'],
        ];
    }

    public function load($data, $formName = null) {
        parent::load($data, $formName);
        $this->count = !empty($this->count) ? (int) $this->count : null;
        $this->price = !empty($this->price) ? $this->price : null;
        $this->totalPrice = bcmul($this->price, $this->count, 2);
        $this->unit = !empty($this->unit) ? $this->unit : null;
    }

    public function attributeLabels() {
        return [
                'name' => Yii::t('main', 'Name'),
                'count' => Yii::t('web', 'Count'),
                'price' => Yii::t('web', 'Price'),
        		'unit' => Yii::t('documents', 'M.u.'),
        ];
    }

    public function init() {
        $this->name = '';
        $this->count = null;
        $this->price = null;
        
        $this->unit = !empty(Yii::$app->params['defaultUnit']) ? Yii::t('documents', Yii::$app->params['defaultUnit']) : null;

        $this->totalPrice = 0.00;
    }

}
