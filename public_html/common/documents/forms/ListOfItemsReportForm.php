<?php

namespace common\documents\forms;

use Yii;
use yii\base\Model;
use yii\helpers\ArrayHelper;
use common\models\aq\ProductCategoryQuery;

class ListOfItemsReportForm extends Model {

    public $dateFrom;
    public $dateTo;
    public $categoryIds;
    
    private $categoryOptions;

    public function init() {
        parent::init();
        $this->dateFrom = date('d.m.Y', time() - 365 * 24 * 60 * 60);
        $this->dateTo = date('d.m.Y', time() - 335 * 24 * 60 * 60);

        $this->categoryIds = [];        
        
        $categories = ProductCategoryQuery::getCategoryList(false);
        $this->categoryOptions = ArrayHelper::map($categories, 'id', 'name');
        
        foreach($this->categoryOptions as $id => $name) {
        	if($name != 'Wyposażenie') {
        		$this->categoryIds[] = $id;
        	}
        }
    }

    public function attributeLabels() {
        return [
	            'dateFrom' => Yii::t('web', 'Date from'),
	            'dateTo' => Yii::t('web', 'Date to'),
	            'categoryIds' => Yii::t('web', 'Product categories'),
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
        		[['dateFrom', 'dateTo'], 'required'],
        		[['categoryIds'], 'safe'],
        		['dateFrom', 'validateDates'],
        ];
    }
    
    public function validateDates() {
    	if(strtotime($this->dateTo) < strtotime($this->dateFrom)){
    		$this->addError('dateFrom', Yii::t('documents', 'Date "to" cannot be earlier than date "from"'));
    	}
    }

    public function getCategoryOptions() {
    	return $this->categoryOptions;
    }
}
