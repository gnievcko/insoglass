<?php

namespace common\documents\forms;

use Yii;
use yii\base\Model;
use common\documents\sections\SectionsMapper;

class RepairTestsProtocolForm extends Model implements HasIndexInterface{

    public $device;
    public $result;

    public function rules() {
        return [
                [['device', 'result'], 'safe'],
        ];
    }

    public function init() {
        
    }

    public function attributeLabels() {
        return [
            'device' => Yii::t('documents', 'Device'),
            'result' => Yii::t('documents', 'Result'),
        ];
    }

    public function getType() {
        return SectionsMapper::REPAIR_PROTOCOL;
    }

    public function hasIndex() {
        return false;
    }

}
