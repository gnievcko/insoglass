<?php

namespace common\documents\forms;

use Yii;
use yii\base\Model;
use common\documents\sections\SectionsMapper;

class LightMeasurementForm extends Model implements HasIndexInterface {

    public $name;
    public $value;

    public function rules() {
        return [
                [['name', 'value'], 'safe'],
        ];
    }

    public function init() {
        
    }

    public function attributeLabels() {
        return [
            'name' => Yii::t('documents', 'Measurement point'),
            'value' => Yii::t('documents', 'Value of measurement'),
        ];
    }

    public function getType() {
        return SectionsMapper::LIGHT_MEASUREMENT;
    }

    public function hasIndex() {
        return false;
    }

}
