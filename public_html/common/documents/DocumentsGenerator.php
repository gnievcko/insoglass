<?php

namespace common\documents;

use Yii;
use MongoDB\BSON\ObjectID;
use common\documents\sections\SectionsMapper;
use common\documents\exceptions\StorageNotFoundException;
use common\models\ar\InvoiceHistory;
use common\models\aq\InvoiceHistoryQuery;

class DocumentsGenerator {

    public static function reconstruct($documentId, $historyId, $edit = false) {
    	if(!isset(Yii::$app->params['isDocumentStorageAvailable']) || !Yii::$app->params['isDocumentStorageAvailable']) {
			throw new StorageNotFoundException();
    	}
    	
    	$query = ['_id' => new ObjectID($documentId), 'history._id' => new ObjectID($historyId)];
        $fieldsToFetch = [
            'type' => 1, 
            'history.$' => 1, 
            'sections' => 1, 
            'header' => 1, 
            'footer' => 1,
        ];
        $documentsCollection = Yii::$app->mongodb->getCollection('documents');
        $documentData = $documentsCollection->findOne($query, $fieldsToFetch);
        if($documentData == null) {
            throw new \InvalidArgumentException("Document with id {$documentId} and history id {$historyId} does not exist");
        }
                
        return self::reinitializeDocument($documentData, $edit);
    }

    private static function reinitializeDocument($documentData, $edit) {
        if($edit) {
            $document = new Document($documentData['type'], (string)$documentData['_id']);
        } else {
            $document = new Document($documentData['type']);
        }
        self::generateHeader($document, $documentData);
        self::generateSections($document, $documentData['sections']);
        self::generateFooter($document, $documentData);
        $document->loadFromHistoricalData($documentData['history'][0]);

        return $document;
    }

    private static function generateHeader($document, $documentData) {
        if(!empty($documentData['header'])) {
            $headerClass = SectionsMapper::SECTIONS[$documentData['header']];
            $document->setHeader(new $headerClass());
        }
    }

    private static function generateSections($document, $sectionsTypes) {
        foreach($sectionsTypes as $sectionType) {
            $sectionClass = SectionsMapper::SECTIONS[$sectionType];
            $document->appendSection(new $sectionClass());
        }
    }

    private static function generateFooter($document, $documentData) {
        if(!empty($documentData['footer'])) {
            $footerClass = SectionsMapper::SECTIONS[$documentData['footer']];
            $document->setFooter(new $footerClass());
        }
    }

    public static function generate($documentType) {
        $document = new Document($documentType);

        self::prepareStaticParts($document);
        self::prepareDocumentSections($document);

        return $document;
    }

    private static function prepareStaticParts($document) {
        $documentTypeData = (new \yii\db\Query())->select([
            'header_section.symbol as header', 'footer_section.symbol as footer', 'dt.margin_top', 'dt.margin_bottom', 'dt.margin_left', 'dt.margin_right',
            'dt.format', 'dt.orientation',
        ])
            ->from('document_type dt')
            ->join('LEFT JOIN', 'document_section header_section', 'header_section.id=dt.document_section_header_id')
            ->join('LEFT JOIN', 'document_section footer_section', 'footer_section.id=dt.document_section_footer_id')
            ->where(['dt.symbol' => $document->getType()])
            ->one();

        $document->marginTop = $documentTypeData['margin_top'];
        $document->marginBottom = $documentTypeData['margin_bottom'];
        $document->marginLeft = $documentTypeData['margin_left'];
        $document->marginRight = $documentTypeData['margin_right'];
        $document->format = $documentTypeData['format'];
        $document->orientation = $documentTypeData['orientation'];

        self::generateHeader($document, $documentTypeData);
        self::generateFooter($document, $documentTypeData);
    }

    private static function prepareDocumentSections($document) {
        $sectionsTypes = (new \yii\db\Query())->select(['ds.symbol'])
            ->from('document_section ds')
            ->join('INNER JOIN', 'document_type_section dts', 'dts.document_section_id = ds.id')
            ->join('INNER JOIN', 'document_type dt', 'dt.id = dts.document_type_id')
            ->andWhere(['dt.symbol' => $document->getType()])
            ->orderBy(['dts.sequence_number' => SORT_ASC])
            ->column();
		
        self::generateSections($document, $sectionsTypes);
    }

    public static function save($document, $updateNumeration = true) {
    	$sections = $document->getSectionsSequence();
    	if(!empty($sections)) {
    		foreach($sections as $section) {
    			$section->save();
    		}
    	}
    	
    	if(isset(Yii::$app->params['isDocumentStorageAvailable']) && Yii::$app->params['isDocumentStorageAvailable']) {
	        $documentsCollection = Yii::$app->mongodb->getCollection('documents');
	        $documentId = ($document->getId() === null) ? new ObjectID() : new ObjectID($document->getId());
            $historyId = new ObjectID();
	        $serializedDocument = $document->toArray();
	        $documentHistoryEntry = ['_id' => $historyId, 'createdAt' => time(), 'authorId' => Yii::$app->user->id] + $document->toArray();
	        Yii::$app->mongodb->createCommand()->update('documents', ['_id' => $documentId], [
	            '$push' => ['history' => $documentHistoryEntry],
	            '$set' => [
	                '_id' => $documentId, 
	                'type' => $document->getType(), 
	                'sections' => $document->getSectionsQueue(), 
	                'header' => empty($document->getHeader()) ? null : $document->getHeader()->getType(),
	                'footer' => empty($document->getFooter()) ? null : $document->getFooter()->getType(),
	            ],
	        ], ['upsert' => true]);
	        
	        if(in_array($document->getType(), DocumentType::getInvoiceTypes())) {
	            InvoiceHistoryQuery::saveInvoiceHistory($document, $documentId->__toString(), $updateNumeration);
	        }
	        
            return ['documentId' =>  $documentId, 'historyId' => $historyId];
    	}
        return ['documentId' => null, 'historyId' => null];
    }
}
