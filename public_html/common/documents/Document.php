<?php

namespace common\documents;

use yii\widgets\ActiveForm;
use common\models\ar\Language;

class Document {

    public $marginTop = 0;
    public $marginBottom = 0;
    public $marginLeft = 0;
    public $marginRight = 0;
    public $format = 'A4';
    public $orientation = 'P';
	
    private $id;
    private $type;
    private $isEditable = false;
    private $header = null;
    private $sections = [];
    private $sectionsQueue = [];
    private $footer = null;
    
    public function __construct($type, $id = null) {
        $this->id = $id;
        $this->type = $type;
    }

    public function getType() {
        return $this->type;
    }

    public function makeEditable() {
        $this->setEditable(true);
    }

    public function makeReadOnly() {
        $this->setEditable(false);
    }

    private function setEditable($isEditable) {
        $this->isEditable = $isEditable;
        foreach($this->sections as $section) {
            $section->setEditable($this->isEditable);
        }
    }

    public function isEditable() {
        return $this->isEditable;
    }

    public function getSectionsSequence() {
        $sectionsSequence = [];
        foreach($this->sectionsQueue as $sectionName) {
            $sectionsSequence[] = $this->sections[$sectionName];
        }

        return $sectionsSequence;
    }

    public function getSectionsQueue() {
        return $this->sectionsQueue;
    }

    public function setHeader($header) {
        $this->header = $header;
    }

    public function getHeader() {
        return $this->header;
    }

    public function setFooter($footer) {
        $this->footer = $footer;
    }

    public function getFooter() {
        return $this->footer;
    }

    public function appendSection($section) {
        $this->sections[$section->getType()] = $section;
        $this->sectionsQueue[] = $section->getType();
    }

    public function loadInput($input) {
        foreach($this->sections as $section) {
            $section->loadInput($input);
        }
    }

    public function getSection($sectionName) {
    	return $this->sections[$sectionName];
    }

    public function loadFromHistoricalData($data) {
        if(!empty($this->header)) {
            $this->header->loadInput([$this->header->formName() => $data['header']]);
        }
        foreach($data['sections'] as $sectionData) {
        	$sectionType = $sectionData['SECTION_TYPE'];
            $section = $this->sections[$sectionType];

            $section->loadInput([$section->formName() => $sectionData]);
        }
        if(!empty($this->footer)) {
            $this->footer->loadInput([$this->footer->formName() => $data['footer']]);
        }

        $this->marginTop = $data['marginTop'];
        $this->marginBottom = $data['marginBottom'];
        $this->marginLeft = $data['marginLeft'];
        $this->marginRight = $data['marginRight'];
        $this->format = $data['format'];
        $this->orientation = $data['orientation'];
    }

    public function getId() {
        return $this->id;
    }

    public function toArray() {
        $sections = array_map(function($sectionName) {
            $section = $this->sections[$sectionName];
            $serializedSection = $section->toArray() + ['SECTION_TYPE' => $section->getType()];
            return $serializedSection;
        }, $this->sectionsQueue);		
        return [
            'header' => empty($this->header) ? null : $this->header->toArray() + ['SECTION_TYPE' => $this->header->getType()],
            'sections' => $sections,
            'footer' => empty($this->footer) ? null : $this->footer->toArray() + ['SECTION_TYPE' => $this->footer->getType()],
            'marginTop' => $this->marginTop,
            'marginBottom' => $this->marginBottom,
            'marginLeft' => $this->marginLeft,
            'marginRight' => $this->marginRight,
            'orientation' => $this->orientation,
            'format' => $this->format,
        ];
    }

    public function getModels() {
        $nestedModels = array_map(function($section) {
            return $section->getModels();
        }, $this->sections);

        if(!empty($this->header)) {
            $nestedModels[] = [$this->header];
        }
        if(!empty($this->footer)) {
            $nestedModels[] = [$this->footer];
        }

        return array_reduce($nestedModels, function($flatModels, $modelsArray) {
            return array_merge($flatModels, $modelsArray);
        }, []);
    }

    public function performActiveValidation() {
        return array_reduce($this->getModels(), function($errors, $model) {
            return $errors + (is_array($model) ? ActiveForm::validateMultiple($model) : ActiveForm::validate($model));
        }, []);
    }
}
