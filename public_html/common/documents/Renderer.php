<?php

namespace common\documents;

interface Renderer {
    public function render(Document $document);
}
