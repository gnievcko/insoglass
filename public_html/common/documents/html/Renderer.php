<?php

namespace common\documents\html;

use Yii;
use common\documents\Document;
use common\documents\Renderer as DocumentRenderer;
use common\models\aq\DocumentQuery;
use common\documents\DocumentType;

class Renderer implements DocumentRenderer {
    
    public function render(Document $document) {
    	$lastInvoiceDate = null;
    	
    	if(in_array($document->getType(), array_diff(DocumentType::getInvoiceTypes(), [DocumentType::CORRECTIVE_INVOICE])) 
    			&& Yii::$app->params['isDocumentStorageAvailable']) {
    		$orderId = $document->getSectionsSequence()[1]['orderId'];
    		$lastInvoiceDate = DocumentQuery::getLastInvoiceDateByOrderId($orderId, $document->getType());
    	}
    	
        return Yii::$app->controller->renderFile(__DIR__.'/views/document.php', [
        		'document' => $document, 
        		'lastInvoiceDate' => $lastInvoiceDate,
        ]);
    }
}
