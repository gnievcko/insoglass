<?php
$edit = $section->isEditable();
    
$this->title = Yii::t('documents', 'Custom protocol').' - '.Yii::$app->name;
?>


<div class="text-center">
	<div class="title-with-subtitle">
    	<div style="width: 60%;	margin: 16px auto;"><?= $edit ? $form->field($section, 'title')->textarea(['class' => 'form-control smallTextarea', 'placeholder' => Yii::t('documents','Type name of the protocol')])->label(false) : $section->title ?></div> 
        	<div class="document-date">
                <div><h4 class="mt-0"><?= $section->getAttributeLabel('date') ?>: 
        		<?= $edit ? $form->field($section, 'date')->textInput()->label(false) : $section->date ?></h4></div>
            </div>
    </div>
</div>