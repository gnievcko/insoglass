<?php

use yii\bootstrap\Html;
use common\documents\sections\OfferDetailsCustomData;

$option = ['class' => 'form-control xSmallTextarea', 'placeHolder' => Yii::t('documents', 'Write thead name')];
?> 

<div class="table-responsive">
    <table class="table page-break-inside table-bordered grid-table invoice-table">
        <thead>
            <tr>
                <?php 
	           	$i = 0;
	           	foreach($table['theads'] as $key => $th) {
	           		$style = '';
	           		if($edit) {
		           		$style = ($i == 0 ? 'width: 7%' : ($i == 1 ? 'width: 29%' : ''));
		           		if(!empty($style)) {
		           			$style = ' style="'.$style.'"';
		           		}
	           		}
	           		
	            	echo '<th'.$style.'>'.($edit ? Html::textarea('tables['.$uniq.'][theads]['.$key.']', (isset($table['theads'][$key]) ? $table['theads'][$key] : ''), $option) : ($table['theads'][$key] ?: '-')).'</td>';
	            	$i++;
	            } ?>
                <?php if($edit): ?>
                    <th></th>
                <?php endif; ?>
            </tr>
        </thead>
        <tbody>
            <?php 
            $i = 0;
            foreach($table['rows'] as $idx => $row): ?>
                <?=
                Yii::$app->controller->renderFile(__DIR__.'/row.php', [
                    'table' => $table,
                    'idx' => $i,
                    'row' => $row,
                    'edit' => $edit,
                    'uniq' => $uniq,
                    'formModel' => new OfferDetailsCustomData(),
                ]);
                ?>
            <?php 
            $i++;
            endforeach; ?>
        </tbody>
        <tfoot>
        	<tr>
	        	<?php for($i = 0; $i < count($table['theads']) - 2; ++$i) { ?>
	        		<td></td>
	        	<?php } ?>
	        	<td><?= Yii::t('main', 'Total') ?></td>
	        	<td><span name="tables[<?= $uniq ?>][summary]">0</span></td> 
	        	<td></td>
        	</tr>
        </tfoot>
    </table>
</div>