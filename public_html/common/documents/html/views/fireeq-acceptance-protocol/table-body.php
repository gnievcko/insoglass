<?php
    use common\helpers\StringHelper;
    $edit = $section->isEditable();
?>

<?php 
$i = 0;
foreach($section->products as $idx => $product): ?>
    <?= 
        Yii::$app->controller->renderFile(__DIR__.'/product-row.php', [
            'idx' => $i, 
            'product' => $product, 
            'edit' => $edit, 
        ] + (isset($form) ? ['form' => $form] : [])) 
    ?>
<?php 
$i++;
endforeach; 
?>
