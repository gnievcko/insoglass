<?php 
use yii\helpers\Html;
use yii\web\View;
?>


<table class="fireeq-acceptance-row" width="100%">
	<tr>
    	<td width="30%"><?= $form->field($row, "[{$idx}]number")->textInput(['placeholder' => Yii::t('main', 'Title')])->label(false) ?></div></td>
	</tr>
	<tr>
		<td width="90%">
			<?= $form->field($row, "[{$idx}]text")->textArea(['placeholder' => Yii::t('main', 'Description')])->label(false) ?>
		</td>
		<td width="10%" align="center">
			<?php if($editMode) {?>
				<div>
					<div class="base-icon garbage-icon action-icon remove-fireeq-acceptance-row-btn"></div>
				</div>
				
    		<?php }?>
		</td>

	</tr>
</table>

<?php  $this->registerJs('
    (function() {
       $(document).on("click",".remove-fireeq-acceptance-row-btn", function (e) {
	  		e.preventdefault;
	  		$(this).parent().parent().parent().parent().remove();
			$("#firegearRowCount").val($(".fireeq-acceptance-row").size());
		});
    })();
    ', View::POS_END);
?>




