<?php

use yii\helpers\Url;

$edit = $section->isEditable();
$this->title = \Yii::t('documents', 'Repair protocol').' - '.Yii::$app->name;
?>
<div class="light_measurement">
    <div class="text-center">
        <div>
            <h3 class="upper"><?= \Yii::t('documents', 'Repair protocol') ?></h3> 
            <div class="document-date">
                <div><h4 class="mt-0"><?= $section->getAttributeLabel('date').(!$edit ? $section->date : '') ?></h4></div>
                <?= $edit ? $form->field($section, 'date')->textInput()->label(false) : '' ?>
            </div>
        </div>
    </div>
	<div class="col-xs-12">
		<div class="row">
		    <div class="col-sm-2"><?= $section->getAttributeLabel('object') ?> <?= !$edit ? $section->object : '' ?></div>
		    <div class="col-sm-5"><?= $edit ? $form->field($section, 'object')->textInput(['placeholder' => Yii::t('documents','Type an object')])->label(false) : '' ?></div>
	    </div>
	</div>
	<div class="col-xs-12">
		<div class="row">
		    <div class="col-sm-2"><?= ($edit || !empty($section->repairer)) ? $section->getAttributeLabel('repairer').':': '' ?> <?= !$edit ? $section->repairer : '' ?></div>
		    <div class="col-sm-5"><?= $edit ? $form->field($section, 'repairer')->textInput(['placeholder' => Yii::t('documents','Type a repairer')])->label(false) : '' ?></div>
	   </div>
	</div>
	 <div class="col-xs-12">
		<div class="row">  
		    <div class="col-sm-2"><?= ($edit || !empty($section->description)) ? $section->getAttributeLabel('description').':': '' ?></div>
		    <div class="col-sm-5"><?= $edit ? $form->field($section, 'description')->textArea(['placeholder' => Yii::t('documents','Type description here')])->label(false) : $section->description ?></div>
		</div>
	</div>
	
	 <?php if($edit): ?>
        <div class="col-xs-12 title-with-icon">
            <div class="base-icon description-icon main-content-icons"></div>
            <h3><?php echo Yii::t('documents','List of places of repairs\' list') ?></h3>         
        </div>
    <?php endif; ?>
	
	
	<div class="col-xs-12">
		<div class="row">
	    <?php
	    if($edit || count($section->rows)) { ?>
	    <div class="col-sm-7">
	        <?php echo ( $edit ? $form->field($section, 'tableName')->textInput()->label(false) : $section->tableName) ;?>
	    </div>
	        <?php echo $this->renderFile('@common/documents/html/views/repair-table/table.php', [
	            'section' => $section,
	            'edit' => $edit,
	            'form' => $form,
	            'url' => Url::to(['protocol/get-row-repair'])
	        ]);
	    }
	    ?>
		</div>
	</div>
	<?php if($edit): ?>
		<div class="col-xs-12 title-with-icon">
            <div class="base-icon description-icon main-content-icons"></div>
            <h3><?php echo Yii::t('documents','Detailed repairs\' list') ?></h3>
        </div>
    <?php endif; ?>
	
    <?php
    if($edit || count($section->rowsReplacements)) {
    ?>
    <div class="col-xs-12">
		<div class="row">
		        <?php $edit ? $form->field($section, 'tableNameReplacements')->textInput()->label(false) : '<strong>'.$section->tableNameReplacements.'</strong>'?>
		        <?= $this->renderFile('@common/documents/html/views/single-table/table.php', 
		        		['section' => $section, 
		        				'name' =>'replacements', 
		        				'rows' => $section->rowsReplacements, 
		        				'edit' => $edit, 
		        				'formModel' => $section->formReplacements, 
		        				'form' => $form, 
		        				'url' => Url::to(['protocol/get-row-repair-replacements'])])?>
		    
		</div>
	</div>
    <?php } ?>
    
    <?php
    if($edit || count($section->rowsTests)) { ?>
    	<div class="col-xs-12">
    		<div class="row">
        		<?php $edit ? $form->field($section, 'tableNameTests')->textInput()->label(false) : '<strong>'.$section->tableNameTests.'</strong>' ?>
       			<?= $this->renderFile('@common/documents/html/views/single-table/table.php', 
       					['section' => $section, 
       							'name' =>'tests', 
       							'rows' => $section->rowsTests, 
       							'edit' => $edit, 
       							'formModel' => $section->formTests, 
       							'form' => $form, 
       							'url' => Url::to(['protocol/get-row-repair-tests'])])?>
       		</div>
       	</div>
    <?php } ?>
    
    <?php if($edit): ?>
	<div class="col-xs-12 title-with-icon">	
		<div class="base-icon description-icon main-content-icons"></div><h3><?= $section->getAttributeLabel('text') ?></h3>
	</div>

    <?php endif; ?>
    <div class="col-xs-12">
    	<div class="row">
	    	<div class="col-sm-7">
	        	<?= $edit ? $form->field($section, 'text')->textArea(['placeholder' => Yii::t('documents','Type additional text')])->label(false) :  nl2br($section->text)  ?>
	        </div>
        </div>
    </div>

    <?php
    if(!$edit && $section->remarks) {
        echo '<div>'.Yii::t('documents', 'Remarks').':</div>';
    }
    ?>
    <?php if($edit): ?>
        <div class="col-xs-12 title-with-icon">
        	<div class="base-icon description-icon main-content-icons"></div>
        	<h3><?= $section->getAttributeLabel('remarks') ?></h3>
        </div>
    <?php endif; ?>

    <div class="col-xs-12">
    	<div class="row">
	    	<div class="col-sm-7">
	        	<?= $edit ? $form->field($section, 'remarks')->textArea(['placeholder' => Yii::t('documents','Type remarks')])->label(false) : nl2br($section->remarks) ?>
	        </div>
        </div>
    </div>
    <div class="col-xs-12 text-center ">
    	<div class="row">
	        <div class="col-sm-6 page-break-inside">
	            <div><?= Yii::t('documents', 'CREATED BY') ?>
	            <br/>
	            ...................................................
	            <br/>
	            <?= (Yii::t('documents', 'Repairer - Contractor')) ?></div>
	        </div>
	        <div class="col-sm-6 page-break-inside">
	            <div><?= Yii::t('documents', 'ACCEPTED BY') ?>
	            <br/>
	            ....................................................
	            <br/>
	            <?= (Yii::t('documents', 'User')) ?></div>
	        </div>
        </div>
    </div>
</div>
