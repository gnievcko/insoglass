<?php 
    use yii\web\View;
    use yii\helpers\Url;
    use common\helpers\StringHelper;
    use yii\helpers\ArrayHelper;
    use yii\helpers\Html;

    $this->registerJsFile('@web/js/documents/order-confirmation-products.js?'.uniqid(), ['depends' => [\yii\web\JqueryAsset::className()]]);
    $currencySymbol = $section->getCurrencySymbol();
?>
<div class="order-confirmation-products">
<div class="col-xs-12">
    <div class="col-sm-3" id="order-confirmation-currency">
        <?= 
        $form->field($section, 'currencyId')
        ->dropDownList($section->getCurrencies(), ['class' => 'currency-select form-control'])
        ?>  
    </div>
</div>

<div class="col-xs-12">
<div class="table-responsive">
    <table class="table table-bordered grid-table order-confirmation-table">
        <thead>
            <tr>
                <th class="product-no">
                    <?= Yii::t('documents', 'No.') ?>
                </th>
                <th style="width: 327px">
                    <?= $section->getAttributeLabel('name') ?>
                </th>
                <th style="min-width: 110px">
                    <?= $section->getAttributeLabel('count') ?>
                </th>
                <th style="min-width: 90px">
                    <?= $section->getAttributeLabel('unit') ?>
                </th>
                <th style="min-width: 150px">
                    <?= $section->getAttributeLabel('netPrice') ?>
                </th>
                <th style="width: 90px">
                    <?= $section->getAttributeLabel('vatRate') ?>
                </th>
                <th style="width: 140px">
                    <?= $section->getAttributeLabel('grossValue') ?>
                </th>
                <th class="product-actions"></th>
            </tr>
        </thead>
        <tbody>
            <?php 
            $i = 0;
            foreach($section->products as $prefix => $product): ?>
                <?= Yii::$app->controller->renderFile(__DIR__.'/order-confirmation-products/product-row.php', [
                		'product' => $product, 
                		'prefix' => $i, 
                		'form' => $form, 
                		'currencySymbol' => $section->getCurrencySymbol(),
                ]) ?>
            <?php 
            $i++;
            endforeach ?>
        </tbody>
    </table>
</div>
<div class="row">
	<div class="col-xs-3">
	    <span class="btn btn-link add-order-confirmation-product-button">
	        <span class="glyphicon glyphicon-plus-sign"></span>
	        <span><?= Yii::t('web', 'Add product') ?></span>
	    </span>
    </div>
	<div class="table-responsive col-xs-6 col-xs-offset-2 order-confirmation-table-summary" style="text-align: center">
				<?= 
				Yii::$app->controller->renderFile(__DIR__.'/order-confirmation-products/products-summary-body.php', [
						'products' => $section->products,
				])
				?>
	</div>
</div>

</div>
</div>	
<?php
    $this->registerJs('
        (function() {
            initializeOrderConfirmationProductsTables({
				summaryUrl: "'.Url::to(['documents/order-confirmation/calculate-summary']).'",
                productRowUrl: "'.Url::to(['documents/order-confirmation/get-order-confirmation-product-row']).'",
                totalGrossPriceUrl: "'.Url::to(['documents/order-confirmation/total-gross-price']).'",
				initialCurrencySymbol: "'.$section->getCurrencySymbol().'"
            });
        })()
    ', View::POS_END);
?>
