<?php  
	use common\helpers\StringHelper;
?>

<tr class="offer-details-product">
    <td><?= $prefix + 1 ?></td>
    <td><?= $form->field($product, "[{$prefix}]name")->textarea(['class' => 'form-control smallTextarea', 'placeholder' => Yii::t('web', 'Name')])->label(false) ?></td>
    <td>
        <?= 
            $form->field($product, "[{$prefix}]price")
                ->textInput([
                        'class' => 'form-control offer-details-product-price', 
                        'type' => 'number', 
                        'step' => 0.01, 
                        'placeholder' => Yii::t('web', 'Price')

                ])
                ->label(false) 
        ?>
    </td>
    <td><?= $form->field($product, "[{$prefix}]unit")->textInput(['class' => 'form-control', 'placeholder' => Yii::t('documents', 'M.u.'), 'readonly' => !empty(Yii::$app->params['defaultUnit']) ? true : false])->label(false) ?></td>
    <td>
        <?= $form->field($product, "[{$prefix}]count")->textInput([
        		'class' => 'form-control offer-details-product-count', 
        		'type' => 'number', 
        		'step' => 1, 
        		'placeholder' => Yii::t('web', 'Count')])->label(false) ?>
    </td>
    <td class="offer-details-product-total-price">
        <?= StringHelper::getFormattedCost((float)$product->totalPrice) ?>
    </td>
    <td>
        <div class="base-icon delete-icon action-icon remove-offer-details-product-btn"></div>
    </td>
</tr>
