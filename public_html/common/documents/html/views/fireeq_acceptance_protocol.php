<?php

use yii\web\View;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use common\documents\forms\FireEqAcceptanceProductForm;

$editMode = $section->isEditable();
$this->title = $section->getAttributeLabel('title').' - '.Yii::$app->name;
$rowCount = count($section->rows);
?>

<div class="text-center">
        <div>
            <h3 class=""><?= $section->getAttributeLabel('title')?></h3> 
            <div class="document-date">
                <div><h4 class="">
                	<?= $section->getAttributeLabel('from')?>
                	<?= $editMode ? $form->field($section, 'dateOperation')->textInput()->label(false) : $section->dateOperation ?>
                </h4></div>
                
            </div>
        </div>
    </div>
	<div class="col-xs-12 row"> 
		<div class="col-sm-12"><label><?= $section->getAttributeLabel('onObject')?></label></div>
		<div class="col-sm-7"><?= $editMode ? 
				$form->field($section, 'companyName')->textInput(['placeHolder' => $section->getAttributeLabel('companyName')])->label(false) . 
					' ' . $form->field($section, 'companyAddress')->textInput(['placeHolder' => $section->getAttributeLabel('address')])->label(false) : 
				$section->companyName . ' ' . $section->companyAddress?>
		</div>
	</div>

<div class="col-xs-12 title-with-icon">
	<div class="base-icon description-icon main-content-icons"></div>
	<h3><?php echo Yii::t('documents','Description of individual inpsections')?></h3>
</div>

<div class="col-xs-12 row fireeq-acceptance-rows">
	<div class="col-xs-12 row">
	    <?php if($editMode): ?>
	   		<span class="add-row-button btn btn-link">
	            <span class="glyphicon glyphicon-plus-sign"></span>
	            <span><?= Yii::t('documents', 'Add description') ?></span>
	        </span>
	    <?php endif; ?>
    </div>
          
    <div id="rows" class="col-sm-7">
	    <?php
        
        foreach($section->rows as $key => $row) {
            $fire = new FireEqAcceptanceProductForm();
            $fire->load(["{$fire->formName()}" => $row]);
	    	 echo Yii::$app->controller->renderFile(__DIR__.'/fireeq-acceptance-protocol/product-row.php',[
	            		'editMode' => $editMode,
                        'row' => $fire,
	    				'form' => $form,
	    	 			'idx' => $key,
	    		]); 
		}?>
	</div>
    <div class="col-xs-12 row">
	<?php if($editMode): ?>
   		<span class="add-row-button btn btn-link">
            <span class="glyphicon glyphicon-plus-sign"></span>
            <span><?= Yii::t('documents', 'Add description') ?></span>
        </span>
    <?php endif; ?>
    </div>
</div>
<input type="hidden" name="rowCount" id="firegearRowCount" value="<?=$rowCount ?>"/>
<input type="hidden" name="nextInsert" id="nextInsert" value="<?=$rowCount ?>"/>

<div class="col-xs-12 row text-center">
	<div class="col-sm-6">
		<div><?= $section->getAttributeLabel('createdBy') ?></div>
		<br>
		<div>....................................................</div>
		<div><?= $section->getAttributeLabel('(Repairer - Contractor)') ?></div>
	</div>
	<div class="col-sm-6">
		<div><?= $section->getAttributeLabel('acceptedBy') ?></div>
		<br>
		<div>....................................................</div>
		<div><?= $section->getAttributeLabel('(user)') ?></div>
	</div>
</div>

<br><br>

<?php  $this->registerJs('
    (function() {
      	$(document).on("click",".add-fireeq-row-button", function (e) {
	  		e.preventdefault;
					
			$.get( "'.Url::to(["document/get-fire-eq-acceptance-product-row"]).'", { editMode: "' . $editMode . '", rowCount: $(".fireeq-acceptance-row").length, idx: $("#nextInsert").val()} )
  				.done(function( data ) {
					$("#rows").append( data );
					$("#firegearRowCount").val($(".fireeq-acceptance-row").size());
					$("#nextInsert").val(parseInt($("#nextInsert").val()) + 1);
  			});		

		});
    })();
    ', View::POS_END);
?>