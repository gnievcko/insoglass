<?php 
    use yii\web\View;
    use yii\helpers\Html;
    use yii\helpers\Url;

    $edit = $section->isEditable();

    $this->registerJsFile('@web/js/documents/invoice-products.js?'.uniqid(), ['depends' => [\yii\web\JqueryAsset::className()]]);
?>

<div class="invoice-products">
<div class="col-xs-12 row">
	<?php if(strpos(Yii::$app->requestedRoute, 'corrective-invoice') !== false) { ?>
		<h3><?= Yii::t('documents', 'Data after correction') ?></h3>
	<?php } ?>
	
	<div class="col-sm-7">
    <?php 
        echo $edit ? $form->field($section, 'currencyId')->dropDownList($section->getCurrencies(), ['class' => 'currency-select form-control']) 
                   : Html::activeHiddenInput($section, 'currencyId')
    ?>
    </div>
</div>
<div class="col-xs-12 row">
    <?php if($edit): ?>
        <!--  <span class="add-invoice-product-button">
            <span class="glyphicon glyphicon-plus-sign"></span>
            <span>-->
            <?php /*Yii::t('web', 'Add product') */?>
            <!--  </span>
        </span>-->
        <div class="btn btn-link add-item-button add-invoice-product-button" data-items-container-selector="#product-variants" data-item-view-url="<?= Url::to(['site/get-product-variant-form']) ?>">
			<span class="glyphicon glyphicon-plus-sign"></span>
	        <span><?= Yii::t('web', 'Add product') ?></span>
		</div>
    <?php endif; ?>
</div>
<div class="col-xs-12">
<div class="table-responsive">
    <table class="table table-bordered grid-table invoice-table">
        <thead>
            <tr>
                <th>
                    <?= $section->getAttributeLabel('no') ?>
                </th>
                <th style="width: 327px">
                    <?= $section->getAttributeLabel('name') ?>
                </th>
                <th style="width: 90px">
                    <?= $section->getAttributeLabel('count') ?>
                </th>
                <th style="width: 90px">
                    <?= $section->getAttributeLabel('unit') ?>
                </th>
                <th style="width: 90px">
                	<?= $section->getAttributeLabel('category') ?>
                </th>
                <th>
                    <?= $section->getAttributeLabel('netPrice') ?>
                </th>
                <th style="width: 140px">
                    <?= $section->getAttributeLabel('netValue') ?>
                </th>
                <th>
                    <?= $section->getAttributeLabel('vatRate') ?>
                </th>
                <th style="width: 130px">
                    <?= $section->getAttributeLabel('vatAmount') ?>
                </th>
                <th style="width: 135px">
                    <?= $section->getAttributeLabel('grossValue') ?>
                </th>
                <?php if($edit): ?>
                    <th><?php echo Yii::t('web','Actions')?></th>
                <?php endif; ?>
            </tr>
        </thead>
        <tbody>
            <?= Yii::$app->controller->renderFile(__DIR__.'/invoice-products/table-body.php', ['section' => $section] + (isset($form) ? ['form' => $form] : [])) ?>
        </tbody>
    </table>
    </div>
</div>
    <?php if($edit): ?>
        <div class="ajax-loader">
            <?= Html::img('@web/images/ajax-loader-big-circle.gif') ?>
        </div>
<div class="col-xs-12 row">
            <div class="col-sm-6 ">
            	
               <!-- <span class="add-invoice-product-button">
                    <span class="glyphicon glyphicon-plus-sign"></span>
                    <span> --> 
                    <?php //Yii::t('web', 'Add product') ?>
                  <!--   </span>
                </span> --> 
                <div class=""> 
	                <div class="btn btn-link add-item-button add-invoice-product-button" data-items-container-selector="#product-variants" data-item-view-url="<?= Url::to(['site/get-product-variant-form']) ?>">
						<span class="glyphicon glyphicon-plus-sign"></span>
				        <span><?= Yii::t('web', 'Add product') ?></span>
					</div>
				</div> 
            </div>
            <div class="col-sm-6 text-right">
                <span class="btn btn-link add-item-button remove-invoice-products-button remove-all-items-btn">
                    <span class="glyphicon glyphicon-minus-sign"></span>
                    <span><?= Yii::t('web', 'Remove all products') ?></span>
                </span>
            </div>
</div>
    <?php endif; ?>
<div class="col-xs-12 row">
    <div class="col-sm-7">
        <?= $edit ? $form->field($section, 'amountVerbally')->textInput(['readonly' => true]) : $section->getAttributeLabel('amountVerbally').': '.$section->amountVerbally ?>
    </div>
</div>
</div>

<?php
    $this->registerJs('
        (function() {
            initializeInvoiceProductsTables(
                "invoice-products", 
                {
                    calculationsUrl: "'.Url::to(['document/calculate-invoice-amounts']).'",
    				verbalCalculationsUrl: "'.Url::to(['document/calculate-invoice-amount-verbally']).'",
                    productRowUrl: "'.Url::to(['document/get-invoice-product-row']).'",
                    edit: '.(int)$edit.',
                    initialCurrencySymbol: "'.$section->getCurrencySymbol().'"
                }
            );
        })();
    ', View::POS_END);
?>
