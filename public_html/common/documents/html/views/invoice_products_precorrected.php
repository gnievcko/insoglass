<?php 
    use yii\web\View;
    use yii\helpers\Html;
    use yii\helpers\Url;

    $edit = false;

    $this->registerJsFile('@web/js/documents/invoice-products.js?'.uniqid(), ['depends' => [\yii\web\JqueryAsset::className()]]);
?>


<div class="invoice-products-precorrected">
	<div class="col-xs-12 row">
		<h3><?= Yii::t('documents', 'Data before correction') ?></h3>
		<div class="col-sm-7">
	    <?php 
	        echo Html::activeHiddenInput($section, 'currencyId');
	    ?>
	    </div>
	</div>
	<div class="col-xs-12">
		<div class="table-responsive">
		    <table class="table table-bordered grid-table invoice-table">
		        <thead>
		            <tr>
		                <th>
		                    <?= $section->getAttributeLabel('no') ?>
		                </th>
		                <th style="width: 327px">
		                    <?= $section->getAttributeLabel('name') ?>
		                </th>
		                <th style="width: 90px">
		                    <?= $section->getAttributeLabel('count') ?>
		                </th>
		                <th style="width: 90px">
		                    <?= $section->getAttributeLabel('unit') ?>
		                </th>
		                <th style="width: 90px">
		                	<?= $section->getAttributeLabel('category') ?>
		                </th>
		                <th>
		                    <?= $section->getAttributeLabel('netPrice') ?>
		                </th>
		                <th style="width: 140px">
		                    <?= $section->getAttributeLabel('netValue') ?>
		                </th>
		                <th>
		                    <?= $section->getAttributeLabel('vatRate') ?>
		                </th>
		                <th style="width: 130px">
		                    <?= $section->getAttributeLabel('vatAmount') ?>
		                </th>
		                <th style="width: 135px">
		                    <?= $section->getAttributeLabel('grossValue') ?>
		                </th>
		            </tr>
		        </thead>
		        <tbody>
		            <?= Yii::$app->controller->renderFile(__DIR__.'/invoice-products-precorrected/table-body.php', ['section' => $section] + (isset($form) ? ['form' => $form] : [])) ?>
		        </tbody>
		    </table>
	    </div>
	</div>
	<div class="col-xs-12 row">
    	<div class="col-sm-7">
        	<?= $section->getAttributeLabel('amountVerbally').': '.$section->amountVerbally ?>
    	</div>
	</div>
</div>

<?php
    $this->registerJs('
	        (function() {
	            initializeInvoiceProductsTables(
	                "invoice-products-precorrected", 
	                {
	                    calculationsUrl: "'.Url::to(['document/calculate-invoice-amounts']).'",
	    				verbalCalculationsUrl: "'.Url::to(['document/calculate-invoice-amount-verbally']).'",
	                    productRowUrl: "'.Url::to(['document/get-invoice-product-row']).'",
	                    edit: '.(int)$edit.',
	                    initialCurrencySymbol: "'.$section->getCurrencySymbol().'"
	                }
	            );
	        })();
    ', View::POS_END);
?>
