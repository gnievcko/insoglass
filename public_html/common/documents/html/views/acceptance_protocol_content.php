<?php 
    use kartik\datecontrol\DateControl;

    $edit = $section->isEditable();
?>

<div class="col-xs-12 row">
	<div class="col-sm-12"><label>1. <?= $section->getAttributeLabel('orderingParty') ?>:</label></div>
	<div class="col-sm-7"><?= $form->field($section, 'orderingParty')->label(false) ?></div>
</div>

<div class="col-xs-12 row">
	<div class="col-sm-12"><label>2. <?= $section->getAttributeLabel('object') ?>:</label></div>
	<div class="col-sm-7"><?= $form->field($section, 'object')->label(false) ?></div>
</div>

<div class="col-xs-12 row">
	<div class="col-sm-12"><label>3. <?= $section->getAttributeLabel('orderDescription') ?>:</label></div>
	<div class="col-sm-7"><?= $form->field($section, 'orderDescription')->textArea(['placeholder' => Yii::t('documents','Type description here')])->label(false) ?></div>
</div>

<div class="col-xs-12 row">
	<div class="col-sm-12"><label>4. <?= $section->getAttributeLabel('works') ?>:</label></div>
	<div class="col-sm-7"><?= $form->field($section, 'works')->textArea(['placeholder' => Yii::t('documents','Type work range')])->label(false) ?></div>
</div>

<div class="col-xs-12 row">
	<div class="col-sm-12"><label>5. <?= $section->getAttributeLabel('executor') ?>:</label></div>
	<div class="col-sm-7"><?= $form->field($section, 'executor')->label(false) ?></div>
</div>

<div class="col-xs-12 row">
	<div class="col-sm-12"><label>6. <?= Yii::t('documents', 'Supervising board consisting of') ?>:</label></div>
	<div class="col-xs-12" style="margin: 16px 0px">
		<div class="col-sm-6">
			<div>
	            <?= Yii::t('documents', 'Ordering party') ?>
	        </div>
	        <?php for($i = 0; $i < 3; ++$i): ?>
	            <div>
	                <?= $form->field($section, "orderingPartySupervisingBoard[{$i}]")->textInput(['placeholder' => Yii::t('documents','Type ordering person')])->label(false) ?>
	            </div>
	        <?php endfor ?>
		</div>
		<div class="col-sm-6">
	        <div>
	            <?= Yii::t('documents', 'Executor') ?>
	        </div>
	        <?php for($i = 0; $i < 3; ++$i): ?>
	            <div>
	                <?= $form->field($section, "executorSupervisingBoard[{$i}]")->textInput(['placeholder' => Yii::t('documents','Type executor')])->label(false) ?>
	            </div>
	        <?php endfor ?>
	    </div>
    </div>
</div>

<div class="col-xs-12 row">
	<div class="col-sm-7 row">
		<div class="col-sm-6">
		    <?= $form->field($section, 'worksBeginDate')->widget(DateControl::classname(), [
		            'language' => Yii::$app->language,
		            'type' => DateControl::FORMAT_DATE,
		            'ajaxConversion' => false,
		            'displayFormat' => 'php:d.m.Y',
		            'options' => [
		                'pluginOptions' => [
		                    'autoclose' => true,
		                ],
		            ]
		        ]);
		    ?>
		</div>
		<div class="col-sm-6">
		    <?= $form->field($section, 'worksEndDate')->widget(DateControl::classname(), [
		            'language' => Yii::$app->language,
		            'type' => DateControl::FORMAT_DATE,
		            'ajaxConversion' => false,
		            'displayFormat' => 'php:d.m.Y',
		            'options' => [
		                'pluginOptions' => [
		                    'autoclose' => true,
		                ],
		            ]
		        ]);
		    ?>
		</div>
	</div>
</div>
<div class="col-xs-12 row">
	<div class="col-sm-12">
	    <?php 
	    	//echo $form->field($section, 'wasExecutedInComplianceWithOrder')->checkbox(); 
	    	//echo $form->field($section, 'hasFlaws')->checkbox();
	    	
	    	echo $form->field($section, 'wasExecutedInComplianceWithOrder')->hiddenInput()->label(false);
	    	echo $form->field($section, 'hasFlaws')->hiddenInput()->label(false);
	    ?>
    </div>
    <div class="col-sm-7">
    	<?= $form->field($section, 'flawsDescription')->textArea(['placeholder' => Yii::t('documents','Type flaws description')]) ?>
    </div>
</div>
<div class="col-xs-12 row">
	<div class="col-sm-7 row">
	    <div class="col-sm-6">
	    	<?= $form->field($section, 'flawsRemovalDate')->widget(DateControl::classname(), [
	            'language' => Yii::$app->language,
	            'type' => DateControl::FORMAT_DATE,
	            'ajaxConversion' => false,
	            'displayFormat' => 'php:d.m.Y',
	            'options' => [
	                'pluginOptions' => [
	                    'autoclose' => true,
	                ],
	            ]
	        ]);
	    ?>
	    </div>
    </div>
</div>
<div class="col-xs-12 row">
	<div class="col-sm-12"><label>7. <?= $section->getAttributeLabel('remarks') ?>:</label></div>
	<div class="col-sm-7"><?= $form->field($section, 'remarks')->textArea(['placeholder' => Yii::t('documents','Type remarks')])->label(false) ?></div>
</div>
<?= $this->renderFile(__DIR__.'/acceptance-protocol/signatures.php'); ?>
