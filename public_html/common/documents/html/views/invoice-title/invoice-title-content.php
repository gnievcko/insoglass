<?php 
use kartik\datecontrol\DateControl;
use Mindseater\PHPUtils\Dates\Formatter;
use common\helpers\StringHelper;
use kartik\select2\Select2;
use common\models\aq\OrderTypeQuery;
use yii\web\JsExpression;
use yii\helpers\Url;
use yii\helpers\ArrayHelper;
use common\documents\sections\SectionsMapper;
?>

<div class="col-sm-7 col-xs-12">
	<div class="row">
		<div class="col-sm-6">
		    <?= 
		        $editMode
		            ? $form->field($section, 'dateOfIssue')->widget(DateControl::classname(), [
		                    'language' => \Yii::$app->language,
		                    'type' => DateControl::FORMAT_DATE,
		                    'ajaxConversion' => false,
		                    'displayFormat' => 'php:d.m.Y',
		                    'saveFormat' => 'php:Y-m-d',
		            		'disabled' => in_array($section->getType(), [SectionsMapper::INVOICE_TITLE]) && !empty(Yii::$app->request->get('edit')),
		                    'options' => [
		                        'pluginOptions' => [
		                            'autoclose' => true
		                        ]
		                    ]
		                ])
		            : $section->getAttributeLabel('dateOfIssue').': '.(new Formatter())->from($section->dateOfIssue, 'Y-m-d')->format('d.m.Y')->getDate()
		    ?>
		</div>
		<div class="col-sm-6">
		    <?= 
		        $editMode 
		            ? $form->field($section, 'dateOfSale')->widget(DateControl::classname(), [
		                    'language' => \Yii::$app->language,
		                    'type' => DateControl::FORMAT_DATE,
		                    'ajaxConversion' => false,
		                    'displayFormat' => 'php:d.m.Y',
		                    'saveFormat' => 'php:Y-m-d',
		                    'options' => [
		                        'pluginOptions' => [
		                            'autoclose' => true
		                        ]
		                    ]
		                ]) 
		            : $section->getAttributeLabel('dateOfSale').': '.(new Formatter())->from($section->dateOfSale, 'Y-m-d')->format('d.m.Y')->getDate().'<br/><br/>'
		    ?>
		</div>
	</div>
</div>
<div class="col-sm-12">
	<div class="row">
	    <div class="col-sm-7">
	        <!--  <span><?php //Yii::t('documents', 'Seller') ?></span>-->
	        <span><?= $section->sellerName ?></span><br />
	        <span><?= $section->sellerAddress ?></span><br />
	        <span><?= Yii::t('documents', 'VAT ID') ?>: <?= $section->sellerVatId ?></span>
	    </div>
    </div>
</div>
<div class="col-sm-12" style="margin-top:32px">
	<div class="row">
	    <div class="col-sm-7">
	        <span><?= Yii::t('documents', 'Buyer') ?></span><br />
	        <span>
	            <?= $editMode ? $form->field($section, 'buyerName')->textInput(['placeholder' => $section->getAttributeLabel('buyerName')])->label(false) : $section->buyerName ?>
	        </span><br />
	        <span>
	            <?= 
	                $editMode 
	                    ? $form->field($section, 'buyerAddress')->textInput(['placeholder' => $section->getAttributeLabel('buyerAddress')])->label(false) 
	                    : $section->buyerAddress 
	            ?>
	        </span>
	        <span>
	            <?= $editMode ? $form->field($section, 'buyerVatId')->textInput(['placeholder' => Yii::t('documents','Type VAT Id')]) : Yii::t('documents', 'VAT ID').': '.$section->buyerVatId ?>
	        </span>
	        <?php if(Yii::$app->params['isContractTypeVisible']) { ?>
		        <span><?= Yii::t('web', 'Type of contract') ?></span><br />
		        <span>
		            <?= $form->field($section, 'contractTypeId')->dropDownList($section->contractTypes, ['prompt' => Yii::t('web','Choose type of contract')])->label(false); ?>
		        </span>
	        <?php } ?>
	        <?php if(Yii::$app->params['isOrderTypeVisible']) { ?>
		        <span><?= StringHelper::translateOrderToOffer('web', 'Order type') ?></span><br />
		        <span>        	
		            <?= $form->field($section, 'orderTypeIds')->widget(Select2::classname(), [
			                    'language' => Yii::$app->language,
			                    'showToggleAll' => false,
			                    'options' => ['placeholder' => StringHelper::translateOrderToOffer('web', 'Choose type of the order'), 'class' => 'form-control'],
			                    'data' => ArrayHelper::map(OrderTypeQuery::getOrderTypesByIds($section->orderTypeIds), 'id', 'name'),
			                    'pluginOptions' => [
			                            'multiple' => true,
			                            'allowClear' => true,
			                            'minimumInputLength' => 0,
			                            'ajax' => [
			                                    'url' => Url::to(['order-type/find']),
			                                    'dataType' => 'json',
			                                    'delay' => 250,
			                                    'data' => new JsExpression('function(params) { 
			                                        return {term:params.term, page:params.page, size: 20}; 
			                                    }'),
			                                    'processResults' => new JsExpression('function (data, params) { 
			                                        params.page = params.page || 1; 
			                                        
			                                        return {
			                                            results: data.items,
			                                            pagination: {
			                                                more: data.more
			                                            }
			                                        };
			                                    }'),
			                                    'cache' => true
			                            ],
			                    		'templateResult' => new JsExpression('function(item) { if(item.loading) return item.text; return item.name; }'),
			                    		'templateSelection' => new JsExpression('function (item) { return item.name || item.text; }'),
			                    ],
			        		'pluginEvents' => [ ],
						])->label(false); 
			        ?>
				</span>
			<?php }
			else { ?>
				<?php $form->field($section, 'orderTypeIds')->hiddenInput()->label(false) ?>
			<?php } ?>
	        
	    </div>
    </div>
</div>
