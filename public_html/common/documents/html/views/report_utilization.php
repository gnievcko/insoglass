<?php 
use common\helpers\StringHelper;
$this->title = $section->getAttributeLabel('report-utilizations').' - '.Yii::$app->name;
?>

<div class="text-center"><h1><?= $section->getAttributeLabel('report-utilizations') ?></h1></div>

<div class="text-center">
	<?= $section->getAttributeLabel('forPeriod') . ': ' .  
	$section->dateFrom . ' - ' .
	$section->dateTo ?>
</div>

<div class="text-center"><?= $section->getAttributeLabel('generatedFor') . ': ' . $section->user ?></div>

<div class="text-center"><?= $section->getAttributeLabel('generationDate') . ': ' . $section->dateGenerated ?></div>

<br><br>

<h3><?= $section->getAttributeLabel('utilizationsByProduct') ?></h3>

<?php if(!empty($section->countByProduct)) {?>
	<div class="table-responsive">
		<table class="table">
			<tr>
		    	<th><?= $section->getAttributeLabel('product') ?></th>
		    	<th><?= $section->getAttributeLabel('count') ?></td>
		  	</tr>
			<?php foreach($section->countByProduct as $row) {?>
		  		<tr>
		    		<td><?= $row['name'] ?></th>
		    		<td><?= $row['count'] ?></td>
		  		</tr>
		  	<?php }?>
		</table>
	</div>
<?php }
else {
	echo $section->getAttributeLabel('none');
}?>

<h3><?= $section->getAttributeLabel('utilizationOperations') ?></h3>

<?php if(!empty($section->history)) {?>
	<div class="table-responsive">
		<table class="table">
		  	<tr>
			    <th><?= $section->getAttributeLabel('product') ?></th>
			    <th><?= $section->getAttributeLabel('count') ?></th>
			    <th><?= $section->getAttributeLabel('user') ?></th>
			    <th><?= $section->getAttributeLabel('userReceiving') ?></th>
			    <th><?= $section->getAttributeLabel('description') ?></th>
			    <th><?= $section->getAttributeLabel('dateCreation') ?></th>
		  	</tr>
			<?php foreach($section->history as $row) {?>
		  		<tr>
		    		<td width="20%"><?= $row['name'] ?></td>
		    		<td align="center" width="10%"><?= $row['count']?></td>
		    		<td width="20%"><?= $row['user'] ?></td>
		    		<td width="20%"><?= $row['userReceiving'] ?></td>
		    		<td width="20%"><?= $row['description'] ?></td>
		    		<td width="10%"><?= $row['date_creation'] ?></td>
		  		</tr>
		  	<?php }?>
		</table>
	</div>
<?php }
else {
	echo $section->getAttributeLabel('none');
}?>