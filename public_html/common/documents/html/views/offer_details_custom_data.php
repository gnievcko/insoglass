<?php
use yii\helpers\Url;
use yii\helpers\Html;
use yii\web\View;
use frontend\models\OrderTableForm;
use common\documents\sections\OfferDetailsCustomData;
	
$edit = $section->isEditable();
$this->registerJsFile('@web/js/documents/addTable.js?'.uniqid(), ['depends' => [\yii\web\JqueryAsset::className()]]);
$this->registerJsFile('@web/js/documents/tables.js?'.uniqid(), ['depends' => [\yii\jui\JuiAsset::className()]]);
$this->registerJsFile('@web/js/order-form.js?'.uniqid(), ['depends' => [\yii\web\JqueryAsset::className()]]);
?>

<div class="order-table-form">
	<?php if(Yii::$app->params['isOrderCustomDataVisible']): ?>
	<?php if($edit): ?>
		<div class="col-xs-12 title-with-icon">
			<div class="base-icon table-icon main-content-icons"></div>
			<h3><?= Yii::t('web', 'Additional data') ?></h3>
		</div>
		<div class="col-xs-12">
			<div class="row">
				<div class="col-sm-2">
					<a class="btn add-table-button btn-default ">
						<span><?= Yii::t('documents', 'Add table') ?></span>
					</a>
					<?= $form->field($section, 'isVisible')->hiddenInput()->label(false) ?>
				</div>
				<div class="col-sm-4 column-count-block">
					<div class="inline-block"><label><?= Yii::t('web', 'Number of additional columns in a table') ?></label></div>
					<div class="inline-block column-number"><?php
						echo $form->field($section, 'columnCount')->dropDownList(array_combine(range(5, 7, 1), range(1, 3, 1)), 
								['class' => 'form-control th-count'])->label(false);
					?></div>
				</div>
				<div class="col-sm-6">
					<a class="btn remove-single-table-button btn-default pull-right" disabled="disabled">
						<span><?= Yii::t('documents', 'Remove table') ?></span>
					</a>
				</div>
			</div> 
		</div>
	<?php endif; ?>
		
		<div class="custom-tables col-sm-12"><?php
			if(empty($section->tables)) {
				$section->tables = OfferDetailsCustomData::EMPTY_DATA;
			}
			
			if(!OrderTableForm::isCustomDataEmpty($section->tables)) {
				foreach($section->tables as $table) {
					if(count($table['rows']) > 0) {
						echo $this->renderFile('@common/documents/html/views/offer-details-custom-data/table.php', ['table' => $table, 'edit' => $edit]);
					}
				}
			}
		?></div>
		<?php if($edit): ?>
			<div class="ajax-loader">
				<?php echo Html::img('@web/images/ajax-loader.gif') ?>
			</div>
		<?php endif; ?>
	<?php else: ?>
		<?= $form->field($section, 'isVisible')->hiddenInput()->label(false) ?>
		<?= $form->field($section, 'columnCount')->hiddenInput()->label(false) ?>
	<?php endif; ?>
</div>

<?php
    $this->registerJs('
				initializeAddTable("custom-tables", {
						rowUrl: "' . Url::to(['protocol/get-table']) . '",
	    				edit: "' . $edit . '",
	                    sectionClass: "order-table-form",
    					limitToSingle: true,
    					tableViewsDir: "'.'@common/documents/html/views/offer-details-custom-data'.'",
		        });
    		
    			OrderForm.initRecalculateCustomData("'.Url::to(['order/get-custom-data-summary']).'", "'.Url::to(['order/get-custom-data-numeration']).'", "custom-tables");
	        ', View::POS_END);
?>