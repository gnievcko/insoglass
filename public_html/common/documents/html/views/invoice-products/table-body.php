<?php
    use common\helpers\StringHelper;

    $edit = $section->isEditable();
    $currencySymbol = $section->getCurrencySymbol();
?>

<?php 
$i = 0;
foreach($section->products as $idx => $product): ?>
    <?= 
        Yii::$app->controller->renderFile(__DIR__.'/product-row.php', [
            'idx' => $i, 
            'product' => $product, 
            'edit' => $edit, 
            'currencySymbol' => $currencySymbol,
        ] + (isset($form) ? ['form' => $form] : [])) 
    ?>
<?php 
$i++;
endforeach; ?>
<?php 
    $summary = $section->calculateSummary();
    $total = $summary['total'];
    foreach($summary['vats'] as $vat): 
?>
    <tr>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td >
            <?= StringHelper::getFormattedCost($vat['netValue']).' '.$currencySymbol ?>
        </td>
        <td><?= $vat['vatLabel'] ?></td>
        <td>
            <?= StringHelper::getFormattedCost($vat['vatAmount']).' '.$currencySymbol ?>
        </td>
        <td <?php if($edit) echo 'colspan="2"' ?>>
            <?= StringHelper::getFormattedCost($vat['grossValue']).' '.$currencySymbol ?>
        </td>
    </tr>
<?php endforeach; ?>
<tr>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td colspan="2"><?= Yii::t('documents', 'Total') ?>:</td>
    <td>
        <?= StringHelper::getFormattedCost($total['netValue']).' '.$currencySymbol ?>
    </td>
    <td> - </td>
    <td>
        <?= StringHelper::getFormattedCost($total['vatAmount']).' '.$currencySymbol ?>
    </td>
    <td id="total-gross-value" <?php if($edit) echo 'colspan="2"' ?>>
        <?= StringHelper::getFormattedCost($total['grossValue']).' '.$currencySymbol ?>
    </td>
</tr>
