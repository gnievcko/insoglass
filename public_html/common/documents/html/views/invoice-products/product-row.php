<?php
    use common\helpers\StringHelper;

    $netValue = bcmul($product->price, $product->count, 2);
    $vatAmount = bcmul($netValue, $product->vatRate, 2);
    $grossValue = bcadd($netValue, $vatAmount, 2);

    $recalculateInputsOptions = ['class' => 'recalculates form-control'];
    $recalculateVatRateInputsOptions = ['class' => 'recalculates form-control vat-rate-select'];
    $numericFieldsCommonOptions = ['type' => 'number'];
    $areaOptions = ['class' => 'form-control smallTexarea invoice-product-name'];
?>

<tr class="invoice-product">
    <td><?= $idx + 1 ?></td>
    <td><?= $edit ? $form->field($product, "[{$idx}]name")->textArea($areaOptions,['placeholder' => Yii::t('documents','Type name')])->label(false) : $product->name ?></td>
    <td><?= $edit ? $form->field($product, "[{$idx}]count")->textInput($recalculateInputsOptions + $numericFieldsCommonOptions)->label(false) : $product->count ?></td>
    <td><?= $edit ? $form->field($product, "[{$idx}]unit")->textInput(['readonly' => !empty(Yii::$app->params['defaultUnit']) ? true : false])->label(false) : $product->unit ?></td>
    <td><?= $edit ? $form->field($product, "[{$idx}]category")->label(false) : $product->category ?></td>
    <td>
        <?= 
            $edit 
                ? $form->field($product, "[{$idx}]price")->textInput($recalculateInputsOptions + $numericFieldsCommonOptions + ['step' => '0.01'])->label(false) 
                : StringHelper::getFormattedCost($product->price).' '.$currencySymbol 
        ?>
    </td>
    <td><?= StringHelper::getFormattedCost($netValue).' '.$currencySymbol ?></td>
    <td><?= $edit 
                ? ($form->field($product, "[{$idx}]vatRate")->dropDownList($product->getVatRates(), $recalculateVatRateInputsOptions)->label(false))
                : (
                        ($product->vatRate === '') ? Yii::t('documents', 'Vat-free') :
                        (($product->vatRate === 'NP') ? Yii::t('documents', 'Not applicable') : bcmul($product->vatRate, 100, 0).' %')
                        
                 )
        ?>
    </td>

    <td><?= StringHelper::getFormattedCost($vatAmount).' '.$currencySymbol ?></td>
    <td><?= StringHelper::getFormattedCost($grossValue).' '.$currencySymbol ?></td>
    <?php if($edit): ?>
        <td>
        	<div class="base-icon copy-icon action-icon copy-item-btn copy-invoice-product-btn"></div>
        	<div class="base-icon garbage-icon action-icon remove-item-btn remove-invoice-product-btn"></div>
        </td>
    <?php endif; ?>
</tr>
