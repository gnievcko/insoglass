<?php

use yii\helpers\Url;

$edit = $section->isEditable();
$this->title = \Yii::t('documents', 'Protocol of light intensity measurement').' - '.Yii::$app->name;
?>
<div class="light_measurement">
    <div class="text-center">
        <div class="title-with-subtitle">
            <h3 class="upper"><?= \Yii::t('documents', 'Protocol of light intensity measurement') ?></h3> 
            <div class="document-date">
                <div><h4 class="mt-0"><?= $section->getAttributeLabel('date').(!$edit ? $section->date : '') ?></h4></div>
                <?= $edit ? $form->field($section, 'date')->textInput()->label(false) : '' ?>
            </div>
        </div>
    </div>
 <div class="col-xs-12 row">  
    <div class="col-sm-2"><label><?= $section->getAttributeLabel('object') ?> <?= !$edit ? $section->object : '' ?></label></div>
    <div class="col-sm-5"><?= $edit ? $form->field($section, 'object')->textInput()->label(false) : '' ?></div>
</div>
 <div class="col-xs-12 row"> 
    <div class="col-sm-2"><label><?= ($edit || !empty($section->repairer)) ? $section->getAttributeLabel('repairer').':' : '' ?> <?= !$edit ? $section->repairer : '' ?></label></div>
    <div class="col-sm-5"><?= $edit ? $form->field($section, 'repairer')->textInput()->label(false) : '' ?></div>
   </div>
 <div class="col-xs-12 row"> 
    <div class="col-sm-2"><label><?= ($edit || !empty($section->measurementTool)) ? $section->getAttributeLabel('measurementTool').':' : '' ?> <?= !$edit ? $section->measurementTool : '' ?></label></div>
    <div class="col-sm-5"><?= $edit ? $form->field($section, 'measurementTool')->textInput()->label(false) : '' ?></div>
</div> 
    <?php if($edit): ?>
    <?php endif; ?>

    <?php
    if($edit || count($section->rows)) {
        echo $this->renderFile('@common/documents/html/views/single-table/table.php', ['section' => $section, 'name' => 'light', 'rows' => $section->rows, 'formModel' => $section->form, 'edit' => $edit, 'form' => $form, 'url' => Url::to(['protocol/get-row-light'])]);
    }
    ?>
    <div class="col-xs-12 row">
    	<div class="col-sm-7">
        	<?= $edit ? $form->field($section, 'preText')->textArea() : nl2br($section->preText) ?>
        </div>
    </div>
    <div class="col-xs-12 row">
    	<div class="col-sm-7">
    		<strong><?= !$edit ? $section->getAttributeLabel('lightIntensityFormula') : '' ?></strong></div>
    </div>
    <div class="col-xs-12 row">
    	<div class="col-sm-7">
        	<?= $edit ? $form->field($section, 'lightIntensityFormula')->textArea() : nl2br($section->lightIntensityFormula) ?>
        </div>
    </div>
    <div class="col-xs-12 row">
    	<div class="col-sm-7">
    		<strong><?= !$edit ? $section->getAttributeLabel('lightUniformityFormula') : '' ?></strong></div>
    </div>
    <div class="col-xs-12 row">
    	<div class="col-sm-7">
        	<?= $edit ? $form->field($section, 'lightUniformityFormula')->textArea() : nl2br($section->lightUniformityFormula) ?>
        </div>
    </div>

    <div class="col-xs-12 row">
    	<div class="col-sm-4">
    		<?= ($edit || !empty($section->time)) ? $section->getAttributeLabel('time').':' : '' ?> <?= !$edit ? $section->time : '' ?>
    		<?= $edit ? $form->field($section, 'time')->textInput()->label(false) : '' ?>
    	</div>
    </div>
    <div class="col-xs-12 row">
    	<div class="col-sm-4">
			<?= ($edit || !empty($section->testDate)) ? $section->getAttributeLabel('testDate').':' : '' ?> <?= !$edit ? $section->testDate : '' ?>
    		<?= $edit ? $form->field($section, 'testDate')->textInput()->label(false) : '' ?>
    	</div>
   	</div>
	<div class="col-xs-12 row">
	    <div class="col-sm-4">
			<?= ($edit || !empty($section->measurementDate)) ? $section->getAttributeLabel('measurementDate').':' : '' ?> <?= !$edit ? $section->measurementDate : '' ?>
	   		<?= $edit ? $form->field($section, 'measurementDate')->textInput()->label(false) : '' ?>
	   	</div>
	</div>


    <?php if($edit): ?>
        <div class="col-xs-12"><?= Yii::t('documents', 'Protocol remarks') ?></div>
    <?php endif; ?>
    <?php
    if(!$edit && $section->text) {
        echo '<div><strong>'.Yii::t('documents', 'Remarks').':</strong></div>';
    }
    ?>
    <div class="col-xs-12 row">
    	<div class="col-sm-7">
    		<?= $edit ? $form->field($section, 'text')->textArea()->label(false) : nl2br($section->text) ?>
    	</div>
        
    </div>
    <br/><br/><br/>
    <div class="row text-center">
        <div class="col-sm-6">
            <div><?= Yii::t('documents', 'CREATED BY') ?></div>
            <br>
            <div>....................................................</div>
            <div><?= (Yii::t('documents', 'Repairer - Contractor')) ?></div>
        </div>
        <div class="col-sm-6">
            <div><?= Yii::t('documents', 'ACCEPTED BY') ?></div>
            <br>
            <div>....................................................</div>
            <div><?= (Yii::t('documents', 'User')) ?></div>
        </div>
    </div>
</div>