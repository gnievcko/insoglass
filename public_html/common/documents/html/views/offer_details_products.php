<?php 
    use yii\web\View;
    use yii\helpers\Url;
    use common\helpers\StringHelper;
    use yii\helpers\ArrayHelper;

    $this->registerJsFile('@web/js/documents/offer-details-products.js?'.uniqid(), ['depends' => [\yii\web\JqueryAsset::className()]]);
?>

<div class="col-xs-12 row">
    <div class="col-sm-3" id="offer-details-currency">
        <?= 
            $form->field($section, 'currencyId')
                ->dropDownList(ArrayHelper::map($section->currencies, 'id', 'short_symbol'))
        ?>  
    </div>
</div>

<div class="offer-details-products col-xs-12">
	<div class="hint-block">
		<?= Yii::t('documents_orders', 'The table with products can be displayed in another way in a resulting PDF file, regarding available data. If sum of cost is equal to 0, the summary row will not be visible.') ?>
	</div>
	<div class="table-responsive">
    <table class="table table-bordered grid-table invoice-table">
        <thead>
            <tr>
                <th class="product-no">
                    <?= Yii::t('documents', 'No.') ?>
                </th>
                <th class="product-assortment">
                    <?= $section->getAttributeLabel('assortment') ?>
                </th>
                <th class="product-unit-price">
                    <?= $section->getAttributeLabel('priceUnit') ?>
                </th>
                <th class="product-unit">
                    <?= $section->getAttributeLabel('unit') ?>
                </th>
                <th class="product-count">
                    <?= $section->getAttributeLabel('count') ?>
                </th>
                <th class="product-total-price">
                    <?= $section->getAttributeLabel('priceTotal') ?>
                    [<span><?= $section->currency->symbol ?></span>]
                </th>
                <th class="product-actions"></th>
            </tr>
        </thead>
        <tbody>
            <?php 
            $i = 0;
            foreach($section->products as $prefix => $product): ?>
                <?= Yii::$app->controller->renderFile(__DIR__.'/offer-details-products/product-row.php', [
                		'product' => $product, 
                		'prefix' => $i, 
                		'form' => $form, 
                		'currencies' => $section->getCurrencies(),
                ]) ?>
            <?php 
            $i++;
            endforeach ?>
        </tbody>
        <tfoot>
            <tr>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td><?= Yii::t('web', 'Summary') ?></td>
                <td class="summary-cost text-center"><?= StringHelper::getFormattedCost($section->summaryCost).' '.$section->currency->symbol ?></td>
                <td></td>
            </tr>
        </tfoot>
    </table>
</div>
    <span class="btn btn-link add-offer-details-product-button">
        <span class="glyphicon glyphicon-plus-sign"></span>
        <span><?= Yii::t('web', 'Add product') ?></span>
    </span>
</div>


<?php
    $this->registerJs('
        (function() {
            initializeOfferDetailsProductsTables({
                summaryUrl: "'.Url::to(['documents/offer-details/calculate-summary']).'",
                productRowUrl: "'.Url::to(['documents/offer-details/get-offer-details-product-row']).'",
                currencySelector: "#offer-details-currency select:first",
                totalGrossPriceUrl: "'.Url::to(['documents/offer-details/total-gross-price']).'"
            });
        })()
    ', View::POS_END);
?>
