<?php 
use kartik\datecontrol\DateControl;
use Mindseater\PHPUtils\Dates\Formatter;

$edit = $section->isEditable();
$dateFormatter = new Formatter();
    
$this->title = Yii::t('documents', 'Work acceptance protocol').' - '.Yii::$app->name;
?>

<div class="text-center">
    <h3><?= Yii::t('documents', 'Work acceptance protocol') ?></h3>
    <div class="row">
    	<div class="col-sm-offset-4 col-sm-1"><h4><?php echo Yii::t('documents', 'from date')?>:</h4></div>  
    	<div class="col-sm-3">
	        <?php if($edit): ?>
	            <?= /*$form->field($section, 'date')->widget(DateControl::classname(), [
	                    'language' => Yii::$app->language,
	                    'type' => DateControl::FORMAT_DATE,
	                    'ajaxConversion' => false,
	                    'displayFormat' => 'php:d.m.Y',
	                    'options' => [
	                        'pluginOptions' => [
	                            'autoclose' => true,
	                        ]
	                    ]
	                ])->label(false);*/
					$form->field($section, 'date')->textInput()->label(false)
	            ?>
	       	<?php else: ?>
		           <?= $section->getAttributeLabel('date') ?>: <?= $dateFormatter->from($section->date, 'Y-m-d')->format('d.m.Y')->getDate() ?>
		    <?php endif ?>
	    </div>
	</div> 
</div>
