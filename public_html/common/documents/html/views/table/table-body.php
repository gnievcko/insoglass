<?php

use yii\bootstrap\Html;
use common\documents\sections\ServiceProtocol;
use common\helpers\Utility;

$option = ['class' => 'form-control xSmallTextarea rows-sensitive', 'placeHolder' => Yii::t('documents', 'Write thead name'), 'rows' => 2];
?> 
<?php if($edit): ?>
	<div class="col-xs-12">
		<div class="row">
		    <div class="service-order-table-bg">
		    	<div class="col-xs-6">
		    		<div class="row">
		    			<div class="col-xs-12">
			        		<?= Yii::t('documents', 'Table name'); ?>
			        	</div>
			        	<div class="col-xs-11">
				        	<?= Html::input('text', 'tables['.$uniq.'][name]', !empty($table['name']) ? $table['name'] : '', ['class' => 'form-control', 'placeholder'=>Yii::t('documents','Type table name')]) ?>
				        </div>
				        <div class="col-xs-1">
				        	<div class="base-icon garbage-icon action-icon remove-table remove-table-icon"></div>
				        </div>
		        	</div>
		        </div>
		        <div class="col-xs-5 pull-right">
		        	<?= Yii::t('documents', 'Select table copy option'); ?>
		        	<div class="select-group">
			        		<?= Html::dropDownList('dropdown-table-copy-mode', '', [
			        				Utility::PROTOCOL_COPY_TABLE_WITHOUT_MODIFICATION => Yii::t('documents', 'Without modification'),
			        				Utility::PROTOCOL_COPY_TABLE_WITH_ADDITION_ONE_COLUMN => Yii::t('documents', 'With addition of one column'),
			        				Utility::PROTOCOL_COPY_TABLE_WITH_REMOVING_ONE_COLUMN => Yii::t('documents', 'With removing one column')
			        		], ['class' => 'form-control']) ?>
							<?= Html::button(Yii::t('web', 'Copy'), ['class' => 'btn btn-default btn-small copy-table-button']) ?>
					</div>
		        </div>
		    </div>
		</div>
	</div>
<?php endif; ?>
<?php if(!$edit) : ?>
    <strong><?= $table['name'] ?></strong>
<?php endif; ?>
<div class="col-xs-12">
    <table class="table page-break-inside">
        <thead>
            <tr>
            	<?php $i = 0;
                foreach($table['theads'] as $key => $th) {
                	$style = '';
                	if($i == 0) {
                		$style = ' style="width: 86px"';
                	}
					echo '<th'.$style.'>'.($edit ? Html::textarea('tables['.$uniq.'][theads]['.$key.']', (isset($table['theads'][$key]) ? $table['theads'][$key] : ''), $option) : ($table['theads'][$key] ?: '-')).'</td>';
					$i++;
                }
                ?>
                <?php if($edit): ?>
                    <th></th>
                <?php endif; ?>
            </tr>
        </thead>
        <tbody>
        	<tr>
        		<?php foreach($table['theads'] as $key => $th) { ?>
        			<th class="clear-data-column"><span class="glyphicon glyphicon-erase clear-data-column-btn"></span></th>
        		<?php } ?>
        	</tr>
        
            <?php
            $i = 0;
            foreach($table['rows'] as $idx => $row): ?>
                <?=
                Yii::$app->controller->renderFile(__DIR__.'/row.php', [
                    'table' => $table,
                    'idx' => $i,
                    'row' => $row,
                    'edit' => $edit,
                    'uniq' => $uniq,
                    'formModel' => new ServiceProtocol(),
                ]);
                ?>
            <?php 
                $i++;
            endforeach; ?>
        </tbody>
    </table>
</div>