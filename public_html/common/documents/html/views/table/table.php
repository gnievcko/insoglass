<?php

use yii\bootstrap\Html;
use yii\helpers\Url;

$this->registerJsFile('@web/js/documents/tables.js?'.uniqid(), ['depends' => [\yii\jui\JuiAsset::className()]]);

$uniq = uniqid();
?>
<div class="document-tables-<?= $uniq ?> document-table" >
    
    <?= Yii::$app->controller->renderFile(__DIR__.'/table-body.php', ['table' => $table, 'uniq' => $uniq, 'edit' => $edit]) ?>

    <?php if($edit): ?>
        <div class="ajax-loader text-center">
            <?php echo Html::img('@web/images/ajax-loader.gif') ?>
        </div>
    <?php endif; ?>

    <?php if($edit): ?>
    <div class="row">
    	<div class="col-xs-9">
	        <span class="add-row-button btn btn-link">
	            <span class="glyphicon glyphicon-plus-sign"></span>
	            <span><?= Yii::t('web', 'Add row') ?></span>
	        </span>
	        <?= Html::input('number', 'input-table-add-row-number', 1, ['class' => 'form-control', 'min' => 1, 'style' => [
	        		'width' => '9%', 'display' => 'inline'
	        ]]) ?>
		</div>
		<div class="col-xs-2">
        	<?= Html::button(Yii::t('web', 'Fill numbering'), ['class' => 'btn btn-default btn-small  pull-right fill-numbering-table-button']) ?>
        </div>
    </div>
    <?php endif; ?>
</div>
<?php
$script = '
        (function() {
            initializeTables(
                "document-tables-'.$uniq.'",
                {
                    rowUrl: "'.Url::to(['protocol/get-row']).'",
                    uniq: "'.$uniq.'",
                    edit: '.(int) $edit.',
                    type: "",
                    count: '.count($table['theads']).',
    				startIdx: '.(isset($table['rows']) ? count($table['rows']) : 0).'
                }
            );
        })();
    ';
if(isset($showScript) && $showScript) {
    echo '<script>'.$script.'</script>';
} else {
    $this->registerJs($script);
}
?>
