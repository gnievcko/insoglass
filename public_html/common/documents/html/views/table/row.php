<?php
    use yii\bootstrap\Html;
    
    $option = ['class' => 'form-control xSmallTextarea rows-sensitive', 'placeHolder' => Yii::t('documents', 'Write value'), 'rows' => 4];
 ?>

<tr class="document-table-row">
    <?php
    	if(isset($initData) && !empty($initData)) {
    		foreach($initData as $key => $v) {
    			echo '<td>'.($edit ? Html::textarea('tables['.$uniq.'][rows]['.$idx.']['.$key.']', !empty($v) ? $v : '', $option) : ($v ?: '-')).'</td>';
    		}	
    	}
    	else {
    		foreach($table['theads'] as $key => $th) {
    			echo '<td>'.($edit ? Html::textarea('tables['.$uniq.'][rows]['.$idx.']['.$key.']', !empty($row[$key]) ? $row[$key] : '', $option) : ($row[$key] ?: '-')).'</td>';
    		}
    	}
    ?>
    <?php if($edit): ?>
        <td style="width: 72px">
        	<div class="base-icon copy-icon action-icon copy-row-btn"></div>
            <div class="base-icon garbage-icon action-icon remove-row-btn"></div>
        </td>
    <?php endif; ?>
</tr>
