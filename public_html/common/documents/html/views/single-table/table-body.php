<?php

$edit = $section->isEditable();
?>

	<div class="table-responsive">
		<table class="table table-bordered grid-table">
		    <thead>
		        <tr>
		            <?php if(!$edit && !$formModel->hasIndex()) echo '<th>'.Yii::t('documents', 'Id').'</th>'; ?>
		            <?php foreach($formModel->getAttributes() as $name => $val) : ?>
		                <th>
		                    <?= $formModel->getAttributeLabel($name) ?>
		                    <span class="glyphicon glyphicon-erase clear-data-column-btn pull-right" data-name="<?= $formModel->formName() ?>"></span>
		                </th>
		            <?php endforeach; ?>
		            <?php if($edit): ?>
		                <th><?php echo Yii::t('web', 'Actions')?></th>
		            <?php endif; ?>
		        </tr>
		    </thead>
		    <tbody>
		        <?php
		        $i = 0;
		        foreach($rows as $idx => $row):
		              $formModel->load(["{$formModel->formName()}" => $row]);
		            ?>
		            <?=
		          
		            Yii::$app->controller->renderFile(__DIR__.'/row.php', [
		                'idx' => $i,
		                'row' => $row,
		                'edit' => $edit,
		                'formModel' => $formModel
		                    ] + (isset($form) ? ['form' => $form] : []));
		            $i++;
		            ?>
		        <?php endforeach; ?>
		    </tbody>
		</table>
	</div>

