<?php
use yii\web\View;
use yii\helpers\Html;

$this->registerJsFile('@web/js/documents/tables.js?'.uniqid(), ['depends' => [ \yii\jui\JuiAsset::className()]]);
?>
<div class="col-xs-12 document-tables-<?= $name ?>" >

    <?= Yii::$app->controller->renderFile(__DIR__.'/table-body.php', ['section' => $section, 'rows' => $rows, 'formModel' => $formModel] + (isset($form) ? ['form' => $form] : [])) ?>

    <?php if($edit): ?>
        <div class="ajax-loader">
            <?php echo Html::img('@web/images/ajax-loader.gif') ?>
        </div>

        <span class="add-row-button btn btn-link" >
            <span class="glyphicon glyphicon-plus-sign"></span>
            <span><?= Yii::t('web', 'Add row') ?></span>
        </span>
        <?= Html::input('number', 'input-table-add-row-number', 1, ['class' => 'form-control', 'min' => 1, 'style' => [
        		'width' => '7%', 'display' => 'inline'
        ]]) ?>
    <?php endif; ?>

</div>

<?php
$this->registerJs('
        (function() {
            initializeTables(
                "document-tables-'.$name.'",
                {
                    rowUrl: "'.$url.'",
                    edit: '.(int) $edit.',
                    startIdx: '.(int)(count($rows)).'
                }
            );
        })();
    ', View::POS_END);
?>

