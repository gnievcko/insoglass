<tr class="document-table-row">
	<?php if(!$edit && !$formModel->hasIndex()) echo '<td>'.($idx).'</td>'; ?>
	<?php foreach ($formModel->getAttributes() as $key => $value) : ?>
		<td><?= $edit ? $form->field($formModel, "[{$idx}]". $key)->textarea(['class' => 'form-control xSmallTextarea rows-sensitive', 'rows' => 4])->label(false) : ($row[$key] ?: '-') ?></td>
	<?php endforeach; ?>

    <?php if($edit): ?>
        <td class="text-center">
        	<div class="base-icon copy-icon action-icon copy-item-btn copy-row-btn"></div>
            <div class="base-icon garbage-icon action-icon remove-item-btn remove-row-btn" style="margin: auto"></div>
        </td>
    <?php endif; ?>
</tr>
