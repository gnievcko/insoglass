<?php if(isset($documents) && !empty($documents)): ?>
    <div>
        <div class="btn btn-primary import">
            <?= Yii::t('web', 'Import') ?>
        </div>
    </div><br/>
    <?php endIf; ?>
<?php
$this->registerJsFile('@web/js/documents/import.js?'.uniqid(), ['depends' => [\yii\web\JqueryAsset::className()]]);
echo $documentView;
if(isset($documents) && !empty($documents)) {
    echo Yii::$app->controller->renderPartial('@common/documents/html/views/modal.php', ['documents' => $documents, 'orderId' => $orderId]);
}
?>