<?php
    use Mindseater\PHPUtils\Text\StringSequenceGenerator;
?>

<div class="text-center">
    <h4><?= Yii::t('documents', 'Supervising board signatures') ?>:</h4>
</div>

<?php 
    $dotsGenerator = new StringSequenceGenerator('.');
    $numberOfDots = 50;
?>
<div class="col-xs-12" style="margin-bottom: 16px">
	<div class="row">
	    <div class="col-sm-6 text-center">
	        <div>
	            <?= Yii::t('documents', 'Ordering party') ?>
	        </div>
	        <div style="margin-top: 8px">
	            a) <?= $dotsGenerator->repeat($numberOfDots) ?>
	        </div>
	        <div style="margin-top: 8px">
	            b) <?= $dotsGenerator->repeat($numberOfDots) ?>
	        </div>
	    </div>
	    <div class="col-sm-6 text-center">
	        <div>
	            <?= Yii::t('documents', 'Executor') ?>
	        </div>
	        <div style="margin-top: 8px">
	            a) <?= $dotsGenerator->repeat($numberOfDots) ?>
	        </div>
	        <div style="margin-top: 8px">
	            b) <?= $dotsGenerator->repeat($numberOfDots) ?>
	        </div>
	    </div>
    </div>
</div>
