<?php 
    use yii\web\View;
    use yii\helpers\Html;
    use yii\helpers\Url;

    $edit = $section->isEditable() && !empty(Yii::$app->params['isWarehouseDocumentProductsEditable']);

    $this->registerJsFile('@web/js/documents/goods-received-products.js?'.uniqid(), ['depends' => [\yii\web\JqueryAsset::className()]]);
?>

<div class="goods-received-products">
    <?php if($edit): ?>
        <span class="add-goods-received-product-button">
            <span class="glyphicon glyphicon-plus-sign"></span>
            <span><?= Yii::t('web', 'Add product') ?></span>
        </span>
    <?php endif; ?>
    
    <table class="table">
        <thead>
            <tr>
                <th>
                    <?= $section->getAttributeLabel('index') ?>
                </th>
                <th style="min-width: 250px;">
                    <?= $section->getAttributeLabel('name') ?>
                </th>
                <th>
                    <?= $section->getAttributeLabel('count') ?>
                </th>
                <th>
                    <?= $section->getAttributeLabel('price') ?>
                </th>
                <th>
                    <?= $section->getAttributeLabel('unit') ?>
                </th>
                <th>
                	<?= $section->getAttributeLabel('note') ?>
                </th>
                <th>
                    <?= $section->getAttributeLabel('costSum') ?>
                </th>
                <?php if($edit): ?>
                    <th></th>
                <?php endif; ?>
            </tr>
        </thead>
        <tbody>
            <?= Yii::$app->controller->renderFile(__DIR__.'/goods-received-products/table-body.php', ['section' => $section] + (isset($form) ? ['form' => $form] : [])) ?>
        </tbody>
    </table>
    <div class="ajax-loader">
        <?php //echo Html::img('@web/images/ajax-loader-big-circle.gif') ?>
    </div>

    <?php if($edit): ?>
        <span class="add-goods-received-product-button">
            <span class="glyphicon glyphicon-plus-sign"></span>
            <span><?= Yii::t('web', 'Add product') ?></span>
        </span>
    <?php endif; ?>

</div>

<?php
    $this->registerJs('
        (function() {
            initializeGoodsReceivedProductsTables(
                "goods-received-products", 
                {
    				calculationsUrl: "'.Url::to(['document/calculate-goods-received-amounts']).'",
                    productRowUrl: "'.Url::to(['document/get-goods-received-product-row']).'",
                    edit: '.(int)$edit.'
                }
            );
        })()
    ', View::POS_END);
?>
