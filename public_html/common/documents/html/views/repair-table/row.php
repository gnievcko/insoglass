<?php

use yii\helpers\Html;
use common\helpers\StringHelper;
?>

<div class="document-table-row">
    <div class="col-sm-7">

            <?php
            if(!empty($row)) {
                $formModel['place'] = $row['place'];
                $formModel['description'] = $row['description'];
            }
            ?>

        <div><?= $edit ? $form->field($formModel, "[{$idx}]".'place')
                ->textInput(['placeHolder' => $formModel->getAttributeLabel('place')])
                ->label(false) : '<strong>'.($row['place'] ?: '-').'</strong>' ?></div>
        <div><?= $edit ? $form->field($formModel, "[{$idx}]".'description')
                ->textArea(['placeHolder' => $formModel->getAttributeLabel('description')])
                ->label(false) : (nl2br($row['description']) ?: '-') ?></div>

    </div>
    <div class="col-sm-1"><?php if($edit): ?>
            <div>
                <div class="base-icon garbage-icon action-icon remove-row-btn"></div>
            </div>
        <?php endif; ?>
    </div>
</div>