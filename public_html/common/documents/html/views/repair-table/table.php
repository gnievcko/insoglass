<?php

use yii\web\View;
use yii\helpers\Html;

$this->registerJsFile('@web/js/documents/repair.js?'.uniqid(), ['depends' => [\yii\jui\JuiAsset::className()]]);
$edit = $section->isEditable();
?>
<div class="document-tables col-xs-12" >
	<div class="row">
	    <div class="table-body">
	        <?php
	        $i = 0;
	        foreach($section->rows as $idx => $row):
	            ?>
	            <?=
	            Yii::$app->controller->renderFile(__DIR__.'/row.php', [
	                'idx' => $i,
	                'row' => $row,
	                'edit' => $edit,
	                'formModel' => $section->form
	                    ] + (isset($form) ? ['form' => $form] : []));
	            $i++;
	            ?>
	        <?php endforeach; ?>
	        <?php if($edit): ?>
	        <div class="ajax-loader">
	            <?php echo Html::img('@web/images/ajax-loader.gif') ?>
	        </div>
			<div class="col-xs-12">
				<div class="row">
			        <span class="add-row-button btn btn-link" >
			            <span class="glyphicon glyphicon-plus-sign" ></span>
			            <span><?= Yii::t('documents', 'Add place') ?></span>
			        </span>
		        </div>
	        </div>
	    <?php endif; ?>
		</div>
    </div>
</div>
<?php
$this->registerJs('
        (function() {
            repair(
                "document-tables",
                {
                    rowUrl: "'.$url.'",
                    edit: '.(int) $edit.',
                    startIdx: '.(int)$i.'
                }
            );
        })();
    ', View::POS_END);
?>
