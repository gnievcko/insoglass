<?php 
use common\helpers\StringHelper;
$this->title = $section->getAttributeLabel('report-orders').' - '.Yii::$app->name;
?>

<div class="text-center"><h2><i><?= $section->getAttributeLabel('report-orders') ?></i></h2></div>

<div class="text-center">
	<?= $section->getAttributeLabel('forPeriod') . ': ' .  
	$section->dateFrom . ' - ' .
	$section->dateTo ?>
</div>

<div class="text-center"><?= $section->getAttributeLabel('generatedFor') . ': ' . $section->user ?></div>

<div class="text-center"><?= $section->getAttributeLabel('generationDate') . ': ' . $section->dateGenerated ?></div>

<br><br>

<div><?= $section->getAttributeLabel('openOrderCount') . ': ' . $section->statistics['openOrderCount']?></div>

<div><?= $section->getAttributeLabel('closedOrderCount') . ': ' . $section->statistics['closedOrderCount']?></div>

<div><?= $section->getAttributeLabel('ongoingOrderCount') . ': ' . $section->statistics['ongoingOrderCount']?></div>

<div><?= $section->getAttributeLabel('handledOrderCount') . ': ' . $section->statistics['handledOrderCount']?></div>

<br>

<h4><?= $section->getAttributeLabel('orderCountByStatus') ?></h4>

<?php if(!empty($section->orderCountByStatus)) {?>
	
		<table class="table-protocol-detail" border="1">
			<tr>
		    	<th><?= $section->getAttributeLabel('status') ?></th>
		    	<th><?= $section->getAttributeLabel('number') ?></th>
		  	</tr>
			<?php foreach($section->orderCountByStatus as $row) {?>
		  		<tr>
		    		<td style="width: 80%"><?= $row['name'] ?></td>
		    		<td><?= $row['count'] ?></td>
		  		</tr>
		  	<?php }?>
		</table>
	
<?php }
else {
	echo $section->getAttributeLabel('none');
}?>

<h4><?= $section->getAttributeLabel('orderCountByClient') ?></h4>

<?php if(!empty($section->orderCountByClient)) {?>
	<div class="table-responsive">
		<table class="table-protocol-detail" border="1">
			<tr>
		    	<th><?= $section->getAttributeLabel('client') ?></th>
		    	<th><?= $section->getAttributeLabel('number') ?></th>
		  	</tr>
			<?php foreach($section->orderCountByClient as $row) {?>
		  		<tr>
		    		<td style="width: 80%"><?= $row['name'] ?></td>
		    		<td><?= $row['count'] ?></td>
		  		</tr>
		  	<?php }?>
		</table>
	</div>
<?php }
else {
	echo $section->getAttributeLabel('none');
}?>

<h4><?= $section->getAttributeLabel('orderCountByAuthor') ?></h4>

<?php if(!empty($section->orderCountByAuthor)) {?>
	<div class="table-responsive">
		<table class="table-protocol-detail" border="1">
			<tr>
		    	<th><?= $section->getAttributeLabel('author') ?></th>
		    	<th><?= $section->getAttributeLabel('number') ?></th>
		  	</tr>
			<?php foreach($section->orderCountByAuthor as $row) {?>
				<tr>
					<td style="width: 80%"><?= $row['name'] ?></td>
					<td><?= $row['count'] ?></td>
				</tr>
			<?php }?>
		</table>
	</div>
<?php }
else {
	echo $section->getAttributeLabel('none');
}?>

<h4><?= $section->getAttributeLabel('orderCountByEmployeeUpdating') ?></h4>

<?php if(!empty($section->orderCountByEmployeeUpdating)) {?>
	<div class="table-responsive">
		<table class="table-protocol-detail" border="1">
			<tr>
		    	<th><?= $section->getAttributeLabel('employee') ?></th>
		    	<th><?= $section->getAttributeLabel('number') ?></th>
		  	</tr>
			<?php foreach($section->orderCountByEmployeeUpdating as $row) {?>
			  	<tr>
				    <td style="width: 80%"><?= $row['name'] ?></td>
				    <td><?= $row['count'] ?></td>
			  	</tr>
			 <?php }?>
		</table>
	</div>
<?php }
else {
	echo $section->getAttributeLabel('none');
}?>

<h4><?= $section->getAttributeLabel('upcomingOrders') ?></h4>

<?php if(!empty($section->upcomingOrders)) {?>
	<div class="table-responsive">
		<table class="table-protocol-detail" border="1">
		  	<tr>
			    <th><?= $section->getAttributeLabel('title') ?></th>
			    <th><?= $section->getAttributeLabel('currentStatus') ?></th>
			    <th><?= $section->getAttributeLabel('totalCost') ?></th>
			    <th><?= $section->getAttributeLabel('dateDeadline') ?></th>
		  	</tr>
			<?php foreach($section->upcomingOrders as $row) {?>
		  		<tr>
		    		<td><?= $row['title'] ?></td>
		    		<td><?= $row['status']?></td>
		    		<td><?= StringHelper::getFormattedCost($row['sum']) ?></td>
		    		<td><?= !empty($row['date_deadline']) ? StringHelper::getFormattedDateFromDate($row['date_deadline']) : '-'?></td>
		  		</tr>
		  	<?php }?>
		</table>
	</div>
<?php }
else {
	echo $section->getAttributeLabel('none');
}?>


