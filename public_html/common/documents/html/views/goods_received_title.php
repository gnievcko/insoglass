<?php 
use yii\helpers\Html;
use kartik\datecontrol\DateControl;
use kartik\datecontrol\Module;
    
$editMode = $section->isEditable();
    
$this->title = \Yii::t('documents', 'Goods Received Note (GRN)').' - '.\Yii::$app->name;
?>

<div class="text-center">
    <div>
    	<?php if(!empty($section->issueId) && !empty($section->issueNumber)) { ?>
        	<h2><?= \Yii::t('documents', 'Accounting of goods issued no. {number}', ['number' => $section->issueNumber]) ?></h2>
        <?php } 
        else {?>
        	<h2><?= \Yii::t('documents', 'Goods Received Note (GRN)') ?></h2>
        <?php } ?> 
    </div>
</div>

<table style="margin-top: 10px; margin-bottom: 20px <?= $editMode ? '; width: 50%' : '' ?>">
	<tr>
		<th><?= $section->getAttributeLabel('number') ?></th>
		<td><?= $editMode ? $form->field($section, 'number')->textInput(['readonly' => true])->label(false) : $section->number ?></td>
	</tr>
	<tr>
		<th><?= $section->getAttributeLabel('warehouseName') ?></th>
		<td><?= $editMode ? $form->field($section, 'warehouseName')->textInput()->label(false) : $section->warehouseName ?></td>
	</tr>
	<tr>
		<th><?= $section->getAttributeLabel('supplierName') ?></th>
		<td><?= $editMode ? $form->field($section, 'supplierName')->textInput()->label(false) : ($section->supplierName ?: '-') ?></td>
	</tr>
	<tr>
		<th><?= $section->getAttributeLabel('userConfirmingName') ?></th>
		<td><?= $editMode ? $form->field($section, 'userConfirmingName')->textInput()->label(false) : $section->userConfirmingName ?></td>
	</tr>
	<tr>
		<th><?= $section->getAttributeLabel('dateDelivery') ?></th>
		<td><?=
			$editMode 
				? $form->field($section, 'dateDelivery')->widget(DateControl::classname(), [
	                    'language' => \Yii::$app->language,
	                    'type' => DateControl::FORMAT_DATETIME,
	                    'ajaxConversion' => false,
	                    'displayFormat' => 'php:d.m.Y H:i',
						'saveFormat' => 'php:d.m.Y H:i',
	                    'options' => [
		                        'pluginOptions' => [
		                            	'autoclose' => true
		                        ]
	                    ]
	                ])->label(false)
            	: $section->dateDelivery 
		?></td>
	</tr>
	<tr>
		<th><?= $section->getAttributeLabel('dateReception') ?></th>
		<td><?=
			$editMode 
				? $form->field($section, 'dateReception')->widget(DateControl::classname(), [
	                    'language' => \Yii::$app->language,
	                    'type' => DateControl::FORMAT_DATETIME,
	                    'ajaxConversion' => false,
	                    'displayFormat' => 'php:d.m.Y H:i',
						'saveFormat' => 'php:d.m.Y H:i',
	                    'options' => [
		                        'pluginOptions' => [
		                            	'autoclose' => true
		                        ]
	                    ]
	                ])->label(false)
            	: $section->dateReception 
		?></td>
	</tr>
	<tr>
		<th><?= $section->getAttributeLabel('dateIssue') ?></th>
		<td><?=
			$editMode 
				? $form->field($section, 'dateIssue')->widget(DateControl::classname(), [
	                    'language' => \Yii::$app->language,
	                    'type' => DateControl::FORMAT_DATETIME,
	                    'ajaxConversion' => false,
	                    'displayFormat' => 'php:d.m.Y H:i',
						'saveFormat' => 'php:d.m.Y H:i',
	                    'options' => [
		                        'pluginOptions' => [
		                            	'autoclose' => true
		                        ]
	                    ]
	                ])->label(false)
            	: $section->dateIssue 
		?></td>
	</tr>
    <tr>
    	<th style="vertical-align: top"><?= $section->getAttributeLabel('description') ?></th>
    	<td><?= $editMode ? $form->field($section, 'description')->textarea()->label(false) : ($section->description ?: '-') ?></td>
    </tr>
    
    <?= $editMode ? $form->field($section, 'issueId')->hiddenInput()->label(false) : '' ?>
    <?= $editMode ? $form->field($section, 'issueNumber')->hiddenInput()->label(false) : '' ?>
</table>
