<?php

use yii\helpers\Url;
use yii\bootstrap\Html;
use yii\web\View;
use dosamigos\ckeditor\CKEditor;
use backend\helpers\Utility;

$edit = $section->isEditable();
$this->title = \Yii::t('documents', 'Weighting CO2 protocol') . ' - ' . Yii::$app->name;
$this->registerJsFile('@web/js/documents/addTable.js?'.uniqid(), ['depends' => [\yii\web\JqueryAsset::className()]]);
?>

<div class="weight-protocol">
    <div class="text-center">
        <h3><?= \Yii::t('documents', 'Weighting CO2 protocol') ?></h3> 
        <div class="row">
            <div class="col-sm-offset-4 col-sm-1">
                <h4><?= $section->getAttributeLabel('date') . (!$edit ? $section->date : '') ?></h4>
            </div>  
            <div class="col-sm-3">
              <?= $edit ? $form->field($section, 'date')->textInput()->label(false) : '' ?>
            </div>
        </div>
    </div>

    <div class="col-xs-12 row">
        <div class="col-sm-7">
            <label><?= $section->getAttributeLabel('object') ?> <?= !$edit ? $section->object : '' ?></label>
            <div><?= $edit ? $form->field($section, 'object')->textInput()->label(false) : '' ?></div>
        </div>
    </div>	
    <div class="col-xs-12 row">
        <div class="col-sm-7">
                <label><?= ($edit || !empty($section->repairer)) ? $section->getAttributeLabel('repairer') . ':' : '' ?> <?= !$edit ? $section->repairer : '' ?></label>
            <div><?= $edit ? $form->field($section, 'repairer')->textInput()->label(false) : '' ?></div>
        </div>
    </div>
    <div class="col-xs-12 row">
        <div class="col-sm-7">
                <label><?= ($edit || !empty($section->measurementTool)) ? $section->getAttributeLabel('measurementTool') . ':' : '' ?> <?= !$edit ? $section->measurementTool : '' ?></label>
            <div><?= $edit ? $form->field($section, 'measurementTool')->textInput()->label(false) : '' ?></div>
        </div>
    </div>
    <?php if ($edit): ?>
        <h3>
            <div class="table-icon"></div> <?= Yii::t('documents', 'Add table') ?>
        </h3>
        <div class="col-xs-12 row" style="margin: 16px 0px"> 
            <div class="col-sm-2">
                <a class="btn add-table-button btn-default ">
                    <span><?= Yii::t('documents', 'Add table') ?></span>
                </a>
            </div>
            <div class="col-sm-4">
                <div class="inline-block"><label><?= Yii::t('web', 'Number of columns in a next table') ?></label></div>
                <div class="inline-block column-number"><?php
                  echo Html::dropDownList('count', 4, $section->numberList, ['class' => 'form-control th-count']);
                  ?></div>
            </div>
        </div>
    <?php endif; ?>
    <div class="weight-tables col-sm-12">
      <?php
      if (empty($section->tables)) {
          $section->tables = [['rows' => [[]]]];
      }
      foreach ($section->tables as $table) {
          if (count($table['rows']) > 0) {
              echo $this->renderFile('@common/documents/html/views/table/table.php', ['table' => $table, 'edit' => $edit]);
          }
      }
      ?>
    </div>
    <?php if ($edit): ?>
        <div class="ajax-loader">
          <?php echo Html::img('@web/images/ajax-loader.gif') ?>
        </div>
    <?php endif; ?>
    <br/>
    <br/>
    <br/>
    <?php
    $this->registerJs('
	            initializeAddTable(
	                "weight-tables",
	                {
	                    rowUrl: "' . Url::to(['protocol/get-table']) . '",
    					edit: "' . $edit . '",
                        sectionClass: "weight-protocol",
	                }
	            );
	        
	    ', View::POS_END);
    ?>

    <?php if ($edit): ?>
        <div class="col-xs-12 title-with-icon">
        	<div class="base-icon description-icon main-content-icons"></div><h3><?= $section->getAttributeLabel('text') ?></h3>
        </div>
    <?php endif; ?>



    <div class="col-sm-12">
      <?= $edit ? $form->field($section, 'text')->textArea()->label(false) : '<strong>' . nl2br($section->text) . '</strong><br><br>' ?>
    </div>

    <?php if(!$edit && $section->remarks) {
        echo '<div><strong><u>' . Yii::t('documents', 'Remarks').':</u></strong></div>';
    } ?>
    <?php if ($edit): ?>
        <div class="col-xs-12 title-with-icon">
        	<div class="base-icon remark-icon main-content-icons"></div>
        	<h3><?= Yii::t('documents', 'Protocol remarks') ?></h3>
        </div>
    <?php endif; ?>
    <div class="col-sm-12">
      <?= $edit ? $form->field($section, 'remarks')->textArea()->label(false) : nl2br($section->remarks) ?>
    </div>
    
    <?php if(!$edit && $section->key) { 
      echo '<div><strong><u>' . Yii::t('documents', 'Key').':</u></strong></div>';
    } ?>
    <?php if($edit): ?>
		<div class="col-xs-12 title-with-icon">
        	<div class="base-icon remark-icon main-content-icons"></div>
            <h3><?= Yii::t('documents', 'Key') ?></h3>
        </div>
    <?php endif; ?>
    <div class="col-sm-12">
    	<?= $edit ? $form->field($section, 'key')->widget(CKEditor::className(), Utility::getCKEditorParams())->label(false) : $section->key ?>
    </div>
    
    <div class="row text-center">
        <div class="col-sm-6">
            <div><?= Yii::t('documents', 'CREATED BY') ?></div>
            <br>
            <div>....................................................</div>
            <div><?= (Yii::t('documents', 'Repairer - Contractor')) ?></div>
        </div>
        <div class="col-sm-6">
            <div><?= Yii::t('documents', 'ACCEPTED BY') ?></div>
            <br>
            <div>....................................................</div>
            <div><?= (Yii::t('documents', 'User')) ?></div>
        </div>
    </div>
</div>