<div class="col-xs-12 title-with-icon">
	<div class="base-icon table-icon main-content-icons"></div>
	<h3><?= Yii::t('web', 'Additional data') ?></h3>
</div>
<div class="col-xs-12 row">
    <div class="col-sm-3">
        <?= $form->field($section, 'prepaymentAmount')->textInput([
        		'placeholder' => $section->getAttributeLabel('prepaymentAmount'),
        ]) ?>
    </div>
</div>
<div class="col-xs-12 row">
    <div class="col-sm-3">
        <?= $form->field($section, 'companyBank')->textInput([
            'placeholder' => $section->getAttributeLabel('companyBank')
        ]) ?>
    </div>
</div>
<div class="col-xs-12 row">
    <div class="col-sm-3">
        <?= $form->field($section, 'companyBankAccount')->textInput([
            'placeholder' => $section->getAttributeLabel('companyBankAccount')
        ]) ?>
    </div>
</div>
<div class="col-xs-12 row">
    <div class="col-sm-3">
        <?= $form->field($section, 'companySwift')->textInput([
            'placeholder' => $section->getAttributeLabel('companySwift')
        ]) ?>
    </div>
</div>