<?php
    use yii\widgets\ActiveForm;
    use yii\helpers\Html;
use common\bundles\EnhancedDialogAsset;
use common\documents\DocumentType;
use common\helpers\StringHelper;
use common\models\aq\OrderQuery;

    $form = ActiveForm::begin([
        'enableAjaxValidation' => true,
        'enableClientValidation' => false,
    ]);

    $header = $document->getHeader();
    if($header !== null) {
        $viewPath = __DIR__.'/'.strtolower($header->getType()).'.php';
        echo Yii::$app->controller->renderFile($viewPath, ['section' => $header, 'form' => $form]);
    }

    foreach($document->getSectionsSequence() as $section) {
        $viewPath = __DIR__.'/'.strtolower($section->getType()).'.php';
        echo Yii::$app->controller->renderFile($viewPath, ['section' => $section, 'form' => $form]);
    }    

    $footer = $document->getFooter();
    if($footer !== null) {
        $viewPath = __DIR__.'/'.strtolower($footer->getType()).'.php';
        echo Yii::$app->controller->renderFile($viewPath, ['section' => $footer, 'form' => $form]);
    }
?>

<?php if($document->isEditable()): ?>
	
	<?php if(in_array($document->getType(), [DocumentType::INVOICE]) && empty(Yii::$app->request->get('edit', 0))) { ?>
		<div class="order-reminder-container row">
			<h3><?= StringHelper::translateOrderToOffer('web', 'You can set an alert in the form of a task so the system can remind you about the possibility of renewing the order.') ?></h3>
			<?php $invoiceTitleSection = $document->getSectionsSequence()[1];
			echo Yii::$app->controller->renderFile('@common/components/views/orderReminderTrait.php', [
					'form' => $form, 
					'model' => $invoiceTitleSection, 
					'order' => OrderQuery::getDetails($invoiceTitleSection['orderId']),
			]) ?>
		</div>
	<?php } ?>
	
	<?php if(in_array($document->getType(), array_diff(DocumentType::getInvoiceTypes(), [DocumentType::CORRECTIVE_INVOICE]))): ?>
		<div class="col-sm-12 margint-16">
		    <div class="text-right">
		    	<?php $text = !empty($lastInvoiceDate)
		    			? StringHelper::translateOrderToOffer('documents', 
								'Are you sure you want to generate this invoice?<br/>The invoice for this order was generated at {date}.', 
								['date' => $lastInvoiceDate])
						: Yii::t('documents', 'Are you sure you want to generate this invoice?');
		    	?>
		    
		        <?= Html::submitButton(Yii::t('main', 'Generate'), ['class' => 'btn btn-primary', 'data' => [
						'confirm' => $text,
				],]) ?>
		    </div>
		</div>
	<?php else: ?>
		<div class="col-sm-12 margint-16">
		    <div class="text-right">
		        <?= Html::submitButton(Yii::t('main', 'Generate'), ['class' => 'btn btn-primary']) ?>
		    </div>
		</div>
	<?php endif; ?>
<?php endif; ?>

<?php ActiveForm::end(); EnhancedDialogAsset::register($this); ?>
