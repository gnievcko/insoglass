<?php
use yii\helpers\Url;
use yii\web\View;
use yii\helpers\Html;

$this->registerJsFile('@web/js/documents/addTable.js?'.uniqid(), ['depends' => [\yii\web\JqueryAsset::className()]]);
$edit = $section->isEditable();
$this->title = \Yii::t('documents', 'Service protocol').' - '.Yii::$app->name;
?>

<div class="service-protocol">
    <div class="text-center">
        <div class="title-with-subtitle">
            <h3 class="upper"><?= \Yii::t('documents', 'Service protocol') ?></h3> 
            <div class="document-date">
                <div><h4 class="mt-0"><?= $section->getAttributeLabel('date').(!$edit ? $section->date : '') ?></h4></div>
                <?= $edit ? $form->field($section, 'date')->textInput()->label(false) : '' ?>
            </div>
        </div>
    </div>
    <div class="col-xs-12">
    	<div class="row">
	    	<div class="col-sm-12"><?= ($edit || !empty($section->relatedTo)) ? $section->getAttributeLabel('relatedTo').':': '' ?> <?= !$edit ? $section->relatedTo : '' ?></div>
	    	<div class="col-sm-7"><?= $edit ? $form->field($section, 'relatedTo')->textInput()->label(false) : '' ?></div>
    	</div>
    </div>
    <div class="col-xs-12">
    	<div class="row">
	        <div class="col-sm-12"><?= $section->getAttributeLabel('object') ?> <?= !$edit ? $section->object : '' ?></div>
	    	<div class="col-sm-7"><?= $edit ? $form->field($section, 'object')->textInput()->label(false) : '' ?></div>
    	</div>
    </div>
	<div class="col-xs-12">
    	<div class="row">
			<div class="col-sm-12"><?= ($edit || !empty($section->repairer)) ? $section->getAttributeLabel('repairer').':': '' ?> <?= !$edit ? $section->repairer : '' ?></div>
	    	<div class="col-sm-7"><?= $edit ? $form->field($section, 'repairer')->textInput()->label(false) : '' ?></div>
    	</div>
	</div>
  
    <?php if($edit): ?>
    <div class="col-xs-12 title-with-icon">
        <div class="base-icon add-icon main-content-icons"></div><h3><?= Yii::t('documents', 'Add table') ?></h3>
    </div>    
        <div class="add-table-btn-label col-xs-12">
        	<a class="btn add-table-button btn-default">
           		<span><?= Yii::t('documents', 'Add table') ?></span>
        	</a>
        	<div class="label-col-table">
        		<label>
        			<?php echo Yii::t('web','Number of columns in a next table')?>
        		</label>
        	</div>
                <?php
                    echo Html::dropDownList('count', 4, $section->numberList, ['class' => 'form-control th-count']);
                ?>
        </div>
    <div class="service-order-table col-xs-12">    
	    <?php endif; ?>
	    
	    <div class="service-tables ">
	        <?php
	
	        if(empty($section->tables)) {
	            $section->tables = [['rows' => [[]]]];
	        }
	        foreach($section->tables as $table) {
	            if(count($table['rows']) > 0) {
	                
	                echo $this->renderFile('@common/documents/html/views/table/table.php', ['table' => $table, 'edit' => $edit]);
	            }
	        }
	        ?>
	    </div>
	    <?php if($edit): ?>
	        <div class="ajax-loader">
	            <?php echo Html::img('@web/images/ajax-loader.gif') ?>
	        </div>
	    <?php endif; ?>
	    <?php
	    $this->registerJs('
		            initializeAddTable(
		                "service-tables",
		                {
		                    rowUrl: "'.Url::to(['protocol/get-table']).'",
	    					edit: "'.$edit.'",
	                        sectionClass: "service-protocol",
		                }
		            );
		        
		    ', View::POS_END);
	    ?>
    </div>
    
    <?php if($edit): ?>
   	<div class="col-xs-12 title-with-icon">
         <div class="base-icon remark-icon main-content-icons"></div><h3><?= Yii::t('documents', 'Additional text') ?></h3>
    </div>
    <?php endif; ?>
    <?php
    if(!$edit && $section->additionalText) {
        echo '<div><strong><u>'.Yii::t('documents', 'Additional text').':</u></strong></div>';
    }
    ?>
    <div class="col-xs-12">
    	<?= $edit ? $form->field($section, 'additionalText')->textArea(['placeholder' => Yii::t('documents','Type additional text')])->label(false) : nl2br($section->additionalText) ?>
    </div>
    
    <?php if($edit): ?>
   <div class="col-xs-12 title-with-icon">
        <div class="base-icon remark-icon main-content-icons"></div><h3><?= Yii::t('documents', 'Protocol remarks') ?></h3>
    </div>
    <?php endif; ?>
    <?php
    if(!$edit && $section->text) {
        echo '<div><strong><u>'.Yii::t('documents', 'Remarks').':</u></strong></div>';
    }
    ?>
    <div class="col-xs-12">
        <?= $edit ? $form->field($section, 'text')->textArea(['placeholder' => Yii::t('documents','Type remarks')])->label(false) : nl2br($section->text) ?>
    </div>

    <div class="col-xs-12 text-center signes">
    	<div class="row">
	        <div class="col-xs-6 page-break-inside">
	            <div><?= Yii::t('documents', 'CREATED BY') ?>
	            <br/>
	            ...................................................
	            <br/>
	            <?= (Yii::t('documents', 'Repairer - Contractor')) ?></div>
	        </div>
	        <div class="col-xs-6 page-break-inside">
	            <div><?= Yii::t('documents', 'ACCEPTED BY') ?>
	            <br/>
	            ....................................................
	            <br/>
	            <?= (Yii::t('documents', 'User')) ?></div>
	        </div>
        </div>
    </div>
</div>

