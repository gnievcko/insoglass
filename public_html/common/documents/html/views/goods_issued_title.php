<?php
use yii\helpers\Html;
use kartik\datecontrol\DateControl;
use kartik\datecontrol\Module;

$editMode = $section->isEditable();

$this->title = \Yii::t('documents', 'Goods Issue Note (GIN)').' - '.\Yii::$app->name;
?>

<div class="text-center">
    <div>
        <h2><?= \Yii::t('documents', 'Goods Issued no. {number}', ['number' => $section->number]) ?></h2> 
    </div>
</div>

<?= $form->field($section, 'isNumberPredefined')->hiddenInput()->label(false) ?>

<?= $form->field($section, 'number')->hiddenInput()->label(false) ?>
 
<div class="row col-xs-12">
	<div class="col-sm-7">
		<label><?= $section->getAttributeLabel('warehouseName') ?></label>
		<div><?= $editMode ? $form->field($section, 'warehouseName')->textInput()->label(false) : $section->warehouseName ?></div>
	</div>
</div>
<div class="row col-xs-12">
	<div class="col-sm-7">
		<label><?= $section->getAttributeLabel('userReceivingName') ?></label>
		<div><?= $editMode ? $form->field($section, 'userReceivingName')->textInput()->label(false) : $section->userReceivingName ?></div>
	</div>
</div>
<div class="row col-xs-12">
	<div class="col-sm-7">
		<label><?= $section->getAttributeLabel('userConfirmingName') ?></label>
		<div><?= $editMode ? $form->field($section, 'userConfirmingName')->textInput()->label(false) : $section->userConfirmingName ?></div>
	</div>
</div>
<div class="row col-xs-7">
	<div class="col-sm-6">
		<label><?= $section->getAttributeLabel('dateOperation') ?></label>
		<div><?=
			$editMode 
				? $form->field($section, 'dateOperation')->widget(DateControl::classname(), [
	                    'language' => \Yii::$app->language,
	                    'type' => DateControl::FORMAT_DATETIME,
	                    'ajaxConversion' => false,
	                    'displayFormat' => 'php:d.m.Y H:i',
						'saveFormat' => 'php:d.m.Y H:i',
	                    'options' => [
		                        'pluginOptions' => [
		                            	'autoclose' => true
		                        ]
	                    ]
	                ])->label(false)
            	: $section->dateOperation 
		?></div>
	</div>
	<div class="col-sm-6">
		<label><?= $section->getAttributeLabel('dateIssue') ?></label>
		<div><?=
			$editMode 
				? $form->field($section, 'dateIssue')->widget(DateControl::classname(), [
	                    'language' => \Yii::$app->language,
	                    'type' => DateControl::FORMAT_DATETIME,
	                    'ajaxConversion' => false,
	                    'displayFormat' => 'php:d.m.Y H:i',
						'saveFormat' => 'php:d.m.Y H:i',
	                    'options' => [
		                        'pluginOptions' => [
		                            	'autoclose' => true
		                        ]
	                    ]
	                ])->label(false)
            	: $section->dateIssue 
		?></div>
	</div>
</div>
<div class="row col-xs-12">
	<div class="col-sm-7">
		<label><?= $section->getAttributeLabel('goesTo') ?></label>
		<div><?= $editMode ? $form->field($section, 'goesTo')->textarea()->label(false) : ($section->goesTo ?: '-') ?></div>
	</div>
</div>
<div class="row col-xs-12">
	<div class="col-sm-7">
		<label><?= $section->getAttributeLabel('status') ?></label>
		<div><?= $editMode ? $form->field($section, 'status')->radioList([
				Yii::t('documents', 'Not accounted'), 
				Yii::t('documents', 'Accounted')
		])->label(false) : (!empty($section->status) ? Yii::t('documents', 'Accounted') : Yii::t('documents', 'Not accounted')) ?></div>
	</div>
</div>
<div class="row col-xs-12">
	<div class="col-sm-7">
		<label><?= $section->getAttributeLabel('description') ?></label>
		<div><?= $editMode ? $form->field($section, 'description')->textarea()->label(false) : ($section->description ?: '-') ?></div>
	</div>
</div>
