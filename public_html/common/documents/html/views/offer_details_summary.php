<?php if(Yii::$app->params['isOrderNoteVisible']) { ?>
	<div class="col-xs-12 row">
		<div class="col-sm-7"><?= $form->field($section, 'note')
	    ->textarea()
	?>
		</div>
	</div>
<?php } ?>
<?php if(Yii::$app->params['isOrderRemarksVisible']) { ?>
	<div class="col-xs-12 row">
		<div class="col-sm-7"><?= $form->field($section, 'remarks')
	    ->textarea()
	?>
		</div>
	</div>
<?php } ?>
<?php if(Yii::$app->params['isExecutionTimeVisible']) { ?>
	<div class="col-xs-12 row">
		<div class="col-sm-7">
		<label><?php echo $section->getAttributeLabel('deadline')?></label>
	<?= $form->field($section, 'deadline')
	    ->textInput(['placeholder' => $section->getAttributeLabel('deadline')])
	    ->label(false) 
	?>
		</div>
	</div>
<?php } ?>
<?php if(Yii::$app->params['isPaymentTermsVisible']) { ?>
	<div class="col-xs-12 row">
		<div class="col-sm-7">
		<label><?php echo $section->getAttributeLabel('termsOfPayment')?></label>
	<?= $form->field($section, 'termsOfPayment') 
	    ->textInput(['placeholder' => $section->getAttributeLabel('termsOfPayment')])
	    ->label(false) 
	?>
		</div>
	</div>
<?php } ?>
<?php if(Yii::$app->params['isDurationTimeVisible']) { ?>
	<div class="col-xs-12 row">
		<div class="col-sm-7">
			<label><?php echo $section->getAttributeLabel('validityDate')?></label>
	<?= $form->field($section, 'validityDate') 
	    ->textInput(['placeholder' => $section->getAttributeLabel('validityDate')])
	    ->label(false) 
	?>
		</div>
	</div>
<?php } ?>