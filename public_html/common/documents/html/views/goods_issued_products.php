<?php 
    use yii\web\View;
    use yii\helpers\Html;
    use yii\helpers\Url;

    $edit = $section->isEditable() && !empty(Yii::$app->params['isWarehouseDocumentProductsEditable']);

    $this->registerJsFile('@web/js/documents/goods-issued-products.js?'.uniqid(), ['depends' => [\yii\web\JqueryAsset::className()]]);    
?>
<div class="col-xs-12 row">
<div class="goods-issued-products">
    <?php if($edit): ?>
        <span class="btn btn-link add-item-button add-goods-issued-product-button">
            <span class="glyphicon glyphicon-plus-sign"></span>
            <span><?= Yii::t('web', 'Add product') ?></span>
        </span>
    <?php endif; ?>
    
    <table class="table">
        <thead>
            <tr>
                <th>
                    <?= $section->getAttributeLabel('name') ?>
                </th>
                <th>
                    <?= $section->getAttributeLabel('location') ?>
                </th>
                <th>
                    <?= $section->getAttributeLabel('count') ?>
                </th>
                <th>
                    <?= $section->getAttributeLabel('unit') ?>
                </th>
                <?php if($edit): ?>
                    <th></th>
                <?php endif; ?>
            </tr>
        </thead>
        <tbody>
            <?= Yii::$app->controller->renderFile(__DIR__.'/goods-issued-products/table-body.php', ['section' => $section] + (isset($form) ? ['form' => $form] : [])) ?>
        </tbody>
    </table>
    <div class="ajax-loader">
        <?php //echo Html::img('@web/images/ajax-loader-big-circle.gif') ?>
    </div>

    <?php if($edit): ?>
        <span class="btn btn-link add-item-button add-goods-issued-product-button">
            <span class="glyphicon glyphicon-plus-sign"></span>
            <span><?= Yii::t('web', 'Add product') ?></span>
        </span>
    <?php endif; ?>

</div>
</div>
<?php
    $this->registerJs('
        (function() {
            initializeGoodsIssuedProductsTables(
                "goods-issued-products", 
                {
                    productRowUrl: "'.Url::to(['document/get-goods-issued-product-row']).'",
                    edit: '.(int)$edit.'
                }
            );
        })()
    ', View::POS_END);
?>
