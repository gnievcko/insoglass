<?php

$editMode = $section->isEditable();

$this->title = Yii::t('documents', 'Proforma invoice').' - '.Yii::$app->name;

?>
<h2 class="text-center"><?php echo Yii::t('documents', 'Proforma invoice')?></h2>

<div class="col-xs-12 row">
	<div class="col-sm-7">		
	    <div>
	        <?= $editMode ? $form->field($section, 'number')->textInput(['readonly' => true]) : $section->getAttributeLabel('number').': '.$section->number ?> 
	    </div>
	    <div>
	        <?= 
	            $form->field($section, 'isCopy')->hiddenInput()->label(false)
	        ?> 
	    </div>		
	</div>
</div>

<?php require('invoice-title/invoice-title-content.php'); ?>