<?php

$editMode = $section->isEditable();
$this->title = $section->getAttributeLabel('title').' - '.Yii::$app->name;
?>

	


<div class="text-center">
        <div class="title-with-subtitle">
            <h3 class="upper"><?= $section->getAttributeLabel('title')?></h3> 
            <div class="document-date">
                <h4 class="mt-0"><?= $section->getAttributeLabel('from')?><?= $editMode ? $form->field($section, 'dateOperation')->textInput()->label(false) : $section->dateOperation ?></h4>         
            </div>
        </div>
    </div>

<div class="col-xs-12">
	<div class="row">
    	<div class="col-xs-12"><label><?= $section->getAttributeLabel('onObject')?></label></div>
    	<div class="col-sm-7"><?= $editMode ? 
    					$form->field($section, 'companyName')->textInput(['placeHolder' => $section->getAttributeLabel('companyName')])->label(false) . $form->field($section, 'companyAddress')->textInput(['placeHolder' => $section->getAttributeLabel('address')])->label(false) : 
    					$section->companyName . ' ' . $section->companyAddress?>
    	</div>
    </div>
</div>

<div class="col-xs-12">
	<p><strong><?= $section->getAttributeLabel('serviceArrivalConfirmed') ?></strong></p>
</div>


<div class="col-xs-12">
	<div class="row">
    	<div class="col-xs-12"><?= $section->getAttributeLabel('followingTasksCompleted') ?></div>
    	<div class="col-xs-12"><label><?= $section->getAttributeLabel('reasonHoursLabel') ?></label></div>
    	<div class="col-sm-7"><?= $editMode ? $form->field($section, 'reasonHours')->textInput(['placeholder' => Yii::t('documents','Type number of hours')])->label(false) : $section->reasonHours ?></div>
    	<div class="col-xs-12"><label><?= $section->getAttributeLabel('repairHoursLabel') ?></label></div>
    	<div class="col-sm-7"><?= $editMode ? $form->field($section, 'repairHours')->textInput(['placeholder' => Yii::t('documents','Type number of hours')])->label(false) : $section->repairHours ?></div>
    </div>
</div>
<div>
	<div class="col-xs-12">
		<div class="col-xs-12"><label><?= $section->getAttributeLabel('firstHeader')?></label></div>
		<div class="col-sm-7"><?= $editMode ? $form->field($section, 'textArea1')->textArea(['placeholder' => Yii::t('documents','Type description here')])->label(false) : $section->textArea1 ?></div>
	</div>
	<div class="col-xs-12 row">
		<div class="col-xs-12"><label><?= $section->getAttributeLabel('secondHeader')?></label></div>
		<div class="col-sm-7"><?= $editMode ? $form->field($section, 'textArea2')->textArea(['placeholder' => Yii::t('documents','Type description here')])->label(false) : $section->textArea2 ?></div>
	</div>
	<div class="col-xs-12 row">
		<div class="col-xs-12"><label><?= $section->getAttributeLabel('thirdHeader')?></label></div>
		<div class="col-sm-7"><?= $editMode ? $form->field($section, 'textArea3')->textArea(['placeholder' => Yii::t('documents','Type description here')])->label(false) : $section->textArea3 ?></div>
	</div>
	<div class="col-xs-12 row">
		<div class="col-xs-12" style="font-size: 16px; padding: 16px 0px 16px 0px"><strong><?= $section->getAttributeLabel('fourthHeader')?></strong></div>
		<div class="col-xs-12"><label><?= $section->getAttributeLabel('remarks')?></label></div>
		<div class="col-sm-7"><?= $editMode ? $form->field($section, 'textArea4')->textArea(['placeholder' => Yii::t('documents','Type description here')])->label(false) : $section->textArea4 ?></div>
	</div>
	<div class="col-xs-12 row">
		<div class="col-xs-12"><label><?= $section->getAttributeLabel('repairer')?></label></div>
		<div class="col-sm-7"><?= $editMode ? $form->field($section, 'repairer')->textInput(['placeholder' => Yii::t('documents','Type a repairer')])->label(false) : $section->repairer ?></div>
	</div>
</div>

<div class="row text-center">
	<div class="col-sm-6">
		<div><?= $section->getAttributeLabel('createdBy') ?></div>
		<br>
		<div>....................................................</div>
		<div><?= $section->getAttributeLabel('(Repairer - Contractor)') ?></div>
	</div>
	<div class="col-sm-6">
		<div><?= $section->getAttributeLabel('acceptedBy') ?></div>
		<br>
		<div>....................................................</div>
		<div><?= $section->getAttributeLabel('(user)') ?></div>
	</div>
</div>

<br><br><br>


