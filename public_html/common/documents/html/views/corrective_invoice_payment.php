<?php
    use kartik\datecontrol\DateControl;
    use yii\helpers\Html;
    use Mindseater\PHPUtils\Dates\Formatter;
	use Mindseater\PHPUtils\Text\StringSequenceGenerator;
	
    $edit = $section->isEditable();
    $this->registerJsFile('@web/js/documents/invoiceHelper.js?'.uniqid(), ['depends' => [\yii\web\JqueryAsset::className()]]);
?>

<div>
    <div class="row">
        <div class="col-sm-6">
            <?=  $edit ? $form->field($section, 'paymentMethod') : '<b>'.$section->getAttributeLabel('paymentMethod').'</b>: '.$section->paymentMethod ?>
        </div>
      <?php if($edit) : ?>
        <div class="col-sm-6">
            <label>
                <?= Yii::t('documents', 'Set payment method') ?>
            </label>
            <?php
                echo Html::dropDownList('selectMethod', null, $section->methodList, ['class' => 'form-control pull-right']);
            ?>
        </div>
        <?php endif; ?>
    </div>
    <div class="row">
        <div class="col-sm-6">
            <?= 
                $edit 
                    ? $form->field($section, 'paymentDate')->widget(DateControl::classname(), [
                        'language' => \Yii::$app->language,
                        'type' => DateControl::FORMAT_DATE,
                        'ajaxConversion' => false,
                        'displayFormat' => 'php:d.m.Y',
                        'saveFormat' => 'php:Y-m-d',
                        'options' => [
                            'id' => 'invoiceId',
                            'pluginOptions' => [
                                'autoclose' => true
                            ]
                        ]
                      ])
                     : '<b>'.$section->getAttributeLabel('paymentDate').'</b>: '. (!empty($section->paymentDate) ? (new Formatter())->from($section->paymentDate, 'Y-m-d')->format('d.m.Y')->getDate() : '') ?>
        </div>
        <?php if($edit) : ?>
        <div class="col-sm-6">
            <label>
                <?= Yii::t('documents', 'Set payment date') ?>
            </label>
            <?php
                echo Html::dropDownList('addDate', null, $section->dayList, ['class' => 'form-control pull-right']);
            ?>
        </div>
        <?php endif; ?>
    </div>
    <div>
        <?= $edit ? $form->field($section, 'remarks')->textarea() : '<b>'.$section->getAttributeLabel('remarks').'</b>:<br/> '.nl2br($section->remarks) ?>
    </div>
    <div>
        <?= $edit ? $form->field($section, 'modifyReason')->textarea() : '<b>'.$section->getAttributeLabel('modifyReason').'</b>:<br/> '.nl2br($section->modifyReason) ?>
    </div>
    <div>
        <?= '<b>'.$section->getAttributeLabel('priceDifference').'</b>: '.($edit ? $form->field($section, 'priceDifference')->textInput(['readonly' => true])->label(false) : $section->priceDifference) ?>
    </div>
</div>
<br/>
<br/>

<?php 
    $dotsGenerator = new StringSequenceGenerator('.');
    $numberOfDots = 50;
?>

<div class="row text-center">
    <div class="col-sm-6">
        <?= Yii::t('documents', 'Signature of person authorized to receive an invoice') ?>
    </div> 
    <div class="col-sm-6">
        <?= Yii::t('documents', 'Signature of person authorized to issue an invoice') ?>
    </div> 
</div>
<div class="row text-center" style="margin-top: 15px">
    <div class="col-sm-6">
        <?= $dotsGenerator->repeat($numberOfDots) ?>
    </div> 
    <div class="col-sm-6">
        <?= $dotsGenerator->repeat($numberOfDots) ?>
    </div> 
</div>

