<?php 
use yii\helpers\Html;
$editMode = $section->isEditable();
    
$this->title = Yii::t('documents', 'Invoice').' - '.Yii::$app->name;

?>
<h2 class="text-center"><?php echo Yii::t('documents', 'Invoice')?></h2>

<div class="col-xs-12 row">
	<div class="col-sm-7">		
	    <div>
	        <?= $editMode ? $form->field($section, 'number')->textInput(['readonly' => true]) : $section->getAttributeLabel('number').': '.$section->number ?> 
	    </div>
	    <div><?php
	    	if(!Yii::$app->params['generateThreeCopyOfInvoiceOnOnePdf']) {
		        echo $editMode 
		                ? $form->field($section, 'isCopy')->radioList([
		                    0 => Yii::t('documents', 'Original'), 
		                    1 => Yii::t('documents', 'Copy')])->label(false)
		                : Html::tag($section->isCopy ? 'span' : 's', Yii::t('documents', 'Original')).' / '.Html::tag($section->isCopy ? 's' : 'span', Yii::t('documents', 'Copy'));
	    	}
		    else {
		    	echo $form->field($section, 'isCopy')->hiddenInput()->label(false);
		    }
	    ?></div>		
	</div>
</div>

<?php require('invoice-title/invoice-title-content.php'); ?>