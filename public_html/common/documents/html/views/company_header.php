<?php 
    use yii\helpers\Html;
?>

<div class="row">
    <div class="col-sm-6"> 
        <?= Html::img($section->companyLogo, ['style' => 'width: 200px']) ?>
    </div>
    <div class="col-sm-6 text-right"> 
        <?php if(!empty($section->companyCertificateLogo)) {
        	echo Html::img($section->companyCertificateLogo, ['style' => 'height: 130px']);
        } ?>
    </div>
</div>

<div class="row">
    <div class="col-sm-6"> 
        <strong><?= $section->companyFullName ?></strong><br />
        <span><?= $section->companyAddress ?></span><br />
        <span><?= $section->getAttributeLabel('companyVatId') ?>: <?= $section->companyVatId ?></span><br />
        <span><?= $section->getAttributeLabel('companyBankAccount') ?>: <?= $section->companyBankAccount ?></span><br />
        <?php if(!empty($section->companySwift)) { ?>
        	<span><?= $section->getAttributeLabel('companySwift') ?>: <?= $section->companySwift ?></span><br />
        <?php } ?>
        <span><?= $section->companyBank ?></span>
    </div>
</div>
