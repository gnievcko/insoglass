<?php
use dosamigos\ckeditor\CKEditor;
use backend\helpers\Utility;

$edit = $section->isEditable();
?>

<?php if($edit) { ?>
	<?= $form->field($section, 'companyId')->hiddenInput()->label(false) ?>
	<?= $form->field($section, 'orderId')->hiddenInput()->label(false) ?>

	<div class="">
    	<?= $form->field($section, 'text')->widget(CKEditor::className(), Utility::getCKEditorParams())?>
	</div>
<?php } 
else { ?>
	<?= $section->text ?>
<?php } ?>