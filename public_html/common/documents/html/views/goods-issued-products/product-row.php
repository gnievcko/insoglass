<?php
	$recalculateInputsOptions = ['class' => 'recalculates form-control'];
    $numericFieldsCommonOptions = ['type' => 'number'];
?>

<tr class="goods-issued-product">
    <td><?= $edit ? $form->field($product, "[{$idx}]name")->textarea(['class' => 'form-control smallTextarea'])->label(false) : $product->name ?></td>
    <td><?= $edit ? $form->field($product, "[{$idx}]location")->textInput()->label(false) : ($product->location ?: '-') ?></td>
    <td><?= $edit ? $form->field($product, "[{$idx}]count")->textInput($recalculateInputsOptions + $numericFieldsCommonOptions)->label(false) : $product->count ?></td>
    <td><?= $edit ? $form->field($product, "[{$idx}]unit")->textInput(['readonly' => !empty(Yii::$app->params['defaultUnit']) ? true : false])->textInput()->label(false) : $product->unit ?></td>
    <?php if($edit): ?>
        <td>
        	<div class="base-icon garbage-icon action-icon remove-item-btnremove-goods-issued-product-btn"></div>
        </td>
    <?php endif; ?>
</tr>
