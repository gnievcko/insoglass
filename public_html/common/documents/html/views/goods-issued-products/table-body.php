<?php
	$edit = $section->isEditable() && !empty(Yii::$app->params['isWarehouseDocumentProductsEditable']);
?>

<?php 
$i = 0;
foreach($section->products as $idx => $product): ?>
    <?= 
        Yii::$app->controller->renderFile(__DIR__.'/product-row.php', [
            'idx' => $i, 
            'product' => $product, 
            'edit' => $edit, 
        ] + (isset($form) ? ['form' => $form] : [])) 
    ?>
<?php 
$i++;
endforeach; 

//$summary = $section->calculateSummary();
?>
