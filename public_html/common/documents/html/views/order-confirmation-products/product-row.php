<?php 
	use common\helpers\StringHelper;

	$netValue = bcmul($product->price, $product->count, 2);
	$vatAmount = bcmul($netValue, $product->vatRate, 2);
	$grossValue = bcadd($netValue, $vatAmount, 2);

	$areaOptions = ['class' => 'form-control smallTexarea order-confirmation-name'];
	$recalculateInputsOptions = ['class' => 'recalculates form-control'];
	$recalculateVatRateInputsOptions = ['class' => 'recalculates form-control vat-rate-select'];
	$numericFieldsCommonOptions = ['type' => 'number'];

?>
<tr class="order-confirmation-product">
	<td><?= $prefix + 1 ?></td>
	<td><?= $form->field($product, "[{$prefix}]name")->textArea($areaOptions,['placeholder' => Yii::t('documents','Type name')])->label(false) ?></td>
	<td><?= $form->field($product, "[{$prefix}]count")->textInput([
				'placeholder' => $product->getAttributeLabel('count'),
				'class' => 'recalculates form-control order-confirmation-product-count',
				'type' => 'number',
			])->label(false) ?></td>
	<td><?= $form->field($product, "[{$prefix}]unit")->textInput(['placeholder' => $product->getAttributeLabel('unit'), 'readonly' => !empty(Yii::$app->params['defaultUnit']) ? true : false])->label(false)?></td>
	<td><?= $form->field($product, "[{$prefix}]price")->textInput([
				'placeholder' => $product->getAttributeLabel('price'),
				'class' => 'recalculates form-control order-confirmation-product-price',
				'type' => 'number',
				'step' => '0.01',
			])->label(false) ?></td>
	<td><?= $form->field($product, "[{$prefix}]vatRate")->dropDownList($product->getVatRates(), $recalculateVatRateInputsOptions)->label(false) ?></td>
	<td class="gross-price-value"><?= StringHelper::getFormattedCost($grossValue).' '.$currencySymbol ?></td>
	<td>
	    <div class="base-icon delete-icon action-icon remove-item-btn remove-order-confirmation-product-btn"></div>
	</td>
</tr>