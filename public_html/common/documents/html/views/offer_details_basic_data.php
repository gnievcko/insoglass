<?php
    $this->title = Yii::t('documents', 'Offer details protocol').' - '.Yii::$app->name;
?>

<h3 class="text-center"><?= \common\helpers\StringHelper::translateOrderToOffer('main', 'Order') ?></h3>
<div class="col-xs-12 text-center">
    
        <span><?= Yii::t('documents_orders', 'No.') ?></span>
        <span><?= $section->number ?></span>
        <?= $form->field($section, 'number')->hiddenInput()->label(false) ?>

</div>
<div class="col-xs-12 row">
    <div class="col-sm-3">
        <?= $form->field($section, 'dateCreation')->textInput([
            'placeholder' => $section->getAttributeLabel('dateCreation')
        ]) ?>
    </div>
</div>


<div class="col-sm-6 row">
	<div class="col-xs-12">
	    <strong><?= Yii::t('documents', 'To Mr/Mrs')?>:</strong>
	</div>
	<div class="col-sm-12">
		<?= $form->field($section, 'contactPeople') 
					->textarea(['placeholder' => $section->getAttributeLabel('contactPeople')])
					->label(false);
		?>
	</div>
	<?php if(!empty($section->contactPerson)) { ?>
	    <div class="col-sm-12">
	        <?= $form->field($section, 'contactPerson')
	            ->textInput(['placeholder' => $section->getAttributeLabel('contactPerson')])
	            ->label(false) 
	        ?>
	    </div>
    <?php } ?>
    <div class="col-sm-12">
        <?= $form->field($section, 'clientCompanyParent')
            ->textInput(['placeholder' => $section->getAttributeLabel('clientCompanyParent')])
            ->label(false) ?>
    </div>
    <div class="col-sm-12">
        <?= $form->field($section, 'clientCompany')
            ->textInput(['placeholder' => $section->getAttributeLabel('clientCompany')])
            ->label(false) ?>
    </div>
    <div class="col-sm-12">
        <?= $form->field($section, 'clientAddress')
            ->textInput(['placeholder' => $section->getAttributeLabel('clientAddress')])
            ->label(false) 
        ?>
    </div>
    <div class="col-sm-12">
        <?= $form->field($section, 'clientPhone')
            ->textInput(['placeholder' => $section->getAttributeLabel('clientPhone')])
            ->label(false) 
        ?>
    </div>
    <?php if(!empty($section->clientEmail)) { ?>
	    <div class="col-sm-12">
	        <?= $form->field($section, 'clientEmail')
	            ->textInput(['placeholder' => $section->getAttributeLabel('clientEmail')])
	            ->label(false) 
	        ?>
	    </div>
    <?php } ?>
</div>
<div class="col-sm-6 row">
	<div class="col-xs-12">
	   <strong><?= Yii::t('documents_orders', 'From')?>:</strong> 
	</div>
	<div class="col-xs-12 row">
	    <div class="col-sm-12">
	        <?= $form->field($section, 'authorCompany')
	            ->textInput(['placeholder' => $section->getAttributeLabel('authorCompany')])
	            ->label(false) 
	        ?>
	    </div>
	    <div class="col-sm-12">
	        <?= $form->field($section, 'authorAddress')
	            ->textInput(['placeholder' => $section->getAttributeLabel('authorAddress')])
	            ->label(false) 
	        ?>
	    </div>
	    <div class="col-sm-12">
	        <?= $form->field($section, 'authorSalesman')
	            ->textInput(['placeholder' => $section->getAttributeLabel('authorSalesman')])
	            ->label(false) ?>
	    </div>
	    <div class="col-sm-12">
	        <?= $form->field($section, 'authorPosition')
	            ->textInput(['placeholder' => $section->getAttributeLabel('authorPosition')])
	            ->label(false) ?>
	    </div>
	    <div class="col-sm-12">
	        <?= $form->field($section, 'authorPhone')
	            ->textInput(['placeholder' => $section->getAttributeLabel('authorPhone')])
	            ->label(false) ?>
	    </div>
	    <div class="col-sm-12">
	        <?= $form->field($section, 'authorFax')
	            ->textInput(['placeholder' => $section->getAttributeLabel('authorFax')])
	            ->label(false) ?>
	    </div>
	    <div class="col-sm-12">
	        <?= $form->field($section, 'authorEmail')
	            ->textInput(['placeholder' => $section->getAttributeLabel('authorEmail')])
	            ->label(false) ?>
	    </div>
	</div>
</div>


<div class="col-xs-12 row">
	<div class="col-sm-7"><?= $form->field($section, 'title') ?></div>
</div>

<div class="col-xs-12 row">
	<div class="col-sm-7">
	    <?= $form->field($section, 'description')
	        ->textarea(['placeholder' => $section->getAttributeLabel('description')]) 
	        
	    ?>
    </div>
</div>

<div class="col-xs-12">
	<?php 
		echo $form->field($section,'companyId')->hiddenInput()->label(false);
		echo $form->field($section,'orderId')->hiddenInput()->label(false);
	?>
</div>

