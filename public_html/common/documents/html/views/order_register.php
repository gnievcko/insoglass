<?php 
use Yii;
use common\helpers\StringHelper;
use frontend\helpers\AddressHelper;
use common\helpers\Utility;

$this->title = $section->getAttributeLabel('order register').' - '.Yii::$app->name;
?>

<div class="text-center"><h1><?= $section->getAttributeLabel('order register') ?></h1></div>

<div class="text-center">
	<?= $section->getAttributeLabel('forPeriod') . ': ' .  
	$section->dateFrom . ' - ' .
	$section->dateTo ?>
</div>

<div class="text-center"><?= $section->getAttributeLabel('generatedFor') . ': ' . $section->user ?></div>

<div class="text-center"><?= $section->getAttributeLabel('generationDate') . ': ' . $section->dateGenerated ?></div>

<br>

<?php 
$lp = 1;
if(!empty($section->tableData)) {?>
	<div class="table-responsive">
		<table class="table">
			<tr>
		    	<th><?= $section->getAttributeLabel('Id') ?></th>
		    	<th><?= $section->getAttributeLabel('orderDate') ?></th>
		    	<th><?= $section->getAttributeLabel('partyOrdering') ?></th>
		    	<th><?= $section->getAttributeLabel('object') ?></th>
		    	<?php if(Yii::$app->params['isOrderTypeVisible']) { ?>
		    		<th><?= $section->getAttributeLabel('typeOfWork') ?></th>
		    	<?php } ?>
		    	<?php if(Yii::$app->params['isContractTypeVisible']) { ?>
		    		<th><?= $section->getAttributeLabel('typeOfContract') ?></th>
		    	<?php } ?>
		  	</tr>
		  	
			<?php foreach($section->tableData as $row) {
				if(empty($section->invoiceData[$row['id']])) {
					continue;
				}
				
				$company = $row['company'];
				$address1 = AddressHelper::getFullAddress([
		            		'addressMain' => $row['a1Main'],
		                    'addressComplement' => $row['a1Complement'],
		                    'cityName' => $row['a1City'],
		                    'cityZipCode' => $row['a1Zip_code'],
					]);
				if(!empty($address1)) {
					$company .= ', ' . $address1;
				}
				
				$parent = null;
				if(!empty($row['parent'])) {
					$parent = $row['parent'];
					$address2 = AddressHelper::getFullAddress([
		            		'addressMain' => $row['a2Main'],
		            		'addressComplement' => $row['a2Complement'],
		            		'cityName' => $row['a2City'],
		            		'cityZipCode' => $row['a2Zip_code'],
					]);
					if(!empty($address1)) {
						$parent .= ', ' . $address2;
					}
				}		
				?>
		  		<tr>
		  			<td><?= $lp ?></td>
		    		<td><?= $section->invoiceData[$row['id']] ? StringHelper::getFormattedDateFromDate($section->invoiceData[$row['id']]) : '-' ?></td>
		    		<td><?= $company ?></td>
		    		<td><?= !empty($row['parent']) ? $parent : $company ?></td>
		    		<?php if(Yii::$app->params['isOrderTypeVisible']) { ?>
		    			<td><?= $row['orderTypeNames'] ?></td>
		    		<?php } ?>
		    		<?php if(Yii::$app->params['isContractTypeVisible']) { ?>
		    			<td><?= $row['orderTypeSymbols'] == Utility::ORDER_TYPE_MALFUNCTION_FIXING ? Yii::t('web', 'Reporting malfunction') : $row['contract_type'] ?></td>
		    		<?php } ?>
		  		</tr>
		  	<?php
				$lp++;
			 }?>
		</table>
	</div>
<?php }
else {
	echo $section->getAttributeLabel('none');
}?>