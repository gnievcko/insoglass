<div class="row">
	<h4 class="text-center"><?= Yii::t('documents', 'Terms and conditions') ?></h4>

	<ol style="text-align: justify;">
		<li><?= Yii::t('documents', 'Above order with the signature of both sides counts as a sales agrement between the Client and NoteDeco.') ?></li>
		<li><?= Yii::t('documents', 'The buyer confirms that they are VAT registered payer and allows NoteDeco to draw up a VAT document without buyer\'s signature.') ?></li>
		<li><?= Yii::t('documents', 'Terms of payment according to the order. All unpaid depts will be subject of areas of interest.') ?></li>
		<li><?= Yii::t('documents', 'Once the order\'s been sign it can be cancel only after the writting agrement.') ?></li>
		<li><?= Yii::t('documents', 'NoteDeco is obligated to proceed with the order. Any delays will be treated as a subject of areas of interest.') ?></li>
		<li><?= Yii::t('documents', 'This document is not an invoice or receipt.') ?></li>
	</ol>

	<p class="text-center" style="margin-top: 50px"><?= Yii::t('documents', 'I DO CONFIRM THAT I HAVE READ AND UNDERSTOOD THE TERMS AND CONDITIONS OF THE ORDER CONFIRMATION.') ?></p>
</div>

<div class="row text-center">
	<div class="col-sm-6">
		<br />
		<div>....................................................</div>
		<div><?= Yii::t('documents', 'Buyer') ?> (<?= Yii::t('documents', 'signature and stamp') ?>)</div>
	</div>
	<div class="col-sm-6">
		<br />
		<div>....................................................</div>
		<div><?= Yii::t('documents', 'Representative of seller') ?></div>
	</div>

</div>