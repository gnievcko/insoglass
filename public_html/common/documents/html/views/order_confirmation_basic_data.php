<?php

use kartik\datecontrol\DateControl;
use Mindseater\PHPUtils\Dates\Formatter;

$this->title = \Yii::t ( 'documents', 'Order confirmation' ) . ' - ' . Yii::$app->name;
?>

<div class="text-center">
	<h3><?= \Yii::t('documents', 'Order confirmation') ?></h3>
	<div class="row">
		<div class="col-sm-offset-3 col-sm-2">
			<h4><span><?= Yii::t('documents_orders', 'No.') ?></span> <span><?= $section->number?></span>
             <?= $form->field($section, 'number')->hiddenInput()->label(false) ?></h4>
        </div>
		<div class="col-sm-1">
			<h4><?= $section->getAttributeLabel('dateCreation')?></h4>
		</div>

		<div class="col-sm-3">
            <?=$form->field ( $section, 'dateCreation' )->textInput ( [ 'placeholder' => $section->getAttributeLabel ( 'dateCreation' ) ] )->label ( false );?>
        </div>
        <div class="col-sm-offset-1 col-sm-1">
			<?= $form->field($section, "languageId")->dropDownList($section->getLanguages())->label(false) ?>
		</div>
	</div>
</div>
<div class="row">
	<h4><?= Yii::t('documents', 'Client details')?></h4>
</div>
<div class="row">
	<div class="col-sm-6 row">
		<div class="col-sm-12">
		    <?=$form->field ( $section, 'clientCompany' )->textInput ( [ 'placeholder' => $section->getAttributeLabel ( 'clientCompany' ) ] )->label ( $section->getAttributeLabel ( 'clientCompany' ))?>
		</div>
		<div class="col-sm-12">
	        <?=$form->field ( $section, 'clientAddress' )->textInput ( [ 'placeholder' => $section->getAttributeLabel ( 'clientAddress' ) ] )->label ( $section->getAttributeLabel ( 'clientAddress' ))?>
	    </div>
		<div class="col-sm-12">
	        <?=$form->field ( $section, 'clientPhone' )->textInput ( [ 'placeholder' => $section->getAttributeLabel ( 'clientPhoneFull' ) ] )->label ( $section->getAttributeLabel ( 'clientPhoneFull' ))?>
	    </div>
	    <div class="col-sm-12">
	        <?=$form->field ( $section, 'clientFax' )->textInput ( [ 'placeholder' => $section->getAttributeLabel ( 'clientFax' ) ] )->label ( $section->getAttributeLabel ( 'clientFax' ))?>
	    </div>
    	<?php if(!empty($section->clientEmail)) { ?>
		    <div class="col-sm-12">
		        <?=$form->field ( $section, 'clientEmail' )->textInput ( [ 'placeholder' => $section->getAttributeLabel ( 'clientEmail' ) ] )->label ( $section->getAttributeLabel ( 'clientEmail' ))?>
		    </div>
    	<?php } ?>
    	<div class="col-sm-12">
	        <?=$form->field ( $section, 'payment_terms' )->textInput ( [ 'placeholder' => $section->getAttributeLabel ( 'payment_terms' ) ] )->label ( $section->getAttributeLabel ( 'payment_terms' ))?>
	    </div>
	</div>

	
	<div class="col-sm-6 row">
		<div class="col-sm-12">
			<?=$form->field ( $section, 'contactPeople' )->textarea ( [ 'placeholder' => $section->getAttributeLabel ( 'contactPeople' ) ] )->label ( $section->getAttributeLabel ( 'contactPeople' ));?>
		</div>
		<div class="col-sm-12">
			<?=$form->field ( $section, 'shippingAndHandling' )->textInput( [ 'placeholder' => $section->getAttributeLabel ( 'shippingAndHandling' ) ] )->label ( $section->getAttributeLabel ( 'shippingAndHandling' ));?>
		</div>
		<div class="col-sm-12">
		<?= 	$form->field($section, 'shippingDate')->widget(DateControl::classname(), [
					'language' => \Yii::$app->language,
					'type' => DateControl::FORMAT_DATE,
					'ajaxConversion' => false,
					'displayFormat' => 'php:d.m.Y',
					'saveFormat' => 'php:Y-m-d',
					'options' => [
							'id' => 'orderConfirmationId',
							'pluginOptions' => [
									'autoclose' => true
							]
					]
		]);
		?>
		</div>
		<div class="col-sm-12">
	        <?=$form->field ( $section, 'deliveryAddress' )->textInput ( [ 'placeholder' => $section->getAttributeLabel ( 'deliveryAddress' ) ] )->label ( $section->getAttributeLabel ( 'deliveryAddress' ))?>
	   </div>
	</div>
</div>

<div class="row">
	<div class="col-sm-7">
		<?=$form->field ( $section, 'description' )->textarea ( [ 'placeholder' => $section->getAttributeLabel ( 'description' ) ] )->label ( $section->getAttributeLabel ( 'description' ));?>		
	</div>
	<div class="col-sm-offset-1 col-sm-3">
		<?= $form->field($section, 'authorFax')->textInput(['placeholder' => $section->getAttributeLabel('authorFax')])->label(Yii::t('documents', 'Please send back signed copy of this document') . '; ' . $section->getAttributeLabel('authorFax'))?>
	</div>
</div>
<div class="col-xs-12">
	<?php 
		echo $form->field($section,'companyId')->hiddenInput()->label(false);
		echo $form->field($section,'orderId')->hiddenInput()->label(false);
	?>
</div>