<?php

$editMode = $section->isEditable();

$this->title = Yii::t('documents', 'Advance invoice').' - '.Yii::$app->name;

?>
<h2 class="text-center"><?php echo Yii::t('documents', 'Advance invoice')?></h2>

<div class="col-xs-12 row">
	<div class="col-sm-7">		
	    <div>
	        <?= $editMode ? $form->field($section, 'number')->textInput(['readonly' => true]) : $section->getAttributeLabel('number').': '.$section->number ?> 
	    </div>
	    <div>
	        <?= $form->field($section, 'isCopy')->hiddenInput()->label(false) ?>
	        <?= $form->field($section, 'referencedInvoiceId')->hiddenInput()->label(false) ?>
			<?= $form->field($section, 'referencedInvoiceHistoryId')->hiddenInput()->label(false) ?>
	    </div>		
	</div>
</div>

<?php require('invoice-title/invoice-title-content.php'); ?>