<?php
    use yii\helpers\Html;
    use common\helpers\StringHelper;

    $netValue = bcmul($product->price, $product->count, 2);
    $vatAmount = bcmul($netValue, $product->vatRate, 2);
    $grossValue = bcadd($netValue, $vatAmount, 2);

    $recalculateInputsOptions = ['class' => 'recalculates form-control'];
    $numericFieldsCommonOptions = ['type' => 'number'];
    $areaOptions = ['class' => 'form-control smallTexarea invoice-product-name'];
?>

<tr class="invoice-product">
    <td><?= $idx + 1 ?></td>
    <td><?= $edit ? $form->field($product, "[{$idx}]name")->textArea($areaOptions + ['readonly' => true, 'placeholder' => Yii::t('documents','Type name')])->label(false) : $product->name ?></td>
    <td><?= $edit ? $form->field($product, "[{$idx}]count")->textInput($recalculateInputsOptions + $numericFieldsCommonOptions + ['readonly' => true])->label(false) : $product->count ?></td>
    <td><?= $edit ? $form->field($product, "[{$idx}]unit")->textInput(['readonly' => true])->label(false) : $product->unit ?></td>
    <td><?= $edit ? $form->field($product, "[{$idx}]category")->textInput(['readonly' => true])->label(false) : $product->category ?></td>
    <td>
        <?= 
            $edit 
                ? $form->field($product, "[{$idx}]price")->textInput($recalculateInputsOptions + $numericFieldsCommonOptions + ['step' => '0.01', 'readonly' => true])->label(false) 
                : StringHelper::getFormattedCost($product->price).' '.$currencySymbol 
        ?>
    </td>
    <td>
    	<?= StringHelper::getFormattedCost($netValue).' '.$currencySymbol ?>
    	<?= $form->field($product, "[{$idx}]vatRate")->hiddenInput()->label(false); ?>
    </td>
    <td><?= $edit 
                ? ($form->field($product, "[{$idx}]vatRate")->dropDownList($product->getVatRates(), $recalculateInputsOptions + ['disabled' => true])->label(false))
                : (
                        ($product->vatRate === '') ? Yii::t('documents', 'Vat-free') :
                        (($product->vatRate === 'NP') ? Yii::t('documents', 'Not applicable') : bcmul($product->vatRate, 100, 0).' %')
                        
                 )
        ?>
    </td>	

    <td><?= StringHelper::getFormattedCost($vatAmount).' '.$currencySymbol ?></td>
    <td><?= StringHelper::getFormattedCost($grossValue).' '.$currencySymbol ?></td>
</tr>
