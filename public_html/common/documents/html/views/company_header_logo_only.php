<?php 
    use yii\helpers\Html;
?>

<div class="row">
    <div class="col-sm-6"> 
        <?= Html::img($section->companyLogo, ['style' => 'width: 200px']) ?>
    </div>
    <div class="col-sm-6 text-right"> 
        <?php if(!empty($section->companyCertificateLogo)) {
        	echo Html::img($section->companyCertificateLogo, ['style' => 'height: 130px']);
        } ?>
    </div>
</div>