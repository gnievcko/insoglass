<?php
    use common\helpers\StringHelper;

    $edit = $section->isEditable() && !empty(Yii::$app->params['isWarehouseDocumentProductsEditable']);
?>

<?php 
$i = 0;
foreach($section->products as $idx => $product): ?>
    <?= 
        Yii::$app->controller->renderFile(__DIR__.'/product-row.php', [
            'idx' => $i, 
            'product' => $product, 
            'edit' => $edit, 
        ] + (isset($form) ? ['form' => $form] : [])) 
    ?>
<?php 
$i++;
endforeach; 

$summary = $section->calculateSummary();
?>

<tr>
	<td class="bold"><?= $section->getAttributeLabel('total') ?></td>
	<td></td>
	<td class="bold"><?= $summary['count'] ?></td>
	<td></td>
	<td></td>
	<td></td>
	<td class="bold"><?= StringHelper::getFormattedCost($summary['price']).' '.$summary['currency']; ?></td>
</tr>