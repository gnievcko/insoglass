<?php

namespace common\documents\sections;
use common\models\aq\ProductionTaskQuery;



class ProductionProgressDetails extends Section{
    public $tasks;
    public $dateFrom;
    public $dateTo;

    public function init() {
    }

    public function rules() {
        return [
        ];
    }

    public function loadInput($input) {
        $this->dateFrom = $input['dateFrom'];
        $this->dateTo = $input['dateTo'];
        
        $myDateFrom = explode('-', $this->dateFrom);
        $myDateFrom = array_reverse($myDateFrom);
        $myDateFrom = implode('-', $myDateFrom);

        $myDateTo = explode('-', $this->dateTo);
        $myDateTo = array_reverse($myDateTo);
        $myDateTo = implode('-', $myDateTo);
        
        $this->tasks = ProductionTaskQuery::getTasksWithProblemsByPeriod($myDateFrom, $myDateTo);
    }

    public function getType() {
        return SectionsMapper::PRODUCTION_PROGRESS_DETAILS;
    }
}

