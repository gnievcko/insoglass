<?php

namespace common\documents\sections;

use Yii;
use common\documents\sections\SectionsMapper;
use frontend;
use common\models\ar\NumberTemplate;
use common\helpers\NumberTemplateHelper;
use common\helpers\Utility;

class GoodsIssuedTitle extends Section {

	public $number;
    public $dateOperation;
    public $dateIssue;
    public $warehouseName;
    public $userReceivingName;
    public $userConfirmingName;
    public $description;
    public $object;
    public $goesTo;
    public $isNumberPredefined;
    public $status;

    public function rules() {
        return [
            	[['number', 'dateOperation', 'dateIssue', 'userConfirmingName', 'userReceivingName', 'warehouseName', 'isNumberPredefined'], 'required', 'message' => Yii::t('documents', 'This field is required')],
            	[['description', 'object', 'goesTo', 'status'], 'safe'],
        ];
    }

    public function init() {
        $this->dateIssue = (new \DateTime())->format('d.m.Y H:i');
    }
    
    public function loadInput($input) {
    	parent::loadInput($input);
    	// TODO: Helper do przeniesienia
    	if(isset($input[$this->formName()]['userConfirmingEmail']) && isset($input[$this->formName()]['userConfirmingFirstName']) && 
    			isset($input[$this->formName()]['userConfirmingLastName'])) {
		    $this->userConfirmingName = frontend\helpers\UserHelper::getPrettyUserName(
		    		$input[$this->formName()]['userConfirmingEmail'], 
		    		$input[$this->formName()]['userConfirmingFirstName'], 
		    		$input[$this->formName()]['userConfirmingLastName']
			);
    	}
    	
    	if(isset($input[$this->formName()]['userReceivingEmail']) && isset($input[$this->formName()]['userReceivingFirstName']) &&
    			isset($input[$this->formName()]['userReceivingLastName'])) {
    		$this->userReceivingName = frontend\helpers\UserHelper::getPrettyUserName(
    				$input[$this->formName()]['userReceivingEmail'],
    				$input[$this->formName()]['userReceivingFirstName'],
    				$input[$this->formName()]['userReceivingLastName']
    		);
    	}
    	
    	//$this->number = NumberTemplateHelper::getNextNumber(Utility::NUMBER_TEMPLATE_GOODS_ISSUE);
    }

    public function attributeLabels() {
        return [
        		'number' => Yii::t('documents', 'Issue number'),
	            'dateIssue' => Yii::t('documents', 'Date of issue'),
	            'dateOperation' => Yii::t('documents', 'Date of operation'),
        		'warehouseName' => Yii::t('documents', 'Warehouse'),
			    'userReceivingName' => Yii::t('documents', 'Receiving user'),
			    'userConfirmingName' => Yii::t('documents', 'Issuing user'),
			    'description' => Yii::t('documents', 'Remarks'),
        		'object' => Yii::t('documents', 'Object'),
        		'goesTo' => Yii::t('documents', 'Goes to'),
        		'status' => Yii::t('documents', 'Status'),
        ];
    }

    public function getType() {
        return SectionsMapper::GOODS_ISSUED_TITLE;
    }
}