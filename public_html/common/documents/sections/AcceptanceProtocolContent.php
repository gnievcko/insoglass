<?php

namespace common\documents\sections;

use Yii;
use common\documents\sections\SectionsMapper;

class AcceptanceProtocolContent extends Section {

    public $orderingParty;
    public $orderingPartySupervisingBoard = [];
    public $object;
    public $orderDescription;
    public $works;
    public $worksBeginDate;
    public $worksEndDate;
    public $executor;
    public $executorSupervisingBoard = [];
    public $supervisingBoard;
    public $remarks;
    public $wasExecutedInComplianceWithOrder;
    public $hasFlaws;
    public $flawsDescription;
    public $flawsRemovalDate;
    public $orderId;
    public $companyId;

    public function rules() {
        return [
            [['orderId', 'companyId', 'orderingParty', 'object', 'orderDescription', 'executor', 'works', 
              'worksBeginDate', 'worksEndDate', 'wasExecutedInComplianceWithOrder', 'hasFlaws', 
              'flawsDescription', 'flawsRemovalDate', 'orderingPartySupervisingBoard', 
              'executorSupervisingBoard', 'remarks'], 'safe'],
        ];
    }
    
    public function loadInput($input) {
    	parent::loadInput($input);
        $orderId = Yii::$app->request->get('orderId');
        if(isset($orderId)) {
            $order = \common\models\ar\Order::findOne(['id' => $orderId]);
            if(!empty($order)) {
                $this->orderId = $order->id;
                $this->companyId = $order->company->id;
            }
        }
    }
    
    public function init() {
        $config = Yii::$app->params;
        $this->executor = "{$config['owner']['name']} {$config['owner']['address']}";
    }

    public function attributeLabels() {
        return [
            'orderingParty' => Yii::t('documents', 'Ordering party'),
            'object' => Yii::t('documents', 'Object'),
            'orderDescription' => Yii::t('documents', 'Description of order and executed works'),
            'works' => Yii::t('documents', 'Works'),
            'executor' => Yii::t('documents', 'Executor'),
            'supervisingBoard' => Yii::t('documents', 'Supervising board'),
            'remarks' => Yii::t('documents', 'Remarks'),
            'worksBeginDate' => Yii::t('documents', 'Works begin date'),
            'worksEndDate' => Yii::t('documents', 'Works end date'),
            'wasExecutedInComplianceWithOrder' => Yii::t('documents', 'Was executed with compliance with order'),
            'hasFlaws' => Yii::t('documents', 'Has flaws'),
            'flawsDescription' => Yii::t('documents', 'Flaws description'),
            'flawsRemovalDate' => Yii::t('documents', 'Proposed date of flaws removal'),
        ];
    }

    public function getType() {
        return SectionsMapper::ACCEPTANCE_PROTOCOL_CONTENT;
    }
}
