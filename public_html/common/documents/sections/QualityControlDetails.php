<?php

namespace common\documents\sections;
use common\models\aq\ProductionTaskQuery;
use Yii;

class QualityControlDetails extends Section{
    public $tasks;
    public $dateFrom;
    public $dateTo;

    public function init() {
    }

    public function rules() {
        return [
        ];
    }


    public function attributeLabels() {
        return [
            'none' => Yii::t('web', 'None'),
        ];
    }


    public function loadInput($input) {
        $this->dateFrom = $input['dateFrom'];
        $this->dateTo = $input['dateTo'];

        $myDateFrom = explode("-", $this->dateFrom);
        $myDateFrom = array_reverse($myDateFrom);
        $myDateFrom = implode("-", $myDateFrom);

        $myDateTo = explode("-", $this->dateTo);
        $myDateTo = array_reverse($myDateTo);
        $myDateTo = implode("-", $myDateTo);

        $_tasks = ProductionTaskQuery::getProductsWithQFByDate($myDateFrom, $myDateTo);

        $this->tasks = array();

        foreach($_tasks as $item) {
            $this->tasks[$item['orderId']][] = $item;
        }
    }

    public function getType() {
        return SectionsMapper::QUALITY_CONTROL_DETAILS;
    }
}

