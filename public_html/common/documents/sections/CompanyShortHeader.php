<?php

namespace common\documents\sections;

use Yii;
use common\documents\sections\SectionsMapper;
use common\models\ar\Order;
use common\helpers\ArrayHelper;
use common\models\aq\EntityQuery;

class CompanyShortHeader extends Section {

    public $companyLogo;
    public $companyCertificateLogo;
    public $companyFullName;
    public $companyAddress;

    public function rules() {
        return [
        		[['companyLogo', 'companyCertificateLogo', 'companyFullName', 'companyAddress'], 'safe'],
        ];
    }

    public function init() {
    	$config = Yii::$app->params;
        $this->companyLogo = $config['owner']['logo'];
        $this->companyCertificateLogo = $config['owner']['certificateLogo'];
        $this->companyFullName = $config['owner']['name'];
        $this->companyAddress = $config['owner']['address'];
        
        if(!empty(Yii::$app->request->get('orderId'))) {
        	$order = Order::findOne(Yii::$app->request->get('orderId'));
        	if(!empty($order) && !empty($order->entity)) {
        		$this->companyFullName = $order->entity->name;
        		$this->companyAddress = $order->entity->address->getFullAddress(true);
				if(!empty($order->entity->url_logo)) {
    				$this->companyLogo = '@web/images/'.$order->entity->url_logo;
    			}
    			 
    			if(empty($order->entity->is_certified)) {
    				$this->companyCertificateLogo = '';
    			}
        	}
        }
    }
    
    public function loadInput($input) {
		parent::loadInput($input);
		
    	$orderId = ArrayHelper::searchArrayForKey($input, 'orderId');
    	if(!empty($orderId)) {
    		$order = Order::findOne($orderId);
    		if(!empty($order)) {
    			$entity = $order->entity;
    			if(empty($entity)) {
    				$entity = EntityQuery::getDefaultEntity();
    			}
    			 
    			if(!empty($entity) && !empty($entity->url_logo)) {
    				$this->companyLogo = '@web/images/'.$entity->url_logo;
    			}
    			 
    			if(!empty($entity) && empty($entity->is_certified)) {
    				$this->companyCertificateLogo = '';
    			}
    		}
    	}
    }

    public function attributeLabels() {
        return [];
    }

    public function getType() {
        return SectionsMapper::COMPANY_SHORT_HEADER;
    }
}
