<?php
namespace common\documents\sections;

use Yii;
use common\helpers\NumberTemplateHelper;
use common\helpers\Utility;

class CorrectiveInvoiceTitle extends InvoiceTitle {

	public $referencedInvoiceId;
	public $referencedInvoiceHistoryId;
	public $referencedInvoiceNumber;
	public $referencedInvoiceDateOfIssue;
	
	public function loadInput($input) {
		parent::loadInput($input);
		
		$invoiceTitleFormName = (new InvoiceTitle())->formName();
		if(!isset($input[$invoiceTitleFormName]['referencedInvoiceId'])) {
			if($this instanceof DuplicateCorrInvoiceTitle) {
				$invoiceTitleFormName = (new DuplicateCorrInvoiceTitle())->formName();
			}
			else {
				$invoiceTitleFormName = (new CorrectiveInvoiceTitle())->formName();
			}
		}
		
		$this->referencedInvoiceId = $input[$invoiceTitleFormName]['referencedInvoiceId'];
		$this->referencedInvoiceHistoryId = $input[$invoiceTitleFormName]['referencedInvoiceHistoryId'];
		$this->referencedInvoiceNumber = $input[$invoiceTitleFormName]['referencedInvoiceNumber'];
		if(isset($input[$invoiceTitleFormName]['referencedInvoiceDateOfIssue'])) {
			$this->referencedInvoiceDateOfIssue = $input[$invoiceTitleFormName]['referencedInvoiceDateOfIssue'];
		}
		if(isset($input[$invoiceTitleFormName]['number'])) {
			//$this->number = $input[$invoiceTitleFormName]['number'];
		}
		
		$this->dateOfIssue = $input[$invoiceTitleFormName]['dateOfIssue'];
		$this->dateOfSale = $input[$invoiceTitleFormName]['dateOfSale'];
		
		if(empty($this->dateOfIssue)) {
			$this->dateOfIssue = (new \DateTime())->format('Y-m-d');
		}
	}
	
	public function validateDateOfIssue() {
		
	}
	
	public function attributeLabels() {
		$labels = parent::attributeLabels();
		$labels['number'] = Yii::t('documents', 'Corrective invoice number');
		$labels['referencedToInvoice'] = Yii::t('documents', 'Concerns invoice');
		$labels['referencedInvoiceDateOfIssue'] = Yii::t('documents', 'From');
		$labels['dateOfIssue'] = Yii::t('documents', 'Correction\'s date of issue');
		
		return $labels;
	}
	
	protected function getNextNumber() {
		//return $this->referencedInvoiceNumber;
		
		$entityId = $this->entityId;
		if(empty($entityId) && !empty($this->orderId)) {
			$order = Order::findOne($this->orderId);
			$entityId = !empty($order) ? $order->entity_id : null;
		}
		
		return NumberTemplateHelper::getNextNumber(Utility::NUMBER_TEMPLATE_CORRECTIVE_INVOICE, $entityId);
	}
	
	public function getType() {
		return SectionsMapper::CORRECTIVE_INVOICE_TITLE;
	}
}
