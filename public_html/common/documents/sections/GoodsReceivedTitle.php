<?php

namespace common\documents\sections;

use Yii;
use common\documents\sections\SectionsMapper;
use common\helpers\UserNameHelper;
use frontend;

class GoodsReceivedTitle extends Section {

    public $number;
    public $dateDelivery;
    public $dateIssue;
    public $warehouseName;
    public $supplierName;
    public $userConfirmingName;
    public $dateReception;
    public $description;
    public $issueId;
    public $issueNumber;

    public function rules() {
        return [
            	[['number', 'dateIssue', 'dateDelivery', 'dateReception', 'userConfirmingName', 'warehouseName'], 'required', 'message' => Yii::t('documents', 'This field is required')],
            	[['description', 'issueId', 'issueNumber', 'supplierName'], 'safe'],
        ];
    }

    public function init() {
        $this->dateIssue = (new \DateTime())->format('d.m.Y H:i');
    }
    
    public function loadInput($input) {
    	parent::loadInput($input);
    	// TODO: Helper do przeniesienia
    	if(isset($input[$this->formName()]['userEmail']) && isset($input[$this->formName()]['userFirstName']) && 
    			isset($input[$this->formName()]['userLastName'])) {
		    $this->userConfirmingName = frontend\helpers\UserHelper::getPrettyUserName(
		    		$input[$this->formName()]['userEmail'], 
		    		$input[$this->formName()]['userFirstName'], 
		    		$input[$this->formName()]['userLastName']
			);
    	}
    }

    public function attributeLabels() {
        return [
	            'number' => Yii::t('documents', 'Delivery number'),
	            'dateIssue' => Yii::t('documents', 'Date of issue'),
	            'dateDelivery' => Yii::t('documents', 'Date of delivery'),
        		'warehouseName' => Yii::t('documents', 'Warehouse'),
			    'supplierName' => Yii::t('documents', 'Supplier'),
			    'userConfirmingName' => Yii::t('documents', 'Receiving user'),
			    'dateReception' => Yii::t('documents', 'Date of reception'),
			    'description' => Yii::t('documents', 'Remarks'),
        ];
    }

    public function getType() {
        return SectionsMapper::GOODS_RECEIVED_TITLE;
    }
}
