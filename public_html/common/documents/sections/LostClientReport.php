<?php

namespace common\documents\sections;

use Yii;
use common\documents\sections\SectionsMapper;
use frontend\helpers\UserHelper;
use common\helpers\StringHelper;
use common\models\ar\User;
use common\models\aq\DocumentQuery;

class LostClientReport extends Section {

    public $dateFrom;
    public $dateTo;
    public $compareDateFrom;
    public $compareDateTo;
    public $user;
    public $dateGenerated;
    public $tableData;

    public function rules() {
        return [
        ];
    }

    public function init() {
        $request = Yii::$app->request->post();
        
        $pieces = explode('.', $request['LostClientForm']['dateTo']);
        $this->dateTo = $pieces[2] . '-' . $pieces[1] . '-' . $pieces[0];
        $pieces = explode('.', $request['LostClientForm']['dateFrom']);
        $this->dateFrom = $pieces[2] . '-' . $pieces[1] . '-' . $pieces[0];

        $pieces = explode('.', $request['LostClientForm']['compareDateTo']);
        $this->compareDateTo = $pieces[2] . '-' . $pieces[1] . '-' . $pieces[0];
        $pieces = explode('.', $request['LostClientForm']['compareDateFrom']);
        $this->compareDateFrom = $pieces[2] . '-' . $pieces[1] . '-' . $pieces[0];

        $this->tableData = DocumentQuery::getLostClients($this->dateFrom, $this->dateTo, $this->compareDateFrom, $this->compareDateTo);

        $user = User::findOne(Yii::$app->user->id);
        $this->user = UserHelper::getPrettyUserName($user->email, $user->first_name, $user->last_name);
        $this->dateGenerated = StringHelper::getFormattedDateFromTimestamp(time(), true);

        $this->dateTo = $this->dateTo . ' 23:59:59';
        $this->dateFrom = $this->dateFrom . ' 00:00:00';
        
        $this->compareDateTo = $this->compareDateTo . ' 23:59:59';
        $this->compareDateFrom = $this->compareDateFrom . ' 00:00:00';
        
        $this->dateFrom = StringHelper::getFormattedDateFromDate($this->dateFrom);
        $this->dateTo = StringHelper::getFormattedDateFromDate($this->dateTo);
        $this->compareDateFrom = StringHelper::getFormattedDateFromDate($this->compareDateFrom);
        $this->compareDateTo = StringHelper::getFormattedDateFromDate($this->compareDateTo);
    }

    public function attributeLabels() {
        return [
	            'lostClientReport' => Yii::t('documents', 'REPORT - LOST CLIENTS'),
	            'Id' => Yii::t('documents', 'Id'),
	            'user' => Yii::t('main', 'User'),
	            'forFirstPeriod' => Yii::t('web', 'Source period'),
	        	'forSecondPeriod' => Yii::t('web', 'Comparative period'),
	            'generatedFor' => Yii::t('web', 'Generated for'),
	            'generationDate' => Yii::t('web', 'Generation date'),
	            'none' => Yii::t('web', 'None'),
	            'company' => Yii::t('main', 'Company'),
	            'numberOfInvoice' => Yii::t('web', 'Number of invoices'),
	            'totalPrice' => Yii::t('web', 'Total price'),
        ];
    }

    public function getType() {
        return SectionsMapper::LOST_CLIENT;
    }

}
