<?php

namespace common\documents\sections;

use Yii;
use common\documents\sections\SectionsMapper;
use frontend\helpers\UserHelper;
use common\helpers\StringHelper;
use common\models\ar\User;
use common\models\aq\OrderQuery;
use common\models\aq\DocumentQuery;
use common\helpers\ArrayHelper;
use frontend\helpers\AddressHelper;
use common\models\ar\Currency;
use common\helpers\Utility;

class ReminderReport extends Section {
	
	public $dateFrom;
	public $dateTo;
	public $user;
	public $dateGenerated;

	public $data;

    public function init() {
    	$piecesTo = explode( '.', $_GET['dateTo']);
    	$this->dateTo = $piecesTo[2] . '-' . $piecesTo[1] . '-' .$piecesTo[0] . ' 23:59:59';
    	$piecesFrom = explode( '.', $_GET['dateFrom']);
    	$this->dateFrom = $piecesFrom[2] . '-' . $piecesFrom[1] . '-' .$piecesFrom[0] . ' 00:00:00';
    	
    	$this->data = $this->fetchData();
    	
        $user = User::findOne(Yii::$app->user->id);
    	$this->user = UserHelper::getPrettyUserName($user->email, $user->first_name, $user->last_name);
    	$this->dateGenerated = StringHelper::getFormattedDateFromTimestamp(time(), true);
    	
    	$this->dateFrom = StringHelper::getFormattedDateFromDate($this->dateFrom);
    	$this->dateTo = StringHelper::getFormattedDateFromDate($this->dateTo);
    }
    
    public function fetchData() {
    	$userId = Yii::$app->user->id;
    	if(Yii::$app->user->can(Utility::ROLE_MAIN_SALESMAN) || Yii::$app->user->can(Utility::ROLE_MAIN_STOREKEEPER)) {
    		$userId = null;	
    	}
    	
    	$orders = OrderQuery::getOrdersWithReminder($this->dateFrom, $this->dateTo, $userId);
    	$invoices = DocumentQuery::getInvoicesByOrderIds(array_column($orders, 'id'));    	
    	
    	$data = [];
    	foreach($orders as $order) {
    		$row = [];
    		$invoice = [];
    		foreach($invoices as $inv) {
    			if(intval($inv['orderId']) == intval($order['id'])) {
    				$invoice = $inv;
    				break;
    			}
    		}
    		
    		if(empty($invoice)) {
    			continue;
    		}
    		
    		$address = AddressHelper::getFullAddress([
            		'addressMain' => $order['addressMain'],
                    'addressComplement' => $order['addressComplement'],
                    'cityName' => $order['cityName'],
                    'cityZipCode' => $order['zipCode'],
			]);

			$row['invoiceNumber'] = $invoice['number'];
			$row['companyName'] = $order['companyName'];
			$row['companyAddress'] = $address;
			$row['orderType'] = $order['orderType'];
			$row['invoiceAuthor'] = '';
			$row['invoicePrice'] = '';
			
			$author = User::findOne($invoice['authorId']);
			if(!empty($author)) {
				$row['invoiceAuthor'] = UserHelper::getPrettyUserName($author->email, $author->first_name, $author->last_name);
			}
			
			$sum = 0;
			if(isset($invoice['products'])) {
				foreach($invoice['products'] as $product) {
					$sum += intval($product['count']) * floatval($product['price']);
				}
			}
			
			$plnCurrency = Currency::find()->where(['symbol' => Utility::CURRENCY_PLN])->one();
			$row['invoicePrice'] = StringHelper::getFormattedCost($sum).' '.$plnCurrency->short_symbol;
			$data[] = $row;
    	}
    	
    	return $data;
    }

    public function attributeLabels() {
        return [
        		'invoiceNumber' => Yii::t('documents', 'Number of invoice'),
        		'clientName' => Yii::t('web', 'Client'),
        		'clientAddress' => Yii::t('web', 'Client address'),
        		'orderType' => StringHelper::translateOrderToOffer('web', 'Order type'),
        		'invoiceAuthor' => Yii::t('documents', 'Person issuing'),
        		'invoicePrice' => Yii::t('documents', 'Invoice net value'),
        		
        		'reminderReport' => Yii::t('web', 'Reminder report'),
        		'user' => Yii::t('main', 'User'),
        		'forPeriod' => Yii::t('web', 'For period'),
        		'generatedFor' => Yii::t('web', 'Generated for'),
        		'generationDate' => Yii::t('web', 'Generation date'),
        		'none' => Yii::t('web', 'None'),
        ];
    }


    public function getType() {
        return SectionsMapper::REMINDER_REPORT;
    }
}
