<?php

namespace common\documents\sections;

use Yii;
use common\documents\sections\SectionsMapper;
use frontend\helpers\ModelLoaderHelper;
use common\documents\forms\InvoiceProductForm;
use common\models\ar\Currency;
use yii\helpers\ArrayHelper;

class InvoiceProducts extends Section {

    public $currencyId;
    public $amountVerbally = '';
    public $products = [];

    private $currencies = [];

    public function loadInput($data) { 
    	$this->load($data);
    	$classes = $this->getInvoiceProductFormClass();
    	$invoiceProductFormClass = $classes['formName'];
    	
    	if(isset($data[$this->formName()])) {
       		if(isset($data[$this->formName()]['products'])) {
       			$data[$invoiceProductFormClass] = $data[$this->formName()]['products'];
           	}
       	}
       	
       	if(isset($data[$invoiceProductFormClass]) && !empty($invoiceProductFormClass)) {
       		$this->products = ModelLoaderHelper::loadMultiple($data, $classes['class']);
       		foreach($this->products as &$product) {
       			if($product->vatRate > 1) {
       				$product->vatRate /= 100;
       			}
       		}
       	}
    }
    
    protected function getInvoiceProductFormClass() {
    	return [
    			'formName' => (new InvoiceProductForm())->formName(),
    			'class' => InvoiceProductForm::class,
    	];
    }

    public function getModels() {
        return [$this->products, $this];
    }

    public function rules() {
        return [
            [['currencyId'], 'required'],
            [['products', 'amountVerbally'], 'safe']
        ];
    }

    public function init() {
        $config = Yii::$app->params;

        $currencies = Currency::find()->all();
        $this->currencies = ArrayHelper::map($currencies, 'id', 'symbol');
        $this->currencyId = empty($currencies) ? null : reset($currencies)->id;
    }

    public function getCurrencies() {
        return $this->currencies;
    }

    public function getCurrencySymbol() {
        return empty($this->currencies) ? '' : $this->currencies[$this->currencyId];
    }

    public function attributeLabels() {
        return [
            	'no' => Yii::t('documents', 'No.'),
            	'name' => Yii::t('main', 'Name'),
            	'count' => Yii::t('web', 'Amount'),
            	'unit' => Yii::t('documents', 'M.u.'),
        		'category' => Yii::t('web', 'PKWiU'),
            	'netPrice' => Yii::t('documents', 'Net price'),
            	'netValue' => Yii::t('documents', 'Net value'),
            	'vatRate' => Yii::t('documents', 'Vat rate'),
            	'vatAmount' => Yii::t('documents', 'Vat amount'),
            	'grossValue' => Yii::t('documents', 'Gross value'),
            	'amountVerbally' => Yii::t('documents', 'Verbally'),
            	'currencyId' => Yii::t('main', 'Currency'),
        ];
    }

    public function calculateSummary() {
    	$vats = [
            '' => $this->makeVatRow(Yii::t('documents', 'Vat-free'), '0'),
            'NP' => $this->makeVatRow(Yii::t('documents', 'Not applicable'), '0'),
            '0.23' => $this->makeVatRow('23 %', '0.23'),
            '0.08' => $this->makeVatRow('8 %', '0.08'),
            '0' => $this->makeVatRow('0 %', '0'),
        ];
    	
        foreach($this->products as $product) {
            if(isset($vats[(string)$product->vatRate])) {
                $vat = &$vats[(string)$product->vatRate];
                $vat['netValue'] = bcadd($vat['netValue'], bcmul($product->price, $product->count, 2), 2);
            }
        }

        $total = ['netValue' => '0.00', 'vatAmount' => '0.00', 'grossValue' => '0.00'];
        foreach($vats as &$vat) {
            $vat['vatAmount'] = bcmul($vat['netValue'], $vat['vatRate'], 2);
            $vat['grossValue'] = bcadd($vat['netValue'], $vat['vatAmount'], 2);

            $total = [
                'netValue' => bcadd($total['netValue'], $vat['netValue'], 2), 
                'vatAmount' => bcadd($total['vatAmount'], $vat['vatAmount'], 2), 
                'grossValue' => bcadd($total['grossValue'], $vat['grossValue'], 2), 
            ];
        }
		
        $amount = explode('.', $total['grossValue']);
        
        $f = new \NumberFormatter("pl", \NumberFormatter::SPELLOUT);
        $this->amountVerbally =  $f->format($amount[0]) . ' ' .
        Yii::t('web', '{n,plural,=0{zloty} =1{zloty} other{zloty}}', ['n' => $amount[0]]) . ' ' .
        $f->format($amount[1]) . ' ' . 
        Yii::t('web', '{n,plural,=0{groszy} =1{groszy} other{groszy}}', ['n' => $amount[1]]);        
        
        return ['vats' => $vats, 'total' => $total];
    }

    protected function makeVatRow($label, $rate) {
        return ['vatLabel' => $label, 'vatRate' => $rate, 'netValue' => '0.00', 'vatAmount' => '0.00', 'grossValue' => '0.00'];
    }

    public function getType() {
        return SectionsMapper::INVOICE_PRODUCTS;
    }
}
