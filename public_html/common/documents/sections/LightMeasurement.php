<?php

namespace common\documents\sections;

use Yii;
use common\documents\forms\LightMeasurementForm;
use common\documents\sections\SectionsMapper;

class LightMeasurement extends Section {

    public $rows = [[]];
    public $form;
    public $date;
    public $object;
    public $repairer;
    public $measurementTool;
    public $preText;
    public $lightIntensityFormula;
    public $lightUniformityFormula;
    public $time;
    public $testDate;
    public $measurementDate;
    public $text;
    public $orderId;
    public $companyId;
    
    public function rules() {
        return [
                [['object', 'date', 'repairer', 'measurementTool', 'time', 'testDate', 'measurementDate', 'text', 
                  'rows', 'form', 'type', 'orderId', 'companyId', 'preText', 'lightIntensityFormula', 'lightUniformityFormula'], 'safe'],
        ];
    }

    public function loadInput($input) {
        $formName = (new LightMeasurementForm())->formName();
        $this->rows = isset($input[$formName]) ? $input[$formName] : $this->rows;
        $orderId = null;
        $companyId = null;                
        if(isset($input[$this->formName()]['orderId'])) {
            $order = \common\models\ar\Order::findOne(['id' => $input[$this->formName()]['orderId']]);
            if(!empty($order)) {
                $orderId = $order->id;
                $companyId = $order->company->id;
                $this->object = $this->object ?? $order->company->name.', '.$order->company->getAddressName();
            }
        }
        
        parent::loadInput($input);
        $this->orderId = $orderId;
        $this->companyId = $companyId;
       
    }
    
    public function init() {
        $config = Yii::$app->params;
    }

    public function attributeLabels() {
        return [
            	'date' => Yii::t('documents', 'from: '),
            	'object' => Yii::t('documents', 'On object:'),
            	'repairer' => Yii::t('documents', 'Repairer'),
        		'preText' => Yii::t('documents', 'Introductory remarks'),
        		'lightIntensityFormula' => Yii::t('documents', 'Calculating average light intensity'),
        		'lightUniformityFormula' => Yii::t('documents', 'Calculating light uniformity'),
            	'measurementTool' => Yii::t('documents', 'Measurement tool'),
            	'time' => Yii::t('documents', 'Duration of emergency lighting test'),
            	'testDate' => Yii::t('documents', 'Testing hours'),
            	'measurementDate' => Yii::t('documents', 'Hours measurement'),
            	'text' => Yii::t('documents', 'Remarks'),
        ];
    }

    public function getType() {
        return SectionsMapper::LIGHT_MEASUREMENT;
    }

}
