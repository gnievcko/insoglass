<?php

namespace common\documents\sections;

use common\documents\sections\SectionsMapper;
use common\documents\forms\InvoiceProductPrecorrectedForm;

class InvoiceProductsPrecorrected extends InvoiceProducts {

	protected function getInvoiceProductFormClass() {
		return [
				'formName' => (new InvoiceProductPrecorrectedForm())->formName(),
				'class' => InvoiceProductPrecorrectedForm::class,
		];
	}
	
    public function getType() {
        return SectionsMapper::INVOICE_PRODUCTS_PRECORRECTED;
    }
}
