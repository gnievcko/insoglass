<?php
namespace common\documents\sections;

use Yii;
use MongoDB\BSON\ObjectID;

class DuplicateInvoiceTitle extends InvoiceTitle {

	public $referencedInvoiceId;
	public $referencedInvoiceHistoryId;
	public $referencedInvoiceNumber;
	public $referencedInvoiceType;
	public $dateOfIssueDuplicate;
	
	public function loadInput($input) {
		parent::loadInput($input);
		
		$invoiceTitleFormName = (new InvoiceTitle())->formName();
		if(!isset($input[$invoiceTitleFormName]['referencedInvoiceId'])) {
			$invoiceTitleFormName = (new DuplicateInvoiceTitle())->formName();
		}
		
		$this->referencedInvoiceId = $input[$invoiceTitleFormName]['referencedInvoiceId'];
		$this->referencedInvoiceHistoryId = $input[$invoiceTitleFormName]['referencedInvoiceHistoryId'];
		$this->referencedInvoiceNumber = $input[$invoiceTitleFormName]['referencedInvoiceNumber'];
		$this->referencedInvoiceType = $input[$invoiceTitleFormName]['referencedInvoiceType'];
		$this->number = $this->referencedInvoiceNumber;
		
		if($this->referencedInvoiceId instanceof ObjectID) {
			$this->referencedInvoiceId = $this->referencedInvoiceId->__toString();
		}
		if($this->referencedInvoiceHistoryId instanceof ObjectID) {
			$this->referencedInvoiceHistoryId = $this->referencedInvoiceHistoryId->__toString();
		}
		
		$this->dateOfIssue = $input[$invoiceTitleFormName]['dateOfIssue'];
		$this->dateOfSale = $input[$invoiceTitleFormName]['dateOfSale'];
		
		if(empty($this->dateOfIssue)) {
			$this->dateOfIssue = (new \DateTime())->format('Y-m-d');
		}
		
		if(isset($input[$invoiceTitleFormName]['dateOfIssueDuplicate'])) {
			$this->dateOfIssueDuplicate = $input[$invoiceTitleFormName]['dateOfIssueDuplicate'];
		}
		else {
			$this->dateOfIssueDuplicate = (new \DateTime())->format('Y-m-d');
		}
	}
	
	public function validateDateOfIssue() {
		
	}
	
	public function attributeLabels() {
		$labels = parent::attributeLabels();
		$labels['titleInvoice'] = Yii::t('documents', 'Duplicate of invoice number');
		$labels['titleCorrectiveInvoice'] = Yii::t('documents', 'Duplicate of corrective invoice number');
		$labels['dateOfIssueDuplicate'] = Yii::t('documents', 'Date of duplicate issue');
		
		return $labels;
	}
	
	protected function getNextNumber() {
		return $this->referencedInvoiceNumber;
	}
	
	public function getType() {
		return SectionsMapper::DUPLICATE_INVOICE_TITLE;
	}
}
