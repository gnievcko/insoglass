<?php

namespace common\documents\sections;

use Yii;
use common\documents\sections\SectionsMapper;
use common\models\aq\OfferedProductRequirementQuery;
use frontend\helpers\UserHelper;
use common\helpers\StringHelper;
use common\models\ar\User;
use yii\helpers\ArrayHelper;

class IntermediateProducts extends Section {
    
    public $dateFrom;
    public $dateTo;
    public $user;
    public $dateGenerated;
    
    public $countByOrder;
    public $countByIntermediateProduct;

    public function init() {
        $pieces = explode( '.', $_GET['dateTo']);
        $this->dateTo = $pieces[2] . '-' . $pieces[1] . '-' .$pieces[0] . ' 23:59:59';
        $pieces = explode( '.', $_GET['dateFrom']);
        $this->dateFrom = $pieces[2] . '-' . $pieces[1] . '-' .$pieces[0] . ' 00:00:00';
        
        $this->countByOrder = OfferedProductRequirementQuery::getIntermediateProductsByOrder($this->dateFrom, $this->dateTo);
        $cbo2 = OfferedProductRequirementQuery::getIntermediateProductsByOrder($this->dateFrom, $this->dateTo);
        $this->countByOrder = ArrayHelper::index($this->countByOrder, null, 'orderNumber');
        
        $this->countByIntermediateProduct = OfferedProductRequirementQuery::getIntermediateProductsForAllOrders($this->dateFrom, $this->dateTo);
        
        $user = User::findOne(Yii::$app->user->id);
        $this->user = UserHelper::getPrettyUserName($user->email, $user->first_name, $user->last_name);
        $this->dateGenerated = StringHelper::getFormattedDateFromTimestamp(time(), true);
        
        $this->dateFrom = StringHelper::getFormattedDateFromDate($this->dateFrom);
        $this->dateTo = StringHelper::getFormattedDateFromDate($this->dateTo);
    }
    
    public function attributeLabels() {
        return [
            'reportName' => mb_strtoupper(Yii::t('documents', 'Report forecasting executability of orders')),
            'demandOnIntermediateProductsAccordingToOrder' => Yii::t('documents', 'Demand on intermediate products according to order'),
            'demandOnIntermediateProductsAccordingToAllOrders' => Yii::t('documents', 'Demand on intermediate products according to all orders'),
            'none' => Yii::t('web', 'None'),
            'forPeriod' => Yii::t('web', 'For period'),
            'generatedFor' => Yii::t('web', 'Generated for'),
            'generationDate' => Yii::t('web', 'Generation date'),
            
            'product' => Yii::t('web', 'Product'),
            'productCount' => Yii::t('web', 'Number of products remaining to produce'),
            'intermediateProduct' => Yii::t('web', 'Intermediate product'),
            'intermediateProductRequiredCount' => Yii::t('web', 'Intermediate product required count'),
            'intermediateProductsInWarehouses' => Yii::t('web', 'Number of intermediate products in warehouses'),
            'isThereEnoughIntermediateProductsInWarehouses' => Yii::t('web', 'Is there enough intermediate products in warehouses?'),
            'numberToBuy' => Yii::t('web', 'Number needed to be bought'),
            
            'count' => Yii::t('web', 'Count'),
            'user' => Yii::t('documents', 'Issuing user'),
            'dateCreation' => Yii::t('main', 'Creation date'),
            'userReceiving' => Yii::t('documents', 'Receiver'),
            'description' => Yii::t('main', 'Description'),
        ];
    }
    
    public function getType() {
        return SectionsMapper::INTERMEDIATE_PRODUCTS;
    }
}
