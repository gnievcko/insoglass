<?php

namespace common\documents\sections;

use common\helpers\ArrayHelper;
use common\models\ar\Language;

class OrderConfirmationFooter extends Section {
	
	public $languageId;
	
	public function rules() {
		return [
				[['languageId'], 'safe'],
		];
	}
	
	public function loadInput($data) {
		$this->languageId = ArrayHelper::searchArrayForKey($data, 'languageId');		
	}
	
	public function getLanguage(){
		return Language::find()->where(['id' => $this->languageId])->one()['symbol'];
	}
	
	public function getType() {
		return SectionsMapper::ORDER_CONFIRMATION_FOOTER;
	}
}