<?php

namespace common\documents\sections;

use Yii;
use common\documents\sections\SectionsMapper;
use common\models\ar\InformationPage;
use common\helpers\Utility;

class RepairProtocol extends Section {

    public $rows = [[]];
    public $tableName;
    public $form;
    
    public $rowsReplacements = [[]];
    public $tableNameReplacements;
    public $formReplacements;
    
    public $rowsTests = [[]];
    public $tableNameTests;
    public $formTests;
       
    public $date;
    public $object;
    public $repairer;
    public $description;
    public $remarks;
    public $text;
    public $orderId;
    public $companyId;
       
    public function rules() {
        return [
                [[
                    'object', 'date', 'description',
                    'repairer', 'remarks', 'text',
                    'form', 'rows', 'orderId',
                    'companyId', 'formReplacements', 'formTests',
                    'tableName', 'tableNameReplacements', 'tableNameTests',
                    'rowsReplacements', 'rowsTests'
                ], 'safe'],
        ];
    }

    public function loadInput($input) {
        $this->tableName = $this->tableName ?? Yii::t('documents', 'Repaired system');
        $this->tableNameReplacements = $this->tableNameReplacements ?? Yii::t('documents', 'List of replaced and delivered parts');
        $this->tableNameTests = $this->tableNameTests ?? Yii::t('documents', 'System startup/tests');
        
        $this->loadTables($input);
        $orderId = null;
        $companyId = null;
        if(isset($input[$this->formName()]['orderId'])) {
            $order = \common\models\ar\Order::findOne(['id' => $input[$this->formName()]['orderId']]);
            if(!empty($order)) {
                $orderId= $order->id;
                $companyId = $order->company->id;
                $this->object = $this->object ?? $order->company->name.', '.$order->company->getAddressName();
            }
        }
        parent::loadInput($input);

        $this->orderId = $orderId;
        $this->companyId = $companyId;
    }

    public function init() {
        $config = Yii::$app->params;
        
        $defaultAdditionalText = InformationPage::find()->where(['symbol' => Utility::INFORMATION_PAGE_DEFAULT_REPAIR_ADDITIONAL_TEXT])->one()->getTranslation();
        if(!empty($defaultAdditionalText)) {
        	$this->text = trim(strip_tags($defaultAdditionalText->content, '<br/>'));
        }
    }

    public function attributeLabels() {
        return [
            'date' => Yii::t('documents', 'from: '),
            'object' => Yii::t('documents', 'On object:'),
            'repairer' => Yii::t('documents', 'Repairer'),
            'text' => Yii::t('documents', 'Additional text'),
            'remarks' => Yii::t('documents', 'Protocol remarks'),
            'description' => Yii::t('documents', 'Description of damage - range repair'),
            'tableName' => Yii::t('documents', 'Table name'),
            'tableNameReplacements' => Yii::t('documents', 'Table name'),
            'tableNameTests' => Yii::t('documents', 'Table name'),
            ];
    }

    public function getType() {
        return SectionsMapper::REPAIR_PROTOCOL;
    }
    
    private function loadTables($input) {
        $formName = (new \common\documents\forms\RepairProtocolForm())->formName();
        $this->rows = isset($input[$formName]) ? $input[$formName] : $this->rows;
       
        $formName = (new \common\documents\forms\RepairReplacementsProtocolForm())->formName();
        $this->rowsReplacements = isset($input[$formName]) ? $input[$formName] : $this->rowsReplacements;
        
        $formName = (new \common\documents\forms\RepairTestsProtocolForm())->formName();
        $this->rowsTests = isset($input[$formName]) ? $input[$formName] : $this->rowsTests;
    }

}
