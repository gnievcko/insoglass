<?php

namespace common\documents\sections;

use Yii;
use common\documents\sections\SectionsMapper;
use frontend\helpers\UserHelper;
use common\helpers\StringHelper;
use common\models\ar\User;
use common\models\aq\WarehouseProductStatusHistoryQuery;

class GoodsIssuedReport extends Section {
	public $dateFrom;
	public $dateTo;
	public $user;
	public $dateGenerated;

	public $data;

    public function init() {
    	$piecesTo = explode( '.', $_GET['dateTo']);
    	$this->dateTo = $piecesTo[2] . '-' . $piecesTo[1] . '-' .$piecesTo[0] . ' 23:59:59';
    	$piecesFrom = explode( '.', $_GET['dateFrom']);
    	$this->dateFrom = $piecesFrom[2] . '-' . $piecesFrom[1] . '-' .$piecesFrom[0] . ' 00:00:00';
    	$this->data = WarehouseProductStatusHistoryQuery::getIssuedProductsForReport($this->dateFrom, $this->dateTo);
        $user = User::findOne(Yii::$app->user->id);
    	$this->user = UserHelper::getPrettyUserName($user->email, $user->first_name, $user->last_name);
    	$this->dateGenerated = StringHelper::getFormattedDateFromTimestamp(time(), true);
    	
    	$this->dateFrom = StringHelper::getFormattedDateFromDate($this->dateFrom);
    	$this->dateTo = StringHelper::getFormattedDateFromDate($this->dateTo);
    }

    public function attributeLabels() {
        return [
        		'upcomingOrders' => StringHelper::translateOrderToOffer('web', 'Upcoming orders'),
        		'none' => Yii::t('web', 'None'),
        		
        		'title' => Yii::t('web', 'Title'),
        		
        		'currentStatus' => Yii::t('web', 'Current status'),
        		'totalCost' => Yii::t('web', 'Total cost'),
        		'dateDeadline' => Yii::t('web', 'Deadline'),
        		
        		'report-goods-issued' => Yii::t('documents', 'REPORT - GOODS ISSUED'),
        		'forPeriod' => Yii::t('web', 'For period'),
        		'generatedFor' => Yii::t('web', 'Generated for'),
        		'generationDate' => Yii::t('web', 'Generation date'),
        		
        		'productName' => Yii::t('documents', 'Product name'),
        		'count' => Yii::t('documents', 'Count'),
        		
        		'issuer' => Yii::t('documents', 'Issuer'),
				'issuingType' => Yii::t('documents', 'Issuing type'),
        		'date' => Yii::t('web', 'Date'),
        		'warehouse' => Yii::t('web', 'Warehouse'),
        		'orderTitle' => StringHelper::translateOrderToOffer('main', 'Order'),
        		'userConfirming' => Yii::t('web', 'User confirming'),
        		
        ];
    }


    public function getType() {
        return SectionsMapper::GOODS_ISSUED_REPORT;
    }
}
