<?php
namespace common\documents\sections;

use Yii;
use common\helpers\Utility;
use common\helpers\NumberTemplateHelper;
use common\models\ar\Order;

class AdvanceInvoiceTitle extends InvoiceTitle {

	public $referencedInvoiceId;
	public $referencedInvoiceHistoryId;
	
	public function loadInput($input) {
		parent::loadInput($input);
	
		$invoiceTitleFormName = (new InvoiceTitle())->formName();
		if(!isset($input[$invoiceTitleFormName]['referencedInvoiceId'])) {
			$invoiceTitleFormName = (new AdvanceInvoiceTitle())->formName();
		}
	
		$this->referencedInvoiceId = $input[$invoiceTitleFormName]['referencedInvoiceId'];
		$this->referencedInvoiceHistoryId = $input[$invoiceTitleFormName]['referencedInvoiceHistoryId'];
		if(isset($input[$invoiceTitleFormName]['number'])) {
			$this->number = $input[$invoiceTitleFormName]['number'];
		}
	
		$this->dateOfIssue = $input[$invoiceTitleFormName]['dateOfIssue'];
		$this->dateOfSale = $input[$invoiceTitleFormName]['dateOfSale'];
		
		if(empty($this->dateOfIssue)) {
			$this->dateOfIssue = (new \DateTime())->format('Y-m-d');
		}
	}
	
	public function validateDateOfIssue() {
		
	}
	
	public function attributeLabels() {
		$labels = parent::attributeLabels();
		$labels['number'] = Yii::t('documents', 'Advance invoice number');
		
		return $labels;
	}
	
	protected function getNextNumber() {
		$entityId = $this->entityId;
		if(empty($entityId) && !empty($this->orderId)) {
			$order = Order::findOne($this->orderId);
			$entityId = !empty($order) ? $order->entity_id : null;
		}
		
		return NumberTemplateHelper::getNextNumber(Utility::NUMBER_TEMPLATE_ADVANCE_INVOICE, $entityId);
	}
	
	public function getType() {
		return SectionsMapper::ADVANCE_INVOICE_TITLE;
	}
}
