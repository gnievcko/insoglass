<?php

namespace common\documents\sections;

use Yii;
use common\documents\sections\SectionsMapper;
use yii\helpers\Json;

class OfferDetailsCustomData extends Section {

    const EMPTY_DATA = [['theads' => [], 'rows' => [[]]]];
	
    public $columnCount;
    public $tables = self::EMPTY_DATA;
    public $isVisible;
        
	public function rules() {
    	$rules = [
    			[['isVisible'], 'required'],
    			[['columnCount'], 'integer', 'max' => 7],
    			[['tables'], 'safe'],
    			[['isVisible'], 'boolean'],
    	];
    	
    	if(Yii::$app->params['isOrderCustomDataVisible']) {
    		$rules[] = ['columnCount', 'required'];
    	}
    	
    	return $rules;
    }

   public function loadInput($input) {
        if(isset($input[$this->formName()])) {
        	$formData = $input[$this->formName()];
        	$this->isVisible = $formData['isVisible'];
        	$this->columnCount = !empty($formData) ? $formData['columnCount'] : 0;
        	
        	if(isset($input['tables'])) {
        		$this->tables = $input['tables'];
        	}
        }
        parent::loadInput($input);
    }

    public function init() {
        $config = Yii::$app->params;
    }

    public function attributeLabels() {
    	return [
    			'columnCount' => Yii::t('web', 'Number of columns in a next table')
    	];
    }
    
    public static function isCustomDataEmpty($tables) {
    	return empty($tables) || Json::encode($tables) == '[{"theads":[],"rows":[[]]}]';
    }

    public function getType() {
        return SectionsMapper::OFFER_DETAILS_CUSTOM_DATA;
    }

}
