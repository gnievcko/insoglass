<?php

namespace common\documents\sections;

use Yii;
use common\documents\sections\SectionsMapper;

// TODO: Zrefaktoryzowac widoki do tej sekcji, aby pozbyc sie duplikacji z invoice_payment
class CorrectiveInvoicePayment extends InvoicePayment {

	public $modifyReason;
	public $priceDifference;
	
	public function loadInput($input) {
		parent::loadInput($input);
	
		$formName = (new InvoicePayment())->formName();
		if(!isset($input[$formName]['modifyReason'])) {
			$formName = (new CorrectiveInvoicePayment())->formName();
		}
	
		if(isset($input[$formName]['modifyReason'])) {
			$this->modifyReason = $input[$formName]['modifyReason'];
		}
		
		if(isset($input[$formName]['priceDifference'])) {
			$this->priceDifference = $input[$formName]['priceDifference'];
		}
	}
	
	public function attributeLabels() {
		$labels = parent::attributeLabels();
		//$labels['dataBefore'] = Yii::t('documents', 'Before');
		//$labels['dataNow'] = Yii::t('documents', 'Now');
		$labels['modifyReason'] = Yii::t('documents', 'Reason of correction');
		$labels['priceDifference'] = Yii::t('documents', 'Change of invoice value');
	
		return $labels;
	}
	
	public function rules() {
		$rules = parent::rules();
		$rules[] = [['priceDifference'], 'safe'];
		$rules[] = [['modifyReason'], 'required'];
	
		return $rules;
	}
	
    public function getType() {
        return SectionsMapper::CORRECTIVE_INVOICE_PAYMENT;
    }
}
