<?php
namespace common\documents\sections;

use Yii;
use common\documents\sections\SectionsMapper;
use common\models\aq\OrderQuery;
use frontend\helpers\UserHelper;
use common\helpers\StringHelper;
use common\models\ar\User;
use common\models\aq\DocumentQuery;
use common\models\aq\OrderTypeQuery;

class OrderRegister extends Section {
	public $dateFrom;
	public $dateTo;
	public $user;
	public $dateGenerated;

	public $tableData;
	public $invoiceData;
	public $orderTypeIds;
	public $orderTypeNames;

    public function rules() {
        return [
        ];
    }

    public function init() {

    }
    
    public function loadInput($input) {
    	$pieces = explode('.', $input['dateTo']);
    	$this->dateTo = $pieces[2] . '-' . $pieces[1] . '-' .$pieces[0] . ' 23:59:59';
    	$pieces = explode('.', $input['dateFrom']);
    	$this->dateFrom = $pieces[2] . '-' . $pieces[1] . '-' .$pieces[0] . ' 00:00:00';
    	
    	if(Yii::$app->params['isOrderTypeVisible']) {
    		$this->orderTypeIds = $input['orderTypeIds'];
    		$this->orderTypeNames = array_column(OrderTypeQuery::getOrderTypesByIds($this->orderTypeIds), 'name');
    	}
    	else {
    		$this->orderTypeIds = null;
    	}
    	
    	$this->tableData = OrderQuery::getOrderRegisterReportData($this->dateFrom, $this->dateTo, $this->orderTypeIds);
    	$this->invoiceData = DocumentQuery::getInvoiceIssueDates(array_column($this->tableData, 'id'));
    	 
    	$user = User::findOne(Yii::$app->user->id);
    	$this->user = UserHelper::getPrettyUserName($user->email, $user->first_name, $user->last_name);
    	$this->dateGenerated = StringHelper::getFormattedDateFromTimestamp(time(), true);
    	
    	$this->dateFrom = StringHelper::getFormattedDateFromDate($this->dateFrom);
    	$this->dateTo = StringHelper::getFormattedDateFromDate($this->dateTo);
    }

    public function attributeLabels() {
        return [
        		'order register' => Yii::t('documents', 'ORDER REGISTER'),
        		'Id' => Yii::t('documents', 'Id'),
        		//'orderDate' => StringHelper::translateOrderToOffer('web', 'Order date'),
        		'orderDate' => Yii::t('documents', 'Execution date'),
        		'partyOrdering' => Yii::t('web', 'Party ordering'),
        		'object' => Yii::$app->params['isChildCompanyCalledObject'] ? Yii::t('web', 'Object') : Yii::t('web', 'Client'),
        		'typeOfWork' => Yii::t('documents', 'Type of work'),
        		'user' => Yii::t('main', 'User'),
        		'typeOfContract' => Yii::t('documents', 'Type of contract'),      		
        		'forPeriod' => Yii::t('web', 'For period'),
        		'generatedFor' => Yii::t('web', 'Generated for'),
        		'generationDate' => Yii::t('web', 'Generation date'),
        		'none' => Yii::t('web', 'None'),
        ];
    }


    public function getType() {
        return SectionsMapper::ORDER_REGISTER;
    }
}
