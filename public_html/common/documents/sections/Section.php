<?php

namespace common\documents\sections;

use yii\base\Model;

abstract class Section extends Model {

    private $isEditable = false;

    public function setEditable($isEditable) {
        $this->isEditable = $isEditable;
    }

    public function isEditable() {
        return $this->isEditable;
    }

    public function loadInput($input) {
        $this->load($input);
    }

    public abstract function getType();

    public function getModels() {
        return [$this];
    }
    
    // wykorzystywane w szczególnych przypadkach
    public function save() {
    	
    }
}
