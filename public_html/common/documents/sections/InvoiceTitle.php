<?php

namespace common\documents\sections;

use Yii;
use common\documents\sections\SectionsMapper;
use yii\db\Query;
use common\helpers\NumberTemplateHelper;
use common\helpers\Utility;
use common\models\ar\Order;
use yii\helpers\ArrayHelper;
use common\models\aq\OrderTypeQuery;
use common\helpers\StringHelper;
use common\models\aq\OrderTypeSetQuery;
use common\components\OrderReminderTrait;

class InvoiceTitle extends Section {

	use OrderReminderTrait {
		init as traitInit;
		rules as traitRules;
		attributeLabels as traitAttributeLabels;
		save as traitSave;
	}
	
    const INVOICE_ORIGINAL = 'INVOICE_ORIGINAL';
    const INVOICE_COPY = 'INVOICE_COPY';

    public $number;
    public $dateOfIssue;
    public $dateOfSale;
    public $sellerName;
    public $sellerAddress;
    public $sellerVatId;
    public $buyerName;
    public $buyerAddress;
    public $buyerVatId;
    public $isCopy;
    public $orderId;
    public $companyId;
    public $contractTypeId;
    public $contractTypes;
    public $orderTypeIds;
    //public $orderTypes; 
    public $entityId;

    public function rules() {
        $rules = [
                [['number', 'dateOfIssue', 'dateOfSale', 'sellerName', 'sellerAddress', 'sellerVatId', 'buyerName', 'buyerAddress'],
	                'required', 'message' => Yii::t('documents', 'This field is required')
	            ],
                [['orderId', 'companyId', 'buyerVatId', 'isCopy', 'entityId'], 'safe'],
                [['dateOfIssue', 'dateOfSale'], 'date', 'format' => 'php:Y-m-d', 'message' => Yii::t('web', 'Invalid date format')],
                [['dateOfIssue'], 'validateDateOfIssue'],
        ];
        
        if(Yii::$app->params['isOrderTypeVisible']) {
        	$rules[] = [['orderTypeIds', 'contractTypeId'], 'required', 'message' => Yii::t('documents', 'This field is required')];
        }
        else {
        	$rules[] = [['orderTypeIds', 'contractTypeId'], 'safe'];
        }
        
        if(!is_subclass_of($this, InvoiceTitle::className())) {
        	$rules = array_merge($rules, $this->traitRules());
        }
        
        return $rules;
    }

    public function validateDateOfIssue() {
    	if(empty(Yii::$app->request->get('edit'))) {
	    	$dbDate = (new Query())->select(['date_last_invoice'])
	    			->from('application_cache')
	    			->where(['entity_id' => $this->entityId])->one();
	
	        if(!empty($dbDate)) {
	            $dateThisInvoice = $this->dateOfIssue . ' 00:00:00';
	            if ($dateThisInvoice < $dbDate['date_last_invoice']) {
	                $lastInvoiceDate = explode(' ', $dbDate['date_last_invoice']);
	                $this->addError('dateOfIssue', Yii::t('web', 'Date of issue cannot be prior to previous invoice issued') . ' (' . $lastInvoiceDate[0] . ')');
	            }
	        }
    	}
    }

    public function loadInput($input) {    	
        $orderId = null;
        $companyId = null;
        $formName = (new InvoiceTitle())->formName();
        
        if(isset($input[$formName]['orderId'])) {
            $order = Order::findOne(['id' => $input[$formName]['orderId']]);
            if(!empty($order)) {
                $orderId = $order->id;
                $companyId = $order->company->id;
                
                $company = $order->company;
                if(!empty($order->company->parentCompany)) {
                	$company = $order->company->parentCompany;
                }
                
                $this->buyerAddress = $company->getAddressName();
                $this->buyerName = $company->name;
                $this->buyerVatId = $company->vat_identification_number;
                $this->contractTypeId = $order->contract_type_id;
                if(Yii::$app->params['isOrderTypeVisible']) {
                	$this->orderTypeIds = array_column(OrderTypeSetQuery::getOrderTypesByOrderId($order->id), 'id');
                }
                else {
                	$this->orderTypeIds = null;
                }
            }
        }
        
        $companyId = !empty($input[$formName]['companyId']) ? $input[$formName]['companyId'] : $companyId;
        $this->buyerAddress = !empty($input[$formName]['buyerAddress']) ? $input[$formName]['buyerAddress'] : $this->buyerAddress;
        $this->buyerName = !empty($input[$formName]['buyerName']) ? $input[$formName]['buyerName'] : $this->buyerName;
        $this->buyerVatId = !empty($input[$formName]['buyerVatId']) ? $input[$formName]['buyerVatId'] : $this->buyerVatId;
        $this->contractTypeId = !empty($input[$formName]['contractTypeId']) ? $input[$formName]['contractTypeId'] : $this->contractTypeId;
        
        $this->orderTypeIds = !empty($input[$formName]['orderTypeId']) ? $input[$formName]['orderTypeId'] : $this->orderTypeIds;
        if(!empty($this->orderTypeIds) && !is_array($this->orderTypeIds)) {
        	$this->orderTypeIds = [$this->orderTypeIds];
        }

        parent::loadInput($input);
        $this->orderId = $orderId;
        $this->companyId = $companyId;
        
        if(empty($this->dateOfIssue)) {
        	$this->dateOfIssue = (new \DateTime())->format('Y-m-d');
        }
    }

    public function init() {
        $config = Yii::$app->params;
        $this->sellerName = $config['owner']['name'];
        $this->sellerAddress = $config['owner']['address'];
        $this->sellerVatId = $config['owner']['vatId'];
        $this->isCopy = 0;
        
        $this->contractTypes = ArrayHelper::map(OrderTypeQuery::getContractTypes(), 'id', 'name');        
        //$this->orderTypes = ArrayHelper::map(OrderTypeQuery::getOrderTypes(), 'id', 'name');
        
        $order = null;
        if(!empty(Yii::$app->request->get('orderId'))) {
        	$this->orderId = Yii::$app->request->get('orderId');
            $order = Order::findOne(Yii::$app->request->get('orderId'));
        }

        if(!empty($order)) {
            $this->buyerAddress = $order->company->getAddressName();
            $this->buyerName = $order->company->name;
            $this->buyerVatId = $order->company->vat_identification_number;
            $this->contractTypeId = $order->contract_type_id;
            if(Yii::$app->params['isOrderTypeVisible']) {
            	$this->orderTypeIds = array_column(OrderTypeSetQuery::getOrderTypesByOrderId($order->id), 'id');
           	}
           	else {
           		$this->orderTypeIds = null;
           	}

            if(!empty($order->entity)) {
                $this->sellerName = $order->entity->name;
                $this->sellerAddress = $order->entity->address->getFullAddress(true);
                $this->sellerVatId = $order->entity->vat_identification_number;
                $this->entityId = $order->entity_id;
            }
        }
        
        $this->number = $this->getNextNumber();
        
        if(!is_subclass_of($this, InvoiceTitle::className())) {
        	$this->traitInit();
        }
    }
    
    protected function getNextNumber() {
    	$entityId = $this->entityId;
    	if(empty($entityId) && !empty($this->orderId)) {
    		$order = Order::findOne($this->orderId);
    		$entityId = !empty($order) ? $order->entity_id : null;
    	}
    	
    	return NumberTemplateHelper::getNextNumber(Utility::NUMBER_TEMPLATE_INVOICE, $entityId);
    }

    public function attributeLabels() {
        $labels = [
            	'number' => Yii::t('documents', 'Invoice number'),
            	'dateOfIssue' => Yii::t('documents', 'Date of issue'),
            	'dateOfSale' => Yii::t('documents', 'Date of sale'),
            	'buyerName' => Yii::t('main', 'Name'),
            	'buyerAddress' => Yii::t('main', 'Address'),
            	'buyerVatId' => Yii::t('documents', 'VAT ID'),
            	'contractTypes' => Yii::t('documents', 'Contract type'),
        		'orderTypes' => StringHelper::translateOrderToOffer('web', 'Order types'),
        ];
        
        if(!is_subclass_of($this, InvoiceTitle::className())) {
        	$labels = array_merge($labels, $this->traitAttributeLabels());
        }
        
        return $labels;
    }
    
    public function save() {
    	if(!is_subclass_of($this, InvoiceTitle::className()) && empty(Yii::$app->request->get('edit', 0))) {
    		$this->traitSave(new \DateTime($this->dateOfSale));
    	}
    }

    public function getType() {
        return SectionsMapper::INVOICE_TITLE;
    }

}
