<?php
namespace common\documents\sections;

use Yii;
use common\documents\sections\SectionsMapper;
use frontend\helpers\UserHelper;
use common\helpers\StringHelper;
use common\models\ar\User;
use common\helpers\Utility;
use common\models\aq\TaskQuery;

class TaskReport extends Section {
	public $dateFrom;
	public $dateTo;
	public $user;
	public $dateGenerated;

	public $tableData;

    public function rules() {
        return [
        ];
    }

    public function init() {
    	$pieces = explode( '.', $_GET['dateTo']);
    	$this->dateTo = $pieces[2] . '-' . $pieces[1] . '-' .$pieces[0] . ' 23:59:59';
    	$pieces = explode( '.', $_GET['dateFrom']);
    	$this->dateFrom = $pieces[2] . '-' . $pieces[1] . '-' .$pieces[0] . ' 00:00:00';

    	$this->tableData = TaskQuery::getTaskReportData($this->dateFrom, $this->dateTo);
    	
        $user = User::findOne(Yii::$app->user->id);
    	$this->user = UserHelper::getPrettyUserName($user->email, $user->first_name, $user->last_name);
    	$this->dateGenerated = StringHelper::getFormattedDateFromTimestamp(time(), true);

    	$this->dateFrom = StringHelper::getFormattedDateFromDate($this->dateFrom);
    	$this->dateTo = StringHelper::getFormattedDateFromDate($this->dateTo);
    }

    public function attributeLabels() {
        return [
        		'taskReport' => Yii::t('documents', 'REPORT - TASKS'),
        		'Id' => Yii::t('documents', 'Id'),
        		'user' => Yii::t('main', 'User'),  		
        		'forPeriod' => Yii::t('web', 'For period'),
        		'generatedFor' => Yii::t('web', 'Generated for'),
        		'generationDate' => Yii::t('web', 'Generation date'),
        		'title' => Yii::t('web', 'Title'),
        		'taskPriority' => Yii::t('web', 'Task priority'),
        		'taskType' => Yii::t('main', 'Task type'),
        		'dateReminder' => Yii::t('main', 'Reminder date'),
        		'dateDeadline' => Yii::t('web', 'Deadline'),
        		'message' => Yii::t('main', 'Message'),
        		'ongoingTasks' => Yii::t('web', 'Ongoing tasks'),
        		'completedTasks' => Yii::t('web', 'Completed tasks'),
        		'none' => Yii::t('web', 'None'),
        ];
    }

    public function getType() {
        return SectionsMapper::TASK_REPORT;
    }
}
