<?php

namespace common\documents\sections;

use Yii;
use common\documents\sections\SectionsMapper;

class TitleWithDate extends Section {

    public $date;

    public function rules() {
        return [
            [['date'], 'safe'],
        ];
    }

    public function attributeLabels() {
        return [
            'date' => Yii::t('documents', 'From day'),
        ];
    }

    public function getType() {
        return SectionsMapper::TITLE_WITH_DATE;
    }
}
