<?php
namespace common\documents\sections;

use Yii;
use common\helpers\Utility;
use common\helpers\NumberTemplateHelper;
use common\models\ar\Order;

class ProformaInvoiceTitle extends InvoiceTitle {

	public function validateDateOfIssue() {
		
	}
	
	public function attributeLabels() {
		$labels = parent::attributeLabels();
		$labels['number'] = Yii::t('documents', 'Proforma invoice number');
		
		return $labels;
	}
	
	protected function getNextNumber() {
		$entityId = $this->entityId;
		if(empty($entityId) && !empty($this->orderId)) {
			$order = Order::findOne($this->orderId);
			$entityId = !empty($order) ? $order->entity_id : null;
		}
		
		return NumberTemplateHelper::getNextNumber(Utility::NUMBER_TEMPLATE_PROFORMA_INVOICE, $entityId);
	}
	
	public function getType() {
		return SectionsMapper::PROFORMA_INVOICE_TITLE;
	}
}
