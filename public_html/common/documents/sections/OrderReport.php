<?php

namespace common\documents\sections;

use Yii;
use common\documents\sections\SectionsMapper;
use common\models\aq\OrderQuery;
use frontend\helpers\UserHelper;
use common\helpers\StringHelper;
use common\models\ar\User;

class OrderReport extends Section {
	public $dateFrom;
	public $dateTo;
	public $user;
	public $dateGenerated;

	public $statistics; //[openOrderCount, closedOrderCount, ongoingOrderCount, handledOrderCount]

	public $orderCountByStatus;

	public $orderCountByClient;

	public $orderCountByAuthor;

	public $orderCountByEmployeeUpdating;

	public $upcomingOrders;

    public function rules() {
        return [

        ];
    }

    public function init() {

    	$pieces = explode( '.', $_GET['dateTo']);
    	$this->dateTo = $pieces[2] . '-' . $pieces[1] . '-' .$pieces[0] . ' 23:59:59';
    	$pieces = explode( '.', $_GET['dateFrom']);
    	$this->dateFrom = $pieces[2] . '-' . $pieces[1] . '-' .$pieces[0] . ' 00:00:00';

    	$this->statistics = OrderQuery::getOrderStatistics($this->dateFrom, $this->dateTo);
    	$this->orderCountByStatus = OrderQuery::getOrderCountByStatus($this->dateFrom, $this->dateTo);
    	$this->orderCountByClient = OrderQuery::getOrderCountByClient($this->dateFrom, $this->dateTo);
    	$this->orderCountByAuthor = OrderQuery::getOrderCountByAuthor($this->dateFrom, $this->dateTo);
    	$this->orderCountByEmployeeUpdating = OrderQuery::getOrderCountByEmployeeUpdating($this->dateFrom, $this->dateTo);
    	$this->upcomingOrders = OrderQuery::getUpcomingOrders($this->dateFrom, $this->dateTo);
    	$user = User::findOne(Yii::$app->user->id);
    	$this->user = UserHelper::getPrettyUserName($user->email, $user->first_name, $user->last_name);
    	$this->dateGenerated = StringHelper::getFormattedDateFromTimestamp(time(), true);
    	
    	$this->dateFrom = StringHelper::getFormattedDateFromDate($this->dateFrom);
    	$this->dateTo = StringHelper::getFormattedDateFromDate($this->dateTo);

    }

    public function attributeLabels() {
        return [
        		'openOrderCount' => StringHelper::translateOrderToOffer('web', 'Number of open orders'),
        		'closedOrderCount' => StringHelper::translateOrderToOffer('web', 'Number of closed orders'),
        		'ongoingOrderCount' => StringHelper::translateOrderToOffer('web', 'Number of ongoing orders'),
        		'handledOrderCount' => StringHelper::translateOrderToOffer('web', 'Number of handled orders'),
        		'orderCountByStatus' => StringHelper::translateOrderToOffer('web', 'Orders by status'),
        		'orderCountByClient' => StringHelper::translateOrderToOffer('web', 'Orders by client'),
        		'orderCountByAuthor' => StringHelper::translateOrderToOffer('web', 'Orders by author'),
        		'orderCountByEmployeeUpdating' => StringHelper::translateOrderToOffer('web', 'Orders by employee updating'),
        		'upcomingOrders' => StringHelper::translateOrderToOffer('web', 'Upcoming orders'),
        		'none' => Yii::t('web', 'None'),
        		'title' => Yii::t('web', 'Title'),
        		'currentStatus' => Yii::t('web', 'Current status'),
        		'totalCost' => Yii::t('web', 'Total cost'),
        		'dateDeadline' => Yii::t('web', 'Deadline'),
        		'report-orders' => StringHelper::translateOrderToOffer('web', 'REPORT - ORDERS'),
        		'forPeriod' => Yii::t('web', 'For period'),
        		'generatedFor' => Yii::t('web', 'Generated for'),
        		'generationDate' => Yii::t('web', 'Generation date'),
        		'number' => Yii::t('web', 'Number'),
        		'status' => Yii::t('web', 'Status'),
        		'client' => Yii::t('web', 'Client'),
        		'author' => Yii::t('web', 'Author'),
        		'employee' => Yii::t('main', 'Employee'),
        ];
    }


    public function getType() {
        return SectionsMapper::REPORT_ORDER;
    }
}
