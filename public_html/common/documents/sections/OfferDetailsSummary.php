<?php

namespace common\documents\sections;

use Yii;
use common\documents\sections\SectionsMapper;

class OfferDetailsSummary extends Section {

    public $deadline;
    public $termsOfPayment;
    public $validityDate;
    public $remarks;
    public $note;

    public function rules() {
        return [
            [['deadline', 'termsOfPayment', 'validityDate', 'remarks', 'note'], 'safe'],
        ];
    }

    public function attributeLabels() {
        return [
	            'deadline' => Yii::t('documents_orders', 'Deadline'),
	            'termsOfPayment' => Yii::t('documents_orders', 'Terms of payment'),
	            'validityDate' => Yii::t('documents_orders', 'Validity date'),
	            'remarks' => Yii::t('web', 'Remarks'),
        		'note' => Yii::t('web', 'Note').' ('.mb_strtolower(Yii::t('web', 'Displayed as bold'), 'UTF-8').')',
        ];
    }

    public function getType() {
        return SectionsMapper::OFFER_DETAILS_SUMMARY;
    }
}
