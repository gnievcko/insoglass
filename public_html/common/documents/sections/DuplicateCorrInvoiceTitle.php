<?php
namespace common\documents\sections;

use Yii;
use MongoDB\BSON\ObjectID;

class DuplicateCorrInvoiceTitle extends CorrectiveInvoiceTitle {

	public $referencedCorrInvoiceId;
	public $referencedCorrInvoiceHistoryId;
	public $referencedCorrInvoiceNumber;
	public $referencedCorrInvoiceType;
	public $dateOfIssueDuplicate;
	
	public function loadInput($input) {
		parent::loadInput($input);
		
		$invoiceTitleFormName = (new InvoiceTitle())->formName();
		if(!isset($input[$invoiceTitleFormName]['referencedCorrInvoiceId'])) {
			$invoiceTitleFormName = (new DuplicateCorrInvoiceTitle())->formName();
		}		
		
		$this->referencedCorrInvoiceId = $input[$invoiceTitleFormName]['referencedCorrInvoiceId'];
		$this->referencedCorrInvoiceHistoryId = $input[$invoiceTitleFormName]['referencedCorrInvoiceHistoryId'];
		$this->referencedCorrInvoiceNumber = $input[$invoiceTitleFormName]['referencedCorrInvoiceNumber'];
		$this->referencedCorrInvoiceType = $input[$invoiceTitleFormName]['referencedCorrInvoiceType'];
		$this->number = $this->referencedCorrInvoiceNumber;
		
		if($this->referencedCorrInvoiceId instanceof ObjectID) {
			$this->referencedCorrInvoiceId = $this->referencedCorrInvoiceId->__toString();
		}
		if($this->referencedCorrInvoiceHistoryId instanceof ObjectID) {
			$this->referencedCorrInvoiceHistoryId = $this->referencedCorrInvoiceHistoryId->__toString();
		}
		
		$this->dateOfIssue = $input[$invoiceTitleFormName]['dateOfIssue'];
		$this->dateOfSale = $input[$invoiceTitleFormName]['dateOfSale'];
		
		if(empty($this->dateOfIssue)) {
			$this->dateOfIssue = (new \DateTime())->format('Y-m-d');
		}
		
		if(isset($input[$invoiceTitleFormName]['dateOfIssueDuplicate'])) {
			$this->dateOfIssueDuplicate = $input[$invoiceTitleFormName]['dateOfIssueDuplicate'];
		}
		else {
			$this->dateOfIssueDuplicate = (new \DateTime())->format('Y-m-d');
		}
	}
	
	public function validateDateOfIssue() {
		
	}
	
	public function attributeLabels() {
		$labels = parent::attributeLabels();
		$labels['titleCorrectiveInvoice'] = Yii::t('documents', 'Duplicate of corrective invoice number');
		$labels['dateOfIssueDuplicate'] = Yii::t('documents', 'Date of duplicate issue');
		
		return $labels;
	}
	
	protected function getNextNumber() {
		return $this->referencedCorrInvoiceNumber;
	}
	
	public function getType() {
		return SectionsMapper::DUPLICATE_CORR_INVOICE_TITLE;
	}
}
