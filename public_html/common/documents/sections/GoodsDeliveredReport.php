<?php

namespace common\documents\sections;

use Yii;
use common\documents\sections\SectionsMapper;
use frontend\helpers\UserHelper;
use common\helpers\StringHelper;
use common\models\ar\User;
use common\models\aq\WarehouseDeliveryQuery;

class GoodsDeliveredReport extends Section {
	public $dateFrom;
	public $dateTo;
	public $user;
	public $dateGenerated;

	public $data;

    public function init() {
    	$piecesTo = explode( '.', $_GET['dateTo']);
    	$this->dateTo = $piecesTo[2] . '-' . $piecesTo[1] . '-' .$piecesTo[0] . ' 23:59:59';
    	$piecesFrom = explode( '.', $_GET['dateFrom']);
    	$this->dateFrom = $piecesFrom[2] . '-' . $piecesFrom[1] . '-' .$piecesFrom[0] . ' 00:00:00';
    	
    	$this->data = WarehouseDeliveryQuery::getDeliveredProductsForReport($this->dateFrom, $this->dateTo);
    	
        $user = User::findOne(Yii::$app->user->id);
    	$this->user = UserHelper::getPrettyUserName($user->email, $user->first_name, $user->last_name);
    	$this->dateGenerated = StringHelper::getFormattedDateFromTimestamp(time(), true);
    	
    	$this->dateFrom = StringHelper::getFormattedDateFromDate($this->dateFrom);
    	$this->dateTo = StringHelper::getFormattedDateFromDate($this->dateTo);
    }

    public function attributeLabels() {
        return [
        		'none' => Yii::t('web', 'None'),
        		
        		'title' => Yii::t('web', 'Title'),
        		
        		'report-goods-delivered' => Yii::t('documents', 'REPORT - GOODS DELIVERED'),
        		'forPeriod' => Yii::t('web', 'For period'),
        		'generatedFor' => Yii::t('web', 'Generated for'),
        		'generationDate' => Yii::t('web', 'Generation date'),
        		
        		'deliveryNumber' => Yii::t('web', 'Delivery number'),
        		'warehouseSupplier' => Yii::t('web', 'Supplier'),
        		'dateDelivery' => Yii::t('web', 'Delivery date'),
        		'unitPrice' => Yii::t('web', 'Unit price'),
        		'count' => Yii::t('web', 'Count'),
        		'totalPrice' => Yii::t('web', 'Total price'),
        		'description' => Yii::t('web', 'Description'),
        		
        		'productName' => Yii::t('documents', 'Product name'),
        		'date' => Yii::t('web', 'Date'),
        		'warehouse' => Yii::t('web', 'Warehouse'),
        		'userConfirming' => Yii::t('web', 'User confirming'),
        		'sets' => Yii::t('documents', 'sets'),
        		'note' => Yii::t('web', 'Note'),
        		
        ];
    }


    public function getType() {
        return SectionsMapper::GOODS_DELIVERED_REPORT;
    }
}
