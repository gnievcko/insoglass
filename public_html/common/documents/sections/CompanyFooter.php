<?php

namespace common\documents\sections;

use Yii;
use common\documents\sections\SectionsMapper;
use common\models\ar\Order;

class CompanyFooter extends Section {

	public $isVisible;
    public $nationalCourtRegister;
    public $courtAddress;
    public $shareCapital;
    public $vatId;
    public $companyRegistrationNumber;
    public $bankAddress;
    public $bankAccount;
    public $swift;

    public function rules() {
        return [
        		[['isVisible', 'nationalCourtRegister', 'courtAddress', 'shareCapital', 'vatId',
        				'companyRegistrationNumber', 'bankAddress', 'bankAccount', 'swift'], 'safe'],
        ];
    }

    public function init() {
    	$config = Yii::$app->params;
        $this->isVisible = 1;
        $this->nationalCourtRegister = $config['owner']['nationalCourtRegister'];
        $this->courtAddress = $config['owner']['courtAddress'];
        $this->shareCapital = $config['owner']['shareCapital'];
        $this->vatId = $config['owner']['vatId'];
        $this->companyRegistrationNumber = $config['owner']['regon'];
        $this->bankAddress = $config['owner']['bankAddress'];
        $this->bankAccount = $config['owner']['bank'];
        $this->swift = $config['owner']['swift'];
        
        if(!empty(Yii::$app->request->get('orderId'))) {
        	$order = Order::findOne(Yii::$app->request->get('orderId'));
        	if(!empty($order) && !empty($order->entity) && empty($order->entity->is_footer)) {
        		$this->isVisible = false;
        	}
        }
    }
    
    public function loadInput($data) {
    	$this->load($data);
    	$this->isVisible = intval($this->isVisible);
    }

    public function attributeLabels() {
        return [
            'nationalCourtRegister' => Yii::t('documents', 'National court register'),
            'shareCapital' => Yii::t('documents', 'Share capital'),
            'vatId' => Yii::t('documents', 'VAT ID'),
            'companyRegistrationNumber' => Yii::t('documents', 'Company registration number'),
            'swift' => Yii::t('documents', 'SWIFT'),
        ];
    }

    public function getType() {
        return SectionsMapper::COMPANY_FOOTER;
    }
}
