<?php

namespace common\documents\sections;

use Yii;
use common\documents\sections\SectionsMapper;
use common\models\ar\InformationPage;
use common\helpers\Utility;
use yii\helpers\Json;

class ServiceProtocolFdafas extends ServiceProtocol {

	public function loadInput($input) {
		$formName = (new ServiceProtocol())->formName();
		$formNameFdafas = (new ServiceProtocolFdafas())->formName();
		
		if(isset($input[$formNameFdafas]) && isset($input[$formName])) {
			$input[$formNameFdafas] = array_merge($input[$formNameFdafas], $input[$formName]);
		}
		elseif(isset($input[$formName])) {
			$input[$formNameFdafas] = $input[$formName];
		}
		
		parent::loadInput($input);		
	}
	
	public function init() {
		$config = Yii::$app->params;
	
		$defaultAdditionalText = InformationPage::find()->where(['symbol' => Utility::INFORMATION_PAGE_DEFAULT_SERVICE_ADDITIONAL_TEXT])->one()->getTranslation();
		if(!empty($defaultAdditionalText)) {
			$this->additionalText = trim(strip_tags($defaultAdditionalText->content, '<br/>'));
		}
	
		$defaultTables = InformationPage::find()->where(['symbol' => Utility::INFORMATION_PAGE_DEFAULT_SERVICE_FDAFAS_TABLES])->one()->getTranslation();
		if(!empty($defaultTables)) {
			$tbl = Json::decode(strip_tags($defaultTables->content));
			if(!empty($tbl)) {
				$this->tables = [];
				foreach($tbl['tables'] as $arr) {
					$this->tables = array_merge($this->tables, $arr);
				}
			}
		}
	
		$dt = new \DateTime();
		$this->date = $dt->format('Y-m').'-.............';
	}
	
    public function getType() {
        return SectionsMapper::SERVICE_PROTOCOL_FDAFAS;
    }

}
