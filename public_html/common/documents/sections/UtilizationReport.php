<?php

namespace common\documents\sections;

use Yii;
use common\documents\sections\SectionsMapper;
use common\models\aq\WarehouseProductStatusHistoryQuery;
use frontend\helpers\UserHelper;
use common\helpers\StringHelper;
use common\models\ar\User;

class UtilizationReport extends Section {
	public $dateFrom;
	public $dateTo;
	public $user;
	public $dateGenerated;

	public $countByProduct;

	public $history;

    public function rules() {
        return [

        ];
    }

    public function init() {
    	$pieces = explode( '.', $_GET['dateTo']);
    	$this->dateTo = $pieces[2] . '-' . $pieces[1] . '-' .$pieces[0] . ' 23:59:59';
    	$pieces = explode( '.', $_GET['dateFrom']);
    	$this->dateFrom = $pieces[2] . '-' . $pieces[1] . '-' .$pieces[0] . ' 00:00:00';

    	$this->countByProduct = WarehouseProductStatusHistoryQuery::getUtilisedProductCountByDate($this->dateFrom, $this->dateTo);
    	$this->history = WarehouseProductStatusHistoryQuery::getUtilisationHistoryByDate($this->dateFrom, $this->dateTo);
    	
        $user = User::findOne(Yii::$app->user->id);
    	$this->user = UserHelper::getPrettyUserName($user->email, $user->first_name, $user->last_name);
    	$this->dateGenerated = StringHelper::getFormattedDateFromTimestamp(time(), true);

    	$this->dateFrom = StringHelper::getFormattedDateFromDate($this->dateFrom);
    	$this->dateTo = StringHelper::getFormattedDateFromDate($this->dateTo);

    }

    public function attributeLabels() {
        return [
        		'upcomingOrders' => StringHelper::translateOrderToOffer('web', 'Upcoming orders'),
        		'none' => Yii::t('web', 'None'),
        		
        		'product' => Yii::t('web', 'Product'),
        		'count' => Yii::t('web', 'Count'),
        		'user' => Yii::t('documents', 'Issuing user'),
        		'dateCreation' => Yii::t('main', 'Creation date'),
        		'userReceiving' => Yii::t('documents', 'Receiver'),
        		'description' => Yii::t('main', 'Description'),
        		
        		'report-utilizations' => Yii::t('documents', 'REPORT - UTILIZATIONS'),
        		'forPeriod' => Yii::t('web', 'For period'),
        		'generatedFor' => Yii::t('web', 'Generated for'),
        		'generationDate' => Yii::t('web', 'Generation date'),
        		'utilizationsByProduct' => Yii::t('documents', 'Utilizations by product'),
        		'utilizationOperations' => Yii::t('documents', 'Utilization operations'),
        ];
    }


    public function getType() {
        return SectionsMapper::REPORT_UTILIZATION;
    }
}
