<?php

namespace common\documents\sections;

use Yii;
use common\documents\sections\SectionsMapper;
use common\models\ar\Language;
use yii\helpers\ArrayHelper;

class OrderConfirmationBasicData extends Section {
	
	public $number;
	public $title;
	public $dateCreation;
	
	public $languageId;
	
	public $contactPeople;

	public $clientCompany;
	public $clientAddress;
	public $clientFax;
	
	public $shippingAndHandling;

	public $authorFax;
	
	public $shippingDate;
	public $payment_terms;
	public $deliveryAddress;
	
	public $description;

	public $orderId;
	public $companyId;
	
	// ponizsze pola zostaja jedynie dla zgodnosci z wczesniejszym rozwiazaniem
	// sa aktywowane jedynie wtedy, kiedy dla archiwalnych dokumentow zostana odczytane
	public $contactPerson;
	public $clientPhone;
	public $clientEmail;
	
	public function rules() {
		return [
				[[
						'contactPerson', 'orderId', 'companyId', 'contactPeople',
						'number', 'title', 'dateCreation', 'clientCompany',
						'clientAddress', 'clientPhone', 'clientEmail', 'description',
						'payment_terms', 'shippingDate', 'shippingAndHandling', 'clientFax', 'deliveryAddress', 'languageId', 'authorFax',
				], 'safe'],
		];
	}
	
	public function loadInput($input) {
		parent::loadInput($input);
		$this->orderId = (int)$this->orderId;
		$this->companyId = (int)$this->companyId;
		
	}
	
	public function init() {
		
	}
	public function getLanguage(){
		return Language::find()->where(['id' => $this->languageId])->one()['symbol'];
	}
	public function attributeLabels() {
		return [
				'dateCreation' => Yii::t('documents', 'From'),
				
				'contactPeople' => Yii::t('documents', 'Contact person'),
				'contactPerson' => Yii::t('web', 'Contact person'),
				'clientCompany' => Yii::t('main', 'Company'),
				'clientAddress' => Yii::t('main', 'Address'),
				'clientPhone' => Yii::t('documents', 'tel'),
				'clientEmail' => Yii::t('main', 'Email'),
				'clientPhoneFull' => Yii::t('main', 'Phone'),
				'title' => Yii::$app->params['isOrderTitleIsLabeledAsConcerns'] ? Yii::t('documents_orders', 'Refers to') : (Yii::$app->params['isOrderTitleLabeledAsOrderName'] ? Yii::t('web', 'Job name') : Yii::t('web', 'Title')),

				'description' => Yii::t('web', 'Description'),
				'payment_terms' => Yii::t('web', 'Terms of payment'),
				'shippingDate' => Yii::t('documents', 'Shipment date'),
				'deliveryAddress' => Yii::t('documents', 'Delivery address'),
				'clientFax' => Yii::t('main', 'Fax'),
				'authorFax' => Yii::t('main', 'Fax'),
				'shippingAndHandling' => Yii::t('documents', 'Shipping and handling'),
				'number' => Yii::t('documents_orders', 'No.'),
		];
	}
	public function getLanguages(){
		$languages =  Language::find()->all();
		return ArrayHelper::map($languages, 'id', 'name');
	}
	public function getType() {
		return SectionsMapper::ORDER_CONFIRMATION_BASIC_DATA;
	}
	
}