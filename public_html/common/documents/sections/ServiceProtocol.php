<?php

namespace common\documents\sections;

use Yii;
use common\documents\sections\SectionsMapper;
use common\models\ar\InformationPage;
use common\helpers\Utility;
use yii\helpers\Json;

class ServiceProtocol extends Section {

    public $tables = [['theads' => ['', '', '', '', ''], 'rows' => [[]]]];
    public $date;
    public $object = null;
    public $repairer;
    public $relatedTo;
    public $additionalText;
    public $text;
    public $orderId;
    public $companyId;
    public $numberList = [1, 2, 3, 4, 5, 6, 7];

    public function rules() {
        return [
                [['object', 'date', 'repairer', 'relatedTo', 'additionalText', 'text', 'tables', 'orderId', 'companyId'], 'safe'],
        ];
    }

    public function loadInput($input) {
        $orderId = null;
        $companyId = null;
        $object = null;        
        if(isset($input[$this->formName()]['orderId'])) {
            $order = \common\models\ar\Order::findOne(['id' => $input[$this->formName()]['orderId']]);
            if(!empty($order)) {
                $orderId = $order->id;
                $companyId = $order->company->id;
                $this->object = $this->object  ?? $order->company->name.', '.$order->company->getAddressName();
                $this->relatedTo = $order->title;
            }
        }

        parent::loadInput($input);
        $this->orderId = $orderId;
        $this->companyId = $companyId;
    }

    public function init() {
        $config = Yii::$app->params;
        
        $defaultAdditionalText = InformationPage::find()->where(['symbol' => Utility::INFORMATION_PAGE_DEFAULT_SERVICE_ADDITIONAL_TEXT])->one()->getTranslation();
        if(!empty($defaultAdditionalText)) {
        	$this->additionalText = trim(strip_tags($defaultAdditionalText->content, '<br/>'));
        }
        
        $defaultTables = InformationPage::find()->where(['symbol' => Utility::INFORMATION_PAGE_DEFAULT_SERVICE_TABLES])->one()->getTranslation();
        if(!empty($defaultTables)) {
        	$tbl = Json::decode(strip_tags($defaultTables->content));
        	if(!empty($tbl)) {
        		$this->tables = [];
	        	foreach($tbl['tables'] as $arr) {
	        		$this->tables = array_merge($this->tables, $arr);
	        	}
        	}
        }
        
        $dt = new \DateTime();
        $this->date = $dt->format('Y-m').'-.............';
    }

    public function attributeLabels() {
        return [
            	'date' => Yii::t('documents', 'from: '),
            	'object' => Yii::t('documents', 'On object:'),
            	'repairer' => Yii::t('documents', 'Repairer'),
            	'relatedTo' => Yii::t('documents', 'Related to'),
        		'additionalText' => Yii::t('documents', 'Additional text'),
            	'text' => Yii::t('documents', 'Remarks'),
        ];
    }

    public function getType() {
        return SectionsMapper::SERVICE_PROTOCOL;
    }

}
