<?php

namespace common\documents\sections;

use Yii;
use common\documents\sections\SectionsMapper;

class TitleWithDateEditable extends Section {

	public $title;
	public $date;

	public function rules() {
		return [
				[[], 'required', 'message' => Yii::t('documents', 'This field is required')],
				[['title', 'date'], 'safe'],
		];
	}

	public function attributeLabels() {
		return [
				'title' => Yii::t('documents', 'Title'),
				'date' => Yii::t('documents', 'of day'),
		];
	}

	public function getType() {
		return SectionsMapper::TITLE_WITH_DATE_EDITABLE;
	}
}
