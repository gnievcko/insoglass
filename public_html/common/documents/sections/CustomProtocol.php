<?php

namespace common\documents\sections;

use Yii;
use common\documents\sections\SectionsMapper;
use common\models\ar\Order;
use common\documents\DocumentType;

class CustomProtocol extends Section {

	public $text;
	public $orderId;
	public $companyId;

	public function rules() {
		return [
				[[], 'required', 'message' => Yii::t('documents', 'This field is required')],
				[['text', 'orderId', 'companyId'], 'safe'],
		];
	}
	
	public function loadInput($input) {
		parent::loadInput($input);
		
		if(empty($this->companyId)) {
			$order = Order::findOne(['id' => $this->orderId]);
			if(!empty($order)) {
				$this->companyId = $order->company->id;
			}
		}		
		
		$this->orderId = intval($this->orderId);
		$this->companyId = intval($this->companyId);
	}

	public function attributeLabels() {
		return [
				'text' => Yii::t('documents', 'Content'),
		];
	}

	public function getType() {
		return SectionsMapper::CUSTOM_PROTOCOL;
	}
}
