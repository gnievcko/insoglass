<?php

namespace common\documents\sections;

use Yii;
use common\documents\sections\SectionsMapper;
use common\helpers\UserNameHelper;
use frontend;

class DeliveryDetailsTitle extends Section {

    public $number;
    public $warehouseName;
    public $supplierName;
    public $userOrderingName;
    public $dateOrder;
    public $description;

    public function rules() {
        return [
            	[['number', 'dateOrder', 'userOrderingName', 'supplierName', 'warehouseName'], 'required', 'message' => Yii::t('documents', 'This field is required')],
            	[['description'], 'safe'],
        ];
    }

    public function init() {
        $this->dateOrder = (new \DateTime())->format('d.m.Y H:i');
    }
    
    public function loadInput($input) {
    	parent::loadInput($input);
    	// TODO: Helper do przeniesienia
    	if(isset($input[$this->formName()]['userEmail']) && isset($input[$this->formName()]['userFirstName']) && 
    			isset($input[$this->formName()]['userLastName'])) {
		    $this->userOrderingName = frontend\helpers\UserHelper::getPrettyUserName(
		    		$input[$this->formName()]['userEmail'], 
		    		$input[$this->formName()]['userFirstName'], 
		    		$input[$this->formName()]['userLastName']
			);
    	}
    }

    public function attributeLabels() {
        return [
	            'number' => Yii::t('documents', 'Order number').' / Order number',
        		'warehouseName' => Yii::t('documents', 'Warehouse').' / Warehouse',
			    'supplierName' => Yii::t('documents', 'Supplier').' / Supplier',
			    'userOrderingName' => Yii::t('documents', 'Ordering person').' / Ordering person',
			    'dateOrder' => Yii::t('documents', 'Order date').' / Order date',
			    'description' => Yii::t('documents', 'Remarks').' / Remarks',
        ];
    }

    public function getType() {
        return SectionsMapper::DELIVERY_DETAILS_TITLE;
    }
}
