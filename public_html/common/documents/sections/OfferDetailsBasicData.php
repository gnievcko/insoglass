<?php

namespace common\documents\sections;

use Yii;
use common\documents\sections\SectionsMapper;
use common\helpers\StringHelper;

class OfferDetailsBasicData extends Section {

    public $number;
    public $title;
    public $dateCreation;
    
    public $contactPeople;
    public $clientCompanyParent;
    public $clientCompany;
    public $clientAddress;
    
    public $authorCompany;
    public $authorAddress;
    public $authorSalesman;
    public $authorPhone;
    public $authorFax;
    public $authorEmail;
    public $authorPosition;
    
    public $remarks;
    
    public $description;
    
    public $orderId;
    public $companyId;
    
    // ponizsze pola zostaja jedynie dla zgodnosci z wczesniejszym rozwiazaniem
    // sa aktywowane jedynie wtedy, kiedy dla archiwalnych dokumentow zostana odczytane
    public $contactPerson;
    public $clientPhone;
    public $clientEmail;

    public function rules() {
        return [
            [[
            		'contactPerson', 'orderId', 'companyId', 'remarks', 'contactPeople',
              		'number', 'title', 'dateCreation', 'clientCompany', 'authorCompany', 'clientCompanyParent',
              		'clientAddress', 'clientPhone', 'clientEmail', 'authorAddress',
              		'authorSalesman', 'authorEmail', 'title', 'description', 'authorPhone', 'authorAddress',
            		'authorPosition',
            ], 'safe'],
        ];
    }
    
    public function loadInput($input) {
        parent::loadInput($input);
        
        $this->orderId = (int)$this->orderId;
        $this->companyId = (int)$this->companyId;
        
        if(empty($this->authorCompany)) {
        	$config = Yii::$app->params;
        	$this->authorCompany = $config['owner']['name'];
        	$this->authorAddress = $config['owner']['address'];
        }
        
    }
    
    public function init() {
          
    }

    public function attributeLabels() {
        return [
	            'dateCreation' => Yii::t('web', 'Date'),
	            
	        	'contactPeople' => Yii::t('web', 'Contact people'),
	            'contactPerson' => Yii::t('web', 'Contact person'),
        		'clientCompanyParent' => Yii::t('web', 'Parent company'),
	            'clientCompany' => Yii::t('main', 'Company'),
	            'clientAddress' => Yii::t('main', 'Address'),
	            'clientPhone' => Yii::t('main', 'Phone'),
	            'clientEmail' => Yii::t('main', 'Email'),
	            
	            'authorCompany' => Yii::t('main', 'Company'),
	            'authorAddress' => Yii::t('main', 'Address'),
	            'authorSalesman' => Yii::t('main', 'Salesman'),
	            'authorPhone' => Yii::t('main', 'Phone'),
	            'authorFax' => Yii::t('main', 'Fax'),
	            'authorEmail' => Yii::t('main', 'Email'),
        		'authorPosition' => Yii::t('web', 'Position'),
	            
	            'title' => Yii::$app->params['isOrderTitleIsLabeledAsConcerns'] ? Yii::t('documents_orders', 'Refers to') : (Yii::$app->params['isOrderTitleLabeledAsOrderName'] ? Yii::t('web', 'Job name') : Yii::t('web', 'Title')),
	            
	            'remarks' => Yii::t('web', 'Remarks'),
	        		
	            'description' => Yii::t('web', 'Description'),
        ];
    }

    public function getType() {
        return SectionsMapper::OFFER_DETAILS_BASIC_DATA;
    }
}
