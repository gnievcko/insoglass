<?php

namespace common\documents\sections;

use Yii;
use common\documents\sections\SectionsMapper;
use frontend\helpers\UserHelper;
use common\helpers\StringHelper;
use common\models\ar\User;
use common\models\aq\ProductCategoryQuery;
use common\documents\models\ListOfItemsReportModel;

class ListOfItemsReport extends Section {
    
    public $dateFrom;
    public $dateTo;
    public $user;
    public $dateGenerated;
    public $tableData;
    
    public $categoryIds;
    public $categoryNames;

    public function rules() {
        return [
        		[['dateFrom', 'dateTo'], 'required'],
        		['dateFrom', 'validateDates'],
        ];
    }
    
    public function validateDates() {
    	if(strtotime($this->dateTo) < strtotime($this->dateFrom)){
    		$this->addError('dateFrom', Yii::t('documents', 'Date "to" cannot be earlier than date "from"'));
    	}
    }

    public function init() {
        
    }
    
    public function loadInput($input) {
    	$request = Yii::$app->request;
    	
    	$pieces = explode('.', $input['dateTo']);
    	$this->dateTo = $pieces[2] . '-' . $pieces[1] . '-' .$pieces[0];
    	$pieces = explode('.', $input['dateFrom']);
    	$this->dateFrom = $pieces[2] . '-' . $pieces[1] . '-' .$pieces[0];
    	
    	$this->categoryIds = $input['categoryIds'];
    	$this->categoryNames = array_column(ProductCategoryQuery::getProductCategoriesByIds($this->categoryIds), 'name');
    	
    	$model = new ListOfItemsReportModel();
    	$this->tableData = $model->prepareProductsByDateRange($this->dateFrom, $this->dateTo, $this->categoryIds);
    	
    	$user = User::findOne(Yii::$app->user->id);
    	$this->user = UserHelper::getPrettyUserName($user->email, $user->first_name, $user->last_name);
    	$this->dateGenerated = StringHelper::getFormattedDateFromTimestamp(time(), true);
    	
    	$this->dateTo = StringHelper::getFormattedDateFromDate($this->dateTo);
    	$this->dateFrom = StringHelper::getFormattedDateFromDate($this->dateFrom);
    }

    public function attributeLabels() {
        return [
	            'listOfItemsReport' => Yii::t('documents', 'REPORT - LIST OF ITEMS'),
	            'Id' => Yii::t('documents', 'Id'),
	            'user' => Yii::t('main', 'User'),
	            'forPeriod' => Yii::t('web', 'For period'),
	            'generatedFor' => Yii::t('web', 'Generated for'),
	            'generationDate' => Yii::t('web', 'Generation date'),
	            'none' => Yii::t('web', 'None'),
	            'totalPrice' => Yii::t('web', 'Total price'),
	            'price' => Yii::t('web', 'Price'),
	            'name' => Yii::t('documents', 'Name'),
	            'value' => Yii::t('documents', 'Value'),
	            'M.u.' => Yii::t('documents', 'M.u.'),
	            'amount' => Yii::t('web', 'Amount'),
	            'pc.' => Yii::t('documents', 'pc.'),
        		'location' => Yii::t('documents', 'Location'),
        ];
    }

    public function getType() {
        return SectionsMapper::LIST_OF_ITEMS;
    }
}