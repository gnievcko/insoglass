<?php

namespace common\documents\sections;

use Yii;
use common\documents\sections\SectionsMapper;
use frontend;

class MalfunctionProtocol extends Section {

    public $dateOperation;
    public $companyName;
    public $companyAddress;
    public $reasonHours;
    public $repairHours;
    public $textArea1;
    public $textArea2;
    public $textArea3;
    public $textArea4;
    public $repairer;
    public $companyId;
    public $orderId;

    public function rules() {
        return [
                [['reasonHours', 'repairHours', 'orderId', 'companyId', 'dateOperation', 'companyName', 'companyAddress'], 'safe'],
        ];
    }

    public function init() {
        $this->dateOperation = (new \DateTime())->format('Y-m-................');
    }

    public function loadInput($input) {
        $orderId = null;
        $companyId = null;
        if(isset($input[$this->formName()]['orderId'])) {
            $order = \common\models\ar\Order::findOne(['id' => $input[$this->formName()]['orderId']]);
            if(!empty($order)) {
                $orderId = $order->id;
                $companyId = $order->company->id;
                $this->companyAddress = $this->companyAddress ?? $order->company->getAddressName();
                $this->companyName = $this->companyName ?? $order->company->name;
            }
        }
        
        parent::loadInput($input);
        $this->orderId = $orderId;
        $this->companyId = $companyId;
        
        if(isset($input[$this->formName()]['dateOperation'])) {
            $this->dateOperation = $input[$this->formName()]['dateOperation'];
        }

        if(isset($input[$this->formName()]['companyName'])) {
            $this->companyName = $input[$this->formName()]['companyName'];
        }

        if(isset($input[$this->formName()]['companyAddress'])) {
            $this->companyAddress = $input[$this->formName()]['companyAddress'];
        }

        if(isset($input[$this->formName()]['reasonHours'])) {
            $this->reasonHours = $input[$this->formName()]['reasonHours'];
        }

        if(isset($input[$this->formName()]['companyAddress'])) {
            $this->repairHours = $input[$this->formName()]['repairHours'];
        }

        if(isset($input[$this->formName()]['textArea1'])) {
            $this->textArea1 = $input[$this->formName()]['textArea1'];
        }

        if(isset($input[$this->formName()]['textArea2'])) {
            $this->textArea2 = $input[$this->formName()]['textArea2'];
        }

        if(isset($input[$this->formName()]['textArea3'])) {
            $this->textArea3 = $input[$this->formName()]['textArea3'];
        }

        if(isset($input[$this->formName()]['textArea4'])) {
            $this->textArea4 = $input[$this->formName()]['textArea4'];
        }

        if(isset($input[$this->formName()]['repairer'])) {
            $this->repairer = $input[$this->formName()]['repairer'];
        }

        $this->orderId = (int) $this->orderId;
        $this->companyId = (int) $this->companyId;
    }

    public function attributeLabels() {
        return [
            'onObject' => Yii::t('documents', 'On object:'),
            'from' => Yii::t('documents', 'from: '),
            'title' => Yii::t('documents', 'Break-down protocol'),
            'createdBy' => Yii::t('documents', 'CREATED BY'),
            'acceptedBy' => Yii::t('documents', 'ACCEPTED BY'),
            '(user)' => '('.Yii::t('documents', 'User').')',
            '(Repairer - Contractor)' => '('.Yii::t('documents', 'Repairer - Contractor').')',
            'companyName' => Yii::t('web', 'Client name'),
            'address' => Yii::t('main', 'Address'),
            'serviceArrivalConfirmed' => Yii::t('documents', 'Arrival of repair service has been confirmed.'),
            'followingTasksCompleted' => Yii::t('documents', 'Following tasks have been successfully completed'),
            'reasonHoursLabel' => Yii::t('documents', '1. Diagnosing and determining the reason of break-down - num of hours = '),
            'repairHoursLabel' => Yii::t('documents', '2. Actions related to fixing the break-down - num of hours = '),
            'firstHeader' => Yii::t('documents', 'Cause of break-down has been determined:'),
            'secondHeader' => Yii::t('documents', 'Scope of work:'),
            'thirdHeader' => Yii::t('documents', 'List of parts used to fix the break-down'),
            'fourthHeader' => Yii::t('documents', 'It has been stated that the break-down had been fixed and that the fire-fighting equipment works properly.'),
            'remarks' => Yii::t('documents', 'Remarks'),
            'repairer' => Yii::t('documents', 'Repairer'),
        ];
    }

    public function getType() {
        return SectionsMapper::MALFUNCTION_PROTOCOL;
    }

}
