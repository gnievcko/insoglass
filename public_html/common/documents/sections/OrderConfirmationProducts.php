<?php
namespace common\documents\sections;

use Yii;
use common\documents\sections\SectionsMapper;
use frontend\helpers\ModelLoaderHelper;
use common\documents\forms\OrderConfirmationProductForm;
use yii\helpers\ArrayHelper;
use common\models\ar\Currency;
use common\helpers\ArrayHelper as ArrayHelper1;
use common\models\ar\Language;

class OrderConfirmationProducts extends Section {
	
	public $products = [];
	public $currency;
	public $currencyId;
	
	public $languageId;
	
	public $currencies;
	
	public function loadInput($data) {
		$this->load($data);
		$classes = $this->getOrderConfirmationProductFormClass();
		$orderConfirmationProductFormClass = $classes['formName'];
		
		$this->languageId = ArrayHelper1::searchArrayForKey($data, 'languageId');
		
		if(isset($data[$this->formName()])) {
			if(isset($data[$this->formName()]['products'])) {
				$data[$orderConfirmationProductFormClass] = $data[$this->formName()]['products'];
			}
		}
		
		if(isset($data[$orderConfirmationProductFormClass]) && !empty($orderConfirmationProductFormClass)) {
			$this->products = ModelLoaderHelper::loadMultiple($data, $classes['class']);
			foreach($this->products as &$product) {
				if($product->vatRate > 1) {
					$product->vatRate /= 100;
				}
			}
		}
	}
	protected function getOrderConfirmationProductFormClass() {
		return [
				'formName' => (new OrderConfirmationProductForm())->formName(),
				'class' => OrderConfirmationProductForm::class,
		];
	}
	public function getLanguage(){
		return Language::find()->where(['id' => $this->languageId])->one()['symbol'];
	}
	public function getModels() {
		return [$this->products, $this];
	}
	
	public function rules() {
		return [
				[['products', 'currencyId'], 'safe'],
		];
	}
	
	public function init() {
		$this->currencies = Currency::find()->all();
		$currenciesLookup = array_reduce($this->currencies, function($lookup, $currency) {
			return $lookup + [$currency->symbol => $currency];
		}, []);
			
		$currencies = Currency::find()->all();

		$this->currencies = ArrayHelper::map($currencies, 'id', 'symbol');
		$this->currencyId = empty($currencies) ? null : reset($currencies)->id;
		
	}
	
	public function attributeLabels() {
		return [
				'currencyId' => Yii::t('main', 'Currency'),
				'count' => Yii::t('documents', 'Quantity'),
				'unit' => Yii::t('documents', 'M.u.'),
				'netPrice' => Yii::t('documents', 'Price'),
				'vatRate' => Yii::t('documents', 'VAT rate'),
				'grossValue' => Yii::t('documents', 'Subtotal'),
				'name' => Yii::t('documents', 'Product name'),
		];
	}

	public function getCurrencySymbol() {
		return empty($this->currencies) ? '' : $this->currencies[$this->currencyId];
	}
	public function getCurrencies() {
		return $this->currencies;
	}
	public function getType() {
		return SectionsMapper::ORDER_CONFIRMATION_PRODUCTS;	
	}
	public function getVatRates() {
		return Yii::$app->params['vatRate'];
	}
}