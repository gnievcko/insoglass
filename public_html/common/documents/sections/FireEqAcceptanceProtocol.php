<?php 

namespace common\documents\sections;

use Yii;
use common\documents\sections\SectionsMapper;
use frontend\helpers\ModelLoaderHelper;
use common\documents\forms\FireEqAcceptanceProductForm;
use frontend;

class FireEqAcceptanceProtocol extends Section {

    public $dateOperation;
    public $companyName;
    public $companyAddress;
    
    public $orderId;
    public $companyId;
    
    public $rows = [[],[],[],[]];
    
    public function rules() {
        return [
                [['orderId', 'companyId', 'rows', 'dateOperation', 'companyName', 'companyAddress'], 'safe']
            ];

    }

    public function init() {
        $this->dateOperation = (new \DateTime())->format('Y-m-................');
    }
    
    public function getModels() {
    	return [$this->rows, $this];
    }
    
    public function loadInput($input) {

    	if(isset($input[$this->formName()]['dateOperation'])) {
		    $this->dateOperation  = $input[$this->formName()]['dateOperation'];
    	}
    	
    	if(isset($input[$this->formName()]['companyName'])) {
    		$this->companyName  = $input[$this->formName()]['companyName'];
    	}
    	
    	if(isset($input[$this->formName()]['companyAddress'])) {
    		$this->companyAddress  = $input[$this->formName()]['companyAddress'];
    	}
        $orderId = null;
        $companyId = null;
        if(isset($input[$this->formName()]['orderId'])) {
            $order = \common\models\ar\Order::findOne(['id' => $input[$this->formName()]['orderId']]);
            if(!empty($order)) {
                $orderId = $order->id;
                $companyId = $order->company->id;
                $this->companyAddress = $this->companyAddress ?? $order->company->getAddressName();
                $this->companyName = $this->companyName ?? $order->company->name;
            }
        }
        if(!empty($input['FireEqAcceptanceProductForm'])) {
        	$this->rows = ModelLoaderHelper::loadMultiple($input, FireEqAcceptanceProductForm::class);
        }
        
        parent::loadInput($input);        
        $this->orderId = $orderId;
        $this->companyId = $companyId;
    }

    public function attributeLabels() {
        return [
	            'onObject' => Yii::t('documents', 'On object:'),
        		'from' => Yii::t('documents', 'from: '),
			    'title' => Yii::t('documents', 'PROTOCOL OF EXAMINATION OF FIRE SIGNALING SYSTEM'),
			    'createdBy' => Yii::t('documents', 'CREATED BY'),
			    'acceptedBy' => Yii::t('documents', 'ACCEPTED BY'),
        		'(user)' =>  '(' . Yii::t('documents', 'User') . ')',
        		'(Repairer - Contractor)' => '(' . Yii::t('documents', 'Repairer - Contractor') . ')',
        		'companyName' => Yii::t('web', 'Client name'),
        		'address' => Yii::t('main', 'Address'),
        ];
    }

    public function getType() {
        return SectionsMapper::FIREEQ_ACCEPTANCE_PROTOCOL;
    }
}