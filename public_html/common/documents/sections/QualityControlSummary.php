<?php

namespace common\documents\sections;

use Yii;
use common\models\ar\User;
use frontend\helpers\UserHelper;
use common\helpers\StringHelper;

class QualityControlSummary extends Section{
    
    public $dateFrom;
    public $dateTo;
    public $user;
    public $dateGenerated;

    public function init() {
    }

    public function rules() {
        return [];
    }

    public function loadInput($input) {
        $this->dateFrom = $input['dateFrom'];
        $this->dateTo = $input['dateTo'];
        
        $user = User::findOne(Yii::$app->user->id);
        $this->user = UserHelper::getPrettyUserName($user->email, $user->first_name, $user->last_name);
        $this->dateGenerated = StringHelper::getFormattedDateFromTimestamp(time(), true);
        
        $this->dateFrom = StringHelper::getFormattedDateFromDate($this->dateFrom);
        $this->dateTo = StringHelper::getFormattedDateFromDate($this->dateTo);
    }

    public function getType() {
        return SectionsMapper::QUALITY_CONTROL_SUMMARY;
    }
}