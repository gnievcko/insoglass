<?php
namespace common\documents\sections;

use Yii;
use common\documents\sections\SectionsMapper;
use frontend\helpers\ModelLoaderHelper;
use common\documents\forms\FireEqAcceptanceProductForm;

class FireEqAcceptanceProtocolProducts extends Section {

	public $products = [];

	public function loadInput($data) {
		$this->load($data);
		$this->products = ModelLoaderHelper::loadMultiple($data, FireEqAcceptanceProductForm::class);
	}

	public function getModels() {
		return [$this->products, $this];
	}

	public function rules() {
		return [

		];
	}

	public function init() {
		//$config = Yii::$app->params;
	}

	public function attributeLabels() {
		return [
				'index' => Yii::t('main', 'Warehouse index'),
				'name' => Yii::t('main', 'Name'),
				'count' => Yii::t('web', 'Count'),
		];
	}

	public function getType() {
		return SectionsMapper::FIREEQ_ACCEPTANCE_PRODUCTS;
	}
}
