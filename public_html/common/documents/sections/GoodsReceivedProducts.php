<?php

namespace common\documents\sections;

use Yii;
use common\documents\sections\SectionsMapper;
use frontend\helpers\ModelLoaderHelper;
use common\models\ar\Currency;
use common\documents\forms\GoodsReceivedProductForm;
use common\helpers\Utility;

class GoodsReceivedProducts extends Section {

    public $products = [];
    public $doubleName = false;
    
    private $labels = [];
    
    public function loadInput($data) {
        $this->load($data);

        if(isset($data[$this->formName()]['products']) && !empty($data[$this->formName()]['products'])) {
            $data[(new GoodsReceivedProductForm)->formName()]= $data[$this->formName()]['products'];
        }
        
        $this->products = ModelLoaderHelper::loadMultiple($data, GoodsReceivedProductForm::class);
            
        foreach($this->products as &$product) {
        	if(!empty($product->priceUnit)) {
        		$product->price = $product->priceUnit;
        		$product->unit = !empty($product->unit) ? $product->unit : Yii::t('documents', 'pc.');
        		$product->costSum = $product->count * $product->price;
        	}
        	elseif(!empty($product->priceGroup)) {
        		$product->price = $product->priceGroup;
        		$product->unit = Yii::t('documents', 'ps.').(!empty($product->unit) ? ' ('.$product->unit.')' : '');
        		$product->costSum = $product->price;
        	}
        	elseif(!empty($product->count) && !empty($product->price) && !empty($product->unit)) {
        		if(!empty($product->priceUnit)) {
        			$product->costSum = $product->price * $product->count;
        		}
        		elseif(!empty($product->priceGroup)) {
        			$product->costSum = $product->price;
        		}
        	}
        	
        	if(!empty($product->parentName)) {
        		$product->name = $product->parentName.' ('.mb_strtolower($product->name, 'UTF-8').')';
        	}
        }
        $this->setLabels();
    }

    public function getModels() {
        return [$this->products, $this];
    }

    public function rules() {
        return [
            ['doubleName', 'safe'],
        ];
    }

    public function init() {
        //$config = Yii::$app->params;
    }

    public function attributeLabels() {
        return $this->labels;
    }
    
    public function calculateSummary() {
    	$count = 0;
    	$price = 0;
    	
    	$currenciesLocal = Currency::find()->asArray(true)->all();
    	$currencyPln = $currenciesLocal[array_search(Utility::CURRENCY_PLN, array_column($currenciesLocal, 'symbol'))];
    	
    	foreach($this->products as $product) {
    		$count += $product->count;
    		
    		$priceParts = explode(' ', $product->costSum);
    		if(count($priceParts) < 2) {
    			$price += ($product->costSum * $currencyPln['conversion_value']); 
    		}
    		else {
    			$currencyKey = array_search($priceParts[1], array_column($currenciesLocal, 'short_symbol'));
    			if(!empty($currencyKey)) {
    				$price += ($price[0] * $currenciesLocal[$currencyKey]['conversion_value']);
    			}
    			else {
    				$price += ($price[0] * $currencyPln['conversion_value']);
    			}
    		}
    	}
    
    	return ['count' => $count, 'price' => $price, 'currency' => $currencyPln['short_symbol']];
    }

    public function getType() {
        return SectionsMapper::GOODS_RECEIVED_PRODUCTS;
    }
    
    private function setLabels() {
        if($this->doubleName) {
             $this->labels = [
                    'index' => Yii::t('main', 'Warehouse index').' / Warehouse index',
                    'name' => Yii::t('main', 'Name').' / Name',
                    'count' => Yii::t('web', 'Count').' / Count',
                    'price' => Yii::t('web', 'Price').' / Price',
                    'unit' => Yii::t('documents', 'Unit').' / Unit',
                    'costSum' => Yii::t('documents', 'Value').' / Value',
             		'total' => Yii::t('documents', 'Total').' / Total',
             		'note' => Yii::t('main', 'Description').' / Description',
            ];
        } else {
            $this->labels = [
                    'index' => Yii::t('main', 'Warehouse index'),
                    'name' => Yii::t('main', 'Name'),
                    'count' => Yii::t('web', 'Count'),
                    'price' => Yii::t('web', 'Price'),
                    'unit' => Yii::t('documents', 'Unit'),
                    'costSum' => Yii::t('documents', 'Value'),
            		'total' => Yii::t('documents', 'Total'),
            		'note' => Yii::t('main', 'Description'),
            ];
        }
    }
}
