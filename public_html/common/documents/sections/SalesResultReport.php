<?php

namespace common\documents\sections;

use Yii;
use common\documents\sections\SectionsMapper;
use frontend\helpers\UserHelper;
use common\helpers\StringHelper;
use common\models\ar\User;
use common\models\aq\DocumentQuery;
use common\models\aq\CompanyQuery;

class SalesResultReport extends Section {
	public $dateFrom;
	public $dateTo;
	public $user;
	public $dateGenerated;

	public $data;

    public function init() {
    	$piecesTo = explode( '.', $_GET['dateTo']);
    	$this->dateTo = $piecesTo[2] . '-' . $piecesTo[1] . '-' .$piecesTo[0] . ' 23:59:59';
    	$piecesFrom = explode( '.', $_GET['dateFrom']);
    	$this->dateFrom = $piecesFrom[2] . '-' . $piecesFrom[1] . '-' .$piecesFrom[0] . ' 00:00:00';
    	
    	$invoicedOrdersIds = DocumentQuery::getOrderIdsFromInvoices($this->dateFrom, $this->dateTo);
    	$this->data = CompanyQuery::getSalesResultReportData($this->dateFrom, $this->dateTo, $invoicedOrdersIds);
    	
        $user = User::findOne(Yii::$app->user->id);
    	$this->user = UserHelper::getPrettyUserName($user->email, $user->first_name, $user->last_name);
    	$this->dateGenerated = StringHelper::getFormattedDateFromTimestamp(time(), true);
    	
    	$this->dateFrom = StringHelper::getFormattedDateFromDate($this->dateFrom);
    	$this->dateTo = StringHelper::getFormattedDateFromDate($this->dateTo);
    }

    public function attributeLabels() {
        return [
        		'report-sales-result' => Yii::t('documents', 'REPORT - SALES RESULT'),
        		'forPeriod' => Yii::t('web', 'For period'),
        		'generatedFor' => Yii::t('web', 'Generated for'),
        		'generationDate' => Yii::t('web', 'Generation date'),
        		
        		'name' => Yii::t('web', 'Name'),
        		'newClients' => Yii::t('web', 'New clients'),
        		'ordersForClients' => StringHelper::translateOrderToOffer('web', 'Orders for clients'),
        		'invoicedClients' => Yii::t('web', 'Invoiced clients'),
        ];
    }


    public function getType() {
        return SectionsMapper::SALES_RESULT_REPORT;
    }
}
