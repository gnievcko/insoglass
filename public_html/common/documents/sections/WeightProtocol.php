<?php

namespace common\documents\sections;

use Yii;
use common\documents\sections\SectionsMapper;
use common\models\ar\InformationPage;
use common\helpers\Utility;
use yii\helpers\Json;

class WeightProtocol extends Section {

    public $tables = [['theads' => ['', '', '', '', ''], 'rows' => [[]]]];
    public $form;
    public $type;
    public $date;
    public $object;
    public $repairer;
    public $measurementTool;
    public $remarks;
    public $text;
    public $orderId;
    public $companyId;
    public $numberList = [1, 2, 3, 4, 5, 6, 7];
    public $key;
        
    public function rules() {
        return [
                [['object', 'date', 'repairer', 'measurementTool', 'remarks', 'text', 'form', 'type', 
                		'orderId', 'companyId', 'tables', 'key'], 'safe'],
        ];
    }

   public function loadInput($input) {        
        $orderId = null;
        $companyId = null;
        
        if(isset($input[$this->formName()]['orderId'])) {
            $order = \common\models\ar\Order::findOne(['id' => $input[$this->formName()]['orderId']]);
            if(!empty($order)) {
               $orderId = $order->id;
                $companyId = $order->company->id;
                $this->object = $this->object ?? $order->company->name.', '.$order->company->getAddressName();
            }
        }
        parent::loadInput($input);

        $this->orderId = $orderId;
        $this->companyId = $companyId;
    }

    public function init() {
        $config = Yii::$app->params;
        
        $defaultTables = InformationPage::find()->where(['symbol' => Utility::INFORMATION_PAGE_DEFAULT_WEIGHT_TABLES])->one()->getTranslation();
        if(!empty($defaultTables)) {
        	$tbl = Json::decode(strip_tags($defaultTables->content));
        	if(!empty($tbl)) {
        		$this->tables = [];
        		foreach($tbl['tables'] as $arr) {
        			$this->tables = array_merge($this->tables, $arr);
        		}
        	}
        }
        
        $defaultAdditionalText = InformationPage::find()->where(['symbol' => Utility::INFORMATION_PAGE_DEFAULT_WEIGHT_ADDITIONAL_TEXT])->one()->getTranslation();
        if(!empty($defaultAdditionalText)) {
        	$this->text = trim(strip_tags($defaultAdditionalText->content, '<br/>'));
        }
        
        $defaultKey = InformationPage::find()->where(['symbol' => Utility::INFORMATION_PAGE_DEFAULT_WEIGHT_KEY])->one()->getTranslation();
        if(!empty($defaultKey)) {
        	$this->key = $defaultKey->content;
        }
    }

    public function attributeLabels() {
        return [
            	'date' => Yii::t('documents', 'from: '),
            	'object' => Yii::t('documents', 'On object:'),
            	'repairer' => Yii::t('documents', 'Repairer'),
            	'measurementTool' => Yii::t('documents', 'Measurement tool'),
            	'text' => Yii::t('documents', 'Additional text'),
            	'remarks' => Yii::t('documents', 'Protocol remarks'),
        		'key' => Yii::t('documents', 'Key'),
        ];
    }

    public function getType() {
        return SectionsMapper::WEIGHT_PROTOCOL;
    }

}
