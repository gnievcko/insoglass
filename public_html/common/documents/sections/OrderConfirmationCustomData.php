<?php
namespace common\documents\sections;

use Yii;
use common\documents\sections\SectionsMapper;
use common\helpers\ArrayHelper;
use common\models\ar\Language;

class OrderConfirmationCustomData extends Section {

	public $companyBankAccount;
	public $companySwift;
	public $companyBank;
	public $prepaymentAmount;

	public $languageId;
	
	public function rules() {
		return [
				[['companyBankAccount', 'companySwift', 'languageId',
						'companyBank', 'prepaymentAmount', ], 'safe'],
		];
	}
	public function loadInput($data) {
		
		if(isset($data[$this->formName()])) {
			$this->load($data);
		}
		$this->languageId = ArrayHelper::searchArrayForKey($data, 'languageId');
		
	}
	public function getLanguage(){
		return Language::find()->where(['id' => $this->languageId])->one()['symbol'];
	}
	public function attributeLabels() {
		return [
				'companyBankAccount' => Yii::t('documents', 'Bank account'),
				'companySwift' => Yii::t('documents', 'Swift code'),
				'prepaymentAmount' => Yii::t('documents', 'To be paid'),
				'companyBank' => Yii::t('documents', 'Bank name'),
		];
	}
	
	public function getType() {
		return SectionsMapper::ORDER_CONFIRMATION_CUSTOM_DATA;
	}
}