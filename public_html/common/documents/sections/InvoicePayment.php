<?php

namespace common\documents\sections;

use Yii;
use common\documents\sections\SectionsMapper;

class InvoicePayment extends Section {

    public $paymentMethod;
    public $paymentDate;
    public $remarks;
    public $dayList = [];
    public $methodList = [];
    
    public function __construct($config = array()) {
        parent::__construct($config);
        $this->methodList = [
                '' => Yii::t('documents', 'Select payment method'),
                Yii::t('documents', 'Cash paid') => Yii::t('documents', 'Cash paid'),
                Yii::t('documents', 'Money transfer') => Yii::t('documents', 'Money transfer'),
                Yii::t('documents', 'Compensation') =>  Yii::t('documents', 'Compensation'), 
        ];
        $this->dayList = [
                '' => Yii::t('documents', 'Select number of days'),
                7 => 7,
                14 => 14,
        		21 => 21,
                30 => 30,
        		45 => 45,
                60 => 60,
                90 => 90
        ]; 
    }
    
    public function rules() {
        return [
            [['paymentDate'], 'required'],
            [['paymentDate'], 'date', 'format' => 'php:Y-m-d', 'message' => Yii::t('web', 'Invalid date format')],
            [['remarks', 'paymentMethod'], 'string'],
        ];
    }

    public function attributeLabels() {
        return [
            'paymentMethod' => Yii::t('documents', 'Payment method'),
            'paymentDate' => Yii::t('documents', 'Payment date'),
            'remarks' => Yii::t('documents', 'Remarks'),
        ];
    }

    public function getType() {
        return SectionsMapper::INVOICE_PAYMENT;
    }
}
