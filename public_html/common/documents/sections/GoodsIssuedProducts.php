<?php

namespace common\documents\sections;

use Yii;
use common\documents\sections\SectionsMapper;
use frontend\helpers\ModelLoaderHelper;
use common\documents\forms\GoodsIssuedProductForm;

class GoodsIssuedProducts extends Section {

    public $products = [];
    
    public function loadInput($data) {
        $this->load($data);
        if(isset($data[$this->formName()]['products']) && !empty($data[$this->formName()]['products'])) {
            $data[(new GoodsIssuedProductForm)->formName()]= $data[$this->formName()]['products'];
        }
        
        $this->products = ModelLoaderHelper::loadMultiple($data, GoodsIssuedProductForm::class);

        foreach($this->products as &$product) {
        	if(!empty($product->parentName)) {
        		$product->name = $product->parentName.' ('.mb_strtolower($product->name, 'UTF-8').')';
        	}
        	
        	if(!empty($product->stillage) || !empty($product->shelf)) {
        		$location = (!empty($product->stillage)) ? $product->stillage : '-';
        		$location .= '/';
        		$location .= (!empty($product->shelf)) ? $product->shelf : '-';
        		$product->location = $location;
        	}
        }
    }

    public function getModels() {
        return [$this->products, $this];
    }

    public function rules() {
        return [

        ];
    }

    public function init() {
        //$config = Yii::$app->params;
    }

    public function attributeLabels() {
        return [
        		'location' => Yii::t('web', 'Location'),
        		'name' => Yii::t('main', 'Name'),
        		'count' => Yii::t('web', 'Count'),
        		'unit' => Yii::t('main', 'Unit'),
        ];
    }

    public function getType() {
        return SectionsMapper::GOODS_ISSUED_PRODUCTS;
    }
}
