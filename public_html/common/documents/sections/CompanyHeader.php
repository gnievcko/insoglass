<?php

namespace common\documents\sections;

use Yii;
use common\documents\sections\SectionsMapper;
use common\models\ar\Order;
use common\models\aq\EntityQuery;
use common\helpers\ArrayHelper;

class CompanyHeader extends Section {

    public $companyLogo;
    public $companyCertificateLogo;
    public $companyFullName;
    public $companyAddress;
    public $companyVatId;
    public $companyBankAccount;
    public $companySwift;
    public $companyBank;

    public function rules() {
        return [
        		[['companyLogo', 'companyCertificateLogo', 'companyFullName', 'companyAddress', 'companyVatId', 
        				'companyBankAccount', 'companySwift', 'companyBank'], 'safe'],
        ];
    }
    
    public function loadInput($data) {
    	$config = Yii::$app->params;
    	$this->companyLogo = $config['owner']['logo'];
    	$this->companyCertificateLogo = $config['owner']['certificateLogo'];
    	$this->companyFullName = $config['owner']['name'];
    	$this->companyAddress = $config['owner']['address'];
    	$this->companyVatId = $config['owner']['vatId'];
    	$this->companyBankAccount = $config['owner']['bank'];
    	$this->companySwift = $config['owner']['swift'];
    	$this->companyBank = $config['owner']['bankAddress'];

    	if(isset($data[$this->formName()])) {
    		$this->load($data);
    	}
    	
    	$orderId = Yii::$app->request->get('orderId');
    	if(empty($orderId)) {
    		$orderId = ArrayHelper::searchArrayForKey($data, 'orderId');
    	}
    	
    	if(!empty($orderId)) {
    		$order = Order::findOne($orderId);
    		if(!empty($order)) {
    			$entity = $order->entity;
    			if(empty($entity)) {
    				$entity = EntityQuery::getDefaultEntity();
    			}
    			
    			if(!empty($entity)) {
    				$this->companyFullName = $entity->name;
    				$this->companyAddress = $entity->address->getFullAddress(true);
    				$this->companyVatId = $entity->vat_identification_number;
    				$accountParts = explode(', SWIFT: ', $entity->account_number);
    				$this->companyBankAccount = $accountParts[0];
    				$this->companySwift = (count($accountParts) > 1 ? $accountParts[1] : '');
    				$this->companyBank = $entity->bank_name;
    				
    				if(!empty($entity->url_logo)) {
    					$this->companyLogo = '@web/images/'.$entity->url_logo;
    				}
    				
    				if(empty($entity->is_certified)) {
	    				$this->companyCertificateLogo = '';
	    			}
    			}
    		}
    	}
    }

    public function attributeLabels() {
        return [
            'companyVatId' => Yii::t('documents', 'VAT ID'),
            'companyBankAccount' => Yii::t('documents', 'Bank account'),
            'companySwift' => Yii::t('documents', 'SWIFT'),
        ];
    }       

    public function getType() {
        return SectionsMapper::COMPANY_HEADER;
    }
}
