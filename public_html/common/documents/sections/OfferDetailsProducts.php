<?php

namespace common\documents\sections;

use Yii;
use common\documents\sections\SectionsMapper;
use frontend\helpers\ModelLoaderHelper;
use common\documents\forms\OfferDetailsProductForm;
use yii\helpers\ArrayHelper;
use common\models\ar\Currency;

class OfferDetailsProducts extends Section {

    public $products = [];
    public $summaryCost = 0;
    public $currency;
    public $currencyId;
    
    public $currencies;

    public function loadInput($data) {
        $this->load($data);
        $this->currency = Currency::findOne($this->currencyId);
        if(isset($data[$this->formName()])) {
            if(!isset($data[(new OfferDetailsProductForm())->formName()]) && isset($data[$this->formName()]['products'])) {
                $data[(new OfferDetailsProductForm())->formName()] = $data[$this->formName()]['products'];
            }
        }
        if(isset($data[(new OfferDetailsProductForm())->formName()])) {
            $this->products = ModelLoaderHelper::loadMultiple($data, OfferDetailsProductForm::class);
        }

        $this->calculateSummary();
    }
    
    public function getModels() {
    	return [$this->products, $this];
    }

    public function rules() {
        return [
            [['products', 'summaryCost', 'currencyId'], 'safe'],
        ];
    }

    public function init() {
        $this->currencies = Currency::find()->all();
        $currenciesLookup = array_reduce($this->currencies, function($lookup, $currency) {
            return $lookup + [$currency->symbol => $currency];
        }, []);
        
        $this->currency = $currenciesLookup[\common\helpers\Utility::CURRENCY_PLN];
        $this->currencyId = $this->currency->id;
    }
    
    private function calculateSummary() {
    	$conversionValuesLookup = ArrayHelper::map($this->currencies, 'id', 'conversion_value');
    	$this->summaryCost = array_reduce($this->products, function($summary, $product) use($conversionValuesLookup) {
    		return $summary + bcmul(
    				bcmul($product->price, ArrayHelper::getValue($conversionValuesLookup, $this->currencyId, 0), 2), 
    				$product->count, 2);
    	}, '0.00');
    }

    public function attributeLabels() {
        return [
	            'currencyId' => Yii::t('main', 'Currency'),
    	        'assortment' => Yii::t('web', 'Assortment'),
            	'priceUnit' => Yii::t('documents_orders', 'Price per unit'),
            	'count' => Yii::t('documents_orders', 'Count'),
            	'priceTotal' => Yii::t('documents_orders', 'Total price'),
        		'unit' => Yii::t('documents', 'M.u.'),		
        ];
    }
    
    public function getCurrencies() {
    	return $this->currencies;
    }

    public function getType() {
        return SectionsMapper::OFFER_DETAILS_PRODUCTS;
    }
}
