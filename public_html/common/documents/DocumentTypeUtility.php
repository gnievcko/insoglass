<?php

namespace common\documents;
use common\documents\DocumentType;
use yii\helpers\ArrayHelper;
use common\models\aq\DocumentTypeQuery;
class DocumentTypeUtility {
	
    const URL = [
	        DocumentType::INVOICE => 'document/invoice',
	        DocumentType::ACCEPTANCE_PROTOCOL => 'documents/acceptance-protocol',
	        DocumentType::GOODS_ISSUED => 'document/generate-goods-issued',
            DocumentType::GOODS_RECEIVED => 'document/generate-goods-received',
	        DocumentType::OFFER_DETAILS => 'documents/offer-details',
	        DocumentType::LIGHT_MEASUREMENT => 'protocol/light-measurement',
	        DocumentType::SERVICE_PROTOCOL => 'protocol/service',
	        DocumentType::WEIGHT_PROTOCOL => 'protocol/weight',
	        DocumentType::REPAIR_PROTOCOL => 'protocol/repair',
	        DocumentType::FIREEQ_ACCEPTANCE_PROTOCOL => 'document/generate-firefighting-eq-acceptance-protocol',
	        DocumentType::MALFUNCTION_PROTOCOL => 'document/generate-malfunction-protocol',
    		DocumentType::CUSTOM_PROTOCOL => 'protocol/custom',
            DocumentType::DELIVERY_DETAILS => 'document/delivery-details',        
    		DocumentType::PROFORMA_INVOICE => 'document/proforma-invoice',
    		DocumentType::CORRECTIVE_INVOICE => 'document/corrective-invoice',
    		DocumentType::ADVANCE_INVOICE => 'document/advance-invoice',
    		DocumentType::DUPLICATE_INVOICE => 'document/duplicate-invoice',
    		DocumentType::DUPLICATE_CORRECTIVE_INVOICE => 'document/duplicate-corrective-invoice',
    		DocumentType::SERVICE_PROTOCOL_FDAFAS => 'protocol/service-fdafas',
    		DocumentType::ORDER_CONFIRMATION => 'documents/order-confirmation',
            DocumentType::PRODUCTION_PROGRESS => 'documents/production-progress',
            DocumentType::INTERMEDIATE_PRODUCTS => 'documents/intermediate-products',
    ];
    
    static function translation() {
        $query = new \yii\db\Query;
        $data = $query->select('document_type.symbol as symbol, document_type_translation.name as name')
                ->from('document_type')
                ->leftJoin('document_type_translation', 'document_type.id = document_type_translation.document_type_id')
                ->leftJoin('language', 'document_type_translation.language_id = document_type_translation.language_id')
                ->where([ 'language.symbol' => [\Yii::$app->language, null] ])
                ->all();

        return ArrayHelper::map($data, 'symbol', 'name');
    }

    public static function getAvailableDocuments() {
    	return array_column(DocumentTypeQuery::getAvailableDocumentTypes(), 'symbol');    	
    }
}
