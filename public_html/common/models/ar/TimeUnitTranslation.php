<?php

namespace common\models\ar;

use Yii;

/**
 * This is the model class for table "time_unit_translation".
 *
 * @property integer $time_unit_id
 * @property integer $language_id
 * @property integer $value
 * @property string $name
 *
 * @property TimeUnit $timeUnit
 * @property Language $language
 */
class TimeUnitTranslation extends \yii\db\ActiveRecord {

    /**
     * @inheritdoc
     */
    public static function tableName() {
        return 'time_unit_translation';
    }

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
				[['time_unit_id', 'language_id', 'value', 'name'], 'required'],
				[['time_unit_id', 'language_id', 'value'], 'integer'],
				[['name'], 'string', 'max' => 128],
				[['time_unit_id'], 'exist', 'skipOnError' => true, 'targetClass' => TimeUnit::className(), 'targetAttribute' => ['time_unit_id' => 'id']],
				[['language_id'], 'exist', 'skipOnError' => true, 'targetClass' => Language::className(), 'targetAttribute' => ['language_id' => 'id']]
		];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return [
            	'timeUnitSymbol' => Yii::t('main', 'Time unit'),
            	'languageName' => Yii::t('main', 'Language'),
            	'value' => Yii::t('main', 'Value'),
            	'name' => Yii::t('main', 'Name'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTimeUnit() {
        return $this->hasOne(TimeUnit::className(), ['id' => 'time_unit_id']);
    }
    
    public function getTimeUnitSymbol() {
    	return $this->timeUnit->symbol;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLanguage() {
        return $this->hasOne(Language::className(), ['id' => 'language_id']);
    }
    
    public function getLanguageName() {
    	return $this->language->name;
    }

    /**
     * @inheritdoc
     * @return \common\models\aq\TimeUnitTranslationQuery the active query used by this AR class.
     */
    public static function find() {
        return new \common\models\aq\TimeUnitTranslationQuery(get_called_class());
    }
}
