<?php

namespace common\models\ar;

use Yii;

/**
 * This is the model class for table "task".
 *
 * @property integer $id
 * @property integer $task_type_id
 * @property integer $task_priority_id
 * @property integer $user_id
 * @property string $title
 * @property string $message
 * @property integer $user_notified_id
 * @property string $date_reminder
 * @property string $date_deadline
 * @property integer $is_complete 
 * @property integer $user_completing_id 
 * @property string $date_completion 
 * @property string $date_creation
 *
 * @property Alert[] $alerts
 * @property TaskPriority $taskPriority
 * @property TaskType $taskType
 * @property User $user
 * @property User $userCompleting
 * @property User $userNotified
 * @property TaskData[] $taskDatas
 * @property TaskTypeReference[] $taskTypeReferences
 */
class Task extends \yii\db\ActiveRecord {

    /**
     * @inheritdoc
     */
    public static function tableName() {
        return 'task';
    }

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
				[['task_type_id', 'task_priority_id', 'user_id', 'title', 'message'], 'required'],
				[['task_type_id', 'task_priority_id', 'user_id', 'user_notified_id', 'is_active', 'is_complete', 'user_completing_id'], 'integer'],
				[['date_reminder', 'date_deadline', 'date_completion', 'date_creation'], 'safe'],
        		[['title'], 'string', 'max' => 128],
				[['message'], 'string', 'max' => 2048],
				[['task_priority_id'], 'exist', 'skipOnError' => true, 'targetClass' => TaskPriority::className(), 'targetAttribute' => ['task_priority_id' => 'id']],
				[['task_type_id'], 'exist', 'skipOnError' => true, 'targetClass' => TaskType::className(), 'targetAttribute' => ['task_type_id' => 'id']],
				[['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['user_id' => 'id']],
				[['user_completing_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['user_completing_id' => 'id']],
				[['user_notified_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['user_notified_id' => 'id']]
		];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return [
            	'id' => Yii::t('main', 'ID'),
            	'task_type_id' => Yii::t('main', 'Task Type ID'),
            	'task_priority_id' => Yii::t('main', 'Task Priority ID'),
            	'user_id' => Yii::t('main', 'User'),
        		'title' => Yii::t('main', 'Title'),
            	'message' => Yii::t('main', 'Message'),
            	'user_notified_id' => Yii::t('main', 'User Notified ID'),
            	'date_reminder' => Yii::t('main', 'Date Reminder'),
            	'date_deadline' => Yii::t('main', 'Date Deadline'),
            	'is_active' => Yii::t('main', 'Is Active'),
            	'is_complete' => Yii::t('main', 'Is Complete'), 
               	'user_completing_id' => Yii::t('main', 'User Completing ID'), 
               	'date_completion' => Yii::t('main', 'Date Completion'), 
            	'date_creation' => Yii::t('main', 'Date Creation'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAlerts() {
        return $this->hasMany(Alert::className(), ['task_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTaskPriority() {
        return $this->hasOne(TaskPriority::className(), ['id' => 'task_priority_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTaskType() {
        return $this->hasOne(TaskType::className(), ['id' => 'task_type_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser() {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
	public function getUserCompleting() {
       	return $this->hasOne(User::className(), ['id' => 'user_completing_id']);
   	}

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUserNotified() {
        return $this->hasOne(User::className(), ['id' => 'user_notified_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTaskDatas() {
        return $this->hasMany(TaskData::className(), ['task_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTaskTypeReferences() {
        return $this->hasMany(TaskTypeReference::className(), ['id' => 'task_type_reference_id'])->viaTable('task_data', ['task_id' => 'id']);
    }

    /**
     * @inheritdoc
     * @return \common\models\aq\TaskQuery the active query used by this AR class.
     */
    public static function find() {
        return new \common\models\aq\TaskQuery(get_called_class());
    }
}
