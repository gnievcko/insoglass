<?php

namespace common\models\ar;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\Expression;

/**
 * This is the model class for table "order_history_photo".
 *
 * @property integer $id
 * @property integer $order_history_id
 * @property integer $order_offered_product_id
 * @property integer $user_id
 * @property string $url_photo
 * @property string $url_thumbail
 * @property string $description
 * @property string $date_creation
 *
 * @property User $user
 * @property OrderHistory $orderHistory
 * @property OrderOfferedProduct $orderOfferedProduct
 */
class OrderHistoryPhoto extends \yii\db\ActiveRecord {

    /**
     * @inheritdoc
     */
    public static function tableName() {
        return 'order_history_photo';
    }

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
				[['order_history_id', 'user_id', 'url_photo', 'url_thumbnail'], 'required'],
				[['order_history_id', 'order_offered_product_id', 'user_id', 'url_thumbnail'], 'integer'],
				[['date_creation'], 'safe'],
				[['url_photo'], 'string', 'max' => 256],
				[['description'], 'string', 'max' => 512],
				[['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['user_id' => 'id']],
				[['order_history_id'], 'exist', 'skipOnError' => true, 'targetClass' => OrderHistory::className(), 'targetAttribute' => ['order_history_id' => 'id']],
				[['order_offered_product_id'], 'exist', 'skipOnError' => true, 'targetClass' => OrderOfferedProduct::className(), 'targetAttribute' => ['order_offered_product_id' => 'id']]
		];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return [
            	'id' => Yii::t('main', 'ID'),
            	'order_history_id' => Yii::t('main', 'Order History ID'),
            	'order_offered_product_id' => Yii::t('main', 'Order Offered Product ID'),
            	'user_id' => Yii::t('main', 'User'),
            	'url_photo' => Yii::t('main', 'Url Photo'),
            	'description' => Yii::t('main', 'Description'),
            	'date_creation' => Yii::t('main', 'Date Creation'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser() {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrderHistory() {
        return $this->hasOne(OrderHistory::className(), ['id' => 'order_history_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrderOfferedProduct() {
        return $this->hasOne(OrderOfferedProduct::className(), ['id' => 'order_offered_product_id']);
    }

    /**
     * @inheritdoc
     * @return \common\models\aq\OrderHistoryPhotoQuery the active query used by this AR class.
     */
    public static function find() {
        return new \common\models\aq\OrderHistoryPhotoQuery(get_called_class());
    }
    
    public function behaviors() {
    	return [
    			[
    					'class' => TimestampBehavior::className(),
    					'createdAtAttribute' => 'date_creation',
    					'updatedAtAttribute' => false,
    					'value' => new Expression('NOW()'),
    			],
    	];
    }
}
