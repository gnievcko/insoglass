<?php

namespace common\models\ar;

use Yii;

/**
 * This is the model class for table "task_priority_translation".
 *
 * @property integer $task_priority_id
 * @property integer $language_id
 * @property string $name
 *
 * @property TaskPriority $taskPriority
 * @property Language $language
 */
class TaskPriorityTranslation extends \yii\db\ActiveRecord {

    /**
     * @inheritdoc
     */
    public static function tableName() {
        return 'task_priority_translation';
    }

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
				[['task_priority_id', 'language_id', 'name'], 'required'],
				[['task_priority_id', 'language_id'], 'integer'],
				[['name'], 'string', 'max' => 256],
				[['task_priority_id'], 'exist', 'skipOnError' => true, 'targetClass' => TaskPriority::className(), 'targetAttribute' => ['task_priority_id' => 'id']],
				[['language_id'], 'exist', 'skipOnError' => true, 'targetClass' => Language::className(), 'targetAttribute' => ['language_id' => 'id']]
		];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return [
            	'task_priority_id' => Yii::t('main', 'Task Priority ID'),
            	'language_id' => Yii::t('main', 'Language ID'),
            	'name' => Yii::t('main', 'Name'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTaskPriority() {
        return $this->hasOne(TaskPriority::className(), ['id' => 'task_priority_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLanguage() {
        return $this->hasOne(Language::className(), ['id' => 'language_id']);
    }

    /**
     * @inheritdoc
     * @return \common\models\aq\TaskPriorityTranslationQuery the active query used by this AR class.
     */
    public static function find() {
        return new \common\models\aq\TaskPriorityTranslationQuery(get_called_class());
    }
}
