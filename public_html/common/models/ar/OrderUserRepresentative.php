<?php

namespace common\models\ar;

use Yii;

/**
 * This is the model class for table "order_user_representative".
 *
 * @property integer $order_id
 * @property integer $user_representative_id
 *
 * @property User $userRepresentative
 * @property Order $order
 */
class OrderUserRepresentative extends \yii\db\ActiveRecord {

    /**
     * @inheritdoc
     */
    public static function tableName() {
        return 'order_user_representative';
    }

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
				[['order_id', 'user_representative_id'], 'required'],
				[['order_id', 'user_representative_id'], 'integer'],
				[['user_representative_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['user_representative_id' => 'id']],
				[['order_id'], 'exist', 'skipOnError' => true, 'targetClass' => Order::className(), 'targetAttribute' => ['order_id' => 'id']]
		];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return [
            	'order_id' => Yii::t('main', 'Order'),
            	'user_representative_id' => Yii::t('main', 'User Representative ID'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUserRepresentative() {
        return $this->hasOne(User::className(), ['id' => 'user_representative_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrder() {
        return $this->hasOne(Order::className(), ['id' => 'order_id']);
    }

    /**
     * @inheritdoc
     * @return \common\models\aq\OrderUserRepresentativeQuery the active query used by this AR class.
     */
    public static function find() {
        return new \common\models\aq\OrderUserRepresentativeQuery(get_called_class());
    }
}
