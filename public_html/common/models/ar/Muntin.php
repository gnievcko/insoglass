<?php

namespace common\models\ar;

use Yii;

/**
 * This is the model class for table "muntin".
 *
 * @property integer $id
 * @property string $symbol
 *
 * @property MuntinTranslation[] $muntinTranslations
 * @property Language[] $languages
 * @property OrderOfferedProduct[] $orderOfferedProducts
 */
class Muntin extends \yii\db\ActiveRecord {

    /**
     * @inheritdoc
     */
    public static function tableName() {
        return 'muntin';
    }

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
			[['symbol'], 'required'],
			[['symbol'], 'string', 'max' => 32],
			[['symbol'], 'unique']
		];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return [
            'id' => Yii::t('main', 'ID'),
            'symbol' => Yii::t('main', 'Symbol'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMuntinTranslations() {
        return $this->hasMany(MuntinTranslation::className(), ['muntin_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLanguages() {
        return $this->hasMany(Language::className(), ['id' => 'language_id'])->viaTable('muntin_translation', ['muntin_id' => 'id']);
    }

    public function getTranslation() {
        $translations = $this->muntinTranslations;
        if(empty($translations)) {
            return null;
        }
         
        foreach($translations as $t) {
            if($t->language->symbol === Yii::$app->language) {
                return $t;
            }
        }
         
        return $translations[0];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrderOfferedProducts() {
        return $this->hasMany(OrderOfferedProduct::className(), ['muntin_id' => 'id']);
    }

    /**
     * @inheritdoc
     * @return \common\models\aq\MuntinQuery the active query used by this AR class.
     */
    public static function find() {
        return new \common\models\aq\MuntinQuery(get_called_class());
    }
}
