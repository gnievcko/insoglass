<?php

namespace common\models\ar;

use Yii;

/**
 * This is the model class for table "ral_colour".
 *
 * @property integer $id
 * @property integer $ral_colour_group_id
 * @property string $symbol
 * @property string $rgb_hash
 *
 * @property RalColourGroup $ralColourGroup
 * @property RalColourTranslation[] $ralColourTranslations
 * @property Language[] $languages
 */
class RalColour extends \yii\db\ActiveRecord {

    /**
     * @inheritdoc
     */
    public static function tableName() {
        return 'ral_colour';
    }

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
			[['ral_colour_group_id'], 'integer'],
			[['symbol', 'rgb_hash'], 'required'],
			[['symbol'], 'string', 'max' => 32],
			[['rgb_hash'], 'string', 'max' => 7],
			[['symbol'], 'unique'],
			[['ral_colour_group_id'], 'exist', 'skipOnError' => true, 'targetClass' => RalColourGroup::className(), 'targetAttribute' => ['ral_colour_group_id' => 'id']]
		];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return [
            'id' => Yii::t('main', 'ID'),
            'ral_colour_group_id' => Yii::t('main', 'Ral Colour Group ID'),
            'symbol' => Yii::t('main', 'Symbol'),
            'rgb_hash' => Yii::t('main', 'Rgb Hash'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRalColourGroup() {
        return $this->hasOne(RalColourGroup::className(), ['id' => 'ral_colour_group_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRalColourTranslations() {
        return $this->hasMany(RalColourTranslation::className(), ['ral_colour_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLanguages() {
        return $this->hasMany(Language::className(), ['id' => 'language_id'])->viaTable('ral_colour_translation', ['ral_colour_id' => 'id']);
    }

    /**
     * @inheritdoc
     * @return \common\models\aq\RalColourQuery the active query used by this AR class.
     */
    public static function find() {
        return new \common\models\aq\RalColourQuery(get_called_class());
    }
}
