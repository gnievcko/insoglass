<?php

namespace common\models\ar;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\Expression;

/**
 * This is the model class for table "company".
 *
 * @property integer $id
 * @property integer $parent_company_id
 * @property string $name
 * @property string $email
 * @property string $phone1
 * @property string $phone2
 * @property string $fax1
 * @property string $vat_identification_number
 * @property string $taxpayer_identification_number
 * @property string $krs_number
 * @property integer $address_id
 * @property integer $address_postal_id
 * @property string $note
 * @property integer $language_id
 * @property integer $currency_id
 * @property integer $is_artificial
 * @property integer $is_active
 * @property string $date_creation
 * @property string $date_modification
 * @property integer $user_id
 *
 * @property Address $address
 * @property Address $addressPostal
 * @property Currency $currency
 * @property Language $language
 * @property Company $parentCompany
 * @property Company[] $companies
 * @property Order[] $orders
 * @property User[] $users
 * @property CompanyGroupSet[] $companyGroupSets 
 * @property CompanyGroup[] $companyGroups 
 * @property Alert[] $alerts 
 */
class Company extends \yii\db\ActiveRecord {

    /**
     * @inheritdoc
     */
    public static function tableName() {
        return 'company';
    }

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
				[['parent_company_id', 'address_id', 'address_postal_id', 'language_id', 'currency_id', 'is_artificial', 'is_active'], 'integer'],
				[['name'], 'required'],
				[['date_creation', 'date_modification'], 'safe'],
				[['name'], 'string', 'max' => 128],
				[['email'], 'string', 'max' => 64],
        		[['note'], 'string', 'max' => 2048],
        		[['email'], 'email'],
				[['phone1', 'phone2', 'fax1', 'vat_identification_number', 'taxpayer_identification_number', 'krs_number'], 'string', 'max' => 32],
        		[['email', 'phone1', 'phone2', 'fax1', 'vat_identification_number', 'taxpayer_identification_number', 'krs_number'], 'default', 'value' => null],
				[['address_id'], 'exist', 'skipOnError' => true, 'targetClass' => Address::className(), 'targetAttribute' => ['address_id' => 'id']],
				[['address_postal_id'], 'exist', 'skipOnError' => true, 'targetClass' => Address::className(), 'targetAttribute' => ['address_postal_id' => 'id']],
				[['currency_id'], 'exist', 'skipOnError' => true, 'targetClass' => Currency::className(), 'targetAttribute' => ['currency_id' => 'id']],
				[['language_id'], 'exist', 'skipOnError' => true, 'targetClass' => Language::className(), 'targetAttribute' => ['language_id' => 'id']],
				[['parent_company_id'], 'exist', 'skipOnError' => true, 'targetClass' => Company::className(), 'targetAttribute' => ['parent_company_id' => 'id']]
		];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return [
            	'id' => Yii::t('main', 'ID'),
            	'parent_company_id' => Yii::t('main', 'Parent Company ID'),
            	'name' => Yii::t('main', 'Name'),
            	'email' => Yii::t('main', 'E-mail address'),
            	'phone1' => Yii::t('main', 'Phone'),
            	'phone2' => Yii::t('main', 'Alternative phone'),
        		'fax1' => Yii::t('main', 'Fax'),
        		'vat_identification_number' => Yii::t('main', 'VAT Identification Number'),
        		'taxpayer_identification_number' => Yii::t('main', 'Taxpayer Identification Number'),
        		'krs_number' => Yii::t('main', 'KRS number'),
            	'address_id' => Yii::t('main', 'Address ID'),
            	'address_postal_id' => Yii::t('main', 'Address Postal ID'),
        		'note' => Yii::t('main', 'Note'),
            	'language_id' => Yii::t('main', 'Language ID'),
            	'currency_id' => Yii::t('main', 'Currency ID'),
            	'is_artificial' => Yii::t('main', 'Is artificial (individual)'),
            	'is_active' => Yii::t('main', 'Is active'),
            	'date_creation' => Yii::t('main', 'Creation date'),
            	'date_modification' => Yii::t('main', 'Last modification date'),
        		'parentCompanyName' => Yii::t('main', 'Parent company'),
        		'addressName' => Yii::t('main', 'Address'),
        		'addressPostalName' => Yii::t('main', 'Address for correspondence'),
        		'languageName' => Yii::t('main', 'Default language'),
        		'currencyName' => Yii::t('main', 'Default currency'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAddress() {
        return $this->hasOne(Address::className(), ['id' => 'address_id']);
    }
    
    public function getAddressName() {
    	return !empty($this->address) ? $this->address->fullAddress : null;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAddressPostal() {
        return $this->hasOne(Address::className(), ['id' => 'address_postal_id']);
    }
    
    public function getAddressPostalName() {
    	return !empty($this->addressPostal) ? $this->addressPostal->fullAddress : null;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCurrency() {
        return $this->hasOne(Currency::className(), ['id' => 'currency_id']);
    }
    
    public function getCurrencyName() {
    	return !empty($this->currency) ? $this->currency->symbol : null;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLanguage() {
        return $this->hasOne(Language::className(), ['id' => 'language_id']);
    }
    
	public function getLanguageName() {
    	return !empty($this->language) ? $this->language->name : null;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getParentCompany() {
        return $this->hasOne(Company::className(), ['id' => 'parent_company_id']);
    }
    
    public function getParentCompanyName() {
    	return !empty($this->parentCompany) ? $this->parentCompany->name : null;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCompanies() {
        return $this->hasMany(Company::className(), ['parent_company_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrders() {
        return $this->hasMany(Order::className(), ['company_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUsers() {
        return $this->hasMany(User::className(), ['company_id' => 'id']);
    }

	/** 
	* @return \yii\db\ActiveQuery 
	*/ 
	public function getCompanyGroupSets() { 
		return $this->hasMany(CompanyGroupSet::className(), ['company_id' => 'id']); 
	} 
	 
	/** 
	* @return \yii\db\ActiveQuery 
	*/ 
	public function getCompanyGroups() { 
		return $this->hasMany(CompanyGroup::className(), ['id' => 'company_group_id'])->viaTable('company_group_set', ['company_id' => 'id']); 
	}
	
	/**
	 * @return \yii\db\ActiveQuery
	 */
	public function getAlerts() {
		return $this->hasMany(Alert::className(), ['company_id' => 'id']);
	}

    /**
     * @inheritdoc
     * @return \common\models\aq\CompanyQuery the active query used by this AR class.
     */
    public static function find() {
        return new \common\models\aq\CompanyQuery(get_called_class());
    }
    
    public function behaviors() {
    	return [
    			[
    					'class' => TimestampBehavior::className(),
    					'createdAtAttribute' => 'date_creation',
    					'updatedAtAttribute' => 'date_modification',
    					'value' => new Expression('NOW()'),
    			],
    	];
    }
}
