<?php

namespace common\models\ar;

use Yii;

/**
 * This is the model class for table "number_template".
 *
 * @property integer $id
 * @property string $symbol
 * @property integer $entity_id 
 * @property string $separator
 * @property string $template
 * @property integer $is_active
 *
 * @property Entity $entity 
 * @property NumberTemplateStorage $numberTemplateStorage
 */
class NumberTemplate extends \yii\db\ActiveRecord {

    /**
     * @inheritdoc
     */
    public static function tableName() {
        return 'number_template';
    }

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
				[['symbol', 'separator', 'template'], 'required'],
				[['entity_id', 'is_active'], 'integer'],
				[['symbol'], 'string', 'max' => 64],
				[['separator'], 'string', 'max' => 10],
				[['template'], 'string', 'max' => 255],
				[['symbol', 'entity_id'], 'unique', 'targetAttribute' => ['symbol', 'entity_id'], 'message' => 'The combination of Symbol and Entity ID has already been taken.'],
				[['entity_id'], 'exist', 'skipOnError' => true, 'targetClass' => Entity::className(), 'targetAttribute' => ['entity_id' => 'id']]
		];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return [
            	'id' => Yii::t('main', 'ID'),
            	'symbol' => Yii::t('main', 'Symbol'),
        		'entity_id' => Yii::t('main', 'Entity ID'),
            	'separator' => Yii::t('main', 'Separator'),
            	'template' => Yii::t('main', 'Template'),
            	'is_active' => Yii::t('main', 'Is Active'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getNumberTemplateStorage() {
        return $this->hasOne(NumberTemplateStorage::className(), ['number_template_id' => 'id']);
    }
    
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getEntity() {
    	return $this->hasOne(Entity::className(), ['id' => 'entity_id']);
    }

    /**
     * @inheritdoc
     * @return \common\models\aq\NumberTemplateQuery the active query used by this AR class.
     */
    public static function find() {
        return new \common\models\aq\NumberTemplateQuery(get_called_class());
    }
}
