<?php

namespace common\models\ar;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\Expression;

/**
 * This is the model class for table "information_page".
 *
 * @property integer $id
 * @property string $symbol
 * @property integer $priority
 * @property integer $is_active
 * @property string $date_creation
 *
 * @property InformationPageTranslation[] $informationPageTranslations
 * @property Language[] $languages
 */
class InformationPage extends \yii\db\ActiveRecord {

    /**
     * @inheritdoc
     */
    public static function tableName() {
        return 'information_page';
    }

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
				[['symbol'], 'required'],
        		[['symbol'], 'match', 'pattern' => '/^[\w\-_]+$/'],
				[['priority', 'is_active'], 'integer'],
				[['date_creation'], 'safe'],
				[['symbol'], 'string', 'max' => 32],
				[['symbol'], 'unique']
		];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return [
            	'id' => Yii::t('main', 'ID'),
            	'symbol' => Yii::t('main', 'Symbol'),
            	'priority' => Yii::t('main', 'Priority'),
            	'is_active' => Yii::t('main', 'Is active'),
            	'date_creation' => Yii::t('main', 'Creation date'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */

    public function getActiveInformationPageTranslations() {
    	return $this->hasMany(InformationPageTranslation::className(), ['information_page_id' => 'id'])->where(['is_active' => 1]);
    }
    
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLanguages() {
        return $this->hasMany(Language::className(), ['id' => 'language_id'])->viaTable('information_page_translation', ['information_page_id' => 'id']);
    }

    public function getTranslation() {
    	$translations = $this->getActiveInformationPageTranslations()->all();

		if(empty($translations)) {
			return null;
		}
   
		foreach($translations as $t) {
				if($t->language->symbol === Yii::$app->language) {
						return $t;
				}
 		}
 
		return $translations[0];
     }    

    /**
     * @inheritdoc
     * @return \common\models\aq\InformationPageQuery the active query used by this AR class.
     */
    public static function find() {
        return new \common\models\aq\InformationPageQuery(get_called_class());
    }
    
    public function behaviors() {
    	return [
    			[
    					'class' => TimestampBehavior::className(),
    					'createdAtAttribute' => 'date_creation',
    					'updatedAtAttribute' => false,
    					'value' => new Expression('NOW()'),
    			],
    	];
    }
}
