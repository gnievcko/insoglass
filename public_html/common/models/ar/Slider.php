<?php

namespace common\models\ar;

use Yii;

/**
 * This is the model class for table "slider".
 *
 * @property integer $id
 * @property string $name
 *
 * @property SliderBanner[] $sliderBanners
 * @property Banner[] $banners
 */
class Slider extends \yii\db\ActiveRecord {

    /**
     * @inheritdoc
     */
    public static function tableName() {
        return 'slider';
    }

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
				[['name'], 'required'],
				[['name'], 'string', 'max' => 50],
				[['name'], 'unique']
		];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return [
            	'id' => Yii::t('main', 'ID'),
            	'name' => Yii::t('main', 'Name'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSliderBanners() {
        return $this->hasMany(SliderBanner::className(), ['slider_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBanners() {
        return $this->hasMany(Banner::className(), ['id' => 'banner_id'])->viaTable('slider_banner', ['slider_id' => 'id']);
    }

    /**
     * @inheritdoc
     * @return \common\models\aq\SliderQuery the active query used by this AR class.
     */
    public static function find() {
        return new \common\models\aq\SliderQuery(get_called_class());
    }
}
