<?php

namespace common\models\ar;

use Yii;

/**
 * This is the model class for table "order_status_translation".
 *
 * @property integer $order_status_id
 * @property integer $language_id
 * @property string $name
 * @property string $name_client
 *
 * @property Language $language
 * @property OrderStatus $orderStatus
 */
class OrderStatusTranslation extends \yii\db\ActiveRecord {

    /**
     * @inheritdoc
     */
    public static function tableName() {
        return 'order_status_translation';
    }

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
				[['order_status_id', 'language_id', 'name'], 'required'],
				[['order_status_id', 'language_id'], 'integer'],
				[['name', 'name_client'], 'string', 'max' => 128],
				[['language_id'], 'exist', 'skipOnError' => true, 'targetClass' => Language::className(), 'targetAttribute' => ['language_id' => 'id']],
				[['order_status_id'], 'exist', 'skipOnError' => true, 'targetClass' => OrderStatus::className(), 'targetAttribute' => ['order_status_id' => 'id']],
        		[['order_status_id'], 'unique', 'targetAttribute' => ['order_status_id', 'language_id'], 'comboNotUnique' => Yii::t('main', 'The chosen order status-language pair has a translation already.')],
		];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return [
            	'order_status_id' => Yii::t('main', 'Order Status ID'),
            	'language_id' => Yii::t('main', 'Language ID'),
            	'name' => Yii::t('main', 'Name'),
            	'name_client' => Yii::t('main', 'Name visible for client'),
        		'languageName' => Yii::t('main', 'Language'),
        		'orderStatusSymbol' => Yii::t('main', 'Status zamówienia'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLanguage() {
        return $this->hasOne(Language::className(), ['id' => 'language_id']);
    }

    public function getLanguageName() {
    	return $this->language->name;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrderStatus() {
        return $this->hasOne(OrderStatus::className(), ['id' => 'order_status_id']);
    }

    public function getOrderStatusSymbol() {
        return $this->orderStatus->symbol;
    }

    /**
     * @inheritdoc
     * @return \common\models\aq\OrderStatusTranslationQuery the active query used by this AR class.
     */
    public static function find() {
        return new \common\models\aq\OrderStatusTranslationQuery(get_called_class());
    }
}
