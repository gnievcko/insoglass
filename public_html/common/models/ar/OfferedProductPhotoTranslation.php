<?php

namespace common\models\ar;

use Yii;

/**
 * This is the model class for table "offered_product_photo_translation".
 *
 * @property integer $offered_product_photo_id
 * @property integer $language_id
 * @property string $description
 *
 * @property Language $language
 * @property OfferedProductPhoto $offeredProductPhoto
 */
class OfferedProductPhotoTranslation extends \yii\db\ActiveRecord {

    /**
     * @inheritdoc
     */
    public static function tableName() {
        return 'offered_product_photo_translation';
    }

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
				[['offered_product_photo_id', 'language_id'], 'required'],
				[['offered_product_photo_id', 'language_id'], 'integer'],
				[['description'], 'string', 'max' => 512],
				[['language_id'], 'exist', 'skipOnError' => true, 'targetClass' => Language::className(), 'targetAttribute' => ['language_id' => 'id']],
				[['offered_product_photo_id'], 'exist', 'skipOnError' => true, 'targetClass' => OfferedProductPhoto::className(), 'targetAttribute' => ['offered_product_photo_id' => 'id']]
		];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return [
            	'offered_product_photo_id' => Yii::t('main', 'Offered Product Photo ID'),
            	'language_id' => Yii::t('main', 'Language ID'),
            	'description' => Yii::t('main', 'Description'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLanguage() {
        return $this->hasOne(Language::className(), ['id' => 'language_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOfferedProductPhoto() {
        return $this->hasOne(OfferedProductPhoto::className(), ['id' => 'offered_product_photo_id']);
    }

    /**
     * @inheritdoc
     * @return \common\models\aq\OfferedProductPhotoTranslationQuery the active query used by this AR class.
     */
    public static function find() {
        return new \common\models\aq\OfferedProductPhotoTranslationQuery(get_called_class());
    }
}
