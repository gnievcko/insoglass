<?php

namespace common\models\ar;

use Yii;

/**
 * This is the model class for table "order_type".
 *
 * @property integer $id
 * @property string $symbol
 *
 * @property Order[] $orders
 * @property OrderTypeTranslation[] $orderTypeTranslations
 * @property Language[] $languages
 * @property OrderTypeSet[] $orderTypeSets 
 */
class OrderType extends \yii\db\ActiveRecord {

    /**
     * @inheritdoc
     */
    public static function tableName() {
        return 'order_type';
    }

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
				[['symbol'], 'required'],
				[['symbol'], 'string', 'max' => 32]
		];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return [
            	'id' => Yii::t('main', 'ID'),
            	'symbol' => Yii::t('main', 'Symbol'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrders() {
        return $this->hasMany(Order::className(), ['id' => 'order_id'])->viaTable('order_type_set', ['order_type_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrderTypeTranslations() {
        return $this->hasMany(OrderTypeTranslation::className(), ['order_type_id' => 'id']);
    }
    
    public function getOrderTypeSets() {
    	return $this->hasMany(OrderTypeSet::className(), ['order_type_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLanguages() {
        return $this->hasMany(Language::className(), ['id' => 'language_id'])->viaTable('order_type_translation', ['order_type_id' => 'id']);
    }
    
    public function getTranslation() {
    	$translations = $this->orderTypeTranslations;
    
    	if(empty($translations)) {
    		return null;
    	}
    	 
    	foreach($translations as $t) {
    		if($t->language->symbol === Yii::$app->language) {
    			return $t;
    		}
    	}
    
    	return $translations[0];
    }

    /**
     * @inheritdoc
     * @return \common\models\aq\OrderTypeQuery the active query used by this AR class.
     */
    public static function find() {
        return new \common\models\aq\OrderTypeQuery(get_called_class());
    }
}
