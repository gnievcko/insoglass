<?php

namespace common\models\ar;

use Yii;

/**
 * This is the model class for table "qa_factor_type_translation".
 *
 * @property integer $qa_factor_type_id
 * @property integer $language_id
 * @property string $name
 *
 * @property QaFactorType $qaFactorType
 * @property Language $language
 */
class QaFactorTypeTranslation extends \yii\db\ActiveRecord {

    /**
     * @inheritdoc
     */
    public static function tableName() {
        return 'qa_factor_type_translation';
    }

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
			[['qa_factor_type_id', 'language_id', 'name'], 'required'],
			[['qa_factor_type_id', 'language_id'], 'integer'],
			[['name'], 'string', 'max' => 256],
			[['qa_factor_type_id'], 'exist', 'skipOnError' => true, 'targetClass' => QaFactorType::className(), 'targetAttribute' => ['qa_factor_type_id' => 'id']],
			[['language_id'], 'exist', 'skipOnError' => true, 'targetClass' => Language::className(), 'targetAttribute' => ['language_id' => 'id']]
		];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return [
            'qa_factor_type_id' => Yii::t('main', 'Qa Factor Type ID'),
            'language_id' => Yii::t('main', 'Language ID'),
            'name' => Yii::t('main', 'Name'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getQaFactorType() {
        return $this->hasOne(QaFactorType::className(), ['id' => 'qa_factor_type_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLanguage() {
        return $this->hasOne(Language::className(), ['id' => 'language_id']);
    }

    /**
     * @inheritdoc
     * @return \common\models\aq\QaFactorTypeTranslationQuery the active query used by this AR class.
     */
    public static function find() {
        return new \common\models\aq\QaFactorTypeTranslationQuery(get_called_class());
    }
}
