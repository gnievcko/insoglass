<?php

namespace common\models\ar;

use Yii;

/**
 * This is the model class for table "number_template_storage".
 *
 * @property integer $id
 * @property integer $number_template_id
 * @property string $current_number
 *
 * @property NumberTemplate $numberTemplate
 */
class NumberTemplateStorage extends \yii\db\ActiveRecord {

    /**
     * @inheritdoc
     */
    public static function tableName() {
        return 'number_template_storage';
    }

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
				[['number_template_id', 'current_number'], 'required'],
				[['number_template_id'], 'integer'],
				[['current_number'], 'string', 'max' => 255],
				[['number_template_id'], 'unique'],
				[['number_template_id'], 'exist', 'skipOnError' => true, 'targetClass' => NumberTemplate::className(), 'targetAttribute' => ['number_template_id' => 'id']]
		];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return [
            	'id' => Yii::t('main', 'ID'),
            	'number_template_id' => Yii::t('main', 'Number Template ID'),
            	'current_number' => Yii::t('main', 'Current Number'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getNumberTemplate() {
        return $this->hasOne(NumberTemplate::className(), ['id' => 'number_template_id']);
    }

    /**
     * @inheritdoc
     * @return \common\models\aq\NumberTemplateStorageQuery the active query used by this AR class.
     */
    public static function find() {
        return new \common\models\aq\NumberTemplateStorageQuery(get_called_class());
    }
}
