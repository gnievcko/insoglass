<?php

namespace common\models\ar;

use Yii;

/**
 * This is the model class for table "country_translation".
 *
 * @property integer $country_id
 * @property integer $language_id
 * @property string $name
 *
 * @property Country $country
 * @property Language $language
 */
class CountryTranslation extends \yii\db\ActiveRecord {

    /**
     * @inheritdoc
     */
    public static function tableName() {
        return 'country_translation';
    }

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
				[['country_id', 'language_id', 'name'], 'required'],
				[['country_id', 'language_id'], 'integer'],
				[['name'], 'string', 'max' => 128],
				[['country_id'], 'exist', 'skipOnError' => true, 'targetClass' => Country::className(), 'targetAttribute' => ['country_id' => 'id']],
				[['language_id'], 'exist', 'skipOnError' => true, 'targetClass' => Language::className(), 'targetAttribute' => ['language_id' => 'id']],
                                [['country_id'], 'unique', 'targetAttribute' => ['country_id', 'language_id'], 'comboNotUnique' => Yii::t('main', 'The chosen country-language pair has a translation already.')],
		];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return [
            	'country_id' => Yii::t('main', 'Country ID'),
            	'language_id' => Yii::t('main', 'Language ID'),
            	'name' => Yii::t('main', 'Name'),
        		'countrySymbol' => Yii::t('main', 'Country'),
        		'languageName' => Yii::t('main', 'Language'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCountry() {
        return $this->hasOne(Country::className(), ['id' => 'country_id']);
    }
    
    public function getCountrySymbol() {
    	return $this->country->symbol;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLanguage() {
        return $this->hasOne(Language::className(), ['id' => 'language_id']);
    }
    
    public function getLanguageName() {
    	return $this->language->name;
    }

    /**
     * @inheritdoc
     * @return \common\models\aq\CountryTranslationQuery the active query used by this AR class.
     */
    public static function find() {
        return new \common\models\aq\CountryTranslationQuery(get_called_class());
    }
}
