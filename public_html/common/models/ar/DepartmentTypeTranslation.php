<?php

namespace common\models\ar;

use Yii;

/**
 * This is the model class for table "department_type_translation".
 *
 * @property integer $department_type_id
 * @property integer $language_id
 * @property string $name
 *
 * @property DepartmentType $departmentType
 * @property Language $language
 */
class DepartmentTypeTranslation extends \yii\db\ActiveRecord {

    /**
     * @inheritdoc
     */

    public static function tableName() {
        return 'department_type_translation';
    }

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
				[['department_type_id', 'language_id', 'name'], 'required'],
				[['department_type_id', 'language_id'], 'integer'],
				[['name'], 'string', 'max' => 128],
				[['department_type_id'], 'exist', 'skipOnError' => true, 'targetClass' => DepartmentType::className(), 'targetAttribute' => ['department_type_id' => 'id']],
				[['language_id'], 'exist', 'skipOnError' => true, 'targetClass' => Language::className(), 'targetAttribute' => ['language_id' => 'id']],
                [['department_type_id'], 'unique', 'targetAttribute' => ['department_type_id', 'language_id'], 'comboNotUnique' => Yii::t('main', 'The chosen type-language pair has a translation already.')],

        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return [
            	'department_type_id' => Yii::t('main', 'Department type id'),
            	'language_id' => Yii::t('main', 'Language ID'),
            	'name' => Yii::t('main', 'Name'),
                'languageName' => Yii::t('main','Language'),
                'department' => Yii::t('main','Department'),
                'departmentTypeSymbol' => Yii::t('main','Department type symbol'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDepartmentType() {
        return $this->hasOne(DepartmentType::className(), ['id' => 'department_type_id']);
    }
    public function getDepartmentTypeSymbol(){
        return $this->departmentType->symbol;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLanguage() {
        return $this->hasOne(Language::className(), ['id' => 'language_id']);
    }

    public function getLanguageName() {
        return $this->language->name;
    }
    public function getLanguageSymbol() {
        return $this->language->symbol;
    }
    /**
     * @inheritdoc
     * @return \common\models\aq\DepartmentTypeTranslationQuery the active query used by this AR class.
     */
    public static function find() {
        return new \common\models\aq\DepartmentTypeTranslationQuery(get_called_class());
    }
}
