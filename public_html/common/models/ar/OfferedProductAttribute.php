<?php

namespace common\models\ar;

use Yii;

/**
 * This is the model class for table "offered_product_attribute".
 *
 * @property integer $offered_product_id
 * @property integer $product_attribute_type_id
 * @property string $value
 * @property ProductAttributeType $productAttributeType
 * @property OfferedProduct $offeredProduct
 */
class OfferedProductAttribute extends \yii\db\ActiveRecord {

    /**
     * @inheritdoc
     */
    public static function tableName() {
        return 'offered_product_attribute';
    }

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
				[['offered_product_id', 'product_attribute_type_id', 'value'], 'required'],
				[['offered_product_id', 'product_attribute_type_id'], 'integer'],
				[['value'], 'string', 'max' => 64],
				[['product_attribute_type_id'], 'exist', 'skipOnError' => true, 'targetClass' => ProductAttributeType::className(), 'targetAttribute' => ['product_attribute_type_id' => 'id']],
				[['offered_product_id'], 'exist', 'skipOnError' => true, 'targetClass' => OfferedProduct::className(), 'targetAttribute' => ['offered_product_id' => 'id']]
		];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return [
            	'offered_product_id' => Yii::t('main', 'Offered Product ID'),
            	'product_attribute_type_id' => Yii::t('main', 'Product Attribute Type ID'),
            	'value' => Yii::t('main', 'Value'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProductAttributeType() {
        return $this->hasOne(ProductAttributeType::className(), ['id' => 'product_attribute_type_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOfferedProduct() {
        return $this->hasOne(OfferedProduct::className(), ['id' => 'offered_product_id']);
    }

    /**
     * @inheritdoc
     * @return \common\models\aq\OfferedProductAttributeQuery the active query used by this AR class.
     */
    public static function find() {
        return new \common\models\aq\OfferedProductAttributeQuery(get_called_class());
    }
}
