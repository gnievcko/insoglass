<?php

namespace common\models\ar;

use Yii;

/**
 * This is the model class for table "product_category".
 *
 * @property integer $id
 * @property string $symbol
 * @property integer $parent_category_id
 *
 * @property ProductCategory $parentCategory
 * @property ProductCategory[] $productCategories
 * @property ProductCategorySet[] $productCategorySets
 * @property Product[] $products
 * @property ProductCategoryTranslation[] $productCategoryTranslations
 * @property Language[] $languages
 */
class ProductCategory extends \yii\db\ActiveRecord {

    /**
     * @inheritdoc
     */
    public static function tableName() {
        return 'product_category';
    }

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
				[['symbol'], 'required'],
				[['parent_category_id'], 'integer'],
				[['symbol'], 'string', 'max' => 32],
				[['symbol'], 'unique'],
				[['parent_category_id'], 'exist', 'skipOnError' => true, 'targetClass' => ProductCategory::className(), 'targetAttribute' => ['parent_category_id' => 'id']]
		];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return [
            	'id' => Yii::t('main', 'ID'),
            	'symbol' => Yii::t('main', 'Symbol'),
            	'parent_category_id' => Yii::t('main', 'Parent Category ID'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getParentCategory() {
        return $this->hasOne(ProductCategory::className(), ['id' => 'parent_category_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProductCategories() {
        return $this->hasMany(ProductCategory::className(), ['parent_category_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProductCategorySets() {
        return $this->hasMany(ProductCategorySet::className(), ['product_category_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProducts() {
        return $this->hasMany(Product::className(), ['id' => 'product_id'])->viaTable('product_category_set', ['product_category_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProductCategoryTranslations() {
        return $this->hasMany(ProductCategoryTranslation::className(), ['product_category_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLanguages() {
        return $this->hasMany(Language::className(), ['id' => 'language_id'])->viaTable('product_category_translation', ['product_category_id' => 'id']);
    }

    /**
     * @inheritdoc
     * @return \common\models\aq\ProductCategoryQuery the active query used by this AR class.
     */
    public static function find() {
        return new \common\models\aq\ProductCategoryQuery(get_called_class());
    }
}
