<?php

namespace common\models\ar;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\Expression;

/**
 * This is the model class for table "invoice_history".
 *
 * @property integer $id
 * @property integer $document_type_id
 * @property integer $entity_id
 * @property string $document_id
 * @property string $date_issue 
 * @property string $number 
 * @property integer $is_last
 * @property string $date_creation
 *
 * @property Entity $entity
 * @property DocumentType $documentType
 */
class InvoiceHistory extends \yii\db\ActiveRecord {

    /**
     * @inheritdoc
     */
    public static function tableName() {
        return 'invoice_history';
    }

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
				[['document_type_id', 'entity_id', 'document_id'], 'required'],
				[['document_type_id', 'entity_id', 'is_last'], 'integer'],
				[['date_issue', 'date_creation'], 'safe'],
               	[['document_id', 'number'], 'string', 'max' => 256],
				[['entity_id'], 'exist', 'skipOnError' => true, 'targetClass' => Entity::className(), 'targetAttribute' => ['entity_id' => 'id']],
				[['document_type_id'], 'exist', 'skipOnError' => true, 'targetClass' => DocumentType::className(), 'targetAttribute' => ['document_type_id' => 'id']]
		];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return [
            	'id' => Yii::t('main', 'ID'),
            	'document_type_id' => Yii::t('main', 'Document Type ID'),
            	'entity_id' => Yii::t('main', 'Entity ID'),
            	'document_id' => Yii::t('main', 'Document ID'),
        		'date_issue' => Yii::t('main', 'Date Issue'),
        		'number' => Yii::t('main', 'Number'),
            	'is_last' => Yii::t('main', 'Is Last'),
            	'date_creation' => Yii::t('main', 'Date Creation'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getEntity() {
        return $this->hasOne(Entity::className(), ['id' => 'entity_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDocumentType() {
        return $this->hasOne(DocumentType::className(), ['id' => 'document_type_id']);
    }

    /**
     * @inheritdoc
     * @return \common\models\aq\InvoiceHistoryQuery the active query used by this AR class.
     */
    public static function find() {
        return new \common\models\aq\InvoiceHistoryQuery(get_called_class());
    }
    
    public function behaviors() {
    	return [
    			[
    					'class' => TimestampBehavior::className(),
    					'createdAtAttribute' => 'date_creation',
    					'updatedAtAttribute' => false,
    					'value' => new Expression('NOW()'),
    			],
    	];
    }
}
