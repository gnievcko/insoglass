<?php

namespace common\models\ar;

use Yii;

/**
 * This is the model class for table "user_workstation".
 *
 * @property integer $user_id
 * @property integer $workstation_id
 *
 * @property Workstation $workstation
 * @property User $user
 */
class UserWorkstation extends \yii\db\ActiveRecord {

    /**
     * @inheritdoc
     */
    public static function tableName() {
        return 'user_workstation';
    }

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
				[['user_id', 'workstation_id'], 'required'],
				[['user_id', 'workstation_id'], 'integer'],
				[['workstation_id'], 'exist', 'skipOnError' => true, 'targetClass' => Workstation::className(), 'targetAttribute' => ['workstation_id' => 'id']],
				[['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['user_id' => 'id']]
		];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return [
            	'user_id' => Yii::t('main', 'User'),
            	'workstation_id' => Yii::t('main', 'Workstation ID'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getWorkstation() {
        return $this->hasOne(Workstation::className(), ['id' => 'workstation_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser() {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }

    /**
     * @inheritdoc
     * @return \common\models\aq\UserWorkstationQuery the active query used by this AR class.
     */
    public static function find() {
        return new \common\models\aq\UserWorkstationQuery(get_called_class());
    }
}
