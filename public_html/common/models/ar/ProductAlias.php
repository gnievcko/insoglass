<?php

namespace common\models\ar;

use Yii;

/**
 * This is the model class for table "product_alias".
 *
 * @property integer $id
 * @property integer $product_id
 * @property integer $language_id
 * @property string $name
 *
 * @property Language $language
 * @property Product $product
 */
class ProductAlias extends \yii\db\ActiveRecord {

    /**
     * @inheritdoc
     */
    public static function tableName() {
        return 'product_alias';
    }

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
				[['product_id', 'language_id', 'name'], 'required'],
				[['product_id', 'language_id'], 'integer'],
				[['name'], 'string', 'max' => 512],
				[['language_id'], 'exist', 'skipOnError' => true, 'targetClass' => Language::className(), 'targetAttribute' => ['language_id' => 'id']],
				[['product_id'], 'exist', 'skipOnError' => true, 'targetClass' => Product::className(), 'targetAttribute' => ['product_id' => 'id']]
		];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return [
            	'id' => Yii::t('main', 'ID'),
            	'product_id' => Yii::t('main', 'Product'),
            	'language_id' => Yii::t('main', 'Language ID'),
            	'name' => Yii::t('main', 'Name'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLanguage() {
        return $this->hasOne(Language::className(), ['id' => 'language_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProduct() {
        return $this->hasOne(Product::className(), ['id' => 'product_id']);
    }

    /**
     * @inheritdoc
     * @return \common\models\aq\ProductAliasQuery the active query used by this AR class.
     */
    public static function find() {
        return new \common\models\aq\ProductAliasQuery(get_called_class());
    }
}
