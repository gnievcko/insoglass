<?php

namespace common\models\ar;

use Yii;

/**
 * This is the model class for table "order_type_set".
 *
 * @property integer $order_id
 * @property integer $order_type_id
 *
 * @property OrderType $orderType
 * @property Order $order
 */
class OrderTypeSet extends \yii\db\ActiveRecord {

    /**
     * @inheritdoc
     */
    public static function tableName() {
        return 'order_type_set';
    }

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
				[['order_id', 'order_type_id'], 'required'],
				[['order_id', 'order_type_id'], 'integer'],
				[['order_type_id'], 'exist', 'skipOnError' => true, 'targetClass' => OrderType::className(), 'targetAttribute' => ['order_type_id' => 'id']],
				[['order_id'], 'exist', 'skipOnError' => true, 'targetClass' => Order::className(), 'targetAttribute' => ['order_id' => 'id']]
		];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return [
            	'order_id' => Yii::t('main', 'Order'),
            	'order_type_id' => Yii::t('main', 'Order Type ID'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrderType() {
        return $this->hasOne(OrderType::className(), ['id' => 'order_type_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrder() {
        return $this->hasOne(Order::className(), ['id' => 'order_id']);
    }

    /**
     * @inheritdoc
     * @return \common\models\aq\OrderTypeSetQuery the active query used by this AR class.
     */
    public static function find() {
        return new \common\models\aq\OrderTypeSetQuery(get_called_class());
    }
}
