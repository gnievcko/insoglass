<?php

namespace common\models\ar;

use Yii;

/**
 * This is the model class for table "alert_priority".
 *
 * @property integer $id
 * @property string $symbol
 * @property integer $priority
 *
 * @property AlertPriorityTranslation[] $alertPriorityTranslations
 * @property Language[] $languages
 * @property AlertType[] $alertTypes
 */
class AlertPriority extends \yii\db\ActiveRecord {

    /**
     * @inheritdoc
     */
    public static function tableName() {
        return 'alert_priority';
    }

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
				[['symbol', 'priority'], 'required'],
				[['priority'], 'integer'],
				[['symbol'], 'string', 'max' => 32],
				[['symbol'], 'unique']
		];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return [
            	'id' => Yii::t('main', 'ID'),
            	'symbol' => Yii::t('main', 'Symbol'),
            	'priority' => Yii::t('main', 'Priority'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAlertPriorityTranslations() {
        return $this->hasMany(AlertPriorityTranslation::className(), ['alert_priority_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLanguages() {
        return $this->hasMany(Language::className(), ['id' => 'language_id'])->viaTable('alert_priority_translation', ['alert_priority_id' => 'id']);
    }
    
    public function getTranslation() {
    	$translations = $this->alertPriorityTranslations;
    	if(empty($translations)) {
    		return null;
    	}
    
    	foreach($translations as $t) {
    		if($t->language->symbol === Yii::$app->language) {
    			return $t;
    		}
    	}
    
    	return $translations[0];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAlertTypes() {
        return $this->hasMany(AlertType::className(), ['alert_priority_id' => 'id']);
    }

    /**
     * @inheritdoc
     * @return \common\models\aq\AlertPriorityQuery the active query used by this AR class.
     */
    public static function find() {
        return new \common\models\aq\AlertPriorityQuery(get_called_class());
    }
}
