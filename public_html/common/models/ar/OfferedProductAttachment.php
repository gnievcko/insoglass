<?php

namespace common\models\ar;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\Expression;

/**
 * This is the model class for table "offered_product_attachment".
 *
 * @property integer $id
 * @property integer $offered_product_id
 * @property integer $user_id
 * @property string $url_file
 * @property string $name
 * @property string $description
 * @property string $date_creation
 *
 * @property User $user
 * @property OfferedProduct $offeredProduct
 */
class OfferedProductAttachment extends \yii\db\ActiveRecord {

    /**
     * @inheritdoc
     */
    public static function tableName() {
        return 'offered_product_attachment';
    }

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
				[['offered_product_id', 'user_id', 'url_file', 'name'], 'required'],
				[['offered_product_id', 'user_id'], 'integer'],
				[['date_creation'], 'safe'],
				[['url_file', 'name'], 'string', 'max' => 256],
				[['description'], 'string', 'max' => 512],
				[['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['user_id' => 'id']],
				[['offered_product_id'], 'exist', 'skipOnError' => true, 'targetClass' => OfferedProduct::className(), 'targetAttribute' => ['offered_product_id' => 'id']]
		];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return [
            	'id' => Yii::t('main', 'ID'),
            	'offered_product_id' => Yii::t('main', 'Offered Product ID'),
            	'user_id' => Yii::t('main', 'User'),
            	'url_file' => Yii::t('main', 'Url File'),
            	'name' => Yii::t('main', 'Name'),
            	'description' => Yii::t('main', 'Description'),
            	'date_creation' => Yii::t('main', 'Date Creation'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser() {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOfferedProduct() {
        return $this->hasOne(OfferedProduct::className(), ['id' => 'offered_product_id']);
    }

    /**
     * @inheritdoc
     * @return \common\models\aq\OfferedProductAttachmentQuery the active query used by this AR class.
     */
    public static function find() {
        return new \common\models\aq\OfferedProductAttachmentQuery(get_called_class());
    }
    
    public function behaviors() {
    	return [
    			[
    					'class' => TimestampBehavior::className(),
    					'createdAtAttribute' => 'date_creation',
    					'updatedAtAttribute' => false,
    					'value' => new Expression('NOW()'),
    			],
    	];
    }
}
