<?php

namespace common\models\ar;

use Yii;

/**
 * This is the model class for table "document_section".
 *
 * @property integer $id
 * @property string $symbol
 * @property string $description
 *
 * @property DocumentType[] $documentTypes
 * @property DocumentType[] $documentTypes0
 * @property DocumentTypeSection[] $documentTypeSections
 * @property DocumentType[] $documentTypes1
 */
class DocumentSection extends \yii\db\ActiveRecord {

    /**
     * @inheritdoc
     */
    public static function tableName() {
        return 'document_section';
    }

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
				[['symbol'], 'required'],
				[['description'], 'string'],
				[['symbol'], 'string', 'max' => 30],
				[['symbol'], 'unique']
		];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return [
            	'id' => Yii::t('main', 'ID'),
            	'symbol' => Yii::t('main', 'Symbol'),
            	'description' => Yii::t('main', 'Description'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDocumentTypes() {
        return $this->hasMany(DocumentType::className(), ['document_section_footer_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDocumentTypes0() {
        return $this->hasMany(DocumentType::className(), ['document_section_header_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDocumentTypeSections() {
        return $this->hasMany(DocumentTypeSection::className(), ['document_section_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDocumentTypes1() {
        return $this->hasMany(DocumentType::className(), ['id' => 'document_type_id'])->viaTable('document_type_section', ['document_section_id' => 'id']);
    }

    /**
     * @inheritdoc
     * @return \common\models\aq\DocumentSectionQuery the active query used by this AR class.
     */
    public static function find() {
        return new \common\models\aq\DocumentSectionQuery(get_called_class());
    }
}
