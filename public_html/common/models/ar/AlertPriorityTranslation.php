<?php

namespace common\models\ar;

use Yii;

/**
 * This is the model class for table "alert_priority_translation".
 *
 * @property integer $alert_priority_id
 * @property integer $language_id
 * @property string $name
 *
 * @property Language $language
 * @property AlertPriority $alertPriority
 */
class AlertPriorityTranslation extends \yii\db\ActiveRecord {

    /**
     * @inheritdoc
     */
    public static function tableName() {
        return 'alert_priority_translation';
    }

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
				[['alert_priority_id', 'language_id', 'name'], 'required'],
				[['alert_priority_id', 'language_id'], 'integer'],
				[['name'], 'string', 'max' => 256],
				[['language_id'], 'exist', 'skipOnError' => true, 'targetClass' => Language::className(), 'targetAttribute' => ['language_id' => 'id']],
				[['alert_priority_id'], 'exist', 'skipOnError' => true, 'targetClass' => AlertPriority::className(), 'targetAttribute' => ['alert_priority_id' => 'id']]
		];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return [
            	'alert_priority_id' => Yii::t('main', 'Alert Priority ID'),
            	'language_id' => Yii::t('main', 'Language ID'),
            	'name' => Yii::t('main', 'Name'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLanguage() {
        return $this->hasOne(Language::className(), ['id' => 'language_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAlertPriority() {
        return $this->hasOne(AlertPriority::className(), ['id' => 'alert_priority_id']);
    }

    /**
     * @inheritdoc
     * @return \common\models\aq\AlertPriorityTranslationQuery the active query used by this AR class.
     */
    public static function find() {
        return new \common\models\aq\AlertPriorityTranslationQuery(get_called_class());
    }
}
