<?php

namespace common\models\ar;

use Yii;

/**
 * This is the model class for table "banner_translation".
 *
 * @property integer $banner_id
 * @property integer $language_id
 * @property string $url_photo
 * @property string $description
 *
 * @property Banner $banner
 * @property Language $language
 */
class BannerTranslation extends \yii\db\ActiveRecord {

    /**
     * @inheritdoc
     */
    public static function tableName() {
        return 'banner_translation';
    }

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
				[['banner_id', 'language_id', 'url_photo'], 'required'],
				[['banner_id', 'language_id'], 'integer'],
				[['url_photo'], 'string', 'max' => 256],
				[['description'], 'string', 'max' => 512],
				[['banner_id'], 'exist', 'skipOnError' => true, 'targetClass' => Banner::className(), 'targetAttribute' => ['banner_id' => 'id']],
				[['language_id'], 'exist', 'skipOnError' => true, 'targetClass' => Language::className(), 'targetAttribute' => ['language_id' => 'id']]
		];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return [
            	'banner_id' => Yii::t('main', 'Banner ID'),
            	'language_id' => Yii::t('main', 'Language ID'),
            	'url_photo' => Yii::t('main', 'Url Photo'),
            	'description' => Yii::t('main', 'Description'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBanner() {
        return $this->hasOne(Banner::className(), ['id' => 'banner_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLanguage() {
        return $this->hasOne(Language::className(), ['id' => 'language_id']);
    }

    /**
     * @inheritdoc
     * @return \common\models\aq\BannerTranslationQuery the active query used by this AR class.
     */
    public static function find() {
        return new \common\models\aq\BannerTranslationQuery(get_called_class());
    }
}
