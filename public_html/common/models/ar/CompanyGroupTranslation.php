<?php

namespace common\models\ar;

use Yii;

/**
 * This is the model class for table "company_group_translation".
 *
 * @property integer $company_group_id
 * @property integer $language_id
 * @property string $name
 *
 * @property CompanyGroup $companyGroup
 * @property Language $language
 */
class CompanyGroupTranslation extends \yii\db\ActiveRecord {

    /**
     * @inheritdoc
     */
    public static function tableName() {
        return 'company_group_translation';
    }

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
				[['company_group_id', 'language_id', 'name'], 'required'],
				[['company_group_id', 'language_id'], 'integer'],
				[['name'], 'string', 'max' => 256],
				[['company_group_id'], 'exist', 'skipOnError' => true, 'targetClass' => CompanyGroup::className(), 'targetAttribute' => ['company_group_id' => 'id']],
				[['language_id'], 'exist', 'skipOnError' => true, 'targetClass' => Language::className(), 'targetAttribute' => ['language_id' => 'id']],
        		[['company_group_id'], 'unique', 'targetAttribute' => ['company_group_id', 'language_id'], 'comboNotUnique' => Yii::t('main', 'The chosen company group-language pair has a translation already.')],
		];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return [
            	'company_group_id' => Yii::t('main', 'Client Group ID'),
            	'language_id' => Yii::t('main', 'Language ID'),
            	'name' => Yii::t('main', 'Name'),
        		'companyGroupSymbol' => Yii::t('main', 'Client group symbol'),
        		'languageName' => Yii::t('main', 'Language'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCompanyGroup() {
        return $this->hasOne(CompanyGroup::className(), ['id' => 'company_group_id']);
    }
    
    public function getCompanyGroupSymbol() {
    	return $this->companyGroup->symbol;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLanguage() {
        return $this->hasOne(Language::className(), ['id' => 'language_id']);
    }
    
    public function getLanguageName() {
    	return $this->language->name;
    }

    /**
     * @inheritdoc
     * @return \common\models\aq\CompanyGroupTranslationQuery the active query used by this AR class.
     */
    public static function find() {
        return new \common\models\aq\CompanyGroupTranslationQuery(get_called_class());
    }
}
