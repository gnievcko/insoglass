<?php

namespace common\models\ar;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\Expression;

/**
 * This is the model class for table "warehouse".
 *
 * @property integer $id
 * @property string $name
 * @property string $index
 * @property integer $parent_warehouse_id
 * @property integer $address_id
 * @property integer $is_active
 * @property string $date_creation
 *
 * @property UserWarehouse[] $userWarehouses
 * @property User[] $users
 * @property Address $address
 * @property Warehouse $parentWarehouse
 * @property Warehouse[] $warehouses
 * @property WarehouseDelivery[] $warehouseDeliveries
 * @property WarehouseProduct[] $warehouseProducts
 * @property Product[] $products
 * @property Alert[] $alerts
 */
class Warehouse extends \yii\db\ActiveRecord {

    /**
     * @inheritdoc
     */
    public static function tableName() {
        return 'warehouse';
    }

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
				[['name'], 'required'],
				[['parent_warehouse_id', 'address_id', 'is_active'], 'integer'],
				[['date_creation'], 'safe'],
        		[['index'], 'string', 'max' => 48],
				[['name'], 'string', 'max' => 256],
				[['address_id'], 'exist', 'skipOnError' => true, 'targetClass' => Address::className(), 'targetAttribute' => ['address_id' => 'id']],
				[['parent_warehouse_id'], 'exist', 'skipOnError' => true, 'targetClass' => Warehouse::className(), 'targetAttribute' => ['parent_warehouse_id' => 'id']]
		];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return [
            	'id' => Yii::t('main', 'ID'),
            	'name' => Yii::t('main', 'Name'),
        		'index' => Yii::t('main', 'Warehouse index'),
            	'parent_warehouse_id' => Yii::t('main', 'Parent Warehouse ID'),
            	'address_id' => Yii::t('main', 'Address ID'),
            	'is_active' => Yii::t('main', 'Is active'),
            	'date_creation' => Yii::t('main', 'Date creation'),
        		'parentWarehouseName' => Yii::t('main', 'Parent warehouse'),
        		'addressData' => Yii::t('main', 'Address'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUserWarehouses() {
        return $this->hasMany(UserWarehouse::className(), ['warehouse_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUsers() {
        return $this->hasMany(User::className(), ['id' => 'user_id'])->viaTable('user_warehouse', ['warehouse_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAddress() {
        return $this->hasOne(Address::className(), ['id' => 'address_id']);
    }
    
    public function getAddressData() {
    	return !empty($this->address) ? $this->address->fullAddress : null;
    }
    

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getParentWarehouse() {
        return $this->hasOne(Warehouse::className(), ['id' => 'parent_warehouse_id']);
    }
    
    public function getParentWarehouseName() {
    	return !empty($this->parentWarehouse) ? $this->parentWarehouse->name : null;
    }
    
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getWarehouses() {
        return $this->hasMany(Warehouse::className(), ['parent_warehouse_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getWarehouseDeliveries() {
        return $this->hasMany(WarehouseDelivery::className(), ['warehouse_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getWarehouseProducts() {
        return $this->hasMany(WarehouseProduct::className(), ['warehouse_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProducts() {
        return $this->hasMany(Product::className(), ['id' => 'product_id'])->viaTable('warehouse_product', ['warehouse_id' => 'id']);
    }
    
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAlerts() {
    	return $this->hasMany(Alert::className(), ['company_id' => 'id']);
    }

    /**
     * @inheritdoc
     * @return \common\models\aq\WarehouseQuery the active query used by this AR class.
     */
    public static function find() {
        return new \common\models\aq\WarehouseQuery(get_called_class());
    }
    
    public function behaviors() {
    	return [
    			[
    					'class' => TimestampBehavior::className(),
    					'createdAtAttribute' => 'date_creation',
    					'updatedAtAttribute' => false,
    					'value' => new Expression('NOW()'),
    			],
    	];
    }
}
