<?php

namespace common\models\ar;

use Yii;

/**
 * This is the model class for table "product_photo_translation".
 *
 * @property integer $product_photo_id
 * @property integer $language_id
 * @property string $description
 *
 * @property Language $language
 * @property ProductPhoto $productPhoto
 */
class ProductPhotoTranslation extends \yii\db\ActiveRecord {

    /**
     * @inheritdoc
     */
    public static function tableName() {
        return 'product_photo_translation';
    }

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
				[['product_photo_id', 'language_id'], 'required'],
				[['product_photo_id', 'language_id'], 'integer'],
				[['description'], 'string', 'max' => 512],
				[['language_id'], 'exist', 'skipOnError' => true, 'targetClass' => Language::className(), 'targetAttribute' => ['language_id' => 'id']],
				[['product_photo_id'], 'exist', 'skipOnError' => true, 'targetClass' => ProductPhoto::className(), 'targetAttribute' => ['product_photo_id' => 'id']]
		];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return [
            	'product_photo_id' => Yii::t('main', 'Product Photo ID'),
            	'language_id' => Yii::t('main', 'Language ID'),
            	'description' => Yii::t('main', 'Description'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLanguage() {
        return $this->hasOne(Language::className(), ['id' => 'language_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProductPhoto() {
        return $this->hasOne(ProductPhoto::className(), ['id' => 'product_photo_id']);
    }

    /**
     * @inheritdoc
     * @return \common\models\aq\ProductPhotoTranslationQuery the active query used by this AR class.
     */
    public static function find() {
        return new \common\models\aq\ProductPhotoTranslationQuery(get_called_class());
    }
}
