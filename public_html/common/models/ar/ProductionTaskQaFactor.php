<?php

namespace common\models\ar;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\Expression;

/**
 * This is the model class for table "production_task_qa_factor".
 *
 * @property integer $id
 * @property integer $production_task_id
 * @property integer $qa_factor_type_id
 * @property string $value
 * @property integer $user_id
 * @property string $date_creation
 *
 * @property User $user
 * @property QaFactorType $qaFactorType
 * @property ProductionTask $productionTask
 */
class ProductionTaskQaFactor extends \yii\db\ActiveRecord {

    /**
     * @inheritdoc
     */
    public static function tableName() {
        return 'production_task_qa_factor';
    }

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
			[['production_task_id', 'qa_factor_type_id', 'value', 'user_id'], 'required'],
			[['production_task_id', 'qa_factor_type_id', 'user_id'], 'integer'],
			[['date_creation'], 'safe'],
			[['value'], 'string', 'max' => 64],
			[['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['user_id' => 'id']],
			[['qa_factor_type_id'], 'exist', 'skipOnError' => true, 'targetClass' => QaFactorType::className(), 'targetAttribute' => ['qa_factor_type_id' => 'id']],
			[['production_task_id'], 'exist', 'skipOnError' => true, 'targetClass' => ProductionTask::className(), 'targetAttribute' => ['production_task_id' => 'id']]
		];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return [
            'id' => Yii::t('main', 'ID'),
            'production_task_id' => Yii::t('main', 'Production Task ID'),
            'qa_factor_type_id' => Yii::t('main', 'Qa Factor Type ID'),
            'value' => Yii::t('main', 'Value'),
            'user_id' => Yii::t('main', 'User ID'),
            'date_creation' => Yii::t('main', 'Date Creation'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser() {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getQaFactorType() {
        return $this->hasOne(QaFactorType::className(), ['id' => 'qa_factor_type_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProductionTask() {
        return $this->hasOne(ProductionTask::className(), ['id' => 'production_task_id']);
    }

    /**
     * @inheritdoc
     * @return \common\models\aq\ProductionTaskQaFactorQuery the active query used by this AR class.
     */
    public static function find() {
        return new \common\models\aq\ProductionTaskQaFactorQuery(get_called_class());
    }
    
    public function behaviors() {
        return [
            [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'date_creation',
                'updatedAtAttribute' => false,
                'value' => new Expression('NOW()'),
            ],
        ];
    }
}
