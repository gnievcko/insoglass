<?php

namespace common\models\ar;

use Yii;

/**
 * This is the model class for table "product_attribute_type".
 *
 * @property integer $id
 * @property integer $product_attribute_group_id 
 * @property string $symbol
 * @property string $variable_type
 * @property string $attribute_checked_ids
 * @property string $attribute_blocked_ids
 * @property integer $is_visible 
 * @property integer $is_visible_for_client
 *
 * @property ActivityProductAttribute[] $activityProductAttributes
 * @property Activity[] $activities
 * @property OfferedProductAttribute[] $offeredProductAttributes
 * @property ProductAttributeGroup $productAttributeGroup 
 * @property OfferedProduct[] $offeredProducts
 * @property OrderOfferedProductAttribute[] $orderOfferedProductAttributes
 * @property OrderOfferedProduct[] $orderOfferedProducts
 * @property ProductAttributeTypeTranslation[] $productAttributeTypeTranslations
 * @property Language[] $languages
 * @property ProductionPathStepAttribute[] $productionPathStepAttributes 
 * @property ProductionPathStep[] $productionPathSteps 
 */
class ProductAttributeType extends \yii\db\ActiveRecord {

    /**
     * @inheritdoc
     */
    public static function tableName() {
        return 'product_attribute_type';
    }

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
        		[['product_attribute_group_id', 'is_visible'], 'integer'],
				[['symbol', 'variable_type'], 'required'],
				[['symbol', 'variable_type'], 'string', 'max' => 32],
                [['attribute_checked_ids', 'attribute_blocked_ids'], 'string', 'max' => 64],
                [['attribute_checked_ids'], 'default', 'value' => null],
                [['attribute_blocked_ids'], 'default', 'value' => null],
                [['attribute_checked_ids', 'attribute_blocked_ids'], 'validateString'],
				[['symbol'], 'unique'],
        		[['product_attribute_group_id'], 'exist', 'skipOnError' => true, 'targetClass' => ProductAttributeGroup::className(), 'targetAttribute' => ['product_attribute_group_id' => 'id']],
		];
    }

    public function validateString($attribute, $params) {
        $pattern = '/[[:digit:]]+ (,[[:digit:]]+)*/';
        if(!empty($this->attribute_blocked_ids)) {
            if($this->attribute_blocked_ids === '') {
                $this->attribute_blocked_ids = null;
            }
            else if(!preg_match($pattern, $this->attribute_blocked_ids)) {
                $this->addError('attribute_blocked_ids', Yii::t('web', 'Wrong data entered'));  
            }
        }
        if(!empty($this->attribute_checked_ids)) {
            if($this->attribute_checked_ids === '') {
                $this->attribute_checked_ids = null;
            }
            else if(preg_match($pattern, $this->attribute_checked_ids)) {
                $this->addError('attribute_checked_ids', Yii::t('web', 'Wrong data entered'));
            }
        }
        
        return true;
    }
    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return [
            	'id' => Yii::t('main', 'ID'),
            	'product_attribute_group_id' => Yii::t('main', 'Product Attribute Group ID'), 
                'symbol' => Yii::t('main', 'Symbol'),
                'variable_type' => Yii::t('main', 'Variable Type'),
                'attribute_checked_ids' => Yii::t('main', 'Attribute Checked Ids'),
                'attribute_blocked_ids' => Yii::t('main', 'Attribute Blocked Ids'),
                'is_visible' => Yii::t('main', 'Is visible'),
                'groupSymbol' => Yii::t('main', 'Group'),
        ];
    }
    
    public function getProductAttributeGroup() {
    	return $this->hasOne(ProductAttributeGroup::className(), ['id' => 'product_attribute_group_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getActivityProductAttributes() {
        return $this->hasMany(ActivityProductAttribute::className(), ['product_attribute_type_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getActivities() {
        return $this->hasMany(Activity::className(), ['id' => 'activity_id'])->viaTable('activity_product_attribute', ['product_attribute_type_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOfferedProductAttributes() {
        return $this->hasMany(OfferedProductAttribute::className(), ['product_attribute_type_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOfferedProducts() {
        return $this->hasMany(OfferedProduct::className(), ['id' => 'offered_product_id'])->viaTable('offered_product_attribute', ['product_attribute_type_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrderOfferedProductAttributes() {
        return $this->hasMany(OrderOfferedProductAttribute::className(), ['product_attribute_type_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrderOfferedProducts() {
        return $this->hasMany(OrderOfferedProduct::className(), ['id' => 'order_offered_product_id'])->viaTable('order_offered_product_attribute', ['product_attribute_type_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProductAttributeTypeTranslations() {
        return $this->hasMany(ProductAttributeTypeTranslation::className(), ['product_attribute_type_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLanguages() {
        return $this->hasMany(Language::className(), ['id' => 'language_id'])->viaTable('product_attribute_type_translation', ['product_attribute_type_id' => 'id']);
    }
    
    public function getProductionPathStepAttributes() {
    	return $this->hasMany(ProductionPathStepAttribute::className(), ['product_attribute_type_id' => 'id']);
    }
    
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProductionPathSteps() {
    	return $this->hasMany(ProductionPathStep::className(), ['id' => 'production_path_step_id'])->viaTable('production_path_step_attribute', ['product_attribute_type_id' => 'id']);
    }

    public function getGroupSymbol() {
        return $this->productAttributeGroup->symbol;
    }
    /**
     * @inheritdoc
     * @return \common\models\aq\ProductAttributeTypeQuery the active query used by this AR class.
     */
    public static function find() {
        return new \common\models\aq\ProductAttributeTypeQuery(get_called_class());
    }
}
