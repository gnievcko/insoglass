<?php

namespace common\models\ar;

use Yii;

/**
 * This is the model class for table "banner".
 *
 * @property integer $id
 * @property string $symbol
 *
 * @property BannerTranslation[] $bannerTranslations
 * @property Language[] $languages
 * @property SliderBanner[] $sliderBanners
 * @property Slider[] $sliders
 */
class Banner extends \yii\db\ActiveRecord {

    /**
     * @inheritdoc
     */
    public static function tableName() {
        return 'banner';
    }

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
				[['symbol'], 'required'],
				[['symbol'], 'string', 'max' => 32],
				[['symbol'], 'unique']
		];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return [
            	'id' => Yii::t('main', 'ID'),
            	'symbol' => Yii::t('main', 'Symbol'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBannerTranslations() {
        return $this->hasMany(BannerTranslation::className(), ['banner_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLanguages() {
        return $this->hasMany(Language::className(), ['id' => 'language_id'])->viaTable('banner_translation', ['banner_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSliderBanners() {
        return $this->hasMany(SliderBanner::className(), ['banner_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSliders() {
        return $this->hasMany(Slider::className(), ['id' => 'slider_id'])->viaTable('slider_banner', ['banner_id' => 'id']);
    }

    /**
     * @inheritdoc
     * @return \common\models\aq\BannerQuery the active query used by this AR class.
     */
    public static function find() {
        return new \common\models\aq\BannerQuery(get_called_class());
    }
}
