<?php

namespace common\models\ar;

use Yii;

/**
 * This is the model class for table "currency".
 *
 * @property integer $id
 * @property string $symbol
 * @property string $short_symbol
 * @property string $conversion_value
 *
 * @property Company[] $companies
 * @property OfferedProductTranslation[] $offeredProductTranslations
 * @property ProductTranslation[] $productTranslations
 * @property User[] $users
 * @property WarehouseDeliveryProduct[] $warehouseDeliveryProducts
 */
class Currency extends \yii\db\ActiveRecord {

    /**
     * @inheritdoc
     */
    public static function tableName() {
        return 'currency';
    }

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
				[['symbol', 'conversion_value'], 'required'],
				[['conversion_value'], 'number'],
				[['symbol', 'short_symbol'], 'string', 'max' => 10],
				[['symbol'], 'unique'],
				[['short_symbol'], 'unique']
		];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return [
            	'id' => Yii::t('main', 'ID'),
            	'symbol' => Yii::t('main', 'Symbol'),
            	'short_symbol' => Yii::t('main', 'Short symbol'),
            	'conversion_value' => Yii::t('main', 'Conversion Value'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCompanies() {
        return $this->hasMany(Company::className(), ['currency_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOfferedProductTranslations() {
        return $this->hasMany(OfferedProductTranslation::className(), ['currency_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProductTranslations() {
        return $this->hasMany(ProductTranslation::className(), ['currency_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUsers() {
        return $this->hasMany(User::className(), ['currency_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getWarehouseDeliveryProducts() {
        return $this->hasMany(WarehouseDeliveryProduct::className(), ['currency_id' => 'id']);
    }

    /**
     * @inheritdoc
     * @return \common\models\aq\CurrencyQuery the active query used by this AR class.
     */
    public static function find() {
        return new \common\models\aq\CurrencyQuery(get_called_class());
    }
}
