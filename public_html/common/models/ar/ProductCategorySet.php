<?php

namespace common\models\ar;

use Yii;

/**
 * This is the model class for table "product_category_set".
 *
 * @property integer $product_id
 * @property integer $product_category_id
 *
 * @property ProductCategory $productCategory
 * @property Product $product
 */
class ProductCategorySet extends \yii\db\ActiveRecord {

    /**
     * @inheritdoc
     */
    public static function tableName() {
        return 'product_category_set';
    }

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
				[['product_id', 'product_category_id'], 'required'],
				[['product_id', 'product_category_id'], 'integer'],
				[['product_category_id'], 'exist', 'skipOnError' => true, 'targetClass' => ProductCategory::className(), 'targetAttribute' => ['product_category_id' => 'id']],
				[['product_id'], 'exist', 'skipOnError' => true, 'targetClass' => Product::className(), 'targetAttribute' => ['product_id' => 'id']]
		];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return [
            	'product_id' => Yii::t('main', 'Product'),
            	'product_category_id' => Yii::t('main', 'Product Category ID'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProductCategory() {
        return $this->hasOne(ProductCategory::className(), ['id' => 'product_category_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProduct() {
        return $this->hasOne(Product::className(), ['id' => 'product_id']);
    }

    /**
     * @inheritdoc
     * @return \common\models\aq\ProductCategorySetQuery the active query used by this AR class.
     */
    public static function find() {
        return new \common\models\aq\ProductCategorySetQuery(get_called_class());
    }
}
