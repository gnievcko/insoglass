<?php

namespace common\models\ar;

use Yii;

/**
 * This is the model class for table "province_zip_code_rule".
 *
 * @property integer $id
 * @property integer $province_id
 * @property string $zip_code
 *
 * @property Province $province
 */
class ProvinceZipCodeRule extends \yii\db\ActiveRecord {

    /**
     * @inheritdoc
     */
    public static function tableName() {
        return 'province_zip_code_rule';
    }

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
				[['province_id'], 'integer'],
				[['zip_code'], 'required'],
				[['zip_code'], 'string', 'max' => 16],
				[['province_id'], 'exist', 'skipOnError' => true, 'targetClass' => Province::className(), 'targetAttribute' => ['province_id' => 'id']]
		];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return [
            	'id' => Yii::t('main', 'ID'),
            	'province_id' => Yii::t('main', 'Province ID'),
            	'zip_code' => Yii::t('main', 'Zip Code'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProvince() {
        return $this->hasOne(Province::className(), ['id' => 'province_id']);
    }

    /**
     * @inheritdoc
     * @return \common\models\aq\ProvinceZipCodeRuleQuery the active query used by this AR class.
     */
    public static function find() {
        return new \common\models\aq\ProvinceZipCodeRuleQuery(get_called_class());
    }
}
