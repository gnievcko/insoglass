<?php

namespace common\models\ar;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\Expression;
use common\helpers\StringHelper;

/**
 * This is the model class for table "user_order_salesman_history".
 *
 * @property integer $id
 * @property integer $user_id
 * @property string $date
 * @property integer $history_id
 * @property integer $is_added
 *
 * @property OrderHistory $history
 * @property User $user
 */
class UserOrderSalesmanHistory extends \yii\db\ActiveRecord {

    /**
     * @inheritdoc
     */
    public static function tableName() {
        return 'user_order_salesman_history';
    }

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
				[['user_id', 'history_id', 'is_added'], 'required'],
				[['user_id', 'history_id', 'is_added'], 'integer'],
				[['date'], 'safe'],
				[['history_id'], 'exist', 'skipOnError' => true, 'targetClass' => OrderHistory::className(), 'targetAttribute' => ['history_id' => 'id']],
				[['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['user_id' => 'id']]
		];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return [
            	'id' => Yii::t('main', 'ID'),
            	'userName' => Yii::t('main', 'Salesman'),
            	'date' => Yii::t('main', 'Date'),
            	'history_id' => StringHelper::translateOrderToOffer('main', 'Order update id'),
            	'is_added' => Yii::t('main', 'Operation type'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getHistory() {
        return $this->hasOne(OrderHistory::className(), ['id' => 'history_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser() {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }
    
    public function getUserName() {
    	return User::discoverNameForm($this->user);
    }

    /**
     * @inheritdoc
     * @return \common\models\aq\UserOrderSalesmanHistoryQuery the active query used by this AR class.
     */
    public static function find() {
        return new \common\models\aq\UserOrderSalesmanHistoryQuery(get_called_class());
    }
    
    public function behaviors() {
    	return [
    			[
    					'class' => TimestampBehavior::className(),
    					'createdAtAttribute' => 'date',
    					'value' => new Expression('NOW()'),
    			],
    	];
    }
}
