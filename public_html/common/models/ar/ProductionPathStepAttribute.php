<?php

namespace common\models\ar;

use Yii;

/**
 * This is the model class for table "production_path_step_attribute".
 *
 * @property integer $production_path_step_id
 * @property integer $product_attribute_type_id
 *
 * @property ProductAttributeType $productAttributeType
 * @property ProductionPathStep $productionPathStep
 */
class ProductionPathStepAttribute extends \yii\db\ActiveRecord {

    /**
     * @inheritdoc
     */
    public static function tableName() {
        return 'production_path_step_attribute';
    }

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
				[['production_path_step_id', 'product_attribute_type_id'], 'required'],
				[['production_path_step_id', 'product_attribute_type_id'], 'integer'],
				[['product_attribute_type_id'], 'exist', 'skipOnError' => true, 'targetClass' => ProductAttributeType::className(), 'targetAttribute' => ['product_attribute_type_id' => 'id']],
				[['production_path_step_id'], 'exist', 'skipOnError' => true, 'targetClass' => ProductionPathStep::className(), 'targetAttribute' => ['production_path_step_id' => 'id']]
		];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return [
            	'production_path_step_id' => Yii::t('main', 'Production Path Step ID'),
            	'product_attribute_type_id' => Yii::t('main', 'Product Attribute Type ID'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProductAttributeType() {
        return $this->hasOne(ProductAttributeType::className(), ['id' => 'product_attribute_type_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProductionPathStep() {
        return $this->hasOne(ProductionPathStep::className(), ['id' => 'production_path_step_id']);
    }

    /**
     * @inheritdoc
     * @return \common\models\aq\ProductionPathStepAttributeQuery the active query used by this AR class.
     */
    public static function find() {
        return new \common\models\aq\ProductionPathStepAttributeQuery(get_called_class());
    }
}
