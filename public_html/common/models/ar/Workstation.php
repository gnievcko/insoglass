<?php

namespace common\models\ar;

use Yii;

/**
 * This is the model class for table "workstation".
 *
 * @property integer $id
 * @property integer $department_id
 * @property string $symbol
 *
 * @property UserWorkstation[] $userWorkstations
 * @property User[] $users
 * @property Department $department
 * @property WorkstationTranslation[] $workstationTranslations
 * @property Language[] $languages
 */
class Workstation extends \yii\db\ActiveRecord {

    /**
     * @inheritdoc
     */
    public static function tableName() {
        return 'workstation';
    }

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
				[['department_id', 'symbol'], 'required'],
				[['department_id'], 'integer'],
				[['symbol'], 'string', 'max' => 32],
				[['symbol'], 'unique'],
				[['department_id'], 'exist', 'skipOnError' => true, 'targetClass' => Department::className(), 'targetAttribute' => ['department_id' => 'id']]
		];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return [
            	'id' => Yii::t('main', 'ID'),
            	'department_id' => Yii::t('main', 'Department ID'),
            	'symbol' => Yii::t('main', 'Symbol'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUserWorkstations() {
        return $this->hasMany(UserWorkstation::className(), ['workstation_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUsers() {
        return $this->hasMany(User::className(), ['id' => 'user_id'])->viaTable('user_workstation', ['workstation_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDepartment() {
        return $this->hasOne(Department::className(), ['id' => 'department_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getWorkstationTranslations() {
        return $this->hasMany(WorkstationTranslation::className(), ['workstation_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLanguages() {
        return $this->hasMany(Language::className(), ['id' => 'language_id'])->viaTable('workstation_translation', ['workstation_id' => 'id']);
    }

    /**
     * @inheritdoc
     * @return \common\models\aq\WorkstationQuery the active query used by this AR class.
     */
    public static function find() {
        return new \common\models\aq\WorkstationQuery(get_called_class());
    }
    
    public function getTranslation() {
        $translations = $this->workstationTranslations;
        if(empty($translations)) {
            return null;
        }
        
        foreach($translations as $t) {
            if($t->language->symbol === Yii::$app->language) {
                return $t;
            }
        }
        
        return $translations[0];
    }
}
