<?php

namespace common\models\ar;

use Yii;

/**
 * This is the model class for table "warehouse_delivery_product".
 *
 * @property integer $id
 * @property integer $warehouse_delivery_id
 * @property integer $product_id
 * @property integer $count
 * @property string $price_unit
 * @property string $price_group
 * @property integer $currency_id
 * @property string $unit
 * @property string $note
 *
 * @property Currency $currency
 * @property WarehouseDelivery $warehouseDelivery
 * @property Product $product
 */
class WarehouseDeliveryProduct extends \yii\db\ActiveRecord {

    /**
     * @inheritdoc
     */
    public static function tableName() {
        return 'warehouse_delivery_product';
    }

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
				[['warehouse_delivery_id', 'product_id', 'count'], 'required'],
				[['warehouse_delivery_id', 'product_id', 'count', 'currency_id'], 'integer'],
				[['price_unit', 'price_group'], 'number'],
        		[['unit'], 'string', 'max' => 10],
        		[['note'], 'string', 'max' => 1024],
				[['currency_id'], 'exist', 'skipOnError' => true, 'targetClass' => Currency::className(), 'targetAttribute' => ['currency_id' => 'id']],
				[['warehouse_delivery_id'], 'exist', 'skipOnError' => true, 'targetClass' => WarehouseDelivery::className(), 'targetAttribute' => ['warehouse_delivery_id' => 'id']],
				[['product_id'], 'exist', 'skipOnError' => true, 'targetClass' => Product::className(), 'targetAttribute' => ['product_id' => 'id']]
		];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return [
            	'id' => Yii::t('main', 'ID'),
            	'warehouse_delivery_id' => Yii::t('main', 'Warehouse Delivery ID'),
            	'product_id' => Yii::t('main', 'Product'),
            	'count' => Yii::t('main', 'Count'),
            	'price_unit' => Yii::t('main', 'Price Unit'),
            	'price_group' => Yii::t('main', 'Price Group'),
            	'currency_id' => Yii::t('main', 'Currency ID'),
        		'unit' => Yii::t('main', 'Unit'),
        		'note' => Yii::t('main', 'Note'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCurrency() {
        return $this->hasOne(Currency::className(), ['id' => 'currency_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getWarehouseDelivery() {
        return $this->hasOne(WarehouseDelivery::className(), ['id' => 'warehouse_delivery_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProduct() {
        return $this->hasOne(Product::className(), ['id' => 'product_id']);
    }

    /**
     * @inheritdoc
     * @return \common\models\aq\WarehouseDeliveryProductQuery the active query used by this AR class.
     */
    public static function find() {
        return new \common\models\aq\WarehouseDeliveryProductQuery(get_called_class());
    }
}
