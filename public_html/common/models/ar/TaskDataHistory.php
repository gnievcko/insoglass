<?php

namespace common\models\ar;

use Yii;

/**
 * This is the model class for table "task_data_history".
 *
 * @property integer $task_history_id
 * @property integer $task_type_reference_id
 * @property integer $referenced_object_id
 *
 * @property TaskTypeReference $taskTypeReference
 * @property TaskHistory $taskHistory
 */
class TaskDataHistory extends \yii\db\ActiveRecord {

    /**
     * @inheritdoc
     */
    public static function tableName() {
        return 'task_data_history';
    }

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
				[['task_history_id', 'task_type_reference_id', 'referenced_object_id'], 'required'],
				[['task_history_id', 'task_type_reference_id', 'referenced_object_id'], 'integer'],
				[['task_type_reference_id'], 'exist', 'skipOnError' => true, 'targetClass' => TaskTypeReference::className(), 'targetAttribute' => ['task_type_reference_id' => 'id']],
				[['task_history_id'], 'exist', 'skipOnError' => true, 'targetClass' => TaskHistory::className(), 'targetAttribute' => ['task_history_id' => 'id']]
		];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return [
            	'task_history_id' => Yii::t('main', 'Task History ID'),
            	'task_type_reference_id' => Yii::t('main', 'Task Type Reference ID'),
            	'referenced_object_id' => Yii::t('main', 'Referenced Object ID'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTaskTypeReference() {
        return $this->hasOne(TaskTypeReference::className(), ['id' => 'task_type_reference_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTaskHistory() {
        return $this->hasOne(TaskHistory::className(), ['id' => 'task_history_id']);
    }

    /**
     * @inheritdoc
     * @return \common\models\aq\TaskDataHistoryQuery the active query used by this AR class.
     */
    public static function find() {
        return new \common\models\aq\TaskDataHistoryQuery(get_called_class());
    }
}
