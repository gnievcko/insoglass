<?php

namespace common\models\ar;

use Yii;

/**
 * This is the model class for table "order_status_transition".
 *
 * @property integer $order_status_predecessor_id
 * @property integer $order_status_successor_id
 *
 * @property OrderStatus $orderStatusSuccessor
 * @property OrderStatus $orderStatusPredecessor
 */
class OrderStatusTransition extends \yii\db\ActiveRecord {

    /**
     * @inheritdoc
     */
    public static function tableName() {
        return 'order_status_transition';
    }

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
				[['order_status_predecessor_id', 'order_status_successor_id'], 'required'],
				[['order_status_predecessor_id', 'order_status_successor_id'], 'integer'],
				[['order_status_successor_id'], 'exist', 'skipOnError' => true, 'targetClass' => OrderStatus::className(), 'targetAttribute' => ['order_status_successor_id' => 'id']],
				[['order_status_predecessor_id'], 'exist', 'skipOnError' => true, 'targetClass' => OrderStatus::className(), 'targetAttribute' => ['order_status_predecessor_id' => 'id']],
                [['order_status_predecessor_id'], 'compare', 'compareAttribute'=>'order_status_successor_id', 'operator'=>'!='],
        		[['order_status_predecessor_id'], 'unique', 'targetAttribute' => ['order_status_predecessor_id', 'order_status_successor_id'], 'comboNotUnique' => Yii::t('main', 'The chosen statuses transition already exists.')],
		];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return [
            	'order_status_predecessor_id' => Yii::t('main', 'Predecessor status'),
            	'order_status_successor_id' => Yii::t('main', 'Successor status'),
            	'orderStatusSuccessorTranslation' => Yii::t('main', 'Successor'),
            	'orderStatusPredecessorTranslation' => Yii::t('main', 'Predecessor'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrderStatusSuccessor() {
        return $this->hasOne(OrderStatus::className(), ['id' => 'order_status_successor_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrderStatusPredecessor() {
        return $this->hasOne(OrderStatus::className(), ['id' => 'order_status_predecessor_id']);
    }

    public function getOrderStatusPredecessorSymbol() {
        return $this->orderStatusPredecessor->symbol;
    }

    public function getOrderStatusSuccessorSymbol() {
        return $this->orderStatusSuccessor->symbol;
    }

    public function getOrderStatusPredecessorTranslation() {
        return empty($this->orderStatusPredecessor->orderStatusTranslation) ? $this->orderStatusPredecessorSymbol : $this->orderStatusPredecessor->orderStatusTranslation->name;
    }

    public function getOrderStatusSuccessorTranslation() {
        return empty($this->orderStatusSuccessor->orderStatusTranslation) ? $this->orderStatusSuccessorSymbol : $this->orderStatusSuccessor->orderStatusTranslation->name;
    }

    /**
     * @inheritdoc
     * @return \common\models\aq\OrderStatusTransitionQuery the active query used by this AR class.
     */
    public static function find() {
        return new \common\models\aq\OrderStatusTransitionQuery(get_called_class());
    }
}
