<?php

namespace common\models\ar;

use Yii;

/**
 * This is the model class for table "alert".
 *
 * @property integer $id
 * @property integer $alert_type_id
 * @property integer $user_id
 * @property string $message
 * @property integer $is_active
 * @property string $date_creation
 * @property integer $order_id
 * @property integer $product_id
 * @property integer $company_id
 * @property integer $task_id
 * @property integer $warehouse_id
 *
 * @property AlertType $alertType
 * @property Company $company
 * @property Order $order
 * @property Product $product
 * @property Task $task
 * @property User $user
 * @property User $user0
 * @property Warehouse $warehouse
 */
class Alert extends \yii\db\ActiveRecord {

    /**
     * @inheritdoc
     */
    public static function tableName() {
        return 'alert';
    }

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
				[['alert_type_id', 'message'], 'required'],
				[['alert_type_id', 'user_id', 'is_active', 'order_id', 'product_id', 'company_id', 'task_id', 'warehouse_id'], 'integer'],
				[['date_creation'], 'safe'],
				[['message'], 'string', 'max' => 2048],
				[['alert_type_id'], 'exist', 'skipOnError' => true, 'targetClass' => AlertType::className(), 'targetAttribute' => ['alert_type_id' => 'id']],
				[['company_id'], 'exist', 'skipOnError' => true, 'targetClass' => Company::className(), 'targetAttribute' => ['company_id' => 'id']],
				[['order_id'], 'exist', 'skipOnError' => true, 'targetClass' => Order::className(), 'targetAttribute' => ['order_id' => 'id']],
				[['product_id'], 'exist', 'skipOnError' => true, 'targetClass' => Product::className(), 'targetAttribute' => ['product_id' => 'id']],
				[['task_id'], 'exist', 'skipOnError' => true, 'targetClass' => Task::className(), 'targetAttribute' => ['task_id' => 'id']],
				[['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['user_id' => 'id']],
				[['warehouse_id'], 'exist', 'skipOnError' => true, 'targetClass' => Warehouse::className(), 'targetAttribute' => ['warehouse_id' => 'id']]
		];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return [
            	'id' => Yii::t('main', 'ID'),
            	'alert_type_id' => Yii::t('main', 'Alert Type'),
            	'user_id' => Yii::t('main', 'User'),
            	'message' => Yii::t('main', 'Message'),
            	'is_active' => Yii::t('main', 'Is Active'),
            	'date_creation' => Yii::t('main', 'Date Creation'),
            	'order_id' => Yii::t('main', 'Order'),
            	'product_id' => Yii::t('main', 'Product'),
            	'company_id' => Yii::t('main', 'Company'),
            	'task_id' => Yii::t('main', 'Task'),
            	'warehouse_id' => Yii::t('main', 'Warehouse'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAlertType() {
        return $this->hasOne(AlertType::className(), ['id' => 'alert_type_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCompany() {
        return $this->hasOne(Company::className(), ['id' => 'company_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrder() {
        return $this->hasOne(Order::className(), ['id' => 'order_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProduct() {
        return $this->hasOne(Product::className(), ['id' => 'product_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTask() {
        return $this->hasOne(Task::className(), ['id' => 'task_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser() {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getWarehouse() {
        return $this->hasOne(Warehouse::className(), ['id' => 'warehouse_id']);
    }

    /**
     * @inheritdoc
     * @return \common\models\aq\AlertQuery the active query used by this AR class.
     */
    public static function find() {
        return new \common\models\aq\AlertQuery(get_called_class());
    }
}
