<?php

namespace common\models\ar;

use Yii;

/**
 * This is the model class for table "user_order_salesman".
 *
 * @property integer $user_id
 * @property integer $order_id
 * @property integer $is_main
 *
 * @property Order $order
 * @property User $user
 */
class UserOrderSalesman extends \yii\db\ActiveRecord {

    /**
     * @inheritdoc
     */
    public static function tableName() {
        return 'user_order_salesman';
    }

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
				[['user_id', 'order_id'], 'required'],
				[['user_id', 'order_id', 'is_main'], 'integer'],
				[['order_id'], 'exist', 'skipOnError' => true, 'targetClass' => Order::className(), 'targetAttribute' => ['order_id' => 'id']],
				[['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['user_id' => 'id']]
		];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return [
            	'user_id' => Yii::t('main', 'User'),
            	'order_id' => Yii::t('main', 'Order'),
            	'is_main' => Yii::t('main', 'Is Main'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrder() {
        return $this->hasOne(Order::className(), ['id' => 'order_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser() {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }

    /**
     * @inheritdoc
     * @return \common\models\aq\UserOrderSalesmanQuery the active query used by this AR class.
     */
    public static function find() {
        return new \common\models\aq\UserOrderSalesmanQuery(get_called_class());
    }
}
