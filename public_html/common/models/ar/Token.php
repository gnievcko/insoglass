<?php

namespace common\models\ar;

use Yii;
use common\helpers\Utility;
use yii\helpers\Html;
use common\components\MessageProvider;


/**
 * This is the model class for table "token".
 *
 * @property integer $id
 * @property integer $user_id
 * @property integer $token_type_id
 * @property string $date_expiration
 * @property string $token
 *
 * @property TokenType $tokenType
 * @property User $user
 */
class Token extends \yii\db\ActiveRecord {

    /**
     * @inheritdoc
     */
    public static function tableName() {
        return 'token';
    }

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
				[['user_id', 'token_type_id', 'token'], 'required'],
				[['user_id', 'token_type_id'], 'integer'],
				[['date_expiration'], 'safe'],
				[['token'], 'string', 'max' => 50],
				[['token_type_id'], 'exist', 'skipOnError' => true, 'targetClass' => TokenType::className(), 'targetAttribute' => ['token_type_id' => 'id']],
				[['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['user_id' => 'id']],
                ['token', 'unique'],
		];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return [
            	'id' => Yii::t('main', 'ID'),
            	'user_id' => Yii::t('main', 'User'),
            	'token_type_id' => Yii::t('main', 'Token Type ID'),
            	'date_expiration' => Yii::t('main', 'Date Expiration'),
            	'token' => Yii::t('main', 'Token'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTokenType() {
        return $this->hasOne(TokenType::className(), ['id' => 'token_type_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser() {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }

    /**
     * @inheritdoc
     * @return \common\models\aq\TokenQuery the active query used by this AR class.
     */
    public static function find() {
        return new \common\models\aq\TokenQuery(get_called_class());
    }
    public function regenerate($mode = null, $onlyResend = false) {
		$sent = false;
		
		if($this->tokenType->symbol === Utility::TOKEN_TYPE_RESET_PASSWORD) {
			
			if(!$onlyResend && !$this->regenerateHashes(\Yii::$app->params['user.passwordResetTokenExpire'])) {
				return false;
			}
			
			$resetLink = Yii::$app->urlManager->createAbsoluteUrl(['site/reset-password', 'token' => $this->token]);
			$link = Html::a(Html::encode($resetLink), $resetLink);
			
			$messageProvider = new MessageProvider();
			$sent = $messageProvider->send($this->user, NULL, Utility::EMAIL_TYPE_RESET_PASSWORD, $mode, [], ['{firstname}' => $this->user->first_name, '{lastname}' => $this->user->last_name, '{link_with_token}' => $link], false);
		}
		return $sent;
    }
    public function regenerateHashes($seconds = 259200) {
    	$this->token = Yii::$app->security->generateRandomString();
    	$this->date_expiration = date('Y-m-d H:i:s', time() + $seconds);
    
    	while(!$this->validate(['token'])) {
    		$this->token = Yii::$app->security->generateRandomString();
    	}
    
    	return $this->save();
    }
}
