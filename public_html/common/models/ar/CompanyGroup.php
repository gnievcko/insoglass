<?php

namespace common\models\ar;

use Yii;

/**
 * This is the model class for table "company_group".
 *
 * @property integer $id
 * @property string $symbol
 * @property integer $is_predefined
 *
 * @property CompanyGroupSet[] $companyGroupSets
 * @property Company[] $companies
 * @property CompanyGroupTranslation[] $companyGroupTranslations
 * @property Language[] $languages
 * @property UserCompanySalesman[] $userCompanySalesmen
 * @property User[] $users
 */
class CompanyGroup extends \yii\db\ActiveRecord {

    /**
     * @inheritdoc
     */
    public static function tableName() {
        return 'company_group';
    }

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
				[['symbol'], 'required'],
				[['is_predefined'], 'integer'],
				[['symbol'], 'string', 'max' => 32],
				[['symbol'], 'unique']
		];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return [
            	'id' => Yii::t('main', 'ID'),
            	'symbol' => Yii::t('main', 'Symbol'),
            	'is_predefined' => Yii::t('main', 'Is predefined'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCompanyGroupSets() {
        return $this->hasMany(CompanyGroupSet::className(), ['company_group_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCompanies() {
        return $this->hasMany(Company::className(), ['id' => 'company_id'])->viaTable('company_group_set', ['company_group_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCompanyGroupTranslations() {
        return $this->hasMany(CompanyGroupTranslation::className(), ['company_group_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLanguages() {
        return $this->hasMany(Language::className(), ['id' => 'language_id'])->viaTable('company_group_translation', ['company_group_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUserCompanySalesmen() {
        return $this->hasMany(UserCompanySalesman::className(), ['company_group_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUsers() {
        return $this->hasMany(User::className(), ['id' => 'user_id'])->viaTable('user_company_salesman', ['company_group_id' => 'id']);
    }
    
    public function getTranslation() {
    	$translations = $this->companyGroupTranslations;
    	if(empty($translations)) {
    		return null;
    	}
    
    	foreach($translations as $t) {
    		if($t->language->symbol === Yii::$app->language) {
    			return $t;
    		}
    	}
    
    	return $translations[0];
    }

    public function getExactTranslationOrNull() {
    	$translations = $this->companyGroupTranslations;
    	foreach($translations as $t) {
    		if($t->language->symbol === Yii::$app->language) {
    			return $t;
    		}
    	}
        return null;
    }

    /**
     * @inheritdoc
     * @return \common\models\aq\CompanyGroupQuery the active query used by this AR class.
     */
    public static function find() {
        return new \common\models\aq\CompanyGroupQuery(get_called_class());
    }
}
