<?php

namespace common\models\ar;

use Yii;

/**
 * This is the model class for table "product_status".
 *
 * @property integer $id
 * @property string $symbol
 * @property integer $is_active
 * @property integer $is_positive
 *
 * @property ProductStatusReference[] $productStatusReferences
 * @property ProductStatusTranslation[] $productStatusTranslations
 * @property Language[] $languages
 * @property WarehouseProductStatusHistory[] $warehouseProductStatusHistories
 */
class ProductStatus extends \yii\db\ActiveRecord {

    /**
     * @inheritdoc
     */
    public static function tableName() {
        return 'product_status';
    }

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
				[['symbol', 'is_active', 'is_positive'], 'required'],
				[['symbol'], 'string', 'max' => 32],
				[['symbol'], 'unique'],
        		[['is_positive'], 'integer'],
        		[['is_active'], 'integer'],
		];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return [
            	'id' => Yii::t('main', 'ID'),
            	'symbol' => Yii::t('main', 'Symbol'),
        		'is_active' => Yii::t('main', 'Is active'),
        		'is_positive' => Yii::t('main', 'Does operation add items'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProductStatusReferences() {
        return $this->hasMany(ProductStatusReference::className(), ['product_status_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProductStatusTranslations() {
        return $this->hasMany(ProductStatusTranslation::className(), ['product_status_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLanguages() {
        return $this->hasMany(Language::className(), ['id' => 'language_id'])->viaTable('product_status_translation', ['product_status_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getWarehouseProductStatusHistories() {
        return $this->hasMany(WarehouseProductStatusHistory::className(), ['product_status_id' => 'id']);
    }
    
    public function getTranslation() {
    	$translations = $this->productStatusTranslations;
    	if(empty($translations)) {
    		return $this->symbol;
    	}
    
    	foreach($translations as $t) {
    		if($t->language->symbol === Yii::$app->language) {
    			return $t;
    		}
    	}
    
    	return $translations[0];
    }

    public function getSymbolTranslation(){
    	$translations = $this->productStatusTranslations;
    	if(empty($translations)) {
    		return $this->symbol;
    	}
    
    	foreach($translations as $t) {
    		if($t->language->symbol === Yii::$app->language) {
    			return $t->name;
    		}
    	}
    
    	return $this->symbol;
    }
    /**
     * @inheritdoc
     * @return \common\models\aq\ProductStatusQuery the active query used by this AR class.
     */
    public static function find() {
        return new \common\models\aq\ProductStatusQuery(get_called_class());
    }
}
