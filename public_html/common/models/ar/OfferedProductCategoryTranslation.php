<?php

namespace common\models\ar;

use Yii;

/**
 * This is the model class for table "offered_product_category_translation".
 *
 * @property integer $offered_product_category_id
 * @property integer $language_id
 * @property string $name
 * @property string $description
 *
 * @property OfferedProductCategory $offeredProductCategory
 * @property Language $language
 */
class OfferedProductCategoryTranslation extends \yii\db\ActiveRecord {

    /**
     * @inheritdoc
     */
    public static function tableName() {
        return 'offered_product_category_translation';
    }

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
				[['offered_product_category_id', 'language_id', 'name'], 'required'],
				[['offered_product_category_id', 'language_id'], 'integer'],
				[['name'], 'string', 'max' => 128],
				[['description'], 'string', 'max' => 1024],
				[['offered_product_category_id'], 'exist', 'skipOnError' => true, 'targetClass' => OfferedProductCategory::className(), 'targetAttribute' => ['offered_product_category_id' => 'id']],
				[['language_id'], 'exist', 'skipOnError' => true, 'targetClass' => Language::className(), 'targetAttribute' => ['language_id' => 'id']]
		];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return [
            	'offered_product_category_id' => Yii::t('main', 'Offered Product Category ID'),
            	'language_id' => Yii::t('main', 'Language ID'),
            	'name' => Yii::t('main', 'Name'),
            	'description' => Yii::t('main', 'Description'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOfferedProductCategory() {
        return $this->hasOne(OfferedProductCategory::className(), ['id' => 'offered_product_category_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLanguage() {
        return $this->hasOne(Language::className(), ['id' => 'language_id']);
    }

    /**
     * @inheritdoc
     * @return \common\models\aq\OfferedProductCategoryTranslationQuery the active query used by this AR class.
     */
    public static function find() {
        return new \common\models\aq\OfferedProductCategoryTranslationQuery(get_called_class());
    }
}
