<?php

namespace common\models\ar;

use Yii;

/**
 * This is the model class for table "time_unit".
 *
 * @property integer $id
 * @property string $symbol
 * @property string $conversion_minute
 *
 * @property OfferedProductTranslation[] $offeredProductTranslations
 * @property TimeUnitTranslation[] $timeUnitTranslations
 */
class TimeUnit extends \yii\db\ActiveRecord {

    /**
     * @inheritdoc
     */
    public static function tableName() {
        return 'time_unit';
    }

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
				[['symbol', 'conversion_minute'], 'required'],
				[['conversion_minute'], 'number'],
				[['symbol'], 'string', 'max' => 32],
				[['symbol'], 'unique']
		];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return [
            	'id' => Yii::t('main', 'ID'),
            	'symbol' => Yii::t('main', 'Symbol'),
            	'conversion_minute' => Yii::t('main', 'Conversion Minute'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOfferedProductTranslations() {
        return $this->hasMany(OfferedProductTranslation::className(), ['time_unit_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTimeUnitTranslations() {
        return $this->hasMany(TimeUnitTranslation::className(), ['time_unit_id' => 'id']);
    }

    /**
     * @inheritdoc
     * @return \common\models\aq\TimeUnitQuery the active query used by this AR class.
     */
    public static function find() {
        return new \common\models\aq\TimeUnitQuery(get_called_class());
    }
}
