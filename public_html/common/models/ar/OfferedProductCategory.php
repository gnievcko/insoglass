<?php

namespace common\models\ar;

use Yii;

/**
 * This is the model class for table "offered_product_category".
 *
 * @property integer $id
 * @property string $symbol
 * @property integer $parent_category_id
 * @property integer $effort_factor
 *
 * @property OfferedProductCategory $parentCategory
 * @property OfferedProductCategory[] $offeredProductCategories
 * @property OfferedProductCategorySet[] $offeredProductCategorySets
 * @property OfferedProduct[] $offeredProducts
 * @property OfferedProductCategoryTranslation[] $offeredProductCategoryTranslations
 * @property Language[] $languages
 */
class OfferedProductCategory extends \yii\db\ActiveRecord {

    /**
     * @inheritdoc
     */
    public static function tableName() {
        return 'offered_product_category';
    }

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
                [['effort_factor'], 'safe'],
				[['symbol'], 'required'],
				[['parent_category_id'], 'integer'],
				[['symbol'], 'string', 'max' => 32],
				[['symbol'], 'unique'],
				[['parent_category_id'], 'exist', 'skipOnError' => true, 'targetClass' => OfferedProductCategory::className(), 'targetAttribute' => ['parent_category_id' => 'id']]
		];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return [
            	'id' => Yii::t('main', 'ID'),
            	'symbol' => Yii::t('main', 'Symbol'),
            	'parent_category_id' => Yii::t('main', 'Parent Category ID'),
                'effort_factor' => Yii::t('main', 'Effort factor'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getParentCategory() {
        return $this->hasOne(OfferedProductCategory::className(), ['id' => 'parent_category_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOfferedProductCategories() {
        return $this->hasMany(OfferedProductCategory::className(), ['parent_category_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOfferedProductCategorySets() {
        return $this->hasMany(OfferedProductCategorySet::className(), ['offered_product_category_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOfferedProducts() {
        return $this->hasMany(OfferedProduct::className(), ['id' => 'offered_product_id'])->viaTable('offered_product_category_set', ['offered_product_category_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOfferedProductCategoryTranslations() {
        return $this->hasMany(OfferedProductCategoryTranslation::className(), ['offered_product_category_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLanguages() {
        return $this->hasMany(Language::className(), ['id' => 'language_id'])->viaTable('offered_product_category_translation', ['offered_product_category_id' => 'id']);
    }

    /**
     * @inheritdoc
     * @return \common\models\aq\OfferedProductCategoryQuery the active query used by this AR class.
     */
    public static function find() {
        return new \common\models\aq\OfferedProductCategoryQuery(get_called_class());
    }
}
