<?php

namespace common\models\ar;

use Yii;

/**
 * This is the model class for table "client_modification_history".
 *
 * @property integer $id
 * @property string $modification
 */
class ClientModificationHistory extends \yii\db\ActiveRecord {

    /**
     * @inheritdoc
     */
    public static function tableName() {
        return 'client_modification_history';
    }

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
				[['modification'], 'required'],
				[['modification'], 'string']
		];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return [
            	'id' => Yii::t('main', 'ID'),
            	'modification' => Yii::t('main', 'Modification'),
        ];
    }

    /**
     * @inheritdoc
     * @return \common\models\aq\ClientModificationHistoryQuery the active query used by this AR class.
     */
    public static function find() {
        return new \common\models\aq\ClientModificationHistoryQuery(get_called_class());
    }
}
