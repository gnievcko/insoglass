<?php

namespace common\models\ar;

use Yii;

class FileTemporaryStorage extends \yii\db\ActiveRecord {

    /**
     * @inheritdoc
     */
    public static function tableName() {
        return 'file_temporary_storage';
    }

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
		];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return [
                'id' => Yii::t('main', 'ID'),
            	'file_hash' => Yii::t('main', 'Encoded file name'),
            	'date_creation' => Yii::t('main', 'Date creation'),
                'original_file_name' => Yii::t('main', 'Original file name'),
        ];
    }
    
    /**
     * @inheritdoc
     * @return \common\models\aq\FileTemporaryStorage the active query used by this AR class.
     */
    public static function find() {
        return new \common\models\aq\FileTemporaryStorageQuery(get_called_class());
    }
}
