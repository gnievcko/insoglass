<?php

namespace common\models\ar;

use Yii;

/**
 * This is the model class for table "province".
 *
 * @property integer $id
 * @property integer $country_id
 * @property string $symbol
 * @property string $name
 *
 * @property City[] $cities
 * @property Country $country
 */
class Province extends \yii\db\ActiveRecord {

    /**
     * @inheritdoc
     */
    public static function tableName() {
        return 'province';
    }

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
				[['country_id', 'symbol', 'name'], 'required'],
				[['country_id'], 'integer'],
				[['symbol'], 'string', 'max' => 5],
				[['name'], 'string', 'max' => 64],
				[['country_id'], 'exist', 'skipOnError' => true, 'targetClass' => Country::className(), 'targetAttribute' => ['country_id' => 'id']]
		];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return [
            	'id' => Yii::t('main', 'ID'),
            	'country_id' => Yii::t('main', 'Country ID'),
            	'symbol' => Yii::t('main', 'Symbol'),
            	'name' => Yii::t('main', 'Name'),
        		'countryName' => Yii::t('main', 'Country'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCities() {
        return $this->hasMany(City::className(), ['province_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCountry() {
        return $this->hasOne(Country::className(), ['id' => 'country_id']);
    }
    
    public function getCountryName() {
    	$translation = $this->country->getTranslation();
    	return !empty($translation) ? $translation->name : $this->country->symbol;
    }

    /**
     * @inheritdoc
     * @return \common\models\aq\ProvinceQuery the active query used by this AR class.
     */
    public static function find() {
        return new \common\models\aq\ProvinceQuery(get_called_class());
    }
}
