<?php

namespace common\models\ar;

use Yii;
use yii\db\Expression;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "production_task".
 *
 * @property integer $id
 * @property integer $task_id
 * @property integer $order_offered_product_id
 * @property integer $production_path_step_id
 * @property string $task_required_ids
 * @property string $task_successor_ids
 * @property integer $is_available
 * @property integer $total_count_made
 * @property integer $total_count_failed
 * @property string $qa_factor_value
 * @property string $date_last_modification
 *
 * @property OrderOfferedProduct $orderOfferedProduct
 * @property ProductionPathStep $productionPathStep
 * @property Task $task
 * @property ProductionTaskHistory[] $productionTaskHistories
 */
class ProductionTask extends \yii\db\ActiveRecord {

    /**
     * @inheritdoc
     */
    public static function tableName() {
        return 'production_task';
    }

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
				[['task_id', 'order_offered_product_id', 'production_path_step_id'], 'required'],
				[['id', 'task_id', 'order_offered_product_id', 'production_path_step_id', 'is_available', 'total_count_made', 'total_count_failed'], 'integer'],
				[['date_last_modification', 'qa_factor_value'], 'safe'],
                [['task_required_ids', 'task_successor_ids'], 'string', 'max' => 64],
				[['order_offered_product_id'], 'exist', 'skipOnError' => true, 'targetClass' => OrderOfferedProduct::className(), 'targetAttribute' => ['order_offered_product_id' => 'id']],
				[['production_path_step_id'], 'exist', 'skipOnError' => true, 'targetClass' => ProductionPathStep::className(), 'targetAttribute' => ['production_path_step_id' => 'id']],
				[['task_id'], 'exist', 'skipOnError' => true, 'targetClass' => Task::className(), 'targetAttribute' => ['task_id' => 'id']]
		];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return [
            	'id' => Yii::t('main', 'ID'),
            	'task_id' => Yii::t('main', 'Task'),
            	'order_offered_product_id' => Yii::t('main', 'Order Offered Product ID'),
            	'production_path_step_id' => Yii::t('main', 'Production Path Step ID'),
                'task_required_ids' => Yii::t('main', 'Task Required Ids'),
                'task_successor_ids' => Yii::t('main', 'Task Successor Ids'),
            	'is_available' => Yii::t('main', 'Is Available'),
            	'total_count_made' => Yii::t('main', 'Total Count Made'),
            	'total_count_failed' => Yii::t('main', 'Total Count Failed'),
            	'date_last_modification' => Yii::t('main', 'Date Last Modification'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrderOfferedProduct() {
        return $this->hasOne(OrderOfferedProduct::className(), ['id' => 'order_offered_product_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProductionPathStep() {
        return $this->hasOne(ProductionPathStep::className(), ['id' => 'production_path_step_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTask() {
        return $this->hasOne(Task::className(), ['id' => 'task_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProductionTaskHistories() {
        return $this->hasMany(ProductionTaskHistory::className(), ['production_task_id' => 'id']);
    }

    /**
     * @inheritdoc
     * @return \common\models\aq\ProductionTaskQuery the active query used by this AR class.
     */
    public static function find() {
        return new \common\models\aq\ProductionTaskQuery(get_called_class());
    }
    
    public function behaviors() {
    	return [
    			[
    					'class' => TimestampBehavior::className(),
    					'createdAtAttribute' => 'date_last_modification',
    					'updatedAtAttribute' => false,
    					'value' => new Expression('NOW()'),
    			],
    	];
    }
}
