<?php

namespace common\models\ar;

use Yii;

/**
 * This is the model class for table "product_attribute_group_translation".
 *
 * @property integer $product_attribute_group_id
 * @property integer $language_id
 * @property string $name
 *
 * @property ProductAttributeGroup $productAttributeGroup
 * @property Language $language
 */
class ProductAttributeGroupTranslation extends \yii\db\ActiveRecord {

    /**
     * @inheritdoc
     */
    public static function tableName() {
        return 'product_attribute_group_translation';
    }

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
				[['product_attribute_group_id', 'language_id', 'name'], 'required'],
				[['product_attribute_group_id', 'language_id'], 'integer'],
				[['name'], 'string', 'max' => 128],
				[['product_attribute_group_id'], 'exist', 'skipOnError' => true, 'targetClass' => ProductAttributeGroup::className(), 'targetAttribute' => ['product_attribute_group_id' => 'id']],
				[['language_id'], 'exist', 'skipOnError' => true, 'targetClass' => Language::className(), 'targetAttribute' => ['language_id' => 'id']],
                [['product_attribute_group_id'], 'unique', 'targetAttribute' => ['product_attribute_group_id', 'language_id'], 'comboNotUnique' => Yii::t('main','The chosen country-language pair has a translation already.')],
		];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return [
            	'product_attribute_group_id' => Yii::t('main', 'Product Attribute Group ID'),
            	'language_id' => Yii::t('main', 'Language ID'),
            	'name' => Yii::t('main', 'Name'),
                'symbol' => Yii::t('main', 'Symbol'),
                'languageName' => Yii::t('main', 'Language'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProductAttributeGroup() {
        return $this->hasOne(ProductAttributeGroup::className(), ['id' => 'product_attribute_group_id']);
    }

    public function getSymbol() {
        return $this->productAttributeGroup->symbol;
    }
    
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLanguage() {
        return $this->hasOne(Language::className(), ['id' => 'language_id']);
    }

    public function getLanguageName() {
        return $this->language->name;
    }
    /**
     * @inheritdoc
     * @return \common\models\aq\ProductAttributeGroupTranslationQuery the active query used by this AR class.
     */
    public static function find() {
        return new \common\models\aq\ProductAttributeGroupTranslationQuery(get_called_class());
    }
}
