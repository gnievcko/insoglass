<?php

namespace common\models\ar;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\Expression;

/**
 * This is the model class for table "order_offered_product_attachment".
 *
 * @property integer $id
 * @property integer $order_offered_product_id
 * @property integer $user_id
 * @property string $url_file
 * @property string $name
 * @property string $description
 * @property string $date_creation
 *
 * @property User $user
 * @property OrderOfferedProduct $orderOfferedProduct
 */
class OrderOfferedProductAttachment extends \yii\db\ActiveRecord {
    public $urlFile = 11;
    
    /**
     * @inheritdoc
     */
    public static function tableName() {
        return 'order_offered_product_attachment';
    }

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
				[['order_offered_product_id', 'user_id', 'url_file', 'name'], 'required'],
				[['order_offered_product_id', 'user_id'], 'integer'],
				[['date_creation'], 'safe'],
				[['url_file', 'name'], 'string', 'max' => 256],
				[['description'], 'string', 'max' => 512],
				[['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['user_id' => 'id']],
				[['order_offered_product_id'], 'exist', 'skipOnError' => true, 'targetClass' => OrderOfferedProduct::className(), 'targetAttribute' => ['order_offered_product_id' => 'id']]
		];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return [
            	'id' => Yii::t('main', 'ID'),
            	'order_offered_product_id' => Yii::t('main', 'Order Offered Product ID'),
            	'user_id' => Yii::t('main', 'User'),
            	'url_file' => Yii::t('main', 'Url File'),
            	'name' => Yii::t('main', 'Name'),
            	'description' => Yii::t('main', 'Description'),
            	'date_creation' => Yii::t('main', 'Date Creation'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser() {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrderOfferedProduct() {
        return $this->hasOne(OrderOfferedProduct::className(), ['id' => 'order_offered_product_id']);
    }

    /**
     * @inheritdoc
     * @return \common\models\aq\OrderOfferedProductAttachmentQuery the active query used by this AR class.
     */
    public static function find() {
        return new \common\models\aq\OrderOfferedProductAttachmentQuery(get_called_class());
    }
    
    public function behaviors() {
    	return [
    			[
    					'class' => TimestampBehavior::className(),
    					'createdAtAttribute' => 'date_creation',
    					'updatedAtAttribute' => false,
    					'value' => new Expression('NOW()'),
    			],
    	];
    }
}
