<?php

namespace common\models\ar;

use Yii;

/**
 * This is the model class for table "production_path_step".
 *
 * @property integer $id
 * @property integer $production_path_id
 * @property integer $activity_id
 * @property integer $is_requirement_type_or
 * @property string $step_required_ids
 * @property string $step_successor_ids
 * @property string $attribute_needed_ids 
 * @property string $attribute_forbidden_ids 
 *
 * @property Activity $activity
 * @property ProductionPath $productionPath
 * @property ProductionPathStepAttribute[] $productionPathStepAttributes 
 * @property ProductAttributeType[] $productAttributeTypes 
 * @property ProductionTask[] $productionTasks
 */
class ProductionPathStep extends \yii\db\ActiveRecord {

    /**
     * @inheritdoc
     */
    public static function tableName() {
        return 'production_path_step';
    }

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
				[['production_path_id', 'activity_id'], 'required'],
				[['production_path_id', 'activity_id', 'is_requirement_type_or'], 'integer'],
				[['step_required_ids', 'step_successor_ids', 'attribute_needed_ids', 'attribute_forbidden_ids'], 'string', 'max' => 64],
				[['activity_id'], 'exist', 'skipOnError' => true, 'targetClass' => Activity::className(), 'targetAttribute' => ['activity_id' => 'id']],
				[['production_path_id'], 'exist', 'skipOnError' => true, 'targetClass' => ProductionPath::className(), 'targetAttribute' => ['production_path_id' => 'id']]
		];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return [
            	'id' => Yii::t('main', 'ID'),
            	'production_path_id' => Yii::t('main', 'Production Path ID'),
            	'activity_id' => Yii::t('main', 'Activity ID'),
                'is_requirement_type_or' => Yii::t('main', 'Is Requirement Type Or'),
            	'step_required_ids' => Yii::t('main', 'Step Required Ids'),
            	'step_successor_ids' => Yii::t('main', 'Step Successor Ids'),
                'attribute_needed_ids' => Yii::t('main', 'Attribute Needed Ids'),
                'attribute_forbidden_ids' => Yii::t('main', 'Attribute Forbidden Ids'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getActivity() {
        return $this->hasOne(Activity::className(), ['id' => 'activity_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProductionPath() {
        return $this->hasOne(ProductionPath::className(), ['id' => 'production_path_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProductionTasks() {
        return $this->hasMany(ProductionTask::className(), ['production_path_step_id' => 'id']);
    }
    
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProductionPathStepAttributes() {
        return $this->hasMany(ProductionPathStepAttribute::className(), ['production_path_step_id' => 'id']);
    }
    
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProductAttributeTypes() {
        return $this->hasMany(ProductAttributeType::className(), ['id' => 'product_attribute_type_id'])->viaTable('production_path_step_attribute', ['production_path_step_id' => 'id']);
    }

    /**
     * @inheritdoc
     * @return \common\models\aq\ProductionPathStepQuery the active query used by this AR class.
     */
    public static function find() {
        return new \common\models\aq\ProductionPathStepQuery(get_called_class());
    }
}
