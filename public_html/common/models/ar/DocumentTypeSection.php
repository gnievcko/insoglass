<?php

namespace common\models\ar;

use Yii;

/**
 * This is the model class for table "document_type_section".
 *
 * @property integer $document_type_id
 * @property integer $document_section_id
 * @property integer $sequence_number
 *
 * @property DocumentSection $documentSection
 * @property DocumentType $documentType
 */
class DocumentTypeSection extends \yii\db\ActiveRecord {

    /**
     * @inheritdoc
     */
    public static function tableName() {
        return 'document_type_section';
    }

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
				[['document_type_id', 'document_section_id', 'sequence_number'], 'required'],
				[['document_type_id', 'document_section_id', 'sequence_number'], 'integer'],
				[['document_type_id', 'sequence_number'], 'unique', 'targetAttribute' => ['document_type_id', 'sequence_number'], 'message' => 'The combination of Document Type ID and Sequence Number has already been taken.'],
				[['document_section_id'], 'exist', 'skipOnError' => true, 'targetClass' => DocumentSection::className(), 'targetAttribute' => ['document_section_id' => 'id']],
				[['document_type_id'], 'exist', 'skipOnError' => true, 'targetClass' => DocumentType::className(), 'targetAttribute' => ['document_type_id' => 'id']]
		];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return [
            	'document_type_id' => Yii::t('main', 'Document Type ID'),
            	'document_section_id' => Yii::t('main', 'Document Section ID'),
            	'sequence_number' => Yii::t('main', 'Sequence Number'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDocumentSection() {
        return $this->hasOne(DocumentSection::className(), ['id' => 'document_section_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDocumentType() {
        return $this->hasOne(DocumentType::className(), ['id' => 'document_type_id']);
    }

    /**
     * @inheritdoc
     * @return \common\models\aq\DocumentTypeSectionQuery the active query used by this AR class.
     */
    public static function find() {
        return new \common\models\aq\DocumentTypeSectionQuery(get_called_class());
    }
}
