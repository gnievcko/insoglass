<?php

namespace common\models\ar;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\Expression;

/**
 * This is the model class for table "offered_product".
 *
 * @property integer $id
 * @property string $symbol
 * @property integer $parent_offered_product_id
 * @property integer $priority
 * @property integer $user_id
 * @property integer $is_artificial 
 * @property integer $is_available
 * @property integer $is_active
 * @property string $date_deletion 
 * @property string $attribute_value
 * @property integer $production_path_id
 * @property string $date_creation
 *
 * @property User $user
 * @property ProductionPath $productionPath
 * @property OfferedProduct $parentOfferedProduct
 * @property OfferedProduct[] $offeredProducts
 * @property OfferedProductCategorySet[] $offeredProductCategorySets
 * @property OfferedProductCategory[] $offeredProductCategories
 * @property OfferedProductPhoto[] $offeredProductPhotos
 * @property OfferedProductTranslation[] $offeredProductTranslations
 * @property OfferedProductAttachment[] $offeredProductAttachments 
 * @property Language[] $languages
 * @property OrderOfferedProduct[] $orderOfferedProducts
 * @property OfferedProductAttribute[] $offeredProductAttributes 
 * @property ProductAttributeType[] $productAttributeTypes 
 * @property OfferedProductRequirement[] $offeredProductRequirements 
 */
class OfferedProduct extends \yii\db\ActiveRecord {

    /**
     * @inheritdoc
     */
    public static function tableName() {
        return 'offered_product';
    }

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
				[['symbol', 'is_artificial', 'is_available', 'is_active', 'user_id'], 'required'],
				[['parent_offered_product_id', 'priority', 'is_artificial', 'is_available', 'is_active', 'user_id', 'production_path_id'], 'integer'],
				[['date_creation', 'date_deletion'], 'safe'],
				[['symbol'], 'string', 'max' => 32],
        		[['attribute_value'], 'string', 'max' => 1024],
				[['symbol'], 'unique'],
				[['parent_offered_product_id'], 'exist', 'skipOnError' => true, 'targetClass' => OfferedProduct::className(), 'targetAttribute' => ['parent_offered_product_id' => 'id']],
        		[['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['user_id' => 'id']],
        		[['production_path_id'], 'exist', 'skipOnError' => true, 'targetClass' => ProductionPath::className(), 'targetAttribute' => ['production_path_id' => 'id']],
		];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return [
            	'id' => Yii::t('main', 'ID'),
            	'symbol' => Yii::t('main', 'Symbol'),
            	'parent_offered_product_id' => Yii::t('main', 'Parent Offered Product ID'),
            	'priority' => Yii::t('main', 'Priority'),
        		'is_artificial' => Yii::t('main', 'Is Artificial'),
            	'is_active' => Yii::t('main', 'Is Active'),
            	'date_creation' => Yii::t('main', 'Date Creation'),
        		'user_id' => Yii::t('main', 'User'),
        		'date_deletion' => Yii::t('main', 'Date Deletion'),
        		'attribute_value' => Yii::t('main', 'Attribute Value'),
        		'production_path_id' => Yii::t('main', 'Production Path ID'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getParentOfferedProduct() {
        return $this->hasOne(OfferedProduct::className(), ['id' => 'parent_offered_product_id']);
    }
    
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser() {
    	return $this->hasOne(User::className(), ['id' => 'user_id']);
    }
    
    public function getProductionPath() {
    	return $this->hasOne(ProductionPath::className(), ['id' => 'production_path_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOfferedProducts() {
        return $this->hasMany(OfferedProduct::className(), ['parent_offered_product_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOfferedProductCategorySets() {
        return $this->hasMany(OfferedProductCategorySet::className(), ['offered_product_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOfferedProductCategories() {
        return $this->hasMany(OfferedProductCategory::className(), ['id' => 'offered_product_category_id'])->viaTable('offered_product_category_set', ['offered_product_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOfferedProductPhotos() {
        return $this->hasMany(OfferedProductPhoto::className(), ['offered_product_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOfferedProductTranslations() {
        return $this->hasMany(OfferedProductTranslation::className(), ['offered_product_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLanguages() {
        return $this->hasMany(Language::className(), ['id' => 'language_id'])->viaTable('offered_product_translation', ['offered_product_id' => 'id']);
    }
    
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOfferedProductAttachments() {
    	return $this->hasMany(OfferedProductAttachment::className(), ['offered_product_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrderOfferedProducts() {
        return $this->hasMany(OrderOfferedProduct::className(), ['offered_product_id' => 'id']);
    }
    
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOfferedProductAttributes() {
        return $this->hasMany(OfferedProductAttribute::className(), ['offered_product_id' => 'id']);
    }
    
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProductAttributeTypes() {
        return $this->hasMany(ProductAttributeType::className(), ['id' => 'product_attribute_type_id'])->viaTable('offered_product_attribute', ['offered_product_id' => 'id']);
    }
    
    public function getOfferedProductRequirements() {
        return $this->hasMany(OfferedProductRequirement::className(), ['offered_product_id' => 'id']);
    }
    
    public function getTranslation() {
    	$translations = $this->offeredProductTranslations;
    	if(empty($translations)) {
    		return null;
    	}
    
    	foreach($translations as $t) {
    		if($t->language->symbol === Yii::$app->language) {
    			return $t;
    		}
    	}
    
    	return $translations[0];
    }

    /**
     * @inheritdoc
     * @return \common\models\aq\OfferedProductQuery the active query used by this AR class.
     */
    public static function find() {
        return new \common\models\aq\OfferedProductQuery(get_called_class());
    }
    
    public function behaviors() {
    	return [
    			[
    					'class' => TimestampBehavior::className(),
    					'createdAtAttribute' => 'date_creation',
    					'updatedAtAttribute' => false,
    					'value' => new Expression('NOW()'),
    			],
    	];
    }
}
