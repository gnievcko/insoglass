<?php

namespace common\models\ar;

use Yii;
use yii\web\IdentityInterface;
use yii\behaviors\TimestampBehavior;
use yii\db\Expression;
use yii\helpers\ArrayHelper;
use common\components\UploadTrait;
use common\models\ar\TokenType;
use common\models\ar\Token;
use common\components\MessageProvider;
use common\helpers\StrengthValidator;

/**
 * This is the model class for table "user".
 *
 * @property integer $id
 * @property integer $company_id
 * @property string $email
 * @property string $contact_email
 * @property string $password
 * @property string $first_name
 * @property string $last_name
 * @property string $phone1
 * @property string $phone2
 * @property string $url_photo
 * @property string $note
 * @property integer $language_id
 * @property integer $currency_id
 * @property string $position
 * @property integer $is_active
 * @property integer $can_logged
 * @property string $date_creation
 * @property string $date_modification
 *
 * @property Order[] $orders
 * @property OrderAttachment[] $orderAttachments
 * @property OrderHistory[] $orderHistories
 * @property OrderHistoryPhoto[] $orderHistoryPhotos
 * @property Company $company
 * @property Currency $currency
 * @property Language $language
 * @property UserCompanySalesman[] $userCompanySalesmen
 * @property Company[] $companies
 * @property UserOrderSalesman[] $userOrderSalesmen
 * @property Order[] $orders0
 * @property UserRole[] $userRoles
 * @property UserActions[] $userActions
 * @property timestamp $LastActionTime
 * @property Role[] $roles
 * @property UserWarehouse[] $userWarehouses
 * @property Warehouse[] $warehouses
 * @property WarehouseDelivery[] $warehouseDeliveries
 * @property WarehouseProductStatusHistory[] $warehouseProductStatusHistories
 * @property Alert[] $alerts
 * @property Alert[] $alerts0
 * @property Alert[] $alerts1
 * @property Task[] $tasks
 * @property Task[] $tasks0
 * @property Task[] $tasks1
 * @property Token[] $tokens
 * @property OrderUserRepresentative[] $orderUserRepresentatives 
 *
 */
class User extends \yii\db\ActiveRecord implements IdentityInterface {

    use UploadTrait;

    const USER_TYPE_EMPLOYEE = 'employee';

    public $file_photo;

    /**
     * @inheritdoc
     */
    public static function tableName() {
        return 'user';
    }

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
				[['email'], 'required'],
				[['date_creation', 'date_modification', 'lastActionTime'], 'safe'],
        		[['company_id', 'language_id', 'currency_id', 'is_active', 'can_logged', 'address_id'], 'integer'],
				[['email', 'password', 'contact_email'], 'string', 'max' => 64],
        		[['pesel', 'id_number'], 'string', 'max' => 64],
				[['email'], 'unique'],
        		[['email', 'contact_email'], 'email'],
        		[['first_name', 'last_name', 'phone1', 'phone2', 'phone3'], 'string', 'max' => 32],
        		[['url_photo', 'position'], 'string', 'max' => 256],
        		[['note'], 'string', 'max' => 1024],
        		[['company_id'], 'exist', 'skipOnError' => true, 'targetClass' => Company::className(), 'targetAttribute' => ['company_id' => 'id']],
        		[['currency_id'], 'exist', 'skipOnError' => true, 'targetClass' => Currency::className(), 'targetAttribute' => ['currency_id' => 'id']],
        		[['language_id'], 'exist', 'skipOnError' => true, 'targetClass' => Language::className(), 'targetAttribute' => ['language_id' => 'id']],
        		[['address_id'], 'exist', 'skipOnError' => true, 'targetClass' => Address::className(), 'targetAttribute' => ['address_id' => 'id']],
                [['email', 'contact_email', 'date_creation', 'date_modification', 'date_employment', 'company_id', 'language_id', 'currency_id', 'is_active', 'first_name',
                  'last_name', 'phone1', 'phone2', 'url_photo', 'note'], 'default', 'value' => null],
				[['file_photo'], 'file', 'skipOnEmpty' => true, 'extensions' => 'png, jpg, jpeg'],
                [['password'], StrengthValidator::className()]+Yii::$app->params['strengthPasswordConfig']
		];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return [
            	'id' => Yii::t('main', 'ID'),
        		'company_id' => Yii::t('main', 'Client ID'),
            	'email' => Yii::t('main', 'E-mail address'),
            	'password' => Yii::t('main', 'Password'),
        		'first_name' => Yii::t('main', 'First name'),
        		'last_name' => Yii::t('main', 'Last name'),
        		'phone1' => Yii::t('main', 'Phone'),
               	'phone2' => Yii::t('main', 'Alternative phone'),
        		'phone3' => Yii::t('web', 'Private phone'),
        		'id_number' => Yii::t('web', 'ID number'),
        		'pesel' => Yii::t('web', 'Pesel'),
        		'addressName' => Yii::t('main', 'Address'),
        		'url_photo' => Yii::t('main', 'Photo'),
        		'note' => Yii::t('main', 'Note'),
        		'language_id' => Yii::t('main', 'Language ID'),
        		'currency_id' => Yii::t('main', 'Currency ID'),
        		'is_active' => Yii::t('main', 'Is active'),
        		'lastActionTime' => Yii::t('main', 'Last login'),
	            'date_creation' => Yii::t('main', 'Creation date'),
	            'date_modification' => Yii::t('main', 'Modification date'),
	            'fullName' => Yii::t('main', 'Full name'),
				'file_photo' => Yii::t('main', 'Photo'),
        		'date_employment' => Yii::t('web', 'Employment date'),
                'can_logged' => Yii::t('main', 'Can log in'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    
    public function getAddress() {
    	return $this->hasOne(Address::className(), ['id' => 'address_id']);
    }
    
    public function getAddressName() {
    	return !empty($this->address) ? $this->address->fullAddress : null;
    }
    
	public function getOrders() {
		return $this->hasMany(Order::className(), ['user_representative_id' => 'id']);
	}

    /**
     * @return \yii\db\ActiveQuery
     */
	public function getOrderAttachments() {
		return $this->hasMany(OrderAttachment::className(), ['user_id' => 'id']);
	}

	/**
	 * @return \yii\db\ActiveQuery
	 */
	public function getOrderHistories() {
		return $this->hasMany(OrderHistory::className(), ['user_id' => 'id']);
	}

	/**
	 * @return \yii\db\ActiveQuery
	 */
	public function getOrderHistoryPhotos() {
		return $this->hasMany(OrderHistoryPhoto::className(), ['user_id' => 'id']);
	}

	/**
	 * @return \yii\db\ActiveQuery
	 */
	public function getCompany() {
		return $this->hasOne(Company::className(), ['id' => 'company_id']);
	}

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCurrency() {
    	return $this->hasOne(Currency::className(), ['id' => 'currency_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLanguage() {
    	return $this->hasOne(Language::className(), ['id' => 'language_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUserRoles() {
    	return $this->hasMany(UserRole::className(), ['user_id' => 'id']);
    }

    public function getUserActions() {
    	return $this->hasMany(UserAction::className(), ['user_id' => 'id']);
    }

    public function getUserAction() {
    	return $this->hasOne(UserAction::className(), ['user_id' => 'id']);
    }

    public function getLastActionTime() {
    	return $this->getUserActions()->max('date_action');
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRoles() {
    	return $this->hasMany(Role::className(), ['id' => 'role_id'])->viaTable('user_role', ['user_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUserCompanySalesmen() {
    	return $this->hasMany(UserCompanySalesman::className(), ['user_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCompanies() {
    	return $this->hasMany(Company::className(), ['id' => 'company_id'])->viaTable('user_company_salesman', ['user_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUserOrderSalesmen() {
    	return $this->hasMany(UserOrderSalesman::className(), ['user_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrders0() {
    	return $this->hasMany(Order::className(), ['id' => 'order_id'])->viaTable('user_order_salesman', ['user_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUserWarehouses() {
    	return $this->hasMany(UserWarehouse::className(), ['user_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getWarehouses() {
    	return $this->hasMany(Warehouse::className(), ['id' => 'warehouse_id'])->viaTable('user_warehouse', ['user_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getWarehouseDeliveries() {
    	return $this->hasMany(WarehouseDelivery::className(), ['user_confirming_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getWarehouseProductStatusHistories() {
    	return $this->hasMany(WarehouseProductStatusHistory::className(), ['user_confirming_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAlerts() {
    	return $this->hasMany(Alert::className(), ['user_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTasks() {
    	return $this->hasMany(Task::className(), ['user_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTasks0() {
    	return $this->hasMany(Task::className(), ['user_completing_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTasks1() {
    	return $this->hasMany(Task::className(), ['user_notified_id' => 'id']);
    }
    
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrderUserRepresentatives() {
    	return $this->hasMany(OrderUserRepresentative::className(), ['user_representative_id' => 'id']);
    }

    /**
     * @inheritdoc
     * @return \common\models\aq\UserQuery the active query used by this AR class.
     */
    public static function find() {
        return new \common\models\aq\UserQuery(get_called_class());
    }

    public function behaviors() {
    	return [
    			[
    					'class' => TimestampBehavior::className(),
    					'createdAtAttribute' => 'date_creation',
    					'updatedAtAttribute' => 'date_modification',
    					'value' => new Expression('NOW()'),
    			],
    	];
    }

    public function hasRole($roleSymbol) {
    	$roles = UserRole::find()->joinWith('role r')->select('symbol')->where(['user_id' => $this->id])->column();
    	return in_array($roleSymbol, $roles);
    }

    /**
     * @inheritdoc
     */
    public static function findIdentity($id) {
    	return static::findOne(['id' => $id, 'is_active' => 1]);
    }

    /**
     * @inheritdoc
     */
    public static function findIdentityByAccessToken($token, $type = null) {
    	throw new NotSupportedException('"findIdentityByAccessToken" is not implemented.');
    }

    /**
     * Finds user by email
     *
     * @param string $email
     * @return static|null
     */
    public static function findByEmail($email, $conditions = []) {
    	return static::findOne(ArrayHelper::merge(['email' => $email], $conditions));
    }

    /**
     * @inheritdoc
     */
    public function getId() {
    	return $this->id;
    }

    /**
     * @inheritdoc
     */
    public function getAuthKey() {
    	return null;
    }

    /**
     * @inheritdoc
     */
    public function validateAuthKey($authKey) {
    	return false;
    }

    /**
     * Validates password
     *
     * @param string $password password to validate
     * @return boolean if password provided is valid for current user
     */
    public function validatePassword($password) {
    	return Yii::$app->security->validatePassword($password, $this->password);
    }

    /**
     * Generates password hash from password and sets it to the model
     *
     * @param string $password
     */
    public function setPassword($password) {
    	$this->password = Yii::$app->security->generatePasswordHash($password);
    }

    public function getIdentityName() {
    	//return $this->email;
    	return $this->first_name ?: Yii::t('main', 'User');
    }

    public function getFullName() {
        return $this->first_name.(empty($this->first_name) ? '' : ' ').$this->last_name;
    }

	public static function discoverNameForm($obj) {
		if(!empty($obj)) {
			return (!empty($obj->first_name) && !empty($obj->last_name) ? "{$obj->first_name} {$obj->last_name}" :
					$obj->email ?: $obj->id);
		}
		else {
			return null;
		}
	}
	/**
	 * Finds out if password reset token is valid
	 *
	 * @param common\models\ar\Token | NULL $token
	 * @return boolean
	 */
	public static function isPasswordResetTokenValid(&$token, &$sent, $mode = null) {
		if(empty($token)) {
			return false;
		}

		if(empty($token->date_expiration)) {
			return true;
		}

		$expire = strtotime($token->date_expiration);
		if($expire >= time()) {
			return true;
		}

		$sent = $token->regenerate($mode);
		return null;
	}

	/**
	 * Generates new password reset token
	 */
	public function generateToken($symbol) {
		$tokenType = TokenType::findOne(['symbol' => $symbol]);
		if(!$tokenType) {
			return false;
		}

		$token = new Token();
		$token->token = Yii::$app->security->generateRandomString();
		$token->token_type_id = $tokenType->id;
		$token->user_id = $this->id;

		$token->date_expiration = date('Y-m-d H:i:s', time() + \Yii::$app->params['user.passwordResetTokenExpire']);

		while(!$token->validate(['token'])) {
			$token->token = Yii::$app->security->generateRandomString();
		}


		if(!$token->save()) {
			return false;
		}

		return $token;
	}
	public function getResetPasswordToken() {
		return Token::find()->joinWith('tokenType')
			->where('user_id = :userId AND token_type.symbol = :symbol', [':userId' => $this->id, ':symbol' => \common\helpers\Utility::TOKEN_TYPE_RESET_PASSWORD])->one();
	}
	/**
	 * Finds user by password reset token
	 *
	 * @param string $token password reset token
	 * @return static|null
	 */
	public static function findByPasswordResetToken($token) {
		$tokenType = TokenType::findOne(['symbol' => \common\helpers\Utility::TOKEN_TYPE_RESET_PASSWORD]);
		if(!$tokenType) {
			return null;
		}

		$sent = false;
		$t = Token::findOne(['token' => $token, 'token_type_id' => $tokenType->id]);
		if(!static::isPasswordResetTokenValid($t, $sent, MessageProvider::MODE_EMAIL)) {
			return null;
		}

		return Token::findOne(['token' => $token, 'token_type_id' => $tokenType->id])->user;
	}

	/**
	 * Removes password reset token
	 */
	public function removePasswordResetToken($token) {
		$tokenType = TokenType::findOne(['symbol' => \common\helpers\Utility::TOKEN_TYPE_RESET_PASSWORD]);
		if(!$tokenType) {
			return null;
		}

		Token::deleteAll(['token' => $token, 'token_type_id' => $tokenType->id]);
	}
}
