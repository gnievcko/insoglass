<?php

namespace common\models\ar;

use Yii;

/**
 * This is the model class for table "order_priority_translation".
 *
 * @property integer $order_priority_id
 * @property integer $language_id
 * @property string $name
 *
 * @property OrderPriority $orderPriority
 * @property Language $language
 */
class OrderPriorityTranslation extends \yii\db\ActiveRecord {

    /**
     * @inheritdoc
     */
    public static function tableName() {
        return 'order_priority_translation';
    }

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
				[['order_priority_id', 'language_id', 'name'], 'required'],
				[['order_priority_id', 'language_id'], 'integer'],
				[['name'], 'string', 'max' => 128],
				[['order_priority_id'], 'exist', 'skipOnError' => true, 'targetClass' => OrderPriority::className(), 'targetAttribute' => ['order_priority_id' => 'id']],
				[['language_id'], 'exist', 'skipOnError' => true, 'targetClass' => Language::className(), 'targetAttribute' => ['language_id' => 'id']]
		];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return [
            	'order_priority_id' => Yii::t('main', 'Order Priority ID'),
            	'language_id' => Yii::t('main', 'Language ID'),
            	'name' => Yii::t('main', 'Name'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrderPriority() {
        return $this->hasOne(OrderPriority::className(), ['id' => 'order_priority_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLanguage() {
        return $this->hasOne(Language::className(), ['id' => 'language_id']);
    }

    /**
     * @inheritdoc
     * @return \common\models\aq\OrderPriorityTranslationQuery the active query used by this AR class.
     */
    public static function find() {
        return new \common\models\aq\OrderPriorityTranslationQuery(get_called_class());
    }
}
