<?php

namespace common\models\ar;

use Yii;

/**
 * This is the model class for table "order_modification_history".
 *
 * @property integer $id
 * @property integer $order_id
 * @property string $modification
 *
 * @property Order $order
 */
class OrderModificationHistory extends \yii\db\ActiveRecord {

    /**
     * @inheritdoc
     */
    public static function tableName() {
        return 'order_modification_history';
    }

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
				[['order_id'], 'integer'],
				[['modification'], 'required'],
				[['modification'], 'string'],
				[['order_id'], 'exist', 'skipOnError' => true, 'targetClass' => Order::className(), 'targetAttribute' => ['order_id' => 'id']]
		];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return [
            	'id' => Yii::t('main', 'ID'),
            	'order_id' => Yii::t('main', 'Order'),
            	'modification' => Yii::t('main', 'Modification'),
        ];
    }
    
    public function __construct($orderId) {
    	$this->order_id = $orderId;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrder() {
        return $this->hasOne(Order::className(), ['id' => 'order_id']);
    }

    /**
     * @inheritdoc
     * @return \common\models\aq\OrderModificationHistoryQuery the active query used by this AR class.
     */
    public static function find() {
        return new \common\models\aq\OrderModificationHistoryQuery(get_called_class());
    }
}
