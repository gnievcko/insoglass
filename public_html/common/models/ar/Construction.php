<?php

namespace common\models\ar;

use Yii;

/**
 * This is the model class for table "construction".
 *
 * @property integer $id
 * @property string $symbol
 *
 * @property ConstructionTranslation[] $constructionTranslations
 */
class Construction extends \yii\db\ActiveRecord {

    /**
     * @inheritdoc
     */
    public static function tableName() {
        return 'construction';
    }

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
			[['symbol'], 'required'],
			[['symbol'], 'string', 'max' => 32],
			[['symbol'], 'unique']
		];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return [
            'id' => Yii::t('main', 'ID'),
            'symbol' => Yii::t('main', 'Symbol'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getConstructionTranslations() {
        return $this->hasMany(ConstructionTranslation::className(), ['construction_id' => 'id']);
    }

    public function getLanguages() {
        return $this->hasMany(Language::className(), ['id' => 'language_id'])->viaTable('construction_translation', ['construction_id' => 'id']);
    }

    public function getTranslation() {
        $translations = $this->constructionTranslations;
        if(empty($translations)) {
            return null;
        }
         
        foreach($translations as $t) {
            if($t->language->symbol === Yii::$app->language) {
                return $t;
            }
        }
         
        return $translations[0];
    }

    /**
     * @inheritdoc
     * @return \common\models\aq\ConstructionQuery the active query used by this AR class.
     */
    public static function find() {
        return new \common\models\aq\ConstructionQuery(get_called_class());
    }
}
