<?php

namespace common\models\ar;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\Expression;

/**
 * This is the model class for table "warehouse_product_status_history".
 *
 * @property integer $id
 * @property integer $operation_id 
 * @property integer $warehouse_product_id
 * @property integer $product_status_id
 * @property integer $user_confirming_id
 * @property integer $count
 * @property string $description
 * @property integer $history_blocked_id 
 * @property integer $warehouse_delivery_id 
 * @property string $date_creation
 * @property string $unit
 *
 * @property WarehouseProductOperation $operation
 * @property User $userConfirming
 * @property WarehouseProduct $warehouseProduct
 * @property ProductStatus $productStatus
 * @property WarehouseProductStatusHistory $historyBlocked
 * @property WarehouseProductStatusHistoryData[] $warehouseProductStatusHistoryDatas
 * @property ProductStatusReference[] $productStatusReferences
 */
class WarehouseProductStatusHistory extends \yii\db\ActiveRecord {

    /**
     * @inheritdoc
     */
    public static function tableName() {
        return 'warehouse_product_status_history';
    }

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
				[['operation_id', 'warehouse_product_id', 'product_status_id', 'user_confirming_id', 'count'], 'required'],
				[['operation_id', 'warehouse_product_id', 'product_status_id', 'user_confirming_id', 'count', 'currency_id', 'history_blocked_id', 'warehouse_delivery_id'], 'integer'],
				[['date_creation'], 'safe'],
        		[['price_unit', 'price_group'], 'number'],
				[['description'], 'string', 'max' => 2048],
        		[['unit'], 'string', 'max' => 10],
        		[['history_blocked_id'], 'default', 'value' => null],
        		[['warehouse_delivery_id'], 'exist', 'skipOnError' => true, 'targetClass' => WarehouseDelivery::className(), 'targetAttribute' => ['warehouse_delivery_id' => 'id']],
				[['user_confirming_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['user_confirming_id' => 'id']],
				[['warehouse_product_id'], 'exist', 'skipOnError' => true, 'targetClass' => WarehouseProduct::className(), 'targetAttribute' => ['warehouse_product_id' => 'id']],
        		[['history_blocked_id'], 'exist', 'skipOnError' => true, 'targetClass' => WarehouseProductStatusHistory::className(), 'targetAttribute' => ['history_blocked_id' => 'id']],
				[['product_status_id'], 'exist', 'skipOnError' => true, 'targetClass' => ProductStatus::className(), 'targetAttribute' => ['product_status_id' => 'id']],
        		[['operation_id'], 'exist', 'skipOnError' => true, 'targetClass' => WarehouseProductOperation::className(), 'targetAttribute' => ['operation_id' => 'id']],
		];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return [
            	'id' => Yii::t('main', 'ID'),
        		'operation_id' => Yii::t('main', 'Operation ID'),
            	'warehouse_product_id' => Yii::t('main', 'Warehouse Product ID'),
            	'product_status_id' => Yii::t('main', 'Product Status ID'),
            	'user_confirming_id' => Yii::t('main', 'User Confirming ID'),
            	'count' => Yii::t('main', 'Count'),
            	'description' => Yii::t('main', 'Description'),
        		'history_blocked_id' => Yii::t('main', 'History Blocked ID'),
        		'price_unit' => Yii::t('main', 'Price Unit'),
        		'currency_id' => Yii::t('main', 'Currency'),
        		'price_group' => Yii::t('main', 'Price Group'),
            	'date_creation' => Yii::t('main', 'Date Creation'),
        		'warehouse_delivery_id' => Yii::t('main', 'Warehouse Delivery ID'),
        		'unit' => Yii::t('main', 'Unit'),
        ];
    }
    
    public function getOperation() {
    	return $this->hasOne(WarehouseProductOperation::className(), ['id' => 'operation_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUserConfirming() {
        return $this->hasOne(User::className(), ['id' => 'user_confirming_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getWarehouseProduct() {
        return $this->hasOne(WarehouseProduct::className(), ['id' => 'warehouse_product_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProductStatus() {
        return $this->hasOne(ProductStatus::className(), ['id' => 'product_status_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getWarehouseProductStatusHistoryDatas() {
        return $this->hasMany(WarehouseProductStatusHistoryData::className(), ['history_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
	public function getProductStatusReferences() {
		return $this->hasMany(ProductStatusReference::className(), ['id' => 'product_status_reference_id'])->viaTable('warehouse_product_status_history_data', ['history_id' => 'id']);
	}
	
	public function getHistoryBlocked() {
		return $this->hasOne(WarehouseProductStatusHistory::className(), ['id' => 'history_blocked_id']);
	}
	
	public function getWarehouseDelivery() {
		return $this->hasOne(WarehouseDelivery::className(), ['id' => 'warehouse_delivery_id']);
	}
	
	public function getWarehouseProductStatusHistories() {
		return $this->hasMany(WarehouseProductStatusHistory::className(), ['history_blocked_id' => 'id']);
	}

    /**
     * @inheritdoc
     * @return \common\models\aq\WarehouseProductStatusHistoryQuery the active query used by this AR class.
     */
    public static function find() {
        return new \common\models\aq\WarehouseProductStatusHistoryQuery(get_called_class());
    }
    
    public function behaviors() {
    	return [
    			[
    					'class' => TimestampBehavior::className(),
    					'createdAtAttribute' => 'date_creation',
    					'updatedAtAttribute' => false,
    					'value' => new Expression('NOW()'),
    			],
    	];
    }
}
