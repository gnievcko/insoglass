<?php

namespace common\models\ar;

use Yii;

/**
 * This is the model class for table "order_offered_product_attribute".
 *
 * @property integer $order_offered_product_id
 * @property integer $product_attribute_type_id
 * @property string $value
 *
 * @property ProductAttributeType $productAttributeType
 * @property OrderOfferedProduct $orderOfferedProduct
 */
class OrderOfferedProductAttribute extends \yii\db\ActiveRecord {

    /**
     * @inheritdoc
     */
    public static function tableName() {
        return 'order_offered_product_attribute';
    }

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
				[['order_offered_product_id', 'product_attribute_type_id', 'value'], 'required'],
				[['order_offered_product_id', 'product_attribute_type_id'], 'integer'],
				[['value'], 'string', 'max' => 64],
				[['product_attribute_type_id'], 'exist', 'skipOnError' => true, 'targetClass' => ProductAttributeType::className(), 'targetAttribute' => ['product_attribute_type_id' => 'id']],
				[['order_offered_product_id'], 'exist', 'skipOnError' => true, 'targetClass' => OrderOfferedProduct::className(), 'targetAttribute' => ['order_offered_product_id' => 'id']]
		];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return [
            	'order_offered_product_id' => Yii::t('main', 'Order Offered Product ID'),
            	'product_attribute_type_id' => Yii::t('main', 'Product Attribute Type ID'),
            	'value' => Yii::t('main', 'Value'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProductAttributeType() {
        return $this->hasOne(ProductAttributeType::className(), ['id' => 'product_attribute_type_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrderOfferedProduct() {
        return $this->hasOne(OrderOfferedProduct::className(), ['id' => 'order_offered_product_id']);
    }

    /**
     * @inheritdoc
     * @return \common\models\aq\OrderOfferedProductAttributeQuery the active query used by this AR class.
     */
    public static function find() {
        return new \common\models\aq\OrderOfferedProductAttributeQuery(get_called_class());
    }
}
