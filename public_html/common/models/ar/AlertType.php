<?php

namespace common\models\ar;

use Yii;

/**
 * This is the model class for table "alert_type".
 *
 * @property integer $id
 * @property string $symbol
 * @property integer $alert_priority_id
 *
 * @property Alert[] $alerts
 * @property AlertPriority $alertPriority
 * @property AlertTypeTranslation[] $alertTypeTranslations
 * @property Language[] $languages
 */
class AlertType extends \yii\db\ActiveRecord {

    /**
     * @inheritdoc
     */
    public static function tableName() {
        return 'alert_type';
    }

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
				[['symbol', 'alert_priority_id'], 'required'],
				[['alert_priority_id'], 'integer'],
				[['symbol'], 'string', 'max' => 32],
				[['symbol'], 'unique'],
				[['alert_priority_id'], 'exist', 'skipOnError' => true, 'targetClass' => AlertPriority::className(), 'targetAttribute' => ['alert_priority_id' => 'id']]
		];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return [
            	'id' => Yii::t('main', 'ID'),
            	'symbol' => Yii::t('main', 'Symbol'),
            	'alert_priority_id' => Yii::t('main', 'Alert Priority ID'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAlerts() {
        return $this->hasMany(Alert::className(), ['alert_type_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAlertPriority() {
        return $this->hasOne(AlertPriority::className(), ['id' => 'alert_priority_id']);
    }
    
    public function getTranslation() {
    	$translations = $this->alertTypeTranslations;
    	if(empty($translations)) {
    		return null;
    	}
    
    	foreach($translations as $t) {
    		if($t->language->symbol === Yii::$app->language) {
    			return $t;
    		}
    	}
    
    	return $translations[0];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAlertTypeTranslations() {
        return $this->hasMany(AlertTypeTranslation::className(), ['alert_type_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLanguages() {
        return $this->hasMany(Language::className(), ['id' => 'language_id'])->viaTable('alert_type_translation', ['alert_type_id' => 'id']);
    }

    /**
     * @inheritdoc
     * @return \common\models\aq\AlertTypeQuery the active query used by this AR class.
     */
    public static function find() {
        return new \common\models\aq\AlertTypeQuery(get_called_class());
    }
}
