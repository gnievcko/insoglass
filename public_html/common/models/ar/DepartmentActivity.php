<?php

namespace common\models\ar;

use Yii;

/**
 * This is the model class for table "department_activity".
 *
 * @property integer $department_id
 * @property integer $activity_id
 *
 * @property Activity $activity
 * @property Department $department
 */
class DepartmentActivity extends \yii\db\ActiveRecord {

    /**
     * @inheritdoc
     */
    public static function tableName() {
        return 'department_activity';
    }

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
				[['department_id', 'activity_id'], 'required'],
				[['department_id', 'activity_id'], 'integer'],
				[['activity_id'], 'exist', 'skipOnError' => true, 'targetClass' => Activity::className(), 'targetAttribute' => ['activity_id' => 'id']],
				[['department_id'], 'exist', 'skipOnError' => true, 'targetClass' => Department::className(), 'targetAttribute' => ['department_id' => 'id']]
		];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return [
            	'department_id' => Yii::t('main', 'Department ID'),
            	'activity_id' => Yii::t('main', 'Activity ID'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getActivity() {
        return $this->hasOne(Activity::className(), ['id' => 'activity_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDepartment() {
        return $this->hasOne(Department::className(), ['id' => 'department_id']);
    }

    /**
     * @inheritdoc
     * @return \common\models\aq\DepartmentActivityQuery the active query used by this AR class.
     */
    public static function find() {
        return new \common\models\aq\DepartmentActivityQuery(get_called_class());
    }
}
