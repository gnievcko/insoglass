<?php

namespace common\models\ar;

use Yii;

/**
 * This is the model class for table "language".
 *
 * @property integer $id
 * @property string $symbol
 * @property string $name
 * @property integer $is_active
 *
 * @property AlertTypeTranslation[] $alertTypeTranslations
 * @property AlertType[] $alertTypes
 * @property BannerTranslation[] $bannerTranslations
 * @property Banner[] $banners
 * @property Company[] $companies
 * @property CompanyGroupTranslation[] $companyGroupTranslations
 * @property CompanyGroup[] $companyGroups
 * @property CountryTranslation[] $countryTranslations
 * @property Country[] $countries
 * @property EmailTemplate[] $emailTemplates
 * @property EmailType[] $emailTypes
 * @property InformationPageTranslation[] $informationPageTranslations
 * @property InformationPage[] $informationPages
 * @property OfferedProductCategoryTranslation[] $offeredProductCategoryTranslations
 * @property OfferedProductCategory[] $offeredProductCategories
 * @property OfferedProductPhotoTranslation[] $offeredProductPhotoTranslations
 * @property OfferedProductPhoto[] $offeredProductPhotos
 * @property OfferedProductTranslation[] $offeredProductTranslations
 * @property OfferedProduct[] $offeredProducts
 * @property OrderStatusTranslation[] $orderStatusTranslations
 * @property OrderStatus[] $orderStatuses
 * @property ProductAlias[] $productAliases
 * @property ProductCategoryTranslation[] $productCategoryTranslations
 * @property ProductCategory[] $productCategories
 * @property ProductPhotoTranslation[] $productPhotoTranslations
 * @property ProductPhoto[] $productPhotos
 * @property ProductStatusTranslation[] $productStatusTranslations
 * @property ProductStatus[] $productStatuses
 * @property ProductTranslation[] $productTranslations
 * @property Product[] $products
 * @property RoleTranslation[] $roleTranslations
 * @property Role[] $roles
 * @property TaskTypeTranslation[] $taskTypeTranslations
 * @property TaskType[] $taskTypes
 * @property TimeUnitTranslation[] $timeUnitTranslations
 * @property User[] $users
 */
class Language extends \yii\db\ActiveRecord {

    /**
     * @inheritdoc
     */
    public static function tableName() {
        return 'language';
    }

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
				[['symbol', 'name', 'is_active'], 'required'],
				[['is_active'], 'integer'],
				[['symbol'], 'string', 'max' => 5],
				[['name'], 'string', 'max' => 32],
				[['symbol'], 'unique']
		];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return [
            	'id' => Yii::t('main', 'ID'),
            	'symbol' => Yii::t('main', 'Symbol'),
            	'name' => Yii::t('main', 'Name'),
            	'is_active' => Yii::t('main', 'Is active'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAlertTypeTranslations() {
        return $this->hasMany(AlertTypeTranslation::className(), ['language_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAlertTypes() {
        return $this->hasMany(AlertType::className(), ['id' => 'alert_type_id'])->viaTable('alert_type_translation', ['language_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBannerTranslations() {
        return $this->hasMany(BannerTranslation::className(), ['language_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBanners() {
        return $this->hasMany(Banner::className(), ['id' => 'banner_id'])->viaTable('banner_translation', ['language_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCompanies() {
        return $this->hasMany(Company::className(), ['language_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCompanyGroupTranslations() {
        return $this->hasMany(CompanyGroupTranslation::className(), ['language_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCompanyGroups() {
        return $this->hasMany(CompanyGroup::className(), ['id' => 'company_group_id'])->viaTable('company_group_translation', ['language_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCountryTranslations() {
        return $this->hasMany(CountryTranslation::className(), ['language_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCountries() {
        return $this->hasMany(Country::className(), ['id' => 'country_id'])->viaTable('country_translation', ['language_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getEmailTemplates() {
        return $this->hasMany(EmailTemplate::className(), ['language_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getEmailTypes() {
        return $this->hasMany(EmailType::className(), ['id' => 'email_type_id'])->viaTable('email_template', ['language_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getInformationPageTranslations() {
        return $this->hasMany(InformationPageTranslation::className(), ['language_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getInformationPages() {
        return $this->hasMany(InformationPage::className(), ['id' => 'information_page_id'])->viaTable('information_page_translation', ['language_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOfferedProductCategoryTranslations() {
        return $this->hasMany(OfferedProductCategoryTranslation::className(), ['language_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOfferedProductCategories() {
        return $this->hasMany(OfferedProductCategory::className(), ['id' => 'offered_product_category_id'])->viaTable('offered_product_category_translation', ['language_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOfferedProductPhotoTranslations() {
        return $this->hasMany(OfferedProductPhotoTranslation::className(), ['language_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOfferedProductPhotos() {
        return $this->hasMany(OfferedProductPhoto::className(), ['id' => 'offered_product_photo_id'])->viaTable('offered_product_photo_translation', ['language_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOfferedProductTranslations() {
        return $this->hasMany(OfferedProductTranslation::className(), ['language_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOfferedProducts() {
        return $this->hasMany(OfferedProduct::className(), ['id' => 'offered_product_id'])->viaTable('offered_product_translation', ['language_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrderStatusTranslations() {
        return $this->hasMany(OrderStatusTranslation::className(), ['language_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrderStatuses() {
        return $this->hasMany(OrderStatus::className(), ['id' => 'order_status_id'])->viaTable('order_status_translation', ['language_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProductAliases() {
        return $this->hasMany(ProductAlias::className(), ['language_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProductCategoryTranslations() {
        return $this->hasMany(ProductCategoryTranslation::className(), ['language_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProductCategories() {
        return $this->hasMany(ProductCategory::className(), ['id' => 'product_category_id'])->viaTable('product_category_translation', ['language_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProductPhotoTranslations() {
        return $this->hasMany(ProductPhotoTranslation::className(), ['language_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProductPhotos() {
        return $this->hasMany(ProductPhoto::className(), ['id' => 'product_photo_id'])->viaTable('product_photo_translation', ['language_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProductStatusTranslations() {
        return $this->hasMany(ProductStatusTranslation::className(), ['language_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProductStatuses() {
        return $this->hasMany(ProductStatus::className(), ['id' => 'product_status_id'])->viaTable('product_status_translation', ['language_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProductTranslations() {
        return $this->hasMany(ProductTranslation::className(), ['language_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProducts() {
        return $this->hasMany(Product::className(), ['id' => 'product_id'])->viaTable('product_translation', ['language_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRoleTranslations() {
        return $this->hasMany(RoleTranslation::className(), ['language_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRoles() {
        return $this->hasMany(Role::className(), ['id' => 'role_id'])->viaTable('role_translation', ['language_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTaskTypeTranslations() {
        return $this->hasMany(TaskTypeTranslation::className(), ['language_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTaskTypes() {
        return $this->hasMany(TaskType::className(), ['id' => 'task_type_id'])->viaTable('task_type_translation', ['language_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTimeUnitTranslations() {
        return $this->hasMany(TimeUnitTranslation::className(), ['language_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUsers() {
        return $this->hasMany(User::className(), ['language_id' => 'id']);
    }

    /**
     * @inheritdoc
     * @return \common\models\aq\LanguageQuery the active query used by this AR class.
     */
    public static function find() {
        return new \common\models\aq\LanguageQuery(get_called_class());
    }
}
