<?php

namespace common\models\ar;

use Yii;

/**
 * This is the model class for table "product_attribute_type_translation".
 *
 * @property integer $product_attribute_type_id
 * @property integer $language_id
 * @property string $name
 * @property string $hint
 *
 * @property ProductAttributeType $productAttributeType
 * @property Language $language
 */
class ProductAttributeTypeTranslation extends \yii\db\ActiveRecord {

    /**
     * @inheritdoc
     */
    public static function tableName() {
        return 'product_attribute_type_translation';
    }

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
                [['hint'], 'safe'],
				[['product_attribute_type_id', 'language_id', 'name'], 'required'],
				[['product_attribute_type_id', 'language_id'], 'integer'],
				[['name'], 'string', 'max' => 128],
				[['product_attribute_type_id'], 'exist', 'skipOnError' => true, 'targetClass' => ProductAttributeType::className(), 'targetAttribute' => ['product_attribute_type_id' => 'id']],
				[['language_id'], 'exist', 'skipOnError' => true, 'targetClass' => Language::className(), 'targetAttribute' => ['language_id' => 'id']],
                [['product_attribute_type_id'], 'unique', 'targetAttribute' => ['product_attribute_type_id', 'language_id'], 'comboNotUnique' => Yii::t('main','The chosen country-language pair has a translation already.')],
		];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return [
            	'product_attribute_type_id' => Yii::t('main', 'Product Attribute Type ID'),
            	'language_id' => Yii::t('main', 'Language ID'),
            	'name' => Yii::t('main', 'Name'),
                'symbol' => Yii::t('main', 'Symbol'),
                'languageName' => Yii::t('main', 'Language'),
                'hint' => Yii::t('main', 'Hint'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProductAttributeType() {
        return $this->hasOne(ProductAttributeType::className(), ['id' => 'product_attribute_type_id']);
    }

    public function getSymbol() {
        return $this->productAttributeType->symbol;
    }
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLanguage() {
        return $this->hasOne(Language::className(), ['id' => 'language_id']);
    }

    public function getLanguageName() {
        return $this->language->name;
    }
    /**
     * @inheritdoc
     * @return \common\models\aq\ProductAttributeTypeTranslationQuery the active query used by this AR class.
     */
    public static function find() {
        return new \common\models\aq\ProductAttributeTypeTranslationQuery(get_called_class());
    }
}
