<?php

namespace common\models\ar;

use Yii;
use yii\db\Query;

/**
 * This is the model class for table "task_type_reference".
 *
 * @property integer $id
 * @property integer $task_type_id
 * @property string $symbol
 * @property string $name_table
 * @property string $name_column
 * @property string $name_expression
 * @property string $url_details
 * @property integer $is_required
 *
 * @property TaskData[] $taskDatas
 * @property Task[] $tasks
 * @property TaskType $taskType
 * @property TaskTypeReferenceTranslation[] $taskTypeReferenceTranslations
 * @property Language[] $languages
 */
class TaskTypeReference extends \yii\db\ActiveRecord {

    /**
     * @inheritdoc
     */
    public static function tableName() {
        return 'task_type_reference';
    }

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
				[['task_type_id', 'symbol', 'name_table', 'name_column', 'name_expression'], 'required'],
				[['task_type_id', 'is_required'], 'integer'],
				[['symbol', 'name_table', 'name_column'], 'string', 'max' => 64],
        		[['name_expression'], 'string', 'max' => 512],
        		[['url_details'], 'string', 'max' => 256],
				[['task_type_id'], 'exist', 'skipOnError' => true, 'targetClass' => TaskType::className(), 'targetAttribute' => ['task_type_id' => 'id']]
		];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return [
            	'id' => Yii::t('main', 'ID'),
            	'taskTypeName' => Yii::t('main', 'Task type'),
            	'symbol' => Yii::t('main', 'Symbol'),
            	'name_table' => Yii::t('main', 'Table name'),
            	'name_column' => Yii::t('main', 'Column name'),
        		'name_expression' => Yii::t('main', 'Expression name'),
        		'url_details' => Yii::t('main', 'Url details'),
            	'is_required' => Yii::t('main', 'Is required'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTaskDatas() {
        return $this->hasMany(TaskData::className(), ['task_type_reference_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTasks() {
        return $this->hasMany(Task::className(), ['id' => 'task_id'])->viaTable('task_data', ['task_type_reference_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTaskType() {
        return $this->hasOne(TaskType::className(), ['id' => 'task_type_id']);
    }
    
    public function getTaskTypeName() {
    	return (new Query())
    			->select(['ttt.name'])
    			->from('task_type_translation ttt')
    			->join('INNER JOIN', 'language l', 'l.id = ttt.language_id')
    			->where(['ttt.task_type_id' => $this->task_type_id])
    			->andWhere(['l.symbol' => Yii::$app->language])->one()['name'];
    			
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTaskTypeReferenceTranslations() {
        return $this->hasMany(TaskTypeReferenceTranslation::className(), ['reference_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLanguages() {
        return $this->hasMany(Language::className(), ['id' => 'language_id'])->viaTable('task_type_reference_translation', ['reference_id' => 'id']);
    }

    /**
     * @inheritdoc
     * @return \common\models\aq\TaskTypeReferenceQuery the active query used by this AR class.
     */
    public static function find() {
        return new \common\models\aq\TaskTypeReferenceQuery(get_called_class());
    }
}
