<?php

namespace common\models\ar;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\Expression;

/**
 * This is the model class for table "warehouse_product".
 *
 * @property integer $id
 * @property integer $warehouse_id
 * @property integer $product_id
 * @property integer $count
 * @property integer $count_available
 * @property string $date_modification
 * @property integer $count_ordered
 * @property string $stillage 
 * @property string $shelf
 * @property string $price_unit
 * @property string $price_group
 * @property integer $currency_id 
 *
 * @property Currency $currency 
 * @property Product $product
 * @property Warehouse $warehouse
 * @property WarehouseProductStatusHistory[] $warehouseProductStatusHistories
 */
class WarehouseProduct extends \yii\db\ActiveRecord {

    /**
     * @inheritdoc
     */
    public static function tableName() {
        return 'warehouse_product';
    }

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
				[['warehouse_id', 'product_id', 'count', 'count_available'], 'required'],
				[['warehouse_id', 'product_id', 'count', 'count_available', 'count_ordered', 'currency_id'], 'integer'],
				[['date_modification'], 'safe'],
        		[['price_unit', 'price_group'], 'number'],
        		[['stillage', 'shelf'], 'string', 'max' => 20],
				[['warehouse_id', 'product_id'], 'unique', 'targetAttribute' => ['warehouse_id', 'product_id'], 'message' => 'The combination of Warehouse ID and Product ID has already been taken.'],
				[['product_id'], 'exist', 'skipOnError' => true, 'targetClass' => Product::className(), 'targetAttribute' => ['product_id' => 'id']],
				[['warehouse_id'], 'exist', 'skipOnError' => true, 'targetClass' => Warehouse::className(), 'targetAttribute' => ['warehouse_id' => 'id']],
        		[['currency_id'], 'exist', 'skipOnError' => true, 'targetClass' => Currency::className(), 'targetAttribute' => ['currency_id' => 'id']],
		];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return [
            	'id' => Yii::t('main', 'ID'),
            	'warehouse_id' => Yii::t('main', 'Warehouse'),
            	'product_id' => Yii::t('main', 'Product'),
            	'count' => Yii::t('main', 'Count'),
        		'count_available' => Yii::t('main', 'Count Available'),
        		'count_ordered' => Yii::t('web', 'Count ordered'),
            	'date_modification' => Yii::t('main', 'Date Modification'),
        		'count_ordered' => Yii::t('main', 'Count Ordered'),
        		'stillage' => Yii::t('main', 'Stillage'),
        		'shelf' => Yii::t('main', 'Shelf'),
        		'price_unit' => Yii::t('main', 'Price Unit'),
        		'price_group' => Yii::t('main', 'Price Group'),
        		'currency_id' => Yii::t('main', 'Currency ID'),
        ];
    }
    
    public function getCurrency() {
    	return $this->hasOne(Currency::className(), ['id' => 'currency_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProduct() {
        return $this->hasOne(Product::className(), ['id' => 'product_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getWarehouse() {
        return $this->hasOne(Warehouse::className(), ['id' => 'warehouse_id']);
    }
    
    public function getWarehouseSupplier() {
    	return $this->hasOne(WarehouseSupplier::className(), ['id' => 'warehouse_supplier_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getWarehouseProductStatusHistories() {
        return $this->hasMany(WarehouseProductStatusHistory::className(), ['warehouse_product_id' => 'id']);
    }

    /**
     * @inheritdoc
     * @return \common\models\aq\WarehouseProductQuery the active query used by this AR class.
     */
    public static function find() {
        return new \common\models\aq\WarehouseProductQuery(get_called_class());
    }
    
    public function behaviors() {
    	return [
    			[
    					'class' => TimestampBehavior::className(),
    					'createdAtAttribute' => 'date_modification',
    					'updatedAtAttribute' => 'date_modification',
    					'value' => new Expression('NOW()'),
    			],
    	];
    }
}
