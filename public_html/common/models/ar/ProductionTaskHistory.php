<?php

namespace common\models\ar;

use Yii;
use yii\db\Expression;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "production_task_history".
 *
 * @property integer $id
 * @property integer $production_task_id
 * @property integer $user_id
 * @property integer $workstation_id
 * @property integer $count_made
 * @property integer $count_failed
 * @property string $message
 * @property string $date_start
 * @property string $date_stop
 * @property string $date_creation
 *
 * @property User $user
 * @property ProductionTask $productionTask
 */
class ProductionTaskHistory extends \yii\db\ActiveRecord {

    /**
     * @inheritdoc
     */
    public static function tableName() {
        return 'production_task_history';
    }

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
				[['production_task_id', 'user_id', 'workstation_id'], 'required'],
				[['production_task_id', 'user_id', 'workstation_id', 'count_made', 'count_failed'], 'integer'],
				[['date_start', 'date_stop', 'date_creation'], 'safe'],
				[['message'], 'string', 'max' => 1024],
				[['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['user_id' => 'id']],
				[['production_task_id'], 'exist', 'skipOnError' => true, 'targetClass' => ProductionTask::className(), 'targetAttribute' => ['production_task_id' => 'id']],
                [['workstation_id'], 'exist', 'skipOnError' => true, 'targetClass' => Workstation::className(), 'targetAttribute' => ['workstation_id' => 'id']],
		];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return [
            	'id' => Yii::t('main', 'ID'),
            	'production_task_id' => Yii::t('main', 'Production Task ID'),
            	'user_id' => Yii::t('main', 'User'),
            	'workstation_id' => Yii::t('main', 'Workstation ID'),
            	'count_made' => Yii::t('main', 'Count Made'),
            	'count_failed' => Yii::t('main', 'Count Failed'),
            	'message' => Yii::t('main', 'Message'),
            	'date_start' => Yii::t('main', 'Date Start'),
            	'date_stop' => Yii::t('main', 'Date Stop'),
            	'date_creation' => Yii::t('main', 'Date Creation'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser() {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProductionTask() {
        return $this->hasOne(ProductionTask::className(), ['id' => 'production_task_id']);
    }
    
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getWorkstation() {
        return $this->hasOne(Workstation::className(), ['id' => 'id']);
    }

    /**
     * @inheritdoc
     * @return \common\models\aq\ProductionTaskHistoryQuery the active query used by this AR class.
     */
    public static function find() {
        return new \common\models\aq\ProductionTaskHistoryQuery(get_called_class());
    }
    
    public function behaviors() {
    	return [
    			[
    					'class' => TimestampBehavior::className(),
    					'createdAtAttribute' => 'date_creation',
    					'updatedAtAttribute' => false,
    					'value' => new Expression('NOW()'),
    			],
    	];
    }
}
