<?php

namespace common\models\ar;

use Yii;

/**
 * This is the model class for table "user_role".
 *
 * @property integer $user_id
 * @property integer $role_id
 *
 * @property Role $role
 * @property User $user
 */
class UserRole extends \yii\db\ActiveRecord {

    /**
     * @inheritdoc
     */
    public static function tableName() {
        return 'user_role';
    }

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
				[['user_id', 'role_id'], 'required'],
				[['user_id', 'role_id'], 'integer'],
				[['role_id'], 'exist', 'skipOnError' => true, 'targetClass' => Role::className(), 'targetAttribute' => ['role_id' => 'id']],
				[['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['user_id' => 'id']],
        		[['role_id'], 'unique', 'targetAttribute' => ['role_id', 'user_id'], 'comboNotUnique' => Yii::t('main', 'The chosen role is already assigned to this user')],
		];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return [
            	'userName' => Yii::t('main', 'User'),
            	'roleName' => Yii::t('main', 'Role'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRole() {
        return $this->hasOne(Role::className(), ['id' => 'role_id']);
    }

    public function getRoleName() {    	
    	$roleTranslations = $this->role->roleTranslations;
    	foreach($roleTranslations as $rt) {
    		if($rt->language->symbol === Yii::$app->language) {
    			return $rt->name;
    		}
    	}
    	 
    	return (count($roleTranslations) > 0 ? $roleTranslations[0]->name : null);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser() {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }

    public function getUserName() {
    	return User::discoverNameForm($this->user);
    }

    /**
     * @inheritdoc
     * @return \common\models\aq\UserRoleQuery the active query used by this AR class.
     */
    public static function find() {
        return new \common\models\aq\UserRoleQuery(get_called_class());
    }
}
