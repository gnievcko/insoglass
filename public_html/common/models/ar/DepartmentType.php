<?php

namespace common\models\ar;

use Yii;

/**
 * This is the model class for table "department_type".
 *
 * @property integer $id
 * @property string $symbol
 *
 * @property Department[] $departments
 * @property DepartmentTypeTranslation[] $departmentTypeTranslations
 * @property Language[] $languages
 */
class DepartmentType extends \yii\db\ActiveRecord {

    /**
     * @inheritdoc
     */
    public $name;

    public static function tableName() {
        return 'department_type';
    }

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
				[['symbol'], 'required'],
				[['symbol'], 'string', 'max' => 32],
				[['symbol'], 'unique'],
		];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return [
            	'id' => Yii::t('main', 'ID'),
            	'symbol' => Yii::t('main', 'Symbol'),
                'name' => Yii::t('main','Name'),
                'translatedName' => Yii::t('main', 'Translation')
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDepartments() {
        return $this->hasMany(Department::className(), ['department_type_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDepartmentTypeTranslations() {
        return $this->hasMany(DepartmentTypeTranslation::className(), ['department_type_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLanguages() {
        return $this->hasMany(Language::className(), ['id' => 'language_id'])->viaTable('department_type_translation', ['department_type_id' => 'id']);
    }

    /**
     * @inheritdoc
     * @return \common\models\aq\DepartmentTypeQuery the active query used by this AR class.
     */
    public static function find() {
        return new \common\models\aq\DepartmentTypeQuery(get_called_class());
    }
}
