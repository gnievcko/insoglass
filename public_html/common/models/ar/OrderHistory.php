<?php

namespace common\models\ar;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\Expression;

/**
 * This is the model class for table "order_history".
 *
 * @property integer $id
 * @property integer $order_id
 * @property integer $user_id
 * @property integer $order_status_id
 * @property string $message
 * @property string $date_deadline
 * @property string $date_reminder
 * @property integer $is_client_entry
 * @property integer $is_answer_required
 * @property integer $order_custom_data_id 
 * @property string $date_creation
 * @property string $date_shipping
 *
 * @property Order[] $orders
 * @property Order[] $orders0
 * @property OrderAttachment[] $orderAttachments
 * @property OrderStatus $orderStatus
 * @property OrderCustomData $orderCustomData 
 * @property Order $order
 * @property User $user
 * @property OrderHistoryPhoto[] $orderHistoryPhotos
 * @property OrderOfferedProduct[] $orderOfferedProducts
 */
class OrderHistory extends \yii\db\ActiveRecord {

    /**
     * @inheritdoc
     */
    public static function tableName() {
        return 'order_history';
    }

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
				[['order_id', 'user_id', 'order_status_id', 'is_client_entry'], 'required'],
				[['order_id', 'user_id', 'order_status_id', 'is_client_entry', 'is_answer_required', 'order_custom_data_id'], 'integer'],
				[['message', 'modification'], 'string'],
				[['date_deadline', 'date_creation', 'date_reminder', 'date_shipping'], 'safe'],
				[['order_status_id'], 'exist', 'skipOnError' => true, 'targetClass' => OrderStatus::className(), 'targetAttribute' => ['order_status_id' => 'id']],
				[['order_id'], 'exist', 'skipOnError' => true, 'targetClass' => Order::className(), 'targetAttribute' => ['order_id' => 'id']],
				[['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['user_id' => 'id']],
        		[['order_custom_data_id'], 'exist', 'skipOnError' => true, 'targetClass' => OrderCustomData::className(), 'targetAttribute' => ['order_custom_data_id' => 'id']],
		];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return [
            	'id' => Yii::t('main', 'ID'),
            	'order_id' => Yii::t('main', 'Order'),
            	'user_id' => Yii::t('main', 'User'),
            	'order_status_id' => Yii::t('main', 'Order Status ID'),
            	'message' => Yii::t('main', 'Message'),
            	'date_deadline' => Yii::t('main', 'Date Deadline'),
                'date_deadline' => Yii::t('main', 'Reminder date'),
            	'is_client_entry' => Yii::t('main', 'Is Client Entry'),
            	'is_answer_required' => Yii::t('main', 'Is Answer Required'),
            	'date_creation' => Yii::t('main', 'Date Creation'),
                'date_shipping' => Yii::t('web', 'Shipping date'),
        		'order_custom_data_id' => Yii::t('main', 'Order Custom Data ID'),
        		'modification' => Yii::t('main', 'Modification'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrders() {
        return $this->hasMany(Order::className(), ['order_history_last_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrders0() {
        return $this->hasMany(Order::className(), ['order_history_first_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrderAttachments() {
        return $this->hasMany(OrderAttachment::className(), ['order_history_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrderStatus() {
        return $this->hasOne(OrderStatus::className(), ['id' => 'order_status_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrder() {
        return $this->hasOne(Order::className(), ['id' => 'order_id']);
    }
    
    public function getOrderCustomData() {
    	return $this->hasOne(OrderCustomData::className(), ['id' => 'order_custom_data_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser() {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrderHistoryPhotos() {
        return $this->hasMany(OrderHistoryPhoto::className(), ['order_history_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrderOfferedProducts() {
        return $this->hasMany(OrderOfferedProduct::className(), ['order_history_id' => 'id']);
    }

    /**
     * @inheritdoc
     * @return \common\models\aq\OrderHistoryQuery the active query used by this AR class.
     */
    public static function find() {
        return new \common\models\aq\OrderHistoryQuery(get_called_class());
    }
    
    public function behaviors() {
    	return [
    			[
    					'class' => TimestampBehavior::className(),
    					'createdAtAttribute' => 'date_creation',
    					'updatedAtAttribute' => false,
    					'value' => new Expression('NOW()'),
    			],
            ];
    }
}
