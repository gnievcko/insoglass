<?php

namespace common\models\ar;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\Expression;

/**
 * This is the model class for table "warehouse_product_operation".
 *
 * @property integer $id
 * @property integer $warehouse_id
 * @property integer $product_status_id
 * @property integer $user_confirming_id
 * @property string $description
 * @property string $date_creation
 * @property string $number
 * @property integer $is_accounted
 *
 * @property WarehouseDelivery[] $warehouseDeliveries
 * @property User $userConfirming
 * @property ProductStatus $productStatus
 * @property Warehouse $warehouse
 * @property WarehouseProductStatusHistory[] $warehouseProductStatusHistories
 */
class WarehouseProductOperation extends \yii\db\ActiveRecord {

    /**
     * @inheritdoc
     */
    public static function tableName() {
        return 'warehouse_product_operation';
    }

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
				[['warehouse_id', 'product_status_id', 'user_confirming_id'], 'required'],
				[['warehouse_id', 'product_status_id', 'user_confirming_id', 'is_accounted'], 'integer'],
				[['date_creation'], 'safe'],
				[['description'], 'string', 'max' => 2048],
				[['number'], 'string', 'max' => 48],
				[['user_confirming_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['user_confirming_id' => 'id']],
				[['product_status_id'], 'exist', 'skipOnError' => true, 'targetClass' => ProductStatus::className(), 'targetAttribute' => ['product_status_id' => 'id']],
				[['warehouse_id'], 'exist', 'skipOnError' => true, 'targetClass' => Warehouse::className(), 'targetAttribute' => ['warehouse_id' => 'id']]
		];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return [
            	'id' => Yii::t('main', 'ID'),
            	'warehouse_id' => Yii::t('main', 'Warehouse'),
            	'product_status_id' => Yii::t('main', 'Product Status ID'),
            	'user_confirming_id' => Yii::t('main', 'User Confirming ID'),
            	'description' => Yii::t('main', 'Description'),
            	'date_creation' => Yii::t('main', 'Date Creation'),
            	'number' => Yii::t('main', 'Number'),
            	'is_accounted' => Yii::t('main', 'Is Accounted'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getWarehouseDeliveries() {
        return $this->hasMany(WarehouseDelivery::className(), ['operation_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUserConfirming() {
        return $this->hasOne(User::className(), ['id' => 'user_confirming_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProductStatus() {
        return $this->hasOne(ProductStatus::className(), ['id' => 'product_status_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getWarehouse() {
        return $this->hasOne(Warehouse::className(), ['id' => 'warehouse_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getWarehouseProductStatusHistories() {
        return $this->hasMany(WarehouseProductStatusHistory::className(), ['operation_id' => 'id']);
    }

    /**
     * @inheritdoc
     * @return \common\models\aq\WarehouseProductOperationQuery the active query used by this AR class.
     */
    public static function find() {
        return new \common\models\aq\WarehouseProductOperationQuery(get_called_class());
    }
    
    public function behaviors() {
    	return [
    			[
    					'class' => TimestampBehavior::className(),
    					'createdAtAttribute' => 'date_creation',
    					'updatedAtAttribute' => false,
    					'value' => new Expression('NOW()'),
    			],
    	];
    }
}
