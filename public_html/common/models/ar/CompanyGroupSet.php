<?php

namespace common\models\ar;

use Yii;

/**
 * This is the model class for table "company_group_set".
 *
 * @property integer $company_id
 * @property integer $company_group_id
 *
 * @property CompanyGroup $companyGroup
 * @property Company $company
 */
class CompanyGroupSet extends \yii\db\ActiveRecord {

    /**
     * @inheritdoc
     */
    public static function tableName() {
        return 'company_group_set';
    }

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
				[['company_id', 'company_group_id'], 'required'],
				[['company_id', 'company_group_id'], 'integer'],
				[['company_group_id'], 'exist', 'skipOnError' => true, 'targetClass' => CompanyGroup::className(), 'targetAttribute' => ['company_group_id' => 'id']],
				[['company_id'], 'exist', 'skipOnError' => true, 'targetClass' => Company::className(), 'targetAttribute' => ['company_id' => 'id']],
        		[['company_id'], 'unique', 'targetAttribute' => ['company_id', 'company_group_id'], 'comboNotUnique' => Yii::t('main', 'The chosen company-company group pair already exists.')]
		];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return [
            	'company_id' => Yii::t('main', 'Client ID'),
            	'company_group_id' => Yii::t('main', 'Client group ID'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCompanyGroup() {
        return $this->hasOne(CompanyGroup::className(), ['id' => 'company_group_id']);
    }
    
    public function getCompanyGroupSymbol() {
    	return $this->companyGroup->symbol;
    }
    
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCompany() {
        return $this->hasOne(Company::className(), ['id' => 'company_id']);
    }

    public function getCompanyName() {
    	return $this->company->name;
    }
    /**
     * @inheritdoc
     * @return \common\models\aq\CompanyGroupSetQuery the active query used by this AR class.
     */
    public static function find() {
        return new \common\models\aq\CompanyGroupSetQuery(get_called_class());
    }
}
