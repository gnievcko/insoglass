<?php

namespace common\models\ar;

use Yii;

/**
 * This is the model class for table "task_type".
 *
 * @property integer $id
 * @property string $symbol
 * @property integer $priority 
 * @property integer $is_visible
 *
 * @property Task[] $tasks
 * @property TaskTypeReference[] $taskTypeReferences
 * @property TaskTypeTranslation[] $taskTypeTranslations
 * @property TaskHistory[] $taskHistories 
 * @property Language[] $languages
 */
class TaskType extends \yii\db\ActiveRecord {

    /**
     * @inheritdoc
     */
    public static function tableName() {
        return 'task_type';
    }

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
				[['symbol', 'priority'], 'required'],
               	[['priority', 'is_visible'], 'integer'],
				[['symbol'], 'string', 'max' => 32],
				[['symbol'], 'unique']
		];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return [
            	'id' => Yii::t('main', 'ID'),
            	'symbol' => Yii::t('main', 'Symbol'),
        		'priority' => Yii::t('main', 'Priority'),
                'is_visible' => Yii::t('main', 'Is Visible'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTasks() {
        return $this->hasMany(Task::className(), ['task_type_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTaskTypeReferences() {
        return $this->hasMany(TaskTypeReference::className(), ['task_type_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTaskTypeTranslations() {
        return $this->hasMany(TaskTypeTranslation::className(), ['task_type_id' => 'id']);
    }
    
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTaskHistories() {
    	return $this->hasMany(TaskHistory::className(), ['task_type_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLanguages() {
        return $this->hasMany(Language::className(), ['id' => 'language_id'])->viaTable('task_type_translation', ['task_type_id' => 'id']);
    }

    /**
     * @inheritdoc
     * @return \common\models\aq\TaskTypeQuery the active query used by this AR class.
     */
    public static function find() {
        return new \common\models\aq\TaskTypeQuery(get_called_class());
    }
}
