<?php

namespace common\models\ar;

use Yii;

/**
 * This is the model class for table "contract_type_translation".
 *
 * @property integer $language_id
 * @property integer $contract_type_id
 * @property string $name
 *
 * @property ContractType $contractType
 * @property Language $language
 */
class ContractTypeTranslation extends \yii\db\ActiveRecord {

    /**
     * @inheritdoc
     */
    public static function tableName() {
        return 'contract_type_translation';
    }

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
                [['language_id', 'contract_type_id', 'name'], 'required'],
                [['language_id', 'contract_type_id'], 'integer'],
                [['name'], 'string', 'max' => 32],
                [['contract_type_id'], 'exist', 'skipOnError' => true, 'targetClass' => ContractType::className(), 'targetAttribute' => ['contract_type_id' => 'id']],
                [['language_id'], 'exist', 'skipOnError' => true, 'targetClass' => Language::className(), 'targetAttribute' => ['language_id' => 'id']],
                [['contract_type_id'], 'unique', 'targetAttribute' => ['language_id', 'contract_type_id'], 'comboNotUnique' => Yii::t('main', 'The chosen contract type-language pair has a translation already.')],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return [
            'language_id' => Yii::t('main', 'Language ID'),
            'contract_type_id' => Yii::t('main', 'Contract Type ID'),
            'name' => Yii::t('main', 'Name'),
            'contractTypeSymbol' => Yii::t('main', 'Contract type'),
            'languageName' => Yii::t('main', 'Language'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getContractType() {
        return $this->hasOne(ContractType::className(), ['id' => 'contract_type_id']);
    }

    public function getContractTypeSymbol() {
        return $this->contractType->symbol;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLanguage() {
        return $this->hasOne(Language::className(), ['id' => 'language_id']);
    }

    public function getLanguageName() {
        return $this->language->name;
    }

    /**
     * @inheritdoc
     * @return \common\models\aq\ContractTypeTranslationQuery the active query used by this AR class.
     */
    public static function find() {
        return new \common\models\aq\ContractTypeTranslationQuery(get_called_class());
    }

}
