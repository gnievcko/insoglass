<?php

namespace common\models\ar;

use Yii;

/**
 * This is the model class for table "task_data".
 *
 * @property integer $task_id
 * @property integer $task_type_reference_id
 * @property integer $referenced_object_id
 *
 * @property TaskTypeReference $taskTypeReference
 * @property Task $task
 */
class TaskData extends \yii\db\ActiveRecord {

    /**
     * @inheritdoc
     */
    public static function tableName() {
        return 'task_data';
    }

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
				[['task_id', 'task_type_reference_id', 'referenced_object_id'], 'required'],
				[['task_id', 'task_type_reference_id', 'referenced_object_id'], 'integer'],
				[['task_type_reference_id'], 'exist', 'skipOnError' => true, 'targetClass' => TaskTypeReference::className(), 'targetAttribute' => ['task_type_reference_id' => 'id']],
				[['task_id'], 'exist', 'skipOnError' => true, 'targetClass' => Task::className(), 'targetAttribute' => ['task_id' => 'id']]
		];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return [
            	'task_id' => Yii::t('main', 'Task'),
            	'task_type_reference_id' => Yii::t('main', 'Task Type Reference ID'),
            	'referenced_object_id' => Yii::t('main', 'Referenced Object ID'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTaskTypeReference() {
        return $this->hasOne(TaskTypeReference::className(), ['id' => 'task_type_reference_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTask() {
        return $this->hasOne(Task::className(), ['id' => 'task_id']);
    }

    /**
     * @inheritdoc
     * @return \common\models\aq\TaskDataQuery the active query used by this AR class.
     */
    public static function find() {
        return new \common\models\aq\TaskDataQuery(get_called_class());
    }
}
