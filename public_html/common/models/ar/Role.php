<?php

namespace common\models\ar;

use Yii;

/**
 * This is the model class for table "role".
 *
 * @property integer $id
 * @property string $symbol
 *
 * @property RoleTranslation[] $roleTranslations
 * @property Language[] $languages
 * @property UserRole[] $userRoles
 * @property User[] $users
 */
class Role extends \yii\db\ActiveRecord {

    /**
     * @inheritdoc
     */
    public static function tableName() {
        return 'role';
    }

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
				[['symbol'], 'required'],
				[['symbol'], 'string', 'max' => 32],
				[['symbol'], 'unique']
		];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return [
            	'id' => Yii::t('main', 'ID'),
            	'symbol' => Yii::t('main', 'Symbol'),
            	'is_visible' => Yii::t('main', 'Is visible'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRoleTranslations() {
        return $this->hasMany(RoleTranslation::className(), ['role_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLanguages() {
        return $this->hasMany(Language::className(), ['id' => 'language_id'])->viaTable('role_translation', ['role_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUserRoles() {
        return $this->hasMany(UserRole::className(), ['role_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUsers() {
        return $this->hasMany(User::className(), ['id' => 'user_id'])->viaTable('user_role', ['role_id' => 'id']);
    }

    /**
     * @inheritdoc
     * @return \common\models\aq\RoleQuery the active query used by this AR class.
     */
    public static function find() {
        return new \common\models\aq\RoleQuery(get_called_class());
    }
}
