<?php

namespace common\models\ar;

use Yii;

/**
 * This is the model class for table "order_offered_product".
 *
 * @property integer $id
 * @property integer $order_id
 * @property integer $offered_product_id
 * @property integer $order_history_id
 * @property integer $count
 * @property string $price 
 * @property integer $currency_id 
 * @property string $description
 * @property integer $is_active
 * @property string $vat_rate 
 * @property string $unit
 * @property string $attribute_value
 * @property integer $production_path_id
 * @property integer $width
 * @property integer $height
 * 
 * @property OrderHistoryPhoto[] $orderHistoryPhotos
 * @property Currency $currency 
 * @property OrderHistory $orderHistory
 * @property Order $order
 * @property OfferedProduct $offeredProduct
 * @property Product $product
 * @property ProductionPath $productionPath
 * @property OrderOfferedProductAttachment[] $orderOfferedProductAttachments
 * @property OrderOfferedProductAttribute[] $orderOfferedProductAttributes
 * @property ProductAttributeType[] $productAttributeTypes
 * @property ProductionTask[] $productionTasks
 */
class OrderOfferedProduct extends \yii\db\ActiveRecord {

    /**
     * @inheritdoc
     */
    public static function tableName() {
        return 'order_offered_product';
    }

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
				[['order_id', 'offered_product_id'], 'required'],
				[['order_id', 'offered_product_id', 'order_history_id', 'currency_id', 'is_active', 'count', 'production_path_id', 'width', 'height'], 'integer'],
				[['description'], 'string', 'max' => 1024],
                [['vat_rate', 'unit'], 'string', 'max' => 10],
        		[['attribute_value'], 'string', 'max' => 1024],
        		[['price'], 'number'],
				[['order_history_id'], 'exist', 'skipOnError' => true, 'targetClass' => OrderHistory::className(), 'targetAttribute' => ['order_history_id' => 'id']],
				[['order_id'], 'exist', 'skipOnError' => true, 'targetClass' => Order::className(), 'targetAttribute' => ['order_id' => 'id']],
				[['offered_product_id'], 'exist', 'skipOnError' => true, 'targetClass' => OfferedProduct::className(), 'targetAttribute' => ['offered_product_id' => 'id']],
        		[['currency_id'], 'exist', 'skipOnError' => true, 'targetClass' => Currency::className(), 'targetAttribute' => ['currency_id' => 'id']],
        		[['production_path_id'], 'exist', 'skipOnError' => true, 'targetClass' => ProductionPath::className(), 'targetAttribute' => ['production_path_id' => 'id']],
                [['product_id'], 'exist', 'skipOnError' => true, 'targetClass' => Product::className(), 'targetAttribute' => ['product_id' => 'id']],
		];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return [
            	'id' => Yii::t('main', 'ID'),
            	'order_id' => Yii::t('main', 'Order'),
            	'offered_product_id' => Yii::t('main', 'Offered Product ID'),
            	'order_history_id' => Yii::t('main', 'Order History ID'),
            	'count' => Yii::t('main', 'Count'),
        		'price' => Yii::t('main', 'Price'),
        		'currency_id' => Yii::t('main', 'Currency ID'),
            	'description' => Yii::t('main', 'Description'),
            	'is_active' => Yii::t('main', 'Is Active'),
                'vat_rate' => Yii::t('main', 'Vat rate'),
                'unit' => Yii::t('main','Unit'),
        		'attribute_value' => Yii::t('main', 'Attribute Value'),
        		'production_path_id' => Yii::t('main', 'Production Path ID'),
        		'width' => Yii::t('main', 'Width'),
        		'height' => Yii::t('main', 'Height'),
        ];
    }
    
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCurrency() {
    	return $this->hasOne(Currency::className(), ['id' => 'currency_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrderHistoryPhotos() {
        return $this->hasMany(OrderHistoryPhoto::className(), ['order_offered_product_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrderHistory() {
        return $this->hasOne(OrderHistory::className(), ['id' => 'order_history_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrder() {
        return $this->hasOne(Order::className(), ['id' => 'order_id']);
    }

    public function getProductionPath() {
    	return $this->hasOne(ProductionPath::className(), ['id' => 'production_path_id']);
    }
    
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOfferedProduct() {
        return $this->hasOne(OfferedProduct::className(), ['id' => 'offered_product_id']);
    }
    
    public function getProduct() {
        return $this->hasOne(Product::className(), ['id' => 'product_id']);
    }
    
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrderOfferedProductAttachments() {
        return $this->hasMany(OrderOfferedProductAttachment::className(), ['order_offered_product_id' => 'id']);
    }
    
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrderOfferedProductAttributes() {
        return $this->hasMany(OrderOfferedProductAttribute::className(), ['order_offered_product_id' => 'id']);
    }
    
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProductAttributeTypes() {
        return $this->hasMany(ProductAttributeType::className(), ['id' => 'product_attribute_type_id'])->viaTable('order_offered_product_attribute', ['order_offered_product_id' => 'id']);
    }
    
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProductionTasks() {
        return $this->hasMany(ProductionTask::className(), ['order_offered_product_id' => 'id']);
    }

    /**
     * @inheritdoc
     * @return \common\models\aq\OrderOfferedProductQuery the active query used by this AR class.
     */
    public static function find() {
        return new \common\models\aq\OrderOfferedProductQuery(get_called_class());
    }
}
