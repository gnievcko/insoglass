<?php

namespace common\models\ar;

use Yii;

/**
 * This is the model class for table "address".
 *
 * @property integer $id
 * @property integer $city_id
 * @property string $main
 * @property string $complement
 *
 * @property City $city
 * @property Company[] $companies
 * @property Company[] $companies0
 * @property Warehouse[] $warehouses
 */
class Address extends \yii\db\ActiveRecord {

    /**
     * @inheritdoc
     */
    public static function tableName() {
        return 'address';
    }

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
				[['city_id', 'main'], 'required'],
				[['city_id'], 'integer'],
				[['main', 'complement'], 'string', 'max' => 64],
        		[['complement'], 'default', 'value' => null],
				[['city_id'], 'exist', 'skipOnError' => true, 'targetClass' => City::className(), 'targetAttribute' => ['city_id' => 'id']]
		];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return [
            	'id' => Yii::t('main', 'ID'),
            	'city_id' => Yii::t('main', 'City ID'),
            	'main' => Yii::t('main', 'Address'),
            	'complement' => Yii::t('main', 'cont.'),
        		'cityName' => Yii::t('main', 'City'),
        ];
    }
    
    public function getFullAddress($withoutProvince = false) {
    	$str = $this->main.(!empty($this->complement) ? ' '.$this->complement : '').', '.$this->city->zip_code.' '.$this->city->name;
    	
    	if(!$withoutProvince && !empty($this->city->province)) {
    		$str .= (!empty($this->city->province) ? ', '.$this->city->province->name.', '.$this->city->province->countryName : '');
    	}
    			
    	return $str;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCity() {
        return $this->hasOne(City::className(), ['id' => 'city_id']);
    }
    
    public function getCityName() {
    	return $this->city->zip_code.' '.$this->city->name;
    }
    
    public function getProvince() {
    	return $this->hasOne(Province::className(), ['id' => 'province_id'])->viaTable('city', ['city_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCompanies() {
        return $this->hasMany(Company::className(), ['address_id' => 'id']);
    }
    
    public function getUsers() {
    	return $this->hasMany(User::className(), ['address_id' => 'id']);
    }
    
    public function getWarehouseSuppliers() {
    	return $this->hasMany(WarehouseSupplier::className(), ['address_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCompanies0() {
        return $this->hasMany(Company::className(), ['address_postal_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getWarehouses() {
        return $this->hasMany(Warehouse::className(), ['address_id' => 'id']);
    }

    /**
     * @inheritdoc
     * @return \common\models\aq\AddressQuery the active query used by this AR class.
     */
    public static function find() {
        return new \common\models\aq\AddressQuery(get_called_class());
    }
}
