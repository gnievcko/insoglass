<?php

namespace common\models\ar;

use Yii;

/**
 * This is the model class for table "activity_product_attribute".
 *
 * @property integer $activity_id
 * @property integer $product_attribute_type_id
 *
 * @property ProductAttributeType $productAttributeType
 * @property Activity $activity
 */
class ActivityProductAttribute extends \yii\db\ActiveRecord {

    /**
     * @inheritdoc
     */
    public static function tableName() {
        return 'activity_product_attribute';
    }

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
				[['activity_id', 'product_attribute_type_id'], 'required'],
				[['activity_id', 'product_attribute_type_id'], 'integer'],
				[['product_attribute_type_id'], 'exist', 'skipOnError' => true, 'targetClass' => ProductAttributeType::className(), 'targetAttribute' => ['product_attribute_type_id' => 'id']],
				[['activity_id'], 'exist', 'skipOnError' => true, 'targetClass' => Activity::className(), 'targetAttribute' => ['activity_id' => 'id']]
		];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return [
            	'activity_id' => Yii::t('main', 'Activity ID'),
            	'product_attribute_type_id' => Yii::t('main', 'Product Attribute Type ID'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProductAttributeType() {
        return $this->hasOne(ProductAttributeType::className(), ['id' => 'product_attribute_type_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getActivity() {
        return $this->hasOne(Activity::className(), ['id' => 'activity_id']);
    }

    /**
     * @inheritdoc
     * @return \common\models\aq\ActivityProductAttributeQuery the active query used by this AR class.
     */
    public static function find() {
        return new \common\models\aq\ActivityProductAttributeQuery(get_called_class());
    }
}
