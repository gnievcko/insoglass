<?php

namespace common\models\ar;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\Expression;

/**
 * This is the model class for table "product".
 *
 * @property integer $id
 * @property string $symbol
 * @property string $index
 * @property integer $parent_product_id
 * @property integer $warehouse_supplier_id 
 * @property integer $priority
 * @property integer $count_minimal
 * @property integer $user_id
 * @property integer $is_visible_for_client
 * @property integer $is_available
 * @property integer $is_active
 * @property string $date_deletion 
 * @property string $date_creation
 *
 * @property User $user
 * @property Product $parentProduct
 * @property Product[] $products
 * @property ProductAlias[] $productAliases
 * @property ProductCategorySet[] $productCategorySets
 * @property ProductCategory[] $productCategories
 * @property ProductPhoto[] $productPhotos
 * @property ProductTranslation[] $productTranslations
 * @property Language[] $languages
 * @property WarehouseDeliveryProduct[] $warehouseDeliveryProducts
 * @property WarehouseProduct[] $warehouseProducts
 * @property Warehouse[] $warehouses
 * @property Alert[] $alerts
 * @property ProductAttachment[] $productAttachments 
 * @property AlertHistory[] $alertHistories
 * @property OrderOfferedProduct[] $orderOfferedProducts
 * @property User $user 
 * @property WarehouseSupplier $warehouseSupplier 
 */
class Product extends \yii\db\ActiveRecord {

    /**
     * @inheritdoc
     */
    public static function tableName() {
        return 'product';
    }

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
				[['symbol', 'is_active', 'is_visible_for_client', 'is_available', 'user_id'], 'required'],
				[['parent_product_id', 'priority', 'count_minimal', 'is_visible_for_client', 'is_available', 'is_active', 'user_id', 'warehouse_supplier_id'], 'integer'],
				[['date_deletion', 'date_creation'], 'safe'],
        		[['index'], 'string', 'max' => 48],
				[['symbol'], 'string', 'max' => 32],
				[['symbol'], 'unique'],
				[['parent_product_id'], 'exist', 'skipOnError' => true, 'targetClass' => Product::className(), 'targetAttribute' => ['parent_product_id' => 'id']],
        		[['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['user_id' => 'id']],
        		[['warehouse_supplier_id'], 'exist', 'skipOnError' => true, 'targetClass' => WarehouseSupplier::className(), 'targetAttribute' => ['warehouse_supplier_id' => 'id']],
		];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return [
            	'id' => Yii::t('main', 'ID'),
            	'symbol' => Yii::t('main', 'Symbol'),
        		'index' => Yii::t('main', 'Warehouse index'),
            	'parent_product_id' => Yii::t('main', 'Parent Product ID'),
            	'priority' => Yii::t('main', 'Priority'),
            	'count_minimal' => Yii::t('main', 'Count Minimal'),
        		'is_visible_for_client' => Yii::t('main', 'Is Visible For Client'),
            	'is_available' => Yii::t('main', 'Is Available'),
                'is_active' => Yii::t('main', 'Is Active'),
                'date_deletion' => Yii::t('main', 'Date Deletion'), 
            	'date_creation' => Yii::t('main', 'Date Creation'),
        		'user_id' => Yii::t('main', 'User'),
        		'warehouse_supplier_id' => Yii::t('main', 'Warehouse Supplier ID'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getParentProduct() {
        return $this->hasOne(Product::className(), ['id' => 'parent_product_id']);
    }
    
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser() {
    	return $this->hasOne(User::className(), ['id' => 'user_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProducts() {
        return $this->hasMany(Product::className(), ['parent_product_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProductAliases() {
        return $this->hasMany(ProductAlias::className(), ['product_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProductCategorySets() {
        return $this->hasMany(ProductCategorySet::className(), ['product_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProductCategories() {
        return $this->hasMany(ProductCategory::className(), ['id' => 'product_category_id'])->viaTable('product_category_set', ['product_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProductPhotos() {
        return $this->hasMany(ProductPhoto::className(), ['product_id' => 'id'])->orderBy('priority');
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProductTranslations() {
        return $this->hasMany(ProductTranslation::className(), ['product_id' => 'id']);
    }
    
    public function getTranslation() {
    	$translations = $this->productTranslations;
    	if(empty($translations)) {
    		return null;
    	}
    
    	foreach($translations as $t) {
    		if($t->language->symbol === Yii::$app->language) {
    			return $t;
    		}
    	}
    
    	return $translations[0];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLanguages() {
        return $this->hasMany(Language::className(), ['id' => 'language_id'])->viaTable('product_translation', ['product_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getWarehouseDeliveryProducts() {
        return $this->hasMany(WarehouseDeliveryProduct::className(), ['product_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getWarehouseProducts() {
        return $this->hasMany(WarehouseProduct::className(), ['product_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getWarehouses() {
        return $this->hasMany(Warehouse::className(), ['id' => 'warehouse_id'])->viaTable('warehouse_product', ['product_id' => 'id']);
    }
    
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAlerts() {
    	return $this->hasMany(Alert::className(), ['company_id' => 'id']);
    }
    
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAlertHistories() {
    	return $this->hasMany(AlertHistory::className(), ['product_id' => 'id']);
    }
    
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrderOfferedProducts() {
    	return $this->hasMany(OrderOfferedProduct::className(), ['product_id' => 'id']);
    }
    
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProductAttachments() {
    	return $this->hasMany(ProductAttachment::className(), ['product_id' => 'id']);
    }
    
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getWarehouseSupplier() {
    	return $this->hasOne(WarehouseSupplier::className(), ['id' => 'warehouse_supplier_id']);
    }

    /**
     * @inheritdoc
     * @return \common\models\aq\ProductQuery the active query used by this AR class.
     */
    public static function find() {
        return new \common\models\aq\ProductQuery(get_called_class());
    }
    
    public function behaviors() {
    	return [
    			[
    					'class' => TimestampBehavior::className(),
    					'createdAtAttribute' => 'date_creation',
    					'updatedAtAttribute' => false,
    					'value' => new Expression('NOW()'),
    			],
    	];
    }
}
