<?php

namespace common\models\ar;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\Expression;

/**
 * This is the model class for table "order_group".
 *
 * @property integer $id
 * @property string $number
 * @property string $date_creation
 *
 * @property Order[] $orders
 */
class OrderGroup extends \yii\db\ActiveRecord {

    /**
     * @inheritdoc
     */
    public static function tableName() {
        return 'order_group';
    }

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
				[['date_creation'], 'safe'],
				[['number'], 'string', 'max' => 48],
				[['number'], 'unique']
		];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return [
            	'id' => Yii::t('main', 'ID'),
            	'number' => Yii::t('main', 'Number'),
            	'date_creation' => Yii::t('main', 'Date Creation'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrders() {
        return $this->hasMany(Order::className(), ['order_group_id' => 'id']);
    }

    /**
     * @inheritdoc
     * @return \common\models\aq\OrderGroupQuery the active query used by this AR class.
     */
    public static function find() {
        return new \common\models\aq\OrderGroupQuery(get_called_class());
    }
    
    public function behaviors() {
    	return [
    			[
    					'class' => TimestampBehavior::className(),
    					'createdAtAttribute' => 'date_creation',
    					'updatedAtAttribute' => false,
    					'value' => new Expression('NOW()'),
    			],
    	];
    }
}
