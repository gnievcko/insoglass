<?php

namespace common\models\ar;

use Yii;

/**
 * This is the model class for table "workstation_translation".
 *
 * @property integer $workstation_id
 * @property integer $language_id
 * @property string $name
 *
 * @property Workstation $workstation
 * @property Language $language
 */
class WorkstationTranslation extends \yii\db\ActiveRecord {

    /**
     * @inheritdoc
     */
    public static function tableName() {
        return 'workstation_translation';
    }

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
				[['workstation_id', 'language_id', 'name'], 'required'],
				[['workstation_id', 'language_id'], 'integer'],
				[['name'], 'string', 'max' => 128],
				[['workstation_id'], 'exist', 'skipOnError' => true, 'targetClass' => Workstation::className(), 'targetAttribute' => ['workstation_id' => 'id']],
				[['language_id'], 'exist', 'skipOnError' => true, 'targetClass' => Language::className(), 'targetAttribute' => ['language_id' => 'id']]
		];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return [
            	'workstation_id' => Yii::t('main', 'Workstation ID'),
            	'language_id' => Yii::t('main', 'Language ID'),
            	'name' => Yii::t('main', 'Name'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getWorkstation() {
        return $this->hasOne(Workstation::className(), ['id' => 'workstation_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLanguage() {
        return $this->hasOne(Language::className(), ['id' => 'language_id']);
    }

    /**
     * @inheritdoc
     * @return \common\models\aq\WorkstationTranslationQuery the active query used by this AR class.
     */
    public static function find() {
        return new \common\models\aq\WorkstationTranslationQuery(get_called_class());
    }
}
