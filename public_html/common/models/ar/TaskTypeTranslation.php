<?php

namespace common\models\ar;

use Yii;

/**
 * This is the model class for table "task_type_translation".
 *
 * @property integer $task_type_id
 * @property integer $language_id
 * @property string $name
 *
 * @property TaskType $taskType
 * @property Language $language
 */
class TaskTypeTranslation extends \yii\db\ActiveRecord {

    /**
     * @inheritdoc
     */
    public static function tableName() {
        return 'task_type_translation';
    }

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
				[['task_type_id', 'language_id', 'name'], 'required'],
				[['task_type_id', 'language_id'], 'integer'],
				[['name'], 'string', 'max' => 128],
        		[['task_type_id'], 'unique', 'targetAttribute' => ['task_type_id', 'language_id'], 'comboNotUnique' => Yii::t('main', 'The chosen task type-language pair has a translation already.')],
				[['task_type_id'], 'exist', 'skipOnError' => true, 'targetClass' => TaskType::className(), 'targetAttribute' => ['task_type_id' => 'id']],
				[['language_id'], 'exist', 'skipOnError' => true, 'targetClass' => Language::className(), 'targetAttribute' => ['language_id' => 'id']]
		];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return [
            	'task_type_id' => Yii::t('main', 'Task type ID'),
        		'taskTypeSymbol' => Yii::t('main', 'Task type'),
            	'language_id' => Yii::t('main', 'Language ID'),
        		'languageName' => Yii::t('main', 'Language'),
            	'name' => Yii::t('main', 'Name'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTaskType() {
        return $this->hasOne(TaskType::className(), ['id' => 'task_type_id']);
    }
    
    public function getTaskTypeSymbol() {
    	return $this->taskType->symbol;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLanguage() {
        return $this->hasOne(Language::className(), ['id' => 'language_id']);
    }
    
    public function getLanguageName() {
    	return $this->language->name;
    }

    /**
     * @inheritdoc
     * @return \common\models\aq\TaskTypeTranslationQuery the active query used by this AR class.
     */
    public static function find() {
        return new \common\models\aq\TaskTypeTranslationQuery(get_called_class());
    }
}
