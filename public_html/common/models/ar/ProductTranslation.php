<?php

namespace common\models\ar;

use Yii;

/**
 * This is the model class for table "product_translation".
 *
 * @property integer $product_id
 * @property integer $language_id
 * @property string $name
 * @property string $description
 * @property string $remarks
 * @property string $price_default
 * @property string $unit_default
 * @property integer $currency_id
 *
 * @property Currency $currency
 * @property Language $language
 * @property Product $product
 */
class ProductTranslation extends \yii\db\ActiveRecord {

    /**
     * @inheritdoc
     */
    public static function tableName() {
        return 'product_translation';
    }

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
				[['product_id', 'language_id', 'name'], 'required'],
				[['product_id', 'language_id', 'currency_id'], 'integer'],
				[['price_default'], 'number'],
				[['name'], 'string', 'max' => 2048],
				[['description'], 'string', 'max' => 2048],
        		[['remarks'], 'string', 'max' => 1024],
        		[['unit_default'], 'string', 'max' => 10],
				[['currency_id'], 'exist', 'skipOnError' => true, 'targetClass' => Currency::className(), 'targetAttribute' => ['currency_id' => 'id']],
				[['language_id'], 'exist', 'skipOnError' => true, 'targetClass' => Language::className(), 'targetAttribute' => ['language_id' => 'id']],
				[['product_id'], 'exist', 'skipOnError' => true, 'targetClass' => Product::className(), 'targetAttribute' => ['product_id' => 'id']]
		];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return [
            	'product_id' => Yii::t('main', 'Product'),
            	'language_id' => Yii::t('main', 'Language ID'),
            	'name' => Yii::t('main', 'Name'),
            	'description' => Yii::t('main', 'Description'),
        		'remarks' => Yii::t('main', 'Remarks'),
            	'price_default' => Yii::t('main', 'Price Default'),
            	'currency_id' => Yii::t('main', 'Currency ID'),
        		'unit_default' => Yii::t('main', 'Unit Default'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCurrency() {
        return $this->hasOne(Currency::className(), ['id' => 'currency_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLanguage() {
        return $this->hasOne(Language::className(), ['id' => 'language_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProduct() {
        return $this->hasOne(Product::className(), ['id' => 'product_id']);
    }

    /**
     * @inheritdoc
     * @return \common\models\aq\ProductTranslationQuery the active query used by this AR class.
     */
    public static function find() {
        return new \common\models\aq\ProductTranslationQuery(get_called_class());
    }
}
