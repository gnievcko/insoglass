<?php

namespace common\models\ar;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\Expression;

/**
 * This is the model class for table "product_attachment".
 *
 * @property integer $id
 * @property integer $product_id
 * @property integer $user_id
 * @property string $url_file
 * @property string $name
 * @property string $description
 * @property string $date_creation
 *
 * @property User $user
 * @property Product $product
 */
class ProductAttachment extends \yii\db\ActiveRecord {

    /**
     * @inheritdoc
     */
    public static function tableName() {
        return 'product_attachment';
    }

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
				[['product_id', 'user_id', 'url_file', 'name'], 'required'],
				[['product_id', 'user_id'], 'integer'],
				[['date_creation'], 'safe'],
				[['url_file', 'name'], 'string', 'max' => 256],
				[['description'], 'string', 'max' => 512],
				[['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['user_id' => 'id']],
				[['product_id'], 'exist', 'skipOnError' => true, 'targetClass' => Product::className(), 'targetAttribute' => ['product_id' => 'id']]
		];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return [
            	'id' => Yii::t('main', 'ID'),
            	'product_id' => Yii::t('main', 'Product'),
            	'user_id' => Yii::t('main', 'User'),
            	'url_file' => Yii::t('main', 'Url File'),
        		'name' => Yii::t('main', 'Name'),
            	'description' => Yii::t('main', 'Description'),
            	'date_creation' => Yii::t('main', 'Date Creation'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser() {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProduct() {
        return $this->hasOne(Product::className(), ['id' => 'product_id']);
    }

    /**
     * @inheritdoc
     * @return \common\models\aq\ProductAttachmentQuery the active query used by this AR class.
     */
    public static function find() {
        return new \common\models\aq\ProductAttachmentQuery(get_called_class());
    }
    
    public function behaviors() {
    	return [
    			[
    					'class' => TimestampBehavior::className(),
    					'createdAtAttribute' => 'date_creation',
    					'updatedAtAttribute' => false,
    					'value' => new Expression('NOW()'),
    			],
    	];
    }
}
