<?php

namespace common\models\ar;

use Yii;
use yii\db\Query;

/**
 * This is the model class for table "product_attribute_group".
 *
 * @property integer $id
 * @property string $symbol
 *
 * @property ProductAttributeGroupTranslation[] $productAttributeGroupTranslations
 * @property Language[] $languages
 * @property ProductAttributeType[] $productAttributeTypes
 */
class ProductAttributeGroup extends \yii\db\ActiveRecord {

    /**
     * @inheritdoc
     */
    public static function tableName() {
        return 'product_attribute_group';
    }

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
				[['symbol'], 'required'],
				[['symbol'], 'string', 'max' => 32],
				[['symbol'], 'unique']
		];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return [
            	'id' => Yii::t('main', 'ID'),
            	'symbol' => Yii::t('main', 'Symbol'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProductAttributeGroupTranslations() {
        return $this->hasMany(ProductAttributeGroupTranslation::className(), ['product_attribute_group_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLanguages() {
        return $this->hasMany(Language::className(), ['id' => 'language_id'])->viaTable('product_attribute_group_translation', ['product_attribute_group_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProductAttributeTypes() {
        return $this->hasMany(ProductAttributeType::className(), ['product_attribute_group_id' => 'id']);
    }

    /**
     * @inheritdoc
     * @return \common\models\aq\ProductAttributeGroupQuery the active query used by this AR class.
     */
    public static function find() {
        return new \common\models\aq\ProductAttributeGroupQuery(get_called_class());
    }
    
}
