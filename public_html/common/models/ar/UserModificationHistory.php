<?php

namespace common\models\ar;

use Yii;

/**
 * This is the model class for table "user_modification_history".
 *
 * @property integer $id
 * @property string $modification
 */
class UserModificationHistory extends \yii\db\ActiveRecord {

    /**
     * @inheritdoc
     */
    public static function tableName() {
        return 'user_modification_history';
    }

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
				[['modification'], 'required'],
				[['modification'], 'string']
		];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return [
            	'id' => Yii::t('main', 'ID'),
            	'modification' => Yii::t('main', 'Modification'),
        ];
    }

    /**
     * @inheritdoc
     * @return \common\models\aq\UserModificationHistoryQuery the active query used by this AR class.
     */
    public static function find() {
        return new \common\models\aq\UserModificationHistoryQuery(get_called_class());
    }
}
