<?php

namespace common\models\ar;

use Yii;

/**
 * This is the model class for table "order_priority".
 *
 * @property integer $id
 * @property string $symbol
 * @property integer $priority
 *
 * @property Order[] $orders
 * @property OrderPriorityTranslation[] $orderPriorityTranslations
 * @property Language[] $languages
 */
class OrderPriority extends \yii\db\ActiveRecord {

    /**
     * @inheritdoc
     */
    public static function tableName() {
        return 'order_priority';
    }

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
				[['symbol', 'priority'], 'required'],
				[['priority'], 'integer'],
				[['symbol'], 'string', 'max' => 32],
				[['symbol'], 'unique']
		];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return [
            	'id' => Yii::t('main', 'ID'),
            	'symbol' => Yii::t('main', 'Symbol'),
            	'priority' => Yii::t('main', 'Priority'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrders() {
        return $this->hasMany(Order::className(), ['order_priority_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrderPriorityTranslations() {
        return $this->hasMany(OrderPriorityTranslation::className(), ['order_priority_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLanguages() {
        return $this->hasMany(Language::className(), ['id' => 'language_id'])->viaTable('order_priority_translation', ['order_priority_id' => 'id']);
    }

    public function getTranslation() {
        $translations = $this->orderPriorityTranslations;
        foreach($translations as $t) {
            if($t->language->symbol === Yii::$app->language) {
                return $t;
            }
        }
        return null;
    }
    
    /**
     * @inheritdoc
     * @return \common\models\aq\OrderPriorityQuery the active query used by this AR class.
     */
    public static function find() {
        return new \common\models\aq\OrderPriorityQuery(get_called_class());
    }
}
