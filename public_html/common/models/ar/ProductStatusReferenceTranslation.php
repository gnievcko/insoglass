<?php

namespace common\models\ar;

use Yii;

/**
 * This is the model class for table "product_status_reference_translation".
 *
 * @property integer $reference_id
 * @property integer $language_id
 * @property string $name
 *
 * @property Language $language
 * @property ProductStatusReference $reference
 */
class ProductStatusReferenceTranslation extends \yii\db\ActiveRecord {

    /**
     * @inheritdoc
     */
    public static function tableName() {
        return 'product_status_reference_translation';
    }

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
				[['reference_id', 'language_id', 'name'], 'required'],
				[['reference_id', 'language_id'], 'integer'],
				[['name'], 'string', 'max' => 512],
				[['language_id'], 'exist', 'skipOnError' => true, 'targetClass' => Language::className(), 'targetAttribute' => ['language_id' => 'id']],
				[['reference_id'], 'exist', 'skipOnError' => true, 'targetClass' => ProductStatusReference::className(), 'targetAttribute' => ['reference_id' => 'id']],
        		[['reference_id'], 'unique', 'targetAttribute' => ['reference_id', 'language_id'], 'comboNotUnique' => Yii::t('main', 'The chosen reference and language have a translation already')],
        		
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return [
            	'reference_id' => Yii::t('main', 'Reference ID'),
            	'language_id' => Yii::t('main', 'Language ID'),
            	'name' => Yii::t('main', 'Name'),
        		'referenceSymbol' => Yii::t('main', 'Symbol'),
        		'languageName' => Yii::t('main', 'Language'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLanguage() {
        return $this->hasOne(Language::className(), ['id' => 'language_id']);
    }
    
    public function getLanguageName() {
    	return $this->language->name;
    }
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getReference() {
        return $this->hasOne(ProductStatusReference::className(), ['id' => 'reference_id']);
    }
    
    public function getReferenceSymbol(){
    	return $this->reference->symbol;
    }

    /**
     * @inheritdoc
     * @return \common\models\aq\ProductStatusReferenceTranslationQuery the active query used by this AR class.
     */
    public static function find() {
        return new \common\models\aq\ProductStatusReferenceTranslationQuery(get_called_class());
    }
}
