<?php

namespace common\models\ar;

use Yii;

/**
 * This is the model class for table "role_translation".
 *
 * @property integer $role_id
 * @property integer $language_id
 * @property string $name
 *
 * @property Role $role
 * @property Language $language
 */
class RoleTranslation extends \yii\db\ActiveRecord {

    /**
     * @inheritdoc
     */
    public static function tableName() {
        return 'role_translation';
    }

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
				[['role_id', 'language_id', 'name'], 'required'],
				[['role_id', 'language_id'], 'integer'],
				[['name'], 'string', 'max' => 128],
				[['role_id'], 'exist', 'skipOnError' => true, 'targetClass' => Role::className(), 'targetAttribute' => ['role_id' => 'id']],
				[['language_id'], 'exist', 'skipOnError' => true, 'targetClass' => Language::className(), 'targetAttribute' => ['language_id' => 'id']],
        		[['role_id'], 'unique', 'targetAttribute' => ['role_id', 'language_id'], 'comboNotUnique' => Yii::t('main', 'The chosen role-language pair has a translation already.')],
		];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return [
            	'role_id' => Yii::t('main', 'Role ID'),
            	'language_id' => Yii::t('main', 'Language ID'),
            	'name' => Yii::t('main', 'Name'),
            	'roleSymbol' => Yii::t('main', 'Role'),
            	'languageName' => Yii::t('main', 'Language'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRole() {
        return $this->hasOne(Role::className(), ['id' => 'role_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLanguage() {
        return $this->hasOne(Language::className(), ['id' => 'language_id']);
    }

    public function getRoleSymbol() {
    	return $this->role->symbol;
    }

    public function getLanguageName() {
    	return $this->language->name;
    }

    /**
     * @inheritdoc
     * @return \common\models\aq\RoleTranslationQuery the active query used by this AR class.
     */
    public static function find() {
        return new \common\models\aq\RoleTranslationQuery(get_called_class());
    }
}
