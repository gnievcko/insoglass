<?php

namespace common\models\ar;

use Yii;

/**
 * This is the model class for table "department_translation".
 *
 * @property integer $department_id
 * @property integer $language_id
 * @property string $name
 *
 * @property Department $department
 * @property Language $language
 */
class DepartmentTranslation extends \yii\db\ActiveRecord {

    /**
     * @inheritdoc
     */
    public static function tableName() {
        return 'department_translation';
    }

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
				[['department_id', 'language_id', 'name'], 'required'],
				[['department_id', 'language_id'], 'integer'],
				[['name'], 'string', 'max' => 256],
				[['department_id'], 'exist', 'skipOnError' => true, 'targetClass' => Department::className(), 'targetAttribute' => ['department_id' => 'id']],
				[['language_id'], 'exist', 'skipOnError' => true, 'targetClass' => Language::className(), 'targetAttribute' => ['language_id' => 'id']]
		];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return [
            	'department_id' => Yii::t('main', 'Department ID'),
            	'language_id' => Yii::t('main', 'Language ID'),
            	'name' => Yii::t('main', 'Name'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDepartment() {
        return $this->hasOne(Department::className(), ['id' => 'department_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLanguage() {
        return $this->hasOne(Language::className(), ['id' => 'language_id']);
    }

    /**
     * @inheritdoc
     * @return \common\models\aq\DepartmentTranslationQuery the active query used by this AR class.
     */
    public static function find() {
        return new \common\models\aq\DepartmentTranslationQuery(get_called_class());
    }


}
