<?php

namespace common\models\ar;

use Yii;

/**
 * This is the model class for table "alert_history".
 *
 * @property integer $id
 * @property integer $alert_type_id
 * @property integer $user_id
 * @property string $message
 * @property integer $user_deactivating_id
 * @property string $date_deactivation
 * @property string $date_creation
 * @property integer $order_id
 * @property integer $product_id
 * @property integer $company_id
 * @property integer $task_id
 * @property integer $task_history_id
 * @property integer $warehouse_id
 *
 * @property Warehouse $warehouse
 * @property AlertType $alertType
 * @property Company $company
 * @property Order $order
 * @property Product $product
 * @property Task $task
 * @property User $user
 * @property User $userDeactivating
 */
class AlertHistory extends \yii\db\ActiveRecord {

    /**
     * @inheritdoc
     */
    public static function tableName() {
        return 'alert_history';
    }

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
				[['alert_type_id', 'user_id', 'message'], 'required'],
				[['alert_type_id', 'user_id', 'user_deactivating_id', 'order_id', 'product_id', 'company_id', 'task_id', 'task_history_id', 'warehouse_id'], 'integer'],
				[['date_deactivation', 'date_creation'], 'safe'],
				[['message'], 'string', 'max' => 2048],
				[['warehouse_id'], 'exist', 'skipOnError' => true, 'targetClass' => Warehouse::className(), 'targetAttribute' => ['warehouse_id' => 'id']],
				[['alert_type_id'], 'exist', 'skipOnError' => true, 'targetClass' => AlertType::className(), 'targetAttribute' => ['alert_type_id' => 'id']],
				[['company_id'], 'exist', 'skipOnError' => true, 'targetClass' => Company::className(), 'targetAttribute' => ['company_id' => 'id']],
				[['order_id'], 'exist', 'skipOnError' => true, 'targetClass' => Order::className(), 'targetAttribute' => ['order_id' => 'id']],
				[['product_id'], 'exist', 'skipOnError' => true, 'targetClass' => Product::className(), 'targetAttribute' => ['product_id' => 'id']],
				[['task_id'], 'exist', 'skipOnError' => true, 'targetClass' => Task::className(), 'targetAttribute' => ['task_id' => 'id']],
        		[['task_history_id'], 'exist', 'skipOnError' => true, 'targetClass' => TaskHistory::className(), 'targetAttribute' => ['task_history_id' => 'id']],
				[['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['user_id' => 'id']],
				[['user_deactivating_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['user_deactivating_id' => 'id']]
		];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return [
            	'id' => Yii::t('main', 'ID'),
            	'alert_type_id' => Yii::t('main', 'Alert Type'),
            	'user_id' => Yii::t('main', 'User'),
            	'message' => Yii::t('main', 'Message'),
            	'user_deactivating_id' => Yii::t('main', 'User Deactivating ID'),
            	'date_deactivation' => Yii::t('main', 'Date Deactivation'),
            	'date_creation' => Yii::t('main', 'Date Creation'),
            	'order_id' => Yii::t('main', 'Order'),
            	'product_id' => Yii::t('main', 'Product'),
            	'company_id' => Yii::t('main', 'Company'),
            	'task_id' => Yii::t('main', 'Task'),
        		'task_history_id' => Yii::t('main', 'Task History ID'),
            	'warehouse_id' => Yii::t('main', 'Warehouse'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getWarehouse() {
        return $this->hasOne(Warehouse::className(), ['id' => 'warehouse_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAlertType() {
        return $this->hasOne(AlertType::className(), ['id' => 'alert_type_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCompany() {
        return $this->hasOne(Company::className(), ['id' => 'company_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrder() {
        return $this->hasOne(Order::className(), ['id' => 'order_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProduct() {
        return $this->hasOne(Product::className(), ['id' => 'product_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTask() {
        return $this->hasOne(Task::className(), ['id' => 'task_id']);
    }
    
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTaskHistory() {
    	return $this->hasOne(TaskHistory::className(), ['id' => 'task_history_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser() {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUserDeactivating() {
        return $this->hasOne(User::className(), ['id' => 'user_deactivating_id']);
    }

    /**
     * @inheritdoc
     * @return \common\models\aq\AlertHistoryQuery the active query used by this AR class.
     */
    public static function find() {
        return new \common\models\aq\AlertHistoryQuery(get_called_class());
    }
}
