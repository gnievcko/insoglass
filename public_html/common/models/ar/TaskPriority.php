<?php

namespace common\models\ar;

use Yii;

/**
 * This is the model class for table "task_priority".
 *
 * @property integer $id
 * @property string $symbol
 * @property integer $priority
 *
 * @property Task[] $tasks
 * @property TaskPriorityTranslation[] $taskPriorityTranslations
 * @property Language[] $languages
 */
class TaskPriority extends \yii\db\ActiveRecord {

    /**
     * @inheritdoc
     */
    public static function tableName() {
        return 'task_priority';
    }

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
				[['symbol', 'priority'], 'required'],
				[['priority'], 'integer'],
				[['symbol'], 'string', 'max' => 32],
				[['symbol'], 'unique']
		];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return [
            	'id' => Yii::t('main', 'ID'),
            	'symbol' => Yii::t('main', 'Symbol'),
            	'priority' => Yii::t('main', 'Priority'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTasks() {
        return $this->hasMany(Task::className(), ['task_priority_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTaskPriorityTranslations() {
        return $this->hasMany(TaskPriorityTranslation::className(), ['task_priority_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLanguages() {
        return $this->hasMany(Language::className(), ['id' => 'language_id'])->viaTable('task_priority_translation', ['task_priority_id' => 'id']);
    }

    /**
     * @inheritdoc
     * @return \common\models\aq\TaskPriorityQuery the active query used by this AR class.
     */
    public static function find() {
        return new \common\models\aq\TaskPriorityQuery(get_called_class());
    }
}
