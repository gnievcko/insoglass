<?php

namespace common\models\ar;

use Yii;

/**
 * This is the model class for table "ral_colour_group_translation".
 *
 * @property integer $ral_colour_group_id
 * @property integer $language_id
 * @property string $name
 *
 * @property Language $language
 * @property RalColourGroup $ralColourGroup
 */
class RalColourGroupTranslation extends \yii\db\ActiveRecord {

    /**
     * @inheritdoc
     */
    public static function tableName() {
        return 'ral_colour_group_translation';
    }

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
			[['ral_colour_group_id', 'language_id', 'name'], 'required'],
			[['ral_colour_group_id', 'language_id'], 'integer'],
			[['name'], 'string', 'max' => 128],
			[['language_id'], 'exist', 'skipOnError' => true, 'targetClass' => Language::className(), 'targetAttribute' => ['language_id' => 'id']],
			[['ral_colour_group_id'], 'exist', 'skipOnError' => true, 'targetClass' => RalColourGroup::className(), 'targetAttribute' => ['ral_colour_group_id' => 'id']]
		];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return [
            'ral_colour_group_id' => Yii::t('main', 'Ral Colour Group ID'),
            'language_id' => Yii::t('main', 'Language ID'),
            'name' => Yii::t('main', 'Name'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLanguage() {
        return $this->hasOne(Language::className(), ['id' => 'language_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRalColourGroup() {
        return $this->hasOne(RalColourGroup::className(), ['id' => 'ral_colour_group_id']);
    }

    /**
     * @inheritdoc
     * @return \common\models\aq\RalColourGroupTranslationQuery the active query used by this AR class.
     */
    public static function find() {
        return new \common\models\aq\RalColourGroupTranslationQuery(get_called_class());
    }
}
