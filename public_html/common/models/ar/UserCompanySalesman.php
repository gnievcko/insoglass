<?php

namespace common\models\ar;

use Yii;

/**
 * This is the model class for table "user_company_salesman".
 *
 * @property integer $user_id
 * @property integer $company_group_id
 *
 * @property CompanyGroup $companyGroup
 * @property User $user
 */
class UserCompanySalesman extends \yii\db\ActiveRecord {


    /**
     * @inheritdoc
     */
    public static function tableName() {
        return 'user_company_salesman';
    }

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
				[['user_id', 'company_group_id'], 'required'],
				[['user_id', 'company_group_id'], 'integer'],
				[['company_group_id'], 'exist', 'skipOnError' => true, 'targetClass' => CompanyGroup::className(), 'targetAttribute' => ['company_group_id' => 'id']],
				[['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['user_id' => 'id']]
		];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return [
            	'user_id' => Yii::t('main', 'User'),
            	'company_group_id' => Yii::t('main', 'Client group ID'),
        		'companyGroupSymbol' => Yii::t('main', 'Client group'),
        		'userName' => Yii::t('main', 'User'),

        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCompanyGroup() {
        return $this->hasOne(CompanyGroup::className(), ['id' => 'company_group_id']);
    }

    //TODO
    public function getCompanyGroupSymbol() {
    	$translation = $this->companyGroup->getTranslation();
    	return !empty($translation) ? $translation->name : $this->companyGroup->symbol;
    }
    

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser() {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }
    
	public function getUserName() {
    	return User::discoverNameForm($this->user);
    }
    
    public function getCompanyGroupTranslations() {
    	return $this->hasMany(CompanyGroupTranslation::className(), ['company_group_id' => 'company_group_id']);
    }

}
