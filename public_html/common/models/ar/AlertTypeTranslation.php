<?php

namespace common\models\ar;

use Yii;

/**
 * This is the model class for table "alert_type_translation".
 *
 * @property integer $alert_type_id
 * @property integer $language_id
 * @property string $name
 *
 * @property AlertType $alertType
 * @property Language $language
 */
class AlertTypeTranslation extends \yii\db\ActiveRecord {

    /**
     * @inheritdoc
     */
    public static function tableName() {
        return 'alert_type_translation';
    }

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
				[['alert_type_id', 'language_id', 'name'], 'required'],
				[['alert_type_id', 'language_id'], 'integer'],
				[['name'], 'string', 'max' => 128],
				[['alert_type_id'], 'exist', 'skipOnError' => true, 'targetClass' => AlertType::className(), 'targetAttribute' => ['alert_type_id' => 'id']],
				[['language_id'], 'exist', 'skipOnError' => true, 'targetClass' => Language::className(), 'targetAttribute' => ['language_id' => 'id']],
        		[['alert_type_id'], 'unique', 'targetAttribute' => ['alert_type_id', 'language_id'], 'comboNotUnique' => Yii::t('main', 'The chosen alert type and language have a translation already')],
		];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return [
            	'alert_type_id' => Yii::t('main', 'Alert type'),
            	'language_id' => Yii::t('main', 'Language ID'),
            	'name' => Yii::t('main', 'Name'),
        		'alertTypeSymbol' => Yii::t('main', 'Symbol'),
        		'languageName' => Yii::t('main', 'Language'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAlertType() {
        return $this->hasOne(AlertType::className(), ['id' => 'alert_type_id']);
    }
	
    public function getAlertTypeSymbol() {
    	return $this->alertType->symbol;
    }
    
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLanguage() {
        return $this->hasOne(Language::className(), ['id' => 'language_id']);
    }

    public function getLanguageName() {
    	return $this->language->name;
    }
    
    
    /**
     * @inheritdoc
     * @return \common\models\aq\AlertTypeTranslationQuery the active query used by this AR class.
     */
    public static function find() {
        return new \common\models\aq\AlertTypeTranslationQuery(get_called_class());
    }
}
