<?php

namespace common\models\ar;

use Yii;

/**
 * This is the model class for table "ral_colour_group".
 *
 * @property integer $id
 * @property string $symbol
 * @property integer $index
 * @property string $rgb_hash
 *
 * @property RalColour[] $ralColours
 * @property RalColourGroupTranslation[] $ralColourGroupTranslations
 * @property Language[] $languages
 */
class RalColourGroup extends \yii\db\ActiveRecord {

    /**
     * @inheritdoc
     */
    public static function tableName() {
        return 'ral_colour_group';
    }

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
			[['symbol', 'index', 'rgb_hash'], 'required'],
			[['index'], 'integer'],
			[['symbol'], 'string', 'max' => 32],
			[['rgb_hash'], 'string', 'max' => 7],
			[['symbol'], 'unique']
		];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return [
            'id' => Yii::t('main', 'ID'),
            'symbol' => Yii::t('main', 'Symbol'),
            'index' => Yii::t('main', 'Index'),
            'rgb_hash' => Yii::t('main', 'Rgb Hash'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRalColours() {
        return $this->hasMany(RalColour::className(), ['ral_colour_group_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRalColourGroupTranslations() {
        return $this->hasMany(RalColourGroupTranslation::className(), ['ral_colour_group_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLanguages() {
        return $this->hasMany(Language::className(), ['id' => 'language_id'])->viaTable('ral_colour_group_translation', ['ral_colour_group_id' => 'id']);
    }

    /**
     * @inheritdoc
     * @return \common\models\aq\RalColourGroupQuery the active query used by this AR class.
     */
    public static function find() {
        return new \common\models\aq\RalColourGroupQuery(get_called_class());
    }
}
