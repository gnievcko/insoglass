<?php

namespace common\models\ar;

use Yii;

/**
 * This is the model class for table "warehouse_supplier".
 *
 * @property integer $id
 * @property string $name
 * @property string $contact_person 
 * @property string $phone 
 * @property string $email 
 * @property integer $address_id 
 * @property integer $is_active
 * @property string $date_creation
 *
 * @property Product[] $products 
 * @property WarehouseDelivery[] $warehouseDeliveries
 * @property Address $address 
 */
class WarehouseSupplier extends \yii\db\ActiveRecord {

    /**
     * @inheritdoc
     */
    public static function tableName() {
        return 'warehouse_supplier';
    }

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
				[['address_id', 'is_active'], 'integer'],
				[['name'], 'required'],
				[['date_creation'], 'safe'],
        		[['name', 'contact_person'], 'string', 'max' => 256],
        		[['phone'], 'string', 'max' => 32],
        		[['email'], 'string', 'max' => 64],
        		[['address_id'], 'exist', 'skipOnError' => true, 'targetClass' => Address::className(), 'targetAttribute' => ['address_id' => 'id']],
        		[['contact_person', 'phone', 'email'], 'default', 'value' => null],
		];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return [
            	'id' => Yii::t('main', 'ID'),
            	'name' => Yii::t('main', 'Name'),
        		'contact_person' => Yii::t('main', 'Contact Person'),
        		'phone' => Yii::t('main', 'Phone'),
        		'email' => Yii::t('main', 'Email'),
        		'address_id' => Yii::t('main', 'Address ID'),
            	'is_active' => Yii::t('main', 'Is Active'),
            	'date_creation' => Yii::t('main', 'Date Creation'),
        ];
    }
    
    public function getProducts() {
    	return $this->hasMany(Product::className(), ['warehouse_supplier_id' => 'id']);
    }

    public function getWarehouseDeliveries() {
        return $this->hasMany(WarehouseDelivery::className(), ['warehouse_supplier_id' => 'id']);
    }
    
    public function getAddress() {
    	return $this->hasOne(Address::className(), ['id' => 'address_id']);
    }
    
    public function getWarehouseProductStatusHistories() {
    	return $this->hasMany(WarehouseProduct::className(), ['warehouse_supplier_id' => 'id']);
    }
    
    public function getAddressName() {
    	return !empty($this->address) ? $this->address->fullAddress : null;
    }

    /**
     * @inheritdoc
     * @return \common\models\aq\WarehouseSupplierQuery the active query used by this AR class.
     */
    public static function find() {
        return new \common\models\aq\WarehouseSupplierQuery(get_called_class());
    }
}
