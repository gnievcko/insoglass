<?php

namespace common\models\ar;

use Yii;

/**
 * This is the model class for table "order_custom_data".
 *
 * @property integer $id
 * @property integer $column_count
 * @property string $content
 *
 * @property OrderHistory[] $orderHistories
 */
class OrderCustomData extends \yii\db\ActiveRecord {

    /**
     * @inheritdoc
     */
    public static function tableName() {
        return 'order_custom_data';
    }

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
				[['content', 'column_count'], 'required'],
				[['content'], 'string'],
        		[['column_count'], 'integer'],
		];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return [
            	'id' => Yii::t('main', 'ID'),
        		'column_count' => Yii::t('main', 'Column Count'),
            	'content' => Yii::t('main', 'Content'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrderHistories() {
        return $this->hasMany(OrderHistory::className(), ['order_custom_data_id' => 'id']);
    }

    /**
     * @inheritdoc
     * @return \common\models\aq\OrderCustomDataQuery the active query used by this AR class.
     */
    public static function find() {
        return new \common\models\aq\OrderCustomDataQuery(get_called_class());
    }
}
