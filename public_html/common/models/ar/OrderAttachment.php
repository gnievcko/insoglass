<?php

namespace common\models\ar;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\Expression;

/**
 * This is the model class for table "order_attachment".
 *
 * @property integer $id
 * @property integer $order_id
 * @property integer $order_history_id
 * @property integer $user_id
 * @property string $url_file
 * @property string $name
 * @property string $description
 * @property string $date_creation
 *
 * @property User $user
 * @property Order $order
 * @property OrderHistory $orderHistory
 */
class OrderAttachment extends \yii\db\ActiveRecord {

    /**
     * @inheritdoc
     */
    public static function tableName() {
        return 'order_attachment';
    }

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
				[['order_id', 'user_id', 'url_file', 'name'], 'required'],
				[['order_id', 'order_history_id', 'user_id'], 'integer'],
				[['date_creation'], 'safe'],
				[['url_file', 'name'], 'string', 'max' => 256],
				[['description'], 'string', 'max' => 512],
				[['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['user_id' => 'id']],
				[['order_id'], 'exist', 'skipOnError' => true, 'targetClass' => Order::className(), 'targetAttribute' => ['order_id' => 'id']],
				[['order_history_id'], 'exist', 'skipOnError' => true, 'targetClass' => OrderHistory::className(), 'targetAttribute' => ['order_history_id' => 'id']]
		];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return [
            	'id' => Yii::t('main', 'ID'),
            	'order_id' => Yii::t('main', 'Order'),
            	'order_history_id' => Yii::t('main', 'Order History ID'),
            	'user_id' => Yii::t('main', 'User'),
            	'url_file' => Yii::t('main', 'Url File'),
            	'description' => Yii::t('main', 'Description'),
            	'date_creation' => Yii::t('main', 'Date Creation'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser() {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrder() {
        return $this->hasOne(Order::className(), ['id' => 'order_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrderHistory() {
        return $this->hasOne(OrderHistory::className(), ['id' => 'order_history_id']);
    }

    /**
     * @inheritdoc
     * @return \common\models\aq\OrderAttachmentQuery the active query used by this AR class.
     */
    public static function find() {
        return new \common\models\aq\OrderAttachmentQuery(get_called_class());
    }
    
    public function behaviors() {
    	return [
    			[
    					'class' => TimestampBehavior::className(),
    					'createdAtAttribute' => 'date_creation',
    					'updatedAtAttribute' => false,
    					'value' => new Expression('NOW()'),
    			],
    	];
    }
}
