<?php

namespace common\models\ar;

use Yii;

/**
 * This is the model class for table "order_type_translation".
 *
 * @property integer $language_id
 * @property integer $order_type_id
 * @property string $name
 *
 * @property Language $language
 * @property OrderType $orderType
 */
class OrderTypeTranslation extends \yii\db\ActiveRecord {

    /**
     * @inheritdoc
     */
    public static function tableName() {
        return 'order_type_translation';
    }

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
				[['language_id', 'order_type_id', 'name'], 'required'],
				[['language_id', 'order_type_id'], 'integer'],
				[['name'], 'string', 'max' => 128],
				[['language_id'], 'exist', 'skipOnError' => true, 'targetClass' => Language::className(), 'targetAttribute' => ['language_id' => 'id']],
				[['order_type_id'], 'exist', 'skipOnError' => true, 'targetClass' => OrderType::className(), 'targetAttribute' => ['order_type_id' => 'id']]
		];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return [
            	'language_id' => Yii::t('main', 'Language ID'),
            	'order_type_id' => Yii::t('main', 'Order Type ID'),
            	'name' => Yii::t('main', 'Name'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLanguage() {
        return $this->hasOne(Language::className(), ['id' => 'language_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrderType() {
        return $this->hasOne(OrderType::className(), ['id' => 'order_type_id']);
    }

    /**
     * @inheritdoc
     * @return \common\models\aq\OrderTypeTranslationQuery the active query used by this AR class.
     */
    public static function find() {
        return new \common\models\aq\OrderTypeTranslationQuery(get_called_class());
    }
}
