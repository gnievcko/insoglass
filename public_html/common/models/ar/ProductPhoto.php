<?php

namespace common\models\ar;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\Expression;

/**
 * This is the model class for table "product_photo".
 *
 * @property integer $id
 * @property integer $product_id
 * @property string $url_photo
 * @property string $url_thumbnail
 * @property integer $priority
 * @property string $date_creation
 * @property string $description
 *
 * @property Product $product
 * @property ProductPhotoTranslation[] $productPhotoTranslations
 * @property Language[] $languages
 */
class ProductPhoto extends \yii\db\ActiveRecord {

    /**
     * @inheritdoc
     */
    public static function tableName() {
        return 'product_photo';
    }

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
				[['product_id', 'url_photo', 'url_thumbnail'], 'required'],
				[['product_id', 'priority', 'url_thumbnail'], 'integer'],
				[['date_creation'], 'safe'],
				[['url_photo'], 'string', 'max' => 256],
        		[['description'], 'string', 'max' => 512],
				[['product_id'], 'exist', 'skipOnError' => true, 'targetClass' => Product::className(), 'targetAttribute' => ['product_id' => 'id']]
		];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return [
            	'id' => Yii::t('main', 'ID'),
            	'product_id' => Yii::t('main', 'Product'),
            	'url_photo' => Yii::t('main', 'Url Photo'),
            	'priority' => Yii::t('main', 'Priority'),
            	'date_creation' => Yii::t('main', 'Date Creation'),
        		'description' => Yii::t('main', 'Description'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProduct() {
        return $this->hasOne(Product::className(), ['id' => 'product_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProductPhotoTranslations() {
        return $this->hasMany(ProductPhotoTranslation::className(), ['product_photo_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLanguages() {
        return $this->hasMany(Language::className(), ['id' => 'language_id'])->viaTable('product_photo_translation', ['product_photo_id' => 'id']);
    }

    /**
     * @inheritdoc
     * @return \common\models\aq\ProductPhotoQuery the active query used by this AR class.
     */
    public static function find() {
        return new \common\models\aq\ProductPhotoQuery(get_called_class());
    }
    
    public function behaviors() {
    	return [
    			[
    					'class' => TimestampBehavior::className(),
    					'createdAtAttribute' => 'date_creation',
    					'updatedAtAttribute' => false,
    					'value' => new Expression('NOW()'),
    			],
    	];
    }
}
