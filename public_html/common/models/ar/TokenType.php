<?php

namespace common\models\ar;

use Yii;

/**
 * This is the model class for table "token_type".
 *
 * @property integer $id
 * @property string $symbol
 *
 * @property Token[] $tokens
 */
class TokenType extends \yii\db\ActiveRecord {

    /**
     * @inheritdoc
     */
    public static function tableName() {
        return 'token_type';
    }

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
				[['symbol'], 'required'],
				[['symbol'], 'string', 'max' => 50]
		];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return [
            	'id' => Yii::t('main', 'ID'),
            	'symbol' => Yii::t('main', 'Symbol'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTokens() {
        return $this->hasMany(Token::className(), ['token_type_id' => 'id']);
    }

    /**
     * @inheritdoc
     * @return \common\models\aq\TokenTypeQuery the active query used by this AR class.
     */
    public static function find() {
        return new \common\models\aq\TokenTypeQuery(get_called_class());
    }
}
