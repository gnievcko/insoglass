<?php

namespace common\models\ar;

use Yii;

/**
 * This is the model class for table "product_status_reference".
 *
 * @property integer $id
 * @property integer $product_status_id
 * @property string $symbol
 * @property string $name_table
 * @property string $name_column
 * @property string $name_expression
 * @property string $url_details
 * @property integer $is_required
 * @property string $field_type
 *
 * @property ProductStatus $productStatus
 * @property ProductStatusReferenceTranslation[] $productStatusReferenceTranslations
 * @property Language[] $languages
 * @property WarehouseProductStatusHistoryData[] $warehouseProductStatusHistoryDatas
 * @property WarehouseProductStatusHistory[] $histories
 */
class ProductStatusReference extends \yii\db\ActiveRecord {

    /**
     * @inheritdoc
     */
    public static function tableName() {
        return 'product_status_reference';
    }

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
				[['product_status_id', 'symbol'/*, 'name_table', 'name_column', 'name_expression'*/], 'required'],
				[['product_status_id', 'is_required'], 'integer'],
				[['symbol', 'name_table', 'name_column'], 'string', 'max' => 64],
        		[['name_expression', 'url_details'], 'string', 'max' => 512],
        		[['field_type'], 'string', 'max' => 32],
				[['product_status_id'], 'exist', 'skipOnError' => true, 'targetClass' => ProductStatus::className(), 'targetAttribute' => ['product_status_id' => 'id']]
		];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return [
            	'id' => Yii::t('main', 'ID'),
            	'product_status_id' => Yii::t('main', 'Product Status ID'),
            	'symbol' => Yii::t('main', 'Symbol'),
            	'name_table' => Yii::t('main', 'Name table'),
            	'name_column' => Yii::t('main', 'Name column'),
        		'name_expression' => Yii::t('main', 'Name expression'),
        		'url_details' => Yii::t('main', 'Url details'),
            	'is_required' => Yii::t('main', 'Is required'),
        		'productStatusSymbol' => Yii::t('main', 'Status symbol'),
        		'field_type' => Yii::t('main', 'Field type')
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProductStatus() {
        return $this->hasOne(ProductStatus::className(), ['id' => 'product_status_id']);
    }

    public function getProductStatusSymbol() {
    	return $this->productStatus->getSymbolTranslation();
    }
    
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProductStatusReferenceTranslations() {
        return $this->hasMany(ProductStatusReferenceTranslation::className(), ['reference_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLanguages() {
        return $this->hasMany(Language::className(), ['id' => 'language_id'])->viaTable('product_status_reference_translation', ['reference_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getWarehouseProductStatusHistoryDatas() {
        return $this->hasMany(WarehouseProductStatusHistoryData::className(), ['product_status_reference_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getHistories() {
        return $this->hasMany(WarehouseProductStatusHistory::className(), ['id' => 'history_id'])->viaTable('warehouse_product_status_history_data', ['product_status_reference_id' => 'id']);
    }

    /**
     * @inheritdoc
     * @return \common\models\aq\ProductStatusReferenceQuery the active query used by this AR class.
     */
    public static function find() {
        return new \common\models\aq\ProductStatusReferenceQuery(get_called_class());
    }
}
