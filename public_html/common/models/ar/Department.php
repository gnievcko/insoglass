<?php

namespace common\models\ar;

use Yii;

/**
 * This is the model class for table "department".
 *
 * @property integer $id
 * @property integer $department_type_id
 * @property string $symbol
 *
 * @property DepartmentType $departmentType
 * @property DepartmentActivity[] $departmentActivities
 * @property Activity[] $activities
 * @property DepartmentTranslation[] $departmentTranslations
 * @property Language[] $languages
 * @property UserDepartment[] $userDepartments
 * @property User[] $users
 * @property Workstation[] $workstations
 */
class Department extends \yii\db\ActiveRecord {

    /**
     * @inheritdoc
     */
    public static function tableName() {
        return 'department';
    }

    public function getTranslation() {
        $translations = $this->departmentTranslations;
        foreach($translations as $t) {
            if($t->language->symbol === Yii::$app->language) {
                return $t;
            }
        }
        return null;
    }



    /**
     * @inheritdoc
     */
    public function rules() {
        return [
				[['department_type_id', 'symbol'], 'required'],
				[['department_type_id'], 'integer'],
				[['symbol'], 'string', 'max' => 32],
				[['symbol'], 'unique'],
				[['department_type_id'], 'exist', 'skipOnError' => true, 'targetClass' => DepartmentType::className(), 'targetAttribute' => ['department_type_id' => 'id']]
		];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return [
            	'id' => Yii::t('main', 'ID'),
            	'department_type_id' => Yii::t('main', 'Department Type ID'),
            	'symbol' => Yii::t('main', 'Symbol'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDepartmentType() {
        return $this->hasOne(DepartmentType::className(), ['id' => 'department_type_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDepartmentActivities() {
        return $this->hasMany(DepartmentActivity::className(), ['deparment_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getActivities() {
        return $this->hasMany(Activity::className(), ['id' => 'activity_id'])->viaTable('department_activity', ['deparment_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDepartmentTranslations() {
        return $this->hasMany(DepartmentTranslation::className(), ['department_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLanguages() {
        return $this->hasMany(Language::className(), ['id' => 'language_id'])->viaTable('department_translation', ['department_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUserDepartments() {
        return $this->hasMany(UserDepartment::className(), ['deparment_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUsers() {
        return $this->hasMany(User::className(), ['id' => 'user_id'])->viaTable('user_department', ['deparment_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getWorkstations() {
        return $this->hasMany(Workstation::className(), ['department_id' => 'id']);
    }

    /**
     * @inheritdoc
     * @return \common\models\aq\DepartmentQuery the active query used by this AR class.
     */
    public static function find() {
        return new \common\models\aq\DepartmentQuery(get_called_class());
    }
}
