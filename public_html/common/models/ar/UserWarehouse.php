<?php

namespace common\models\ar;

use Yii;

/**
 * This is the model class for table "user_warehouse".
 *
 * @property integer $user_id
 * @property integer $warehouse_id
 *
 * @property Warehouse $warehouse
 * @property User $user
 */
class UserWarehouse extends \yii\db\ActiveRecord {

    /**
     * @inheritdoc
     */
    public static function tableName() {
        return 'user_warehouse';
    }

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
				[['user_id', 'warehouse_id'], 'required'],
				[['user_id', 'warehouse_id'], 'integer'],
				[['warehouse_id'], 'exist', 'skipOnError' => true, 'targetClass' => Warehouse::className(), 'targetAttribute' => ['warehouse_id' => 'id']],
				[['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['user_id' => 'id']],
        		[['user_id'], 'unique', 'targetAttribute' => ['user_id', 'warehouse_id'], 'comboNotUnique' => Yii::t('main', 'The chosen user is already assigned to this warehouse')],
		];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return [
            	'userName' => Yii::t('main', 'User'),
            	'warehouseName' => Yii::t('main', 'Warehouse'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getWarehouse() {
        return $this->hasOne(Warehouse::className(), ['id' => 'warehouse_id']);
    }
    
    public function getWarehouseName() {
    	return $this->warehouse->name;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser() {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }
    
    public function getUserName() {
    	return User::discoverNameForm($this->user);
    }
    
    /**
     * @inheritdoc
     * @return \common\models\aq\UserWarehouseQuery the active query used by this AR class.
     */
    public static function find() {
        return new \common\models\aq\UserWarehouseQuery(get_called_class());
    }
}
