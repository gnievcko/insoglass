<?php

namespace common\models\ar;

use Yii;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "application_cache".
 *
 * @property integer $id
 * @property string $date_last_invoice
 * @property integer $entity_id 
 * 
 * @property Entity $entity 
 */
class ApplicationCache extends \yii\db\ActiveRecord {

    /**
     * @inheritdoc
     */

    const SCENARIO_ADD = 'add';
    const SCENARIO_EDIT = 'edit';

    public function scenarios() {
        $scenarios = parent::scenarios();
        $scenarios[self::SCENARIO_ADD] = ['entity_id', 'date_last_invoice'];
        $scenarios[self::SCENARIO_EDIT] = ['entity_id', 'date_last_invoice'];
        return $scenarios;
    }

    public static function tableName() {
        return 'application_cache';
    }

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
                [['date_last_invoice'], 'required'],
        		[['entity_id'], 'validateEntity', 'skipOnEmpty' => false, 'on' => self::SCENARIO_ADD ],
		];
    }

    /**
     * @inheritdoc
     */

    public function validateEntity($attribute, $params){
        if(empty( $this->$attribute )){
            $this->$attribute = null;
        }
        $entitiesCount = $this->find()->where(['entity_id'=> $this->$attribute])->count();

        if($entitiesCount > 0) {
            $this->addError($attribute, Yii::t('main','Application cache of this economic entity already exists.'));
        }
    }

    public function attributeLabels() {
        return [
            	'id' => Yii::t('main', 'ID'),
            	'date_last_invoice' => Yii::t('main', 'Last invoice issue date'),
        		'entity_id' => Yii::t('main', 'Economic entity ID'),
                'entityName' => Yii::t('main','Economic entity name'),
        ];
    }

    /**
     * @inheritdoc
     * @return \common\models\aq\ApplicationCacheQuery the active query used by this AR class.
     */
    public function getEntity() {
    	return $this->hasOne(Entity::className(), ['id' => 'entity_id']);
    }

    public function getEntityName(){
        if (empty($this->entity)) return null;
        return $this->entity->name;
    }

    
    /**
     * @inheritdoc
     * @return \common\models\aq\ApplicationCacheQuery the active query used by this AR class.
     */
    public static function find() {
        return new \common\models\aq\ApplicationCacheQuery(get_called_class());
    }

    public function setAddScenario() {
        $this->setScenario(self::SCENARIO_ADD);
    }

    public function setEditScenario() {
        $this->setScenario(self::SCENARIO_EDIT);
    }

    public function isEditScenarioSet() {
        return $this->getScenario() == self::SCENARIO_EDIT;
    }
    
}
