<?php

namespace common\models\ar;

use Yii;

/**
 * This is the model class for table "slider_banner".
 *
 * @property integer $slider_id
 * @property integer $banner_id
 * @property integer $priority
 *
 * @property Banner $banner
 * @property Slider $slider
 */
class SliderBanner extends \yii\db\ActiveRecord {

    /**
     * @inheritdoc
     */
    public static function tableName() {
        return 'slider_banner';
    }

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
				[['slider_id', 'banner_id', 'priority'], 'required'],
				[['slider_id', 'banner_id', 'priority'], 'integer'],
				[['banner_id'], 'exist', 'skipOnError' => true, 'targetClass' => Banner::className(), 'targetAttribute' => ['banner_id' => 'id']],
				[['slider_id'], 'exist', 'skipOnError' => true, 'targetClass' => Slider::className(), 'targetAttribute' => ['slider_id' => 'id']]
		];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return [
            	'slider_id' => Yii::t('main', 'Slider ID'),
            	'banner_id' => Yii::t('main', 'Banner ID'),
            	'priority' => Yii::t('main', 'Priority'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBanner() {
        return $this->hasOne(Banner::className(), ['id' => 'banner_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSlider() {
        return $this->hasOne(Slider::className(), ['id' => 'slider_id']);
    }

    /**
     * @inheritdoc
     * @return \common\models\aq\SliderBannerQuery the active query used by this AR class.
     */
    public static function find() {
        return new \common\models\aq\SliderBannerQuery(get_called_class());
    }
}
