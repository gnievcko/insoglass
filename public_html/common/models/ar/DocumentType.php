<?php

namespace common\models\ar;

use Yii;

/**
 * This is the model class for table "document_type".
 *
 * @property integer $id
 * @property string $symbol
 * @property string $description
 * @property integer $document_section_header_id
 * @property integer $document_section_footer_id
 * @property integer $margin_top
 * @property integer $margin_bottom
 * @property integer $margin_left
 * @property integer $margin_right
 * @property string $format
 * @property string $orientation
 * @property integer $is_for_order 
 * @property integer $is_for_warehouse 
 * @property integer $is_for_report 
 *
 * @property DocumentSection $documentSectionFooter
 * @property DocumentSection $documentSectionHeader
 * @property DocumentTypeSection[] $documentTypeSections
 * @property DocumentSection[] $documentSections
 * @property DocumentTypeTranslation[] $documentTypeTranslations
 * @property Language[] $languages
 */
class DocumentType extends \yii\db\ActiveRecord {

    /**
     * @inheritdoc
     */
    public static function tableName() {
        return 'document_type';
    }

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
				[['symbol'], 'required'],
				[['description'], 'string'],
				[['document_section_header_id', 'document_section_footer_id', 'margin_top', 'margin_bottom', 'margin_left', 'margin_right', 'is_for_order', 'is_for_warehouse', 'is_for_report'], 'integer'],
				[['symbol'], 'string', 'max' => 30],
				[['format', 'orientation'], 'string', 'max' => 16],
				[['symbol'], 'unique'],
				[['document_section_footer_id'], 'exist', 'skipOnError' => true, 'targetClass' => DocumentSection::className(), 'targetAttribute' => ['document_section_footer_id' => 'id']],
				[['document_section_header_id'], 'exist', 'skipOnError' => true, 'targetClass' => DocumentSection::className(), 'targetAttribute' => ['document_section_header_id' => 'id']]
		];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return [
            	'id' => Yii::t('main', 'ID'),
            	'symbol' => Yii::t('main', 'Symbol'),
            	'description' => Yii::t('main', 'Description'),
            	'document_section_header_id' => Yii::t('main', 'Document Section Header ID'),
            	'document_section_footer_id' => Yii::t('main', 'Document Section Footer ID'),
            	'margin_top' => Yii::t('main', 'Margin Top'),
            	'margin_bottom' => Yii::t('main', 'Margin Bottom'),
            	'margin_left' => Yii::t('main', 'Margin Left'),
            	'margin_right' => Yii::t('main', 'Margin Right'),
            	'format' => Yii::t('main', 'Format'),
            	'orientation' => Yii::t('main', 'Orientation'),
        		'is_for_order' => Yii::t('main', 'Is For Order'),
        		'is_for_warehouse' => Yii::t('main', 'Is For Warehouse'),
        		'is_for_report' => Yii::t('main', 'Is For Report'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDocumentSectionFooter() {
        return $this->hasOne(DocumentSection::className(), ['id' => 'document_section_footer_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDocumentSectionHeader() {
        return $this->hasOne(DocumentSection::className(), ['id' => 'document_section_header_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDocumentTypeSections() {
        return $this->hasMany(DocumentTypeSection::className(), ['document_type_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDocumentSections() {
        return $this->hasMany(DocumentSection::className(), ['id' => 'document_section_id'])->viaTable('document_type_section', ['document_type_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDocumentTypeTranslations() {
        return $this->hasMany(DocumentTypeTranslation::className(), ['document_type_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLanguages() {
        return $this->hasMany(Language::className(), ['id' => 'language_id'])->viaTable('document_type_translation', ['document_type_id' => 'id']);
    }
    
    /**
    * @return strinh
    */
    public function getTranslation() {
        /*$text = $this->getDocumentTypeTranslations()->joinWith('language')->where(['language.symbol' => Yii::$app->language])->one();
        
        if(empty($text)) {
            return $this->symbol;
        } else {
            return $text->name;
        }*/
    	
    	$translations = $this->documentTypeTranslations;
    	if(empty($translations)) {
    		return null;
    	}
    	
    	foreach($translations as $t) {
    		if($t->language->symbol === Yii::$app->language) {
    			return $t;
    		}
    	}
    	
    	return $translations[0];
                
    }
    /**
     * @inheritdoc
     * @return \common\models\aq\DocumentTypeQuery the active query used by this AR class.
     */
    public static function find() {
        return new \common\models\aq\DocumentTypeQuery(get_called_class());
    }
}
