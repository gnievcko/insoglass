<?php

namespace common\models\ar;

use Yii;

/**
 * This is the model class for table "activity".
 *
 * @property integer $id
 * @property string $symbol
 * @property integer $effort_factor
 * @property integer $is_node
 * @property integer $is_qa_required
 *
 * @property ActivityProductAttribute[] $activityProductAttributes
 * @property ProductAttributeType[] $productAttributeTypes
 * @property ActivityTranslation[] $activityTranslations
 * @property Language[] $languages
 * @property DepartmentActivity[] $departmentActivities
 * @property Department[] $departments
 * @property ProductionPathStep[] $productionPathSteps
 */
class Activity extends \yii\db\ActiveRecord {

    /**
     * @inheritdoc
     */
    public static function tableName() {
        return 'activity';
    }

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
			[['symbol'], 'required'],
			[['effort_factor', 'is_node', 'is_qa_required'], 'integer'],
			[['symbol'], 'string', 'max' => 32],
			[['symbol'], 'unique']
		];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return [
            'id' => Yii::t('main', 'ID'),
            'symbol' => Yii::t('main', 'Symbol'),
            'effort_factor' => Yii::t('main', 'Effort Factor'),
            'is_node' => Yii::t('main', 'Is Node'),
            'is_qa_required' => Yii::t('main', 'Is Qa Required'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getActivityProductAttributes() {
        return $this->hasMany(ActivityProductAttribute::className(), ['activity_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProductAttributeTypes() {
        return $this->hasMany(ProductAttributeType::className(), ['id' => 'product_attribute_type_id'])->viaTable('activity_product_attribute', ['activity_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getActivityTranslations() {
        return $this->hasMany(ActivityTranslation::className(), ['activity_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLanguages() {
        return $this->hasMany(Language::className(), ['id' => 'language_id'])->viaTable('activity_translation', ['activity_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDepartmentActivities() {
        return $this->hasMany(DepartmentActivity::className(), ['activity_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDepartments() {
        return $this->hasMany(Department::className(), ['id' => 'department_id'])->viaTable('department_activity', ['activity_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProductionPathSteps() {
        return $this->hasMany(ProductionPathStep::className(), ['activity_id' => 'id']);
    }
    
    public function getTranslation() {
        $translations = $this->activityTranslations;
        if(empty($translations)) {
            return null;
        }
         
        foreach($translations as $t) {
            if($t->language->symbol === Yii::$app->language) {
                return $t;
            }
        }
         
        return $translations[0];
    }

    /**
     * @inheritdoc
     * @return \common\models\aq\ActivityQuery the active query used by this AR class.
     */
    public static function find() {
        return new \common\models\aq\ActivityQuery(get_called_class());
    }
}
