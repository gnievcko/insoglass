<?php

namespace common\models\ar;

use Yii;

/**
 * This is the model class for table "city".
 *
 * @property integer $id
 * @property integer $province_id
 * @property string $name
 * @property string $zip_code
 *
 * @property Address[] $addresses
 * @property Province $province
 */
class City extends \yii\db\ActiveRecord {

    /**
     * @inheritdoc
     */
    public static function tableName() {
        return 'city';
    }

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
				[['province_id'], 'integer'],
				[['name', 'zip_code'], 'required'],
				[['name'], 'string', 'max' => 64],
				[['zip_code'], 'string', 'max' => 10],
				[['province_id'], 'exist', 'skipOnError' => true, 'targetClass' => Province::className(), 'targetAttribute' => ['province_id' => 'id']]
		];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return [
            	'id' => Yii::t('main', 'ID'),
            	'province_id' => Yii::t('main', 'Province ID'),
            	'name' => Yii::t('main', 'Name'),
            	'zip_code' => Yii::t('main', 'Zip code'),
        		'provinceName' => Yii::t('main', 'Province'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAddresses() {
        return $this->hasMany(Address::className(), ['city_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProvince() {
        return $this->hasOne(Province::className(), ['id' => 'province_id']);
    }
    
    public function getProvinceName() {
    	return !empty($this->province) ? $this->province->name : null;
    }

    /**
     * @inheritdoc
     * @return \common\models\aq\CityQuery the active query used by this AR class.
     */
    public static function find() {
        return new \common\models\aq\CityQuery(get_called_class());
    }
}
