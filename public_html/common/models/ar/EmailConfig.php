<?php

namespace common\models\ar;

use Yii;

/**
 * This is the model class for table "email_config".
 *
 * @property integer $id
 * @property string $email
 * @property string $password
 * @property integer $port
 * @property string $protocol
 * @property string $host
 * @property integer $starttls
 * @property integer $smtp_auth
 * @property string $noreply_email
 *
 */
class EmailConfig extends \yii\db\ActiveRecord {

    /**
     * @inheritdoc
     */
    public static function tableName() {
        return 'email_config';
    }

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
				[['email', 'password', 'port', 'protocol', 'host'], 'required'],
				[['port', 'starttls', 'smtp_auth'], 'integer'],
				[['email', 'host', 'noreply_email'], 'string', 'max' => 64],
				[['password', 'protocol'], 'string', 'max' => 32],
        		[['email', 'noreply_email'], 'email'],
				[['email', 'host'], 'unique', 'targetAttribute' => ['email', 'host'], 'message' => 'The combination of Email and Host has already been taken.']
		];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return [
            	'id' => Yii::t('main', 'ID'),
            	'email' => Yii::t('main', 'E-mail address'),
            	'password' => Yii::t('main', 'Password'),
            	'port' => Yii::t('main', 'Port'),
            	'protocol' => Yii::t('main', 'Protocol'),
            	'host' => Yii::t('main', 'Host'),
            	'starttls' => Yii::t('main', 'Encoding (SSL)'),
            	'smtp_auth' => Yii::t('main', 'SMTP Authorization'),
            	'noreply_email' => Yii::t('main', 'No-reply e-mail address'),
        ];
    }

    /**
     * @inheritdoc
     * @return \common\models\aq\EmailConfigQuery the active query used by this AR class.
     */
    public static function find() {
        return new \common\models\aq\EmailConfigQuery(get_called_class());
    }
}
