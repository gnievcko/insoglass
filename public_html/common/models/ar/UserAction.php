<?php

namespace common\models\ar;

use Yii;
use frontend\helpers\UserHelper;

/**
 * This is the model class for table "user_action".
 *
 * @property integer $id
 * @property integer $user_id
 * @property string $date_action
 *
 * @property User $user
 */
class UserAction extends \yii\db\ActiveRecord {

    /**
     * @inheritdoc
     */
    public static function tableName() {
        return 'user_action';
    }

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
				[['user_id'], 'required'],
				[['user_id'], 'integer'],
				[['date_action'], 'safe'],
				[['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['user_id' => 'id']]
		];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return [
            	'id' => Yii::t('main', 'ID'),
            	'userName' => Yii::t('main', 'User'),
            	'date_action' => Yii::t('main', 'Date'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLanguage() {
        return $this->hasOne(Language::className(), ['id' => 'language_id']);
    }
    
    public function getUser() {
    	return $this->hasOne(User::className(), ['id' => 'user_id']);
    }
    
    public function getUserName() {
    	return UserHelper::getPrettyUserName($this->user->email, $this->user->first_name, $this->user->last_name);
    }

    /**
     * @inheritdoc
     * @return \common\models\aq\UserActionQuery the active query used by this AR class.
     */
    public static function find() {
        return new \common\models\aq\UserActionQuery(get_called_class());
    }
}
