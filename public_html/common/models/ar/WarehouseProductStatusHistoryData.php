<?php

namespace common\models\ar;

use Yii;

/**
 * This is the model class for table "warehouse_product_status_history_data".
 *
 * @property integer $history_id
 * @property integer $product_status_reference_id
 * @property integer $referenced_object_id
 * @property string $custom_value 
 *
 * @property ProductStatusReference $productStatusReference
 * @property WarehouseProductStatusHistory $history
 */
class WarehouseProductStatusHistoryData extends \yii\db\ActiveRecord {

    /**
     * @inheritdoc
     */
    public static function tableName() {
        return 'warehouse_product_status_history_data';
    }

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
				[['history_id', 'product_status_reference_id'], 'required'],
				[['history_id', 'product_status_reference_id', 'referenced_object_id'], 'integer'],
        		[['custom_value'], 'string', 'max' => 2048],
				[['product_status_reference_id'], 'exist', 'skipOnError' => true, 'targetClass' => ProductStatusReference::className(), 'targetAttribute' => ['product_status_reference_id' => 'id']],
				[['history_id'], 'exist', 'skipOnError' => true, 'targetClass' => WarehouseProductStatusHistory::className(), 'targetAttribute' => ['history_id' => 'id']]
		];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return [
            	'history_id' => Yii::t('main', 'History ID'),
            	'product_status_reference_id' => Yii::t('main', 'Product Status Reference ID'),
            	'referenced_object_id' => Yii::t('main', 'Referenced Object ID'),
        		'custom_value' => Yii::t('main', 'Custom Value'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProductStatusReference() {
        return $this->hasOne(ProductStatusReference::className(), ['id' => 'product_status_reference_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getHistory() {
        return $this->hasOne(WarehouseProductStatusHistory::className(), ['id' => 'history_id']);
    }

    /**
     * @inheritdoc
     * @return \common\models\aq\WarehouseProductStatusHistoryDataQuery the active query used by this AR class.
     */
    public static function find() {
        return new \common\models\aq\WarehouseProductStatusHistoryDataQuery(get_called_class());
    }
}
