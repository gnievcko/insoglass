<?php

namespace common\models\ar;

use Yii;

/**
 * This is the model class for table "email_type".
 *
 * @property integer $id
 * @property integer $email_config_id
 * @property string $symbol
 * @property string $tags
 * @property integer $is_archived
 * @property integer $is_active
 *
 * @property EmailTemplate[] $emailTemplates
 * @property Language[] $languages
 */
class EmailType extends \yii\db\ActiveRecord {

    /**
     * @inheritdoc
     */
    public static function tableName() {
        return 'email_type';
    }

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
				[['symbol', 'is_enabled'], 'required'],
				[['is_archived', 'is_active'], 'integer'],
				[['symbol'], 'string', 'max' => 32],
        		[['tags'], 'string', 'max' => 128],
				[['symbol'], 'unique'],
		];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return [
            	'id' => Yii::t('main', 'ID'),
            	'symbol' => Yii::t('main', 'Symbol'),
        		'tags' => Yii::t('main', 'Tags to fill in'),
            	'is_archived' => Yii::t('main', 'Is archived'),
            	'is_active' => Yii::t('main', 'Is active'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getEmailTemplates() {
        return $this->hasMany(EmailTemplate::className(), ['email_type_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLanguages() {
        return $this->hasMany(Language::className(), ['id' => 'language_id'])->viaTable('email_template', ['email_type_id' => 'id']);
    }

    /**
     * @inheritdoc
     * @return \common\models\aq\EmailTypeQuery the active query used by this AR class.
     */
    public static function find() {
        return new \common\models\aq\EmailTypeQuery(get_called_class());
    }
    
    public function getBestTemplate() {
    	$templates = $this->emailTemplates;
    
    	foreach($templates as $template) {
    		if($template->language->symbol === Yii::$app->language) {
    			return $template;
    		}
    	}
    
    	if(!empty($templates)) {
    		return $templates[0];
    	}
    
    	return null;
    }
    
    public function getFooterTags() {
    	// TODO: Do przemyślenia, czy będzie potrzebne
    	/*$baseUrl = 'http://ihaho.com.pl/';
    	$hostInfo = 'http://ihaho.com.pl';
    	if(Yii::$app->id !== 'app-console') {
    		$baseUrl = Yii::$app->getRequest()->getFrontendBaseUrl();
    		$hostInfo = Yii::$app->getRequest()->getHostInfo();
    	}
    
    	return [
    			'{baseAbsoluteUrl}' => $baseUrl,
    			'{contacts}' => Yii::t('web', 'Contacts'),
    			'{name}' => Yii::$app->params['owner']['name'],
    			'{address}' => Yii::$app->params['owner']['address'],
    			'{email}' => Yii::$app->params['owner']['email'],
    			'{phone}' => Yii::$app->params['owner']['phone'],
    			'{www}' => $hostInfo,
    			'{vatin}' => Yii::$app->params['owner']['vatin'],
    			'{regon}' => Yii::$app->params['owner']['regon'],
    			'{bank}' => Yii::$app->params['owner']['bank'],
    			'{mobilelabel}' => Yii::t('main', 'Phone'),
    			'{emaillabel}' => Yii::t('web', 'E-mail'),
    			'{vatinlabel}' => Yii::t('web', 'VATIN'),
    			'{regonlabel}' => 'REGON',
    			'{banklabel}' => 'mBank',
    	];*/
    	return [];
    }
    
    public function replacePlaceholders($content, $overwritedTags) {
    	foreach($overwritedTags as $k => $v) {
    		$content = str_replace($k, $v, $content);
    	}
    
    	$content = strtr($content, $this->getFooterTags());
    
    	return $content;
    }
}
