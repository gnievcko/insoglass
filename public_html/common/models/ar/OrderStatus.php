<?php

namespace common\models\ar;

use Yii;
use common\helpers\StringHelper;

/**
 * This is the model class for table "order_status".
 *
 * @property integer $id
 * @property string $symbol
 * @property integer $is_order
 * @property integer $is_visible
 * @property integer $is_visible_for_client
 *
 * @property OrderHistory[] $orderHistories
 * @property OrderStatusTransition[] $orderStatusTransitions
 * @property OrderStatusTransition[] $orderStatusTransitions0
 * @property OrderStatus[] $orderStatusSuccessors
 * @property OrderStatus[] $orderStatusPredecessors
 * @property OrderStatusTranslation[] $orderStatusTranslations
 * @property Language[] $languages
 */
class OrderStatus extends \yii\db\ActiveRecord {

    /**
     * @inheritdoc
     */
    public static function tableName() {
        return 'order_status';
    }

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
				[['symbol', 'is_order', 'is_visible', 'is_visible_for_client'], 'required'],
				[['is_order', 'is_visible', 'is_visible_for_client', 'is_terminal'], 'integer'],
				[['symbol'], 'string', 'max' => 32],
				[['symbol'], 'unique']
		];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return [
            	'id' => Yii::t('main', 'ID'),
            	'symbol' => Yii::t('main', 'Symbol'),
            	'is_order' => StringHelper::translateOrderToOffer('main', 'Is order'),
            	'is_visible' => Yii::t('main', 'Is visible'),
            	'is_visible_for_client' => Yii::t('main', 'Is visible for client'),
            	'is_terminal' => Yii::t('main', 'Is terminal'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrderHistories() {
        return $this->hasMany(OrderHistory::className(), ['order_status_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrderStatusTransitions() {
        return $this->hasMany(OrderStatusTransition::className(), ['order_status_predecessor_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrderStatusTransitions0() {
        return $this->hasMany(OrderStatusTransition::className(), ['order_status_successor_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrderStatusSuccessors() {
        return $this->hasMany(OrderStatus::className(), ['id' => 'order_status_successor_id'])->viaTable('order_status_transition', ['order_status_predecessor_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrderStatusPredecessors() {
        return $this->hasMany(OrderStatus::className(), ['id' => 'order_status_predecessor_id'])->viaTable('order_status_transition', ['order_status_successor_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrderStatusTranslations() {
        return $this->hasMany(OrderStatusTranslation::className(), ['order_status_id' => 'id']);
    }

    public function getOrderStatusTranslation() {
    	$translations = $this->orderStatusTranslations;
    	if(empty($translations)) {
    		return null;
    	}
    	 
    	foreach($translations as $t) {
    		if($t->language->symbol === Yii::$app->language) {
    			return $t;
    		}
    	}
    	 
    	return $translations[0];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLanguages() {
        return $this->hasMany(Language::className(), ['id' => 'language_id'])->viaTable('order_status_translation', ['order_status_id' => 'id']);
    }

    /**
     * @inheritdoc
     * @return \common\models\aq\OrderStatusQuery the active query used by this AR class.
     */
    public static function find() {
        return new \common\models\aq\OrderStatusQuery(get_called_class());
    }
}
