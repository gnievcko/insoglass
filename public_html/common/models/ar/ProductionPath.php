<?php

namespace common\models\ar;

use Yii;

/**
 * This is the model class for table "production_path".
 *
 * @property integer $id
 * @property string $symbol
 * @property string $topological_order
 * @property integer $is_default
 * @property integer $is_active
 *
 * @property OfferedProduct[] $offeredProducts
 * @property OrderOfferedProduct[] $orderOfferedProducts
 * @property ProductionPathStep[] $productionPathSteps
 * @property ProductionPathTranslation[] $productionPathTranslations
 * @property Language[] $languages
 */
class ProductionPath extends \yii\db\ActiveRecord {

    /**
     * @inheritdoc
     */
    public static function tableName() {
        return 'production_path';
    }

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
				[['symbol'], 'required'],
				[['is_default', 'is_active'], 'integer'],
				[['symbol'], 'string', 'max' => 32],
				[['topological_order'], 'string', 'max' => 1024],
				[['symbol'], 'unique']
		];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return [
            	'id' => Yii::t('main', 'ID'),
            	'symbol' => Yii::t('main', 'Symbol'),
            	'topological_order' => Yii::t('main', 'Topological Order'),
            	'is_default' => Yii::t('main', 'Is Default'),
                'is_active' => Yii::t('main', 'Is Active'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOfferedProducts() {
        return $this->hasMany(OfferedProduct::className(), ['production_path_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrderOfferedProducts() {
        return $this->hasMany(OrderOfferedProduct::className(), ['production_path_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProductionPathSteps() {
        return $this->hasMany(ProductionPathStep::className(), ['production_path_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProductionPathTranslations() {
        return $this->hasMany(ProductionPathTranslation::className(), ['production_path_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLanguages() {
        return $this->hasMany(Language::className(), ['id' => 'language_id'])->viaTable('production_path_translation', ['production_path_id' => 'id']);
    }

    /**
     * @inheritdoc
     * @return \common\models\aq\ProductionPathQuery the active query used by this AR class.
     */
    public static function find() {
        return new \common\models\aq\ProductionPathQuery(get_called_class());
    }
}
