<?php

namespace common\models\ar;

use Yii;

/**
 * This is the model class for table "configuration".
 *
 * @property integer $id
 * @property string $path_mysqldump
 * @property string $path_mongodbdump
 * @property string $path_dbbackups
 * @property integer $days_dbbackups_expiration
 */
class Configuration extends \yii\db\ActiveRecord {

    /**
     * @inheritdoc
     */
    public static function tableName() {
        return 'configuration';
    }

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
				[['days_dbbackups_expiration'], 'integer'],
				[['path_mysqldump', 'path_mongodbdump', 'path_dbbackups'], 'string', 'max' => 1024]
		];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return [
            	'id' => Yii::t('main', 'ID'),
            	'path_mysqldump' => Yii::t('main', 'Path to Mysqldump'),
            	'path_mongodbdump' => Yii::t('main', 'Path to Mongodbdump'),
            	'path_dbbackups' => Yii::t('main', 'Path to Dbbackups'),
            	'days_dbbackups_expiration' => Yii::t('main', 'Database backups expiration time (days)'),
        ];
    }

    /**
     * @inheritdoc
     * @return \common\models\aq\ConfigurationQuery the active query used by this AR class.
     */
    public static function find() {
        return new \common\models\aq\ConfigurationQuery(get_called_class());
    }
}
