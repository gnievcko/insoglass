<?php

namespace common\models\ar;

use Yii;

/**
 * This is the model class for table "muntin_translation".
 *
 * @property integer $muntin_id
 * @property integer $language_id
 * @property string $name
 *
 * @property Language $language
 * @property Muntin $muntin
 */
class MuntinTranslation extends \yii\db\ActiveRecord {

    /**
     * @inheritdoc
     */
    public static function tableName() {
        return 'muntin_translation';
    }

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
			[['muntin_id', 'language_id', 'name'], 'required'],
			[['muntin_id', 'language_id'], 'integer'],
			[['name'], 'string', 'max' => 256],
			[['language_id'], 'exist', 'skipOnError' => true, 'targetClass' => Language::className(), 'targetAttribute' => ['language_id' => 'id']],
			[['muntin_id'], 'exist', 'skipOnError' => true, 'targetClass' => Muntin::className(), 'targetAttribute' => ['muntin_id' => 'id']],
            [['muntin_id'], 'unique', 'targetAttribute' => ['muntin_id', 'language_id'], 'comboNotUnique' => Yii::t('main', 'The chosen muntin-language pair has a translation already.')],
		];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return [
            'muntin_id' => Yii::t('main', 'Muntin ID'),
            'language_id' => Yii::t('main', 'Language ID'),
            'name' => Yii::t('main', 'Name'),
            'muntinSymbol' => Yii::t('main', 'Muntin'),
            'languageName' => Yii::t('main', 'Language'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLanguage() {
        return $this->hasOne(Language::className(), ['id' => 'language_id']);
    }

    public function getLanguageName() {
        return $this->language->name;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMuntin() {
        return $this->hasOne(Muntin::className(), ['id' => 'muntin_id']);
    }

    public function getMuntinSymbol() {
        return $this->muntin->symbol;
    }

    /**
     * @inheritdoc
     * @return \common\models\aq\MuntinTranslationQuery the active query used by this AR class.
     */
    public static function find() {
        return new \common\models\aq\MuntinTranslationQuery(get_called_class());
    }
}
