<?php

namespace common\models\ar;

use Yii;

/**
 * This is the model class for table "contract_type".
 *
 * @property integer $id
 * @property string $symbol
 * @property integer $is_active
 *
 * @property Company[] $companies
 * @property ContractTypeTranslation[] $contractTypeTranslations
 * @property Language[] $languages
 */
class ContractType extends \yii\db\ActiveRecord {

    /**
     * @inheritdoc
     */
    public static function tableName() {
        return 'contract_type';
    }

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
				[['symbol', 'is_active'], 'required'],
				[['is_active'], 'integer'],
				[['symbol'], 'string', 'max' => 32]
		];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return [
            	'id' => Yii::t('main', 'ID'),
            	'symbol' => Yii::t('main', 'Symbol'),
            	'is_active' => Yii::t('main', 'Is active'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getContractTypeTranslations() {
        return $this->hasMany(ContractTypeTranslation::className(), ['contract_type_id' => 'id']);
    }
    
    public function getTranslation() {
    	$translations = $this->getContractTypeTranslations()->all();
    	
    	if(empty($translations)) {
    		return null;
    	}
    	
    	foreach($translations as $t) {
    		if($t->language->symbol === Yii::$app->language) {
    			return $t;
    		}
    	}
    	
    	return $translations[0];
    }    

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLanguages() {
        return $this->hasMany(Language::className(), ['id' => 'language_id'])->viaTable('contract_type_translation', ['contract_type_id' => 'id']);
    }

    /**
     * @inheritdoc
     * @return \common\models\aq\ContractTypeQuery the active query used by this AR class.
     */
    public static function find() {
        return new \common\models\aq\ContractTypeQuery(get_called_class());
    }
}
