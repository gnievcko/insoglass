<?php

namespace common\models\ar;

use Yii;

/**
 * This is the model class for table "product_requirement".
 *
 * @property integer $id
 * @property integer $product_id
 * @property integer $product_required_id
 * @property integer $count
 *
 * @property Product $productRequired
 * @property Product $product
 */
class ProductRequirement extends \yii\db\ActiveRecord {

    /**
     * @inheritdoc
     */
    public static function tableName() {
        return 'product_requirement';
    }

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
				[['product_id', 'product_required_id', 'count'], 'required'],
				[['product_id', 'product_required_id', 'count'], 'integer'],
				[['product_required_id'], 'exist', 'skipOnError' => true, 'targetClass' => Product::className(), 'targetAttribute' => ['product_required_id' => 'id']],
				[['product_id'], 'exist', 'skipOnError' => true, 'targetClass' => Product::className(), 'targetAttribute' => ['product_id' => 'id']]
		];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return [
            	'id' => Yii::t('main', 'ID'),
            	'product_id' => Yii::t('main', 'Product'),
            	'product_required_id' => Yii::t('main', 'Product Required ID'),
            	'count' => Yii::t('main', 'Count'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProductRequired() {
        return $this->hasOne(Product::className(), ['id' => 'product_required_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProduct() {
        return $this->hasOne(Product::className(), ['id' => 'product_id']);
    }

    /**
     * @inheritdoc
     * @return \common\models\aq\ProductRequirementQuery the active query used by this AR class.
     */
    public static function find() {
        return new \common\models\aq\ProductRequirementQuery(get_called_class());
    }
}
