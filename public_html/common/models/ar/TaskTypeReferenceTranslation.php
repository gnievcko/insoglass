<?php

namespace common\models\ar;

use Yii;

/**
 * This is the model class for table "task_type_reference_translation".
 *
 * @property integer $reference_id
 * @property integer $language_id
 * @property string $name
 *
 * @property Language $language
 * @property TaskTypeReference $reference
 */
class TaskTypeReferenceTranslation extends \yii\db\ActiveRecord {

    /**
     * @inheritdoc
     */
    public static function tableName() {
        return 'task_type_reference_translation';
    }

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
				[['reference_id', 'language_id', 'name'], 'required'],
				[['reference_id', 'language_id'], 'integer'],
        		[['reference_id'], 'unique', 'targetAttribute' => ['reference_id', 'language_id'], 'comboNotUnique' => Yii::t('main', 'The chosen task type reference-language pair has a translation already.')],
				[['name'], 'string', 'max' => 512],
				[['language_id'], 'exist', 'skipOnError' => true, 'targetClass' => Language::className(), 'targetAttribute' => ['language_id' => 'id']],
				[['reference_id'], 'exist', 'skipOnError' => true, 'targetClass' => TaskTypeReference::className(), 'targetAttribute' => ['reference_id' => 'id']]
		];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return [
            	'referenceSymbol' => Yii::t('main', 'Task type reference'),
            	'languageName' => Yii::t('main', 'Language'),
            	'name' => Yii::t('main', 'Name'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLanguage() {
        return $this->hasOne(Language::className(), ['id' => 'language_id']);
    }
    
    public function getLanguageName() {
    	return $this->language->name;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getReference() {
        return $this->hasOne(TaskTypeReference::className(), ['id' => 'reference_id']);
    }
    public function getReferenceSymbol() {
    	return $this->reference->symbol;
    }

    /**
     * @inheritdoc
     * @return \common\models\aq\TaskTypeReferenceTranslationQuery the active query used by this AR class.
     */
    public static function find() {
        return new \common\models\aq\TaskTypeReferenceTranslationQuery(get_called_class());
    }
}
