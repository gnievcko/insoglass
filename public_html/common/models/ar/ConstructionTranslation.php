<?php

namespace common\models\ar;

use Yii;

/**
 * This is the model class for table "construction_translation".
 *
 * @property integer $construction_id
 * @property integer $language_id
 * @property string $name
 *
 * @property Construction $construction
 * @property Language $language
 */
class ConstructionTranslation extends \yii\db\ActiveRecord {

    /**
     * @inheritdoc
     */
    public static function tableName() {
        return 'construction_translation';
    }

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
			[['construction_id', 'language_id', 'name'], 'required'],
			[['construction_id', 'language_id'], 'integer'],
			[['name'], 'string', 'max' => 256],
			[['construction_id'], 'exist', 'skipOnError' => true, 'targetClass' => Construction::className(), 'targetAttribute' => ['construction_id' => 'id']],
            [['language_id'], 'exist', 'skipOnError' => true, 'targetClass' => Language::className(), 'targetAttribute' => ['language_id' => 'id']],
            [['construction_id'], 'unique', 'targetAttribute' => ['construction_id', 'language_id'], 'comboNotUnique' => Yii::t('main', 'The chosen construction-language pair has a translation already.')],
		];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return [
            'construction_id' => Yii::t('main', 'Construction ID'),
            'language_id' => Yii::t('main', 'Language ID'),
            'name' => Yii::t('main', 'Name'),
            'constructionSymbol' => Yii::t('main', 'Construction'),
            'languageName' => Yii::t('main', 'Language'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getConstruction() {
        return $this->hasOne(Construction::className(), ['id' => 'construction_id']);
    }

    public function getConstructionSymbol() {
        return $this->construction->symbol;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLanguage() {
        return $this->hasOne(Language::className(), ['id' => 'language_id']);
    }

    public function getLanguageName() {
        return $this->language->name;
    }

    /**
     * @inheritdoc
     * @return \common\models\aq\ConstructionTranslationQuery the active query used by this AR class.
     */
    public static function find() {
        return new \common\models\aq\ConstructionTranslationQuery(get_called_class());
    }
}
