<?php

namespace common\models\ar;

use Yii;

/**
 * This is the model class for table "offered_product_requirement".
 *
 * @property integer $id
 * @property integer $offered_product_id
 * @property integer $product_required_id
 * @property integer $count
 * @property string $unit
 * @property string $note
 *
 * @property OfferedProduct $offeredProduct
 * @property Product $productRequired
 */
class OfferedProductRequirement extends \yii\db\ActiveRecord {

    /**
     * @inheritdoc
     */
    public static function tableName() {
        return 'offered_product_requirement';
    }

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
			[['offered_product_id', 'product_required_id', 'count'], 'required'],
			[['offered_product_id', 'product_required_id', 'count'], 'integer'],
			[['unit'], 'string', 'max' => 10],
			[['note'], 'string', 'max' => 1024],
			[['offered_product_id'], 'exist', 'skipOnError' => true, 'targetClass' => OfferedProduct::className(), 'targetAttribute' => ['offered_product_id' => 'id']],
			[['product_required_id'], 'exist', 'skipOnError' => true, 'targetClass' => Product::className(), 'targetAttribute' => ['product_required_id' => 'id']]
		];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return [
            'id' => Yii::t('main', 'ID'),
            'offered_product_id' => Yii::t('main', 'Offered Product ID'),
            'product_required_id' => Yii::t('main', 'Product Required ID'),
            'count' => Yii::t('main', 'Count'),
            'unit' => Yii::t('main', 'Unit'),
            'note' => Yii::t('main', 'Note'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOfferedProduct() {
        return $this->hasOne(OfferedProduct::className(), ['id' => 'offered_product_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProductRequired() {
        return $this->hasOne(Product::className(), ['id' => 'product_required_id']);
    }

    /**
     * @inheritdoc
     * @return \common\models\aq\OfferedProductRequirementQuery the active query used by this AR class.
     */
    public static function find() {
        return new \common\models\aq\OfferedProductRequirementQuery(get_called_class());
    }
}
