<?php

namespace common\models\ar;

use Yii;

/**
 * This is the model class for table "shape_translation".
 *
 * @property integer $shape_id
 * @property integer $language_id
 * @property string $name
 *
 * @property Language $language
 * @property Shape $shape
 */
class ShapeTranslation extends \yii\db\ActiveRecord {

    /**
     * @inheritdoc
     */
    public static function tableName() {
        return 'shape_translation';
    }

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
			[['shape_id', 'language_id', 'name'], 'required'],
			[['shape_id', 'language_id'], 'integer'],
			[['name'], 'string', 'max' => 256],
            [['shape_id'], 'exist', 'skipOnError' => true, 'targetClass' => Shape::className(), 'targetAttribute' => ['shape_id' => 'id']],
            [['language_id'], 'exist', 'skipOnError' => true, 'targetClass' => Language::className(), 'targetAttribute' => ['language_id' => 'id']],
            [['shape_id'], 'unique', 'targetAttribute' => ['shape_id', 'language_id'], 'comboNotUnique' => Yii::t('main', 'The chosen shape-language pair has a translation already.')],
		];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return [
            'shape_id' => Yii::t('main', 'Shape ID'),
            'language_id' => Yii::t('main', 'Language ID'),
            'name' => Yii::t('main', 'Name'),
            'shapeSymbol' => Yii::t('main', 'Shape'),
            'languageName' => Yii::t('main', 'Language'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLanguage() {
        return $this->hasOne(Language::className(), ['id' => 'language_id']);
    }

    public function getLanguageName() {
        return $this->language->name;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getShape() {
        return $this->hasOne(Shape::className(), ['id' => 'shape_id']);
    }

    public function getShapeSymbol() {
        return $this->shape->symbol;
    }

    /**
     * @inheritdoc
     * @return \common\models\aq\ShapeTranslationQuery the active query used by this AR class.
     */
    public static function find() {
        return new \common\models\aq\ShapeTranslationQuery(get_called_class());
    }
}
