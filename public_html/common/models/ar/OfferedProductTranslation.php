<?php

namespace common\models\ar;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\Expression;

/**
 * This is the model class for table "offered_product_translation".
 *
 * @property integer $offered_product_id
 * @property integer $language_id
 * @property string $name
 * @property string $description
 * @property string $price
 * @property string $unit
 * @property integer $currency_id
 * @property integer $expected_time
 * @property integer $time_unit_id
 * @property string $date_creation
 *
 * @property TimeUnit $timeUnit
 * @property Currency $currency
 * @property Language $language
 * @property OfferedProduct $offeredProduct
 */
class OfferedProductTranslation extends \yii\db\ActiveRecord {

    /**
     * @inheritdoc
     */
    public static function tableName() {
        return 'offered_product_translation';
    }

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
				[['offered_product_id', 'language_id', 'name'], 'required'],
				[['offered_product_id', 'language_id', 'currency_id', 'expected_time', 'time_unit_id'], 'integer'],
				[['price'], 'number'],
				[['date_creation'], 'safe'],
				[['name'], 'string', 'max' => 512],
				[['description'], 'string', 'max' => 2048],
        		[['unit'], 'string', 'max' => 10],
				[['time_unit_id'], 'exist', 'skipOnError' => true, 'targetClass' => TimeUnit::className(), 'targetAttribute' => ['time_unit_id' => 'id']],
				[['currency_id'], 'exist', 'skipOnError' => true, 'targetClass' => Currency::className(), 'targetAttribute' => ['currency_id' => 'id']],
				[['language_id'], 'exist', 'skipOnError' => true, 'targetClass' => Language::className(), 'targetAttribute' => ['language_id' => 'id']],
				[['offered_product_id'], 'exist', 'skipOnError' => true, 'targetClass' => OfferedProduct::className(), 'targetAttribute' => ['offered_product_id' => 'id']]
		];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return [
            	'offered_product_id' => Yii::t('main', 'Offered Product ID'),
            	'language_id' => Yii::t('main', 'Language ID'),
            	'name' => Yii::t('main', 'Name'),
            	'description' => Yii::t('main', 'Description'),
            	'price' => Yii::t('main', 'Price'),
            	'currency_id' => Yii::t('main', 'Currency ID'),
            	'expected_time' => Yii::t('main', 'Expected Time'),
            	'time_unit_id' => Yii::t('main', 'Time Unit ID'),
            	'date_creation' => Yii::t('main', 'Date Creation'),
        		'unit' => Yii::t('main', 'Unit'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTimeUnit() {
        return $this->hasOne(TimeUnit::className(), ['id' => 'time_unit_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCurrency() {
        return $this->hasOne(Currency::className(), ['id' => 'currency_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLanguage() {
        return $this->hasOne(Language::className(), ['id' => 'language_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOfferedProduct() {
        return $this->hasOne(OfferedProduct::className(), ['id' => 'offered_product_id']);
    }

    /**
     * @inheritdoc
     * @return \common\models\aq\OfferedProductTranslationQuery the active query used by this AR class.
     */
    public static function find() {
        return new \common\models\aq\OfferedProductTranslationQuery(get_called_class());
    }
    
    public function behaviors() {
    	return [
    			[
    					'class' => TimestampBehavior::className(),
    					'createdAtAttribute' => 'date_creation',
    					'updatedAtAttribute' => false,
    					'value' => new Expression('NOW()'),
    			],
    	];
    }
}
