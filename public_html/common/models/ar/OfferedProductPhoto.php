<?php

namespace common\models\ar;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\Expression;

/**
 * This is the model class for table "offered_product_photo".
 *
 * @property integer $id
 * @property integer $offered_product_id
 * @property string $url_photo
 * @property string $url_thumbnail
 * @property integer $priority
 * @property string $date_creation
 * @property string $description
 *
 * @property OfferedProduct $offeredProduct
 * @property OfferedProductPhotoTranslation[] $offeredProductPhotoTranslations
 * @property Language[] $languages
 */
class OfferedProductPhoto extends \yii\db\ActiveRecord {

    /**
     * @inheritdoc
     */
    public static function tableName() {
        return 'offered_product_photo';
    }

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
				[['offered_product_id', 'url_photo', 'url_thumbnail'], 'required'],
				[['offered_product_id', 'priority', 'url_thumbnail'], 'integer'],
				[['date_creation'], 'safe'],
				[['url_photo'], 'string', 'max' => 256],
        		[['description'], 'string', 'max' => 512],
				[['offered_product_id'], 'exist', 'skipOnError' => true, 'targetClass' => OfferedProduct::className(), 'targetAttribute' => ['offered_product_id' => 'id']]
		];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return [
            	'id' => Yii::t('main', 'ID'),
            	'offered_product_id' => Yii::t('main', 'Offered Product ID'),
            	'url_photo' => Yii::t('main', 'Url Photo'),
            	'priority' => Yii::t('main', 'Priority'),
            	'date_creation' => Yii::t('main', 'Date Creation'),
        		'description' => Yii::t('main', 'Description'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOfferedProduct() {
        return $this->hasOne(OfferedProduct::className(), ['id' => 'offered_product_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOfferedProductPhotoTranslations() {
        return $this->hasMany(OfferedProductPhotoTranslation::className(), ['offered_product_photo_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLanguages() {
        return $this->hasMany(Language::className(), ['id' => 'language_id'])->viaTable('offered_product_photo_translation', ['offered_product_photo_id' => 'id']);
    }

    /**
     * @inheritdoc
     * @return \common\models\aq\OfferedProductPhotoQuery the active query used by this AR class.
     */
    public static function find() {
        return new \common\models\aq\OfferedProductPhotoQuery(get_called_class());
    }
    
    public function behaviors() {
    	return [
    			[
    					'class' => TimestampBehavior::className(),
    					'createdAtAttribute' => 'date_creation',
    					'updatedAtAttribute' => false,
    					'value' => new Expression('NOW()'),
    			],
    	];
    }
}
