<?php

namespace common\models\ar;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\Expression;

/**
 * This is the model class for table "warehouse_delivery".
 *
 * @property integer $id
 * @property integer $warehouse_id
 * @property integer $warehouse_supplier_id
 * @property string $number
 * @property integer $user_confirming_id
 * @property integer $is_artificial
 * @property string $date_delivery
 * @property string $date_creation
 * @property string $description
 * @property string $is_active
 * @property string $is_confirmed
 * @property integer $operation_id
 *
 * @property WarehouseProductOperation $operation
 * @property User $userConfirming
 * @property Warehouse $warehouse
 * @property WarehouseSupplier $warehouseSupplier
 * @property WarehouseDeliveryProduct[] $warehouseDeliveryProducts
 * @property WarehouseProductStatusHistory[] $warehouseProductStatusHistories 
 */
class WarehouseDelivery extends \yii\db\ActiveRecord {

    /**
     * @inheritdoc
     */
    public static function tableName() {
        return 'warehouse_delivery';
    }

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
				[['warehouse_id', 'number', 'user_confirming_id'], 'required'],
				[['warehouse_id', 'warehouse_supplier_id', 'user_confirming_id', 'is_artificial', 'is_active', 'operation_id'], 'integer'],
				[['date_delivery', 'date_creation', 'is_confirmed'], 'safe'],
				[['number'], 'string', 'max' => 48],
        		[['description'], 'string', 'max' => 2048],
        		[['operation_id'], 'exist', 'skipOnError' => true, 'targetClass' => WarehouseProductOperation::className(), 'targetAttribute' => ['operation_id' => 'id']],
				[['user_confirming_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['user_confirming_id' => 'id']],
				[['warehouse_id'], 'exist', 'skipOnError' => true, 'targetClass' => Warehouse::className(), 'targetAttribute' => ['warehouse_id' => 'id']],
				[['warehouse_supplier_id'], 'exist', 'skipOnError' => true, 'targetClass' => WarehouseSupplier::className(), 'targetAttribute' => ['warehouse_supplier_id' => 'id']]
		];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return [
            	'id' => Yii::t('main', 'ID'),
            	'warehouse_id' => Yii::t('main', 'Warehouse'),
            	'warehouse_supplier_id' => Yii::t('main', 'Warehouse Supplier ID'),
            	'number' => Yii::t('main', 'Number'),
            	'user_confirming_id' => Yii::t('main', 'User Confirming ID'),
            	'is_artificial' => Yii::t('main', 'Is Artificial'),
            	'date_delivery' => Yii::t('main', 'Date Delivery'),
            	'date_creation' => Yii::t('main', 'Date Creation'),
        		'is_confirmed' => Yii::t('web', 'Is receipt confirmed'),
        		'description' => Yii::t('main', 'Description'),
        		'is_active' => Yii::t('main', 'Is Active'),
        		'operation_id' => Yii::t('main', 'Operation ID'),
        ];
    }
    
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOperation() {
    	return $this->hasOne(WarehouseProductOperation::className(), ['id' => 'operation_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUserConfirming() {
        return $this->hasOne(User::className(), ['id' => 'user_confirming_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getWarehouse() {
        return $this->hasOne(Warehouse::className(), ['id' => 'warehouse_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getWarehouseSupplier() {
        return $this->hasOne(WarehouseSupplier::className(), ['id' => 'warehouse_supplier_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getWarehouseDeliveryProducts() {
        return $this->hasMany(WarehouseDeliveryProduct::className(), ['warehouse_delivery_id' => 'id']);
    }
    
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getWarehouseProductStatusHistories() {
    	return $this->hasMany(WarehouseProductStatusHistory::className(), ['warehouse_delivery_id' => 'id']);
    }

    /**
     * @inheritdoc
     * @return \common\models\aq\WarehouseDeliveryQuery the active query used by this AR class.
     */
    public static function find() {
        return new \common\models\aq\WarehouseDeliveryQuery(get_called_class());
    }
    
    public function behaviors() {
    	return [
    			[
    					'class' => TimestampBehavior::className(),
    					'createdAtAttribute' => 'date_creation',
    					'updatedAtAttribute' => false,
    					'value' => new Expression('NOW()'),
    			],
    	];
    }
}
