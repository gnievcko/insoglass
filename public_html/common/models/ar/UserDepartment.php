<?php

namespace common\models\ar;

use Yii;

/**
 * This is the model class for table "user_department".
 *
 * @property integer $user_id
 * @property integer $department_id
 * @property integer $is_lead
 *
 * @property Department $department
 * @property User $user
 */
class UserDepartment extends \yii\db\ActiveRecord {

    /**
     * @inheritdoc
     */
    public static function tableName() {
        return 'user_department';
    }

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
				[['user_id', 'department_id'], 'required'],
				[['user_id', 'department_id', 'is_lead'], 'integer'],
				[['department_id'], 'exist', 'skipOnError' => true, 'targetClass' => Department::className(), 'targetAttribute' => ['department_id' => 'id']],
				[['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['user_id' => 'id']]
		];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return [
            	'user_id' => Yii::t('main', 'User'),
            	'department_id' => Yii::t('main', 'Department ID'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDepartment() {
        return $this->hasOne(Department::className(), ['id' => 'department_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser() {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }

    /**
     * @inheritdoc
     * @return \common\models\aq\UserDepartmentQuery the active query used by this AR class.
     */
    public static function find() {
        return new \common\models\aq\UserDepartmentQuery(get_called_class());
    }
}
