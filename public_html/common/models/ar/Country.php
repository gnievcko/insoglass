<?php

namespace common\models\ar;

use Yii;

/**
 * This is the model class for table "country".
 *
 * @property integer $id
 * @property string $symbol
 * @property integer $priority
 *
 * @property CountryTranslation[] $countryTranslations
 * @property Language[] $languages
 * @property Province[] $provinces
 */
class Country extends \yii\db\ActiveRecord {

    /**
     * @inheritdoc
     */
    public static function tableName() {
        return 'country';
    }

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
				[['symbol', 'priority'], 'required'],
				[['priority'], 'integer'],
				[['symbol'], 'string', 'max' => 5],
				[['symbol'], 'unique']
		];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return [
            	'id' => Yii::t('main', 'ID'),
            	'symbol' => Yii::t('main', 'Symbol'),
            	'priority' => Yii::t('main', 'Priority'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCountryTranslations() {
        return $this->hasMany(CountryTranslation::className(), ['country_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLanguages() {
        return $this->hasMany(Language::className(), ['id' => 'language_id'])->viaTable('country_translation', ['country_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProvinces() {
        return $this->hasMany(Province::className(), ['country_id' => 'id']);
    }
    
    public function getTranslation() {
    	$translations = $this->countryTranslations;
    	if(empty($translations)) {
    		return null;
    	}
    	 
    	foreach($translations as $t) {
    		if($t->language->symbol === Yii::$app->language) {
    			return $t;
    		}
    	}
    	 
    	return $translations[0];
    }

    /**
     * @inheritdoc
     * @return \common\models\aq\CountryQuery the active query used by this AR class.
     */
    public static function find() {
        return new \common\models\aq\CountryQuery(get_called_class());
    }
}
