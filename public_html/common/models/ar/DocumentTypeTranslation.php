<?php

namespace common\models\ar;

use Yii;

/**
 * This is the model class for table "document_type_translation".
 *
 * @property integer $document_type_id
 * @property integer $language_id
 * @property string $name
 *
 * @property DocumentType $documentType
 * @property Language $language
 */
class DocumentTypeTranslation extends \yii\db\ActiveRecord {

    /**
     * @inheritdoc
     */
    public static function tableName() {
        return 'document_type_translation';
    }

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
				[['document_type_id', 'language_id', 'name'], 'required'],
				[['document_type_id', 'language_id'], 'integer'],
				[['name'], 'string', 'max' => 128],
				[['document_type_id'], 'exist', 'skipOnError' => true, 'targetClass' => DocumentType::className(), 'targetAttribute' => ['document_type_id' => 'id']],
				[['language_id'], 'exist', 'skipOnError' => true, 'targetClass' => Language::className(), 'targetAttribute' => ['language_id' => 'id']]
		];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return [
            	'document_type_id' => Yii::t('main', 'Document Type ID'),
            	'language_id' => Yii::t('main', 'Language ID'),
            	'name' => Yii::t('main', 'Name'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDocumentType() {
        return $this->hasOne(DocumentType::className(), ['id' => 'document_type_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLanguage() {
        return $this->hasOne(Language::className(), ['id' => 'language_id']);
    }

    /**
     * @inheritdoc
     * @return \common\models\aq\DocumentTypeTranslationQuery the active query used by this AR class.
     */
    public static function find() {
        return new \common\models\aq\DocumentTypeTranslationQuery(get_called_class());
    }
}
