<?php

namespace common\models\ar;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\Expression;

/**
 * This is the model class for table "user_attachment".
 *
 * @property integer $id
 * @property integer $user_attached_id
 * @property integer $user_id
 * @property string $url_file
 * @property string $name
 * @property string $description
 * @property string $date_creation
 *
 * @property User $user
 * @property User $userAttached
 */
class UserAttachment extends \yii\db\ActiveRecord {

    /**
     * @inheritdoc
     */
    public static function tableName() {
        return 'user_attachment';
    }

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
				[['user_attached_id', 'user_id', 'url_file', 'name'], 'required'],
				[['user_attached_id', 'user_id'], 'integer'],
				[['date_creation'], 'safe'],
				[['url_file', 'name'], 'string', 'max' => 256],
				[['description'], 'string', 'max' => 512],
				[['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['user_id' => 'id']],
				[['user_attached_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['user_attached_id' => 'id']]
		];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return [
            	'id' => Yii::t('main', 'ID'),
            	'user_attached_id' => Yii::t('main', 'User Attached ID'),
            	'user_id' => Yii::t('main', 'User'),
            	'url_file' => Yii::t('main', 'Url File'),
            	'name' => Yii::t('main', 'Name'),
            	'description' => Yii::t('main', 'Description'),
            	'date_creation' => Yii::t('main', 'Date Creation'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser() {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUserAttached() {
        return $this->hasOne(User::className(), ['id' => 'user_attached_id']);
    }

    /**
     * @inheritdoc
     * @return \common\models\aq\UserAttachmentQuery the active query used by this AR class.
     */
    public static function find() {
        return new \common\models\aq\UserAttachmentQuery(get_called_class());
    }
    
    public function behaviors() {
    	return [
    			[
    					'class' => TimestampBehavior::className(),
    					'createdAtAttribute' => 'date_creation',
    					'updatedAtAttribute' => false,
    					'value' => new Expression('NOW()'),
    			],
    	];
    }
}
