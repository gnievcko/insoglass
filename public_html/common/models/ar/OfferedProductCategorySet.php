<?php

namespace common\models\ar;

use Yii;

/**
 * This is the model class for table "offered_product_category_set".
 *
 * @property integer $offered_product_id
 * @property integer $offered_product_category_id
 *
 * @property OfferedProductCategory $offeredProductCategory
 * @property OfferedProduct $offeredProduct
 */
class OfferedProductCategorySet extends \yii\db\ActiveRecord {

    /**
     * @inheritdoc
     */
    public static function tableName() {
        return 'offered_product_category_set';
    }

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
				[['offered_product_id', 'offered_product_category_id'], 'required'],
				[['offered_product_id', 'offered_product_category_id'], 'integer'],
				[['offered_product_category_id'], 'exist', 'skipOnError' => true, 'targetClass' => OfferedProductCategory::className(), 'targetAttribute' => ['offered_product_category_id' => 'id']],
				[['offered_product_id'], 'exist', 'skipOnError' => true, 'targetClass' => OfferedProduct::className(), 'targetAttribute' => ['offered_product_id' => 'id']]
		];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return [
            	'offered_product_id' => Yii::t('main', 'Offered Product ID'),
            	'offered_product_category_id' => Yii::t('main', 'Offered Product Category ID'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOfferedProductCategory() {
        return $this->hasOne(OfferedProductCategory::className(), ['id' => 'offered_product_category_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOfferedProduct() {
        return $this->hasOne(OfferedProduct::className(), ['id' => 'offered_product_id']);
    }

    /**
     * @inheritdoc
     * @return \common\models\aq\OfferedProductCategorySetQuery the active query used by this AR class.
     */
    public static function find() {
        return new \common\models\aq\OfferedProductCategorySetQuery(get_called_class());
    }
}
