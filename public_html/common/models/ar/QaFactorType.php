<?php

namespace common\models\ar;

use Yii;

/**
 * This is the model class for table "qa_factor_type".
 *
 * @property integer $id
 * @property string $symbol
 * @property string $variable_type
 * @property integer $priority
 * @property integer $is_visible
 *
 * @property ProductionTaskQaFactor[] $productionTaskQaFactors
 * @property QaFactorTypeTranslation[] $qaFactorTypeTranslations
 * @property Language[] $languages
 */
class QaFactorType extends \yii\db\ActiveRecord {

    /**
     * @inheritdoc
     */
    public static function tableName() {
        return 'qa_factor_type';
    }

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
			[['symbol', 'variable_type', 'priority'], 'required'],
			[['priority', 'is_visible'], 'integer'],
			[['symbol', 'variable_type'], 'string', 'max' => 32],
			[['symbol'], 'unique']
		];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return [
            'id' => Yii::t('main', 'ID'),
            'symbol' => Yii::t('main', 'Symbol'),
            'variable_type' => Yii::t('main', 'Variable Type'),
            'priority' => Yii::t('main', 'Priority'),
            'is_visible' => Yii::t('main', 'Is Visible'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProductionTaskQaFactors() {
        return $this->hasMany(ProductionTaskQaFactor::className(), ['qa_factor_type_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getQaFactorTypeTranslations() {
        return $this->hasMany(QaFactorTypeTranslation::className(), ['qa_factor_type_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLanguages() {
        return $this->hasMany(Language::className(), ['id' => 'language_id'])->viaTable('qa_factor_type_translation', ['qa_factor_type_id' => 'id']);
    }

    /**
     * @inheritdoc
     * @return \common\models\aq\QaFactorTypeQuery the active query used by this AR class.
     */
    public static function find() {
        return new \common\models\aq\QaFactorTypeQuery(get_called_class());
    }
}
