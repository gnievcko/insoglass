<?php

namespace common\models\ar;

use Yii;

/**
 * This is the model class for table "product_status_translation".
 *
 * @property integer $product_status_id
 * @property integer $language_id
 * @property string $name
 *
 * @property ProductStatus $productStatus
 * @property Language $language
 */
class ProductStatusTranslation extends \yii\db\ActiveRecord {

    /**
     * @inheritdoc
     */
    public static function tableName() {
        return 'product_status_translation';
    }

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
				[['product_status_id', 'language_id', 'name'], 'required'],
				[['product_status_id', 'language_id'], 'integer'],
				[['name'], 'string', 'max' => 128],
				[['product_status_id'], 'exist', 'skipOnError' => true, 'targetClass' => ProductStatus::className(), 'targetAttribute' => ['product_status_id' => 'id']],
				[['language_id'], 'exist', 'skipOnError' => true, 'targetClass' => Language::className(), 'targetAttribute' => ['language_id' => 'id']],
        		[['product_status_id'], 'unique', 'targetAttribute' => ['product_status_id', 'language_id'], 'comboNotUnique' => Yii::t('main', 'The chosen product status and language have a translation already')],
        		
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return [
            	'product_status_id' => Yii::t('main', 'Product Status ID'),
            	'language_id' => Yii::t('main', 'Language ID'),
            	'name' => Yii::t('main', 'Name'),
        		'productStatusSymbol' => Yii::t('main', 'Symbol'),
        		'languageName' => Yii::t('main', 'Language'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProductStatus() {
        return $this->hasOne(ProductStatus::className(), ['id' => 'product_status_id']);
    }
    
    public function getProductStatusSymbol() {
    	return $this->productStatus->symbol;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLanguage() {
        return $this->hasOne(Language::className(), ['id' => 'language_id']);
    }
    
    public function getLanguageName() {
    	return $this->language->name;
    }

    /**
     * @inheritdoc
     * @return \common\models\aq\ProductStatusTranslationQuery the active query used by this AR class.
     */
    public static function find() {
        return new \common\models\aq\ProductStatusTranslationQuery(get_called_class());
    }
}
