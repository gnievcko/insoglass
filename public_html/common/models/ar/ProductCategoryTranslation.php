<?php

namespace common\models\ar;

use Yii;

/**
 * This is the model class for table "product_category_translation".
 *
 * @property integer $product_category_id
 * @property integer $language_id
 * @property string $name
 * @property string $description
 *
 * @property ProductCategory $productCategory
 * @property Language $language
 */
class ProductCategoryTranslation extends \yii\db\ActiveRecord {

    /**
     * @inheritdoc
     */
    public static function tableName() {
        return 'product_category_translation';
    }

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
				[['product_category_id', 'language_id', 'name'], 'required'],
				[['product_category_id', 'language_id'], 'integer'],
				[['name'], 'string', 'max' => 128],
				[['description'], 'string', 'max' => 1024],
				[['product_category_id'], 'exist', 'skipOnError' => true, 'targetClass' => ProductCategory::className(), 'targetAttribute' => ['product_category_id' => 'id']],
				[['language_id'], 'exist', 'skipOnError' => true, 'targetClass' => Language::className(), 'targetAttribute' => ['language_id' => 'id']]
		];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return [
            	'product_category_id' => Yii::t('main', 'Product Category ID'),
            	'language_id' => Yii::t('main', 'Language ID'),
            	'name' => Yii::t('main', 'Name'),
            	'description' => Yii::t('main', 'Description'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProductCategory() {
        return $this->hasOne(ProductCategory::className(), ['id' => 'product_category_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLanguage() {
        return $this->hasOne(Language::className(), ['id' => 'language_id']);
    }

    /**
     * @inheritdoc
     * @return \common\models\aq\ProductCategoryTranslationQuery the active query used by this AR class.
     */
    public static function find() {
        return new \common\models\aq\ProductCategoryTranslationQuery(get_called_class());
    }
}
