<?php

namespace common\models\ar;

use Yii;

/**
 * This is the model class for table "activity_translation".
 *
 * @property integer $activity_id
 * @property integer $language_id
 * @property string $name
 *
 * @property Activity $activity
 * @property Language $language
 */
class ActivityTranslation extends \yii\db\ActiveRecord {

    /**
     * @inheritdoc
     */
    public static function tableName() {
        return 'activity_translation';
    }

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
				[['activity_id', 'language_id', 'name'], 'required'],
				[['activity_id', 'language_id'], 'integer'],
				[['name'], 'string', 'max' => 256],
				[['activity_id'], 'exist', 'skipOnError' => true, 'targetClass' => Activity::className(), 'targetAttribute' => ['activity_id' => 'id']],
				[['language_id'], 'exist', 'skipOnError' => true, 'targetClass' => Language::className(), 'targetAttribute' => ['language_id' => 'id']]
		];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return [
            	'activity_id' => Yii::t('main', 'Activity ID'),
            	'language_id' => Yii::t('main', 'Language ID'),
            	'name' => Yii::t('main', 'Name'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getActivity() {
        return $this->hasOne(Activity::className(), ['id' => 'activity_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLanguage() {
        return $this->hasOne(Language::className(), ['id' => 'language_id']);
    }

    /**
     * @inheritdoc
     * @return \common\models\aq\ActivityTranslationQuery the active query used by this AR class.
     */
    public static function find() {
        return new \common\models\aq\ActivityTranslationQuery(get_called_class());
    }
}
