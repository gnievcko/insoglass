<?php

namespace common\models\ar;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\Expression;

/**
 * This is the model class for table "order".
 *
 * @property integer $id
 * @property integer $company_id
 * @property integer $entity_id 
 * @property integer $order_group_id
 * @property string $number
 * @property string $number_for_client
 * @property string $title
 * @property string $description
 * @property string $description_note 
 * @property string $description_cont
 * @property integer $user_id
 * @property integer $order_history_first_id
 * @property integer $order_history_last_id
 * @property integer $is_active
 * @property integer $is_template
 * @property string $date_creation
 * @property string $date_modification
 * @property integer $is_invoiced 
 * @property string $duration_time 
 * @property string $execution_time 
 * @property string $payment_terms 
 * @property integer $contract_type_id
 * @property integer $is_deleted 
 * @property integer $user_deleting_id
 * @property integer $is_artificial
 * @property integer $user_responsible_id
 * @property integer $order_priority_id 
 *
 * @property User $user
 * @property OrderHistory $orderHistoryLast
 * @property Company $company
 * @property Entity $entity 
 * @property OrderGroup $orderGroup
 * @property OrderHistory $orderHistoryFirst
 * @property OrderAttachment[] $orderAttachments
 * @property OrderHistory[] $orderHistories
 * @property OrderOfferedProduct[] $orderOfferedProducts
 * @property UserOrderSalesman[] $userOrderSalesmen
 * @property User[] $users
 * @property Alert[] $alerts
 * @property ContractType $contractType
 * @property User $userDeleting
 * @property OrderUserRepresentative[] $orderUserRepresentatives 
 * @property OrderTypeSet[] $orderTypeSets 
 * @property OrderType[] $orderTypes 
 * @property User $userResponsible
 * @property OrderPriority $orderPriority
 */
class Order extends \yii\db\ActiveRecord {

    /**
     * @inheritdoc
     */
    public static function tableName() {
        return 'order';
    }

    /**
     * @inheritdoc
     */
    public function rules() {

        $rules = [
                    [['company_id', 'order_group_id', 'number', 'title', 'user_id', 'entity_id'], 'required'],
                    [['company_id', 'entity_id', 'order_group_id', 'order_history_first_id', 'order_history_last_id', 
                    		'is_active', 'is_template', 'user_id', 'is_invoiced', 'contract_type_id', 'is_deleted', 
                    		'user_deleting_id', 'is_artificial', 'order_priority_id'], 'integer'],
                    [['description', 'description_note', 'description_cont'], 'string'],
                    [['is_template', 'is_artificial'], 'default', 'value' => 0],
                    [['date_creation', 'date_modification'], 'safe'],
                    [['number'], 'string', 'max' => 48],
                    [['title'], 'string', 'max' => 256],
                    [['duration_time', 'execution_time', 'payment_terms'], 'string', 'max' => 64],
                    [['number'], 'unique'],
                    [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['user_id' => 'id']],
                    [['order_history_last_id'], 'exist', 'skipOnError' => true, 'targetClass' => OrderHistory::className(), 'targetAttribute' => ['order_history_last_id' => 'id']],
                    [['company_id'], 'exist', 'skipOnError' => true, 'targetClass' => Company::className(), 'targetAttribute' => ['company_id' => 'id']],
                    [['order_group_id'], 'exist', 'skipOnError' => true, 'targetClass' => OrderGroup::className(), 'targetAttribute' => ['order_group_id' => 'id']],
                    [['order_history_first_id'], 'exist', 'skipOnError' => true, 'targetClass' => OrderHistory::className(), 'targetAttribute' => ['order_history_first_id' => 'id']],
                    [['entity_id'], 'exist', 'skipOnError' => true, 'targetClass' => Entity::className(), 'targetAttribute' => ['entity_id' => 'id']],
                    [['user_deleting_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['user_deleting_id' => 'id']],
	        		[['order_priority_id'], 'exist', 'skipOnError' => true, 'targetClass' => OrderPriority::className(), 'targetAttribute' => ['order_priority_id' => 'id']],
        ];

        if(isset(Yii::$app->params['isNumberForClientVisible']) && Yii::$app->params['isNumberForClientVisible']) {
            $rules[] = [['number_for_client'], 'required'];
            $rules[] = [['number_for_client'], 'string', 'max' => 48];
        }
        
        if(isset(Yii::$app->params['isUserResponsibleVisible']) && Yii::$app->params['isUserResponsibleVisible']) {
            $rules[] = [['user_responsible_id'], 'integer'];
            $rules[] = [['user_responsible_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['user_responsible_id' => 'id']];
        }
        
        return $rules;
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return [
            'id' => Yii::t('main', 'ID'),
            'company_id' => Yii::t('main', 'Client ID'),
            'entity_id' => Yii::t('main', 'Entity ID'),
            'order_group_id' => Yii::t('main', 'Order Group ID'),
            'number' => Yii::t('main', 'Number'),
            'number_for_client' => Yii::t('main', 'Number for client'),
            'title' => Yii::t('main', 'Title'),
            'description' => Yii::t('main', 'Description'),
            'description_note' => Yii::t('main', 'Description Note'),
            'description_cont' => Yii::t('main', 'Description Cont'),
            'user_id' => Yii::t('main', 'User'),
            'order_history_first_id' => Yii::t('main', 'Order History First ID'),
            'order_history_last_id' => Yii::t('main', 'Order History Last ID'),
            'is_active' => Yii::t('main', 'Is Active'),
            'is_template' => Yii::t('main', 'Is Template'),
            'date_creation' => Yii::t('main', 'Date Creation'),
            'date_modification' => Yii::t('main', 'Date Modification'),
            'is_invoiced' => Yii::t('main', 'Is Invoiced'),
            'duration_time' => Yii::t('web', 'Duration time'),
            'execution_time' => Yii::t('web', 'Executiont time'),
            'payment_terms' => Yii::t('web', 'Terms of payment'),
            'contract_type_id' => Yii::t('main', 'Contract Type ID'),
            'is_deleted' => Yii::t('main', 'Is Deleted'),
            'user_deleting_id' => Yii::t('main', 'User Deleting ID'),
            'user_responsible_id' => Yii::t('main', 'User Responisble ID'),
        	'order_priority_id' => Yii::t('main', 'Order Priority ID'),
        ];
    }

    public function getOrderPriority() {
    	return $this->hasOne(OrderPriority::className(), ['id' => 'order_priority_id']);
    }
    
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrderHistoryLast() {
        return $this->hasOne(OrderHistory::className(), ['id' => 'order_history_last_id']);
    }
    public function getOrderStatus() {
        return $this->orderHistoryLast->orderStatus;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCompany() {
        return $this->hasOne(Company::className(), ['id' => 'company_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrderGroup() {
        return $this->hasOne(OrderGroup::className(), ['id' => 'order_group_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrderHistoryFirst() {
        return $this->hasOne(OrderHistory::className(), ['id' => 'order_history_first_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrderAttachments() {
        return $this->hasMany(OrderAttachment::className(), ['order_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrderHistories() {
        return $this->hasMany(OrderHistory::className(), ['order_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrderOfferedProducts() {
        return $this->hasMany(OrderOfferedProduct::className(), ['order_id' => 'id']);
    }
    
    public function getActiveOrderOfferedProducts() {
        return $this->hasMany(OrderOfferedProduct::className(), ['order_id' => 'id'])->where(['is_active' => 1]);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUserOrderSalesmen() {
        return $this->hasMany(UserOrderSalesman::className(), ['order_id' => 'id']);
    }

    public function getUser() {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUsers() {
        return $this->hasMany(User::className(), ['id' => 'user_id'])->viaTable('user_order_salesman', ['order_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAlerts() {
        return $this->hasMany(Alert::className(), ['company_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getEntity() {
        return $this->hasOne(Entity::className(), ['id' => 'entity_id']);
    }

    public function getContractType() {
        return $this->hasOne(ContractType::className(), ['id' => 'contract_type_id']);
    }

    public function getUserDeleting() {
        return $this->hasOne(User::className(), ['id' => 'user_deleting_id']);
    }
    
    public function getUserResponsible() {
        return $this->hasOne(User::className(), ['id' => 'user_responsible_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrderUserRepresentatives() {
        return $this->hasMany(OrderUserRepresentative::className(), ['order_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUserRepresentatives() {
        return $this->hasMany(User::className(), ['id' => 'user_representative_id'])->viaTable('order_user_representative', ['order_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrderTypeSets() {
        return $this->hasMany(OrderTypeSet::className(), ['order_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery 
     */
    public function getOrderTypes() {
        return $this->hasMany(OrderType::className(), ['id' => 'order_type_id'])->viaTable('order_type_set', ['order_id' => 'id']);
    }

    /**
     * @inheritdoc
     * @return \common\models\aq\OrderQuery the active query used by this AR class.
     */
    public static function find() {
        return new \common\models\aq\OrderQuery(get_called_class());
    }

    public function behaviors() {
        return [
            [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'date_creation',
                'updatedAtAttribute' => 'date_modification',
                'value' => new Expression('NOW()'),
            ],
        ];
    }

}
