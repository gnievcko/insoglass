<?php

namespace common\models\ar;

use Yii;

/**
 * This is the model class for table "production_path_translation".
 *
 * @property integer $production_path_id
 * @property integer $language_id
 * @property string $name
 *
 * @property ProductionPath $productionPath
 * @property Language $language
 */
class ProductionPathTranslation extends \yii\db\ActiveRecord {

    /**
     * @inheritdoc
     */
    public static function tableName() {
        return 'production_path_translation';
    }

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
				[['production_path_id', 'language_id', 'name'], 'required'],
				[['production_path_id', 'language_id'], 'integer'],
				[['name'], 'string', 'max' => 256],
				[['production_path_id'], 'exist', 'skipOnError' => true, 'targetClass' => ProductionPath::className(), 'targetAttribute' => ['production_path_id' => 'id']],
				[['language_id'], 'exist', 'skipOnError' => true, 'targetClass' => Language::className(), 'targetAttribute' => ['language_id' => 'id']]
		];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return [
            	'production_path_id' => Yii::t('main', 'Production Path ID'),
            	'language_id' => Yii::t('main', 'Language ID'),
            	'name' => Yii::t('main', 'Name'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProductionPath() {
        return $this->hasOne(ProductionPath::className(), ['id' => 'production_path_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLanguage() {
        return $this->hasOne(Language::className(), ['id' => 'language_id']);
    }

    /**
     * @inheritdoc
     * @return \common\models\aq\ProductionPathTranslationQuery the active query used by this AR class.
     */
    public static function find() {
        return new \common\models\aq\ProductionPathTranslationQuery(get_called_class());
    }
}
