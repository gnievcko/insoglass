<?php

namespace common\models\ar;

use Yii;

/**
 * This is the model class for table "information_page_translation".
 *
 * @property integer $information_page_id
 * @property integer $language_id
 * @property string $title
 * @property string $content
 * @property string $keyword
 * @property string $description
 * @property integer $is_active
 *
 * @property Language $language
 * @property InformationPage $informationPage
 */
class InformationPageTranslation extends \yii\db\ActiveRecord {

    /**
     * @inheritdoc
     */
    public static function tableName() {
        return 'information_page_translation';
    }

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
				[['information_page_id', 'language_id', 'title'], 'required'],
				[['information_page_id', 'language_id', 'is_active'], 'integer'],
				[['content'], 'string'],
				[['title', 'keyword', 'description'], 'string', 'max' => 255],
				[['language_id'], 'exist', 'skipOnError' => true, 'targetClass' => Language::className(), 'targetAttribute' => ['language_id' => 'id']],
				[['information_page_id'], 'exist', 'skipOnError' => true, 'targetClass' => InformationPage::className(), 'targetAttribute' => ['information_page_id' => 'id']],
        		//TODO: wymaga poprawki, wyskakuje dziwny komentarz
        		[['information_page_id'], 'unique', 'targetAttribute' => ['information_page_id', 'language_id'], 'comboNotUnique' => Yii::t('main', 'The chosen information page-language pair has a translation already.')]
		];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return [
            	'information_page_id' => Yii::t('main', 'Information Page ID'),
            	'language_id' => Yii::t('main', 'Language ID'),
            	'title' => Yii::t('main', 'Title'),
            	'content' => Yii::t('main', 'Content'),
            	'keyword' => Yii::t('main', 'Keyword'),
            	'description' => Yii::t('main', 'Description'),
            	'is_active' => Yii::t('main', 'Is active'),
        		'informationPageSymbol' => Yii::t('main', 'Information page'),
        		'languageName' => Yii::t('main', 'Language'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLanguage() {
        return $this->hasOne(Language::className(), ['id' => 'language_id']);
    }
    
    public function getLanguageName() {
       return $this->language->name;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getInformationPage() {
        return $this->hasOne(InformationPage::className(), ['id' => 'information_page_id']);
    }
    
    public function getInformationPageSymbol() {
    		return $this->informationPage->symbol;
  	}

    /**
     * @inheritdoc
     * @return \common\models\aq\InformationPageTranslationQuery the active query used by this AR class.
     */
    public static function find() {
        return new \common\models\aq\InformationPageTranslationQuery(get_called_class());
    }
}
