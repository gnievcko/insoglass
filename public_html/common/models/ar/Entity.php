<?php

namespace common\models\ar;

use Yii;

/**
 * This is the model class for table "entity".
 *
 * @property integer $id
 * @property string $name
 * @property integer $address_id
 * @property string $taxpayer_identification_number
 * @property string $vat_identification_number
 * @property string $krs_number
 * @property string $email
 * @property string $phone
 * @property string $fax
 * @property string $account_number
 * @property string $bank_name
 * @property string $share_capital
 * @property string $website_address
 * @property string $url_logo 
 * @property integer $is_certified 
 * @property integer $is_footer 
 * @property integer $is_active
 *
 * @property Address $address
 * @property Order[] $orders 
 */
class Entity extends \yii\db\ActiveRecord {

    /**
     * @inheritdoc
     */
    public static function tableName() {
        return 'entity';
    }

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
				[['name', 'address_id', 'taxpayer_identification_number', 'vat_identification_number', 'krs_number', 'email', 'phone', 'account_number', 'bank_name', 'is_active'], 'required'],
				[['address_id', 'is_active', 'is_certified', 'is_footer'], 'integer'],
				[['share_capital'], 'number'],
				[['name', 'website_address'], 'string', 'max' => 255],
				[['taxpayer_identification_number', 'vat_identification_number', 'phone', 'fax'], 'string', 'max' => 32],
        		[['krs_number', 'account_number'], 'string', 'max' => 150],
				[['email'], 'string', 'max' => 64],
				[['bank_name'], 'string', 'max' => 128],
        		[['url_logo'], 'string', 'max' => 256],
				[['name'], 'unique'],
				[['address_id'], 'exist', 'skipOnError' => true, 'targetClass' => Address::className(), 'targetAttribute' => ['address_id' => 'id']]
		];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return [
            	'id' => Yii::t('main', 'ID'),
            	'name' => Yii::t('main', 'Name'),
            	'address_id' => Yii::t('main', 'Address'),
            	'taxpayer_identification_number' => Yii::t('main', 'Taxpayer Identification Number'),
            	'vat_identification_number' => Yii::t('main', 'VAT Identification Number'),
            	'krs_number' => Yii::t('main', 'KRS number'),
            	'email' => Yii::t('main', 'Email'),
            	'phone' => Yii::t('main', 'Phone'),
            	'fax' => Yii::t('main', 'Fax'),
            	'account_number' => Yii::t('main', 'Account number'),
            	'bank_name' => Yii::t('main', 'Bank name'),
            	'share_capital' => Yii::t('main', 'Share capital'),
            	'website_address' => Yii::t('main', 'Website address'),
        		'url_logo' => Yii::t('main', 'Url Logo'),
        		'is_certified' => Yii::t('main', 'Is Certified'),
        		'is_footer' => Yii::t('main', 'Is Footer'),
            	'is_active' => Yii::t('main', 'Is active'),
        ];
    }

    public function getAddressName() {
    	return !empty($this->address) ? $this->address->fullAddress : null;
    }
    
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAddress() {
        return $this->hasOne(Address::className(), ['id' => 'address_id']);
    }

    /**
     * @inheritdoc
     * @return \common\models\aq\EntityQuery the active query used by this AR class.
     */
    public static function find() {
        return new \common\models\aq\EntityQuery(get_called_class());
    }
}
