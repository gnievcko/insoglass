<?php

namespace common\models\aq;

use Yii;
use yii\db\Query;
/**
 * This is the ActiveQuery class for [[\common\models\ar\OrderCustomData]].
 *
 * @see \common\models\ar\OrderCustomData
 */
class OrderCustomDataQuery extends \yii\db\ActiveQuery {

    /**
     * @inheritdoc
     * @return \common\models\ar\OrderCustomData[]|array
     */
    public function all($db = null) {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return \common\models\ar\OrderCustomData|array|null
     */
    public function one($db = null) {
        return parent::one($db);
    }
    
    public static function getCustomDataByOrderId($orderId) {
    	return (new Query())->select(['ocd.id', 'ocd.column_count as columnCount', 'ocd.content'])
    			->from('order_custom_data ocd')
    			->join('INNER JOIN', 'order_history oh', 'ocd.id = oh.order_custom_data_id')
    			->where(['oh.order_id' => intval($orderId)])
    			->orderBy(['ocd.id' => SORT_DESC])
    			->all();
    }
    
    public static function getCurrentCustomDataByOrderId($orderId) {
    	return (new Query())->select(['oh.id as ohid', 'ocd.id', 'ocd.column_count as columnCount', 'ocd.content'])
    			->from('order_history oh')
    			->join('INNER JOIN', 'order_custom_data ocd', 'oh.order_custom_data_id = ocd.id')
    			->where(['oh.order_id' => intval($orderId)])
    			->orderBy(['oh.id' => SORT_DESC])
    			->limit(1)
    			->one();
    }
    
}