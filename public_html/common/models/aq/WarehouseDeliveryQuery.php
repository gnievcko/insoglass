<?php

namespace common\models\aq;

use Yii;
use yii\db\Query;
use common\models\ar\ProductPhoto;

/**
 * This is the ActiveQuery class for [[\common\models\ar\WarehouseDelivery]].
 *
 * @see \common\models\ar\WarehouseDelivery
 */
class WarehouseDeliveryQuery extends \yii\db\ActiveQuery {

    /*public function active() {
        $this->andWhere('[[status]]=1');
        return $this;
    }*/

    /**
     * @inheritdoc
     * @return \common\models\ar\WarehouseDelivery[]|array
     */
    public function all($db = null) {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return \common\models\ar\WarehouseDelivery|array|null
     */
    public function one($db = null) {
        return parent::one($db);
    }
    
    public static function getWarehouseDeliveriesQuery($params = []){
    	
    	$positionCountSubquery = (new Query())->select(['COUNT(wdp.id)'])
    			->from('warehouse_delivery_product wdp')
    			->where('wdp.warehouse_delivery_id = wd.id');
    	
    	$currencySymbolSubquery = (new Query())->select(['c.short_symbol'])
    			->from('currency c')
    			->join('LEFT JOIN', 'warehouse_delivery_product wdp', 'wdp.currency_id = c.id')
    			->where('wdp.warehouse_delivery_id = wd.id')->distinct();
    	   	
    	
    	$totalPriceSubquery = (new Query())
    			->select(['SUM(COALESCE(price_unit * count, price_group)) as totalPrice'])
    			->from('warehouse_delivery_product wdp')
    			->where('wdp.warehouse_delivery_id = wd.id');  	
    	
    	$query = (new Query())
    			->select([
    					'wd.id', 'w.name as warehouseName', 'ws.name as supplierName', 'wd.number','UNIX_TIMESTAMP(wd.date_delivery) as dateDelivery',
    					'COALESCE(CONCAT(u.first_name, " ", u.last_name), u.email) as userConfirmingName',
    					'positionCount' => $positionCountSubquery, 'totalPrice' => $totalPriceSubquery, 
    					'currencySymbol' => $currencySymbolSubquery, 'wd.is_confirmed as isConfirmed',
    			])
    			->from('warehouse_delivery wd')
    			->join('INNER JOIN', 'warehouse w', 'wd.warehouse_id = w.id')
    			->join('LEFT JOIN', 'warehouse_supplier ws', 'ws.id = wd.warehouse_supplier_id')
    			->join('INNER JOIN', 'user u', 'u.id = wd.user_confirming_id')
    			->where(['wd.is_artificial' => false, 'wd.is_active' => 1]);
    					
    	$columnsMap = [
    			'id' => 'wd.id',
    			'warehouseName' => 'w.name',
    			'supplierName' => 'ws.name',
    			'number' => 'wd.number',
    			'dateDelivery' => 'wd.date_delivery',
    			'userConfirmingName' => 'COALESCE(CONCAT(u.first_name, " ", u.last_name), u.email)',
    			'positionCount' => '('.$positionCountSubquery->createCommand()->rawSql.')',
    			'totalPrice' => '('.$totalPriceSubquery->createCommand()->rawSql.')',
    			'currencySymbol' => '('.$currencySymbolSubquery->createCommand()->rawSql.')',
    	];

        if(!empty($params['warehouseIds'])) {
            $query->andWhere(['wd.warehouse_id' => $params['warehouseIds']]);
        }
    	
        if(!empty($params['supplierIds'])) {
            $query->andWhere(['or',
                ['wd.warehouse_supplier_id' => $params['supplierIds']],
                ['wd.warehouse_supplier_id' => null ]
                ]);
        }
    	
    	if(!empty($params['numberFilterString'])) {
    		$query->andWhere(['LIKE', 'wd.number', $params['numberFilterString']]);
    	}
    	
    	if(!empty($params['supplierName'])) {
    		$query->andWhere(['LIKE', 'ws.name', $params['supplierName']]);
    	}
    	
    	if(!empty($params['sortDir']) && !empty($columnsMap[$params['sortField']])) {
    		$query->orderBy([
    				$columnsMap[$params['sortField']] => (!empty($params['sortDir']) && $params['sortDir'] == 'desc') ? SORT_DESC : SORT_ASC
    		]);
    	}

    	return $query;
    }

    public static function getDetails($deliveryId) {
        return (new Query())->select(['wd.id', 'w.name as warehouseName', 'ws.name as warehouseSupplier', 'wd.number', 
		            'u.first_name as userFirstName', 'u.last_name as userLastName', 'u.email as userEmail',
		            'UNIX_TIMESTAMP(wd.date_delivery) as dateDelivery', 'UNIX_TIMESTAMP(wd.date_creation) as dateCreation',
		            'w.id as warehouseId', 'u.id as userConfirmingId', 'ws.id as warehouseSupplierId', 'wd.description',
		        	'wd.is_confirmed as isConfirmed', 'wd.operation_id as operationId', 'wpo.number as operationNumber',
        		])
	            ->from('warehouse_delivery wd')
	            ->join('INNER JOIN', 'warehouse w', 'w.id = wd.warehouse_id')
	            ->join('INNER JOIN', 'user u', 'u.id = wd.user_confirming_id')
	            ->join('LEFT JOIN', 'warehouse_supplier ws', 'ws.id = wd.warehouse_supplier_id')
	            ->join('LEFT JOIN', 'warehouse_product_operation wpo', 'wd.operation_id = wpo.id')
	            ->andWhere(['wd.id' => $deliveryId, 'wd.is_artificial' => 0, 'wd.is_active' => 1])
	            ->one();
    }

    public static function getDeliveryProductsQuery($deliveryId, $params = []) {
        $query = (new Query())->select(['wdp.product_id as id', 'wdp.count', 'wdp.price_unit', 'wdp.price_group', 'wdp.currency_id',
            	'c.short_symbol as currencySymbol', 'pt.name', 'c.conversion_value as currencyConversionValue', 
        		'pt1.name as parentName', 'wdp.note as note', 'wdp.unit',
        	])
            ->from('warehouse_delivery_product wdp')
            ->join('INNER JOIN', 'product p', 'wdp.product_id = p.id')
            ->join('INNER JOIN', 'product_translation pt', 'pt.product_id = p.id')
            ->join('INNER JOIN', 'language l', 'l.id = pt.language_id')
            ->join('INNER JOIN', 'currency c', 'c.id = wdp.currency_id')
            ->join('LEFT JOIN', 'product p1', 'p.parent_product_id = p1.id')
            ->join('LEFT JOIN', 'product_translation pt1', 'p1.id = pt1.product_id')
            ->join('LEFT JOIN', 'language l1', 'pt1.language_id = l1.id')
            ->andWhere(['l.symbol' => \Yii::$app->language])
            ->andWhere(['or', ['l1.symbol' => null], ['l1.symbol' => \Yii::$app->language]])
            ->andWhere(['wdp.warehouse_delivery_id' => $deliveryId]);

        if(!empty($params['name'])) {
            $query->andWhere(['like', 'pt.name', $params['name']]);
        }

    	$columnsMap = [
    			'name' => 'pt.name',
    			'count' => 'wdp.count',
    			'note' => 'wdp.note',
    			'unit' => 'wdp.unit',
    	];

    	if(!empty($params['sortDir']) && !empty($columnsMap[$params['sortField']])) {
            $query->orderBy([
                $columnsMap[$params['sortField']] => (!empty($params['sortDir']) && $params['sortDir'] == 'desc') ? SORT_DESC : SORT_ASC
            ]);
        }

        return $query;
    }
    
    public static function getHeaderInformation($id) {
    	return (new Query())->select(['wd.id', 'w.name as warehouseName', 'ws.name as supplierName', 'wd.number', 
		            'u.first_name as userFirstName', 'u.last_name as userLastName', 'u.email as userEmail',
		            'DATE_FORMAT(wd.date_delivery, "%d.%m.%Y %H:%i") as dateDelivery', 'DATE_FORMAT(wd.date_creation, 
    				"%d.%m.%Y %H:%i") as dateReception', 'w.id as warehouseId', 'u.id as userConfirmingId', 
    				'ws.id as warehouseSupplierId', 'wd.description', 'wd.operation_id as issueId', 'wpo.number as issueNumber',
		        ])
	            ->from('warehouse_delivery wd')
	            ->join('INNER JOIN', 'warehouse w', 'w.id = wd.warehouse_id')
	            ->join('INNER JOIN', 'user u', 'u.id = wd.user_confirming_id')
	            ->join('LEFT JOIN', 'warehouse_supplier ws', 'ws.id = wd.warehouse_supplier_id')
	            ->join('LEFT JOIN', 'warehouse_product_operation wpo', 'wd.operation_id = wpo.id')
	            ->andWhere(['wd.id' => intval($id), 'wd.is_active' => 1])
	            ->one();
    }
    
    public static function getDeliveredProductsForReport($dateFrom, $dateTo) {
    	
    	$countSubquery = (new Query())->select(['SUM(COALESCE(price_unit, price_group) * count)'])
    			->from('warehouse_delivery_product wdp')
    			->where('wdp.warehouse_delivery_id = wd.id');
    			
    	$query = (new Query())->select(['wd.id as deliveryId', 'wd.description as deliveryDescription', 'w.name as warehouseName', 'ws.name as warehouseSupplierName', 
    				'wd.number as deliveryNumber', 'u.first_name as userConfirmingFirstName', 'u.last_name as userConfirmingLastName', 
    				'wd.date_delivery as dateDelivery', 'wd.date_creation as dateCreation',
    				'pt.name as productName', 'wdp.count as deliveryCount', 'wdp.price_unit as deliveryPriceUnit', 'wdp.price_group as deliveryPriceGroup',
    				'c.short_symbol as currencySymbol', 'wdp.note as productNote', 'totalPrice' => $countSubquery,
    				'parentt.name as parentName' 
    	])
    			->from('warehouse_delivery wd')
    			->join('INNER JOIN', 'warehouse w', 'w.id = wd.warehouse_id')
    			->join('LEFT JOIN', 'warehouse_supplier ws', 'ws.id = wd.warehouse_supplier_id')
    			->join('LEFT JOIN', 'user u', 'u.id = wd.user_confirming_id')
    			->join('LEFT JOIN', 'warehouse_delivery_product wdp', 'wd.id = wdp.warehouse_delivery_id')
    			->join('LEFT JOIN', 'product p', 'wdp.product_id = p.id')
    			->join('LEFT JOIN', 'product_translation parentt', 'parentt.product_id = p.parent_product_id')
    			->join('INNER JOIN', 'product_translation pt', 'p.id = pt.product_id')
    			->join('INNER JOIN', 'language l', 'l.id = pt.language_id')
    			->join('LEFT JOIN', 'currency c', 'c.id = wdp.currency_id') 
    			->where('wd.is_artificial = 0')
    			->andWhere('wd.is_confirmed = 1')
    			->andWhere('wd.is_active = 1')
    			->andWhere('wd.date_creation >= :dateFrom',  [':dateFrom' => $dateFrom])
    			->andWhere('wd.date_creation <= :dateTo', [':dateTo' => $dateTo])
    			->andWHere(['l.symbol' => Yii::$app->language])
    			->all();
    		
    	return $query;
	
    }
}
