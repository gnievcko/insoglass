<?php
namespace common\models\aq;

use Yii;
use common\helpers\Utility;
use common\helpers\RoleHelper;
use yii\db\Query;

/**
 * This is the ActiveQuery class for [[\common\models\ar\UserCompanySalesman]].
 *
 * @see \common\models\ar\UserCompanySalesman
 */
class UserCompanySalesmanQuery extends \yii\db\ActiveQuery {

    /*public function active() {
        $this->andWhere('[[status]]=1');
        return $this;
    }*/

    /**
     * @inheritdoc
     * @return \common\models\ar\UserCompanySalesman[]|array
     */
    public function all($db = null) {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return \common\models\ar\UserCompanySalesman|array|null
     */
    public function one($db = null) {
        return parent::one($db);
    }
    
    public static function getSalesmenByCompanyId($companyId) {
    	return Yii::$app->db->createCommand('
    					SELECT DISTINCT u.id, u.email, u.first_name as firstName, u.last_name as lastName,
    						u.url_photo as urlPhoto
    					FROM user_company_salesman ucs
    					JOIN user u ON ucs.user_id = u.id
    					JOIN company_group_set cgs ON ucs.company_group_id = cgs.company_group_id   
    					WHERE cgs.company_id = :companyId
    					AND u.is_active = 1
    			')
        		->bindValues([
        				':companyId' => intval($companyId),
        		])
        		->queryAll();
    }

    public static function findSalespeople($params) {
        $query = (new \yii\db\Query())
	            ->select(['u.id', 'u.first_name', 'u.last_name', 'u.email'])
	            ->distinct()
	            ->from('user u')
	            ->join('INNER JOIN', 'user_company_salesman ucs', 'ucs.user_id = u.id')
	            ->join('INNER JOIN', 'company_group_set cgs', 'cgs.company_group_id = ucs.company_group_id')
	            ->andWhere('u.is_active');

        if(!empty($params['name'])) {
            $query->andWhere(['like', 'CONCAT(u.first_name, " ", u.last_name)', $params['name']]);
        }
        if(!empty($params['clientId'])) {
            $query->andWhere(['=', 'cgs.company_id', $params['clientId']]);
        }
        if(!empty($params['ignoredSalespeopleIds'])) {
            $query->andWhere(['not in', 'u.id', $params['ignoredSalespeopleIds']]);
        }

        return $query;
    }
    
    public static function findSalespeopleWithoutClient($params) {
    	$query = (new \yii\db\Query())
		    	->select(['u.id', 'u.first_name', 'u.last_name', 'u.email'])
		    	->distinct()
		    	->from('user u')
		    	->join('INNER JOIN', 'user_role ur', 'u.id = ur.user_id')
		    	->join('INNER JOIN', 'role r', 'ur.role_id = r.id')
		    	->andWhere('u.is_active = 1')
		    	->andWhere(['in', 'r.symbol', RoleHelper::getSalesmanRoles()]);
    
    	if(!empty($params['name'])) {
    		$query->andWhere(['like', 'CONCAT(u.first_name, " ", u.last_name, " ", u.email)', $params['name']]);
    	}
    	if(!empty($params['ignoredSalespeopleIds'])) {
    		$query->andWhere(['not in', 'u.id', $params['ignoredSalespeopleIds']]);
    	}
    
    	return $query;
    }
    
    public function getCompanyGroupsTranslationsByUserId($id){
		$query = (new Query())
				->select('ucs.company_group_id, cgt.name')
				->from('user_company_salesman ucs')
				->join('INNER JOIN', 'company_group_translation cgt', 'ucs.company_group_id = cgt.company_group_id')
				->join('INNER JOIN', 'language l', 'l.id = cgt.language_id')
				->join('INNER JOIN', 'company_group cg', 'ucs.company_group_id = cg.id')
				->where(['l.symbol' => Yii::$app->language])
				->andWhere(['ucs.user_id' => $id])
				->andWhere(['cg.is_predefined' => 0]);

		return $query;			
	}
	
	public static function getGroupsByUserId($userId, $onlyNotPredefined = true) {
		return Yii::$app->db->createCommand('
    				SELECT cg.id, cgt.name
					FROM user_company_salesman ucs
					JOIN company_group cg on ucs.company_group_id = cg.id
					JOIN company_group_translation cgt on cgt.company_group_id = cg.id
    				JOIN language l ON cgt.language_id = l.id
    				WHERE l.symbol = :lSymbol 
					AND ucs.user_id = :userId
    				'.(!empty($onlyNotPredefined) ? ' AND cg.is_predefined = 0 ' : '').'
    			')
	    			->bindValues([
	    					':userId' => intval($userId),
	    					'lSymbol' => Yii::$app->language,
	    			])
	    			->queryAll();
	}
}
