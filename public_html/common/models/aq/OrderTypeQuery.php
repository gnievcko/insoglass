<?php

namespace common\models\aq;

use Yii;
use yii\db\Query;

/**
 * This is the ActiveQuery class for [[\common\models\ar\OrderType]].
 *
 * @see \common\models\ar\OrderType
 */
class OrderTypeQuery extends \yii\db\ActiveQuery {

    /*public function active() {
        $this->andWhere('[[status]]=1');
        return $this;
    }*/

    /**
     * @inheritdoc
     * @return \common\models\ar\OrderType[]|array
     */
    public function all($db = null) {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return \common\models\ar\OrderType|array|null
     */
    public function one($db = null) {
        return parent::one($db);
    }
    
    public static function getOrderTypes() {
    	return (new Query())->select(['ot.id', 'ott.name'])
		    	->from('order_type ot')
		    	->join('INNER JOIN', 'order_type_translation ott', 'ot.id = ott.order_type_id')
		    	->join('INNER JOIN', 'language l', 'ott.language_id = l.id')
		    	->where(['l.symbol' => Yii::$app->language])
		    	->orderBy(['ott.name' => SORT_ASC])
    			->all();
    }
    
	public static function getOrderTypesByIds($ids = []) {
    	return (new Query())->select(['ot.id', 'ott.name'])
		    	->from('order_type ot')
		    	->join('INNER JOIN', 'order_type_translation ott', 'ot.id = ott.order_type_id')
		    	->join('INNER JOIN', 'language l', 'ott.language_id = l.id')
		    	->where(['l.symbol' => Yii::$app->language])
		    	->andWhere(['ot.id' => $ids])
		    	->orderBy(['ott.name' => SORT_ASC])
    			->all();
    }
    
    public static function getActiveOrderTypesQuery($params = []) {
    	$query = (new Query())->select(['ot.id', 'ott.name'])
    			->from('order_type ot')
    			->join('INNER JOIN', 'order_type_translation ott', 'ot.id = ott.order_type_id')
    			->join('INNER JOIN', 'language l')
    			->where(['l.symbol' => Yii::$app->language]);
    	
    	if(!empty($params['name'])) {
    		$query->andWhere(['like', 'ott.name', $params['name']]);
    	}
    			
    	$columnsMap = [
    			'id' => 'ot.id',
    			'name' => 'ott.name',
    	];
    			
    	if(!empty($params['sortDir']) && !empty($columnsMap[$params['sortField']])) {
    		$query->orderBy([
    			$columnsMap[$params['sortField']] => (!empty($params['sortDir']) && $params['sortDir'] == 'desc') ? SORT_DESC : SORT_ASC
    		]);
    	}
    	else {
    		$query->orderBy(['ott.name' => SORT_ASC]);
    	}
    			
    	return $query;
    }
    
    public static function getDetails($id) {
    	return (new Query())->select(['ot.id', 'ott.name'])
		    	->from('order_type ot')
		    	->join('INNER JOIN', 'order_type_translation ott', 'ot.id = ott.order_type_id')
		    	->join('INNER JOIN', 'language l')
		    	->where(['l.symbol' => Yii::$app->language])
    			->andWhere(['ot.id' => intval($id)])
    			->one();
    }
    
    public static function findOrderTypes($params = []) {
    	$query = (new \yii\db\Query())
		    	->select(['ott.name', 'ot.id'])
		    	->from('order_type ot')
		    	->join('INNER JOIN', 'order_type_translation ott', 'ot.id = ott.order_type_id')
		    	->join('INNER JOIN', 'language l', 'ott.language_id = l.id');
    
    	if(!empty($params['name'])) {
    		$query->andWhere(['like', 'ott.name', $params['name']]);
    	}
    
    	return $query;
    }
    
    // TODO: Do przeniesienia?
    public static function getContractTypes($onlyActives = true){
            $query = (new Query())->select(['ct.id', 'ctt.name'])
    			->from('contract_type ct')
    			->join('INNER JOIN', 'contract_type_translation ctt', 'ct.id = ctt.contract_type_id')
    			->join('INNER JOIN', 'language l', 'ctt.language_id = l.id')
    			->where(['l.symbol' => Yii::$app->language])
    			->orderBy(['ctt.name' => SORT_ASC]);
    		
    	if($onlyActives) {
    		$query->andWhere(['ct.is_active' => 1]);
    	}
    		
    	return $query->all();
    }
    
}