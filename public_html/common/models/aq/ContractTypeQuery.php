<?php

namespace common\models\aq;

use Yii;
use yii\db\Query;

/**
 * This is the ActiveQuery class for [[\common\models\ar\ContractType]].
 *
 * @see \common\models\ar\ContractType
 */
class ContractTypeQuery extends \yii\db\ActiveQuery {

    /*public function active() {
        $this->andWhere('[[status]]=1');
        return $this;
    }*/

    /**
     * @inheritdoc
     * @return \common\models\ar\ContractType[]|array
     */
    public function all($db = null) {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return \common\models\ar\ContractType|array|null
     */
    public function one($db = null) {
        return parent::one($db);
    }
    
    public static function getContractTypes($onlyActives = true) {
    	$query = (new Query())->select(['ct.id', 'ctt.name'])
    			->from('contract_type ct')
    			->join('INNER JOIN', 'contract_type_translation ctt', 'ct.id = ctt.contract_type_id')
    			->join('INNER JOIN', 'language l', 'ctt.language_id = l.id')
    			->where(['l.symbol' => Yii::$app->language])
    			->orderBy(['ctt.name' => SORT_ASC]);
    		
    	if($onlyActives) {
    		$query->andWhere(['ct.is_active' => 1]);
    	}
    		
    	return $query->all();
    }
}