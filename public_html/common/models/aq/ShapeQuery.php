<?php

namespace common\models\aq;

use Yii;
use yii\db\Query;
/**
 * This is the ActiveQuery class for [[\common\models\ar\Shape]].
 *
 * @see \common\models\ar\Shape
 */
class ShapeQuery extends \yii\db\ActiveQuery {

    /*public function active() {
        $this->andWhere('[[status]]=1');
        return $this;
    }*/

    /**
     * @inheritdoc
     * @return \common\models\ar\Shape[]|array
     */
    public function all($db = null) {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return \common\models\ar\Shape|array|null
     */
    public function one($db = null) {
        return parent::one($db);
    }
    
    public static function getNames(){
        $query = (new Query())->select(['s.id', 'st.name'])
            ->from('shape s')
            ->innerJoin('shape_translation st', 's.id = st.shape_id')
            ->innerJoin('language l', 'st.language_id = l.id')
            ->where(['l.symbol' => Yii::$app->language])
            ->orderBy(['st.name' => SORT_ASC]);
        
        return $query->all();
    }
    
    public static function getByName($name) {
        return (new Query())->select(['s.id'])
                ->from('shape s')
                ->innerJoin('shape_translation st', 's.id = st.shape_id')
                ->innerJoin('language l', 'st.language_id = l.id')
                ->where(['l.symbol' => Yii::$app->language])
                ->andWhere(['st.name' => $name])
                ->one();
    }
}