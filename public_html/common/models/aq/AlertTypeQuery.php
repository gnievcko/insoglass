<?php

namespace common\models\aq;

use Yii;
use yii\db\Query;
use common\helpers\Utility;
/**
 * This is the ActiveQuery class for [[\common\models\ar\AlertType]].
 *
 * @see \common\models\ar\AlertType
 */
class AlertTypeQuery extends \yii\db\ActiveQuery {
    /* public function active() {
      $this->andWhere('[[status]]=1');
      return $this;
      } */

    /**
     * @inheritdoc
     * @return \common\models\ar\AlertType[]|array
     */
    public function all($db = null) {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return \common\models\ar\AlertType|array|null
     */
    public function one($db = null) {
        return parent::one($db);
    }

    public static function getAlertTypeAndAlertCountQuery() {
        $userId = Yii::$app->user->id;
        $countSubquery = (new Query())->select(['COUNT(a.id)'])
                ->from('alert a')
                ->where('a.alert_type_id = at.id')
                ->andWhere('a.is_active = 1');

        $countSubquery->andWhere(['or', ['a.user_id' => intval($userId)], ['a.user_id' => null]]);

        return (new Query())->select(['att.name', 'number' => $countSubquery])
                        ->from('alert_type at')
                        ->join('INNER JOIN', 'alert_type_translation att', 'at.id = att.alert_type_id')
                        ->join('INNER JOIN', 'language l', 'att.language_id = l.id')
                        ->where(['at.is_enabled' => 1])
                        ->andWhere(['l.symbol' => Yii::$app->language])
                        ->orderBy('number DESC')->all();
    }

    public static function getAllTypes() {
        return (new Query())->select(['at.id', 'att.name'])
                        ->from('alert_type at')
                        ->join('INNER JOIN', 'alert_type_translation att', 'at.id = att.alert_type_id')
                        ->join('INNER JOIN', 'language l', 'att.language_id = l.id')
                        ->andWhere(['l.symbol' => Yii::$app->language])
                        ->andWhere(['at.is_enabled' => true])
                        ->orderBy('at.id')
                        ->all();
    }

}
