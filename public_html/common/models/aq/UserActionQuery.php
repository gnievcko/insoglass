<?php

namespace common\models\aq;
use yii\db\Query;

/**
 * This is the ActiveQuery class for [[\common\models\ar\UserAction]].
 *
 * @see \common\models\ar\UserAction
 */
class UserActionQuery extends \yii\db\ActiveQuery {

    /*public function active() {
        $this->andWhere('[[status]]=1');
        return $this;
    }*/

    /**
     * @inheritdoc
     * @return \common\models\ar\UserAction[]|array
     */
    public function all($db = null) {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return \common\models\ar\UserAction|array|null
     */
    public function one($db = null) {
        return parent::one($db);
    }
    
    public function getLastActionDateByUser($userId) {
    	return (new Query())->select('date_action')
    			->from('user_action')
    			->where(['user_id' => $userId])
    			->orderBy('date_action DESC')
    			->limit(1)->one()['date_action'];
    }
}