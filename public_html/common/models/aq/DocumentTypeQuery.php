<?php

namespace common\models\aq;

use Yii;
use yii\db\Query;
/**
 * This is the ActiveQuery class for [[\common\models\ar\DocumentType]].
 *
 * @see \common\models\ar\DocumentType
 */
class DocumentTypeQuery extends \yii\db\ActiveQuery {

    /*public function active() {
        $this->andWhere('[[status]]=1');
        return $this;
    }*/

    /**
     * @inheritdoc
     * @return \common\models\ar\DocumentType[]|array
     */
    public function all($db = null) {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return \common\models\ar\DocumentType|array|null
     */
    public function one($db = null) {
        return parent::one($db);
    }
    
    public static function getDocumentTypes($isForOrder = null, $isForWarehouse = null, $isForReport = null) {
    	$query = (new Query())->select(['dt.id', 'dt.symbol', 'dtt.name'])
    			->from('document_type dt')
    			->join('INNER JOIN', 'document_type_translation dtt', 'dt.id = dtt.document_type_id')
    			->join('INNER JOIN', 'language l', 'dtt.language_id = l.id')
    			->where(['l.symbol' => Yii::$app->language])
    			->orderBy(['dt.id' => SORT_ASC]);
    			
    	if(!is_null($isForOrder)) {
    		$query->andWhere(['dt.is_for_order' => intval($isForOrder)]);
    	}
    	if(!is_null($isForWarehouse)) {
    		$query->andWhere(['dt.is_for_warehouse' => intval($isForWarehouse)]);
    	}
    	if(!is_null($isForReport)) {
    		$query->andWhere(['dt.is_for_report' => intval($isForReport)]);
    	}
    	
    	return $query->all();
    }
    
    public static function getAvailableDocumentTypes() {
    	return (new Query())->select(['dt.symbol'])
    			->from('document_type dt')
    			->where(['or', 
    					['dt.is_for_order' => true],
    					['dt.is_for_warehouse' => true],
    					['dt.is_for_report' => true],
				])
				->all();
    }
}