<?php

namespace common\models\aq;

use Yii;
use yii\db\Query;
use common\models\ar\ProductCategory;

/**
 * This is the ActiveQuery class for [[\common\models\ar\ProductCategory]].
 *
 * @see \common\models\ar\ProductCategory
 */
class ProductCategoryQuery extends \yii\db\ActiveQuery {

    /*public function active() {
        $this->andWhere('[[status]]=1');
        return $this;
    }*/

    /**
     * @inheritdoc
     * @return \common\models\ar\ProductCategory[]|array
     */
    public function all($db = null) {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return \common\models\ar\ProductCategory|array|null
     */
    public function one($db = null) {
        return parent::one($db);
    }

    public static function getCategoryList($withNoCategory = true) {
    	$union = '';
    	if($withNoCategory) {
    		$union = ' UNION (SELECT 0, "'.Yii::t('main', 'No category').'" from DUAL) ';
    	}
    	
    	return Yii::$app->db->createCommand('
    				(SELECT pc.id as id, pct.name as name
	    				FROM product_category pc
	    				JOIN product_category_translation pct ON pc.id = pct.product_category_id
	    				JOIN language l ON pct.language_id = l.id
	    				WHERE l.symbol = :languageSymbol
    					ORDER BY name ASC) 
    				'.$union
    			)
        		->bindValues([
        				':languageSymbol' => Yii::$app->language,
        		])
        		->queryAll();
    }

    public static function getActiveProductCategoryQuery($params = []) {

    	$subquery = (new Query())
    			->select(['product_category_translation.name'])
    			->from('product_category_translation')
    			->where('product_category_translation.product_category_id = pc.parent_category_id');

    	$query = (new Query())
		    	->select(['pc.id as id', 'pc.symbol as symbol', 'parentCategoryName' => $subquery, 'pct.name as name', 'pct.description'])
		    	->from('product_category pc')
		    	->join('INNER JOIN', 'product_category_translation pct', 'pc.id = pct.product_category_id')
		    	->join('INNER JOIN', 'language l', 'pct.language_id = l.id')
		    	->where(['l.symbol' => \Yii::$app->language]);

    	if(!empty($params['productCategorySymbol'])) {
    		$query->andWhere(['like', 'pct.name', $params['productCategorySymbol']]);
    	}

    	$columnsMap = [
    			'id' => 'pc.id',
    			'symbol' => 'pc.symbol',
    			'name' => 'pct.name',
    			'description' => 'pct.description',
    			'parentCategoryName' => '(' . $subquery->createCommand()->rawSql . ')',
    	];

    	if(!empty($params['sortDir']) && !empty($columnsMap[$params['sortField']])) {
    		$query->orderBy([
    				$columnsMap[$params['sortField']] => (!empty($params['sortDir']) && $params['sortDir'] == 'desc') ? SORT_DESC : SORT_ASC
    		]);
    	}
    	else {
    		$query->orderBy(['name' => SORT_ASC]);
    	}

    	return $query;
    }

    public static function getDetails($id) {
    	$subquery = (new Query())
    			->select(['product_category_translation.name'])
    			->from('product_category_translation')
    			->where('product_category_translation.product_category_id = pc.parent_category_id');

    	return (new Query())
		    	->select(['pc.id as id', 'parentCategoryName' => $subquery, 'pct.name as name', 'pct.description'])
		    	->from('product_category pc')
		    	->join('INNER JOIN', 'product_category_translation pct', 'pc.id = pct.product_category_id')
		    	->join('INNER JOIN', 'language l', 'pct.language_id = l.id')
		    	->where(['l.symbol' => \Yii::$app->language])
		    	->andWhere(['pc.id' => $id])->one();
    }
    
    public static function getProductCategoriesByIds($ids = []) {
    	return (new Query())->select(['pc.id', 'pct.name'])
		    	->from('product_category pc')
		    	->join('INNER JOIN', 'product_category_translation pct', 'pc.id = pct.product_category_id')
		    	->join('INNER JOIN', 'language l', 'pct.language_id = l.id')
		    	->where(['l.symbol' => Yii::$app->language])
		    	->andWhere(['pc.id' => $ids])
		    	->orderBy(['pct.name' => SORT_ASC])
		    	->all();
    }
    
    public static function getByName($name) {
        return (new Query())->select(['pc.id'])
                ->from('product_category pc')
                ->innerJoin('product_category_translation pct', 'pc.id = pct.product_category_id')
                ->innerJoin('language l', 'pct.language_id = l.id')
                ->where(['LOWER(pct.name)' => mb_strtolower($name, 'UTF-8')])
                ->andWhere(['l.symbol' => Yii::$app->language])
                ->one();
    }
}
