<?php

namespace common\models\aq;

use Yii;

/**
 * This is the ActiveQuery class for [[\common\models\ar\UserOrderSalesman]].
 *
 * @see \common\models\ar\UserOrderSalesman
 */
class UserOrderSalesmanQuery extends \yii\db\ActiveQuery {

    /*public function active() {
        $this->andWhere('[[status]]=1');
        return $this;
    }*/

    /**
     * @inheritdoc
     * @return \common\models\ar\UserOrderSalesman[]|array
     */
    public function all($db = null) {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return \common\models\ar\UserOrderSalesman|array|null
     */
    public function one($db = null) {
        return parent::one($db);
    }
    
    public static function getSalesmenByOrderId($orderId) {
    	return Yii::$app->db->createCommand('
    					SELECT u.id, u.email, u.first_name as firstName, u.last_name as lastName, 
    						uos.is_main as isMain, u.url_photo as urlPhoto
    					FROM user_order_salesman uos
    					JOIN user u ON uos.user_id = u.id
    					WHERE uos.order_id = :orderId
    					AND u.is_active = 1
    			')
    			->bindValues([
    					':orderId' => intval($orderId),
    			])
    			->queryAll();
    }
}