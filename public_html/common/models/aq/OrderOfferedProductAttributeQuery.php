<?php

namespace common\models\aq;

use Yii;
use yii\db\Query;

/**
 * This is the ActiveQuery class for [[\common\models\ar\OrderOfferedProductAttribute]].
 *
 * @see \common\models\ar\OrderOfferedProductAttribute
 */
class OrderOfferedProductAttributeQuery extends \yii\db\ActiveQuery {

    /*public function active() {
        $this->andWhere('[[status]]=1');
        return $this;
    }*/

    /**
     * @inheritdoc
     * @return \common\models\ar\OrderOfferedProductAttribute[]|array
     */
    public function all($db = null) {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return \common\models\ar\OrderOfferedProductAttribute|array|null
     */
    public function one($db = null) {
        return parent::one($db);
    }
    
    public static function getProductAttributes($productId, $all = true, $onlyVisibleForClient = false) {
        // 'pag.id as groupId', 'pagt.name as groupName', 'pat.id as attributeId', 'patt.name as attributeName', 'oopa.value'])
        $query = (new Query())->select(['pat.id', 'pat.product_attribute_group_id as groupId',
                    'pat.variable_type as variableType', 'pat.is_visible as isVisible', 'patt.name',
                    'oopa.value', 'patt.hint',
                ])
                ->from('order_offered_product_attribute oopa')
                ->innerJoin('product_attribute_type pat', 'oopa.product_attribute_type_id = pat.id')
                ->innerJoin('product_attribute_type_translation patt', 'patt.product_attribute_type_id = pat.id')
                ->innerJoin('language l', 'l.id = patt.language_id')
                ->innerJoin('product_attribute_group pag', 'pag.id = pat.product_attribute_group_id')
                ->where([
                    'oopa.order_offered_product_id' => $productId,
                    'l.symbol' => Yii::$app->language,
                    'pat.is_visible' => true,                    
                ]);
                
        if(!$all) {
            $query->andWhere(['>=', 'oopa.value', 1]);
        }
        
        if($onlyVisibleForClient) {
            $query->andWhere(['pat.is_visible_for_client' => true]);
        }
        
        $query->orderBy(['pat.product_attribute_group_id' => SORT_ASC, 'pat.id' => SORT_ASC]);
        
        return $query->all();
    }
    
}