<?php

namespace common\models\aq;

use Yii;
use yii\db\Query;
use \DateTime;
use \DateInterval;
use common\helpers\Utility;
use yii\db\Expression;

/**
 * This is the ActiveQuery class for [[\common\models\ar\Task]].
 *
 * @see \common\models\ar\Task
 */
class TaskQuery extends \yii\db\ActiveQuery {

    /*public function active() {
        $this->andWhere('[[status]]=1');
        return $this;
    }*/

    /**
     * @inheritdoc
     * @return \common\models\ar\Task[]|array
     */
    public function all($db = null) {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return \common\models\ar\Task|array|null
     */
    public function one($db = null) {
        return parent::one($db);
    }
    
    public static function getActiveTasksQuery($userId, $params = []) {
    	$query = (new Query())->select(['t.id', 't.title', 't.is_complete as isComplete',     			
    					't.date_deadline as dateDeadline'])
    			->from('task t')
    			->where('t.is_active = 1')
    			->andWhere(['or', 
    					['t.user_notified_id' => null, 't.user_id' => intval($userId)],
    					['t.user_notified_id' => intval($userId)],
    			]);
    	
    	if(!empty($params['taskTypeId'])) {
    		$query->andWhere(['t.task_type_id' => $params['taskTypeId']]);
    	}
    		
    	$query->orderBy([new Expression('-t.date_deadline DESC'), 't.date_creation' => SORT_DESC]);
    	return $query;
    }
    
    public static function isTaskVisibleByUserId($id, $userId) {
    	$res = (new Query())->from('task t')
		    	->where('t.is_active = 1')
		    	->andWhere(['t.id' => intval($id)])
		    	->andWhere(['or',
		    			['t.user_notified_id' => null, 't.user_id' => intval($userId)],
		    			['t.user_notified_id' => intval($userId)],
		    	])
		    	->count();
		    	
		return $res > 0 ? true : false; 
    }
    
    public static function getDetails($id) {
    	$data = (new Query())->select(['ttt.name as type', 'tpt.name as priority', 't.title', 't.message', 
    				'DATE(t.date_reminder) as dateReminder', 'DATE(t.date_deadline) as dateDeadline', 
					'u1.first_name as authorFirstName', 'u1.last_name as authorLastName', 'u1.email as authorEmail',
					'u2.first_name as notifiedFirstName', 'u2.last_name as notifiedLastName', 'u2.email as notifiedEmail', 
 				])
 				->from('task t')
 				->join('INNER JOIN', 'task_type_translation ttt', 't.task_type_id = ttt.task_type_id')
 				->join('INNER JOIN', 'language l', 'ttt.language_id = l.id')
 				->join('INNER JOIN', 'task_priority_translation tpt', 't.task_priority_id = tpt.task_priority_id')
 				->join('INNER JOIN', 'language l1', 'tpt.language_id = l1.id')
 				->join('LEFT JOIN', 'user u1', 't.user_id = u1.id')
 				->join('LEFT JOIN', 'user u2', 't.user_notified_id = u2.id')
 				->where(['t.id' => intval($id)])
    			->one();
    	
    	$referenceSchemas = (new Query())->select(['ttr.id', 'ttr.name_table', 'ttr.name_column', 'ttr.name_expression',
    				'ttr.url_details', 'td.referenced_object_id', 'ttrt.name'])
    			->from('task_data td')
    			->join('INNER JOIN', 'task_type_reference ttr', 'td.task_type_reference_id = ttr.id')
    			->join('INNER JOIN', 'task_type_reference_translation ttrt', 'ttr.id = ttrt.reference_id')
    			->join('INNER JOIN', 'language l', 'ttrt.language_id = l.id')
    			->where(['td.task_id' => intval($id)])
    			->andWhere(['l.symbol' => Yii::$app->language])
    			->orderBy(['ttr.id' => SORT_ASC])
    			->all();
    			
    	$data['results'] = TaskQuery::getTaskReferences($id);
    			 
    	return $data;
    }
    
    public static function getTaskReferences($id) {
    	$referenceSchemas = (new Query())->select(['ttr.id', 'ttr.name_table', 'ttr.name_column', 'ttr.name_expression',
    			'ttr.url_details', 'td.referenced_object_id', 'ttrt.name'])
    			->from('task_data td')
    			->join('INNER JOIN', 'task_type_reference ttr', 'td.task_type_reference_id = ttr.id')
    			->join('INNER JOIN', 'task_type_reference_translation ttrt', 'ttr.id = ttrt.reference_id')
    			->join('INNER JOIN', 'language l', 'ttrt.language_id = l.id')
    			->where(['td.task_id' => intval($id)])
    			->andWhere(['l.symbol' => Yii::$app->language])
    			->orderBy(['ttr.id' => SORT_ASC])
    			->all();
    			 
    	$results = [];
    	foreach($referenceSchemas as $schema) {
    		$row = Yii::$app->db->createCommand('
		    			SELECT '.$schema['name_expression'].' as name
		    			FROM `'.$schema['name_table'].'`
		    			WHERE '.$schema['name_column'].' = :id
		    		')
		    	    ->bindValues([
    	    				':id' => intval($schema['referenced_object_id'])
    	    		])
		    	    ->queryOne();
    	
    	    $results[] = [
    	    		'referenceId' => $schema['id'],
    	    		'url' => $schema['url_details'],
    	    		'label' => $schema['name'],
    	    		'name' => $row['name'],
    	    		'id' => $schema['referenced_object_id'],
    	    		'table' => $schema['name_table'],
    	 	];
    	}
    	
    	return $results;
    }
    
    public static function getFinishedTaskaCountInGivenInterval($beginning = null, $end = null) {
    	$dateTime1 = new DateTime($beginning);
    	$dateTime2 = new DateTime($end);
    	$interval = $dateTime1->diff($dateTime2);
    	 
    	$dateTime2->add(DateInterval::createFromDateString('1 day'));

    	$extractWhat = null;
    	
    	if ($interval->days == 365 || $interval->days == 366) {
    		$extractWhat = 'EXTRACT(MONTH FROM oh.date_creation) as time';
    		$maxCol = 11;
    	}
    	else {
    		$extractWhat = 'EXTRACT(DAY FROM oh.date_creation) as time';
    		$maxCol = $interval->days;
    		
    	}
    	
    	$query = (new Query())->select([$extractWhat, 'SUM(oof.count * oof.price) as number'])
	    	->from('order o')
	    	->join('INNER JOIN', 'order_offered_product oof', 'oof.order_id = o.id')
	    	->join('INNER JOIN', 'order_history oh', 'o.order_history_last_id = oh.id')
	    	->join('INNER JOIN', 'order_status os', 'oh.order_status_id = os.id')
	    	->where('os.is_terminal = 1')
	    	->andWhere(['NOT IN', 'os.symbol', [Utility::ORDER_STATUS_CANCELLED, Utility::ORDER_STATUS_REJECTED_BY_CLIENT]])
	    	->andWhere('oh.date_creation >= :beginning', [':beginning' => $dateTime1->format('Y-m-d')])
	    	->andWhere('oh.date_creation <= :end', [':end' => $dateTime2->format('Y-m-d')])
    		->groupBy('time');
    	
    	$col = 0;
    	
    	$queryResult = $query->all();
    	
    	$resultData = [];
    	$subtract = intval(substr($beginning, strlen($beginning) - 2));
    	
    	for($i = 0; $i < sizeof($queryResult); $i++) {
    		while($col != $queryResult[$i]['time'] - ($subtract)) {
    			$col++;
    			$resultData[] = 0;
    		}
    		$resultData[$col] = $queryResult[$i]['number'];
    	}
    	 
    	while($col != $maxCol) {
    		$col++;
    		$resultData[] = 0;
    	}
    	
    	return [$resultData];	
    }

    public static function getTaskDeadlineCountInGivenInterval($beginning = null, $end = null) {
    	$dateTime1 = new DateTime($beginning);
    	$dateTime2 = new DateTime($end);
    	$interval = $dateTime1->diff($dateTime2);
    	
    	$dateTime2->add(DateInterval::createFromDateString('1 day'));
    	
    	$subquery = (new Query())->select(['oh.date_deadline'])
    			->from('order o')
    			->join('INNER JOIN', 'order_history oh', 'o.order_history_last_id = oh.id')
    			->where(['o.is_active' => 1])
    			->andWhere(['o.is_deleted' => 0])
    			->andWhere('oh.date_deadline >= :beginning', [':beginning' => $dateTime1->format('Y-m-d')])
    			->andWhere('oh.date_deadline <= :end', [':end' => $dateTime2->format('Y-m-d')]);

    	
    	if ($interval->days == 365 || $interval->days == 366) {
    		$query = (new Query())->select(['count(date_deadline) as number', 'EXTRACT(MONTH FROM date_deadline) as time'])
    		->from('(' . $subquery->createCommand()->rawSql . ') as x')
    		->groupBy('time');
    		$maxCol = 11;
    	}
    	else {
    		$query = (new Query())->select(['count(date_deadline) as number', 'EXTRACT(DAY FROM date_deadline) as time'])
    		->from('(' . $subquery->createCommand()->rawSql . ') as x')
    		->groupBy('time');
    		$maxCol = $interval->days;
    	}
    	
    	$col = 0;
    	$queryResult = $query->all();

    	$resultData = [];
    	$subtract = intval(substr($beginning, strlen($beginning) - 2));
    	 
    	for($i = 0; $i < sizeof($queryResult); $i++) {
    		while($col != $queryResult[$i]['time'] - ($subtract)) {
    			$col++;
    			$resultData[] = 0;
    		}
    		$resultData[$col] = $queryResult[$i]['number'];
    	}
    	
    	while($col != $maxCol) {
    		$col++;
    		$resultData[] = 0;
    	}
    	return [$resultData]; 	
    }
    
    public static function getTaskReportData($dateFrom, $dateTo) {
    	return Yii::$app->db->createCommand('
    			SELECT t.title, t.message, t.date_reminder, t.date_deadline, ttt.name as task_type, tpt.name as task_priority, t.is_complete,
    						u.first_name, u.last_name, u.email, 
    						un.first_name as notified_first_name, un.last_name as notified_last_name, un.email as notified_email
    			FROM task t
    			LEFT JOIN task_type_translation ttt ON ttt.task_type_id = t.task_type_id
    			LEFT JOIN task_priority_translation tpt ON tpt.task_priority_id = t.task_priority_id
    			LEFT JOIN user u ON	u.id = t.user_id
    			LEFT JOIN user un on un.id = t.user_notified_id
    			LEFT JOIN language l on l.id = ttt.language_id
    			WHERE t.date_creation >= :dateFrom
					AND t.date_creation <= :dateTo
    				AND (l.symbol = :lSymbol OR l.symbol IS NULL)
    			ORDER BY t.date_creation
    			')
        			->bindValues([
        					':dateFrom' => $dateFrom,
        					':dateTo' => $dateTo,
        					'lSymbol' => Yii::$app->language,
        			])
        			->queryAll();
    }
    
    public static function getTasksByOrderId($orderId, $params = []) {
    	$query = (new Query())->select(['t.id', 't.title', 't.message', 'ttt.name as taskTypeName', 't.is_complete as isComplete',
    				'tpt.name as taskPriorityName', 't.date_deadline as dateDeadline', 't.date_creation as dateCreation',
    				'u.id as authorId', 'u.first_name as authorFirstName', 'u.last_name as authorLastName', 'u.email as authorEmail',
    				'u1.id as assignedId', 'u1.first_name as assignedFirstName', 'u1.last_name as assignedLastName', 'u1.email as assignedEmail',
    			])
    			->from('task t')
    			->join('INNER JOIN', 'task_type tt', 't.task_type_id = tt.id')
    			->join('INNER JOIN', 'task_type_translation ttt', 'tt.id = ttt.task_type_id')
    			->join('INNER JOIN', 'language l', 'ttt.language_id = l.id')
    			->join('INNER JOIN', 'task_priority tp', 't.task_priority_id = tp.id')
    			->join('INNER JOIN', 'task_priority_translation tpt', 'tp.id = tpt.task_priority_id')
    			->join('INNER JOIN', 'language l1', 'tpt.language_id = l1.id')
    			->join('INNER JOIN', 'task_type_reference ttr', '(tt.id = ttr.task_type_id AND ttr.name_table = "order" AND ttr.name_column = "id")')
    			->join('INNER JOIN', 'task_data td', '(ttr.id = td.task_type_reference_id AND t.id = td.task_id)')
    			->join('INNER JOIN', 'user u', 't.user_id = u.id')
    			->join('LEFT JOIN', 'user u1', 't.user_notified_id = u1.id')
    			->andWhere(['l.symbol' => Yii::$app->language])
    			->andWhere(['l1.symbol' => Yii::$app->language])
    			->andWhere(['td.referenced_object_id' => intval($orderId)]);
    		
		if(!Yii::$app->user->can(Utility::ROLE_ADMIN) || !Yii::$app->params['canAdminSeeAllTasks']) {
			$query->andWhere(['or',
					['t.user_notified_id' => null, 't.user_id' => intval(Yii::$app->user->id)],
					['t.user_notified_id' => intval(Yii::$app->user->id)],
			]);
		}
    			
    	$columnsMap = [
    			'title' => 't.title',
    			'type' => 'ttt.name',
    			'userAssigned' => 'COALESCE(CONCAT(u1.first_name, u1.last_name), CONCAT(u.first_name, u.last_name))',
    			'message' => 't.message',
    			'priority' => 'tpt.name',
    			'dateCreation' => 't.date_creation',
    			'dateDeadline' => 't.date_deadline',
    	];
    	
    	if(!empty($params['sortDir']) && !empty($columnsMap[$params['sortField']])) {
    		$query->orderBy([
    				$columnsMap[$params['sortField']] => (!empty($params['sortDir']) && $params['sortDir'] == 'desc') ? SORT_DESC : SORT_ASC
    		]);
    	}
    	else {
    		$query->orderBy([new Expression('-t.date_deadline DESC'), 't.date_creation' => SORT_DESC]);
    	}
    	
    	return $query;
    }
    
}
