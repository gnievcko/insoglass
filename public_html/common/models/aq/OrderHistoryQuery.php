<?php

namespace common\models\aq;

use Yii;
use yii\db\Query;

/**
 * This is the ActiveQuery class for [[\common\models\ar\OrderHistory]].
 *
 * @see \common\models\ar\OrderHistory
 */
class OrderHistoryQuery extends \yii\db\ActiveQuery {

    /*public function active() {
        $this->andWhere('[[status]]=1');
        return $this;
    }*/

    /**
     * @inheritdoc
     * @return \common\models\ar\OrderHistory[]|array
     */
    public function all($db = null) {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return \common\models\ar\OrderHistory|array|null
     */
    public function one($db = null) {
        return parent::one($db);
    }
    
    public static function getOrderEntries($id) {
    	return Yii::$app->db->createCommand('
    				SELECT u.email, u.first_name as firstName, u.last_name as lastName, ost.name as status, 
    					os.symbol as statusSymbol, oh.date_creation as dateCreation, oh.message,
    					oh.date_reminder as dateReminder, oh.date_deadline as dateDeadline, oh.modification, oh.id as historyId
    				FROM order_history oh
    				JOIN user u ON oh.user_id = u.id
    				JOIN order_status os ON oh.order_status_id = os.id
    				JOIN order_status_translation ost ON os.id = ost.order_status_id
    				JOIN language l ON ost.language_id = l.id
    				WHERE oh.order_id = :id
    				AND l.symbol = :languageSymbol
    				ORDER BY oh.date_creation DESC
    			')
        		->bindValues([
        				':id' => intval($id),
        				':languageSymbol' => Yii::$app->language,
        		])
        		->queryAll();
    }
    
    public static function getPreviousOrderHistory($id, $orderId) {
    	return (new Query())->select(['oh.id', 'oh.order_custom_data_id as orderCustomDataId'])
    			->from('order_history oh')
    			->where(['oh.order_id' => intval($orderId)])
    			->andWhere(['<>', 'oh.id', intval($id)])
    			->orderBy(['oh.id' => SORT_DESC])
    			->limit(1)
    			->one();
    }
    
    //public static function get
}