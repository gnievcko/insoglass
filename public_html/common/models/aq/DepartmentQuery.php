<?php

namespace common\models\aq;

use Yii;
use yii\db\Query;
use common\helpers\Utility;

/**
 * This is the ActiveQuery class for [[\common\models\ar\Department]].
 *
 * @see \common\models\ar\Department
 */
class DepartmentQuery extends \yii\db\ActiveQuery {

    /*public function active() {
        $this->andWhere('[[status]]=1');
        return $this;
    }*/

    /**
     * @inheritdoc
     * @return \common\models\ar\Department[]|array
     */
    public function all($db = null) {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return \common\models\ar\Department|array|null
     */
    public function one($db = null) {
        return parent::one($db);
    }

    public static function getDepartmentsQuery($params = []) {
        $columnsMap = [
            'name'=> 'dtr.name',
            'type'=> 'dttr.name',
            'id'=>'dp.id',
            'manager'=>'ud.is_lead',
        ];
        $query = new Query();
        $query->select(['id'=>'dp.id','type'=>'dttr.name','name'=> 'dtr.name'])
            ->from('department dp')
            ->join('INNER JOIN', 'department_type dpt', 'dpt.id = dp.department_type_id')
            ->join('INNER JOIN', 'department_translation dtr' , 'dp.id = dtr.department_id')
            ->join('INNER JOIN', 'language l', 'dtr.language_id = l.id')
            ->join('INNER JOIN', 'department_type_translation dttr' , 'dpt.id = dttr.department_type_id && l.id = dttr.language_id')
            ->andWhere(['l.symbol' => Yii::$app->language]);

        $query->leftJoin('user_department ud', 'dp.id = ud.department_id AND ud.is_lead = 1')
            ->leftJoin( 'user u', 'u.id = ud.user_id')
            ->addSelect(['manager'=> 'CONCAT_WS(" ",u.first_name,u.last_name)']);

        if(!empty($params['name'])){
            $query->andWhere(['like', 'dtr.name', $params['name']]);
        }
        
        if(!empty($params['typeId'])){
            $query->andWhere(['dp.department_type_id' => $params['typeId']]);
        }
        if(!empty($params['sortDir']) && !empty($columnsMap[$params['sortField']])) {
            $query->orderBy([
                $columnsMap[$params['sortField']] => (!empty($params['sortDir']) && $params['sortDir'] == 'desc') ? SORT_DESC : SORT_ASC
            ]);
        }
        return $query;
    }

    public static function getDetails($id) {
            return (new Query())->select(['dp.id AS id','dttr.name as type', 'dtr.name AS name'])
            ->from('department dp')
            ->join('INNER JOIN', 'department_type dpt', 'dpt.id = dp.department_type_id')
            ->join('INNER JOIN', 'department_translation dtr' , 'dp.id = dtr.department_id')
            ->join('INNER JOIN', 'department_type_translation dttr' , 'dpt.id = dttr.department_type_id')
            ->join('INNER JOIN', 'language l', 'dtr.language_id = l.id')
            ->andWhere(['dp.id' => intval($id)])
            ->andWhere(['l.symbol' => Yii::$app->language]) 
        ->one();
    }

    public static function getDepartmentsByIds() {
        $query = (new Query())->select(['dt.department_id as id', 'dt.name'])
                ->from('department_translation dt')
                ->innerJoin('language l', 'l.id = dt.language_id')
                ->leftJoin('user_department ud', 'ud.department_id = dt.department_id')
                ->leftJoin('user u', 'u.id = ud.user_id')
                ->where(['l.symbol' => Yii::$app->language]);
            
        if(!Yii::$app->user->can(Utility::ROLE_ADMIN) && !Yii::$app->user->can(Utility::ROLE_MAIN_WORKER)) {
            $query->andWhere(['u.id' => \Yii::$app->user->id]);
        }
        
        return $query->all();
    }
    
    public static function getDepartmentList() {
        return (new Query())
            ->select(['d.id as id', 'dt.name as name'])
            ->from('department d')
            ->join('INNER JOIN', 'department_translation dt', 'd.id = dt.department_id')
            ->join('INNER JOIN', 'language l', 'dt.language_id = l.id')
            ->where(['l.symbol' => Yii::$app->language])
            ->orderBy('dt.name ASC')
            ->all();
    }
}