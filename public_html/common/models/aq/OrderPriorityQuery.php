<?php

namespace common\models\aq;

use Yii;
use yii\db\Query;
use common\helpers\Utility;
/**
 * This is the ActiveQuery class for [[\common\models\ar\OrderPriority]].
 *
 * @see \common\models\ar\OrderPriority
 */
class OrderPriorityQuery extends \yii\db\ActiveQuery {

    /*public function active() {
        $this->andWhere('[[status]]=1');
        return $this;
    }*/

    /**
     * @inheritdoc
     * @return \common\models\ar\OrderPriority[]|array
     */
    public function all($db = null) {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return \common\models\ar\OrderPriority|array|null
     */
    public function one($db = null) {
        return parent::one($db);
    }
    
    public static function getAll() {
        return (new Query())->select(['op.id', 'op.symbol', 'opt.name'])
                ->from('order_priority op')
                ->innerJoin('order_priority_translation opt', 'op.id = opt.order_priority_id')
                ->innerJoin('language l', 'opt.language_id = l.id')
                ->where(['l.symbol' => Yii::$app->language])
                ->orderBy(['op.priority' => SORT_ASC])
                ->all();
    }
    
    public static function getIdBySymbol($symbol) {
        return (new Query())->select(['op.id'])
                ->from('order_priority op')
                ->where(['op.symbol' => $symbol])
                ->scalar();
    }
}