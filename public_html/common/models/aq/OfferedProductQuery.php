<?php

namespace common\models\aq;

use Yii;
use yii\db\Expression;
use yii\db\Query;
use common\helpers\Utility;

/**
 * This is the ActiveQuery class for [[\common\models\ar\OfferedProduct]].
 *
 * @see \common\models\ar\OfferedProduct
 */
class OfferedProductQuery extends \yii\db\ActiveQuery {

	const MODE_OFFERED = 1;
	const MODE_WAREHOUSE_VISIBLE = 2;
	
    /*public function active() {
        $this->andWhere('[[status]]=1');
        return $this;
    }*/

    /**
     * @inheritdoc
     * @return \common\models\ar\OfferedProduct[]|array
     */
    public function all($db = null) {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return \common\models\ar\OfferedProduct|array|null
     */
    public function one($db = null) {
        return parent::one($db);
    }
    
    public static function getActiveOfferedProductsQuery($params = []) {
    	$queryOfferedProducts = self::getListQueryForProductsOfType(self::MODE_OFFERED, $params);
    	$queryProducts = self::getListQueryForProductsOfType(self::MODE_WAREHOUSE_VISIBLE, $params);    	
	    $resultQuery = (new Query())->from(['table' => $queryOfferedProducts->union($queryProducts)]);
    	
    	$columnsMap = [
    			'name' => 'name',
    	        'symbol' => 'symbol',
    			'isAvailable' => 'isAvailable',
    			'category' => 'category',
    			'version' => 'version',
    	];
    	 
    	if(!empty($params['sortDir']) && !empty($columnsMap[$params['sortField']])) {
    		$resultQuery->orderBy([
    				$columnsMap[$params['sortField']] => (!empty($params['sortDir']) && $params['sortDir'] == 'desc') ? SORT_DESC : SORT_ASC
    		]);
    	}
    	
    	return $resultQuery;
    }
    
    private static function getListQueryForProductsOfType($type, $params) {
    	$dbName = $type == self::MODE_OFFERED ? 'offered_product' : 'product';
    	
    	$photoSubquery = (new Query())->select(['opp.url_photo'])->from($dbName.'_photo opp')
		    	->where('opp.'.$dbName.'_id = op.id')
		    	->orderBy(['opp.priority' => SORT_ASC, 'opp.id' => SORT_ASC])->limit(1);
    	 
    	$categorySubquery = (new Query())->select(['GROUP_CONCAT(opct.name ORDER BY opct.name SEPARATOR ", ")'])
		    	->from($dbName.'_category_set opcs')
		    	->join('INNER JOIN', $dbName.'_category opc', 'opcs.'.$dbName.'_category_id = opc.id')
		    	->join('INNER JOIN', $dbName.'_category_translation opct', 'opc.id = opct.'.$dbName.'_category_id')
		    	->join('INNER JOIN', 'language l', 'opct.language_id = l.id')
		    	->where(['l.symbol' => Yii::$app->language])
		    	->andWhere('opcs.'.$dbName.'_id = op.id');
    	
    	$versionSubquery = (new Query())->select(['GROUP_CONCAT(opvt.name ORDER BY opvt.name SEPARATOR ", ")'])
		    	->from($dbName.' opv')
		    	->join('INNER JOIN', $dbName.'_translation opvt', 'opv.id = opvt.'.$dbName.'_id')
		    	->join('INNER JOIN', 'language l', 'opvt.language_id = l.id')
		    	->where(['l.symbol' => Yii::$app->language])
		    	->andWhere(['opv.is_active' => true])
		    	->andWhere('opv.parent_'.$dbName.'_id = op.id')
    			->orderBy(['opv.priority' => SORT_ASC, 'opvt.name' => SORT_ASC]);
    	 
    	$query = (new Query())->distinct()
		    	->select([
		    			'op.id', 'op.symbol', 'opt.name', 'op.date_creation as dateCreation', 'op.is_available as isAvailable',
		    			'urlPhoto' => $photoSubquery, 'category' => $categorySubquery, 'version' => $versionSubquery
		    	])
		    	->from($dbName.' op')
		    	->join('INNER JOIN', $dbName.'_translation opt', 'op.id = opt.'.$dbName.'_id')
		    	->join('INNER JOIN', 'language l', 'opt.language_id = l.id')
		    	->where([
		    			'l.symbol' => \Yii::$app->language,
		    			'op.is_active' => true,
		    			'op.parent_'.$dbName.'_id' => null
    			]);
		    	
		if($type == self::MODE_OFFERED) {
			$query->andWhere(['op.is_artificial' => false]);
			$query->addSelect([new Expression('"'.Utility::PRODUCT_TYPE_OFFERED_PRODUCT.'" as type')]);
		}
		else {
			$query->andWhere(['op.is_visible_for_client' => true]);
			$query->addSelect([new Expression('"'.Utility::PRODUCT_TYPE_PRODUCT.'" as type')]);
		}
    	
    	if(!empty($params['name'])) {
    		$query->andWhere(['like', 'opt.name', $params['name']]);
    	}
    	
    	$tempCategoryIds = array_filter(empty($params['categoryIds']) ? [] : $params['categoryIds'], function($id) {
    		return !empty($id);
    	});
    	
    	$categoryIds = [];
    	foreach($tempCategoryIds as $tcid) {
	    	$firstLetter = substr($tcid, 0, 1);
			if(($type == self::MODE_OFFERED && $firstLetter == 'o') || 
				($type == self::MODE_WAREHOUSE_VISIBLE && $firstLetter == 'p')) {
				$categoryIds[] = substr($tcid, 1);
    		}
    	}    	
    	
    	if(!empty($tempCategoryIds) && count($tempCategoryIds) < intval($params['allCategoriesCount'])) {
    		$subquery = (new Query())->select(['opcs.'.$dbName.'_id'])->from($dbName.'_category_set opcs')
    				->where('opcs.'.$dbName.'_id = op.id')
    				->andWhere(['opcs.'.$dbName.'_category_id' => $categoryIds]);
    	
    		$query->andWhere(['exists', $subquery]);
    	}
    	
    	return $query;
    }
    
    public static function getDetails($id, $type) {
    	$dbName = $type == Utility::PRODUCT_TYPE_OFFERED_PRODUCT ? 'offered_product' : 'product';    	
    	
    	return Yii::$app->db->createCommand('
    				SELECT op.id, op.symbol as symbol, opt.name, opt.description, op.is_available as isAvailable, op.date_creation as dateCreation,
    					opt.price'.($type == Utility::PRODUCT_TYPE_PRODUCT ? '_default' : '').' as price, c.short_symbol as currency,
    					u.email, u.first_name as firstName, u.last_name as lastName, 
    					opt.unit'.($type == Utility::PRODUCT_TYPE_PRODUCT ? '_default' : '').' as unit, op.width, op.height, st.name as shape,
    					(
							SELECT GROUP_CONCAT(opct.name SEPARATOR ", ")
							FROM '.$dbName.'_category_set opcs
							JOIN '.$dbName.'_category opc ON opcs.'.$dbName.'_category_id = opc.id
							JOIN '.$dbName.'_category_translation opct ON opc.id = opct.'.$dbName.'_category_id
							JOIN language l1 ON opct.language_id = l1.id
							WHERE l1.symbol = :languageSymbol
							AND opcs.'.$dbName.'_id = op.id
						) as category,
    					(
							SELECT GROUP_CONCAT(opvt.name SEPARATOR ", ")
							FROM '.$dbName.' opv
							JOIN '.$dbName.'_translation opvt ON opv.id = opvt.'.$dbName.'_id
							JOIN language l1 ON opvt.language_id = l1.id
							WHERE l1.symbol = :languageSymbol
							AND opv.is_active = 1
							AND opv.parent_'.$dbName.'_id = op.id
							ORDER BY opv.priority ASC, opvt.name ASC
						) as version,
    					"'.$type.'" as type
    				FROM '.$dbName.' op
    				JOIN '.$dbName.'_translation opt ON op.id = opt.'.$dbName.'_id
    				JOIN user u ON op.user_id = u.id
    				JOIN language l ON opt.language_id = l.id
    				LEFT JOIN currency c ON opt.currency_id = c.id
                    LEFT JOIN shape_translation st on op.shape_id = st.shape_id
                    LEFT JOIN language l2 on st.language_id = l2.id
    				WHERE op.id = :id
    				AND l.symbol = :languageSymbol
                    AND (l2.symbol = :languageSymbol OR l2.symbol IS NULL)
    			')
        		->bindValues([
        				':id' => intval($id),
        				':languageSymbol' => Yii::$app->language,
        		])
        		->queryOne();
    }

    public static function findOfferedProducts($params) {
        $productsQuery = (new \yii\db\Query())
	            ->select(['p.id', 'pt.name as name', 'pt1.name as parentName', 'pt.price_default as price', 
	            		'c.short_symbol', 'pt.currency_id', 'pt.unit_default as unit',
	            		new Expression('"'.Utility::PRODUCT_TYPE_PRODUCT.'" as type'), 
	                'p.shape_id as shapeId', 'p.width', 'p.height',
	            ])
	            ->from('product p')
	            ->join('INNER JOIN', 'product_translation pt', 'p.id = pt.product_id')
	            ->join('INNER JOIN', 'language l', 'pt.language_id = l.id')
	            ->join('LEFT JOIN', 'product p1', 'p.parent_product_id = p1.id')
	            ->join('LEFT JOIN', 'product_translation pt1', 'p1.id = pt1.product_id')
	            ->join('LEFT JOIN', 'language l1', 'pt1.language_id = l1.id')
	            ->join('LEFT JOIN', 'currency c', 'c.id = pt.currency_id')
	            ->andWhere('p.is_active')
	            ->andWhere('p.is_visible_for_client')
        		->andWhere(['l.symbol' => Yii::$app->language])
    			->andWhere(['or', 'l1.symbol IS NULL', ['l1.symbol' => Yii::$app->language]]);

        $filterName = !empty($params['name']);
        if($filterName) {
            if(Yii::$app->params['isProductSymbolEditable']) {
                $productsQuery->andWhere(['or', 
                    ['like', 'pt.name', $params['name']],
                    ['like', 'p.symbol', $params['name']],
                ]);
            }
            else {
                $productsQuery->andWhere(['like', 'pt.name', $params['name']]);
            }
        }

        $offeredProductsQuery = (new \yii\db\Query())
	            ->select(['op.id', 'opt.name', 'opt1.name as parentName', 'opt.price as price', 
	            		'c.short_symbol', 'opt.currency_id', 'opt.unit',
	            		new Expression('"'.Utility::PRODUCT_TYPE_OFFERED_PRODUCT.'" as type'),
                        'op.shape_id as shapeId', 'op.width', 'op.height',
	            ])
	            ->from('offered_product op')
	            ->join('INNER JOIN', 'offered_product_translation opt', 'op.id = opt.offered_product_id')
	            ->join('INNER JOIN', 'language l', 'opt.language_id = l.id')
	            ->join('LEFT JOIN', 'currency c', 'c.id = opt.currency_id')
	            ->join('LEFT JOIN', 'offered_product op1', 'op.parent_offered_product_id = op1.id')
	            ->join('LEFT JOIN', 'offered_product_translation opt1', 'op1.id = opt1.offered_product_id')
	            ->join('LEFT JOIN', 'language l1', 'opt1.language_id = l1.id')
	            ->andWhere('op.is_active')
                ->andWhere('!op.is_artificial')
                ->andWhere(['l.symbol' => Yii::$app->language])
                ->andWhere(['or', 'l1.symbol IS NULL', ['l1.symbol' => Yii::$app->language]]);
        
        if($filterName) {
            if(Yii::$app->params['isProductSymbolEditable']) {
                $offeredProductsQuery->andWhere(['or',
                    ['like', 'opt.name', $params['name']],
                    ['like', 'op.symbol', $params['name']],
                ]);
            }
            else {
                $offeredProductsQuery->andWhere(['like', 'opt.name', $params['name']]);
            }
        }

        if(Yii::$app->params['isProductCanBeOffered']) { 
            return (new Query())->from(['t' => $productsQuery->union($offeredProductsQuery)])->orderBy(['name' => SORT_ASC]);
        }
        else {
            return $offeredProductsQuery->orderBy(['opt.name' => SORT_ASC]);
        }
    }
    
    public static function getAttachments($offeredProductId) {
    	return Yii::$app->db->createCommand('
    				SELECT opa.id as id, opa.url_file as urlFile, opa.name, opa.description, opa.date_creation as dateCreation,
    					u.first_name as firstName, u.last_name as lastName
					FROM offered_product_attachment opa
					JOIN user u ON opa.user_id = u.id
					WHERE opa.offered_product_id = :offeredProductId
    				ORDER BY opa.date_creation
    			')
        		->bindValues([
        				':offeredProductId' => intval($offeredProductId),
        		])
        		->queryAll();
    }
    
    public static function getBySymbolAndName($symbol, $name) {
        return (new Query())->select(['op.id'])
                ->from('offered_product op')
                ->innerJoin('offered_product_translation opt', 'op.id = opt.offered_product_id')
                ->innerJoin('language l', 'opt.language_id = l.id')
                ->where(['l.symbol' => Yii::$app->language])
                ->andWhere(['op.symbol' => $symbol])
                ->andWhere(['opt.name' => $name])
                ->one();
    }
    
    public static function getBySymbol($symbol) {
        return (new Query())->select(['op.id'])
                ->from('offered_product op')
                ->Where(['op.symbol' => $symbol])
                ->one();
    }
    
    public static function getRequiredProductsQuery($id, $params = []) {
        $query = (new Query())->select(['opr.product_required_id as id', 'opr.count', 'pt.name', 
                    'pt1.name as parentName', 'opr.note as note', 'opr.unit',
                ])
                ->from('offered_product_requirement opr')
                ->innerJoin('product p', 'opr.product_required_id = p.id')
                ->innerJoin('product_translation pt', 'pt.product_id = p.id')
                ->innerJoin('language l', 'l.id = pt.language_id')
                ->leftJoin('product p1', 'p.parent_product_id = p1.id')
                ->leftJoin('product_translation pt1', 'p1.id = pt1.product_id')
                ->leftJoin('language l1', 'pt1.language_id = l1.id')
                ->andWhere(['l.symbol' => Yii::$app->language])
                ->andWhere(['or', ['l1.symbol' => null], ['l1.symbol' => \Yii::$app->language]])
                ->andWhere(['opr.offered_product_id' => intval($id)]);
    
        if(!empty($params['name'])) {
            $query->andWhere(['like', 'pt.name', $params['name']]);
        }
    
        $columnsMap = [
            'name' => 'pt.name',
            'count' => 'opr.count',
            'note' => 'opr.note',
            'unit' => 'opr.unit',
        ];
    
        if(!empty($params['sortDir']) && !empty($columnsMap[$params['sortField']])) {
            $query->orderBy([
                $columnsMap[$params['sortField']] => (!empty($params['sortDir']) && $params['sortDir'] == 'desc') ? SORT_DESC : SORT_ASC
            ]);
        }
    
        return $query;
    }
}
