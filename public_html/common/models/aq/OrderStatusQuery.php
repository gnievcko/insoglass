<?php

namespace common\models\aq;

use Yii;
use yii\db\Query;

/**
 * This is the ActiveQuery class for [[\common\models\ar\OrderStatus]].
 *
 * @see \common\models\ar\OrderStatus
 */
class OrderStatusQuery extends \yii\db\ActiveQuery {

    /*public function active() {
        $this->andWhere('[[status]]=1');
        return $this;
    }*/

    /**
     * @inheritdoc
     * @return \common\models\ar\OrderStatus[]|array
     */
    public function all($db = null) {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return \common\models\ar\OrderStatus|array|null
     */
    public function one($db = null) {
        return parent::one($db);
    }
    
    public static function getOrderStatusAndOrderCountQuery() {
    	$countSubquery = (new Query())->select(['COUNT(o.id)'])
    			->from('order o')
    			->join('INNER JOIN', 'order_history oh', 'o.order_history_last_id = oh.id')
    			->where('oh.order_status_id = os.id')
    			->andWhere('o.is_active = 1')
    			->andWhere('o.is_deleted = 0');
    
    	return (new Query())->select(['ost.name', 'number' => $countSubquery])
    			->from('order_status os')
    			->join('INNER JOIN', 'order_status_translation ost', 'os.id = ost.order_status_id')
    			->join('INNER JOIN', 'language l', 'ost.language_id = l.id')
    			->where(['is_terminal' => 0])
    			->andWhere(['l.symbol' => Yii::$app->language])
    			->orderBy('number DESC')->all();
    }

	public static function isOrderStatusPositiveTerminal($orderStatusId) {
		return (new Query())->select(['is_positive'])->from('order_status')->where(['id' => $orderStatusId])->scalar();
	}
	
	public static function getBySymbol($symbol) {
	    return (new Query())->select(['os.id'])
	           ->from('order_status os')
	           ->where(['symbol' => $symbol])
	           ->one();
	}
}
