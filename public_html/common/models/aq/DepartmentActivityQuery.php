<?php

namespace common\models\aq;
use yii\db\Query;
use yii;

/**
 * This is the ActiveQuery class for [[\common\models\ar\DepartmentActivity]].
 *
 * @see \common\models\ar\DepartmentActivity
 */
class DepartmentActivityQuery extends \yii\db\ActiveQuery {

    /*public function active() {
        $this->andWhere('[[status]]=1');
        return $this;
    }*/

    /**
     * @inheritdoc
     * @return \common\models\ar\DepartmentActivity[]|array
     */
    public function all($db = null) {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return \common\models\ar\DepartmentActivity|array|null
     */
    public function one($db = null) {
        return parent::one($db);
    }
    public function getActivitiesByIds($activityIds){
        $query = (new Query())
            ->select(['a.id as id','atr.name as name'])
            ->from('department_activity da')
            ->join('INNER JOIN', 'activity a', 'da.activity_id = a.id')
            ->join('INNER JOIN', 'activity_translation atr', 'a.id = atr.activity_id')
            ->join('INNER JOIN', 'language l', 'atr.language_id = l.id')
            ->where(['l.symbol' => Yii::$app->language])
            ->andWhere(['in','a.id', (empty($activityIds)?[]: $activityIds)])
            ->all();
            return $query;
    }
    public function getActivitiesByDepartmentId($id){
        $query = (new Query())
            ->select(['a.id as id','atr.name as name'])
            ->from('department_activity da')
            ->join('INNER JOIN', 'activity a', 'da.activity_id = a.id')
            ->join('INNER JOIN', 'activity_translation atr', 'a.id = atr.activity_id')
            ->join('INNER JOIN', 'language l', 'atr.language_id = l.id')
            ->where(['l.symbol' => Yii::$app->language])
            ->andWhere(['da.department_id'=>intval($id)])
            ->all();
        return $query;
    }
}