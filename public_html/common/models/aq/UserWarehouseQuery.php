<?php

namespace common\models\aq;

use Yii;
use common\models\ar\Role;
use common\models\ar\User;
use common\helpers\Utility;
use common\models\ar\UserWarehouse;
use common\models\ar\Warehouse;
use yii\db\Query;

/**
 * This is the ActiveQuery class for [[\common\models\ar\UserWarehouse]].
 *
 * @see \common\models\ar\UserWarehouse
 */
class UserWarehouseQuery extends \yii\db\ActiveQuery {

    /*public function active() {
        $this->andWhere('[[status]]=1');
        return $this;
    }*/

    /**
     * @inheritdoc
     * @return \common\models\ar\UserWarehouse[]|array
     */
    public function all($db = null) {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return \common\models\ar\UserWarehouse|array|null
     */
    public function one($db = null) {
        return parent::one($db);
    }
    
    public static function getAvailableWarehouses() {
    	$user = Yii::$app->user;
    	if($user->can(Utility::ROLE_MAIN_STOREKEEPER)) {
    		return Warehouse::find()->select('id')->where(['is_active' => true])->column();
    	}
    	
    	return UserWarehouse::find()->joinWith('warehouse w')->select('warehouse_id')
    			->where('w.is_active = 1 AND user_id = :userId', [':userId' => intval($user->id)])
    			->column();
    }
    
    public function getWarehousesByUserId($id){
    	$query = (new Query())
    			->select('w.name')
    			->from('warehouse w')
    			->join('INNER JOIN', 'user_warehouse uw', 'w.id = uw.warehouse_id')
    			->where(['uw.user_id' => $id]); 
    	return $query;
    }

    public function getWarehouseIdsByUserId($id){
    	$query = (new Query())
    	->select('w.id')
    	->from('warehouse w')
    	->join('INNER JOIN', 'user_warehouse uw', 'w.id = uw.warehouse_id')
    	->where(['uw.user_id' => $id]);
    	return $query->all();
    }
    
}