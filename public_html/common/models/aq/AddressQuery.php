<?php

namespace common\models\aq;

/**
 * This is the ActiveQuery class for [[\common\models\ar\Address]].
 *
 * @see \common\models\ar\Address
 */
class AddressQuery extends \yii\db\ActiveQuery {

    /*public function active() {
        $this->andWhere('[[status]]=1');
        return $this;
    }*/

    /**
     * @inheritdoc
     * @return \common\models\ar\Address[]|array
     */
    public function all($db = null) {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return \common\models\ar\Address|array|null
     */
    public function one($db = null) {
        return parent::one($db);
    }
}