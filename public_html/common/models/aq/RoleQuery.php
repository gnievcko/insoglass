<?php

namespace common\models\aq;

use Yii;
use common\helpers\Utility;
use common\helpers\RoleHelper;
use yii\db\Query;


/**
 * This is the ActiveQuery class for [[\common\models\ar\Role]].
 *
 * @see \common\models\ar\Role
 */
class RoleQuery extends \yii\db\ActiveQuery {

    /*public function active() {
        $this->andWhere('[[status]]=1');
        return $this;
    }*/

    /**
     * @inheritdoc
     * @return \common\models\ar\Role[]|array
     */
    public function all($db = null) {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return \common\models\ar\Role|array|null
     */
    public function one($db = null) {
        return parent::one($db);
    }
    
    public static function findAvailableRoles($params = []) {
		$roleSymbols = [];
    	
    	if(Yii::$app->user->can(Utility::ROLE_ADMIN)) {
    		$roleSymbols = RoleHelper::getEmployeeRoles();
    	}
    	else { 
    		if(Yii::$app->user->can(Utility::ROLE_SALESMAN)) {
    			if(Yii::$app->user->can(Utility::ROLE_MAIN_SALESMAN)) {
    				$roleSymbols = array_merge($roleSymbols, RoleHelper::getSalesmanRoles());	
    			}
    			else {
    				$roleSymbols = array_merge($roleSymbols, [ Utility::ROLE_SALESMAN ]);
    			}
    		}
    		
    		if(Yii::$app->user->can(Utility::ROLE_STOREKEEPER)) {
    			if(Yii::$app->user->can(Utility::ROLE_MAIN_STOREKEEPER)) {
    				$roleSymbols = array_merge($roleSymbols, RoleHelper::getStorekeeperRoles());
    			}
    			else {
    				$roleSymbols = array_merge($roleSymbols, [ Utility::ROLE_STOREKEEPER ]);
    			}
    		}
    	}
    	 
    	$query = (new Query())
    	 		->select(['r.id', 'rt.name'])
    	 		->from('role r')
    	 		->join('INNER JOIN', 'role_translation rt', 'r.id = rt.role_id')
    	 		->join('INNER JOIN', 'language l', 'rt.language_id = l.id')
    	 		->where('l.symbol = :symbol', [':symbol' => Yii::$app->language])
    	 		->andWhere(['r.symbol' => $roleSymbols])
                ->andWhere(['r.is_visible' => 1])
    	 		->orderBy(['r.symbol' => SORT_ASC]);
  		
    	if(!empty($params['term'])) {
    		$query->andWhere(['like', 'rt.name', $params['term']]);
    	}  

    	return $query;
	}
	
	public function getRolesTranslationsByUserId($id) {
		$query = (new Query())
				->select(['GROUP_CONCAT(rt.name SEPARATOR ", ") as name'])
				->from('user_role ur')
				->join('INNER JOIN', 'role_translation rt', 'rt.role_id = ur.role_id')
				->join('INNER JOIN', 'language l', 'l.id = rt.language_id')
				->where(['l.symbol' => Yii::$app->language])
				->andWhere(['ur.user_id' => $id]);

		return $query;			
	}
	
	public function getRolesByUserIdQuery($id) {
		$query = (new Query())
				->select(['ur.role_id as id', 'rt.name'])
				->from('user_role ur')
				->join('INNER JOIN', 'role_translation rt', 'rt.role_id = ur.role_id')
				->join('INNER JOIN', 'language l', 'l.id = rt.language_id')
				->where(['l.symbol' => Yii::$app->language])
				->andWhere(['ur.user_id' => $id]);
		return $query;
	}
    	
	
	
}