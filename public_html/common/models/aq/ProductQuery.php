<?php

namespace common\models\aq;

use Yii;
use yii\db\Query;
/**
 * This is the ActiveQuery class for [[\common\models\ar\Product]].
 *
 * @see \common\models\ar\Product
 */
class ProductQuery extends \yii\db\ActiveQuery {

    /*public function active() {
        $this->andWhere('[[status]]=1');
        return $this;
    }*/

    /**
     * @inheritdoc
     * @return \common\models\ar\Product[]|array
     */
    public function all($db = null) {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return \common\models\ar\Product|array|null
     */
    public function one($db = null) {
        return parent::one($db);
    }
    
    public static function getActiveProductsQuery($params = []) {
    	$photoSubquery = (new Query())->select(['pp.url_photo'])->from('product_photo pp')
    			->where('pp.product_id = p.id')
    			->orderBy(['pp.priority' => SORT_ASC, 'pp.id' => SORT_ASC])->limit(1);
    
    	$categorySubquery = (new Query())->select(['GROUP_CONCAT(pct.name ORDER BY pct.name SEPARATOR ", ")'])
		    	->from('product_category_set pcs')
		    	->join('INNER JOIN', 'product_category pc', 'pcs.product_category_id = pc.id')
		    	->join('INNER JOIN', 'product_category_translation pct', 'pc.id = pct.product_category_id')
		    	->join('INNER JOIN', 'language l1', 'pct.language_id = l1.id')
		    	->where(['l1.symbol' => Yii::$app->language])
		    	->andWhere('pcs.product_id = p.id');
    	 
    	$versionSubquery = (new Query())->select(['GROUP_CONCAT(pvt.name ORDER BY pvt.name SEPARATOR ", ")'])
		    	->from('product pv')
		    	->join('INNER JOIN', 'product_translation pvt', 'pv.id = pvt.product_id')
		    	->join('INNER JOIN', 'language l1', 'pvt.language_id = l1.id')
		    	->where(['l1.symbol' => Yii::$app->language])
		    	->andWhere(['pv.is_active' => true])
		    	->andWhere('pv.parent_product_id = p.id')
		    	->orderBy(['pv.priority' => SORT_ASC, 'pvt.name' => SORT_ASC]);
    	
		/*$aliasSubquery = (new Query())->select(['GROUP_CONCAT(pa.name ORDER BY pa.name SEPARATOR ", ")'])
				->from('product_alias pa')
				->join('INNER JOIN', 'language l1', 'pa.language_id = l1.id')
				->where(['l1.symbol' => Yii::$app->language])
				->andWhere('pa.product_id = p.id')
				->orderBy(['pa.name' => SORT_ASC]);*/
    
    	$query = (new Query())
		    	->select([
		    			'p.id', 'pt.name', 'p.index', 'wp.stillage', 'wp.shelf', 'p.is_visible_for_client as isVisibleForClient', 
		    			'UNIX_TIMESTAMP(p.date_creation) as dateCreation', 'urlPhoto' => $photoSubquery, 
		    			'category' => $categorySubquery, 'version' => $versionSubquery/*, 'alias' => $aliasSubquery*/,
		    			'ws.name as supplier',
		    	])
		    	->from('product p')
		    	->join('INNER JOIN', 'product_translation pt', 'p.id = pt.product_id')
		    	->join('INNER JOIN', 'language l', 'pt.language_id = l.id')
		    	->join('LEFT JOIN', 'warehouse_product wp', 'wp.product_id = p.id')
		    	->join('LEFT JOIN', 'warehouse_supplier ws', 'p.warehouse_supplier_id = ws.id')
		    	->where([
		    			'l.symbol' => \Yii::$app->language,
		    			'p.is_active' => true,
		    			'p.parent_product_id' => null
		    	]);    	
    	 
    	if(!empty($params['name'])) {
    		$subquery = (new Query())->select(['pa.id'])
    				->from('product_alias pa')
    				->join('INNER JOIN', 'language l1', 'pa.language_id = l1.id')
    				->where(['l1.symbol' => Yii::$app->language])
    				->andWhere('pa.product_id = p.id')
    				->andWhere(['like', 'pa.name', $params['name']]);
    		
    		$query->andWhere(['or', 
    				['like', 'pt.name', $params['name']],
    				['exists', $subquery],
    		]);
    	}
    	 
    	$categoryIds = array_filter(empty($params['categoryIds']) ? [] : $params['categoryIds'], function($id) {
    		return !empty($id);
    	});

    	if(!empty($params['categoryIds'])){
	    	$includeNoCategory = false;
	    	if(($params['categoryIds'][0] !== '')) {
	    		if(in_array(0, $params['categoryIds'])) {
	    			$includeNoCategory = true;
	    		}
	    	}
	    	
	    	$subquery = (new Query())->select(['pr.id'])
	    			->from('product_category_set pcs')
	    			->join('RIGHT JOIN', 'product pr', 'pr.id = pcs.product_id')
	    			->where(['pcs.product_category_id' => $categoryIds]);
	    	
	    	if($includeNoCategory) {
	    		$subquery->orWhere(['pcs.product_category_id' => NULL]);
	    	}
	    	
	    	$query->andWhere(['p.id' => $subquery]);
    	}
    	
    	if(!empty($params['location'])){
    		$query->andWhere(['or',
    				['like', 'stillage', $params['location']],
    				['like', 'shelf', $params['location']]
    		]);	
    	}
    	
    	$columnsMap = [
    			'name' => 'pt.name',
    			'index' => 'p.index',
    			'isVisibleForClient' => 'p.is_visible_for_client',
    			'stillageShelf' => 'CONCAT(wp.stillage, "/", wp.shelf)',
    			'dateCreation' => 'p.date_creation',
    			//'alias' => '('.$aliasSubquery->createCommand()->rawSql.')',
    			'version' => '('.$versionSubquery->createCommand()->rawSql.')',
    			'category' => '('.$categorySubquery->createCommand()->rawSql.')',
    			'supplier' => 'ws.name',
    	];
    
    	if(!empty($params['sortDir']) && !empty($columnsMap[$params['sortField']])) {
    		$query->orderBy([
    				$columnsMap[$params['sortField']] => (!empty($params['sortDir']) && $params['sortDir'] == 'desc') ? SORT_DESC : SORT_ASC
    		]);
    	}
    	 
    	return $query;
    }
    
    public static function getDetails($id) {
    	return Yii::$app->db->createCommand('
    				SELECT p.id, p.index, pt.name, pt.description, p.is_available as isAvailable, 
    					ws.id as manufacturerId, ws.name as manufacturerName, ws.contact_person as manufacturerContactPerson, ws.phone as manufacturerPhone, ws.email as manufacturerEmail, 
    					a.main as manufacturerAddress, a.complement as manufacturerComplement, ct.name as manufacturerCity, ct.zip_code as manufacturerZipCode, pv.name as manufacturerProvince, cnt.name as manufacturerCountry,  
    					p.is_visible_for_client as isVisibleForClient, 
    					p.date_creation as dateCreation, pt.price_default as price, c.short_symbol as currency, p.count_minimal as countMinimal,
    					u.email, u.first_name as firstName, u.last_name as lastName, wp.stillage, wp.shelf, pt.unit_default as unit, 
    					wp.price_unit as priceLastDelivery, c1.short_symbol as currencyLastDelivery,
    					(
							SELECT GROUP_CONCAT(pct.name ORDER BY pct.name SEPARATOR ", ")
							FROM product_category_set pcs
							JOIN product_category pc ON pcs.product_category_id = pc.id
							JOIN product_category_translation pct ON pc.id = pct.product_category_id
							JOIN language l1 ON pct.language_id = l1.id
							WHERE l1.symbol = :languageSymbol
							AND pcs.product_id = p.id
						) as category,
    					(
							SELECT GROUP_CONCAT(pvt.name ORDER BY pvt.name SEPARATOR ", ")
							FROM product pv
							JOIN product_translation pvt ON pv.id = pvt.product_id
							JOIN language l1 ON pvt.language_id = l1.id
							WHERE l1.symbol = :languageSymbol
							AND pv.is_active = 1
							AND pv.parent_product_id = p.id
							ORDER BY pv.priority ASC, pvt.name ASC
						) as version,
    					(
    						SELECT GROUP_CONCAT(pa.name ORDER BY pa.name SEPARATOR ", ")
    						FROM product_alias pa
    						JOIN language l1 ON pa.language_id = l1.id
    						WHERE l1.symbol = :languageSymbol
    						AND pa.product_id = p.id
    						ORDER BY pa.name
    					) as alias
    				FROM product p   
    				JOIN product_translation pt ON p.id = pt.product_id
    				JOIN user u ON p.user_id = u.id
    				JOIN language l ON pt.language_id = l.id
                    JOIN warehouse_product wp ON wp.product_id = p.id
    				LEFT JOIN currency c ON pt.currency_id = c.id
    				LEFT JOIN warehouse_supplier ws ON ws.id = p.warehouse_supplier_id
    				LEFT JOIN address a ON ws.address_id = a.id
    				LEFT JOIN city ct ON a.city_id = ct.id
    				LEFT JOIN province pv ON ct.province_id = pv.id
    				LEFT JOIN country cn ON pv.country_id = cn.id
    				LEFT JOIN country_translation cnt ON cn.id = cnt.country_id
    				LEFT JOIN language l1 ON cnt.language_id = l1.id
    				LEFT JOIN currency c1 ON wp.currency_id = c1.id
    				WHERE p.id = :id
    				AND l.symbol = :languageSymbol
    				AND (l1.symbol IS NULL OR l1.symbol = :languageSymbol)
    			')
        		->bindValues([
        				':id' => intval($id),
        				':languageSymbol' => Yii::$app->language,
        		])
        		->queryOne();
    }
    
    public static function findProducts($params) {
    	$query = (new Query())->select(['p.id', 'pt.name as name', 'pt1.name as parentName'])
    			->from('product p')
    			->join('INNER JOIN', 'product_translation pt', 'p.id = pt.product_id')
    			->join('INNER JOIN', 'language l', 'pt.language_id = l.id')
    			->join('LEFT JOIN', 'product p1', 'p.parent_product_id = p1.id')
    			->join('LEFT JOIN', 'product_translation pt1', 'p1.id = pt1.product_id')
    			->join('LEFT JOIN', 'language l1', 'pt1.language_id = l1.id')
    			->where(['l.symbol' => Yii::$app->language])
    			->andWhere(['or', 'l1.symbol IS NULL', ['l1.symbol' => Yii::$app->language]])
    			->andWhere(['p.is_active' => 1]);
    	
    	if(!empty($params['name'])) {
    		$query->andWhere(['or',
    				['like', 'pt.name', $params['name']],
    				['like', 'pt1.name', $params['name']]
    		]);
    	}
    	
    	return $query;
    }
    
    public static function getAttachments($productId) {
    	return Yii::$app->db->createCommand('
    				SELECT pa.id as id, pa.url_file as urlFile, pa.name, pa.description, pa.date_creation as dateCreation,
    					u.first_name as firstName, u.last_name as lastName
					FROM product_attachment pa
					JOIN user u ON pa.user_id = u.id
					WHERE pa.product_id = :productId
    				ORDER BY pa.date_creation
    			')
        		->bindValues([
        				':productId' => intval($productId),
        		])
        		->queryAll();
    }
}