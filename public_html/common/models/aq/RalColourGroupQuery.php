<?php

namespace common\models\aq;

use yii\db\Query;
use common\models\ar\Language;

/**
 * This is the ActiveQuery class for [[\common\models\ar\RalColourGroup]].
 *
 * @see \common\models\ar\RalColourGroup
 */
class RalColourGroupQuery extends \yii\db\ActiveQuery {

    
    public static function getAllColours() {
        $query = (new Query())->select(['rcg.symbol as groupSymbol', 'rc.id', 'rc.symbol', 'rc.rgb_hash as rgbHash', 'rc.is_colour_reversed as isColourReversed'])
            ->from('ral_colour_group rcg')
            ->innerJoin('ral_colour_group_translation rcgt', 'rcgt.ral_colour_group_id = rcg.id')
            ->innerJoin('language l', 'rcgt.language_id = l.id')
            ->innerJoin('ral_colour rc', 'rc.ral_colour_group_id = rcg.id')
            ->where(['l.symbol' => \Yii::$app->language])
            ->orderBy('index');
           
        return $query->all();
    }
    
    public static function getGroups() {
        $query = (new Query())->select(['rcgt.name', 'rcg.symbol', 'rcg.id', 'rcg.rgb_hash as rgbHash'])
            ->from('ral_colour_group rcg')
            ->innerJoin('ral_colour_group_translation rcgt', 'rcg.id = rcgt.ral_colour_group_id')
            ->innerJoin('language l', 'rcgt.language_id = l.id')
            ->where(['l.symbol' => \Yii::$app->language])
            ->orderBy('index');
        
        return $query->all();
    }
    
    
    /*public function active() {
        $this->andWhere('[[status]]=1');
        return $this;
    }*/

    /**
     * @inheritdoc
     * @return \common\models\ar\RalColourGroup[]|array
     */
    public function all($db = null) {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return \common\models\ar\RalColourGroup|array|null
     */
    public function one($db = null) {
        return parent::one($db);
    }
}