<?php

namespace common\models\aq;

use Yii;
use common\models\ar\ProductPhoto;
use yii\db\Query;
/**
 * This is the ActiveQuery class for [[\common\models\ar\ProductPhoto]].
 *
 * @see \common\models\ar\ProductPhoto
 */
class ProductPhotoQuery extends \yii\db\ActiveQuery {

    /*public function active() {
        $this->andWhere('[[status]]=1');
        return $this;
    }*/

    /**
     * @inheritdoc
     * @return \common\models\ar\ProductPhoto[]|array
     */
    public function all($db = null) {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return \common\models\ar\ProductPhoto|array|null
     */
    public function one($db = null) {
        return parent::one($db);
    }
    
    public static function getPhotosByProductId($productId) {
    	return Yii::$app->db->createCommand('
    				SELECT pp.url_photo as urlPhoto, pp.url_thumbnail as urlThumbnail,
    					pp.description, pp.date_creation as dateCreation
    				FROM product_photo pp
    				WHERE pp.product_id = :productId
    				ORDER BY pp.priority
    			')
        		->bindValues([
        				':productId' => intval($productId),
        		])
        		->queryAll();
    }
    
    public static function loadProductsPhotos(&$products) {
    	$photos = ProductPhoto::find()->where(['product_id' => array_column($products, 'id')])->orderBy('priority')->all();
    	$photosLookup = [];
    	foreach($photos as $photo) {
    		if(empty($photosLookup[$photo->id])) {
    			$photosLookup[$photo->product_id] = $photo;
    		}
    	}
    
    	foreach($products as &$product) {
    		if(isset($photosLookup[$product['id']])) {
    			$product['urlThumbnail'] = $photosLookup[$product['id']]->url_thumbnail;
    		}
    	}
    }
    
    public static function getMainPhoto($id) {
    	return (new Query())->select(['pp.url_photo'])
    			->from('product_photo pp')
    			->where(['pp.product_id' => intval($id)])
    			->orderBy(['pp.priority' => SORT_ASC, 'pp.id' => SORT_ASC])
    			->limit(1)
    			->scalar();
    }
}