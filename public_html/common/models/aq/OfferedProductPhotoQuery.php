<?php

namespace common\models\aq;

use Yii;
use common\helpers\Utility;
use yii\db\Query;
/**
 * This is the ActiveQuery class for [[\common\models\ar\OfferedProductPhoto]].
 *
 * @see \common\models\ar\OfferedProductPhoto
 */
class OfferedProductPhotoQuery extends \yii\db\ActiveQuery {

    /*public function active() {
        $this->andWhere('[[status]]=1');
        return $this;
    }*/

    /**
     * @inheritdoc
     * @return \common\models\ar\OfferedProductPhoto[]|array
     */
    public function all($db = null) {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return \common\models\ar\OfferedProductPhoto|array|null
     */
    public function one($db = null) {
        return parent::one($db);
    }
    
    public static function getPhotosByProductId($productId, $type) {
    	$dbName = $type == Utility::PRODUCT_TYPE_OFFERED_PRODUCT ? 'offered_product' : 'product';
    	
    	return Yii::$app->db->createCommand('
    				SELECT opp.url_photo as urlPhoto, opp.url_thumbnail as urlThumbnail, 
    					opp.description, opp.date_creation as dateCreation
    				FROM '.$dbName.'_photo opp
    				WHERE opp.'.$dbName.'_id = :productId
    				ORDER BY opp.priority
    			')
        		->bindValues([
        				':productId' => intval($productId),
        		])
        		->queryAll();
    }
    
    public static function getMainPhoto($id) {
    	return (new Query())->select(['opp.url_photo'])
		    	->from('offered_product_photo opp')
		    	->where(['opp.offered_product_id' => intval($id)])
		    	->orderBy(['opp.priority' => SORT_ASC, 'opp.id' => SORT_ASC])
		    	->limit(1)
		    	->scalar();
    }
}