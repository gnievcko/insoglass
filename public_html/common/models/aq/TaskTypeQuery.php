<?php

namespace common\models\aq;

use Yii;
use yii\db\Query;
use common\models\aq\WarehouseDeliveryQuery;
use common\helpers\Utility;
/**
 * This is the ActiveQuery class for [[\common\models\ar\TaskType]].
 *
 * @see \common\models\ar\TaskType
 */
class TaskTypeQuery extends \yii\db\ActiveQuery {

    /*public function active() {
        $this->andWhere('[[status]]=1');
        return $this;
    }*/

    /**
     * @inheritdoc
     * @return \common\models\ar\TaskType[]|array
     */
    public function all($db = null) {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return \common\models\ar\TaskType|array|null
     */
    public function one($db = null) {
        return parent::one($db);
    }
    
    public static function getAllTypes() {
    	return (new Query())->select(['tt.id', 'ttt.name'])
		    	->from('task_type tt')
		    	->join('INNER JOIN', 'task_type_translation ttt', 'tt.id = ttt.task_type_id')
		    	->join('INNER JOIN', 'language l', 'ttt.language_id = l.id')
		    	->andWhere(['l.symbol' => Yii::$app->language])
		    	->andWhere(['tt.is_visible' => true])
		    	->orderBy('tt.priority ASC')
    	  		->all();
    }
    
    public static function getTaskTypeAndTaskCountQuery() {
        $userId = Yii::$app->user->id;
    	$countSubquery = (new Query())->select(['COUNT(t.id)'])
            	->from('task t')
            	->where('t.task_type_id = tt.id')
            	->andWhere('t.is_active = 1')
            	->andWhere('t.is_complete = 0');
        
        $countSubquery->andWhere(['or',
            ['t.user_notified_id' => null, 't.user_id' => intval($userId)],
            ['t.user_notified_id' => intval($userId)],
        ]);
        
    	return (new Query())->select(['ttt.name', 'number' => $countSubquery])
		    	->from('task_type tt')
		    	->join('INNER JOIN', 'task_type_translation ttt', 'tt.id = ttt.task_type_id')
		    	->join('INNER JOIN', 'language l', 'ttt.language_id = l.id')
		    	->andWhere(['l.symbol' => Yii::$app->language])
		    	->orderBy('number DESC')->all();	
    }
    
    public static function getBySymbol($symbol) {
        return (new Query())->select(['tt.id', 'tt.symbol', 'ttt.name'])
                ->from('task_type tt')
                ->join('INNER JOIN', 'task_type_translation ttt', 'tt.id = ttt.task_type_id')
                ->join('INNER JOIN', 'language l', 'ttt.language_id = l.id')
                ->where(['tt.symbol' => $symbol])
                ->andWhere(['l.symbol' => Yii::$app->language])
                ->one();
    }

}