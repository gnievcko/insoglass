<?php

namespace common\models\aq;

use Yii;
use yii\db\Query;
/**
 * This is the ActiveQuery class for [[\common\models\ar\ProductStatusReference]].
 *
 * @see \common\models\ar\ProductStatusReference
 */
class ProductStatusReferenceQuery extends \yii\db\ActiveQuery {

    /*public function active() {
        $this->andWhere('[[status]]=1');
        return $this;
    }*/

    /**
     * @inheritdoc
     * @return \common\models\ar\ProductStatusReference[]|array
     */
    public function all($db = null) {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return \common\models\ar\ProductStatusReference|array|null
     */
    public function one($db = null) {
        return parent::one($db);
    }
    
    public static function getAdditionalFieldsByProductStatusId($productStatusId) {
    	return Yii::$app->db->createCommand('
    				SELECT psr.id, psr.symbol, psrt.name, psr.name_table as nameTable, 
    					psr.name_column as nameColumn, psr.is_required as isRequired, psr.field_type as fieldType
    				FROM product_status_reference psr
    				JOIN product_status_reference_translation psrt ON psr.id = psrt.reference_id
    				JOIN language l ON psrt.language_id = l.id
    				WHERE psr.product_status_id = :productStatusId
    				AND l.symbol = :languageSymbol
    				ORDER BY psr.id
    			')
    			->bindValues([
    					':productStatusId' => intval($productStatusId),
    					':languageSymbol' => Yii::$app->language,
    			])
    			->queryAll();
    }
    
    public static function getAdditionalFieldsByProductStatusIds($productStatusIds) {
    	return (new Query())->select(['psr.id', 'psr.symbol', 'psrt.name', 'psr.name_table as nameTable',
    				'psr.name_column as nameColumn', 'psr.is_required as isRequired', 'psr.product_status_id as productStatusId',
    				'psr.field_type as fieldType',	
    			])
    			->from('product_status_reference psr')
    			->join('INNER JOIN', 'product_status_reference_translation psrt', 'psr.id = psrt.reference_id')
    			->join('INNER JOIN', 'language l', 'psrt.language_id = l.id')
    			->where(['psr.product_status_id' => $productStatusIds])
    			->andWhere(['l.symbol' => Yii::$app->language])
    			->orderBy(['psr.id' => SORT_ASC])
    			->all();
    }
    
    public static function getFieldTypesByIds($ids) {
    	return (new Query())->select(['psr.id', 'psr.field_type as fieldType'])
    			->from('product_status_reference psr')
    			->where(['psr.id' => $ids])
    			->all();
    }
}