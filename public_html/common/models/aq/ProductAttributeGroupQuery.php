<?php

namespace common\models\aq;

use Yii;
use yii\db\Query;

/**
 * This is the ActiveQuery class for [[\common\models\ar\ProductAttributeGroup]].
 *
 * @see \common\models\ar\ProductAttributeGroup
 */
class ProductAttributeGroupQuery extends \yii\db\ActiveQuery {

    /*public function active() {
        $this->andWhere('[[status]]=1');
        return $this;
    }*/

    /**
     * @inheritdoc
     * @return \common\models\ar\ProductAttributeGroup[]|array
     */
    public function all($db = null) {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return \common\models\ar\ProductAttributeGroup|array|null
     */
    public function one($db = null) {
        return parent::one($db);
    }
    
    public static function getAll() {
        return (new Query())->select(['pag.id', 'pagt.name'])
                ->from('product_attribute_group pag')
                ->innerJoin('product_attribute_group_translation pagt', 'pag.id = pagt.product_attribute_group_id')
                ->innerJoin('language l', 'pagt.language_id = l.id')
                ->where(['l.symbol' => Yii::$app->language])
                ->orderBy(['pag.id' => SORT_ASC])
                ->all();
    }
    
    public static function getNamesList($productAttributeGroupId = null) {
        $query =  (new Query())
                ->select(['p.id as id', 'pt.name as name'])
                ->from('product_attribute_group p')
                ->join('INNER JOIN', 'product_attribute_group_translation pt', 'p.id = pt.product_attribute_group_id')
                ->join('INNER JOIN', 'language l', 'l.id = pt.language_id')
                ->where(['l.symbol' => Yii::$app->language]);
        if(!empty($productAttributeGroupId)) {
            $query->andWhere(['p.id' => $productAttributeGroupId]);
        }
        
        return $query->all();
    }
}