<?php

namespace common\models\aq;

use Yii;
use yii\db\Query;
/**
 * This is the ActiveQuery class for [[\common\models\ar\QaFactorType]].
 *
 * @see \common\models\ar\QaFactorType
 */
class QaFactorTypeQuery extends \yii\db\ActiveQuery {

    /*public function active() {
        $this->andWhere('[[status]]=1');
        return $this;
    }*/

    /**
     * @inheritdoc
     * @return \common\models\ar\QaFactorType[]|array
     */
    public function all($db = null) {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return \common\models\ar\QaFactorType|array|null
     */
    public function one($db = null) {
        return parent::one($db);
    }
    
    public static function getAllVisible() {
        return (new Query())->select(['qat.id', 'qat.symbol', 'qatt.name', 'qat.variable_type as variableType', 
                    'qat.is_required as isRequired'])
                ->from('qa_factor_type qat')
                ->innerJoin('qa_factor_type_translation qatt', 'qat.id = qatt.qa_factor_type_id')
                ->innerJoin('language l', 'qatt.language_id = l.id')
                ->where(['l.symbol' => Yii::$app->language])
                ->andWhere(['qat.is_visible' => true])
                ->orderBy(['qat.priority' => SORT_ASC])
                ->all();
    }
}