<?php

namespace common\models\aq;

/**
 * This is the ActiveQuery class for [[\common\models\ar\FileTemporaryStorage]].
 *
 * @see \common\models\ar\FileTemporaryStorage
 */
class FileTemporaryStorageQuery extends \yii\db\ActiveQuery {

    /**
     * @inheritdoc
     * @return \common\models\ar\FileTemporaryStorage[]|array
     */
    public function all($db = null) {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return \common\models\ar\FileTemporaryStorage|array|null
     */
    public function one($db = null) {
        return parent::one($db);
    }
}
