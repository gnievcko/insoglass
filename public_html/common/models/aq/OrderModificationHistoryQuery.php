<?php

namespace common\models\aq;

use yii\db\Query;

/**
 * This is the ActiveQuery class for [[\common\models\ar\OrderModificationHistory]].
 *
 * @see \common\models\ar\OrderModificationHistory
 */
class OrderModificationHistoryQuery extends \yii\db\ActiveQuery {

	
	public static function findAllByOrderId($orderId) {
		return (new Query())
			->from('order_modification_history')
			->where('order_id = :orderId', [':orderId' => $orderId])
			->orderBy('id desc')
			->all();
	}
    /*public function active() {
        $this->andWhere('[[status]]=1');
        return $this;
    }*/

    /**
     * @inheritdoc
     * @return \common\models\ar\OrderModificationHistory[]|array
     */
    public function all($db = null) {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return \common\models\ar\OrderModificationHistory|array|null
     */
    public function one($db = null) {
        return parent::one($db);
    }
}