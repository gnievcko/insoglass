<?php

namespace common\models\aq;

use Yii;
use common\helpers\Utility;
use yii\db\Query;
use yii\db\Expression;
use common\models\ar\Order;

/**
 * This is the ActiveQuery class for [[\common\models\ar\Order]].
 *
 * @see \common\models\ar\Order
 */
class OrderQuery extends \yii\db\ActiveQuery {
    /* public function active() {
      $this->andWhere('[[status]]=1');
      return $this;
      } */

    /**
     * @inheritdoc
     * @return \common\models\ar\Order[]|array
     */
    public function all($db = null) {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return \common\models\ar\Order|array|null
     */
    public function one($db = null) {
        return parent::one($db);
    }

    public static function getOrdersQuery($params = []) {
        $query = (new Query())
                ->select(['o.id', 'o.number as number', 'o.company_id', 'c.name as companyName',
                    'pc.name as parentCompanyName', 'o.title', 'ctt.name as contractType',
                    'o.date_creation',
                    'UNIX_TIMESTAMP(o.date_creation) as date_creation_timestamp',
                    'o.date_modification',
                    'UNIX_TIMESTAMP(o.date_modification) as date_modification_timestamp',
                    'oh.date_deadline', 'oh.date_shipping',
                    'UNIX_TIMESTAMP(oh.date_deadline) as date_deadline_timestamp',
                    'o.number_for_client as numberForClient',
                    'CONCAT(u.first_name, " ", u.last_name) as userResponsible',
                    'os.symbol as orderStatusSymbol',
                    'uu.company_id as companyIdOfCreator',
                    'ost.name as statusName', 'o.is_active', new Expression('
            		  	EXISTS(SELECT *
            		  	FROM order_history_photo 
            		  		JOIN order_history ON order_history.id = order_history_photo.order_history_id
            		  	WHERE order_history.order_id = o.id) AS has_photo
            		 '), new Expression('
            		  	EXISTS(SELECT *
            		  	FROM order_attachment
            		  	WHERE order_id = o.id) AS has_attachment
            		 ')])
                ->from('order o')
                ->join('INNER JOIN', 'order_history oh', 'oh.id = o.order_history_last_id')
                ->join('INNER JOIN', 'order_status os', 'os.id = oh.order_status_id')
                ->join('INNER JOIN', 'order_status_translation ost', 'ost.order_status_id = os.id')
                ->join('INNER JOIN', 'language l', 'l.id = ost.language_id')
                ->join('INNER JOIN', 'company c', 'c.id = o.company_id')
                ->join('LEFT JOIN', 'user u', 'u.id = o.user_responsible_id')
                ->join('LEFT JOIN', 'user uu', 'uu.id = o.user_id')
                ->join('LEFT JOIN', 'address a', 'c.address_id = a.id')
                ->join('LEFT JOIN', 'city ct', 'a.city_id = ct.id')
                ->join('LEFT JOIN', 'contract_type_translation ctt', 'o.contract_type_id = ctt.contract_type_id')
                ->join('LEFT OUTER JOIN', 'company pc', 'c.parent_company_id = pc.id')
                ->where(['l.symbol' => \Yii::$app->language])
                ->andWhere(['o.is_deleted' => 0])
                ->andWhere(['o.is_artificial' => 0]);

        if(!\Yii::$app->user->can(Utility::ROLE_MAIN_SALESMAN) && !\Yii::$app->user->can(Utility::ROLE_CLIENT)) {
            // $query->join('INNER JOIN', 'company_group_set cgs', 'cgs.company_id = c.id');
            $query->join('LEFT OUTER JOIN', 'company_group_set cgs', 'cgs.company_id = c.id');
            // $query->join('INNER JOIN', 'user_company_salesman ucs', 'ucs.company_group_id = cgs.company_group_id');
            $query->join('LEFT OUTER JOIN', 'user_company_salesman ucs', 'ucs.company_group_id = cgs.company_group_id');
            // show orders associated with the current user through the user_order_salesman table
            $query->join('LEFT OUTER JOIN', 'user_order_salesman uos', 'uos.order_id = o.id');
            $query->andWhere(['or',
            		['ucs.user_id' => intval(Yii::$app->user->id)],
            		['o.user_responsible_id' => intval(Yii::$app->user->id)],
            		['exists', (new Query())->select('t.id')
		            		->from('task t')
		            		->join('INNER JOIN', 'task_data td', 't.id = td.task_id')
		            		->join('INNER JOIN', 'task_type_reference ttr', '(td.task_type_reference_id = ttr.id AND ttr.name_table = "order" AND ttr.name_column = "id")')
		            		->where('td.referenced_object_id = o.id')
		            		->andWhere(['or',
		            				['t.user_notified_id' => null, 't.user_id' => intval(Yii::$app->user->id)],
		            				['t.user_notified_id' => intval(Yii::$app->user->id)],
		            		])
            		],
                    ['uos.user_id' => intval(Yii::$app->user->id)]
            ]);
            $query->distinct();
        }

        if(Yii::$app->user->can(Utility::ROLE_CLIENT) && !Yii::$app->user->can(Utility::ROLE_ADMIN)) {
            $query->join('INNER JOIN', 'order_user_representative our', 'our.order_id = o.id');
            $query->andWhere(['or', ['our.user_representative_id' => \Yii::$app->user->id],
                                ['o.user_id' => \Yii::$app->user->id]]);
            $query->distinct();
        }

        if(!empty($params['orderName'])) {
            $query->andWhere(['like', 'o.title', $params['orderName']]);
        }
        if(!empty($params['orderClientId'])) {
            $query->andWhere(['c.id' => $params['orderClientId']]);
        }
        if(!empty($params['orderClientName'])) {
            $query->andWhere(['like', 'c.name', $params['orderClientName']]);
        }
        if(!empty($params['orderParentClientName'])) {
            $query->andWhere(['like', 'pc.name', $params['orderParentClientName']]);
        }

        if(!empty($params['cityName'])) {
            $query->andWhere(['or',
                ['like', 'LOWER(ct.name)', mb_strtolower($params['cityName'], 'UTF-8')],
                ['like', 'LOWER(ct.zip_code)', mb_strtolower($params['cityName'], 'UTF-8')],
            ]);
        }

        if(!empty($params['orderType'])) {
            if($params['orderType'] == Utility::ORDER_TYPE_OPEN) {
                $query->andWhere(['o.is_active' => 1]);
            } else if($params['orderType'] == Utility::ORDER_TYPE_ARCHIVAL) {
                $query->andWhere(['o.is_active' => 0]);
            } else if($params['orderType'] == Utility::ORDER_TYPE_TEMPLATE) {
                $query->andWhere(['o.is_template' => 1]);
            }
        }

        if(!empty($params['orderStatusesIds'])) {
            $query->andWhere(['ost.order_status_id' => $params['orderStatusesIds']]);
        }

        if(!empty($params['orderNumber'])) {
            $query->andWhere(['like', 'o.number', $params['orderNumber']]);
        }

        if(!empty($params['dateFrom'])) {
            $query->andWhere(['>=', 'DATE(o.date_creation)', $params['dateFrom']]);
        }

        if(!empty($params['dateTo'])) {
            $query->andWhere(['<=', 'DATE(o.date_creation)', $params['dateTo']]);
        }
        
        if(!empty($params['orderNumberForClient'])) {
             $query->andWhere(['like', 'o.number_for_client', $params['orderNumberForClient']]);
        }
        
        if(!empty($params['isVisibleForClient'])) {
            $query->andWhere(['os.is_visible_for_client' => 1]);
        }

        $columnsMap = [
            'number' => 'o.number',
            'name' => 'o.title',
            'create_date' => 'o.date_creation',
            'date_deadline' => 'oh.date_deadline',
            'client' => 'c.name',
            'status' => 'ost.name',
            'contractType' => 'ctt.contractType',
            'modification_date' => 'o.date_modification',
            'orderNumberForClient' => 'o.number_for_client',
            'user_responsible' => 'CONCAT(u.first_name, " ", u.last_name)'
        ];

        if(!empty($params['sortDir']) && !empty($columnsMap[$params['sortField']])) {
            $sortFactor = [
                $columnsMap[$params['sortField']] => (!empty($params['sortDir']) && $params['sortDir'] == 'desc') ? SORT_DESC : SORT_ASC,
            ];

            if($params['sortField'] != 'number') {
                $sortFactor = array_merge($sortFactor, [$columnsMap['number'] => SORT_DESC]);
            }

            $query->orderBy($sortFactor);
        }

        return $query;
    }

    public static function getDetails($id) {
        return Yii::$app->db->createCommand('
    				SELECT o.id,o.is_active as isActive, ent.name as entity, ent.id as entityId,
                        CONCAT(COALESCE(og.number, ""), o.number) as number, o.title, o.description,
                		o2.number as parentOrderNumber, o2.title as parentOrderTitle, 
						o.parent_order_id as parentOrderId,
                        o.description_cont as descriptionCont, o.description_note as descriptionNote, CONCAT(ur.first_name, " ", ur.last_name) as userResponsible,
    					o.duration_time, o.execution_time, o.payment_terms,
    					o.date_creation as dateCreation, u.email, u.first_name as firstName,
                        opt.name as priorityName, 
                        u.last_name as lastName, c.name as companyName, c.id as companyId, 
                        pc.name as parentCompanyName, pc.id as parentCompanyId,
						ost.name as status, oh.date_deadline as dateDeadline, oh.date_reminder as dateReminder, GREATEST(oh.date_creation, o.date_modification) as dateLastModification,
    					o.order_history_last_id, oh.date_shipping as shippingDate,
                        o.is_invoiced as isInvoiced, 
                        addr.main as companyAddress, addr.complement as companyAddressComplement, city.name as cityName, city.zip_code as cityZipCode,
                        addr2.main as postalCompanyAddress, addr2.complement as postalCmpanyAddressComplement, city2.name as postalCityName, city2.zip_code as postalCityZipCode,
                        c.phone1 as companyPhone1,
                        c.phone2 as companyPhone2, c.fax1 as companyFax, ctt.name as contractType, o.is_deleted as isDeleted, ocd.content as customData,
        				u.phone1 as userPhone1, u.phone2 as userPhone2, u.position, o.number_for_client as numberForClient
    				FROM `order` o
					LEFT JOIN `order` o2 on o.parent_order_id = o2.id
    				JOIN order_group og ON o.order_group_id = og.id
    				LEFT JOIN entity ent on o.entity_id = ent.id
    				JOIN user u ON o.user_id = u.id
        			LEFT JOIN user ur ON o.user_responsible_id = ur.id
    				JOIN company c ON o.company_id = c.id
                    LEFT OUTER JOIN company pc ON pc.id = c.parent_company_id
    				JOIN order_history oh ON o.order_history_last_id = oh.id
    				JOIN order_status os ON oh.order_status_id = os.id
    				JOIN order_status_translation ost ON os.id = ost.order_status_id
    				JOIN language l ON ost.language_id = l.id
                    LEFT JOIN address addr ON addr.id = c.address_id
                    LEFT JOIN city ON city.id = addr.city_id
                    LEFT JOIN address addr2 ON addr2.id = c.address_postal_id
                    LEFT JOIN city city2 ON city2.id = addr2.city_id
                    LEFT JOIN contract_type_translation ctt ON o.contract_type_id = ctt.contract_type_id 
                    LEFT JOIN order_custom_data ocd ON oh.order_custom_data_id = ocd.id
                    LEFT JOIN order_priority_translation opt on opt.order_priority_id = o.order_priority_id
                    LEFT JOIN language l2 ON opt.language_id = l2.id
    				WHERE o.id = :id
    				AND l.symbol = :languageSymbol
                    AND (l.symbol = :languageSymbol OR l2.symbol IS NULL)
    			')
                ->bindValues([
                    ':id' => intval($id),
                    ':languageSymbol' => Yii::$app->language,
                ])
                ->queryOne();
    }

    public static function getChildrenOrders($id) {
        return (new Query())->select(['o.id', 'o.title', 'o.number'])
                        ->from('order o')
                        ->where('o.parent_order_id = :id', [':id' => $id])
                        ->all();
    }

    public static function getOrderProducts($orderId, $params = []) {
        $commonColumns = ['oop.id', 'oop.price', 'oop.count', 'oop.currency_id', 'oop.currency_id as currencyId',
            'c.conversion_value as conversionValue', 'parent.name as parentName', 'c.short_symbol as currency',
            'translation.description', 'translation.name', 'oop.vat_rate as vatRate', 'oop.unit as unit', 
            'oop.height as height', 'oop.width as width', 'ct.name as construction', 'oop.construction_id', 
            'st.name as shape', 'oop.shape_id', 'mt.name as muntin', 'oop.muntin_id',
            'oop.remarks', 'oop.remarks2', 'oop.ral_colour_id as ralColourId', 'rc.rgb_hash as ralRgbHash', 'rc.symbol as ralColourSymbol',
            'ovp.url_photo as urlPhoto','ovp.height as heightPhoto', 'ovp.width as widthPhoto', 'ovp.top as topPhoto', 'ovp.left as leftPhoto'
        ];
        
        $offeredProductsQuery = (new Query())->select(array_merge(['oop.offered_product_id as productId', 'op.symbol', 'op.parent_offered_product_id as parentId'], $commonColumns, ['op.is_artificial as isArtificial', new Expression('"' . Utility::PRODUCT_TYPE_OFFERED_PRODUCT . '" as type')]))
                ->from('order_offered_product oop')
                ->join('INNER JOIN', 'order o', 'o.id = oop.order_id')
                ->join('INNER JOIN', 'offered_product op', 'op.id = oop.offered_product_id')
                ->join('INNER JOIN', 'currency c', 'c.id = oop.currency_id')
                ->join('INNER JOIN', 'offered_product_translation translation', 'translation.offered_product_id = op.id')
                ->join('INNER JOIN', 'language l', 'l.id = translation.language_id')
                ->join('LEFT JOIN', 'offered_product_translation parent', 'parent.offered_product_id = op.parent_offered_product_id AND parent.language_id = translation.language_id')
                ->leftJoin('construction_translation ct', 'oop.construction_id = ct.construction_id')
                ->leftJoin('language l2', 'l2.id = ct.language_id')
                ->leftJoin('shape_translation st', 'oop.shape_id = st.shape_id')
                ->leftJoin('language l3', 'l3.id = st.language_id')
                ->leftJoin('muntin_translation mt', 'oop.muntin_id = mt.muntin_id')
                ->leftJoin('language l4', 'l4.id = mt.language_id')
                ->leftJoin('ral_colour rc', 'oop.ral_colour_id = rc.id')
                ->leftJoin('order_visualisation_photo ovp', 'ovp.order_offered_product_id = oop.id')
                ->andWhere(['o.id' => $orderId])
                ->andWhere('oop.is_active')
                ->andWhere(['l.symbol' => \Yii::$app->language])
                ->andWhere([
                    'OR',
                    'l2.symbol IS NULL',
                    ['l2.symbol' => Yii::$app->language]
                ])
                ->andWhere([
                    'OR',
                    'l3.symbol IS NULL',
                    ['l3.symbol' => Yii::$app->language]
                ])
                ->andWhere([
                    'OR',
                    'l4.symbol IS NULL',
                    ['l4.symbol' => Yii::$app->language]
                ]);

        if(empty($params['withArtificial'])) {
            $offeredProductsQuery->andWhere(['op.is_artificial' => 0]);//->andWhere('oop.order_history_id = o.order_history_last_id');
        } else {
            /* $offeredProductsQuery->andWhere(['or',
              'oop.order_history_id = o.order_history_last_id',
              ['and', 'op.is_artificial = 1', 'oop.order_history_id = o.order_history_first_id'],
              ]); */
        }

        $productsQuery = (new Query())->select(array_merge(['oop.product_id as productId', 'p.symbol', 'p.parent_product_id as parentId'], $commonColumns, [new Expression('0 as isArtificial'), new Expression('"' . Utility::PRODUCT_TYPE_PRODUCT . '" as type')]))
                ->from('order_offered_product oop')
                ->join('INNER JOIN', 'order o', 'o.id = oop.order_id')
                ->join('INNER JOIN', 'currency c', 'c.id = oop.currency_id')
                ->join('INNER JOIN', 'product p', 'p.id = oop.product_id')
                ->join('INNER JOIN', 'product_translation translation', 'translation.product_id = oop.product_id')
                ->join('INNER JOIN', 'language l', 'l.id = translation.language_id')
                ->join('LEFT JOIN', 'product_translation parent', 'parent.product_id = p.parent_product_id AND parent.language_id = translation.language_id')
                ->leftJoin('construction_translation ct', 'oop.construction_id = ct.construction_id')
                ->leftJoin('language l2', 'l2.id = ct.language_id')
                ->leftJoin('shape_translation st', 'oop.shape_id = st.shape_id')
                ->leftJoin('language l3', 'l3.id = st.language_id')
                ->leftJoin('muntin_translation mt', 'oop.muntin_id = mt.muntin_id')
                ->leftJoin('language l4', 'l4.id = mt.language_id')
                ->leftJoin('ral_colour rc', 'oop.ral_colour_id = rc.id')
                ->leftJoin('order_visualisation_photo ovp', 'ovp.order_offered_product_id = oop.id')
                ->andWhere(['o.id' => $orderId])
                ->andWhere('oop.is_active')
                ->andWhere(['l.symbol' => \Yii::$app->language])
                ->andWhere([
                    'OR',
                    'l2.symbol IS NULL',
                    ['l2.symbol' => Yii::$app->language]
                ])
                ->andWhere([
                    'OR',
                    'l3.symbol IS NULL',
                    ['l3.symbol' => Yii::$app->language]
                ])
                ->andWhere([
                    'OR',
                    'l4.symbol IS NULL',
                    ['l4.symbol' => Yii::$app->language]
                ]);
        ; //->andWhere('oop.order_history_id = o.order_history_last_id');
        //Yii::error($offeredProductsQuery->union($productsQuery)->orderBy(['isArtificial' => SORT_ASC])->createCommand()->rawSql);

        $resultQuery = (new Query())->from(['table' => $offeredProductsQuery->union($productsQuery)]);

        return $resultQuery->orderBy(['isArtificial' => SORT_ASC])->all();
    }

    public static function getRelatedEmployeesIds($orderId) {
        return (new Query())->select(['uos.user_id'])
                        ->from('user_order_salesman uos')
                        ->andWhere(['uos.order_id' => $orderId])
                        ->column();
    }

    public static function findOrders($params = []) {
        $query = (new \yii\db\Query())
                ->select(['o.id', 'CONCAT(o.title, " (", c.name, ")") as name'])
                ->from('order o')
                ->join('INNER JOIN', 'company c', 'o.company_id = c.id')
                ->andWhere('o.is_active = 1')
                ->andWhere('o.is_deleted = 0')
                ->andWHere('o.is_artificial = 0');

        if(!empty($params['name'])) {
            $query->andWhere(['or',
                ['like', 'c.name', $params['name']],
                ['like', 'o.title', $params['name']],
                ['like', 'o.number', $params['name']],
            ]);
        }

        return $query;
    }

    public static function findParentOrders($params = []) {
        $query = (new \yii\db\Query())
                ->select(['o.id', 'CONCAT(o.number, " (", o.title, ")") as name'])
                ->from('order o')
                ->andWhere('o.is_active = 1')
                ->andWhere('o.is_deleted = 0')
                ->andWHere('o.is_artificial = 0');

        if(!empty($params['name'])) {
            $query->andWhere(['or',
                ['like', 'o.title', $params['name']],
                ['like', 'o.number', $params['name']],
            ]);
        }

        return $query;
    }

    public static function getPhotos($orderId) {
        return Yii::$app->db->createCommand('
    				SELECT u.email, u.first_name as firstName, u.last_name as lastName,
                                ohp.url_photo as urlPhoto, ohp.description, ohp.date_creation as dateCreation,
                                ohp.url_thumbnail as urlThumbnail
    				FROM order_history_photo ohp
    				JOIN order_history oh ON ohp.order_history_id = oh.id
    				JOIN user u ON ohp.user_id = u.id
    				WHERE oh.order_id = :orderId
    			')
                        ->bindValues([
                            ':orderId' => intval($orderId),
                        ])
                        ->queryAll();
    }

    public static function getAttachments($orderId) {
        return Yii::$app->db->createCommand('
    				SELECT oa.id as id, oa.url_file as urlFile, oa.name, oa.description, oa.date_creation as dateCreation,
    					u.first_name as firstName, u.last_name as lastName
					FROM order_attachment oa
					JOIN user u ON oa.user_id = u.id
					WHERE oa.order_id = :orderId
    				ORDER BY oa.date_creation
    			')
                        ->bindValues([
                            ':orderId' => intval($orderId),
                        ])
                        ->queryAll();
    }
    public static function getOrderProductAttachments($offeredProductId) {
    	return Yii::$app->db->createCommand('
    				SELECT opa.id as id, opa.url_file as urlFile, opa.name, opa.description, opa.date_creation as dateCreation,
    					u.first_name as firstName, u.last_name as lastName
					FROM order_offered_product_attachment opa
					JOIN user u ON opa.user_id = u.id
					WHERE opa.order_offered_product_id = :offeredProductId
    				ORDER BY opa.date_creation
    			')
        		->bindValues([
        				':offeredProductId' => intval($offeredProductId),
        		])
        		->queryAll();
    }
    public static function getCosts($orderId) {
        return Yii::$app->db->createCommand('
    				SELECT op.id, opt.name, opt.currency_id, oop.price, oop.count, oop.vat_rate as vatRate, oop.unit as unit,
                        oop.width, oop.height, oop.construction_id as constructionId, oop.shape_id as shapeId, 
                        oop.muntin_id as muntinId, remarks, remarks2, oop.id as offered_product_id,
                        oop.ral_colour_id as ralColourId, rc.rgb_hash as ralRgbHash, rc.symbol as ralColourSymbol,
                        ovp.url_photo as urlPhoto, ovp.height as heightPhoto, ovp.width as widthPhoto, ovp.top as topPhoto, ovp.left as leftPhoto
					FROM offered_product op
					JOIN offered_product_translation opt ON opt.offered_product_id = op.id
                    JOIN order_offered_product oop ON oop.offered_product_id = op.id
                    LEFT JOIN ral_colour rc ON oop.ral_colour_id = rc.id 
                    LEFT JOIN order_visualisation_photo ovp ON ovp.order_offered_product_id = oop.id
                    WHERE oop.order_id = :orderId
                    AND op.is_artificial = 1
                    AND opt.language_id = (SELECT id FROM language WHERE symbol = :languageSymbol) 
                    AND oop.is_active = 1
    			')
                        ->bindValues([
                            ':languageSymbol' => Yii::$app->language,
                            ':orderId' => intval($orderId),
                        ])
                        ->queryAll();
    }

    public static function getOrderCountByStatus($dateFrom, $dateTo) {
        return Yii::$app->db->createCommand('
	    			SELECT ost.name, count(ost.name) as count
			    	FROM `order` o
			    	JOIN order_history oh ON o.id = oh.order_id
			    	JOIN order_status_translation ost on oh.order_status_id = ost.order_status_id
			    	JOIN `language` l on l.id = ost.language_id
			    	WHERE l.symbol = :lSymbol
			    	AND oh.date_creation =
			    		(SELECT MAX(oh1.date_creation)
	    						FROM order_history oh1
	    						WHERE oh1.order_id = o.id
	    						AND oh1.date_creation >= :dateFrom
	    						AND oh1.date_creation <= :dateTo
	    				)
	        		AND o.is_deleted = 0
        			AND o.is_artificial = 0
	    			GROUP BY ost.name
    			')
                        ->bindValues([
                            ':lSymbol' => Yii::$app->language,
                            ':dateFrom' => $dateFrom,
                            ':dateTo' => $dateTo,
                        ])
                        ->queryAll();
    }

    public static function getOrderCountByClient($dateFrom, $dateTo) {
        return Yii::$app->db->createCommand('
					SELECT c.name, COUNT(c.name) as `count`
					FROM `order` o
					JOIN company c on o.company_id = c.id
					WHERE DATE(o.date_creation) <= :dateTo
	    			AND DATE(o.date_creation) >= :dateFrom
					AND 0 = (SELECT COUNT(*)
							FROM order_history oh
							JOIN order_status os ON oh.order_status_id = os.id
							WHERE oh.order_id = o.id
							AND DATE(oh.date_creation) <= :dateTo
							AND os.is_terminal = 1
						)
	        		AND o.is_deleted = 0
        			AND o.is_artificial = 0
					GROUP BY c.name
    			')
                        ->bindValues([
                            ':dateFrom' => $dateFrom,
                            ':dateTo' => $dateTo,
                        ])
                        ->queryAll();
    }

    public static function getOrderCountByAuthor($dateFrom, $dateTo) {
        return Yii::$app->db->createCommand('
			    	SELECT CONCAT(u.first_name, " ",  u.last_name) as name, COUNT(CONCAT(u.first_name, " ",  u.last_name)) as count FROM `order` o
			    	JOIN user u ON u.id = o.user_id
			    	JOIN order_history oh ON oh.id = o.order_history_first_id
			    	WHERE oh.date_creation >= :dateFrom
			    	AND oh.date_creation <= :dateTo
        			AND o.is_deleted = 0
        			AND o.is_artificial = 0
			    	GROUP BY CONCAT(u.first_name, " ",  u.last_name)
    			')
                        ->bindValues([
                            ':dateFrom' => $dateFrom,
                            ':dateTo' => $dateTo,
                        ])
                        ->queryAll();
    }

    public static function getOrderCountByEmployeeUpdating($dateFrom, $dateTo) {
        return Yii::$app->db->createCommand('
					SELECT CONCAT(u.first_name, " ",  u.last_name) as name, COUNT(CONCAT(u.first_name, " ",  u.last_name)) as count 
	    			FROM `order` o
					JOIN user u ON u.id = o.user_id
					JOIN order_history oh ON oh.order_id = o.id
					WHERE oh.date_creation >= :dateFrom
					AND oh.date_creation <= :dateTo
	        		AND o.is_deleted = 0
        			AND o.is_artificial = 0
					GROUP BY CONCAT(u.first_name, " ",  u.last_name)
    			')
                        ->bindValues([
                            ':dateFrom' => $dateFrom,
                            ':dateTo' => $dateTo,
                        ])
                        ->queryAll();
    }

    public static function getOrderStatistics($dateFrom, $dateTo) {
        $result['openOrderCount'] = Yii::$app->db->createCommand('
					SELECT COUNT(*) as `count` 
	    			FROM `order` o
					JOIN order_history oh ON oh.id = o.order_history_first_id
					JOIN order_status os ON os.id = oh.order_status_id
					WHERE oh.date_creation >= :dateFrom
					AND oh.date_creation <= :dateTo
					AND os.symbol = :symbol
	        		AND o.is_deleted = 0
        			AND o.is_artificial = 0
    			')
                ->bindValues([
                    ':dateFrom' => $dateFrom,
                    ':dateTo' => $dateTo,
                    ':symbol' => Utility::ORDER_STATUS_NEW,
                ])
                ->queryScalar();

        $result['closedOrderCount'] = Yii::$app->db->createCommand('
					SELECT COUNT(*) as `count` 
					FROM `order` o
					JOIN order_history oh ON oh.id = o.order_history_last_id
					JOIN order_status os ON os.id = oh.order_status_id
					WHERE oh.date_creation >= :dateFrom
					AND oh.date_creation <= :dateTo
	    	    	AND os.is_terminal = 1 
	        		AND o.is_deleted = 0
        			AND o.is_artificial = 0
    			')
                ->bindValues([
                    ':dateFrom' => $dateFrom,
                    ':dateTo' => $dateTo,
                ])
                ->queryScalar();

        // TODO: Z ponizszym zapytaniem bedzie problem, gdy bedzie mozna zmieniac status ze zamknietego na otwarty
        $result['ongoingOrderCount'] = Yii::$app->db->createCommand('
				 	SELECT COUNT(o.id) as `count`
    	    	 	FROM `order` o
    	    	 	WHERE
    	    	 	DATE(o.date_creation) <= :dateTo
        			AND o.is_deleted = 0
        			AND o.is_artificial = 0
    	    	 	AND
    	    	 	0 = (
        					SELECT COUNT(*)
    	    	 			FROM order_history oh
    	    	 			JOIN order_status os ON oh.order_status_id = os.id
    	    	 			WHERE oh.order_id = o.id
    	    	 			AND DATE(oh.date_creation) <= :dateTo
    	    	 			AND os.is_terminal = 1
        			);
    			')
                ->bindValues([
                    ':dateTo' => $dateTo,
                ])
                ->queryScalar();

        $result['handledOrderCount'] = Yii::$app->db->createCommand('
					SELECT COUNT(DISTINCT(o.id)) as `count` 
					FROM `order` o
	    	    	JOIN order_history oh ON oh.order_id = o.id
	    	    	JOIN order_status os ON os.id = oh.order_status_id
	    	    	WHERE oh.date_creation >= :dateFrom
	    	    	AND oh.date_creation <= :dateTo
        			AND o.is_deleted = 0
        			AND o.is_artificial = 0
    			')
                ->bindValues([
                    ':dateFrom' => $dateFrom,
                    ':dateTo' => $dateTo,
                ])
                ->queryScalar();

        return $result;
    }

    public static function getUpcomingOrders($dateFrom, $dateTo) {
        return Yii::$app->db->createCommand('
		    		SELECT o.title, ost.name AS `status`, 
    					(
    						SELECT SUM(oop.`count` * oop.price) 
    						FROM order_offered_product oop
    						WHERE oop.order_id = o.id
        					AND oop.is_active = 1
    						GROUP BY oop.order_id
    					) `sum`,
    					oh.date_deadline 
    				FROM `order` o
    				JOIN order_history oh ON o.id = oh.order_id
    				JOIN order_status os ON os.id = oh.order_status_id
    				JOIN order_status_translation ost ON ost.order_status_id = os.id
    				WHERE os.is_terminal = 0
    				AND oh.date_creation = (
    					SELECT MAX(oh1.date_creation)
    					FROM order_history oh1
    					WHERE oh1.order_id = o.id
    					AND oh.date_creation >= :dateFrom
						AND oh.date_creation <= :dateTo
    				)
        			AND o.is_deleted = 0
        			AND o.is_artificial = 0
    				ORDER BY oh.date_deadline IS NULL
    			')
                        ->bindValues([
                            ':dateFrom' => $dateFrom,
                            ':dateTo' => $dateTo,
                        ])
                        ->queryAll();
    }

    // case(currrate.currentrate) when null then 1 else currrate.currentrate end
    public static function getOrderRegisterReportData($dateFrom, $dateTo, $orderTypeIds = []) {
        $orderTypeSymbolSubquery = (new Query())->select([new Expression('GROUP_CONCAT(ot.symbol SEPARATOR ",")')])
                ->from('order_type ot')
                ->join('INNER JOIN', 'order_type_set ots', 'ot.id = ots.order_type_id')
                ->where('ots.order_id = o.id');

        $orderTypeNameSubquery = (new Query())->select([new Expression('GROUP_CONCAT(ott.name SEPARATOR ", ")')])
                ->from('order_type ot')
                ->join('INNER JOIN', 'order_type_translation ott', 'ot.id = ott.order_type_id')
                ->join('INNER JOIN', 'language l', 'ott.language_id = l.id')
                ->join('INNER JOIN', 'order_type_set ots', 'ot.id = ots.order_type_id')
                ->where('ots.order_id = o.id')
                ->andWhere(['l.symbol' => Yii::$app->language]);

        $query = (new Query())->select(['o.id', 'o.date_creation', 'c.name as company', 'parent.name as parent',
                    'ctt.name as contract_type', 'a1.main as a1Main', 'a1.complement as a1Complement',
                    'c1.zip_code as a1Zip_code', 'c1.name as a1City', 'a2.main as a2Main', 'a2.complement as a2Complement',
                    'c2.zip_code as a2Zip_code', 'c2.name as a2City', 'orderTypeSymbols' => $orderTypeSymbolSubquery,
                    'orderTypeNames' => $orderTypeNameSubquery])
                ->from('order o')
                ->join('LEFT JOIN', 'company c', 'o.company_id = c.id')
                ->join('LEFT JOIN', 'company parent', 'c.parent_company_id = parent.id')
                ->join('LEFT JOIN', 'contract_type_translation ctt', 'ctt.contract_type_id = o.contract_type_id')
                ->join('LEFT JOIN', 'address a1', 'a1.id = c.address_id')
                ->join('LEFT JOIN', 'city c1', 'a1.city_id = c1.id')
                ->join('LEFT JOIN', 'address a2', 'a2.id = parent.address_id')
                ->join('LEFT JOIN', 'city c2', 'a2.city_id = c2.id')
                ->where(['>=', 'o.date_creation', $dateFrom])
                ->andWhere(['<=', 'o.date_creation', $dateTo])
                ->andWhere(['o.is_deleted' => false])
                ->andWhere(['o.is_artificial' => false])
                ->orderBy(['o.date_creation' => SORT_ASC]);

        if(Yii::$app->params['isOrderTypeVisible']) {
            $query->andWhere(['in', 'o.id', (new Query())->select(['ots.order_id'])
                        ->from('order_type_set ots')
                        ->where(['in', 'ots.order_type_id', !empty($orderTypeIds) ? $orderTypeIds : []])
            ]);
        }

        return $query->all();
    }

    public static function getOrdersWithReminder($dateFrom, $dateTo, $userId) {
        $orderTypeSubquery = (new Query())->select([new Expression('GROUP_CONCAT(ott.name SEPARATOR ", ")')])
                ->from('order_type_set ots')
                ->join('INNER JOIN', 'order_type_translation ott', 'ots.order_type_id = ott.order_type_id')
                ->join('INNER JOIN', 'language l', 'ott.language_id = l.id')
                ->where('ots.order_id = o.id')
                ->andWhere(['l.symbol' => Yii::$app->language])
                ->orderBy(['ott.name' => SORT_ASC]);

        $query = (new Query())->select(['o.id', 'orderType' => $orderTypeSubquery, 'c.name as companyName',
                    'a.main as addressMain', 'a.complement as addressComplement',
                    'ct.zip_code as zipCode', 'ct.name as cityName',
                ])
                ->from('task t')
                ->join('INNER JOIN', 'task_data td', 't.id = td.task_id')
                ->join('INNER JOIN', 'task_type tt', 't.task_type_id = tt.id')
                ->join('INNER JOIN', 'task_type_reference ttr', 'tt.id = ttr.task_type_id')
                ->join('INNER JOIN', 'order o', 'td.referenced_object_id = o.id')
                ->join('INNER JOIN', 'company c', 'o.company_id = c.id')
                ->join('LEFT JOIN', 'address a', 'c.address_id = a.id')
                ->join('LEFT JOIN', 'city ct', 'a.city_id = ct.id')
                ->where(['tt.symbol' => Utility::TASK_TYPE_ASSOCIATED_WITH_ORDER])
                ->andWhere(['ttr.symbol' => 'order_data'])
                ->andWhere(['>=', 't.date_deadline', $dateFrom])
                ->andWhere(['<=', 't.date_deadline', $dateTo])
                ->orderBy(['o.id' => SORT_ASC]);

        if(!empty($userId)) {
            $query->andWhere(['or',
                ['t.user_notified_id' => intval($userId)],
                ['and', ['t.user_notified_id' => null], ['t.user_id' => intval($userId)]],
            ]);
        }

        return $query->all();
    }

    public static function getByNumberPart($numberPart) {
        return (new Query())->select(['o.id'])
                        ->from('order o')
                        ->where(['like', 'number', $numberPart])
                        ->all();
    }
    
    public static function getOrdersToCalendar($dateType) {
        return (new Query())->select(['count(oh.' . $dateType . ') as title', 'oh.' . $dateType . ' as dateEnd'])
        ->from('order o')
        ->join('LEFT JOIN', 'order_history oh', 'oh.id = o.order_history_last_id')
        ->leftJoin('order_status os', 'os.id = oh.order_status_id')
        ->where(['not', ['oh.' . $dateType => null]])
        ->andWhere(['os.is_terminal' => false])
        ->groupBy(['oh.' . $dateType])
        ->all();
    }

    public static function getStatusByOrderId($orderId) {
        return (new Query())->select(['os.symbol'])
                ->from('order o')
                ->join('INNER JOIN', 'order_history oh', 'o.order_history_last_id = oh.id')
                ->join('INNER JOIN', 'order_status os', 'oh.order_status_id = os.id')
                ->where(['o.id' => intval($orderId)])
                ->one();
    }
    
    public static function getDetailsForProductionOrderDetails($orderId) {
        $query = (new Query())->select(['o.title as title', 'o.number as number', 'oh.date_deadline as dateDeadline',
            'oh.date_shipping as dateShipping', 'opt.name as priorityName', 'c.name as customerName', 'c.id as customerId', 'o.description',
        ])
            ->from('order o')
            ->innerJoin('company c', 'c.id = o.company_id')
            ->innerJoin('order_history oh', 'oh.id = o.order_history_last_id')
            ->leftJoin('order_priority op', 'op.id = o.order_priority_id')
            ->leftJoin('order_priority_translation opt', 'opt.order_priority_id = op.id')
            ->leftJoin('language l', 'l.id = opt.language_id')
            ->where([
                'OR',
                ['l.symbol' => \Yii::$app->language],
                'l.symbol IS NULL'
            ])
            ->andWhere(['o.id' => $orderId]);
        
        return $query->one();
    }
    
   public static function getProductsForProductionOrderDetails($orderId) {
       $query = (new Query())->select(['oop.id', 'op.symbol', 'op.id as productId', 'opt.name as productName', 'oop.count', 'oop.width', 'oop.height',
           'ct.name as construction', 'st.name as shape', 'mt.name as muntin', 'oop.remarks', 'oop.remarks2',
           'rc.symbol as ralColourSymbol', 'rc.rgb_hash as ralRgbHash',
       ])
            ->from('order o')
            ->innerJoin('order_offered_product oop', 'oop.order_id = o.id')
            ->innerJoin('offered_product op', 'op.id = oop.offered_product_id')
            ->leftJoin('offered_product_translation opt', 'op.id = opt.offered_product_id')
            ->leftJoin('language l3', 'l3.id = opt.language_id')
            ->leftJoin('construction_translation ct', 'oop.construction_id = ct.construction_id')
            ->leftJoin('language l', 'l.id = ct.language_id')
            ->leftJoin('shape_translation st', 'oop.shape_id = st.shape_id')
            ->leftJoin('language l2', 'l2.id = st.language_id')
            ->leftJoin('muntin_translation mt', 'oop.muntin_id = mt.muntin_id')
            ->leftJoin('language l4', 'l4.id = mt.language_id')
            ->leftJoin('ral_colour rc', 'oop.ral_colour_id = rc.id')
            ->where([
                'oop.order_id' => $orderId,
                'oop.is_active' => 1
            ])
            ->andWhere([
                'OR',
                'l.symbol IS NULL',
                ['l.symbol' => Yii::$app->language]
            ])
            ->andWhere([   
                'OR',
                'l2.symbol IS NULL',
                ['l2.symbol' => Yii::$app->language]
            ])
            ->andWhere([
                'OR',
                'l3.symbol IS NULL',
                ['l3.symbol' => Yii::$app->language]
            ])            
            ->andWhere([
                'OR',
                'l4.symbol IS NULL',
                ['l4.symbol' => Yii::$app->language]
            ]);
       
        return $query->all();
   }
   
   public static function getActiveOrderQuery($userId, $params = []) {
       $query = (new Query())->select(['o.id', 'o.title', 'os.symbol as status',
           'oh.date_deadline as dateDeadline'])
           ->from('order o')
           ->join('INNER JOIN', 'order_history oh', 'oh.id = o.order_history_last_id')
           ->join('INNER JOIN', 'order_status os', 'os.id = oh.order_status_id');
           
           if(!empty($params['orderStatusId'])) {
               $query->andWhere(['os.id' => $params['orderStatusId']]);
           }
           
           $query->orderBy([new Expression('-oh.date_deadline DESC'), 'o.date_creation' => SORT_DESC]);
           return $query;
   }
   
   public static function getAllStatuses() {
       return (new Query())->select(['os.id', 'ost.name'])
               ->from('order_status os')
               ->join('INNER JOIN', 'order_status_translation ost', 'os.id = ost.order_status_id')
               ->join('INNER JOIN', 'language l', 'ost.language_id = l.id')
               ->andWhere(['l.symbol' => Yii::$app->language])
               ->all();
   }
   
   public static function getShortOrder($date, $dateType) {
       return (new Query())->select(['o.id', 'o.description', 'o.title', 'oh.' . $dateType . ' as dateEnd'
               ])
               ->from('order o')
               ->join('LEFT JOIN', 'order_history oh', 'oh.id = o.order_history_last_id')
               ->where(['oh.' . $dateType => $date])
               ->all();
   }
   
   public static function getDelayedOrders() {
       return (new Query())->select('COUNT(*) as count')
            ->from('`order` o')
            ->innerJoin('`order_history` oh', 'oh.id = o.order_history_last_id')
            ->innerJoin('`order_status` os', 'oh.order_status_id = os.id')
            ->where('oh.date_deadline < CURRENT_DATE()')
            ->andWhere(['os.is_terminal' => 0])
            ->one();
   }
}
