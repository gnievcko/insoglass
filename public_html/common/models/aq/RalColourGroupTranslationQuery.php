<?php

namespace common\models\aq;

/**
 * This is the ActiveQuery class for [[\common\models\ar\RalColourGroupTranslation]].
 *
 * @see \common\models\ar\RalColourGroupTranslation
 */
class RalColourGroupTranslationQuery extends \yii\db\ActiveQuery {

    /*public function active() {
        $this->andWhere('[[status]]=1');
        return $this;
    }*/

    /**
     * @inheritdoc
     * @return \common\models\ar\RalColourGroupTranslation[]|array
     */
    public function all($db = null) {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return \common\models\ar\RalColourGroupTranslation|array|null
     */
    public function one($db = null) {
        return parent::one($db);
    }
}