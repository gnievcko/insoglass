<?php

namespace common\models\aq;
use yii\db\Query;

/**
 * This is the ActiveQuery class for [[\common\models\ar\DepartmentTranslation]].
 *
 * @see \common\models\ar\DepartmentTranslation
 */
class DepartmentTranslationQuery extends \yii\db\ActiveQuery
{

    /*public function active() {
        $this->andWhere('[[status]]=1');
        return $this;
    }*/

    /**
     * @inheritdoc
     * @return \common\models\ar\DepartmentTranslation[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return \common\models\ar\DepartmentTranslation|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }

    public function getDuplicates($name, $typeId, $lang, $id) {
        $query = new Query();
        $query->select('*')
                ->from('department_translation dtr')
                ->join('INNER JOIN', 'department d', 'd.id = dtr.department_id')
                ->join('INNER JOIN', 'department_type dt', 'd.department_type_id = dt.id')
                ->andWhere(['dtr.language_id' => $lang])
                ->andWhere(['dt.id' => $typeId])
                ->andWhere(['dtr.name' => $name]);
        
        if(!empty($id)) {
            $query->andWhere(['!=', 'd.id', intval($id)]);
        }
        return $query;
    }
}