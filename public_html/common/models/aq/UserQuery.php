<?php

namespace common\models\aq;

use Yii;
use common\models\ar\User;
use common\models\ar\UserRole;

use common\models\ar\UserCompanySalesman;
use common\models\ar\Order;
use common\helpers\Utility;
use common\models\ar\Company;
use yii\db\Query;
use common\helpers\RoleHelper;

/**
 * This is the ActiveQuery class for [[\common\models\ar\User]].
 *
 * @see \common\models\ar\User
 */
class UserQuery extends \yii\db\ActiveQuery {

    /*public function active() {
        $this->andWhere('[[status]]=1');
        return $this;
    }*/

    /**
     * @inheritdoc
     * @return \common\models\ar\User[]|array
     */
    public function all($db = null) {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return \common\models\ar\User|array|null
     */
    public function one($db = null) {
        return parent::one($db);
    }

    public function employee() {
        return $this->andWhere(['not in', 'id', UserRole::find()->select('user_id')
            ->innerJoin('role', 'role.id = user_role.role_id')
            ->where(['role.symbol' => 'client'])
        ]);
    }
    
    public function client() {
    	return $this->andWhere(['in', 'id', UserRole::find()->select('user_id')
    			->innerJoin('role', 'role.id = user_role.role_id')
    			->where(['role.symbol' => 'client'])
    	]);
    }

    public static function isSalesmanAssignedToCompanyByOrderId($id, $orderId) {
    	if(UserCompanySalesman::find()->where('user_id = :userId', [':userId' => $id])->count() == 0) {
    		// TODO: Sprawdzic, co autor mial na mysli w zakomentowanym kodzie
    		//return !empty(Order::findOne($orderId));
    		return false;
    	}

    	$result = Yii::$app->db->createCommand('
    				SELECT COUNT(c.id)
    				FROM user_company_salesman ucs
    				JOIN company_group_set cgs ON ucs.company_group_id = cgs.company_group_id
    				JOIN company c ON cgs.company_id = c.id
    				WHERE ucs.user_id = :userId
    				AND c.id = (
    					SELECT o.company_id
    					FROM `order` o
    					WHERE o.id = :orderId
    				)
    				AND c.is_active = 1
    			')
        		->bindValues([
        				':userId' => intval($id),
        				':orderId' => intval($orderId),
        		])
        		->queryScalar();

        return $result > 0;
    }

    // TODO: Może jakoś zrefaktoryzować?
    public static function isSalesmanAssignedToCompanyByCompanyId($id, $companyId) {
    	if(UserRole::find()->joinWith('role')
    			->where(['user_id' => $id])
    			->andWhere(['role.symbol' => [Utility::ROLE_MAIN_SALESMAN, Utility::ROLE_ADMIN]])
    			->count() > 0) {
    		$company = Company::findOne($companyId);

    		return !empty($company) && !empty($company->is_active);
    	}

    	$result = Yii::$app->db->createCommand('
    				SELECT COUNT(c.id)
    				FROM user_company_salesman ucs
    				JOIN company_group_set cgs ON ucs.company_group_id = cgs.company_group_id
    				JOIN company c ON cgs.company_id = c.id
    				WHERE ucs.user_id = :userId
    				AND c.id = :companyId
    				AND c.is_active = 1
    			')
        		->bindValues([
        				':userId' => intval($id),
        				':companyId' => intval($companyId),
        		])
        		->queryScalar();

        return $result > 0;
    }

    public static function isSalesmanAssignedToOrder($id, $orderId) {
    	// SELECT COUNT(uos.user_id)
    	// JOIN `order` o ON uos.order_id = o.id
    	// WHERE uos.order_id = :orderId
    	
    	$result = Yii::$app->db->createCommand('
    				SELECT COUNT(*)
    				FROM user_order_salesman uos
                    RIGHT OUTER JOIN `order` o ON uos.order_id = o.id
                    WHERE o.id = :orderId
    				AND (
    					uos.user_id = :userId
    					OR 
    					o.user_responsible_id = :userId
    					OR
    					0 < (
							SELECT COUNT(t.id)
							FROM task t
							JOIN task_data td ON t.id = td.task_id
							JOIN task_type_reference ttr ON (td.task_type_reference_id = ttr.id AND ttr.name_table = "order" AND ttr.name_column = "id")
							WHERE td.referenced_object_id = o.id
							AND (
								(t.user_notified_id IS NULL AND t.user_id = :userId)
								OR
								(t.user_notified_id = :userId)
							)
						)
    				)
    			')
    			->bindValues([
    	  				':userId' => intval($id),
    	    			':orderId' => intval($orderId),
    	    	])
    	    	->queryScalar();

		return $result > 0;
    }

    public static function getContactPeopleByCompanyId($companyId) {
    	return Yii::$app->db->createCommand('
    				SELECT DISTINCT u.id, u.contact_email as email, u.first_name as firstName, u.last_name as lastName,
    					u.url_photo as urlPhoto, u.phone1 as phone
    				FROM user u
    				JOIN user_role ur ON u.id = ur.user_id
    				JOIN role r ON ur.role_id = r.id
    				WHERE u.company_id = :companyId
    				AND r.symbol = :roleClientSymbol
    				AND u.is_active = 1
    			')
        		->bindValues([
        				':companyId' => intval($companyId),
        				':roleClientSymbol' => Utility::ROLE_CLIENT,
        		])
        		->queryAll();
    }

    public static function getActiveEmployeeQuery($params = []) {

    	$userRoleSubquery = (new Query())->select(['GROUP_CONCAT(rt.name SEPARATOR ", ")'])
    			->from('role_translation rt')
    			->join('INNER JOIN', 'role r', 'r.id = rt.role_id')
    			->join('INNER JOIN', 'user_role ur', 'ur.role_id = r.id')
    			->join('INNER JOIN', 'language l', 'rt.language_id = l.id')
    			->where(['l.symbol' => Yii::$app->language])
    			->andWhere('ur.user_id = u.id');

    	$query = (new Query())
    			->select(['u.id', 'u.first_name', 'u.last_name', 'u.email', 'u.url_photo', 'u.phone1', 'role' => $userRoleSubquery])
    			->from('user u')
    			->join('LEFT JOIN', 'user_role ur', 'u.id = ur.user_id')
    			->join('LEFT JOIN', 'role r', 'ur.role_id = r.id')
    			->join('LEFT JOIN', 'role_translation rt', 'rt.role_id = r.id')
                ->andWhere('u.is_active')
    			->distinct();

    	$userRoleQuery = (new Query())
    			->select(['r.symbol'])
    			->from('role r')
    			->join('INNER JOIN', 'user_role ur', 'ur.role_id = r.id')
    			->where('ur.user_id = :id', [':id' => Yii::$app->user->id]);
    	
    	$query->andWhere(['or',
    			['r.symbol' => RoleHelper::getMyUnderlingEmployeeRoles()],
    			'r.symbol IS NULL',
    	]);	

    	$columnsMap = [
    			'id' => 'u.id',
    			'first_name' => 'u.first_name',
    			'last_name' => 'u.last_name',
    			'email' => 'u.email',
    			'role' => 'rt.name',
    			'url_photo' => 'u.url_photo',
    			'phone' => 'u.phone1',
    			'role' => 'r.name',
    	];

        if (!empty($params['filterString'])) {
        	$query->andWhere(['or',
        			['like', 'u.first_name', $params['filterString']],
        			['like', 'u.last_name', $params['filterString']],
        			['like', 'u.email', $params['filterString']],
        	]);
    	}


    	$roleIds = array_filter(empty($params['roleIds']) ? [] : $params['roleIds'], function($id) {
    		return !empty($id);
    	});


    	if(!empty($params['roleIds'])){
    		$includeNoRoles = false;
    		if(($params['roleIds'][0] !== '')) {
    			if(in_array(0, $params['roleIds'])) {
    				$includeNoRoles = true;
    			}
    		}

    		$subquery = (new Query())->select(['us.id'])
    				->from('user_role ur')
    				->join('RIGHT JOIN', 'user us', 'us.id = ur.user_id')
    				->where(['ur.role_id' => array_values($roleIds)]);

    		if($includeNoRoles) {
    			$subquery->orWhere(['ur.role_id' => NULL]);
    		}

    		$query->andWhere(['u.id' => $subquery]);
    	}

    	if(!empty($params['sortDir']) && !empty($columnsMap[$params['sortField']])) {
    		$query->orderBy([
    				$columnsMap[$params['sortField']] => (!empty($params['sortDir']) && $params['sortDir'] == 'desc') ? SORT_DESC : SORT_ASC
    		]);
    	}
    	else {
    		$query->orderBy(['last_name' => SORT_ASC]);
    	}

    	return $query;
    }

    public static function findEmployees($params = []) {
    	$query = (new \yii\db\Query())
		    	->select(['u.id', 'u.first_name as firstName', 'u.last_name as lastName', 'u.email as email'])
                ->distinct()
		    	->from('user u')
		    	->join('INNER JOIN', 'user_role ur', 'u.id = ur.user_id')
		    	->join('INNER JOIN', 'role r', 'ur.role_id = r.id')
    			->andWhere('u.is_active = 1');

    	$query->andWhere(['not in', 'r.symbol', [Utility::ROLE_CLIENT, Utility::ROLE_ADMIN]]);

    	if(!empty($params['name'])) {
    		$query->andWhere(['or',
    				['like', 'u.first_name', $params['name']],
    				['like', 'u.last_name', $params['name']],
    				['like', 'u.email', $params['name']],
    		]);
    	}

        if(!empty($params['roles'])) {
    		$query->andWhere(['r.symbol' => $params['roles']]);
        }

    	return $query;
	}

    public static function getRoleList() {

    	$roleSubstring = RoleHelper::getMyUnderlingEmployeeRoles();

    	$subquery =(new Query())->select([0, Yii::t('main', 'no role')])->from('dual');

    	return (new Query())->select(['r.id as id', 'rt.name as name'])
    			->from('role r')
    			->join('INNER JOIN', 'role_translation rt', 'r.id = rt.role_id')
    			->join('INNER JOIN', 'language l', 'rt.language_id = l.id')
    			->where(['l.symbol' => Yii::$app->language])
    			->andWhere(['r.symbol' => $roleSubstring])
                ->andWhere(['r.is_visible' => 1])
    			->union('(SELECT 0, "' . Yii::t('main', 'No role') . '" from DUAL)')
    			->orderBy('name ASC')->all();
    }

    public static function getDetails($id){    	
    	return (new Query())
		    	->select(['u.id', 'u.email', 'u.first_name', 'u.last_name',
		    			'u.phone1', 'u.phone2', 'u.phone3', 'u.url_photo', 'u.note',
		    			'u.date_employment', 'u.pesel', 'u.id_number',
		    			'l.name as language_name',
		    			'a.main as addressMain', 'a.complement as addressComplement', 'c.name cityName', 'c.zip_code as cityZipCode', 'p.name as provinceName', 'ct.name as countryName',
		    			'cu.symbol as currency_symbol',
		    			'u.is_active', 'u.date_creation', 'ua.date_action as last_login', 'u.date_modification', 
		    			'u.position',
		    	])
		    	->from('user u')
		    	->join('LEFT JOIN', 'language l', 'l.id = u.language_id')
		    	->join('LEFT JOIN', 'currency cu', 'cu.id = u.currency_id')
		    	->join('LEFT JOIN', 'user_action ua', 'ua.user_id = u.id')
		    	->join('LEFT JOIN', 'address a', 'a.id = u.address_id')
		    	->join('LEFT JOIN', 'city c', 'c.id = a.city_id')
		    	->join('LEFT JOIN', 'province p', 'p.id = c.province_id')
		    	->join('LEFT JOIN', 'country_translation ct', 'ct.country_id = p.country_id')
		    	->join('LEFT JOIN', 'language l2', 'l2.id = ct.language_id')
		    	->where(['u.id' => $id, 'u.is_active' => 1])
		    	->andWhere(['OR', 
		    			['l2.symbol' => Yii::$app->language],
		    			'l2.symbol IS NULL'
		    	])
		    	->andWhere(['not exists', 
		    			(new Query())->select('r.id')
		    					->from('role r')
		    					->join('INNER JOIN', 'user_role ur', 'r.id = ur.role_id')
		    					->where(['r.symbol' => Utility::ROLE_CLIENT])
		    					->andWhere('ur.user_id = u.id')
		    	])
		    	->orderBy('ua.date_action DESC')
		    	->one();
    }

    public static function getActiveStorekeepers() {
    	return (new Query())
		    	->select(['u.id', 'u.first_name', 'u.last_name', 'u.email'])
		    	->from('user u')
		        ->join('INNER JOIN', 'user_role ur', 'ur.user_id = u.id')
		        ->join('INNER JOIN', 'role r', 'r.id = ur.role_id')
		    	->andWhere(['u.is_active' => 1, 'r.symbol' => Utility::ROLE_STOREKEEPER])
		    	->all();
    }
    
    public static function getActiveMainStorekeepers() {
    	return (new Query())
		    	->select(['u.id', 'u.first_name', 'u.last_name', 'u.email'])
		    	->from('user u')
		        ->join('INNER JOIN', 'user_role ur', 'ur.user_id = u.id')
		        ->join('INNER JOIN', 'role r', 'r.id = ur.role_id')
		    	->andWhere(['u.is_active' => 1, 'r.symbol' => Utility::ROLE_MAIN_STOREKEEPER])
		    	->all();
    }
    
    public static function getAttachments($userId) {
    	return Yii::$app->db->createCommand('
    				SELECT ua.id as id, ua.url_file as urlFile, ua.name, ua.description, ua.date_creation as dateCreation,
    					u.first_name as firstName, u.last_name as lastName
					FROM user_attachment ua
					JOIN user u ON ua.user_id = u.id
					WHERE ua.user_attached_id = :userId
    				ORDER BY ua.date_creation
    			')
        		->bindValues([
        				':userId' => intval($userId),
        		])
        		->queryAll();
    }
    
    public static function getByFirstOrLastName($name) {
 		return (new Query())->select(['u.id'])
    			->from('user u')
    			->where(['u.company_id' => null])
    			->andWhere(['LIKE', 'LOWER(CONCAT(u.first_name, " ", u.last_name))', mb_strtolower($name, 'UTF-8')])
    			->all();
    }
    
    public static function getMainPhoto($id) {
    	return (new Query())->select(['u.url_photo'])
    			->from('user u')
    			->where(['u.id' => intval($id)])
    			->scalar();
    }
    
    public static function getClientRepresentatives($params = []) {
    	$query = (new Query())
		    	->select(['u.id', 'u.first_name as firstName', 'u.last_name as lastName', 'u.email', 'u.contact_email as contactEmail',
		    			'u.note', 'u.phone1 as phone', 'c.name as companyName', 'u.can_logged as canLog'])
		    	->from('user u')
		    	->join('LEFT JOIN', 'user_role ur', 'u.id = ur.user_id')
		    	->join('LEFT JOIN', 'role r', 'ur.role_id = r.id')
		    	->join('LEFT JOIN', 'company c', 'c.id = u.company_id')
		    	->andWhere('u.is_active')
		    	->andWhere(['r.symbol' => Utility::ROLE_CLIENT])
		    	->distinct();    	
    	
    	if(!empty($params['canLog'])) {
    		if($params['canLog'] == Utility::CLIENT_REPRESENTATIVE_LOG_PERMISSION_ONLY) {
    			$query->andWhere(['u.can_logged' => 1]);
    		} else if($params['canLog'] == Utility::CLIENT_REPRESENTATIVE_LOG_PROHIBITION_ONLY) {
    			$query->andWhere(['u.can_logged' => 0]);
    		}
    	}
    	
    	$columnsMap = [
    			'firstName' => 'u.first_name',
    			'lastName' => 'u.last_name',
    			'email' => 'u.contact_email',
    			'phone' => 'u.phone1',
    			'companyName' => 'c.name',
    			'note' => 'u.note',
    	];
    	
    	if(!empty($params['name'])) {
    		$query->andWhere(['or',
    				['like', 'u.first_name', $params['name']],
    				['like', 'u.last_name', $params['name']],
    				['like', 'u.contact_email', $params['name']],
    		]);
    	}
    	
    	if(!empty($params['companyName'])) {
    		$query->andWhere(['like', 'c.name', $params['companyName']]);
    	}
    	
    	if(!empty($params['sortDir']) && !empty($columnsMap[$params['sortField']])) {
    		$query->orderBy([
    				$columnsMap[$params['sortField']] => (!empty($params['sortDir']) && $params['sortDir'] == 'desc') ? SORT_DESC : SORT_ASC
    		]);
    	}
    	else {
    		$query->orderBy(['c.name' => SORT_ASC]);
    	}
    	
    	return $query;
    }
    
    public static function getClientRepresentativeDetails($id){
    	return (new Query())
    	->select(['u.id', 'u.email', 'u.first_name as firstName', 'u.last_name as lastName', 'u.can_logged as canLogged', 
    			'u.company_id as companyId', 'u.phone1', 'u.note', 'u.contact_email as contactEmail', 
    			'u.date_creation as dateCreation', 'ua.date_action as lastLogin', 'c.name as companyName',
    	])
    	->from('user u')
    	->join('LEFT JOIN', 'user_action ua', 'ua.user_id = u.id')
    	->join('LEFT JOIN', 'user_role ur', 'u.id = ur.user_id')
    	->join('LEFT JOIN', 'role r', 'ur.role_id = r.id')
    	->join('LEFT JOIN', 'company c', 'c.id = u.company_id')
    	->where(['u.id' => $id, 'u.is_active' => 1])
    	->andWhere(['r.symbol' => Utility::ROLE_CLIENT])
    	->orderBy('ua.date_action DESC')
    	->one();
    }
    
    public static function isOrderRepresentant($orderId, $userId) {
        return (new Query())
            ->from('order o')
            ->join('INNER JOIN', 'order_user_representative our', 'our.order_id = o.id')
            ->where(['o.id' => $orderId, 'our.user_representative_id' => $userId])
            ->count() > 0;
    }
    
    public static function getAdmins() {
        return (new Query())->select(['u.id'])
                ->from('user u')
                ->innerJoin('user_role ur', 'u.id = ur.user_id')
                ->innerJoin('role r', 'ur.role_id = r.id')
                ->where(['r.symbol' => Utility::ROLE_ADMIN])
                ->column();
    }
    
    public static function isClientOrderCreator($orderId, $userId) {
        return (new Query())
            ->from('order o')
            ->where(['o.id' => $orderId, 'user_id' => $userId])
            ->count() > 0;
    }
}
