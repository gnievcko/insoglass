<?php

namespace common\models\aq;

use Yii;
use yii\db\Query;
use \DateTime;
use \DateInterval;
use common\helpers\StringHelper;
use common\models\ar\Order;
use common\models\ar\Product;
use common\models\ar\Company;
use common\models\ar\Task;
use common\models\ar\Warehouse;
/**
 * This is the ActiveQuery class for [[\common\models\ar\Alert]].
 *
 * @see \common\models\ar\Alert
 */
class AlertQuery extends \yii\db\ActiveQuery {

    /*public function active() {
        $this->andWhere('[[status]]=1');
        return $this;
    }*/

    /**
     * @inheritdoc
     * @return \common\models\ar\Alert[]|array
     */
    public function all($db = null) {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return \common\models\ar\Alert|array|null
     */
    public function one($db = null) {
        return parent::one($db);
    }

    public static function getAlertPriorityCountInGivenInterval($beginning = null, $end = null) {
        $userId = Yii::$app->user->id;
    	$dateTime1 = new DateTime($beginning);
    	$dateTime2 = new DateTime($end);
    	$interval = $dateTime1->diff($dateTime2);

    	$dateTime2->add(DateInterval::createFromDateString('1 day'));

    	$subquery = (new Query())->select(['apt.name', 'a.date_creation'])
		    	->from('alert a')
		    	->join('INNER JOIN', 'alert_type at', 'a.alert_type_id = at.id')
		    	->join('INNER JOIN', 'alert_priority ap', 'at.alert_priority_id = ap.id')
		    	->join('INNER JOIN', 'alert_priority_translation apt', 'apt.alert_priority_id = ap.id')
		    	->join('INNER JOIN', 'language l', 'l.id = apt.language_id')
		    	->where(['l.symbol' => Yii::$app->language])
		    	->andWhere('a.is_active = 1')
                ->andWhere(['or', ['a.user_id' => intval($userId)], ['a.user_id' => null]])
		    	->andWhere('date_creation >= :beginning', [':beginning' => $dateTime1->format('Y-m-d')])
		    	->andWhere('date_creation <= :end', [':end' => $dateTime2->format('Y-m-d')]);

    	if ($interval->days == 365 || $interval->days == 366) {
    		$query = (new Query())->select(['count(name) as number', 'name', 'EXTRACT(MONTH FROM date_creation) as time'])
    			->from('(' . $subquery->createCommand()->rawSql . ') as x')
    			->groupBy(['name', 'time']);
    			$maxCol = 12;
    	}
    	else {
    		$query = (new Query())->select(['count(name) as number', 'name', 'EXTRACT(DAY FROM date_creation) as time'])
    		->from('(' . $subquery->createCommand()->rawSql . ') as x')
    		->groupBy(['name', 'time']);
    		$maxCol = $interval->days+1;//must add 1, becouse diff between 1 and 31 is 30, so...
    	}

    	$row = 0;
    	$col = 0;

    	$queryResult = $query->all();
    	$currentLabel = count($queryResult) ? $queryResult[0]['name'] : '';

    	$resultLabels = [];
    	$resultData = [];
    	$subtract = intval(substr($beginning, strlen($beginning) - 2));

    	for($i = 0; $i < sizeof($queryResult); $i++) {
    		if ($queryResult[$i]['name'] != $currentLabel) {
    			while($col != $maxCol) {
    				$col++;
    				$resultData[$row][] = 0;
    			}
    			$col = 0;
    			$row++;
    			$currentLabel = $queryResult[$i]['name'];
    		}
    		$resultLabels[$row] = $queryResult[$i]['name'];

    		while($col != $queryResult[$i]['time'] - ($subtract)) {
    			$col++;
    			$resultData[$row][] = 0;
    		}
    		$resultData[$row][$col] = $queryResult[$i]['number'];
    	}

    	while($col != $maxCol) {
          	$col++;
    		$resultData[$row][] = 0;
    	}

    	$result[0] = !empty($resultLabels) ? $resultLabels : [''];
    	$result[1] = $resultData;

    	return $result;
    }

    public static function getActiveAlertsQuery($userId, $params = []) {
    	$query = (new Query())->select(['a.id', 'a.message', 'a.date_creation as dateCreation', 'ap.symbol as prioritySymbol'])
		    	->from('alert a')
		    	->join('INNER JOIN', 'alert_type at', 'a.alert_type_id = at.id')
		    	->join('INNER JOIN', 'alert_priority ap', 'at.alert_priority_id = ap.id')
		    	->andWhere(['or', ['a.user_id' => intval($userId)], ['a.user_id' => null]])
    			->andWhere(['at.is_enabled' => true]);

    	if(!empty($params['alertTypeId'])) {
    		$query->andWhere(['a.alert_type_id' => $params['alertTypeId']]);
    	}

    	$query->orderBy(['a.date_creation' => SORT_DESC]);
    	return $query;
    }

    public static function getDetails($id) {
    	$data = (new Query())->select(['att.name as type', 'apt.name as priority', 'a.message', 'a.date_creation as dateCreation',
		    			'u.first_name as firstName', 'u.last_name as lastName', 'u.email as email',
    					'a.order_id as orderId', 'a.product_id as productId', 'a.company_id as companyId', 'a.task_id as taskId', 'a.warehouse_id as warehouseId',
		    	])
		    	->from('alert a')
		    	->join('INNER JOIN', 'alert_type at', 'a.alert_type_id = at.id')
		    	->join('INNER JOIN', 'alert_type_translation att', 'at.id = att.alert_type_id')
		    	->join('INNER JOIN', 'language l', 'att.language_id = l.id')
		    	->join('INNER JOIN', 'alert_priority_translation apt', 'at.alert_priority_id = apt.alert_priority_id')
		    	->join('INNER JOIN', 'language l1', 'apt.language_id = l1.id')
		    	->join('LEFT JOIN', 'user u', 'a.user_id = u.id')
		    	->where(['a.id' => intval($id)])
		    	->one();

    	$data['results'] = [];
    	if(!empty($data['orderId'])) {
    		$data['results'][] = [
    				'url' => 'order/details/{id}',
    				'label' => StringHelper::translateOrderToOffer('main', 'Concerns order'),
    				'name' => Order::findOne($data['orderId'])->title,
    				'id' => $data['orderId'],
    				'table' => 'order',
    		];
    	}
    	if(!empty($data['productId'])) {
    		$data['results'][] = [
    				'url' => 'product/details/{id}',
    				'label' => Yii::t('main', 'Concerns product'),
    				'name' => Product::findOne($data['productId'])->getTranslation()->name,
    				'id' => $data['productId'],
    				'table' => 'product',
    		];
    	}
    	if(!empty($data['companyId'])) {
    		$data['results'][] = [
    				'url' => 'client/details/{id}',
    				'label' => Yii::t('main', 'Concerns client'),
    				'name' => Company::findOne($data['companyId'])->name,
    				'id' => $data['companyId'],
    				'table' => 'company',
    		];
    	}
    	if(!empty($data['taskId'])) {
    		$data['results'][] = [
    				'url' => '',
    				'label' => Yii::t('main', 'Concerns task'),
    				'name' => Task::findOne($data['taskId'])->title,
    				'id' => $data['taskId'],
    				'table' => 'task',
    		];
    	}
    	if(!empty($data['warehouseId'])) {
    		$data['results'][] = [
    				'url' => 'warehouse/details/{id}',
    				'label' => Yii::t('main', 'Concerns warehouse'),
    				'name' => Warehouse::findOne($data['warehouseId'])->name,
    				'id' => $data['warehouseId'],
    				'table' => 'warehouse',
    		];
    	}

    	return $data;
    }
}
