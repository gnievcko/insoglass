<?php

namespace common\models\aq;

use Yii;
use yii\db\Query;

/**
 * This is the ActiveQuery class for [[\common\models\ar\Activity]].
 *
 * @see \common\models\ar\Activity
 */
class ActivityQuery extends \yii\db\ActiveQuery {

    /*public function active() {
        $this->andWhere('[[status]]=1');
        return $this;
    }*/

    /**
     * @inheritdoc
     * @return \common\models\ar\Activity[]|array
     */
    public function all($db = null) {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return \common\models\ar\Activity|array|null
     */
    public function one($db = null) {
        return parent::one($db);
    }
    
    public static function getDetails($id){
        return (new Query())
                ->select(['a.id as id', 'atr.name as name', 'a.effort_factor'])
                ->from(['activity a'])
                ->join('INNER JOIN', 'activity_translation atr', 'a.id = atr.activity_id')
                ->join('INNER JOIN', 'language l', 'atr.language_id = l.id')
                ->where(['l.symbol' => Yii::$app->language])
                ->andWhere(['a.id' => $id])->one();
    }
    
    public static function getActivityQuery($params = []){
        $query = (new Query())
                ->select(['a.id', 'a.symbol', 'atr.name'])
                ->from('activity a')
                ->join('INNER JOIN', 'activity_translation atr', 'a.id = atr.activity_id')
                ->join('INNER JOIN', 'language l', 'atr.language_id = l.id')
                ->where(['l.symbol' => Yii::$app->language]);
        
        $columnsMap = [
            'id' => 'a.id',
            'symbol' => 'a.symbol',
            'name' => 'atr.name',
        ];

        if(!empty($params['name'])) {
            $query->andWhere(['like', 'atr.name', $params['name']]);
        }
        if(!empty($params['sortDir']) && !empty($columnsMap[$params['sortField']])){
            $query->orderBy([
                $columnsMap[$params['sortField']] => (!empty($params['sortDir']) && $params['sortDir'] == 'desc') ? SORT_DESC : SORT_ASC
            ]);
        }
        else{
            $query->orderBy([$columnsMap['name'] => SORT_ASC]);
        }
        return $query;
    }
}