<?php

namespace common\models\aq;

/**
 * This is the ActiveQuery class for [[\common\models\ar\ProductAttachment]].
 *
 * @see \common\models\ar\ProductAttachment
 */
class ProductAttachmentQuery extends \yii\db\ActiveQuery {

    /*public function active() {
        $this->andWhere('[[status]]=1');
        return $this;
    }*/

    /**
     * @inheritdoc
     * @return \common\models\ar\ProductAttachment[]|array
     */
    public function all($db = null) {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return \common\models\ar\ProductAttachment|array|null
     */
    public function one($db = null) {
        return parent::one($db);
    }
}