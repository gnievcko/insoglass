<?php

namespace common\models\aq;

use Yii;
use yii\db\Query;
use common\helpers\Utility;
use yii\db\Expression;
/**
 * This is the ActiveQuery class for [[\common\models\ar\WarehouseProductStatusHistory]].
 *
 * @see \common\models\ar\WarehouseProductStatusHistory
 */
class WarehouseProductStatusHistoryQuery extends \yii\db\ActiveQuery {

    /*public function active() {
        $this->andWhere('[[status]]=1');
        return $this;
    }*/

    /**
     * @inheritdoc
     * @return \common\models\ar\WarehouseProductStatusHistory[]|array
     */
    public function all($db = null) {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return \common\models\ar\WarehouseProductStatusHistory|array|null
     */
    public function one($db = null) {
        return parent::one($db);
    }
    
    public static function getStatusesQueryByProductId($productId, $params = []) {  
    	$countAdditionalData = (new Query())->select(['COUNT(wpshd.history_id)'])
    			->from('warehouse_product_status_history_data wpshd')
    			->where('wpshd.history_id = wpsh.id');
    	
    	$historyBlockedSubquery = (new Query())->select(['wpsh2.id'])
    			->from('warehouse_product_status_history wpsh2')
    			->where('wpsh2.history_blocked_id = wpsh.id')
    			->orderBy('wpsh2.id')
    			->limit(1);
    		
    	$orderSubquery = (new Query())->select(['wpshd.referenced_object_id'])
    			->from('warehouse_product_status_history_data wpshd')
    			->join('INNER JOIN', 'product_status_reference psr', 'wpshd.product_status_reference_id = psr.id')
    			->where('wpsh.id = wpshd.history_id')
    			->andWhere('psr.name_table = "order"')
    			->limit(1);
    			
    	$query = (new Query())
    			->select([
    					'wpsh.id', 'w.name as warehouse', 'p1.id as parent', 'pt.name as version', 'ps.symbol as statusSymbol', 
    					'ps.is_positive as isPositive', 'pst.name as status', 'wpsh.count', 'wpsh.description', 
    					'UNIX_TIMESTAMP(wpsh.date_creation) as dateCreation',
    					'u.first_name as firstName', 'u.last_name as lastName', 'u.email as email',
    					'countAdditionalData' => $countAdditionalData, 'wpsh.price_unit as price_unit', 'wpsh.price_group as price_group',
    					'c.short_symbol as currencyShortSymbol', 'orderId' => $orderSubquery, 
    					'blockingStatusId' => $historyBlockedSubquery, 'wpsh.warehouse_delivery_id as deliveryId'
    			])
    			->from('warehouse_product_status_history wpsh')
    			->join('INNER JOIN', 'product_status ps', 'wpsh.product_status_id = ps.id')
    			->join('INNER JOIN', 'product_status_translation pst', 'wpsh.product_status_id = pst.product_status_id')
    			->join('INNER JOIN', 'language l', 'pst.language_id = l.id')
    			->join('INNER JOIN', 'warehouse_product wp', 'wpsh.warehouse_product_id = wp.id')
    			->join('INNER JOIN', 'product p', 'wp.product_id = p.id')
    			->join('INNER JOIN', 'product_translation pt', 'p.id = pt.product_id')
    			->join('INNER JOIN', 'language l2', 'pt.language_id = l2.id')
    			->join('INNER JOIN', 'warehouse w', 'wp.warehouse_id = w.id')
    			->join('INNER JOIN', 'user u', 'wpsh.user_confirming_id = u.id')
    			->join('LEFT JOIN', 'currency c', 'c.id = wpsh.currency_id')
    			->join('LEFT JOIN', 'product p1', 'p.parent_product_id = p1.id')
    			->join('LEFT JOIN', 'product_translation pt1', 'p1.id = pt1.product_id')
    			->join('LEFT JOIN', 'language l1', 'pt1.language_id = l1.id')
    			->where(['or', ['p.id' => intval($productId)], ['p.parent_product_id' => intval($productId)]])
    			->andWhere(['l.symbol' => Yii::$app->language])
    			->andWhere(['l2.symbol' => Yii::$app->language])
    			->andWhere(['or', 'l1.symbol IS NULL', ['l1.symbol' => Yii::$app->language]]);
    
    	$warehouseIds = array_filter(empty($params['warehouseIds']) ? [] : $params['warehouseIds'], function($id) {
    		return !empty($id);
    	});
    			 
    	if(!empty($warehouseIds) && count($warehouseIds) < intval($params['allWarehousesCount'])) {
    		$query->andWhere(['wp.warehouse_id' => $warehouseIds]);
    	}
    	//['ps.is_positive', 'wpsh.count']
    	
    	$columnsMap = [
    			'warehouse' => 'w.name',
				'version' => 'pt.name',
				'status' => 'pst.name',
				'count' => 'ps.is_positive, wpsh.count',
				'description' => 'wpsh.description',
				'user' => 'u.last_name',
				'dateCreation' => 'wpsh.date_creation',
    	];
    
    	if(!empty($params['sortDir']) && !empty($columnsMap[$params['sortField']]) && ($params['sortField'] != 'count')) {
    		$query->orderBy([
    				$columnsMap[$params['sortField']] => (!empty($params['sortDir']) && $params['sortDir'] == 'desc') ? SORT_DESC : SORT_ASC
    		]);
    	}
    	
    	if ($params['sortField'] == 'count') {
    		$sortDir = 'ASC';
    		if (!empty($params['sortDir']) && $params['sortDir'] == 'desc') {
    			$sortDir = 'DESC';
    		}
    		$query->orderBy('ps.is_positive ' . $sortDir . ' ,'  . 'wpsh.count ' . $sortDir);
    	}
    
    	return $query;
    }
    
    public static function getHeaderInformationByHistoryId($historyId) {
    	$result = (new Query())->select(['wpsh.id', 'w.name as warehouseName', 'ps.symbol as productStatusSymbol', 'pst.name as productStatusName',
    				'u.first_name as userConfirmingFirstName', 'u.last_name as userConfirmingLastName', 'u.email as userConfirmingEmail',
	    			'DATE_FORMAT(wpsh.date_creation, "%d.%m.%Y %H:%i") as dateOperation', 'w.id as warehouseId', 
	    			'u.id as userConfirmingId', 'wpsh.description', 'u2.first_name as issuerFirstName', 'u2.last_name as issuerLastName',
    				'o.number as orderNumber', 'o.title as orderTitle', 'wpo.number', 'wpo.is_accounted as isAccounted'
		    	])
		    	->from('warehouse_product_status_history wpsh')
		    	->join('INNER JOIN', 'warehouse_product_operation wpo', 'wpsh.operation_id = wpo.id')
		    	->join('INNER JOIN', 'warehouse_product wp', 'wpsh.warehouse_product_id = wp.id')
		    	->join('INNER JOIN', 'warehouse w', 'wp.warehouse_id = w.id')
		    	->join('INNER JOIN', 'user u', 'wpsh.user_confirming_id = u.id')
		    	->join('INNER JOIN', 'warehouse_product_status_history_data wpshd', 'wpsh.id = wpshd.history_id')
		    	->join('LEFT JOIN', 'order o', 'o.id = wpshd.referenced_object_id')
		    	->join('LEFT JOIN', 'user u2', 'u2.id = wpshd.referenced_object_id')
		    	->join('INNER JOIN', 'product_status ps', 'ps.id = wpsh.product_status_id')
		    	->join('INNER JOIN', 'product_status_translation pst', 'wpsh.product_status_id = pst.product_status_id')
		    	->join('INNER JOIN', 'language l', 'pst.language_id = l.id')
		    	->andWhere(['l.symbol' => Yii::$app->language])
		    	->andWhere(['wpsh.id' => intval($historyId)])
		    	->one();    	
		    	
		if(!empty($result)) {
			$resultUser = (new Query())->select(['u1.first_name as userReceivingFirstName', 
						'u1.last_name as userReceivingLastName', 'u1.email as userReceivingEmail'
					])
					->from('warehouse_product_status_history_data wpshd')
					->join('INNER JOIN', 'product_status_reference psr', 'wpshd.product_status_reference_id = psr.id')
    
					->join('INNER JOIN', 'user u1', 'wpshd.referenced_object_id = u1.id')
					->where(['wpshd.history_id' => intval($historyId)])
					->andWhere(['psr.name_table' => 'user'])
					->one();
			
            if(!empty($resultUser)) {
                $result['userReceivingFirstName'] = $resultUser['userReceivingFirstName'];
                $result['userReceivingLastName'] = $resultUser['userReceivingLastName'];
                $result['userReceivingEmail'] = $resultUser['userReceivingEmail'];
            }

            $dataRows = WarehouseProductStatusHistoryDataQuery::getDataByHistoryId($historyId);
            if(!empty($dataRows) && is_array($dataRows)) {
                foreach($dataRows as $data) {
	                if($data['table'] == 'order') {
	                    $order = \common\models\ar\Order::findOne(['id' => $data['id']]);
	                    $address = $order->company->address;
	                    $full = '';
	                    if(!empty($address)) {
	                        $full = $address->getFullAddress();
	                    }
	                    //$result['description'] .="\n".Yii::t('documents', 'Goes to')."\n".$order->company->name.", ".$full;
	                    
	                    $result['goesTo'] = $order->company->name.(!empty($full) ? ', '.$full : '');
	                    break;
	                }
                }
            }
        }
		return $result;
    }
    
    public static function getProductsByHistoryId($historyId) {
    	return (new Query())->select(['p.index', 'pt.name', 'wpsh.count', 'pt1.name as parentName', 'wp.stillage', 
	    			'wp.shelf', 'wpsh.unit',    				
    			])
    			->from('warehouse_product_status_history wpsh')
    			->join('INNER JOIN', 'warehouse_product wp', 'wpsh.warehouse_product_id = wp.id')
    			->join('INNER JOIN', 'product p', 'wp.product_id = p.id')
    			->join('INNER JOIN', 'product_translation pt', 'p.id = pt.product_id')
    			->join('INNER JOIN', 'language l', 'pt.language_id = l.id')
    			->join('LEFT JOIN', 'product p1', 'p.parent_product_id = p1.id')
    			->join('LEFT JOIN', 'product_translation pt1', 'p1.id = pt1.product_id')
    			->join('LEFT JOIN', 'language l1', 'pt1.language_id = l1.id')
    			->where(['wpsh.operation_id' => (new Query())->select(['wpsh1.operation_id'])
    					->from('warehouse_product_status_history wpsh1')
    					->where(['wpsh1.id' => intval($historyId)])
    			])
    			->andWhere(['l.symbol' => Yii::$app->language])
    			->andWhere(['or', ['l1.symbol' => null], ['l1.symbol' => Yii::$app->language]])
    			->orderBy('pt.name')
    			->all();
    }
    
    public static function getUtilisationHistoryByDate($dateFrom, $dateTo) {
    	$subquery = (new Query())->select(['wpshd.custom_value'])
    			->from('warehouse_product_status_history_data wpshd')
    			->join('INNER JOIN', 'product_status_reference psr', 'wpshd.product_status_reference_id = psr.id')
    			->where('wpshd.history_id = wpsh.id')
    			->andWhere(['psr.symbol' => 'userUtilized'])
    			->limit(1);
    	
    	return (new Query())->select(['pt.name', 'wpsh.count', 'CONCAT(u.first_name, " ", u.last_name) as user', 
    				'wpsh.date_creation', 'wpsh.description', 'userReceiving' => $subquery])
    			->from('warehouse_product_status_history wpsh')
    			->join('INNER JOIN', 'warehouse_product wp', 'wpsh.warehouse_product_id = wp.id')
    			->join('INNER JOIN', 'product p', 'wp.product_id = p.id')
    			->join('INNER JOIN', 'product_translation pt', 'pt.product_id = p.id')
    			->join('INNER JOIN', 'user u', 'u.id = wpsh.user_confirming_id')
    			->join('INNER JOIN', 'product_status ps', 'ps.id = wpsh.product_status_id')
    			->where(['ps.symbol' => Utility::PRODUCT_STATUS_UTILIZATION])
    			->andWhere('wpsh.date_creation >= :dateFrom',  [':dateFrom' => $dateFrom])
    			->andWhere('wpsh.date_creation <= :dateTo', [':dateTo' => $dateTo])
    			->andWhere(['p.is_active' => true])
    			->orderBy('wpsh.date_creation DESC')
    			->all();

    }
    
    public static function getUtilisedProductCountByDate($dateFrom, $dateTo) {
    	return (new Query())->select(['pt.name', 'SUM(wpsh.count) as count'])
		    	->from('warehouse_product_status_history wpsh')
		    	->join('INNER JOIN', 'warehouse_product wp', 'wpsh.warehouse_product_id = wp.id')
    			->join('INNER JOIN', 'product p', 'wp.product_id = p.id')
    			->join('INNER JOIN', 'product_translation pt', 'pt.product_id = p.id')
		    	->join('INNER JOIN', 'product_status ps', 'ps.id = wpsh.product_status_id')
		    	->where(['ps.symbol' => Utility::PRODUCT_STATUS_UTILIZATION])
		    	->andWhere('wpsh.date_creation >= :dateFrom',  [':dateFrom' => $dateFrom])
		    	->andWhere('wpsh.date_creation <= :dateTo', [':dateTo' => $dateTo])
		    	->andWhere(['p.is_active' => true])
		    	->groupBy('pt.name')
		    	->all();
    }
    
    public static function getIssuedProductsForReport($dateFrom, $dateTo) {
    	$subQuery = (new Query())->select(['wpsh.id', 'wpsh.date_creation', 'wpsh.description'])
    			->from('warehouse_product_status_history wpsh')
    			->join('INNER JOIN', 'product_status ps', 'wpsh.product_status_id = ps.id')
    			->where(['in', 'ps.symbol', [Utility::PRODUCT_STATUS_ISSUING, Utility::PRODUCT_STATUS_ISSUING_RESERVATION]])
    			->andWhere('wpsh.date_creation >= :dateFrom',  [':dateFrom' => $dateFrom])
    			->andWhere('wpsh.date_creation <= :dateTo', [':dateTo' => $dateTo]);
        
        $result = [];
    	foreach($subQuery->all() as $row) {
            $resultRow = [
                'header' => self::getHeaderInformationByHistoryId($row['id']),
                'products' => self::getProductsByHistoryId($row['id']),
            ];
            $result[$resultRow['header']['dateOperation']] = $resultRow;
    	}
    	return $result;
    }
 
    public static function getBalanceFromDateRange($dateFrom, $dateTo) {
    	return (new Query())->select(['wp.product_id as productId', 'ps.is_positive as isPositive', new Expression('SUM(wpsh.count) AS count')])
    			->from('warehouse_product_status_history wpsh')
    			->join('INNER JOIN', 'warehouse_product wp', 'wpsh.warehouse_product_id = wp.id')
    			->join('INNER JOIN', 'product_status ps', 'wpsh.product_status_id = ps.id')
    			->where('DATE(wpsh.date_creation) >= :dateFrom AND DATE(wpsh.date_creation) <= :dateTo', [
    					':dateFrom' => $dateFrom, ':dateTo' => $dateTo
    			])
    			->groupBy(['wp.product_id', 'ps.is_positive'])
    			->orderBy(['wp.product_id' => SORT_ASC])
    			->all();
    }
}