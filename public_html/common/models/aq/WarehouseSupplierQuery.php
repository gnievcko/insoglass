<?php

namespace common\models\aq;

use Yii;
use common\models\ar\WarehouseSupplier;
use common\models\ar\Warehouse;
use common\helpers\Utility;
use yii\db\Query;

/**
 * This is the ActiveQuery class for [[\common\models\ar\WarehouseSupplier]].
 *
 * @see \common\models\ar\WarehouseSupplier
 */
class WarehouseSupplierQuery extends \yii\db\ActiveQuery {

    /*public function active() {
        $this->andWhere('[[status]]=1');
        return $this;
    }*/

    /**
     * @inheritdoc
     * @return \common\models\ar\WarehouseSupplier[]|array
     */
    public function all($db = null) {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return \common\models\ar\WarehouseSupplier|array|null
     */
    public function one($db = null) {
        return parent::one($db);
    }
 
    public static function getActiveSuppliersQuery($params = []) {
    	 
    	$query = (new Query())
    			->select(['ws.id', 'ws.name', 'ws.is_active', 'ws.date_creation', 'ws.email', 'ws.phone', 'ws.contact_person as contactPerson',
    					'a.main as addressMain', 'a.complement as addressComplement', 'c.name cityName', 'c.zip_code as cityZipCode', 'p.name as provinceName', 'ct.name as countryName',
    			])
    			->from('warehouse_supplier ws')
    			->join('LEFT JOIN', 'address a', 'a.id = ws.address_id')
    			->join('LEFT JOIN', 'city c', 'c.id = a.city_id')
    			->join('LEFT JOIN', 'province p', 'p.id = c.province_id')
    			->join('LEFT JOIN', 'country_translation ct', 'ct.country_id = p.country_id')
    			->join('LEFT JOIN', 'language l', 'l.id = ct.language_id')
    			->where(['l.symbol' => Yii::$app->language])
    			->where('ws.is_active = 1')
    			->andWhere(['or', ['l.symbol' => Yii::$app->language], ['a.main' => null]]);
    
    	$columnsMap = [
    			'id' => 'ws.id',
    			'name' => 'ws.name',
    			'contact_person' => 'ws.contact_person',
    			'phone' => 'ws.phone',
    			'email' => 'ws.email',
    			'address' => 'a.main',
    			'date_creation' => 'ws.date_creation',
    	];
    	
    	if (!empty($params['nameFilterString'])) {
    		$query->andWhere(['like', 'ws.name', $params['nameFilterString']]);
    	}
    		 
    	if(!empty($params['sortDir']) && !empty($columnsMap[$params['sortField']])) {
    		$query->orderBy([
    				$columnsMap[$params['sortField']] => (!empty($params['sortDir']) && $params['sortDir'] == 'desc') ? SORT_DESC : SORT_ASC
    		]);
    	}
    	else {
    		$query->orderBy(['ws.name' => SORT_ASC]);
    	}
    	return $query;
    }
    
    public static function getDetails($id){   	
    	return (new Query())
    			->select(['ws.id', 'ws.name', 'ws.is_active', 'ws.date_creation', 'ws.phone', 'ws.email', 'ws.contact_person as contactPerson',
    					'a.main as addressMain', 'a.complement as addressComplement', 'c.name cityName', 'c.zip_code as cityZipCode', 'p.name as provinceName', 'ct.name as countryName',
    			])
    			->from('warehouse_supplier ws')
    			->join('LEFT JOIN', 'address a', 'a.id = ws.address_id')
    			->join('LEFT JOIN', 'city c', 'c.id = a.city_id')
    			->join('LEFT JOIN', 'province p', 'p.id = c.province_id')
    			->join('LEFT JOIN', 'country_translation ct', 'ct.country_id = p.country_id')
    			->join('LEFT JOIN', 'language l', 'l.id = ct.language_id')
    			->Where(['or', ['l.symbol' => Yii::$app->language], ['a.main' => null]])
    			->andWhere(['ws.id' => $id])
    			->one();
    }
    
    public static function getSupplierList() {  	 
    	return (new Query())->select(['ws.id', 'ws.name'])
    	->from('warehouse_supplier ws')
    	->where(['is_active' => true])
    	->orderBy('ws.name')
    	->all();
    }
       
}
