<?php

namespace common\models\aq;

use Yii;
use common\helpers\Utility;
use yii\db\Expression;
use yii\db\Query;

/**
 * This is the ActiveQuery class for [[\common\models\ar\WarehouseProductOperation]].
 *
 * @see \common\models\ar\WarehouseProductOperation
 */
class WarehouseProductOperationQuery extends \yii\db\ActiveQuery {

    /*public function active() {
        $this->andWhere('[[status]]=1');
        return $this;
    }*/

    /**
     * @inheritdoc
     * @return \common\models\ar\WarehouseProductOperation[]|array
     */
    public function all($db = null) {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return \common\models\ar\WarehouseProductOperation|array|null
     */
    public function one($db = null) {
        return parent::one($db);
    }
    
    public static function findOperations($params = []) {
    	// TODO: W przyszlosci dodac parametr zwiazany z magazynem
    	$query = (new Query())
		    	->select(['wpo.id', 'wpo.number'])
		    	->from('warehouse_product_operation wpo')
		    	->join('INNER JOIN', 'product_status ps', 'wpo.product_status_id = ps.id')
    			->orderBy(['wpo.number' => SORT_ASC]);
    
    	if(!empty($params['number'])) {
    		$query->andWhere(['like', 'wpo.number', $params['number']]);
    	}
    
    	if(!empty($params['types'])) {
    		$query->andWhere(['ps.symbol' => $params['types']]);
    	}
    
    	return $query;
    }
    
	public static function getActiveIssuesQuery($params = []) {
		$companySubquery = (new Query())->select(['c.name'])
				->from('warehouse_product_status_history_data wpshd')
				->join('INNER JOIN', 'product_status_reference psr', 'wpshd.product_status_reference_id = psr.id')
				->join('INNER JOIN', 'order o', 'wpshd.referenced_object_id = o.id')
				->join('INNER JOIN', 'company c', 'o.company_id = c.id')
				->where('wpshd.history_id = wpsh.id')
				->andWhere('psr.symbol = "orderIssued"');
		
		$countSubquery = (new Query())->select(new Expression('COUNT(wpsh.id)'))
				->from('warehouse_product_status_history wpsh')
				->where('wpsh.operation_id = wpo.id');
		
    	$query = (new Query())
    			->distinct()
		    	->select([
		    			'wpo.id', 'UNIX_TIMESTAMP(wpo.date_creation) as dateCreation', 'wpo.number',
		    			'u.first_name as userConfirmingFirstName', 'u.last_name as userConfirmingLastName', 
		    			'u.email as userConfirmingEmail', 'positionCount' => $countSubquery, 
		    			'u1.first_name as userReceivingFirstName', 'u1.last_name as userReceivingLastName', 
		    			'u1.email as userReceivingEmail', 'companyName' => $companySubquery->limit(1),
		    			'wpo.is_accounted as isAccounted',
		    	])
		    	->from('warehouse_product_operation wpo')
		    	->join('INNER JOIN', 'product_status ps', 'wpo.product_status_id = ps.id')
		    	->join('INNER JOIN', 'user u', 'wpo.user_confirming_id = u.id')
		    	->join('LEFT JOIN', 'warehouse_product_status_history wpsh', 'wpo.id = wpsh.operation_id')
		    	->join('LEFT JOIN', 'warehouse_product_status_history_data wpshd', 'wpsh.id = wpshd.history_id')
		    	->join('LEFT JOIN', 'product_status_reference psr', 'wpshd.product_status_reference_id = psr.id')
		    	->join('LEFT JOIN', 'user u1', 'wpshd.referenced_object_id = u1.id')
		    	->where(['ps.symbol' => [Utility::PRODUCT_STATUS_ISSUING, Utility::PRODUCT_STATUS_ISSUING_RESERVATION]])
		    	->andWhere(['or', 'wpshd.history_id IS NULL', 'psr.symbol = "userIssued"', ['ps.symbol' => Utility::PRODUCT_STATUS_ISSUING_RESERVATION]]);

    	if(!empty($params['number'])) {
			$query->andWhere(['like', 'wpo.number', $params['number']]);
    	}
    	
    	$columnsMap = [
    			'number' => 'wpo.number',
    			'positionCount' => 'COUNT(wpsh.id)',
    			'userConfirmingName' => 'CONCAT(u.first_name, u.last_name)',
    			'userReceivingName' => 'CONCAT(u1.first_name, u1.last_name)',
    			'companyName' => '('.$companySubquery->createCommand()->rawSql.')',
    			'isAccounted' => 'wpo.is_accounted',
    			'dateCreation' => 'UNIX_TIMESTAMP(wpo.date_creation)',
    	];

    	if(!empty($params['sortDir']) && !empty($columnsMap[$params['sortField']])) {
            $query->orderBy([
                $columnsMap[$params['sortField']] => (!empty($params['sortDir']) && $params['sortDir'] == 'desc') ? SORT_DESC : SORT_ASC
            ]);
        }

		return $query;
    }
    
    public static function getIssueDetails($id) {
    	$companySubquery = (new Query())->select([new Expression('CONCAT(c.id, "|", c.name)')])
		    	->from('warehouse_product_status_history_data wpshd')
		    	->join('INNER JOIN', 'product_status_reference psr', 'wpshd.product_status_reference_id = psr.id')
		    	->join('INNER JOIN', 'order o', 'wpshd.referenced_object_id = o.id')
		    	->join('INNER JOIN', 'company c', 'o.company_id = c.id')
		    	->where('wpshd.history_id = wpsh.id')
		    	->andWhere('psr.symbol = "orderIssued"');
    	
    	return (new Query())->select(['wpo.id', 'pst.name as type', 'wpo.date_creation as dateCreation', 'wpo.number',
		    		'u.first_name as userConfirmingFirstName', 'u.last_name as userConfirmingLastName',
    				'u.email as userConfirmingEmail',
    				'u1.first_name as userReceivingFirstName', 'u1.last_name as userReceivingLastName',
    				'u1.email as userReceivingEmail', 'companyName' => $companySubquery->limit(1),
    				'w.id as warehouseId', 'w.name as warehouseName', 'wpo.is_accounted as isAccounted',
    				'u.id as userConfirmingId', 'u1.id as userReceivingId', 'wpsh.id as firstProductId',
    				'wpo.description'
    			])
				->from('warehouse_product_operation wpo')
				->join('INNER JOIN', 'warehouse w', 'wpo.warehouse_id = w.id')
		    	->join('INNER JOIN', 'product_status ps', 'wpo.product_status_id = ps.id')
		    	->join('INNER JOIN', 'product_status_translation pst', 'ps.id = pst.product_status_id')
		    	->join('INNER JOIN', 'language l', 'pst.language_id = l.id')
		    	->join('INNER JOIN', 'user u', 'wpo.user_confirming_id = u.id')
		    	->join('LEFT JOIN', 'warehouse_product_status_history wpsh', 'wpo.id = wpsh.operation_id')
		    	->join('LEFT JOIN', 'warehouse_product_status_history_data wpshd', 'wpsh.id = wpshd.history_id')
		    	->join('LEFT JOIN', 'product_status_reference psr', 'wpshd.product_status_reference_id = psr.id')
		    	->join('LEFT JOIN', 'user u1', 'wpshd.referenced_object_id = u1.id')
		    	->where(['ps.symbol' => [Utility::PRODUCT_STATUS_ISSUING, Utility::PRODUCT_STATUS_ISSUING_RESERVATION]])
		    	->andWhere(['or', 'wpshd.history_id IS NULL', 'psr.symbol = "userIssued"', ['ps.symbol' => Utility::PRODUCT_STATUS_ISSUING_RESERVATION]])
		    	->andWhere(['wpo.id' => intval($id)])
		    	->andWhere(['l.symbol' => Yii::$app->language])
		    	->one();		    			 
    }
    
    public static function getIssueProductsQuery($issueId, $params = []) {
    	$query = (new Query())->select([
    				'p.id as id', 'wpsh.count', 'pt.name', 'pt1.name as parentName', 'wpsh.unit',
    			])
		    	->from('warehouse_product_operation wpo')
		    	->join('INNER JOIN', 'warehouse_product_status_history wpsh', 'wpo.id = wpsh.operation_id')
		    	->join('INNER JOIN', 'warehouse_product wp', 'wpsh.warehouse_product_id = wp.id')
		    	->join('INNER JOIN', 'product p', 'wp.product_id = p.id')
		    	->join('INNER JOIN', 'product_translation pt', 'pt.product_id = p.id')
		    	->join('INNER JOIN', 'language l', 'l.id = pt.language_id')
		    	->join('LEFT JOIN', 'product p1', 'p.parent_product_id = p1.id')
		    	->join('LEFT JOIN', 'product_translation pt1', 'p1.id = pt1.product_id')
		    	->join('LEFT JOIN', 'language l1', 'pt1.language_id = l1.id')
		    	->andWhere(['l.symbol' => \Yii::$app->language])
		    	->andWhere(['or', ['l1.symbol' => null], ['l1.symbol' => \Yii::$app->language]])
		    	->andWhere(['wpo.id' => intval($issueId)]);
    
    	if(!empty($params['name'])) {
    		$query->andWhere(['like', 'pt.name', $params['name']]);
    	}
    
    	$columnsMap = [
    			'name' => 'pt.name',
    			'count' => 'wpsh.count',
    			'unit' => 'wpsh.unit',
    	];
    
    	if(!empty($params['sortDir']) && !empty($columnsMap[$params['sortField']])) {
    		$query->orderBy([
    				$columnsMap[$params['sortField']] => (!empty($params['sortDir']) && $params['sortDir'] == 'desc') ? SORT_DESC : SORT_ASC
    		]);
    	}
    
    	return $query;
    }
}