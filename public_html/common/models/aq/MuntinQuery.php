<?php

namespace common\models\aq;

use Yii;
use yii\db\Query;

/**
 * This is the ActiveQuery class for [[\common\models\ar\Muntin]].
 *
 * @see \common\models\ar\Muntin
 */
class MuntinQuery extends \yii\db\ActiveQuery {

    /*public function active() {
        $this->andWhere('[[status]]=1');
        return $this;
    }*/

    /**
     * @inheritdoc
     * @return \common\models\ar\Muntin[]|array
     */
    public function all($db = null) {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return \common\models\ar\Muntin|array|null
     */
    public function one($db = null) {
        return parent::one($db);
    }
    
    public static function getNames(){
        $query = (new Query())->select(['m.id', 'mt.name'])
            ->from('muntin m')
            ->innerJoin('muntin_translation mt', 'm.id = mt.muntin_id')
            ->innerJoin('language l', 'mt.language_id = l.id')
            ->where(['l.symbol' => Yii::$app->language])
            ->orderBy(['mt.name' => SORT_ASC]);
        return $query->all();
    }
    
    public static function getByName($name) {
        return (new Query())->select(['m.id'])
                ->from('muntin m')
                ->innerJoin('muntin_translation mt', 'm.id = mt.muntin_id')
                ->innerJoin('language l', 'mt.language_id = l.id')
                ->where(['l.symbol' => Yii::$app->language])
                ->andWhere(['mt.name' => $name])
                ->one();
    }
}