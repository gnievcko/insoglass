<?php

namespace common\models\aq;

use yii\db\Query;

/**
 * This is the ActiveQuery class for [[\common\models\ar\OfferedProductRequirement]].
 *
 * @see \common\models\ar\OfferedProductRequirement
 */
class OfferedProductRequirementQuery extends \yii\db\ActiveQuery {

    /*public function active() {
        $this->andWhere('[[status]]=1');
        return $this;
    }*/

    /**
     * @inheritdoc
     * @return \common\models\ar\OfferedProductRequirement[]|array
     */
    public function all($db = null) {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return \common\models\ar\OfferedProductRequirement|array|null
     */
    public function one($db = null) {
        return parent::one($db);
    }
    
    public static function getIntermediateProductsByOrder($dateFrom, $dateTo) {
        $query = (new Query())->select(['o.title as orderTitle', 'o.number as orderNumber', 'oh.date_deadline as deadline',
            'opt.name as productName', 'op.symbol as productSymbol', '(oop.count - COALESCE(prodTask.total_count_made, 0)) as productRequiredCount', 
            'pt.name as intermediateProductName', '(opr.count * (oop.count - COALESCE(prodTask.total_count_made, 0))) as intermediateProductRequired',
            '(SELECT SUM(count_available) from warehouse_product wp where wp.product_id = p.id) as intermediateCountAvailable',
        ])
            ->from('order o')
            ->innerJoin('order_offered_product oop', 'oop.order_id = o.id')
            ->innerJoin('offered_product op', 'oop.offered_product_id = op.id')
            ->innerJoin('order_history oh', 'oh.id = o.order_history_last_id')
            ->innerJoin('order_status os', 'os.id = oh.order_status_id')
            ->leftJoin('offered_product_translation opt', 'opt.offered_product_id = op.id')
            ->leftJoin('production_task prodTask', 'prodTask.order_offered_product_id = oop.id')
            ->leftJoin('offered_product_requirement opr', 'opr.offered_product_id = op.id')
            ->leftJoin('product p', 'p.id = opr.product_required_id')
            ->leftJoin('language l', 'l.id = opt.language_id')
            ->leftJoin('product_translation pt', 'p.id = pt.product_id')
            ->leftJoin('language l2', 'l2.id = pt.language_id')
            ->where([
                'os.is_terminal' => 0,
                'oop.is_active' => 1,
            ])
            ->andWhere(['OR',
                'prodTask.total_count_made <= oop.count',
                'prodTask.total_count_made IS NULL'
            ])
            ->andWhere('prodTask.task_successor_ids IS NULL')
            ->andWhere('oh.date_deadline >= :dateFrom',  [':dateFrom' => $dateFrom])
            ->andWhere('oh.date_deadline <= :dateTo', [':dateTo' => $dateTo])
            ->andWhere(['OR',
                'l.symbol IS NULL',
                ['l.symbol' => \Yii::$app->language],
            ])
            ->andWhere(['OR',
                'l2.symbol IS NULL',
                ['l2.symbol' => \Yii::$app->language],
            ]);
            
        return $query->all();
    }
    
    public static function getIntermediateProductsForAllOrders($dateFrom, $dateTo) {
        $query = (new Query())->select(['pt.name as intermediateProductName',
            'SUM(opr.count * (oop.count - COALESCE(prodTask.total_count_made, 0))) as intermediateProductRequired',
            '(SELECT SUM(count_available) from warehouse_product wp where wp.product_id = p.id) as intermediateCountAvailable',
        ])
            ->from('order o')
            ->innerJoin('order_offered_product oop', 'oop.order_id = o.id')
            ->innerJoin('offered_product op', 'oop.offered_product_id = op.id')
            ->innerJoin('offered_product_requirement opr', 'opr.offered_product_id = op.id')
            ->innerJoin('product p', 'p.id = opr.product_required_id')
            ->innerJoin('order_history oh', 'oh.id = o.order_history_last_id')
            ->innerJoin('order_status os', 'os.id = oh.order_status_id')
            ->leftJoin('production_task prodTask', 'prodTask.order_offered_product_id = oop.id')
            ->leftJoin('product_translation pt', 'p.id = pt.product_id')
            ->leftJoin('language l', 'l.id = pt.language_id')
            ->where([
                'os.is_terminal' => 0,
                'oop.is_active' => 1,
            ])
            ->andWhere(['OR',
                'prodTask.total_count_made <= oop.count',
                'prodTask.total_count_made IS NULL'
            ])
            ->andWhere('prodTask.task_successor_ids IS NULL')
            ->andWhere('oh.date_deadline >= :dateFrom',  [':dateFrom' => $dateFrom])
            ->andWhere('oh.date_deadline <= :dateTo', [':dateTo' => $dateTo])
            ->andWhere(['OR',
                'l.symbol IS NULL',
                ['l.symbol' => \Yii::$app->language],
            ])
            ->groupBy(['pt.name']);
            
            
            return $query->all();
    }
}