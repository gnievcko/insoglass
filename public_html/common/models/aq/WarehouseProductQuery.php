<?php

namespace common\models\aq;

use Yii;
use yii\db\Query;
use common\models\aq\WarehouseQuery;
use common\helpers\Utility;

/**
 * This is the ActiveQuery class for [[\common\models\ar\WarehouseProduct]].
 *
 * @see \common\models\ar\WarehouseProduct
 */
class WarehouseProductQuery extends \yii\db\ActiveQuery {

    /*public function active() {
        $this->andWhere('[[status]]=1');
        return $this;
    }*/

    /**
     * @inheritdoc
     * @return \common\models\ar\WarehouseProduct[]|array
     */
    public function all($db = null) {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return \common\models\ar\WarehouseProduct|array|null
     */
    public function one($db = null) {
        return parent::one($db);
    }

    // TODO: Sprawdzic, czy nie idzie tego przyspieszyc
    public static function getActiveProductsQuery($params = []) {
    	$photoSubquery = (new Query())->select(['pp.url_photo'])->from('product_photo pp')
    			->where('pp.product_id = p.id')
    			->orderBy(['pp.priority' => SORT_ASC, 'pp.id' => SORT_ASC])->limit(1);

    	$categorySubquery = (new Query())->select(['GROUP_CONCAT(pct.name ORDER BY pct.name SEPARATOR ", ")'])
    			->from('product_category_set pcs')
    			->join('INNER JOIN', 'product_category pc', 'pcs.product_category_id = pc.id')
    			->join('INNER JOIN', 'product_category_translation pct', 'pc.id = pct.product_category_id')
    			->join('INNER JOIN', 'language l1', 'pct.language_id = l1.id')
    			->where(['l1.symbol' => Yii::$app->language])
    			->andWhere('pcs.product_id = p.id');

    	/*$aliasSubquery = (new Query())->select(['GROUP_CONCAT(pa.name order by pa.name SEPARATOR ", ")'])
    			->from('product_alias pa')
    			->join('INNER JOIN', 'language l1', 'pa.language_id = l1.id')
    			->where(['l1.symbol' => Yii::$app->language])
    			->andWhere('pa.product_id = p.id')
    			->orderBy(['pa.name' => SORT_ASC]);*/

    	$countSubquery = (new Query())->select(['SUM(wp1.count)'])
    			->from('warehouse_product wp1')
    			->join('INNER JOIN', 'product p1', 'wp1.product_id = p1.id')
    			->andWhere(['or', 'wp1.product_id = p.id', 'p1.parent_product_id = p.id']);
    	
    	$countAvailableSubquery = (new Query())->select(['SUM(wp1.count_available)'])
    			->from('warehouse_product wp1')
    			->join('INNER JOIN', 'product p1', 'wp1.product_id = p1.id')
    			->andWhere(['or', 'wp1.product_id = p.id', 'p1.parent_product_id = p.id']);

    	$versionCountSubquery = (new Query())->select(['COUNT(p1.id)'])
    			->from('product p1')
    			->where('p1.parent_product_id = p.id');

    	$query = (new Query())
    			->select([
    					'wp.id', 'p.index', 'pt.name', 'UNIX_TIMESTAMP(p.date_creation) as dateCreation', 'UNIX_TIMESTAMP(wp.date_modification) as dateModification',
    					'p.count_minimal as countMinimal', 'p.id as productId', 'pt.remarks', 'wp.stillage', 'wp.shelf',
    					'urlPhoto' => $photoSubquery, 'category' => $categorySubquery/*, 'alias' => $aliasSubquery*/,
    					'countSum' => $countSubquery, 'countAvailableSum' => $countAvailableSubquery, 'versionCount' => $versionCountSubquery,
    					'wp.price_unit as priceUnit', 'wp.price_group as priceGroup', 'c.short_symbol as currencyShortSymbol',
    					'ws.name as supplier', 'pt.unit_default as unit',
    			])
    			->from('warehouse_product wp')
    			->join('INNER JOIN', 'product p', 'wp.product_id = p.id')
    			->join('INNER JOIN', 'product_translation pt', 'p.id = pt.product_id')
    			->join('INNER JOIN', 'language l', 'pt.language_id = l.id')
    			->join('LEFT JOIN', 'currency c', 'wp.currency_id = c.id')
    			->join('LEFT JOIN', 'warehouse_supplier ws', 'p.warehouse_supplier_id = ws.id')
    			->where([
    					'l.symbol' => Yii::$app->language,
    					'p.parent_product_id' => null,
						'p.is_active' => 1
    			]);

    	if(!empty($params['productId'])) {
    		$query->andWhere(['p.id' => intval($params['productId'])]);
    	}

    	if(!empty($params['name'])) {
    		$subquery = (new Query())->select(['pa.id'])
    				->from('product_alias pa')
    				->join('INNER JOIN', 'language l1', 'pa.language_id = l1.id')
    				->where(['l1.symbol' => Yii::$app->language])
    				->andWhere('pa.product_id = p.id')
    				->andWhere(['like', 'pa.name', $params['name']]);

    		$query->andWhere(['or',
    				['like', 'pt.name', $params['name']],
    				['exists', $subquery],
    		]);
    	}

    	$categoryIds = array_filter(empty($params['categoryIds']) ? [] : $params['categoryIds'], function($id) {
    		return !empty($id);
    	});

    	if(!empty($params['categoryIds'])){
	    	$includeNoCategory = false;
	    	if(($params['categoryIds'][0] !== '')) {
	    		if(in_array(0, $params['categoryIds'])) {
	    			$includeNoCategory = true;
	    		}
	    	}

	    	$subquery = (new Query())->select(['wp.product_id'])
	    			->from('product_category_set pcs')
	    			->join('RIGHT JOIN', 'warehouse_product wp', 'wp.product_id = pcs.product_id')
	    			->where(['pcs.product_category_id' => $categoryIds]);

	    	if($includeNoCategory) {
	    		$subquery->orWhere(['pcs.product_category_id' => NULL]);
	    	}

	    	$query->andWhere(['p.id' => $subquery]);
	    	
	    	if(!empty($params['location'])){
	    		$query->andWhere(['or',
	    				['like', 'stillage', $params['location']],
	    				['like', 'shelf', $params['location']],
	    				['like', 'CONCAT(shelf, "/", stillage)', $params['location']]
	    		]);
	    	}
	    	
    	}
    	$warehouseIds = array_filter(empty($params['warehouseIds']) ? [] : $params['warehouseIds'], function($id) {
    		return !empty($id);
    	});

    	if(!empty($warehouseIds) && count($warehouseIds) < intval($params['allWarehousesCount'])) {
    		$query->andWhere(['wp.warehouse_id' => $warehouseIds]);
    		$countSubquery->andWhere(['wp1.warehouse_id' => $warehouseIds]);
    	}

    	$columnsMap = [
    			'name' => 'pt.name',
    			'count' => '('.$countAvailableSubquery->createCommand()->rawSql.')',
    			'dateCreation' => 'p.date_creation',
    			'stillageShelf' => 'CONCAT(wp.stillage, "/", wp.shelf)',
    			'dateModification' => 'wp.date_modification',
    			'category' => '('.$categorySubquery->createCommand()->rawSql.')',
    			//'alias' => '('.$aliasSubquery->createCommand()->rawSql.')',
    			'lastDeliveryCost' => 'COALESCE(wp.price_unit, wp.price_group)',
    			'supplier' => 'ws.name',
    	];

    	if(!empty($params['sortDir']) && !empty($columnsMap[$params['sortField']])) {
    		$query->orderBy([
    				$columnsMap[$params['sortField']] => (!empty($params['sortDir']) && $params['sortDir'] == 'desc') ? SORT_DESC : SORT_ASC
    		]);
    	}
    	
    	return $query;
    }
	public static function productCount($id) {
		return (new Query())->select(['SUM(wp1.count) as count'])
				->from('warehouse_product wp1')
				->join('INNER JOIN', 'product p1', 'wp1.product_id = p1.id')
				->andWhere(['or', ['wp1.product_id' => $id], ['p1.parent_product_id' => $id]]);
	}

    public static function getVersionsCountByProductId($warehouseIds, $productId) {
    	return (new Query())->select(['pt.name', 'p.index', 'wp.count_available', 'p.count_minimal as countMinimal'])
    			->from('warehouse_product wp')
    			->join('INNER JOIN', 'product p', 'wp.product_id = p.id')
    			->join('INNER JOIN', 'product_translation pt', 'p.id = pt.product_id')
    			->join('INNER JOIN', 'language l', 'pt.language_id = l.id')
    			->where(['wp.warehouse_id' => $warehouseIds])
    			->andWhere(['l.symbol' => Yii::$app->language])
    			->andWhere(['or', ['wp.product_id' => $productId], ['p.parent_product_id' => $productId]])
    			->all();
    }

    public static function getAllProductSubversionsInWarehouses($productId) {

    	$versionsSubquery = (new Query())->select(['pt.name', 'wp.product_id', 'wp.warehouse_id', 'wp.count'])
    			->from('warehouse_product wp')
    			->join('INNER JOIN', 'product p', 'p.id = wp.product_id')
    			->join('INNER JOIN', 'product_translation pt', 'pt.product_id = p.id')
    			->join('INNER JOIN', 'language l', 'pt.language_id = l.id')
    			->where(['or', ['wp.product_id' => $productId], ['p.parent_product_id' => $productId]])
    			->andWhere(['p.is_active' => 1])
    			->andWhere(['l.symbol' => Yii::$app->language]);

    	if (Yii::$app->params['isMoreWarehousesAvailable']) {
    		$warehouseSubquery = WarehouseQuery::getWarehouseList();
    		$warehouseCount = $warehouseSubquery->count();

    	}
    	else {
    		$warehouseSubquery = WarehouseQuery::getDefaultWarehouse();
    		$warehouseCount = 1;
    	}

    	$distinctVersions = (new Query())
    			->select(['name', 'product_id'])
    			->from('(' . $versionsSubquery->createCommand()->rawSql . ') x')
    			->distinct();

    	$versionCount = $distinctVersions->count(); //3
    	$versions = array_column($distinctVersions->all(), 'name');
    	$versionIds = array_column($distinctVersions->all(), 'product_id');
    	$warehouseIds = array_column($warehouseSubquery->all(), 'id');

    	$result[] = array_merge([null], array_column($warehouseSubquery->all(), 'name'));
    	if (Yii::$app->params['isMoreWarehousesAvailable']) {
    		$result[0][] = Yii::t('web', 'Total');
    	}

    	for($i = 0; $i < $versionCount; $i++) { //0..2
    		$tmpResultRow = (new Query())
    				->select(['SUM(count) as count', 'w.id as warehouseId'])
    				->from('warehouse w')
    				->join('left join', '(' . $versionsSubquery->createCommand()->rawSql . ') x', 'w.id = warehouse_id')
    				->where(['product_id' => $versionIds[$i]])
    				->groupBy('w.id')
    				->orderBy('w.id')
    				->all();

    		$pos = 0;
    		$resultRow[] = $versions[$i];

    		for($j = 0; $j < $warehouseCount; $j++) {
    			if(($pos < count($tmpResultRow))) {
    				if($tmpResultRow[$pos]['warehouseId'] == $warehouseIds[$j]) {
	    				$resultRow[] = $tmpResultRow[$pos]['count'];
	    				$pos++;
    				}
    				else {
    					$resultRow[] = 0;
    				}
    			}
    			else {
    				$resultRow[] = 0;
    			}
    		}

    		if (Yii::$app->params['isMoreWarehousesAvailable']) {
    			$resultRow[] = array_sum(array_column($tmpResultRow, 'count'));
    		}

    		$result[] = $resultRow;
    		$resultRow = null;
    		$pos = 0;
    	}
		return $result;
    }

    public static function getProductReservationsQuery($params = []) {
    	$productStatuses = [Utility::PRODUCT_STATUS_RESERVATION];

    	$query = (new Query())->select(['o.title','pt.name', 'wpsh.count', 'wpsh.date_creation', 'CONCAT(COALESCE(u.first_name, ""), "|", COALESCE(u.last_name, ""), "|", u.email) as user',
    				'wpsh.id', 'o.id as orderId', 'wp.product_id as productId', 'wpsh.user_confirming_id as userConfirmingId',
    			])
		    	->from('warehouse_product_status_history wpsh')
		    	->join('INNER JOIN', 'product_status ps', 'wpsh.product_status_id = ps.id')
		    	->join('INNER JOIN', 'warehouse_product_status_history_data wpshd', 'wpsh.id = wpshd.history_id')
		    	->join('INNER JOIN', 'product_status_reference psr', 'ps.id = psr.product_status_id')
		    	->join('INNER JOIN', 'order o', 'o.id = wpshd.referenced_object_id')
		    	->join('INNER JOIN', 'warehouse_product wp', 'wp.id = wpsh.warehouse_product_id')
		    	->join('INNER JOIN', 'product_translation pt', 'pt.product_id = wp.product_id')
		    	->join('INNER JOIN', 'language l', 'l.id = pt.language_id')
		    	->join('INNER JOIN', 'user u', 'u.id = wpsh.user_confirming_id')
		    	->where(['ps.symbol' => $productStatuses])
		    	->andWhere(['l.symbol' => Yii::$app->language])
		    	->andWhere(['psr.name_table' => 'order'])
    			->andWhere(['not exists', 
	    					(new Query())
	    						/*->from('warehouse_product_status_history wpsh1')
	    						->join('INNER JOIN', 'product_status ps1', 'wpsh1.product_status_id = ps1.id')
		    					->join('INNER JOIN', 'warehouse_product_status_history_data wpshd1', 'wpsh1.id = wpshd1.history_id')
		    					->join('INNER JOIN', 'product_status_reference psr1', 'ps1.id = psr1.product_status_id')
	    						->where('wpsh.warehouse_product_id = wpsh1.warehouse_product_id')
	    						->andWhere('wpsh.count = wpsh1.count')
	    						->andWhere(['ps1.symbol' => [Utility::PRODUCT_STATUS_UNDO_RESERVATION, Utility::PRODUCT_STATUS_ISSUING_RESERVATION]])
	    						->andWhere(['psr1.name_table' => 'order'])
	    						->andWhere('wpshd1.referenced_object_id = o.id')*/
	    						->from('warehouse_product_status_history wpsh1')
	    						->where('wpsh1.history_blocked_id = wpsh.id')
    			]);


    	if(!empty($params['orderName'])) {
    		$query->andWhere(['like', 'o.title', $params['orderName']]);
    	}

    	if(!empty($params['productName'])) {
    		$query->andWhere(['like', 'pt.name', $params['productName']]);
    	}

    	$columnsMap = [
    			'title' => 'o.title',
    			'name' => 'pt.name',
    			'count' => 'wpsh.count',
    			'date_creation' => 'wpsh.date_creation',
				'user' => 'CONCAT(COALESCE(u.first_name, ""), "|", COALESCE(u.last_name, ""), "|", u.email)',
    	];

    	if(!empty($params['sortDir']) && !empty($columnsMap[$params['sortField']])) {
    		$query->orderBy([
    				$columnsMap[$params['sortField']] => (!empty($params['sortDir']) && $params['sortDir'] == 'desc') ? SORT_DESC : SORT_ASC
    		]);
    	}

    	return $query;
    }

    public static function getReservedProductsByOrderId($orderId) {
    	return (new Query())->select(['p.id', 'wpsh.id as operationId', 'pt.name', 'parent.name as parentName', 'SUM(wpsh.count) as count', 'COALESCE(CONCAT(u.first_name, " ", u.last_name), u.email, "") as user'])
		    	->from('warehouse_product_status_history wpsh')
		    	->join('INNER JOIN', 'product_status ps', 'wpsh.product_status_id = ps.id')
		    	->join('INNER JOIN', 'warehouse_product_status_history_data wpshd', 'wpsh.id = wpshd.history_id')
		    	->join('INNER JOIN', 'product_status_reference psr', 'ps.id = psr.product_status_id')
		    	->join('INNER JOIN', 'order o', 'o.id = wpshd.referenced_object_id')
		    	->join('INNER JOIN', 'warehouse_product wp', 'wp.id = wpsh.warehouse_product_id')
		    	->join('INNER JOIN', 'product p', 'wp.product_id = p.id')
		    	->join('INNER JOIN', 'product_translation pt', 'pt.product_id = wp.product_id')
		    	->join('LEFT JOIN', 'product_translation parent', 'parent.product_id = p.parent_product_id AND parent.language_id = pt.language_id')
		    	->join('INNER JOIN', 'language l', 'l.id = pt.language_id')
		    	->join('INNER JOIN', 'user u', 'u.id = wpsh.user_confirming_id')
		    	->where(['ps.symbol' => Utility::PRODUCT_STATUS_RESERVATION])
		    	->andWhere(['l.symbol' => Yii::$app->language])
		    	->andWhere(['psr.name_table' => 'order'])
	    		->andWhere(['o.id' => intval($orderId)])
	    		->andWhere(['not exists', 
	    					(new Query())
	    							/*->from('warehouse_product_status_history wpsh1')
	    							->join('INNER JOIN', 'product_status ps1', 'wpsh1.product_status_id = ps1.id')
		    						->join('INNER JOIN', 'warehouse_product_status_history_data wpshd1', 'wpsh1.id = wpshd1.history_id')
		    						->join('INNER JOIN', 'product_status_reference psr1', 'ps1.id = psr1.product_status_id')
	    							->where('wpsh.warehouse_product_id = wpsh1.warehouse_product_id')
	    							->andWhere('wpsh.count = wpsh1.count')
	    							->andWhere(['ps1.symbol' => [Utility::PRODUCT_STATUS_UNDO_RESERVATION, Utility::PRODUCT_STATUS_ISSUING_RESERVATION]])
	    							->andWhere(['psr1.name_table' => 'order'])
	    							->andWhere(['wpshd1.referenced_object_id' => intval($orderId)])*/
	    							->from('warehouse_product_status_history wpsh1')
	    							->where('wpsh1.history_blocked_id = wpsh.id')
	    				])
				->groupBy(['p.id', 'wpsh.id', 'pt.name', 'parent.name', 'user']);
    }
    
    public static function getCountByProductId($productId) {
    	return (new Query())->select(['SUM(wp.count) as count', 'p.count_minimal as countMinimal'])
    			->from('product p')
    			->join('INNER JOIN', 'warehouse_product wp', 'p.id = wp.product_id')
    			->where(['p.id' => intval($productId)])
    			->one();
    }
    
    public static function getProducts($categoryIds = []) {
        $language = Yii::$app->language;
        
        // brzydki hack umozliwiajacy sortowanie w taki sposob, aby nulle byly na koncu
        // tradycyjne rozwiazanie (poprzez wartosc ujemna parametru) nie moze byc zastosowane
        $query = (new Query())
                ->select(['p.id', 'wp.count', 'ppt.name as parentName', 'pt.price_default', 'c.short_symbol', 
                		'pt.name', 'wp.stillage', 'wp.shelf', 'pt.unit_default'])
                ->from('warehouse_product wp')
                ->join('INNER JOIN', 'warehouse w', 'w.id = wp.warehouse_id')
                ->join('INNER JOIN', 'product p', 'p.id = wp.product_id')
                ->join('LEFT JOIN', 'product pp', 'p.parent_product_id = pp.id')
                ->join('LEFT JOIN', 'product_translation pt', 'p.id = pt.product_id')
                ->join('LEFT JOIN', 'product_translation ppt', 'ppt.product_id = pp.id')
                ->join('LEFT JOIN', 'currency c', 'c.id=pt.currency_id')
                ->join('LEFT JOIN', 'language l', 'l.id = pt.language_id')
                ->orderBy(['IFNULL(wp.stillage, "zzzzzzzzzz")' => SORT_ASC, 'IFNULL(wp.shelf, "zzzzzzzzzz")' => SORT_ASC])
                ->where(['w.is_active' => true])
                ->andWhere(['p.is_active' => true])
                ->andWhere(['>', 'wp.count', 0])
                ->andWhere(['or', ['=', 'l.symbol', $language], ['l.symbol' => null]])
        		->orderBy(['pt.name' => SORT_ASC]);
        
        if(!empty($categoryIds)) {
			$query->andWhere(['or', ['in', 'p.id', (new Query())->select(['pcs.product_id'])
					->from('product_category_set pcs')
					->where(['pcs.product_category_id' => $categoryIds])
			], ['not exists', (new Query())->select(['pcs.product_category_id'])
					->from('product_category_set pcs')
					->where('pcs.product_id = p.id')
			]]);
        }
        else {
        	$query->andWhere(['in', 'p.id', (new Query())->select(['pcs.product_id'])
        			->from('product_category_set pcs')
        			->where(['pcs.product_category_id' => $categoryIds])
        	]);
        }
                
        return $query->all();
    }
    
}

