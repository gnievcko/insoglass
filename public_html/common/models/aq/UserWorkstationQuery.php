<?php

namespace common\models\aq;

use yii\db\Query;

/**
 * This is the ActiveQuery class for [[\common\models\ar\UserWorkstation]].
 *
 * @see \common\models\ar\UserWorkstation
 */
class UserWorkstationQuery extends \yii\db\ActiveQuery {

    /*public function active() {
        $this->andWhere('[[status]]=1');
        return $this;
    }*/

    /**
     * @inheritdoc
     * @return \common\models\ar\UserWorkstation[]|array
     */
    public function all($db = null) {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return \common\models\ar\UserWorkstation|array|null
     */
    public function one($db = null) {
        return parent::one($db);
    }    
    
    public static function getWorkersByIds($workerIds) {
        return (new Query())
                ->select(['u.id', "concat_ws(' ', u.first_name, u.last_name) AS fullName"])
                ->from('user_workstation uw')
                ->join('INNER JOIN', 'user u', 'uw.user_id = u.id')
                ->where(['u.id' => $workerIds])
                ->orderBy(['u.id' => SORT_ASC])
                ->all();
    }
    
    public static function getWorkersByWorkstationId($id) {
        return (new Query())
                ->select(['uw.user_id as id', 'u.first_name as firstName','u.last_name as lastName', 'u.email', 'u.url_photo as urlPhoto'])
                ->from('user_workstation uw')
                ->join('INNER JOIN', 'user u', 'uw.user_id = u.id')
                ->where(['uw.workstation_id' => intval($id)])
                ->orderBy(['u.id' => SORT_ASC])
                ->all();
    }
}