<?php

namespace common\models\aq;

use common\models\ar\Entity;
/**
 * This is the ActiveQuery class for [[\common\models\ar\Entity]].
 *
 * @see \common\models\ar\Entity
 */
class EntityQuery extends \yii\db\ActiveQuery {

    /*public function active() {
        $this->andWhere('[[status]]=1');
        return $this;
    }*/

    /**
     * @inheritdoc
     * @return \common\models\ar\Entity[]|array
     */
    public function all($db = null) {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return \common\models\ar\Entity|array|null
     */
    public function one($db = null) {
        return parent::one($db);
    }
    
    public static function getDefaultEntity() {
    	return Entity::find()->where(['is_active' => 1])->orderBy(['id' => SORT_ASC])->limit(1)->one();
    }
}