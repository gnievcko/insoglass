<?php

namespace common\models\aq;

use Yii;
use yii\db\Query;

/**
 * This is the ActiveQuery class for [[\common\models\ar\OfferedProductAttribute]].
 *
 * @see \common\models\ar\OfferedProductAttribute
 */
class OfferedProductAttributeQuery extends \yii\db\ActiveQuery {

    /*public function active() {
        $this->andWhere('[[status]]=1');
        return $this;
    }*/

    /**
     * @inheritdoc
     * @return \common\models\ar\OfferedProductAttribute[]|array
     */
    public function all($db = null) {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return \common\models\ar\OfferedProductAttribute|array|null
     */
    public function one($db = null) {
        return parent::one($db);
    }
    
    public static function getByOfferedProductId($offeredProductId) {
        return (new Query())->select(['pat.id', 'pat.product_attribute_group_id as groupId',
                    'pat.variable_type as variableType', 'pat.is_visible as isVisible', 'patt.name',
                    'opa.value','patt.hint',
                ])
                ->from('offered_product_attribute opa')
                ->innerJoin('product_attribute_type pat', 'opa.product_attribute_type_id = pat.id')
                ->innerJoin('product_attribute_type_translation patt', 'pat.id = patt.product_attribute_type_id')
                ->innerJoin('language l', 'patt.language_id = l.id')
                ->where(['l.symbol' => Yii::$app->language])
                ->andWhere(['opa.offered_product_id' => intval($offeredProductId)])
                ->andWhere(['pat.is_visible' => true])
                ->orderBy(['pat.product_attribute_group_id' => SORT_ASC, 'pat.id' => SORT_ASC])
                ->all();
    
    }
}