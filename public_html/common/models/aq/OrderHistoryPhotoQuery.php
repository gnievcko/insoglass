<?php

namespace common\models\aq;

use Yii;

/**
 * This is the ActiveQuery class for [[\common\models\ar\OrderHistoryPhoto]].
 *
 * @see \common\models\ar\OrderHistoryPhoto
 */
class OrderHistoryPhotoQuery extends \yii\db\ActiveQuery {

    /*public function active() {
        $this->andWhere('[[status]]=1');
        return $this;
    }*/

    /**
     * @inheritdoc
     * @return \common\models\ar\OrderHistoryPhoto[]|array
     */
    public function all($db = null) {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return \common\models\ar\OrderHistoryPhoto|array|null
     */
    public function one($db = null) {
        return parent::one($db);
    }
}
