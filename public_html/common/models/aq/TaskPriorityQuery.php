<?php

namespace common\models\aq;

use Yii;
use common\models\ar\TaskPriority;
use yii\db\Query;

/**
 * This is the ActiveQuery class for [[\common\models\ar\TaskPriority]].
 *
 * @see \common\models\ar\TaskPriority
 */
class TaskPriorityQuery extends \yii\db\ActiveQuery {

    /*public function active() {
        $this->andWhere('[[status]]=1');
        return $this;
    }*/

    /**
     * @inheritdoc
     * @return \common\models\ar\TaskPriority[]|array
     */
    public function all($db = null) {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return \common\models\ar\TaskPriority|array|null
     */
    public function one($db = null) {
        return parent::one($db);
    }
    
    public static function getAllOrdered() {
    	return TaskPriority::find()->alias('tp')->select(['tp.id', 'tpt.name as symbol'])
				->joinWith(['taskPriorityTranslations tpt' => function($q) {
					$q->joinWith('language l');
					$q->where(['l.symbol' => Yii::$app->language]);
				}])
				->orderBy('tp.priority ASC')
				->all();
    }
    
    public static function getBySymbol($symbol) {
        $query = (new Query())->select(['tp.id', 'tp.symbol'])
                ->from('task_priority tp')
                ->where(['tp.symbol' => $symbol]);
        
        return is_array($symbol) ? $query->all() : $query->one();                
    }
}