<?php


namespace common\models\aq;

/**
 * This is the ActiveQuery class for [[\common\models\ar\UserDepartment]].
 *
 * @see \common\models\ar\UserDepartment
 */
class UserDepartmentQuery extends \yii\db\ActiveQuery {

    /*public function active() {
        $this->andWhere('[[status]]=1');
        return $this;
    }*/

    /**
     * @inheritdoc
     * @return \common\models\ar\UserDepartment[]|array
     */
    public function all($db = null) {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return \common\models\ar\UserDepartment|array|null
     */
    public function one($db = null) {
        return parent::one($db);
    }

    public function getUsers($id) {
        $query = (new \yii\db\Query())
            ->select(['ud.user_id as id','u.first_name as first_name','u.last_name as last_name','ud.is_lead as is_lead', 'u.url_photo as urlPhoto', 'u.email as email',  'u.phone1 as phone'])
            ->from('user_department ud')
            ->join('INNER JOIN', 'department d', 'ud.department_id = d.id')
            ->join('INNER JOIN', 'user u', 'ud.user_id = u.id')
            ->andWhere(['d.id'=> intval($id)])
            ->orderBy(['ud.is_lead'=> SORT_DESC])
            ->all();
        return $query;
    }

    public function getUsersCount($id){
        $query = (new \yii\db\Query())
            ->select('COUNT(*)')
            ->from('user_department ud')
            ->join('INNER JOIN', 'department d', 'ud.department_id = d.id')
            ->join('INNER JOIN', 'user u', 'ud.user_id = u.id')
            ->andWhere(['d.id'=> intval($id)])
            ->all();
        return $query;
    }

    public function getManager($id){
        $query = (new \yii\db\Query())
            ->select('u.id as id')
            ->from('user_department ud')
            ->join('INNER JOIN', 'department d', 'ud.department_id = d.id')
            ->join('INNER JOIN', 'user u', 'ud.user_id = u.id')
            ->where(['d.id'=> intval($id)])
            ->andWhere(['ud.is_lead'=>1])
            ->one();
        return $query;
    }
    public function getWorkersByIds($workerIds, $isLead){
        $query = (new \yii\db\Query())
            ->select(['u.id as id','u.first_name as firstName','u.last_name as lastName', 'u.url_photo as urlPhoto'])
            ->from('user_department ud')
            ->join('INNER JOIN', 'department d', 'ud.department_id = d.id')
            ->join('INNER JOIN', 'user u', 'ud.user_id = u.id')
            ->where(['in','u.id',empty($workerIds) ? [] : $workerIds])
            //->andWhere(['ud.is_lead'=> $isLead ? 1 : 0 ])
            ->all();
        return $query;
    }
}