<?php

namespace common\models\aq;

use Yii;
use yii\db\Query;
/**
 * This is the ActiveQuery class for [[\common\models\ar\ProductAttributeType]].
 *
 * @see \common\models\ar\ProductAttributeType
 */
class ProductAttributeTypeQuery extends \yii\db\ActiveQuery {

    /*public function active() {
        $this->andWhere('[[status]]=1');
        return $this;
    }*/

    /**
     * @inheritdoc
     * @return \common\models\ar\ProductAttributeType[]|array
     */
    public function all($db = null) {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return \common\models\ar\ProductAttributeType|array|null
     */
    public function one($db = null) {
        return parent::one($db);
    }
    
    public static function getByProductionPathStepIds($stepIds = []) {
        return (new Query())->select(['ppsa.production_path_step_id as stepId', 'ppsa.product_attribute_type_id as attributeId',
                    
                ])
                ->from('production_path_step_attribute ppsa')
                ->where(['ppsa.production_path_step_id' => $stepIds])
                ->orderBy(['ppsa.production_path_step_id' => SORT_ASC, 'ppsa.product_attribute_type_id' => SORT_ASC])
                ->all();
    }
    
    public static function getAll($onlyVisibleForClient = false) {
        $query = (new Query())->select(['pat.id', 'pat.product_attribute_group_id as groupId', 
                    'pat.variable_type as variableType', 'pat.attribute_checked_ids as attributeCheckedIds', 
                    'pat.attribute_blocked_ids as attributeBlockedIds', 'pat.is_visible as isVisible', 'patt.name', 'patt.hint'])
                ->from('product_attribute_type pat')
                ->innerJoin('product_attribute_type_translation patt', 'pat.id = patt.product_attribute_type_id')
                ->innerJoin('language l', 'patt.language_id = l.id')
                ->where(['l.symbol' => Yii::$app->language])
                ->andWhere(['pat.is_visible' => true])
                ->orderBy(['pat.product_attribute_group_id' => SORT_ASC, 'pat.id' => SORT_ASC]);
        
        if($onlyVisibleForClient) {
            $query->andWhere(['pat.is_visible_for_client' => true]);
        }

        return $query->all();        
    }
    
    public static function getBySymbol($symbol) {
        return (new Query())->select(['pat.id'])
                ->from('product_attribute_type pat')
                ->where(['pat.symbol' => $symbol])
                ->one();
    }
}