<?php

namespace common\models\aq;

use Yii;
use common\helpers\Utility;

/**
 * This is the ActiveQuery class for [[\common\models\ar\CompanyGroup]].
 *
 * @see \common\models\ar\CompanyGroup
 */
class CompanyGroupQuery extends \yii\db\ActiveQuery {

    /*public function active() {
        $this->andWhere('[[status]]=1');
        return $this;
    }*/

    /**
     * @inheritdoc
     * @return \common\models\ar\CompanyGroup[]|array
     */
    public function all($db = null) {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return \common\models\ar\CompanyGroup|array|null
     */
    public function one($db = null) {
        return parent::one($db);
    }
    
    public static function findCompanyGroups($params = []) {
    	$query = (new \yii\db\Query())
		    	->select(['cg.id', 'cgt.name'])
		    	->from('company_group cg')
		    	->join('INNER JOIN', 'company_group_translation cgt' , 'cg.id = cgt.company_group_id')
		    	->join('INNER JOIN', 'language l', 'cgt.language_id = l.id')
		    	->andWhere(['l.symbol' => Yii::$app->language])
    			->andWhere('cg.is_predefined = 0');

    	if(!empty($params['userId']) && !Yii::$app->user->can(Utility::ROLE_MAIN_SALESMAN)) {
    		$companiesSubquery = (new \yii\db\Query())
    				->select(['company_group_id'])
    				->from('user_company_salesman')
    				->where(['user_id' => Yii::$app->user->id]);
    		
    		$query->andWhere(['cg.id' => $companiesSubquery]);
    	}
    	
    	if(!empty($params['name'])) {
    		$query->andWhere(['like', 'cgt.name', $params['name']]);
    	}

    	$columnsMap = [
    			'name' => 'cgt.name',
    	];

    	if(!empty($params['sortDir']) && !empty($columnsMap[$params['sortField']])) {
            $query->orderBy([
                $columnsMap[$params['sortField']] => (!empty($params['sortDir']) && $params['sortDir'] == 'desc') ? SORT_DESC : SORT_ASC
            ]);
        }
    
    	return $query;
    }

    public static function getDetails($id) {
        return (new \yii\db\Query())
		    	->select(['cg.id', 'cgt.name'])
		    	->from('company_group cg')
		    	->join('INNER JOIN', 'company_group_translation cgt' , 'cg.id = cgt.company_group_id')
		    	->join('INNER JOIN', 'language l', 'cgt.language_id = l.id')
                ->andWhere(['cg.id' => $id])
		    	->andWhere(['l.symbol' => Yii::$app->language])
                ->andWhere('cg.is_predefined = 0')
                ->one();
    }
}
