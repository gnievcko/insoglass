<?php

namespace common\models\aq;

/**
 * This is the ActiveQuery class for [[\common\models\ar\ProductCategorySet]].
 *
 * @see \common\models\ar\ProductCategorySet
 */
class ProductCategorySetQuery extends \yii\db\ActiveQuery {

    /*public function active() {
        $this->andWhere('[[status]]=1');
        return $this;
    }*/

    /**
     * @inheritdoc
     * @return \common\models\ar\ProductCategorySet[]|array
     */
    public function all($db = null) {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return \common\models\ar\ProductCategorySet|array|null
     */
    public function one($db = null) {
        return parent::one($db);
    }
}