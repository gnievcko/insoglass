<?php

namespace common\models\aq;

use Yii;
use yii\db\Query;
use common\models\ar\OfferedProductCategory;

/**
 * This is the ActiveQuery class for [[\common\models\ar\OfferedProductCategory]].
 *
 * @see \common\models\ar\OfferedProductCategory
 */
class OfferedProductCategoryQuery extends \yii\db\ActiveQuery {

    /*public function active() {
        $this->andWhere('[[status]]=1');
        return $this;
    }*/

    /**
     * @inheritdoc
     * @return \common\models\ar\OfferedProductCategory[]|array
     */
    public function all($db = null) {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return \common\models\ar\OfferedProductCategory|array|null
     */
    public function one($db = null) {
        return parent::one($db);
    }

    public static function getCategoryList() {
    	return Yii::$app->db->createCommand('
    				(
    					SELECT CONCAT("o", opc.id) as id, opct.name as name
    					FROM offered_product_category opc
    					JOIN offered_product_category_translation opct ON opc.id = opct.offered_product_category_id
    					JOIN language l ON opct.language_id = l.id
    					WHERE l.symbol = :languageSymbol
    				)
    				UNION
    				(
    					SELECT CONCAT("p", pc.id) as id, pct.name as name
    					FROM product_category pc
    					JOIN product_category_translation pct ON pc.id = pct.product_category_id
    					JOIN language l ON pct.language_id = l.id
    					WHERE l.symbol = :languageSymbol
    				)
    				ORDER BY name ASC
    			')
    			->bindValues([
    					':languageSymbol' => Yii::$app->language,
    			])
    			->queryAll();
    }

    public static function getActiveOfferedProductCategoryQuery($params = []) {
        $subquery = (new Query())
                ->select(['offered_product_category_translation.name'])
                ->from('offered_product_category_translation')
                ->where('offered_product_category_translation.offered_product_category_id = opc.parent_category_id');

    	$query = (new Query())
    		->select(['opc.id as id', 'opc.symbol as symbol', 'parentCategoryName' => $subquery, 'opct.name as name', 'opct.description', 'opc.effort_factor'])
    		->from('offered_product_category opc')
    		->join('INNER JOIN', 'offered_product_category_translation opct', 'opc.id = opct.offered_product_category_id')
    		->join('INNER JOIN', 'language l', 'opct.language_id = l.id')
    		->where(['l.symbol' => \Yii::$app->language]);

    	if (!empty($params['offeredProductCategorySymbol'])) {
    		$query->andWhere(['like', 'opct.name', $params['offeredProductCategorySymbol']]);
    	}

    	$columnsMap = [
    			'id' => 'opc.id',
    			'symbol' => 'opc.symbol',
    			'name' => 'opct.name',
    			'description' => 'opct.description',
                'effort_factor' => 'opc.effort_factor',
                'parentCategoryName' => '(' . $subquery->createCommand()->rawSql . ')',
    	];

    	if(!empty($params['sortDir']) && !empty($columnsMap[$params['sortField']])) {
    		$query->orderBy([
    				$columnsMap[$params['sortField']] => (!empty($params['sortDir']) && $params['sortDir'] == 'desc') ? SORT_DESC : SORT_ASC
    		]);
    	}
    	else {
    		$query->orderBy(['name' => SORT_ASC]);
    	}

    	return $query;
    }

    public static function getDetails($id) {
        $subquery = (new Query())
                ->select(['offered_product_category_translation.name'])
                ->from('offered_product_category_translation')
                ->where('offered_product_category_translation.offered_product_category_id = opc.parent_category_id');

        return (new Query())
                ->select(['opc.id as id', 'parentCategoryName' => $subquery, 'opct.name as name', 'opct.description', 'opc.effort_factor as effortFactor'])
                ->from('offered_product_category opc')
                ->join('INNER JOIN', 'offered_product_category_translation opct', 'opc.id = opct.offered_product_category_id')
                ->join('INNER JOIN', 'language l', 'opct.language_id = l.id')
                ->where(['l.symbol' => \Yii::$app->language])
                ->andWhere(['opc.id' => $id])->one();
    }

    public static function getByNameAndParent($name, $parentCategoryId) {
        $query = (new Query())->select(['opc.id'])
                ->from('offered_product_category opc')
                ->innerJoin('offered_product_category_translation opct', 'opc.id = opct.offered_product_category_id')
                ->innerJoin('language l', 'opct.language_id = l.id')
                ->where(['LOWER(opct.name)' => mb_strtolower($name, 'UTF-8')])
                ->andWhere(['l.symbol' => Yii::$app->language]);
                
        if(!empty($parentCategoryId)) {
            $query->andWhere(['opc.parent_category_id' => intval($parentCategoryId)]);
        }
        
        return $query->one();
    }
}
