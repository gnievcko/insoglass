<?php

namespace common\models\aq;

use Yii;

/**
 * This is the ActiveQuery class for [[\common\models\ar\OrderAttachment]].
 *
 * @see \common\models\ar\OrderAttachment
 */
class OrderAttachmentQuery extends \yii\db\ActiveQuery {

    /*public function active() {
        $this->andWhere('[[status]]=1');
        return $this;
    }*/

    /**
     * @inheritdoc
     * @return \common\models\ar\OrderAttachment[]|array
     */
    public function all($db = null) {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return \common\models\ar\OrderAttachment|array|null
     */
    public function one($db = null) {
        return parent::one($db);
    }
}
