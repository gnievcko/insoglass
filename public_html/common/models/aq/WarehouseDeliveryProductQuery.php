<?php

namespace common\models\aq;

use Yii;
use yii\db\Query;

/**
 * This is the ActiveQuery class for [[\common\models\ar\WarehouseDeliveryProduct]].
 *
 * @see \common\models\ar\WarehouseDeliveryProduct
 */
class WarehouseDeliveryProductQuery extends \yii\db\ActiveQuery {

    /*public function active() {
        $this->andWhere('[[status]]=1');
        return $this;
    }*/

    /**
     * @inheritdoc
     * @return \common\models\ar\WarehouseDeliveryProduct[]|array
     */
    public function all($db = null) {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return \common\models\ar\WarehouseDeliveryProduct|array|null
     */
    public function one($db = null) {
        return parent::one($db);
    }
    
    public function getProductsByDeliveryId($deliveryId) {
    	return (new Query())->select(['p.index', 'pt.name', 'wdp.count', 'wdp.price_unit as priceUnit', 'wdp.note',
    				'wdp.price_group as priceGroup', 'c.short_symbol as shortSymbol', 'pt1.name as parentName',
    				'wdp.unit',
    			])
    			->from('warehouse_delivery_product wdp')
    			->join('INNER JOIN', 'product p', 'wdp.product_id = p.id')
    			->join('INNER JOIN', 'product_translation pt', 'p.id = pt.product_id')
    			->join('INNER JOIN', 'language l', 'pt.language_id = l.id')
    			->join('LEFT JOIN', 'currency c', 'wdp.currency_id = c.id')
    			->join('LEFT JOIN', 'product p1', 'p.parent_product_id = p1.id')
    			->join('LEFT JOIN', 'product_translation pt1', 'p1.id = pt1.product_id')
    			->join('LEFT JOIN', 'language l1', 'pt1.language_id = l1.id')
    			->where(['wdp.warehouse_delivery_id' => intval($deliveryId)])
    			->andWhere(['l.symbol' => Yii::$app->language])
    			->andWhere(['or', ['l1.symbol' => null], ['l1.symbol' => Yii::$app->language]])
    			->orderBy('pt.name')
    			->all();
    }
}