<?php

namespace common\models\aq;

use Yii;
use yii\db\Query;
use common\helpers\Utility;
/**
 * This is the ActiveQuery class for [[\common\models\ar\Company]].
 *
 * @see \common\models\ar\Company
 */
class CompanyQuery extends \yii\db\ActiveQuery {

    /*public function active() {
        $this->andWhere('[[status]]=1');
        return $this;
    }*/

    /**
     * @inheritdoc
     * @return \common\models\ar\Company[]|array
     */
    public function all($db = null) {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return \common\models\ar\Company|array|null
     */
    public function one($db = null) {
        return parent::one($db);
    }

    public static function getActiveCompaniesQuery($params = []) {
    	$query = (new Query())
    			->distinct()
		    	->select([
		    			'c.id', 'c.name', 'c.parent_company_id', 'c.email', 'c.phone1 as phone', 'UNIX_TIMESTAMP(c.date_creation) as dateCreation',
		    			'a.main as addressMain', 'a.complement as addressComplement', 'ct.name as cityName',
		    			'ct.zip_code as cityZipCode', 'p.name as provinceName', 
                        'u.first_name as creatorFirstName', 'u.last_name as creatorLastName'
		    	])
		    	->from('company c')
		    	->join('LEFT OUTER JOIN', 'company_group_set cgs', 'c.id = cgs.company_id')
		    	->join('LEFT OUTER JOIN', 'company_group cg', 'cgs.company_group_id = cg.id')
		    	->join('LEFT OUTER JOIN', 'company_group_translation cgt', 'cg.id = cgt.company_group_id')
            	->join('LEFT OUTER JOIN', 'language l', 'cgt.language_id = l.id')
		    	->join('LEFT OUTER JOIN', 'address a', 'c.address_id = a.id')
		    	->join('LEFT OUTER JOIN', 'city ct', 'a.city_id = ct.id')
		    	->join('LEFT OUTER JOIN', 'province p', 'ct.province_id = p.id')
                ->leftJoin('user u', 'u.id = c.user_id')
		    	->where(['c.is_active' => true, 'l.symbol' => \Yii::$app->language])
    			->andWhere(['or',
    					['cg.is_predefined' => false],
    					['cg.symbol' => Utility::COMPANY_GROUP_DEFAULT],
    			]);

		if(!\Yii::$app->user->can(Utility::ROLE_MAIN_SALESMAN)) {
    		$query->andWhere(['c.id' => (new Query())->select('cgs.company_id')
					->from('company_group_set cgs')
    				->join('INNER JOIN', 'user_company_salesman ucs', 'ucs.company_group_id = cgs.company_group_id')
    				->andWhere(['ucs.user_id' => Yii::$app->user->id])
    		]);
    	}

    	if(!empty($params['companyName'])) {
			$query->andWhere(['like', 'c.name', $params['companyName']]);
    	}
    	
    	if(!empty($params['companyType'])) {
    		if($params['companyType'] == Utility::PARENT_COMPANY) {
    			$query->andWhere('c.parent_company_id IS NULL');
    		}
    		else if($params['companyType'] == Utility::CHILD_COMPANY) {
    			$query->andWhere('c.parent_company_id IS NOT NULL');
    		}
    	}
    	
    	if(!empty($params['cityName'])) {
    		$query->andWhere(['or', 
    				['like', 'LOWER(ct.name)', mb_strtolower($params['cityName'], 'UTF-8')],
					['like', 'LOWER(ct.zip_code)', mb_strtolower($params['cityName'], 'UTF-8')],
			]);
    	}

    	$companyGroupIds = array_filter(empty($params['companyGroupIds']) ? [] : $params['companyGroupIds'], function($groupId) {
    		return !empty($groupId) ;
    	});
       
    	if(!empty($companyGroupIds)) {
    		$query->andWhere(['cgt.company_group_id' => $companyGroupIds]);
    	}

    	$columnsMap = [
    			'name' => 'c.name',
    			'email' => 'c.email',
    			'phone' => 'c.phone1',
    			'address' => 'a.main',
    			//'dateCreation' => 'c.date_creation',
				'dateCreation' => 'dateCreation',
                'addedBy' => 'CONCAT(u.first_name, u.last_name)',
    	];

    	if(!empty($params['sortDir']) && !empty($columnsMap[$params['sortField']])) {
            $query->orderBy([
                $columnsMap[$params['sortField']] => (!empty($params['sortDir']) && $params['sortDir'] == 'desc') ? SORT_DESC : SORT_ASC
            ]);
        }

		return $query;
    }

	public static function getDetails($id) {
    	return Yii::$app->db->createCommand('
    				SELECT c.id, c.name, c.email, c.phone1, c.phone2, c.date_creation as dateCreation, c.parent_company_id as parentCompanyId, c.note,
    					(
    						SELECT pc.name
    						FROM company pc
    						WHERE pc.id = c.parent_company_id AND pc.is_active = true
    					) as parentCompanyName,
    					c.fax1, c.vat_identification_number as vatIdentificationNumber,
    					c.taxpayer_identification_number as taxpayerIdentificationNumber, c.krs_number as krsNumber,
    					a1.main as address1Main, a1.complement as address1Complement, ct1.name as city1Name, ct1.zip_code as city1ZipCode, p1.name as province1Name, crt1.name as country1Name,
    					a2.main as address2Main, a2.complement as address2Complement, ct2.name as city2Name, ct2.zip_code as city2ZipCode, p2.name as province2Name, crt2.name as country2Name
    					
    				FROM company c
    				LEFT JOIN address a1 ON c.address_id = a1.id
    				LEFT JOIN city ct1 ON a1.city_id = ct1.id
    				LEFT JOIN province p1 ON ct1.province_id = p1.id
    				LEFT JOIN country cr1 ON p1.country_id = cr1.id
    				LEFT JOIN country_translation crt1 ON cr1.id = crt1.country_id
    				LEFT JOIN language l1 ON crt1.language_id = l1.id
    				LEFT JOIN address a2 ON c.address_postal_id = a2.id
    				LEFT JOIN city ct2 ON a2.city_id = ct2.id
    				LEFT JOIN province p2 ON ct2.province_id = p2.id
    				LEFT JOIN country cr2 ON p2.country_id = cr2.id
    				LEFT JOIN country_translation crt2 ON cr2.id = crt2.country_id
    				LEFT JOIN language l2 ON crt2.language_id = l2.id
    				
    				WHERE c.id = :id
    				AND (l1.symbol IS NULL OR l1.symbol = :languageSymbol)
    				AND (l2.symbol IS NULL OR l2.symbol = :languageSymbol)
    			')
    			->bindValues([
    					':id' => intval($id),
    					':languageSymbol' => Yii::$app->language,
    			])
    			->queryOne();
    }

    public static function findCompanies($params = []) {
        $query = (new \yii\db\Query())
	            ->select(["CONCAT('[',c.id, '] ', c.name) AS name", 'c.id', 'c.parent_company_id', 
	            		'a.main as addressMain', 'ct.name as city', 'ct.zip_code as zipCode'])
	            ->from('company c')
	            ->join('LEFT JOIN', 'address a', 'c.address_id = a.id')
	            ->join('LEFT JOIN', 'city ct', 'a.city_id = ct.id')
	            ->andWhere('c.is_active = 1');
//			    ->andWhere('c.is_artificial = 0');

		if(!Yii::$app->user->can(Utility::ROLE_MAIN_SALESMAN)) {
			$query->andWhere(['c.id' => (new Query())->select('cgs.company_id')
					->from('company_group_set cgs')
			    	->join('INNER JOIN', 'user_company_salesman ucs', 'ucs.company_group_id = cgs.company_group_id')
			    	->andWhere(['ucs.user_id' => Yii::$app->user->id])
			]);
		}

        if(!empty($params['name'])) {
			$query->andWhere(['like', 'c.name', $params['name']]);
        }
        
        if(!empty($params['onlyParentCompanies'])) {
        	$query->andWhere(['c.parent_company_id' => null]);
        }
        elseif(!empty($params['parentCompanyId'])) {
        	$query->andWhere(['c.parent_company_id' => intval($params['parentCompanyId'])]);
        }

        if(!empty($params['excludedCompanyId'])) {
        	$query->andWhere('((0 = :excludedCompanyId OR c.id <> :excludedCompanyId) AND c.parent_company_id IS NULL)',
        			[':excludedCompanyId' => $params['excludedCompanyId']]);
        }

        return $query;
    }

    public static function findEmployees($companyId, $params = []) {
        $query = (new \yii\db\Query())
            ->select(['u.id', 'u.first_name', 'u.last_name', 'u.email'])
            ->from('user u')
            ->andWhere('u.is_active')
            ->andWhere(['=', 'u.company_id', $companyId]);

        if(!empty($params['name'])) {
            $query->andWhere(['like', 'CONCAT(u.first_name, u.last_name)', $params['name']]);
        }

        return $query;
    }
    
    public static function getChildCompanies($parentId) {
    	return (new \yii\db\Query())
    			->select(['c.id', 'c.name'])
    			->from('company c')
    			->where(['c.parent_company_id' => $parentId])
    			->andWhere(['c.is_active' => true])
    			->all();
    }
    
    public static function getOrderCountByEmployee($dateFrom, $dateTo) {
    	return (new \yii\db\Query())
    			->select(['u.id', 'u.first_name', 'u.last_name','COUNT(u.id)'])
    			->from('order o')
    			->join('INNER JOIN', 'user u', 'o.user_id = u.id')
				->where('o.date_creation >= :dateFrom',  [':dateFrom' => $dateFrom])
    			->andWhere('o.date_creation <= :dateTo', [':dateTo' => $dateTo])
    			->orderBy('o.id')
    			->groupBy(['u.id', 'u.first_name', 'u.last_name'])
    			->all();  	
    }    
    
    public static function getSalesResultReportData($dateFrom, $dateTo, $orderIds) {
    	$orderCountSubquery = (new \yii\db\Query())
    			->select(['COUNT(*)'])
    			->from('order o')
    			->where('o.user_id = u.id')
    			->andWhere('o.date_creation >= :dateFrom',  [':dateFrom' => $dateFrom])
    			->andWhere('o.date_creation <= :dateTo', [':dateTo' => $dateTo])
    			->andWhere('o.is_deleted = 0');
    	
    	$companyCountSubquery = (new \yii\db\Query())
		    	->select(['COUNT(*)'])
		    	->from('company c')
		    	->where('c.user_id = u.id')
		    	->andWhere('c.date_creation >= :dateFrom',  [':dateFrom' => $dateFrom])
		    	->andWhere('c.date_creation <= :dateTo', [':dateTo' => $dateTo]);
    	
    	$invoicedOrderCountSubquery = (new \yii\db\Query())
		    	->select(['COUNT(*)'])
		    	->from('order o')
		    	->where(['o.id' => $orderIds])
		    	->andWhere('o.user_id = u.id')
		    	->andWhere('o.is_deleted = 0');

    	$roles = [Utility::ROLE_ADMIN, Utility::ROLE_MAIN_SALESMAN, Utility::ROLE_SALESMAN];
    	
    	$query = (new \yii\db\Query())
    			->select(['u.id', 'u.first_name', 'u.last_name',
    				'orderCount' => $orderCountSubquery,
    				'newClientsCount' => $companyCountSubquery,
    				'invoicedOrdersCount' => $invoicedOrderCountSubquery])
    			->from('user u')
    			->join('INNER JOIN', 'user_role ur', 'u.id = ur.user_id')
    			->join('INNER JOIN', 'role r', 'r.id = ur.role_id')
    			->where(['r.symbol' => $roles ])
    			->orderBy('u.last_name')
    			->distinct();
    			
    	return $query->all();	
    }
    
    
    //TODO - połączyc te 3?
    public static function getNewClientCountByEmployee($dateFrom, $dateTo) {
/*
    	SELECT u.id, u.first_name, u.last_name,
    	(SELECT COUNT(*) FROM `order` o WHERE o.user_id = u.id AND o.date_creation BETWEEN '2015-01-01 00:00:00' AND '2015-12-31 23:59:59') as orderCount,
    	(SELECT COUNT(*) FROM company c WHERE c.user_id AND c.date_creation BETWEEN '2015-01-01 00:00:00' AND '2015-12-31 23:59:59') as companyCount
    	FROM user u
    	JOIN user_role ur ON u.id = ur.user_id
    	JOIN role r ON ur.role_id = r.id
    	WHERE r.symbol IN ('SALESMAN', 'MAIN_SALESMAN', 'ADMIN');
    	*/
    	
    }
    
    public static function getInvoicedOrderCountByEmployee($orderIds) {
		$query = (new \yii\db\Query())
    			->select(['u.id', 'u.first_name', 'u.last_name', 'COUNT(u.id)'])
    			->from('order o')
    			->join('INNER JOIN', 'user u', 'u.id = o.user_id')
    			->where(['o.id' => $orderIds])
    			->groupBy(['u.id', 'u.first_name', 'u.last_name']);
    	
    	
    	return (new \yii\db\Query())
    			->select(['u.id', 'u.first_name', 'u.last_name', 'COUNT(u.id)'])
    			->from('order o')
    			->join('INNER JOIN', 'user u', 'u.id = o.user_id')
    			->where(['o.id' => $orderIds])
    			->groupBy(['u.id', 'u.first_name', 'u.last_name'])
    			->all();
    }   
    
    public static function getByName($name) {
    	return (new Query())->select(['c.id'])
		    	->from('company c')
		    	->where(['like', 'c.name', $name])
		    	->all();
    }
    
    public static function getWithChildrenByName($name) {
    	return (new Query())->select(['c.id'])
    			->from('company c')
    			->join('LEFT JOIN', 'company c1', 'c.parent_company_id = c1.id')
    			->where(['or',
    					['and', ['like', 'c.name', $name], 'c.parent_company_id IS NULL'],
    					['and', ['like', 'c1.name', $name], 'c.parent_company_id IS NOT NULL']
    			])
    			->all();		
    }
}

