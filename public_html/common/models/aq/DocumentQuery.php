<?php

namespace common\models\aq;

use Yii;
use yii\mongodb\Query;
use common\models\ar\Company;
use common\models\ar\Currency;
use common\documents\DocumentType;
use common\helpers\StringHelper;

/**
 * This is the ActiveQuery class for [[\common\models\ar\Task]].
 *
 * @see \common\models\ar\Task
 */
class DocumentQuery {

    public static function getLostClients($dateFrom, $dateTo, $compareDateFrom, $compareDateTo) {
        
        $where = ['between', 'history.sections.dateOfIssue', $compareDateFrom, $compareDateTo];
        $subQuery = new Query();
        $subQuery->select([])
                ->from('documents')
                ->orderBy(['history.sections.companyId' => SORT_DESC, 'history.createdAt' => SORT_DESC])
                ->where($where);
        $diffRows = $subQuery->all();
        
        $where = ['between', 'history.sections.dateOfIssue', $dateFrom, $dateTo];
        $query = new Query();
        $query->select([])
                ->from('documents')
                ->orderBy(['history.sections.companyId' => SORT_DESC, 'history.createdAt' => SORT_DESC])
                ->where($where);
        $rows = $query->all();
        
        foreach($diffRows as $diffRow){
            foreach($rows as $key => $value){
                if($diffRow['history'][0]['sections'][1]['companyId'] == $rows[$key]['history'][0]['sections'][1]['companyId']) {
                    unset($rows[$key]);
                }
            }
        }
        
        $results = [];
        $lastId = 0;
        $lastKey = 0;
        foreach($rows as $key => $value) {
            if($lastId == 0 || $lastId != $rows[$key]['history'][0]['sections'][1]['companyId']) {
                $companyId = $rows[$key]['history'][0]['sections'][1]['companyId'];
                $company = Company::findOne($companyId);
                
                $currencyId = $rows[$key]['history'][0]['sections'][2]['currencyId'];
                $currency = Currency::findOne($currencyId);
                
                $results[$key]['currency'] = $currency->short_symbol;

                if($company->parent_company_id != null) {
                    $parentCompany = Company::findOne($company->parent_company_id);
                    $results[$key]['name'] = $company->name . ' (' . $parentCompany->name . ')';
                } else {
                    $results[$key]['name'] = $company->name;
                }

                $results[$key]['numberOfInvoices'] = 1;
                
                $products = $rows[$key]['history'][0]['sections'][2]['products'];
                $price = 0;
                foreach($products as $product){
                    $price += $product['price'] * $product['count'];
                }
                
                $results[$key]['price'] = $price;
                $lastId = $companyId;
                $lastKey = $key;
                
            } else {
                $results[$lastKey]['numberOfInvoices'] += 1;
                $products = $rows[$key]['history'][0]['sections'][2]['products'];
                $price = 0;
                foreach($products as $product) {
                    $price += $product['price'] * $product['count'];
                }
                $results[$lastKey]['price'] += $price;
            }
        }
        return $results;
    }
    
    public static function getOrderIdsFromInvoices($dateFrom, $dateTo) {
    	$query = new Query();
    	$result = $query->select(['history.sections.orderId'])
    			->from('documents')
    			->where(['type' => 'INVOICE'])
    			->andWhere(['between', 'history.sections.dateOfIssue', $dateFrom, $dateTo])
    			->all();
    	
    	$actualResult = [];
    	
    	if(!empty($result)) {
	    	foreach($result as $row) {
	    		foreach($row['history'][0]['sections'] as $section) {
	    			if(isset($section['orderId'])) {
	    				$actualResult[] = $section['orderId'];
	    			}
	    		}
	    	}    	
    	}
    	
    	return $actualResult;
    }
    
    public static function getInvoiceIssueDates($orderIds) {
    	$result = (new Query())->select(['history.sections.orderId', 'history.sections.dateOfIssue'])
    			->from('documents')
    			->where(['type' => 'INVOICE'])
    			->andWhere(['in', 'history.sections.orderId', array_map('intval', $orderIds)])
    			->orderBy(['history.createdAt' => SORT_ASC])
    			->all();
    	
    	$actualResult = [];
    	
    	if(!empty($result)) {
    		foreach($result as $row) {
    			foreach($row['history'][0]['sections'] as $section) {
    				if(isset($section['orderId']) && isset($section['dateOfIssue'])) {
    					$actualResult[$section['orderId']] = $section['dateOfIssue'];
    				}
    			}
    		}
    	}
    	
    	return $actualResult;
    }
    
    public static function getLastInvoiceIds() {
		if(!isset(Yii::$app->params['isDocumentStorageAvailable']) || empty(Yii::$app->params['isDocumentStorageAvailable'])) {
			return [];
		}
		
		$output = [];
    	/*$documentsCollection = Yii::$app->mongodb->getCollection('documents');
    	$results = $documentsCollection->aggregate([
				[
						'$sort' => ['_id' => 1]
				],
				[
						'$group' => [
								'_id' => '$type', 
								'documentId' => ['$last' => '$_id']
						]
				],
		]);
    	
    	$invoiceTypes = DocumentType::getInvoiceTypes();
    	
    	foreach($results as $row) {
    		if(in_array($row['_id'], $invoiceTypes)) {
    			$output[] = $row['documentId']->__toString();
    		}
    	}*/
		
		$result = InvoiceHistoryQuery::getLastInvoices();
		if(!empty($result)) {
			$output = array_column($result, 'documentId');
		}
    	
    	return $output;
    }
    
    public static function getLastInvoiceDateByOrderId($orderId, $type) {
    	$result = (new Query())->select(['history.createdAt'])
		    	->from('documents')
		    	->where(['type' => $type])
		    	->andWhere(['in', 'history.sections.orderId', intval($orderId)])
		    	->orderBy(['history.createdAt' => SORT_ASC])
		    	->limit(1)
		    	->one();
    	
		if(!empty($result)) {
			$dt = new \DateTime();
			$historyCount = count($result['history']);
			return StringHelper::getFormattedDateFromTimestamp($result['history'][$historyCount-1]['createdAt'], true);
		}
		else {
			return null;
		}
    }
    
    public static function getInvoicesByOrderIds($orderIds = []) {
    	$result = (new Query())->select(['history.sections.orderId', 'history.sections.number', 
    					'history.authorId', 'history.sections.products'])
		    	->from('documents')
		    	->where(['type' => DocumentType::INVOICE])
		    	->andWhere(['in', 'history.sections.orderId', array_map('intval', $orderIds)])
		    	->orderBy(['history.sections.orderId' => SORT_ASC])
		    	->all();
    	
    	$actualResult = [];
    	 
    	if(!empty($result)) {
    		foreach($result as $row) {
    			$invoice = [];
    			$invoice['authorId'] = $row['history'][0]['authorId']; 
    			
    			foreach($row['history'][0]['sections'] as $section) {
    				if(isset($section['orderId'])) {
    					$invoice['orderId'] = $section['orderId'];
    					$invoice['number'] = $section['number'];
    				}
    				elseif(isset($section['products'])) {
    					$invoice['products'] = $section['products'];
    				}
    			}
    			
    			$actualResult[] = $invoice;
    		}
    	}
    	 
    	return $actualResult;
    }
}
