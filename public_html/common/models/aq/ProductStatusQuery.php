<?php

namespace common\models\aq;

use Yii;
use yii\db\Query;
/**
 * This is the ActiveQuery class for [[\common\models\ar\ProductStatus]].
 *
 * @see \common\models\ar\ProductStatus
 */
class ProductStatusQuery extends \yii\db\ActiveQuery {

    /*public function active() {
        $this->andWhere('[[status]]=1');
        return $this;
    }*/

    /**
     * @inheritdoc
     * @return \common\models\ar\ProductStatus[]|array
     */
    public function all($db = null) {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return \common\models\ar\ProductStatus|array|null
     */
    public function one($db = null) {
        return parent::one($db);
    }
    
    public static function getStatusesByNotSymbols($symbols = []) {
    	$query = (new Query())->select(['ps.id', 'pst.name'])
    			->from('product_status ps')
    			->join('INNER JOIN', 'product_status_translation pst', 'ps.id = pst.product_status_id')
    			->join('INNER JOIN', 'language l', 'pst.language_id = l.id')
    			->where(['ps.is_active' => true])
    			->andWhere(['l.symbol' => Yii::$app->language])
    			->orderBy('ps.id');
    			
    	if(!empty($symbols)) {
    		$query->andWhere(['not in', 'ps.symbol', $symbols]);
    	}
    			
    	return $query->all();
    }
}