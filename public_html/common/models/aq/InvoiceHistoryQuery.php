<?php

namespace common\models\aq;

use Yii;
use common\models\ar\DocumentType;
use common\models\ar\InvoiceHistory;
use yii\db\Query;

/**
 * This is the ActiveQuery class for [[\common\models\ar\InvoiceHistory]].
 *
 * @see \common\models\ar\InvoiceHistory
 */
class InvoiceHistoryQuery extends \yii\db\ActiveQuery {

    /*public function active() {
        $this->andWhere('[[status]]=1');
        return $this;
    }*/

    /**
     * @inheritdoc
     * @return \common\models\ar\InvoiceHistory[]|array
     */
    public function all($db = null) {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return \common\models\ar\InvoiceHistory|array|null
     */
    public function one($db = null) {
        return parent::one($db);
    }
    
    public static function saveInvoiceHistory($document, $documentId, $updateNumeration = true) {
    	$transaction = Yii::$app->db->beginTransaction();
    	try {
    		$documentTypeId = DocumentType::find()->where(['symbol' => $document->getType()])->one()->id;
			$section = $document->getSectionsSequence()[1];
    		$entityId = $section->entityId;
    		$dateIssue = $section->dateOfIssue;
    		$number = $section->number;
    		
    		if($updateNumeration) {
        		InvoiceHistory::updateAll(['is_last' => 0], [
        				'document_type_id' => intval($documentTypeId), 
        				'entity_id' => intval($entityId)
    			]);
    		}
    		
	    	$invoiceHistory = new InvoiceHistory();
	    	$invoiceHistory->document_type_id = $documentTypeId;
	    	$invoiceHistory->entity_id = $entityId;
	    	$invoiceHistory->document_id = $documentId;
	    	$invoiceHistory->date_issue = $dateIssue;
	    	$invoiceHistory->number = $number;
	    	$invoiceHistory->is_last = $updateNumeration;
	    	if(!$invoiceHistory->save()) {
	    		throw new \Exception('Invoice history cannot be saved: documentTypeId: '.$documentTypeId
	    				.', entityId: '.$entityId
	    				.', documentId: '.$documentId
	    		);	
	    	}
	    	
	    	$transaction->commit();
    	}
    	catch(\Exception $e) {
    		$transaction->rollBack();
    		Yii::error($e->getMessage());
    		Yii::error($e->getTraceAsString());
    	}
    }
    
    public static function getLastInvoices() {
    	return (new Query())->select(['ih.document_id as documentId'])
    			->from('invoice_history ih')
    			->where(['is_last' => 1])
    			->all();
    }    
    
    public static function getLastButOneInvoice($documentTypeSymbol, $entityId, $number = null) {
        $query = (new Query())->select(['ih.id', 'ih.date_issue as dateIssue'])
                ->from('invoice_history ih')
                ->join('INNER JOIN', 'document_type dt', 'ih.document_type_id = dt.id')
                ->where(['dt.symbol' => $documentTypeSymbol])
                ->andWhere(['ih.entity_id' => intval($entityId)])
                ->andWhere(['is_last' => 0]);
        
        if(!empty($number)) {
            $query->andWhere(['number' => $number]);
        }
        
    	return $query->orderBy(['ih.date_creation' => SORT_DESC])
    			->limit(1)
    			->one();
    }
    
    public static function findByDocumentId($documentId) {
    	return (new Query())->select(['ih.id'])
    			->from('invoice_history ih')
    			->where(['document_id' => $documentId])
    			->one();
    }
    
    public static function deleteInvoiceHistory($documentId, $previousId) {
    	$transaction = Yii::$app->db->beginTransaction();
    	try {
    		InvoiceHistory::deleteAll(['document_id' => $documentId]);
    		if(!empty($previousId)) {
    			InvoiceHistory::updateAll(['is_last' => 1], ['id' => intval($previousId)]);
    		}
    	
    		$transaction->commit();
    	}
    	catch(\Exception $e) {
    		$transaction->rollBack();
    		Yii::error($e->getMessage());
    		Yii::error($e->getTraceAsString());
    	}
    }
}