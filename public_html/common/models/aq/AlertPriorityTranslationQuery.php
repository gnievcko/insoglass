<?php

namespace common\models\aq;

/**
 * This is the ActiveQuery class for [[\common\models\ar\AlertPriorityTranslation]].
 *
 * @see \common\models\ar\AlertPriorityTranslation
 */
class AlertPriorityTranslationQuery extends \yii\db\ActiveQuery {

    /*public function active() {
        $this->andWhere('[[status]]=1');
        return $this;
    }*/

    /**
     * @inheritdoc
     * @return \common\models\ar\AlertPriorityTranslation[]|array
     */
    public function all($db = null) {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return \common\models\ar\AlertPriorityTranslation|array|null
     */
    public function one($db = null) {
        return parent::one($db);
    }
}