<?php

namespace common\models\aq;

use Yii;
use yii\db\Query;
/**
 * This is the ActiveQuery class for [[\common\models\ar\WarehouseProductStatusHistoryData]].
 *
 * @see \common\models\ar\WarehouseProductStatusHistoryData
 */
class WarehouseProductStatusHistoryDataQuery extends \yii\db\ActiveQuery {

    /*public function active() {
        $this->andWhere('[[status]]=1');
        return $this;
    }*/

    /**
     * @inheritdoc
     * @return \common\models\ar\WarehouseProductStatusHistoryData[]|array
     */
    public function all($db = null) {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return \common\models\ar\WarehouseProductStatusHistoryData|array|null
     */
    public function one($db = null) {
        return parent::one($db);
    }
    
    public static function getDataByHistoryId($historyId) {
    	$referenceSchemas = (new Query())->select(['psr.id', 'psr.name_table', 'psr.name_column', 'psr.name_expression', 
    				'psr.url_details', 'wpshd.referenced_object_id', 'wpshd.custom_value', 'psrt.name'])
				->from('warehouse_product_status_history_data wpshd')
				->join('INNER JOIN', 'product_status_reference psr', 'wpshd.product_status_reference_id = psr.id')
				->join('INNER JOIN', 'product_status_reference_translation psrt', 'psr.id = psrt.reference_id')
				->join('INNER JOIN', 'language l', 'psrt.language_id = l.id')
				->where(['wpshd.history_id' => intval($historyId)])
				->andWhere(['l.symbol' => Yii::$app->language])
				->orderBy(['psr.id' => SORT_ASC])
    			->all(); 
    	
    	$results = [];
    	foreach($referenceSchemas as $schema) {
    		if(!empty($schema['name_table'])) {
	    		$row = Yii::$app->db->createCommand('
	    					SELECT '.$schema['name_expression'].' as name
	    					FROM `'.$schema['name_table'].'`
	    					WHERE '.$schema['name_column'].' = :id
	    				')
	    				->bindValues([
	    						':id' => intval($schema['referenced_object_id'])
	    				])
	    				->queryOne();
	    				
	    		$results[] = [
	    				'url' => $schema['url_details'],
	    				'label' => $schema['name'],
	    				'name' => $row['name'], 
	    				'id' => $schema['referenced_object_id'],
	    				'table' => $schema['name_table'],
	    		];
    		}
    		else {
    			$results[] = [
    					'url' => null,
    					'label' => $schema['name'],
    					'name' => $schema['custom_value'],
    					'id' => null,
    					'table' => null,
    			];
    		}
    	}
    	
    	return $results;
    }
}