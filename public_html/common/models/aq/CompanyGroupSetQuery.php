<?php

namespace common\models\aq;

use Yii;
use yii\db\Query;

/**
 * This is the ActiveQuery class for [[\common\models\ar\CompanyGroupSet]].
 *
 * @see \common\models\ar\CompanyGroupSet
 */
class CompanyGroupSetQuery extends \yii\db\ActiveQuery {

    /*public function active() {
        $this->andWhere('[[status]]=1');
        return $this;
    }*/

    /**
     * @inheritdoc
     * @return \common\models\ar\CompanyGroupSet[]|array
     */
    public function all($db = null) {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return \common\models\ar\CompanyGroupSet|array|null
     */
    public function one($db = null) {
        return parent::one($db);
    }
    
    public static function getGroupsByCompanyId($companyId, $onlyNotPredefined = true) {
    	return Yii::$app->db->createCommand('
    				SELECT cg.id, cgt.name
    				FROM company_group_set cgs 
    				JOIN company_group cg ON cgs.company_group_id = cg.id
    				JOIN company_group_translation cgt ON cg.id = cgt.company_group_id
    				JOIN language l ON cgt.language_id = l.id
    				WHERE cgs.company_id = :companyId
    				'.(!empty($onlyNotPredefined) ? ' AND cg.is_predefined = 0 ' : '').'
    			')
    			->bindValues([
    					':companyId' => intval($companyId),
    			])
    			->queryAll();
    }
    
    public static function getPredefinedGroupsByCompanyId($companyId) {
    	return Yii::$app->db->createCommand('
    				SELECT cg.id, cgt.name
    				FROM company_group_set cgs
    				JOIN company_group cg ON cgs.company_group_id = cg.id
    				JOIN company_group_translation cgt ON cg.id = cgt.company_group_id
    				JOIN language l ON cgt.language_id = l.id
    				WHERE cgs.company_id = :companyId
    				AND cg.is_predefined = 1
    			')
    	    	->bindValues([
    	    			':companyId' => intval($companyId),
    	    	])
    	    	->queryAll();
    }

    public static function getCompaniesByGroupId($groupId) {
        return (new Query())->select(['c.id', 'c.name'])
            ->from('company c')
            ->join('INNER JOIN', 'company_group_set cgs', 'cgs.company_id = c.id')
            ->where(['cgs.company_group_id' => $groupId])
            ->all();
    }
}
