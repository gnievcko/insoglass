<?php

namespace common\models\aq;

use Yii;
use yii\db\Query;
/**
 * This is the ActiveQuery class for [[\common\models\ar\ProductionPath]].
 *
 * @see \common\models\ar\ProductionPath
 */
class ProductionPathQuery extends \yii\db\ActiveQuery {

    /*public function active() {
        $this->andWhere('[[status]]=1');
        return $this;
    }*/

    /**
     * @inheritdoc
     * @return \common\models\ar\ProductionPath[]|array
     */
    public function all($db = null) {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return \common\models\ar\ProductionPath|array|null
     */
    public function one($db = null) {
        return parent::one($db);
    }
    
    public static function getDefaultPath() {
        return (new Query())->select(['pp.id', 'pp.topological_order as topologicalOrder'])
                ->from('production_path pp')
                ->where(['pp.is_default' => true])
                ->one();
    }
    
    public static function getById($id) {
        return (new Query())->select(['pp.id', 'pp.topological_order as topologicalOrder'])
                ->from('production_path pp')
                ->where(['pp.id' => intval($id)])
                ->one();
    }
    
    public static function getProductionPathsQuery($params = []) {
        $query = (new Query())->select(['pp.id', 'pp.symbol', 'pp.topological_order as topologicalOrder',
                'pp.is_default as isDefault', 'ppt.name as name'])
            ->from('production_path pp')
            ->innerJoin('production_path_translation ppt', 'pp.id = ppt.production_path_id')
            ->innerJoin('language l', 'ppt.language_id = l.id')
            ->where(['l.symbol' => Yii::$app->language])
            ->andWhere(['pp.is_active' => 1]);
        
        $columnsMap = [
            'symbol' => 'pp.symbol',
            'topologicalOrder' => 'pp.topological_order',
            'isDefault' => 'pp.is_default',
            'name' => 'ppt.name',
        ];
        
        if (!empty($params['name'])) {
            $query->andWhere(['like', 'ppt.name', $params['name']]);
        }
        
        if(!empty($params['sortDir']) && !empty($columnsMap[$params['sortField']])) {
            $query->orderBy([
                $columnsMap[$params['sortField']] => (!empty($params['sortDir']) && $params['sortDir'] == 'desc') ? SORT_DESC : SORT_ASC
            ]);
        }
        else {
            $query->orderBy([$columnsMap['symbol'] => SORT_ASC]);
        }

        return $query;
    }
    
    public static function getDetails($id) {
        $query = (new Query())
            ->select(['pp.id', 'pp.symbol', 'pp.topological_order as topologicalOrder',
                'pp.is_default as isDefault', 'ppt.name as name'])
            ->from('production_path pp')
            ->innerJoin('production_path_translation ppt', 'pp.id = ppt.production_path_id')
            ->innerJoin('language l', 'ppt.language_id = l.id')
            ->where([
                'pp.id' => $id,
                'l.symbol' => Yii::$app->language  
            ]);
            
        return $query->one();
    }
                
    public static function getTranslationById($id) {
        return (new Query())->select(['pp.id', 'ppt.name'])
                ->from('production_path pp')
                ->join('inner join', 'production_path_translation ppt', 'ppt.production_path_id = pp.id')
                ->join('inner join', 'language l', 'ppt.language_id = l.id')
                ->where(['l.symbol' => Yii::$app->language, 'pp.id' => $id])
                ->one();
    }
    
    public static function getAllNotDefault() {
        return (new Query())->select(['pp.id', 'ppt.name'])
                ->from('production_path pp')
                ->innerJoin('production_path_translation ppt', 'pp.id = ppt.production_path_id')
                ->innerJoin('language l', 'ppt.language_id = l.id')
                ->where(['l.symbol' => Yii::$app->language])
                ->andWhere(['pp.is_default' => false])
                ->orderBy(['ppt.name' => SORT_ASC])
                ->all();
    }
}