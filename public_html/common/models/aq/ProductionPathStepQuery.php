<?php

namespace common\models\aq;

use Yii;
use yii\db\Query;
/**
 * This is the ActiveQuery class for [[\common\models\ar\ProductionPathStep]].
 *
 * @see \common\models\ar\ProductionPathStep
 */
class ProductionPathStepQuery extends \yii\db\ActiveQuery {

    /*public function active() {
        $this->andWhere('[[status]]=1');
        return $this;
    }*/

    /**
     * @inheritdoc
     * @return \common\models\ar\ProductionPathStep[]|array
     */
    public function all($db = null) {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return \common\models\ar\ProductionPathStep|array|null
     */
    public function one($db = null) {
        return parent::one($db);
    }
    
    public static function getByProductionPathId($productionPathId) {
        return (new Query())->select(['pps.id', 'pps.activity_id as activityId', 'pps.step_required_ids as requiredIds',
                    'pps.step_successor_ids as successorIds', 'at.name as activityName', 
                    'pps.attribute_needed_ids as attributeNeededIds', 'pps.attribute_forbidden_ids as attributeForbiddenIds',
                ])
                ->from('production_path_step pps')
                ->join('inner join', 'activity a', 'pps.activity_id = a.id')
                ->join('inner join', 'activity_translation at', 'a.id = at.activity_id')
                ->join('inner join', 'language l', 'at.language_id = l.id')
                ->where(['pps.production_path_id' => intval($productionPathId)])
                ->andWhere(['l.symbol' => Yii::$app->language])
                ->all();
    }
}