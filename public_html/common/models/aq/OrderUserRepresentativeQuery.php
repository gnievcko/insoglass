<?php

namespace common\models\aq;

use yii\db\Query;
/**
 * This is the ActiveQuery class for [[\common\models\ar\OrderUserRepresentative]].
 *
 * @see \common\models\ar\OrderUserRepresentative
 */
class OrderUserRepresentativeQuery extends \yii\db\ActiveQuery {

    /*public function active() {
        $this->andWhere('[[status]]=1');
        return $this;
    }*/

    /**
     * @inheritdoc
     * @return \common\models\ar\OrderUserRepresentative[]|array
     */
    public function all($db = null) {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return \common\models\ar\OrderUserRepresentative|array|null
     */
    public function one($db = null) {
        return parent::one($db);
    }
    
    public static function getUserRepresentativesByOrderId($orderId) {
    	return (new Query())->select(['u.id as id', 'u.first_name as firstName', 'u.last_name as lastName',
    				'u.email as email', 'u.contact_email as contactEmail', 'u.phone1 as phone', 'u.phone2 as phone2',
    			])
    			->from('order_user_representative our')
    			->join('INNER JOIN', 'user u', 'our.user_representative_id = u.id')
    			->where(['our.order_id' => intval($orderId)])
    			->orderBy(['u.last_name' => SORT_ASC, 'u.first_name' => SORT_ASC])
    			->all();
    }
}