<?php

namespace common\models\aq;

use Yii;
use yii\db\Query;

/**
 * This is the ActiveQuery class for [[\common\models\ar\OrderOfferedProduct]].
 *
 * @see \common\models\ar\OrderOfferedProduct
 */
class OrderOfferedProductQuery extends \yii\db\ActiveQuery {

    /*public function active() {
        $this->andWhere('[[status]]=1');
        return $this;
    }*/

    /**
     * @inheritdoc
     * @return \common\models\ar\OrderOfferedProduct[]|array
     */
    public function all($db = null) {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return \common\models\ar\OrderOfferedProduct|array|null
     */
    public function one($db = null) {
        return parent::one($db);
    }
    
    // TODO: Tutaj uwzglednić również produkty magazynowe
    public static function getOfferedProductsByOrderId($orderId) {
    	return Yii::$app->db->createCommand('
    				SELECT op.id, ( 
    						SELECT COALESCE(opt1.name, op1.symbol)
    						FROM offered_product op1
    						JOIN offered_product_translation opt1 ON op1.id = opt1.offered_product_id
    						JOIN language l1 ON opt1.language_id = l1.id
    						WHERE (l1.symbol IS NULL OR l1.symbol = :languageSymbol)
    						AND op1.id = op.parent_offered_product_id
    					) as parentName,
    					COALESCE(opt.name, op.symbol) as name, oop.count, op.is_artificial as isArtificial,
    					oop.description, oop.price, c.short_symbol as currency, c.conversion_value as conversionValue
					FROM order_offered_product oop
    				JOIN offered_product op ON oop.offered_product_id = op.id
    				LEFT JOIN offered_product_translation opt ON op.id = opt.offered_product_id
    				LEFT JOIN language l ON opt.language_id = l.id
    				LEFT JOIN currency c ON oop.currency_id = c.id
					WHERE oop.order_id = :orderId
    				AND (l.symbol IS NULL OR l.symbol = :languageSymbol)
    				AND oop.is_active = 1
    				ORDER BY op.is_artificial, COALESCE(opt.name, op.symbol)
    			')
        		->bindValues([
        				':orderId' => intval($orderId),
        				':languageSymbol' => Yii::$app->language,
        		])
        		->queryAll();
    }
    
    public static function getAllByHistoryId($historyId, $params = []) {
        $query = (new Query())
            ->select('oop.*')
            ->from('order_offered_product oop')
            ->innerJoin('offered_product op', 'oop.offered_product_id = op.id')
            ->where(['order_history_id' => $historyId]);
        
        if(isset($params['isArtificial'])) {
            $query->andWhere(['op.is_artificial' => $params['isArtificial']]);
        }
        if(isset($params['isActive'])) {
            $query->andWhere(['oop.is_active' => $params['isActive']]);
        }
        
        return $query->orderBy(['oop.id' => 'asc'])
            ->all();
    }
    
    public static function getProductsForProductionByOrderId($orderId) {
        return (new Query())->select(['oop.id', 'COALESCE(oop.production_path_id, op.production_path_id) as productionPathId', 
                    'COALESCE(oop.attribute_value, op.attribute_value) as attributeValue', 'opt.name',
                ])
                ->from('order_offered_product oop')
                ->join('left join', 'offered_product op', 'oop.offered_product_id = op.id')
                ->join('left join', 'offered_product_translation opt', 'op.id = opt.offered_product_id')
                ->join('left join', 'language l', 'opt.language_id = l.id')
                ->where(['oop.order_id' => intval($orderId)])
                ->andWhere(['oop.is_active' => true])
                ->andWhere(['or', ['l.symbol' => null], ['l.symbol' => Yii::$app->language]])
                ->all();
    }
    
    public static function getProductsReadyForQaAssignment() {
        return (new Query())->select(['pt.order_offered_product_id as orderOfferedProductId', 'pt.id as productionTaskId'])
                ->from('production_task pt')
                ->innerJoin('task t', 'pt.task_id = t.id')
                ->innerJoin('production_path_step pps', 'pt.production_path_step_id = pps.id')
                ->innerJoin('activity a', 'pps.activity_id = a.id')
                ->where(['t.is_complete' => true])
                ->andWhere(['a.is_qa_required' => true])
                ->all();
    }
}