<?php

namespace common\models\aq;

use Yii;
use yii\db\Query;

/**
 * This is the ActiveQuery class for [[\common\models\ar\TaskTypeReference]].
 *
 * @see \common\models\ar\TaskTypeReference
 */
class TaskTypeReferenceQuery extends \yii\db\ActiveQuery {

    /*public function active() {
        $this->andWhere('[[status]]=1');
        return $this;
    }*/

    /**
     * @inheritdoc
     * @return \common\models\ar\TaskTypeReference[]|array
     */
    public function all($db = null) {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return \common\models\ar\TaskTypeReference|array|null
     */
    public function one($db = null) {
        return parent::one($db);
    }
    
    public static function getAdditionalFieldsByTaskTypeId($taskTypeId) {
    	return (new Query())->select(['ttr.id', 'ttr.symbol', 'ttrt.name', 'ttr.name_table as nameTable',
    			'ttr.name_column as nameColumn', 'ttr.is_required as isRequired', 'ttr.task_type_id as taskTypeId'])
    			->from('task_type_reference ttr')
    			->join('INNER JOIN', 'task_type_reference_translation ttrt', 'ttr.id = ttrt.reference_id')
    			->join('INNER JOIN', 'language l', 'ttrt.language_id = l.id')
    			->where(['ttr.task_type_id' => $taskTypeId])
    			->andWhere(['l.symbol' => Yii::$app->language])
    			->orderBy(['ttr.id' => SORT_ASC])
    			->all();
    }
    
    public static function getAdditionalFieldsByTaskTypeIds($taskTypeIds) {
    	return (new Query())->select(['ttr.id', 'ttr.symbol', 'ttrt.name', 'ttr.name_table as nameTable',
    				'ttr.name_column as nameColumn', 'ttr.is_required as isRequired', 'ttr.task_type_id as taskTypeId'])
    			->from('task_type_reference ttr')
    			->join('INNER JOIN', 'task_type_reference_translation ttrt', 'ttr.id = ttrt.reference_id')
    			->join('INNER JOIN', 'language l', 'ttrt.language_id = l.id')
    			->where(['ttr.task_type_id' => $taskTypeIds])
    			->andWhere(['l.symbol' => Yii::$app->language])
    			->orderBy(['ttr.id' => SORT_ASC])
    			->all();
    }
}