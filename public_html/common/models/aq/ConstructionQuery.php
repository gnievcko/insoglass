<?php

namespace common\models\aq;

use Yii;
use yii\db\Query;
/**
 * This is the ActiveQuery class for [[\common\models\ar\Construction]].
 *
 * @see \common\models\ar\Construction
 */
class ConstructionQuery extends \yii\db\ActiveQuery {

    /*public function active() {
        $this->andWhere('[[status]]=1');
        return $this;
    }*/

    /**
     * @inheritdoc
     * @return \common\models\ar\Construction[]|array
     */
    public function all($db = null) {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return \common\models\ar\Construction|array|null
     */
    public function one($db = null) {
        return parent::one($db);
    }
    
    public static function getNames(){
        $query = (new Query())->select(['c.id', 'ct.name'])
            ->from('construction c')
            ->innerJoin('construction_translation ct', 'c.id = ct.construction_id')
            ->innerJoin('language l', 'ct.language_id = l.id')
            ->where(['l.symbol' => Yii::$app->language])
            ->orderBy(['ct.name' => SORT_ASC]);
        return $query->all();
    }
    
    public static function getByName($name) {
        return (new Query())->select(['c.id'])
                ->from('construction c')
                ->innerJoin('construction_translation ct', 'c.id = ct.construction_id')
                ->innerJoin('language l', 'ct.language_id = l.id')
                ->where(['l.symbol' => Yii::$app->language])
                ->andWhere(['ct.name' => $name])
                ->one();
    }
    
}