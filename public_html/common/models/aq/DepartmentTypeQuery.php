<?php

namespace common\models\aq;
use Yii;
use yii\db\Query;

/**
 * This is the ActiveQuery class for [[\common\models\ar\DepartmentType]].
 *
 * @see \common\models\ar\DepartmentType
 */
class DepartmentTypeQuery extends \yii\db\ActiveQuery {

    /*public function active() {
        $this->andWhere('[[status]]=1');
        return $this;
    }*/

    /**
     * @inheritdoc
     * @return \common\models\ar\DepartmentType[]|array
     */
    public function all($db = null) {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return \common\models\ar\DepartmentType|array|null
     */
    public function one($db = null) {
        return parent::one($db);
    }

    public function getAllTypes(){
        return (new Query())->select(['dt.id as id','IFNULL(dttr.name, dt.symbol) as name'])
            ->from('department_type dt')
            ->join('LEFT JOIN', 'department_type_translation dttr' , 'dt.id = dttr.department_type_id')
            ->join('LEFT JOIN', 'language l', 'dttr.language_id = l.id')
            ->andWhere(['or', ['dttr.name' => null],['l.symbol' => Yii::$app->language] ])
            ->orderBy('dttr.name');
    }
}