<?php

namespace common\models\aq;

use Yii;
use common\helpers\Utility;
use common\models\ar\UserCompanySalesman;
/**
 * This is the ActiveQuery class for [[\common\models\ar\CompanyGroupTranslation]].
 *
 * @see \common\models\ar\CompanyGroupTranslation
 */
class CompanyGroupTranslationQuery extends \yii\db\ActiveQuery {

    /*public function active() {
        $this->andWhere('[[status]]=1');
        return $this;
    }*/

    /**
     * @inheritdoc
     * @return \common\models\ar\CompanyGroupTranslation[]|array
     */
    public function all($db = null) {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return \common\models\ar\CompanyGroupTranslation|array|null
     */
    public function one($db = null) {
        return parent::one($db);
    }

    public function forLanguage($languageSymbol) {
    	return $this
    			->joinWith('language')
    			->where(['language.symbol' => $languageSymbol]);
    }

    public function notPredefinedAndAvailable() {
		$param = [];

		if(!Yii::$app->user->can(Utility::ROLE_MAIN_SALESMAN)) {
	    	$subQuery = UserCompanySalesman::find()
	    		->where('user_company_salesman.user_id = :userId AND user_company_salesman.company_group_id = company_group.id',
	    				[':userId' => Yii::$app->user->id]);
			$param = ['exists', $subQuery];
		}
    	return $this
    			->joinWith('companyGroup')
    			->where(['or', ['company_group.symbol' => Utility::COMPANY_GROUP_DEFAULT],
    					['and', ['company_group.is_predefined' => false], $param]
    			]);
    }

    public function onlyDefault() {
    	return $this
				->joinWith('companyGroup')
		    	->where(['company_group.symbol' => Utility::COMPANY_GROUP_DEFAULT]);
    }
}
