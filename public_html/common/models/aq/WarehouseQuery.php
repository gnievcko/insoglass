<?php

namespace common\models\aq;

use yii\db\Query;
use common\models\ar\UserWarehouse;
use common\models\ar\Warehouse;
/**
 * This is the ActiveQuery class for [[\common\models\ar\Warehouse]].
 *
 * @see \common\models\ar\Warehouse
 */
class WarehouseQuery extends \yii\db\ActiveQuery {

    /*public function active() {
        $this->andWhere('[[status]]=1');
        return $this;
    }*/

    /**
     * @inheritdoc
     * @return \common\models\ar\Warehouse[]|array
     */
    public function all($db = null) {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return \common\models\ar\Warehouse|array|null
     */
    public function one($db = null) {
        return parent::one($db);
    }
    
	public static function getWarehouseList() {
    	$availableWarehouses = UserWarehouseQuery::getAvailableWarehouses();
    	
    	return (new Query())->select(['w.id', 'w.name'])
    			->from('warehouse w')
    			->where(['is_active' => true])
    			->orderBy('w.name')
    			->where(['w.id' => $availableWarehouses]);
	}

    public static function findWarehouseGroups($params = []) {
    	$query = (new \yii\db\Query())
		    	->select(['w.id', 'w.name'])
		    	->from('warehouse w')
		    	->andWhere('w.is_active = 1');
    
    	if(!empty($params['name'])) {
    		$query->andWhere(['like', 'w.name', $params['name']]);
    	}
    
    	$columnsMap = [
    			'name' => 'w.name',
    	];
    	    
    	if(!empty($params['sortDir']) && !empty($columnsMap[$params['sortField']])) {
    		$query->orderBy([
    				$columnsMap[$params['sortField']] => (!empty($params['sortDir']) && $params['sortDir'] == 'desc') ? SORT_DESC : SORT_ASC
    		]);
    	}
    
    	return $query;
    }
    
    public static function getDefaultWarehouse() {
    	return Warehouse::find()->orderBy('id')->limit(1);
    }
}