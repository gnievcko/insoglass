<?php

namespace common\models\aq;

use yii\db\Query;

/**
 * This is the ActiveQuery class for [[\common\models\ar\OrderOfferedProductAttachment]].
 *
 * @see \common\models\ar\OrderOfferedProductAttachment
 */
class OrderOfferedProductAttachmentQuery extends \yii\db\ActiveQuery {

    /*public function active() {
        $this->andWhere('[[status]]=1');
        return $this;
    }*/

    /**
     * @inheritdoc
     * @return \common\models\ar\OrderOfferedProductAttachment[]|array
     */
    public function all($db = null) {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return \common\models\ar\OrderOfferedProductAttachment|array|null
     */
    public function one($db = null) {
        return parent::one($db);
    }
    
    public static function getAttachmentsByOrderOfferedProductId($id) {
        $query = (new Query())->select(['oopa.id as id', 'oopa.url_file as urlFile', 'oopa.name', 'oopa.description', 'oopa.date_creation as dateCreation'])
            ->from('order_offered_product_attachment oopa')
            ->where(['oopa.order_offered_product_id' => $id])
            ->orderBy('oopa.date_creation');
        
        return $query->all();
    }
}