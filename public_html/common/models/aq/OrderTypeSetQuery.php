<?php

namespace common\models\aq;

use Yii;
use yii\db\Query;
use common\models\ar\OrderTypeSet;
/**
 * This is the ActiveQuery class for [[\common\models\ar\OrderTypeSet]].
 *
 * @see \common\models\ar\OrderTypeSet
 */
class OrderTypeSetQuery extends \yii\db\ActiveQuery {

    /*public function active() {
        $this->andWhere('[[status]]=1');
        return $this;
    }*/

    /**
     * @inheritdoc
     * @return \common\models\ar\OrderTypeSet[]|array
     */
    public function all($db = null) {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return \common\models\ar\OrderTypeSet|array|null
     */
    public function one($db = null) {
        return parent::one($db);
    }
    
    public static function getOrderTypesByOrderId($orderId) {
    	return (new Query())->select(['ot.id', 'ott.name'])
		    	->from('order_type_set ots')
		    	->join('INNER JOIN', 'order_type ot', 'ots.order_type_id = ot.id')
		    	->join('INNER JOIN', 'order_type_translation ott', 'ot.id = ott.order_type_id')
		    	->join('INNER JOIN', 'language l', 'ott.language_id = l.id')
		    	->where(['ots.order_id' => intval($orderId)])
		    	->orderBy(['ott.name' => SORT_ASC])
		    	->all();
    }
    
    public static function replaceOrderTypesOfOrder($orderId, $orderTypeIds = []) {
    	$transaction = Yii::$app->db->beginTransaction();
    	try {
    		OrderTypeSet::deleteAll(['order_id' => intval($orderId)]);
    		if(!empty($orderTypeIds)) {
    			foreach($orderTypeIds as $orderTypeId) {
    				$ots = new OrderTypeSet();
    				$ots->order_id = $orderId;
    				$ots->order_type_id = $orderTypeId;
    				if(!$ots->save()) {
    					throw new \Exception('Error during saving order_type_set: '.$orderId.', '.$orderTypeId);
    				}
    			}
    		}
    		
    		$transaction->commit();
    	}
    	catch(\Exception $e) {
    		$transaction->rollBack();
    		Yii::error($e->getMessage());
    		Yii::error($e->getTraceAsString());
    	}
    }
}