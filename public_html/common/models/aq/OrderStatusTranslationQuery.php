<?php

namespace common\models\aq;
use yii\db\Query;
use common\helpers\Utility;

/**
 * This is the ActiveQuery class for [[\common\models\ar\OrderStatusTranslation]].
 *
 * @see \common\models\ar\OrderStatusTranslation
 */
class OrderStatusTranslationQuery extends \yii\db\ActiveQuery {

    /*public function active() {
        $this->andWhere('[[status]]=1');
        return $this;
    }*/

    /**
     * @inheritdoc
     * @return \common\models\ar\OrderStatusTranslation[]|array
     */
    public function all($db = null) {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return \common\models\ar\OrderStatusTranslation|array|null
     */
    public function one($db = null) {
        return parent::one($db);
    }

    public function forLanguage($languageSymbol) {
        return $this
            ->joinWith('language')
            ->where(['language.symbol' => $languageSymbol]);
    }
    
    public function visibleForClient($isVisible) {
        return $this
            ->joinWith('orderStatus os')
            ->where(['os.is_visible_for_client' => $isVisible]);
    }

    public function successorsOf($statusId) {
        if(\Yii::$app->user->can(Utility::ROLE_ADMIN) && \Yii::$app->params['isUnlimitedOrderChangeStatusAvailable']) {
            return $this->andWhere(['language_id' => (new Query())->select(['id'])->from('language')->where(['symbol' => \Yii::$app->language])]);
        } else {
        	$statusIdsQuery = (new Query())
        		->select(['order_status_successor_id as status_id'])
        			->from('order_status_transition')
        			->where(['order_status_predecessor_id' => $statusId]);
        	$statusIdsQuery->union("SELECT {$statusId} as `status_id` FROM DUAL");
        	
        	if(\Yii::$app->params['canBacktrackStatuses']) {
        		$statusIdsQuery = $statusIdsQuery->union((new Query())
        				->select(['order_status_predecessor_id as status_id'])
        				->from('order_status_transition')
        				->where(['order_status_successor_id' => $statusId]));
        	}
        	
        	$ids = $statusIdsQuery->all();
        	if(!empty($ids)) {
        		$ids = array_column($ids, 'status_id');
        	}
        	
			$query = $this->andWhere(['order_status_id' => $ids])
            	->andWhere(['language_id' => (new Query())->select(['id'])->from('language')->where(['symbol' => \Yii::$app->language])]);
         	return $query;
        }
    }
}
