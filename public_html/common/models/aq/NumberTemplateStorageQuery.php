<?php

namespace common\models\aq;

use Yii;

/**
 * This is the ActiveQuery class for [[\common\models\ar\NumberTemplateStorage]].
 *
 * @see \common\models\ar\NumberTemplateStorage
 */
class NumberTemplateStorageQuery extends \yii\db\ActiveQuery {

    /*public function active() {
        $this->andWhere('[[status]]=1');
        return $this;
    }*/

    /**
     * @inheritdoc
     * @return \common\models\ar\NumberTemplateStorage[]|array
     */
    public function all($db = null) {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return \common\models\ar\NumberTemplateStorage|array|null
     */
    public function one($db = null) {
        return parent::one($db);
    }
    
    public static function updateCurrentNumberById($id, $currentNumber) {
    	Yii::$app->db->createCommand()->update('number_template_storage', ['current_number' => $currentNumber], 
    			['id' => intval($id)])->execute();
    }
}