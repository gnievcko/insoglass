<?php

namespace common\models\aq;

use yii\db\Query;
use Yii;
use common\helpers\Utility;

/**
 * This is the ActiveQuery class for [[\common\models\ar\Workstation]].
 *
 * @see \common\models\ar\Workstation
 */
class WorkstationQuery extends \yii\db\ActiveQuery {

    /*public function active() {
        $this->andWhere('[[status]]=1');
        return $this;
    }*/

    /**
     * @inheritdoc
     * @return \common\models\ar\Workstation[]|array
     */
    public function all($db = null) {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return \common\models\ar\Workstation|array|null
     */
    public function one($db = null) {
        return parent::one($db);
    }
    
    public static function getWorkstationsByDepartmentId($departmentId) {
        $query = (new Query())->select(['w.id', 'wt.name'])
                ->from('workstation w')
                ->innerJoin('workstation_translation wt', 'w.id = wt.workstation_id')
                ->innerJoin('language l', 'l.id = wt.language_id')
                ->where([
                    'l.symbol' => \Yii::$app->language,
                ]);
        
        if(!Yii::$app->user->can(Utility::ROLE_ADMIN) && !Yii::$app->user->can(Utility::ROLE_MAIN_WORKER)) {
            $query->andWhere(['w.department_id' => intval($departmentId)]);
        }
            
        return $query->all();
    }
    
    /**
     * 
     */
    public static function getActiveWorkstationQuery($params = []) {
        $query = (new Query())
                ->select(['w.id', 'w.symbol', 'w.department_id as departmentId', 'wt.name as name', 'dt.name as departmentName'])
                ->from(['workstation w'])
                ->join('INNER JOIN', 'department_translation dt', 'w.department_id = dt.department_id')
                ->join('INNER JOIN', 'workstation_translation wt', 'w.id = wt.workstation_id')
                ->join('INNER JOIN', 'language l', 'l.id = wt.language_id')
                ->join('INNER JOIN', 'language lan', 'lan.id = dt.language_id')
                ->where(['l.symbol' => Yii::$app->language])
                ->andWhere(['lan.symbol' => Yii::$app->language]);
        
        $columnsMap = [
            'id' => 'w.id',
            'name' => 'wt.name',
            'departmentName' => 'dt.name',
        ];
        
        if(!empty($params['name'])) {
            $query->andWhere(['like', 'wt.name', $params['name']]);
        }
        
        if(!empty($params['departmentId'])) {
            $query->andWhere(['w.department_id' => $params['departmentId']]);
        }
        
        if(!empty($params['sortDir']) && !empty($columnsMap[$params['sortField']])) {
            $query->orderBy([
               $columnsMap[$params['sortField']] => (!empty($params['sortDir']) && $params['sortDir'] == 'desc') ? SORT_DESC : SORT_ASC 
            ]);
        }
        else {
            $query->orderBy([$columnsMap['name'] => SORT_ASC]);
        }

        return $query;
    }
    
    public static function getDetails($id) {
        return (new Query())
            ->select(['w.id as id', 'wt.name as name', 'dt.name as departmentName'])
            ->from(['workstation w'])
            ->join('INNER JOIN', 'workstation_translation wt', 'w.id = wt.workstation_id')
            ->join('INNER JOIN', 'department d', 'd.id = w.department_id')
            ->join('INNER JOIN', 'department_translation dt', 'd.id = dt.department_id')
            ->join('INNER JOIN', 'language l', 'l.id = wt.language_id')
            ->join('INNER JOIN', 'language lan', 'lan.id = dt.language_id')
            ->where(['l.symbol' => Yii::$app->language])
            ->andWhere(['lan.symbol' => Yii::$app->language])
            ->andWhere(['w.id' => $id])->one();
    }
}