<?php

namespace common\models\aq;

use Yii;
use yii\db\Query;
use common\helpers\Utility;

/**
 * This is the ActiveQuery class for [[\common\models\ar\ProductionTask]].
 *
 * @see \common\models\ar\ProductionTask
 */
class ProductionTaskQuery extends \yii\db\ActiveQuery {

    /*public function active() {
        $this->andWhere('[[status]]=1');
        return $this;
    }*/

    /**
     * @inheritdoc
     * @return \common\models\ar\ProductionTask[]|array
     */
    public function all($db = null) {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return \common\models\ar\ProductionTask|array|null
     */
    public function one($db = null) {
        return parent::one($db);
    }
    
    public static function getByOrderId($orderId) {
        return (new Query())->select(['pt.id', 'pt.production_path_step_id as stepId', 'oop.offered_product_id as offeredProductId',
                    'pt.task_required_ids as requiredIds', 'pt.task_successor_ids as successorIds', 'pt.is_available as isAvailable',
                ])
                ->from('production_task pt')
                ->join('inner join', 'order_offered_product oop', 'pt.order_offered_product_id = oop.id')
                ->join('inner join', 'production_path_step pps', 'pt.production_path_step_id = pps.id')
                ->where(['oop.order_id' => intval($orderId)])
                ->orderBy(['pt.id' => SORT_ASC])
                ->all();
    }
    
    public static function getOngoingOrders($params = []) {
        
        $fields = ['o.id as id', 'c.name as customerName', 'o.number as orderNumber',
            'oh.date_deadline as deadline', 'COALESCE(opt.name,"'.Yii::t('web', 'Normal').'") as priority', 'COUNT(oop.offered_product_id) as productCount'];
        
        $query = (new Query())->select($fields)
                ->from('order o')
                ->innerJoin('order_history oh', 'oh.id = o.order_history_last_id')
                ->innerJoin('order_status os', 'oh.order_status_id = os.id')
                ->innerJoin('order_offered_product oop', 'o.id = oop.order_id')
                ->innerJoin('offered_product op', 'op.id = oop.offered_product_id')
                ->innerJoin('production_task pt', 'pt.order_offered_product_id = oop.id')
                ->innerJoin('task t', 'pt.task_id = t.id')
                ->innerJoin('production_path_step pps', 'pt.production_path_step_id = pps.id')
                ->innerJoin('activity a', 'a.id = pps.activity_id')
                ->innerJoin('department_activity da', 'da.activity_id = a.id')
                ->innerJoin('department d', 'd.id = da.department_id')
                ->innerJoin('company c', 'c.id = o.company_id')
                ->leftJoin('order_priority opr', 'o.order_priority_id = opr.id')
                ->leftJoin('order_priority_translation opt', 'opr.id = opt.order_priority_id')
                ->leftJoin('language l', 'l.id = opt.language_id')
                ->where(['or', ['l.symbol' => Yii::$app->language], ['l.symbol' => null]])
                ->andWhere([
                    'o.is_active' => true,
                    'os.is_terminal' => false,
                    't.is_complete' => false,
                    'a.is_node' => false,
                ])
                ->groupBy(['o.id', 'c.name', 'o.number', 'oh.date_deadline', 
                        'opt.name', 'opr.priority']);
            
        if(!Yii::$app->user->can(Utility::ROLE_ADMIN) && !Yii::$app->user->can(Utility::ROLE_MAIN_WORKER)) {
            $query->andWhere(['d.id' => (new Query())
                        ->select(['ud.department_id'])
                        ->from('user_department ud')
                        ->where(['ud.user_id' => Yii::$app->user->id])
                    ]);
        }
            
        $columnsMap = [
            'customerName' => 'c.name',
            'orderNumber' => 'o.number',
            'deadline' => 'oh.date_deadline',
            'priority' => 'opr.priority',
        ];
        
        if (!empty($params['departmentId'])) {
            $query->andWhere(['d.id' => $params['departmentId']]);
        }
            
        if(!empty($params['sortDir']) && !empty($columnsMap[$params['sortField']])) {
            $query->orderBy([
                $columnsMap[$params['sortField']] => (!empty($params['sortDir']) && $params['sortDir'] == 'desc') ? SORT_DESC : SORT_ASC
            ]);
        }
        else {
            $query->orderBy([$columnsMap['deadline'] => SORT_ASC]);
        }
            
        return $query;
        
    }
    
    public static function getOrderProductsCompletionPercentage($orderId) {
        $subquery = (new Query())->select(['(SUM(pt.total_count_made)/ (COUNT(pt.total_count_made) * oop.count) * 100) as partialCount'])
                ->from('order_offered_product oop')
                ->innerJoin('production_task pt', 'pt.order_offered_product_id = oop.id')
                ->innerJoin('task t', 'pt.task_id = t.id')
                ->innerJoin('production_path_step pps', 'pt.production_path_step_id = pps.id')
                ->innerJoin('activity a', 'a.id = pps.activity_id')
                ->where(['and', ['a.is_node'=> false], ['oop.order_id' => $orderId]])
                ->groupBy(['oop.id']);
        
        $query = (new Query())->select(['IF(COUNT(partialCount)<> 0, ROUND(SUM(partialCount)/COUNT(partialCount)), 0) completionPercent'])
                ->from(['subquery'=>$subquery]);

        return $query->scalar();
    }
    
    public static function getOngoingProductedProducts($params = []) {
        $qaSubquery = (new Query())->select(['pt3.qa_factor_value'])
                ->from('production_task pt3')
                ->where('pt3.order_offered_product_id = pt.order_offered_product_id')
                ->andWhere('pt3.qa_factor_value IS NOT NULL')
                ->orderBy(['pt3.id' => SORT_DESC])
                ->limit(1);
        
        $fields = ['o.id as id', 'c.name as customerName', 'o.number as orderNumber',
            'op.symbol as productCode', 'oh.date_deadline as deadline', 'CONCAT(MIN(pt.total_count_made), "/", oop.count) as completedCount',
            'COALESCE(opt.name,"'.Yii::t('web', 'Normal').'") as priority', 'oop.id as orderOfferedProductId', 'qualityNote' => $qaSubquery];
        
        if(!empty($params['forShortList'])) {
            $fields[] = '(select ROUND(SUM(pt2.total_count_made)/ (COUNT(pt2.total_count_made) * oop.count) * 100)
                FROM production_task pt2
                JOIN production_path_step pps ON pt2.production_path_step_id = pps.id
                JOIN activity a ON a.id = pps.activity_id
                where pt2.order_offered_product_id = oop.id AND a.is_node = 0) completionPercent';
        }
        
        $query = (new Query())->select($fields)
                ->from('order o')
                ->innerJoin('order_history oh', 'oh.id = o.order_history_last_id')
                ->innerJoin('order_status os', 'oh.order_status_id = os.id')
                ->innerJoin('order_offered_product oop', 'o.id = oop.order_id')
                ->innerJoin('offered_product op', 'op.id = oop.offered_product_id')
                ->innerJoin('production_task pt', 'pt.order_offered_product_id = oop.id')
                ->innerJoin('task t', 'pt.task_id = t.id')
                ->innerJoin('production_path_step pps', 'pt.production_path_step_id = pps.id')
                ->innerJoin('activity a', 'a.id = pps.activity_id')
                ->innerJoin('department_activity da', 'da.activity_id = a.id')
                ->innerJoin('department d', 'd.id = da.department_id')
                ->innerJoin('company c', 'c.id = o.company_id')
                ->leftJoin('order_priority opr', 'o.order_priority_id = opr.id')
                ->leftJoin('order_priority_translation opt', 'opr.id = opt.order_priority_id')
                ->leftJoin('language l', 'l.id = opt.language_id')
                ->where(['or', ['l.symbol' => Yii::$app->language], ['l.symbol' => null]])
                ->andWhere([
                    'o.is_active' => true,
                    'os.is_terminal' => false,
                    //'t.is_complete' => false,
                    'a.is_node' => false,
                ])
                ->groupBy(['o.id', 'c.name', 'o.number', 'op.symbol', 'oh.date_deadline', 
                        'opt.name', 'oop.id', 'opr.priority', 'oop.count']);
            
        if(!Yii::$app->user->can(Utility::ROLE_ADMIN) && !Yii::$app->user->can(Utility::ROLE_MAIN_WORKER)) {
            $query->andWhere(['d.id' => (new Query())
                        ->select(['ud.department_id'])
                        ->from('user_department ud')
                        ->where(['ud.user_id' => Yii::$app->user->id])
                    ]);
        }
            
        $columnsMap = [
            'customerName' => 'c.name',
            'orderNumber' => 'o.number',
            'productCode' => 'op.symbol',
            'deadline' => 'oh.date_deadline',
            'completedCount' => '(MIN(pt.total_count_made) / oop.count)',
            'priority' => 'opr.priority',
        ];
        
        if (!empty($params['departmentId'])) {
            $query->andWhere(['d.id' => $params['departmentId']]);
        }
        
        if (!empty($params['orderId'])) {
            $query->andWhere(['o.id' => $params['orderId']]);
        }
        
        if (!empty($params['orderOfferedProductId'])) {
            $query->andWhere(['oop.id' => $params['orderOfferedProductId']]);
        }
            
        if(!empty($params['sortDir']) && !empty($columnsMap[$params['sortField']])) {
            $query->orderBy([
                $columnsMap[$params['sortField']] => (!empty($params['sortDir']) && $params['sortDir'] == 'desc') ? SORT_DESC : SORT_ASC
            ]);
        }
        else {
            $query->orderBy([$columnsMap['deadline'] => SORT_ASC]);
        }
            
        return $query;
    }

    public static function getProductsWithQFByDate($dateFrom, $dateTo){
        $query = (new Query())->select(['o.title as orderTitle',
            'o.id as orderId',
            'o.number as orderNumber',
            'pt.qa_factor_value as qa',
            'pt.total_count_made as count',
            'opt.name as productName'])
            ->from('order o')
            ->innerJoin('order_offered_product oop', 'o.id = oop.order_id')
            ->innerJoin('offered_product op', 'op.id = oop.offered_product_id')
            ->leftJoin('offered_product_translation opt', 'opt.offered_product_id = op.id')
            ->innerJoin('production_task pt', 'pt.order_offered_product_id = oop.id')
            ->leftJoin('language l', 'l.id = opt.language_id')
            ->where(['or', ['l.symbol' => Yii::$app->language], ['l.symbol' => null]])
            ->andWhere('pt.qa_factor_value IS NOT NULL')
            ->andWhere(['>=','DATE(pt.date_last_modification)', $dateFrom])
            ->andWhere(['<=','DATE(pt.date_last_modification)', $dateTo])
            ->orderBy('o.id');
        return $query->all();
    }
    
    public static function getDetailsForProductionDetails($orderOfferedProductId) {
        $query = (new Query())->select(['o.number as orderNumber', 'o.id as orderId', 'c.name as customerName',
                    'op.symbol as productCode', 'opt2.name as productName', 'op.id as productId', 'oh.date_deadline as deadline',
                    'pt.total_count_made as completedCount', 'opt.name as priorityName',
                    'd.id as departmentId', 'dt.name as departmentName', 'oop.count as requiredCount',
                    'oop.remarks', 'oop.remarks2',
                ])
                ->from('order o')
                ->innerJoin('order_history oh', 'oh.id = o.order_history_last_id')
                ->innerJoin('order_offered_product oop', 'o.id = oop.order_id')
                ->innerJoin('offered_product op', 'op.id = oop.offered_product_id')
                ->leftJoin('offered_product_translation opt2', 'opt2.offered_product_id = op.id')
                ->leftJoin('language l2', 'l2.id = opt2.language_id')
                ->innerJoin('production_task pt', 'pt.order_offered_product_id = oop.id')
                ->innerJoin('production_path_step pps', 'pt.production_path_step_id = pps.id')
                ->innerJoin('activity a', 'a.id = pps.activity_id')
                ->innerJoin('department_activity da', 'da.activity_id = a.id')
                ->innerJoin('department d', 'd.id = da.department_id')
                ->innerJoin('department_translation dt', 'dt.department_id = d.id')
                ->innerJoin('language l', 'l.id = dt.language_id')
                ->innerJoin('company c', 'c.id = o.company_id')
                ->leftJoin('order_priority_translation opt', 'opt.order_priority_id = o.order_priority_id')
                ->leftJoin('language l1', 'l1.id = opt.language_id')
                ->where(['l.symbol' => Yii::$app->language])
                ->andWhere(['or', ['l1.symbol' => Yii::$app->language], ['l1.symbol' => null]])
                ->andWhere(['oop.id' => intval($orderOfferedProductId)])
                ->andWhere('pt.task_successor_ids IS NULL')
                ->andWhere(['OR',
                    ['l2.symbol' => \Yii::$app->language],
                    'l2.symbol IS NULL'
                ]);
        
        if(!Yii::$app->user->can(Utility::ROLE_ADMIN) && !Yii::$app->user->can(Utility::ROLE_MAIN_WORKER)) {
            $query->andWhere(['d.id' => (new Query())
                    ->select(['ud.department_id'])
                    ->from('user_department ud')
                    ->where(['ud.user_id' => Yii::$app->user->id])
            ]);
        }
        
        return $query->one();
    } 
    
    public static function getTableForProductionDetails($orderOfferedProductId) {
        $query = (new Query())->select(['pt.id as productionTaskId', 'pt.total_count_made as completedCount',
                    'pt.total_count_failed as failedCount', 'oop.count requiredCount',
                    'at.name as activityName', 'pt.is_available as isTaskAvailable', 't.is_complete as isTaskComplete'])
                ->from('order o')
                ->innerJoin('order_offered_product oop', 'oop.order_id = o.id')
                ->innerJoin('production_task pt', 'pt.order_offered_product_id = oop.id')
                ->innerJoin('task t', 'pt.task_id = t.id')
                ->innerJoin('production_path_step pps', 'pt.production_path_step_id = pps.id')
                ->innerJoin('activity a', 'pps.activity_id = a.id')
                ->innerJoin('department_activity da', 'da.activity_id = a.id')
                ->innerJoin('department d', 'd.id = da.department_id')
                ->innerJoin('activity_translation at', 'a.id = at.activity_id')
                ->innerJoin('language l', 'at.language_id = l.id')
                ->where([
                    'oop.id' => intval($orderOfferedProductId),
                    'l.symbol' => Yii::$app->language,
                ])
                ->groupBy(['pt.id', 'pt.total_count_made', 'pt.total_count_failed', 'oop.count', 'at.name', 'pt.is_available']);
                
        if(!Yii::$app->user->can(Utility::ROLE_ADMIN) && !Yii::$app->user->can(Utility::ROLE_MAIN_WORKER)) {
            $query->andWhere(['d.id' => (new Query())
                    ->select(['ud.department_id'])
                    ->from('user_department ud')
                    ->where(['ud.user_id' => Yii::$app->user->id])
            ]);
        }
            
        return $query->all();
    }

    public static function getTasksWithProblemsByPeriod($dateFrom, $dateTo){    
        return (new Query())->select(['pt.total_count_made as completedCount','pt.total_count_failed as failedCount',
                    'op.symbol as productCode', 'o.number as orderNumber', 'c.name as companyName', 'at.name as activityName'
                ])
                ->from('production_task pt')
                ->innerJoin('order_offered_product oop', 'pt.order_offered_product_id = oop.id')
                ->innerJoin('order o', 'oop.order_id = o.id')
                ->innerJoin('company c', 'o.company_id = c.id')
                ->innerJoin('offered_product op', 'oop.offered_product_id = op.id')
                ->innerJoin('production_path_step pps', 'pt.production_path_step_id = pps.id')
                ->innerJoin('activity a', 'pps.activity_id = a.id')
                ->innerJoin('activity_translation at', 'a.id = at.activity_id')
                ->innerJoin('language l', 'at.language_id = l.id')
                ->where(['l.symbol' => Yii::$app->language])
                ->andWhere(['>=','DATE(pt.date_last_modification)', $dateFrom])
                ->andWhere(['<=','DATE(pt.date_last_modification)', $dateTo])
                ->andWhere(['a.is_node' => false])
                ->orderBy(['c.name' => SORT_ASC, 'o.number' => SORT_ASC, 'op.symbol' => SORT_ASC, 'pt.id' => SORT_ASC])
                ->all();
    }
}