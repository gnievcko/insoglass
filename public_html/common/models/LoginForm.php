<?php

namespace common\models;

use Yii;
use yii\base\Model;
use yii\db\Expression;
use common\models\ar\User;
use common\models\ar\UserAction;
use yii\db\yii\db;
use common\models\ar\UserRole;
use common\helpers\Utility;

/**
 * Login form
 */
class LoginForm extends Model {
	
    public $email;
    public $password;

    private $_user;

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
	            [['email', 'password'], 'required'],
	            ['email', 'validateIdentity'],
	            ['password', 'validatePassword'],
        ];
    }
    
    public function attributeLabels() {
    	return [
    			'email' => Yii::t('web', 'E-mail address'),
    			'password' => Yii::t('main', 'Password')
    	];
    }

    /**
     * Validates the password.
     * This method serves as the inline validation for password.
     *
     * @param string $attribute the attribute currently being validated
     * @param array $params the additional name-value pairs given in the rule
     */
    public function validatePassword($attribute, $params) {
        if(!$this->hasErrors()) {
            $user = $this->getUser();
            
            if(!$user || !$user->validatePassword($this->password)) {
                $this->addError($attribute, Yii::t('main', 'Incorrect email or password.'));
                return;
            }
            
            if(!$user->is_active) {
            	$this->addError($attribute, Yii::t('web', 'Your account is not verified yet.'));
            }
        }
    }
    
    public function validateIdentity($attribute, $params) {
    	if(!$this->hasErrors()) {
    		$validator = new \yii\validators\EmailValidator();
    		$error = 'Not valid';
    		if($validator->validate($this->email, $error)) {
    			return true;
    		}
    		
    		$this->addError($attribute, Yii::t('web', 'Incorrect e-mail.'));
    	}
    }

    /**
     * Logs in a user using the provided email and password.
     *
     * @return boolean whether the user is logged in successfully
     */
    public function login() {    	
        if($this->validate()) {
        	$user = $this->getUser();           
        	
        	$rolesData = UserRole::find()->where(['user_id' => $user->id])->all();
        	if(count($rolesData) == 1 && $rolesData[0]->role->symbol == Utility::ROLE_REPAIRER) {
        		Yii::$app->getSession()->setFlash('error', Yii::t('main', 'Incorrect email or password.'));
        		return false;
        	}
        	
        	$isClient = count($rolesData) == 1 && $rolesData[0]->role->symbol == Utility::ROLE_CLIENT;
        	if($isClient && (empty(Yii::$app->params['canLoggedContactPerson']) || empty(Yii::$app->params['customerURL']) ||
        			!$this->isServerNameInCustomerURLs($_SERVER['SERVER_NAME'], Yii::$app->params['customerURL']) || !$user->can_logged)) {
        		Yii::$app->getSession()->setFlash('error', Yii::t('main', 'Incorrect email or password.'));
        		return false;
        	}
        	
        	if(!empty($user)) {
        		if(empty($user->is_active)) {
        			$user->is_active = 1;
        			
        			Yii::$app->getSession()->setFlash('success', Yii::t('web', 'The process of your account activation ended successfully.'));
        		}
        		
        		$action = new UserAction();
        		$action->user_id = $user->id;
        		$action->date_action = new Expression('NOW()');
        		$action->ip_address = $_SERVER['REMOTE_ADDR'];
        		$action->save(false);

        		$user->save(false);
        	}
        	
            return Yii::$app->user->login($user, Yii::$app->params['identityDuration']);
        } 
        else {
            return false;
        }
    }

    /**
     * Finds user by [[email]]
     *
     * @return User|null
     */
    protected function getUser() {
        if($this->_user === null) {
        	$validator = new \yii\validators\EmailValidator();
        	$error = 'Not valid';
        	if($validator->validate($this->email, $error)) {
        		$this->_user = User::findByEmail($this->email);
        	}
        }

        return $this->_user;
    }
    
    private function isServerNameInCustomerURLs($serverName, $customerURLs) {
    	if(!is_array($customerURLs)) {
    		return strstr($serverName, $customerURLs);
    	}
    	
    	if(!empty($customerURLs)) {
    		foreach($customerURLs as $customerURL) {
    			if(strstr($serverName, $customerURL)) {
    				return true;
    			}
    		}
    	}
    		
    	return false;
    }
}
