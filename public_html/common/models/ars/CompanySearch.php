<?php

namespace common\models\ars;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\ar\Company;
use yii\data\SqlDataProvider;
use yii\data\ArrayDataProvider;

/**
 * CompanySearch represents the model behind the search form about `common\models\ar\Company`.
 */
class CompanySearch extends Company {

	public $parentCompanyName;
	public $addressName;
	public $addressPostalName;
	
    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            	[['id', 'parent_company_id', 'address_id', 'address_postal_id', 'language_id', 'currency_id', 'is_artificial', 'is_active'], 'integer'],
				[[
						'name', 'email', 'phone1', 'phone2', 'date_creation', 'date_modification', 'parentCompanyName', 
						'addressName', 'addressPostalName', 'fax1', 'vat_identification_number', 
						'taxpayer_identification_number', 'krs_number', 'note',
				], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios() {
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params) {
    	$this->load($params);
    	if(array_key_exists('CompanySearch', $params)) {
    		$params = $params['CompanySearch'];    		
    	}
    		
		$queryCore = 'FROM company c
				LEFT JOIN company pc ON c.parent_company_id = pc.id
				LEFT JOIN address a ON c.address_id = a.id
				LEFT JOIN city c1 ON a.city_id = c1.id
				LEFT JOIN province p1 ON c1.province_id = p1.id
				LEFT JOIN address ap ON c.address_postal_id = ap.id
				LEFT JOIN city c2 ON a.city_id = c2.id
				LEFT JOIN province p2 ON c2.province_id = p2.id
				WHERE LOWER(c.name) LIKE :name
				AND ('.(isset($params['email']) ? strlen($params['email']) : 0).' = 0 OR LOWER(c.email) LIKE :email)
				AND ('.(isset($params['phone1']) ? strlen($params['phone1']) : 0).' = 0 OR LOWER(c.phone1) LIKE :phone OR LOWER(c.phone2) LIKE :phone)
				AND ('.(isset($params['fax1']) ? strlen($params['fax1']) : 0).' = 0 OR LOWER(c.fax1) LIKE :fax)
				AND ('.(isset($params['vat_identification_number']) ? strlen($params['vat_identification_number']) : 0).' = 0 OR LOWER(c.vat_identification_number) LIKE :vatIdNumber)
				AND ('.(isset($params['taxpayer_identification_number']) ? strlen($params['taxpayer_identification_number']) : 0).' = 0 OR LOWER(c.taxpayer_identification_number) LIKE :taxpayerIdNumber)
				AND ('.(isset($params['note']) ? strlen($params['note']) : 0).' = 0 OR LOWER(c.note) LIKE :note)
				AND ('.(isset($params['krs_number']) ? strlen($params['krs_number']) : 0).' = 0 OR LOWER(c.krs_number) LIKE :krsNumber)
				AND LOWER(c.date_creation) LIKE :dateCreation
				AND LOWER(COALESCE(c.date_modification, "")) LIKE :dateModification
				AND (:id IS NULL OR c.id = :id)
				AND (:languageId IS NULL OR c.language_id = :languageId)
				AND (:currencyId IS NULL OR c.currency_id = :currencyId)
				AND (:isArtificial IS NULL OR c.is_artificial = :isArtificial)
				AND (:isActive IS NULL OR c.is_active = :isActive)
				AND (:parentCompanyNameRaw IS NULL OR LOWER(pc.name) LIKE :parentCompanyName)
				AND (
					:addressNameRaw IS NULL
					OR LOWER(a.main) LIKE :addressName
					OR LOWER(a.complement) LIKE :addressName
					OR LOWER(c1.name) LIKE :addressName
					OR LOWER(p1.name) LIKE :addressName
				)
				AND (
					:addressPostalNameRaw IS NULL
					OR LOWER(ap.main) LIKE :addressPostalName
					OR LOWER(ap.complement) LIKE :addressPostalName
					OR LOWER(c2.name) LIKE :addressPostalName
					OR LOWER(p2.name) LIKE :addressPostalName
				)';
		
		$isArtificial = isset($params['is_artificial']) && strlen($params['is_artificial']) > 0 ? $params['is_artificial'] : null;
		$isActive = isset($params['is_active']) && strlen($params['is_active']) > 0 ? $params['is_active'] : null;
		
		$paramsCore = [
				':name' => mb_strtolower('%'.(isset($params['name']) ? $params['name'] : "").'%'),
				':email' => mb_strtolower('%'.(isset($params['email']) ? $params['email'] : "").'%'),
				':phone' => mb_strtolower('%'.(isset($params['phone1']) ? $params['phone1'] : "").'%'),
				':fax' => mb_strtolower('%'.(isset($params['fax1']) ? $params['fax1'] : "").'%'),
				':vatIdNumber' => mb_strtolower('%'.(isset($params['vat_identification_number']) ? $params['vat_identification_number'] : "").'%'),
				':taxpayerIdNumber' => mb_strtolower('%'.(isset($params['taxpayer_identification_number']) ? $params['taxpayer_identification_number'] : "").'%'),
				':krsNumber' => mb_strtolower('%'.(isset($params['krs_number']) ? $params['krs_number'] : "").'%'),
				':dateCreation' => mb_strtolower('%'.(isset($params['date_creation']) ? $params['date_creation'] : "").'%'),
				':dateModification' => mb_strtolower('%'.(isset($params['date_modification']) ? $params['date_modification'] : "").'%'),
				':id' => isset($params['id']) && !empty($params['id']) ? $params['id'] : null,
				':languageId' => isset($params['language_id']) && !empty($params['language_id']) ? $params['language_id'] : null,
				':currencyId' => isset($params['currency_id']) && !empty($params['currency_id']) ? $params['currency_id'] : null,
				':isArtificial' => $isArtificial,
				':isActive' => $isActive,
				':parentCompanyNameRaw' => (isset($params['parentCompanyName']) && !empty($params['parentCompanyName']) ? $params['parentCompanyName'] : null),
				':addressNameRaw' => (isset($params['addressName']) && !empty($params['addressName']) ? $params['addressName'] : null),
				':addressPostalNameRaw' => (isset($params['addressPostalName']) && !empty($params['addressPostalName']) ? $params['addressPostalName'] : null),
				':parentCompanyName' => mb_strtolower('%'. (isset($params['parentCompanyName']) && !empty($params['parentCompanyName']) ? $params['parentCompanyName'] : null).'%'),
				':addressName' => mb_strtolower('%'.(isset($params['addressName']) && !empty($params['addressName']) ? $params['addressName'] : null).'%'),
				':addressPostalName' => mb_strtolower('%'.(isset($params['addressPostalName']) && !empty($params['addressPostalName']) ? $params['addressPostalName'] : null).'%'),
				':note' => mb_strtolower('%'.(isset($params['note']) && !empty($params['note']) ? $params['note'] : null).'%'),
		];
		
		$sqlCount = Yii::$app->db->createCommand('SELECT COUNT(c.id) '.$queryCore)
				->bindValues($paramsCore)
				->queryScalar();

		$sql = 'SELECT c.id, c.parent_company_id, c.name, c.email, c.phone1, c.phone2, 
				c.fax1, c.vat_identification_number, c.taxpayer_identification_number, c.krs_number,
				c.address_id, c.address_postal_id,
				c.language_id, c.currency_id, c.is_artificial, c.is_active, c.date_creation, c.date_modification
				 '.$queryCore;

		$sqlDataProvider = new SqlDataProvider([
            	'sql' => $sql,
				'params' => $paramsCore,
				'totalCount' => $sqlCount,
        		'sort' => [
        				'attributes' => [
        						'id',
        						'name',
        						'email',
        						'phone1',
        						'is_active',
        						'date_creation',
        				],
        				'defaultOrder' => ['id' => SORT_DESC]
        		],
        ]);

        return $sqlDataProvider;
    }
}
