<?php

namespace common\models\ars;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\ar\ProductStatusReference;
use common\models\ar\Language;

/**
 * ProductStatusReferenceSearch represents the model behind the search form about `common\models\ar\ProductStatusReference`.
 */
class ProductStatusReferenceSearch extends ProductStatusReference {

	public $productStatusSymbol;
    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            	[['id', 'product_status_id', 'is_required'], 'integer'],
				[['symbol', 'name_table', 'name_column', 'name_expression', 'url_details', 'productStatusSymbol', 'field_type'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios() {
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params) {
        $query = ProductStatusReference::find();
        
        $query->joinWith(['productStatus ps']);
        $language = Language::findOne(['symbol' => Yii::$app->language]);
        $query->join('LEFT OUTER JOIN', 'product_status_translation pst', 'ps.id = pst.product_status_id and pst.language_id = :l', [':l' => $language->id]);

        $dataProvider = new ActiveDataProvider([
        		'query' => $query,
        		'sort' => [
        				'attributes' => [
        						'id',
        						'symbol',
        						'is_required',
        						'name_table',
        						'name_column',
        						'name_expression',
        						'url_details',
        						'field_type',
        						'product_status_id',
        						'productStatusSymbol' => [
        								'asc' => ['COALESCE(pst.name, ps.symbol)' => SORT_ASC],
        								'desc' => ['COALESCE(pst.name, ps.symbol)' => SORT_DESC],
        						]
        				]
        		]
        ]);
        
        
        if(!$this->load($params) && !$this->validate()) {
        	return $dataProvider;
        }
        
       	$query->andFilterWhere(['like','LOWER(COALESCE(pst.name, ps.symbol))',mb_strtolower($this->productStatusSymbol, 'UTF-8')]);
       	$query->andFilterWhere(['like','LOWER(name_table)',mb_strtolower($this->name_table, 'UTF-8')]);
       	$query->andFilterWhere(['like','LOWER(name_column)',mb_strtolower($this->name_column, 'UTF-8')]);
       	$query->andFilterWhere(['like','LOWER(name_expression)',mb_strtolower($this->name_expression, 'UTF-8')]);
       	$query->andFilterWhere(['like','LOWER(url_details)',mb_strtolower($this->url_details, 'UTF-8')]);
       	$query->andFilterWhere(['like','LOWER(symbol)',mb_strtolower($this->symbol, 'UTF-8')]);
       	$query->andFilterWhere(['like','LOWER(symbol)',mb_strtolower($this->symbol, 'UTF-8')]);
       	$query->andFilterWhere(['is_required' => $this->is_required]);
       	$query->andFilterWhere(['product_status_id' => $this->product_status_id]);
       	$query->andFilterWhere(['field_type' => $this->field_type]);
        
        return $dataProvider;
    }
}
