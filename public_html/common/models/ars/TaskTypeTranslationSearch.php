<?php

namespace common\models\ars;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\ar\TaskTypeTranslation;

/**
 * TaskTypeTranslationSearch represents the model behind the search form about `common\models\ar\TaskTypeTranslation`.
 */
class TaskTypeTranslationSearch extends TaskTypeTranslation {

	public $taskTypeSymbol;
	public $languageName;
    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            	[['task_type_id', 'language_id'], 'integer'],
				[['name', 'taskTypeSymbol', 'languageName'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios() {
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params) {
        $query = TaskTypeTranslation::find();

        $dataProvider = new ActiveDataProvider([
            	'query' => $query,
        		'sort' => [
        				'attributes' => [
        						'name',
        						'taskTypeSymbol' => [
        								'asc' => ['task_type.symbol' => SORT_ASC],
        								'desc' => ['task_type.symbol' => SORT_DESC],
        						],
        						'languageName' => [
        								'asc' => ['language.name' => SORT_ASC],
        								'desc' => ['language.symbol' => SORT_DESC],
        						],
        				]
        		]
        ]);

        $this->load($params);

        if(!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->joinWith(['taskType' => function ($q) {
        	$q->where('LOWER(task_type.symbol) LIKE :taskTypeSymbol', [':taskTypeSymbol' => '%'.mb_strtolower($this->taskTypeSymbol, 'UTF-8').'%']);
        }])
        ->joinWith(['language' => function ($q) {
        	$q->where('LOWER(language.name) LIKE :languageName', [':languageName' => '%'.mb_strtolower($this->languageName, 'UTF-8').'%']);
        }])
        ->andFilterWhere(['like', 'task_type_translation.name', $this->name]);
        
        return $dataProvider;
    }
}
