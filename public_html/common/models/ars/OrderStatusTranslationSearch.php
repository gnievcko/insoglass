<?php

namespace common\models\ars;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\ar\OrderStatusTranslation;

/**
 * OrderStatusTranslationSearch represents the model behind the search form about `common\models\ar\OrderStatusTranslation`.
 */
class OrderStatusTranslationSearch extends OrderStatusTranslation {

	public $orderStatusSymbol;
	public $languageName;

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            	[['order_status_id', 'language_id'], 'integer'],
				[['name', 'name_client', 'orderStatusSymbol', 'languageName'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios() {
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params) {
        $query = OrderStatusTranslation::find();

        $dataProvider = new ActiveDataProvider([
            	'query' => $query,
        		'sort' => [
        				'attributes' => [
        						'name',
        						'name_client',
        						'orderStatusSymbol' => [
        								'asc' => ['order_status.symbol' => SORT_ASC],
        								'desc' => ['order_status.symbol' => SORT_DESC],
        						],
        						'languageName' => [
        								'asc' => ['language.name' => SORT_ASC],
        								'desc' => ['language.symbol' => SORT_DESC],
        						],
        				]
        		]
        ]);

        $this->load($params);

        if(!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->joinWith(['orderStatus' => function ($q) {
	            	$q->where('LOWER(order_status.symbol) LIKE :orderStatusSymbol', [':orderStatusSymbol' => '%'.mb_strtolower($this->orderStatusSymbol, 'UTF-8').'%']);
	            }])
	            ->joinWith(['language' => function ($q) {
	            	$q->where('LOWER(language.name) LIKE :languageName', [':languageName' => '%'.mb_strtolower($this->languageName, 'UTF-8').'%']);
	            }])
	            ->andFilterWhere(['like', 'order_status_translation.name', $this->name])
                ->andFilterWhere(['like', 'order_status_translation.name_client', $this->name_client]);

        return $dataProvider;
    }
}
