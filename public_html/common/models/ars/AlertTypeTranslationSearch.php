<?php

namespace common\models\ars;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\ar\AlertTypeTranslation;

/**
 * AlertTypeTranslationSearch represents the model behind the search form about `common\models\ar\AlertTypeTranslation`.
 */
class AlertTypeTranslationSearch extends AlertTypeTranslation {

	public $alertTypeSymbol;
	public $languageName;
	
    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            	[['alert_type_id', 'language_id'], 'integer'],
				[['name','alertTypeSymbol','languageName'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios() {
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params) {
        $query = AlertTypeTranslation::find();

        $dataProvider = new ActiveDataProvider([
            	'query' => $query,
        		'sort' => [
        				'attributes' => [
        						'name',
        						'alertTypeSymbol' => [
        								'asc' => ['alert_type.symbol' => SORT_ASC],
        								'desc' => ['alert_type.symbol' => SORT_DESC],
        						],
        						'languageName' => [
        								'asc' => ['language.name' => SORT_ASC],
        								'desc' => ['language.symbol' => SORT_DESC],
        						],
        				]
        		]
        ]);


        if(!$this->load($params) && !$this->validate()) {
            $query->joinWith(['alertType', 'language']);
            return $dataProvider;
        }
        
        $query->joinWith(['alertType' => function ($q) {
	    	$q->where('LOWER(alert_type.symbol) LIKE "%' . mb_strtolower($this->alertTypeSymbol, 'UTF-8') . '%"');
	    }])
	    ->joinWith(['language' => function ($q) {
	    	$q->where('LOWER(language.name) LIKE "%' . mb_strtolower($this->languageName, 'UTF-8') . '%"');
	    }])
	   	->andFilterWhere(['like', 'alert_type_translation.name', $this->name]);

        return $dataProvider;
    }
}
