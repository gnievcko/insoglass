<?php

namespace common\models\ars;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\ar\Province;

/**
 * ProvinceSearch represents the model behind the search form about `common\models\ar\Province`.
 */
class ProvinceSearch extends Province {

	public $countryName;
	
    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            	[['id', 'country_id'], 'integer'],
				[['symbol', 'name', 'countryName'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios() {
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params) {
        $query = Province::find();

        $dataProvider = new ActiveDataProvider([
            	'query' => $query,
        		'sort' => [
        				'attributes' => [
        						'id',
        						'symbol',
        						'name',
        						'countryName' => [
        								'asc' => ['country.symbol' => SORT_ASC],
        								'desc' => ['country.symbol' => SORT_DESC],
        						],
        				]
        		]
        ]);

        if(!$this->load($params) && !$this->validate()) {
            $query->joinWith('country');
            return $dataProvider;
        }
        
        $query->joinWith(['country' => function($q) {
        			if(!empty($this->countryName)) {
	        			$q->joinWith('countryTranslations');
	        			$q->where('LOWER(country_translation.name) LIKE "%'.mb_strtolower($this->countryName, 'UTF-8').'%" OR
	        					LOWER(country.symbol) LIKE "%'.mb_strtolower($this->countryName, 'UTF-8').'%"');
        			}
        		}])
        		->andFilterWhere(['like', 'province.symbol', $this->symbol])
        		->andFilterWhere(['like', 'province.name', $this->name])
        		->andFilterWhere(['province.id' => $this->id]);

        return $dataProvider;
    }
}
