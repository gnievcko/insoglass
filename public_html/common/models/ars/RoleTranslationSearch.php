<?php

namespace common\models\ars;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\ar\RoleTranslation;

/**
 * RoleTranslationSearch represents the model behind the search form about `common\models\ar\RoleTranslation`.
 */
class RoleTranslationSearch extends RoleTranslation {

	public $roleSymbol;
	public $languageName;

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            	[['role_id', 'language_id'], 'integer'],
				[['name', 'roleSymbol', 'languageName'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios() {
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params) {
        $query = RoleTranslation::find();

        $dataProvider = new ActiveDataProvider([
            	'query' => $query,
                'sort' => [
                    'attributes' => [
                            'roleSymbol' => [
                                'asc'  => ['role.symbol' => SORT_ASC],
                                'desc'  => ['role.symbol' => SORT_DESC],
                            ],
                            'languageName' => [
                                'asc'  => ['language.name' => SORT_ASC],
                                'desc'  => ['language.name' => SORT_DESC],
                            ],
                            'name',
                    ],
                ],
        ]);

        $this->load($params);

        if(!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            $query->joinWith(['role', 'language']);
            return $dataProvider;
        }


        $query->select(['role_id', 'language_id', 'role_translation.name'])
	            ->joinWith(['role' => function ($q) {
	            	$q->where('LOWER(role.symbol) LIKE "%' . mb_strtolower($this->roleSymbol, 'UTF-8') . '%"');
	            }])
	            ->joinWith(['language' => function ($q) {
	            	$q->where('LOWER(language.name) LIKE "%' . mb_strtolower($this->languageName, 'UTF-8') . '%"');
	            }])
	            ->andFilterWhere(['like', 'role_translation.name', $this->name]);


        return $dataProvider;
    }
}
