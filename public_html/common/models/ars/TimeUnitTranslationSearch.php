<?php

namespace common\models\ars;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\ar\TimeUnitTranslation;

/**
 * TimeUnitTranslationSearch represents the model behind the search form about `common\models\ar\TimeUnitTranslation`.
 */
class TimeUnitTranslationSearch extends TimeUnitTranslation {

	public $timeUnitSymbol;
	public $languageName;
    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            	[['time_unit_id', 'language_id', 'value'], 'integer'],
				[['name', 'timeUnitSymbol', 'languageName'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios() {
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params) {
        $query = TimeUnitTranslation::find();

        $dataProvider = new ActiveDataProvider([
            	'query' => $query,
        		'sort' => [
        				'attributes' => [
        						'name',
        						'value',
        						'timeUnitSymbol' => [
        								'asc' => ['time_unit.symbol' => SORT_ASC],
        								'desc' => ['time_unit.symbol' => SORT_DESC],
        						],
        						'languageName' => [
        								'asc' => ['language.name' => SORT_ASC],
        								'desc' => ['language.symbol' => SORT_DESC],
        						],
        				]
        		]
        ]);

        $this->load($params);

        if(!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->joinWith(['timeUnit' => function ($q) {
        	$q->where('LOWER(time_unit.symbol) LIKE :timeUnitSymbol', [':timeUnitSymbol' => '%'.mb_strtolower($this->timeUnitSymbol, 'UTF-8').'%']);
        }])
        ->joinWith(['language' => function ($q) {
        	$q->where('LOWER(language.name) LIKE :languageName', [':languageName' => '%'.mb_strtolower($this->languageName, 'UTF-8').'%']);
        }]);
        
        $query->andFilterWhere(['like', 'value', $this->value]);
		$query->andFilterWhere(['like', 'time_unit_translation.name', $this->name]);

        return $dataProvider;
    }
}
