<?php

namespace common\models\ars;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\ar\TaskTypeReference;
use yii\db\Query;

/**
 * TaskTypeReferenceSearch represents the model behind the search form about `common\models\ar\TaskTypeReference`.
 */
class TaskTypeReferenceSearch extends TaskTypeReference {

	public $taskTypeName;
    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            	[['id', 'task_type_id'], 'integer'],
				[['symbol', 'name_table', 'name_column', 'taskTypeName', 'name_expression', 'url_details', 'is_required' ], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios() {
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params) {
    	
    	$query = TaskTypeReference::find();
    	
    	$query->joinWith(['taskType tt'])
    			->joinWith(['languages l'])
    			->joinWith(['taskType.taskTypeTranslations ttt'])
    			->where(['l.symbol' => Yii::$app->language])
    			->addSelect(['task_type_reference.id as id', 'COALESCE(ttt.name,task_type_reference.symbol) as taskTypeName',
    						 'task_type_reference.task_type_id', 'task_type_reference.symbol as symbol',
    						'task_type_reference.name_table as name_table','task_type_reference.name_column as name_column',
    						'task_type_reference.name_expression as name_expression', 'task_type_reference.url_details as url_details',
    						'task_type_reference.is_required'
    			]);
   /* 	 

    	$query = (new Query())->select(['ttr.id as id', 'COALESCE(ttt.name, ttr.symbol) as taskTypeName', 'ttr.symbol as symbol', 'ttr.name_table as name_table', 'ttr.name_column as name_column'])
    			->from('task_type_reference ttr')
    			->join('INNER JOIN', 'task_type tt', 'ttr.task_type_id = tt.id')
    			->join('LEFT JOIN', 'task_type_translation ttt', 'tt.id = ttt.task_type_id')
    			->join('INNER JOIN', 'language l', 'ttt.language_id = l.id')
    			->where(['l.symbol' => Yii::$app->language]);
  */
        $dataProvider = new ActiveDataProvider([
            	'query' => $query,
        		'sort' => [
        				'attributes' => [
        						'id',
        						'taskTypeName',
        						'symbol',
        						'name_table',
        						'name_column',
        						'name_expression',
        						'url_details',
        						'is_required',
        				]
        		]
        		]);

        $this->load($params);

        if(!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere(['like', 'task_type_reference.id', $this->id])
        		->andFilterWhere(['like', 'COALESCE(ttt.name, tt.symbol)', $this->taskTypeName])
        		->andFilterWhere(['like', 'task_type_reference.symbol', $this->symbol])
        		->andFilterWhere(['like', 'name_table', $this->name_table])
        		->andFilterWhere(['like', 'name_column', $this->name_column])
        		->andFilterWhere(['like', 'name_expression', $this->name_expression])
        		->andFilterWhere(['like', 'url_details', $this->url_details])
        		->andFilterWhere(['=', 'is_required', $this->is_required]);
        
        return $dataProvider;
    }
}
