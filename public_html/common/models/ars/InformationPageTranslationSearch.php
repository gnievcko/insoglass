<?php

namespace common\models\ars;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\ar\InformationPageTranslation;

/**
 * InformationPageTranslationSearch represents the model behind the search form about `common\models\ar\InformationPageTranslation`.
 */
class InformationPageTranslationSearch extends InformationPageTranslation {

	
	public $informationPageSymbol;
	public $languageName;
	
    /**
     * @inheritdoc
     */
    public function rules() {
        return [
        		[['information_page_id', 'language_id', 'is_active'], 'integer'],
				[['title', 'content', 'keyword', 'description', 'informationPageSymbol', 'languageName'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios() {
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params) {
        $query = InformationPageTranslation::find();

   $dataProvider = new ActiveDataProvider([
   		'query' => $query,
  	]);
        
    $dataProvider->setSort([
    	'attributes' => [
    			'title',
    			'is_active',
    			'informationPageSymbol' => [
    					'asc' => ['information_page.symbol' => SORT_ASC],
     					'desc' => ['information_page.symbol' => SORT_DESC],
     					'label' => Yii::t('main', 'Information page')
     			],
     			'languageName' => [
     					'asc' => ['language.name' => SORT_ASC],
     					'desc' => ['language.name' => SORT_DESC],
      					'label' => Yii::t('main', 'Language')
      			],
      	]
      ]);

        $this->load($params);

        if(!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
        	$query->joinWith(['informationPage', 'language']);
            return $dataProvider;
        }
      
        $query->select(['information_page_id', 'language_id', 'information_page_translation.title', 'information_page_translation.is_active'])
        	->joinWith(['informationPage' => function ($q) {
        		$q->where('LOWER(information_page.symbol) LIKE "%' . mb_strtolower($this->informationPageSymbol, 'UTF-8') . '%"');
        }])
        ->joinWith(['language' => function ($q) {
        	$q->where('LOWER(language.name) LIKE "%' . mb_strtolower($this->languageName, 'UTF-8') . '%"');
        }])
        ->andFilterWhere(['like', 'title', $this->title])
        ->andFilterWhere([
        		'information_page_translation.is_active' => $this->is_active, 
        		'language_id' => $this->language_id,
        		'information_page_translation.is_active' => $this->is_active, 
        		'information_page_id' => $this->information_page_id,
        ]);
  
        return $dataProvider;
    }
}
