<?php

namespace common\models\ars;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\ar\CompanyGroup;

/**
 * CompanyGroupSearch represents the model behind the search form about `common\models\ar\CompanyGroup`.
 */
class CompanyGroupSearch extends CompanyGroup {

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            	[['id', 'is_predefined'], 'integer'],
				[['symbol'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios() {
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params) {
        $query = CompanyGroup::find();

        $dataProvider = new ActiveDataProvider([
            	'query' => $query,
        ]);

        $this->load($params);

        if(!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'is_predefined' => $this->is_predefined,
        ]);

		$query->andFilterWhere(['like', 'symbol', $this->symbol]);

        return $dataProvider;
    }
}
