<?php

namespace common\models\ars;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\ar\TaskTypeReferenceTranslation;

/**
 * TaskTypeReferenceTranslationSearch represents the model behind the search form about `common\models\ar\TaskTypeReferenceTranslation`.
 */
class TaskTypeReferenceTranslationSearch extends TaskTypeReferenceTranslation {

	public $referenceSymbol;
	public $languageName;
	
    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            	[['reference_id', 'language_id'], 'integer'],
				[['name', 'referenceSymbol', 'languageName'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios() {
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params) {
        $query = TaskTypeReferenceTranslation::find();

        $dataProvider = new ActiveDataProvider([
            	'query' => $query,
        		'sort' => [
        				'attributes' => [
        						'name',
        						'referenceSymbol' => [
        								'asc' => ['task_type_reference.symbol' => SORT_ASC],
        								'desc' => ['task_type_reference.symbol' => SORT_DESC],
        						],
        						'languageName' => [
        								'asc' => ['language.name' => SORT_ASC],
        								'desc' => ['language.symbol' => SORT_DESC],
        						],
        				]
    			]
        ]);

        $this->load($params);

        if(!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->joinWith(['reference' => function ($q) {
        	$q->where('LOWER(task_type_reference.symbol) LIKE :referenceSymbol', [':referenceSymbol' => '%'.mb_strtolower($this->referenceSymbol, 'UTF-8').'%']);
        }])
        ->joinWith(['language' => function ($q) {
        	$q->where('LOWER(language.name) LIKE :languageName', [':languageName' => '%'.mb_strtolower($this->languageName, 'UTF-8').'%']);
        }])
        ->andFilterWhere(['like', 'task_type_reference_translation.name', $this->name]);

        return $dataProvider;
    }
}
