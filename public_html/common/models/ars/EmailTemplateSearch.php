<?php

namespace common\models\ars;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\ar\EmailTemplate;

/**
 * EmailTemplateSearch represents the model behind the search form about `common\models\ar\EmailTemplate`.
 */
class EmailTemplateSearch extends EmailTemplate {

	public $emailTypeName;
	public $languageName;
	
    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            	[['email_type_id', 'language_id'], 'integer'],
				[['subject', 'content_html', 'content_text', 'emailTypeName', 'languageName'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios() {
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params) {
        $query = EmailTemplate::find();

        $dataProvider = new ActiveDataProvider([
            	'query' => $query,
        		'sort' => [
        				'attributes' => [
        						'email_type_id',
        						'emailTypeName' => [
        								'asc' => ['email_type.symbol' => SORT_ASC],
        								'desc' => ['email_type.symbol' => SORT_DESC],
        						],
        						'languageName' => [
        								'asc' => ['language.name' => SORT_ASC],
        								'desc' => ['language.name' => SORT_DESC],
        						],
        						'subject',
        				],
        				'defaultOrder' => ['email_type_id' => SORT_ASC]
        		],
        ]);

        $this->load($params);

        if(!$this->validate()) {
            $query->joinWith(['emailType', 'language']);
            return $dataProvider;
        }
        
        $query->joinWith(['emailType' => function($q) {
        			$q->where('LOWER(email_type.symbol) LIKE "%'.mb_strtolower($this->emailTypeName, 'UTF-8').'%"');
        		}])
        		->joinWith(['language' => function($q) {
        			$q->where('LOWER(language.name) LIKE "%'.mb_strtolower($this->languageName, 'UTF-8').'%"');
        		}])
        		->andFilterWhere(['like', 'subject', $this->subject])
            	->andFilterWhere(['like', 'content_html', $this->content_html])
            	->andFilterWhere(['like', 'content_text', $this->content_text])
        		->andFilterWhere([
        				'language_id' => $this->language_id	
        		]);

        return $dataProvider;
    }
}
