<?php

namespace common\models\ars;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\ar\UserCompanySalesman;
use common\models\ar\Language;

/**
 * UserCompanySalesmanSearch represents the model behind the search form about `common\models\ar\UserCompanySalesman`.
 */
class UserCompanySalesmanSearch extends UserCompanySalesman {

    public $companyGroupSymbol;
    public $userName;
    public $companyGroupId;
    
    
    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            	[['user_id', 'company_group_id'], 'integer'],
        		[['userName', 'companyGroupSymbol'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios() {
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params) {
        $query = UserCompanySalesman::find();
        
        $query->join('INNER JOIN', 'company_group_translation', 'company_group.id = company_group_translation.company_group_id')
        ->join('INNER JOIN', 'language', 'language.id = company_group_translation.language_id')
        ->andWhere(['company_group_translation.language_id' => Language::findOne(['symbol' => Yii::$app->language])->id]);

        $dataProvider = new ActiveDataProvider([
            	'query' => $query,
        ]);
        
        $dataProvider->setSort([
        		'attributes' => [
        				'userName' => [
        						'asc' => ['CONCAT(user.last_name, user.first_name)' => SORT_ASC],
        						'desc' => ['CONCAT(user.last_name, user.first_name)' => SORT_DESC],
        						'label' => Yii::t('main', 'User')
        				],
        				
        				'companyGroupSymbol' => [
        						'asc' => ['company_group_translation.name' => SORT_ASC],
        						'desc' => ['company_group_translation.name' => SORT_DESC],
        						'label' => Yii::t('main', 'Client group')
        				],
        		]
        ]);
        
        $this->load($params);

        $query->joinWith(['user', 'companyGroup']);
        
		if(!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
        	
            return $dataProvider;
        }

        $query->andFilterWhere([
            'user_id' => $this->user_id,
            'company_group_id' => $this->company_group_id,
        ])
        ->andFilterWhere(['like', 'CONCAT(user.first_name, user.last_name)', $this->userName])
        ->andFilterWhere(['like', 'company_group_translation.name', $this->companyGroupSymbol]);

        return $dataProvider;
    }
}
