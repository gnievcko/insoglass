<?php

namespace common\models\ars;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\ar\FileTemporaryStorage;

/**
 * FileTemporaryStorageSearch represents the model behind the search form about `common\models\ar\FileTemporaryStorage`.
 */
class FileTemporaryStorageSearch extends FileTemporaryStorage {

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            	[['id'], 'integer'],
		[['file_hash', 'date_creation', 'original_file_name'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios() {
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params) {
        $query = FileTemporaryStorage::find();

        $dataProvider = new ActiveDataProvider([
            	'query' => $query,
        ]);

        $this->load($params);

        if(!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
        ]);

	$query->andFilterWhere(['like', 'file_hash', $this->file_hash])
              ->andFilterWhere(['like', 'original_file_name', $this->original_file_name])
              ->andFilterWhere(['like', 'date_creation', $this->date_creation]);

        return $dataProvider;
    }
}
