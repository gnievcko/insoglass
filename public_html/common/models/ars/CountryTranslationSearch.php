<?php

namespace common\models\ars;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\ar\CountryTranslation;

/**
 * CountryTranslationSearch represents the model behind the search form about `common\models\ar\CountryTranslation`.
 */
class CountryTranslationSearch extends CountryTranslation {

	public $countrySymbol;
	public $languageName;
	
    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            	[['country_id', 'language_id'], 'integer'],
				[['name', 'countrySymbol', 'languageName'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios() {
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params) {
        $query = CountryTranslation::find();

        $dataProvider = new ActiveDataProvider([
            	'query' => $query,
        		'sort' => [
        				'attributes' => [
        						'name',
        						'countrySymbol' => [
        								'asc' => ['country.symbol' => SORT_ASC],
        								'desc' => ['country.symbol' => SORT_DESC],
        						],
        						'languageName' => [
        								'asc' => ['language.name' => SORT_ASC],
        								'desc' => ['language.name' => SORT_DESC],
        						],
        				],
        		],
        ]);

        if(!$this->load($params) && !$this->validate()) {
            $query->joinWith(['country', 'language']);
            return $dataProvider;
        }
        
        $query->joinWith(['country' => function ($q) {
	    	$q->where('LOWER(country.symbol) LIKE "%'.mb_strtolower($this->countrySymbol, 'UTF-8').'%"');
	    }])
	    ->joinWith(['language' => function ($q) {
	    	$q->where('LOWER(language.name) LIKE "%'.mb_strtolower($this->languageName, 'UTF-8').'%"');
	    }])
	   	->andFilterWhere(['like', 'country_translation.name', $this->name]);

        return $dataProvider;
    }
}
