<?php

namespace common\models\ars;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\ar\ContractTypeTranslation;

/**
 * ContractTypeTranslationSearch represents the model behind the search form about `common\models\ar\ContractTypeTranslation`.
 */
class ContractTypeTranslationSearch extends ContractTypeTranslation {

    public $contractTypeSymbol;
    public $languageName;
    
    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            	[['language_id', 'contract_type_id'], 'integer'],
		[['name', 'contractTypeSymbol', 'languageName'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios() {
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params) {
        $query = ContractTypeTranslation::find();

        $dataProvider = new ActiveDataProvider([
            	'query' => $query,
                'sort' => [
        				'attributes' => [
        						'name',
        						'contractTypeSymbol' => [
        								'asc' => ['contract_type.symbol' => SORT_ASC],
        								'desc' => ['contract_type.symbol' => SORT_DESC],
        						],
        						'languageName' => [
        								'asc' => ['language.name' => SORT_ASC],
        								'desc' => ['language.name' => SORT_DESC],
        						],
        				]
        		]
        ]);

        if(!$this->load($params) && !$this->validate()) {
            $query->joinWith(['contractType', 'language']);
            return $dataProvider;
        }

        $query->joinWith(['contractType' => function ($q) {
	    	$q->where('LOWER(contract_type.symbol) LIKE "%' . mb_strtolower($this->contractTypeSymbol, 'UTF-8') . '%"');
	    }])
	    ->joinWith(['language' => function ($q) {
	    	$q->where('LOWER(language.name) LIKE "%' . mb_strtolower($this->languageName, 'UTF-8') . '%"');
	    }])
	   	->andFilterWhere(['like', 'contract_type_translation.name', $this->name]);

        return $dataProvider;
    }
}
