<?php

namespace common\models\ars;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\ar\DepartmentType;

/**
 * DepartmentTypeSearch represents the model behind the search form about `common\models\ar\DepartmentType`.
 */
class   DepartmentTypeSearch extends DepartmentType {

    /**
     * @inheritdoc
     */

    public function rules() {
        return [
            	[['id'], 'integer'],
                [['id'], 'safe'],
				[['symbol'], 'safe'],
                [['name'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios() {
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = DepartmentType::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => [
                'attributes' => [
                    'id',
                    'symbol',
                ],
            ]
        ]);

        $this->load($params);

        if(!$this->validate()) {
            return $dataProvider;
        }

        if(!$this->load($params) && !$this->validate()) {
            $query->joinWith(['departmentTypeTranslations']);
            return $dataProvider;
        }

        if(empty($params['sort'])){
            $query->OrderBy(['department_type.id' => SORT_DESC]);
        }

        $query->andFilterWhere(['like', 'department_type.symbol', $this->symbol]);
        $query->andFilterWhere(['like', 'department_type.id', $this->id]);
        return $dataProvider;
    }
}