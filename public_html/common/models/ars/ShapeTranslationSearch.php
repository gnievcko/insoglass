<?php

namespace common\models\ars;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\ar\ShapeTranslation;

/**
 * ShapeTranslationSearch represents the model behind the search form about `common\models\ar\ShapeTranslation`.
 */
class ShapeTranslationSearch extends ShapeTranslation {

    public $shapeSymbol;
    public $languageName;

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            [['shape_id', 'language_id'], 'integer'],
			[['name', 'shapeSymbol', 'languageName'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios() {
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params) {
        $query = ShapeTranslation::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
                'sort' => [
                        'attributes' => [
                                'name',
                                'shapeSymbol' => [
                                        'asc' => ['shape_id' => SORT_ASC],
                                        'desc' => ['shape_id' => SORT_DESC],
                                ],
                                'languageName' => [
                                        'asc' => ['language_id' => SORT_ASC],
                                        'desc' => ['language_id' => SORT_DESC],
                                ],
                        ],
                ],
        ]);

        if(!$this->load($params) && !$this->validate()) {
            $query->joinWith(['shape', 'language']);
            return $dataProvider;
        }

        $query->andFilterWhere([
            'shape_id' => $this->shape_id,
            'language_id' => $this->language_id,
        ]);

        $query->joinWith(['shape' => function ($q) {
            $q->where('LOWER(shape.symbol) LIKE "%'.mb_strtolower($this->shapeSymbol, 'UTF-8').'%"');
        }])
        ->joinWith(['language' => function ($q) {
            $q->where('LOWER(language.name) LIKE "%'.mb_strtolower($this->languageName, 'UTF-8').'%"');
        }])
		->andFilterWhere(['like', 'shape_translation.name', $this->name]);

        return $dataProvider;
    }
}
