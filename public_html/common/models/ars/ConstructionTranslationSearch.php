<?php

namespace common\models\ars;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\ar\ConstructionTranslation;

/**
 * ConstructionTranslationSearch represents the model behind the search form about `common\models\ar\ConstructionTranslation`.
 */
class ConstructionTranslationSearch extends ConstructionTranslation {

    public $constructionSymbol;
    public $languageName;

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            [['construction_id', 'language_id'], 'integer'],
			[['name', 'constructionSymbol', 'languageName'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios() {
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params) {
        $query = ConstructionTranslation::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
                'sort' => [
                        'attributes' => [
                                'name',
                                'constructionSymbol' => [
                                        'asc' => ['construction_id' => SORT_ASC],
                                        'desc' => ['construction_id' => SORT_DESC],
                                ],
                                'languageName' => [
                                        'asc' => ['language_id' => SORT_ASC],
                                        'desc' => ['language_id' => SORT_DESC],
                                ],
                        ],
                ],
        ]);

        if(!$this->load($params) && !$this->validate()) {
            $query->joinWith(['construction', 'language']);
            return $dataProvider;
        }

        $query->andFilterWhere([
            'construction_id' => $this->construction_id,
            'language_id' => $this->language_id,
        ]);

        $query->joinWith(['construction' => function ($q) {
            $q->where('LOWER(construction.symbol) LIKE "%'.mb_strtolower($this->constructionSymbol, 'UTF-8').'%"');
        }])
        ->joinWith(['language' => function ($q) {
            $q->where('LOWER(language.name) LIKE "%'.mb_strtolower($this->languageName, 'UTF-8').'%"');
        }])
		->andFilterWhere(['like', 'construction_translation.name', $this->name]);

        return $dataProvider;
    }
}
