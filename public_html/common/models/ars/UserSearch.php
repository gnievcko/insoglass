<?php

namespace common\models\ars;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\ar\User;
use yii\db\Query;

/**
 * UserSearch represents the model behind the search form about `common\models\ar\User`.
 */
class UserSearch extends User {

    public $phone;
    public $fullName;
    public $lastActionTime;

    public function attributeLabels() {
        return parent::attributeLabels() + ['phone' => \Yii::t('main', 'Phone')];
    }

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            	[['id', 'company_id', 'language_id', 'currency_id', 'is_active'], 'integer'],
				[['email', 'password', 'first_name', 'last_name', 'phone', 'phone1', 'url_photo', 'note', 'date_creation', 'date_modification', 'fullName'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios() {
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params) {

    	$query = User::find()->employee();

        $dataProvider = new ActiveDataProvider([
            	'query' => $query,
				'sort' => [
						'attributes' => [
								'id',
                                'fullName' => [
                                    'asc'  => ['last_name' => SORT_ASC],
                                    'desc'  => ['last_name' => SORT_DESC],
                                ],
								'email',
								'phone1',
								'is_active',
								'date_creation',
						],
						'defaultOrder' => [ 'id' => SORT_DESC ]
				],
        ]);

        $this->load($params);

        if(!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'company_id' => $this->company_id,
            'language_id' => $this->language_id,
            'currency_id' => $this->currency_id,
            'is_active' => $this->is_active,
        ]);
        

		$query->andFilterWhere(['like', 'email', $this->email])
            ->andFilterWhere(['like', 'first_name', $this->first_name])
            ->andFilterWhere(['like', 'last_name', $this->last_name])
            ->andFilterWhere(['or', 
                ['like', 'phone1', $this->phone],
                ['like', 'phone2', $this->phone],
            ])
            ->andFilterWhere(['or', 
                ['like', 'first_name', $this->fullName],
                ['like', 'last_name', $this->fullName],
            ])
            ->andFilterWhere(['like', 'date_creation', $this->date_creation])
            ->andFilterWhere(['like', 'phone1', $this->phone1])
            ->andFilterWhere(['like', 'date_modification', $this->date_modification])
            ->andFilterWhere(['like', 'note', $this->note]);

        return $dataProvider;
    }
}
