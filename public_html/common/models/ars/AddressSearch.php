<?php

namespace common\models\ars;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\ar\Address;

/**
 * AddressSearch represents the model behind the search form about `common\models\ar\Address`.
 */
class AddressSearch extends Address {

	public $cityName;
	
    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            	[['id', 'city_id'], 'integer'],
				[['main', 'complement', 'cityName'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios() {
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params) {
        $query = Address::find();
		
        $dataProvider = new ActiveDataProvider([
           		'query' => $query,
           		'sort' => [
           				'attributes' => [
           						'id',
           						'main',
           						'complement',
           						'cityName' => [
           								'asc' => ['city.name' => SORT_ASC],
           								'desc' => ['city.name' => SORT_DESC],
           						],
           				],
           				'defaultOrder' => ['id' => SORT_DESC]
           		]
        ]);
            
        if(!$this->load($params) && !$this->validate()) {
          	$query->joinWith('city');
           	return $dataProvider;
        }
            
        $query->joinWith(['city' => function($q) {
        	    	$q->where('LOWER(city.name) LIKE "%'.mb_strtolower($this->cityName, 'UTF-8').'%" OR
        	    			LOWER(city.zip_code) LIKE "%'.mb_strtolower($this->cityName, 'UTF-8').'%" ');
            	}])
            	->andFilterWhere(['like', 'address.main', $this->main])
            	->andFilterWhere(['like', 'address.complement', $this->complement])
            	->andFilterWhere(['address.id' => $this->id]);

        return $dataProvider;
    }
}
