<?php

namespace common\models\ars;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\ar\MuntinTranslation;

/**
 * MuntinTranslationSearch represents the model behind the search form about `common\models\ar\MuntinTranslation`.
 */
class MuntinTranslationSearch extends MuntinTranslation {

    public $muntinSymbol;
    public $languageName;

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            [['muntin_id', 'language_id'], 'integer'],
			[['name', 'muntinSymbol', 'languageName'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios() {
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params) {
        $query = MuntinTranslation::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
                'sort' => [
                        'attributes' => [
                                'name',
                                'muntinSymbol' => [
                                        'asc' => ['muntin_id' => SORT_ASC],
                                        'desc' => ['muntin_id' => SORT_DESC],
                                ],
                                'languageName' => [
                                        'asc' => ['language_id' => SORT_ASC],
                                        'desc' => ['language_id' => SORT_DESC],
                                ],
                        ],
                ],
        ]);

        if(!$this->load($params) && !$this->validate()) {
            $query->joinWith(['muntin', 'language']);
            return $dataProvider;
        }

        $query->andFilterWhere([
            'muntin_id' => $this->muntin_id,
            'language_id' => $this->language_id,
        ]);

        $query->joinWith(['muntin' => function ($q) {
            $q->where('LOWER(muntin.symbol) LIKE "%'.mb_strtolower($this->muntinSymbol, 'UTF-8').'%"');
        }])
        ->joinWith(['language' => function ($q) {
            $q->where('LOWER(language.name) LIKE "%'.mb_strtolower($this->languageName, 'UTF-8').'%"');
        }])
		->andFilterWhere(['like', 'muntin_translation.name', $this->name]);

        return $dataProvider;
    }
}
