<?php

namespace common\models\ars;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\ar\DepartmentTypeTranslation;

/**
 * DepartmentTypeTranslationSearch represents the model behind the search form about `common\models\ar\DepartmentTypeTranslation`.
 */
class DepartmentTypeTranslationSearch extends DepartmentTypeTranslation {

    /**
     * @inheritdoc
     */
    public $languageName;
    public $languageSymbol;
    public $departmentTypeSymbol;


    public function rules() {
        return [
            	[['department_type_id', 'language_id'], 'integer'],
				[['name'], 'safe'],
                [['languageName','departmentTypeSymbol'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios() {
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params) {
        $query = DepartmentTypeTranslation::find();
        $dataProvider = new ActiveDataProvider([
            	'query' => $query,
                'sort' => [
                    'attributes' => [
                        'languageName' => [
                            'asc' => ['language.name' => SORT_ASC],
                            'desc' => ['language.name' => SORT_DESC],
                        ],
                        'departmentTypeSymbol'=> [
                            'asc' => ['department_type.symbol' => SORT_ASC],
                            'desc' => ['department_type.symbol' => SORT_DESC],
                        ],
                        'name',
                        'department_type_id',
                    ]
                ]
        ]);


        if(!$this->load($params) && !$this->validate()) {
            $query->joinWith(['departmentType', 'language']);
            $query->addSelect(['UPPER(language.symbol) as language.symbol']);
            return $dataProvider;
        }

        $query->joinWith(['language' => function ($q) {
                $q->where('LOWER(language.name) LIKE "%' . mb_strtolower($this->languageName, 'UTF-8') . '%"');
            }])
            ->joinWith(['departmentType' => function ($q) {
                $q->where('LOWER(department_type.symbol) LIKE "%' . mb_strtolower($this->departmentTypeSymbol, 'UTF-8') . '%"');
            }])
            ->andFilterWhere(['like', 'department_type_translation.name', $this->name])
            ->andFilterWhere(['like', 'department_type_translation.department_type_id', $this->department_type_id]);


        return $dataProvider;
    }
}
