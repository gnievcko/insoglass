<?php

namespace common\models\ars;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\ar\UserOrderSalesmanHistory;

/**
 * UserOrderSalesmanHistorySearch represents the model behind the search form about `common\models\ar\UserOrderSalesmanHistory`.
 */
class UserOrderSalesmanHistorySearch extends UserOrderSalesmanHistory {

	public $userName;
    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            	[['id', 'user_id', 'history_id', 'is_added'], 'integer'],
				[['date', 'userName'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios() {
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params) {
        $query = UserOrderSalesmanHistory::find();

        $dataProvider = new ActiveDataProvider([
            	'query' => $query,
        ]);
        
        $dataProvider->setSort([
        		'attributes' => [
        				'id', 
        				'userName' => [
        						'asc'  => ['COALESCE(user.first_name, user.last_name, user.email)' => SORT_ASC],
        						'desc'  => ['COALESCE(user.first_name, user.last_name, user.email)' => SORT_DESC],
        				],
        				'date',
        				'history_id',
        				'is_added',
        		]
        ]);

        $this->load($params);

        if(!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }
        
        $query->joinWith(['user' => function ($q) {
        	$q->where('LOWER(user.first_name) LIKE :userName OR
		        			LOWER(user.last_name) LIKE :userName OR
		        			LOWER(user.email) LIKE :userName', [':userName' => '%'.mb_strtolower($this->userName, 'UTF-8').'%']);
        }]);

        
        $query->andFilterWhere(['like','id', $this->id]);
        $query->andFilterWhere(['like','date', $this->date]);
        $query->andFilterWhere(['like','history_id', $this->history_id]);
        $query->andFilterWhere(['like','is_added', $this->is_added]);

        return $dataProvider;
    }
}
