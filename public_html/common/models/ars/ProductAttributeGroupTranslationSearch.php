<?php

namespace common\models\ars;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\ar\ProductAttributeGroupTranslation;

/**
 * ProductAttributeGroupTranslationSearch represents the model behind the search form about `common\models\ar\ProductAttributeGroupTranslation`.
 */
class ProductAttributeGroupTranslationSearch extends ProductAttributeGroupTranslation {
    
    public $symbol;
    public $languageName;
    
    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            [['product_attribute_group_id', 'language_id'], 'integer'],
				[['name', 'symbol', 'languageName'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios() {
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params) {
        $query = ProductAttributeGroupTranslation::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => [
                'attributes' => [
                    'name',
                    'symbol' => [
                        'asc' => ['product_attribute_group.symbol' => SORT_ASC],
                        'desc' => ['product_attribute_group.symbol' => SORT_DESC],
                    ],
                    'languageName' =>[
                        'asc' => ['language.name' => SORT_ASC],
                        'desc' => ['language.name' => SORT_DESC],
                    ],
                ],
            ],
        ]);

        if(!$this->load($params) && !$this->validate()) {
            $query->joinWith(['productAttributeGroup', 'language']);
            return $dataProvider;
        }

        $query->joinWith(['productAttributeGroup' => function($q) {
            $q->where('LOWER(product_attribute_group.symbol) LIKE "%'.mb_strtolower($this->symbol, 'UTF-8').'%"');
        }])
        ->joinWith(['language' => function($q) {
            $q->where('LOWER(language.name) LIKE "%'.mb_strtolower($this->languageName, 'UTF-8').'%"');
        }])
        ->andFilterWhere([
            'like', 'product_attribute_group_translation.name', $this->name
        ]);

        return $dataProvider;
    }
}
