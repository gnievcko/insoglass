<?php

namespace common\models\ars;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\ar\EmailConfig;

/**
 * EmailConfigSearch represents the model behind the search form about `common\models\ar\EmailConfig`.
 */
class EmailConfigSearch extends EmailConfig {

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            	[['id', 'port', 'starttls', 'smtp_auth'], 'integer'],
				[['email', 'password', 'protocol', 'host', 'noreply_email'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios() {
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params) {
        $query = EmailConfig::find();

        $dataProvider = new ActiveDataProvider([
            	'query' => $query,
        ]);

        $this->load($params);

        if(!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'starttls' => $this->starttls,
            'smtp_auth' => $this->smtp_auth,
        ]);

		$query->andFilterWhere(['like', 'email', $this->email])
            ->andFilterWhere(['like', 'password', $this->password])
            ->andFilterWhere(['like', 'port', $this->port])
            ->andFilterWhere(['like', 'protocol', $this->protocol])
            ->andFilterWhere(['like', 'host', $this->host])
            ->andFilterWhere(['like', 'noreply_email', $this->noreply_email]);

        return $dataProvider;
    }
}
