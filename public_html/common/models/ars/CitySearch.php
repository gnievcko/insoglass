<?php

namespace common\models\ars;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\ar\City;

/**
 * CitySearch represents the model behind the search form about `common\models\ar\City`.
 */
class CitySearch extends City {

	public $provinceName;
	
    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            	[['id', 'province_id'], 'integer'],
				[['name', 'zip_code', 'provinceName'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios() {
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params) {
        $query = City::find();

        $dataProvider = new ActiveDataProvider([
            	'query' => $query,
        		'sort' => [
        				'attributes' => [
        						'id',
        						'zip_code',
        						'name',
        						'provinceName' => [
        								'asc' => ['province.name' => SORT_ASC],
        								'desc' => ['province.name' => SORT_DESC],
        						],
        				],
        				'defaultOrder' => ['id' => SORT_DESC]
        		]
        ]);

        if(!$this->load($params) && !$this->validate()) {
            $query->joinWith('province');
            return $dataProvider;
        }
        
        $query->joinWith(['province' => function($q) {
		        	if(!empty($this->provinceName)) {
		        		$q->where('LOWER(province.name) LIKE "%'.mb_strtolower($this->provinceName, 'UTF-8').'%"');
		        	}
		        }])
		        ->andFilterWhere(['like', 'city.zip_code', $this->zip_code])
		        ->andFilterWhere(['like', 'city.name', $this->name])
		        ->andFilterWhere(['city.id' => $this->id]);

        return $dataProvider;
    }
}
