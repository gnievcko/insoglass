<?php

namespace common\models\ars;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\ar\Alert;
use yii\db\Query;

/**
 * AlertSearch represents the model behind the search form about `common\models\ar\Alert`.
 */
class AlertSearch extends Alert {

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            [['id', 'alert_type_id', 'user_id', 'is_active', 'order_id', 'product_id', 'company_id', 'task_id', 'warehouse_id'], 'integer'],
				[['message', 'date_creation'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios() {
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params) {
        $query = Alert::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if(!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'is_active' => $this->is_active
        ]);

		$query->andFilterWhere(['like', 'date_creation', $this->date_creation])
            ->andFilterWhere(['like', 'message', $this->message])
            ->andFilterWhere(['like', 'alert_type_id', $this->alert_type_id])
            ->andFilterWhere(['like', 'user_id', $this->user_id])
            ->andFilterWhere(['like', 'order_id', $this->order_id])
            ->andFilterWhere(['like', 'product_id', $this->product_id])
            ->andFilterWhere(['like', 'company_id', $this->company_id])
            ->andFilterWhere(['like', 'task_id', $this->task_id])
            ->andFilterWhere(['like', 'warehouse_id', $this->warehouse_id]);

        return $dataProvider;
    }
}
