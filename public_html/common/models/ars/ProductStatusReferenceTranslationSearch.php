<?php

namespace common\models\ars;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\ar\ProductStatusReferenceTranslation;

/**
 * ProductStatusReferenceTranslationSearch represents the model behind the search form about `common\models\ar\ProductStatusReferenceTranslation`.
 */
class ProductStatusReferenceTranslationSearch extends ProductStatusReferenceTranslation {
	
	public $referenceSymbol;
	public $languageName;
    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            	[['reference_id', 'language_id'], 'integer'],
				[['name','referenceSymbol','languageName'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios() {
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params) {
        $query = ProductStatusReferenceTranslation::find();

        $dataProvider = new ActiveDataProvider([
            	'query' => $query,
        		'sort' => [
        				'attributes' => [
        						'name',
        						'referenceSymbol' => [
        								'asc' => ['product_status_reference.symbol' => SORT_ASC],
        								'desc' => ['product_status_reference.symbol' => SORT_DESC],
        						],
        						'languageName' => [
        								'asc' => ['language.name' => SORT_ASC],
        								'desc' => ['language.symbol' => SORT_DESC],
        						],
        				]
        		]
        ]);


        if(!$this->load($params) && !$this->validate()) {
            $query->joinWith(['product_status_reference', 'language']);
            return $dataProvider;
        }
        
        $query->joinWith(['reference' => function ($q) {
	    	$q->where('LOWER(product_status_reference.symbol) LIKE "%' . mb_strtolower($this->referenceSymbol, 'UTF-8') . '%"');
	    }])
	    ->joinWith(['language' => function ($q) {
	    	$q->where('LOWER(language.name) LIKE "%' . mb_strtolower($this->languageName, 'UTF-8') . '%"');
	    }])
	   	->andFilterWhere(['like', 'product_status_reference_translation.name', $this->name]);

        return $dataProvider;
    }
}
