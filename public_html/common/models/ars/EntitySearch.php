<?php

namespace common\models\ars;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\ar\Entity;

/**
 * EntitySearch represents the model behind the search form about `common\models\ar\Entity`.
 */
class EntitySearch extends Entity {

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            	[['id', 'address_id', 'is_active'], 'integer'],
				[['name', 'taxpayer_identification_number', 'vat_identification_number', 'krs_number', 'email', 'phone', 'fax', 'account_number', 'bank_name', 'website_address'], 'safe'],
				[['share_capital'], 'number'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios() {
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params) {
        $query = Entity::find();

        $dataProvider = new ActiveDataProvider([
            	'query' => $query,
        ]);

        $this->load($params);

        if(!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'address_id' => $this->address_id,
            'share_capital' => $this->share_capital,
            'is_active' => $this->is_active,
        ]);

		$query->andFilterWhere(['like', 'name', $this->name])
            ->andFilterWhere(['like', 'taxpayer_identification_number', $this->taxpayer_identification_number])
            ->andFilterWhere(['like', 'vat_identification_number', $this->vat_identification_number])
            ->andFilterWhere(['like', 'krs_number', $this->krs_number])
            ->andFilterWhere(['like', 'email', $this->email])
            ->andFilterWhere(['like', 'phone', $this->phone])
            ->andFilterWhere(['like', 'fax', $this->fax])
            ->andFilterWhere(['like', 'account_number', $this->account_number])
            ->andFilterWhere(['like', 'bank_name', $this->bank_name])
            ->andFilterWhere(['like', 'website_address', $this->website_address]);

        return $dataProvider;
    }
}
