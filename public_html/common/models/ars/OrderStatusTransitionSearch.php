<?php

namespace common\models\ars;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\ar\OrderStatusTransition;

/**
 * OrderStatusTransitionSearch represents the model behind the search form about `common\models\ar\OrderStatusTransition`.
 */
class OrderStatusTransitionSearch extends OrderStatusTransition {

    public $orderStatusPredecessorTranslation;
    public $orderStatusSuccessorTranslation;

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            	[['order_status_predecessor_id', 'order_status_successor_id'], 'integer'],
            	[['orderStatusSuccessorTranslation', 'orderStatusPredecessorTranslation'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios() {
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params) {
        $query = OrderStatusTransition::find();

        $dataProvider = new ActiveDataProvider([
            	'query' => $query,
        		'sort' => [
        				'attributes' => [
                                'orderStatusPredecessorTranslation' => [
        								'asc' => ['predTranslations.name' => SORT_ASC, 'pred.symbol' => SORT_ASC],
        								'desc' => ['predTranslations.name' => SORT_DESC, 'pred.symbol' => SORT_DESC],
        						],
        						'orderStatusSuccessorTranslation' => [
        								'asc' => ['succTranslations.name' => SORT_ASC, 'succ.symbol' => SORT_ASC],
        								'desc' => ['succTranslations.name' => SORT_DESC, 'succ.symbol' => SORT_DESC],
        						],
        				]
        		]
        ]);

        $this->load($params);

        $query->joinWith(['orderStatusSuccessor' => function ($q) {
                    $q->alias('succ')
                        ->joinWith(['orderStatusTranslations' => function ($q) {
                            $q->alias('succTranslations')->joinWith(['language' => function ($q) {
                                $q->alias('succLanguage');
                            }]);
                        }])
                        ->where('LOWER(succ.symbol) LIKE :succTranslation OR LOWER(succTranslations.name) LIKE :succTranslation ', 
                                [':succTranslation' => '%'.mb_strtolower($this->orderStatusSuccessorTranslation, 'UTF-8').'%']);
	            }])
                ->joinWith(['orderStatusPredecessor' => function ($q) {
                    $q->alias('pred')
                        ->joinWith(['orderStatusTranslations' => function ($q) {
                            $q->alias('predTranslations')->joinWith(['language' => function ($q) {
                                $q->alias('predLanguage');
                            }]);
                        }])
                        ->where('LOWER(pred.symbol) LIKE :predTranslation OR LOWER(predTranslations.name) LIKE :predTranslation ', 
                                [':predTranslation' => '%'.mb_strtolower($this->orderStatusPredecessorTranslation, 'UTF-8').'%']);
	            }]);

        if(!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        return $dataProvider;
    }
}
