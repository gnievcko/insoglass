<?php

namespace common\models\ars;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\ar\UserWarehouse;

/**
 * UserWarehouseSearch represents the model behind the search form about `common\models\ar\UserWarehouse`.
 */
class UserWarehouseSearch extends UserWarehouse {

	public $userName;
	public $warehouseName;
	
    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            	[['user_id', 'warehouse_id'], 'integer'],
        		[['userName', 'warehouseName'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios() {
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params) {
        $query = UserWarehouse::find();

        $dataProvider = new ActiveDataProvider([
            	'query' => $query,
        ]);
        
        $dataProvider->setSort([
        		'attributes' => [
        				'userName' => [
        						'asc' => ['user.email' => SORT_ASC],
        						'desc' => ['user.email' => SORT_DESC],
        				],
        				'warehouseName' => [
        						'asc' => ['warehouse.name' => SORT_ASC],
        						'desc' => ['warehouse.name' => SORT_DESC],
        				],
        		]
        ]);
        
        $this->load($params);
        
        $query->joinWith(['user' => function ($q) {
        	$q->where('LOWER(user.first_name) LIKE :userName OR
		        			LOWER(user.last_name) LIKE :userName OR
		        			LOWER(user.email) LIKE :userName', [':userName' => '%'.mb_strtolower($this->userName, 'UTF-8').'%']);
        }])
		        ->joinWith(['warehouse' => function ($q) {
		        	$q->where('LOWER(warehouse.name) LIKE :warehouseName', [':warehouseName' => '%'.mb_strtolower($this->warehouseName, 'UTF-8').'%']);
		        }]);
        

        if(!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        return $dataProvider;
    }
}
