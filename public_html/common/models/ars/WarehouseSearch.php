<?php

namespace common\models\ars;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\ar\Warehouse;

/**
 * WarehouseSearch represents the model behind the search form about `common\models\ar\Warehouse`.
 */
class WarehouseSearch extends Warehouse {
	
	public $parentWarehouseName;
	public $addressData;
	
    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            	[['id', 'parent_warehouse_id', 'address_id', 'is_active'], 'integer'],
				[['name', 'index', 'date_creation', 'parentWarehouseName', 'addressData'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios() {
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params) {
        $query = Warehouse::find();

        $dataProvider = new ActiveDataProvider([
        		'query' => $query,
        		'sort' => [
        				'attributes' => [
        						'id',
        						'name',
        						'index',
        						'is_active',
        						'date_creation',
        						'parent_warehouse_id',
        						'parentWarehouseName' => [
        								'asc' => ['pw.name' => SORT_ASC],
        								'desc' => ['pw.name' => SORT_DESC],
        						]
        				]
        		]
        ]);
        
        
        if(!$this->load($params) && !$this->validate()) {
        	$query->joinWith(['parentWarehouse pw']);
        	return $dataProvider;
        }
        
        $query->joinWith(['parentWarehouse pw']);
        
        $query->joinWith(['address' => function($query) {
        	
        	$query->joinWith(['city' => function($query) {
        		$query->joinWith('province');
        	}]);
        	
        	$query->andFilterWhere(['or', 
        			['like','LOWER(address.main)', mb_strtolower($this->addressData)],
        			['like','LOWER(address.complement)', mb_strtolower($this->addressData)],
        			['like','LOWER(province.name)', mb_strtolower($this->addressData)],
        			['like','LOWER(city.name)', mb_strtolower($this->addressData)],
        			['like','LOWER(city.zip_code)', mb_strtolower($this->addressData)]
        	]);
        	
        }]);
        
        $query->andFilterWhere(['like','LOWER(pw.name)', mb_strtolower($this->parentWarehouseName)]);
        $query->andFilterWhere(['like','LOWER(warehouse.date_creation)', mb_strtolower($this->date_creation)]);
        $query->andFilterWhere(['like','LOWER(warehouse.name)', mb_strtolower($this->name)]);
        $query->andFilterWhere(['like','LOWER(warehouse.index)', mb_strtolower($this->index)]);
        $query->andFilterWhere(['warehouse.id' => $this->id]);
        $query->andFilterWhere(['warehouse.is_active' => $this->is_active]);
        $query->andFilterWhere(['warehouse.parent_warehouse_id' => $this->parent_warehouse_id]);
       	
        return $dataProvider;



    }
}
