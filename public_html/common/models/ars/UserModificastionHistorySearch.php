<?php

namespace common\models\ars;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\ar\UserModificationHistory;

/**
 * UserModificastionHistorySearch represents the model behind the search form about `common\models\ar\UserModificationHistory`.
 */
class UserModificastionHistorySearch extends UserModificationHistory {

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            	[['id'], 'integer'],
				[['modification'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios() {
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params) {
        $query = UserModificationHistory::find();

        $dataProvider = new ActiveDataProvider([
            	'query' => $query,
        ]);

        $this->load($params);

        if(!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
        ]);

		$query->andFilterWhere(['like', 'modification', $this->modification]);

        return $dataProvider;
    }
}
