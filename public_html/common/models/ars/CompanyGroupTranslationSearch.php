<?php

namespace common\models\ars;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\ar\CompanyGroupTranslation;

/**
 * CompanyGroupTranslationSearch represents the model behind the search form about `common\models\ar\CompanyGroupTranslation`.
 */
class CompanyGroupTranslationSearch extends CompanyGroupTranslation {

	public $companyGroupSymbol;
	public $languageName;
    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            	[['company_group_id', 'language_id'], 'integer'],
				[['name', 'companyGroupSymbol', 'languageName'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios() {
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */  
    public function search($params) {
    	$query = CompanyGroupTranslation::find();
    
    	$dataProvider = new ActiveDataProvider([
    			'query' => $query,
    	]);
    
    	$dataProvider->setSort([
    			'attributes' => [
    					'name',
    					'companyGroupSymbol' => [
    							'asc' => ['company_group.symbol' => SORT_ASC],
    							'desc' => ['company_group.symbol' => SORT_DESC],
    							'label' => Yii::t('main', 'Client group')
    					],
    					'languageName' => [
    							'asc' => ['language.name' => SORT_ASC],
    							'desc' => ['language.name' => SORT_DESC],
    							'label' => Yii::t('main', 'Language')
    					],
    			]
    	]);

        $this->load($params);

        if(!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
        	$query->joinWith(['companyGroup', 'language']);
            return $dataProvider;
        }
               
        $query->select(['company_group_id', 'language_id','company_group_translation.name'])
        		->joinWith(['companyGroup' => function ($q) {
        			$q->where('LOWER(company_group.symbol) LIKE "%' . mb_strtolower($this->companyGroupSymbol, 'UTF-8') . '%"');
        		}])
        		->joinWith(['language' => function ($q) {
        			$q->where('LOWER(language.name) LIKE "%' . mb_strtolower($this->languageName, 'UTF-8') . '%"');
        		}])
				->andFilterWhere([
            		'company_group_id' => $this->company_group_id,
            		'language_id' => $this->language_id,
        		])
				->andFilterWhere(['like', 'company_group_translation.name', $this->name]);

        return $dataProvider;
    }
}
