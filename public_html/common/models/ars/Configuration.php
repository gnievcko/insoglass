<?php

namespace common\models\ars;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\ar\Configuration as ConfigurationModel;

/**
 * Configuration represents the model behind the search form about `common\models\ar\Configuration`.
 */
class Configuration extends ConfigurationModel {

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            	[['id', 'days_dbbackups_expiration'], 'integer'],
				[['path_mysqldump', 'path_mongodbdump', 'path_dbbackups'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios() {
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params) {
        $query = ConfigurationModel::find();

        $dataProvider = new ActiveDataProvider([
            	'query' => $query,
        ]);

        $this->load($params);

        if(!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'days_dbbackups_expiration' => $this->days_dbbackups_expiration,
        ]);

		$query->andFilterWhere(['like', 'path_mysqldump', $this->path_mysqldump])
            ->andFilterWhere(['like', 'path_mongodbdump', $this->path_mongodbdump])
            ->andFilterWhere(['like', 'path_dbbackups', $this->path_dbbackups]);

        return $dataProvider;
    }

    public function getId(){
        return ConfigurationModel::find()->one()->id;
    }
}
