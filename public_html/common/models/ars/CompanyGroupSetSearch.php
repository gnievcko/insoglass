<?php

namespace common\models\ars;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\ar\CompanyGroupSet;

/**
 * CompanyGroupSetSearch represents the model behind the search form about `common\models\ar\CompanyGroupSet`.
 */
class CompanyGroupSetSearch extends CompanyGroupSet {

	public $companyName;
	public $companyGroupSymbol;
    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            	[['company_id', 'company_group_id'], 'integer'],
        		[['companyName', 'companyGroupSymbol'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios() {
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params) {
        $query = CompanyGroupSet::find();

        $dataProvider = new ActiveDataProvider([
            	'query' => $query,
        ]);
        
        $dataProvider->setSort([
        		'attributes' => [
        				'companyName' => [
        						'asc' => ['company.name' => SORT_ASC],
        						'desc' => ['company.name' => SORT_DESC],
        						'label' => Yii::t('main', 'Client')
        				],
        				'companyGroupSymbol' => [
        						'asc' => ['company_group.symbol' => SORT_ASC],
        						'desc' => ['company_group.symbol' => SORT_DESC],
        						'label' => Yii::t('main', 'Client group')
        				],
        		]
        ]);
        
        
        $this->load($params);

        if(!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
        	$query->joinWith(['companyGroup', 'company']);
            return $dataProvider;
        }
        
        $query->select(['company_id', 'company_group_id'])
        		->joinWith(['companyGroup' => function ($q) {
        			$q->where('LOWER(company_group.symbol) LIKE "%' . mb_strtolower($this->companyGroupSymbol, 'UTF-8') . '%"');
        		}])
        		->joinWith(['company' => function ($q) {
        			$q->where('LOWER(company.name) LIKE "%' . mb_strtolower($this->companyName, 'UTF-8') . '%"');
        		}])
        		->andFilterWhere([
        				'company_id' => $this->company_id,
        				'company_group_id' => $this->company_group_id,
        		]);
        
        return $dataProvider;
    }
}
