<?php

namespace common\models\ars;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\ar\TimeUnit;

/**
 * TimeUnitSearch represents the model behind the search form about `common\models\ar\TimeUnit`.
 */
class TimeUnitSearch extends TimeUnit {

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            	[['id'], 'integer'],
				[['symbol'], 'safe'],
				[['conversion_minute'], 'number'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios() {
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params) {
        $query = TimeUnit::find();

        $dataProvider = new ActiveDataProvider([
            	'query' => $query,
        ]);

        $this->load($params);

        if(!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }
        
        $query->andFilterWhere(['like', 'id', $this->id]);
        $query->andFilterWhere(['like', 'conversion_minute', $this->conversion_minute]);
		$query->andFilterWhere(['like', 'symbol', $this->symbol]);

        return $dataProvider;
    }
}
