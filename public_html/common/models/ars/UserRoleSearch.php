<?php

namespace common\models\ars;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\ar\UserRole;

/**
 * UserRoleSearch represents the model behind the search form about `common\models\ar\UserRole`.
 */
class UserRoleSearch extends UserRole {

	public $userName;
	public $roleName;

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            	[['user_id', 'role_id'], 'integer'],
        		[['userName', 'roleName'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios() {
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params) {
        $query = UserRole::find();

        $dataProvider = new ActiveDataProvider([
            	'query' => $query,
        ]);

        $dataProvider->setSort([
        		'attributes' => [
        				'roleName' => [
        						'asc' => ['role.symbol' => SORT_ASC],
        						'desc' => ['role.symbol' => SORT_DESC],
        						'label' => Yii::t('main', 'Role')
        				],
        				'userName' => [
        						'asc' => ['user.email' => SORT_ASC],
        						'desc' => ['user.email' => SORT_DESC],
        						'label' => Yii::t('main', 'User')
        				],
        		]
        ]);

        $this->load($params);

        $query->joinWith(['role' => function ($q) {
		        	$q->joinWith(['roleTranslations']);
		        	$q->where('LOWER(role_translation.name) LIKE :roleName', [':roleName' => '%'.mb_strtolower($this->roleName, 'UTF-8').'%']);
		        }])
		        ->joinWith(['user' => function ($q) {
		        	$q->where('LOWER(user.first_name) LIKE :userName OR 
		        			LOWER(user.last_name) LIKE :userName OR 
		        			LOWER(user.email) LIKE :userName', [':userName' => '%'.mb_strtolower($this->userName, 'UTF-8').'%']);
		        }]);


        if(!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        return $dataProvider;
    }
}
