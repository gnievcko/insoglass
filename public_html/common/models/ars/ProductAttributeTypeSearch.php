<?php

namespace common\models\ars;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\ar\ProductAttributeType;

/**
 * ProductAttributeTypeSearch represents the model behind the search form about `common\models\ar\ProductAttributeType`.
 */
class ProductAttributeTypeSearch extends ProductAttributeType {

    public $groupSymbol;
    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            [['id', 'product_attribute_group_id', 'is_visible'], 'integer'],
				[['symbol', 'variable_type', 'attribute_checked_ids', 'attribute_blocked_ids', 'groupSymbol'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios() {
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params) {
        $query = ProductAttributeType::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => [
                'defaultOrder' => ['id'=>SORT_ASC],
                'attributes' => [
                    'id' => [
                        'asc' => ['product_attribute_type.id' => SORT_ASC],
                        'desc' => ['product_attribute_type.id' => SORT_DESC],
                    ],
                    'groupSymbol' => [
                        'asc' => ['product_attribute_group.symbol' => SORT_ASC],
                        'desc' => ['product_attribute_group.symbol' => SORT_DESC],
                    ],
                    'symbol' => [
                        'asc' => ['product_attribute_type.symbol' => SORT_ASC],
                        'desc' => ['product_attribute_type.symbol' => SORT_DESC],
                    ],
                    'variable_type' => [
                        'asc' => ['product_attribute_type.variable_type' => SORT_ASC],
                        'desc' => ['product_attribute_type.variable_type' => SORT_DESC],
                    ],
                    'attribute_checked_ids' => [
                        'asc' => ['product_attribute_type.attribute_checked_ids' => SORT_ASC],
                        'desc' => ['product_attribute_type.attribute_checked_ids' => SORT_DESC],
                    ],
                    'attribute_blocked_ids' => [
                        'asc' => ['product_attribute_type.attribute_blocked_ids' => SORT_ASC],
                        'desc' => ['product_attribute_type.attribute_blocked_ids' => SORT_DESC],
                    ],
                    'is_visible' => [
                        'asc' => ['product_attribute_type.is_visible' => SORT_ASC],
                        'desc' => ['product_attribute_type.is_visible' => SORT_DESC],
                    ],
                ],
            ],
        ]);

        if(!$this->load($params) && !$this->validate()) {
            $query->joinWith(['product_attribute_group']);
            return $dataProvider;
        }

        $query->joinWith(['productAttributeGroup' => function($q) {
            $q->where('LOWER(product_attribute_group.symbol) LIKE "%'.mb_strtolower($this->groupSymbol, 'UTF-8').'%"');
        }]);
        $query->andFilterWhere([
            'id' => $this->id,
            'is_visible' => $this->is_visible,
        ]);

		$query->andFilterWhere(['like', 'symbol', $this->symbol])
            ->andFilterWhere(['like', 'variable_type', $this->variable_type])
            ->andFilterWhere(['like', 'attribute_checked_ids', $this->attribute_checked_ids])
            ->andFilterWhere(['like', 'attribute_blocked_ids', $this->attribute_blocked_ids]);

        return $dataProvider;
    }
}
