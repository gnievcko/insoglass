<?php

namespace common\models\ars;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\ar\ProductStatusTranslation;

/**
 * ProductStatusTranslationSearch represents the model behind the search form about `common\models\ar\ProductStatusTranslation`.
 */
class ProductStatusTranslationSearch extends ProductStatusTranslation {

	public $productStatusSymbol;
	public $languageName;
	
    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            	[['product_status_id', 'language_id'], 'integer'],
				[['name','productStatusSymbol','languageName'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios() {
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params) {
        $query = ProductStatusTranslation::find();

        $dataProvider = new ActiveDataProvider([
            	'query' => $query,
        		'sort' => [
        				'attributes' => [
        						'name',
        						'productStatusSymbol' => [
        								'asc' => ['product_status.symbol' => SORT_ASC],
        								'desc' => ['product_status.symbol' => SORT_DESC],
        						],
        						'languageName' => [
        								'asc' => ['language.name' => SORT_ASC],
        								'desc' => ['language.symbol' => SORT_DESC],
        						],
        				]
        		]
        ]);


        if(!$this->load($params) && !$this->validate()) {
            $query->joinWith(['productStatus', 'language']);
            return $dataProvider;
        }
        
        $query->joinWith(['productStatus' => function ($q) {
	    	$q->where('LOWER(product_status.symbol) LIKE "%' . mb_strtolower($this->productStatusSymbol, 'UTF-8') . '%"');
	    }])
	    ->joinWith(['language' => function ($q) {
	    	$q->where('LOWER(language.name) LIKE "%' . mb_strtolower($this->languageName, 'UTF-8') . '%"');
	    }])
	   	->andFilterWhere(['like', 'product_status_translation.name', $this->name]);

        return $dataProvider;
    }
}
