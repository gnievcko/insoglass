<?php

namespace common\models\ars;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\ar\ApplicationCache;

/**
 * ApplicationCacheSearch represents the model behind the search form about `common\models\ar\ApplicationCache`.
 */
class ApplicationCacheSearch extends ApplicationCache {

    /**
     * @inheritdoc
     */
    public $entityName;

    public function rules() {
        return [
            	[['id', 'entity_id'], 'integer'],
				[['date_last_invoice'], 'safe'],
                [['entityName'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios() {
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params) {
        $query = ApplicationCache::find();

        $dataProvider = new ActiveDataProvider([
            	'query' => $query,
                'sort' => [
                    'attributes' => [
                        'entityName' => [
                            'asc' => ['entity.name' => SORT_ASC],
                            'desc' => ['entity.name' => SORT_DESC],
                        ],
                        'id',
                        'date_last_invoice',
                        'entity_id',
                    ]
                ]

        ]);


        if(!$this->load($params) && !$this->validate()) {
            $query->leftJoin(['entity']);
            return $dataProvider;
        }

        $query->joinWith(['entity' => function ($q) {
            //$q->where('LOWER(entity.name) LIKE "%' . mb_strtolower($this->entityName, 'UTF-8') . '%"');
            $q->where(['or',['like','LOWER(entity.name)', mb_strtolower($this->entityName, 'UTF-8')],['entity.name'=>null]]);
        }]);

        $query->andFilterWhere(['like', 'application_cache.id', $this->id])
            ->andFilterWhere(['like', 'date_last_invoice', $this->date_last_invoice])
            ->andFilterWhere(['like', 'entity_id', $this->entity_id]);
        return $dataProvider;
    }
}
