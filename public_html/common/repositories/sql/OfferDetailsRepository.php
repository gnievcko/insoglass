<?php

namespace common\repositories\sql;

use common\repositories\OfferDetailsRepository as BaseRepository;
use common\models\aq\OrderQuery;
use common\models\aq\UserOrderSalesmanQuery;

class OfferDetailsRepository implements BaseRepository {
    
    public function getDetails($orderId) {
        return OrderQuery::getDetails($orderId);
    }

    public function getSalespeople($orderId) {
        return UserOrderSalesmanQuery::getSalesmenByOrderId($orderId);
    }

    public function getProducts($orderId) {
        return OrderQuery::getOrderProducts($orderId, ['withArtificial' => true]);
    }
}
