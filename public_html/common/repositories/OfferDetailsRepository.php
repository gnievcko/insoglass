<?php

namespace common\repositories;

interface OfferDetailsRepository {
    
    public function getDetails($orderId);

    public function getSalespeople($orderId);

    public function getProducts($orderId);
}
