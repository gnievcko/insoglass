function DynamicForm(formId) {
    var ITEM_CLASS = 'item';
    var ITEMS_CONTAINER_CLASS = 'items-container';
    var REMOVABLE_BTN_CLASS = 'remove-item-btn';
    var REMOVE_ALL_BTN_CLASS = 'remove-all-items-btn';

    var $form = $('#'+formId);
    var that = this;

    this.activateRemoveButtons = function() {
        $form.find('.' + REMOVABLE_BTN_CLASS).each(function(_, button) {
            activateRemoveButton($(button));
        });
        $form.find('.' + REMOVE_ALL_BTN_CLASS).each(function(_, button) {
            activateRemoveAllButton($(button));
        });
    };

    var activateRemoveButton = function($button) {
        var $removableItem = $button.closest('.' + ITEM_CLASS);

        $button.on('click', function() {
            var $itemsContainer = $removableItem.closest('.' + ITEMS_CONTAINER_CLASS);
            $removableItem.remove();
            enumerateItems($itemsContainer.find('.' + ITEM_CLASS));

            that.onRemoveItem($removableItem);
        });
    };

    var activateRemoveAllButton = function($button) {
        $button.on('click', function() {
            var itemsContainerSelector = $button.data('items-container-selector');
            $(itemsContainerSelector).html('');
        });
    };

    var enumerateItems = function($items) {
        $items.each(function(idx, item) {
            assignNewPrefix(idx, $(item));
        });
    }

    var assignNewPrefix = function(newPrefix, $item) {
        var formBaseName = $item.data('form-base-name');
        var activeFormBaseName = $item.data('active-form-base-name')

        var activeFormNameRegex = new RegExp(activeFormBaseName + '-[0-9]+', 'g');
        var baseFormNameRegex = new RegExp(formBaseName + '\[[0-9]+\]', 'g');

        var attributesToUpdate = ['id', 'class', 'for', 'aria-labelledby'];

        $item.find('*').each(function(_, element) {
            var $element = $(element);

            attributesToUpdate.forEach(function(attributeName) {
                var attributeValue = $element.attr(attributeName); 

                if(attributeValue) {
                    $element.attr(attributeName, attributeValue.replace(activeFormNameRegex, activeFormBaseName + '-' + newPrefix));
                }
            });

            var elementName = $element.attr('name');
            if(elementName) {
                $element.attr('name', elementName.replace(baseFormNameRegex, formBaseName + '['+newPrefix +']'));
            }
        })  
    }

    this.activateAddItemButtons = function(buttonsClass) {
        var buttonsClass = buttonsClass || 'add-item-button';

        $form.find('.' + buttonsClass).each(function(_, button) {
            activateAddItemButton($(button));
        });
    };

    var activateAddItemButton = function($button) {
        var $itemsContainer = $($button.data('items-container-selector'));
        var itemViewUrl = $button.data('item-view-url');

        $button.on('click', function() {
            that.addItem($itemsContainer, itemViewUrl);
        });
    };

    this.addItem = function($container, itemViewUrl, requestData) {
        var requestData = requestData || {};
        requestData.prefix = $container.children(':not(script)').size() 
        requestData.formId = formId;

        $.get(itemViewUrl, requestData, function(view) {
            var $view = $(view);

            configureItemEvents($view);
            $container.append($view);
            that.onAddItem($view);
        });
    }

    var configureItemEvents = function($view) {
        $view.find('.' + REMOVABLE_BTN_CLASS).each(function(_, button) {
            activateRemoveButton($(button));
        });
    };

    var GALLERY_CLASS = 'gallery';
    var GALLERY_HINT_CLASS = 'gallery-hint';
    var PHOTO_UPLOAD_CLASS = 'photo-upload';
    var UPLOAD_TYPE_PHOTO = 'photo';

    this.activatePhotoUpload = function() {
        var $gallery = $form.find('.' + GALLERY_CLASS + ':first');
        var $galleryHint = $form.find('.' + GALLERY_HINT_CLASS + ':first');
        var $photoUpload = $form.find('.' + PHOTO_UPLOAD_CLASS + ':first');
        var storageUrl = $gallery.data('file-storage-url');
        var photoViewUrl = $gallery.data('photo-form-url');
        var $ajaxLoader = $photoUpload.closest('.file-upload-trigger:first').find('.ajax-loader:first');

        $photoUpload.on('change', function() {
            $ajaxLoader.show();
            var _this = this;
            sendFile(storageUrl, this, UPLOAD_TYPE_PHOTO, function(photo) {
                appendPhoto(photo);
                $(_this).val("");
            }, function(error) {
                showFatalError(error) 
            }, function() {
                $ajaxLoader.hide();
            });
        });

        var appendPhoto = function(imageData) {
            $.get(photoViewUrl, 
                  {photo: imageData.photo.file_hash, thumbnail: imageData.thumbnail.file_hash, prefix: $gallery.children('.photo-form').size()}, function(thumbnail) {
                var $thumbnail = $(thumbnail);
                initThumbnail($thumbnail);
                $galleryHint.hide();
                $gallery.append($thumbnail);
            });
        }

        var initThumbnail = function($thumbnail) {
            $thumbnail.find('.photo-remove-btn').on('click', function() {
                $thumbnail.remove(); 
                $photos = $gallery.children('.photo-form');
                if($photos.size() === 0) {
                    $galleryHint.show(); 
                } 
                else {
                    enumerateFiles($photos);
                }
            });
        }

        $gallery.find('.photo').each(function(idx, photo) {
            initThumbnail($(photo));
        });
    };

    var DOCUMENTS_CONTAINER_CLASS = 'documents-container';
    var DOCUMENTS_HINT_CLASS = 'documents-hint';
    var DOCUMENT_UPLOAD_CLASS = 'document-upload';
    var UPLOAD_TYPE_DOCUMENT = 'document';

    this.activateDocumentUpload = function() {
        var $documentsContainer = $form.find('.' + DOCUMENTS_CONTAINER_CLASS + ':first');
        var $documentsHint = $form.find('.' + DOCUMENTS_HINT_CLASS + ':first');
        var $documentUpload = $form.find('.' + DOCUMENT_UPLOAD_CLASS + ':first');
        var storageUrl = $documentsContainer.data('file-storage-url');
        var documentViewUrl = $documentsContainer.data('document-form-url');
        var $ajaxLoader = $documentUpload.closest('.file-upload-trigger').find('.ajax-loader:first');

        $documentUpload.on('change', function() {
            $ajaxLoader.show();
            var _this = this;
            sendFile(storageUrl, this, UPLOAD_TYPE_DOCUMENT, function(document) {
                appendDocument(document);
                $(_this).val("");
            }, function(error) {
                showFatalError(error) 
            }, function() {
                $ajaxLoader.hide(); 
            });
        });

        var appendDocument = function(documentData) {
            $.get(documentViewUrl, {document: documentData.file_hash, 
                  name: documentData.original_file_name, prefix: $documentsContainer.children('.document-form').size()}, function(form) {
                var $form = $(form);
                initDocument($form);
                $documentsHint.hide();
                $documentsContainer.append($form);
            });
        }

        var initDocument = function($document) {
            $document.find('.document-remove-btn').on('click', function() {
                $document.remove(); 
                var $documents = $documentsContainer.children('.document-form');
                if($documents.size() === 0) {
                    $documentsHint.show(); 
                } 
                else {
                    enumerateFiles($documents);
                }
            });
        }

        $documentsContainer.find('.document').each(function(idx, document) {
            initDocument($(document));
        });
    }
    
    var PRODUCT_FILES_CONTAINER_CLASS = 'product-files-container';
    var PRODUCT_FILES_HINT_CLASS = 'product-files-hint';
    var PRODUCT_FILES_UPLOAD_CLASS = 'product-files-upload';
    var PRODUCT_FILES_TYPE_DOCUMENT = 'mix';
    var BOX_CLASS = 'product-files';

    this.activateProductFileUpload = function() {
        $(document).on('change', '.'+PRODUCT_FILES_UPLOAD_CLASS, function(e) {
            var $box = $(e.target).closest('.' + BOX_CLASS);
            var $documentsContainer = $box.find('.' + PRODUCT_FILES_CONTAINER_CLASS + ':first');
            var $documentsHint = $box.find('.' + PRODUCT_FILES_HINT_CLASS + ':first');
            var $documentUpload = $box.find('.' + PRODUCT_FILES_UPLOAD_CLASS + ':first');
            var storageUrl = $documentsContainer.data('file-storage-url');
            var documentViewUrl = $documentsContainer.data('document-form-url');
            var $ajaxLoader = $documentUpload.closest('.file-upload-trigger').find('.ajax-loader:first');
            $ajaxLoader.show();
            var productPrefix = $box.attr('data-prefix');
            var _this = this;
            sendFile(storageUrl, this, PRODUCT_FILES_TYPE_DOCUMENT, function(document) {
                appendDocument(document, documentViewUrl, $documentsContainer, $documentsHint,productPrefix);
                $(_this).val("");
            }, function(error) {
                showFatalError(error) 
            }, function() {
                $ajaxLoader.hide(); 
            });
        });

        var appendDocument = function(documentData, documentViewUrl, $documentsContainer, $documentsHint, productPrefix) {
            $.get(documentViewUrl, {
                    document: documentData.file_hash, 
                    name: documentData.original_file_name,
                    prefix: $documentsContainer.children('.document-form').size(),
                    productPrefix: productPrefix
                }, function(form) {
                var $form = $(form);
                initDocument($form, $documentsContainer, $documentsHint);
                $documentsHint.hide();
                $documentsContainer.append($form);
            });
        }

        var initDocument = function() {
            $(document).on('click','.document-remove-btn', function(e) {
                var $box = $(e.target).closest('.' + BOX_CLASS);
                $(e.target).parent('.document').remove();
                var $documentsContainer = $box.find('.' + PRODUCT_FILES_CONTAINER_CLASS + ':first');
                var $documents = $documentsContainer.children('.document-form');
                var $documentsHint = $box.find('.' + PRODUCT_FILES_HINT_CLASS + ':first');
                if($documents.size() === 0) {
                    $documentsHint.show(); 
                } 
                else {
                    enumerateFiles($documents);
                }
            });
        }
        
        initDocument();

    }


    var sendFile = function(url, fileInput, type, onSuccess, onError, doneCallback) {
        var formData = new FormData();
        formData.append(fileInput.name, fileInput.files[0]);
        formData.append('type', type);

        $.ajax({
            url: url,
            type: 'POST',
            success: onSuccess,
            error: onError,
            complete: doneCallback,
            data: formData,
            dataType: 'json',
            cache: false,
            contentType: false,
            processData: false
        });
    }

    var enumerateFiles = function($files) {
        var prefixRegex = /\[files\]\[[0-9]+\]/g;
        $files.each(function(idx, file) {
            var $file = $(file); 
            $file.find('input[type="hidden"]').each(function(_, input) {
                var $input = $(input); 
                var oldName = $input.attr('name');
                $input.attr('name', oldName.replace(prefixRegex, '[files][' + idx + ']'));
            })
        })
    };

    var showFatalError = function(error) {
       var message =  error.responseJSON.file[0];
       alert(message);
    }

    this.onAddItem = function() {};
    this.onRemoveItem = function() {};
}

$(document).ready(function(){
	$("body").on("click", ".attachment-delete-btn", function(e) {
		$id = $(e.target).attr('data-id');
		$fileUrl = $(e.target).attr('data-url');
		$url = "../delete-attachment";
		
		$successFlash = '<div class="alert-success alert fade in"> <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>' + window['removedFileSuccessMsg'] + '</div>';
		$failFlash = '<div class="alert-danger alert fade in"> <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>' + window['removedFileFailMsg'] + '</div>';
		
		 $.post($url, {id: $id, url: $fileUrl }, function () {
         }).done(function() {
        	 $(e.target).parent().remove();
        	 $(".alert-div").append($successFlash);
         })
         .fail(function() {
        	 $(".alert-div").append($failFlash);
         }); 
     });
});
