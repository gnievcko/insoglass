Chart.defaults.global.pointHitDetectionRadius = 1;

window.custom = window.custom
		|| function(tooltip) {
			var tooltipEl = $("#chartjs-tooltip");

			if (!tooltipEl[0]) {
				$("body")
						.append(
								'<div class="popover fade right in" id="chartjs-tooltip" role="tooltip"><div class="arrow"></div><div id="chartjs-tooltip-text" class="popover-content" ></div></div>');
				tooltipEl = $("#chartjs-tooltip");
			}

			if (!tooltip.opacity) {
				tooltipEl.css({
					opacity : 0,
					'z-index' : '-1',
				});
				$(".chartjs-wrap canvas").each(function(index, el) {
					$(el).css("cursor", "default");
				});
				return;
			}

			$(this._chart.canvas).css("cursor", "pointer");

			if (tooltip.body) {
				var innerHtml = [];
				for (var i = tooltip.body.length - 1; i >= 0; i--) {
					if (tooltip.body.length > 1)
						innerHtml
								.push(('<div class="tt"><div class="box" style="background-color:'
										+ tooltip.labelColors[i].backgroundColor
										+ ';border:1px solid '
										+ tooltip.labelColors[i].borderColor
										+ '"></div>' + tooltip.body[i].lines[0] + '</div>'));
					else
						innerHtml.push(('<div class="tt">'
								+ tooltip.body[i].lines[0] + '</div>'));
				}
				$('#chartjs-tooltip-text').html(innerHtml);
			}
				
			var position = $(this._chart.canvas)[0].getBoundingClientRect();
			var scroll = $(window).scrollTop();
			
			//TODO Refactor
			if (tooltip.xAlign == "left") {
				tooltipEl.removeClass('left');
				tooltipEl.removeClass('top');
				tooltipEl.removeClass('bottom');
				tooltipEl.addClass('right');
				tooltipEl.css({
					opacity : 1,
					width : tooltip.width ? (tooltip.width + 'px') : 'auto',
					left : position.left + tooltip.x + 'px',
					top : position.top + tooltip.y + scroll + 'px',
					fontFamily : tooltip.bodyFontFamily,
					fontSize : tooltip.bodyFontSize + 'px',
					color : tooltip.bodyFontColor,
					display : 'block',
					'z-index' : 10,
				});
			} else if (tooltip.xAlign == "right") {
				tooltipEl.removeClass('right');
				tooltipEl.removeClass('top');
				tooltipEl.removeClass('bottom');
				tooltipEl.addClass('left');
				tooltipEl.css({
					opacity : 1,
					width : tooltip.width ? (tooltip.width + 'px') : 'auto',
					left : position.left + tooltip.x - 10 + 'px',
					top : position.top + tooltip.y + scroll + 'px',
					fontFamily : tooltip.bodyFontFamily,
					fontSize : tooltip.bodyFontSize + 'px',
					color : tooltip.bodyFontColor,
					display : 'block',
					'z-index' : 10,
				});
			} else if (tooltip.yAlign == "top") {
				tooltipEl.removeClass('right');
				tooltipEl.removeClass('left');
				tooltipEl.removeClass('top');
				tooltipEl.addClass('bottom');
				tooltipEl.css({
					opacity : 1,
					width : tooltip.width ? (tooltip.width + 'px') : 'auto',
					left : position.left + tooltip.x - 10 + 'px',
					top : position.top + tooltip.y + scroll + 'px',
					fontFamily : tooltip.bodyFontFamily,
					fontSize : tooltip.bodyFontSize + 'px',
					color : tooltip.bodyFontColor,
					display : 'block',
					'z-index' : 10,
				});
			} else if (tooltip.yAlign == "bottom") {
				tooltipEl.removeClass('right');
				tooltipEl.removeClass('left');
				tooltipEl.removeClass('bottom');
				tooltipEl.addClass('top');
				tooltipEl.css({
					opacity : 1,
					width : tooltip.width ? (tooltip.width + 'px') : 'auto',
					left : position.left + tooltip.x - 10 + 'px',
					top : position.top + tooltip.y + scroll + 'px',
					fontFamily : tooltip.bodyFontFamily,
					fontSize : tooltip.bodyFontSize + 'px',
					color : tooltip.bodyFontColor,
					display : 'block',
					'z-index' : 10,
				});
			}
		};