loader = function (type, baseUrl, div, link, chartId, next) {
    this.url = baseUrl;
    var _this = this;
    this.chartId = chartId;
    this.div = div;
    this.type = type;
    this.next = next;
    this.baseUrl = baseUrl;
    
    this.getChart = function () {
        $.ajax({
            url: _this.url,
            method: "get",
            data: {
                isAjax: true,
                chartId: _this.chartId,
                div: _this.div,
                type: _this.type,
            },
            success: function (response) {
                $(_this.div).html(response);
            }
        });
    }
    
    this.update = function (type, baseUrl, div, link, chartId, next) {
        _this.type = type;
        _this.baseUrl = baseUrl;
        _this.div = div;
        _this.chartId = chartId;
        _this.next = next;
        _this.getChart();
    }
    
    this.getChart();
    
    $(div).on('click', '.nextInterval', function () {
        next += 1;
        _this.url = baseUrl + '?next=' + next;
        _this.getChart();
    });
    
    $(div).on('click', '.prevInterval', function () {
        next -= 1;
        _this.url = baseUrl + '?next=' + next;
        _this.getChart();
    });
    
    $(div).on('click', '.changePeriod', function () {
        _this.baseUrl = $(this).attr('data-url');
        _this.type = $(this).attr('data-type');
        next = 0;
    	_this.url = baseUrl + '?next=' + next;
        _this.getChart();
    });

}