function getDetailedHistory(btnId, containerId, dataSourceUrl, showText, hideText) {
	var $button = $("#" + btnId);
	var $container = $("#" + containerId);
	
	 $button.click(function(){
		 var $extend = $button.attr("data-extend");
		 if( $extend == 1) {
			 $button.attr("data-extend", 0);
			 $button.text(hideText);
		 } else {
			 $button.attr("data-extend", 1);
			 $button.text(showText);
		 }
		 
		 $.get(dataSourceUrl,{ "extend": $extend },  function(data){
			 $container.html(data);
		 });
	 })
}
