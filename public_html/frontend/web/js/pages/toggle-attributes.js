function toggleAttributes() {

	var showButtons = function(icon) {
		icon.removeClass("glyphicon-menu-down");
		icon.addClass("glyphicon-menu-up");
	}
	
	var hideButtons = function(icon) {
		icon.removeClass("glyphicon-menu-up");
		icon.addClass("glyphicon-menu-down");
	}
	
	$(document).on("click", ".display-attributes-button", function(){
		var icon = $(this).find(".glyphicon");
		var prefix = $(this).attr("data-index");
                var isArtificial = $(this).attr("data-artificial") == "1" ? true : false;
                
                var idPart = (isArtificial ? 'artificial' : '') + 'productattributeform';
                
		if(icon.hasClass("glyphicon-menu-down")) {
			showButtons(icon);
			$("#"+idPart+"-" + prefix + "-isrowvisible").val(1);
			$(this).parent().next(".attributes-row").slideDown({ "queue": false, "duration": 100, "easing": "linear" });
		}
		else {
			hideButtons(icon);
			$("#"+idPart+"-" + prefix + "-isrowvisible").val(0);
			$(this).parent().next(".attributes-row").slideUp({ "queue": false, "duration": 100, "easing": "linear" });
		}
	});
	
}
