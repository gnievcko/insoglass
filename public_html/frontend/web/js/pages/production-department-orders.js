var initDynamicFeatures = function(url) {
	$("#production-department-orders-list-table").on("click", ".priority-change", function(e) {
		var orderOfferedProductId = $(this).data("order-offered-product-id"); 
		
		$.get(url + "/get-order-priority", { orderOfferedProductId: orderOfferedProductId }, function(data) {
			data = $.parseJSON(data);
			if(data) {
				$("#orderpriorityform-orderid").val(data.orderId);
				$("#orderpriorityform-orderpriorityid").val(data.orderPriorityId);
				$("#orderpriorityform-orderprioritycheckbox").attr("checked", data.orderPriorityCheckbox == 1 ? true : false);
			}
		});
	});
};