var initDynamicFeatures = function(url, idContainer) {
	$(".dynamic-table-data").on("click", ".checkbox-completion", function() {
		if($(this).not(":checked")) {
			var taskId = $(this).data("id");
			var checkboxInput = $(this);

			$.post(url+"/complete", { id: taskId }, function(data) {
			}).done(function() {
				$("td[data-id=" + taskId + "]").addClass("strikeout");
				$("td[data-id=" + taskId + "]").attr("disabled", "true");
				checkboxInput.attr("disabled", "disabled");
				$(".task-deletion[data-id=" + taskId + "]").css("display", "inline-block");
				$(".task-edit[data-id=" + taskId + "]").css("display", "none");
			});
		}
	});

	$(".dynamic-table-data").on("click", ".task-deletion", function() {
		var taskId = $(this).data("id");

		$.post(url+"/delete", { id: taskId }, function(data) {
		}).done(function() {
			$("#" + idContainer).trigger("submit");
		});
	});

	$(".dynamic-table-data").on("click", ".task-title", function() {
		var element = $(this);
		var taskId = $(this).parent().data("id");
		var box = element.next(".task-details");

		if(!element.parent().hasClass("strikeout")) {
			if(element.hasClass("not-extended")) {
				if($(".extended").length) {
					var alreadyShown = $(".extended").parent().children(".task-details");
					alreadyShown.slideUp({ "queue": false, "duration": 300, "easing": "linear"  });
					$(".extended").removeClass("extended").addClass("not-extended");
				}

				element.removeClass("not-extended");
				element.addClass("extended");

				if(box.is(":empty")) {
					$.get(url+"/get-details", { id: taskId }, function(data) {
						box.html(data);
					}).done(function() {
						box.slideDown({ "queue": false, "duration": 300, "easing": "linear" });
					});
				}
				else {
					box.slideDown({ "queue": false, "duration": 300, "easing": "linear" });
				}
			}
			else {
				element.removeClass("extended");
				element.addClass("not-extended");

				box.slideUp({ "queue": false, "duration": 300, "easing": "linear" });
			}
		}
	});
};
