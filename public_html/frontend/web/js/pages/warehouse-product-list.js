var initDynamicFeatures = function() {
	$(".button-show-versions").click(function() {
		var button = $(this);
		var versionsBox = button.next(".versions-details");
		var productId = button.data("id");
		
		if(button.hasClass("glyphicon-menu-down")) {
			button.removeClass("glyphicon-menu-down");
			button.addClass("glyphicon-menu-up");
			
			if(versionsBox.is(":empty")) {
				var whIds = $.query.GET('WarehouseProductListForm[warehouseIds]');
				
				$.get("get-versions-count-box", { id: productId, warehouseIds: whIds }, function(data) {
					versionsBox.html(data);
				}).done(function() {
					versionsBox.slideDown({ "queue": false, "duration": 300, "easing": "linear" });
				});		
			}
			else {
				versionsBox.slideDown({ "queue": false, "duration": 300, "easing": "linear" });
			}
		}
		else {
			button.removeClass("glyphicon-menu-up");
			button.addClass("glyphicon-menu-down");
			
			versionsBox.slideUp({ "queue": false, "duration": 300, "easing": "linear" });
		}
	});
}