var initDynamicFeatures = function(url) {
	$(".dynamic-table-data").on("click", ".alert-title", function() {
		var element = $(this);
		var alertId = $(this).parent().data("id");
		var box = element.next(".alert-details");

		if(element.hasClass("not-extended")) {
			if($(".extended").length) {
				var alreadyShown = $(".extended").parent().children(".alert-details");
				alreadyShown.slideUp({ "queue": false, "duration": 300, "easing": "linear"  });
				$(".extended").removeClass("extended").addClass("not-extended");
			}

			element.removeClass("not-extended");
			element.addClass("extended");

			if(box.is(":empty")) {
				$.get(url+"/get-details", { id: alertId }, function(data) {
					box.html(data);
				}).done(function() {
					box.slideDown({ "queue": false, "duration": 300, "easing": "linear" });
				});
			}
			else {
				box.slideDown({ "queue": false, "duration": 300, "easing": "linear" });
			}
		}
		else {
			element.removeClass("extended");
			element.addClass("not-extended");

			box.slideUp({ "queue": false, "duration": 300, "easing": "linear" });
		}
	});
};
