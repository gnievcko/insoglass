var attributes = [];
var detailsProductionPath = false;
var clickedNodes;
var nodeWithAttributes = [];
var network = null;
var locale = 'en';
var productionPathId = null;
var getAttributePath = null;
var getPathStepPath = null;
var getActivityPath = null;
var saveStepPath = null;
var locales = {
    pl: {
        edit: 'Edytuj',
        del: 'Usuń zaznaczone',
        back: 'Cofnij',
        addNode: 'Dodaj węzeł',
        addEdge: 'Dodaj krawędź',
        editNode: 'Edytuj węzeł',
        editEdge: 'Edytuj krawędź',
        addDescription: 'Kliknij w wolne miejsce aby dodać nowy węzeł',
        edgeDescription: 'Kliknij na węzeł i przeciągnij krawędź do innego węzła, aby je połączyć.',
        editEdgeDescription: 'Kliknij w punkt i przeciągnij go do węzła, aby je połączyć.',
        createEdgeError: 'Nie można powiązać krawędzi z klastrem.',
        deleteClusterError: 'Klastry nie mogą być usunięte.',
        editClusterError: 'Klastry nie mogą być edytowane.'
    },
    en: {
        edit: 'Edit',
        del: 'Delete selected',
        back: 'Back',
        addNode: 'Add Node',
        addEdge: 'Add Edge',
        editNode: 'Edit Node',
        editEdge: 'Edit Edge',
        addDescription: 'Click in an empty space to place a new node.',
        edgeDescription: 'Click on a node and drag the edge to another node to connect them.',
        editEdgeDescription: 'Click on the control points and drag them to a node to connect to it.',
        createEdgeError: 'Cannot link edges to a cluster.',
        deleteClusterError: 'Clusters cannot be deleted.',
        editClusterError: 'Clusters cannot be edited.'
    }
};

var nodes = new vis.DataSet([]);
var edges = new vis.DataSet([]);

data = {nodes: nodes, edges: edges};

function init(lang, attributePath, pathStepPath, activityPath, savePath, id = null, details = false) {
    getAttributePath = attributePath;
    getPathStepPath = pathStepPath;
    getActivityPath = activityPath;
    saveStepPath = savePath;

    locale = lang;
    detailsProductionPath = details;
    if (id) {
        productionPathId = id;
        getNodesAndEdges(id);
    } else {
        draw();
    }

    if (!detailsProductionPath) {
        getActivities();
        getAttributes(id);
}
}

function getAttributes(id) {
    $.get(getAttributePath, {id: id}, function () {
    }).done(function (data) {

        var data = JSON.parse(data);
        attributes = data['attributes'];
        nodeWithAttributes = data['nodeWithAttributes'];

    }).fail(function (msg) {
//        console.log(msg);
    });
}

function getNodesAndEdges(id) {
    var tmp_nodes = [];
    var tmp_edges = [];

    $.get(getPathStepPath, {id: id}, function () {
    }).done(function (data) {
        var data = JSON.parse(data);

//        attributes = data['attributes'];
        var productionPath = data['productionPath'];
        $('#production-path-name').val(productionPath['name']);

        data = data['steps'];

        for (var d in data) {
            tmp_nodes.push({label: data[d]['activityName'], id: data[d]['id'], activityId: data[d]['activityId']});

            var successor = data[d]['successorIds'];
            var successorIds = successor != null ? successor.split(",") : [];
            for (var s in successorIds) {
                tmp_edges.push({from: data[d]['id'], to: successorIds[s], arrows: 'to'});
            }
        }

        nodes = new vis.DataSet(tmp_nodes);
        edges = new vis.DataSet(tmp_edges);

        var data = {
            nodes: nodes,
            edges: edges
        };

        draw(data);

    }).fail(function (msg) {
//        console.log(msg);
    });
}

function destroy() {
    if (network !== null) {
        network.destroy();
        network = null;
    }
}

function draw(dataPath = {nodes: nodes, edges: edges}) {
    data = dataPath;
    destroy();
    if (detailsProductionPath) {
        var manipulation = false;
        var interaction = {
            dragNodes: false,
            dragView: true
        };
    } else {
        var manipulation = {
            initiallyActive: true,
            addNode: false,
            addEdge: function (data, callback) {
                if (data.from != data.to) {
                    data.arrows = 'to';
                    callback(data);
                }
            }
        };

        var interaction = {};
    }

    var container = document.getElementById('production-path');
    var options = {
        locale: 'pl',
        locales: locales,
        layout: {randomSeed: 339899},
        interaction: interaction,
        manipulation: manipulation,
        physics: {
            enabled: false
        }
    };
    network = new vis.Network(container, data, options);

    network.on('click', function (properties) {
        clickNode(properties);
    });
}

function copyAttributesArray() {
    var groups = [];

    for (var g in attributes) {
        var attr = [];
        for (var a in attributes[g]['attributes']) {
            attr.push({'groupId': attributes[g]['attributes'][a]['groupId'], 'id': attributes[g]['attributes'][a]['id'], 'name': attributes[g]['attributes'][a]['name'], 'unblock': false, 'forbidden': false, 'needed': false});
        }

        var group = {id: attributes[g]['id'], name: attributes[g]['name'], attributes: attr};
        groups.push(group);
    }

    return groups;
}

function clickNode(properties) {
    var ids = properties.nodes;
    clickedNodes = nodes.get(ids);
    if (clickedNodes.length > 0) {
        if (typeof nodeWithAttributes[clickedNodes[0]['id']] == 'undefined') {
            nodeWithAttributes[clickedNodes[0]['id']] = copyAttributesArray();
        }
        generateAttributes(nodeWithAttributes[clickedNodes[0]['id']]);
    }
}

function generateAttributes(group) {
    var parent = $('#attributes');
    parent.empty();
    for (var g in group) {
        var div = $('<div />', {
            "class": "group col-sm-" + round(group.length),
        }).appendTo(parent);

        $('<div />', {
            id: 'group-' + group[g]['id'],
            "class": "attribute-title row",
            text: group[g]['name']
        }).appendTo(div);

        var attr = group[g]['attributes'];

        if (attr.length > 0) {
            var icons = $('<div />', {
                "class": "row"
            }).appendTo(div);

            var divIcon = $('<div />', {
                "class": "col-xs-1",
            }).appendTo(icons);

            $('<span />', {
                "class": "glyphicon glyphicon-ok",
                'title': 'Atrybut odblokowujący'
            }).appendTo(divIcon);

            var divIcon = $('<div />', {
                "class": "col-xs-1",
            }).appendTo(icons);

            $('<span />', {
                "class": "glyphicon glyphicon-exclamation-sign",
                'title': 'Atrybut wymagany'
            }).appendTo(divIcon);

            var divIcon = $('<div />', {
                "class": "col-xs-1",
            }).appendTo(icons);

            $('<span />', {
                "class": "glyphicon glyphicon-remove",
                'title': 'Atrybut blokujący'
            }).appendTo(divIcon);
        }

        for (var a in attr) {

            var row = $('<div />', {
                id: 'attribute-' + attr[a]['id'],
                "class": "row"
            }).appendTo(div);

            $('<input />', {
                "class": "col-xs-1 unblock",
                type: "checkbox",
                checked: attr[a]['unblock']
            }).appendTo(row);

            $('<input />', {
                "class": "col-xs-1 needed",
                type: "checkbox",
                disabled: attr[a]['needed'] == true  || attr[a]['unblock'] == true ? false : true,
                checked: attr[a]['needed']
            }).appendTo(row);

            $('<input />', {
                "class": "col-xs-1 forbidden",
                type: "checkbox",
                checked: attr[a]['forbidden']
            }).appendTo(row);

            $('<div />', {
                "class": "col-xs-9",
                text: attr[a]['name']
            }).appendTo(row);
        }
    }
}

$(document).on('click', '.unblock', function () {
    var parent = $(this).parents('.row').first();
    var group = parent.parents('.group').first();
    group = group.find('.attribute-title');
    var needed = parent.find('.needed');
    var forbidden = parent.find('.forbidden');

    var attributeId = (parent.attr('id')).split("-")[1];
    var groupId = (group.attr('id')).split("-")[1];

    checkOptionAttribute(attributeId, groupId, clickedNodes[0]['id'], 'unblock', $(this).is(':checked'));

    if ($(this).is(':checked')) {
        needed.removeAttr('disabled');
        forbidden.prop('checked', false);
        checkOptionAttribute(attributeId, groupId, clickedNodes[0]['id'], 'forbidden', false);
    } else {
        needed.prop('checked', false);
        checkOptionAttribute(attributeId, groupId, clickedNodes[0]['id'], 'needed', false);
        needed.attr('disabled', 'disabled');
    }
});

$(document).on('click', '.needed', function () {
    var parent = $(this).parents('.row').first();
    var group = parent.parents('.group').first();
    group = group.find('.attribute-title');

    var attributeId = (parent.attr('id')).split("-")[1];
    var groupId = (group.attr('id')).split("-")[1];

    checkOptionAttribute(attributeId, groupId, clickedNodes[0]['id'], 'needed', true);
});

$(document).on('click', '.forbidden', function () {
    var parent = $(this).parents('.row').first();
    var group = parent.parents('.group').first();
    group = group.find('.attribute-title');
    var needed = parent.find('.needed');
    var unblock = parent.find('.unblock');

    var attributeId = (parent.attr('id')).split("-")[1];
    var groupId = (group.attr('id')).split("-")[1];

    checkOptionAttribute(attributeId, groupId, clickedNodes[0]['id'], 'forbidden', $(this).is(':checked'));

    if ($(this).is(':checked')) {
        needed.prop('checked', false);
        checkOptionAttribute(attributeId, groupId, clickedNodes[0]['id'], 'needed', false);
        unblock.prop('checked', false);
        checkOptionAttribute(attributeId, groupId, clickedNodes[0]['id'], 'unblock', false);
        needed.attr('disabled', 'disabled');
    }
});

function checkOptionAttribute(attributeId, groupId, nodeId, option, check) {
    var groups = nodeWithAttributes[nodeId];

    for (var g in groups) {
        if (groups[g]['id'] == groupId || (typeof (groups[g]['id']) == 'undefined' && groupId == 'undefined')) {
            var attr = groups[g]['attributes'];
            for (var a in attr) {
                if (attr[a]['id'] == attributeId) {
                    attr[a][option] = check;
                }
            }
        }
    }

    nodeWithAttributes[nodeId] = groups;
}

function getActivities(params = {term: '', fullSize: 1}) {
    $.get(getActivityPath, params, function () {
    }).done(function (data) {
        var data = JSON.parse(data);
        var items = data.items;
        var parent = $('#activities');
        parent.empty();

        for (var i in items) {
            var li = $('<li />', {
                "value": items[i]['id'],
                "class": "activity-item",
                text: items[i]['name']
            }).appendTo(parent);
        }
    }).fail(function (msg) {
        console.log(msg);
    });
}

function searchStepSuccessor(nodeId, edges) {
    var edgesTo = [];

    for (var e in edges) {
        if (edges[e]['from'] == nodeId) {
            edgesTo.push(edges[e]['to']);
        }
    }

    return edgesTo.join();
}

function searchStepRequired(nodeId, edges) {
    var edgesFrom = [];

    for (var e in edges) {
        if (edges[e]['to'] == nodeId) {
            edgesFrom.push(edges[e]['from']);
        }
    }

    return edgesFrom.join();
}

function rand() {
    var n = Math.random() * 1e17;
    return (n + "").substr(1, 16);
}

function round(number) {
    if (number < 5) {
        return 12 / number;
    } else if (number < 7) {
        return 2;
    } else {
        return 1;
    }
}

$('#search-activity').on('change', function () {
    getActivities({term: $(this).val(), fullSize: 1});
});

$('#saveNetworkButton').on('click', function () {
    var pathName = $('#production-path-name').val();
    var nodes = data.nodes._data;
    var edges = data.edges._data;
    var new_nodes = [];
    var attr = [];

    for (var n in nodeWithAttributes) {
        var needed = '';
        var unblock = '';
        var forbidden = '';

        for (var g in nodeWithAttributes[n]) {
            for (var a in nodeWithAttributes[n][g]['attributes']) {
                if (nodeWithAttributes[n][g]['attributes'][a]['needed'] == true) {
                    if (needed == undefined || needed.search(nodeWithAttributes[n][g]['attributes'][a]['id']) == -1) {
                        needed = needed != '' ? needed + ',' + nodeWithAttributes[n][g]['attributes'][a]['id'] : nodeWithAttributes[n][g]['attributes'][a]['id'];
                    }
                }
                if (nodeWithAttributes[n][g]['attributes'][a]['unblock'] == true) {
                    if (unblock == undefined || unblock.search(nodeWithAttributes[n][g]['attributes'][a]['id']) == -1) {
                        unblock = unblock != '' ? unblock + ',' + nodeWithAttributes[n][g]['attributes'][a]['id'] : nodeWithAttributes[n][g]['attributes'][a]['id'];
                    }
                }
                if (nodeWithAttributes[n][g]['attributes'][a]['forbidden'] == true) {
                    if (forbidden == undefined || forbidden.search(nodeWithAttributes[n][g]['attributes'][a]['id']) == -1) {
                        forbidden = forbidden != '' ? forbidden + ',' + nodeWithAttributes[n][g]['attributes'][a]['id'] : nodeWithAttributes[n][g]['attributes'][a]['id'];
                    }
                }
            }
        }
        attr.push({id: n, 'needed': needed, 'unblock': unblock, 'forbidden': forbidden});
    }

    for (var n in nodes) {
        var activityId = nodes[n]['id'].split("-");
        new_nodes.push({id: nodes[n]['id'], activityId: nodes[n]['activityId'], successor_ids: searchStepSuccessor(nodes[n]['id'], edges), required_ids: searchStepRequired(nodes[n]['id'], edges)});
    }


    if (validationPathName(pathName)) {
        $.post(saveStepPath, {nodes: new_nodes, pathName: pathName, productionPathId: productionPathId, attributes: attr}, function () {
        }).done(function (data) {
            console.log('save');
        }).fail(function (msg) {
//            console.log('fail');
        });
    }
});

function validationPathName(pathName) {
    if (pathName != '') {
        return true;
    }

    var errorMsg = $('<span />', {
        "class": "text-danger",
        text: 'Nazwa scieżki produkcyjnenj nie może pozostać bez wartości!'
    });

    $('#production-path-name').after(errorMsg);
    return false;
}
$(document).on('click', '.activity-item', function () {
    var id = rand();
    var node = {id: id, label: $(this).text(), x: 200, activityId: $(this).val()};
    nodes.add(node);
});

