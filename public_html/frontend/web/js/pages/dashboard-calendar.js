var urlJsonCalendar, urlDetails, urlShortOrder;
function init(jsonCalendar, shortOrder, details) {
    urlJsonCalendar = jsonCalendar;
    urlDetails = details;
    urlShortOrder = shortOrder;
    createCalendar();
}

$('#short-order-date').on('change', function () {
    var val = $(this).find(":selected").val();
    $.ajax({
        url: urlJsonCalendar,
        dataType: 'json',
        data: {dateType: val},
        success: function (doc) {
            var events = [];
            $(doc).each(function () {


                events.push({
                    title: $(this).attr('title'),
                    start: $(this).attr('start'),
                    end: $(this).attr('end'),
                    id: $(this).attr('id')
                });
            });


            $('#calendar').fullCalendar('removeEvents');
            $('#calendar').fullCalendar('addEventSource', events);
        }
    });
});
function createCalendar(url) {
    $('#calendar').fullCalendar({
        options: {
            lang: 'pl',
            id: 'calendar'
        },
        header: {
            left: 'prev',
            center: 'title',
            right: 'next'
        },
        clientOptions: {
            contentHeight: 'auto',
            views: {
                month: {
                    columnFormat: 'dd',
                    titleFormat: 'MMMM',
                    timeFormat: 'HH:mm',
                    displayEventTime: true
                }
            }
        },
        eventClick: function (calEvent, jsEvent, view) {
            var val = $('#short-order-date').find(":selected").val();
            $(".fc-event").each(function () {
            	$(this).removeClass("fc-event-selected");
                //$(this).css("background-color", "#e95546");
                //$(this).css("color", "#fff");
            });
            $(this).addClass("fc-event-selected");            
            //$(this).css("background-color", "#fff");
            //$(this).css("color", "#e95546");
            $.get(urlShortOrder, {date: calEvent.id, dateType: val}, function () {
            }).done(function (data) {
                var data = $.parseJSON(data);
                var parent = $("#short-order");
                if (data.length > 0) {
                    parent.empty();
                }

                for (var key in data) {
                    var div = $("<div/>", {
                        class: "short-order-div"
                    }).appendTo(parent);
                    var title = $("<div/>", {
                        text: "#" + data[key]["id"] + " - " + data[key]["title"],
                        class: "short-order-title"
                    }).appendTo(div);
                    var redPoint = $("<div/>", {
                    	text: '&',
                        class: "short-order-point"
                    }).appendTo(title);
                    var description = $("<div/>", {
                        text: data[key]["description"],
                        class: "short-order-description",
                    }).appendTo(div);
                    var link = $("<a/>", {
                        href: urlDetails + '/' + data[key]["id"],
                        text: "Więcej",
                        target: "_blank",
                        class: "short-order-link"
                    }).appendTo(div);
                }
            }).fail(function (msg) {
            });
        },
        events: function (start, end, timezone, callback) {
            var val = $('#short-order-date').find(":selected").val();
            $.ajax({
                url: urlJsonCalendar,
                dataType: 'json',
                data: {dateType: val},
                success: function (doc) {
                    var events = [];
                    $(doc).each(function () {
                        events.push({
                            title: $(this).attr('title'),
                            start: $(this).attr('start'),
                            end: $(this).attr('end'),
                            id: $(this).attr('id')
                        });
                    });

                    $('#calendar').fullCalendar('removeEvents');
                    callback(events);
                }
            });
        }
    });
};