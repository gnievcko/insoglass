$(document).ready(function(){
	$(document).on("click", ".tr-show-modal:not(.modal)", function(event){
		var id = $(this).attr("data-target");
		if(!$(this).hasClass('inactive-row')) {
			if($(event.target).parents(".modal").length == 0 && !$(event.target).hasClass("modal")) {
				$(id).modal("show");
	        }
		}
    });
	
	$(document).on("click", ".change-value", function(event){
		event.preventDefault();
		
		var $difference = 0;
		var $button = $(this);
		var $field = $button.parent().siblings().find(".completed-count");
		
		if($button.hasClass("add-1")) {
			$difference = 1;
		} else if($button.hasClass("add-10")) {
			$difference = 10;
		} else if($button.hasClass("remove-1")) {
			$difference = -1;
		} else if($button.hasClass("remove-10")) {
			$difference = -10;
		}
		if ($field.val().length == 0) {
			$field.val(0);
		}
		
		$field.val(parseInt($field.val()) + $difference);
		
	});
/*	
	function toggleClass() {
	    if($(window).width() <= 992) {
	        $(".list-table > table > tbody > tr").each(function() {
	            $(this).addClass("tr-show-modal");
	        });
	    } else {
	        $(".list-table > table > tbody > tr").each(function() {
	            $(this).removeClass("tr-show-modal");
	        });
	    }
	}

	toggleClass();

	$( window ).resize(function() {
	    toggleClass();
	});
*/	
});    
