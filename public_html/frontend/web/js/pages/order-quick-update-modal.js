$(function() {

	$('.modalButtons').on('click', '#orderQuickUpdateButton', function() {
		var dateDeadline = $("#dateDeadlineId-disp-kvdate").kvDatepicker("getDate")
		var dateReminder = $("#dateReminderId-disp-kvdate").kvDatepicker("getDate")
		var showAlert = false;

		if(dateReminder != null) {
			if (dateReminder < Date.now()) {
				showAlert = true;
			}
			else if(dateDeadline != null) {
				if(dateReminder > dateDeadline) {
					showAlert = true;
				}
			}
		}
		
		if(showAlert) {
			alert(window.alertMessage);
			return false;
		}
		
	});
});