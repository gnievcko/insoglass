function repair(sectionClass, settings) {
    var rowUrl = settings.rowUrl;
    var edit = settings.edit;
    var uniq = settings.uniq || null;
    var startIdx = settings.startIdx ? settings.startIdx : null;
    
    var initializeTable = function ($section) {

        var $tableBody = $section.find('.table-body');
        var $ajaxLoader = $section.find('.ajax-loader:first');
        var $form = $section.closest('form');

        var showLoader = function () {
            //$tableBody.hide();
            $ajaxLoader.show();
        }

        var hideLoader = function () {
            //$tableBody.show();
            $ajaxLoader.hide();
        }

        var reinitializeValidation = function () {
            activateValidationForProduct($tableBody.find('.document-table-row'));
        }

        var addButtons = $section.find('.add-row-button');
        addButtons.on('click', function () {
            showLoader();

            $.get(rowUrl, {edit: edit, uniq: uniq, idx: startIdx}, function (product) {
                var $lastProduct = $tableBody.find('.document-table-row:last');
                $product = $(product);

                if ($lastProduct.size() === 0) {
                    $tableBody.append($product);
                } else {
                    $lastProduct.after($product);
                }

                activateValidationForProduct($product);
                initializeRemoveBtn($product.find('.remove-row-btn:first'));
                startIdx++;
            }).done(hideLoader);
        });

        var activateValidationForProduct = function ($product) {
            $product.find('input, select, textarea').each(function (idx, formControl) {
                var $formControl = $(formControl);
                var controlId = $formControl.attr('id');

                $form.yiiActiveForm('add', {
                    id: controlId,
                    name: $formControl.attr('name'),
                    container: '.field-' + controlId,
                    input: '#' + controlId,
                    enableClientValidation: false,
                    enableAjaxValidation: true
                });
            });
        }

        var initializeRemoveButtons = function () {
            initializeRemoveBtn($tableBody.find('.remove-row-btn'));
        };
        var initializeRemoveBtn = function ($removeBtn) {
            $removeBtn.on('click', function () {
                $(this).closest('.document-table-row').remove();
            });
        };
        initializeRemoveButtons();


        var initializeSelects = function () {
            initializeSelect($tableBody.find('.unit-select'));
        };
    }

    $('.' + sectionClass).each(function (idx, section) {
        initializeTable($(section));
    })
}
