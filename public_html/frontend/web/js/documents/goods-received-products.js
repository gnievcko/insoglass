function initializeGoodsReceivedProductsTables(sectionClass, settings) {
	var calculationsUrl = settings.calculationsUrl;
    var productRowUrl = settings.productRowUrl;
    var edit = settings.edit;

    var initializeProductsSection = function ($section) {

        var $tableBody = $section.find('tbody:first');
        var $ajaxLoader = $section.find('.ajax-loader:first');
        var $form = $section.closest('form');

        var showLoader = function() {
            $tableBody.hide();
            $ajaxLoader.show();
        }

        var hideLoader = function() {
            $tableBody.show();
            $ajaxLoader.hide();
        }

        var recalculateValues = function() {
            var dataToSend = $section.find('input, textarea, select').serialize();
            showLoader();

            $.post(calculationsUrl, dataToSend, function(response) {
                var $response = $(response);
                $tableBody.html($response);
                initializeRemoveButtons();
                initializeInputs();
                initializeSelects();
                reinitializeValidation();
            }).done(hideLoader);
        }

        var reinitializeValidation = function() {
            activateValidationForProduct($tableBody.find('.goods-received-product'));
        }

        var addProductButtons = $section.find('.add-goods-received-product-button');
        addProductButtons.on('click', function() {
            showLoader();

            $.get(productRowUrl, { edit: edit, idx: $tableBody.find('.goods-received-product').size() }, function(product) {
                var $lastGoodsReceivedProduct = $tableBody.find('.goods-received-product:last');
                $product = $(product);

                if($lastGoodsReceivedProduct.size() === 0) {
                    $tableBody.append($product);
                }
                else {
                    $lastGoodsReceivedProduct.after($product);
                }

                activateValidationForProduct($product);

                initializeRemoveBtn($product.find('.remove-goods-received-product-btn:first'));
                initializeInput($product.find('.recalculates'));
            }).done(hideLoader);
        });

        var activateValidationForProduct = function($product) {
            $product.find('input, select, textarea').each(function(idx, formControl) {
                var $formControl = $(formControl);
                var controlId = $formControl.attr('id');

                $form.yiiActiveForm('add', {
                    id: controlId,
                    name: $formControl.attr('name'),
                    container: '.field-' + controlId,
                    input: '#' + controlId,
                    enableClientValidation: false,
                    enableAjaxValidation: true
                });
            });
        }

        var initializeRemoveButtons = function() {
            initializeRemoveBtn($tableBody.find('.remove-goods-received-product-btn'));
        };
        var initializeRemoveBtn = function($removeBtn) {
            $removeBtn.on('click', function() {
                $(this).closest('.goods-received-product').remove();
                recalculateValues();
            });
        };
        initializeRemoveButtons();

        var initializeInputs = function() {
            initializeInput($tableBody.find('.recalculates'));
        };
        var initializeInput = function($input) {
            $input.on('change', recalculateValues);
        }
        initializeInputs();
        
        var initializeSelects = function() {
        	initializeSelect($tableBody.find('.unit-select'));
        };
        var initializeSelect = function($select) {
        	$select.on('change', recalculateValues);
        }
        initializeSelects();
    }

    $('.' + sectionClass).each(function(idx, section) {
        initializeProductsSection($(section));
    })
}

