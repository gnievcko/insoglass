var initializeOrderConfirmationProductsTables = function(settings) {

    var productRowUrl = settings.productRowUrl;
    var currencySymbol = settings.initialCurrencySymbol;
    var totalGrossPriceUrl = settings.totalGrossPriceUrl;
    
    var $form = $('.order-confirmation-products:first').closest('form');
    var productsIdSequence = $form.find('.order-confirmation-product').size();

    var initializeTable = function($section) {
    	
    	var $tableBody = $section.find('tbody:first');
    	var $currencySelect = $section.find('.currency-select:first');
        
    	$summary = $section.find('.order-confirmation-table-summary:first');
        $currencySelect.on('change', function() {
            var selectedCurrencySymbol = $currencySelect.find('option:selected').text();
            currencySymbol = selectedCurrencySymbol;
            
            $section.find('.order-confirmation-product').each(function(_, productRow) {
            	recalculateProductRowValues($(productRow));
            });
        });
        var recalculateSummary = function() {
            $.post(settings.summaryUrl, $tableBody.find('input, select').serialize(), 
            function(summary) {
            	$summary.html(summary);
            });
        };
        var addProductButtons = $section.find('.add-order-confirmation-product-button');
        addProductButtons.on('click', function() {
            $.get(productRowUrl, { prefix: productsIdSequence++, currencySymbol: currencySymbol }, function(product) {
                var $lastOrderConfirmationProduct = $tableBody.find('.order-confirmation-product:last');
                $product = $(product);

                if($lastOrderConfirmationProduct.size() === 0) {
                    $tableBody.prepend($product);
                }
                else {
                	$lastOrderConfirmationProduct.after($product);
                }

                activateValidationForProduct($product);

                initializeRemoveBtn($product.find('.remove-order-confirmation-product-btn:first'));
                initializeProductRow($product);
            });
        });
        var recalculateProductRowValues = function($productRow){
            $.get(totalGrossPriceUrl, {
                priceUnit: $productRow.find('.order-confirmation-product-price:first').val(), 
                count: $productRow.find('.order-confirmation-product-count:first').val(),
                vatRate: $productRow.find('.vat-rate-select').find('option:selected').val()
            }, 
            function(result) {
            	$productRow.find('.gross-price-value').text(result.totalGrossPrice + " " + currencySymbol);
            });
        }
        
        var activateValidationForProduct = function($product) {
            $product.find('input, select, textarea').each(function(idx, formControl) {
                var $formControl = $(formControl);
                var controlId = $formControl.attr('id');

                $form.yiiActiveForm('add', {
                    id: controlId,
                    name: $formControl.attr('name'),
                    container: '.field-' + controlId,
                    input: '#' + controlId,
                    enableClientValidation: false,
                    enableAjaxValidation: true
                });
            });
        };
        var initializeProductRow = function($productRow) {
            $productRow.find('.recalculates').on('change', function() {
            	recalculateProductRowValues($productRow);
            	recalculateSummary();
            });
        };
        $section.find('.order-confirmation-product').each(function(_, productRow) {
        	initializeProductRow($(productRow));
        });
        var initializeRemoveButtons = function() {
            initializeRemoveBtn($tableBody.find('.remove-order-confirmation-product-btn'));
        };
        var initializeRemoveBtn = function($removeBtn) {
            $removeBtn.on('click', function() {
                $(this).closest('.order-confirmation-product').remove();
                recalculateSummary();
            });
        };
        initializeRemoveButtons();

    }
    $('.order-confirmation-products').each(function(_, table) {
        initializeTable($(table));
    });
}