function initializeAddTable(bodyClass, settings) {
	var rowUrl = settings.rowUrl;
	var edit = settings.edit;
    var sectionClass = settings.sectionClass;
	var $body = $('.'+bodyClass);
	var limitToSingle = settings.limitToSingle ? settings.limitToSingle : false;
	var tableViewsDir = settings.tableViewsDir ? settings.tableViewsDir : null;
	
	var $customDataTable = null;
        
	function fixWidthHelper(e, ui) {
	    ui.children().each(function() {
	        $(this).width($(this).width());
	    });
	    return ui;
	}
    
	if($body.children().length > 0) {
		$body.sortable({
		    helper: fixWidthHelper
		});
		
		if(limitToSingle) {
			$customDataTable = $body.children()[0];
    		$('.add-table-button').attr('disabled', 'disabled');
    		$('.column-count-block').css('visibility', 'hidden');
    		$('.remove-single-table-button').removeAttr('disabled');
    		$('#ordertableform-isvisible').val(1);
		}
	}
        
	$('.'+sectionClass).on('click', '.add-table-button', function() {
		if(limitToSingle && $customDataTable) {
			return false;
		}
			
        var count = parseInt($('.th-count').val()) + 1;
        $.get(rowUrl, {edit: edit, count: count, tableViewsDir: tableViewsDir, predefinedHeaders: limitToSingle ? 1 : 0}, function(data) {
	    	$body.append(data);
	    	if(limitToSingle) {
	    		$customDataTable = data;
	    		$('.add-table-button').attr('disabled', 'disabled');
	    		$('.column-count-block').css('visibility', 'hidden');
	    		$('.remove-single-table-button').removeAttr('disabled');
	    		$('#ordertableform-isvisible').val(1);
	    	}
	    });
	});
	
	$body.on('click', '.copy-table-button', function() {
		if(limitToSingle && $customDataTable) {
			return false;
		}
		
		var copiedTable = $(this).closest('.document-table');
		var count = parseInt($('.th-count').val()) + 1;
		var initData = copiedTable.find('[name^="tables"]').serialize();
		var copyOption = $(this).closest('.document-table').find('[name="dropdown-table-copy-mode"]').val();
		
        $.get(rowUrl, {edit: edit, count: count, tableViewsDir: tableViewsDir, predefinedHeaders: limitToSingle ? 1 : 0, initData: initData, copyOption: copyOption}, function(data) {
	    	//$body.append(data);
        	copiedTable.after(data);
        });
	});
        
	$body.on('click', '.remove-table', function () {
		$(this).closest('.document-table').remove();
    });
	
	$('.remove-single-table-button').click(function() {
		$('.document-table').remove();
		$customDataTable = null;
		$('.add-table-button').removeAttr('disabled');
		$('.column-count-block').css('visibility', 'visible');
		$('.remove-single-table-button').attr('disabled', 'disabled');
		$('#ordertableform-isvisible').val(0);
	});
	
}