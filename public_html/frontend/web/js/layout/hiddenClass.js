$(function () {
    $('.required').each(function () {
        $(this).has('input[type="hidden"]').filter(':not(:has(select))').addClass('hasHidden');
        $(this).has('input[type!="hidden"]').removeClass('hasHidden');
    });
});
