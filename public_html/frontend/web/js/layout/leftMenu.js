$(function()	{

	$('.more').each(function()	{
		$(this).parents('li').append('<div class="moreButton"><div class="caret-icon-img"></div></>');
		$(this).parents('li').children('a').attr('onclick', 'return false');
	});

	/*$('.dropdown-slide').click(function(){
		$('.dropdown-menu-top').slideToggle(300);
	});*/

		$('.pasek').on('click', '.navbar-tile', function(){
			if ($(window).width() <= 767) {
				$('.navbar-tile').removeClass('active2');
				$(".more").fadeOut(300);
				if(($(this).children('.more').css('display') != 'block')) {
					$(this).children('.more').fadeIn(300);
					$(this).addClass('active2');
				}
			}
			else {
				if(($(this).children('.more').css('display') != 'block')) {
					$(this).children('.more').fadeIn(300);
				}
			}
		});

		$(document).click(function (e) {
			if ($(window).width() <= 767) {
				var container = $(".navbar-tile");
				var container2 = $(".more");
				if (container.has(e.target).length === 0 && container2.has(e.target).length === 0) {
					container2.fadeOut(300);
					container2.parent('li').removeClass('active2');
				}
			}
		});

		$( window ).resize(function() {
			$('.navbar-tile').removeClass('active2');
			$(".more").attr('style', '');
		});
})
