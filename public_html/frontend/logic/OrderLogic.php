<?php

namespace frontend\logic;
use common\helpers\Utility;

class OrderLogic {
	
	public function processOrder($orders) {
		if(empty($orders)) {
			return $orders;
		}
        
        $today = new \DateTime(); 
		
		foreach($orders as &$order) {
			if(new \DateTime($order['dateDeadline']) <= $today) {
				$order['isWarning'] = true;
			}
			else {
				$order['isWarning'] = false;
			}
		}
		
		return $orders;
	}
	
}
