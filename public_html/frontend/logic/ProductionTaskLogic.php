<?php

namespace frontend\logic;

use Yii;
use common\helpers\Utility;
use common\models\aq\OrderOfferedProductQuery;
use yii\helpers\Json;
use common\models\aq\ProductionPathQuery;
use common\models\aq\ProductionPathStepQuery;
use common\models\aq\ProductAttributeTypeQuery;
use common\helpers\ArrayHelper;
use common\models\ar\Task;
use common\models\ar\ProductionTask;
use common\models\ar\Order;
use common\models\ar\OrderOfferedProductAttachment;
use common\models\ar\OrderOfferedProductAttribute;
use common\models\ar\OfferedProduct;
use common\models\aq\TaskTypeQuery;
use common\helpers\StringHelper;
use common\models\aq\TaskPriorityQuery;
use yii\db\Expression;
use common\models\ar\OrderStatus;
use frontend\models\ProductAttributeForm;
use common\models\ar\OrderOfferedProduct;

class ProductionTaskLogic {

    public function generateTasks($orderId, $forceDefaultProductionPath = true) {
        if(empty($orderId)) {
            throw new \Exception('Parameter "orderId" was not provided');
        }
        
        $transaction = Yii::$app->db->beginTransaction();
        
        try {
            $order = Order::findOne($orderId);
            if(empty($order) || $order->orderHistoryLast->orderStatus->symbol != Utility::ORDER_STATUS_IN_PROGRESS) {
                throw new \Exception('Order is not available or is in incorrect status');
            }
            
            $products = OrderOfferedProductQuery::getProductsForProductionByOrderId($orderId);
            if(empty($products)) {
                throw new \Exception('There are no products for this order');
            }
            
            $defaultPathData = $this->getPathData();
            $taskTypeId = TaskTypeQuery::getBySymbol(Utility::TASK_TYPE_PRODUCTION);
            $taskPriorityIds = \yii\helpers\ArrayHelper::map(
                    TaskPriorityQuery::getBySymbol([Utility::TASK_PRIORITY_MEDIUM, Utility::TASK_PRIORITY_HIGH]), 
                    'symbol', 'id');
            
            foreach($products as $product) {
                $attributeIds = [];
                if(!empty($product['attributeValue'])) {
                    $attributeIds = array_column(Json::decode($product['attributeValue'], true), 'id');
                }
                
                if(!$forceDefaultProductionPath && !empty($product['productionPathId'])) {
                    $pathData = $this->getPathData($product['productionPathId']);
                }
                else {
                    $pathData = $defaultPathData;
                }
                
                $topologicalOrder = explode(',', $pathData['path']['topologicalOrder']);
                $productionTasks = [];
                $allStepsInTasks = [];
                
                for($i = 0; $i < count($topologicalOrder); ++$i) {
                    $step = $pathData['steps'][$topologicalOrder[$i]];
                    
                    if(!$this->isAttributesSatisfied($step, $attributeIds)) {
                        continue;
                    }
                    
                    $task = new Task();
                    $task->task_type_id = $taskTypeId['id'];
                    $task->task_priority_id = !empty($order->orderPriority) && $order->orderPriority->symbol == Utility::ORDER_PRIORITY_HIGH 
                            ? $taskPriorityIds[Utility::TASK_PRIORITY_HIGH] : $taskPriorityIds[Utility::TASK_PRIORITY_MEDIUM];
                    $task->user_id = Yii::$app->user->id;
                    $task->title = Yii::t('main', 'Production task');
                    $task->message = StringHelper::translateOrderToOffer('main', 'Order').': '.$order->number."\n"
                            .Yii::t('main', 'Product').': '.$product['name']."\n"
                            .Yii::t('main', 'Activity').': '.$step['activityName']; 
                    if(!$task->save()) {
                        throw new \Exception('Task cannot be saved: '.print_r($task->getErrors(), true));
                    }
                    
                    $prodTask = new ProductionTask();
                    $prodTask->task_id = $task->id;
                    $prodTask->order_offered_product_id = $product['id'];
                    $prodTask->production_path_step_id = $step['id'];
                    if(!$prodTask->save()) {
                        throw new \Exception('Production task cannot be saved: '.print_r($prodTask->getErrors(), true));
                    }
                    
                    $task->title .= '#'.$prodTask->id.': '.$step['activityName'];
                    $task->save();
                    
                    $productionTasks[] = $prodTask;
                    $allStepsInTasks[] = $step['id'];
                }
                
                for($j = 0; $j < count($productionTasks); ++$j) {
                    $prodTask = $productionTasks[$j];
                    
                    $requiredIds = $this->getActuallyRequiredSteps($prodTask->production_path_step_id, $allStepsInTasks, $pathData);
                    if(!empty($requiredIds)) {
                        $requiredIds = $this->normalizeRequiredSteps($requiredIds, $allStepsInTasks, $pathData);
                    }
                    $requiredTaskIds = $this->getTaskIdsForStepIds(array_unique($requiredIds), $productionTasks);
                    $prodTask->task_required_ids = !empty($requiredTaskIds) ? implode(',', $requiredTaskIds) : null;
                    $prodTask->is_available = empty($requiredIds) || $this->isTasksOnlyNodes($requiredTaskIds) ? 1 : 0;
                    
                    $successorIds = $this->getActuallySuccessorSteps($prodTask->production_path_step_id, $allStepsInTasks, $pathData);
                    if(!empty($successorIds)) {
                        $successorIds = $this->normalizeSuccessorSteps($successorIds, $allStepsInTasks, $pathData);
                    }
                    $successorTaskIds = $this->getTaskIdsForStepIds(array_unique($successorIds), $productionTasks);
                    $prodTask->task_successor_ids = !empty($successorTaskIds) ? implode(',', $successorTaskIds) : null;        
                    
                    if(!$prodTask->save()) {
                        throw new \Exception('Production task cannot be saved: '.print_r($prodTask->getErrors(), true));
                    }
                }
            }
            
            $transaction->commit();
            Yii::$app->getSession()->setFlash('info', Yii::t('web', 'Production tasks have been added to this order.'));
        } 
        catch(\Exception $e) {
            $transaction->rollBack();
            throw $e;
        }
    }    

    private function getPathData($id = null) {
        $path = null;
        if(!empty($id)) {
            $path = ProductionPathQuery::getById($id);
        }
        else {
            $path = ProductionPathQuery::getDefaultPath();   
        }        
        
        if(!empty($path)) {
            $steps = ProductionPathStepQuery::getByProductionPathId($path['id']);
            $attributes = ProductAttributeTypeQuery::getByProductionPathStepIds(array_column($steps, 'id'));
            $steps = ArrayHelper::transformToAssociativeArray($steps, 'id');
            
            foreach($steps as &$step) {
                $step['id'] = intval($step['id']);
                $step['activityId'] = intval($step['activityId']);
                $step['attributeNeededIds'] = !empty($step['attributeNeededIds']) ? explode(',', $step['attributeNeededIds']) : [];
                $step['attributeForbiddenIds'] = !empty($step['attributeForbiddenIds']) ? explode(',', $step['attributeForbiddenIds']) : [];
            }
            
            if(!empty($attributes)) {
                foreach($attributes as &$attribute) {
                    $entry = &$steps[$attribute['stepId']];
                    if(!array_key_exists('attributes', $entry)) {
                        $entry['attributes'] = [];
                    }
            
                    $entry['attributes'][] = intval($attribute['attributeId']);
                }
            }
            
            return [
                'path' => $path,
                'steps' => $steps,
            ];
        }
        
        return null;
    }
    
    private function isAttributesSatisfied($step, $productAttributeIds) {
        return empty($step['attributes']) || (
                    array_intersect($step['attributes'], $productAttributeIds)
                    && !array_intersect($step['attributeForbiddenIds'], $productAttributeIds)
                    && (empty($step['attributeNeededIds']) || $step['attributeNeededIds'] == array_intersect($step['attributeNeededIds'], $productAttributeIds))
                );
    }
    
    private function getActuallyRequiredSteps($stepId, $allStepsInTasks, $pathData, $isNotLimited = false) {
        $currentStep = $pathData['steps'][$stepId];
        if(empty($currentStep['requiredIds'])) {
            return [];
        }
        
        $requiredIds = array_map(function($x) { return (int)$x; }, explode(',', $currentStep['requiredIds']));
        if(empty($requiredIds)) {
            return [];
        }
        
        if($isNotLimited) {
            $result = $requiredIds;
            foreach($requiredIds as $rId) {
                $result = array_merge($result, $this->getActuallyRequiredSteps($rId, $allStepsInTasks, $pathData, $isNotLimited));
            }
        }
        else {
            $result = [];
            $commonIds = array_intersect($requiredIds, $allStepsInTasks);     
            foreach($requiredIds as $rId) {
                if(in_array($rId, $commonIds)) {
                    $result[] = $rId;
                }
                else {
                    $result = array_merge($result, $this->getActuallyRequiredSteps($rId, $allStepsInTasks, $pathData));
                }
            }
        }
        
        return $result;
    }
    
    private function getActuallySuccessorSteps($stepId, $allStepsInTasks, $pathData, $isNotLimited = false) {
        $currentStep = $pathData['steps'][$stepId];
        if(empty($currentStep['successorIds'])) {
            return [];
        }
        
        $successorIds = array_map(function($x) { return (int)$x; }, explode(',', $currentStep['successorIds']));
        if(empty($successorIds)) {
            return [];
        }
    
        if($isNotLimited) {
            $result = $successorIds;
            foreach($successorIds as $sId) {
                $result = array_merge($result, $this->getActuallySuccessorSteps($sId, $allStepsInTasks, $pathData, $isNotLimited));
            }
        }
        else {
            $result = [];
            $commonIds = array_intersect($successorIds, $allStepsInTasks);
            foreach($successorIds as $sId) {
                if(in_array($sId, $commonIds)) {
                    $result[] = $sId;
                }
                else {
                    $result = array_merge($result, $this->getActuallySuccessorSteps($sId, $allStepsInTasks, $pathData));
                }
            }
        }
    
        return $result;
    }
    
    private function normalizeRequiredSteps($requiredIds, $allStepsInTasks, $pathData) {
        $removedIds = [];
    
        foreach($requiredIds as $requiredId) {
            $dependencies = $this->getActuallyRequiredSteps($requiredId, $allStepsInTasks, $pathData, true);
            if(!empty($dependencies)) {
                foreach($dependencies as $dependency) {
                    if(in_array($dependency, $requiredIds)) {
                        $removedIds[] = $dependency;
                    }
                }
            }
        }
    
        return array_values(array_diff($requiredIds, $removedIds));
    }
    
    private function normalizeSuccessorSteps($successorIds, $allStepsInTasks, $pathData) {
        $removedIds = [];
    
        foreach($successorIds as $successorId) {
            $dependencies = $this->getActuallySuccessorSteps($successorId, $allStepsInTasks, $pathData, true);
            
            if(!empty($dependencies)) {
                foreach($dependencies as $dependency) {
                    if(in_array($dependency, $successorIds)) {
                        $removedIds[] = $dependency;
                    }
                }
            }
        }
            
        return array_values(array_diff($successorIds, $removedIds));
    }
    
    private function getTaskIdsForStepIds($stepIds, $productionTasks) {
        $taskIds = [];
        foreach($productionTasks as $prodTask) {
           if(in_array($prodTask->production_path_step_id, $stepIds)) {
               $taskIds[] = $prodTask->id;
           }
        }
        
        return $taskIds;
    }
    
    private function isTasksOnlyNodes($taskIds) {
        if(empty($taskIds)) {
            return false;
        }
        
        try {
            foreach($taskIds as $taskId) {
                $productionTask = ProductionTask::findOne($taskId);
                if(empty($productionTask->productionPathStep->activity->is_node)) {
                    return false;
                }
            }
            
            return true;
        }
        catch(\Exception $e) {
            throw new \Exception('Task checked in terms of being node has not correct structure');
        }
    }
   
    // TODO: To nie ma niczego wspólnego z modułem produkcyjnym - przenieść
    public static function updateOrderOfferedProductIds($old, $new) {       
        $oldCount = count($old);
        $newCount = count($new);
        $result = [];
        
        if($oldCount == $newCount) {
            for($i = 0; $i < $oldCount; $i++) {
                if(self::isSameOfferedProduct($old[$i], $new[$i])) {
                     $result[$old[$i]['id']] = $new[$i]['id'];
                     self::updateOrderOfferedProductIdInAllTables($old[$i]['id'], $new[$i]['id']);
                }
            }
        }
    }
    
    private static function isSameOfferedProduct($oldData, $newData) {
        $oldOfferedProduct = OfferedProduct::findOne($oldData['offered_product_id']);
        $newOfferedProduct = OfferedProduct::findOne($newData['offered_product_id']);
        
        return $oldData['order_id'] == $newData['order_id'] && (
                    $oldData['offered_product_id'] == $newData['offered_product_id'] ||
                    ($oldOfferedProduct->is_artificial && $newOfferedProduct->is_artificial)
                ) &&
                $oldData['count'] == $newData['count'] && 
                $oldData['price'] == $newData['price'];
    }
    
    private static function updateOrderOfferedProductIdInAllTables($old, $new) {
        ProductionTask::updateAll(['order_offered_product_id' => $new], ['order_offered_product_id' => $old]);
        //TODO sprawdzić czemu zmieniały się tylko dla kosztów nie dla produktów
        //OrderOfferedProductAttachment::updateAll(['order_offered_product_id' => $new], ['order_offered_product_id' => $old]);
        OrderOfferedProductAttribute::updateAll(['order_offered_product_id' => $new], ['order_offered_product_id' => $old]);
        
        // TODO: Prawdopodobnie konwersję na JSONa trzeba będzie gdzieś wyrzucić, może do ProductAttributeTypeLogic
        $helpForm = new ProductAttributeForm();
        $helpForm->loadFromObjects(OrderOfferedProductAttribute::find()->where(['order_offered_product_id' => $new])->all());
        $attributeValue = $helpForm->convertFieldsToJson();
        
        if(!empty($attributeValue)) {
            $oop = OrderOfferedProduct::findOne($new);
            $oop->attribute_value = $attributeValue;
            $oop->save();
        }
    }
    
    public function updateProductionPath($productionTask) {
        $requiredNumber = $productionTask->orderOfferedProduct->count;
        $isTaskCompleted = $productionTask->total_count_made >= $requiredNumber;
         
        if($isTaskCompleted) {
            $task = $productionTask->task;
            if($task->is_complete == 0) {
                $task->is_complete = 1;
                $task->user_completing_id = \Yii::$app->user->id;
                $task->date_completion = new Expression('NOW()');
                if(!$task->save()) {
                    throw new \Exception('Task cannot be updated: '.print_r($task->getErrors(), true));
                }
            }
        }
    
        $this->unlockSuccessorTasks($productionTask, true);
    
        $unfinishedEndTasksCount = ProductionTask::find()->alias('pt')->joinWith('task t')->joinWith('orderOfferedProduct oop')->where([
                    'oop.order_id' => $productionTask->orderOfferedProduct->order_id,
                    'pt.task_successor_ids' => null,
                    't.is_complete' => false
                ])->count();
        
        if(empty($unfinishedEndTasksCount)) {
            $order = $productionTask->orderOfferedProduct->order;
            $completedStatusId = OrderStatus::find()->where(['symbol' => Utility::ORDER_STATUS_COMPLETED])->one()->id;
            
            $orderHistory = $order->orderHistoryLast;
            $orderHistory->isNewRecord = true;
            $orderHistory->id = null;
            $orderHistory->user_id = Yii::$app->user->id;
            $orderHistory->order_status_id = $completedStatusId;
            $orderHistory->message = Yii::t('web', 'The production has been completed.');
            $orderHistory->is_client_entry = 0;
            $orderHistory->save();
            
            $order->order_history_last_id = $orderHistory->id;
            $order->save();
        }
    }
    
    private function unlockSuccessorTasks($productionTask, $withCheck) {
        if($withCheck) {
            if($productionTask->total_count_made <= 0 || empty($productionTask->task_successor_ids)) {
                return;
            }
        }
         
        $successorTasks = ProductionTask::find()->where(['id' => explode(',', $productionTask->task_successor_ids)])->all();
    
        if(!empty($successorTasks)) {
            foreach($successorTasks as $succTask) {
                if(empty($succTask->is_available)) {
                    $reqTasks = $this->getRequiredConcreteTasks($succTask, false);
                    $canBeAvailable = true;
    
                    if(!empty($reqTasks)) {
                        foreach($reqTasks as $reqTask) {
                            if(empty($reqTask->productionPathStep->activity->is_node) && $reqTask->total_count_made <= 0) {
                                $canBeAvailable = false;
                                break;
                            }
                        }
                    }
    
                    if($canBeAvailable) {
                        $succTask->is_available = 1;
                        $succTask->save();
                    }
                }
    
                if(!empty($succTask->productionPathStep->activity->is_node)) {
                    $this->unlockSuccessorTasks($succTask, false);
                }
            }
        }
    }
    
    public function getRequiredConcreteTasks($productionTask, $isNodePasses) {
        $requiredTasks = ProductionTask::find()->where(['id' => explode(',', $productionTask->task_required_ids)])->all();
    
        if(!empty($requiredTasks) && $isNodePasses) {
            foreach($requiredTasks as $reqTask) {
                if(!empty($reqTask->productionPathStep->activity->is_node)) {
                    $previousProductionTask = ProductionTask::findOne($reqTask->id);
                    $requiredTasks = array_merge($requiredTasks, $this->getRequiredConcreteTasks($previousProductionTask, true));
                }
            }
        }
    
        return $requiredTasks;
    }
}
