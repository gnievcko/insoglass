<?php

namespace frontend\logic;

class TaskLogic {
	
	public function processTasks($tasks) {
		if(empty($tasks)) {
			return $tasks;
		}
		
		$today = new \DateTime(); 
		
		foreach($tasks as &$task) {
			if(empty($task['isComplete']) && !empty($task['dateDeadline']) 
					&& (new \DateTime($task['dateDeadline']) <= $today)) {
				$task['isWarning'] = true;
			}
			else {
				$task['isWarning'] = false;
			}
		}
		
		return $tasks;
	}
	
}
