<?php

namespace frontend\logic;

class ProductionPathLogic {

    public function sortTopologically($steps) {
        if(empty($steps)) {
            throw new \Exception('No step has been provided');
        }

        $topologicalOrder = [];
        $tempSteps = $steps;

        try {
            while(!empty($tempSteps)) {
                $stepsWithoutRequiredIds = $this->getStepsWithoutRequired($tempSteps);
                if(!empty($stepsWithoutRequiredIds)) {
                    foreach($stepsWithoutRequiredIds as $swr) {
                        $topologicalOrder[] = $swr;
                        $key = array_search($swr, array_column($tempSteps, 'id'));
                        if($key !== false) {
                            unset($tempSteps[$key]);
                            $tempSteps = array_values($tempSteps);
                        }
                    }

                    $tempSteps = $this->removeStepFromRequireds($tempSteps, $stepsWithoutRequiredIds);
                } else {
                    break;
                }
            }
        } catch(\Exception $e) {
            throw new \Exception('Error during sorting occurred: ' . $e->getTraceAsString());
        }

        if(!empty($tempSteps)) {
            throw new \Exception('The path contains a cycle');
        }

        return $topologicalOrder;
    }

    private function getStepsWithoutRequired($steps) {
        $stepsWithoutRequiredIds = [];
        if(!empty($steps)) {
            foreach($steps as $step) {
                if(empty($step['requiredIds'])) {
                    $stepsWithoutRequiredIds[] = $step['id'];
                }
            }
        }

        return $stepsWithoutRequiredIds;
    }

    private function removeStepFromRequireds($steps, $requiredToRemoveIds) {
        $tempSteps = $steps;

        if(!empty($tempSteps) && !empty($requiredToRemoveIds)) {
            foreach($tempSteps as &$step) {
                if(!empty($step['requiredIds'])) {
                    $reqArray = explode(',', $step['requiredIds']);
                    $commonIds = array_intersect($requiredToRemoveIds, $reqArray);
                    if(!empty($commonIds)) {
                        $step['requiredIds'] = implode(',', array_diff($reqArray, $commonIds));
                    }
                }
            }
        }

        return $tempSteps;
    }

    public function convertAttributsToNodes($attributs) {
        $array = [];
        foreach($attributs as $attribut) {
            $array[$attribut['id']] = [
                'needed' => isset($attribut['needed']) && !empty($attribut['needed']) ? $attribut['needed'] : false,
                'forbidden' => isset($attribut['forbidden']) && !empty($attribut['forbidden']) ? $attribut['forbidden'] : false,
                'unblock' => isset($attribut['unblock']) && !empty($attribut['unblock']) ? $attribut['unblock'] : false,
            ];
        }

        return $array;
    }

    public function convertNodeIdsToStepIds($nodes) {
        foreach($nodes as $node) {
            foreach($nodes as &$n) {

                $successors = explode(',', $n['successor_ids']);
                foreach($successors as &$s) {
                    if($s == $node['tmp_id']) {
                        $s = $node['id'];
                    }
                }
                $successors = implode(',', $successors);
                $n['successor_ids'] = $successors;

                $requireds = explode(',', $n['required_ids']);
                foreach($requireds as &$r) {
                    if($r == $node['tmp_id']) {
                        $r = $node['id'];
                    }
                }
                $requireds = implode(',', $requireds);
                $n['required_ids'] = $requireds;
            }
        }
        return $nodes;
    }

    public function checkAttributs($groups, $attribute_id, $option, $value) {
        foreach($groups as &$group) {
            foreach($group['attributes'] as &$attribute) {
                if($attribute['id'] == $attribute_id) {
                    $attribute[$option] = $value;
                }
            }
        }

        return $groups;
    }

}
