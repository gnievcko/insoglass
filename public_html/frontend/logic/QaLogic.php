<?php

namespace frontend\logic;

use common\helpers\Utility;
use common\models\aq\QaFactorTypeQuery;
use common\models\aq\OrderOfferedProductQuery;
use common\helpers\ArrayHelper;

class QaLogic {

    public function getProductsWithQaFactorAssignmentAvailable(\yii\web\User $user) {
        if(!$this->checkBasicQaFactorAssignmentRequirements($user)) {
            return [];
        }
        
        return ArrayHelper::transformToAssociativeArray(
                    OrderOfferedProductQuery::getProductsReadyForQaAssignment(), 'orderOfferedProductId'
                );
    }
    
    private function checkBasicQaFactorAssignmentRequirements(\yii\web\User $user) {
        return $user->can(Utility::ROLE_QA_SPECIALIST) && count(QaFactorTypeQuery::getAllVisible()) > 0;
    }
    
}
