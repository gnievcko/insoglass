<?php

namespace frontend\components;

use Yii;
use common\models\ar\User;
use common\components\MessageTrait;

/**
 * @inheritdoc
 */
class Controller extends \common\components\Controller {	
	
	use MessageTrait;
	
	protected function regenerateCaptcha($testLimit = 0) {
		$config = $this->getCaptchaActionConfig(false);
		$controller = Yii::$app->controller;
		
		$ca = new \yii\captcha\CaptchaAction(
				'captcha',
				$controller,
				$config);
		
		$ca->getVerifyCode(true);
	}
	
	protected function getCaptchaActionConfig($withClass = true) {
		$config = [
				'transparent' => true,
				'foreColor' => 0x402B1A,
				'testLimit' => 0,
				'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
				'offset' => 2,
				'minLength' => 3,
				'maxLength' => 3,
				'width' => 70
		];
		
		if($withClass) {
			$config['class'] = 'yii\captcha\CaptchaAction';
		}
		
		return $config;
	}
	
	public function beforeAction($action) {
		if($action->controller->id == 'order' && $action->id == 'get-custom-data-summary') {
			$this->enableCsrfValidation = false;
		}
		
		if(Yii::$app->params['maintenance']) {
		    if(!($action->controller->id == 'site' && $action->id == 'maintenance')) {
		        return $this->redirect(['/site/maintenance']);    
		    }
		} 
	   
		return parent::beforeAction($action);
	}
}
