<?php
return [
    'defaultPageSize' => 20,
    'defaultDashboardPageSize' => 10,
    'maintenance' => false,
];
