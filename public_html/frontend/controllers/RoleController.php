<?php
namespace frontend\controllers;

use Yii;
use frontend\components\Controller;
use yii\filters\AccessControl;
use common\helpers\Utility;
use common\models\aq\RoleQuery;

class RoleController extends Controller {
	
	public function behaviors() {
		return [
				'access' => [
						'class' => AccessControl::className(),
						'only' => ['find'],
						'rules' => [
								[
										'actions' => ['find', 'findAvailableRoles'],
										'allow' => true, // zmienić na true, gdy będzie potrzebne
										'roles' => [Utility::ROLE_MAIN_SALESMAN, Utility::ROLE_MAIN_STOREKEEPER,
												Utility::ROLE_ADMIN],
								],
						],
				],
		];
	}
	
    public function actionFindAvailableRoles() {
    	    	
    	$request = Yii::$app->request;
    	$term = $request->get('term');
    	$page = $request->get('page', 1);
    	$size = $request->get('size');
    
    	if(empty($term) || empty($page) || empty($size)) {
    		Yii::$app->end();
    	} 
    	$roleQuery = RoleQuery::findAvailableRoles(['term' => $term]);   	
    	$rolesCount = $roleQuery->count();
    	$roles = $roleQuery->limit($size)->offset($size*($page-1))->all();
    
    	echo json_encode([
    			'items' => $roles,
    			'more' => (intval($rolesCount) > $page*$size)
    	]);
    }
}
