<?php

namespace frontend\controllers;

use Yii;
use frontend\components\Controller;
use yii\filters\AccessControl;
use yii\helpers\Url;
use yii\data\Pagination;
use common\helpers\Utility;
use frontend\models\WarehouseProductIssueListForm;
use common\models\aq\WarehouseProductOperationQuery;
use frontend\models\WarehouseProductIssueProductListForm;
use common\models\aq\ProductPhotoQuery;
use common\models\ar\WarehouseProductOperation;
use frontend\models\WarehouseProductIssueForm;
use yii\widgets\ActiveForm;
use yii\web\Response;
use common\documents\DocumentTypeUtility;

class WarehouseProductIssueController extends Controller {
	
    public function behaviors() {
        $allActions = ['list', 'details', 'edit'];

        return [
	            'access' => [
	                'class' => AccessControl::className(),
	                'only' => $allActions,
	                'rules' => [
	                        [
			                        'actions' => $allActions,
			                        'allow' => true, 
			                        'roles' => [Utility::ROLE_STOREKEEPER],
	                    	],
	                ],
	            ],
        ];
    }

    public function actionList() {
        $request = Yii::$app->request;
        $listForm = new WarehouseProductIssueListForm();
        $listForm->load($request->get());

        $activeQuery = WarehouseProductOperationQuery::getActiveIssuesQuery($listForm->toArray());
        $pages = new Pagination([
	            'totalCount' => $activeQuery->count(),
	            'page' => $listForm->page,
	            'defaultPageSize' => \Yii::$app->params['defaultPageSize'],
        ]);
        $data = $activeQuery->limit($pages->defaultPageSize)->offset($pages->page * $pages->defaultPageSize)->all();

        $listData = [
            'data' => $data,
            'pages' => $pages,
            'listForm' => $listForm,
        ];

        if ($request->isAjax) {
            echo json_encode([
                'view' => $this->renderAjax('_issues', $listData),
                'url' => Url::current([], true),
            ]);
        } else {
            return $this->render('list', $listData);
        }
    }

    public function actionDetails($id) {
    	$operation = WarehouseProductOperationQuery::getIssueDetails($id);
        if(empty($operation)) {
            throw new \yii\web\NotFoundHttpException(Yii::t('web', 'Element does not exist or you do not have permission.'));
        }

        $request = \Yii::$app->request;

        $listForm = new WarehouseProductIssueProductListForm();
        $listForm->load($request->get());

        $productsQuery = WarehouseProductOperationQuery::getIssueProductsQuery($id, $listForm->toArray());
		$pages = new Pagination([
				'totalCount' => $productsQuery->count(),
				'page' => $listForm->page,
				'defaultPageSize' => \Yii::$app->params['defaultPageSize'],
		]);
		$products = $productsQuery->limit($pages->defaultPageSize)->offset($pages->page * $pages->defaultPageSize)->all();
        ProductPhotoQuery::loadProductsPhotos($products);

		$listData = [
				'products' => $products,
				'pages' => $pages,
				'listForm' => $listForm,
		];

        if($request->isAjax) {
			echo json_encode([
					'view' => $this->renderAjax('_products', $listData),
					'url' => Url::current([], true),
			]);

        }
        else {
            return $this->render('details', [
            		'operation' => $operation, 
            		'listData' => $listData,
            		'availableDocuments' => DocumentTypeUtility::getAvailableDocuments(),
            ]);
        }
    }
    
    public function actionEdit($id) {
    	$wpo = WarehouseProductOperation::findOne($id);
    	if(empty($wpo) || !in_array($wpo->productStatus->symbol, [Utility::PRODUCT_STATUS_ISSUING, Utility::PRODUCT_STATUS_ISSUING_RESERVATION])) {
    		throw new \yii\web\NotFoundHttpException(Yii::t('web', 'Element does not exist or you do not have permission.'));
    	}
    	
    	$model = new WarehouseProductIssueForm($wpo);
    
    	if(Yii::$app->request->isAjax && $model->load(Yii::$app->request->post())) {
    		Yii::$app->response->format = Response::FORMAT_JSON;
    		return ActiveForm::validate($model);
    	}
    
    	if($model->load(Yii::$app->request->post()) && $model->validate()) {
    		try {
    			$model->saveData();
    			Yii::$app->getSession()->setFlash('success', Yii::t('web', 'Issue data has been updated successfully.'));
    
    			return $this->redirect(['warehouse-product-issue/details', 'id' => $id]);
    		}
    		catch(\Exception $e) {
    			Yii::$app->getSession()->setFlash('error', Yii::t('web', 'Issue data cannot be saved. Check provided values.'));
    			Yii::error($e->getMessage());
    			Yii::error($e->getTraceAsString());
    		}
    	}
    
    	return $this->render('form', [
    			'model' => $model,
    	]);
    }
}