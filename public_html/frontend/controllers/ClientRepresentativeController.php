<?php

namespace frontend\controllers;

use Yii;
use frontend\components\Controller;
use yii\filters\AccessControl;
use common\helpers\Utility;
use frontend\models\ClientRepresentativeForm;
use frontend\models\ClientRepresentativeListForm;
use common\models\aq\UserQuery;
use yii\data\Pagination;
use yii\web\Response;
use yii\widgets\ActiveForm;
use common\models\ar\User;
use yii\helpers\Url;
use yii\filters\VerbFilter;

class ClientRepresentativeController extends Controller {
	
	public function behaviors() {
		$actions = ['list', 'details'];
		
		return [
			'access' => [
				'class' => AccessControl::className(),
				'only' => $actions + ['edit', 'create', 'delete'],
				'rules' => [
						[
							'actions' => $actions,
							'allow' => Yii::$app->params['isClientRepresentativeManagementVisible'],
							'roles' => [Utility::ROLE_SALESMAN],
						],
                        [
							'actions' => ['edit', 'create', 'delete'],
							'allow' => true,
							'roles' => [Utility::ROLE_SALESMAN],
						],
				],
			],
			'verbs' => [
				'class' => VerbFilter::className(),
					'actions' => [
							'delete' => ['post'],
					],
			],
		];
	}
	
	public function actionList() {
		$request = Yii::$app->request;
		
		$listForm = new ClientRepresentativeListForm();
		$listForm->load($request->get());
		
		$itemsQuery = UserQuery::getClientRepresentatives($listForm->toArray());
		
		$pages = new Pagination([
				'totalCount' => $itemsQuery->count(),
				'page' => $listForm->page,
				'defaultPageSize' => \Yii::$app->params['defaultPageSize'],
		]);
		$items = $itemsQuery->limit($pages->defaultPageSize)->offset($pages->page * $pages->defaultPageSize)->all();

		$listData = [
				'items' => $items,
				'pages' => $pages,
				'listForm' => $listForm,
		];
		
		if ($request->isAjax) {
			echo json_encode([
					'view' => $this->renderAjax('_client-representatives', $listData),
					'url' => Url::current([], true),
			]);
		} else {
			return $this->render('list', $listData);
		}
	}
	
	public function actionCreate($companyId = null) {
		if ($companyId && !UserQuery::isSalesmanAssignedToCompanyByCompanyId(Yii::$app->user->id, $companyId)) {
			//Yii::$app->getSession()->setFlash('error', Yii::t('web', 'A company does not exist or you do not have access rights.'));
			//return $this->redirect(['site/index']);
			throw new \yii\web\NotFoundHttpException(Yii::t('web', 'Element does not exist or you do not have permission.'));
		}
		
		$model = new ClientRepresentativeForm($companyId);
		$model->setAddScenario();
		
		if (Yii::$app->request->isAjax && $model->load(Yii::$app->request->post())) {
			Yii::$app->response->format = Response::FORMAT_JSON;
			return ActiveForm::validate($model);
		}
		
		if ($model->load(Yii::$app->request->post()) && $model->validate()) {
			try {
				$representativeId = $model->saveData();
				
				Yii::$app->getSession()->setFlash('success', Yii::t('web', 'Contact person has been added successfully.'));
				
				if(strpos(Yii::$app->request->getReferrer(), 'companyId') !== false) {
					return $this->redirect(['client/details', 'id' => $model->companyId]);
				}
				
				return $this->redirect(['client-representative/details', 'id' => $representativeId]);
			} catch (\Exception $e) {
				throw $e;
				Yii::$app->getSession()->setFlash('error', Yii::t('web', 'Contact person data cannot be saved. Check provided values.'));
				Yii::error($e->getMessage());
				Yii::error($e->getTraceAsString());
			}
		}
		
		return $this->render('form', [
				'companyId' => $companyId,
				'model' => $model,
				'companyData' => $model->getCompanyData(),
		]);

	}
	
	public function actionEdit($id) {
		$user = User::findOne($id);
		if (empty($user) || !UserQuery::isSalesmanAssignedToCompanyByCompanyId(Yii::$app->user->id, $user->company_id)) {
			Yii::$app->getSession()->setFlash('error', Yii::t('web', 'A company does not exist or you do not have access rights.'));
			return $this->redirect(['site/index']);
		}
		
		$model = new ClientRepresentativeForm($user->company_id, $user);
		$model->setEditScenario();
		
		if (Yii::$app->request->isAjax && $model->load(Yii::$app->request->post())) {
			Yii::$app->response->format = Response::FORMAT_JSON;
			return ActiveForm::validate($model);
		}
		
		if ($model->load(Yii::$app->request->post()) && $model->validate()) {
			try {
				$representativeId = $model->saveData();
				
				Yii::$app->getSession()->setFlash('success', Yii::t('web', 'Contact person data has been updated successfully.'));
				
				$ref = Yii::$app->request->get('ref');
				if($ref == 'client') {
					return $this->redirect(['client/details', 'id' => $model->companyId]);
				}
				
				return $this->redirect(['client-representative/details', 'id' => $representativeId]);
			} catch (\Exception $e) {
				Yii::$app->getSession()->setFlash('error', Yii::t('web', 'Contact person data cannot be saved. Check provided values.'));
				Yii::error($e->getMessage());
				Yii::error($e->getTraceAsString());
			}
		}
		
		$params = [
				'companyId' => $user->company_id,
				'model' => $model,
				'companyData' => $model->getCompanyData(),
		];
		
		//if(strpos(Yii::$app->request->getReferrer(), Yii::$app->controller->id) !== false){
		//	$params['details'] = true;
		//}
		
		return $this->render('form', $params);
	}
	
	public function actionDelete() {
		$id = Yii::$app->request->post('id');
		
		try {
			$user = User::find()->where(['id' => $id])->one();
			
			//$user = User::findOne($id);
			if (empty($user) || !UserQuery::isSalesmanAssignedToCompanyByCompanyId(Yii::$app->user->id, $user->company_id)) {
				throw new \Exception('The user has not privilege to delete this contact person: ' . $id);
			}
			
			$user->is_active = 0;
			$user->save();
			
			Yii::$app->getSession()->setFlash('success', Yii::t('web', 'The contact person has been deleted.'));
		} catch (\Exception $e) {
			Yii::info(print_r($e->getMessage(), true), 'info');
			Yii::$app->getSession()->setFlash('error', Yii::t('web', 'The contact person cannot be deleted. Please contact the administrator.'));
		}
		
		if(strpos(Yii::$app->request->getReferrer(), Yii::$app->controller->id.'/details') !== false) {
			return $this->redirect(['list']);
		}
		
		return $this->redirect(Yii::$app->request->getReferrer());
	}
	
	public function actionDetails($id) {
		
		$data = UserQuery::getClientRepresentativeDetails($id);
		
		if(empty($data)) {
			throw new \yii\web\NotFoundHttpException(Yii::t('web', 'Element does not exist or you do not have permission.'));
		}
		
		return $this->render('details', [
				'data' => $data,
		]);
	}
	

}