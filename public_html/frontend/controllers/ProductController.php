<?php
namespace frontend\controllers;

use Yii;
use common\models\aq\WarehouseProductQuery;
use frontend\components\Controller;
use yii\filters\AccessControl;
use common\models\aq\ProductQuery;
use common\helpers\Utility;
use frontend\models\ProductListForm;
use frontend\models\ProductModel;
use yii\data\Pagination;
use yii\helpers\Url;
use common\models\aq\ProductCategoryQuery;
use common\models\ar\Product;
use common\models\ar\ProductAttachment;
use common\models\ar\ProductCategoryTranslation;
use common\models\ar\Currency;
use common\models\ar\Language;
use common\models\aq\ProductPhotoQuery;
use yii\helpers\ArrayHelper;
use yii\web\Response;
use yii\widgets\ActiveForm;
use frontend\models\ProductAliasForm;
use yii\helpers\Html;
use yii\filters\VerbFilter;
use frontend\presenters\ProductPresenter;
use common\models\aq\WarehouseSupplierQuery;
use frontend\helpers\ManufacturerHelper;
use yii\db\Expression;
use common\helpers\UploadHelper;
use common\models\ar\OfferedProductRequirement;

class ProductController extends Controller {

	public function behaviors() {
		$allActions = ['list', 'details', 'delete', 'find', 'create', 'edit', 'get-manufacturer-details',
				'get-main-photo', 'delete-attachment'
		];

		return [
				'access' => [
						'class' => AccessControl::className(),
						'only' => $allActions,
						'rules' => [
								[
										'actions' => ['list', 'details', 'find', 'create', 'get-main-photo'],
										'allow' => true,
										'roles' => [Utility::ROLE_STOREKEEPER],
								],
								[
										'actions' => ['edit', 'delete', 'get-manufacturer-details', 'delete-attachment'],
										'allow' => true,
										'roles' => [Utility::ROLE_STOREKEEPER, Utility::ROLE_SALESMAN],
								],
						],
				],
				'verbs' => [
						'class' => VerbFilter::className(),
						'actions' => [
								'delete' => ['post'],
						],
				],
		];
	}

	public function actionList() {
		$request = Yii::$app->request;
		$listForm = new ProductListForm();
		if(!$request->isAjax) {
			$categoryOptions = ProductCategoryQuery::getCategoryList();
			$listForm->allCategoriesCount = count($categoryOptions);
		}
		$listForm->load($request->get());
		$itemsQuery = ProductQuery::getActiveProductsQuery($listForm->toArray());
		$pages = new Pagination([
				'totalCount' => $itemsQuery->count(),
				'page' => $listForm->page,
				'defaultPageSize' => \Yii::$app->params['defaultPageSize'],
		]);
		$items = $itemsQuery->limit($pages->defaultPageSize)->offset($pages->page * $pages->defaultPageSize)->all();

		$listData = [
				'items' => $items,
				'pages' => $pages,
				'listForm' => $listForm,
		];

		if($request->isAjax) {
			echo json_encode([
					'view' => $this->renderAjax('_products', $listData),
					'url' => Url::current([], true),
			]);
		}
		else {
			$listData['categoryTranslations'] = $categoryOptions;
			return $this->render('list', $listData);
		}
	}
	
	public function actionGetMainPhoto() {
		$request = Yii::$app->request;
		$productId = $request->get('id');
		if(!$request->isAjax || empty($productId)) {
			return '';
		}
	
		$urlImage = ProductPhotoQuery::getMainPhoto($productId);
		return !empty($urlImage) ? '<img src="'.\common\helpers\UploadHelper::getNormalPath($urlImage, 'product_photo').'"/>' : '';
	}

	public function actionDetails($id) {
		if(empty($product = Product::findOne($id)) || empty($product->is_active) || !empty($product->parent_product_id)) {
			//Yii::$app->getSession()->setFlash('error', Yii::t('web', 'A product does not exist.'));
			//return $this->redirect(['site/index']);
			throw new \yii\web\NotFoundHttpException(Yii::t('web', 'Element does not exist or you do not have permission.'));
		}

		$data = ProductQuery::getDetails($id);
		$attachments = ProductQuery::getAttachments($id);

		$photos = [];
		$photosRaw = ProductPhotoQuery::getPhotosByProductId($id);
		if(!empty($photosRaw)) {
			$photos = array_map(function($row) {
				return [
						'url' => $row['urlPhoto'],
						'src' => $row['urlThumbnail'],
						'options' => [
								'title' => $row['description'],
						],
				];
			}, $photosRaw);
		}
		
		return $this->render('details', [
				'data' => (new ProductPresenter())->prepareDataForDetails([
						'details' => $data,
						'photos' => $photos,
						'attachments' => $attachments,
				]),
		]);
	}

	public function actionDelete() {
		$id = Yii::$app->request->post('id');

		try {
			if(empty($product = Product::findOne($id)) || empty($product->is_active) || !empty($product->parent_product_id)) {
				throw new \Exception('The product cannot be deleted: '.$id);
			}
			$product = Product::findOne($id);
			$count = WarehouseProductQuery::productCount($id)->one()['count'];
			$product->is_active = 0;
			$product->date_deletion = new Expression('NOW()');
			$product->save();

			$productVersions = Product::find()
					->where(['parent_product_id' => $id])
					->all();

			foreach($productVersions as $productVersion) {
				$productVersion->is_active = 0;
				$productVersion->date_deletion = new Expression('NOW()');
				$productVersion->save();
			}
			
			OfferedProductRequirement::deleteAll(['product_required_id' => $id]);

			Yii::$app->getSession()->setFlash('success', Yii::t('web', 'The product has been deleted.'));
		}
		catch(\Exception $e) {
			Yii::error($e->getMessage());
			Yii::error($e->getTraceAsString());
			Yii::$app->getSession()->setFlash('error', Yii::t('web', 'The product cannot be deleted. Please contact with the administrator.'));
		}

		if(strpos(Yii::$app->request->getReferrer(), 'warehouse-product/details') !== false) {
			return $this->redirect(['warehouse-product/list']);
		}
		elseif(strpos(Yii::$app->request->getReferrer(), Yii::$app->controller->id.'/details') !== false) {
			return $this->redirect(['list']);
		}
		return $this->redirect(Yii::$app->request->getReferrer());
	}

	public function actionFind() {
		$request = Yii::$app->request;
		$term = $request->get('term');
		$page = $request->get('page', 1);
		$size = $request->get('size');

		if(empty($term) || empty($page) || empty($size)) {
			Yii::$app->end();
		}

		$query = ProductQuery::findProducts(['name' => $term]);
		$count = $query->count();
		$itemsRaw = $query->limit($size)->offset($size * ($page - 1))->all();

		// TODO: Sprawdzić, czy da się to "mądrzej" zrobić funkcjami tablicowymi w PHP lub ArrayHelper, bo samo "map" nie wystarcza
		$items = [];
		if(!empty($itemsRaw)) {
			foreach($itemsRaw as $itemRaw) {
				$item = [];
				$item['id'] = $itemRaw['id'];
				$item['name'] = !empty($itemRaw['parentName']) ? Html::encode($itemRaw['parentName'].' ('.mb_strtolower($itemRaw['name'], 'UTF-8').')') : Html::encode($itemRaw['name']);
				$items[] = $item;
			}
		}

		echo json_encode([
				'items' => $items,
				'more' => (intval($count) > $page * $size)
		]);
	}

    public function actionCreate() {
        $productModel = new ProductModel();        

        $request = Yii::$app->request;
        if ($request->isAjax && $request->isPost) {
            $productModel->load($request->post());
            Yii::$app->response->format = Response::FORMAT_JSON;

            return ActiveForm::validateMultiple($productModel->getProductTranslationsForms())
                + ActiveForm::validateMultiple($productModel->getProductVariantsForms())
                + ActiveForm::validateMultiple($productModel->getPhotosForms())
                + ActiveForm::validateMultiple($productModel->getProductAliasesForms())
                + ActiveForm::validateMultiple($productModel->getDocumentsForms())
                + ActiveForm::validate($productModel->getProductForm());
        }

        if($request->isPost) {
            $productModel->load($request->post());

            try {
                if($productModel->save()) {
                    Yii::$app->getSession()->setFlash('success', Yii::t('web', 'Product has been added successfully.'));
                    return $this->redirect(['product/details', 'id' => $productModel->getProduct()->id]);
                }
                else {
                    Yii::$app->getSession()->setFlash('error', Yii::t('web', 'Product data cannot be saved. Check provided values.'));
                }
            }
            catch(\Exception $e) {
				Yii::$app->getSession()->setFlash('error', Yii::t('web', 'Product data cannot be saved. Check provided values.'));
				Yii::error($e->getMessage());
				Yii::error($e->getTraceAsString());
            }
        }

        $language = Language::find()->where(['symbol' => \Yii::$app->language])->one();
        $productForm = $productModel->getProductForm();

        $productCategories = ProductCategoryTranslation::find()
            ->andWhere(['in', 'product_category_id', empty($productForm->categoriesIds) ? [] : $productForm->categoriesIds])
            ->andWhere(['language_id' => $language->id])
            ->orderBy('name')
            ->all();

        return $this->render('create', $productModel->getAllForms() + [
            'currencies' => ArrayHelper::map(Currency::find()->all(), 'id', 'short_symbol'),
            'productCategories' => $productCategories,
        ]);
    }

    public function actionEdit($id) {
        if(empty($product = Product::findOne($id)) || empty($product->is_active)) {
            throw new \yii\web\NotFoundHttpException();
        }
        $productModel = new ProductModel($id);

        $request = Yii::$app->request;
        if ($request->isAjax && $request->isPost) {
            $productModel->load($request->post());
            Yii::$app->response->format = Response::FORMAT_JSON;

            return ActiveForm::validateMultiple($productModel->getProductTranslationsForms())
                + ActiveForm::validateMultiple($productModel->getProductVariantsForms())
                + ActiveForm::validateMultiple($productModel->getPhotosForms())
                + ActiveForm::validateMultiple($productModel->getProductAliasesForms())
                + ActiveForm::validateMultiple($productModel->getDocumentsForms())
                + ActiveForm::validate($productModel->getProductForm());
        }

        if($request->isPost) {
            $productModel->load($request->post());

            try {
                if($productModel->update()) {
                	Yii::$app->getSession()->setFlash('success', Yii::t('web', 'Product data has been updated successfully.'));
                	if(Yii::$app->request->get('type') === Utility::PRODUCT_TYPE_PRODUCT) {
						return $this->redirect(['offered-product/details', 'id' => $id, 'type' => Utility::PRODUCT_TYPE_PRODUCT]);
					} else {
						return $this->redirect(['product/details', 'id' => $id]);
					}
                }
                else {
                    Yii::$app->getSession()->setFlash('error', Yii::t('web', 'Product data cannot be saved. Check provided values.'));
                }
            }
            catch(\Exception $e) {
				Yii::$app->getSession()->setFlash('error', Yii::t('web', 'Product data cannot be saved. Check provided values.'));
				Yii::error($e->getMessage());
				Yii::error($e->getTraceAsString());
            }
        }

        $language = Language::find()->where(['symbol' => \Yii::$app->language])->one();
        $productForm = $productModel->getProductForm();
        $productCategories = ProductCategoryTranslation::find()
            ->andWhere(['in', 'product_category_id', empty($productForm->categoriesIds) ? [] : $productForm->categoriesIds])
            ->andWhere(['language_id' => $language->id])
            ->orderBy('name')
            ->all();

        $forms = $productModel->getAllForms();
        return $this->render('edit', $forms + [
            'currencies' => ArrayHelper::map(Currency::find()->all(), 'id', 'short_symbol'),
            'productCategories' => $productCategories,
            'productId' => $id,
        ]);
    }

    public function actionGetAliasForm() {
        $request = \Yii::$app->request;

        if($request->isAjax) {
            echo $this->renderAjax('inc/_alias', [
                'productAliasForm' => new ProductAliasForm(),
                'prefix' => $request->get('prefix'),
                'formId' => $request->get('formId'),
            ]);
        }
    }
    
    public function actionGetManufacturerDetails() {
    	$request = Yii::$app->request;
    	$manufacturerId = $request->get('manufacturerId');
    	
    	if(!$request->isAjax || empty($manufacturerId)) {
    		return '';
    	}
    	
    	$data = WarehouseSupplierQuery::getDetails($manufacturerId);
    	
    	return ManufacturerHelper::getFullManufacturerData([
				'id' => $data['id'],
				'name' => $data['name'],
				'contactPerson' => $data['contactPerson'],
				'email' => $data['email'],
				'phone' => $data['phone'],
				'address' => $data['addressMain'],
				'complement' => $data['addressComplement'],
				'city' => $data['cityName'],
				'zipCode' => $data['cityZipCode'],
				'province' => $data['provinceName'],
				'country' => $data['countryName'],
		]);
    }
    
    public function actionDeleteAttachment() {
    	$id = Yii::$app->request->post('id');
    	$url = Yii::$app->request->post('url');
    	$attachment = ProductAttachment::findOne($id);
    	UploadHelper::deleteAttachment($url, $attachment);
    	if(!Yii::$app->request->isAjax) {
    		return $this->redirect(Yii::$app->request->getReferrer());
    	}
    }
    
}
