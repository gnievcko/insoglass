<?php
namespace frontend\controllers;

use Yii;
use frontend\components\Controller;
use yii\filters\AccessControl;
use common\helpers\Utility;
use common\models\aq\UserCompanySalesmanQuery;

class SalesmanController extends Controller {
	
	public function behaviors() {
		return [
				'access' => [
						'class' => AccessControl::className(),
						'only' => ['find'],
						'rules' => [
								[
										'actions' => ['find'],
										'allow' => true,
										'roles' => [Utility::ROLE_SALESMAN],
								],
						],
				],
		];
	}
	
    public function actionFind() {
        $request = Yii::$app->request;
        $term = $request->get('term');
        $page = $request->get('page', 1);
        $size = $request->get('size');
        $clientId = $request->get('clientId');
        $ignoredSalespeopleIds = $request->get('ignoredSalespeopleIds', []);

		if(empty($term) || empty($page) || empty($size) || empty($clientId)) {
			Yii::$app->end();
		}

        $salesmanQuery = UserCompanySalesmanQuery::findSalespeople(['name' => $term, 'clientId' => $clientId, 'ignoredSalespeopleIds' => $ignoredSalespeopleIds]);
        $salespeopleCount = $salesmanQuery->count();
        $salespeople = $salesmanQuery->limit($size)->offset($size*($page-1))->all();

        echo json_encode([
            'items' => $salespeople, 
            'more' => (intval($salespeopleCount) > $page*$size)
        ]);
    }
    
    public function actionFindWithoutClient() {
    	$request = Yii::$app->request;
    	$term = $request->get('term');
    	$page = $request->get('page', 1);
    	$size = $request->get('size');
    	$ignoredSalespeopleIds = $request->get('ignoredSalespeopleIds', []);
    
    	if(empty($term) || empty($page) || empty($size)) {
    		Yii::$app->end();
    	}
    
    	$salesmanQuery = UserCompanySalesmanQuery::findSalespeople2(['name' => $term, 'ignoredSalespeopleIds' => $ignoredSalespeopleIds]);
    	$salespeopleCount = $salesmanQuery->count();
    	$salespeople = $salesmanQuery->limit($size)->offset($size*($page-1))->all();
    
    	echo json_encode([
    			'items' => $salespeople,
    			'more' => (intval($salespeopleCount) > $page*$size)
    	]);
    }
}
