<?php
namespace frontend\controllers;

use Yii;
use frontend\components\Controller;
use yii\filters\AccessControl;

use yii\helpers\Url;
use yii\data\Pagination;
use frontend\models\WarehouseSupplierListForm;
use yii\web\Response;
use yii\widgets\ActiveForm;

use common\helpers\Utility;
use common\models\aq\WarehouseSupplierQuery;
use frontend\models\WarehouseSupplierForm;
use common\models\ar\WarehouseSupplier;

use yii\filters\VerbFilter;
use common\components\GetProvinceByZipCodeTrait;

class WarehouseSupplierController extends Controller {
	use GetProvinceByZipCodeTrait;

	public function behaviors() {
		return [
				'access' => [
						'class' => AccessControl::className(),
						'only' => ['list', 'create', 'edit', 'delete', 'details', 'find'],
						'rules' => [
								[
										'actions' => ['list', 'create', 'edit', 'delete', 'details', 'find'],
										'allow' => true,
										'roles' => [Utility::ROLE_STOREKEEPER]
								],

						],
				],
				'verbs' => [
						'class' => VerbFilter::className(),
						'actions' => [
								'delete' => ['post'],
						],
				],
		];
	}

	public function actionList() {
		$request = Yii::$app->request;
		$listForm = new WarehouseSupplierListForm();

		$listForm->load($request->get());

		$warehouseSupplierQuery = WarehouseSupplierQuery::getActiveSuppliersQuery($listForm->toArray());

		$pages = new Pagination([
				'totalCount' => $warehouseSupplierQuery->count(),
				'page' => $listForm->page,
				'defaultPageSize' => \Yii::$app->params['defaultPageSize'],
		]);
		$items = $warehouseSupplierQuery->limit($pages->defaultPageSize)->offset($pages->page * $pages->defaultPageSize)->all();

		$listData = [
				'items' => $items,
				'pages' => $pages,
				'listForm' => $listForm,
		];

		if($request->isAjax) {
			echo json_encode([
					'view' => $this->renderAjax('_warehouseSuppliers', $listData),
					'url' => Url::current([], true),
			]);
		}
		else {
			return $this->render('list', $listData);

		}
	}

	public function actionCreate() {
		$model = new WarehouseSupplierForm();
		$model->setAddScenario();

		if(Yii::$app->request->isAjax && $model->load(Yii::$app->request->post())) {
			Yii::$app->response->format = Response::FORMAT_JSON;
			return ActiveForm::validate($model);
		}

		if($model->load(Yii::$app->request->post()) && $model->validate()) {
			try {
				$model->saveData();
				Yii::$app->getSession()->setFlash('success', Yii::t('web', 'Supplier has been added successfully.'));

				return $this->redirect(['details', 'id' => $model->id]);
			}
			catch(\Exception $e) {
				Yii::$app->getSession()->setFlash('error', Yii::t('web', 'Supplier cannot be saved. Check provided values.'));
				Yii::error($e->getMessage());
				Yii::error($e->getTraceAsString());
			}
		}

		return $this->render('form', [
				'model' => $model,
		]);
	}

	public function actionEdit($id) {
		$warehouseSupplier = WarehouseSupplier::findOne($id);
		if(empty($warehouseSupplier) || !$warehouseSupplier['is_active']) {
			//Yii::$app->getSession()->setFlash('error', Yii::t('web', 'Supplier does not exist.'));
			//return $this->redirect(['site/index']);
			throw new \yii\web\NotFoundHttpException(Yii::t('web', 'Element does not exist or you do not have permission.'));
		}
		$model = new WarehouseSupplierForm($warehouseSupplier);
		$model->setEditScenario();

		if(Yii::$app->request->isAjax && $model->load(Yii::$app->request->post())) {
			Yii::$app->response->format = Response::FORMAT_JSON;
			return ActiveForm::validate($model);
		}

		if($model->load(Yii::$app->request->post()) && $model->validate()) {
			try {
				$model->saveData();
				Yii::$app->getSession()->setFlash('success', Yii::t('web', 'Supplier has been updated successfully.'));

				return $this->redirect(['details', 'id' => $model->id]);


			}
			catch(\Exception $e) {
				Yii::$app->getSession()->setFlash('error', Yii::t('web', 'Supplier cannot be saved. Check provided values.'));
				Yii::error($e->getMessage());
				Yii::error($e->getTraceAsString());
			}
		}

		return $this->render('form', [
				'model' => $model,
		]);
	}

	public function actionDetails($id) {
		// TODO: Sprawdzać aktywność i inne charakterystyczne parametry
		$data = WarehouseSupplierQuery::getDetails($id);
		if(empty($data) || !$data['is_active']) {
			//Yii::$app->getSession()->setFlash('error', Yii::t('web', 'Supplier does not exist.'));
			//return $this->redirect(['site/index']);
			throw new \yii\web\NotFoundHttpException(Yii::t('web', 'Element does not exist or you do not have permission.'));
		}


		return $this->render('details', [
				'data' => $data,
		]);
	}

	public function actionDelete() {
		$id = Yii::$app->request->post('id');
		$transaction = Yii::$app->db->beginTransaction();

		try {
			$warehouseSupplier = WarehouseSupplier::findOne($id);
			$warehouseSupplier->is_active = 0;
			$warehouseSupplier->save();

			$transaction->commit();

			Yii::$app->getSession()->setFlash('success', Yii::t('web', 'Supplier has been deleted.'));
		}
		catch(\Exception $e) {
			Yii::info(print_r($e->getMessage(), true), 'info');
			Yii::$app->getSession()->setFlash('error', Yii::t('web', 'Supplier cannot be deleted. Please contact the administrator.'));
		}

		if(strpos(Yii::$app->request->getReferrer(), Yii::$app->controller->id.'/details') !== false) {
			return $this->redirect(['list']);
		}
		return $this->redirect(Yii::$app->request->getReferrer());
	}

    public function actionFind() {
        $request = Yii::$app->request;
        $term = $request->get('term');
        $page = $request->get('page', 1);
        $size = $request->get('size');

		if(empty($term) || empty($page) || empty($size)) {
			Yii::$app->end();
		}

        $query = WarehouseSupplierQuery::getActiveSuppliersQuery(['nameFilterString' => $term]);
        $count = $query->count();
        $items = $query->limit($size)->offset($size*($page-1))->all();

        echo json_encode([
	            'items' => $items,
	            'more' => (intval($count) > $page*$size)
        ]);
    }
    
}
