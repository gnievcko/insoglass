<?php
namespace frontend\controllers;

use Yii;
use frontend\components\Controller;
use yii\filters\AccessControl;

use common\helpers\Utility;
use common\models\aq\WarehouseProductOperationQuery;

class WarehouseProductOperationController extends Controller {
	
	public function behaviors() {
		return [
				'access' => [
						'class' => AccessControl::className(),
						'only' => ['find'],
						'rules' => [
								[
										'actions' => ['find'],
										'allow' => true,
										'roles' => [Utility::ROLE_STOREKEEPER]
								],
						],
				],
		];
	}	

	public function actionFind() {
    	$request = Yii::$app->request;
    	$term = $request->get('term');
    	$page = $request->get('page', 1);
    	$size = $request->get('size');

    	if(empty($term) || empty($page) || empty($size)) {
    		Yii::$app->end();
    	}

    	$query = WarehouseProductOperationQuery::findOperations(['number' => $term, 'types' => $request->get('types')]);
    	$count = $query->count();
    	$items = $query->limit($size)->offset($size * ($page - 1))->all();

    	echo json_encode([
    			'items' => $items,
    			'more' => (intval($count) > $page * $size),
    	]);
    }

}
