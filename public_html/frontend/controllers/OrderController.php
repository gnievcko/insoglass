<?php

namespace frontend\controllers;

use Yii;
use yii\data\Pagination;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use yii\helpers\ArrayHelper;
use yii\helpers\Json;
use yii\helpers\Url;
use yii\mongodb\Query;
use yii\web\BadRequestHttpException;
use yii\web\Response;
use common\components\ActiveForm;
use common\alerts\OrderDeadlineAlertHandler;
use common\documents\DocumentTypeUtility;
use common\helpers\UploadHelper;
use common\helpers\Utility;
use common\helpers\StringHelper;
use common\models\ar\Currency;
use common\models\ar\Entity;
use common\models\ar\Order;
use common\models\ar\OrderAttachment;
use common\models\ar\OrderStatus;
use common\models\ar\OrderStatusTranslation;
use common\models\ar\TaskType;
use common\models\ar\User;
use common\models\aq\ConstructionQuery;
use common\models\aq\DocumentQuery;
use common\models\aq\MuntinQuery;
use common\models\aq\OrderHistoryQuery;
use common\models\aq\OrderOfferedProductAttributeQuery;
use common\models\aq\OrderQuery;
use common\models\aq\OrderTypeQuery;
use common\models\aq\OrderTypeSetQuery;
use common\models\aq\OrderUserRepresentativeQuery;
use common\models\aq\ShapeQuery;
use common\models\aq\TaskQuery;
use common\models\aq\UserQuery;
use common\models\aq\UserOrderSalesmanQuery;
use common\models\aq\WarehouseProductQuery;
use frontend\components\Controller;
use frontend\helpers\ModelLoaderHelper;
use frontend\logic\OrderLogic;
use frontend\logic\ProductAttributeTypeLogic;
use frontend\models\AlertForm;
use frontend\models\OrderCostForm;
use frontend\models\OrderEmployeesForm;
use frontend\models\OrderHistoryChangeTracker;
use frontend\models\OrderHistoryForm;
use frontend\models\OrderListForm;
use frontend\models\OrdersListForm;
use frontend\models\OrderModel;
use frontend\models\OrderProductForm;
use frontend\models\OrderTableForm;
use frontend\models\ProductAttributeForm;
use frontend\models\TaskObjectListForm;
use frontend\models\ArtificialProductAttributeForm;
use common\models\ar\OrderOfferedProductAttachment;
use common\models\ar\Shape;
use common\models\ar\RalColour;

class OrderController extends Controller {

    public function behaviors() {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['index', 'list', 'details', 'create', 'get-product-form', 'get-photo-form',
                    'get-document-form', 'get-employee-form', 'update', 'find', 'find-parents', 'copy', 'edit', 'quick-update',
                    'get-user-representatives', 'delete', 'get-all-products-summary', 'get-custom-data-summary',
                    'get-custom-data-numeration', 'get-history-details', 'create-suborder', 'delete-attachment',
                    'json-callendar', 'short-list-dashboard', 'short-list', 'short-order', 'get-color-and-shape'],
                'rules' => [
                    [
                        'actions' => ['index', 'list', 'details', 'create', 'get-product-form', 'get-photo-form',
                            'get-document-form', 'get-employee-form', 'update', 'find-parents', 'copy', 'edit', 'quick-update',
                            'get-user-representatives', 'get-all-products-summary', 'get-custom-data-summary',
                            'get-custom-data-numeration', 'get-history-details', 'create-suborder', 'delete-attachment',
                            'json-callendar', 'short-list-dashboard', 'short-list', 'short-order', 'get-color-and-shape'],
                        'allow' => true,
                        'roles' => [Utility::ROLE_SALESMAN],
                    ],
                    [
                        'actions' => ['delete'],
                        'allow' => true,
                        'roles' => [Utility::ROLE_MAIN_SALESMAN],
                    ],
                    [
                        'actions' => ['find'],
                        'allow' => true,
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    public function actionIndex() {
        return $this->redirect(['order/list']);
    }

    public function actionList() {
        $request = Yii::$app->request;
        $ordersListForm = new OrdersListForm();
        $ordersListForm->load($request->get());


        $activeOrdersQuery = OrderQuery::getOrdersQuery($ordersListForm->toArray());
        $pages = new Pagination([
            'totalCount' => $activeOrdersQuery->count(),
            'page' => $ordersListForm->page,
            'defaultPageSize' => \Yii::$app->params['frontendOrderListPageSize'],
        ]);
        $orders = $activeOrdersQuery->limit($pages->defaultPageSize)->offset($pages->page * $pages->defaultPageSize)->all();

        $ordersListData = [
            'orders' => $orders,
            'pages' => $pages,
            'ordersListForm' => $ordersListForm,
        ];

        if($request->isAjax) {
            echo json_encode([
                'view' => $this->renderAjax('inc/_orders', $ordersListData),
                'url' => Url::current([], true),
            ]);
        } else {
            $ordersListData['ordersStatusesTranslations'] = OrderStatusTranslation::find()->forLanguage(\Yii::$app->language)->all();
            return $this->render('list', $ordersListData);
        }
    }

    public function actionDetails($id) {
        $data = OrderQuery::getDetails($id);
        if(empty($data) || !empty($data['isDeleted']) || (!UserQuery::isSalesmanAssignedToCompanyByOrderId(Yii::$app->user->id, $id) &&
                (!Yii::$app->user->can(Utility::ROLE_MAIN_SALESMAN) && !UserQuery::isSalesmanAssignedToOrder(Yii::$app->user->id, $id)))) {
            throw new \yii\web\NotFoundHttpException(Yii::t('web', 'Element does not exist or you do not have permission.'));
        }

        $request = Yii::$app->request;
        $listForm = new TaskObjectListForm();
        $listForm->load($request->get());

        $reservedProducts = WarehouseProductQuery::getReservedProductsByOrderId($id)->all();
        $contactPeople = OrderUserRepresentativeQuery::getUserRepresentativesByOrderId($id);
        $orderTypes = OrderTypeSetQuery::getOrderTypesByOrderId($id);
        $entries = OrderHistoryQuery::getOrderEntries($id);
        $salesmen = UserOrderSalesmanQuery::getSalesmenByOrderId($id);
        $attachments = OrderQuery::getAttachments($id);
        $products = OrderQuery::getOrderProducts($id, ['withArtificial' => true]);

        if(Yii::$app->params['isAttachmentForProductVisible']) {
            foreach($products as &$product) {
                $product['attachments'] = OrderQuery::getOrderProductAttachments($product['id']);
            }
        }
        $suborders = OrderQuery::getChildrenOrders($id);

        $costSum = 0;
        if(!empty($products)) {
            foreach($products as $p) {
                if(!empty($p['price'])) {
                    $costSum += $p['count'] * ($p['price'] * $p['conversionValue']);
                }
            }
        }

        $photos = [];
        $photosRaw = OrderQuery::getPhotos($id);
        if(!empty($photosRaw)) {
            $photos = array_map(function($row) {
                return [
                    'url' => $row['urlPhoto'],
                    'src' => $row['urlThumbnail'],
                    'options' => [
                        'title' => $row['description'],
                    ],
                ];
            }, $photosRaw);
        }

        $summary;
        $vats;
        OrderModel::calculateSummary($products, $summary, $vats);

        if(!empty($data['customData'])) {
            $data['customDataSummary'] = OrderTableForm::calculateTotalPriceStatic(Json::decode($data['customData']));
        }

        $taskQuery = TaskQuery::getTasksByOrderId($id, $listForm->toArray());
        $pages = new Pagination([
            'totalCount' => $taskQuery->count(),
            'page' => $listForm->page,
            'defaultPageSize' => \Yii::$app->params['defaultPageSize'],
        ]);
        $tasks = $taskQuery->limit($pages->defaultPageSize)->offset($pages->page * $pages->defaultPageSize)->all();
        $taskTypeId = TaskType::find()->where(['symbol' => Utility::TASK_TYPE_ASSOCIATED_WITH_ORDER])->one()->id;
        $orderEditions = OrderHistoryChangeTracker::getAllEditions($id);

        $listData = [
            'items' => $tasks,
            'pages' => $pages,
            'listForm' => $listForm,
            'taskTypeId' => $taskTypeId,
        ];

        if($request->isAjax) {
            echo json_encode([
                'view' => $this->renderAjax('inc/_tasks', $listData),
                'url' => Url::current([], true),
            ]);
        } else {

            if(Yii::$app->params['isProductAttributesVisible'] && !empty($products)) {
                foreach($products as &$product) {
                    $product['attributes'] = (new ProductAttributeTypeLogic())->getAttributeGroupsWithValues(
                            OrderOfferedProductAttributeQuery::getProductAttributes($product['id'], false)
                    );
                }
            }

            $data = [
                'data' => $data,
                'orderEditions' => $orderEditions,
                'contactPeople' => $contactPeople,
                'orderTypes' => $orderTypes,
                'entries' => $entries,
                'salesmen' => $salesmen,
                'attachments' => $attachments,
                'products' => $products,
                'reservedProducts' => $reservedProducts,
                'photos' => $photos,
                'costSum' => $costSum,
                'summary' => $summary,
                'vats' => $vats,
                'plnCurrency' => Currency::find()->where('symbol = :symbol', [':symbol' => Utility::CURRENCY_PLN])->one(),
                'documents' => $this->getDocuments($id),
                'documentsAbleToDelete' => DocumentQuery::getLastInvoiceIds(),
                'availableDocuments' => DocumentTypeUtility::getAvailableDocuments(),
                'listData' => $listData,
                'suborders' => $suborders,
            ];

            return $this->render('details', $data);
        }
    }

    public function actionGetHistoryDetails($id, $extend) {
        if($extend == 1) {
            $tracker = new OrderHistoryChangeTracker();
            $history = $tracker->getEntireHistory($id);
            return $this->renderPartial('inc/_historyDetailsExtended', ['entries' => $history]);
        } else {
            $history = OrderHistoryQuery::getOrderEntries($id);
            return $this->renderPartial('inc/_historyDetailsShort', ['entries' => $history]);
        }
    }

    public function actionCreate($companyId = null, $parentOrderId = null) {
        $request = Yii::$app->request;
        $save = $request->post('save');
        $data = $request->post();
        $orderId = isset($data['OrderCreateForm']['orderId']) ? $data['OrderCreateForm']['orderId'] : null;
        if(!empty($orderId) && $orderId != null) {
            $scenario = OrderModel::SCENARIO_UPDATE_EDIT;
            $orderModel = new OrderModel($scenario, $orderId, $companyId, $parentOrderId);
        } else {
            $scenario = OrderModel::SCENARIO_CREATE;
            $orderModel = new OrderModel($scenario, null, $companyId, $parentOrderId);
        }

        $response = [];

        if($request->isAjax && $request->isPost && $request->post('click') != 1) {
            $orderModel->load($request->post());
            Yii::$app->response->format = Response::FORMAT_JSON;

            $result = ActiveForm::validateMultiple($orderModel->getOrderProductsForms()) + ActiveForm::validateMultiple($orderModel->getDocumentsForms()) + ActiveForm::validateMultiple($orderModel->getPhotosForms()) + ActiveForm::validateMultiple($orderModel->getOrderCostsForms()) + ActiveForm::validate($orderModel->getOrderEmployeesForm()) + ActiveForm::validate($orderModel->getOrderCreateForm()) + ActiveForm::validate($orderModel->getOrderTableForm());

            if(Yii::$app->params['isProductAttributesVisible']) {
                $result += ActiveForm::validateMultiple($orderModel->getProductAttributeForms());
            }

            return $result;
        }

        if($request->isPost) {
            $orderModel->load($data);
            try {
                if($orderModel->save()) {
                    if($request->post('save') == 0) {
                        Yii::$app->getSession()->setFlash('success', StringHelper::translateOrderToOffer('web', 'Order has been added successfully.'));
                        return $this->redirect(['order/details', 'id' => $orderModel->getOrder()->id]);
                    } else {
                        Yii::$app->response->format = Response::FORMAT_JSON;
                        return ['id' => $orderModel->getOrder()->id];
                    }
                }
            } catch(\Exception $e) {
                Yii::$app->getSession()->setFlash('error', StringHelper::translateOrderToOffer('web', 'Order data cannot be saved. Check provided values.'));
                Yii::error($e->getMessage());
                Yii::error($e->getTraceAsString());
            }
        }

        $forms = $orderModel->getAllForms();
        $total;
        $vats;
        $productForms = [];
        OrderModel::calculateSummary($productForms, $total, $vats);
        $shapes = Yii::$app->params['isOrderProductDimensionsVisible'] ? ArrayHelper::map(ShapeQuery::getNames(), 'id', 'name') : [];
        $constructions = Yii::$app->params['isOrderProductDimensionsVisible'] ? ArrayHelper::map(ConstructionQuery::getNames(), 'id', 'name') : [];
        $muntins = Yii::$app->params['isOrderProductDimensionsVisible'] ? ArrayHelper::map(MuntinQuery::getNames(), 'id', 'name') : [];

        return $this->render('create', $forms + [
                    'entities' => ArrayHelper::map(Entity::find()->all(), 'id', 'name'),
                    'currencies' => ArrayHelper::map(Currency::find()->all(), 'id', 'short_symbol'),
                    'orderTypes' => ArrayHelper::map(OrderTypeQuery::getOrderTypes(), 'id', 'name'),
                    'contractTypes' => ArrayHelper::map(OrderTypeQuery::getContractTypes(), 'id', 'name'),
                    'constructions' => $constructions,
                    'shapes' => $shapes,
                    'muntins' => $muntins,
                    'displayFirstAttributes' => true,
                    'invalidForms' => $orderModel->getInvalidForms(),
                    'plnCurrency' => Currency::find()->where('symbol = :symbol', [':symbol' => Utility::CURRENCY_PLN])->one(),
                    'summary' => ['total' => $total, 'vats' => $vats],
                    'scenario' => $orderModel->getScenario()
        ]);
    }

    public function actionGetProductsForm() {
        $form = ActiveForm::begin([
                    'id' => Yii::$app->request->post('id'),
                    'enableAjaxValidation' => true,
                    'enableClientValidation' => false,
                    'options' => [
                        'class' => 'form',
                    ]
        ]);

        $tempOrderProductForms = ModelLoaderHelper::loadMultiple(Yii::$app->request->post(), OrderProductForm::class);
        $tempProductAttributeForms = ModelLoaderHelper::loadMultiple(Yii::$app->request->post(), ProductAttributeForm::class);

        $orderProductForms = !empty($tempOrderProductForms) ? array_values($tempOrderProductForms) : [];
        $productAttributeForms = !empty($tempProductAttributeForms) ? array_values($tempProductAttributeForms) : [];
        $summary;
        $vats;

        if(!empty(Yii::$app->request->post('copyRow'))) {
            $rowId = Yii::$app->request->post('rowId');
            $inserted = [$orderProductForms[$rowId]];
            array_splice($orderProductForms, $rowId, 0, $inserted);

            if(Yii::$app->params['isProductAttributesVisible']) {
                $inserted = [clone($productAttributeForms[$rowId])];
                array_splice($productAttributeForms, $rowId, 0, $inserted);
                foreach($productAttributeForms as $productAttributeForm) {
                    $productAttributeForm->isRowVisible = false;
                }
                $productAttributeForms[$rowId + 1]->isRowVisible = true;
            }
        }

        if(!empty(Yii::$app->request->post('addRow'))) {
            array_push($orderProductForms, new OrderProductForm());

            if(Yii::$app->params['isProductAttributesVisible']) {
                foreach($productAttributeForms as $productAttributeForm) {
                    $productAttributeForm->isRowVisible = false;
                }

                array_push($productAttributeForms, new ProductAttributeForm());
                $productAttributeForms[count($productAttributeForms) - 1]->isRowVisible = true;
            }
        }

        OrderModel::calculateSummary($orderProductForms, $summary, $vats);

        $shapes = Yii::$app->params['isOrderProductDimensionsVisible'] ? ArrayHelper::map(ShapeQuery::getNames(), 'id', 'name') : [];
        $constructions = Yii::$app->params['isOrderProductDimensionsVisible'] ? ArrayHelper::map(ConstructionQuery::getNames(), 'id', 'name') : [];
        $muntins = Yii::$app->params['isOrderProductDimensionsVisible'] ? ArrayHelper::map(MuntinQuery::getNames(), 'id', 'name') : [];

        return $this->renderAjax('inc/_products', [
                    'orderProductsForms' => $orderProductForms,
                    'productAttributeForms' => $productAttributeForms,
                    'form' => $form,
                    'displayFirstAttributes' => false,
                    'orderProductsVats' => $vats,
                    'orderProductsSummary' => $summary,
                    'currencies' => ArrayHelper::map(Currency::find()->all(), 'id', 'short_symbol'),
                    'constructions' => $constructions,
                    'shapes' => $shapes,
                    'muntins' => $muntins,
                    'plnCurrency' => Currency::find()->where('symbol = :symbol', [':symbol' => Utility::CURRENCY_PLN])->one(),
        ]);
    }

    public function actionGetCostsForm() {
        $form = ActiveForm::begin([
                    'id' => Yii::$app->request->post('id'),
                    'enableAjaxValidation' => true,
                    'enableClientValidation' => false,
                    'options' => [
                        'class' => 'form',
                    ]
        ]);

        $tempOrderCostForms = ModelLoaderHelper::loadMultiple(Yii::$app->request->post(), OrderCostForm::class);
        $tempProductAttributeForms = ModelLoaderHelper::loadMultiple(Yii::$app->request->post(), ArtificialProductAttributeForm::class);
        $orderCostsForms = !empty($tempOrderCostForms) ? array_values($tempOrderCostForms) : [];
        $productAttributeForms = !empty($tempProductAttributeForms) ? array_values($tempProductAttributeForms) : [];
        $summary;
        $vats;

        if(!empty(Yii::$app->request->post('copyRow'))) {
            $rowId = Yii::$app->request->post('rowId');
            $inserted = [$orderCostsForms[$rowId]];

            array_splice($orderCostsForms, $rowId, 0, $inserted);

            if(Yii::$app->params['isProductAttributesVisible']) {
                $inserted = [clone($productAttributeForms[$rowId])];
                array_splice($productAttributeForms, $rowId, 0, $inserted);
                foreach($productAttributeForms as $productAttributeForm) {
                    $productAttributeForm->isRowVisible = false;
                }
                $productAttributeForms[$rowId + 1]->isRowVisible = true;
            }
        }

        if(!empty(Yii::$app->request->post('addRow'))) {
            array_push($orderCostsForms, new OrderCostForm());

            if(Yii::$app->params['isProductAttributesVisible']) {
                foreach($productAttributeForms as $productAttributeForm) {
                    $productAttributeForm->isRowVisible = false;
                }

                array_push($productAttributeForms, new ArtificialProductAttributeForm());
                $productAttributeForms[count($productAttributeForms) - 1]->isRowVisible = true;
            }
        }

        $shapes = Yii::$app->params['isOrderProductDimensionsVisible'] ? ArrayHelper::map(ShapeQuery::getNames(), 'id', 'name') : [];
        $constructions = Yii::$app->params['isOrderProductDimensionsVisible'] ? ArrayHelper::map(ConstructionQuery::getNames(), 'id', 'name') : [];
        $muntins = Yii::$app->params['isOrderProductDimensionsVisible'] ? ArrayHelper::map(MuntinQuery::getNames(), 'id', 'name') : [];


        OrderModel::calculateSummary($orderCostsForms, $summary, $vats);
        return $this->renderAjax('inc/_costs', [
                    'orderCostsForms' => $orderCostsForms,
                    'productAttributeForms' => $productAttributeForms,
                    'form' => $form,
                    'displayFirstAttributes' => false,
                    'constructions' => $constructions,
                    'shapes' => $shapes,
                    'muntins' => $muntins,
                    'displayFirstAttributes' => false,
                    'orderCostsVats' => $vats,
                    'orderCostsSummary' => $summary,
                    'currencies' => ArrayHelper::map(Currency::find()->all(), 'id', 'short_symbol'),
                    'plnCurrency' => Currency::find()->where('symbol = :symbol', [':symbol' => Utility::CURRENCY_PLN])->one(),
        ]);
    }

    public function actionGetCustomDataSummary() {
        return Json::encode(OrderTableForm::calculateTotalPrice(Yii::$app->request->post()));
    }

    public function actionGetCustomDataNumeration() {
        return Json::encode(OrderTableForm::numerateRows(Yii::$app->request->post()));
    }

    public function actionGetAllProductsSummary() {
        $orderProductForms = ModelLoaderHelper::loadMultiple(Yii::$app->request->post(), OrderProductForm::class);
        ;
        $orderCostForms = ModelLoaderHelper::loadMultiple(Yii::$app->request->post(), OrderCostForm::class);

        $data = array_merge($orderProductForms, $orderCostForms);
        $total = null;
        $vats = null;
        OrderModel::calculateSummary($data, $total, $vats);

        return $this->renderAjax('inc/_productSummary', [
                    'summary' => ['total' => $total, 'vats' => $vats],
                    'plnCurrency' => Currency::find()->where('symbol = :symbol', [':symbol' => Utility::CURRENCY_PLN])->one(),
        ]);
    }

    public function actionGetProductForm() {
        if(\Yii::$app->request->isAjax) {
            echo $this->renderAjax('inc/_product', [
                'displayFirstAttributes' => false,
                'orderProductForm' => new OrderProductForm(),
                'currencies' => ArrayHelper::map(Currency::find()->all(), 'id', 'short_symbol'),
                'prefix' => \Yii::$app->request->get('prefix'),
                'formId' => \Yii::$app->request->get('formId'),
                'plnCurrency' => Currency::find()->where('symbol = :symbol', [':symbol' => Utility::CURRENCY_PLN])->one(),
            ]);
        }
    }

    public function actionGetEmployeeForm() {
        $request = \Yii::$app->request;
        $employee = User::find()->where(['id' => $request->get('id')])->one();

        if($request->isAjax && $employee) {
            echo $this->renderAjax('inc/_employee', [
                'orderEmployeesForm' => new OrderEmployeesForm(),
                'employee' => $employee,
            ]);
        }
    }

    public function actionGetCostForm() {
        $request = \Yii::$app->request;

        if($request->isAjax) {
            echo $this->renderAjax('inc/_cost', [
                'orderCostForm' => new OrderCostForm(),
                'prefix' => $request->get('prefix'),
                'currencies' => ArrayHelper::map(Currency::find()->all(), 'id', 'short_symbol'),
                'formId' => $request->get('formId'),
                'plnCurrency' => Currency::find()->where('symbol = :symbol', [':symbol' => Utility::CURRENCY_PLN])->one(),
            ]);
        }
    }

    public function actionUpdate($id) {
        $request = Yii::$app->request;
        $data = $request->post();
        $orderHistoryId = isset($data['OrderHistoryForm']['orderHistoryId']) ? $data['OrderHistoryForm']['orderHistoryId'] : null;
        if(!empty($orderHistoryId)) {
            $orderModel = new OrderModel(OrderModel::SCENARIO_STATUS_UPDATE_CONT, $id);
        } else {
            $orderModel = new OrderModel(OrderModel::SCENARIO_STATUS_UPDATE, $id);
        }

        $order = Order::find()->where(['id' => $id])->with('orderHistoryLast')->one();
        if(empty($order) || !empty($order->is_deleted) || ($order->is_active == 0 && !(Yii::$app->user->can(Utility::ROLE_ADMIN) && Yii::$app->params['isUnlimitedOrderChangeStatusAvailable'])) || (!UserQuery::isSalesmanAssignedToCompanyByOrderId(Yii::$app->user->id, $id) &&
                (!Yii::$app->user->can(Utility::ROLE_MAIN_SALESMAN) && !UserQuery::isSalesmanAssignedToOrder(Yii::$app->user->id, $id)))) {
            throw new \yii\web\NotFoundHttpException(Yii::t('web', 'Element does not exist or you do not have permission.'));
        }

        //$orderModel = new OrderModel(OrderModel::SCENARIO_STATUS_UPDATE, $id);
        //$request = Yii::$app->request;
        if($request->isAjax && $request->isPost && $request->post('click') != 1) {
            $orderModel->load($request->post());
            Yii::$app->response->format = Response::FORMAT_JSON;

            $result = ActiveForm::validateMultiple($orderModel->getOrderProductsForms()) + ActiveForm::validateMultiple($orderModel->getDocumentsForms()) + ActiveForm::validateMultiple($orderModel->getPhotosForms()) + ActiveForm::validate($orderModel->getOrderHistoryForm()) + ActiveForm::validateMultiple($orderModel->getOrderCostsForms()) + ActiveForm::validate($orderModel->getOrderEmployeesForm()) + ActiveForm::validate($orderModel->getOrderTableForm());

            if(Yii::$app->params['isProductAttributesVisible']) {
                $result += ActiveForm::validateMultiple($orderModel->getProductAttributeForms());
            }

            return $result;
        }

        if($request->isPost) {
            $orderModel->load($request->post());

            try {
                if($orderModel->save()) {
                    if(empty($request->post('save'))) {
                        OrderDeadlineAlertHandler::check(['orderId' => $order->id]);
                        Yii::$app->getSession()->setFlash('success', StringHelper::translateOrderToOffer('web', 'Order has been updated successfully.'));
                        $orderHistoryForm = new OrderHistoryForm();
                        $orderHistoryForm->load($request->post());
                        //Yii::$app->getSession()->set('positiveTerminalStatus', OrderStatusQuery::isOrderStatusPositiveTerminal($orderHistoryForm->orderStatusId));
                        return $this->redirect(['order/details', 'id' => $id]);
                    } else {
                        Yii::$app->response->format = Response::FORMAT_JSON;
                        return ['id' => $orderModel->getOrderHistoryForm()->orderHistoryId];
                    }
                }
            } catch(\Exception $e) {
                Yii::$app->getSession()->setFlash('error', StringHelper::translateOrderToOffer('web', 'Order data cannot be saved. Check provided values.'));
                Yii::error($e->getMessage());
                Yii::error($e->getTraceAsString());
            }
        }

        $forms = $orderModel->getAllForms();
        $total;
        $vats;
        $productForms = array_merge($forms['orderProductsForms'], $forms['orderCostsForms']);
        OrderModel::calculateSummary($productForms, $total, $vats);
        $shapes = Yii::$app->params['isOrderProductDimensionsVisible'] ? ArrayHelper::map(ShapeQuery::getNames(), 'id', 'name') : [];
        $constructions = Yii::$app->params['isOrderProductDimensionsVisible'] ? ArrayHelper::map(ConstructionQuery::getNames(), 'id', 'name') : [];
        $muntins = Yii::$app->params['isOrderProductDimensionsVisible'] ? ArrayHelper::map(MuntinQuery::getNames(), 'id', 'name') : [];

        return $this->render('update', [
                    'id' => $id,
                    'currencies' => ArrayHelper::map(Currency::find()->all(), 'id', 'short_symbol'),
                    'constructions' => $constructions,
                    'shapes' => $shapes,
                    'muntins' => $muntins,
                    'displayFirstAttributes' => true,
                    'plnCurrency' => Currency::find()->where('symbol = :symbol', [':symbol' => Utility::CURRENCY_PLN])->one(),
                    'orderStatuses' => ArrayHelper::map(OrderStatusTranslation::find()->successorsOf($order->orderHistoryLast->order_status_id)->all(), 'order_status_id', 'name'),
                    'summary' => ['total' => $total, 'vats' => $vats],
                    'scenario' => $orderModel->getScenario()
                        ] + $forms + ['invalidForms' => $orderModel->getInvalidForms()]);
    }

    public function actionQuickUpdate($id) {

        $order = Order::find()->where(['id' => $id])->with('orderHistoryLast')->one();
        if(empty($order) || !empty($order->is_deleted) || ($order->is_active == 0 && !(Yii::$app->user->can(Utility::ROLE_ADMIN) && Yii::$app->params['isUnlimitedOrderChangeStatusAvailable'])) || (!UserQuery::isSalesmanAssignedToCompanyByOrderId(Yii::$app->user->id, $id) &&
                (!Yii::$app->user->can(Utility::ROLE_MAIN_SALESMAN) && !UserQuery::isSalesmanAssignedToOrder(Yii::$app->user->id, $id)))) {
            throw new \yii\web\NotFoundHttpException(Yii::t('web', 'Element does not exist or you do not have permission.'));
        }

        $orderModel = new OrderModel(OrderModel::SCENARIO_QUICK_STATUS_UPDATE, $id);

        //$orderHistoryForm = new OrderHistoryForm();
        //$orderHistoryForm->orderStatusId = $order->orderHistoryLast->order_status_id;
        //$orderHistoryForm->dateDeadline = $order->orderHistoryLast->date_deadline;
        //$orderHistoryForm->dateReminder = $order->orderHistoryLast->date_reminder;

        $request = Yii::$app->request;
        if($request->isAjax && $request->isPost) {
            $orderModel->load($request->post());
            Yii::$app->response->format = Response::FORMAT_JSON;

            return ActiveForm::validate($orderModel->getOrderHistoryForm());
        }

        if($request->isPost) {
            $orderModel->load($request->post());

            try {
                if($orderModel->save()) {
                    OrderDeadlineAlertHandler::check(['orderId' => $order->id]);
                    Yii::$app->getSession()->setFlash('success', StringHelper::translateOrderToOffer('web', 'Order has been updated successfully.'));
                    $orderHistoryForm = new OrderHistoryForm();
                    $orderHistoryForm->load($request->post());
                    //Yii::$app->getSession()->set('positiveTerminalStatus', OrderStatusQuery::isOrderStatusPositiveTerminal($orderHistoryForm->orderStatusId));
                    return $this->redirect(Yii::$app->request->referrer ?: ['order/details', 'id' => $id]);
                }
            } catch(\Exception $e) {
                Yii::$app->getSession()->setFlash('error', StringHelper::translateOrderToOffer('web', 'Order data cannot be saved. Check provided values.'));
                Yii::error($e->getMessage());
                Yii::error($e->getTraceAsString());
            }
        } else {
            $this->layout = 'modal';
        }
        return $this->renderAjax('quickUpdate', [
                    'id' => $id,
                    'orderHistoryForm' => $orderModel->getOrderHistoryForm(),
                    'orderStatuses' => ArrayHelper::map(OrderStatusTranslation::find()->successorsOf($order->orderHistoryLast->order_status_id)->all(), 'order_status_id', 'name'),
        ]);
    }

    public function actionCopy($id) {
        $order = Order::find()->where(['id' => $id])->one();
        if(empty($order) || !empty($order->is_deleted)) {
            throw new \yii\web\NotFoundHttpException(Yii::t('web', 'Element does not exist or you do not have permission.'));
        }
        $orderModel = new OrderModel(OrderModel::SCENARIO_COPY, $id);

        $request = Yii::$app->request;
        if($request->isAjax && $request->isPost) {
            $orderModel->load($request->post());
            Yii::$app->response->format = Response::FORMAT_JSON;

            return ActiveForm::validateMultiple($orderModel->getOrderProductsForms()) + ActiveForm::validateMultiple($orderModel->getDocumentsForms()) + ActiveForm::validateMultiple($orderModel->getPhotosForms()) + ActiveForm::validateMultiple($orderModel->getOrderCostsForms()) + ActiveForm::validate($orderModel->getOrderEmployeesForm()) + ActiveForm::validate($orderModel->getOrderCreateForm());
        }

        if($request->isPost) {
            $orderModel->load($request->post());

            try {
                if($orderModel->save()) {
                    Yii::$app->getSession()->setFlash('success', StringHelper::translateOrderToOffer('web', 'Order has been added successfully.'));
                    return $this->redirect(['order/details', 'id' => $orderModel->getOrder()->id]);
                }
            } catch(\Exception $e) {
                Yii::$app->getSession()->setFlash('error', StringHelper::translateOrderToOffer('web', 'Order data cannot be saved. Check provided values.'));
                Yii::error($e->getMessage());
                Yii::error($e->getTraceAsString());
            }
        }

        $forms = $orderModel->getAllForms();
        $total;
        $vats;
        $productForms = array_merge($forms['orderProductsForms'], $forms['orderCostsForms']);
        OrderModel::calculateSummary($productForms, $total, $vats);
        $shapes = Yii::$app->params['isOrderProductDimensionsVisible'] ? ArrayHelper::map(ShapeQuery::getNames(), 'id', 'name') : [];
        $constructions = Yii::$app->params['isOrderProductDimensionsVisible'] ? ArrayHelper::map(ConstructionQuery::getNames(), 'id', 'name') : [];
        $muntins = Yii::$app->params['isOrderProductDimensionsVisible'] ? ArrayHelper::map(MuntinQuery::getNames(), 'id', 'name') : [];

        return $this->render('copy', $forms + [
                    'entities' => ArrayHelper::map(Entity::find()->all(), 'id', 'name'),
                    'currencies' => ArrayHelper::map(Currency::find()->all(), 'id', 'short_symbol'),
                    'constructions' => $constructions,
                    'shapes' => $shapes,
                    'muntins' => $muntins,
                    'orderTypes' => ArrayHelper::map(OrderTypeQuery::getOrderTypes(), 'id', 'name'),
                    'contractTypes' => ArrayHelper::map(OrderTypeQuery::getContractTypes(), 'id', 'name'),
                    'plnCurrency' => Currency::find()->where('symbol = :symbol', [':symbol' => Utility::CURRENCY_PLN])->one(),
                    'invalidForms' => $orderModel->getInvalidForms(),
                    'summary' => ['total' => $total, 'vats' => $vats],
                    'displayFirstAttributes' => true,
            ]);
    }

    public function actionFind() {
        $request = Yii::$app->request;
        $term = $request->get('term');
        $page = $request->get('page', 1);
        $size = $request->get('size');

        if(empty($term) || empty($page) || empty($size)) {
            Yii::$app->end();
        }

        $query = OrderQuery::findOrders(['name' => $term]);
        $count = $query->count();
        $items = $query->limit($size)->offset($size * ($page - 1))->all();

        echo json_encode([
            'items' => $items,
            'more' => (intval($count) > $page * $size),
        ]);
    }

    public function actionFindParents() {
        $request = Yii::$app->request;
        $term = $request->get('term');
        $page = $request->get('page', 1);
        $size = $request->get('size');

        if(empty($term) || empty($page) || empty($size)) {
            Yii::$app->end();
        }

        $query = OrderQuery::findParentOrders(['name' => $term]);
        $count = $query->count();
        $items = $query->limit($size)->offset($size * ($page - 1))->all();

        echo json_encode([
            'items' => $items,
            'more' => (intval($count) > $page * $size),
        ]);
    }

    public function actionEdit($id) {
        $order = Order::findOne($id);
        if(empty($order) || !empty($order->is_deleted) || (!UserQuery::isSalesmanAssignedToCompanyByOrderId(Yii::$app->user->id, $id) &&
                (!Yii::$app->user->can(Utility::ROLE_MAIN_SALESMAN) && !UserQuery::isSalesmanAssignedToOrder(Yii::$app->user->id, $id)))) {
            //Yii::$app->getSession()->setFlash('error', StringHelper::translateOrderToOffer('web', 'An order does not exist or you do not have access rights.'));
            //return $this->redirect(['site/index']);

            throw new \yii\web\NotFoundHttpException(StringHelper::translateOrderToOffer('web', 'Order data cannot be saved. Check provided values.'));
        }

        $orderModel = new OrderModel(OrderModel::SCENARIO_EDIT, $id);

        $request = Yii::$app->request;
        if($request->isAjax && $request->isPost) {
            $orderModel->load($request->post());
            Yii::$app->response->format = Response::FORMAT_JSON;

            return ActiveForm::validate($orderModel->getOrderCreateForm());
        }

        if($request->isPost) {
            $orderModel->load($request->post());
            try {
                if($orderModel->save()) {
                    Yii::$app->getSession()->setFlash('success', StringHelper::translateOrderToOffer('web', 'Order has been updated successfully.'));
                    return $this->redirect(['order/details', 'id' => $id]);
                }
            } catch(\Exception $e) {
                Yii::$app->getSession()->setFlash('error', StringHelper::translateOrderToOffer('web', 'Order data cannot be saved. Check provided values.'));
                Yii::error($e->getMessage());
                Yii::error($e->getTraceAsString());
            }
        }

        $forms = $orderModel->getAllForms();
        return $this->render('edit', $forms + [
                    'entities' => ArrayHelper::map(Entity::find()->all(), 'id', 'name'),
                    'currencies' => ArrayHelper::map(Currency::find()->all(), 'id', 'short_symbol'),
                    'orderTypes' => ArrayHelper::map(OrderTypeQuery::getOrderTypes(), 'id', 'name'),
                    'contractTypes' => ArrayHelper::map(OrderTypeQuery::getContractTypes(), 'id', 'name'),
                    'invalidForms' => $orderModel->getInvalidForms(),
                    'scenario' => $orderModel->getScenario(),
                    'id' => $id,
        ]);
    }

    public function actionSetAlert() {
        $request = Yii::$app->request;

        if($request->isPost) {
            $form = new AlertForm();
            $form->load($request->post());

            if($request->isAjax) {
                Yii::$app->response->format = Response::FORMAT_JSON;
                return ActiveForm::validate($form);
            }

            try {
                $form->save();
                Yii::$app->getSession()->setFlash('success', StringHelper::translateOrderToOffer('web', 'An order renewing alert has been created successfully'));
            } catch(\Exception $e) {
                Yii::$app->getSession()->setFlash('error', StringHelper::translateOrderToOffer('web', 'Cannot create an order renewing alert'));
                Yii::error($e->getMessage());
                Yii::error($e->getTraceAsString());
            }
            return $this->redirect(['order/details', 'id' => $form->orderId]);
        } else {
            throw new BadRequestHttpException(Yii::t('web', 'set-alert action can be invoked only through POST request'));
        }
    }

    private function getDocuments($id) {
        if(isset(Yii::$app->params['isDocumentStorageAvailable']) && Yii::$app->params['isDocumentStorageAvailable']) {
            $where = ['history.sections.orderId' => (int) $id];
            $query = new Query();

            $query->select([])
                    ->from('documents')
                    ->orderBy(['history.createdAt' => SORT_DESC])
                    ->where($where);

            $rows = $query->all();

            return $rows;
        } else {
            return [];
        }
    }

    public function actionGetUserRepresentatives() {
        $companyId = Yii::$app->request->get('companyId');
        Yii::$app->response->format = Response::FORMAT_JSON;
        return User::findAll(['company_id' => $companyId, 'is_active' => 1]);
    }

    public function actionDelete() {
        $id = Yii::$app->request->post('id');

        try {
            $order = Order::findOne($id);
            if(!empty($order->is_deleted)) {
                throw new \Exception('The user has not privilege to delete this order: ' . $id);
            }

            $order->is_deleted = 1;
            $order->user_deleting_id = Yii::$app->user->id;
            $order->save();

            OrderDeadlineAlertHandler::check(['orderId' => $order->id]);

            Yii::$app->getSession()->setFlash('success', StringHelper::translateOrderToOffer('web', 'The order has been deleted.'));
        } catch(\Exception $e) {
            Yii::error(print_r($e->getMessage(), true));
            Yii::$app->getSession()->setFlash('error', StringHelper::translateOrderToOffer('web', 'The order cannot be deleted. Please contact the administrator.'));
        }

        if(strpos(Yii::$app->request->getReferrer(), Yii::$app->controller->id . '/details') !== false) {
            return $this->redirect(['list']);
        }
        return $this->redirect(Yii::$app->request->getReferrer());
    }

    public function getCostTable() {
        return Yii::$app->controller->renderPartial('inc/_costs', ['orderCostsForms' => $orderCostsForms, 'currencies' => $currencies, 'form' => $form]);
    }

    public function actionJsonCalendar($dateType) {

        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;

        $orders = OrderQuery::getOrdersToCalendar($dateType);

        $events = [];

        foreach($orders AS $order) {
            //Testing
            $Event = new \yii2fullcalendar\models\Event();
            $Event->id = $order['dateEnd'];
            $Event->title = $order['title'];
            $Event->start = date('Y-m-d', strtotime($order['dateEnd']));
            $Event->end = date('Y-m-d', strtotime($order['dateEnd']));
            $events[] = $Event;
        }

        return $events;
    }

    public function actionShortOrder($date, $dateType) {
        $orders = OrderQuery::getShortOrder($date, $dateType);

        echo json_encode($orders);
    }

    public function actionShortList() {
        $request = Yii::$app->request;
        $listForm = new OrderListForm();
        $listForm->load($request->get());

        $itemsQuery = OrderQuery::getActiveOrderQuery(Yii::$app->user->id, $listForm->toArray());
        $pages = new Pagination([
            'totalCount' => $itemsQuery->count(),
            'page' => $listForm->page,
            'defaultPageSize' => 10,
        ]);
        $items = $itemsQuery->limit(10)->offset(0)->all();

        $orderLogic = new OrderLogic();
        $items = $orderLogic->processOrder($items);

        $listData = [
            'items' => $items,
            'pages' => $pages,
            'listForm' => $listForm,
        ];

        if($request->isAjax && !Yii::$app->request->get('dashboard')) {
            return json_encode([
                'view' => $this->renderAjax('inc/_shortOrder', $listData),
                'url' => Url::current([], true),
            ]);
        }
        $listData['orderStatuses'] = ArrayHelper::map(OrderQuery::getAllStatuses(), 'id', 'name');
        $listData['orderStatuses'] = [0 => Yii::t('web', 'All')] + $listData['orderStatuses'];
        if(Yii::$app->request->get('dashboard')) {
            return $this->renderAjax('shortList', $listData);
        } else {
            return $this->render('shortList', $listData);
        }
    }

    public function actionShortListDashboard() {
        $request = Yii::$app->request;
        $ordersListForm = new OrdersListForm();
        $ordersListForm->load($request->get());

        $inProgressStatus = OrderStatus::find()->where(['symbol' => Utility::ORDER_STATUS_IN_PROGRESS])->one();
        $ordersListForm->orderStatusesIds = [$inProgressStatus->id];

        $activeOrdersQuery = OrderQuery::getOrdersQuery($ordersListForm->toArray());
        $pages = new Pagination([
            'totalCount' => $activeOrdersQuery->count(),
            'page' => $ordersListForm->page,
            'defaultPageSize' => \Yii::$app->params['defaultDashboardPageSize'],
        ]);
        $orders = $activeOrdersQuery->limit($pages->defaultPageSize)->offset($pages->page * $pages->defaultPageSize)->all();

        $ordersListData = [
            'orders' => $orders,
            'pages' => $pages,
            'ordersListForm' => $ordersListForm,
        ];

        if($request->isAjax && !Yii::$app->request->get('dashboard')) {
            return json_encode([
                'view' => $this->renderAjax('inc/_orders', $ordersListData),
                'url' => Url::current([], true),
            ]);
        } else {
            $ordersListData['ordersStatusesTranslations'] = OrderStatusTranslation::find()->forLanguage(\Yii::$app->language)->all();
            if(Yii::$app->request->get('dashboard')) {
                return $this->renderAjax('orderListShort', $ordersListData);
            }
        }
    }

    public function actionDeleteAttachment() {
        $id = Yii::$app->request->post('id');
        $url = Yii::$app->request->post('url');
        $attachment = OrderAttachment::findOne($id);
        UploadHelper::deleteAttachment($url, $attachment);
        if(!Yii::$app->request->isAjax) {
            return $this->redirect(Yii::$app->request->getReferrer());
        }
    }

    public function actionGetColorAndShape() {
        $shapeId = Yii::$app->request->get('shapeId');
        $ralColor = Yii::$app->request->get('ralColour');

        $shape = Shape::find()->where(['id' => $shapeId])->one();
        $colour = RalColour::find()->where(['symbol' => $ralColor])->one();

        echo json_encode(['shape' => !empty($shape) ? $shape->identifier : '', 'colour' => !empty($colour) ? $colour->rgb_hash : '']);
    }

}
