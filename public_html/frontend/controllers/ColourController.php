<?php

namespace frontend\controllers;

use Yii;
use frontend\components\Controller;
use yii\filters\AccessControl;
use common\helpers\Utility;
use frontend\models\ClientRepresentativeForm;
use frontend\models\ClientRepresentativeListForm;
use common\models\aq\UserQuery;
use yii\data\Pagination;
use yii\web\Response;
use yii\widgets\ActiveForm;
use common\models\ar\User;
use yii\helpers\Url;
use common\models\aq\RalColourGroupQuery;
use yii\helpers\ArrayHelper;

class ColourController extends Controller {
    
   /* public function behaviors() {
        $actions = ['list', 'create', 'details', 'delete', 'edit'];
        
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => $actions,
                'rules' => [
                    [
                        'actions' => $actions,
                        'allow' => Yii::$app->params['isClientRepresentativeManagementVisible'],
                        'roles' => [Utility::ROLE_SALESMAN],
                    ],
                ],
            ],
        ];
    }
    */
    public function actionRenderRalColourPicker() {
       $colours = RalColourGroupQuery::getAllColours();
       $groups = RalColourGroupQuery::getGroups();
       $colours = ArrayHelper::index($colours, null, 'groupSymbol');
       return $this->render('ralColourPicker', [
           'groups' => $groups,
           'colours' => $colours,
       ]);
       
    }
    
    public function actionGetModalContent() {
        $colours = RalColourGroupQuery::getAllColours();
        $groups = RalColourGroupQuery::getGroups();
        $colours = ArrayHelper::index($colours, null, 'groupSymbol');
        
        return $this->renderPartial('_ralColourPickerModalContent', [
            'groups' => $groups,
            'colours' => $colours,
        ]);
    }
    
    
}