<?php

namespace frontend\controllers;

use Yii;
use frontend\components\Controller;
use yii\filters\AccessControl;
use yii\web\Response;
use yii\widgets\ActiveForm;
use common\documents\DocumentsGenerator;
use common\documents\DocumentType;
use common\documents\pdf\Renderer;
use common\documents\forms\LightMeasurementForm;
use frontend\helpers\DocumentHelper;
use common\documents\DocumentTypeUtility;
use common\helpers\Utility;

//TODO refatoryzacja do zrobienia
class ProtocolController extends Controller {

    public function behaviors() {
        $allActions = ['service', 'light-measurement', 'weight', 'repair', 'get-row', 'get-table', 'get-row-light', 
        		'get-row-weight', 'custom', 'get-row-repair', 'get-row-repair-replacements', 'get-row-repair-tests',
        		'service-fdafas',
        ];

        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => $allActions,
                'rules' => [
                        [
                        'actions' => $allActions,
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
        ];
    }

    public function actionLightMeasurement($orderId = 0, $edit = false) {
    	if(!in_array(DocumentType::LIGHT_MEASUREMENT, DocumentTypeUtility::getAvailableDocuments())) {
    		throw new \yii\web\NotFoundHttpException(Yii::t('web', 'Element does not exist or you do not have permission.'));
    	}
    	
        $request = Yii::$app->request;
        $documentId = $request->get('documentId');
        $historyId = $request->get('historyId');
        $render = $request->isPost;
        $save = true;
        $documentType = DocumentType::LIGHT_MEASUREMENT;
        
        if(!empty($documentId) && !empty($historyId) && $orderId == 0) {
            $document = DocumentsGenerator::reconstruct($documentId, $historyId);
            $render = true;
            $save = false;
        } else {
            if(!empty($documentId) && !empty($historyId)) {
                $document = DocumentsGenerator::reconstruct($documentId, $historyId, $edit);
            } else {
                $document = DocumentsGenerator::generate($documentType);
            }
            $document->makeEditable();
       
            $dataInput = $request->isPost ? $request->post() : [];
        }
            $dataInput['LightMeasurement']['form'] = new LightMeasurementForm();
            $dataInput['LightMeasurement']['orderId'] = $orderId;

            $document->loadInput($dataInput);
        
        if($request->isPost && $request->isAjax) {
            Yii::$app->response->format = Response::FORMAT_JSON;
            return $document->performActiveValidation();
        }

        if($render) {
            if($save) {
                DocumentsGenerator::save($document);
            }
            
            $title = Yii::t('documents', 'Protocol of light intensity measurement');
            
            (new \common\documents\pdf\Renderer([
                        'options' => [
                            'title' => $title
                            ]
                ], $title
            ))->render($document);
        } else {
            $documentView = (new \common\documents\html\Renderer())->render($document);
            return $this->render('form', [
                'documentView' => $documentView,
                'documents' => DocumentHelper::getDocuments($orderId, $documentType),
                'orderId' => $orderId,
            ]);
        }
    }

    public function actionService($orderId = 0, $edit = false, $documentType = null, $title = null) {
    	if(!in_array(DocumentType::SERVICE_PROTOCOL, DocumentTypeUtility::getAvailableDocuments())) {
    		throw new \yii\web\NotFoundHttpException(Yii::t('web', 'Element does not exist or you do not have permission.'));
    	}
    	
        $request = Yii::$app->request;
        $documentId = $request->get('documentId');
        $historyId = $request->get('historyId');
        $render = $request->isPost;
        $save = true;
        
        if(empty($documentType)) {
        	$documentType = DocumentType::SERVICE_PROTOCOL;
        }
        
        if(!empty($documentId) && !empty($historyId) && $orderId == 0) {
            $document = DocumentsGenerator::reconstruct($documentId, $historyId);
            $render = true;
            $save = false;
        } else {
            if(!empty($documentId) && !empty($historyId)) {
                $document = DocumentsGenerator::reconstruct($documentId, $historyId, $edit);
            } else {
                $document = DocumentsGenerator::generate($documentType);
            }
            $document->makeEditable();


            $dataInput = $request->isPost ? $request->post() : [];
            if(isset($dataInput['tables'])) {
                $dataInput['ServiceProtocol']['tables'] = $dataInput['tables'];
            }
            $dataInput['ServiceProtocol']['orderId'] = $orderId ?: 0;

            $document->loadInput($dataInput);
        }

        if($request->isPost && $request->isAjax) {
            Yii::$app->response->format = Response::FORMAT_JSON;
            return $document->performActiveValidation();
        }

        if($render) {
            if($save) {
                DocumentsGenerator::save($document);
            }
            
            if(empty($title)) {
            	$title = Yii::t('documents', 'Service protocol');
            }
            
            (new \common\documents\pdf\Renderer([
                        'options' => [
                            'title' => $title
                            ]
                ], $title
            ))->render($document);
        } else {
            $documentView = (new \common\documents\html\Renderer())->render($document);
            return $this->render('form', [
                'documentView' => $documentView,
                'documents' => DocumentHelper::getDocuments($orderId, $documentType),
                'orderId' => $orderId,
            ]);
        }
    }
    
    public function actionServiceFdafas($orderId = 0, $edit = false) {
    	if(!in_array(DocumentType::SERVICE_PROTOCOL_FDAFAS, DocumentTypeUtility::getAvailableDocuments())) {
    		throw new \yii\web\NotFoundHttpException(Yii::t('web', 'Element does not exist or you do not have permission.'));
    	}
    	
    	$documentType = DocumentType::SERVICE_PROTOCOL_FDAFAS;
    	$title = Yii::t('documents', 'Service protocol of fire alarm system');
    	
    	return $this->actionService($orderId, $edit, $documentType, $title);
    }

    public function actionWeight($orderId = 0, $edit = false) {
    	if(!in_array(DocumentType::WEIGHT_PROTOCOL, DocumentTypeUtility::getAvailableDocuments())) {
    		throw new \yii\web\NotFoundHttpException(Yii::t('web', 'Element does not exist or you do not have permission.'));
    	}
    	
        $request = Yii::$app->request;
        $documentId = $request->get('documentId');
        $historyId = $request->get('historyId');
        $render = $request->isPost;
        $save = true;
        $documentType = DocumentType::WEIGHT_PROTOCOL;
        
        if(!empty($documentId) && !empty($historyId) && $orderId == 0) {
            $document = DocumentsGenerator::reconstruct($documentId, $historyId);
            $dataInput['WeightProtocol']['form'] = new \common\documents\forms\WeightProtocolForm();
            $document->loadInput($dataInput);
            $render = true;
            $save = false;
        } else {
            if(!empty($documentId) && !empty($historyId)) {
                $document = DocumentsGenerator::reconstruct($documentId, $historyId, $edit);
            } else {
                $document = DocumentsGenerator::generate($documentType);
            }
            $document->makeEditable();

            $dataInput = $request->isPost ? $request->post() : [];

            if(isset($dataInput['tables'])) {
                $dataInput['WeightProtocol']['tables'] = $dataInput['tables'];
            }
            $dataInput['WeightProtocol']['orderId'] = $orderId;

            $document->loadInput($dataInput);
        }

        if($request->isPost && $request->isAjax) {
            Yii::$app->response->format = Response::FORMAT_JSON;
            return $document->performActiveValidation();
        }

        if($render) {
            if($save) {
                DocumentsGenerator::save($document);
            }
            
            $title = Yii::t('documents', 'Weighting CO2 protocol');
            
            (new \common\documents\pdf\Renderer([
                        'options' => [
                            'title' => $title,
                            ]
                ], $title
            ))->render($document);
        } else {
            $documentView = (new \common\documents\html\Renderer())->render($document);
            return $this->render('form', [
                'documentView' => $documentView,
                'documents' => DocumentHelper::getDocuments($orderId, $documentType),
                'orderId' => $orderId,
            ]);
        }
    }
    public function actionRepair($orderId = 0, $edit = false) {
    	if(!in_array(DocumentType::REPAIR_PROTOCOL, DocumentTypeUtility::getAvailableDocuments())) {
    		throw new \yii\web\NotFoundHttpException(Yii::t('web', 'Element does not exist or you do not have permission.'));
    	}
    	
        $request = Yii::$app->request;
        $documentId = $request->get('documentId');
        $historyId = $request->get('historyId');
        $render = $request->isPost;
        $save = true;
        $documentType = DocumentType::REPAIR_PROTOCOL;

        if(!empty($documentId) && !empty($historyId) && $orderId == 0 ) {
            $document = DocumentsGenerator::reconstruct($documentId, $historyId);
            $render = true;
            $save = false;
        } else {
            if(!empty($documentId) && !empty($historyId)) {
                $document = DocumentsGenerator::reconstruct($documentId, $historyId, $edit);
            } else {
                $document = DocumentsGenerator::generate($documentType);
            }
            $document->makeEditable();

            $dataInput = $request->isPost ? $request->post() : [];
        }
            $dataInput['RepairProtocol']['form'] = new \common\documents\forms\RepairProtocolForm();
            $dataInput['RepairProtocol']['formReplacements'] = new \common\documents\forms\RepairReplacementsProtocolForm();
            $dataInput['RepairProtocol']['formTests'] = new \common\documents\forms\RepairTestsProtocolForm();
            $dataInput['RepairProtocol']['orderId'] = $orderId;

            $document->loadInput($dataInput);


        if($request->isPost && $request->isAjax) {
            Yii::$app->response->format = Response::FORMAT_JSON;
            return $document->performActiveValidation();
        }

        if($render) {
            if($save) {
                DocumentsGenerator::save($document);
            }
            
            $title = Yii::t('documents', 'Repair protocol');
            
            (new \common\documents\pdf\Renderer([
                        'options' => [
                            'title' => $title,
                            ]
                ], $title
            ))->render($document);
        } else {
            $documentView = (new \common\documents\html\Renderer())->render($document);
            return $this->render('form', [
                'documentView' => $documentView,
                'documents' => DocumentHelper::getDocuments($orderId, $documentType),
                'orderId' => $orderId
            ]);
        }
    }

    public function actionGetRow() {
        $request = Yii::$app->request;
        $theadCount = $request->get('count');
        $tableViewsDir = $request->get('tableViewsDir');
        $tableViewsDir = !empty($tableViewsDir) ? $tableViewsDir : '@common/documents/html/views/table';
        
        $table = [
            	'theads' => [],
        ];
        
        for($i = 0; $i < $theadCount; $i++) {
            $table['theads'] = array_merge($table['theads'], ['']);
        }
        
        return $this->renderFile($tableViewsDir.'/row.php', [
        		'edit' => $request->get('edit'),
                'idx' => $request->get('idx'),
                'uniq' => $request->get('uniq'),
                'table' => $table,
        		'initData' => $request->get('initData'),
        ]);
    }

    public function actionGetTable() {
        $request = Yii::$app->request;
        $theadCount = $request->get('count');
        $tableViewsDir = $request->get('tableViewsDir');
        $tableViewsDir = !empty($tableViewsDir) ? $tableViewsDir : '@common/documents/html/views/table';
        $predefinedHeaders = intval($request->get('predefinedHeaders', 0));
        
        $table = [
	            'theads' => [],
	            'rows' => [[]],
        ];
        
        if(!empty($predefinedHeaders)) {
        	$table['theads'] = [ Yii::t('documents', 'No.'), Yii::t('web', 'Name') ];
        	
        	for($i = 0; $i < $theadCount - 5; $i++) {
        		$table['theads'] = array_merge($table['theads'], ['']);
        	}
        	
        	$table['theads'] = array_merge($table['theads'], [ 
        			Yii::t('web', 'Unit price'), Yii::t('web', 'Count'), Yii::t('web', 'Total price') 
        	]);
        }
        else {
        	$initData = $request->get('initData');
        	$copyOption = $request->get('copyOption');
        	if(!empty($initData) && isset($copyOption)) {
        		$table = $this->prepareCopiedTable($initData, $copyOption);
        	}
        	else {
	        	for($i = 0; $i < $theadCount; $i++) {
	        		$table['theads'] = array_merge($table['theads'], ['']);
	        	}
        	}
        }
        
        return $this->renderFile($tableViewsDir.'/table.php', [
        		'edit' => $request->get('edit'),
                'table' => ['rows' => [[]]],
                'showScript' => true,
                'table' => $table,
        ]);
    }
    
    private function prepareCopiedTable($initData, $copyOption) {
    	$data = [];
    	parse_str($initData, $data);
    	
    	$table = [];
    	$cleanData = reset($data['tables']);
    	$table['name'] = $cleanData['name'];
    	$table['theads'] = $cleanData['theads'];
    	$table['rows'] = [];
    	
    	if($copyOption == Utility::PROTOCOL_COPY_TABLE_WITH_ADDITION_ONE_COLUMN) {
    		$table['theads'][] = '';
    	}
    	
    	if($copyOption == Utility::PROTOCOL_COPY_TABLE_WITHOUT_MODIFICATION) {
    		$table['theads'] = $cleanData['theads'];
    		$table['rows'] = $cleanData['rows'];
    	}
    	else {
    		$columnSub = $copyOption == Utility::PROTOCOL_COPY_TABLE_WITH_REMOVING_ONE_COLUMN ? 1 : 0;
    		$table['theads'] = array_slice($cleanData['theads'], 0, count($cleanData['theads']) - $columnSub);
    		if($copyOption == Utility::PROTOCOL_COPY_TABLE_WITH_ADDITION_ONE_COLUMN) {
    			$table['theads'][] = '';
    		}
    		
    		foreach($cleanData['rows'] as $row) {
    			$newRow = [];
    			for($i = 0; $i < count($row) - $columnSub; ++$i) {
					$newRow[] = $row[$i];	
    			}
    			
    			if($copyOption == Utility::PROTOCOL_COPY_TABLE_WITH_ADDITION_ONE_COLUMN) {
    				$newRow[] = '';
    			}
					
    			$table['rows'][] = $newRow;
    		}
    	}
    	
    	return $table;
    }

    public function actionGetRowLight() {
        $row = new LightMeasurementForm();
        $request = Yii::$app->request;
        $initData = $request->get('initData');
        if(!empty($initData)) {
        	$row->name = $initData[0];
        	$row->value = $initData[1];
        }

        return $this->renderFile('@common/documents/html/views/single-table/row.php', [
                    'formModel' => $row,
                    'form' => new \yii\widgets\ActiveForm(),
                    'edit' => $request->get('edit'),
                    'idx' => $request->get('idx'),
        ]);
    }

    public function actionGetRowRepair() {
        $row = new \common\documents\forms\RepairProtocolForm();

        $request = Yii::$app->request;
        return $this->renderFile('@common/documents/html/views/repair-table/row.php', [
                    'formModel' => $row,
                    'form' => new \yii\widgets\ActiveForm(),
                    'edit' => $request->get('edit'),
                    'idx' => $request->get('idx'),
        ]);
    }
    
    public function actionGetRowRepairReplacements() {
        $row = new \common\documents\forms\RepairReplacementsProtocolForm();
        $request = Yii::$app->request;
        $initData = $request->get('initData');
        if(!empty($initData)) {
        	$row->name = $initData[0];
        	$row->count = $initData[1];
        }
        
        return $this->renderFile('@common/documents/html/views/single-table/row.php', [
                    'formModel' => $row,
                    'form' => new \yii\widgets\ActiveForm(),
                    'edit' => $request->get('edit'),
                    'idx' => $request->get('idx'),
        ]);
    }
       
    public function actionGetRowRepairTests() {
        $row = new \common\documents\forms\RepairTestsProtocolForm();
        $request = Yii::$app->request;
        $initData = $request->get('initData');
        if(!empty($initData)) {
        	$row->device = $initData[0];
        	$row->result = $initData[1];
        }
        
        return $this->renderFile('@common/documents/html/views/single-table/row.php', [
                    'formModel' => $row,
                    'form' => new \yii\widgets\ActiveForm(),
                    'edit' => $request->get('edit'),
                    'idx' => $request->get('idx'),
        ]);
    }
    public function actionGetRowWeight() {
        $row = new \common\documents\forms\WeightProtocolForm();

        $request = Yii::$app->request;
        return $this->renderFile('@common/documents/html/views/single-table/row.php', [
                    'formModel' => $row,
                    'form' => new \yii\widgets\ActiveForm(),
                    'edit' => $request->get('edit'),
                    'idx' => $request->get('idx'),
        ]);
    }
    public function actionCustom($orderId = 0, $edit = false) {
    	if(!in_array(DocumentType::CUSTOM_PROTOCOL, DocumentTypeUtility::getAvailableDocuments())) {
    		throw new \yii\web\NotFoundHttpException(Yii::t('web', 'Element does not exist or you do not have permission.'));
    	}
    	
    	$request = Yii::$app->request;
    	$documentId = $request->get('documentId');
    	$historyId = $request->get('historyId');
    	$render = $request->isPost;
    	$save = true;
    	$documentType = DocumentType::CUSTOM_PROTOCOL;
    	
    	if(!empty($documentId) && !empty($historyId) && $orderId == 0) {
    		$document = DocumentsGenerator::reconstruct($documentId, $historyId);
    		$render = true;
    		$save = false;
    	} 
    	else {
    		if(!empty($documentId) && !empty($historyId)) {
    			$document = DocumentsGenerator::reconstruct($documentId, $historyId, $edit);
    		} 
    		else {
    			$document = DocumentsGenerator::generate($documentType);
    		}
    		$document->makeEditable();
    	
    		$dataInput = $request->isPost ? $request->post() : [];
    	
    		$dataInput['CustomProtocol']['orderId'] = $orderId;
    	
    		$document->loadInput($dataInput);
    	}
    	
    	if($request->isPost && $request->isAjax) {
    		Yii::$app->response->format = Response::FORMAT_JSON;
    		return $document->performActiveValidation();
    	}
    	
    	if($render) {
    		if($save) {
    			DocumentsGenerator::save($document);
    		}
    		
    		$title = Yii::t('documents', 'Custom protocol');
    		
    		(new \common\documents\pdf\Renderer([
	    				'options' => [
	    						'title' => $title,
	    				]
	    		], $title
			))->render($document);
    	} else {
    		$documentView = (new \common\documents\html\Renderer())->render($document);
    		return $this->render('form', [
    				'documentView' => $documentView,
    				'documents' => DocumentHelper::getDocuments($orderId, $documentType),
    				'orderId' => $orderId
    		]);
    	}
    }
}
