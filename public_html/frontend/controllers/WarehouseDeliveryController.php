<?php
namespace frontend\controllers;

use Yii;
use frontend\components\Controller;
use yii\filters\AccessControl;
use common\helpers\Utility;
use yii\data\Pagination;
use yii\helpers\Url;
use frontend\models\WarehouseDeliveryListForm;
use common\models\aq\WarehouseDeliveryQuery;
use common\models\ar\WarehouseDelivery;
use common\models\aq\WarehouseQuery;
use common\models\aq\WarehouseSupplierQuery;
use yii\web\Response;
use yii\widgets\ActiveForm;
use frontend\models\WarehouseDeliveryProductListForm;
use frontend\models\WarehouseDeliveryProductForm;
use frontend\models\WarehouseDeliveryModel;
use yii\filters\VerbFilter;
use common\models\aq\ProductPhotoQuery;
use common\models\ar\WarehouseProductOperation;
use common\documents\DocumentTypeUtility;

class WarehouseDeliveryController extends Controller {

	public function behaviors() {
        $actions = ['list', 'create', 'edit', 'delete', 'details', 'get-product-form', 'confirm'];
		return [
				'access' => [
						'class' => AccessControl::className(),
						'only' => $actions,
						'rules' => [
								[
										'actions' => ['list', 'create', 'details', 'get-product-form', 'confirm', 'delete'],
										'allow' => true,
										'roles' => [Utility::ROLE_STOREKEEPER],
								],
								[
										'actions' => ['edit'],
										'allow' => false,
								],
						],
				],
				'verbs' => [
						'class' => VerbFilter::className(),
						'actions' => [
								'delete' => ['post'],
						],
				],
		];
	}

	public function actionList() {
		$request = Yii::$app->request;
		$listForm = new WarehouseDeliveryListForm();
		if(!$request->isAjax) {
			$warehouseOptions = WarehouseQuery::getWarehouseList()->all();
			$listForm->allWarehousesCount = count($warehouseOptions);
			$supplierOptions = WarehouseSupplierQuery::getSupplierList();
			$listForm->allSuppliersCount = count($supplierOptions);
		}

		$listForm->load($request->get());

		$itemsQuery = WarehouseDeliveryQuery::getWarehouseDeliveriesQuery($listForm->toArray());

		$pages = new Pagination([
				'totalCount' => $itemsQuery->count(),
				'page' => $listForm->page,
				'defaultPageSize' => \Yii::$app->params['defaultPageSize'],
		]);
		$items = $itemsQuery->limit($pages->defaultPageSize)->offset($pages->page * $pages->defaultPageSize)->all();

		$listData = [
				'items' => $items,
				'pages' => $pages,
				'listForm' => $listForm,
		];

		if($request->isAjax) {
			echo json_encode([
					'view' => $this->renderAjax('_deliveries', $listData),
					'url' => Url::current([], true),
			]);
		}
		else {
			$listData['suppliers'] = $supplierOptions;
			$listData['warehouses'] = $warehouseOptions;
			return $this->render('list', $listData);
		}
	}

	public function actionDetails($id) {
        $delivery = WarehouseDeliveryQuery::getDetails($id);
        if(empty($delivery)) {
            throw new \yii\web\NotFoundHttpException(Yii::t('web', 'Element does not exist or you do not have permission.'));
        }

        $request = \Yii::$app->request;

        $listForm = new WarehouseDeliveryProductListForm();
        $listForm->load($request->get());

        $productsQuery = WarehouseDeliveryQuery::getDeliveryProductsQuery($id, $listForm->toArray());
		$pages = new Pagination([
				'totalCount' => $productsQuery->count(),
				'page' => $listForm->page,
				'defaultPageSize' => \Yii::$app->params['defaultPageSize'],
		]);
		$products = $productsQuery->limit($pages->defaultPageSize)->offset($pages->page * $pages->defaultPageSize)->all();
        ProductPhotoQuery::loadProductsPhotos($products);

		$listData = [
				'products' => $products,
				'pages' => $pages,
				'listForm' => $listForm,
		];

        if($request->isAjax) {
			echo json_encode([
					'view' => $this->renderAjax('_products', $listData),
					'url' => Url::current([], true),
			]);

        }
        else {
            return $this->render('details', [
            		'delivery' => $delivery, 
            		'listData' => $listData,
            		'availableDocuments' => DocumentTypeUtility::getAvailableDocuments(),
            ]);
        }
	}

	public function actionCreate($issueId = null) {
        $deliveryModel = new WarehouseDeliveryModel();

        $request = Yii::$app->request;
        if($request->isPost) {
            $deliveryModel->load($request->post());
            if($request->isAjax) {
                Yii::$app->response->format = Response::FORMAT_JSON;

                return ActiveForm::validateMultiple($deliveryModel->getProductsForms())
                    + ActiveForm::validate($deliveryModel->getWarehouseDeliveryForm());
            }
            else {
                try {
                    if($deliveryModel->save()) {
                        Yii::$app->getSession()->setFlash('success', Yii::t('web', 'Delivery has been added successfully.'));
                        return $this->redirect(['warehouse-delivery/details', 'id' => $deliveryModel->getDelivery()->id]);
                    }
                }
                catch(\Exception $e) {
                	throw $e;
                    Yii::$app->getSession()->setFlash('error', Yii::t('web', 'Delivery data cannot be saved. Check provided values.'));
                    Yii::error($e->getMessage());
                    Yii::error($e->getTraceAsString());
                }
            }
        }
        
        if(!empty($issueId)) {
        	$wpo = WarehouseProductOperation::findOne($issueId);
        	if(!empty($wpo) && in_array($wpo->productStatus->symbol, [Utility::PRODUCT_STATUS_ISSUING, Utility::PRODUCT_STATUS_ISSUING_RESERVATION])) {
        		$form = $deliveryModel->getWarehouseDeliveryForm();
        		$form->description = Yii::t('web', 'Return of products from Issue').' '.(!empty($wpo->number) 
						? mb_strtolower(Yii::t('web', 'Of number'), 'UTF-8').' '.$wpo->number  
        				: '#'.$wpo->id
				);
        		$form->isConfirmed = true;
        		$form->operationId = $wpo->id;
                $deliveryModel->restoreProductsFromIssueForms($issueId);
        	}
        }

        return $this->render('create', $deliveryModel->getAllForms());
	}

	public function actionEdit($id) {
        if(!WarehouseDelivery::find()->where(['id' => $id, 'is_active' => 1])->exists()) {
            throw new \yii\web\NotFoundHttpException(Yii::t('web', 'Element does not exist or you do not have permission.'));
        }
        $deliveryModel = new WarehouseDeliveryModel($id);

        $request = Yii::$app->request;
        if($request->isPost) {
            $deliveryModel->load($request->post());
            if($request->isAjax) {
                Yii::$app->response->format = Response::FORMAT_JSON;

                return ActiveForm::validateMultiple($deliveryModel->getProductsForms())
                    && ActiveForm::validate($deliveryModel->getWarehouseDeliveryForm());
            }
            else {
                try {
                    if($deliveryModel->save()) {
                        Yii::$app->getSession()->setFlash('success', Yii::t('web', 'Delivery has been updated successfully.'));
                        return $this->redirect(['warehouse-delivery/details', 'id' => $id]);
                    }
                }
                catch(\Exception $e) {
                    Yii::$app->getSession()->setFlash('error', Yii::t('web', 'Delivery data cannot be saved. Check provided values.'));
                    Yii::error($e->getMessage());
                    Yii::error($e->getTraceAsString());
                }
            }
        }

        return $this->render('edit', $deliveryModel->getAllForms());
	}

	public function actionDelete() {
		$id = Yii::$app->request->post('id');
		
		try {
            /*$deliveryModel = new WarehouseDeliveryModel($id, false);
            $deliveryModel->delete();(*/
			
			$delivery = WarehouseDelivery::findOne($id);
			if(empty($delivery) || empty($delivery->is_active) || !empty($delivery->is_confirmed)) {
				throw new \Exception('The deliery cannot be deleted: '.$id);
			}
			
			$delivery->is_active = 0;
			$delivery->save();			
			Yii::$app->getSession()->setFlash('success', Yii::t('web', 'The delivery has been deleted.'));
		}
		catch(\Exception $e) {
			Yii::info(print_r($e->getMessage(), true), 'info');
			Yii::$app->getSession()->setFlash('error', Yii::t('web', 'The delivery cannot be deleted. Please contact with the administrator.'));
		}

		if(strpos(Yii::$app->request->getReferrer(), Yii::$app->controller->id.'/details') !== false) {
			return $this->redirect(['list']);
		}

		return $this->redirect(Yii::$app->request->getReferrer());
	}

    public function actionGetProductForm() {
		$request = Yii::$app->request;

		if($request->isAjax) {
			echo $this->renderAjax('inc/product', [
					'productForm' => WarehouseDeliveryProductForm::loadProductForm($request->get('productId')),
					'prefix' => $request->get('prefix'),
					'formId' => $request->get('formId'),
			]);
		}
    }
    
    public function actionConfirm() {
    	$request = Yii::$app->request;
    	$deliveryId = $request->post('id');
		if(!WarehouseDelivery::find()->where(['id' => $deliveryId, 'is_active' => 1])->exists() || !$request->isPost) {
            throw new \yii\web\NotFoundHttpException(Yii::t('web', 'Element does not exist or you do not have permission.'));
        }
        $deliveryModel = new WarehouseDeliveryModel($deliveryId);
		$deliveryModel->getWarehouseDeliveryForm()->isConfirmed = 1;
        
     	try {
        	if($deliveryModel->save()) {
            	Yii::$app->getSession()->setFlash('success', Yii::t('web', 'Delivery has been updated successfully.'));
                return $this->redirect(['warehouse-delivery/details', 'id' => $deliveryId]);
      		}
      	}
     	catch(\Exception $e) {
        	Yii::$app->getSession()->setFlash('error', Yii::t('web', 'Delivery data cannot be saved. Check provided values.'));
         	Yii::error($e->getMessage());
            Yii::error($e->getTraceAsString());
      	}

        return $this->render(Yii::app()->request->urlReferrer);
    }
}
