<?php

namespace frontend\controllers;

use Yii;
use frontend\components\Controller;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use common\helpers\Utility;
use common\models\aq\TaskPriorityQuery;
use frontend\models\TaskForm;
use common\models\aq\TaskTypeQuery;
use common\models\aq\TaskTypeReferenceQuery;
use yii\helpers\ArrayHelper;
use yii\web\Response;
use yii\widgets\ActiveForm;
use common\models\aq\TaskQuery;
use frontend\models\TaskListForm;
use yii\data\Pagination;
use yii\helpers\Url;
use common\models\ar\Task;
use yii\db\Expression;
use frontend\helpers\UserHelper;
use common\alerts\TaskDeadlineAlertHandler;
use common\models\ar\TaskHistory;
use common\models\ar\TaskDataHistory;
use common\models\ar\TaskData;
use common\models\ar\AlertHistory;
use common\models\ar\Alert;
use common\models\ar\User;
use common\models\ar\Product;
use common\models\ar\TaskType;
use frontend\logic\TaskLogic;
use common\models\ar\Order;

class TaskController extends Controller {

	public function behaviors() {
		$allActions = ['create', 'short-list', 'get-details', 'complete', 'delete', 'edit'];

		return [
				'access' => [
						'class' => AccessControl::className(),
						'only' => $allActions,
						'rules' => [
								[
										'actions' => $allActions,
										'allow' => true,
										'roles' => [Utility::ROLE_SALESMAN, Utility::ROLE_STOREKEEPER],
								],
						],
				],
				'verbs' => [
						'class' => VerbFilter::className(),
						'actions' => [
								'complete' => ['post'],
								'delete' => ['post'],
						],
				],
		];
	}

	public function actionShortList() {
		$request = Yii::$app->request;
        $listForm = new TaskListForm();
        $listForm->load($request->get());

        $itemsQuery = TaskQuery::getActiveTasksQuery(Yii::$app->user->id, $listForm->toArray());
        $pages = new Pagination([
        		'totalCount' => $itemsQuery->count(),
        		'page' => $listForm->page,
        		'defaultPageSize' => 10,
        ]);
        $items = $itemsQuery->limit(10)->offset(0)->all();

        $taskLogic = new TaskLogic();
        $items = $taskLogic->processTasks($items);
        
        $listData = [
        		'items' => $items,
        		'pages' => $pages,
        		'listForm' => $listForm,
        ];

        if($request->isAjax && !Yii::$app->request->get('dashboard')) {
        	return json_encode([
        			'view' => $this->renderAjax('_tasks', $listData),
        			'url' => Url::current([], true),
        	]);
        }
		$listData['taskTypes'] = ArrayHelper::map(TaskTypeQuery::getAllTypes(), 'id', 'name');
		$listData['taskTypes'] = [ 0 => Yii::t('web', 'All')] + $listData['taskTypes'];
		if(Yii::$app->request->get('dashboard')) {
			return $this->renderAjax('shortList', $listData);
		}
        else {
        	return $this->render('shortList', $listData);
        }
	}

	public function actionCreate($taskTypeId = null, $userNotifiedId = null, $productId = null, $orderId = null) {
		$model = new TaskForm();
		$model->setAddScenario();

		if(Yii::$app->request->isAjax && $model->load(Yii::$app->request->post())) {
			Yii::$app->response->format = Response::FORMAT_JSON;
			return ActiveForm::validate($model);
		}
		
		if($model->load(Yii::$app->request->post()) && $model->validate()) {
			try {
				$model->saveData();
				
				Yii::$app->getSession()->setFlash('success', Yii::t('web', 'Task has been added successfully.'));
				
				$taskType = TaskType::findOne($model->taskTypeId);
				if($taskType->symbol == Utility::TASK_TYPE_ASSOCIATED_WITH_ORDER && !empty($model->fields) && !empty(yii::$app->request->get('orderId'))) {
					return $this->redirect(['order/details', 'id' => $model->fields[2]]);
				}
				
				return $this->redirect(['dashboard/index']);
			}
			catch(\Exception $e) {
				Yii::$app->getSession()->setFlash('error', Yii::t('web', 'Task data cannot be saved. Check provided values.'));
				Yii::error($e->getMessage());
				Yii::error($e->getTraceAsString());
			}
		}
		
		if(!empty($taskTypeId)) {
			$model->taskTypeId = $taskTypeId;
			
			$taskType = TaskType::findOne($taskTypeId);
			if($taskType->symbol == Utility::TASK_TYPE_ORDER_PRODUCT) {
				$model->title = Yii::t('web', 'Product to order');
				$model->message = Yii::t('web', 'Please order the product given below.');
			}
		}
		
		$userNotifiedData = null;
		if(!empty($userNotifiedId)) {
			$model->userNotifiedId = $userNotifiedId;
			$user = User::findOne($userNotifiedId);
			$userNotifiedData = [$userNotifiedId => UserHelper::getPrettyUserName($user->email, $user->first_name, $user->last_name)];
		}

		$taskTypes = TaskTypeQuery::getAllTypes();
		$fieldArrays = $this->getAdditionalFields($model, $taskTypes, true, $productId, $orderId, $taskTypeId);
		
		return $this->render('form', [
				'model' => $model,
				'priorityData' => $this->getPriorityData(),
				'taskTypes' => $taskTypes,
				'fields' => $fieldArrays['fields'],
				'userNotifiedData' => $userNotifiedData,
				'fieldsInitialValues' => $fieldArrays['initials'],
		]);
	}
	
	public function actionEdit($id) {
		$task = Task::findOne($id);
		$userId = Yii::$app->user->id;
		if(empty($task) || !empty($task->is_complete) ||  (!empty($task->userNotified) && $task->user_notified_id != $userId) || 
				(empty($task->userNotified) && $task->user_id != $userId)) {
			throw new \yii\web\NotFoundHttpException(Yii::t('web', 'Element does not exist or you do not have permission.'));
		}
		
		$model = new TaskForm($task);
		$model->setEditScenario();
	
		if(Yii::$app->request->isAjax && $model->load(Yii::$app->request->post())) {
			Yii::$app->response->format = Response::FORMAT_JSON;
			return ActiveForm::validate($model);
		}
	
		if($model->load(Yii::$app->request->post()) && $model->validate()) {
			try {
				$model->saveData();
				Yii::$app->getSession()->setFlash('success', Yii::t('web', 'Task has been updated successfully.'));
	
				return $this->redirect(['dashboard/index']);
			}
			catch(\Exception $e) {
				Yii::$app->getSession()->setFlash('error', Yii::t('web', 'Task data cannot be saved. Check provided values.'));
				Yii::error($e->getMessage());
				Yii::error($e->getTraceAsString());
			}
		}
		
		$taskTypes = TaskTypeQuery::getAllTypes();
		$fieldArrays = $this->getAdditionalFields($model, $taskTypes, false);
		
		return $this->render('form', [
				'model' => $model,
				'priorityData' => $this->getPriorityData(),
				'taskTypes' => $taskTypes,
				'fields' => $fieldArrays['fields'],
				'userNotifiedData' => $model->userNotifiedData,
				'fieldsInitialValues' => $model->fieldsData,
		]);
	}
	
	private function getAdditionalFields($model, $taskTypes, $saveFields = true, $productId = null, $orderId = null, $taskTypeId = null) {
		$fields = TaskTypeReferenceQuery::getAdditionalFieldsByTaskTypeIds(ArrayHelper::getColumn($taskTypes, 'id'));
		$fieldsInitialValues = [];
		
		if(!empty($fields)) {
			if((!empty($productId) || !empty($orderId)) && !empty($taskTypeId)) {
				foreach($fields as $field) {
					if($field['taskTypeId'] == $taskTypeId) {
						if($field['nameTable'] == 'product') {
							$product = Product::findOne($productId);
							$productTranslation = $product->getTranslation();
							if(!empty($productTranslation)) {
								$productName = $productTranslation->name;
							}
							else {
								$productName = $product->symbol;
							}
			
							$fieldsInitialValues[$field['id']] = ['id' => $productId, 'name' => $productName];
							break;
						}
						elseif($field['nameTable'] == 'order') {
							$order = Order::findOne($orderId);
							$fieldsInitialValues[$field['id']] = ['id' => $orderId, 'name' => $order->title];
							break;
						}
					}
				}
			}
				
			if($saveFields) {
				$model->setAdditionalFields($fields, $fieldsInitialValues);
			}
		}
		
		return [ 'fields' => $fields, 'initials' => $fieldsInitialValues ];
	}

	public function actionGetDetails($id) {
		if(!Yii::$app->request->isAjax) {
			return '';
		}

		$taskId = Yii::$app->request->get('id');
		$data = TaskQuery::getDetails($taskId);
		if(empty($data)) {
			return Yii::t('web', 'No detailed data about task.');
		}
		
		return $this->renderAjax('inc/details', [
				'data' => $data,
		]);
	}

	public function actionComplete() {
		$id = Yii::$app->request->post('id');

		try {
			if(!TaskQuery::isTaskVisibleByUserId($id, Yii::$app->user->id)) {
				throw new Exception('Task '.$id.' is unavailable for user '.Yii::$app->user->id.' to complete.');
			}

			$task = Task::findOne($id);
			$task->date_completion = new Expression('NOW()');
			$task->user_completing_id = Yii::$app->user->id;
			$task->is_complete = 1;
			$task->save();
			
			TaskDeadlineAlertHandler::check(['taskId' => $task->id]);
			
			if(strpos(Yii::$app->request->getReferrer(), 'order/details') !== false) {
	        	return $this->redirect(Yii::$app->request->getReferrer());
	        }
		}
		catch(\Exception $e) {
			Yii::error(print_r($e->getMessage(), true));
			Yii::$app->getSession()->setFlash('error', Yii::t('web', 'There is no task with such identifier.'));
		}
	}

	public function actionDelete() {
		$id = Yii::$app->request->post('id');

		$transaction = Yii::$app->db->beginTransaction();
		try {
			if(!TaskQuery::isTaskVisibleByUserId($id, Yii::$app->user->id)) {
				throw new \Exception('Task '.$id.' is unavailable for user '.Yii::$app->user->id.' to delete.');
			}

			$task = Task::findOne($id);
			
			$taskHistory = new TaskHistory();
			$taskHistory->task_type_id = $task->task_type_id;
			$taskHistory->task_priority_id = $task->task_priority_id;
			$taskHistory->user_id = $task->user_id;
			$taskHistory->title = $task->title;
			$taskHistory->message = $task->message;
			$taskHistory->user_notified_id = $task->user_notified_id;
			$taskHistory->date_reminder = $task->date_reminder;
			$taskHistory->date_deadline = $task->date_deadline;
			$taskHistory->is_complete = $task->is_complete;
			$taskHistory->user_completing_id = $task->user_completing_id;
			$taskHistory->date_completion = $task->date_completion;
			$taskHistory->date_creation = $task->date_creation;
			$taskHistory->save();
			
			foreach($task->taskDatas as $taskData) {
				$taskDataHistory = new TaskDataHistory();
				$taskDataHistory->task_history_id = $taskHistory->id;
				$taskDataHistory->task_type_reference_id = $taskData->task_type_reference_id;
				$taskDataHistory->referenced_object_id = $taskData->referenced_object_id;
				$taskDataHistory->save();
			}
			
			foreach($task->alerts as $alert) {
				$alertHistory = new AlertHistory();
				$alertHistory->alert_type_id = $alert->alert_type_id;
				$alertHistory->user_id = $alert->user_id;
				$alertHistory->message = $alert->message;
				$alertHistory->date_deactivation = new Expression('NOW()');
				$alertHistory->date_creation = $alert->date_creation;
				$alertHistory->order_id = $alert->order_id;
				$alertHistory->product_id = $alert->product_id;
				$alertHistory->company_id = $alert->company_id;
				$alertHistory->task_history_id = $taskHistory->id;
				$alertHistory->warehouse_id = $alert->warehouse_id;
				$alertHistory->save();
			}
			
			AlertHistory::updateAll(['task_history_id' => $taskHistory->id], ['task_id' => $task->id]);
			AlertHistory::updateAll(['task_id' => null], ['task_id' => $task->id]);
			Alert::deleteAll(['task_id' => $task->id]);
			TaskData::deleteAll(['task_id' => $task->id]);
			$task->delete();
			
			$transaction->commit();
			
			if(strpos(Yii::$app->request->getReferrer(), 'order/details') !== false) {
				return $this->redirect(Yii::$app->request->getReferrer());
			}
		}
		catch(\Exception $e) {
			$transaction->rollBack();
			Yii::error(print_r($e->getMessage(), true), 'info');
			Yii::$app->getSession()->setFlash('error', Yii::t('web', 'There is no task with such identifier.'));
		}
	}

	private function getPriorityData() {
		$priorities = TaskPriorityQuery::getAllOrdered();

		$data = [
				'min' => 1,
				'max' => count($priorities),
				'initialValue' => ceil(count($priorities) / 2),
				'step' => 1,
				'labels' => '',
		];

		for($i = 0; $i < count($priorities); ++$i) {
			$data['labels'] .= 'if(val <= '.($i + 1).') return \''.$priorities[$i]->symbol.'\';';
		}

		return $data;
	}
}
