<?php

namespace frontend\controllers;

use Yii;
use frontend\components\Controller;
use yii\filters\AccessControl;
use common\helpers\Utility;
use yii\web\Response;
use yii\widgets\ActiveForm;
use frontend\models\ProductionTaskQaAssignmentForm;

class QaController extends Controller {

	public function behaviors() {
		$allActions = ['create'];

		return [
			'access' => [
				'class' => AccessControl::className(),
				'only' => $allActions,
				'rules' => [
					[
						'actions' => $allActions,
						'allow' => true,
						'roles' => [Utility::ROLE_QA_SPECIALIST],
					],
				],
			],
		];
	}

	public function actionCreate($productionTaskId) {
		$model = new ProductionTaskQaAssignmentForm($productionTaskId);
		
		if(Yii::$app->request->isAjax && $model->load(Yii::$app->request->post())) {
			Yii::$app->response->format = Response::FORMAT_JSON;
			return ActiveForm::validate($model);
		}
		
		if($model->load(Yii::$app->request->post()) && $model->validate()) {
			try {
				$model->saveData();
				
				Yii::$app->getSession()->setFlash('success', Yii::t('web', 'Quality note has been saved.'));
				
    			if(strpos(Yii::$app->request->getReferrer(), Yii::$app->controller->id) !== false) {
                    return $this->redirect(['production/index']);
                }
                else {
                    return $this->redirect(Yii::$app->request->getReferrer());
                }
			}
			catch(\Exception $e) {
				Yii::$app->getSession()->setFlash('error', Yii::t('web', 'Quality note cannot be saved. Check provided values.'));
				Yii::error($e->getMessage());
				Yii::error($e->getTraceAsString());
			}
		}
		
		return $this->render('form', [
				'model' => $model,
		]);
	}
	
}
