<?php

namespace frontend\controllers;

use Yii;
use frontend\components\Controller;
use yii\filters\AccessControl;
use yii\web\Response;
use yii\widgets\ActiveForm;
use yii\filters\VerbFilter;
use common\documents\DocumentsGenerator;
use common\documents\DocumentType;
use common\documents\html\Renderer;
use common\models\aq\OrderQuery;
use common\documents\forms\InvoiceProductForm;
use common\documents\sections\InvoiceProducts;
use common\documents\sections\FireEqAcceptanceProtocol;
use common\documents\sections\MalfunctionProtocol;
use common\documents\sections\SectionsMapper;
use common\documents\forms\GoodsReceivedProductForm;
use common\models\ar\Order;
use common\models\aq\WarehouseDeliveryProductQuery;
use common\documents\sections\GoodsReceivedTitle;
use common\models\aq\WarehouseDeliveryQuery;
use common\documents\sections\GoodsReceivedProducts;
use common\documents\sections\GoodsIssuedTitle;
use common\documents\forms\GoodsIssuedProductForm;
use common\models\aq\WarehouseProductStatusHistoryQuery;
use common\helpers\NumberTemplateHelper;
use common\helpers\Utility;
use common\models\ar\ApplicationCache;
use frontend\helpers\DocumentHelper;
use common\documents\forms\FireEqAcceptanceProductForm;
use common\documents\sections\DeliveryDetailsTitle;
use common\documents\sections\InvoiceTitle;
use MongoDB\BSON\ObjectID;
use common\documents\models\CorrectiveInvoiceModel;
use common\documents\models\AdvanceInvoiceModel;
use common\models\aq\DocumentQuery;
use common\models\aq\InvoiceHistoryQuery;
use common\documents\DocumentTypeUtility;
use common\models\aq\OrderTypeSetQuery;
use common\documents\models\DuplicateInvoiceModel;
use common\models\ar\OrderStatus;
use frontend\models\OrderTableForm;
use yii\helpers\Json;
use common\documents\models\DuplicateCorrectiveInvoiceModel;
use common\alerts\OrderDeadlineAlertHandler;

class DocumentController extends Controller {

    public function behaviors() {
        $allActions = ['test', 'generate-goods-received', 'generate-goods-received', 'generate-goods-issued', 
        		'invoice', 'invoice-all', 'calculate-invoice-amounts', 'calculate-invoice-amount-verbally', 
        		'get-invoice-product-row', 'calculate-goods-received-amounts', 'get-goods-received-product-row',
        		'get-goods-issued-product-row', 'delivery-details', 'generate-firefighting-eq-acceptance-protocol',
        		'get-fire-eq-acceptance-product-row', 'generate-malfunction-protocol', 'proforma-invoice',
        		'corrective-invoice', 'advance-invoice', 'duplicate-invoice', 'delete', 'duplicate-corrective-invoice',
        ];

        return [
	            'access' => [
						'class' => AccessControl::className(),
		                'only' => $allActions,
		                'rules' => [
		                		[
		                				'actions' => ['delete'],
		                				'allow' => true,
		                				'roles' => [Utility::ROLE_ADMIN],
		                		],
		                		[
		                				'actions' => ['corrective-invoice', 'advance-invoice', 'duplicate-invoice', 'duplicate-corrective-invoice'],
		                				'allow' => Yii::$app->params['isDocumentStorageAvailable'],
		                				'roles' => ['@'],
		                		],
								[
		                        		'actions' => array_diff($allActions, [
		                        				'delete', 'corrective-invoice', 'advance-invoice', 'duplicate-invoice', 'duplicate-corrective-invoice',
		                        		]),
		                        		'allow' => true,
		                        		'roles' => ['@'],
		                    	],
		                ],
	            ],
        		'verbs' => [
        				'class' => VerbFilter::className(),
        				'actions' => [
        						'delete' => ['post'],
        				],
        		],
        ];
    }

    public function actionInvoice($edit = false, $type = null, $referencedInvoiceId = null, $referencedInvoiceHistoryId = null) {
    	if(!in_array(DocumentType::INVOICE, DocumentTypeUtility::getAvailableDocuments())) {
	    	throw new \yii\web\NotFoundHttpException(Yii::t('web', 'Element does not exist or you do not have permission.'));
		}
    	
    	$request = Yii::$app->request;
        $documentId = $request->get('documentId');
        $historyId = $request->get('historyId');
        $render = $request->isPost;
        $save = true;       

        $transaction = null;
        $needToCreateTransaction = null;
        
        if(!empty($documentId) && !empty($historyId) && !$edit) {
            //TODO set link in all view file
            if(Yii::$app->params['generateThreeCopyOfInvoiceOnOnePdf'] && empty($type)) {
                return $this->redirect(['document/invoice-all', 'documentId' => $documentId, 'historyId' => $historyId]);
            } else {
                $document = DocumentsGenerator::reconstruct($documentId, $historyId);
                $render = true;
                $save = false;
            }
        } else {
            if($edit) {
                $document = DocumentsGenerator::reconstruct($documentId, $historyId, $edit);
            } 
            else {
                $document = DocumentsGenerator::generate(!empty($type) ? $type : DocumentType::INVOICE);
            }
            $document->makeEditable();

            $request = Yii::$app->request; 

            //$dataInput = $request->isPost && !$edit ? $request->post() : [(new InvoiceProductForm())->formName() => OrderQuery::getOrderProducts($request->get('orderId'), ['withArtificial' => 1])];
            
            $order = Order::findOne($request->get('orderId'));
            $dataInput = $request->isPost || $edit ? $request->post() : [(new InvoiceProductForm())->formName() => $this->getInvoiceProducts($order)];
            $dataInput[(new InvoiceTitle())->formName()]['orderId'] = $request->get('orderId');
            $entityId = !empty($order) ? $order->entity_id : null;
            
            if($request->isPost) {
                if(!$request->isAjax && $request->get('edit') == 0/* && !in_array($type, [DocumentType::CORRECTIVE_INVOICE])*/) {
                	$templateSymbol = Utility::NUMBER_TEMPLATE_INVOICE;
                	if($type == DocumentType::PROFORMA_INVOICE) {
                		$templateSymbol = Utility::NUMBER_TEMPLATE_PROFORMA_INVOICE;
                	}
                	elseif($type == DocumentType::ADVANCE_INVOICE) {
                		$templateSymbol = Utility::NUMBER_TEMPLATE_ADVANCE_INVOICE;
                	}     
                	elseif($type == DocumentType::CORRECTIVE_INVOICE) {
                		$templateSymbol = Utility::NUMBER_TEMPLATE_CORRECTIVE_INVOICE;
                	}
//TODO::ADDED 
                	$transaction = Yii::$app->db->getTransaction();
                	$needToCreateTransaction = empty($transaction);
                	if($needToCreateTransaction) {
                		$transaction = Yii::$app->db->beginTransaction('SERIALIZABLE');
                	}
                	
                    $currentNumber = NumberTemplateHelper::getAndSaveNextNumber('', $templateSymbol, $entityId);
                    $dataInput[(new InvoiceTitle())->formName()]['number'] = $currentNumber['number'];
                }
                
                $document->loadInput($dataInput);
                if($request->isAjax) {
                    Yii::$app->response->format = Response::FORMAT_JSON;
                    return $document->performActiveValidation();
                }
            }
            elseif(!$edit || in_array($type, [DocumentType::CORRECTIVE_INVOICE, DocumentType::ADVANCE_INVOICE])) {
				if(isset(Yii::$app->params['isDocumentStorageAvailable']) && !empty(Yii::$app->params['isDocumentStorageAvailable'])) {
	            	if(!isset($referencedInvoiceId) && isset($documentId)) {
	            		$referencedInvoiceId = $documentId;
	            	}
	            	if(!isset($referencedInvoiceHistoryId) && isset($historyId)) {
	            		$referencedInvoiceHistoryId = $historyId;
	            	}
	            	
	            	$query = ['_id' => new ObjectID($referencedInvoiceId), 'history._id' => new ObjectID($referencedInvoiceHistoryId)];
	            	$fieldsToFetch = [
	            			'type' => 1,
	            			'history.$' => 1,
	            			'sections' => 1,
	            			'header' => 1,
	            			'footer' => 1,
	            	];
	            	 
	            	$documentData = Yii::$app->mongodb->getCollection('documents')->findOne($query, $fieldsToFetch);
	            	
	            	if(!empty($documentData)) {
	            		if(!isset($documentData['history'][0]['sections'][1]['entityId'])) {
	            			$documentData['history'][0]['sections'][1]['entityId'] = $entityId;
	            		}
	            		
	            		if($type == DocumentType::CORRECTIVE_INVOICE) {   
	            			if(!in_array($documentData['type'], [DocumentType::INVOICE, DocumentType::CORRECTIVE_INVOICE])) {
	            				throw new \yii\web\NotFoundHttpException(Yii::t('web', 'Element does not exist or you do not have permission.'));
	            			}
	            				
	            			$dataModel = new CorrectiveInvoiceModel($dataInput, $documentData);
	            		}
	            		elseif($type == DocumentType::ADVANCE_INVOICE) {
	            			if(!in_array($documentData['type'], [DocumentType::PROFORMA_INVOICE, DocumentType::ADVANCE_INVOICE])) {
	            				throw new \yii\web\NotFoundHttpException(Yii::t('web', 'Element does not exist or you do not have permission.'));
	            			}
	            			
	            			$dataModel = new AdvanceInvoiceModel($dataInput, $documentData);
	            		}
	            		
	            		$dataInput = $dataModel->prepareInputData($edit);
	            	}
				}
            	
            	$document->loadInput($dataInput);
            }
        }

        if($render) {
        	if($save) {                  
                try {
                    $sectionSequence = $document->getSectionsSequence();                    
                    $order = Order::findOne($request->get('orderId'));
                    
                    if(Yii::$app->params['isContractTypeVisible']) {
                    	$contractTypeId = $sectionSequence[1]->contractTypeId;
                    	$order->contract_type_id = $contractTypeId;
                    }
                    
                    if(empty($type)) {
                    	$invoicedStatus = OrderStatus::find()->where(['symbol' => Utility::ORDER_STATUS_INVOICED])->one();
                    	if(!empty($invoicedStatus)) {
                    		$orderHistory = $order->orderHistoryLast;
                    		$orderHistory->isNewRecord = true;
                    		$orderHistory->id = null;
                    		$orderHistory->user_id = Yii::$app->user->id;
                    		$orderHistory->order_status_id = $invoicedStatus->id;
                    		$orderHistory->message = Yii::t('web', 'The invoice {number} has been created.', ['number' => $sectionSequence[1]->number]);
                    		$orderHistory->is_client_entry = 0;
                    		$orderHistory->save();
                    		
                    		$order->order_history_last_id = $orderHistory->id;
                    		OrderDeadlineAlertHandler::check(['orderId' => $order->id]);
                    	}
                    }
                    
                    $order->save();
                    
                    if(Yii::$app->params['isOrderTypeVisible']) {
                    	$orderTypeIds = $sectionSequence[1]->orderTypeIds;
                    	OrderTypeSetQuery::replaceOrderTypesOfOrder($order->id, $orderTypeIds);
                    }
                    
                    if($needToCreateTransaction) {
                    	$transaction->commit();
                    }        
                } catch (\Exception $e) {   
                	$transaction = Yii::$app->db->getTransaction();
                	if(!empty($transaction)) {
                		$transaction->rollback();
                	} 
                	Yii::error($e->getMessage());
                	Yii::error($e->getTraceAsString());
                }
                
                $sectionTitle = SectionsMapper::INVOICE_TITLE;
                if($type == DocumentType::PROFORMA_INVOICE) {
                	$sectionTitle = SectionsMapper::PROFORMA_INVOICE_TITLE;
                }
                elseif($type == DocumentType::CORRECTIVE_INVOICE) {
                	$sectionTitle = SectionsMapper::CORRECTIVE_INVOICE_TITLE;
                }
                elseif($type == DocumentType::ADVANCE_INVOICE) {
                	$sectionTitle = SectionsMapper::ADVANCE_INVOICE_TITLE;
                }
                
                $documentHistory = DocumentsGenerator::save($document, !$edit);
                
                if(empty($type) && !$edit) {	    
                	$appCache = ApplicationCache::find()->where(['entity_id' => $entityId])->one();  
	                $appCache->date_last_invoice = $document->getSectionsSequence()[1]->dateOfIssue;
	                $appCache->save();
	                if(Yii::$app->params['generateThreeCopyOfInvoiceOnOnePdf'] && Yii::$app->params['isDocumentStorageAvailable']) {
	                    return $this->redirect(['document/invoice-all', 'documentId' => $documentHistory['documentId']->__toString(), 'historyId' => $documentHistory['historyId']->__toString()]);
	                }
                }
            }
            
            $title = Yii::t('documents', 'Invoice');
            $dt = \common\models\ar\DocumentType::find()->where(['symbol' => (!empty($type) ? $type : DocumentType::INVOICE)])->one();
            if(!empty($dt) && !empty($dt->getTranslation())) {
            	$title = $dt->getTranslation()->name;
            }
            
            $number = $document->getSectionsSequence()[1]['number'];
            $title = $title.(!empty($number) ? ' '.$number : '');
            
            (new \common\documents\pdf\Renderer([
                'options' => [
                     'title' => $title,
                ]
            ], (!empty($number) ? $number : '')
            ))->render($document);
        } else {
            $documentView = (new \common\documents\html\Renderer())->render($document);
            return $this->render('test', ['documentView' => $documentView]);
        }
    }
    
    public function actionProformaInvoice($edit = false) {
    	if(!in_array(DocumentType::PROFORMA_INVOICE, DocumentTypeUtility::getAvailableDocuments())) {
    		throw new \yii\web\NotFoundHttpException(Yii::t('web', 'Element does not exist or you do not have permission.'));
    	}
    	
    	return $this->actionInvoice($edit, DocumentType::PROFORMA_INVOICE);
    }
    
    public function actionCorrectiveInvoice($invoiceId = null, $historyId = null, $edit = false) {
    	if(!in_array(DocumentType::CORRECTIVE_INVOICE, DocumentTypeUtility::getAvailableDocuments())) {
    		throw new \yii\web\NotFoundHttpException(Yii::t('web', 'Element does not exist or you do not have permission.'));
    	}
    	
    	return $this->actionInvoice($edit, DocumentType::CORRECTIVE_INVOICE, $invoiceId, $historyId);
    }
    
    public function actionAdvanceInvoice($invoiceId = null, $historyId = null, $edit = false) {
    	if(!in_array(DocumentType::ADVANCE_INVOICE, DocumentTypeUtility::getAvailableDocuments())) {
    		throw new \yii\web\NotFoundHttpException(Yii::t('web', 'Element does not exist or you do not have permission.'));
    	}
    	
    	return $this->actionInvoice($edit, DocumentType::ADVANCE_INVOICE, $invoiceId, $historyId);
    }
    
	public function actionDuplicateInvoice($invoiceId = null, $historyId = null, $type = null, $edit = false) {
		if(!in_array(DocumentType::DUPLICATE_INVOICE, DocumentTypeUtility::getAvailableDocuments())) {
			throw new \yii\web\NotFoundHttpException(Yii::t('web', 'Element does not exist or you do not have permission.'));
		}
		
		$request = Yii::$app->request;
		$documentId = $request->get('documentId');
		$historyId = $request->get('historyId');
		$render = true;
		$save = true;       
	
		if(!empty($documentId) && !empty($historyId) && !$edit) {
			$document = DocumentsGenerator::reconstruct($documentId, $historyId);
			$render = true;
			$save = false;
		} 
		else {
			if($edit) {
				$document = DocumentsGenerator::reconstruct($documentId, $historyId, $edit);
			} 
			else {
				$document = DocumentsGenerator::generate(!empty($type) ? $type : DocumentType::DUPLICATE_INVOICE);
			}
	
			$dataInput = !$edit ? [(new InvoiceProductForm())->formName() => OrderQuery::getOrderProducts($request->get('orderId'), ['withArtificial' => 1])] : [];
			
			$dataInput[(new InvoiceTitle())->formName()]['orderId'] = $request->get('orderId');
			$order = Order::findOne($request->get('orderId'));
			$entityId = !empty($order) ? $order->entity_id : null;
			
			if(!$edit) {
				if(isset(Yii::$app->params['isDocumentStorageAvailable']) && !empty(Yii::$app->params['isDocumentStorageAvailable'])) {
					if(!isset($invoiceId) && isset($documentId)) {
						$invoiceId = $documentId;
					}
					
					$query = ['_id' => new ObjectID($invoiceId), 'history._id' => new ObjectID($historyId)];
					$fieldsToFetch = [
							'type' => 1,
							'history.$' => 1,
							'sections' => 1,
							'header' => 1,
							'footer' => 1,
					];
					 
					$documentData = Yii::$app->mongodb->getCollection('documents')->findOne($query, $fieldsToFetch);
					
					if(!empty($documentData)) {
						if(!in_array($documentData['type'], array_diff(DocumentType::getInvoiceTypes(), [DocumentType::DUPLICATE_INVOICE, DocumentType::DUPLICATE_CORRECTIVE_INVOICE]))) {
							throw new \yii\web\NotFoundHttpException(Yii::t('web', 'Element does not exist or you do not have permission.'));
						}
						
						if(!isset($documentData['history'][0]['sections'][1]['entityId'])) {
							$documentData['history'][0]['sections'][1]['entityId'] = $entityId;
						}
						
						if($type == DocumentType::DUPLICATE_CORRECTIVE_INVOICE) {
							$dataModel = new DuplicateCorrectiveInvoiceModel($dataInput, $documentData);
						}
						else {
							$dataModel = new DuplicateInvoiceModel($dataInput, $documentData);
						}
						
						$dataInput = $dataModel->prepareInputData($edit);
					}
				}
				
				$document->loadInput($dataInput);
			}
		}	
	
		if($save) {       		
			$documentHistory = DocumentsGenerator::save($document);
		}
		
		if($type == DocumentType::DUPLICATE_CORRECTIVE_INVOICE) {
			$title = Yii::t('documents', 'Duplicate of corrective invoice');
		}
		else {
			$title = Yii::t('documents', 'Duplicate of invoice');
		}
		
		$dt = \common\models\ar\DocumentType::find()->where(['symbol' => (!empty($type) ? $type : DocumentType::DUPLICATE_INVOICE)])->one();
		if(!empty($dt) && !empty($dt->getTranslation())) {
			$title = $dt->getTranslation()->name;
		}
		
		$number = $document->getSectionsSequence()[1]['number'];
		$title = $title.(!empty($number) ? ' '.$number : '');
		
		(new \common\documents\pdf\Renderer([
			'options' => [
				 'title' => $title,
			]
		], (!empty($number) ? $number : '')
		))->render($document);
    }
    
    public function actionDuplicateCorrectiveInvoice($invoiceId = null, $historyId = null, $edit = false) {
    	if(!in_array(DocumentType::DUPLICATE_CORRECTIVE_INVOICE, DocumentTypeUtility::getAvailableDocuments())) {
    		throw new \yii\web\NotFoundHttpException(Yii::t('web', 'Element does not exist or you do not have permission.'));
    	}
    	
    	return $this->actionDuplicateInvoice($invoiceId, $historyId, DocumentType::DUPLICATE_CORRECTIVE_INVOICE);
    }

    public function actionCalculateInvoiceAmounts() {
    	$invoiceProducts = new InvoiceProducts();
        $invoiceProducts->setEditable(true);

        $invoiceProductFormName = (new InvoiceProductForm())->formName();
        $input = Yii::$app->request->post();
        $input[$invoiceProductFormName] = empty($input[$invoiceProductFormName]) ? [] : array_values($input[$invoiceProductFormName]);
        $invoiceProducts->loadInput($input);

        return $this->renderFile('@common/documents/html/views/invoice-products/table-body.php', ['section' => $invoiceProducts, 'form' => new \yii\widgets\ActiveForm()]);
    }    
    
    public function actionCalculateInvoiceAmountVerbally() {
    	$invoiceProducts = new InvoiceProducts();
    	$invoiceProducts->setEditable(true);
    	
    	$invoiceProductFormName = (new InvoiceProductForm())->formName();
    	$input = Yii::$app->request->post();
    	$input[$invoiceProductFormName] = empty($input[$invoiceProductFormName]) ? [] : array_values($input[$invoiceProductFormName]);
    	$invoiceProducts->loadInput($input);
    	$invoiceProducts->calculateSummary();
    	
    	return $invoiceProducts->amountVerbally;
    }

    public function actionGetInvoiceProductRow() {
        $product = new InvoiceProductForm();
		$request = Yii::$app->request;
		$initData = $request->get('initData');
        if(!empty($initData)) {
        	$keys = array_keys($initData);
        	$product->name = $initData[$keys[0]];
        	$product->count = $initData[$keys[1]];
        	$product->unit = $initData[$keys[2]];
        	$product->category = $initData[$keys[3]];
        	$product->price = $initData[$keys[4]];
        	$product->vatRate = $initData[$keys[5]];
        }
        
		return $this->renderFile('@common/documents/html/views/invoice-products/product-row.php', [
                    'product' => $product,
                    'form' => new \yii\widgets\ActiveForm(),
                    'edit' => $request->get('edit'),
                    'idx' => $request->get('idx'),
                    'currencySymbol' => $request->get('currencySymbol'),
        ]);
    }

    public function actionInvoiceAll() {
    	if(!in_array(DocumentType::INVOICE, DocumentTypeUtility::getAvailableDocuments())) {
    		throw new \yii\web\NotFoundHttpException(Yii::t('web', 'Element does not exist or you do not have permission.'));
    	}
    	
        $pdf = new \kartik\mpdf\Pdf();
        $number = null;
        $original = $this->saveInvoice(true, $number);
        $copy = $this->saveInvoice();
        
        $filenames = [
            $original,
            $copy,
            $copy
        ];        

        if($filenames) {
            $filesTotal = sizeof($filenames);
            $fileNumber = 1;

            $pdf->api->SetImportUse();

            foreach($filenames as $fileName) {
                $pagesInFile = $pdf->api->SetSourceFile($fileName);
                for($i = 1; $i <= $pagesInFile; $i++) {
                    $tplId = $pdf->api->ImportPage($i);
                    $pdf->api->UseTemplate($tplId);
                    if(($fileNumber < $filesTotal) || ($i != $pagesInFile)) {
                        $pdf->api->writeHTML('<pagebreak />');
                    }
                }
                $fileNumber++;
            }
        }
        
        $title = Yii::t('documents', 'Invoice').(!empty($number) ? ' '.$number : '');        
        $pdf->api->setTitle($title);
        $pdf->api->Output((!empty($number) ? $number : '').'.pdf', 'I');
        //TODO safe remove
        unlink($original);
        unlink($copy);
    }
    
    //TODO refactor, very slow
    private function saveInvoice($isOriginal = false, &$number = 1) {
        $request = Yii::$app->request;
        $name = \common\helpers\UploadHelper::getUploadPath().'/invoice.'.uniqid().'.pdf';
        $documentId = $request->get('documentId');
        $historyId = $request->get('historyId');
        $save = true;
        if(!empty($documentId) && !empty($historyId)) {
            $document = DocumentsGenerator::reconstruct($documentId, $historyId);            
            $render = true;
            $save = false;
            $document->getSectionsSequence()[1]->isCopy = !$isOriginal;
            
            if(empty($number)) {
            	$number = $document->getSectionsSequence()[1]['number'];
            }
            
            (new \common\documents\pdf\Renderer([
                'options' => [
                    'title' => Yii::t('documents', 'Invoice'),
                    
                ],
                'filename' => $name,
                'destination' => \kartik\mpdf\Pdf::DEST_FILE,
            ]))->render($document);
            return str_replace('\\','/',$name);
        } else {
            throwException(\NewException());
        }
    }
    
    private function getInvoiceProducts($order) {
    	if(empty($order)) {
    		return [];
    	}
    	
    	$products = OrderQuery::getOrderProducts($order->id, ['withArtificial' => 1]);
    	$customData = [];
    	if($order->orderHistoryLast->orderCustomData) {
    		$customData = OrderTableForm::getCostsTable(Json::decode($order->orderHistoryLast->orderCustomData->content));
    	}
    	
    	return array_merge($products, $customData);
    }

    public function actionGenerateGoodsReceived($deliveryId = 0) {
    	if(!in_array(DocumentType::GOODS_RECEIVED, DocumentTypeUtility::getAvailableDocuments())) {
    		throw new \yii\web\NotFoundHttpException(Yii::t('web', 'Element does not exist or you do not have permission.'));
    	}
    	
        $request = Yii::$app->request;
        $documentId = $request->get('documentId');
        $historyId = $request->get('historyId');
        $render = $request->isPost;
        $save = true;

        if(!empty($documentId) && !empty($historyId)) {
            $document = DocumentsGenerator::reconstruct($documentId, $historyId);
            $render = true;
            $save = false;
        } else {
            $document = DocumentsGenerator::generate(DocumentType::GOODS_RECEIVED);
            $document->makeEditable();

            $request = Yii::$app->request;
            $dataInput = $request->isPost ? $request->post() : [];

            $goodsReceivedTitleName = (new GoodsReceivedTitle())->formName();
            $goodsReceivedProductFormName = (new GoodsReceivedProductForm())->formName();

            if(!array_key_exists($goodsReceivedTitleName, $dataInput)) {
                $dataInput[$goodsReceivedTitleName] = WarehouseDeliveryQuery::getHeaderInformation($deliveryId);
            }
            if(!array_key_exists($goodsReceivedProductFormName, $dataInput)) {
                $dataInput[$goodsReceivedProductFormName] = WarehouseDeliveryProductQuery::getProductsByDeliveryId($deliveryId);
            }

            $document->loadInput($dataInput);
        
        if($request->isPost && $request->isAjax) {
            Yii::$app->response->format = Response::FORMAT_JSON;

            return $document->performActiveValidation();
        }
        
        }
        if($render) {
            if($save) {
                DocumentsGenerator::save($document);
            }
            
            $title = Yii::t('documents', 'Goods Received Note (GRN)');
            $number = $document->getSectionsSequence()[1]['number'];
            $filename = !empty($number) ? $number : '';
            
            (new \common\documents\pdf\Renderer([
                'options' => [
                    'title' => $title
                ]
            ], $filename
            ))->render($document);
        } else {
            $documentView = (new \common\documents\html\Renderer())->render($document);
            return $this->render('test', ['documentView' => $documentView]);
        }
    }

    public function actionCalculateGoodsReceivedAmounts() {
        $products = new GoodsReceivedProducts();
        $products->setEditable(true);

        $productFormName = (new GoodsReceivedProductForm())->formName();
        $input = Yii::$app->request->post();
        $input[$productFormName] = empty($input[$productFormName]) ? [] : array_values($input[$productFormName]);
        $products->loadInput($input);

        return $this->renderFile('@common/documents/html/views/goods-received-products/table-body.php', ['section' => $products, 'form' => new \yii\widgets\ActiveForm()]);
    }

    public function actionGetGoodsReceivedProductRow() {
        $product = new GoodsReceivedProductForm();

        $request = Yii::$app->request;
        return $this->renderFile('@common/documents/html/views/goods-received-products/product-row.php', [
                    'product' => $product,
                    'form' => new \yii\widgets\ActiveForm(),
                    'edit' => $request->get('edit'),
                    'idx' => $request->get('idx'),
        ]);
    }

    public function actionGenerateGoodsIssued($historyId = 0) {
    	if(!in_array(DocumentType::GOODS_ISSUED, DocumentTypeUtility::getAvailableDocuments())) {
    		throw new \yii\web\NotFoundHttpException(Yii::t('web', 'Element does not exist or you do not have permission.'));
    	}
    	
        $request = Yii::$app->request;
        $documentId = $request->get('documentId');
        $historyId = $request->get('historyId');
        $render = $request->isPost;
        $save = true;

        if(!empty($documentId) && !empty($historyId)) {
            $document = DocumentsGenerator::reconstruct($documentId, $historyId);
            $render = true;
            $save = false;
        } else {
            $document = DocumentsGenerator::generate(DocumentType::GOODS_ISSUED);
            $document->makeEditable();

            $dataInput = $request->isPost ? $request->post() : [];
            $goodsIssuedTitleName = (new GoodsIssuedTitle())->formName();
            $goodsIssuedProductFormName = (new GoodsIssuedProductForm())->formName();

            if(!array_key_exists($goodsIssuedTitleName, $dataInput)) {
                $dataInput[$goodsIssuedTitleName] = WarehouseProductStatusHistoryQuery::getHeaderInformationByHistoryId($historyId);
                $dataInput[$goodsIssuedTitleName]['status'] = intval(!empty($dataInput[$goodsIssuedTitleName]['isAccounted']));
                $dataInput[$goodsIssuedTitleName]['isNumberPredefined'] = 1;
                if(empty($dataInput[$goodsIssuedTitleName]['number'])) {
                	$dataInput[$goodsIssuedTitleName]['isNumberPredefined'] = 0;
                	$dataInput[$goodsIssuedTitleName]['number'] = NumberTemplateHelper::getNextNumber(Utility::NUMBER_TEMPLATE_GOODS_ISSUE);
                }
            }
            if(!array_key_exists($goodsIssuedProductFormName, $dataInput)) {
                $dataInput[$goodsIssuedProductFormName] = WarehouseProductStatusHistoryQuery::getProductsByHistoryId($historyId);
            }

            if($request->isPost && !$request->isAjax) {
                if(empty(Yii::$app->db->getTransaction())) {
                    $transaction = Yii::$app->db->beginTransaction('SERIALIZABLE');
                }
                
            	if(empty($dataInput[$goodsIssuedTitleName]['isNumberPredefined'])) {
            		$currentNumber = NumberTemplateHelper::getAndSaveNextNumber('', Utility::NUMBER_TEMPLATE_GOODS_ISSUE);
					$dataInput[$goodsIssuedTitleName]['number'] = $currentNumber['number'];
            	}
            }

            $document->loadInput($dataInput);

            if($request->isPost && $request->isAjax) {
                Yii::$app->response->format = Response::FORMAT_JSON;

                return $document->performActiveValidation();
            }
        }
        if($render) {
            try{ 
                if($save) {
                    DocumentsGenerator::save($document);
                }
                
                $title = Yii::t('documents', 'Goods Issue Note (GIN)');
                $number = $document->getSectionsSequence()[1]['number'];
                $filename = !empty($number) ? ' '.$number : '';
                
                (new \common\documents\pdf\Renderer([
                    'options' => [
    	                'title' => $title
    	            ]], $filename))->render($document);
    	            $transaction = Yii::$app->db->getTransaction();
    	            if(!empty($transaction)) {
    	                $transaction->commit();
    	            } 
            } catch(\Exception $e) {
                $transaction = Yii::$app->db->getTransaction();
                if(!empty($transaction)) {
                    $transaction->rollback();
                }
                Yii::error($e->getMessage());
                Yii::error($e->getTraceAsString());
            }
        } else {
            $documentView = (new \common\documents\html\Renderer())->render($document);
            return $this->render('test', ['documentView' => $documentView]);
        }
    }

    public function actionGetGoodsIssuedProductRow() {
        $product = new GoodsIssuedProductForm();

        $request = Yii::$app->request;
        return $this->renderFile('@common/documents/html/views/goods-issued-products/product-row.php', [
                    'product' => $product,
                    'form' => new \yii\widgets\ActiveForm(),
                    'edit' => $request->get('edit'),
                    'idx' => $request->get('idx'),
        ]);
    }

    public function actionDeliveryDetails($deliveryId = 0) {
    	if(!in_array(DocumentType::DELIVERY_DETAILS, DocumentTypeUtility::getAvailableDocuments())) {
    		throw new \yii\web\NotFoundHttpException(Yii::t('web', 'Element does not exist or you do not have permission.'));
    	}
    	
        $request = Yii::$app->request;
        $documentId = $request->get('documentId');
        $historyId = $request->get('historyId');
        $render = $request->isPost;
        $save = true;

        if(!empty($documentId) && !empty($historyId)) {
            $document = DocumentsGenerator::reconstruct($documentId, $historyId);
            $render = true;
            $save = false;
        } else {
            $document = DocumentsGenerator::generate(DocumentType::DELIVERY_DETAILS);
            $document->makeEditable();

            $request = Yii::$app->request;
            $dataInput = $request->isPost ? $request->post() : [];

            $deliveryDetailsTitleName = (new DeliveryDetailsTitle())->formName();
            $goodsReceivedProductFormName = (new GoodsReceivedProductForm())->formName();
            $goodsReceivedProducts = (new GoodsReceivedProducts())->formName();

            if(!array_key_exists($deliveryDetailsTitleName, $dataInput)) {
                $dataInput[$deliveryDetailsTitleName] = WarehouseDeliveryQuery::getHeaderInformation($deliveryId);
            }
            if(!array_key_exists($goodsReceivedProductFormName, $dataInput)) {
                $dataInput[$goodsReceivedProductFormName] = WarehouseDeliveryProductQuery::getProductsByDeliveryId($deliveryId);
            }

            $dataInput[$goodsReceivedProducts]['doubleName'] = true;

            $document->loadInput($dataInput);

            if($request->isPost && $request->isAjax) {
                    Yii::$app->response->format = Response::FORMAT_JSON;

                    return $document->performActiveValidation();
            }
        }
    	if($render) {
    		if($save) {
            	DocumentsGenerator::save($document);
            }
            
            $title = Yii::t('web', 'Order details');
            
    		(new \common\documents\pdf\Renderer([
                        'options' => [
                            'title' => $title,
                            ]
                ], $title
            ))->render($document);
        } else {
            $documentView = (new \common\documents\html\Renderer())->render($document);
            return $this->render('test', ['documentView' => $documentView]);
        }
    }
  
    public function actionGenerateFirefightingEqAcceptanceProtocol($orderId = 0, $edit = false) {
    	if(!in_array(DocumentType::FIREEQ_ACCEPTANCE_PROTOCOL, DocumentTypeUtility::getAvailableDocuments())) {
    		throw new \yii\web\NotFoundHttpException(Yii::t('web', 'Element does not exist or you do not have permission.'));
    	}
    	
        $request = Yii::$app->request;
        $documentId = $request->get('documentId');
        $historyId = $request->get('historyId');
        $render = $request->isPost;
        $save = true;
        $documentType = DocumentType::FIREEQ_ACCEPTANCE_PROTOCOL;

        if(!empty($documentId) && !empty($historyId) && $orderId == 0) {
            $document = DocumentsGenerator::reconstruct($documentId, $historyId);
            $render = true;
            $save = false;
        } else {
            if(!empty($documentId) && !empty($historyId)) {
                $document = DocumentsGenerator::reconstruct($documentId, $historyId, $edit);
            } else {
                $document = DocumentsGenerator::generate($documentType);
            }

            $document->makeEditable();

            $request = Yii::$app->request;
            $dataInput = $request->isPost ? $request->post() : [];

            $fireEqAcceptanceProtocolName = (new FireEqAcceptanceProtocol())->formName();

            $dataInput[$fireEqAcceptanceProtocolName]['orderId'] = $request->get('orderId');

            $document->loadInput($dataInput);
            if($request->isPost && $request->isAjax) {
                Yii::$app->response->format = Response::FORMAT_JSON;

                return $document->performActiveValidation();
            }
        }
        if($render) {
            if($save) {
                DocumentsGenerator::save($document);
            }
            
            $title = Yii::t('documents', 'PROTOCOL OF EXAMINATION OF FIRE SIGNALING SYSTEM');
            
            (new \common\documents\pdf\Renderer(['options' =>
	            [
	            	'title' => $title,
	        	]
            ], $title
            ))->render($document);
        } else {
            $documentView = (new \common\documents\html\Renderer())->render($document);
            return $this->render('test', [
                        'documentView' => $documentView,
                        'documents' => DocumentHelper::getDocuments($orderId, $documentType),
                        'orderId' => $orderId,
            ]);
        }
    }

    public function actionGetFireEqAcceptanceProductRow() {
        $request = Yii::$app->request;
        $row = new FireEqAcceptanceProductForm();
        return $this->renderFile('@common/documents/html/views/fireeq-acceptance-protocol/product-row.php', [
                    'editMode' => $request->get('editMode'),
                    'row' => $row,
                    'form' => new \yii\widgets\ActiveForm(),
                    'idx' => $request->get('idx'),
        ]);
    }

    public function actionGenerateMalfunctionProtocol($orderId = 0, $edit = false) {
    	if(!in_array(DocumentType::MALFUNCTION_PROTOCOL, DocumentTypeUtility::getAvailableDocuments())) {
    		throw new \yii\web\NotFoundHttpException(Yii::t('web', 'Element does not exist or you do not have permission.'));
    	}
    	
        $request = Yii::$app->request;
        $documentId = $request->get('documentId');
        $historyId = $request->get('historyId');
        $render = $request->isPost;
        $save = true;
        $documentType = DocumentType::MALFUNCTION_PROTOCOL;

        if(!empty($documentId) && !empty($historyId) && $orderId == 0) {
            $document = DocumentsGenerator::reconstruct($documentId, $historyId);
            $render = true;
            $save = false;
        } else {
            if(!empty($documentId) && !empty($historyId)) {
                $document = DocumentsGenerator::reconstruct($documentId, $historyId, $edit);
            } else {
                $document = DocumentsGenerator::generate($documentType);
            }
            $document->makeEditable();

            $request = Yii::$app->request;
            $dataInput = $request->isPost ? $request->post() : [];

            $malfunctionProtocolName = (new MalfunctionProtocol())->formName();

            if(!array_key_exists($malfunctionProtocolName, $dataInput)) {
                $dataInput[$malfunctionProtocolName] = [];
            }

            $dataInput[$malfunctionProtocolName]['orderId'] = $request->get('orderId');

            $document->loadInput($dataInput);
            if($request->isPost && $request->isAjax) {
                Yii::$app->response->format = Response::FORMAT_JSON;

                return $document->performActiveValidation();
            }
        }
        if($render) {
            if($save) {
                DocumentsGenerator::save($document);
            }
            
            $title = Yii::t('documents', 'Break-down protocol');
            
            (new \common\documents\pdf\Renderer(['options' => [
            		'title' => $title
            ]], $title))->render($document);
        } else {
            $documentView = (new \common\documents\html\Renderer())->render($document);
            return $this->render('test', [
                        'documentView' => $documentView,
                        'documents' => DocumentHelper::getDocuments($orderId, $documentType),
                        'orderId' => $orderId,
            ]);
        }
    }

    public function actionDelete() {       
    	$documentId = Yii::$app->request->post('documentId');
    	$historyId = Yii::$app->request->post('historyId');
    	
    	$transaction = Yii::$app->db->beginTransaction('SERIALIZABLE');
    	try {
    		$query = ['_id' => new ObjectID($documentId), 'history._id' => new ObjectID($historyId)];
	        $fieldsToFetch = [
		            'type' => 1, 
		            'history.$' => 1, 
		            'sections' => 1, 
		            'header' => 1, 
		            'footer' => 1,
	        ];
	        
	        $documentsCollection = Yii::$app->mongodb->getCollection('documents');
	        $documentData = $documentsCollection->findOne($query, $fieldsToFetch);
	        
	        $lastInvoiceIds = DocumentQuery::getLastInvoiceIds();
	        
    		if(empty($documentData) 
    				|| !in_array($documentData['type'], DocumentType::getInvoiceTypes()) 
    				|| !in_array($documentId, $lastInvoiceIds)) {
    			throw new \Exception('The user has not privilege to delete this document: ' . $documentId.' ('.$historyId.')');
    		}
    		
    		$entityId = null;
    		$orderId = $documentData['history'][0]['sections'][1]['orderId'];
    		if(!empty($orderId)) {
    			$order = Order::findOne($orderId);
    			$entityId = !empty($order) ? $order->entity_id : null;
    		}    		
    		
    		$numberTemplateSymbol = null;

    		if($documentData['type'] == DocumentType::INVOICE) {
    		    $numberTemplateSymbol = Utility::NUMBER_TEMPLATE_INVOICE;
    		}
    		elseif($documentData['type'] == DocumentType::PROFORMA_INVOICE) {
    		    $numberTemplateSymbol = Utility::NUMBER_TEMPLATE_PROFORMA_INVOICE;
    		}
    		elseif($documentData['type'] == DocumentType::ADVANCE_INVOICE) {
    		    $numberTemplateSymbol = Utility::NUMBER_TEMPLATE_ADVANCE_INVOICE;
    		}
    		
    		$currentNumber = NumberTemplateHelper::undoNumber($numberTemplateSymbol, $entityId);
    		$lastButOneInvoice = InvoiceHistoryQuery::getLastButOneInvoice($documentData['type'], $entityId, $currentNumber);
    		
    		
    		if($documentData['type'] == DocumentType::INVOICE) {
    		    $appCache = ApplicationCache::find()->where(['entity_id' => $entityId])->one();
    		    $appCache->date_last_invoice = !empty($lastButOneInvoice) ? $lastButOneInvoice['dateIssue'] : null;
    		    $appCache->save();
    		}
    		
    		//$lastInvoice = InvoiceHistoryQuery::findByDocumentId($documentId);
    		
    		//if(!empty($lastInvoice)) {
    		    InvoiceHistoryQuery::deleteInvoiceHistory($documentId, (!empty($lastButOneInvoice) ? $lastButOneInvoice['id'] : null));
    		//}
    		
    		$result = $documentsCollection->remove($query);
    		if(!$result) {
    			throw new \Exception('The document cannot be deleted: ' . $documentId.' ('.$historyId.')');
    		}
    	
    		$transaction->commit();
    		Yii::$app->getSession()->setFlash('success', Yii::t('web', 'The document has been deleted.'));
    	} catch (\Exception $e) {
    		$transaction->rollBack();
    		Yii::error($e->getMessage());
    		Yii::error($e->getTraceAsString());
    		Yii::$app->getSession()->setFlash('error', Yii::t('web', 'The document cannot be deleted. Please contact the administrator.'));
    	}
    	
    	return $this->redirect(Yii::$app->request->getReferrer());
    }
}
