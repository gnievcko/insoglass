<?php
namespace frontend\controllers;

use Yii;
use frontend\components\Controller;
use yii\filters\AccessControl;
use common\helpers\Utility;
use common\models\aq\OfferedProductCategoryQuery;
use frontend\models\OfferedProductCategoryListForm;
use common\models\ar\OfferedProductCategorySet;
use common\models\ar\OfferedProductCategoryTranslation;
use yii\helpers\Url;
use yii\data\Pagination;
use frontend\models\OfferedProductCategoryForm;
use yii\web\Response;
use yii\widgets\ActiveForm;
use common\models\ar\OfferedProductCategory;
use common\models\ar\Language;

class OfferedProductCategoryController extends Controller {

	public function behaviors() {
		$salesmanActions = ['list', 'find', 'details'];
		if(Yii::$app->params['canCudOfferedProducts']) {
			$salesmanActions = array_merge($salesmanActions, ['create', 'edit', 'delete']);
		}
		
		
		return [
				'access' => [
						'class' => AccessControl::className(),
						'only' => ['list', 'create', 'edit', 'delete', 'find', 'details'],
						'rules' => [
								[
										'actions' => $salesmanActions,
										'allow' => true, // zmienić na true, gdy będzie potrzebne
										'roles' => [Utility::ROLE_SALESMAN],
								],
						],
				],
		];
	}


	public function actionList() {
		$request = Yii::$app->request;
		$offeredProductCategoryListForm = new OfferedProductCategoryListForm();
		$offeredProductCategoryListForm->load($request->get());

		if(!$request->isAjax) {
			$categoryOptions = OfferedProductCategoryQuery::getCategoryList();
		}

		$itemsQuery = OfferedProductCategoryQuery::getActiveOfferedProductCategoryQuery($offeredProductCategoryListForm->toArray());
		$pages = new Pagination([
				'totalCount' => $itemsQuery->count(),
				'page' => $offeredProductCategoryListForm->page,
				'defaultPageSize' => \Yii::$app->params['defaultPageSize'],
		]);
		$items = $itemsQuery->limit($pages->defaultPageSize)->offset($pages->page * $pages->defaultPageSize)->all();

		$listData = [
				'offeredProductCategories' => $items,
				'pages' => $pages,
				'offeredProductCategoryListForm' => $offeredProductCategoryListForm,
		];

		if($request->isAjax) {
			echo json_encode([
					'view' => $this->renderAjax('_offeredProductCategories', $listData),
					'url' => Url::current([], true),
			]);
		}
		else {
			$listData['categoryTranslations'] = $categoryOptions;
			return $this->render('list', $listData);
		}
	}

	public function actionCreate() {
		$model = new OfferedProductCategoryForm();
		$model->setAddScenario();

		if(Yii::$app->request->isAjax && $model->load(Yii::$app->request->post())) {
			Yii::$app->response->format = Response::FORMAT_JSON;
			return ActiveForm::validate($model);
		}

		if($model->load(Yii::$app->request->post()) && $model->validate()) {
			try {
				$model->saveData();
				Yii::$app->getSession()->setFlash('success', Yii::t('web', 'Offered product category has been added successfully.'));

				return $this->redirect(['details', 'id' => $model->id]);
			}
			catch(\Exception $e) {
				Yii::$app->getSession()->setFlash('error', Yii::t('web', 'Offered Product category cannot be saved. Check provided values.'));
				Yii::error($e->getMessage());
				Yii::error($e->getTraceAsString());
			}
		}

		return $this->render('form', [
				'model' => $model,
		]);
	}

	public function actionEdit($id) {
		$offeredProductCategory = OfferedProductCategory::findOne($id);
		if(empty($offeredProductCategory)) {
			throw new \yii\web\NotFoundHttpException(Yii::t('web', 'Element does not exist or you do not have permission.'));
		}
		$model = new OfferedProductCategoryForm($offeredProductCategory);
		$model->setEditScenario();

		if(Yii::$app->request->isAjax && $model->load(Yii::$app->request->post())) {
			Yii::$app->response->format = Response::FORMAT_JSON;
			return ActiveForm::validate($model);
		}

		if($model->load(Yii::$app->request->post()) && $model->validate()) {
			try {
				$model->saveData();
				Yii::$app->getSession()->setFlash('success', Yii::t('web', 'Offered product category has been updated successfully.'));

				return $this->redirect(['list']);
			}
			catch(\Exception $e) {
				Yii::$app->getSession()->setFlash('error', Yii::t('web', 'Offered Product category cannot be saved. Check provided values.'));
				Yii::error($e->getMessage());
				Yii::error($e->getTraceAsString());
			}
		}

		return $this->render('form', [
				'model' => $model,
		]);
	}

	public function actionDelete() {
		$id = Yii::$app->request->post('id');

		$transaction = Yii::$app->db->beginTransaction();
		try {

			$offeredProductCategory = OfferedProductCategory::findOne($id);

			OfferedProductCategoryTranslation::deleteAll(['offered_product_category_id' => $id]);
			OfferedProductCategorySet::deleteAll(['offered_product_category_id' => $id]);

			$categories = OfferedProductCategory::find(['parent_category_id' => $id])->all();

			foreach($categories as $category) {
				$category->parent_category_id = null;
				$category->save();
			}

			$offeredProductCategory->delete();
			$transaction->commit();

			Yii::$app->getSession()->setFlash('success', Yii::t('web', 'Offered product category has been deleted.'));
		}
		catch(\Exception $e) {
			Yii::info(print_r($e->getMessage(), true), 'info');
			Yii::$app->getSession()->setFlash('error', Yii::t('web', 'Offered product category cannot be deleted. Please contact the administrator.'));
		}

		if(strpos(Yii::$app->request->getReferrer(), Yii::$app->controller->id.'/details') !== false) {
			return $this->redirect(['list']);
		}
		return $this->redirect(Yii::$app->request->getReferrer());
	}

	public function actionDetails($id) {
		$data = OfferedProductCategoryQuery::getDetails($id);
		if(empty($data)) {
			throw new \yii\web\NotFoundHttpException(Yii::t('web', 'Element does not exist or you do not have permission.'));
		}
		return $this->render('details', [
				'data' => $data,
		]);
	}

    public function actionFind() {
        $request = Yii::$app->request;
        $term = $request->get('term');
        $page = $request->get('page', 1);
        $size = $request->get('size');

		if(empty($term) || empty($page) || empty($size)) {
			Yii::$app->end();
		}

        $language = Language::find()->where(['symbol' => \Yii::$app->language])->one();
        $query = OfferedProductCategoryTranslation::find()->andWhere(['like', 'name', $term])->andWhere(['language_id' => $language->id]);
        $matchedCategoriesCount = $query->count();
        $categories = $query->limit($size)->offset($size*($page-1))->all();

        echo json_encode([
            'items' => array_map(function($category) {
                return ['id' => $category->offered_product_category_id, 'name' => $category->name];
            }, $categories),
            'more' => (intval($matchedCategoriesCount) > $page*$size)]);
        Yii::$app->end();
    }
}
