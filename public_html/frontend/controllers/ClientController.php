<?php

namespace frontend\controllers;

use Yii;
use frontend\components\Controller;
use yii\filters\AccessControl;
use yii\helpers\Url;
use yii\data\Pagination;
use common\helpers\Utility;
use common\models\aq\CompanyQuery;
use frontend\models\CompanyListForm;
use common\models\ar\CompanyGroupTranslation;
use common\models\aq\UserQuery;
use common\models\aq\UserCompanySalesmanQuery;
use common\models\aq\CompanyGroupSetQuery;
use frontend\models\CompanyForm;
use yii\web\Response;
use yii\widgets\ActiveForm;
use common\models\ar\Company;
use yii\filters\VerbFilter;
use frontend\models\ClientRepresentativeForm;
use common\models\ar\User;
use common\helpers\ChangeHelper;
use common\models\ar\ClientModificationHistory;
use yii\mongodb\Query;
use yii\helpers\ArrayHelper;
use common\models\aq\ContractTypeQuery;
use common\components\GetProvinceByZipCodeTrait;
use common\models\aq\DocumentQuery;
use common\documents\DocumentTypeUtility;

class ClientController extends Controller {
	use GetProvinceByZipCodeTrait;

    public function behaviors() {
        $allActions = ['list', 'details',
                       'create', 'edit', 'delete',
        ];

        return [
	            'access' => [
	                'class' => AccessControl::className(),
	                'only' => $allActions,
	                'rules' => [
	                        [
    	                        'actions' => $allActions,
    	                        'allow' => true, // zmienić na true, gdy będzie potrzebne
    	                        'roles' => [Utility::ROLE_SALESMAN],
    	                    ],
	                ],
	            ],
	            'verbs' => [
	                'class' => VerbFilter::className(),
	                'actions' => [
	                    'delete' => ['post'],
	                ],
	            ],
        ];
    }

    public function actionList() {
        $request = Yii::$app->request;
        $companyListForm = new CompanyListForm();
        $companyListForm->load($request->get());

        $activeCompaniesQuery = CompanyQuery::getActiveCompaniesQuery($companyListForm->toArray());
        $pages = new Pagination([
            'totalCount' => $activeCompaniesQuery->count(),
            'page' => $companyListForm->page,
            'defaultPageSize' => \Yii::$app->params['defaultPageSize'],
        ]);
        $companies = $activeCompaniesQuery->limit($pages->defaultPageSize)->offset($pages->page * $pages->defaultPageSize)->all();

        $companyListData = [
            'companies' => $companies,
            'pages' => $pages,
            'companyListForm' => $companyListForm,
        ];

        if ($request->isAjax) {
            echo json_encode([
                'view' => $this->renderAjax('_companies', $companyListData),
                'url' => Url::current([], true),
            ]);
        } else {
            $companyListData['companyGroupTranslations'] = CompanyGroupTranslation::find()->forLanguage(Yii::$app->language)->notPredefinedAndAvailable()->all();
            if (empty($companyListData['companyGroupTranslations'])) {
                $companyListData['companyGroupTranslations'] = CompanyGroupTranslation::find()->forLanguage(Yii::$app->language)->onlyDefault()->all();
            }

            return $this->render('list', $companyListData);
        }
    }

    public function actionDetails($id) {
        if (!UserQuery::isSalesmanAssignedToCompanyByCompanyId(Yii::$app->user->id, $id)) {
            //Yii::$app->getSession()->setFlash('error', Yii::t('web', 'A company does not exist or you do not have access rights.'));
            //return $this->redirect(['site/index']);
            throw new \yii\web\NotFoundHttpException(Yii::t('web', 'Element does not exist or you do not have permission.'));
        }

        $data = CompanyQuery::getDetails($id);
        $salesmen = UserCompanySalesmanQuery::getSalesmenByCompanyId($id);
        $contactPeople = UserQuery::getContactPeopleByCompanyId($id);
        $groups = CompanyGroupSetQuery::getGroupsByCompanyId($id);
        $childCompanies = CompanyQuery::getChildCompanies($id);

        return $this->render('details', [
                'data' => $data,
                'salesmen' => $salesmen,
                'contactPeople' => $contactPeople,
                'groups' => $groups,
				'documents' => $this->getDocuments($id),
                'childCompanies' => $childCompanies,
        		'documentsAbleToDelete' => DocumentQuery::getLastInvoiceIds(),
        		'availableDocuments' => DocumentTypeUtility::getAvailableDocuments(),
        ]);
    }

    public function actionCreate() {
        $model = new CompanyForm();
        $model->setAddScenario();
        $isEditScenarioSet = $model->isEditScenarioSet();        

        if (Yii::$app->request->isAjax && $model->load(Yii::$app->request->post())) {
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ActiveForm::validate($model);
            	+ ActiveForm::validateMultiple($model->getContactPeopleForms());
        }

        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            try {
                $model->saveData(Utility::ACTION_CREATE);
                Yii::$app->getSession()->setFlash('success', Yii::t('web', 'Client has been added successfully.'));

                return $this->redirect(['details', 'id' => $model->id]);
            } catch (\Exception $e) {
                Yii::$app->getSession()->setFlash('error', Yii::t('web', 'Client data cannot be saved. Check provided values.'));
                Yii::error($e->getMessage());
                Yii::error($e->getTraceAsString());
            }
        }

        return $this->render('form', [
                    'model' => $model,
                    'parentCompanyData' => null,
                    'contractTypes' => ArrayHelper::map(ContractTypeQuery::getContractTypes(), 'id', 'name'),
                    'authorizedSalesmenData' => [],
                    'groupsData' => [],
        			'contactPeopleForms' => $model->getContactPeopleForms(),
        			'isEditScenarioSet' => $isEditScenarioSet,
        ]);
    }

    public function actionEdit($id) {
        if(!UserQuery::isSalesmanAssignedToCompanyByCompanyId(Yii::$app->user->id, $id)) {
            //Yii::$app->getSession()->setFlash('error', Yii::t('web', 'A company does not exist or you do not have access rights.'));
            //return $this->redirect(['site/index']);
            throw new \yii\web\NotFoundHttpException(Yii::t('web', 'Element does not exist or you do not have permission.'));
        }

        $company = Company::findOne($id);
        $model = new CompanyForm($company);
        $model->setEditScenario();
        $contactPeopleForms = $model->getContactPeopleForms();
        $isEditScenarioSet = $model->isEditScenarioSet();

        if (Yii::$app->request->isAjax && $model->load(Yii::$app->request->post())) {
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ActiveForm::validate($model);
        }

        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            try {
                $model->saveData(Utility::ACTION_EDIT);

                Yii::$app->getSession()->setFlash('success', Yii::t('web', 'Client data has been updated successfully.'));

                return $this->redirect(['details', 'id' => $model->id]);
            } catch (\Exception $e) {
                Yii::$app->getSession()->setFlash('error', Yii::t('web', 'Client data cannot be saved. Check provided values.'));
                Yii::error($e->getMessage());
                Yii::error($e->getTraceAsString());
            }
        }

        return $this->render('form', [
                    'model' => $model,
                    'parentCompanyData' => $model->getParentCompanyData(),
                    'contractTypes' => ArrayHelper::map(ContractTypeQuery::getContractTypes(), 'id', 'name'),
                    'authorizedSalesmenData' => [],
                    'groupsData' => $model->getGroupsData(),
        			'contactPeopleForms' => $contactPeopleForms,
        			'isEditScenarioSet' => $isEditScenarioSet,
        ]);
    }

    public function actionDelete() {
        $id = Yii::$app->request->post('id');

        try {
            if (!UserQuery::isSalesmanAssignedToCompanyByCompanyId(Yii::$app->user->id, $id)) {
                throw new \Exception('The user has not privilege to delete this client: ' . $id);
            }

            $company = Company::findOne($id);
            $company->is_active = 0;
            $changeHelper = new ChangeHelper($company, Utility::ACTION_DELETE);
            $changeHelper->saveChanges($id, new ClientModificationHistory());
            $company->save();

            Yii::$app->getSession()->setFlash('success', Yii::t('web', 'The client has been deleted.'));
        } catch (\Exception $e) {
            Yii::info(print_r($e->getMessage(), true), 'info');
            Yii::$app->getSession()->setFlash('error', Yii::t('web', 'The client cannot be deleted. Please contact the administrator.'));
        }

        if (strpos(Yii::$app->request->getReferrer(), Yii::$app->controller->id . '/details') !== false) {
            return $this->redirect(['list']);
        }
        return $this->redirect(Yii::$app->request->getReferrer());
    }

    private function getDocuments($id) {
        if (isset(Yii::$app->params['isDocumentStorageAvailable']) && Yii::$app->params['isDocumentStorageAvailable']) {
            $where = [
                'history.sections.companyId' => (int) $id,
                'history.isMigrated' => 1
            ];
            $query = new Query();

            $query->select([])
                    ->from('documents')
                    ->orderBy(['history.createdAt' => SORT_DESC])
                    ->where($where);

            $rows = $query->all();

            return $rows;
        } else {
            return [];
        }
    }
    
    public function actionGetContactPersonForm() {
    	if(\Yii::$app->request->isAjax) {
    		$form = new ClientRepresentativeForm();
    		$form->setAddScenario();
    		echo $this->renderAjax('inc/_contactPerson', [
    				'contactPersonForm' => $form,
    				'prefix' => \Yii::$app->request->get('prefix'),
    				'formId' => \Yii::$app->request->get('formId'),
    		]);
    	}
    }
}
