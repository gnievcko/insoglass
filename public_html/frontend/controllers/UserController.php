<?php
namespace frontend\controllers;

use Yii;
use frontend\components\Controller;
use yii\filters\AccessControl;
use yii\web\Response;
use yii\widgets\ActiveForm;

use common\models\ar\User;
use common\helpers\Utility;
use frontend\models\UserAccountForm;
use common\models\aq\UserCompanySalesmanQuery;
use common\models\aq\UserQuery;
use common\models\aq\RoleQuery;

class UserController extends Controller {

	public function behaviors() {
		$allActions = [
				'account', 'accountEdit',
		];

		return [
				'access' => [
						'class' => AccessControl::className(),
						'only' => $allActions,
						'rules' => [
								[
										'actions' => $allActions,
										'allow' => true,
										'roles' => [Utility::ROLE_SALESMAN, Utility::ROLE_STOREKEEPER],
								],
						],
				],
		];
	}

	public function actionAccount(){
		$user = User::findOne(Yii::$app->user->id);
		$companyGroups = UserCompanySalesmanQuery::getCompanyGroupsTranslationsByUserId($user->id)->all();
		$warehouses = Yii::$app->params['isMoreWarehousesAvailable'] ? UserWarehouseQuery::getWarehousesByUserId($id)->all() : [];
		$roles = RoleQuery::getRolesTranslationsByUserId($user->id)->one()['name'];
		return $this->render('details', [
				'model' => $user,
				'companyGroups' => $companyGroups,
				'warehouses' => $warehouses,
				'roles' => $roles,
		]);

	}

	public function actionAccountEdit(){
		$user = User::findOne(Yii::$app->user->id);
		$model = new UserAccountForm($user);
		$model->setEditScenario();

		if(Yii::$app->request->isAjax && $model->load(Yii::$app->request->post())) {
			Yii::$app->response->format = Response::FORMAT_JSON;
			return ActiveForm::validate($model);
		}

		if($model->load(Yii::$app->request->post()) && $model->validate()) {
			$model->saveData();
			Yii::$app->getSession()->setFlash('success', Yii::t('web', 'Informaction has been updated successfully.'));
			return $this->redirect(['account']);
		}

		return $this->render('form', [
				'model' => $model
		]);

	}
}
