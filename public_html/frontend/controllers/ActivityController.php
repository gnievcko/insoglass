<?php

namespace frontend\controllers;

use common\models\aq\ActivityQuery;
use frontend\components\Controller;
use Yii;
use yii\filters\AccessControl;
use yii\helpers\Url;
use yii\data\Pagination;
use common\models\ar\Activity;
use common\models\ar\ActivityTranslation;
use frontend\models\ActivityForm;
use frontend\models\ActivityListForm;
use yii\web\Response;
use common\components\ActiveForm;
use yii\filters\VerbFilter;
use common\helpers\Utility;

class ActivityController extends Controller {

    public function behaviors() {
        $allActions = ['list', 'details', 'create', 'edit', 'delete', 'find'];

        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => $allActions,
                'rules' => [
                    [
                        'actions' => $allActions,
                        'allow' => Yii::$app->params['isProductionVisible'],
                        'roles' => [ Utility::ROLE_WORKER ],
                    ],
                    [
                        'actions' => ['find'],
                        'allow' => true,
                    ],
                ],
            ],
            'verb' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    public function actionList() {
        $request = Yii::$app->request;
        $activitiesListForm = new ActivityListForm();
        $activitiesListForm->load($request->get());

        $activityQuery = ActivityQuery::getActivityQuery($activitiesListForm->toArray());
        $pages = new Pagination([
            'totalCount' => $activityQuery->count(),
            'page' => $activitiesListForm->page,
            'defaultPageSize' => PHP_INT_MAX,
        ]);

        $activities = $activityQuery->limit($pages->defaultPageSize)->offset($pages->page * $pages->defaultPageSize)->all();

        $activitiesData = [
            'activities' => $activities,
            'pages' => $pages,
            'activitiesListForm' => $activitiesListForm,
        ];

        if($request->isAjax) {
            echo json_encode([
                'view' => $this->renderAjax('_activities', $activitiesData),
                'url' => Url::current([], true),
            ]);
        } else {
            return $this->render('list', $activitiesData);
        }
    }

    public function actionDelete() {
        $id = Yii::$app->request->post('id');

        $transaction = Yii::$app->db->beginTransaction();
        try {
            ActivityTranslation::deleteAll(['activity_id' => $id]);
            Activity::deleteAll(['id' => $id]);

            $transaction->commit();
            Yii::$app->getSession()->setFlash('success', Yii::t('web', 'Activity has been deleted.'));
        } catch(\Exception $e) {
            $transaction->rollBack();
            Yii::error($e->getMessage());
            Yii::error($e->getTraceAsString());
            Yii::$app->getSession()->setFlash('error', Yii::t('web', 'Activity cannot be delete, please connect with adminstrator'));
        }

        if(strpos(Yii::$app->request->getReferrer(), Yii::$app->controller->id . '/details') != false) {
            return $this->redirect(['list']);
        }

        return $this->redirect(Yii::$app->request->getReferrer());
    }

    public function actionDetails($id) {
        $details = ActivityQuery::getDetails($id);
        if(empty($details)) {
            throw new \yii\web\NotFoundHttpException(Yii::t('web', 'Element doesn\'t exist or you do not have permission'));
        }

        return $this->render('details', ['details' => $details]);
    }

    public function actionCreate() {
        $model = new ActivityForm();
        $model->setAddScenario();

        if(Yii::$app->request->isAjax && $model->load(Yii::$app->request->post())) {
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ActiveForm::validate($model);
        }

        if($model->load(Yii::$app->request->post()) && $model->validate()) {
            try {
                $model->saveData();
                Yii::$app->getSession()->setFlash('success', Yii::t('web', 'Activity has been added successfully.'));

                return $this->redirect(['details', 'id' => $model->id]);
            } catch(\Exception $e) {
                Yii::$app->getSession()->setFlash('error', Yii::t('web', 'Activity cannot be added, please connect with administrator'));
                Yii::error($e->getMessage());
                Yii::error($e->getTraceAsString());
            }
        }

        return $this->render('form', [
                    'model' => $model,
        ]);
    }

    public function actionEdit($id) {
        $activity = Activity::findOne($id);
        if(empty($activity)) {
            throw new \yii\web\NotFoundHttpException(Yii::t('web', 'Element does not exist or you do not have permission'));
        }

        $model = new ActivityForm($activity);
        $model->setEditScenario();

        if(Yii::$app->request->isAjax && $model->load(Yii::$app->request->post())) {
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ActiveForm::validate($model);
        }

        if($model->load(Yii::$app->request->post()) && $model->validate()) {
            try {
                $model->saveData();
                Yii::$app->getSession()->setFlash('success', Yii::t('web', 'Activity has been updated successfully.'));
                return $this->redirect(['details', 'id' => $model->id]);
            } catch(\Exception $e) {
                Yii::$app->getSession()->setFlash('error', Yii::t('web', 'Activity cannot be saved. Check provided values'));
                Yii::error($e->getMessage());
                Yii::error($e->getTraceAsString());
            }
        }

        return $this->render('form', [
            'model' => $model,
        ]);
    }

    public function actionFind() {
        $request = Yii::$app->request;
        $term = $request->get('term');
        $page = $request->get('page', 1);
        $size = $request->get('size');
        $fullSize = $request->get('fullSize');

        if(empty($fullSize) && (empty($term) || empty($page) || empty($size))) {
            Yii::$app->end();
        }

        $query = ActivityQuery::getActivityQuery(['name' => $term, 'page' => $page, 'size' => $size]);
        $count = $query->count();
        if(empty($fullSize)) {
            $items = $query->limit($size)->offset($size * ($page - 1))->all();
        } else {
            $items = $query->all();
        }

        echo json_encode([
            'items' => $items,
            'more' => (intval($count) > $page * $size),
        ]);
    }

}