<?php

namespace frontend\controllers;

use Yii;
use frontend\components\Controller;
use yii\filters\AccessControl;
use yii\helpers\Url;
use yii\data\Pagination;
use yii\web\Response;
use common\components\ActiveForm;
use yii\helpers\ArrayHelper;
use frontend\models\DepartmentsListForm;
use frontend\models\DepartmentForm;
use common\models\aq\DepartmentQuery;
use common\models\aq\DepartmentTypeQuery;
use common\models\ar\Department;
use common\models\ar\UserDepartment;
use common\models\ar\DepartmentActivity;
use common\models\aq\DepartmentActivityQuery;
use common\models\ar\DepartmentTranslation;
use common\models\aq\UserDepartmentQuery;
use yii\filters\VerbFilter;
use common\helpers\Utility;

class DepartmentController extends Controller {
    public function behaviors() {
        $allActions = ['list','delete','create','details','edit', 'find'];
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => $allActions,
                'rules' => [
                    [
                        'actions' => $allActions,
                        'allow' => Yii::$app->params['isProductionVisible'],
                        'roles' => [ Utility::ROLE_WORKER ],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    public function actionList() {
        $listForm = new DepartmentsListForm();
        $request = Yii::$app->request;
        $listForm->load($request->get());
        $departmentQuery = DepartmentQuery::getDepartmentsQuery($listForm->toArray());

        $pages = new Pagination([
            'totalCount' => $departmentQuery->count(),
            'page' => $listForm->page,
            'defaultPageSize' => \Yii::$app->params['defaultPageSize'],
        ]);

        $departments = $departmentQuery->limit($pages->defaultPageSize)->offset($pages->page * $pages->defaultPageSize)->all();

        $typesQuery = DepartmentTypeQuery::getAllTypes();
        $types = $typesQuery->all();
        $map = ArrayHelper::map($types, 'id', 'name');

        $departmentListData = [
            'departments' => $departments,
            'pages' => $pages,
            'listForm' => $listForm,
            'departmentTypes'=> $map,
        ];

        if($request->isAjax) {
            echo json_encode([
                'view' => $this->renderAjax('_departments', $departmentListData),
                'url' => Url::current([], true),
            ]);
        }
        else{
            return $this->render('list', $departmentListData);
        }
    }

    public function actionDelete() {
        $id = Yii::$app->request->post('id');
        try{
            UserDepartment::deleteAll(['department_id' => $id]);
            DepartmentTranslation::deleteAll(['department_id' => $id]);
            DepartmentActivity::deleteAll(['department_id'=>$id]);
            Department::deleteAll(['id' => $id]);
            Yii::$app->getSession()->setFlash('success', Yii::t('web', 'Department has been deleted.'));
        }
        catch(\Exception $e) {
            Yii::error(print_r($e->getMessage(), true), 'error');
            Yii::error(print_r($e->getTraceAsString(), true), 'error');
            Yii::$app->getSession()->setFlash('error', Yii::t('web', 'Department cannot be deleted. Please contact the administrator.'));
        }

        if(strpos(Yii::$app->request->getReferrer(), Yii::$app->controller->id.'/details') !== false) {
            return $this->redirect(['list']);
        }
        return $this->redirect(Yii::$app->request->getReferrer());
    }

    public function actionCreate() {
        $model = new DepartmentForm();
        $model->setAddScenario();

        if(Yii::$app->request->isAjax && $model->load(Yii::$app->request->post())) {
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ActiveForm::validate($model);
        }

        if($model->load(Yii::$app->request->post()) && $model->validate()) {
            try {
                Yii::info(Yii::$app->request->post(), 'info');
                $model->saveData();
                Yii::info($model, 'info');
                Yii::$app->getSession()->setFlash('success', Yii::t('web', 'Department has been added successfully.'));
                return $this->redirect(['details', 'id' => $model->id]);
            }
            catch (\Exception $e) {
                Yii::$app->getSession()->setFlash('error', Yii::t('web', 'Department data cannot be saved. Check provided values.'));
                Yii::error($e->getMessage());
                Yii::error($e->getTraceAsString());
            }
        }
        $typesQuery = DepartmentTypeQuery::getAllTypes();
        $types = $typesQuery->all();
        $map = ArrayHelper::map($types, 'id', 'name');

        if(empty($types)) {
            Yii::$app->getSession()->setFlash('error', Yii::t('web', 'You have to declare some department types first.'));
            return $this->redirect(['list',[]]);
        }
        else{
            return $this->render('form', [
                'model' => $model,
                'departmentTypes' => $map,
                'workers'=> $model->getWorkersData(),
                'manager' => $model->getManagerData(),
                'activities'=> $model->getActivitiesData(),
            ]);
        }
    }

    public function actionDetails($id) {
        $details = DepartmentQuery::getDetails($id);
        $users = UserDepartmentQuery::getUsers($id);
        $activities = DepartmentActivityQuery::getActivitiesByDepartmentId($id);

        if(empty($details)) {
            throw new \yii\web\NotFoundHttpException(Yii::t('web', 'Element does not exist or you do not have permission.'));
        }
        return $this->render('details', [
            'details' => $details,
            'users' => $users,
            'activities'=>$activities,
        ]);
    }

    public function actionEdit($id) {
        $department = Department::findOne($id);

        if(empty($department)) {
            throw new \yii\web\NotFoundHttpException(Yii::t('web', 'Element does not exist or you do not have permission.'));
        }
        $model = new DepartmentForm($department);
        $model->setEditScenario();
        if(Yii::$app->request->isAjax && $model->load(Yii::$app->request->post())) {
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ActiveForm::validate($model);
        }

        if($model->load(Yii::$app->request->post()) && $model->validate()) {
            try {
                $model->saveData();
                Yii::$app->getSession()->setFlash('success', Yii::t('web', 'Department has been updated successfully.'));

                return $this->redirect(['details' , 'id' => $model->id]);
            }
            catch(\Exception $e) {
                Yii::$app->getSession()->setFlash('error', Yii::t('web', 'Department data cannot be saved. Check provided values.'));
                Yii::error($e->getMessage());
                Yii::error($e->getTraceAsString());
            }
        }
        $typesQuery = DepartmentTypeQuery::getAllTypes();
        $types = $typesQuery->all();
        $map = ArrayHelper::map($types, 'id', 'name');

        if(empty($types)) {
            Yii::$app->getSession()->setFlash('error', Yii::t('web', 'You have to declare some department types first.'));
            return $this->redirect(['list',[]]);
        }
        else{
            return $this->render('form', [
                'model' => $model,
                'departmentTypes' => $map,
                'workers' => $model->getWorkersData(),
                'manager' => $model->getManagerData(),
                'activities'=> $model->getActivitiesData(),
            ]);
        }
    }
    
    public function actionFind() {
        $request = Yii::$app->request;
        $term = $request->get('term');
        $size = $request->get('size');
        $page = $request->get('page');
        
        if(empty($term) || empty($page) || empty($size)) {
            Yii::$app->end();
        }
        
        $query = DepartmentQuery::getDepartmentsQuery(['name'=>$term, 'page'=>$page, 'size'=>$size]);
        $count = $query->count();
        $items = $query->limit($size)->offset($size * ($page - 1))->all();
        
        echo json_encode([
            'items' => $items,
            'more' => (intval($count) > $page * $size),
        ]);
    }
}
