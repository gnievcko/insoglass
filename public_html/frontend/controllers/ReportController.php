<?php

namespace frontend\controllers;

use Yii;
use frontend\components\Controller;
use frontend\models\ReportForm;
use frontend\models\LostClientForm;
use yii\filters\AccessControl;
use yii\helpers\Url;
use yii\web\Response;
use yii\widgets\ActiveForm;
use common\documents\DocumentsGenerator;
use common\documents\DocumentType;
use common\documents\pdf\Renderer;
use common\helpers\Utility;
use common\helpers\StringHelper;
use common\documents\DocumentTypeUtility;
use common\documents\forms\OrderRegisterForm;
use common\documents\forms\ListOfItemsReportForm;

class ReportController extends Controller {

    public function behaviors() {
        $allActions = ['utilization-report', 'task-report', 'lost-client-report', 'list-of-items-report',
				'order-report', 'order-register', 'goods-issued-report', 'goods-delivered-report',
				'sales-result-report', 'index', 'reminder-report', 'intermediate-products-report'];

        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => $allActions,
                'rules' => [
                        [
		                        'actions' => ['utilization-report', 'task-report', 'lost-client-report', 
		                        		'list-of-items-report', 'order-report', 'order-register', 
		                        		'goods-issued-report', 'goods-delivered-report', 'sales-result-report',
		                              'intermediate-products-report'],
		                        'allow' => true,
		                        'roles' => [Utility::ROLE_MAIN_SALESMAN, Utility::ROLE_MAIN_STOREKEEPER],
	                    ],
                		[
                				'actions' => ['index', 'reminder-report'],
                				'allow' => true,
                				'roles' => [Utility::ROLE_SALESMAN, Utility::ROLE_STOREKEEPER],
                		],
                ],
            ],
        ];
    }

    public function actionUtilizationReport() {
    	if(!in_array(DocumentType::REPORT_UTILIZATION, DocumentTypeUtility::getAvailableDocuments())) {
    		throw new \yii\web\NotFoundHttpException(Yii::t('web', 'Element does not exist or you do not have permission.'));
    	}
    	
    	$this->generateReport(DocumentType::REPORT_UTILIZATION);
    }

    public function actionTaskReport() {
    	if(!in_array(DocumentType::TASK_REPORT, DocumentTypeUtility::getAvailableDocuments())) {
    		throw new \yii\web\NotFoundHttpException(Yii::t('web', 'Element does not exist or you do not have permission.'));
    	}
    	
    	$this->generateReport(DocumentType::TASK_REPORT);
    }

    public function actionLostClientReport() {
    	if(!in_array(DocumentType::LOST_CLIENT, DocumentTypeUtility::getAvailableDocuments())) {
    		throw new \yii\web\NotFoundHttpException(Yii::t('web', 'Element does not exist or you do not have permission.'));
    	}
    	
        $request = Yii::$app->request;
        
        $model = new LostClientForm();
        if($request->isAjax && $model->load($request->post())) {
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ActiveForm::validate($model);
        }

        if($model->load($request->post()) && $model->validate()) {
            $postLostClientForm = $request->post('LostClientForm');
            try {
                if(date_create_from_format('d.m.Y', $postLostClientForm['dateFrom']) > date_create_from_format('d.m.Y', $postLostClientForm['dateTo'])) {
                    throw new \Exception();
                }
                if(date_create_from_format('d.m.Y', $postLostClientForm['compareDateFrom']) > date_create_from_format('d.m.Y', $postLostClientForm['compareDateTo'])) {
                    throw new \Exception();
                }
                if(date_create_from_format('d.m.Y', $postLostClientForm['dateFrom']) > date_create_from_format('d.m.Y', $postLostClientForm['compareDateFrom'])) {
                    throw new \Exception();
                }
                if(date_create_from_format('d.m.Y', $postLostClientForm['dateTo']) > date_create_from_format('d.m.Y', $postLostClientForm['compareDateFrom'])) {
                    throw new \Exception();
                }
                $document = DocumentsGenerator::generate(DocumentType::LOST_CLIENT);
                $dataInput = null;
                $document->loadInput($dataInput);
                
                $title = str_replace(' ', '_', DocumentTypeUtility::translation()[$document->getType()])
                		.'_'.$request->get('dateFrom').'_'.$request->get('dateTo')
                		.'_'.$postLostClientForm['compareDateFrom'].'_'.$postLostClientForm['compareDateTo'];

                $documentView = (new Renderer([
                				'options' => [
                						'title' => $title,	
                				],
                		], $title
                ))->render($document);
            } catch(\Exception $e) {
                 Yii::error($e->getMessage());
                throw new \yii\web\NotFoundHttpException(Yii::t('web', 'Element does not exist or you do not have permission.'));
            }
        } else {
            $dateTo = $request->get('dateTo');
            $dateFrom = $request->get('dateFrom');
            
            $model->dateFrom = $dateFrom;
            $model->dateTo = $dateTo;
            $model->compareDateFrom = date('d.m.Y', strtotime($dateTo) + 7 * 24 * 60 * 60);
            $model->compareDateTo = date('d.m.Y', strtotime($dateTo) + (strtotime($dateTo) - strtotime($dateFrom)) + 7 * 24 * 60 * 60);
            return $this->render('lostClientReport', [
					'model' => $model,
            ]);
        }
    }

    public function actionOrderReport() {
    	if(!in_array(DocumentType::REPORT_ORDER, DocumentTypeUtility::getAvailableDocuments())) {
    		throw new \yii\web\NotFoundHttpException(Yii::t('web', 'Element does not exist or you do not have permission.'));
    	}
    	
    	$this->generateReport(DocumentType::REPORT_ORDER, [], true);
    }

    public function actionOrderRegister() {
    	if(!in_array(DocumentType::ORDER_REGISTER, DocumentTypeUtility::getAvailableDocuments())) {
    		throw new \yii\web\NotFoundHttpException(Yii::t('web', 'Element does not exist or you do not have permission.'));
    	}
    	
    	$request = Yii::$app->request;
    	
    	$model = new OrderRegisterForm();
    	if($request->isAjax && $model->load($request->post())) {
    		Yii::$app->response->format = Response::FORMAT_JSON;
    		return ActiveForm::validate($model);
    	}
    	
    	if($request->isPost && $model->load($request->post()) && $model->validate()) {
    		$this->generateReport(DocumentType::ORDER_REGISTER, $model->attributes, false, ['dateFrom' => $model->dateFrom, 'dateTo' => $model->dateTo]);
    	} 
    	else {
    		$dateTo = $request->get('dateTo');
    		$dateFrom = $request->get('dateFrom');
    	
    		$model->dateFrom = $dateFrom;
    		$model->dateTo = $dateTo;
    		
    		return $this->render('orderRegister', [
    				'model' => $model,
    		]);
    	}
    }
    
    public function actionListOfItemsReport() {
    	if(!in_array(DocumentType::LIST_OF_ITEMS, DocumentTypeUtility::getAvailableDocuments())) {
    		throw new \yii\web\NotFoundHttpException(Yii::t('web', 'Element does not exist or you do not have permission.'));
    	}
    	
    	$request = Yii::$app->request;
    	 
    	$model = new ListOfItemsReportForm();
    	if($request->isAjax && $model->load($request->post())) {
    		Yii::$app->response->format = Response::FORMAT_JSON;
    		return ActiveForm::validate($model);
    	}
    	 
    	if($request->isPost && $model->load($request->post()) && $model->validate()) {
    		$this->generateReport(DocumentType::LIST_OF_ITEMS, $model->attributes, true, ['dateFrom' => $model->dateFrom, 'dateTo' => $model->dateTo]);
    	}
    	else {
    		$dateTo = $request->get('dateTo');
    		$dateFrom = $request->get('dateFrom');
    		 
    		$model->dateFrom = $dateFrom;
    		$model->dateTo = $dateTo;
    	
    		return $this->render('listOfItemsReport', [
    				'model' => $model,
    		]);
    	}
    }

    public function actionGoodsIssuedReport() {
    	if(!in_array(DocumentType::GOODS_ISSUED_REPORT, DocumentTypeUtility::getAvailableDocuments())) {
    		throw new \yii\web\NotFoundHttpException(Yii::t('web', 'Element does not exist or you do not have permission.'));
    	}
    	
    	$this->generateReport(DocumentType::GOODS_ISSUED_REPORT);
    }
    
    public function actionGoodsDeliveredReport() {
    	if(!in_array(DocumentType::GOODS_DELIVERED_REPORT, DocumentTypeUtility::getAvailableDocuments())) {
    		throw new \yii\web\NotFoundHttpException(Yii::t('web', 'Element does not exist or you do not have permission.'));
    	}

    	$this->generateReport(DocumentType::GOODS_DELIVERED_REPORT);
    }

    public function actionProductionProgressReport() {
        if(!in_array(DocumentType::PRODUCTION_PROGRESS, DocumentTypeUtility::getAvailableDocuments())) {
            throw new \yii\web\NotFoundHttpException(Yii::t('web', 'Element does not exist or you do not have permission.'));
        }
        $request = Yii::$app->request;

        $dateTo = $request->get('dateTo');
        $dateFrom = $request->get('dateFrom');


        if(!isset($dateTo ) || !isset($dateFrom)){
            throw new \yii\web\NotFoundHttpException(Yii::t('web', 'Date range must be set.'));
        }
        $dateTo = str_replace(".", "-",(string)$dateTo);
        $dateFrom = str_replace(".", "-",(string)$dateFrom);

        $data = [
            'dateTo' => $dateTo,
            'dateFrom' => $dateFrom,
        ];

        $this->generateReport(DocumentType::PRODUCTION_PROGRESS, $data);
    }

    public function actionQualityControlReport() {
        if(!in_array(DocumentType::QUALITY_CONTROL, DocumentTypeUtility::getAvailableDocuments())) {
            throw new \yii\web\NotFoundHttpException(Yii::t('web', 'Element does not exist or you do not have permission.'));
        }
        $request = Yii::$app->request;

        $dateTo = $request->get('dateTo');
        $dateFrom = $request->get('dateFrom');


        if(!isset($dateTo ) || !isset($dateFrom)){
            throw new \yii\web\NotFoundHttpException(Yii::t('web', 'Date range must be set.'));
        }
        $dateTo = str_replace(".", "-",(string)$dateTo);
        $dateFrom = str_replace(".", "-",(string)$dateFrom);

        $data = [
            'dateTo' => $dateTo,
            'dateFrom' => $dateFrom,
        ];

        $this->generateReport(DocumentType::QUALITY_CONTROL, $data);
    }

    public function actionSalesResultReport() {	
    	if(!in_array(DocumentType::SALES_RESULT_REPORT, DocumentTypeUtility::getAvailableDocuments())) {
    		throw new \yii\web\NotFoundHttpException(Yii::t('web', 'Element does not exist or you do not have permission.'));
    	}
    	
    	$this->generateReport(DocumentType::SALES_RESULT_REPORT); 
    }
    
    public function actionReminderReport() {
    	if(!in_array(DocumentType::REMINDER_REPORT, DocumentTypeUtility::getAvailableDocuments())) {
    		throw new \yii\web\NotFoundHttpException(Yii::t('web', 'Element does not exist or you do not have permission.'));
    	}
    	 
    	$this->generateReport(DocumentType::REMINDER_REPORT);
    }
    
    public function actionIntermediateProductsReport() {
        if(!in_array(DocumentType::INTERMEDIATE_PRODUCTS, DocumentTypeUtility::getAvailableDocuments())) {
            throw new \yii\web\NotFoundHttpException(Yii::t('web', 'Element does not exist or you do not have permission.'));
        }
        
        $this->generateReport(DocumentType::INTERMEDIATE_PRODUCTS);
    }
        
    private function generateReport($documentType, $data = [], $limit = false, $params = []) {
    	if($limit) {
    		set_time_limit(600);
    		ini_set('memory_limit', '1024M');
    	}
    	
    	try {
    		$request = Yii::$app->request;
    		if(date_create_from_format('d.m.Y', $request->get('dateFrom')) > date_create_from_format('d.m.Y', $request->get('dateTo'))) {
    			throw new \Exception();
    		}
    		$document = DocumentsGenerator::generate($documentType);
    		$dataInput = $data;
    		$document->loadInput($dataInput);
    		$dateFrom = $request->get('dateFrom');
    		$dateTo = $request->get('dateTo');
    		if(isset($params['dateFrom'])) {
    			$dateFrom = $params['dateFrom'];
    		}
    		if(isset($params['dateTo'])) {
    			$dateTo = $params['dateTo'];
    		}
    		$title = str_replace(' ', '_', DocumentTypeUtility::translation()[$document->getType()]).'_'.$dateFrom.'_'.$dateTo;
    		return (new Renderer([
    						'options' => [
    								'title' => $title,
    						]
    				], $title
    		))->render($document);
    	} catch(\Exception $e) {
    		Yii::error($e->getMessage());
    		Yii::error($e->getTraceAsString());
    		throw new \yii\web\NotFoundHttpException(Yii::t('web', 'Element does not exist or you do not have permission.'));
    	}
    }

    public function actionIndex() {
        $model = new ReportForm();

        $request = Yii::$app->request;
        if($request->isAjax && $model->load($request->post())) {
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ActiveForm::validate($model);
        }

        $options = [
            Yii::t('web', 'Last week'),
            Yii::t('web', 'Last month'),
            Yii::t('web', 'Last quarter'),
            Yii::t('web', 'Last year'),
            Yii::t('web', 'Manually selected range'),
        ];
        
        $availableDocuments = DocumentTypeUtility::getAvailableDocuments();
        $isUserMain = Yii::$app->user->can(Utility::ROLE_MAIN_SALESMAN) || Yii::$app->user->can(Utility::ROLE_MAIN_STOREKEEPER) ||
                Yii::$app->user->can(Utility::ROLE_MAIN_WORKER);
        
        $reports = [
            [
                'url' => [
                    'pdf' => Url::to(['report/order-report'])
                ],
                'name' => StringHelper::translateOrderToOffer('main', 'Orders'),
                'text' => StringHelper::translateOrderToOffer('web', 'Report on orders'),
                'actionList' => in_array(DocumentType::REPORT_ORDER, $availableDocuments) && $isUserMain ? 'show' : 'hide',
            ],
            [
                'url' => [
                    'pdf' => Url::to(['report/utilization-report'])
                ],
                'name' => Yii::t('web', 'Utilizations'),
                'text' => Yii::t('web', 'Report on utilizations'),
                'actionList' => in_array(DocumentType::REPORT_UTILIZATION, $availableDocuments) && $isUserMain ? 'show' : 'hide',
            ],
            [
                'url' => [
                    'pdf' => Url::to(['report/order-register'])
                ],
                'name' => StringHelper::translateOrderToOffer('web', 'Order register'),
                'text' => StringHelper::translateOrderToOffer('web', 'Register of orders within a selected period.'),
                'actionList' => (Yii::$app->params['isDocumentStorageAvailable'] && in_array(DocumentType::ORDER_REGISTER, $availableDocuments) && $isUserMain) ? 'show' : 'hide',
            ],
            [
                'url' => [
                    'pdf' => Url::to(['report/task-report'])
                ],
                'name' => StringHelper::translateOrderToOffer('web', 'Tasks'),
                'text' => StringHelper::translateOrderToOffer('web', 'Report on tasks'),
                'actionList' => in_array(DocumentType::TASK_REPORT, $availableDocuments) && $isUserMain ? 'show' : 'hide',
            ],
            [
                'url' => [
                    'pdf' => Url::to(['report/goods-issued-report'])
                ],
                'name' => StringHelper::translateOrderToOffer('web', 'Goods issued'),
                'text' => StringHelper::translateOrderToOffer('web', 'Report on goods issued'),
                'actionList' => in_array(DocumentType::GOODS_ISSUED_REPORT, $availableDocuments) && $isUserMain ?'show' : 'hide',
            ],
        	[
        		'url' => [
        				'pdf' => Url::to(['report/goods-delivered-report'])
        		],
        		'name' => StringHelper::translateOrderToOffer('web', 'Goods delivered'),
        		'text' => StringHelper::translateOrderToOffer('web', 'Report on goods delivered'),
        		'actionList' => in_array(DocumentType::GOODS_DELIVERED_REPORT, $availableDocuments) && $isUserMain ? 'show' : 'hide',
        	],
        	[
        		'url' => [
        				'pdf' => Url::to(['report/sales-result-report'])
        		],
        		'name' => StringHelper::translateOrderToOffer('web', 'Sales result'),
        		'text' => StringHelper::translateOrderToOffer('web', 'Report on sales result'),
        		'actionList' => in_array(DocumentType::SALES_RESULT_REPORT, $availableDocuments) && $isUserMain ? 'show' : 'hide',
    		],
            [
                'url' => [
                    'pdf' => Url::to(['report/list-of-items-report'])
                ],
                'name' => StringHelper::translateOrderToOffer('web', 'List of items'),
                'text' => StringHelper::translateOrderToOffer('web', 'List of items'),
                'actionList' => in_array(DocumentType::LIST_OF_ITEMS, $availableDocuments) && $isUserMain ? 'show' : 'hide',
            ],
            [
                'url' => [
                    'pdf' => Url::to(['report/lost-client-report'])
                ],
                'name' => StringHelper::translateOrderToOffer('web', 'Lost Clients'),
                'text' => StringHelper::translateOrderToOffer('web', 'Report on lost clients'),
                'actionList' => (Yii::$app->params['isDocumentStorageAvailable'] && in_array(DocumentType::LOST_CLIENT, $availableDocuments) && $isUserMain) ? 'show' : 'hide',
            ],
    		[
    			'url' => [
					'pdf' => Url::to(['report/reminder-report']) 
    			],
    			'name' => Yii::t('web', 'Reminder report'),
				'text' => StringHelper::translateOrderToOffer('web', 'Report with reminders to orders\' renewal'),
				'actionList' => (Yii::$app->params['isDocumentStorageAvailable'] && in_array(DocumentType::REMINDER_REPORT, $availableDocuments)) ? 'show' : 'hide',
    		],
            [
                'url' => [
                    'pdf' => Url::to(['report/intermediate-products-report'])
                ],
                'name' => Yii::t('web', 'Forecast of orders executability'),
                'text' => StringHelper::translateOrderToOffer('web', 'Report of demend on intermediate products'),
                'actionList' => (Yii::$app->params['isOfferedProductDependOnProduct'] && in_array(DocumentType::INTERMEDIATE_PRODUCTS, $availableDocuments)) ? 'show' : 'hide',
            ],
            [
                'url' => [
                    'pdf' => Url::to(['report/production-progress-report'])
                ],
                'name' => Yii::t('web', 'Production progress'),
                'text' => Yii::t('web', 'Report of progress and losses during production'),
                'actionList' => (Yii::$app->params['isProductionVisible'] && in_array(DocumentType::PRODUCTION_PROGRESS, $availableDocuments) && $isUserMain) ? 'show' : 'hide',
            ],
            [
                'url' => [
                    'pdf' => Url::to(['report/quality-control-report'])
                ],
                'name' => Yii::t('main', 'Quality control'),
                'text' => Yii::t('main', 'Quality control report'),
                'actionList' => (Yii::$app->params['isProductionVisible'] && in_array(DocumentType::QUALITY_CONTROL, $availableDocuments) && $isUserMain) ? 'show' : 'hide',
            ],
        ];
            
        return $this->render('index', [
            'model' => $model,
            'options' => $options,
            'reports' => $reports,
        ]);
    }

}
