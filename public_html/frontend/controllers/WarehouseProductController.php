<?php

namespace frontend\controllers;

use Yii;
use frontend\components\Controller;
use yii\filters\AccessControl;
use common\helpers\Utility;
use yii\data\Pagination;
use yii\helpers\Url;
use common\models\aq\WarehouseQuery;
use frontend\models\WarehouseProductListForm;
use common\models\aq\ProductCategoryQuery;
use common\models\aq\WarehouseProductQuery;
use common\models\ar\Warehouse;
use frontend\models\WarehouseProductForm;
use frontend\models\WarehouseProductItemForm;
use yii\web\Response;
use yii\widgets\ActiveForm;
use common\models\aq\ProductStatusQuery;
use common\models\ar\ProductStatus;
use common\models\aq\ProductStatusReferenceQuery;
use yii\helpers\ArrayHelper;
use common\models\aq\WarehouseProductStatusHistoryQuery;
use common\models\aq\WarehouseProductStatusHistoryDataQuery;
use frontend\models\WarehouseProductDetailsForm;
use frontend\models\ProductReservationListForm;
use common\models\ar\WarehouseProductStatusHistory;
use common\models\ar\ProductStatusReference;
use common\models\ar\WarehouseProduct;
use common\models\ar\WarehouseProductStatusHistoryData;
use yii\filters\VerbFilter;
use common\models\aq\ProductQuery;
use common\alerts\ProductMinimalCountAlertHandler;
use common\models\ar\WarehouseProductOperation;
use common\helpers\NumberTemplateHelper;
use common\documents\DocumentTypeUtility;

class WarehouseProductController extends Controller {

    public function behaviors() {
        $allActions = Yii::$app->params['isWarehouseVisible'] ? ['list', 'details', 'create', 'update', 'get-versions-count-box', 'get-item-form',
            'reservation-list', 'undo-reservation', 'issue-reservation',
                ] : [];

        $rules = !Yii::$app->params['isWarehouseVisible'] ?
                [
                [
                'allow' => false,
                'roles' => ['@'],
            ]
                ] :
                [
                [
                'actions' => ['list', 'details', 'create', 'update', 'get-versions-count-box', 'get-item-form',
                    'reservation-list', 'issue-reservation',],
                'allow' => true,
                'roles' => [Utility::ROLE_STOREKEEPER],
            ],
                [
                'actions' => ['undo-reservation'],
                'allow' => true,
                'roles' => [Utility::ROLE_SALESMAN, Utility::ROLE_STOREKEEPER],
            ]
        ];

        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => $allActions,
                'rules' => $rules,
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'undo-reservation' => ['post'],
                    'issue-reservation' => ['post'],
                ],
            ],
        ];
    }

    public function actionList() {
        $request = Yii::$app->request;
        $listForm = new WarehouseProductListForm();
        if(!$request->isAjax) {
            $categoryOptions = ProductCategoryQuery::getCategoryList();
            $listForm->allCategoriesCount = count($categoryOptions);

            $warehouseOptions = WarehouseQuery::getWarehouseList()->all();
            $listForm->allWarehousesCount = count($warehouseOptions);
        }
        $listForm->load($request->get());

        $itemsQuery = WarehouseProductQuery::getActiveProductsQuery($listForm->toArray());
        $pages = new Pagination([
            'totalCount' => $itemsQuery->count(),
            'page' => $listForm->page,
            'defaultPageSize' => \Yii::$app->params['defaultPageSize'],
        ]);
        $items = $itemsQuery->limit($pages->defaultPageSize)->offset($pages->page * $pages->defaultPageSize)->all();

        $listData = [
            'items' => $items,
            'pages' => $pages,
            'listForm' => $listForm,
        ];

        if($request->isAjax) {
            echo json_encode([
                'view' => $this->renderAjax('_products', $listData),
                'url' => Url::current([], true),
            ]);
        } else {
            $listData['categoryTranslations'] = $categoryOptions;
            $listData['warehouses'] = $warehouseOptions;
            return $this->render('list', $listData);
        }
    }

    public function actionDetails($productId) {
        $request = Yii::$app->request;
        $productDetails = WarehouseProductQuery::getActiveProductsQuery(['productId' => $productId])->limit(1)->one();
        if(empty($productDetails)) {
            throw new \yii\web\NotFoundHttpException(Yii::t('web', 'Element does not exist or you do not have permission.'));
        }
        $detailsForm = new WarehouseProductDetailsForm();
        if(!$request->isAjax) {
            $warehouseOptions = WarehouseQuery::getWarehouseList()->all();
            $detailsForm->allWarehousesCount = count($warehouseOptions);
        }
        $detailsForm->load($request->get());

        $itemsQuery = WarehouseProductStatusHistoryQuery::getStatusesQueryByProductId($productId, $detailsForm->toArray());
        $pages = new Pagination([
            'totalCount' => $itemsQuery->count(),
            'page' => $detailsForm->page,
            'defaultPageSize' => \Yii::$app->params['defaultPageSize'],
        ]);
        $items = $itemsQuery->limit($pages->defaultPageSize)->offset($pages->page * $pages->defaultPageSize)->all();
        $subversions = WarehouseProductQuery::getAllProductSubversionsInWarehouses($productId);

        $subversionsDim[] = count($subversions);
        if(count($subversions) != 0) {
            $subversionsDim[] = count($subversions[0]);
        }

        if(!empty($items)) {
            foreach($items as &$item) {
                if($item['countAdditionalData'] > 0) {
                    $item['additionalData'] = WarehouseProductStatusHistoryDataQuery::getDataByHistoryId($item['id']);
                }
            }
        }

        $listData = [
            	'items' => $items,
            	'pages' => $pages,
            	'listForm' => $detailsForm,
            	'productId' => $productId,
            	'subversions' => $subversions,
            	'subversionsDim' => $subversionsDim,
        		'availableDocuments' => DocumentTypeUtility::getAvailableDocuments(),
        ];

        if($request->isAjax) {
            echo json_encode([
                'view' => $this->renderAjax('_statuses', $listData),
                'url' => Url::current([], true),
            ]);
        } else {
            if(!empty($productId)) {
                $productDetails['attachments'] = ProductQuery::getAttachments($productId);
            }

            $listData['warehouses'] = $warehouseOptions;
            $listData['productDetails'] = $productDetails;

            return $this->render('details', $listData);
        }
    }

    public function actionCreate($productId = null) {
        $model = new WarehouseProductForm();
        $model->setAddScenario();

        if(Yii::$app->request->isAjax && $model->load(Yii::$app->request->post())) {
            Yii::$app->response->format = Response::FORMAT_JSON;
            $errors = ActiveForm::validate($model) + ActiveForm::validateMultiple($model->warehouseProductItemForms);
            return $model->translateErrorMessages($errors);
        }

        if($model->load(Yii::$app->request->post()) && $model->validate()) {
            try {
                $model->saveData();
                Yii::$app->getSession()->setFlash('success', Yii::t('web', 'Products has been added successfully to warehouse.'));

                return $this->redirect(['list']);
            } catch(\Exception $e) {
                Yii::$app->getSession()->setFlash('error', Yii::t('web', 'Products cannot be added to warehouse. Check provided values.'));
                Yii::error($e->getMessage());
                Yii::error($e->getTraceAsString());
            }
        }

        return $this->render('form', [
                    'model' => $model,
                    'product' => empty($productId) ? null : new WarehouseProductItemForm($productId),
        ]);
    }

    public function actionUpdate($productId = null, $productStatusId = null) {
        $model = new WarehouseProductForm();
        $model->setUpdateScenario();

        if(Yii::$app->request->isAjax && $model->load(Yii::$app->request->post())) {
            Yii::$app->response->format = Response::FORMAT_JSON;
            $errors = ActiveForm::validate($model) + ActiveForm::validateMultiple($model->warehouseProductItemForms);
            return $model->translateErrorMessages($errors);
        }

        if($model->load(Yii::$app->request->post()) && $model->validate()) {
            try {
                $model->saveData();
                Yii::$app->getSession()->setFlash('success', Yii::t('web', 'Products in warehouse have been updated successfully.'));

                return $this->redirect(['list']);
            } catch(\Exception $e) {
                Yii::$app->getSession()->setFlash('error', Yii::t('web', 'Products in warehouse cannot be updated. Check provided values.'));
                Yii::error($e->getMessage());
                Yii::error($e->getTraceAsString());
            }
        }

        $operationTypes = ProductStatusQuery::getStatusesByNotSymbols([Utility::PRODUCT_STATUS_ADDITION, Utility::PRODUCT_STATUS_UNDO_RESERVATION, Utility::PRODUCT_STATUS_ISSUING_RESERVATION]);
        $fields = ProductStatusReferenceQuery::getAdditionalFieldsByProductStatusIds(ArrayHelper::getColumn($operationTypes, 'id'));
        if(!empty($fields)) {
            $model->setAdditionalFields($fields);
        }

        $model->productStatusId = $productStatusId;

        return $this->render('form', [
                    'model' => $model,
                    'operationTypes' => $operationTypes,
                    'fields' => $fields,
                    'product' => empty($productId) ? null : new WarehouseProductItemForm($productId),
        ]);
    }

    public function actionGetVersionsCountBox() {
        // TODO: Sprobowac nie generowac HTML-a w tym miejscu, tylko na widoku lub JS-em
        if(!Yii::$app->request->isAjax) {
            return '';
        }

        $productId = Yii::$app->request->get('id');
        $warehouseIds = Yii::$app->request->get('warehouseIds');
        if(empty($warehouseIds)) {
            $warehouseIds = Warehouse::find()->select('id')->where(['is_active' => true])->column();
        }

        $data = WarehouseProductQuery::getVersionsCountByProductId($warehouseIds, $productId);
        if(empty($data)) {
            return Yii::t('web', 'No data about versions\'s count.');
        }

        $response = '<p class="versions-details-title">' . Yii::t('web', 'Particular versions count') . '</p>';
        $response .= '<p class="versions-details-body">';
        foreach($data as $product) {
            $response .= '<span class="product-name">' . $product['name'] . '</span>';
            $response .= '<span class="product-count pull-right">' . $product['count_available'] . (!empty($product['countMinimal']) ? ' (' . $product['countMinimal'] . ')' : '') . '</span></br>';
        }
        $response .= '</p>';

        return $response;
    }

    public function actionGetItemForm() {
        $request = Yii::$app->request;

        if($request->isAjax) {
            echo $this->renderAjax('inc/_item', [
                'warehouseProductItemForm' => new WarehouseProductItemForm($request->get('productId')),
                'prefix' => $request->get('prefix'),
                'formId' => $request->get('formId'),
            ]);
        }
    }

    public function actionReservationList() {
        $request = Yii::$app->request;
        $listForm = new ProductReservationListForm();

        $listForm->load($request->get());

        $itemsQuery = WarehouseProductQuery::getProductReservationsQuery($listForm->toArray());
        $pages = new Pagination([
            'totalCount' => $itemsQuery->count(),
            'page' => $listForm->page,
            'defaultPageSize' => \Yii::$app->params['defaultPageSize'],
        ]);
        $items = $itemsQuery->limit($pages->defaultPageSize)->offset($pages->page * $pages->defaultPageSize)->all();

        $listData = [
            'items' => $items,
            'pages' => $pages,
            'listForm' => $listForm,
        ];

        if($request->isAjax) {
            echo json_encode([
                'view' => $this->renderAjax('_reservedProducts', $listData),
                'url' => Url::current([], true),
            ]);
        } else {
            return $this->render('reservationList', $listData);
        }
    }

    public function actionUndoReservation() {
        $operationId = Yii::$app->request->post('operationId');
        $orderId = Yii::$app->request->post('orderId');

        $transaction = Yii::$app->db->beginTransaction();
        try {
            $reservation = WarehouseProductStatusHistory::findOne($operationId);

            $productStatus = ProductStatus::find()->where(['symbol' => Utility::PRODUCT_STATUS_UNDO_RESERVATION])->one();

            $operation = new WarehouseProductOperation();
            $operation->warehouse_id = $reservation->warehouseProduct->warehouse_id;
            $operation->product_status_id = $productStatus->id;
            $operation->user_confirming_id = Yii::$app->user->id;
            $operation->description = Yii::t('web', 'Cancellation of reservation') . ' #' . $operationId;
            $operation->save();

            $history = new WarehouseProductStatusHistory();
            $history->operation_id = $operation->id;
            $history->warehouse_product_id = $reservation->warehouse_product_id;
            $history->product_status_id = $productStatus->id;
            $history->user_confirming_id = Yii::$app->user->id;
            $history->count = $reservation->count;
            $history->description = Yii::t('web', 'Cancellation of reservation') . ' #' . $operationId;
            $history->history_blocked_id = $reservation->id;
            $history->save();

            $warehouseProduct = WarehouseProduct::findOne($reservation->warehouse_product_id);
            $warehouseProduct->count_available += $reservation->count;
            $warehouseProduct->save();

            $additionalFields = ProductStatusReference::find()->where(['product_status_id' => $productStatus->id])->all();
            if(!empty($additionalFields)) {
                foreach($additionalFields as $field) {
                    if($field->name_table == 'order') {
                        $historyData = new WarehouseProductStatusHistoryData();
                        $historyData->history_id = $history->id;
                        $historyData->product_status_reference_id = $field->id;
                        $historyData->referenced_object_id = $orderId;
                        $historyData->save();
                    }
                }
            }

            $transaction->commit();
        } catch(\Exception $e) {
            $transaction->rollBack();
            Yii::info(print_r($e->getMessage(), true), 'info');
            Yii::$app->getSession()->setFlash('error', Yii::t('web', 'The reservation of the selected product cannot be canceled. Please contact the administrator.'));
        }

        Yii::$app->getSession()->setFlash('success', Yii::t('web', 'The reservation has been cancelled.'));
        return $this->redirect(Yii::$app->request->getReferrer());
    }

    public function actionIssueReservation() {
        $operationId = Yii::$app->request->post('operationId');
        $orderId = Yii::$app->request->post('orderId');
        $flashDisplayed = false;

        $transaction = Yii::$app->db->beginTransaction();
        try {
            $reservation = WarehouseProductStatusHistory::findOne($operationId);
            $warehouseProduct = WarehouseProduct::findOne($reservation->warehouse_product_id);
            if($reservation->count > $warehouseProduct->count) {
                Yii::$app->getSession()->setFlash('error', Yii::t('web', 'There is no enough items of this product in a warehouse.'));
                $flashDisplayed = true;
                throw new \Exception('There is no enough items of this product in a warehouse.');
            }

            $productStatus = ProductStatus::find()->where(['symbol' => Utility::PRODUCT_STATUS_ISSUING_RESERVATION])->one();

            $operation = new WarehouseProductOperation();
            $operation->warehouse_id = $reservation->warehouseProduct->warehouse_id;
            $operation->product_status_id = $productStatus->id;
            $operation->user_confirming_id = Yii::$app->user->id;
            $operation->description = Yii::t('web', 'Issuing with reservation') . ' #' . $operationId;

            $number = NumberTemplateHelper::getAndSaveNextNumber('', Utility::NUMBER_TEMPLATE_GOODS_ISSUE);
            $operation->number = $number['number'];
            $operation->is_accounted = 0;

            $operation->save();

            $history = new WarehouseProductStatusHistory();
            $history->operation_id = $operation->id;
            $history->warehouse_product_id = $reservation->warehouse_product_id;
            $history->product_status_id = $productStatus->id;
            $history->user_confirming_id = Yii::$app->user->id;
            $history->count = $reservation->count;
            $history->description = Yii::t('web', 'Issuing with reservation') . ' #' . $operationId;
            $history->history_blocked_id = $reservation->id;
            $history->save();

            $warehouseProduct->count -= $reservation->count;
            $warehouseProduct->save();

            if(!empty($warehouseProduct->product->count_minimal) && $warehouseProduct->count < $warehouseProduct->product->count_minimal) {
                ProductMinimalCountAlertHandler::add(['productId' => $warehouseProduct->product_id, 'userId' => Yii::$app->user->id]);
            }

            $additionalFields = ProductStatusReference::find()->where(['product_status_id' => $productStatus->id])->all();
            if(!empty($additionalFields)) {
                foreach($additionalFields as $field) {
                    if($field->name_table == 'order') {
                        $historyData = new WarehouseProductStatusHistoryData();
                        $historyData->history_id = $history->id;
                        $historyData->product_status_reference_id = $field->id;
                        $historyData->referenced_object_id = $orderId;
                        $historyData->save();
                    }
                }
            }

            $transaction->commit();
            Yii::$app->getSession()->setFlash('success', Yii::t('web', 'The product has been issued with reservation.'));
        } catch(\Exception $e) {
            $transaction->rollBack();
            Yii::error(print_r($e->getMessage(), true));
            if(empty($flashDisplayed)) {
                Yii::$app->getSession()->setFlash('error', Yii::t('web', 'The selected product cannot be issued with reservation. Please contact the administrator.'));
            }
        }


        return $this->redirect(Yii::$app->request->getReferrer());
    }

}
