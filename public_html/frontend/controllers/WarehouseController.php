<?php

namespace frontend\controllers;

use Yii;
use frontend\components\Controller;
use yii\filters\AccessControl;
use common\helpers\Utility;
use common\models\aq\WarehouseQuery;
use frontend\models\WarehouseForm;
use yii\web\Response;
use yii\widgets\ActiveForm;
use common\models\ar\Warehouse;
use common\components\GetProvinceByZipCodeTrait;

class WarehouseController extends Controller {

    use GetProvinceByZipCodeTrait;

    public function behaviors() {

        $allActions = Yii::$app->params['isWarehouseVisible'] ? ['find-warehouse-groups', 'create', 'details', 'edit', 'index'] : [];

        $rules = !Yii::$app->params['isWarehouseVisible'] ? [
                [
                'allow' => false,
                'roles' => ['@'],
            ]
                ] :
                [
                [
                'actions' => ['find-warehouse-groups', 'edit', 'details'],
                'allow' => true,
                'roles' => [Utility::ROLE_MAIN_STOREKEEPER],
            ],
                [
                'actions' => ['index'],
                'allow' => true,
                'roles' => [Utility::ROLE_STOREKEEPER],
            ],
                [
                'actions' => ['create'],
                'allow' => false,
                'roles' => [Utility::ROLE_MAIN_STOREKEEPER],
            ],
        ];

        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => $allActions,
                'rules' => $rules,
            ],
        ];
    }

    public function actionFindWarehouseGroups() {
        $request = Yii::$app->request;
        $term = $request->get('term');
        $page = $request->get('page', 1);
        $size = $request->get('size');

        if(empty($term) || empty($page) || empty($size)) {
            Yii::$app->end();
        }

        // TODO: dlaczego "groups"?
        $query = WarehouseQuery::findWarehouseGroups(['name' => $term]);
        $count = $query->count();
        $data = $query->limit($size)->offset($size * ($page - 1))->all();

        echo json_encode([
            'items' => $data,
            'more' => (intval($count) > $page * $size)
        ]);
    }

    public function actionCreate() {
        $model = new WarehouseForm();
        $model->setAddScenario();

        if(Yii::$app->request->isAjax && $model->load(Yii::$app->request->post())) {
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ActiveForm::validate($model);
        }

        if($model->load(Yii::$app->request->post()) && $model->validate()) {
            try {
                $model->saveData();
                Yii::$app->getSession()->setFlash('success', Yii::t('web', 'Warehouse has been added successfully.'));

                return $this->redirect(['details', 'id' => $model->id]);
            } catch(\Exception $e) {
                Yii::$app->getSession()->setFlash('error', Yii::t('web', 'Warehouse cannot be saved. Check provided values.'));
                Yii::error($e->getMessage());
                Yii::error($e->getTraceAsString());
            }
        }

        return $this->render('form', [
                    'model' => $model,
        ]);
    }

    public function actionEdit($id = null) {
        $warehouse = null;
        if(!empty($id) && Yii::$app->params['isMoreWarehousesAvailable']) {
            $warehouse = Warehouse::findOne($id);
        } else {
            $warehouse = WarehouseQuery::getDefaultWarehouse()->one();
        }
        if(empty($warehouse)) {
            throw new \yii\web\NotFoundHttpException(Yii::t('web', 'Element does not exist or you do not have permission.'));
        }
        $model = new WarehouseForm($warehouse);
        $model->setEditScenario();

        if(Yii::$app->request->isAjax && $model->load(Yii::$app->request->post())) {
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ActiveForm::validate($model);
        }

        if($model->load(Yii::$app->request->post()) && $model->validate()) {
            try {
                $model->saveData();
                Yii::$app->getSession()->setFlash('success', Yii::t('web', 'Warehouse has been updated successfully.'));

                return $this->redirect(['warehouse/details', 'id' => $id]);
            } catch(\Exception $e) {
                Yii::$app->getSession()->setFlash('error', Yii::t('web', 'Warehouse cannot be saved. Check provided values.'));
                Yii::error($e->getMessage());
                Yii::error($e->getTraceAsString());
            }
        }

        return $this->render('form', [
                    'model' => $model,
        ]);
    }

    public function actionDetails($id = null) {
        $model = null;
        if(!empty($id) && Yii::$app->params['isMoreWarehousesAvailable']) {
            $model = Warehouse::findOne($id);
        } else {
            $model = WarehouseQuery::getDefaultWarehouse()->one();
        }

        if(!empty($model)) {
            return $this->render('details', [
                        'model' => $model,
            ]);
        } else {
            //Yii::$app->getSession()->setFlash('error', Yii::t('web', 'A warehouse does not exist or you do not have access rights.'));
            //return $this->redirect(['site/index']);
            throw new \yii\web\NotFoundHttpException(Yii::t('web', 'Element does not exist or you do not have permission.'));
        }
    }

    public function actionIndex() {
        // TODO: tymczasowe przekierowanie
        return $this->redirect(['warehouse-product/list']);
    }

}
