<?php
namespace frontend\controllers;

use Yii;
use frontend\components\Controller;
use yii\filters\AccessControl;
use common\helpers\Utility;
use common\models\aq\ProductCategoryQuery;
use frontend\models\ProductCategoryListForm;
use common\models\ar\ProductCategorySet;
use common\models\ar\ProductCategoryTranslation;
use yii\helpers\Url;
use yii\data\Pagination;
use frontend\models\ProductCategoryForm;
use yii\web\Response;
use yii\widgets\ActiveForm;
use common\models\ar\ProductCategory;
use common\models\ar\Language;

class ProductCategoryController extends Controller {

	public function behaviors() {
		return [
				'access' => [
						'class' => AccessControl::className(),
						'only' => ['list', 'create', 'edit', 'delete', 'find', 'details'],
						'rules' => [
								[
										'actions' => ['list', 'create', 'edit', 'delete', 'find', 'details'],
										'allow' => true,
										'roles' => [Utility::ROLE_STOREKEEPER],
								],
						],
				],
		];
	}


	public function actionList() {
		$request = Yii::$app->request;
		$productCategoryListForm = new ProductCategoryListForm();
		$productCategoryListForm->load($request->get());

		if(!$request->isAjax) {
			$categoryOptions = ProductCategoryQuery::getCategoryList();
			//$listForm->allCategoriesCount = count($categoryOptions);
		}

		$itemsQuery = ProductCategoryQuery::getActiveProductCategoryQuery($productCategoryListForm->toArray());
		$pages = new Pagination([
				'totalCount' => $itemsQuery->count(),
				'page' => $productCategoryListForm->page,
				'defaultPageSize' => \Yii::$app->params['defaultPageSize'],
		]);
		$items = $itemsQuery->limit($pages->defaultPageSize)->offset($pages->page * $pages->defaultPageSize)->all();

		$listData = [
				'productCategories' => $items,
				'pages' => $pages,
				'productCategoryListForm' => $productCategoryListForm,
		];

		if($request->isAjax) {
			echo json_encode([
					'view' => $this->renderAjax('_productCategories', $listData),
					'url' => Url::current([], true),
			]);
		}
		else {
			$listData['categoryTranslations'] = $categoryOptions;
			return $this->render('list', $listData);
		}
	}

	public function actionCreate() {
		$model = new ProductCategoryForm();
		$model->setAddScenario();

		if(Yii::$app->request->isAjax && $model->load(Yii::$app->request->post())) {
			Yii::$app->response->format = Response::FORMAT_JSON;
			return ActiveForm::validate($model);
		}

		if($model->load(Yii::$app->request->post()) && $model->validate()) {
			try {
				$model->saveData();
				Yii::$app->getSession()->setFlash('success', Yii::t('web', 'Product category has been added successfully.'));
				return $this->redirect(['details', 'id' => $model->id]);
			}
			catch(\Exception $e) {
				Yii::$app->getSession()->setFlash('error', Yii::t('web', 'Product category cannot be saved. Check provided values.'));
				Yii::error($e->getMessage());
				Yii::error($e->getTraceAsString());
			}
		}

		return $this->render('form', [
				'model' => $model,
		]);
	}

	public function actionEdit($id) {
		$productCategory = ProductCategory::findOne($id);
		if(empty($productCategory)) {
			throw new \yii\web\NotFoundHttpException(Yii::t('web', 'Element does not exist or you do not have permission.'));
		}
		$model = new ProductCategoryForm($productCategory);
		$model->setEditScenario();

		if(Yii::$app->request->isAjax && $model->load(Yii::$app->request->post())) {
			Yii::$app->response->format = Response::FORMAT_JSON;
			return ActiveForm::validate($model);
		}

		if($model->load(Yii::$app->request->post()) && $model->validate()) {
			try {
				$model->saveData();
				Yii::$app->getSession()->setFlash('success', Yii::t('web', 'Product category has been updated successfully.'));

				return $this->redirect(['details', 'id' => $model->id]);
			}
			catch(\Exception $e) {
				Yii::$app->getSession()->setFlash('error', Yii::t('web', 'Product category cannot be saved. Check provided values.'));
				Yii::error($e->getMessage());
				Yii::error($e->getTraceAsString());
			}
		}

		return $this->render('form', [
				'model' => $model,
		]);
	}

	public function actionDetails($id) {
		$data = ProductCategoryQuery::getDetails($id);
		if(empty($data)) {
			throw new \yii\web\NotFoundHttpException(Yii::t('web', 'Element does not exist or you do not have permission.'));
		}
		return $this->render('details', [
				'data' => $data,
		]);
	}

	public function actionDelete() {
		$id = Yii::$app->request->post('id');

		$transaction = Yii::$app->db->beginTransaction();
		try {

			$productCategory = ProductCategory::findOne($id);

			ProductCategoryTranslation::deleteAll(['product_category_id' => $id]);
			ProductCategorySet::deleteAll(['product_category_id' => $id]);

			$categories = ProductCategory::find()
					->where(['parent_category_id'=> $id])->all();

			foreach($categories as $category) {
				$category->parent_category_id = null;
				$category->save();
			}

			$productCategory->delete();
			$transaction->commit();

			Yii::$app->getSession()->setFlash('success', Yii::t('web', 'Product category has been deleted.'));
		}
		catch(\Exception $e) {
			Yii::info(print_r($e->getMessage(), true), 'info');
			Yii::$app->getSession()->setFlash('error', Yii::t('web', 'Product category cannot be deleted. Please contact the administrator.'));
		}

		if(strpos(Yii::$app->request->getReferrer(), Yii::$app->controller->id.'/details') !== false) {
			return $this->redirect(['list']);
		}
		return $this->redirect(Yii::$app->request->getReferrer());
	}

    public function actionFind() {
        $request = Yii::$app->request;
        $term = $request->get('term');
        $page = $request->get('page', 1);
        $size = $request->get('size');

		if(empty($term) || empty($page) || empty($size)) {
			Yii::$app->end();
		}

        $language = Language::find()->where(['symbol' => \Yii::$app->language])->one();
        $query = ProductCategoryTranslation::find()->andWhere(['like', 'name', $term])->andWhere(['language_id' => $language->id]);
        $matchedCategoriesCount = $query->count();
        $categories = $query->limit($size)->offset($size*($page-1))->all();

        echo json_encode([
            'items' => array_map(function($category) {
                return ['id' => $category->product_category_id, 'name' => $category->name];
            }, $categories),
            'more' => (intval($matchedCategoriesCount) > $page*$size)]);
        Yii::$app->end();
    }
}
