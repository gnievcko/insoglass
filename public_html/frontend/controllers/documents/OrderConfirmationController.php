<?php
namespace frontend\controllers\documents;

use Yii;
use yii\filters\AccessControl;
use common\documents\DocumentController;
use common\documents\DocumentType;
use common\helpers\Utility;
use common\documents\DocumentTypeUtility;
use common\repositories\sql\OfferDetailsRepository;
use common\models\ar\Order;
use common\documents\dataSources\OrderConfirmationDataSource;
use common\helpers\StringHelper;
use common\documents\forms\OrderConfirmationProductForm;
use common\documents\sections\OrderConfirmationProducts;

class OrderConfirmationController extends DocumentController {
	
	public function behaviors() {
		$actions = ['index', 'getOrderConfirmationProductRow', 'totalGrossPrice'];
		return [
				'access' => [
						'class' => AccessControl::className(),
						'only' => $actions,
						'rules' => [
								[
										'actions' => $actions,
										'allow' => true,
										'roles' => [Utility::ROLE_SALESMAN],
								],
						],
				],
		];
	}
	
	public function actionIndex($orderId = 0, $edit = false) {
		if(!in_array(DocumentType::ORDER_CONFIRMATION, DocumentTypeUtility::getAvailableDocuments())) {
			throw new \yii\web\NotFoundHttpException(Yii::t('web', 'Element does not exist or you do not have permission.'));
		}
		
		$order = Order::findOne($orderId);
		if(!empty($order) && !empty($order->is_deleted)) {
			throw new \yii\web\NotFoundHttpException(Yii::t('web', 'Element does not exist or you do not have permission.'));
		}
		
		$this->orderId = $orderId;
		$this->title = \Yii::t('documents', 'Order confirmation');
		$this->edit = $edit;
		
		$dataSource = new OrderConfirmationDataSource(new OfferDetailsRepository(), $orderId);
		
		return $this->createAndView(DocumentType::ORDER_CONFIRMATION, $dataSource);
	}
	
	public function actionGetOrderConfirmationProductRow($prefix) {
		$request = Yii::$app->request;
		return $this->renderFile('@common/documents/html/views/order-confirmation-products/product-row.php', [
				'product' => new OrderConfirmationProductForm(),
				'form' => new \yii\widgets\ActiveForm(),
				'prefix' => $prefix,
				'currencySymbol' => $request->get('currencySymbol'),
		]);
	}
	
	public function actionTotalGrossPrice($priceUnit, $count, $vatRate = 0) {
		\Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
		
		$netValue = bcmul((float)$priceUnit, (float)$count, 2);
		$grossValue = $netValue + bcmul($netValue, (float)$vatRate, 2);
		
		return ['totalGrossPrice' => StringHelper::getFormattedCost($grossValue)];
	}
	
	public function actionCalculateSummary() {
		$products = new OrderConfirmationProducts();
		$products->loadInput(Yii::$app->request->post());
		return $this->renderFile('@common/documents/html/views/order-confirmation-products/products-summary-body.php', [
				'products' => $products->products,
		]);
	}
}