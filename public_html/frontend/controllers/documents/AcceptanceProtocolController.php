<?php
namespace frontend\controllers\documents;

use Yii;
use yii\filters\AccessControl;
use common\helpers\Utility;
use common\documents\DocumentType;
use common\documents\DocumentController;
use common\documents\dataSources\AcceptanceProtocolDataSource;
use common\documents\DocumentTypeUtility;

class AcceptanceProtocolController extends DocumentController {
	
	public function behaviors() {
		return [
				'access' => [
						'class' => AccessControl::className(),
						'only' => ['index'],
						'rules' => [
								[
										'actions' => ['index'],
										'allow' => true,
										'roles' => [Utility::ROLE_SALESMAN],
								],
						],
				],
		];
	}

    public function actionIndex($orderId = 0, $edit = false) {
    	if(!in_array(DocumentType::ACCEPTANCE_PROTOCOL, DocumentTypeUtility::getAvailableDocuments())) {
    		throw new \yii\web\NotFoundHttpException(Yii::t('web', 'Element does not exist or you do not have permission.'));
    	}
    	
        $this->orderId = $orderId;
        $this->title = \Yii::t('documents', 'Work acceptance protocol');
        $this->edit = $edit;
    	$dataSource = new AcceptanceProtocolDataSource($orderId);
    	return $this->createAndView(DocumentType::ACCEPTANCE_PROTOCOL, $dataSource);
   	}
}
