<?php

namespace frontend\controllers;

use frontend\components\Controller;
use Yii;
use yii\filters\AccessControl;
use yii\data\Pagination;
use frontend\models\DocumentList;
use yii\helpers\Url;
use common\documents\DocumentTypeUtility;
use frontend\models\WarehouseDocumentListForm;
use frontend\models\OrderDocumentListForm;
use common\helpers\Utility;
use common\models\aq\UserQuery;
use common\models\aq\OrderQuery;
use common\models\aq\CompanyQuery;

class DocumentListController extends Controller {

    public function behaviors() {
        $allActions = ['order', 'warehouse'];

        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => $allActions,
                'rules' => [
                        [
		                        'actions' => ['order'],
		                        'allow' => Yii::$app->params['isDocumentStorageAvailable'],
		                        'roles' => [Utility::ROLE_SALESMAN],
	                    ],
                		[
		                		'actions' => ['warehouse'],
		                		'allow' => Yii::$app->params['isDocumentStorageAvailable'],
		                		'roles' => [Utility::ROLE_STOREKEEPER],
                		],
                ],
            ],
        ];
    }

    public function actionOrder() {
        $request = Yii::$app->request;
        $documentListForm = new OrderDocumentListForm();
        $documentListForm->load($request->get());
        $where = $this->prepareWhereConditionForOrderList($documentListForm);            
        $items = DocumentList::findAll(\Yii::$app->params['defaultPageSize'], $documentListForm->page, $documentListForm->sortDir, $documentListForm->sortField, $where);

        $pages = new Pagination([
            'totalCount' => DocumentList::count($where),
            'page' => $documentListForm->page,
            'defaultPageSize' => \Yii::$app->params['defaultPageSize'],
        ]);
        
        $listData = [
            'documentListForm' => $documentListForm,
            'pages' => $pages,
            'items' => $items,
            'documentTypeTranslations' => DocumentTypeUtility::translation(),
            'sortFields' => OrderDocumentListForm::getSortFields()
        ];
        
        if ($request->isAjax) {
            echo json_encode([
                'view' => $this->renderAjax('list', $listData),
                'url' => Url::current([], true),
            ]);
        } else {
            return $this->render('order', $listData);
        }
    }
    
    public function actionWarehouse() {
        $request = Yii::$app->request;
        $documentListForm = new WarehouseDocumentListForm();
        $documentListForm->load($request->get());
        $where = $this->prepareWhereConditionForWarehouseList($documentListForm);
        
        $items = DocumentList::findAll(
        		\Yii::$app->params['defaultPageSize'], $documentListForm->page, 
        		$documentListForm->sortDir, $documentListForm->sortField, $where        	
        );

        $pages = new Pagination([
            'totalCount' => DocumentList::count($where),
            'page' => $documentListForm->page,
            'defaultPageSize' => \Yii::$app->params['defaultPageSize'],
        ]);
        
        $listData = [
            'documentListForm' => $documentListForm,
            'pages' => $pages,
            'items' => $items,
            'documentTypeTranslations' => DocumentTypeUtility::translation(),
            'sortFields' => WarehouseDocumentListForm::getSortFields()
        ];
        
        if ($request->isAjax) {
            echo json_encode([
                'view' => $this->renderAjax('list', $listData),
                'url' => Url::current([], true),
            ]);
        } else {
            return $this->render('warehouse', $listData);
        }
    }
    
    private function prepareWhereConditionForOrderList($documentListForm) {
    	$where = ['and'];    	
    	
    	if(!empty($documentListForm->filterString)) {
    		$where[] = ['LIKE', 'history.sections.number', $documentListForm->filterString];
    	}
    	
    	if(!empty($documentListForm->clientName)) {
    		$clientIds = CompanyQuery::getByName($documentListForm->clientName);
    		$a = ['and'];
    		$a[] = ['OR',
    				['LIKE', 'history.sections.orderingParty', $documentListForm->clientName],
    				['LIKE', 'history.sections.clientCompany', $documentListForm->clientName],
    				['LIKE', 'history.sections.companyName', $documentListForm->clientName],
    				['LIKE', 'history.sections.object', $documentListForm->clientName],
    				['LIKE', 'history.sections.buyerName', $documentListForm->clientName],
    				['IN', 'history.sections.companyId', array_map(function($x) { return (int)$x; }, array_column($clientIds, 'id'))]
    		];
    		
    		$where[] = $a;
    	}
    	
    	if(!empty($documentListForm->address)) {
    		$a = ['and'];
    		$a[] = ['OR',
    				['LIKE', 'history.sections.companyAddress', $documentListForm->address],
    				['LIKE', 'history.sections.clientAddress', $documentListForm->address],
    				['LIKE', 'history.sections.buyerAddress', $documentListForm->address],
    		];
    		
    		$where[] = $a;
    	}
    	
    	if(!empty($documentListForm->author)) {
    		$users = UserQuery::getByFirstOrLastName($documentListForm->author);
    		$where[] = ['IN', 'history.authorId', !empty($users) ? array_map(function($x) { return (int)$x; }, array_column($users, 'id')) : []];
    	}
    	
    	if(!empty($documentListForm->orderNumber)) {
    		$orders = OrderQuery::getByNumberPart($documentListForm->orderNumber);
    		$where[] = ['IN', 'history.sections.orderId', !empty($orders) ? array_map(function($x) { return (int)$x; }, array_column($orders, 'id')) : []];
    	}
    	
    	if(!empty($documentListForm->parentClientName)) {
    		$companies = CompanyQuery::getWithChildrenByName($documentListForm->parentClientName);
    		$where[] = ['IN', 'history.sections.companyId', !empty($companies) ? array_map(function($x) { return (int)$x; }, array_column($companies, 'id')) : []];
    	}
    	
    	if(!empty($documentListForm->address)) {
    		$a = ['and'];
    		$a[] = ['OR',
    				['LIKE', 'history.sections.companyAddress', $documentListForm->address],
    				['LIKE', 'history.sections.clientAddress', $documentListForm->address],
    				['LIKE', 'history.sections.buyerAddress', $documentListForm->address],
    		];
    		
    		$where[] = $a;
    	}
    	
    	if(!empty($documentListForm->dateFrom)) {
    		$a = ['>=', 'history.sections.dateOfIssue', $documentListForm->dateFrom];
    		$where[] = $a;
    	}
    	
    	if(!empty($documentListForm->dateTo)) {
    		$a = ['<=', 'history.sections.dateOfIssue', $documentListForm->dateTo];
    		$where[] = $a;
    	}
    	
    	$a = ['IN', 'type', (!empty($documentListForm->type) ? $documentListForm->type : [])];
    	$where[] = $a;
    	
    	return $where;
    }
    
    private function prepareWhereConditionForWarehouseList($documentListForm) {
    	$where = [];
    	
    	if(!empty($documentListForm->filterString)) {
    		$where = ['and'];
    		$where[] = ['LIKE', 'history.sections.number', $documentListForm->filterString];
    	}
    	
    	if(!empty($documentListForm->employeeName)) {
    		$a = ['and'];
    		$a[] = ['OR',
    				['LIKE', 'history.sections.userReceivingName', $documentListForm->employeeName],
    				['LIKE', 'history.sections.userConfirmingName', $documentListForm->employeeName],
    				['LIKE', 'history.sections.userOrderingName', $documentListForm->employeeName],
    		];
    		if(empty($where)) {
    			$where = $a;
    		} else {
    			$where[] = $a;
    		}
    	}
    	
    	if(!empty($documentListForm->clientName)) {
    		$a = ['and'];
    		$a[] = ['OR',
    				['LIKE', 'history.sections.goesTo', $documentListForm->clientName],
    				['LIKE', 'history.sections.object', $documentListForm->clientName],
    				['LIKE', 'history.sections.description', $documentListForm->clientName],
    		];
    		if(empty($where)) {
    			$where = $a;
    		} else {
    			$where[] = $a;
    		}
    	}
    	
    	$a = ['IN', 'type', (!empty($documentListForm->type) ? $documentListForm->type : [])];
    	if(empty($where)) {
    		$where = $a;
    	} else {
    		$where[] = $a;
    	}
    	
    	return $where;
    }
}
