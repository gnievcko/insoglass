<?php

namespace frontend\controllers;

use Yii;
use frontend\components\Controller;
use common\models\aq\TaskTypeQuery;
use common\models\aq\AlertTypeQuery;
use common\models\aq\OrderStatusQuery;
use yii\helpers\Url;
use common\models\aq\AlertQuery;
use common\models\aq\TaskQuery;
use \DateTime;
use yii\filters\AccessControl;
use Mindseater\PHPUtils\Dates\Formatter;
use common\helpers\StringHelper;

class ChartController extends Controller {

	public function behaviors() {
		$allActions = ['task-chart', 'alert-chart', 'offer-chart', 'line', 'bar'];

		return [
				'access' => [
						'class' => AccessControl::className(),
						'only' => $allActions,
						'rules' => [
								[
										'actions' => $allActions,
										'allow' => true, // zmienić na true, gdy będzie potrzebne
										'roles' => ['@'],
								],
						],
				],
		];
	}

	public function actionTaskChart() {
		$data = TaskTypeQuery::getTaskTypeAndTaskCountQuery();
		return $this->renderDoughnutChart($data, Yii::t('web', 'Number of tasks'), '{n,plural,=0{tasks} =1{task} other{tasks}}', 'glyphicon glyphicon-tasks');
	}

	public function actionAlertChart() {
		$data = AlertTypeQuery::getAlertTypeAndAlertCountQuery();
		return $this->renderDoughnutChart($data, Yii::t('web', 'Number of alerts'), '{n,plural,=0{problems} =1{problem} other{problems}}', 'glyphicon glyphicon-alert');
	}

	public function actionOfferChart() {
		$data = OrderStatusQuery::getOrderStatusAndOrderCountQuery();
		return $this->renderDoughnutChart($data, Yii::t('web', 'Current offers'), '{n,plural,=0{offers} =1{offer} other{offers}}', 'glyphicon glyphicon-file');
	}

	public function renderDoughnutChart($data, $title, $legendText, $icon) {
		//TODO:: jeśli chce się tooltipy, trzeba je dodac do aq z aliasem 'tooltipText'

		return $this->render('piechart', [
				'data' => $data,
				'legendText' => $legendText,
				'icon' => $icon,
				'title' => $title,
		]);

	}

	public function actionLine(){
		$queryData = [];
		$data = array();		
     
        $next = Yii::$app->request->get('next') ?: 0;
        
        $type = Yii::$app->request->get('type') ?: 1;
        $data['xAxis'] = $this->getLabels($type);
        
        $this->getDate($type, $begin, $end);
        $this->moveDate($type, $next, $begin, $end);
        
        $queryData = AlertQuery::getAlertPriorityCountInGivenInterval($begin, $end);

		$data['data'] = $queryData[1];
		$data['labels'] = $queryData[0];
		$data['div'] = !empty(Yii::$app->request->get('div')) ? Yii::$app->request->get('div') : '.container';
		$data['chartId'] = !empty(Yii::$app->request->get('chartId')) ? Yii::$app->request->get('chartId') : 'line1';
		$data['urlAjax'] = Url::to(['chart/line']);
		$data['type'] = $type;
		$data['title'] = Yii::t('web', 'Alert number in time');
        
        $data['begin'] = $this->formatDateToDisplay($begin);
        $data['end'] = $this->formatDateToDisplay($end);
        
		if(Yii::$app->request->isAjax) {
			return $this->renderAjax('line', $data);
		}
		else {
			return $this->render('line', $data);
		}
	}

	public function actionBar(){
		$data = array();
        
        $next = Yii::$app->request->get('next') ?: 0;
        
        $type = Yii::$app->request->get('type') ?: 1;
        $data['xAxis'] = $this->getLabels($type);
        
        $this->getDate($type, $begin, $end);
        $this->moveDate($type, $next, $begin, $end);
        
        $data['data'] = TaskQuery::getFinishedTaskaCountInGivenInterval($begin, $end);       
		$data['labels'] = [Yii::t('web', 'Total price')];
        $data['title'] = StringHelper::translateOrderToOffer('web', 'Order value');
				
		$data['div'] = !empty(Yii::$app->request->get('div')) ? Yii::$app->request->get('div') : '.container';
		$data['chartId'] = !empty(Yii::$app->request->get('chartId')) ? Yii::$app->request->get('chartId') : 'line1';
		$data['urlAjax'] = Url::to(['chart/bar']);
		$data['type'] = !empty(Yii::$app->request->get('type')) ? Yii::$app->request->get('type') : 1;
        $data['type'] = $type;
        $data['begin'] = $this->formatDateToDisplay($begin);
        $data['end'] = $this->formatDateToDisplay($end);
        
		if(Yii::$app->request->isAjax){
			return $this->renderAjax('bar', $data);
		}
		else{
			return $this->render('bar', $data);
		}
	}
	
	public function actionDeadlineTaskCountBar(){
		$data = array();
	
		$next = Yii::$app->request->get('next') ?: 0;
	
		$type = Yii::$app->request->get('type') ?: 1;
		$data['xAxis'] = $this->getLabels($type);
	
		$this->getDate($type, $begin, $end);
		$this->moveDate($type, $next, $begin, $end);
	
		$data['data'] = TaskQuery::getTaskDeadlineCountInGivenInterval($begin, $end);
		$data['labels'] = [StringHelper::translateOrderToOffer('web', 'Number of orders')];
		$data['title'] = StringHelper::translateOrderToOffer('web', 'Orders deadlines');
	
		$data['div'] = !empty(Yii::$app->request->get('div')) ? Yii::$app->request->get('div') : '.container';
		$data['chartId'] = !empty(Yii::$app->request->get('chartId')) ? Yii::$app->request->get('chartId') : 'line1';
		$data['urlAjax'] = Url::to(['chart/bar']);
		$data['type'] = !empty(Yii::$app->request->get('type')) ? Yii::$app->request->get('type') : 1;
		$data['type'] = $type;
		$data['begin'] = $this->formatDateToDisplay($begin);
		$data['end'] = $this->formatDateToDisplay($end);
	
		if(Yii::$app->request->isAjax){
			return $this->renderAjax('bar', $data);
		}
		else{
			return $this->render('bar', $data);
		}
	}
	
    private function getLabels($type = 1) {
        switch($type) {
            case 1:
                return $data['xAxis'] = ['STY', 'LUT', 'MAR', 'KWI', 'MAJ', 'CZE', 'LIP', 'SIE', 'WRZ', 'PAZ', 'LIS', 'GRU'];
                break;
            case 2:
                $now = new DateTime('now');
                return $data['xAxis'] = range(1,cal_days_in_month(CAL_GREGORIAN, $now->format('m'), $now->format('Y')));
                break;
            case 3:
                return $data['xAxis'] = ['PON', 'WTO', 'SRD', 'CZW', 'PIA', 'SOB', 'NIE'];
                break;
        }
    }
    
    private function getDate($type, &$begin, &$end) {
        if($type == 1) {
            $begin = strtotime('Jan 1');
            $end = strtotime('Dec 31');
		}
		else if($type == 2) {
            $begin = strtotime('first day of this month');
            $end = strtotime('last day of this month');
		}
		else if($type == 3) {
            $begin = strtotime('monday this week');
            $end = strtotime('sunday this week');
		}
    }
    
    private function moveDate($type, $next, &$begin, &$end) {
        $interval = ['years', 'month', 'week'];
        
        $add = $next>0 ? '+'.$next : $next;
        
        $begin = strtotime($add.$interval[$type-1], $begin);
        $end = strtotime($add.$interval[$type-1], $end);
        
        $begin = date('Y-m-d', $begin);
        $end = date('Y-m-d', $end);
    }
    
    private function formatDateToDisplay($date) {
        $dateFormatter = new Formatter();
        $datetimeFormat = 'Y-m-d';
        $dateFormat = 'd.m.Y';

        return $dateFormatter->from($date, $datetimeFormat)->format($dateFormat)->getDate();
    }

}
