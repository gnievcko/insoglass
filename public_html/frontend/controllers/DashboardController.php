<?php

namespace frontend\controllers;

use Yii;
use frontend\components\Controller;
use yii\filters\AccessControl;

class DashboardController extends Controller{

	public function behaviors() {
		$allActions = ['index'];

		return [
				'access' => [
						'class' => AccessControl::className(),
						'only' => $allActions,
						'rules' => [
								[
										'actions' => $allActions,
										'allow' => true, // zmienić na true, gdy będzie potrzebne
										'roles' => ['@'],
								],
						],
				],
		];
	}

	public function actionIndex(){
		return $this->render('index', [
		]);
	}
}
