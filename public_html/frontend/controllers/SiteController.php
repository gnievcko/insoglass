<?php

namespace frontend\controllers;

use Yii;
use common\helpers\Utility;
use common\models\LoginForm;
use common\models\ar\Company;
use frontend\components\Controller;
use frontend\models\PasswordResetRequestForm;
use frontend\models\ResetPasswordForm;
use frontend\models\ContactForm;
use frontend\models\RegisterForm;
use yii\base\InvalidParamException;
use yii\web\BadRequestHttpException;
use yii\helpers\StringHelper;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use yii\base\InvalidValueException;
use frontend\models\ProductVariantForm;

/**
 * Site controller
 */
class SiteController extends Controller {
	
    /**
     * @inheritdoc
     */
    public function behaviors() {
        return [
	            'access' => [
		                'class' => AccessControl::className(),
		                'only' => ['logout', 'signup', 'maintenance', 'index', 'login', 'find-companies', 'contact',
		                		'request-password-reset', 'reset-password', 'get-product-variant-form'],
		                'rules' => [
			                    [
				                        'actions' => ['logout', 'find-companies', 'get-product-variant-form'],
				                        'allow' => true,
				                        'roles' => ['@'],
			                    ],
		                		[
				                		'actions' => ['login'],
				                		'allow' => true,
				                		'roles' => ['?'],
		                		],
		                		[
		                				'actions' => ['index', 'maintenance'],
		                				'allow' => true,
		                		],
		                		[
		                				'actions' => ['signup', 'contact', 'request-password-reset', 'reset-password'],
		                				'allow' => true,
		                		],
		                ],
	            ],
	            'verbs' => [
		                'class' => VerbFilter::className(),
		                'actions' => [
		                    	'logout' => ['post'],
		                ],
	            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function actions() {
        return [
            'error' => [
                	'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => $this->getCaptchaActionConfig(),
        ];
    }
    
    public function beforeAction($action) {
    	if(!parent::beforeAction($action)) {
    		return false;
    	}
    	
    	return true;
    }

    public function actionMaintenance() {
        if(!Yii::$app->params['maintenance']) {
            return $this->redirect(['dashboard/index']);
        }
    	return $this->render('maintenance');
    }
    
    /**
     * Displays homepage.
     *
     * @return mixed
     */
    public function actionIndex() {
    	if(!Yii::$app->user->isGuest) {
    		return $this->redirect(['dashboard/index']);
    	}
    	else {
            $this->layout = 'guest';
    		return $this->redirect(['site/login']);
    	}
    }
    
    /**
     * Logs in a user.
     *
     * @return mixed
     */
    public function actionLogin() {
    	if(!Yii::$app->user->isGuest) {
    		return $this->goHome();
    	}

        $this->layout = 'guest';
    
    	$model = new LoginForm();
    	
    	if(Yii::$app->request->isAjax && $model->load(Yii::$app->request->post())) {
    		Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
    		return \yii\widgets\ActiveForm::validate($model);
    	}

    	if($model->load(Yii::$app->request->post()) && $model->login()) {
    		if(StringHelper::startsWith(Yii::$app->getUser()->getReturnUrl(), Yii::$app->urlManager->getBaseUrl().'/admin/')) {
    			return $this->goHome();
    		}
    		
    		return $this->goBack();
    	}
    	else {
    		return $this->render('login', [
    				'model' => $model,
    		]);
    	}
    }
    
    /**
     * Logs out the current user.
     *
     * @return mixed
     */
    public function actionLogout() {
    	Yii::$app->user->logout();
    
    	return $this->goHome();
    }
    
    /**
     * Displays contact page.
     *
     * @return mixed
     */
    public function actionContact() {
    	$model = new ContactForm();
    	
    	if($model->load(Yii::$app->request->post()) && $model->validate()) {
    		
    		if($model->sendEmail(Yii::$app->params['supportEmail'])) {
    			Yii::$app->session->setFlash('warning', Yii::t('web', 'Thank you for contacting us. We will respond to you as soon as possible.'));
    		}
    		else {
    			Yii::$app->session->setFlash('error', Yii::t('web', 'There was an error sending email.'));
    		}
    
    		return $this->refresh();
    	}
    	else {
    		$this->regenerateCaptcha();
    		
    		return $this->render('contact', [
    				'model' => $model,
    		]);
    	}
    }
    
    /**
     * Signs user up.
     *
     * @return mixed
     */
    public function actionSignup() {
    	$model = new RegisterForm();    	
    	
    	if(Yii::$app->request->isAjax && $model->load(Yii::$app->request->post())) {
    		Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
    		return \yii\widgets\ActiveForm::validate($model);
    	}
    	
    	if($model->load(Yii::$app->request->post())) {
    		if($model->register()) {
	    		return $this->redirect(['site/login']);
    		}
    	}    	
    	
    	$this->regenerateCaptcha();
    
    	return $this->render('signup', [
    			'model' => $model,
    	]);
    }
    
    /**
     * Requests password reset.
     *
     * @return mixed
     */
    public function actionRequestPasswordReset() {
    	$this->layout = 'guest';
    	$model = new PasswordResetRequestForm();
    	if($model->load(Yii::$app->request->post()) && $model->validate()) {
    		if($model->sendToken()) {
    			if($model->isEmailMode()) {
    				Yii::$app->session->setFlash('warning', Yii::t('web', 'Check your email for further instructions.'));
    			}
    			
    			return $this->goHome();
    		}
    		else {
    			Yii::$app->session->setFlash('error', Yii::t('web', 'Sorry, we are unable to reset password for email provided.'));
    		}
    	}
    
    	$this->regenerateCaptcha();
    	
    	return $this->render('requestPasswordResetToken', [
    			'model' => $model,
    	]);
    }    
    
    /**
     * Resets password.
     *
     * @param string $token
     * @return mixed
     * @throws BadRequestHttpException
     */
    public function actionResetPassword($token) {
    	$this->layout = 'guest';
    	try {
    		$model = new ResetPasswordForm($token);
    	}
    	catch(InvalidParamException $e) {
    		Yii::$app->getSession()->setFlash('error', $e->getMessage());
    		
    		return $this->goHome();
    	}
    	catch(InvalidValueException $e2) {
    		Yii::$app->getSession()->setFlash('error', $e2->getMessage());
    		
    		return $this->goHome();
    	}
    
    	if($model->load(Yii::$app->request->post()) && $model->validate() && $model->resetPassword()) {
    		Yii::$app->session->setFlash('warning', Yii::t('web', 'New password was saved.'));
    
    		return $this->goHome();
    	}
    
    	return $this->render('resetPassword', [
    			'model' => $model,
    			]);
    }

    public function actionFindCompanies() {
        $request = Yii::$app->request;
        $term = $request->get('term');
        $page = $request->get('page', 1);
        $size = $request->get('size');

		if(empty($term) || empty($page) || empty($size)) {
			Yii::$app->end();
		}

        $query = Company::find()->andWhere(['like', 'name', $term]);
        $matchedCompaniesCount = $query->count();
        $companies = $query->limit($size)->offset($size*($page-1))->all();

        echo json_encode([
            'items' => array_map(function($company) {
                return ['id' => $company->id, 'name' => '[' . $company->id . '] '. $company->name]; 
            }, $companies), 
            'more' => (intval($matchedCompaniesCount) > $page*$size)]);
        Yii::$app->end();
    }

    public function actionGetProductVariantForm() {
        $request = \Yii::$app->request;
                
        if($request->isAjax) {
            echo $this->renderAjax('/inc/product_variant', [
                'productVariantForm' => new ProductVariantForm(),
                'prefix' => $request->get('prefix'),
                'formId' => $request->get('formId'),
            	'isWarehouse' => !strstr($request->getReferrer(), Utility::PRODUCT_TYPE_OFFERED_PRODUCT),
            ]);
        }
    }
}
