<?php
namespace frontend\controllers;

use Yii;
use yii\data\Pagination;
use yii\filters\AccessControl;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use yii\web\Response;
use yii\widgets\ActiveForm;

use frontend\components\Controller;
use frontend\logic\ProductAttributeTypeLogic;
use frontend\models\DepartmentOrdersListForm;
use frontend\models\DepartmentShortOrdersListForm;
use frontend\models\DepartmentProductOrderListForm;
use frontend\models\ProductionAddItemsForm;
use common\models\aq\DepartmentQuery;
use common\models\aq\OrderQuery;
use common\models\aq\OrderOfferedProductAttachmentQuery;
use common\models\aq\OrderOfferedProductAttributeQuery;
use common\models\aq\ProductionTaskQuery;
use common\models\aq\UserOrderSalesmanQuery;
use common\models\aq\WorkstationQuery;
use common\helpers\Utility;
use frontend\models\OrderPriorityForm;
use common\models\aq\OrderPriorityQuery;
use common\models\ar\OrderOfferedProduct;
use yii\helpers\Json;
use frontend\logic\QaLogic;

class ProductionController extends Controller {
    
    public function behaviors() {
        
        $allActions = ['index', 'department-orders', 'order-details', 'add-items', 'production-details',
            'short-department-orders', 'production-order-details', 'get-order-priority',
        ];
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => $allActions,
                'rules' => [
                    [
                        'actions' => $allActions,
                        'allow' => Yii::$app->params['isProductionVisible'],
                        'roles' => [Utility::ROLE_WORKER, Utility::ROLE_QA_SPECIALIST],
                    ],
                ],
            ],
        ];
    }
    
    public function actionIndex() {
        $delayCount = OrderQuery::getDelayedOrders();
        return $this->render('index', ['delayCount' => $delayCount['count']]);
    }
    
    public function actionDepartmentOrders() {
        $request = Yii::$app->request;
        $orderPriorityModel = new OrderPriorityForm();
        
        if(Yii::$app->request->isAjax && $orderPriorityModel->load($request->post())) {
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ActiveForm::validate($orderPriorityModel);
        }
        
        if($orderPriorityModel->load($request->post()) && $orderPriorityModel->validate()) {
            try {
                $orderPriorityModel->saveData();
                Yii::$app->getSession()->setFlash('success', Yii::t('web', 'Priority has been changed.'));
            }
            catch(\Exception $e) {
                Yii::$app->getSession()->setFlash('error', Yii::t('web', 'Priority cannot be changed.'));
                Yii::error($e->getMessage());
                Yii::error($e->getTraceAsString());
            }
        }
        
        $listForm = new DepartmentOrdersListForm();
        $listForm->load($request->get());
 
        if(!$request->isAjax) {
            $departments = ArrayHelper::map(DepartmentQuery::getDepartmentsByIds(), 'id', 'name');
            if(empty($departments)) {
                throw new \yii\web\NotFoundHttpException(Yii::t('web', 'Element does not exist or you do not have permission.'));
            }
        }
   
        $ongoingProductsQuery = ProductionTaskQuery::getOngoingProductedProducts($listForm->toArray());
        
        $pages = new Pagination([
            'totalCount' => $ongoingProductsQuery->count(),
            'page' => $listForm->page,
            'defaultPageSize' => \Yii::$app->params['defaultPageSize'],
        ]);
        $items = $ongoingProductsQuery->limit($pages->defaultPageSize)->offset($pages->page * $pages->defaultPageSize)->all();
        
        $qaAssignmentAvailableProducts = (new QaLogic())->getProductsWithQaFactorAssignmentAvailable(Yii::$app->user);
        
        $listData = [
            'items' => $items,
            'pages' => $pages,
            'listForm' => $listForm,
            'qaAssignmentAvailableProducts' => $qaAssignmentAvailableProducts,
        ];
        
        if($request->isAjax) {
            echo json_encode([
                'view' => $this->renderAjax('_orders', $listData),
                'url' => Url::current([], true),
            ]);
        }
        else {
            $orderPriorities = ArrayHelper::map(OrderPriorityQuery::getAll(), 'id', 'name');
            
            $listData['departments'] = $departments;
            $listData['orderPriorityModel'] = $orderPriorityModel;
            $listData['orderPriorities'] = $orderPriorities;
            return $this->render('departmentOrders', $listData);
        }
    }
    
    public function actionProductionDetails($orderOfferedProductId) {        
        $detailsData = ProductionTaskQuery::getDetailsForProductionDetails($orderOfferedProductId);

        if(empty($detailsData)) {
            throw new \yii\web\NotFoundHttpException(Yii::t('web', 'Element does not exist or you do not have permission.'));
        }
        
        $listData = ProductionTaskQuery::getTableForProductionDetails($orderOfferedProductId);
        
        $departmentId = $detailsData['departmentId'];
        $workstations = ArrayHelper::map(WorkstationQuery::getWorkstationsByDepartmentId($departmentId), 'id', 'name');
        
        $addItemsForm = new ProductionAddItemsForm();  
        
        $qaAssignmentAvailableProducts = (new QaLogic())->getProductsWithQaFactorAssignmentAvailable(Yii::$app->user);
        $qaAssignmentProductionTaskId = null;
        if(!empty($qaAssignmentAvailableProducts) && array_key_exists($orderOfferedProductId, $qaAssignmentAvailableProducts)) {
            $qaAssignmentProductionTaskId = $qaAssignmentAvailableProducts[$orderOfferedProductId]['productionTaskId'];
        }
        
        return $this->render('productionDetails', [
            'detailsData' => $detailsData,
            'listData' => $listData,
            'workstations' => $workstations,
            'addItemsForm' => $addItemsForm,
            'qaAssignmentProductionTaskId' => $qaAssignmentProductionTaskId,
        ]);
    }
    
    public function actionProductionOrderDetails($orderId) {        
        $request = Yii::$app->request;
        $listForm = new DepartmentProductOrderListForm();
        $listForm->load($request->get());
        
        $products = ProductionTaskQuery::getOngoingProductedProducts($listForm->toArray() + ['forShortList' => true, 'orderId' => $orderId]);

        if(empty($products)) {
            throw new \yii\web\NotFoundHttpException(Yii::t('web', 'Element does not exist or you do not have permission.'));
        }
        
        $pages = new Pagination([
            'totalCount' => $products->count(),
            'page' => $listForm->page,
            'defaultPageSize' => PHP_INT_MAX,
        ]);

        $items = $products->limit($pages->defaultPageSize)->offset($pages->page * $pages->defaultPageSize)->all();
        
        $qaAssignmentAvailableProducts = (new QaLogic())->getProductsWithQaFactorAssignmentAvailable(Yii::$app->user);
        
        $listData = [
            'items' => $items,
            'pages' => $pages,
            'listForm' => $listForm,
            'qaAssignmentAvailableProducts' => $qaAssignmentAvailableProducts,
        ];
        if($request->isAjax) {
            echo json_encode([
                'view' => $this->renderAjax('_shortProducts', $listData),
                'url' => Url::current([], true),
            ]);
        } else {
            return $this->render('departmentOrderProducts', $listData);
        }
    }
    
    public function actionAddItems($productionTaskId) {   
        $request = Yii::$app->request;
        $addItemsForm = new ProductionAddItemsForm($productionTaskId);
        
        if($request->isAjax && $addItemsForm->load($request->post())) {
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ActiveForm::validate($addItemsForm);
        }
        
        if($addItemsForm->load($request->post()) && $addItemsForm->validate()) {
            $transaction = Yii::$app->db->beginTransaction();
            try {
                $addItemsForm->save();
                $transaction->commit();
                Yii::$app->getSession()->setFlash('success', Yii::t('web', 'A number of completed products has been updated.'));
            } catch(\Exception $e) {
                $transaction->rollBack();
                Yii::error($e->getMessage());
                Yii::error($e->getTraceAsString());
                Yii::$app->getSession()->setFlash('error', Yii::t('web', 'An error occured while updating number of completed products.'));
            }
            
        }
        return $this->redirect(Yii::$app->request->getReferrer());
    }
    
    
    public function actionOrderDetails($id) {
        $data = OrderQuery::getDetailsForProductionOrderDetails($id);
        
        if(empty($data)) {
            throw new \yii\web\NotFoundHttpException(Yii::t('web', 'Element does not exist or you do not have permission.'));
        }
        
        $salesmen = UserOrderSalesmanQuery::getSalesmenByOrderId($id);
        $products = OrderQuery::getProductsForProductionOrderDetails($id);
        $attachments = OrderQuery::getAttachments($id);
        
        foreach($products as &$product) {
            $product['attributes'] =  (new ProductAttributeTypeLogic())->getAttributeGroupsWithValues(
                    OrderOfferedProductAttributeQuery::getProductAttributes($product['id'], false)
            );
            
            $product['attachments'] = OrderOfferedProductAttachmentQuery::getAttachmentsByOrderOfferedProductId($product['id']);
        }
            
        return $this->render('orderDetails', [
            'data' => $data,
            'salesmen' => $salesmen,
            'products' => $products,
            'attachments' => $attachments,
        ]);
               
    } 
    
    public function actionShortDepartmentOrders() { 
        $request = Yii::$app->request;
        $listForm = new DepartmentShortOrdersListForm();
        $listForm->load($request->get());
        
        $ongoingOrdersQuery = ProductionTaskQuery::getOngoingOrders($listForm->toArray());

        $pages = new Pagination([
            'totalCount' => $ongoingOrdersQuery->count(),
            'page' => $listForm->page,
            'defaultPageSize' => PHP_INT_MAX,
        ]);
        $items = $ongoingOrdersQuery->limit($pages->defaultPageSize)->offset($pages->page * $pages->defaultPageSize)->all();
        
        foreach($items as &$item) {
            $item['completionPercent'] = ProductionTaskQuery::getOrderProductsCompletionPercentage($item['id']);
        }
        $listData = [
            'items' => $items,
            'pages' => $pages,
            'listForm' => $listForm,
        ];
        
        if($request->isAjax && !Yii::$app->request->get('dashboard')) {
            echo json_encode([
                'view' => $this->renderAjax('_shortOrders', $listData),
                'url' => Url::current([], true),
            ]);
        }
        else {
            if(Yii::$app->request->get('dashboard')) {
                return $this->renderAjax('departmentOrdersShort', $listData);
            }
        }
    }
    
    public function actionGetOrderPriority() {
        try {
            $orderOfferedProductId = Yii::$app->request->get('orderOfferedProductId');
            $orderOfferedProduct = OrderOfferedProduct::findOne($orderOfferedProductId);
            $orderPriority = $orderOfferedProduct->order->orderPriority;
            
            echo Json::encode([
                'orderId' => $orderOfferedProduct->order_id,
                'orderPriorityId' => $orderOfferedProduct->order->order_priority_id,
                'orderPriorityCheckbox' => !empty($orderPriority) && $orderPriority->symbol == Utility::ORDER_PRIORITY_HIGH ? 1 : 0,
            ]);
        }
        catch(\Exception $e) {
            Yii::error($e->getMessage());
            Yii::error($e->getTraceAsString());
        }
    }
}
