<?php

namespace frontend\controllers;

use Yii;
use frontend\components\Controller;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use common\helpers\Utility;
use common\models\aq\AlertTypeQuery;
use yii\helpers\ArrayHelper;
use common\models\aq\AlertQuery;
use frontend\models\AlertListForm;
use yii\data\Pagination;
use yii\helpers\Url;
use common\models\ar\Alert;
use frontend\helpers\UserHelper;
use frontend\helpers\AdditionalFieldHelper;

class AlertController extends Controller {
	
	public function behaviors() {
		$allActions = ['short-list', 'get-details'];
	
		return [
				'access' => [
						'class' => AccessControl::className(),
						'only' => $allActions,
						'rules' => [
								[
										'actions' => $allActions,
										'allow' => true, 
										'roles' => [Utility::ROLE_SALESMAN, Utility::ROLE_STOREKEEPER],
								],
						],
				],
				'verbs' => [
						'class' => VerbFilter::className(),
						'actions' => [
						],
				],
		];
	}
	
	public function actionShortList() {
		$request = Yii::$app->request;
		$listForm = new AlertListForm();
		$listForm->load($request->get());
	
		$itemsQuery = AlertQuery::getActiveAlertsQuery(Yii::$app->user->id, $listForm->toArray());
		$pages = new Pagination([
				'totalCount' => $itemsQuery->count(),
				'page' => $listForm->page,
				'defaultPageSize' => 10,
		]);
		$items = $itemsQuery->limit(10)->offset(0)->all();
	
		$listData = [
				'items' => $items,
				'pages' => $pages,
				'listForm' => $listForm,
		];
	
		if($request->isAjax && !Yii::$app->request->get('dashboard')) {
			return json_encode([
					'view' => $this->renderAjax('_alerts', $listData),
					'url' => Url::current([], true),
			]);
		}
		$listData['alertTypes'] = ArrayHelper::map(AlertTypeQuery::getAllTypes(), 'id', 'name');
		$listData['alertTypes'] = [ 0 => Yii::t('web', 'All')] + $listData['alertTypes'];
		if(Yii::$app->request->get('dashboard')) {
			return $this->renderAjax('shortList', $listData);
		}
		else {
			return $this->render('shortList', $listData);
		}
	}
	
	public function actionGetDetails($id) {
		// TODO: Sprobowac nie generowac HTML-a w tym miejscu, tylko na widoku lub JS-em
		if(!Yii::$app->request->isAjax) {
			return '';
		}
	
		$alertId = Yii::$app->request->get('id');
		$data = AlertQuery::getDetails($alertId);
		if(empty($data)) {
			return Yii::t('web', 'No detailed data about alert.');
		}
	
		$response = '<p class="alert-details-title">'.Yii::t('web', 'Alert details').'</p>';
		$response .= '<p class="alert-details-body">';
	
		$response .= '<span class="alert-header">'.Yii::t('main', 'Title').':</span> '.$data['message'];
		$response .= '<br/><span class="alert-header">'.Yii::t('main', 'Priority').':</span> '.$data['priority'];
		$response .= '<br/><span class="alert-header">'.Yii::t('main', 'Author').':</span> '.UserHelper::getPrettyUserName($data['email'], $data['firstName'], $data['lastName']);
		$response .= '<br/><span class="alert-header">'.Yii::t('main', 'Creation date').':</span> '.$data['dateCreation'];

		if(!empty($data['results'])) {
			$response .= '<br/><span class="alert-header">'.Yii::t('web', 'Additional data').':</span><br/>'.AdditionalFieldHelper::getFormattedData($data['results']);
		}
	
		$response .= '</p>';
	
		return $response;
	}
}
