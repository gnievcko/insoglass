<?php
namespace frontend\controllers;

use Yii;
use frontend\components\Controller;
use yii\filters\AccessControl;
use common\helpers\Utility;
use yii\helpers\Url;
use yii\data\Pagination;
use yii\web\Response;
use yii\widgets\ActiveForm;
use common\models\aq\OrderTypeQuery;
use frontend\models\OrderTypeListForm;
use frontend\models\OrderTypeForm;
use common\helpers\StringHelper;
use common\models\ar\OrderType;
use common\models\ar\OrderTypeTranslation;
use common\models\ar\OrderTypeSet;

class OrderTypeController extends Controller {

	public function behaviors() {
		return [
				'access' => [
						'class' => AccessControl::className(),
						'only' => ['list', 'create', 'edit', 'delete', 'find', 'details'],
						'rules' => [
								[
										'actions' => ['list', 'create', 'edit', 'delete', 'find', 'details'],
										'allow' => true && Yii::$app->params['isOrderTypeVisible'],
										'roles' => [Utility::ROLE_SALESMAN],
								],
						],
				],
		];
	}

	public function actionList() {
		$request = Yii::$app->request;
		$listForm = new OrderTypeListForm();
		$listForm->load($request->get());
	
		$activeQuery = OrderTypeQuery::getActiveOrderTypesQuery($listForm->toArray());
		$pages = new Pagination([
				'totalCount' => $activeQuery->count(),
				'page' => $listForm->page,
				'defaultPageSize' => \Yii::$app->params['defaultPageSize'],
		]);
		$data = $activeQuery->limit($pages->defaultPageSize)->offset($pages->page * $pages->defaultPageSize)->all();
	
		$listData = [
				'data' => $data,
				'pages' => $pages,
				'listForm' => $listForm,
		];
	
		if ($request->isAjax) {
			echo json_encode([
					'view' => $this->renderAjax('_types', $listData),
					'url' => Url::current([], true),
			]);
		} 
		else {
			return $this->render('list', $listData);
		}
	}

	public function actionCreate() {
		$model = new OrderTypeForm();
		$model->setAddScenario();

		if(Yii::$app->request->isAjax && $model->load(Yii::$app->request->post())) {
			Yii::$app->response->format = Response::FORMAT_JSON;
			return ActiveForm::validate($model);
		}

		if($model->load(Yii::$app->request->post()) && $model->validate()) {
			try {
				$model->saveData();
				Yii::$app->getSession()->setFlash('success', StringHelper::translateOrderToOffer('web', 'Order type has been added successfully.'));
				return $this->redirect(['details', 'id' => $model->id]);
			}
			catch(\Exception $e) {
				Yii::$app->getSession()->setFlash('error', StringHelper::translateOrderToOffer('web', 'Order type cannot be saved. Check provided values.'));
				Yii::error($e->getMessage());
				Yii::error($e->getTraceAsString());
			}
		}

		return $this->render('form', [
				'model' => $model,
		]);
	}

	public function actionEdit($id) {
		$orderType = OrderType::findOne($id);
		if(empty($orderType)) {
			throw new \yii\web\NotFoundHttpException(Yii::t('web', 'Element does not exist or you do not have permission.'));
		}
		$model = new OrderTypeForm($orderType);
		$model->setEditScenario();

		if(Yii::$app->request->isAjax && $model->load(Yii::$app->request->post())) {
			Yii::$app->response->format = Response::FORMAT_JSON;
			return ActiveForm::validate($model);
		}

		if($model->load(Yii::$app->request->post()) && $model->validate()) {
			try {
				$model->saveData();
				Yii::$app->getSession()->setFlash('success', StringHelper::translateOrderToOffer('web', 'Order type has been updated successfully.'));

				return $this->redirect(['details', 'id' => $model->id]);
			}
			catch(\Exception $e) {
				Yii::$app->getSession()->setFlash('error', StringHelper::translateOrderToOffer('web', 'Order type cannot be saved. Check provided values.'));
				Yii::error($e->getMessage());
				Yii::error($e->getTraceAsString());
			}
		}

		return $this->render('form', [
				'model' => $model,
		]);
	}

	public function actionDetails($id) {
		$data = OrderTypeQuery::getDetails($id);
		if(empty($data)) {
			throw new \yii\web\NotFoundHttpException(Yii::t('web', 'Element does not exist or you do not have permission.'));
		}
		
		return $this->render('details', [
				'data' => $data,
		]);
	}

	public function actionDelete() {
		$id = Yii::$app->request->post('id');

		$transaction = Yii::$app->db->beginTransaction();
		try {
			$orderType = OrderType::findOne($id);

			OrderTypeTranslation::deleteAll(['order_type_id' => $id]);
			OrderTypeSet::deleteAll(['order_type_id' => $id]);

			$orderType->delete();
			$transaction->commit();

			Yii::$app->getSession()->setFlash('success', StringHelper::translateOrderToOffer('web', 'Order type has been deleted.'));
		}
		catch(\Exception $e) {
			Yii::info(print_r($e->getMessage(), true), 'info');
			Yii::$app->getSession()->setFlash('error', StringHelper::translateOrderToOffer('web', 'Order type cannot be deleted. Please contact the administrator.'));
		}

		if(strpos(Yii::$app->request->getReferrer(), Yii::$app->controller->id.'/details') !== false) {
			return $this->redirect(['list']);
		}
		return $this->redirect(Yii::$app->request->getReferrer());
	}

	public function actionFind() {
    	$request = Yii::$app->request;
    	$term = $request->get('term');
    	$page = $request->get('page', 1);
    	$size = $request->get('size');

    	if(empty($term) || empty($page) || empty($size)) {
    		Yii::$app->end();
    	}

    	$query = OrderTypeQuery::findOrderTypes(['name' => $term]);
    	$count = $query->count();
    	$data = $query->limit($size)->offset($size * ($page - 1))->all();

    	echo json_encode([
    			'items' => $data,
				'total' => count($data),
    			'more' => (intval($count) > $page * $size)
    	]);
    }
}
