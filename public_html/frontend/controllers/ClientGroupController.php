<?php

namespace frontend\controllers;

use Yii;
use frontend\components\Controller;
use yii\filters\AccessControl;
use yii\helpers\Url;
use yii\data\Pagination;
use common\helpers\Utility;
use yii\web\Response;
use yii\widgets\ActiveForm;
use yii\filters\VerbFilter;
use frontend\models\ClientsGroupsListForm;
use frontend\models\ClientsGroupForm;
use common\models\aq\CompanyGroupQuery;
use common\models\aq\CompanyGroupSetQuery;
use common\models\ar\CompanyGroup;
use common\models\ar\CompanyGroupTranslation;
use common\models\ar\CompanyGroupSet;

class ClientGroupController extends Controller {

	public function behaviors() {
		$allActions = ['list', 'details', 'edit', 'delete', 'create'];

		return [
				'access' => [
						'class' => AccessControl::className(),
						'only' => $allActions,
						'rules' => [
								[
										'actions' => ['details', 'edit', 'delete', 'create'],
										'allow' => true, 
										'roles' => [Utility::ROLE_MAIN_SALESMAN],
								],
								[
										'actions' => ['list'],
										'allow' => true,
										'roles' => [Utility::ROLE_SALESMAN],
								],
						],
				],
				'verbs' => [
						'class' => VerbFilter::className(),
						'actions' => [
								'delete' => ['post'],
						],
				],
		];
	}

    public function actionList() {
		$request = Yii::$app->request;
        $clientsGroupsForm = new ClientsGroupsListForm();
        $clientsGroupsForm->load($request->get());

        $customGroupsQuery = CompanyGroupQuery::findCompanyGroups($clientsGroupsForm->toArray());
        $pages = new Pagination([
	            'totalCount' => $customGroupsQuery->count(),
	            'page' => $clientsGroupsForm->page,
	            'defaultPageSize' => \Yii::$app->params['defaultPageSize'],
        ]);
        $groups = $customGroupsQuery->limit($pages->defaultPageSize)->offset($pages->page * $pages->defaultPageSize)->all();

        $groupsListData = [
	            'groups' => $groups,
	            'pages' => $pages,
	            'clientsGroupsForm' => $clientsGroupsForm,
        ];

        if($request->isAjax) {
            echo json_encode([
	                'view' => $this->renderAjax('_groups', $groupsListData),
	                'url' => Url::current([], true),
            ]);
        }
        else {
            return $this->render('list', $groupsListData);
        }
    }

	public function actionDelete() {
		$id = Yii::$app->request->post('id');

		try {
            CompanyGroupSet::deleteAll(['company_group_id' => $id]);
            CompanyGroupTranslation::deleteAll(['company_group_id' => $id]);
            CompanyGroup::deleteAll(['id' => $id]);

			Yii::$app->getSession()->setFlash('success', Yii::t('web', 'Clients group has been deleted.'));
		}
		catch(\Exception $e) {
			Yii::error(print_r($e->getMessage(), true));
			Yii::error(print_r($e->getTraceAsString(), true));
			Yii::$app->getSession()->setFlash('error', Yii::t('web', 'Clients group cannot be deleted. Please contact the administrator.'));
		}

		if(strpos(Yii::$app->request->getReferrer(), Yii::$app->controller->id.'/details') !== false) {
			return $this->redirect(['list']);
		}
		return $this->redirect(Yii::$app->request->getReferrer());
	}

	public function actionDetails($id) {
		$groupDetails = CompanyGroupQuery::getDetails($id);
		if(empty($groupDetails)) {
			throw new \yii\web\NotFoundHttpException(Yii::t('web', 'Element does not exist or you do not have permission.'));
		}
		
		$clients = CompanyGroupSetQuery::getCompaniesByGroupId($id);
		
		return $this->render('details', [
				'groupDetails' => $groupDetails,
                'clients' => $clients,
		]);
	}

	public function actionCreate() {
		$model = new ClientsGroupForm();
		$model->setAddScenario();

		if(Yii::$app->request->isAjax && $model->load(Yii::$app->request->post())) {
			Yii::$app->response->format = Response::FORMAT_JSON;
			return ActiveForm::validate($model);
		}

		if($model->load(Yii::$app->request->post()) && $model->validate()) {
			try {
				$model->saveData();
				Yii::$app->getSession()->setFlash('success', Yii::t('web', 'Clients group has been added successfully.'));

				return $this->redirect(['details', 'id' => $model->id]);
			}
			catch(\Exception $e) {
				Yii::$app->getSession()->setFlash('error', Yii::t('web', 'Clients group data cannot be saved. Check provided values.'));
				Yii::error($e->getMessage());
				Yii::error($e->getTraceAsString());
			}
		}

		return $this->render('form', [
				'model' => $model,
                'clientsData' => [],
		]);
	}

	public function actionEdit($id) {
        $group = CompanyGroup::findOne($id);
        $model = new ClientsGroupForm($group);
        if(empty($group)) {
			throw new \yii\web\NotFoundHttpException(Yii::t('web', 'Element does not exist or you do not have permission.'));
		}$model->setEditScenario();

		if(Yii::$app->request->isAjax && $model->load(Yii::$app->request->post())) {
			Yii::$app->response->format = Response::FORMAT_JSON;
			return ActiveForm::validate($model);
		}

		if($model->load(Yii::$app->request->post()) && $model->validate()) {
			try {
				$model->saveData();
				Yii::$app->getSession()->setFlash('success', Yii::t('web', 'Clients group has been updated successfully.'));

				return $this->redirect(['details' , 'id' => $model->id]);
			}
			catch(\Exception $e) {
				Yii::$app->getSession()->setFlash('error', Yii::t('web', 'Clients group data cannot be saved. Check provided values.'));
				Yii::error($e->getMessage());
				Yii::error($e->getTraceAsString());
			}
		}

		return $this->render('form', [
				'model' => $model,
                'clientsData' => $model->getClientsData(),
		]);
	}
}
