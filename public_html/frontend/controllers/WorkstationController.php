<?php
namespace frontend\controllers;

use Yii;
use backend\components\Controller;
use yii\data\Pagination;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use frontend\models\WorkstationListForm;
use common\models\aq\WorkstationQuery;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use yii\web\Response;
use yii\widgets\ActiveForm;
use frontend\models\WorkstationForm;
use common\models\ar\Workstation;
use common\models\ar\WorkstationTranslation;
use common\models\aq\UserWorkstationQuery;
use common\models\ar\UserWorkstation;
use common\models\aq\DepartmentQuery;
use common\helpers\Utility;

class WorkstationController extends Controller {
    
    public function behaviors() {
        $allActions = ['list', 'create', 'edit', 'details', 'delete'];
        return[
            'access' => [
                'class' => AccessControl::className(),
                'only' => $allActions,
                'rules' => [
                    [
                        'actions' =>$allActions,
                        'allow' => Yii::$app->params['isProductionVisible'],
                        'roles' => [ Utility::ROLE_WORKER ],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' =>  ['post'],
                ],
            ],
        ];
    }
    
    public function actionList() {
        $request = Yii::$app->request;
        $workstationListForm = new WorkstationListForm();
       
        $workstationListForm->load($request->get());
        if(!$request->isAjax) {
            $derpartmentOptions = DepartmentQuery::getDepartmentList();
        }

        $activeWorkstationQuery = WorkstationQuery::getActiveWorkstationQuery($workstationListForm->toArray());
        $pages = new Pagination([
            'totalCount' => $activeWorkstationQuery->count(),
            'page' => $workstationListForm->page,
            'defaultPageSize' => Yii::$app->params['defaultPageSize'],
        ]);
        
        $workstations = $activeWorkstationQuery->limit($pages->defaultPageSize)->offset($pages->page * $pages->defaultPageSize)->all();
        
        $workstationData = [
            'workstations' => $workstations,
            'pages' => $pages,
            'workstationListForm' => $workstationListForm,
        ];
        
        if($request->isAjax) {
            echo json_encode([
                'view' => $this->renderAjax('_workstations', $workstationData),
                'url' => Url::current([],true),
            ]);
        }
        else {
            $workstationData['departmentTranslations'] = $derpartmentOptions;
            return $this->render('list', $workstationData);
        }
    }
    
    public function actionCreate() {
        $model = new WorkstationForm();
        $model->setAddScenario();
        
        if(Yii::$app->request->isAjax && $model->load(Yii::$app->request->post())) {
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ActiveForm::validate($model);
        }
        
        if($model->load(Yii::$app->request->post()) && $model->validate()) {
            try {
                $model->saveData();
                Yii::$app->getSession()->setFlash('success', Yii::t('web', 'Workstation has been added successfully'));
                
                return $this->redirect(['details', 'id' => $model->id]);
            }
            catch(\Exception $e) {
                Yii::$app->getSession()->setFlash('error', Yii::t('web', 'Workstation data cannot be saved. Check provided values.'));
                Yii::error($e->getMessage());
                Yii::error($e->getTraceAsString());
            }
        }
        
        $departmentData = DepartmentQuery::getDepartmentList();
        $departmentData = ArrayHelper::map($departmentData, 'id', 'name');

        return $this->render('form', [
            'model' => $model,
            'departmentData' => $departmentData,
            'workersData' => [],
        ]);
    }
    
    public function actionEdit($id) {
        $workstation = Workstation::findOne($id);
        if(empty($workstation)) {
            throw new \yii\web\NotFoundHttpException(Yii::t('web', 'Element does not exist or you do not have permission'));
        }
        
        $model = new WorkstationForm($workstation);
        $model->setEditScenario();
        
        if(Yii::$app->request->isAjax && $model->load(Yii::$app->request->post())) {
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ActiveForm::validate($model);
        }
        
        if($model->load(Yii::$app->request->post()) && $model->validate()) {
            try {
                $model->saveData();
                
                Yii::$app->getSession()->setFlash('success', Yii::t('web', 'Workstation data has been updated succussfully'));
                
                return $this->redirect(['details', 'id' => $model->id]);
            }
            catch(\Exception $e) {
                Yii::$app->getSession()->setFlash('error', Yii::t('web', 'Workstation data cannot be saved. Check provided values.'));
                Yii:error($e->getMessage());
                Yii::error($e->getTraceAsString());
            }
        }
        
        $departmentData = DepartmentQuery::getDepartmentList();
        $departmentData = ArrayHelper::map($departmentData, 'id', 'name');
        
        $workersData = UserWorkstationQuery::getWorkersByIds($model->workerIds);
        $workersData = ArrayHelper::map($workersData, 'id', 'fullName');
        
        return $this->render('form',[
            'model' => $model,
            'departmentData' => $departmentData,
            'workersData' => $workersData,
        ]);
    }
    
    public function actionDetails($id) {
        $details = WorkstationQuery::getDetails($id);
        
        if(empty($details)) {
            throw new \yii\web\NotFoundHttpException(Yii::t('web', 'Element does not exist or you do not have permission'));
        }
        
        $workers = UserWorkstationQuery::getWorkersByWorkstationId($id);
        return $this->render('details', [
            'details' => $details,
            'workers' => $workers,
        ]);
    }
    
    public function actionDelete() {
        $id = Yii::$app->request->post('id');
        
        $transaction = Yii::$app->db->beginTransaction();
        try {
            WorkstationTranslation::deleteAll(['workstation_id' => $id]);
            UserWorkstation::deleteAll(['workstation_id' => $id]);
            Workstation::deleteAll(['id' => $id]);
            
            $transaction->commit();
            Yii::$app->getSession()->setFlash('success', Yii::t('web', 'Workstation has been deleted.'));
        }
        catch(\Exception $e) {
            $transaction->rollBack();
            Yii::error($e->getMessage());
            Yii::error($e->getTraceAsString());
            Yii::$app->getSession()->setFlash('error', Yii::t('web', 'Workstation cannot be deleted. Please contact the administrator.'));
        }
        
        if(strpos(Yii::$app->request->getReferrer(), Yii::$app->controller->id.'/details') != false) {
            return $this->redirect(['list']);
        }
        
        return $this->redirect(Yii::$app->request->getReferrer());
    }
}