<?php
namespace frontend\controllers;

use Yii;
use frontend\components\Controller;
use yii\filters\AccessControl;
use common\helpers\Utility;
use common\models\aq\CompanyQuery;
use common\models\aq\CompanyGroupQuery;
use frontend\helpers\AddressHelper;

class CompanyController extends Controller {

	public function behaviors() {
		return [
				'access' => [
						'class' => AccessControl::className(),
						'only' => ['find', 'find-employees', 'find-company-groups'],
						'rules' => [
								[
										'actions' => ['find', 'find-employees', 'find-company-groups'],
										'allow' => true,
										'roles' => [Utility::ROLE_SALESMAN],
								],
						],
				],
		];
	}

    public function actionFind() {
        $request = Yii::$app->request;
        $term = $request->get('term', '');
        $page = $request->get('page', 1);
        $size = $request->get('size');
        $excludedCompanyId = $request->get('excludedCompanyId', 0);
        $onlyParentCompanies = $request->get('onlyParentCompanies', false);
        $parentCompanyId = $request->get('parentCompanyId', 0);
        $raw = $request->get('raw', false);

		if((empty($term) && empty($raw)) || empty($page) || empty($size)) {
            if(!Yii::$app->params['isClientListImmediatelyVisible']){
                Yii::$app->end();
            }
		}

        $companyQuery = CompanyQuery::findCompanies([
        		'name' => $term, 
        		'excludedCompanyId' => $excludedCompanyId,
        		'onlyParentCompanies' => $onlyParentCompanies,
        		'parentCompanyId' => $parentCompanyId,
        ]);
        $companiesCount = $companyQuery->count();
        $companies = $companyQuery->limit($size)->offset($size*($page-1))->all();
        
        $companies = array_map(function($company) { 
		        	return array_merge($company, ['address' => !empty(Yii::$app->params['isCompanySearchWithAddress'])
							? AddressHelper::getFullAddress([
		       						'addressMain' => $company['addressMain'], 
		        					'cityName' => $company['city'], 
		        					'cityZipCode' => $company['zipCode']
		        			]) : '']);
				}, $companies);
        
        if(empty($raw)) {
	        echo json_encode([
		            'items' => $companies,
		            'more' => (intval($companiesCount) > $page*$size)
	        ]);
        }
        else {
        	$data = [];
        	foreach($companies as $company) {
        		$data[] = ['id' => $company['id'], 'text' => $company['name'], 
        				'address' => !empty(Yii::$app->params['isCompanySearchWithAddress']) 
        					? AddressHelper::getFullAddress([
        							'addressMain' => $company['addressMain'], 
        							'cityName' => $company['city'], 
        							'cityZipCode' => $company['zipCode']
        					])
        					: '',
        		];
        	}
        	
        	echo json_encode($data);
        }
    }

    public function actionFindEmployees() {
        $request = Yii::$app->request;
        $term = $request->get('term');
        $page = $request->get('page', 1);
        $size = $request->get('size');
        $companyId = $request->get('companyId');

        if (empty($page) || empty($size) || empty($companyId)) {
            Yii::$app->end();
        }
        if(empty($term)) {
            $employeesQuery = CompanyQuery::findEmployees($companyId);
        } else {
            $employeesQuery = CompanyQuery::findEmployees($companyId, ['name' => $term]);
        }
        
        $employeesCount = $employeesQuery->count();
        $employees = $employeesQuery->limit($size)->offset($size*($page-1))->all();

        echo json_encode([
            'items' => $employees,
            'more' => (intval($employeesCount) > $page*$size)
        ]);
    }

    public function actionFindCompanyGroups() {
    	$request = Yii::$app->request;
    	$term = $request->get('term');
    	$page = $request->get('page', 1);
    	$size = $request->get('size');

    	if(empty($term) || empty($page) || empty($size)) {
    		Yii::$app->end();
    	}

    	$query = CompanyGroupQuery::findCompanyGroups(['name' => $term, 'userId' => Yii::$app->user->id]);
    	$count = $query->count();
    	$data = $query->limit($size)->offset($size * ($page - 1))->all();

    	echo json_encode([
    			'items' => $data,
				'total' => count($data),
    			'more' => (intval($count) > $page * $size)
    	]);
    }
}
