<?php

namespace frontend\controllers;

use Yii;
use yii\data\Pagination;
use yii\filters\AccessControl;
use yii\helpers\Url;
use frontend\components\Controller;
use frontend\logic\ProductionPathLogic;
use frontend\models\ProductionPathListForm;
use frontend\logic\ProductAttributeTypeLogic;
use common\models\ar\Language;
use common\models\ar\ProductionPath;
use common\models\ar\ProductionPathStep;
use common\models\ar\ProductionPathTranslation;
use common\models\ar\OfferedProduct;
use common\models\aq\ProductionPathQuery;
use common\models\aq\ProductionPathStepQuery;
use common\models\ar\ProductionPathStepAttribute;
use common\helpers\RandomSymbolGenerator;
use common\helpers\Utility;

class ProductionPathController extends Controller {

    public function behaviors() {
        $allActions = ['index', 'list', 'create', 'edit', 'save-step-path', 'get-production-path-steps', 'details', 'get-attributes-for-production-path-steps', 'delete'];

        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => $allActions,
                'rules' => [
                    [
                        'actions' => $allActions,
                        'allow' => Yii::$app->params['isProductionVisible'],
                        'roles' => [Utility::ROLE_WORKER],
                    ],
                ],
            ],
        ];
    }

    public function actionIndex() {
        return $this->redirect(['production-path/list']);
    }

    public function actionList() {
        $request = Yii::$app->request;
        $listForm = new ProductionPathListForm();
        $listForm->load($request->get());

        $itemsQuery = ProductionPathQuery::getProductionPathsQuery($listForm->toArray());
        $pages = new Pagination([
            'totalCount' => $itemsQuery->count(),
            'page' => $listForm->page,
            'defaultPageSize' => \Yii::$app->params['defaultPageSize'],
        ]);
        $items = $itemsQuery->limit($pages->defaultPageSize)->offset($pages->page * $pages->defaultPageSize)->all();

        $listData = [
            'items' => $items,
            'pages' => $pages,
            'listForm' => $listForm,
        ];

        if($request->isAjax) {
            echo json_encode([
                'view' => $this->renderAjax('_productionPaths', $listData),
                'url' => Url::current([], true),
            ]);
        } else {
            return $this->render('list', $listData);
        }
    }

    public function actionDetails($id) {
        $data = ProductionPathQuery::getDetails($id);

        if(empty($data)) {
            throw new \yii\web\NotFoundHttpException(Yii::t('web', 'Element does not exist or you do not have permission.'));
        }

        return $this->render('details', [
                    'data' => $data,
                    'id' => $id
        ]);
    }

    public function actionCreate() {
        return $this->render('create', [
        ]);
    }

    public function actionEdit($id) {
        return $this->render('edit', ['id' => $id]);
    }

    public function actionSaveStepPath() {
        $request = Yii::$app->request;
        $nodes = $request->post('nodes');
        $pathName = $request->post('pathName');
        $productionPathId = $request->post('productionPathId');
        $attributes = $request->post('attributes');

        $languageId = Language::find()->where(['symbol' => Yii::$app->language])->one()->id;

        $productionPathLogic = new ProductionPathLogic();
        $transaction = Yii::$app->db->beginTransaction();

        try {
            if(empty($productionPathId)) {
                $path = new ProductionPath();
                $path->symbol = RandomSymbolGenerator::generate(ProductionPath::className(), 'symbol', 32);
                $path->save();

                $pathTranslation = new ProductionPathTranslation();
                $pathTranslation->name = $pathName;
                $pathTranslation->language_id = $languageId;
                $pathTranslation->production_path_id = $path->id;
                $pathTranslation->save();
                $productionPathId = $path->id;
            } else {
                $pathTranslation = ProductionPathTranslation::findOne(['production_path_id' => $productionPathId, 'language_id' => $languageId]);
                $pathTranslation->name = $pathName;
                $pathTranslation->save();
            }


            $oldSteps = ProductionPathStep::findAll(['production_path_id' => $productionPathId]);
            foreach($oldSteps as $oldStep) {
                ProductionPathStepAttribute::deleteAll(['production_path_step_id' => $oldStep['id']]);
            }

            ProductionPathStep::deleteAll(['production_path_id' => $productionPathId]);

            $attributes = $productionPathLogic->convertAttributsToNodes($attributes);
            foreach($nodes as &$node) {
                $pathSteps = new ProductionPathStep();
                $pathSteps->production_path_id = $productionPathId;
                $pathSteps->activity_id = $node['activityId'];
                $pathSteps->is_requirement_type_or = 0;

                if(isset($attributes[$node['id']]) && !empty($attributes[$node['id']])) {
                    $pathSteps->attribute_needed_ids = isset($attributes[$node['id']]['needed']) && !empty($attributes[$node['id']]['needed']) ? $attributes[$node['id']]['needed'] : null;
                    $pathSteps->attribute_forbidden_ids = isset($attributes[$node['id']]['forbidden']) && !empty($attributes[$node['id']]['forbidden']) ? $attributes[$node['id']]['forbidden'] : null;
                }

                $pathSteps->save();

                $node['tmp_id'] = $node['id'];
                $node['id'] = $pathSteps->id;
            }

            $new_nodes = $productionPathLogic->convertNodeIdsToStepIds($nodes);

            foreach($new_nodes as $n) {
                $pathSteps = ProductionPathStep::findOne($n['id']);
                $pathSteps->step_successor_ids = empty($n['successor_ids']) ? null : $n['successor_ids'];
                $pathSteps->step_required_ids = empty($n['required_ids']) ? null : $n['required_ids'];
                $pathSteps->save();
            }

            $steps = ProductionPathStepQuery::getByProductionPathId($productionPathId);
            $sortSteps = implode(',', $productionPathLogic->sortTopologically($steps));

            $productionPath = ProductionPath::findOne($productionPathId);
            $productionPath->topological_order = $sortSteps;
            $productionPath->save();

            foreach($new_nodes as $n) {
                if(isset($attributes[$n['tmp_id']]) && !empty($attributes[$n['tmp_id']])) {
                    $unblockAttributes = isset($attributes[$n['tmp_id']]['unblock']) && !empty($attributes[$n['tmp_id']]['unblock']) ? $attributes[$n['tmp_id']]['unblock'] : '';
                    $unblockAttributes = explode(',', $unblockAttributes);
                    foreach($unblockAttributes as $unblockAttribute) {
                        $pathStepAttribute = new ProductionPathStepAttribute();
                        $pathStepAttribute->production_path_step_id = $n['id'];
                        $pathStepAttribute->product_attribute_type_id = $unblockAttribute;
                        $pathStepAttribute->save();
                    }
                }
            }

            $transaction->commit();

            Yii::$app->getSession()->setFlash('success', Yii::t('main', 'Production path has been save.'));
            return $this->redirect(['details', 'id' => $productionPathId]);
        } catch(\Exception $e) {
            $transaction->rollBack();
            Yii::error($e->getMessage());
            Yii::error($e->getTraceAsString());
            Yii::$app->getSession()->setFlash('error', Yii::t('main', 'Production path cannot be save, please contact the administrator.'));
            return $this->redirect(Yii::$app->request->referrer);
        }
    }

    public function actionGetProductionPathSteps() {
        $request = Yii::$app->request;
        $id = $request->get('id');
        $languageId = Language::find()->where(['symbol' => Yii::$app->language])->one()->id;
        $steps = ProductionPathStepQuery::getByProductionPathId($id);
        $productionPath = ProductionPathQuery::getTranslationById($id);

        echo json_encode(['steps' => $steps, 'productionPath' => $productionPath]);
    }

    public function actionGetAttributesForProductionPathSteps($id = null) {

        $productionPathLogic = new ProductionPathLogic();
        $nodeWithAttributes = [];
        $attributes = (new ProductAttributeTypeLogic())->getAttributeGroupsWithTypes();
        if($id != null) {
            $steps = ProductionPathStep::findAll(['production_path_id' => $id]);
            $nodeWithAttributes = [];
            foreach($steps as $step) {
                $stepAttributes = ProductionPathStepAttribute::findAll(['production_path_step_id' => $step['id']]);
                $nodeWithAttributes[$step['id']] = $attributes;
                $forbidden = explode(',', $step->attribute_forbidden_ids);
                $needed = explode(',', $step->attribute_needed_ids);

                foreach($forbidden as $f) {
                    $nodeWithAttributes[$step['id']] = $productionPathLogic->checkAttributs($nodeWithAttributes[$step['id']], $f, 'forbidden', true);
                }

                foreach($needed as $n) {
                    $nodeWithAttributes[$step['id']] = $productionPathLogic->checkAttributs($nodeWithAttributes[$step['id']], $n, 'needed', true);
                }

                foreach($stepAttributes as $stepAttribute) {
                    $nodeWithAttributes[$step['id']] = $productionPathLogic->checkAttributs($nodeWithAttributes[$step['id']], $stepAttribute->product_attribute_type_id, 'unblock', true);
                }
            }
        }

        echo json_encode(['attributes' => $attributes, 'nodeWithAttributes' => $nodeWithAttributes]);
    }

    public function actionDelete($id) {
        $transaction = Yii::$app->db->beginTransaction();
        try {
            $offeredProduct = OfferedProduct::find()->where(['production_path_id' => $id])->one();
            if(!(count($offeredProduct) > 0)) {
                $productionPath = ProductionPath::findOne(['id' => $id]);
                $productionPath->is_active = 0;
                $productionPath->save();

                $transaction->commit();
                Yii::$app->getSession()->setFlash('success', Yii::t('web', 'The production path has been deleted.'));
                return $this->redirect(['index']);
            } else {
                $transaction->rollBack();
                Yii::$app->getSession()->setFlash('error', Yii::t('main', 'The object cannot be deleted because it is used by other objects.'));
            }
        } catch(\Exception $e) {
            $transaction->rollBack();
            Yii::$app->getSession()->setFlash('error', Yii::t('main', 'The object cannot be deleted because it is used by other objects.'));
        }

        return $this->redirect(Url::to(['details', 'id' => $id]));
    }

}
