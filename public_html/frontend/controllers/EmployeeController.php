<?php
namespace frontend\controllers;

use Yii;
use frontend\components\Controller;
use yii\filters\AccessControl;
use yii\helpers\Url;
use yii\web\Response;
use yii\widgets\ActiveForm;
use yii\data\Pagination;
use yii\helpers\ArrayHelper;
use yii\helpers\FileHelper;
use yii\db\Query;

use common\helpers\Utility;
use common\helpers\UploadHelper;
use console\controllers\RbacController;

use common\models\aq\UserQuery;
use common\models\aq\RoleQuery;
use common\models\aq\UserCompanySalesmanQuery;
use common\models\aq\UserWarehouseQuery;
use common\models\aq\CompanyGroupQuery;

use common\models\ar\User;
use common\models\ar\UserAttachment;
use common\models\ar\Warehouse;
use common\models\ar\Role;

use frontend\models\EmployeeListForm;
use frontend\models\EmployeeForm;
use common\models\ar\UserModificationHistory;
use common\helpers\ChangeHelper;
use yii\filters\VerbFilter;
use common\components\GetProvinceByZipCodeTrait;

class EmployeeController extends Controller {
	use GetProvinceByZipCodeTrait;

	public function behaviors() {
		return [
				'access' => [
						'class' => AccessControl::className(),
						'only' => ['index', 'list', 'create', 'edit', 'delete', 'find', 'details', 'get-main-photo', 'delete-attachment'],
						'rules' => [
								[
										'actions' => ['index', 'list', 'create', 'edit', 'delete', 'details',
												'get-main-photo', 'delete-attachment'],
										'allow' => true, 
										'roles' => [Utility::ROLE_MAIN_SALESMAN, Utility::ROLE_MAIN_STOREKEEPER]
								],
								[
										'actions' => ['find'],
										'allow' => true,
								],
						],
				],
				'verbs' => [
						'class' => VerbFilter::className(),
						'actions' => [
								'delete' => ['post'],
						],
				],
		];
	}

	public function actionIndex() {
		return $this->redirect(['employee/list']);
	}

	public function actionList() {
		$request = Yii::$app->request;
		$listForm = new EmployeeListForm();
		$listForm->load($request->get());

		if(!$request->isAjax) {
			$roleOptions = UserQuery::getRoleList();
			$listForm->allRolesCount = count($roleOptions);
		}

		$employeeQuery = UserQuery::getActiveEmployeeQuery($listForm->toArray());

		$pages = new Pagination([
				'totalCount' => $employeeQuery->count(),
				'page' => $listForm->page,
				'defaultPageSize' => \Yii::$app->params['defaultPageSize'],
		]);
		$items = $employeeQuery->limit($pages->defaultPageSize)->offset($pages->page * $pages->defaultPageSize)->all();

		$listData = [
				'items' => $items,
				'pages' => $pages,
				'listForm' => $listForm,
		];
		
		if($request->isAjax) {
			echo json_encode([
					'view' => $this->renderAjax('_users', $listData),
					'url' => Url::current([], true),
			]);
		}
		else {
			$listData['roleTranslations'] = $roleOptions;
			return $this->render('list', $listData);
		}
	}
	
	public function actionGetMainPhoto() {
		$request = Yii::$app->request;
		$userId = $request->get('id');
		if(!$request->isAjax || empty($userId)) {
			return '';
		}
	
		$urlImage = UserQuery::getMainPhoto($userId);
		return !empty($urlImage) ? '<img src="'.\common\helpers\UploadHelper::getNormalPath($urlImage, 'user_photo').'"/>' : '';
	}

	public function actionCreate() {
		$model = new EmployeeForm();
		$model->setAddScenario();

		if(Yii::$app->request->isAjax && $model->load(Yii::$app->request->post())) {
			Yii::$app->response->format = Response::FORMAT_JSON;
			return ActiveForm::validate($model) + ActiveForm::validateMultiple($model->getDocumentsForms());
		}
		
		if(Yii::$app->request->isPost) {
			if($model->load(Yii::$app->request->post()) && $model->validate()) {
				try {
					$model->saveData(Utility::ACTION_CREATE);
					
					$rbacController = new RbacController('rbac', Yii::$app->module);
					if(!empty($model->roles)) {
						foreach ($model->roles as $addRole){
							$rbacController->assignRoleToUser(Role::find()->where(['id' => $addRole])->one()->symbol, $model->id);
						}
					}
					
					Yii::$app->getSession()->setFlash('success', Yii::t('web', 'Employee has been added successfully.'));
	
					return $this->redirect(['details', 'id' => $model->id]);
				}
				catch(\Exception $e) {
					Yii::$app->getSession()->setFlash('error', Yii::t('web', 'Employee cannot be saved. Check provided values.'));
					Yii::error($e->getMessage());
					Yii::error($e->getTraceAsString());
				}
			}
		}

		return $this->render('form', [
				'model' => $model,
				'clientGroupsData' => [],
				'warehouseGroupsData' => [],
                'rolesData' => ArrayHelper::map(
                	RoleQuery::findAvailableRoles()->all(),
                    'id',
                    'name'
                ),

		]);
	}

	public function actionEdit($id) {
		$employee = User::findOne($id);
		if(empty($employee) || in_array(Utility::ROLE_CLIENT, array_column($employee->roles, 'symbol'))) {
			throw new \yii\web\NotFoundHttpException(Yii::t('web', 'Element does not exist or you do not have permission.'));
		}
		
		$model = new EmployeeForm($employee);
		$model->setEditScenario();
		
		if(Yii::$app->request->isAjax && $model->load(Yii::$app->request->post())) {
			Yii::$app->response->format = Response::FORMAT_JSON;
			return ActiveForm::validate($model);
		}

		if(Yii::$app->request->isPost) {
			if($model->load(Yii::$app->request->post()) && $model->validate()) {
				try {
					$rbacController = new RbacController('rbac', Yii::$app->module);
					$oldRoles = (new Query())->select(['role_id'])->from('user_role')
							->where(['user_id' => $model->id])->all();
					if(!empty($oldRoles)) {
						foreach ($oldRoles as $deleteRole){
							if (!in_array($deleteRole, !empty($model->roles) ? $model->roles : [])){
								$rbacController->revokeRoleFromUser(Role::find()->where(['id' => $deleteRole])->one()->symbol, $model->id);
							}
						}
					}
					if(!empty($model->roles)) {
						foreach ($model->roles as $addRole){
							if (!in_array($addRole, $oldRoles)){
								$rbacController->assignRoleToUser(Role::find()->where(['id' => $addRole])->one()->symbol, $model->id);
							}
						}
					}
	
					$model->saveData(Utility::ACTION_EDIT);
					Yii::$app->getSession()->setFlash('success', Yii::t('web', 'Employee has been updated successfully.'));
	
					return $this->redirect(['details', 'id' => $model->id]);
				}
				catch(\Exception $e) {
					throw $e;
					Yii::$app->getSession()->setFlash('error', Yii::t('web', 'Employee cannot be saved. Check provided values.'));
					Yii::error($e->getMessage());
					Yii::error($e->getTraceAsString());
				}
			}
		}

		return $this->render('form', [
				'model' => $model,
				'clientGroupsData' => ArrayHelper::map(
										CompanyGroupQuery::findCompanyGroups(['userId' => Yii::$app->user->id])->all(),
										'id',
										'name'
									),
				'warehouseGroupsData' => ArrayHelper::map(
										Warehouse::find()->where(['id' => $model->warehouseGroupIds])->all(),
										'id',
										'name'
									),
				'rolesData' => ArrayHelper::map(
										RoleQuery::findAvailableRoles()->all(),
										'id',
										'name'
									),
		]);
	}

	public function actionDetails($id) {
		$data = UserQuery::getDetails($id);

        if(empty($data)) {
            throw new \yii\web\NotFoundHttpException(Yii::t('web', 'Element does not exist or you do not have permission.'));
        }

		$roles = RoleQuery::getRolesTranslationsByUserId($id)->one()['name'];
		$companyGroups = UserCompanySalesmanQuery::getCompanyGroupsTranslationsByUserId($id)->all();
		$attachments = UserQuery::getAttachments($id);

		return $this->render('details', [
				'data' => $data,
				'roles' => $roles,
				'companyGroups' => $companyGroups,
				'attachments' => $attachments,
				'warehouses' => Yii::$app->params['isMoreWarehousesAvailable'] ? UserWarehouseQuery::getWarehousesByUserId($id)->all() : [],
		]);
	}

	public function actionDelete() {
		$id = Yii::$app->request->post('id');

		$transaction = Yii::$app->db->beginTransaction();
		try {
			if((!UserQuery::isSalesmanAssignedToCompanyByCompanyId(Yii::$app->user->id, $id) && (!Yii::$app->user->can(Utility::ROLE_ADMIN)))) {
				throw new \Exception('The user has no privilege to delete this employee: '.$id);
			}

			$employee = User::findOne($id);			
			if(empty($employee) || in_array(Utility::ROLE_CLIENT, array_column($employee->roles, 'symbol'))) {
				throw new \Exception('The user has no privilege to delete this employee: '.$id);
			}
			
			// Póki użytkownik jest dezaktywowany, nie ma sensu usuwać mu wszystkich danych
			/*$oldPhotoRelativePath = preg_replace('|^'.Url::to('@web/storage/index\?f='.'|'), '', $employee->url_photo);
    		$oldFile = FileHelper::normalizePath(UploadHelper::getUploadPath().'/'.$oldPhotoRelativePath);
    		@unlink($oldFile);

    		$rbacController = new RbacController('rbac', Yii::$app->module);
    		$roleIdQuery = (new Query())->select(['role_id'])->from('user_role')->where(['user_id' => $employee->id])->all();
    		if(!empty($roleIdQuery)){
	    		foreach ($roleIdQuery as $roleId){
	    			$rbacController->revokeRoleFromUser(Role::find()->where(['id' => $roleId])->one()->symbol, $employee->id);
	    		}
    		}*/
    		
    		$changeHelper = new ChangeHelper($employee, Utility::ACTION_DELETE);
			$changeHelper->saveChanges($id, new UserModificationHistory());
			$employee->is_active = 0;
			$employee->save();

			$transaction->commit();

			Yii::$app->getSession()->setFlash('success', Yii::t('web', 'Employee has been deleted.'));
		}
		catch(\Exception $e) {
			Yii::$app->getSession()->setFlash('error', Yii::t('web', 'Employee cannot be deleted. Please contact the administrator.'));
		}

		if(strpos(Yii::$app->request->getReferrer(), Yii::$app->controller->id.'/details') !== false) {
			return $this->redirect(['list']);
		}
		return $this->redirect(Yii::$app->request->getReferrer());
	}

	public function actionFind() {
    	$request = Yii::$app->request;
    	$term = $request->get('term');
    	$page = $request->get('page', 1);
    	$size = $request->get('size');

    	if(empty($term) || empty($page) || empty($size)) {
    		Yii::$app->end();
    	}

    	$query = UserQuery::findEmployees(['name' => $term, 'roles' => $request->get('roles')]);
    	$count = $query->count();
    	$items = $query->limit($size)->offset($size * ($page - 1))->all();

    	echo json_encode([
    			'items' => $items,
    			'more' => (intval($count) > $page * $size),
    	]);
    }
    
    public function actionDeleteAttachment() {
    	$id = Yii::$app->request->post('id');
    	$url = Yii::$app->request->post('url');
    	$attachment = UserAttachment::findOne($id);
    	UploadHelper::deleteAttachment($url, $attachment);
    	if(!Yii::$app->request->isAjax) {
    		return $this->redirect(Yii::$app->request->getReferrer());
    	}
    }

}
