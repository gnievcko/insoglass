<?php
namespace frontend\controllers;

use Yii;
use yii\web\BadRequestHttpException;
use yii\helpers\FileHelper;
use yii\filters\AccessControl;

use frontend\components\Controller;
use common\components\UploadTrait;
use common\helpers\UploadHelper;

use frontend\models\PhotoTemporaryStorageForm;
use frontend\models\PhotoForm;
use frontend\models\DocumentTemporaryStorageForm;
use frontend\models\DocumentForm;
use yii\web\UploadedFile;
use common\models\ar\OrderAttachment;
use common\models\ar\ProductAttachment;
use common\models\ar\OfferedProductAttachment;
use common\models\ar\UserAttachment;
use frontend\models\ProductTemporaryStorageForm;
use frontend\models\OrderProductForm;
use frontend\models\OrderCostForm;
/**
 * Storage controller
 */
class StorageController extends Controller {

	use UploadTrait;

	/**
	 * @inheritdoc
	 */
	public function behaviors() {
		return [
				'access' => [
						'class' => AccessControl::className(),
						'only' => ['index', 'upload', 'file'],
						'rules' => [
								[
										'actions' => ['index', 'file'],
										'allow' => true
								],
								[
										'actions' => ['upload'],
										'allow' => true,
										'roles' => ['@'],
								],
						],
				],
		];
	}

	public function actionIndex($f) {
		$originalKey = $f;
		$f = FileHelper::normalizePath(UploadHelper::getUploadPath().'/'.$f);

		if(!file_exists($f)) {
			throw new BadRequestHttpException(Yii::t('main', 'The requested page does not exist. Please verify the address of the page.'));
		}		
		
		$realFilename = $this->getRealFilename($originalKey);

		Yii::$app->response->format = \yii\web\Response::FORMAT_RAW;

		header('Content-Type: ' . mime_content_type($f));
		if(!empty($realFilename)) {
			header('Content-Disposition: inline; filename="'.$realFilename.'"');
		}
		echo file_get_contents($f);
	}
	
	private function getRealFilename($f) {
		$whereCondition = ['SUBSTRING(url_file, LOCATE("f=", url_file) + 2)' => $f];
		
		$queries = [];
		$queries[] = OrderAttachment::find()->where($whereCondition);
		$queries[] = ProductAttachment::find()->where($whereCondition);
		$queries[] = UserAttachment::find()->where($whereCondition);
		$queries[] = OfferedProductAttachment::find()->where($whereCondition);
		
		foreach($queries as $query) {
			$row = $query->one();
			if(!empty($row)) {
				return $row->name;
			}
		}
			
		return null;
	}

	public function actionUpload() {		
        $fileType = Yii::$app->request->post('type');

        //TODO: Errors handling

        if($fileType == 'photo') {
            $uploadForm = new PhotoTemporaryStorageForm();
        }
        else if($fileType == 'document') {
            $uploadForm = new DocumentTemporaryStorageForm();
        }
        else if($fileType == 'mix') {
            $uploadForm = new ProductTemporaryStorageForm();
        }
        else {
            Yii::$app->end();
        }
        $uploadForm->file = UploadedFile::getInstance($uploadForm, 'file');
        if(!$uploadForm->validate()){
            $e = $uploadForm->getErrors();
            Yii::$app->response->statusCode = 400;
            echo json_encode($e);
        }
        if (($uploadDetails = $uploadForm->upload()) !== null) {
            echo json_encode($uploadDetails);
        }
	}

    public function actionGetPhotoForm() {
        $request = \Yii::$app->request;
        $photoForm = new PhotoForm();
        $photoForm->image = $request->get('photo');
        $photoForm->thumbnail = $request->get('thumbnail');

        if($request->isAjax && $photoForm->validate()) {
            echo $this->renderPartial('/inc/photo', [
                'photoForm' => $photoForm,
                'prefix' => $request->get('prefix'),
            ]);
        }
    }

    public function actionGetDocumentForm() {
        $request = \Yii::$app->request;
        $documentForm = new DocumentForm();
        $documentForm->document = $request->get('document');
        $documentForm->name = $request->get('name');

        if($request->isAjax && $documentForm->validate()) {
            echo $this->renderPartial('/inc/document', [
                'documentForm' => $documentForm,
                'prefix' => $request->get('prefix'),
            ]);
        }
    }
    
    public function actionGetProductFileForm() {
        $request = \Yii::$app->request;
        $documentForm = new DocumentForm();
        $documentForm->document = $request->get('document');
        $documentForm->name = $request->get('name');
        
        $prefix = $request->get('prefix');
        $productForm = new OrderProductForm();
        if($request->get('form') != $productForm->formName()) {
            $productForm = new OrderCostForm();
        }
        if($request->isAjax && $documentForm->validate()) {
            echo $this->renderPartial('/inc/productFile', [
                'prefix' => $prefix,
                'productForm' => $productForm,
                'file' => $documentForm,
                'productPrefix' => $request->get('productPrefix'),
            ]);
        }
    }
    
    public function actionFile($name, $f) {
		$originalKey = $f;
		$f = FileHelper::normalizePath(UploadHelper::getUploadPath().'/'.$f);

		if(!file_exists($f)) {
			throw new BadRequestHttpException(Yii::t('main', 'The requested page does not exist. Please verify the address of the page.'));
		}		
		
		//$realFilename = $this->getRealFilename($originalKey);

		Yii::$app->response->format = \yii\web\Response::FORMAT_RAW;

		header('Content-Type: ' . mime_content_type($f));
		if(!empty($realFilename)) {
			header('Content-Disposition: inline; filename="'.$realFilename.'"');
		}
		echo file_get_contents($f);
	}
}
