<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace frontend\assets;

use yii\web\AssetBundle;

/**
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class AppAsset extends AssetBundle {
	
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
			'css/main.css',
            'css/vis.min.css',
    ];
    public $js = [
    		'js/ifvisible.min.js',
    		'js/jquery.query-object.js',
    		'js/global.min.js',
    		'js/modal-form.js',
//            'js/vis-network.min.js',
            'js/vis.min.js',
            'js/d3.min.js',
    ];
    public $depends = [
	        'yii\web\YiiAsset',
	        'yii\bootstrap\BootstrapPluginAsset',
    ];
}
