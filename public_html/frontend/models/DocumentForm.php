<?php

namespace frontend\models;

use Yii;
use yii\base\Model;

class DocumentForm extends Model {
	
	public $id;
	public $document;
	public $name;
	public $description;
	public $makeCopy;
	
	public function attributeLabels() {
		return [
				'document' => \Yii::t('web', 'Document'),
				'name' => \Yii::t('main', 'Name'),
				'description' => \Yii::t('main', 'Description'),
		];
	}
	
	public function rules() {
		return [
				[['document', 'name'], 'required'],
				[['id'], 'integer'],
				[['description'], 'string'],
				['makeCopy', 'default', 'value' => false],
		];
	}
}
