<?php

namespace frontend\models;

use Yii;
use yii\base\Model;
use common\helpers\StringHelper;

class ProductReservationListForm extends Model {
	
    public $page;
	public $name;
    public $sortDir;
    public $sortField;
    
    public $orderName;
    public $productName;

    const DEFAULT_PAGE = 0;
    const DEFAULT_SORT_DIR = 'asc';
    const DEFAULT_SORT_FIELD = 'name';

    public function init() {
        parent::init();
        $this->page = self::DEFAULT_PAGE;
        $this->sortDir = self::DEFAULT_SORT_DIR;
        $this->sortField = self::DEFAULT_SORT_FIELD;
    }

    public static function getSortFields() { 
        return [
        		['name' => 'title', 'label' => StringHelper::translateOrderToOffer('main', 'Order'), 'isSortable' => true, 'style' => 'width: 25%'],
	            ['name' => 'name', 'label' => Yii::t('web', 'Product'), 'isSortable' => true, 'style' => 'width: 25%'],
        		['name' => 'count','label' => Yii::t('web', 'Count'), 'isSortable' => true, 'style' => 'width: 8%'],
        		['name' => 'date_creation', 'label' => Yii::t('main', 'Creation date'), 'isSortable' => true, 'style' => 'width: 10%'],
        		['name' => 'user', 'label' => Yii::t('web', 'User confirming'), 'isSortable' => true, 'style' => 'width: 15%'],
        ];
    }

    public function attributeLabels() {
        return [
        		'orderName' => StringHelper::translateOrderToOffer('main', 'Order'),
        		'productName' => Yii::t('web', 'Product'),
        		'warehouseIds' => Yii::t('main', 'Warehouses'),
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules() {
        return [

				[['page'], 'integer'],
				[['sortDir'], 'in', 'range' => ['asc', 'desc']],
				[['sortField'], 'in', 'range' => array_column(self::getSortFields(), 'name')],
				[['orderName', 'productName'], 'safe'],
	            ['page', 'default', 'value' => self::DEFAULT_PAGE],
	            ['sortDir', 'default', 'value' => self::DEFAULT_SORT_DIR],
	            ['sortField', 'default', 'value' => self::DEFAULT_SORT_FIELD],

        ];
    }
}
