<?php

namespace frontend\models;

use Yii;
use yii\base\Model;

class OfferedProductRequiredProductListForm extends Model {
	
    public $page;
	public $name;
    public $sortDir;
    public $sortField;

    const DEFAULT_PAGE = 0;
    const DEFAULT_SORT_DIR = 'asc';
    const DEFAULT_SORT_FIELD = 'name';

    public function init() {
        parent::init();
        $this->page = self::DEFAULT_PAGE;
        $this->sortDir = self::DEFAULT_SORT_DIR;
        $this->sortField = self::DEFAULT_SORT_FIELD;
    }

    public static function getSortFields() {
        $fields = [
            ['name' => 'name', 'label' => Yii::t('main', 'Name'), 'isSortable' => true, 'style' => 'width: 25%'],
            ['name' => 'count', 'label' => Yii::t('web', 'Count'), 'isSortable' => true],
            ['name' => 'unit', 'label' => Yii::t('main', 'Unit'), 'isSortable' => true],
            ['name' => 'note', 'label' => Yii::t('web', 'Note'), 'isSortable' => true, 'style' => 'width: 25%'],
        ];

        if(Yii::$app->params['isProductPhotoOnListVisible']){
            $fields = array_merge([['name' => 'urlPhoto', 'label' => Yii::t('main', 'Photo'), 'isSortable' => false, 'style' => 'width: 10%']], $fields);
        };
        return $fields;
    }

    public function attributeLabels() {
        return [
        		'name' => Yii::t('web', 'Name'),
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
				[['page'], 'integer'],
				[['sortDir'], 'in', 'range' => ['asc', 'desc']],
				[['sortField'], 'in', 'range' => array_column(self::getSortFields(), 'name')],
				[['name'], 'safe'],
	            ['page', 'default', 'value' => self::DEFAULT_PAGE],
	            ['sortDir', 'default', 'value' => self::DEFAULT_SORT_DIR],
	            ['sortField', 'default', 'value' => self::DEFAULT_SORT_FIELD],
        ];
    }
}
