<?php

namespace frontend\models;

use Yii;
use yii\base\Model;
use common\models\ar\Language;
use common\models\ar\WorkstationTranslation;
use common\models\ar\Workstation;
use common\helpers\RandomSymbolGenerator;
use common\models\ar\UserWorkstation;
use common\models\aq\UserWorkstationQuery;

class WorkstationForm extends Model {
    
    const SCENARIO_ADD = 'add';
    const SCENARIO_EDIT = 'edit';
    
    public $id;
    public $name;
    public $departmentId;
    public $workerIds;
    
    public function __construct($workstation = null, $config = []) {
        if(!empty($workstation)) {
            $this->loadData($workstation);
        }
        
        parent::__construct($config);
    }
    
    public function loadData($workstation) {
        $languageId = Language::find()->where(['symbol' => Yii::$app->language])->one()->id;
        $this->id = $workstation->id;
        $this->departmentId = $workstation->department_id;
        $workstationTranslation = $workstation->getTranslation();
        $this->name = $workstationTranslation->name;
        
        $workers = UserWorkstationQuery::getWorkersByWorkstationId($this->id);
        $this->workerIds = array_map(function($worker) {
                    return $worker['id'];
                }, $workers);
    }
    
    public function scenarios() {
        $scenarios = parent::scenarios();
        $scenarios[self::SCENARIO_ADD] = ['name', 'departmentId', 'workerIds'];
        $scenarios[self::SCENARIO_EDIT] = ['id', 'name', 'departmentId', 'workerIds'];
        return $scenarios;
    }
    
    public function rules() {
        return [
            [['name'], 'required'],
            [['name'], 'string', 'max' => 128],
            [['name'], 'validateName'],
            [['departmentId'], 'required'],
            [['workerIds'], 'safe'],
        ];
    }
    
    public function validateName($attribute, $params) {
        $languageId = Language::find()->where(['symbol' => Yii::$app->language])->one()->id;
        if($this->isEditScenarioSet()) {
            if(!empty(WorkstationTranslation::find()
                    ->where(['name' => $this->name])
                    ->andWhere(['language_id' => intval($languageId)])
                    ->andWhere(['!=', 'workstation_id', intval($this->id)])
                    ->one())) {
                $this->addError('name', Yii::t('web', 'Name already exists'));
            }
        }
        else {
            if(!empty(WorkstationTranslation::find()
                    ->where(['name' => $this->name])
                    ->andWhere(['language_id' => intval($languageId)])
                    ->one())) {
                $this->addError('name', Yii::t('web', 'Name already exists'));
            }
        }
    }
    
    public function attributeLabels() {
        return [
            'name' => Yii::t('main', 'Name'),
            'departmentId' => Yii::t('main', 'Department'),
            'workerIds' => Yii::t('web', 'Workers'),
        ];
    }
    public function saveData() {
        $transaction = Yii::$app->db->beginTransaction();
        try {
            $languageId = Language::find()->where(['symbol' => Yii::$app->language])->one()->id;
            
            if(empty($this->id)) {
                $workstation = new Workstation();
                $workstationTranslation = new WorkstationTranslation();
            }
            else {
                $workstation = Workstation::findOne($this->id);
                $workstationTranslation = WorkstationTranslation::find()
                    ->where(['workstation_id' => $this->id])
                    ->andWhere(['language_id' => $languageId])->one();
            }
            if(!$this->isEditScenarioSet()) {
                $workstation->symbol = RandomSymbolGenerator::generate($workstation,'symbol', 32);
            }
            
            $workstation->department_id = $this->departmentId;
            if(!$workstation->save()) {
                throw new \Exception('Workstation cannot be saved: '.print_r($workstation->getErrors(), true));
            }
            
            $workstationTranslation->workstation_id = $workstation->id;
            $workstationTranslation->name = $this->name;
            $workstationTranslation->language_id = $languageId;
            if(!$workstationTranslation->save()) {
                throw new \Exception('Workstation translation cannot be saved: '.print_r($workstationTranslation->getErrors(), true));
            }
            
            $this->saveAssignedWorkers($workstation);
            $this->id = $workstation->id;
            $transaction->commit();
        }
        catch(\Exception $e) {
            $transaction->rollBack();
            Yii::error($e->getMessage());
            Yii::error($e->getTraceAsString());
        }
    }
    
    private function saveAssignedWorkers($workstation) {
        UserWorkstation::deleteAll(['workstation_id' => $workstation->id]);
        
        if(!empty($this->workerIds)) {
            $rowsAffected = Yii::$app->db->createCommand()
                    ->batchInsert('user_workstation', ['user_id', 'workstation_id'], array_map(function($workerId) use($workstation) {
                        return ['user_id' => $workerId, 'workstation_id' => $workstation->id];
                    }, $this->workerIds))->execute();
                    
            if($rowsAffected == 0) {
                throw new \Exception('Workstation workers cannot be saved: '.print_r($this->workerIds, true));
            }
        }
    }
    
    public function setAddScenario() {
        $this->setScenario(self::SCENARIO_ADD);
    }
    
    public function setEditScenario() {
        $this->setScenario(self::SCENARIO_EDIT);
    }
    
    public function isEditScenarioSet() {
        return $this->getScenario() == self::SCENARIO_EDIT;
    }
}