<?php

namespace frontend\models;

use Yii;
use yii\base\Model;
use common\models\ar\WarehouseProductOperation;
use common\helpers\Utility;
use common\models\ar\ProductStatus;

class WarehouseProductIssueForm extends Model {
	
	public $id;
	public $number;
	public $isAccounted;
	
	private $issueStatusIds;
	
	public function __construct($issue = null, $config = []) {
		if(!empty($issue)) {
			$this->loadData($issue);
		}
		
		$this->issueStatusIds = ProductStatus::find()->select('id')->where([
				'symbol' => [Utility::PRODUCT_STATUS_ISSUING, Utility::PRODUCT_STATUS_ISSUING_RESERVATION],				
		])->column();
		
		parent::__construct($config);
	}	
	
	public function rules() {
		return [
				[['id'], 'required'],
				[['id'], 'integer'],
				[['number', 'isAccounted'], 'safe'],
				[['number'], 'validateNumber'],
				[['number'], 'default', 'value' => null],
		];
	}
	
	public function validateNumber($attribute, $params) {
		if(!empty($this->number)) {
			$count = WarehouseProductOperation::find()->where([
						'number' => $this->number,
						'product_status_id' => $this->issueStatusIds,
					])
					->andWhere(['<>', 'id', $this->id])
					->count();
	
			if(intval($count) > 0) {
				$this->addError($attribute, Yii::t('web', 'There is another issue with such number.'));
			}
		}
	}
	
	public function attributeLabels() {
		return [
				'number' => Yii::t('web', 'Number'),
				'isAccounted' => Yii::t('web', 'Is accounted'),
		];
	}
	
	public function saveData() {
		$transaction = Yii::$app->db->beginTransaction();
		try {
			$wpo = WarehouseProductOperation::findOne($this->id);
			
			$wpo->number = $this->number;
			$wpo->is_accounted = $this->isAccounted;
			
			$wpo->save();
			$transaction->commit();
		}
		catch(\Exception $e) {
			$transaction->rollBack();
			throw new \Exception('Could not save edited data of issue');
		}
	}
	
	public function loadData($issue) {
		$this->id = $issue->id;
		$this->number = $issue->number;
		$this->isAccounted = $issue->is_accounted;
	}
}
