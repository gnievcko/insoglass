<?php

namespace frontend\models;

use Yii;
use yii\base\Model;

class WarehouseProductIssueListForm extends Model {
	
    public $page;
	public $sortDir;
    public $sortField;
    
    public $number;

    const DEFAULT_PAGE = 0;
    const DEFAULT_SORT_DIR = 'desc';
    const DEFAULT_SORT_FIELD = 'dateCreation';

    public function init() {
        parent::init();
        $this->page = self::DEFAULT_PAGE;
        $this->sortDir = self::DEFAULT_SORT_DIR;
        $this->sortField = self::DEFAULT_SORT_FIELD;
    }

    public static function getSortFields() { 
        return [
	            ['name' => 'number', 'label' => Yii::t('web', 'Number'), 'isSortable' => true,],
        		['name' => 'positionCount', 'label' => Yii::t('web', 'Number of positions'), 'isSortable' => true,],
        		['name' => 'userConfirmingName', 'label' => Yii::t('web', 'Issuing user'), 'isSortable' => true,],
        		['name' => 'userReceivingName', 'label' => Yii::t('web', 'Receiving user'), 'isSortable' => true,],
        		['name' => 'companyName', 'label' => Yii::t('web', 'Client'), 'isSortable' => true,],
        		['name' => 'isAccounted', 'label' => Yii::t('web', 'Is accounted'), 'isSortable' => true],
	            ['name' => 'dateCreation', 'label' => Yii::t('main', 'Creation date'), 'isSortable' => true,],
        ];
    }

    public function attributeLabels() {
        return [
        		'number' => Yii::t('web', 'Number'),
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
				[['page'], 'integer'],
				[['sortDir'], 'in', 'range' => ['asc', 'desc']],
				[['sortField'], 'in', 'range' => array_column(self::getSortFields(), 'name')],
				[['number'], 'safe'],
	            ['page', 'default', 'value' => self::DEFAULT_PAGE],
	            ['sortDir', 'default', 'value' => self::DEFAULT_SORT_DIR],
	            ['sortField', 'default', 'value' => self::DEFAULT_SORT_FIELD],
        ];
    }
}
