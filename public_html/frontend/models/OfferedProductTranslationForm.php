<?php

namespace frontend\models;

use Yii;
use yii\base\Model;

class OfferedProductTranslationForm extends Model {

    public $name;
    public $description;
    public $price;
    public $currencyId;
    public $languageId;
    public $unit;

    public function attributeLabels() {
        $priceLabel = '';
        if(Yii::$app->params['isOrderProductDimensionsVisible']) {
            $priceLabel = Yii::t(Yii::$app->params['isPlaceholderInputLabel'] ? 'web' : 'main', 'Price [per sq. m]');
        } else {
            $priceLabel = Yii::t('web', 'Price');
        }
        
        return [
            	'name' => \Yii::t('web', 'Product name'),
            	'description' => \Yii::t('web', 'Description'),
                'price' => $priceLabel,
            	'currencyId' => \Yii::t('main', 'Currency'),
        		'unit' => \Yii::t('main', 'Unit'),
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
	            [['name', 'languageId'], 'required'],
	            [['name'], 'string', 'max' => 2048],
	            [['description'], 'string', 'max' => 2048],
        		[['unit'], 'string', 'max' => 10],
	            [['price'], 'number'],
	            [['currencyId'], 'integer'],
	            [['languageId'], 'integer'],
        		[['unit'], 'default', 'value' => null],
        ];
    }
}
