<?php

namespace frontend\models;

use Yii;
use yii\base\Model;

class OfferedProductListForm extends Model {
	
    public $page;
	public $name;
    public $sortDir;
    public $sortField;
    public $categoryIds;
    
    public $allCategoriesCount;

    const DEFAULT_PAGE = 0;
    const DEFAULT_SORT_DIR = 'asc';
    const DEFAULT_SORT_FIELD = 'name';

    public function init() {
        parent::init();
        $this->page = self::DEFAULT_PAGE;
        $this->sortDir = self::DEFAULT_SORT_DIR;
        $this->sortField = self::DEFAULT_SORT_FIELD;
    }

    public static function getSortFields() {
        $tableHead = [];

        if(\Yii::$app->params['isProductPhotoOnListVisible']) {
            $tableHead = [['name' => 'photo', 'label' => Yii::t('main', 'Photo'), 'isSortable' => false, 'style' => 'width: 10%']];
        }

        $tableHead = array_merge($tableHead, [
            ['name' => 'name', 'label' => Yii::t('main', 'Name'), 'isSortable' => true],
        ]);
        if(\Yii::$app->params['isProductSymbolEditable']) {
            $tableHead = array_merge($tableHead, [['name' => 'symbol', 'label' => Yii::t('web', 'Code'), 'isSortable' => true]]);
        }
        
        $tableTail = [
            ['name' => 'category', 'label' => Yii::t('web', 'Categories'), 'isSortable' => true],
            ['name' => 'version', 'label' => Yii::t('web', 'Versions'), 'isSortable' => true],
            ['name' => 'isAvailable', 'label' => Yii::t('web', 'Is available'), 'isSortable' => true, 'style' => 'width: 11%'],
        ];
        
        return array_merge($tableHead, $tableTail);
    }

    public function attributeLabels() {
        return [
        		'name' => Yii::t('web', 'Product name'),
        		'categoryIds' => Yii::t('web', 'Categories'),
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
				[['page', 'allCategoriesCount'], 'integer'],
        		[['categoryIds'], 'each', 'rule' => ['integer']],
				[['sortDir'], 'in', 'range' => ['asc', 'desc']],
				[['sortField'], 'in', 'range' => array_column(self::getSortFields(), 'name')],
				[['name'], 'safe'],
	            ['page', 'default', 'value' => self::DEFAULT_PAGE],
	            ['sortDir', 'default', 'value' => self::DEFAULT_SORT_DIR],
	            ['sortField', 'default', 'value' => self::DEFAULT_SORT_FIELD],
        ];
    }
}
