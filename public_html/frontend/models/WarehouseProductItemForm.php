<?php

namespace frontend\models;

use Yii;
use yii\base\Model;
use common\models\ar\Product;
use common\models\ar\ProductPhoto;
use yii\helpers\Url;

class WarehouseProductItemForm extends Model {

	public $productId;
	
	public $urlThumbnail;
    public $name;
    public $count;
    public $unit;

    public function __construct($productId = 0, $config = []) {
    	if(!empty($productId)) {
    		$this->loadData($productId);
    	}
    	
    	parent::__construct($config);
    }
    
    public function attributeLabels() {
        return [
        		'productId' => Yii::t('main', 'Product ID'),
	        	'urlThumbnail' => Yii::t('main', 'Photo'),
	            'name' => Yii::t('main', 'Name'),
	            'count' => Yii::t('web', 'Write count'),
        		'unit' => Yii::t('main', 'Unit'),
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
	            [['productId', 'count'], 'required'],
        		[['unit'], 'string', 'max' => 10],
        		[['urlThumbnail', 'name'], 'safe'],
        		[['productId', 'count'], 'integer', 'min' => 1],
        ];
    }
    
    private function loadData($productId) {
    	$product = Product::findOne($productId);
    	$translation = $product->getTranslation();
    	$photo = ProductPhoto::find()->where('product_id = :productId', [':productId' => $productId])
    			->orderBy(['priority' => SORT_ASC, 'id' => SORT_ASC])->limit(1)->one();
    	
    	$name = !empty($translation) ? $translation->name : $product->symbol;
    	if(!empty($product->parent_product_id)) {
    		$parentTranslation = $product->parentProduct->getTranslation();
    		$parentName = !empty($parentTranslation) ? $parentTranslation->name : $product->parentProduct->symbol;
    		$name = $parentName.' ('.mb_strtolower($name).')';
    	}
    	
    	$unit = !empty($translation) && !empty($translation->unit_default) ? $translation->unit_default : mb_strtolower(Yii::t('web', 'Unit'), 'UTF-8'); 
    		
    	$this->name = $name;    	
    	$this->unit = $unit;
    	$this->productId = $productId;
    	$this->urlThumbnail = !empty($photo) ? $photo->url_thumbnail : Url::to('@web/images/placeholders/product.png');

    }
}
