<?php

namespace frontend\models;

use Yii;
use yii\base\Model;
use common\models\ar\Product;
use common\models\ar\ProductPhoto;

class OfferedProductRequiredProductForm extends Model {

    public $name;
    public $thumbnail;

	public $productId;
	public $count;
	public $unit;
	public $note;

    public function attributeLabels() {
        return [
        	'productId' => Yii::t('web', 'Product'),
        	'count' => Yii::t('web', 'Count'),
            'note' => Yii::t('web', 'Note'),
            'unit' => Yii::t('main', 'Unit'),
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
        	[['productId', 'count'], 'required'],
        	['note', 'string', 'max' => 1024],
    		[['unit'], 'string', 'max'=> 10],
        ];
    }

    public static function loadProductForm($productId) {
        $form = new OfferedProductRequiredProductForm();

    	$product = Product::findOne($productId);
    	$translation = $product->getTranslation();
    	$photo = ProductPhoto::find()->where('product_id = :productId', [':productId' => $productId])
    			->orderBy(['priority' => SORT_ASC, 'id' => SORT_ASC])->limit(1)->one();
    	
    	$unit = !empty($translation) && !empty($translation->unit_default) ? $translation->unit_default : mb_strtolower(Yii::t('web', 'Unit'), 'UTF-8');
    	
    	$form->productId = $productId;
    	$form->thumbnail = !empty($photo) ? $photo->url_thumbnail : '@web/images/placeholders/product.png';
    	$form->count = 1;
    	$form->unit = $unit;

        if(empty($translation)) {
            $form->name = $product->symbol;
        }
        else {
	        $name = !empty($translation) ? $translation->name : $product->symbol;
	    	if(!empty($product->parent_product_id)) {
	    		$parentTranslation = $product->parentProduct->getTranslation();
	    		$parentName = !empty($parentTranslation) ? $parentTranslation->name : $product->parentProduct->symbol;
	    		$name = $parentName.' ('.mb_strtolower($name).')';
	    	}
	    	
	    	$form->name = $name;
        }

        return $form;
    }
}
