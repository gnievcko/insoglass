<?php

namespace frontend\models;

use Yii;
use yii\base\Model;

class OrderEmployeesForm extends Model {

    public $employeesIds;

    public function attributeLabels() {
        return [
            'employeesIds' => Yii::t('main', 'Employees'),
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            [['employeesIds'], 'safe'],
        ];
    }
}
