<?php
namespace frontend\models;

use Yii;
use common\models\ar\OrderHistory;
use common\models\aq\OrderHistoryQuery;
use yii\base\Model;
use yii\helpers\BaseJson;
use common\models\ar\User;
use common\models\ar\Product;
use common\models\ar\OfferedProduct;
use common\models\ar\Currency;
use common\models\aq\OrderModificationHistoryQuery;
use common\models\ar\Company;
use common\models\ar\ContractType;
use common\models\ar\Entity;
use common\models\ar\Order;

class OrderHistoryChangeTracker extends Model{
	public $newHistory;
	public $previousHistory;
	
	private $userAssignmentDiff;
	private $result;
	
	public function initialize($newHistory) {
		$this->newHistory = $newHistory;
		$this->fetchOldHistory();	
	}
	
	private function fetchOldHistory() {
		$previousHistoryId = OrderHistoryQuery::getPreviousOrderHistory(
				$this->newHistory->id, $this->newHistory->order_id)['id'];
		$this->previousHistory = OrderHistory::findOne($previousHistoryId);	
	}  
	
	public function fetchUserAssignment($diff) {
		if(!empty($diff)){
			
			$array = [];
			foreach($diff as $employee) {
				$user = User::findOne($employee[0]);
				$array['user_id'] = $employee[0];
				$array['first_name'] = $user->first_name;
				$array['last_name'] = $user->last_name;
				$array['is_added'] = $employee[2];
				$this->userAssignmentDiff[] = $array;
			}
		}
		
	}
	
	public function compareAndSave(){
		$this->compareHistory();
		
		if(!empty($this->userAssignmentDiff)) {
			$this->result['userAssignment'] = $this->userAssignmentDiff;
		}
		
		$this->compareProducts();
		
		$this->newHistory->modification = BaseJson::encode($this->result);
		$this->newHistory->save();
		
	}
	
	private function compareHistory() {
		$append = [];
		$oldOrder = $this->previousHistory->getAttributes([
				'date_deadline',
				'date_reminder',
				'date_shipping'
		]);
		
		$newOrder = $this->newHistory->getAttributes([
				'date_deadline',
				'date_reminder',
				'date_shipping'
		]);
		
		foreach($oldOrder as $key => $value) {
			if ($value != $newOrder[$key]) {
				$append[$key] = [
						'old' => $value,
						'new' => $newOrder[$key]
				];
			}
		}
		
		if(!empty($append)) {
			$this->result['history'] = $append;
		}
	}
	private function compareProducts() {
		
	    $exceptAttributes = ['id', 'order_history_id', 'is_active', 'attribute_value'];
		$oldNormalProducts = [];
		$oldFakeProducts = [];
		foreach($this->previousHistory->orderOfferedProducts as $product) {
			if(!empty($product['product_id'])) {
			    $oldNormalProducts[] = $product->getAttributes(null, $exceptAttributes);
			} else {
				if($product->offeredProduct['is_artificial'] == 1) {
				    $record = $product->getAttributes(null, array_merge($exceptAttributes, ['offered_product_id']));
					$record['name'] = $product->offeredProduct->translation->name;
					$oldFakeProducts[] = $record;
				} else {
				    $oldNormalProducts[] = $product->getAttributes(null, $exceptAttributes);
				}
			}
		}
		
		$newNormalProducts = [];
		$newFakeProducts = [];
		foreach($this->newHistory->orderOfferedProducts as $product) {
			if(!empty($product['product_id'])) {
			    $newNormalProducts[] = $product->getAttributes(null, $exceptAttributes);
			} else {
				if($product->offeredProduct['is_artificial'] == 1) {
				    $record = $product->getAttributes(null, array_merge($exceptAttributes, ['offered_product_id']));
					$record['name'] = $product->offeredProduct->translation->name;
					$newFakeProducts[] = $record;
				} else {
				    $newNormalProducts[] = $product->getAttributes(null, $exceptAttributes);
				}
			}
		}
		
		$newNormalProducts = array_merge($newNormalProducts, $newFakeProducts);
		$oldNormalProducts = array_merge($oldNormalProducts, $oldFakeProducts);
		
		$oldNormalSize = count($oldNormalProducts);
		$newNormalSize = count($newNormalProducts);
		$removedProducts = [];
		for($i = 0; $i < $oldNormalSize; $i++) {
			$addToArray = true;
			for($j = 0; $j < $newNormalSize; $j++) {
				if(empty(array_diff($oldNormalProducts[$i], $newNormalProducts[$j]))) {
					$addToArray = false;
				}
			}
			
			if(empty($oldNormalProducts[$i]['offered_product_id']) && empty($oldNormalProducts[$i]['product_id'])) {
			    $addToArray = false;
			}
			
			if($addToArray) {
				$removedProducts[] = $oldNormalProducts[$i];
			}
			
		}
		
		$addedProducts = [];
		for($i = 0; $i < $newNormalSize; $i++) {
			$addToArray = true;
			for($j = 0; $j < $oldNormalSize; $j++) {
				if(empty(array_diff($newNormalProducts[$i], $oldNormalProducts[$j]))) {
					$addToArray = false;
				}
			}
			
			if($addToArray) {
				$addedProducts[] = $newNormalProducts[$i];
			}
		}	
		
		
		if(!empty($removedProducts)) {
			$result['removed'] = $removedProducts;
		}
		
		if(!empty($addedProducts)) {
			$result['added'] = $addedProducts;
		}
		
		if(!empty($result)) {
			$this->result['products'] = $result;
		}
	
	}
	
	public function getEntireHistory($orderId) {
		$historyData = OrderHistoryQuery::getOrderEntries($orderId);
		$historyHelps = [];
		foreach($historyData as $historyDatum) {
			$historyHelps[] = BaseJson::decode($historyDatum['modification']);
		}

		$index = 0;
		foreach($historyHelps as &$historyHelp) {
			if(!empty($historyHelp)) {

				if(!empty($historyHelp['products']['added'])) {
					foreach($historyHelp['products']['added'] as &$product) {
						if(!empty($product['product_id'])) {
							$product['name'] = Product::findOne($product['product_id'])->translation->name;
						} else if(!empty($product['offered_product_id'])) {
							$product['name'] = OfferedProduct::findOne($product['offered_product_id'])->translation->name;
						}
						$product['currencyShortSymbol'] = Currency::findOne($product['currency_id'])->short_symbol;
					}	
				}
				
				if(!empty($historyHelp['products']['removed'])) {
					foreach($historyHelp['products']['removed'] as &$product) {
						if(!empty($product['product_id'])) {
							$product['name'] = Product::findOne($product['product_id'])->translation->name;
						} else if(!empty($product['offered_product_id'])) {
							$product['name'] = OfferedProduct::findOne($product['offered_product_id'])->translation->name;
						}
						$product['currencyShortSymbol'] = Currency::findOne($product['currency_id'])->short_symbol;
					}
				}
				
			}
			
			$historyData[$index]['modification'] = $historyHelp;
			$index++;
			
		}
		
		return $historyData;
		
	}
	
	public static function getAllEditions($orderId) {
		$entries = OrderModificationHistoryQuery::findAllByOrderId($orderId);
		$editions = []; 
		
		foreach($entries as $entry) {
			$edition = BaseJson::decode($entry['modification']);
			$edition['target id'] = $entry['order_id'];
			if(!empty($edition['fields changed'])) {
				$editions[] = $edition;
			}
		}
			
		foreach($editions as &$edition) {

			$user = User::findOne($edition['user id']);
			$edition['user']['first_name'] = $user->first_name;
			$edition['user']['last_name'] = $user->last_name;
			$edition['user']['email'] = $user->email;
			$edition['user']['id'] = $edition['user id'];
			unset($edition['user id']);
			
			if(!empty($edition['fields changed']['company_id']['old'])) {
				$company = Company::findOne($edition['fields changed']['company_id']['old']);
				$edition['fields changed']['company']['old'] = $company;
			}
			
			if(!empty($edition['fields changed']['company_id']['new'])) {
				$company = Company::findOne($edition['fields changed']['company_id']['new']);
				$edition['fields changed']['company']['new'] = $company;
			}
			unset($edition['fields changed']['company_id']);
			
			if(!empty($edition['fields changed']['entity_id']['old'])) {
				$entity = Entity::findOne($edition['fields changed']['entity_id']['old']);
				$edition['fields changed']['entity']['old'] = $entity;
			}
			
			if(!empty($edition['fields changed']['entity_id']['new'])) {
				$entity = Entity::findOne($edition['fields changed']['entity_id']['new']);
				$edition['fields changed']['entity']['new'] = $entity;
			}
			unset($edition['fields changed']['entity_id']);
			
			if(!empty($edition['fields changed']['contract_type_id']['old'])) {
				$contractName = ContractType::findOne($edition['fields changed']['contract_type_id']['old'])->translation->name;
				$edition['fields changed']['contract_type']['old'] = $contractName;
			}
			
			if(!empty($edition['fields changed']['contract_type_id']['new'])) {
				$contractName = ContractType::findOne($edition['fields changed']['contract_type_id']['new'])->translation->name;
				$edition['fields changed']['contract_type']['new'] = $contractName;
			}
			unset($edition['fields changed']['contract_type_id']);
			
			if(!empty($edition['fields changed']['parent_order_id']['old'])) {
				$order = Order::findOne($edition['fields changed']['parent_order_id']['old']); 
				$edition['fields changed']['parent_order']['old']['name'] = $order->number . ' (' . $order->title . ')'; 
				$edition['fields changed']['parent_order']['old']['id'] = $edition['fields changed']['parent_order_id']['old'];
			}
			if(!empty($edition['fields changed']['parent_order_id']['new'])) {
				$order = Order::findOne($edition['fields changed']['parent_order_id']['new']);
				$edition['fields changed']['parent_order']['new']['name'] = $order->number . ' (' . $order->title . ')';
				$edition['fields changed']['parent_order']['new']['id'] = $edition['fields changed']['parent_order_id']['new'];
			}
			unset($edition['fields changed']['parent_order_id']);
		}
		return $editions;
	}
	
}
