<?php

namespace frontend\models;

use Yii;
use yii\base\Model;

class CompanyListForm extends Model {
	
    public $page;
	public $companyName;
    public $sortDir;
    public $sortField;
    public $companyGroupIds;
    public $companyType;
    public $cityName;
    public $addedBy;

    const DEFAULT_PAGE = 0;
    const DEFAULT_SORT_DIR = 'asc';
    const DEFAULT_SORT_FIELD = 'name';

    public function init() {
        parent::init();
        $this->page = self::DEFAULT_PAGE;
        $this->sortDir = self::DEFAULT_SORT_DIR;
        $this->sortField = self::DEFAULT_SORT_FIELD;
    }

    public static function getSortFields() { 
        return [
	            ['name' => 'name', 'label' => Yii::t('main', 'Name')],
	            ['name' => 'email', 'label' => Yii::t('main', 'E-mail address')],
	            ['name' => 'phone', 'label' => Yii::t('main', 'Phone')],
	            ['name' => 'address', 'label' => Yii::t('main', 'Address')],
	            ['name' => 'dateCreation', 'label' => Yii::t('main', 'Creation date')],
                ['name' => 'addedBy', 'label' => Yii::t('web', 'Added by')],
        ];
    }

    public function attributeLabels() {
        return [
        		'companyName' => Yii::t('web', 'Client name'),
        		'companyGroupIds' => Yii::t('web', 'Client groups'),
        		'cityName' => Yii::t('web', 'City name or zip code'),
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
				[['page'], 'integer'],
        		[['companyGroupIds'], 'each', 'rule' => ['integer']],
				[['sortDir'], 'in', 'range' => ['asc', 'desc']],
				[['sortField'], 'in', 'range' => array_column(self::getSortFields(), 'name')],
				[['companyName', 'companyType', 'addedBy', 'cityName'], 'safe'],
	            ['page', 'default', 'value' => self::DEFAULT_PAGE],
	            ['sortDir', 'default', 'value' => self::DEFAULT_SORT_DIR],
	            ['sortField', 'default', 'value' => self::DEFAULT_SORT_FIELD],
        ];
    }
}
