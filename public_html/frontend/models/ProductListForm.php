<?php

namespace frontend\models;

use Yii;
use yii\base\Model;

class ProductListForm extends Model {
	
    public $page;
	public $name;
	public $location;
    public $sortDir;
    public $sortField;
    public $categoryIds;
    
    public $allCategoriesCount;

    const DEFAULT_PAGE = 0;
    const DEFAULT_SORT_DIR = 'asc';
    const DEFAULT_SORT_FIELD = 'name';

    public function init() {
        parent::init();
        $this->page = self::DEFAULT_PAGE;
        $this->sortDir = self::DEFAULT_SORT_DIR;
        $this->sortField = self::DEFAULT_SORT_FIELD;
    }

    public static function getSortFields() {
        $fields = [];

        if(\Yii::$app->params['isProductPhotoOnListVisible']) {
            $fields = [['name' => 'photo', 'label' => Yii::t('main', 'Photo'), 'isSortable' => false, 'style' => 'width: 8%']];

        }

        $fields = array_merge($fields,[
	            ['name' => 'name', 'label' => Yii::t('main', 'Name'), 'isSortable' => true, 'style' => 'width: 20%'],
        		//['name' => 'index', 'label' => Yii::t('main', 'Index'), 'isSortable' => true],
        		['name' => 'stillageShelf', 'label' => Yii::t('web', 'Stillage/shelf'), 'isSortable' => true, 'style' => 'width: 10%'],
        		//['name' => 'alias', 'label' => Yii::t('web', 'Common names'), 'isSortable' => true],
        		['name' => 'supplier', 'label' => Yii::t('web', 'Supplier'), 'isSortable' => true],
        		['name' => 'category', 'label' => Yii::t('web', 'Categories'), 'isSortable' => true],
        		['name' => 'version', 'label' => Yii::t('web', 'Versions'), 'isSortable' => true]]);

        if(\Yii::$app->params['isProductCanBeOffered']) {
            $fields = array_merge($fields, [['name' => 'isVisibleForClient', 'label' => Yii::t('main', 'Is visible for client'), 'isSortable' => true, 'style' => 'width: 10%']]);
        }

        $fields = array_merge($fields,[['name' => 'dateCreation', 'label' => Yii::t('main', 'Creation date'), 'isSortable' => true, 'style' => 'width: 10%']]
        );

        return $fields;
    }

    public function attributeLabels() {
        return [
        		'name' => Yii::t('web', 'Product name'),
        		'categoryIds' => Yii::t('web', 'Categories'),
        		'location' => Yii::t('web', 'Stillage/shelf'),
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
				[['page', 'allCategoriesCount'], 'integer'],
        		[['categoryIds'], 'each', 'rule' => ['integer']],
				[['sortDir'], 'in', 'range' => ['asc', 'desc']],
				[['sortField'], 'in', 'range' => array_column(self::getSortFields(), 'name')],
				[['name', 'location', 'stillageShelf'], 'safe'],
	            ['page', 'default', 'value' => self::DEFAULT_PAGE],
	            ['sortDir', 'default', 'value' => self::DEFAULT_SORT_DIR],
	            ['sortField', 'default', 'value' => self::DEFAULT_SORT_FIELD],
        ];
    }
}
