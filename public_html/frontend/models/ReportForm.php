<?php

namespace frontend\models;

use Yii;
use yii\base\Model;

class ReportForm extends Model {

    public $type;
	public $dateFrom;
	public $dateTo;


    public function init() {
        parent::init();
		$this->dateFrom = date('d.m.Y', time()-7*24*60*60);
		$this->dateTo = date('d.m.Y', time());
    }

    public function attributeLabels() {
        return [
        		'type' => Yii::t('web', 'Range'),
				'dateFrom' => Yii::t('web', 'Date from'),
				'dateTo' => Yii::t('web', 'Date to'),
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
				[['type'], 'integer'],
                [['dateFrom', 'dateTo'], 'required'],
                [['dateFrom','dateTo'], 'validateDates'],

        ];
    }
    public function validateDates() {
        if(strtotime($this->dateTo) < strtotime($this->dateFrom)){
            $this->addError('dateFrom', Yii::t('documents', 'Date "to" cannot be earlier than date "from"'));
        }
    }
}
