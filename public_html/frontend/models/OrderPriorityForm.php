<?php

namespace frontend\models;

use Yii;
use yii\base\Model;
use common\models\ar\Order;
use common\models\aq\OrderPriorityQuery;
use common\helpers\Utility;

class OrderPriorityForm extends Model {

    public $orderId;
    public $orderPriorityId;
    public $orderPriorityCheckbox;

	public function rules() {
		return [
			[['orderId'], 'required'],
		    //[['orderPriorityId'], 'required'],
		    [['orderPriorityCheckbox'], 'safe'],
		    
		    [['orderPriorityId'], 'safe'],
		];
	}

    public function attributeLabels() {
        return [
            'orderPriorityId' => Yii::t('main', 'Priority'),
            'orderPriorityCheckbox' => Yii::t('web', 'High priority'),
        ];
    }

	public function saveData() {
		$transaction = Yii::$app->db->beginTransaction();
		try {
		    if(empty($this->orderPriorityId)) {
		        $this->orderPriorityId = OrderPriorityQuery::getIdBySymbol(Utility::ORDER_PRIORITY_MEDIUM);
		        if(!empty($this->orderPriorityCheckbox)) {
                    $this->orderPriorityId = OrderPriorityQuery::getIdBySymbol(Utility::ORDER_PRIORITY_HIGH); 
		        }
		    }
		    
			$order = Order::findOne($this->orderId);
			$order->order_priority_id = $this->orderPriorityId;
			$order->save();

			Yii::info('Priority of the order '.$order->id.' has been changed to '.$this->orderPriorityId.' by '.Yii::$app->user->id, 'info');
			$transaction->commit();
		}
		catch(\Exception $e) {
			$transaction->rollBack();
			Yii::error($e->getMessage());
			Yii::error($e->getTraceAsString());
			throw new \Exception('Could not save order priority to production task '.$this->productionTaskId);
		}
	}
}
