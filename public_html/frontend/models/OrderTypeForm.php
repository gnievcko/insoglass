<?php

namespace frontend\models;

use Yii;
use yii\base\Model;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use common\models\ar\ProductCategory;
use common\models\ar\ProductCategoryTranslation;
use common\models\ar\Language;
use common\helpers\RandomSymbolGenerator;
use common\models\ar\OrderTypeTranslation;
use common\models\ar\OrderType;

class OrderTypeForm extends Model {

	const SCENARIO_ADD = 'add';
	const SCENARIO_EDIT = 'edit';

    public $id;
    public $name;

	public function __construct($orderType = null, $config = []) {
		if(!empty($orderType)) {
			$this->loadData($orderType);
		}

		parent::__construct($config);
	}

	public function scenarios() {
		$scenarios = parent::scenarios();
		$scenarios[self::SCENARIO_ADD] = ['name'];
		$scenarios[self::SCENARIO_EDIT] = ['id', 'name'];
		return $scenarios;
	}

	public function rules() {
		return [
				[['name'], 'required'],
				[['name'], 'string', 'max' => 128],
				[['name'], 'validateName'],
		];
	}

	public function validateName ($attribute, $params) {
		$languageId = Language::find()->where('symbol = :symbol', ['symbol' => Yii::$app->language])->one()->id;
		if ($this->isEditScenarioSet()) {
			if(!empty(OrderTypeTranslation::find()->where('name = :name', [':name' => $this->name])
						->andWhere('language_id = :languageId', [':languageId' => $languageId])
						->andWhere('order_type_id != :id', [':id' => $this->id])
						->one())) {
				$this->addError('name', Yii::t('web', 'Name already exists'));
			}
		}
		else {
			if(!empty(OrderTypeTranslation::find()->where('name = :name', [':name' => $this->name])
						->andWhere('language_id = :id', [':id' => $languageId])
						->one())) {
				$this->addError('name', Yii::t('web', 'Name already exists'));
			}
		}
	}

    public function attributeLabels() {
        return [
            	'id' => Yii::t('main', 'ID'),
            	'name' => Yii::t('main', 'Name'),

        ];
    }

	public function saveData() {
		$languageId = Language::find()->where('symbol = :symbol', ['symbol' =>Yii::$app->language])->one()->id;
		if(!empty($this->id)) {
			$type = OrderType::findOne($this->id);
			$typeTranslation = $type->getTranslation();
		}

		if(empty($type)) {
			$type = new OrderType();
		}
		if(empty($typeTranslation)) {
			$typeTranslation = new OrderTypeTranslation();
		}

		$transaction = Yii::$app->db->beginTransaction();
		try {
			if(!$this->isEditScenarioSet()) {
				$type->symbol = RandomSymbolGenerator::generate($type, 'symbol', 32);
			}

			if(!$type->save()) {
				throw new \Exception('Order type cannot be saved');	
			}
			
			$this->id = $type->id;

			$typeTranslation->order_type_id = $this->id;
			$typeTranslation->language_id = $languageId;
			$typeTranslation->name = $this->name;

			$typeTranslation->save();
			$transaction->commit();
		}
		catch(\Exception $e) {
			$transaction->rollBack();
			Yii::error($e->getMessage());
			Yii::error($e->getTraceAsString());
			throw new \Exception('Could not save order type data');
		}
	}

    public function loadData($orderType) {
    	$translation = $orderType->getTranslation();
    	$this->id = $orderType->id;
    	$this->name = $translation->name;
    }

    public function setAddScenario() {
    	$this->setScenario(self::SCENARIO_ADD);
    }

    public function setEditScenario() {
    	$this->setScenario(self::SCENARIO_EDIT);
    }

    public function isEditScenarioSet() {
    	return $this->getScenario() == self::SCENARIO_EDIT;
    }
}
