<?php

namespace frontend\models;

use Yii;
use yii\base\Model;
use common\helpers\StringHelper;

class DepartmentProductOrderListForm extends DepartmentOrdersListForm {

    public static function getSortFields() {
        return [
            ['name' => 'productCode', 'label' => Yii::t('web', 'Product code'), 'isSortable' => true],
            ['name' => 'completedCount', 'label' => Yii::t('web', 'Number of completed'), 'isSortable' => true],
            ['name' => 'progress', 'label' => Yii::t('web', 'Progress'), 'isSortable' => false],
            //['name' => 'durationTime', 'label' => Yii::t('web', 'Duration') . ' [h]', 'isSortable' => true],
            ['name' => 'deadline', 'label' => Yii::t('web', 'Deadline'), 'isSortable' => true],
            ['name' => 'qualityNote', 'label' => Yii::t('web', 'Quality note'), 'isSortable' => false],
        ];
    }
}