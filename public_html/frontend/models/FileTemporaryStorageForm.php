<?php

namespace frontend\models;

use yii\base\Model;

abstract class FileTemporaryStorageForm extends Model {

    public static function getExtensionsString($extensionPrefix = '') {
        $prefixedExtensions = array_map(function($fileExtension) use($extensionPrefix) {
            return "{$extensionPrefix}{$fileExtension}";
        }, static::$ALLOWED_EXTENSIONS);

        return implode(', ', $prefixedExtensions);
    }
}
