<?php

namespace frontend\models;

use Yii;
use yii\base\Model;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use yii\db\Query;
use common\models\ar\Warehouse;
use frontend\helpers\AddressHelper;

class WarehouseForm extends Model {
	
	const SCENARIO_ADD = 'add';
	const SCENARIO_EDIT = 'edit';
	
	public $id;
    public $name;
    public $index;
	public $parentWarehouseId;
	public $isActive;
	public $dateCreation;
	
	public $address;
	public $city;
	public $zipCode;
	public $provinceId;

	public function __construct($warehouseSupplier = null, $config = []) {
		if(!empty($warehouseSupplier)) {
			$this->loadData($warehouseSupplier);
		}
	
		parent::__construct($config);
	}
	
	public function scenarios() {
		$scenarios = parent::scenarios();
		$scenarios[self::SCENARIO_ADD] = ['name', 'index', 'parentWarehouseId', 'isActive', 'dateCreation', 'address', 'city', 'zipCode', 'provinceId'];
		$scenarios[self::SCENARIO_EDIT] = ['id', 'name', 'index', 'warehouseId', 'isActive', 'dateCreation', 'address', 'city', 'zipCode', 'provinceId'];
		return $scenarios;
	}
	
	public function rules() {
		return [				
				[['name'], 'required'],
				[['parentWarehouseId', 'is_active'], 'integer'],
				[['name'], 'string', 'max' => 64],
				[['index'], 'string', 'max' => 48],
				[['name'], 'validateUniqueness'],
				[['address', 'city', 'zipCode', 'provinceId'], 'validateAddress'],
				[['index'], 'default', 'value' => null],
		];
	}
	
	public function validateUniqueness(){
		$query = (new Query())
    			->select(['name'])
    			->from('warehouse')
    			->where(['name' => $this->name]);
		if ($this->isEditScenarioSet()){
    		$query->andWhere('id != :id', [':id' => $this->id]);
		}		
	
		if (($query->count() != 0)){
			$this->addError('name', Yii::t('web', 'Name already exists'));
		}
	}
	
	public function validateAddress($attribute, $params) {
		$sum = !empty($this->address) + !empty($this->city) + !empty($this->zipCode) + !empty($this->provinceId);
		if($sum > 0 && $sum < 4) {
			$this->addError('address', Yii::t('web', 'Address is incomplete.'));
			$this->addError('city', '');
			$this->addError('zipCode', '');
			$this->addError('provinceId', '');
		}
	}
	
    public function attributeLabels() {
        return [
        		'name' => Yii::t('main', 'Name'),
        		'index' => Yii::t('main', 'Warehouse index'),
	            'parentWarehouseId' => Yii::t('main', 'Parent warehouse'),
	            'address' => Yii::t('main', 'Address'),
	        	'city' => Yii::t('main', 'City'),
        		'zipCode' => Yii::t('main', 'Zip code'),
        		'provinceId' => Yii::t('web', 'Select voivodeship'),
        ];
    }
    
	public function saveData() {
		if(!empty($this->id)) {
			$warehouse = Warehouse::findOne($this->id);
		}		
		if(empty($warehouse)) {
			$warehouse = new Warehouse();
		}
		
		$transaction = Yii::$app->db->beginTransaction();
		try {
			
			$warehouse->name = $this->name;
			$warehouse->index = $this->index;
			$warehouse->parent_warehouse_id = $this->parentWarehouseId;
			$warehouse->is_active = 1;

			$warehouse->address_id = AddressHelper::getAddressId([
					'address' => $this->address, 'city' => $this->city,
					'zipCode' => $this->zipCode, 'provinceId' => $this->provinceId
			]);
			
			$warehouse->save();
			
			// potrzebne do przekierowania
			$this->id = $warehouse->id;
			
			$transaction->commit();
		}
		catch(\Exception $e) {
			$transaction->rollBack();
			throw new \Exception('Could not save address');
		}		
	}
	
    public function loadData($warehouse) {
    	$this->id = $warehouse->id;
    	$this->name = $warehouse->name;
    	$this->index = $warehouse->index;
    	$this->parentWarehouseId = $warehouse->parent_warehouse_id;
    	$this->dateCreation = $warehouse->date_creation;
    	
    	$address = $warehouse->address;
    	if(!empty($address)) {
    		$this->address = $address->main.(!empty($address->complement) ? ' '.$address->complement : '');
    		$this->city = $address->city->name;
    		$this->zipCode = $address->city->zip_code;
    	
    		if(!empty($address->city->province)) {
    			$this->provinceId = $address->city->province->id;
    		}
    	}	
    	
    }

    public function setAddScenario() {
    	$this->setScenario(self::SCENARIO_ADD);
    }
    
    public function setEditScenario() {
    	$this->setScenario(self::SCENARIO_EDIT);
    }
    
    public function isEditScenarioSet() {
    	return $this->getScenario() == self::SCENARIO_EDIT;
    }
}