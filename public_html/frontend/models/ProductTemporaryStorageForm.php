<?php

namespace frontend\models;

use common\helpers\UploadHelper;
use Yii;

class ProductTemporaryStorageForm extends FileTemporaryStorageForm {

    public $file;
    public static $ALLOWED_EXTENSIONS = ['*'];

    public function __construct($config = array()) {
        parent::__construct($config);
        //self::$ALLOWED_EXTENSIONS = [Yii::t('main', 'All')];
    }
    
    public function rules() {
        return [
            //[['file'], 'file', 'skipOnEmpty' => false, 'extensions' => self::$ALLOWED_EXTENSIONS],
            [
                ['file'],
                'file', 
                'skipOnEmpty' => false,
                'maxSize' => \frontend\helpers\AdditionalFieldHelper::getMaxFileSize(),
                'tooBig' => Yii::t('main', 'File is too large'),
                'wrongExtension' => Yii::t('main','Only {extensions} types are allowed')
            ],
        ];
    }

    public function upload() {
        if ($this->validate()) {
            $documentPath = UploadHelper::saveTemporaryFile($this->file);

            $originalDocumentName = "{$this->file->getBaseName()}.{$this->file->getExtension()}";
            $documentDetails = UploadHelper::storeInformationAboutUploadedFile($documentPath['name'], $originalDocumentName);

            return $documentDetails;
        } else {
            return null;
        }
    }

}
