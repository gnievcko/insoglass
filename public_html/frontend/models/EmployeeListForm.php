<?php

namespace frontend\models;

use Yii;
use yii\base\Model;
use common\models\ar\User;

class EmployeeListForm extends Model {
	
    public $page;
    public $sortDir;
    public $sortField;
    
    public $roleIds;
    public $filterString;
    public $allRolesCount;

    const DEFAULT_PAGE = 0;
    const DEFAULT_SORT_DIR = 'asc';
    const DEFAULT_SORT_FIELD = 'name';

    public function init() {
        parent::init();
        $this->page = self::DEFAULT_PAGE;
        $this->sortDir = self::DEFAULT_SORT_DIR;
        $this->sortField = self::DEFAULT_SORT_FIELD;
    }

    public static function getSortFields() { 
        return [
	            ['name' => 'url_photo', 'label' => Yii::t('main', 'Photo'), 'isSortable' => false],
        		['name' => 'first_name', 'label' => Yii::t('main', 'First name'), 'isSortable' => true],
	            ['name' => 'last_name', 'label' => Yii::t('main', 'Last name'), 'isSortable' => true],
        		['name' => 'role', 'label' => Yii::t('main', 'Role'), 'isSortable' => false],
        		['name' => 'email', 'label' => Yii::t('main', 'E-mail'), 'isSortable' => true],
        		['name' => 'phone', 'label' => Yii::t('web', 'Phone number'), 'isSortable' => true],
        ];
    }

    public function attributeLabels() {
        return [
        		'Filter' => Yii::t('web', 'Filter'),
        		'roleIds' => Yii::t('main', 'Role'),
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
				[['page', 'allRolesCount'], 'integer'],
				[['sortDir'], 'in', 'range' => ['asc', 'desc']],
        		[['roleIds'], 'each', 'rule' => ['integer']],
				[['sortField'], 'in', 'range' => array_column(self::getSortFields(), 'name')],    		
				[['filterString'], 'safe'],
	            ['page', 'default', 'value' => self::DEFAULT_PAGE],
	            ['sortDir', 'default', 'value' => self::DEFAULT_SORT_DIR],
	            ['sortField', 'default', 'value' => self::DEFAULT_SORT_FIELD],
        ];
    }
}