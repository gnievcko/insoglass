<?php
namespace frontend\models;

use common\models\ar\User;
use yii\base\InvalidParamException;
use yii\base\Model;
use Yii;
use common\helpers\StrengthValidator;
/**
 * Password reset form
 */
class ResetPasswordForm extends Model {

	public $password;
	public $password2;
	/**
	 * @var \common\models\ar\User
	 */
	private $_user;

	/**
	 * @var string
	 */
	private $_token;

	/**
	 * Creates a form model given a token.
	 *
	 * @param  string                          $token
	 * @param  array                           $config name-value pairs that will be used to initialize the object properties
	 * @throws \yii\base\InvalidParamException if token is empty or not valid
	 */
	public function __construct($token, $config = []) {
		if(empty($token) || !is_string($token)) {
			throw new InvalidParamException(Yii::t('web', 'Token cannot be blank.'));
		}

		$this->_user = User::findByPasswordResetToken($token);

		if(!$this->_user) {
			throw new InvalidParamException(Yii::t('web', 'Your token is invalid or expired.'));
		}

		$this->_token = $token;

		parent::__construct($config);
	}

	/**
	 * @inheritdoc
	 */
	public function rules() {
		return [
				[['password', 'password2'], 'required'],
				['password2', 'compare', 'compareAttribute' => 'password', 'message' => Yii::t('web', 'New password must be repeated correctly')],
                [['password'], StrengthValidator::className()]+Yii::$app->params['strengthPasswordConfig']
		];
	}

	/**
	 * @inheritdoc
	 */
	public function attributeLabels() {
		return [
				'password' => Yii::t('web', 'New password'),
    			'password2' => Yii::t('web', 'Repeat new password'),
		];
	}

	/**
	 * Resets password.
	 *
	 * @return boolean if password was reset.
	 */
	public function resetPassword() {
		if($this->validate() == false)
			return false;
		$user = $this->_user;
		$user->setPassword($this->password);
		$user->removePasswordResetToken($this->_token);

		return $user->save(false);
	}
}