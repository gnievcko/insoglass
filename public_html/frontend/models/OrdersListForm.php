<?php

namespace frontend\models;

use Yii;
use yii\base\Model;
use common\components\MessageProvider;
use common\models\ar\Address;
use common\models\ar\City;
use common\models\ar\Company;
use common\models\ar\User;
use common\models\ar\UserAction;
use common\models\ar\Language;
use common\helpers\Utility;
use common\models\ar\Currency;
use common\models\ar\DistributionChannel;
use common\models\ar\Province;
use common\helpers\StringHelper;

class OrdersListForm extends Model {

    public $page;
    public $orderNumber;
    public $orderName;
    public $orderClientId;
    public $orderClientName;
    public $orderParentClientName;
    public $orderStatusesIds;
    public $cityName;
    public $dateFrom;
    public $dateTo;
    public $sortDir;
    public $sortField;
    public $orderType;

    const DEFAULT_PAGE = 0;
    const DEFAULT_SORT_DIR = 'desc';
    const DEFAULT_SORT_FIELD = 'create_date';

    public function init() {
        parent::init();
        $this->page = self::DEFAULT_PAGE;
        $this->sortDir = self::DEFAULT_SORT_DIR;
        $this->sortField = self::DEFAULT_SORT_FIELD;
    }

    public static function getSortFields() {
        $fields = [
            ['name' => 'number', 'label' => Yii::t('web', 'Number')],
            ['name' => 'name', 'label' => Yii::$app->params['isOrderTitleIsLabeledAsConcerns'] ? Yii::t('web', 'Concerns') : (Yii::$app->params['isOrderTitleLabeledAsOrderName'] ? Yii::t('web', 'Job name') : Yii::t('web', 'Title'))],
            ['name' => 'create_date', 'label' => Yii::t('main', 'Creation date'), 'style' => 'width: 8%'],
            ['name' => 'date_shipping', 'label' => Yii::t('web', 'Date shipping'), 'style' => 'width: 8%'],
            ['name' => 'client', 'label' => Yii::t('main', 'Client')],
                //['name' => 'modification_date', 'label' => Yii::t('main', 'Modification date'), 'style' => 'width: 8%'],
        ];
        $contract_type = [['name' => 'contract_type', 'label' => Yii::t('main', 'Contract type'), 'style' => 'width: 8%']];
        if(Yii::$app->params['isContractTypeVisible']) {
            $fields = array_merge($fields, $contract_type);
        }

        $fields[] = ['name' => 'status', 'label' => Yii::t('main', 'Status')];

        $files = [['name' => 'files', 'label' => Yii::t('web', 'Files'), 'isSortable' => false]];
        if(Yii::$app->params['isFileVisibleInOrderList']) {
            $fields = array_merge($fields, $files);
        }

        return $fields;
    }

    public function attributeLabels() {
        return [
            'orderNumber' => StringHelper::translateOrderToOffer('web', 'Order number'),
            'orderName' => Yii::$app->params['isOrderTitleIsLabeledAsConcerns'] ? Yii::t('web', 'Concerns') : (Yii::$app->params['isOrderTitleLabeledAsOrderName'] ? Yii::t('web', 'Job name') : StringHelper::translateOrderToOffer('web', 'Order name')),
            'orderStatusesIds' => StringHelper::translateOrderToOffer('main', 'Order status'),
            'orderClientId' => Yii::t('main', 'Client'),
            'orderClientName' => Yii::t('web', 'Client (verbally)'),
            'orderParentClientName' => Yii::t('main', 'Parent company'),
            'orderType' => StringHelper::translateOrderToOffer('web', 'Order type'),
            'contractType' => StringHelper::translateOrderToOffer('web', 'Contract type'),
            'cityName' => Yii::t('web', 'City name or zip code'),
            'dateFrom' => Yii::t('web', 'Created after (inclusive)'),
            'dateTo' => Yii::t('web', 'Created before (inclusive)'),
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            [['page', 'orderClientId'], 'integer'],
            [['orderStatusesIds'], 'each', 'rule' => ['integer']],
            [['sortDir'], 'in', 'range' => ['asc', 'desc']],
            [['sortField'], 'in', 'range' => array_column(self::getSortFields(), 'name')],
            [['orderName', 'orderType', 'orderNumber', 'orderClientName', 'cityName', 'dateFrom', 'dateTo',
            'orderParentClientName'], 'safe'],
            ['page', 'default', 'value' => self::DEFAULT_PAGE],
            ['sortDir', 'default', 'value' => self::DEFAULT_SORT_DIR],
            ['sortField', 'default', 'value' => self::DEFAULT_SORT_FIELD],
        ];
    }

}
