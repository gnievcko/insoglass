<?php

namespace frontend\models;

use Yii;
use yii\base\Model;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use common\models\ar\ProductCategory;
use common\models\ar\ProductCategoryTranslation;
use common\models\ar\Language;
use common\helpers\RandomSymbolGenerator;

class ProductCategoryForm extends Model {

	const SCENARIO_ADD = 'add';
	const SCENARIO_EDIT = 'edit';

    public $id;
    public $name;
	public $parentCategoryId;
	public $description;

	public function __construct($productCategory = null, $config = []) {
		if(!empty($productCategory)) {
			$this->loadData($productCategory);
		}

		parent::__construct($config);
	}

	public function scenarios() {
		$scenarios = parent::scenarios();
		$scenarios[self::SCENARIO_ADD] = ['name', 'parentCategoryId', 'description'];
		$scenarios[self::SCENARIO_EDIT] = ['id', 'name', 'parentCategoryId', 'description'];
		return $scenarios;
	}

	public function rules() {
		return [
				[['name'], 'required'],
				[['parent_category_id'], 'integer'],
				[['description'], 'string', 'max' => 1024],
				[['name'], 'validateName'],
				[['description'], 'default', 'value' => null],
				[['parent_category_id'], 'exist', 'skipOnError' => true, 'targetClass' => ProductCategory::className(), 'targetAttribute' => ['parent_category_id' => 'id']]
		];
	}

	public function validateName ($attribute, $params) {
		$languageId = Language::find()->where('symbol = :symbol', ['symbol' => Yii::$app->language])->one()->id;
		if ($this->isEditScenarioSet()) {
			if (!empty(ProductCategoryTranslation::find()->where('name = :name', [':name' => $this->name])
				->andWhere('language_id = :languageId', [':languageId' => $languageId])
				->andWhere('product_category_id != :id', [':id' => $this->id])->one())) {
					$this->addError('name', Yii::t('web', 'Name already exists'));
			}
		}
		else {
			if (!empty(ProductCategoryTranslation::find()->where('name = :name', [':name' => $this->name])
								->andWhere('language_id = :id', [':id' => $languageId])->one())) {
				$this->addError('name', Yii::t('web', 'Name already exists'));
			}
		}
	}

    public function attributeLabels() {
        return [
        		'parentCategoryId' => Yii::t('main', 'Parent Category'),
            	'id' => Yii::t('main', 'ID'),
            	'name' => Yii::t('main', 'Name'),
            	'parent_category_id' => Yii::t('main', 'Parent Category'),
        		'description' => Yii::t('main', 'Description'),

        ];
    }

	public function saveData() {
		$languageId = Language::find()->where('symbol = :symbol', ['symbol' =>Yii::$app->language])->one()->id;
		if(!empty($this->id)) {
			$productCategory = ProductCategory::findOne($this->id);
			$productCategoryTranslation =
				ProductCategoryTranslation::find()->where('product_category_id = :id', [':id' => $this->id])
				->andWhere('language_id = :languageId', [':languageId' => $languageId])->one();

		}

		if(empty($productCategory)) {
			$productCategory = new ProductCategory();
		}
		if(empty($productCategoryTranslation)) {
			$productCategoryTranslation = new ProductCategoryTranslation();
		}

		$transaction = Yii::$app->db->beginTransaction();
		try {
			if (!$this->isEditScenarioSet()) {
				$productCategory->symbol = RandomSymbolGenerator::generate($productCategory, 'symbol', 32);
			}

			$productCategory->parent_category_id = $this->parentCategoryId;
			$productCategory->save();
			$this->id = $productCategory->id;

			$productCategoryTranslation->product_category_id = $productCategory->id;
			$productCategoryTranslation->language_id = $languageId;
			$productCategoryTranslation->name = $this->name;
			$productCategoryTranslation->description = $this->description;

			$productCategoryTranslation->save();
			$transaction->commit();
		}
		catch(\Exception $e) {
			$transaction->rollBack();
			throw new \Exception('Could not save product category data');
		}
	}

    public function loadData($productCategory) {
    	$languageId = Language::find()->where('symbol = :symbol', ['symbol' =>Yii::$app->language])->one()->id;
    	$this->id = $productCategory->id;
    	$productCategoryTranslation = ProductCategoryTranslation::find()
    		->where('product_category_id = :id', [':id' => $this->id])
    		->andWhere('language_id = :languageId', [':languageId' => $languageId])->one();
    	$this->parentCategoryId = $productCategory->parent_category_id;
    	$this->description = $productCategoryTranslation->description;
    	$this->name = $productCategoryTranslation->name;
    }

    public function setAddScenario() {
    	$this->setScenario(self::SCENARIO_ADD);
    }

    public function setEditScenario() {
    	$this->setScenario(self::SCENARIO_EDIT);
    }

    public function isEditScenarioSet() {
    	return $this->getScenario() == self::SCENARIO_EDIT;
    }
}
