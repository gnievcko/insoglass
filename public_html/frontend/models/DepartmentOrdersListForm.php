<?php

namespace frontend\models;

use Yii;
use yii\base\Model;
use common\helpers\StringHelper;

class DepartmentOrdersListForm extends Model {
    
    public $page;
    public $sortDir;
    public $sortField;
    
    public $departmentId;
    
    const DEFAULT_PAGE = 0;
    const DEFAULT_SORT_DIR = 'desc';
    const DEFAULT_SORT_FIELD = 'priority';

    public function init() {
        parent::init();
        $this->page = self::DEFAULT_PAGE;
        $this->sortDir = self::DEFAULT_SORT_DIR;
        $this->sortField = self::DEFAULT_SORT_FIELD;
    }
    
    public static function getSortFields() {
        return [
            ['name' => 'customerName', 'label' => Yii::t('web', 'Customer name'), 'isSortable' => true],
            ['name' => 'orderNumber', 'label' => StringHelper::translateOrderToOffer('web', 'Order number'), 'isSortable' => true],
            ['name' => 'productCode', 'label' => Yii::t('web', 'Product code'), 'isSortable' => true],
            ['name' => 'deadline', 'label' => Yii::t('web', 'Deadline'), 'isSortable' => true],
            ['name' => 'completedCount', 'label' => Yii::t('web', 'Number of completed'), 'isSortable' => true],
            ['name' => 'priority', 'label' => Yii::t('main', 'Priority'), 'isSortable' => true],
            ['name' => 'qualityNote', 'label' => Yii::t('web', 'Quality note'), 'isSortable' => false],
        ];
    }
    
    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            [['page', 'departmentId'], 'integer'],
            [['sortDir'], 'in', 'range' => ['asc', 'desc']],
            [['sortField'], 'in', 'range' => array_column(self::getSortFields(), 'name')],
            ['page', 'default', 'value' => self::DEFAULT_PAGE],
            ['sortDir', 'default', 'value' => self::DEFAULT_SORT_DIR],
            ['sortField', 'default', 'value' => self::DEFAULT_SORT_FIELD],
        ];
    }
}