<?php

namespace frontend\models;

use Yii;
use yii\base\Model;
use common\models\ar\Product;
use common\models\ar\ProductPhoto;

class WarehouseDeliveryProductForm extends Model {

    const PRICE_TYPE_PER_UNIT = 'PRICE_TYPE_PER_UNIT';
    const PRICE_TYPE_PER_PACKAGE = 'PRICE_TYPE_PER_PACKAGE';

    public $name;
    public $thumbnail;

	public $productId;
	public $count;
	public $price;
    public $priceType;
    public $unit;
    public $note;

    public function attributeLabels() {
        return [
            	'productId' => \Yii::t('web', 'Product'),
            	'count' => \Yii::t('web', 'Count'),
            	'price' => \Yii::t('web', 'Price'),
            	'note' => \Yii::t('web', 'Note'),
        		'unit' => \Yii::t('main', 'Unit'),
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            	[['productId', 'count', 'price', 'priceType'], 'required'],
            	['note', 'string', 'max' => 1024],
        		[['unit'], 'string', 'max'=> 10],
        ];
    }

    public static function loadProductForm($productId) {
        $form = new WarehouseDeliveryProductForm();

    	$product = Product::findOne($productId);
    	$translation = $product->getTranslation();
    	$photo = ProductPhoto::find()->where('product_id = :productId', [':productId' => $productId])
    			->orderBy(['priority' => SORT_ASC, 'id' => SORT_ASC])->limit(1)->one();
    	
    	$unit = !empty($translation) && !empty($translation->unit_default) ? $translation->unit_default : mb_strtolower(Yii::t('web', 'Unit'), 'UTF-8');
    	
    	$form->productId = $productId;
    	$form->thumbnail = !empty($photo) ? $photo->url_thumbnail : '@web/images/placeholders/product.png';
    	$form->count = 1;
    	$form->unit = $unit;
        $form->priceType = self::PRICE_TYPE_PER_UNIT;

        if(empty($translation)) {
            $form->name = $product->symbol;
        }
        else {
	        $name = !empty($translation) ? $translation->name : $product->symbol;
	    	if(!empty($product->parent_product_id)) {
	    		$parentTranslation = $product->parentProduct->getTranslation();
	    		$parentName = !empty($parentTranslation) ? $parentTranslation->name : $product->parentProduct->symbol;
	    		$name = $parentName.' ('.mb_strtolower($name).')';
	    	}
	    	
	    	$form->name = $name;
            $form->price = $translation->price_default;
        }

        return $form;
    }
}
