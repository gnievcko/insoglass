<?php

namespace frontend\models;

use Yii;
use yii\base\Model;
use common\components\validators\RequiredIfFormEmptyValidator;

class ProductAliasForm extends Model {

    public $name;

    public function attributeLabels() {
        return [
            'name' => \Yii::t('web', 'Name'),
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            [['name'], RequiredIfFormEmptyValidator::className()],
        ];
    }
}
