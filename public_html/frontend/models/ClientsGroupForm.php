<?php

namespace frontend\models;

use Yii;
use yii\base\Model;
use common\models\ar\CompanyGroup;
use common\models\ar\CompanyGroupSet;
use common\models\ar\Company;
use common\models\ar\CompanyGroupTranslation;
use common\models\ar\Language;
use common\helpers\RandomSymbolGenerator;
use yii\helpers\ArrayHelper;

class ClientsGroupForm extends Model {

	const SCENARIO_ADD = 'add';
	const SCENARIO_EDIT = 'edit';

    public $id;
    public $name;
    public $clientIds;

	public function __construct($group = null, $config = []) {
		if(!empty($group)) {
			$this->loadData($group);
		}

		parent::__construct($config);
	}

    public function loadData($group) {
    	$this->id = $group->id;

        $translation = $group->getTranslation();
    	$this->name = !empty($translation) ? $translation->name : $group->symbol;

    	$clients = CompanyGroupSet::find()->where(['company_group_id' => $this->id])->all();
        $this->clientIds = array_column($clients, 'company_id');
    }

	public function scenarios() {
		$scenarios = parent::scenarios();
		$scenarios[self::SCENARIO_ADD] = ['name', 'clientIds'];
		$scenarios[self::SCENARIO_EDIT] = ['id', 'name', 'clientIds'];
		return $scenarios;
	}

	public function rules() {
        $rules = [
            [['name'], 'required'],
			[['name'], 'string', 'max' => 256],
			[['name'], 'validateName'],
			[['clientIds'], 'each', 'rule' => ['integer']],
		];
        
        if($this->isEditScenarioSet()) {
            $rules[] = [['id'], 'required'];
        }
        
        return $rules;
	}

	public function validateName($attribute, $params) {
		$languageId = Language::find()->where(['symbol' => Yii::$app->language])->one()->id;
		$query = CompanyGroupTranslation::find()
                ->where(['name' => $this->name])
				->andWhere(['language_id' => $languageId]);
		
		if($this->isEditScenarioSet()) {
			$query->andWhere(['<>', 'company_group_id', $this->id]);
		}
		
		if(!empty($query->one())) {
            $this->addError('name', Yii::t('web', 'Name already exists'));
		}
	}

    public function attributeLabels() {
        return [
	        'name' => Yii::t('main', 'Name'),
            'clientIds' => Yii::t('web', 'Clients'),
        ];
    }

	public function saveData() {
		$transaction = Yii::$app->db->beginTransaction();
		try {
			if($this->isEditScenarioSet()) {
                $group = CompanyGroup::findOne($this->id);
                $translation = $group->getTranslation() ?: $this->createTranslation(group);
			}
			else {
                $group = new CompanyGroup();
                $group->symbol = RandomSymbolGenerator::generate(CompanyGroup::class);
                $group->save();
                
                $translation = $this->createTranslation($group);
			}

            $translation->name = $this->name;
            $translation->save();

			$this->saveAssignedClients($group);
			$this->id = $group->id;

			$transaction->commit();
		}
		catch(\Exception $e) {
			$transaction->rollBack();
			Yii::error($e->getMessage());
			Yii::error($e->getTraceAsString());
			throw new \Exception('Could not save client data');
		}
	}

    private function createTranslation($group) {
        $translation = new CompanyGroupTranslation();
        $translation->company_group_id = $group->id;
        $translation->language_id = Language::find()->where(['symbol' => Yii::$app->language])->one()->id;

        return $translation;
    }

	private function saveAssignedClients($group) {
        CompanyGroupSet::deleteAll(['company_group_id' => $group->id]);

        if(!empty($this->clientIds)) {
            Yii::$app->db->createCommand()->batchInsert('company_group_set', ['company_id', 'company_group_id'], array_map(function($clientId) use($group) {
                        return ['company_id' => $clientId, 'company_group_id' => $group->id];
                    }, $this->clientIds))->execute();
        }
	}

    public function getClientsData() {
        $clients = Company::find()->where(['id' => empty($this->clientIds) ? [] : $this->clientIds])->all();

        return array_map(function($client) {
            return [$client->id => $client->name];
        }, $clients);
    }

    public function setAddScenario() {
    	$this->setScenario(self::SCENARIO_ADD);
    }

    public function setEditScenario() {
    	$this->setScenario(self::SCENARIO_EDIT);
    }

    public function isEditScenarioSet() {
    	return $this->getScenario() == self::SCENARIO_EDIT;
    }
}
