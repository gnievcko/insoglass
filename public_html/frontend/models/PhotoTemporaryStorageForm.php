<?php

namespace frontend\models;

use Yii;
use common\helpers\UploadHelper;
use common\helpers\ImageHelper;

class PhotoTemporaryStorageForm extends FileTemporaryStorageForm {

	public $file;

    public static $ALLOWED_EXTENSIONS = ['png', 'jpg', 'bmp', 'jpeg', 'gif', 'jpe'];
	
    public function rules() {
        return [
            	[
                    ['file'],
                    'file',
                    'extensions' => self::$ALLOWED_EXTENSIONS,
                    'maxSize' => \frontend\helpers\AdditionalFieldHelper::getMaxFileSize(),
                    'tooBig' => Yii::t('main', 'File is too large'),
                    'wrongExtension' => Yii::t('main','Only {extensions} types are allowed')
                ],
        ];
    }

    public function upload() {
        if ($this->validate()) {
            $originalPhotoPath = UploadHelper::saveTemporaryFile($this->file);
            $thumbnailPath = ImageHelper::generateThumbnail($originalPhotoPath['fullName'], $this->file->extension);

            $originalPhotoName = "{$this->file->getBaseName()}.{$this->file->getExtension()}";
            $originalPhotoDetails = UploadHelper::storeInformationAboutUploadedFile($originalPhotoPath['name'], $originalPhotoName);
            $thumbnailDetails = UploadHelper::storeInformationAboutUploadedFile($thumbnailPath['name'], $originalPhotoName);
            
            return [
                	'photo' => $originalPhotoDetails,
                	'thumbnail' => $thumbnailDetails,
            ];
        } else {
            return null;
        }
    }
}
