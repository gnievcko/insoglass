<?php

namespace frontend\models;

use Yii;
use yii\base\Model;
use common\models\ar\Language;
use common\models\ar\Activity;
use common\models\ar\ActivityTranslation;
use common\helpers\RandomSymbolGenerator;


class ActivityForm extends Model {
    
    const SCENARIO_ADD = 'add';
    const SCENARIO_EDIT = 'edit';
    
    public $id;
    public $name;
    public $effortFactor;
    
    public function __construct($activity = null, $config = []) {
        if(!empty($activity)) {
            $this->loadData($activity);
        }
        
        parent::__construct($config);
    }
    
    public function scenarios() {
        $scenarios = parent::scenarios();
        $scenarios[self::SCENARIO_ADD] = ['name', 'effortFactor'];
        $scenarios[self::SCENARIO_EDIT] = ['id', 'name', 'effortFactor'];
        return $scenarios;
    }
    
    public function rules() {
        return [
            [['name'], 'required'],
            [['name'], 'validateName'],
        ];
    }
    
    public function validateName($attribute, $params) {
        $languageId = Language::find()->where('symbol = :symbol', [':symbol' => Yii::$app->language])->one()->id;
        if($this->isEditScenarioSet()) {
            if(!empty(ActivityTranslation::find()->where('name = :name', [':name' =>$this->name])
                ->andWhere('language_id = :languageId', [':languageId' => $languageId])
                ->andWhere('activity_id != :id', [':id' => $this->id])->one())) {
                    $this->addError('name', Yii::t('web', 'Name already exists'));
            }
        }
        else{
            if(!empty(ActivityTranslation::find()->where('name = :name', [':name' =>$this->name])
                ->andWhere('language_id = :languageId', [':languageId' => $languageId])->one())) {
                    $this->addError('name', Yii::t('web', 'Name already exists'));
            }
        }
    } 
    
    public function attributeLabels() {
        return [
            'id' => Yii::t('main', 'ID'),
            'name' => Yii::t('main', 'Name'),
            'effortFactor' => Yii::t('web', 'Effort factor'),
        ];
    }
    
    public function saveData() {
        
        $transaction = Yii::$app->db->beginTransaction();
        try{
            $languageId = Language::find()->where(['symbol' => Yii::$app->language ])->one()->id;
            
            if(!empty($this->id)) {
                $activity = Activity::findOne($this->id);
                $activityTranslation =
                ActivityTranslation::find()->where(['activity_id' => $this->id])
                        ->andWhere(['language_id' => $languageId])->one();
            }
            if(empty($activity)) {
                $activity = new Activity();
            }
            if(empty($activityTranslation)) {
                $activityTranslation = new ActivityTranslation();
            }
            if(!$this->isEditScenarioSet()) {
                $activity->symbol = RandomSymbolGenerator::generate($activity,'symbol',32);
            }
            $activity->effort_factor = $this->effortFactor;
            $activity->save();
            
            $activityTranslation->activity_id = $activity->id;
            $activityTranslation->language_id = $languageId;
            $activityTranslation->name = $this->name;
            $activityTranslation->save();
            
            $this->id = $activity->id;
            $transaction->commit();
        }
        catch(\Exception $e){
            $transaction->rollBack();
            Yii::error($e->getMessage());
            Yii::error($e->getTraceAsString()); 
            throw new \Exception('Could not save activity data');
        }
        
    }
    
    public function loadData($activity) {
        $this->id = $activity->id;
        $activityTranslation = $activity->getTranslation();
        $this->name = !empty($activityTranslation) ? $activityTranslation->name : $activity->symbol;
        $this->effortFactor = $activity->effort_factor;
    }
    
    public function isEditScenarioSet() {
        return $this->getScenario() == self::SCENARIO_EDIT;
    }
    
    public function setAddScenario() {
        $this->setScenario(self::SCENARIO_ADD);
    }
    
    public function setEditScenario() {
        $this->setScenario(self::SCENARIO_EDIT);
    } 
}