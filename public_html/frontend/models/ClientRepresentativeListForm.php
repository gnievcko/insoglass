<?php
namespace frontend\models;

use Yii;
use yii\base\Model;

class ClientRepresentativeListForm extends Model {
	
	public $page;
	public $sortDir;
	public $sortField;
	
	public $companyName;
	public $name;
	public $canLog;
	
	const DEFAULT_PAGE = 0;
	const DEFAULT_SORT_DIR = 'asc';
	const DEFAULT_SORT_FIELD = 'name';
	
	public function init() {
		parent::init();
		$this->page = self::DEFAULT_PAGE;
		$this->sortDir = self::DEFAULT_SORT_DIR;
		$this->sortField = self::DEFAULT_SORT_FIELD;
	}
	
	public static function getSortFields() { 
		return [
				['name' => 'companyName', 'label' => Yii::t('web', 'Client name'), 'isSortable' => true, 'style' => 'width: 16%'],
				['name' => 'firstName', 'label' => Yii::t('main', 'First name'), 'isSortable' => true, 'style' => 'width: 12%'],
				['name' => 'lastName', 'label' => Yii::t('main', 'Last name'), 'isSortable' => true, 'style' => 'width: 12%'],
				['name' => 'email', 'label' => Yii::t('main', 'E-mail address'), 'isSortable' => true, 'style' => 'width: 17%'],
				['name' => 'phone', 'label' => Yii::t('main', 'Phone'), 'isSortable' => true, 'style' => 'width: 12%'],
				['name' => 'canLog', 'label' => Yii::t('main', 'Can log in'), 'isSortable' => false, 'style' => 'width: 12%', 'isHidden' => !Yii::$app->params['canLoggedContactPerson']],
				
		];
	}
	
	public function attributeLabels() {
		return [
				'name' => Yii::t('main', 'Name'),
				'companyName' => Yii::t('web', 'Client name'),
				'canLog' => Yii::t('main', 'Can log in'),
		];
	}
	
	public function rules() {
		return [
				[['page'], 'integer'],
				[['sortDir'], 'in', 'range' => ['asc', 'desc']],
				[['sortField'], 'in', 'range' => array_column(self::getSortFields(), 'name')],
				[['companyName', 'name', 'canLog'], 'safe'],
				['page', 'default', 'value' => self::DEFAULT_PAGE],
				['sortDir', 'default', 'value' => self::DEFAULT_SORT_DIR],
				['sortField', 'default', 'value' => self::DEFAULT_SORT_FIELD],
		];
	}
	
}