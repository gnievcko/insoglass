<?php

namespace frontend\models;

use Yii;
use yii\base\Model;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use common\models\ar\OfferedProductCategory;
use common\models\ar\OfferedProductCategoryTranslation;
use common\models\ar\Language;
use common\helpers\RandomSymbolGenerator;

class OfferedProductCategoryForm extends Model {

	const SCENARIO_ADD = 'add';
	const SCENARIO_EDIT = 'edit';

    public $id;
    public $name;
	public $parentCategoryId;
	public $effortFactor;
	public $description;

	public function __construct($offeredProductCategory = null, $config = []) {
		if(!empty($offeredProductCategory)) {
			$this->loadData($offeredProductCategory);
		}

		parent::__construct($config);
	}

	public function scenarios() {
		$scenarios = parent::scenarios();
		$scenarios[self::SCENARIO_ADD] = ['name', 'parentCategoryId', 'description', 'effortFactor'];
		$scenarios[self::SCENARIO_EDIT] = ['id', 'name', 'parentCategoryId', 'description', 'effortFactor'];
		return $scenarios;
	}

	public function rules() {
		return [
                [['effortFactor'], 'safe'],
				[['name'], 'required'],
				[['parent_category_id'], 'integer'],
				[['description'], 'string', 'max' => 1024],
				[['name'], 'validateName'],
				[['description'], 'default', 'value' => null],
				[['parent_category_id'], 'exist', 'skipOnError' => true, 'targetClass' => OfferedProductCategory::className(), 'targetAttribute' => ['parent_category_id' => 'id']]
		];
	}

	public function validateName ($attribute, $params) {
		$languageId = Language::find()->where('symbol = :symbol', ['symbol' => Yii::$app->language])->one()->id;
		if ($this->isEditScenarioSet()) {
			if (!empty(OfferedProductCategoryTranslation::find()->where('name = :name', [':name' => $this->name])
								->andWhere('language_id = :languageId', [':languageId' => $languageId])
								->andWhere('offered_product_category_id != :id', [':id' => $this->id])->one())) {
									$this->addError('name', Yii::t('web', 'Name already exists'));
								}
		}
		else {
			if (!empty(OfferedProductCategoryTranslation::find()->where('name = :name', [':name' => $this->name])
								->andWhere('language_id = :id', [':id' => $languageId])->one())) {
				$this->addError('name', Yii::t('web', 'Name already exists'));
			}
		}
	}

    public function attributeLabels() {
        return [
        		'parentCategoryId' => Yii::t('main', 'Parent Category'),
            	'id' => Yii::t('main', 'ID'),
            	'name' => Yii::t('main', 'Name'),
            	'parent_category_id' => Yii::t('main', 'Parent Category ID'),
        		'description' => Yii::t('main', 'Description'),
                'effortFactor' => Yii::t('web', 'Effort factor'),
        ];
    }

	public function saveData() {
		$languageId = Language::find()->where('symbol = :symbol', ['symbol' => Yii::$app->language])->one()->id;
		if(!empty($this->id)) {
			$offeredProductCategory = OfferedProductCategory::findOne($this->id);
			$offeredProductCategoryTranslation =
				OfferedProductCategoryTranslation::find()->where('offered_product_category_id = :id', [':id' => $this->id])
				->andWhere('language_id = :languageId', [':languageId' => $languageId])->one();

		}

		if(empty($offeredProductCategory)) {
			$offeredProductCategory = new OfferedProductCategory();
		}
		if(empty($offeredProductCategoryTranslation)) {
			$offeredProductCategoryTranslation = new OfferedProductCategoryTranslation();
		}

		$transaction = Yii::$app->db->beginTransaction();
		try {
			if (!$this->isEditScenarioSet()) {
			    $offeredProductCategory->symbol = RandomSymbolGenerator::generate($offeredProductCategory, 'symbol', 32);
			}
            $offeredProductCategory->effort_factor = $this->effortFactor;
			$offeredProductCategory->parent_category_id = $this->parentCategoryId;
			$offeredProductCategory->save();
			$this->id = $offeredProductCategory->id;

			$offeredProductCategoryTranslation->offered_product_category_id = $offeredProductCategory->id;
			$offeredProductCategoryTranslation->language_id = $languageId;
			$offeredProductCategoryTranslation->name = $this->name;
			$offeredProductCategoryTranslation->description = $this->description;

			$offeredProductCategoryTranslation->save();

			$transaction->commit();
		}
		catch(\Exception $e) {
			$transaction->rollBack();
			throw new \Exception('Could not save offered product category data');
		}
	}

    public function loadData($offeredProductCategory) {
    	$languageId = Language::find()->where('symbol = :symbol', ['symbol' =>Yii::$app->language])->one()->id;
    	$this->id = $offeredProductCategory->id;
    	$this->effortFactor = $offeredProductCategory->effort_factor;
    	$offeredProductCategoryTranslation = OfferedProductCategoryTranslation::find()
    		->where('offered_product_category_id = :id', [':id' => $this->id])
    		->andWhere('language_id = :languageId', [':languageId' => $languageId])->one();
    	$this->parentCategoryId = $offeredProductCategory->parent_category_id;
    	$this->description = $offeredProductCategoryTranslation->description;
    	$this->name = $offeredProductCategoryTranslation->name;
    }

    public function setAddScenario() {
    	$this->setScenario(self::SCENARIO_ADD);
    }

    public function setEditScenario() {
    	$this->setScenario(self::SCENARIO_EDIT);
    }

    public function isEditScenarioSet() {
    	return $this->getScenario() == self::SCENARIO_EDIT;
    }
}
