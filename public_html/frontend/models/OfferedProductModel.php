<?php

namespace frontend\models;

use Yii;
use yii\base\Model;
use yii\helpers\Url;
use frontend\helpers\ModelLoaderHelper;
use common\helpers\UploadHelper;

use common\models\ar\OfferedProduct;
use common\models\ar\OfferedProductTranslation;
use common\models\ar\Language;
use common\models\ar\OfferedProductPhoto;

use common\helpers\RandomSymbolGenerator;
use common\models\aq\OfferedProductQuery;
use common\models\ar\OfferedProductRequirement;

class OfferedProductModel {

    private $languageId;
    private $offeredProduct = null;

    private $offeredProductForm;
    private $offeredProductTranslationsForms = [];
    private $productVariantsForms = [];
    private $offeredProductPhotosForms = [];
    private $documentsForms = [];
    private $oldDocumentsForms = [];
    private $productAttributeForm;
    private $requiredProductsForms = [];

    public function __construct($offeredProductId = null) {
        $this->languageId = Language::find()->where(['symbol' => \Yii::$app->language])->one()->id;
        if($offeredProductId === null) {
            $this->initializeEmptyForms();
        }
        else {
            $this->restoreStateFromDatabase($offeredProductId);
        }
    }

    private function restoreStateFromDatabase($offeredProductId) {
        $this->offeredProduct = OfferedProduct::findOne($offeredProductId);
        $this->restoreOfferedProductForm();
        $this->restoreTranslationsForms();
        $this->restoreVariantsForms();
        $this->restorePhotosForms();
        $this->restoreDocumentsForms();
        $this->restoreAttributeForm();
        $this->restoreRequiredProductsForms();
    }

    private function restoreOfferedProductForm() {
        $this->offeredProductForm = new OfferedProductForm();
        $this->offeredProductForm->isAvailable = $this->offeredProduct->is_available;
        $this->offeredProductForm->id = $this->offeredProduct->id;
        if(\Yii::$app->params['isProductSymbolEditable']) {
            $this->offeredProductForm->symbol = $this->offeredProduct->symbol;
        }
        
        if(Yii::$app->params['isOrderProductDimensionsVisible']) {
            $this->offeredProductForm->height = $this->offeredProduct->height;
            $this->offeredProductForm->width = $this->offeredProduct->width;
            $this->offeredProductForm->shapeId = $this->offeredProduct->shape_id;
        }
        
        if(Yii::$app->params['isProductionVisible']) {
            $this->offeredProductForm->productionPathId = $this->offeredProduct->production_path_id;
        }
        

        $this->offeredProductForm->categoriesIds = array_map(function($categorySet) {
                    return $categorySet->offered_product_category_id;
                }, $this->offeredProduct->offeredProductCategorySets);
    }

    private function restoreTranslationsForms() {
        $this->offeredProductTranslationsForms = array_map(function($translation) {
            $translationForm = new OfferedProductTranslationForm();
            $translationForm->name = $translation->name;
            $translationForm->description = $translation->description;
            $translationForm->price = $translation->price;
            $translationForm->currencyId = $translation->currency_id;
            $translationForm->languageId = $translation->language_id;
            $translationForm->unit = $translation->unit;

            return $translationForm;
        }, $this->offeredProduct->offeredProductTranslations);
    }

    private function restoreVariantsForms() {
        $this->productVariantsForms = array_map(function($variant) {
            $variantForm = new ProductVariantForm();
            $variantForm->name = $variant->name;
            $variantForm->description = $variant->description;
            $variantForm->variantId = $variant->offered_product_id;

            return $variantForm;
        }, OfferedProductTranslation::find()->where([
            'offered_product_id' => (new \yii\db\Query())->select('id')->from('offered_product')->where(['parent_offered_product_id' => $this->offeredProduct->id]),
            'language_id' => $this->languageId,
        ])->all());
    }

    private function restorePhotosForms() {
        $this->offeredProductPhotosForms = array_map(function($photo) {
            $photoForm = new PhotoForm();

            $photoForm->image = UploadHelper::getFileNameByUrl($photo->url_photo);
            $photoForm->thumbnail = UploadHelper::getFileNameByUrl($photo->url_thumbnail);
            $photoForm->description = $photo->description;

            return $photoForm;
        }, $this->offeredProduct->offeredProductPhotos);
    }
    
    private function restoreDocumentsForms() {
    	$this->oldDocumentsForms = array_map(function($document) {
    		$documentForm = new DocumentForm();
    		$documentForm->id = $document['id'];
    		$documentForm->document = $document['urlFile'];
    		$documentForm->name = $document['name'];
    		$documentForm->makeCopy = true;
    
    		return $documentForm;
    	}, OfferedProductQuery::getAttachments($this->offeredProduct->id));
    }
    
    private function restoreAttributeForm() {
        $this->productAttributeForm = new ProductAttributeForm();
        $this->productAttributeForm->loadFromObjects($this->offeredProduct->offeredProductAttributes);
    }
    
    private function restoreRequiredProductsForms() {
        $requiredProducts = OfferedProductRequirement::find()
                ->with('productRequired.productTranslations')
                ->with('productRequired.productPhotos')
                ->where(['offered_product_id' => $this->offeredProduct->id])
                ->all();
    
        $this->requiredProductsForms = array_map(function($requiredProduct) {
            $requiredProductForm = new OfferedProductRequiredProductForm();
            $productTranslation = $requiredProduct->productRequired->getTranslation();
    
            $requiredProductForm->name = empty($productTranslation) ? $requiredProduct->productRequired->symbol : $productTranslation->name;
    
            $photos = $requiredProduct->productRequired->productPhotos;
            $requiredProductForm->thumbnail = empty($photos) ? '@web/images/placeholders/product.png' : $photos[0]->url_thumbnail;
            $requiredProductForm->productId = $requiredProduct->product_required_id;
            $requiredProductForm->count = $requiredProduct->count;
            $requiredProductForm->note = $requiredProduct->note;
            $requiredProductForm->unit = $requiredProduct->unit;
    
            return $requiredProductForm;
        }, $requiredProducts);
    }

    private function initializeEmptyForms() {
        $this->offeredProductForm = new OfferedProductForm();

        $translationForm = new OfferedProductTranslationForm();
        $translationForm->languageId = Language::find()->where(['symbol' => \Yii::$app->language])->one()->id;
        $this->offeredProductTranslationsForms = [$translationForm]; 

        $this->productVariantsForms = [];
        $this->offeredProductPhotosForms = [];
        $this->documentsForms = [];
        
        $this->productAttributeForm = new ProductAttributeForm();
        $this->requiredProductsForms = [];
    }

    public function load($data) {
        $this->offeredProductForm = ModelLoaderHelper::loadOne($data, OfferedProductForm::class);
        $this->offeredProductTranslationsForms = ModelLoaderHelper::loadMultiple($data, OfferedProductTranslationForm::class);
        $this->productVariantsForms = ModelLoaderHelper::loadMultiple($data, ProductVariantForm::class);
        $this->offeredProductPhotosForms = ModelLoaderHelper::loadMultiple($data, PhotoForm::class);
        $this->documentsForms = ModelLoaderHelper::loadMultiple($data, DocumentForm::class);
        $this->productAttributeForm = ModelLoaderHelper::loadOne($data, ProductAttributeForm::class);
        $this->requiredProductsForms = ModelLoaderHelper::loadMultiple($data, OfferedProductRequiredProductForm::class);
    }

    public function save() {
        if($this->validate()) {
            $this->persist();
            return true;
        }
        else {
            return false; 
        }
    }

    private function validate() {
        return $this->offeredProductForm->validate() 
            && Model::validateMultiple($this->offeredProductTranslationsForms)
            && Model::validateMultiple($this->productVariantsForms)
            && Model::validateMultiple($this->offeredProductPhotosForms)
            && Model::validateMultiple($this->documentsForms)
            && $this->productAttributeForm->validate();
    }
        
    private function persist() {
        $transaction = \Yii::$app->db->beginTransaction();

        try {
            if($this->offeredProduct === null) {
                $this->offeredProduct = $this->saveOfferedProduct();
            }
            else {
                $this->updateOfferedProduct();
            }
            $this->saveOfferedProductTranslations();
            $this->assignToCategories();
            $this->saveVariants();
            $this->savePhotos();
            $this->saveDocuments();
            $this->saveAttributes();
            $this->saveRequiredProducts();

            $transaction->commit();
        }
        catch(\Exception $e) {
            $transaction->rollback();
            throw $e;
        }
    }

    private function updateOfferedProduct() {
        $this->offeredProduct->is_available = $this->offeredProductForm->isAvailable;
        if(\Yii::$app->params['isProductSymbolEditable']) {
            $this->offeredProduct->symbol = $this->offeredProductForm->symbol;
        }
        
        if(Yii::$app->params['isOrderProductDimensionsVisible']) {
            $this->offeredProduct->height = $this->offeredProductForm->height;
            $this->offeredProduct->width = $this->offeredProductForm->width;
            $this->offeredProduct->shape_id = $this->offeredProductForm->shapeId;
        }
        
        if(Yii::$app->params['isProductionVisible']) {
            $this->offeredProduct->production_path_id = $this->offeredProductForm->productionPathId;
        }
        
        if(!$this->offeredProduct->save()) {
            throw new \Exception('Could not update offered product');
        }
    }

    private function saveOfferedProduct() {
        $offeredProduct = new OfferedProduct();
        $offeredProduct->symbol = \Yii::$app->params['isProductSymbolEditable'] ? 
            $this->offeredProductForm->symbol : 
            RandomSymbolGenerator::generate(OfferedProduct::class);
        $offeredProduct->is_artificial = 0;
        $offeredProduct->is_available = $this->offeredProductForm->isAvailable;
        $offeredProduct->is_active = 1;
        
        if(Yii::$app->params['isOrderProductDimensionsVisible']) {
            $offeredProduct->height = $this->offeredProductForm->height;
            $offeredProduct->width = $this->offeredProductForm->width;
            $offeredProduct->shape_id = $this->offeredProductForm->shapeId;
        }
        
        if(Yii::$app->params['isProductionVisible']) {
            $offeredProduct->production_path_id = $this->offeredProductForm->productionPathId;
        }
        
        $offeredProduct->user_id = \Yii::$app->user->identity->id;
        if(!$offeredProduct->save()) {
            throw new \Exception('Could not save offered product');
        }

        return $offeredProduct;
    }

    private function saveOfferedProductTranslations() {
        \Yii::$app->db->createCommand()->delete('offered_product_translation', ['offered_product_id' => $this->offeredProduct->id])->execute();

        \Yii::$app->db->createCommand()->batchInsert('offered_product_translation', 
            ['offered_product_id', 'language_id', 'name', 'description', 'price', 'unit', 'currency_id'], array_map(function($translationForm) {
                return [$this->offeredProduct->id, $translationForm->languageId, $translationForm->name, $translationForm->description, 
                        $translationForm->price, $translationForm->unit, $translationForm->currencyId]; 
            }, $this->offeredProductTranslationsForms))->execute();
    }

    private function assignToCategories() {
        \Yii::$app->db->createCommand()->delete('offered_product_category_set', ['offered_product_id' => $this->offeredProduct->id])->execute();

        \Yii::$app->db->createCommand()->batchInsert('offered_product_category_set', 
            ['offered_product_id', 'offered_product_category_id'], array_map(function($categoryId) {
                return [$this->offeredProduct->id, $categoryId]; 
            }, empty($this->offeredProductForm->categoriesIds) ? [] : $this->offeredProductForm->categoriesIds))->execute();
    }

    private function saveVariants() {
        $translationsLanguage = Language::find()->where(['symbol' => \Yii::$app->language])->one();

        $variantsIdsToPersist = [];
        $variantsTranslationsToInsert = [];
        
        foreach(ModelLoaderHelper::skipEmpty($this->productVariantsForms) as $variantForm) {
            $variantIdToPersist = $variantForm->variantId;
            if(empty($variantIdToPersist)) {
                $newVariant = $this->createNewVariant($variantForm);
                $variantIdToPersist = $newVariant->id;
            }

            $variantsTranslationsToInsert[] = [$variantIdToPersist, $translationsLanguage->id, $variantForm->name, $variantForm->description];
            $variantsIdsToPersist[] = $variantIdToPersist;
        }

        \Yii::$app->db->createCommand()->delete('offered_product_translation', ['in', 'offered_product_id', (new \yii\db\Query())
            ->select('id')
            ->from('offered_product')
            ->andWhere(['parent_offered_product_id' => $this->offeredProduct->id])
        ])
        ->execute();

        \Yii::$app->db->createCommand()->delete('offered_product', ['and', 
            ['=', 'parent_offered_product_id', $this->offeredProduct->id],
            ['not in', 'id', $variantsIdsToPersist],
        ])->execute();

        \Yii::$app->db->createCommand()->batchInsert('offered_product_translation', ['offered_product_id', 'language_id', 'name', 'description'], $variantsTranslationsToInsert)
            ->execute();
    }

    private function createNewVariant($variantForm) {
        $variant = new OfferedProduct();
        $variant->symbol = RandomSymbolGenerator::generate(OfferedProduct::class);
        $variant->is_artificial = $this->offeredProduct->is_artificial;
        $variant->is_available = $this->offeredProduct->is_available;
        $variant->is_active = $this->offeredProduct->is_active;
        $variant->user_id = $this->offeredProduct->user_id;
        $variant->parent_offered_product_id = $this->offeredProduct->id;
        if(!$variant->save()) {
            throw new \Exception('Could not save offered product variant');
        }

        return $variant;
    }

    private function savePhotos() {
        $photosToSave = array_map(function($photoForm) {
            return Url::to(['storage/index', 'f' => $photoForm->image], true);
        }, $this->offeredProductPhotosForms);

        \Yii::$app->db->createCommand()->delete('offered_product_photo', ['offered_product_id' => $this->offeredProduct->id])->execute();
        \Yii::$app->db->createCommand()
            ->batchInsert('offered_product_photo', ['offered_product_id', 'url_photo', 'url_thumbnail', 'description'], array_map(function($photoForm) {
            return [$this->offeredProduct->id, Url::to(['storage/index', 'f' => $photoForm->image], true), Url::to(['storage/index', 'f' => $photoForm->thumbnail], true), $photoForm->description] ;
        }, $this->offeredProductPhotosForms))->execute();

        $photosToRemove = OfferedProductPhoto::find()->andWhere(['offered_product_id' => $this->offeredProduct->id])->andWhere(['not in', 'url_photo', $photosToSave])->all();

        $this->removeFromDisk($photosToRemove);
    }
    
    private function saveDocuments() {
    	$currentUserId = \Yii::$app->user->identity->id;
    	$filesToSave = [];
    	$copies = [];
    
    	foreach($this->documentsForms as $documentForm) {
    		if($documentForm->makeCopy) {
    			$copy = ['hash' => UploadHelper::copy($documentForm->document), 'name' => $documentForm->name];
    			$copies[] = $copy;
    			$filesToSave[] = $copy;
    		}
    		else {
    			$filesToSave[] = ['hash' => $documentForm->document, 'name' => $documentForm->name, 'description' => $documentForm->description];
    		}
    	}
    
    	\Yii::$app->db->createCommand()->batchInsert('file_temporary_storage',
    			['file_hash', 'original_file_name'],
    			array_map(function($copy) {
    				return ['file_hash' => $copy['hash'], 'original_file_name' => $copy['name']];
    			}, $copies))->execute();
    
    	\Yii::$app->db->createCommand()->batchInsert('offered_product_attachment',
    			['offered_product_id', 'url_file', 'name', 'user_id', 'description'],
    			array_map(function($fileToSave) use($currentUserId) {
    				return [$this->offeredProduct->id, Url::to(['storage/index', 'f' => $fileToSave['hash']], true), $fileToSave['name'], $currentUserId, $fileToSave['description']];
    			}, $filesToSave))->execute();
    
    	\Yii::$app->db->createCommand()->delete('file_temporary_storage', ['in', 'file_hash', array_column($filesToSave, 'hash')])->execute();
    }
    
    private function saveAttributes() {
        $arrayObjects = $this->productAttributeForm->convertFieldsToArrayObjects();
        
        \Yii::$app->db->createCommand()->delete('offered_product_attribute', ['offered_product_id' => $this->offeredProduct->id])->execute();
        \Yii::$app->db->createCommand()->update('offered_product', ['attribute_value' => null], ['id' => $this->offeredProduct->id])->execute();
        if(!empty($arrayObjects)) {
            \Yii::$app->db->createCommand()
                    ->batchInsert('offered_product_attribute', ['offered_product_id', 'product_attribute_type_id', 'value'], array_map(function($obj) {
                        return [$this->offeredProduct->id, $obj['product_attribute_type_id'], $obj['value']];
                    }, $arrayObjects))->execute();
                    
            \Yii::$app->db->createCommand()->update('offered_product', 
                    ['attribute_value' => $this->productAttributeForm->convertFieldsToJson()], 
                    ['id' => $this->offeredProduct->id])->execute();
        }
    }
    
    private function saveRequiredProducts() {
        Yii::$app->db->createCommand()->delete('offered_product_requirement', ['offered_product_id' => $this->offeredProduct->id])->execute();
    
        Yii::$app->db->createCommand()->batchInsert('offered_product_requirement',
                    ['offered_product_id', 'product_required_id', 'count', 'unit', 'note'],
                    array_map(function($requiredProductForm) {
                        return [
                            $this->offeredProduct->id,
                            $requiredProductForm->productId,
                            $requiredProductForm->count,
                            $requiredProductForm->unit,
                            $requiredProductForm->note,
                        ];
                    }, $this->requiredProductsForms)
                )->execute();
    }

    private function removeFromDisk($photosToRemove) {
        foreach($photosToRemove as $photo) {
            UploadHelper::removeByUrl($photo->url_photo);
            UploadHelper::removeByUrl($photo->url_thumbnail);
        }
    }
    
    private function removeFilesFromDisk($filesToRemove) {
    	foreach($filesToRemove as $file) {
    		UploadHelper::removeByUrl($file->url_file);
    	}
    }

    public function getInvalidForms() {
        $invalidForms = [];
        if($this->offeredProductForm->hasErrors()) {
            $invalidForms[] = $this->offeredProductForm;
        }

        return $invalidForms 
            + ModelLoaderHelper::getErrorForms($this->offeredProductTranslationsForms)
            + ModelLoaderHelper::getErrorForms($this->roductVariantsForms)
            + ModelLoaderHelper::getErrorForms($this->offeredProductPhotosForms)
            + ModelLoaderHelper::getErrorForms($this->documentsForms)
            + ModelLoaderHelper::getErrorForms($this->productAttributeForm)
            + ModelLoaderHelper::getErrorForms($this->requiredProductsForms);
    }

    public function getAllForms() {
        return [
            'offeredProductForm' => $this->offeredProductForm,
            'offeredProductTranslationsForms' => $this->offeredProductTranslationsForms,
            'productVariantsForms' => $this->productVariantsForms,
            'photosForms' => $this->offeredProductPhotosForms,
        	'documentsForms' => $this->documentsForms,
        	'oldDocumentsForms' => $this->oldDocumentsForms,
            'productAttributeForm' => $this->productAttributeForm,
            'requiredProductsForms' => $this->requiredProductsForms,
        ];
    }

    public function getOfferedProductForm() {
        return $this->offeredProductForm;
    }

    public function getOfferedProductTranslationsForms() {
        return $this->offeredProductTranslationsForms;
    }

    public function getProductVariantsForms() {
        return $this->productVariantsForms;
    }

    public function getOfferedProductPhotosForms() {
        return $this->offeredProductPhotosForms;
    }
    
    public function getDocumentsForms() {
    	return $this->documentsForms;
    }
    
    public function getProductAttributeForm() {
        return $this->productAttributeForm;
    }
    
    public function getRequiredProductsForms() {
        return $this->requiredProductsForms;
    }

    public function update() {
        if($this->validate()) {
            $this->persist();

            return true;
        }
        else {
            return false;
        }
    }

    public function getOfferedProduct() {
        return $this->offeredProduct;
    }
}
