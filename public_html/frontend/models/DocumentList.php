<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace frontend\models;

use Yii;
use yii\mongodb\Query;
use common\models\ar\Order;
use common\models\ar\User;
use common\models\ar\Company;
/**
 * Description of Documents
 *
 * @author Bartek
 */
class DocumentList {

    static public function findAll($limit = null, $page = 0, $sort = 'desc', $sortField = 'history.createdAt', $where = null) {
        if (isset(Yii::$app->params['isDocumentStorageAvailable']) && Yii::$app->params['isDocumentStorageAvailable']) {
            $query = new Query();
            $query->select([])->from('documents');
            
            if (!empty($sort) && !empty($sortField)) {
                $query->addOrderBy([$sortField => DocumentList::mongoSort($sort)]);
            }

            if (!empty($where)) {
                $query->where($where);
            }
            

            if (!empty($page)) {
                $query->offset = $page * $limit;
            }

            if (!empty($limit)) {
                $rows = $query->limit($limit);
            }

            $rows = $query->all();
            
            for ($i = 0; $i < count($rows); $i++) {
                usort($rows[$i]['history'], function($a, $b) {
                    return $a['createdAt'] < $b['createdAt'];
                });
                $rows[$i]['history'] = $rows[$i]['history'][0];
                $orderId = 0;
                $companyId = 0;

                foreach ($rows[$i]['history']['sections'] as $section) {
                    if(isset($section['orderId'])) {
                        $orderId = $section['orderId'];
                        //break;
                    }
                    elseif(isset($section['companyId'])) {
                    	$companyId = $section['companyId'];
                    }
                }
                
                $rows[$i]['order'] = $order = Order::find()->where(['id' => $orderId])->one();
                $rows[$i]['company'] = !empty($order) ? $order->company : Company::findOne($companyId);
                
                $authorId = isset($rows[$i]['history']['authorId']) ? $rows[$i]['history']['authorId'] : null;
                $rows[$i]['author'] = !empty($authorId) ? User::find()->where(['id' => $authorId])->one() : null;
            }

            return $rows;
        } else {
            return [];
        }
    }

    static public function count($where = null) {
        if (isset(Yii::$app->params['isDocumentStorageAvailable']) && Yii::$app->params['isDocumentStorageAvailable']) {
            $query = new Query();

            $query->select([])->from('documents');

            if (!empty($where)) {
                $query->where($where);
            }

            $count = $query->count();

            return $count;
        } else {
            return 0;
        }
    }

    static private function mongoSort($sort) {
        if ($sort == 'desc') {
            return SORT_DESC;
        } else {
            return SORT_ASC;
        }
    }

}
