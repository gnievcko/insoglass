<?php

namespace frontend\models;

use Yii;
use yii\base\Model;
use common\helpers\ProvinceHelper;
use common\models\ar\WarehouseSupplier;
use yii\db\Query;
use frontend\helpers\AddressHelper;

class WarehouseSupplierForm extends Model {
	
	const SCENARIO_ADD = 'add';
	const SCENARIO_EDIT = 'edit';
	
	public $id;
    public $name;
	public $isActive;
	public $dateCreation;
	
	public $contactPerson;
	public $phone;
	public $email;
	
	public $address;
	public $city;
	public $zipCode;
	public $provinceId;
	
	public $provinces;

	public function __construct($warehouseSupplier = null, $config = []) {
		if(!empty($warehouseSupplier)) {
			$this->loadData($warehouseSupplier);
		}
		$this->provinces = ProvinceHelper::getListOfNonArtificialProvinces();
	
		parent::__construct($config);
	}
	
	public function scenarios() {
		$scenarios = parent::scenarios();
		$scenarios[self::SCENARIO_ADD] = ['name', 'isActive', 'dateCreation', 'address', 'city', 'zipCode', 'provinceId', 'contactPerson', 'phone', 'email'];
		$scenarios[self::SCENARIO_EDIT] = ['id', 'name', 'isActive', 'dateCreation', 'address', 'city', 'zipCode', 'provinceId', 'contactPerson', 'phone', 'email'];
		return $scenarios;
	}
	
	public function rules() {
		return [				
				[['name'], 'required'],
				[['is_active'], 'integer'],
				[['email', 'address', 'city'], 'string', 'max' => 64],
				[['name', 'contactPerson'], 'string', 'max' => 256],
				[['email'], 'validateEmailUnique'],
				[['email'], 'email'],
				[['name'], 'validateUniqueness'],
				[['phone'], 'string', 'max' => 32],
				[['zipCode'], 'string', 'max' => 10],
				[['address', 'city', 'zipCode', 'provinceId'], 'validateAddress'],
				[['contactPerson', 'email', 'phone'], 'default', 'value' => null],
		];
	}
	
	public function validateUniqueness(){
		$query = (new Query())
    			->select(['name'])
    			->from('warehouse_supplier')
    			->where(['name' => $this->name]);
		if ($this->isEditScenarioSet()){
    		$query->andWhere('id != :id', [':id' => $this->id]);
		}		
	
		if (($query->count() != 0)){
			$this->addError('name', Yii::t('web', 'Name must be unique.'));
		}
	}
	
	public function validateAddress($attribute, $params) {
		$sum = !empty($this->address) + !empty($this->city) + !empty($this->zipCode) + !empty($this->provinceId);
		if($sum > 0 && $sum < 4) {
			$this->addError('address', Yii::t('web', 'Address is incomplete.'));
			$this->addError('city', '');
			$this->addError('zipCode', '');
			$this->addError('provinceId', '');
		}
	}
	
	public function validateEmailUnique() {
		$query = (new Query())->select(['id', 'email'])
		->from('user')
		->where('email = :email', [':email' => $this->email]);
		if($this->isEditScenarioSet()) {
			$query->andWhere('id != :id', [':id' => $this->id]);
		}
	
		if($query->count() != 0) {
			$this->addError('email', Yii::t('web', 'E-mail must be unique.'));
		}
	}
	
    public function attributeLabels() {
        return [
        		'name' => Yii::t('web', 'Name'),
        		'isActive' => Yii::t('main', 'Is active'),
            	'dateCreation' => Yii::t('main', 'Creation date'),
        		'email' => Yii::t('main', 'E-mail'),
        		'phone' => Yii::t('web', 'Phone number'),
        		'city' => Yii::t('main', 'City'),
        		'zipCode' => Yii::t('main', 'Zip code'),
        		'provinceId' => Yii::t('main', 'Voivodeship'),
        		'address' => Yii::t('main', 'Address'),
        		'contactPerson' => Yii::t('web', 'Contact person'),
        ];
    }
    
	public function saveData() {
		if(!empty($this->id)) {
			$warehouseSupplier = WarehouseSupplier::findOne($this->id);
		}		
		if(empty($warehouseSupplier)) {
			$warehouseSupplier = new WarehouseSupplier();
		}
		
		$transaction = Yii::$app->db->beginTransaction();
		try {
			$warehouseSupplier->name = $this->name;
			$warehouseSupplier->is_active = 1;
			$warehouseSupplier->email = $this->email;
			$warehouseSupplier->phone = $this->phone;
			$warehouseSupplier->contact_person = $this->contactPerson;
			
			$warehouseSupplier->address_id = AddressHelper::getAddressId([
					'address' => $this->address, 'city' => $this->city,
					'zipCode' => $this->zipCode, 'provinceId' => $this->provinceId
			]);
			
			$warehouseSupplier->save();
			$this->id = $warehouseSupplier->id;
			$transaction->commit();
		}
		catch(\Exception $e) {
			$transaction->rollBack();
			throw new \Exception('Could not save supplier');
		}		
	}
	
    public function loadData($warehouseSupplier) {
    	$this->id = $warehouseSupplier->id;
    	$this->name = $warehouseSupplier->name;
    	$this->isActive = $warehouseSupplier->is_active;
    	$this->dateCreation = $warehouseSupplier->date_creation;
    	$this->email = $warehouseSupplier->email;
    	$this->phone = $warehouseSupplier->phone;
    	$this->contactPerson = $warehouseSupplier->contact_person;
    	
    	$address = $warehouseSupplier->address;
    	 
    	if(!empty($address)) {
    		$this->address = $address->main.(!empty($address->complement) ? ' '.$address->complement : '');
    		$this->city = $address->city->name;
    		$this->zipCode = $address->city->zip_code;
    		 
    		if(!empty($address->city->province)) {
    			$this->provinceId = $address->city->province->id;
    		}
    	}
    	
    }

    public function setAddScenario() {
    	$this->setScenario(self::SCENARIO_ADD);
    }
    
    public function setEditScenario() {
    	$this->setScenario(self::SCENARIO_EDIT);
    }
    
    public function isEditScenarioSet() {
    	return $this->getScenario() == self::SCENARIO_EDIT;
    }
}