<?php

namespace frontend\models;

use Yii;
use yii\base\Model;
use common\models\ar\Company;
use yii\helpers\ArrayHelper;
use common\models\ar\User;
use common\models\ar\UserRole;
use common\helpers\Utility;
use console\controllers\RbacController;
use common\models\ar\Role;
use common\helpers\StrengthValidator;

class ClientRepresentativeForm extends Model {

    const SCENARIO_ADD = 'add';
    const SCENARIO_EDIT = 'edit';

    public $id;
    public $firstName;
    public $lastName;
    public $email;
    public $phone1;
    public $note;
    public $companyId;
    public $password;
    public $canLogged;

    public function __construct($companyId = null, $user = null, $config = []) {
        $this->companyId = $companyId;
        if(!empty($user)) {
            $this->loadData($user);
        }

        parent::__construct($config);
    }

    public function scenarios() {
        $scenarios = parent::scenarios();
        $scenarios[self::SCENARIO_ADD] = ['firstName', 'lastName', 'email', 'phone1', 'note', 'companyId', 'canLogged', 'password'];
        $scenarios[self::SCENARIO_EDIT] = ['id', 'firstName', 'lastName', 'email', 'phone1', 'note', 'companyId', 'canLogged', 'password'];
        return $scenarios;
    }

    public function rules() {
    	$requiredFields = ['firstName', 'lastName', 'companyId'];
    	if(Yii::$app->params['canLoggedContactPerson']){
    		$requiredFields[] = 'email';
    	}
        return [
        	[$requiredFields, 'required'],
            [['email', 'password'], 'string', 'max' => 64],
            [['email'], 'email'],
            [['email'], 'validateEmail'],
        	[['firstName'], 'validatePassword'],
            [['note'], 'string', 'max' => 1024],
            [['firstName', 'lastName', 'phone1'], 'string', 'max' => 32],
            [['email', 'phone1', 'note'], 'default', 'value' => null],
            [['companyId, id', 'canLogged'], 'integer'],
            [['password'], StrengthValidator::className()]+Yii::$app->params['strengthPasswordConfig']
        ];
    }

    public function attributeLabels() {
        return [
            'firstName' => Yii::t('main', 'First name'),
            'lastName' => Yii::t('main', 'Last name'),
            'email' => Yii::t('main', 'E-mail address'),
            'phone1' => Yii::t('main', 'Phone'),
            'note' => Yii::t('main', 'Note'),
            'companyId' => Yii::t('main', 'Company'),
            'canLogged' => Yii::t('main', 'Can log in'),
            'password' => Yii::t('main', 'Password'),
        ];
    }

    public function validateEmail() {
        if (Yii::$app->params['canLoggedContactPerson']) {
            if (!($this->scenario === self::SCENARIO_EDIT && $this->email === User::find()->where(['id' => $this->id])->select('email')->scalar())) {
                if (User::find()->where(['email' => $this->email])->count() > 0) {
                    $this->addError('email', Yii::t('main', 'A person with the given e-mail address already exists!'));
                }
            }
        }
    }

    public function validatePassword() {
    	if(!empty($this->canLogged) && empty($this->password)) {
    		$this->addError('password', Yii::t('web', 'Password is required if a contact person can log in'));
    	}
    }

    public function saveData() {
        $user = null;
        if(!empty($this->id)) {
            $user = User::findOne($this->id);
        }

        if(empty($user)) {
            $user = new User();
        }
        $transaction = Yii::$app->db->beginTransaction();
        try {
            $user->first_name = $this->firstName;
            $user->last_name = $this->lastName;

            if(Yii::$app->params['canLoggedContactPerson']) {
                $user->email = $this->email;
                $user->can_logged = !empty($this->canLogged) ? $this->canLogged : 0;
                if ($this->password != $user->password) {
                    $user->password = !empty($this->password) ? Yii::$app->security->generatePasswordHash($this->password) : null;
                }
            } else {
                $user->email = uniqid() . '@osobakontaktowa.pl';
            }
            $user->contact_email = $this->email;
            $user->phone1 = $this->phone1;
            $user->note = $this->note;
            $user->company_id = $this->companyId;
            $user->is_active = 1;

            $user->save();

            if(UserRole::find()->alias('ur')->joinWith('role r')
                            ->where('ur.user_id = :userId AND r.symbol = :roleClientSymbol', [':userId' => $user->id, ':roleClientSymbol' => Utility::ROLE_CLIENT])
                            ->count() == 0) {
                $role = Role::find()->where(['symbol' => Utility::ROLE_CLIENT])->one();

                $userRole = new UserRole();
                $userRole->user_id = $user->id;
                $userRole->role_id = $role->id;
                $userRole->save();

                $rbacController = new RbacController('rbac', Yii::$app->module);
                $rbacController->assignRoleToUser($role->symbol, $user->id);
            }

            $transaction->commit();
            
            return $user->id;
        } catch(\Exception $e) {
            throw $e;
            $transaction->rollBack();
            throw new \Exception('Could not save client representative data');
        }
    }

    public function loadData($user) {
        $this->id = $user->id;
        $this->firstName = $user->first_name;
        $this->lastName = $user->last_name;
        $this->email = $user->contact_email;
        $this->phone1 = $user->phone1;
        $this->note = $user->note;
        $this->companyId = $user->company_id;
        
        if(Yii::$app->params['canLoggedContactPerson']) {
            $this->canLogged = $user->can_logged;
            $this->password = $user->password;
        }
    }

    public function getCompanyData() {
        $companyData = Company::find()->where('id = :id', [':id' => $this->companyId])->all();
        $companyData = ArrayHelper::map($companyData, 'id', 'name');
        return $companyData;
    }

    public function setAddScenario() {
        $this->setScenario(self::SCENARIO_ADD);
    }

    public function setEditScenario() {
        $this->setScenario(self::SCENARIO_EDIT);
    }

    public function isEditScenarioSet() {
        return $this->getScenario() == self::SCENARIO_EDIT;
    }

}
