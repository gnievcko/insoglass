<?php

namespace frontend\models;

use Yii;
use yii\base\Model;
use yii\helpers\Html;
use common\models\ar\Company;
use common\models\ar\UserCompanySalesman;
use common\helpers\CompanyHelper;
use common\models\aq\CompanyGroupSetQuery;
use common\models\aq\UserCompanySalesmanQuery;
use common\models\ar\User;
use common\models\ar\UserRole;
use common\models\ar\Role;
use frontend\helpers\UserHelper;
use common\models\ar\CompanyGroupSet;
use common\models\ar\CompanyGroup;
use yii\helpers\ArrayHelper;
use frontend\helpers\AddressHelper;
use common\helpers\ChangeHelper;
use common\models\ar\ClientModificationHistory;
use frontend\helpers\ModelLoaderHelper;
use console\controllers\RbacController;
use common\helpers\Utility;
use frontend\models\ClientRepresentativeForm;

class CompanyForm extends Model {

	const SCENARIO_ADD = 'add';
	const SCENARIO_EDIT = 'edit';

    public $id;
    public $name;
	public $email;
	public $phone1;
	public $phone2;

	public $fax1;
	public $vatIdentificationNumber;
	public $taxpayerIdentificationNumber;
	public $krsNumber;
	public $note;

	public $address1;
	public $city1;
	public $zipCode1;
	public $province1Id;

	public $address2;
	public $city2;
	public $zipCode2;
	public $province2Id;

	public $parentCompanyId;
	public $authorizedSalesmanIds;
	public $groupIds;
    public $userId;
    
    private $contactPeopleForms = [];

    public function __construct($company = null, $config = []) {
		if(!empty($company)) {
			$this->loadData($company);
		}
		else {
			$this->contactPeopleForms = [new ClientRepresentativeForm()];
		}

		parent::__construct($config);
	}

	public function scenarios() {
		$scenarios = parent::scenarios();
		$scenarios[self::SCENARIO_ADD] = ['name', 'email', 'phone1', 'phone2', 'address1', 'city1', 'zipCode1', 'province1Id',
				'address2', 'city2', 'zipCode2', 'province2Id', 'authorizedSalesmanIds', 'groupIds', 'parentCompanyId',
				'fax1', 'vatIdentificationNumber', 'taxpayerIdentificationNumber', 'krsNumber', 'note', 'contractTypeId', 'userId',
				'contactPeopleForms',
		];
		$scenarios[self::SCENARIO_EDIT] = ['id', 'name', 'email', 'phone1', 'phone2', 'address1', 'city1', 'zipCode1', 'province1Id',
				'address2', 'city2', 'zipCode2', 'province2Id', 'authorizedSalesmanIds', 'groupIds', 'parentCompanyId',
				'fax1', 'vatIdentificationNumber', 'taxpayerIdentificationNumber', 'krsNumber', 'note', 'contactPeopleForms',
		];
		return $scenarios;
	}

	public function rules() {
		return [
				[['name'], 'required'],
				[['name'], 'string', 'max' => 128],
				['note', 'string', 'max' => 2048],
				[['email', 'address1', 'address2', 'city1', 'city2'], 'string', 'max' => 64],
				[['email'], 'email'],
				[['phone1', 'phone2', 'fax1', 'vatIdentificationNumber', 'taxpayerIdentificationNumber', 'krsNumber'], 'string', 'max' => 32],
				[['email', 'phone1', 'phone2', 'fax1', 'vatIdentificationNumber', 'taxpayerIdentificationNumber', 'krsNumber'], 'default', 'value' => null],
				[['zipCode1', 'zipCode2'], 'string', 'max' => 10],
				[['province1Id', 'province2Id', 'parentCompanyId'], 'integer'],
				['phone2', 'validatePhone1'],
				[['address1', 'city1', 'zipCode1', 'province1Id'], 'validateAddress1'],
				[['address2', 'city2', 'zipCode2', 'province2Id'], 'validateAddress2'],
		];
	}

	public function validatePhone1($attribute, $params) {
		if(empty($this->phone1) && !empty($this->phone2)) {
			$this->addError('phone1', Yii::t('web', 'Phone must be set if alternative phone has been provided.'));
		} else if(!empty($this->phone1) && $this->phone1 == $this->phone2) {
			$this->addError('phone1', Yii::t('web', 'Alternate phone must be different from the phone.'));
		}
	}

	public function validateAddress1($attribute, $params) {
		$sum = !empty($this->address1) + !empty($this->city1) + !empty($this->zipCode1) + !empty($this->province1Id);
		if($sum > 0 && $sum < 4) {
			$this->addError('address1', Yii::t('web', 'Address is incomplete.'));
			$this->addError('city1', '');
			$this->addError('zipCode1', '');
			$this->addError('province1Id', '');
		}
	}

	public function validateAddress2($attribute, $params) {
		$sum = !empty($this->address2) + !empty($this->city2) + !empty($this->zipCode2) + !empty($this->province2Id);
		if($sum > 0 && $sum < 4) {
			$this->addError('address2', Yii::t('web', 'Address is incomplete.'));
			$this->addError('city2', '');
			$this->addError('zipCode2', '');
			$this->addError('province2Id', '');
		}

		if(empty($this->address1) && !empty($this->address2)) {
			$this->addError('address1', Yii::t('web', 'Main address must be set if address for correspondence has been provided.'));
			$this->addError('city1', '');
			$this->addError('zipCode1', '');
			$this->addError('province1Id', '');
		}
	}

    public function attributeLabels() {
        return [
	            'name' => Yii::t('main', 'Name'),
	            'email' => Yii::t('main', 'E-mail address'),
	            'phone1' => Yii::t('main', 'Phone number'),
	        	'phone2' => Yii::t('main', 'Alternative phone'),
	        	'address1' => Yii::t('main', 'Address'),
        		'fax1' => Yii::t('main', 'Fax'),
        		'vatIdentificationNumber' => Yii::t('main', 'VAT Identification Number'),
        		'taxpayerIdentificationNumber' => Yii::t('main', 'Taxpayer Identification Number'),
        		'krsNumber' => Yii::t('main', 'KRS number'),
	        	'city1' => Yii::t('main', 'City'),
        		'zipCode1' => Yii::t('main', 'Zip code'),
        		'province1Id' => Yii::t('main', 'Voivodeship'),
        		'address2' => Yii::t('main', 'Address'),
        		'city2' => Yii::t('main', 'City'),
        		'zipCode2' => Yii::t('main', 'Zip code'),
        		'province2Id' => Yii::t('main', 'Voivodeship'),
                'authorizedSalesmanIds' => Yii::t('web', 'Authorized salesmen'),
        		'groupIds' => Yii::t('web', 'Groups'),
        		'parentCompanyId' => Yii::t('main', 'Parent company'),
        		'note' => Yii::t('main', 'Note'),
        ];
    }

	public function saveData($action) {
		$company = null;
		if(!empty($this->id)) {
			$company = Company::findOne($this->id);
		}

		if(empty($company)) {
			$company = new Company();
            $company->user_id = Yii::$app->user->id;
		}

		$transaction = Yii::$app->db->beginTransaction();
		try {
			
			//TODO
			$company->name = $this->name;
			$company->email = $this->email;
			$company->phone1 = $this->phone1;
			$company->phone2 = $this->phone2;
			$company->fax1 = $this->fax1;
			$company->vat_identification_number = $this->vatIdentificationNumber;
			$company->taxpayer_identification_number = $this->taxpayerIdentificationNumber;
			$company->krs_number = $this->krsNumber;
			$company->parent_company_id = $this->parentCompanyId;
			$company->note = $this->note;

			$company->address_id = AddressHelper::getAddressId([
					'address' => $this->address1, 'city' => $this->city1,
					'zipCode' => $this->zipCode1, 'provinceId' => $this->province1Id
			]);
			$company->address_postal_id = AddressHelper::getAddressId([
					'address' => $this->address2, 'city' => $this->city2,
					'zipCode' => $this->zipCode2, 'provinceId' => $this->province2Id
			]);

			$changeHelper = new ChangeHelper($company, $action);
			$company->save();
			
			$this->id = $company->id;
			
			$this->assignContactPeopleToClient();
			
			$oldIds = CompanyGroupSetQuery::getGroupsByCompanyId($company->id);

			$changeHelper->addAttribute(
					'company_group_id',
					!empty($oldIds) ? array_column($oldIds, 'id') : [],
					!empty($this->groupIds) ? $this->groupIds : []
			);

			$changeHelper->saveChanges($company->id, new ClientModificationHistory());

			if($this->isEditScenarioSet()) {
				CompanyHelper::updateCompanyGroup($company);
			}
			else {
				$newPredefinedGroupId = CompanyHelper::addNewCompanyGroup($company);

				$ucs = new UserCompanySalesman();
				$ucs->user_id = Yii::$app->user->id;
				$ucs->company_group_id = $newPredefinedGroupId;
				$ucs->save();
			}

			$this->saveCompanyGroups($company);

			$transaction->commit();
		}
		catch(\Exception $e) {
			throw $e;
			$transaction->rollBack();
			throw new \Exception('Could not save client data');
		}
	}

	private function saveCompanyGroups($company) {
		$groups = $company->companyGroups;
		if(empty($this->groupIds)) {
			$this->groupIds = [];
		}

		$predefinedGroups = CompanyGroupSetQuery::getPredefinedGroupsByCompanyId($company->id);
		foreach($predefinedGroups as $preGroup) {
			$this->groupIds[] = $preGroup['id'];
		}

		$idsToDelete = [];
		$idsToAdd = $this->groupIds;

		if(!empty($groups)) {
			$existingIds = [];
			foreach($groups as $group) {
				$existingIds[] = $group->id;
			}

			$idsToDelete = array_diff($existingIds, $this->groupIds);
			$idsToAdd = array_diff($this->groupIds, $existingIds);
		}

		if(count($idsToDelete) > 0) {
			foreach($idsToDelete as $itd) {
				CompanyGroupSet::deleteAll(['company_id' => $company->id, 'company_group_id' => $itd]);
			}
		}

		if(count($idsToAdd) > 0) {
			foreach($idsToAdd as $ita) {
				$companyGroupSet = new CompanyGroupSet();
				$companyGroupSet->company_id = $company->id;
				$companyGroupSet->company_group_id = $ita;
				$companyGroupSet->save();
			}
		}
	}
	
	private function assignContactPeopleToClient() {
		$oldUsers = User::find()->where(['company_id' => $this->id, 'is_active' => 1])->all();
		$contactPeopleIds = [];

		foreach($this->contactPeopleForms as $contactPerson) {
			$contactPeopleIds[] = $contactPerson->id;
			$contactPerson->companyId = $this->id;
			$contactPerson->saveData();		
		}
		
		if(!empty($oldUsers)) {
			foreach($oldUsers as $oldUser) {
				if(!in_array($oldUser['id'], $contactPeopleIds)) {
					$user = User::findOne(['id' => $oldUser['id']]);
					$user->is_active = 0;
					$user->save();
					$rbacController = new RbacController('rbac', Yii::$app->module);
					$rbacController->revokeRoleFromUser(Utility::ROLE_CLIENT, $oldUser['id']);
				}
			}		
		}
	}
	
	public function load($data, $formName = NULL) {
		$result = parent::load($data);
		$this->contactPeopleForms = ModelLoaderHelper::loadMultiple($data, ClientRepresentativeForm::class, true);
		return $result;
	}
	
    public function loadData($company) {
    	$this->id = $company->id;
    	$this->name = $company->name;
    	$this->email = $company->email;
    	$this->phone1 = $company->phone1;
    	$this->phone2 = $company->phone2;
    	$this->fax1 = $company->fax1;
    	$this->vatIdentificationNumber = $company->vat_identification_number;
    	$this->taxpayerIdentificationNumber = $company->taxpayer_identification_number;
    	$this->krsNumber = $company->krs_number;
    	$this->parentCompanyId = $company->parent_company_id;
    	$this->note = $company->note;

    	$address = $company->address;
    	if(!empty($address)) {
    		$this->address1 = $address->main.(!empty($address->complement) ? ' '.$address->complement : '');
    		$this->city1 = $address->city->name;
    		$this->zipCode1 = $address->city->zip_code;

    		if(!empty($address->city->province)) {
    			$this->province1Id = $address->city->province->id;
    		}
    	}

    	$addressPostal = $company->addressPostal;
    	if(!empty($addressPostal)) {
    		$this->address2 = $addressPostal->main.(!empty($addressPostal->complement) ? ' '.$addressPostal->complement : '');
    		$this->city2 = $addressPostal->city->name;
    		$this->zipCode2 = $addressPostal->city->zip_code;

    		if(!empty($addressPostal->city->province)) {
    			$this->province2Id = $addressPostal->city->province->id;
    		}
    	}

    	$salesmen = UserCompanySalesmanQuery::getSalesmenByCompanyId($company->id);
    	if(!empty($salesmen)) {
    		$this->authorizedSalesmanIds = array_column($salesmen, 'id');
    	}

    	$groups = CompanyGroupSetQuery::getGroupsByCompanyId($company->id);
    	if(!empty($groups)) {
    		$this->groupIds = array_column($groups, 'id');
    	}
    	
    	$this->loadContactPeople();	
    }
    
    public function loadContactPeople() {
    	$contactPeople = User::find()->where(['company_id' => $this->id, 'is_active' => 1])->all();
    	
    	if(!empty($contactPeople)) {
	    	foreach($contactPeople as $contactPerson) {
	    		$userForm = new ClientRepresentativeForm();
	    		$userForm->setEditScenario();
	    		$userForm->loadData($contactPerson);	    		
	    		$this->contactPeopleForms[] = $userForm;
	    	} 	
    	}
    }

    public function getParentCompanyData() {
    	$parentCompanyData = Company::find()->where('id = :id', [':id' => $this->parentCompanyId])->one();
    	$clientData = [];
    	if(!empty($parentCompanyData)) {
    		$address = !empty($parentCompanyData->address) ? AddressHelper::getFullAddress([
    				'addressMain' => $parentCompanyData->address->main, 'cityName' => $parentCompanyData->address->city->name, 
    				'cityZipCode' => $parentCompanyData->address->city->zip_code]) : '';
    		$clientData[$parentCompanyData->id] = '['.$parentCompanyData->id.'] '.$parentCompanyData->name.(!empty($address) ? '<span class="address-highlight">, '.$address.'</span>' : '');
    	}
    	
    	return $clientData;
    }

    public function getAuthorizedSalesmenData() {
    	$salesmenData = [];
    	if(!empty($this->authorizedSalesmanIds)) {
	    	foreach($this->authorizedSalesmanIds as $authorizedSalesmanId) {
	    		$user = User::findOne($authorizedSalesmanId);
	    		$salesmenData[$user->id] = UserHelper::getPrettyUserName($user['contact_email'], $user['first_name'], $user['last_name']);
	    	}
    	}

    	return $salesmenData;
    }

    public function getGroupsData() {
    	$groupsData = [];
    	if(!empty($this->groupIds)) {
	    	foreach($this->groupIds as $groupId) {
	    		$group = CompanyGroup::findOne($groupId);
	    		$translation = $group->getTranslation();
	    		$name = !empty($translation) ? $translation->name : $group->symbol;
	    		$groupsData[$group->id] = $name;
	    	}
    	}

    	return $groupsData;
    }
    
    public function getContactPeopleForms() {
    	return $this->contactPeopleForms;	
    }

    public function setAddScenario() {
    	$this->setScenario(self::SCENARIO_ADD);
    }

    public function setEditScenario() {
    	$this->setScenario(self::SCENARIO_EDIT);
    }

    public function isEditScenarioSet() {
    	return $this->getScenario() == self::SCENARIO_EDIT;
    }
}
