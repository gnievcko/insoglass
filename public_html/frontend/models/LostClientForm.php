<?php

namespace frontend\models;

use Yii;
use yii\base\Model;

class LostClientForm extends Model {

    public $type;
    public $dateFrom;
    public $dateTo;
    public $compareDateFrom;
    public $compareDateTo;

    public function __construct($document = null, $config = []) {
        if(!empty($document)) {
            $this->loadData($document);
        }

        parent::__construct($config);
    }

    public function init() {
        parent::init();
        $this->dateFrom = date('d.m.Y', time() - 365 * 24 * 60 * 60);
        $this->dateTo = date('d.m.Y', time() - 335 * 24 * 60 * 60);

        $this->compareDateFrom = date('d.m.Y', time() - 7 * 24 * 60 * 60);
        $this->compareDateTo = date('d.m.Y', time());
    }

    public function attributeLabels() {
        return [
            'dateFrom' => Yii::t('web', 'Date from'),
            'dateTo' => Yii::t('web', 'Date to'),
            'compareDateFrom' => Yii::t('web', 'Date from'),
            'compareDateTo' => Yii::t('web', 'Date to'),
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
        ];
    }

    public function loadData($document) {
        // uzupełnić, jeśli wymagana jest edycja zadania
    }

}
