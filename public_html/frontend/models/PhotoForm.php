<?php

namespace frontend\models;

use Yii;
use yii\base\Model;

class PhotoForm extends Model {
	
    public $image;
    public $thumbnail;
    public $makeCopy;
    public $description;

    public function attributeLabels() {
        return [
            	'image' => Yii::t('web', 'Image'),
            	'thumbnail' => Yii::t('web', 'Thumbnail'),
        		'description' => Yii::t('web', 'Description'),
        ];
    }

	public function rules() {		
		return [
            [['image', 'thumbnail'], 'required'],
            [['makeCopy', 'description'], 'default', 'value' => false],
		];
	}
}
