<?php

namespace frontend\models;

use Yii;
use yii\base\Model;
use common\models\ar\Task;
use frontend\helpers\AdditionalFieldHelper;
use common\models\ar\TaskData;
use common\models\aq\TaskPriorityQuery;
use common\models\aq\TaskTypeReferenceQuery;
use frontend\helpers\UserHelper;
use common\models\aq\TaskQuery;
use common\models\aq\TaskTypeQuery;
use common\models\ar\TaskTypeReference;
use common\alerts\TaskDeadlineAlertHandler;
use common\models\ar\Product;

class TaskForm extends Model {

	const SCENARIO_ADD = 'add';
	const SCENARIO_EDIT = 'edit';

    public $id;
    public $title;
    public $priorityValue;
    public $message;
    public $dateReminder;
    public $dateDeadline;
    public $userNotifiedId;
    public $taskTypeId;
    public $fields = [];
    
    public $userNotifiedData = [];
    public $fieldsData = [];

	public function __construct($task = null, $config = []) {
		if(!empty($task)) {
			$this->loadData($task);
		}

		parent::__construct($config);
	}

	public function scenarios() {
		$scenarios = parent::scenarios();
		$scenarios[self::SCENARIO_ADD] = ['title', 'priorityValue', 'message', 'dateReminder', 'dateDeadline', 'userNotifiedId', 'taskTypeId', 'fields'];
		$scenarios[self::SCENARIO_EDIT] = ['id', 'title', 'priorityValue', 'message', 'dateReminder', 'dateDeadline', 'userNotifiedId', 'taskTypeId', 'fields'];
		return $scenarios;
	}

	public function rules() {
		return [
				[['title', 'priorityValue', 'message', 'taskTypeId'], 'required'],
				[['title'], 'string', 'max' => 128],
				[['message'], 'string', 'max' => 2048],
				[['priorityValue', 'userNotifiedId', 'taskTypeId'], 'integer'],
				[['dateReminder', 'dateDeadline'], 'safe'],
				[['fields'], 'each', 'rule' => ['integer']],
				[['fields'], 'each', 'rule' => ['default', 'value' => null]],
				['fields', 'validateAdditionalField'],
				[['dateReminder', 'dateDeadline', 'userNotifiedIds'], 'default', 'value' => null],
		];
	}

	public function validateAdditionalField($attribute, $params) {
		$errors = AdditionalFieldHelper::validateAdditionalFields($this->fields, TaskTypeReferenceQuery::getAdditionalFieldsByTaskTypeId($this->taskTypeId));
		if(!empty($errors)) {
			foreach($errors as $error) {
				$this->addError('fields['.$error['id'].']', $error['message']);
			}
		}
	}

    public function attributeLabels() {
        return [
	            'title' => Yii::t('main', 'Title'),
        		'priorityValue' => Yii::t('main', 'Priority'),
        		'message' => Yii::t('main', 'Detailed task description'),
        		'dateDeadline' => Yii::t('main', 'Deadline date'),
        		'dateReminder' => Yii::t('main', 'Reminder date'),
        		'userNotifiedId' => Yii::t('main', 'Employee receiving task'),
        		'taskTypeId' => Yii::t('main', 'Task type'),
        ];
    }

	public function saveData() {
		$task = null;
		if(!empty($this->id)) {
			$task = Task::findOne($this->id);
		}

		if(empty($task)) {
			$task = new Task();
		}
		
		$transaction = Yii::$app->db->beginTransaction();
		try {
			$isDateChanged = $this->isEditScenarioSet() && 
					((new \DateTime($task->date_reminder))->format('Y-m-d') != $this->dateReminder
					|| (new \DateTime($task->date_deadline))->format('Y-m-d') != $this->dateDeadline);
			
			$priorities = TaskPriorityQuery::getAllOrdered();
			$task->task_priority_id = $priorities[$this->priorityValue - 1]->id;

			$task->task_type_id = $this->taskTypeId;
			$task->user_id = Yii::$app->user->id;
			$task->title = $this->title;
			$task->message = $this->message;
			$task->user_notified_id = $this->userNotifiedId;
			$task->date_reminder = $this->dateReminder;
			$task->date_deadline = $this->dateDeadline;
			$task->is_active = 1;

			if(!$task->save()) {
				Yii::error(print_r($task->getErrors(), true));
				throw new \Exception('Error during saving the task.');
			}
			
			if(!empty($task->taskDatas)) {
				TaskData::deleteAll(['task_id' => $task->id]);
			}

			if(!empty($this->fields)) {
				foreach($this->fields as $k => $v) {
					if(!empty($v) && TaskTypeReference::findOne($k)->task_type_id == $this->taskTypeId) {
						$data = new TaskData();
						$data->task_id = $task->id;
						$data->task_type_reference_id = $k;
						$data->referenced_object_id = $v;
						if(!$data->save()) {
							Yii::error(print_r($data->getErrors(), true));
							throw new \Exception('Error during saving the field.');
						}
					}
				}
			}
			
			if($isDateChanged) {
				TaskDeadlineAlertHandler::check([
						'taskId' => $task->id,
						'forceIfIncomplete' => true,
				]);
			}

			$transaction->commit();
		}
		catch(\Exception $e) {
			$transaction->rollBack();
			Yii::error($e->getMessage());
			Yii::error($e->getTraceAsString());
			throw new \Exception('Could not save task data');
		}
	}

    public function loadData($task) {
    	$this->id = $task->id;
    	$this->title = $task->title;
    	$this->priorityValue = $task->task_priority_id;
    	$this->message = $task->message;
    	if(!empty($task->date_reminder)) {
    		$this->dateReminder = (new \DateTime($task->date_reminder))->format('Y-m-d');
    	}
    	if(!empty($task->date_deadline)) {
    		$this->dateDeadline = (new \DateTime($task->date_deadline))->format('Y-m-d');
    	}
    	$this->userNotifiedId = $task->user_notified_id;
    	$this->taskTypeId = $task->task_type_id;
    	
    	if(!empty($task->userNotified)) {
    		$this->userNotifiedData = [$task->user_notified_id => UserHelper::getPrettyUserName($task->userNotified->email, 
    				$task->userNotified->first_name, $task->userNotified->last_name)];
    	}
    	
    	$additionalFields = TaskTypeReferenceQuery::getAdditionalFieldsByTaskTypeIds(
    			array_column(TaskTypeQuery::getAllTypes(), 'id'));
    	foreach($additionalFields as $af) {
    		$this->fields[$af['id']] = null;
    	}
    	
    	$references = TaskQuery::getTaskReferences($task->id);
    	if(!empty($references)) {
    		foreach($references as $ref) {
    			$name = $ref['name'];
    			if($ref['table'] == 'product') {
    				$name = Product::findOne($ref['id'])->getTranslation()->name;
    			}
    			
    			$this->fields[$ref['referenceId']] = $ref['id'];
    			$this->fieldsData[$ref['referenceId']] = ['id' => $ref['id'], 'name' => $name];
    		}
    	}
    }

    public function setAddScenario() {
    	$this->setScenario(self::SCENARIO_ADD);
    }

    public function setEditScenario() {
    	$this->setScenario(self::SCENARIO_EDIT);
    }

    public function isEditScenarioSet() {
    	return $this->getScenario() == self::SCENARIO_EDIT;
    }

    public function setAdditionalFields($additionalFields, $initialValues = []) {
    	foreach($additionalFields as $field) {
    		$this->fields[$field['id']] = isset($initialValues[$field['id']]) ? $initialValues[$field['id']]['id'] : null;
    	}
    }
}
