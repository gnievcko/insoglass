<?php

namespace frontend\models;

use Yii;
use yii\base\Model;

class TaskObjectListForm extends Model {

	public $page;
	public $sortDir;
	public $sortField;

	const DEFAULT_PAGE = 0;
	const DEFAULT_SORT_DIR = 'desc';
	const DEFAULT_SORT_FIELD = 'dateDeadline';

	public function init() {
		parent::init();
		$this->page = self::DEFAULT_PAGE;
		$this->sortDir = self::DEFAULT_SORT_DIR;
		$this->sortField = self::DEFAULT_SORT_FIELD;
	}

	public static function getSortFields() {
		return [
				['name' => 'title', 'label' => Yii::t('main', 'Title'), 'isSortable' => true],
				['name' => 'type', 'label' => Yii::t('web', 'Type'), 'isSortable' => true],
				['name' => 'userAssigned', 'label' => Yii::t('web', 'Assigned employee'), 'isSortable' => true],
				['name' => 'message', 'label' => Yii::t('main', 'Detailed task description'), 'isSortable' => true],
				['name' => 'priority', 'label' => Yii::t('main', 'Priority'), 'isSortable' => true],
				['name' => 'dateCreation', 'label' => Yii::t('main', 'Creation date'), 'isSortable' => true],
				['name' => 'dateDeadline', 'label' => Yii::t('main', 'Deadline date'), 'isSortable' => true],
		];
	}

	public function attributeLabels() {
		return [
				
		];
	}

	/**
	 * @inheritdoc
	 */
	public function rules() {
		return [
				[['page'], 'integer'],
				[['sortDir'], 'in', 'range' => ['asc', 'desc']],
				[['sortField'], 'in', 'range' => array_column(self::getSortFields(), 'name')],
				['page', 'default', 'value' => self::DEFAULT_PAGE],
				['sortDir', 'default', 'value' => self::DEFAULT_SORT_DIR],
				['sortField', 'default', 'value' => self::DEFAULT_SORT_FIELD],
		];
	}
}
