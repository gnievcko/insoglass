<?php

namespace frontend\models;

use Yii;
use yii\base\Model;
use kartik\password\StrengthValidator;
use common\models\ar\User;
use common\helpers\Utility;

class PasswordChangeForm extends Model {
	
	public $currentPassword;
	public $newPassword;
	public $newPasswordRepeated;
	
	public function rules() {		
		return [
				[['currentPassword', 'newPassword', 'newPasswordRepeated'], 'required'],
				['currentPassword', 'validatePassword'],
				['newPasswordRepeated', 'compare', 'compareAttribute' => 'newPassword', 'message' => Yii::t('web', 'New password must be repeated correctly.')],
		];
	}
	
	public function attributeLabels() {
		return [
				'currentPassword' => Yii::t('web', 'Current password'),
				'newPassword' => Yii::t('web', 'New password'),
				'newPasswordRepeated' => Yii::t('web', 'Repeat new password'),
		];
	}
	
	public function validatePassword($attribute, $params) {
		$user = User::find()->where('id = :id', [':id' => Yii::$app->user->id])->one();
            
        if(!$user || !$user->validatePassword($this->currentPassword)) {
        	$this->addError($attribute, Yii::t('web', 'Incorrect current password.'));
        }
    }
    
    public function saveFormData($user) {
    	$user->setPassword($this->newPassword);
    	
    	if(!$user->save()) {
			throw new \Exception('Could not save new password');
		}
    }
	
}