<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace frontend\models;

use yii\base\Model;
use Yii;
use common\helpers\StringHelper;
use yii\helpers\ArrayHelper;
use common\models\aq\DocumentTypeQuery;
/**
 * Description of DocumentListForm
 *
 * @author Bartek
 */
class OrderDocumentListForm extends Model {

    public $filterString;
    public $clientName;
    public $parentClientName;
    public $orderNumber;
    public $type;
    public $address;
    public $dateFrom;
    public $dateTo;
    public $author;
    public $page;
    public $sortDir;
    public $sortField;

    const DEFAULT_PAGE = 0;
    const DEFAULT_SORT_DIR = 'desc';
    const DEFAULT_SORT_FIELD = 'history.createdAt';

    public function init() {
        parent::init();
        $this->page = self::DEFAULT_PAGE;
        $this->sortDir = self::DEFAULT_SORT_DIR;
        $this->sortField = self::DEFAULT_SORT_FIELD;
        $this->type = [];
    }

    public static function getSortFields() {
        return [
                ['name' => 'type', 'label' => Yii::t('main', 'Type'), 'isSortable' => false],
                ['name' => 'history.createdAt', 'label' => Yii::t('main', 'Date'), 'isSortable' => true],
                ['name' => 'history.sections.number', 'label' => Yii::t('web', 'Number'), 'isSortable' => true],
                ['name' => 'Client', 'label' => StringHelper::translateOrderToOffer('main', 'Client'), 'isSortable' => false, 'style' => 'width: 30%'],
        		['name' => 'order', 'label' => StringHelper::translateOrderToOffer('main', 'Order'), 'isSortable' => false],
                ['name' => 'Author', 'label' => StringHelper::translateOrderToOffer('web', 'Author'), 'isSortable' => false],
        ];
    }

    public function attributeLabels() {
        return [
            	'Filter' => Yii::t('web', 'Filter'),
        		'dateFrom' => Yii::t('web', 'Issued after (inclusive)'),
        		'dateTo' => Yii::t('web', 'Issued before (inclusive)'),
        		'author' => Yii::t('web', 'First or last name'),
        		'address' => Yii::t('main', 'Address'),
        ];
    }

    public function rules() {
        return [
                [['filterString', 'type', 'clientName', 'address', 'dateFrom', 'dateTo', 'author', 
                		'orderNumber', 'parentClientName'], 'safe'],
                [['page', 'allRolesCount'], 'integer'],
                [['sortDir'], 'in', 'range' => ['asc', 'desc']],
                [['sortField'], 'in', 'range' => array_column(self::getSortFields(), 'name')],
                ['page', 'default', 'value' => self::DEFAULT_PAGE],
                ['sortDir', 'default', 'value' => self::DEFAULT_SORT_DIR],
                ['sortField', 'default', 'value' => self::DEFAULT_SORT_FIELD],
        ];
    }

    public function getTypeList() {
		return ArrayHelper::map(DocumentTypeQuery::getDocumentTypes(1), 'symbol', 'name');
    }
    
    public function name() {
        return 'OrderDocumentList';
    }
}
