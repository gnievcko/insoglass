<?php

namespace frontend\models;

use Yii;
use yii\base\Model;
use common\models\ar\ProductionTask;
use common\models\ar\ProductionTaskHistory;
use frontend\logic\ProductionTaskLogic;

class ProductionAddItemsForm extends Model {
	
    public $completedCount;
    public $damagedCount;
    public $productionTaskId;
    public $workstationId;
    public $message;
    
    public $dateStart;
    public $dateStop;
    
    public function __construct($productionTaskId = null, $config = []) {
		$this->productionTaskId = $productionTaskId;
		parent::__construct($config);
	}

    public function attributeLabels() {
        return [
            'completedCount' => Yii::t('web', 'Number of completed'),
        	'damagedCount' => Yii::t('web', 'Number of damaged'),
            'workstationId' => Yii::t('web', 'Workstation'),
            'message' => Yii::t('main', 'Message'),
            'dateStart' => Yii::t('web', 'Date start'),
            'dateStop' => Yii::t('web', 'Date stop'),
        ];
    }

	public function rules() {		
		return [
		    [['workstationId'], 'required'],
            [['completedCount', 'damagedCount', 'workstationId'], 'integer'],
		    [['completedCount', 'damagedCount'], 'default', 'value' => 0],
		    [['completedCount'], 'validateCompletedCount'],
		    [['message'], 'string', 'max' => 1024],
		    [['dateStart', 'dateStop'], 'date'],
		];
	}
	
    public function validateCompletedCount($attribute, $params) {
        $productionTask = ProductionTask::findOne($this->productionTaskId);
        $updatedCompletedCount = $productionTask->total_count_made + $this->completedCount;
        
        if(!empty($productionTask->orderOfferedProduct->count) && $updatedCompletedCount > $productionTask->orderOfferedProduct->count) {
            $this->addError('completedCount', Yii::t('web', 'Completed count cannot be greater than total count of this product to produce'));
        }
        
        if(!empty($productionTask->task_required_ids)) {
            $requiredTasks = (new ProductionTaskLogic())->getRequiredConcreteTasks($productionTask, true);
            
            if(!empty($requiredTasks)) {
                foreach($requiredTasks as $reqTask) {
                    if(empty($reqTask->productionPathStep->activity->is_node) && $updatedCompletedCount > $reqTask->total_count_made) {
                        $this->addError('completedCount', Yii::t('web', 'Completed count cannot be greater than currently produced item count in previous tasks'));
                    }
                }
            }
        }
	}
	
	public function save() {
	    $productionTask = ProductionTask::findOne(['id' => $this->productionTaskId]);
	    $productionTask->total_count_made += $this->completedCount;
	    $productionTask->total_count_failed += $this->damagedCount;
	    
	    if(!$productionTask->save()) {
            throw new \Exception('Production task cannot be updated: '.print_r($productionTask->getErrors(), true));    
	    }
	    
	    (new ProductionTaskLogic())->updateProductionPath($productionTask);	    

	    $history = new ProductionTaskHistory();
	    $history->count_made = $this->completedCount;
	    $history->user_id = \Yii::$app->user->id;
	    $history->count_failed = $this->damagedCount;
	    $history->production_task_id = $this->productionTaskId;
	    $history->workstation_id = $this->workstationId;
	    $history->message = !empty($this->message) ? $this->message : null;
	    if(!$history->save()) {
	        throw new \Exception('Production task history cannot be saved: '.print_r($history->getErrors(), true));
	    }
	}
	
	
}
