<?php

namespace frontend\models;

use yii\base\Model;

class ProductForm extends Model {

    public $categoriesIds;
    public $index;
    public $countMinimal;
    public $initialCount;
    public $warehouseId;
    public $isVisibleForClient;
    public $isAvailable;
    public $stillage;
    public $shelf;
	public $warehouseSupplierId;

    public function attributeLabels() {
        return [
            'categoriesIds' => \Yii::t('web', 'Categories'),
            'index' => \Yii::t('main', 'Warehouse index'),
            'countMinimal' => \Yii::t('main', 'Desired minimal count'),
            'warehouseId' => \Yii::t('main', 'Warehouse'),
            'initialCount' => \Yii::t('web', 'Initial count'),
            'isVisibleForClient' => \Yii::t('main', 'Is visible for client'),
            'isAvailable' => \Yii::t('web', 'Product is available in offer'),
            'stillage' => \Yii::t('web', 'Stillage'),
            'shelf' => \Yii::t('web', 'Shelf'),
        	'warehouseSupplierId' => \Yii::t('web', 'Supplier'),
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            [['isVisibleForClient', 'isAvailable', 'warehouseSupplierId'], 'safe'],
            [['categoriesIds', 'initialCount', 'warehouseId', 'index'], 'safe'],
            [['countMinimal', 'initialCount', 'warehouseId', 'warehouseSupplierId'], 'integer'],
            [['index'], 'string', 'max' => 48],
            [['stillage', 'shelf'], 'string', 'max' => 20],
            [['index', 'stillage', 'shelf'], 'default', 'value' => null],
        ];
    }

}
