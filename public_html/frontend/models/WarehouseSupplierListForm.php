<?php

namespace frontend\models;

use Yii;
use yii\base\Model;

class WarehouseSupplierListForm extends Model {
	
    public $page;
    public $sortDir;
    public $sortField;
    
    public $nameFilterString;

    const DEFAULT_PAGE = 0;
    const DEFAULT_SORT_DIR = 'asc';
    const DEFAULT_SORT_FIELD = 'name';

    public function init() {
        parent::init();
        $this->page = self::DEFAULT_PAGE;
        $this->sortDir = self::DEFAULT_SORT_DIR;
        $this->sortField = self::DEFAULT_SORT_FIELD;
    }

    public static function getSortFields() { 
        return [
        		['name' => 'name', 'label' => Yii::t('main', 'Name'), 'isSortable' => true, 'style' => 'width: 20%'],
        		['name' => 'contact_person', 'label' => Yii::t('web', 'Contact person'), 'isSortable' => true],
        		['name' => 'phone', 'label' => Yii::t('main', 'Phone'), 'isSortable' => true],
        		['name' => 'email', 'label' => Yii::t('main', 'E-mail address'), 'isSortable' => true],
        		['name' => 'address', 'label' => Yii::t('main', 'Address'), 'isSortable' => true, 'style' => 'width: 25%'],
        		['name' => 'date_creation', 'label' => Yii::t('main', 'Creation date'), 'isSortable' => true, 'style' => 'width: 10%'],
        ];
    }

    public function attributeLabels() {
        return [
        		'Name' => Yii::t('web', 'Write supplier name'),
        		'Warehouse' => Yii::t('web', 'Warehouse'),
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
				[['page'], 'integer'],
				[['sortDir'], 'in', 'range' => ['asc', 'desc']],
				[['sortField'], 'in', 'range' => array_column(self::getSortFields(), 'name')],     		
				[['nameFilterString', 'address'], 'safe'],
	            ['page', 'default', 'value' => self::DEFAULT_PAGE],
	            ['sortDir', 'default', 'value' => self::DEFAULT_SORT_DIR],
	            ['sortField', 'default', 'value' => self::DEFAULT_SORT_FIELD],
        ];
    }
}