<?php

namespace frontend\models;

use Yii;
use yii\base\Model;

class ListOfNItemsForm extends Model {

    public $type;
    public $date;
    public $clientId;

    public function __construct($document = null, $config = []) {
        if(!empty($document)) {
            $this->loadData($document);
        }

        parent::__construct($config);
    }

    public function init() {
        parent::init();
    }
    
        public function attributeLabels() {
        return [
            'date' => Yii::t('web', 'Date'),
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
        ];
    }

    public function loadData($document) {
        // uzupełnić, jeśli wymagana jest edycja zadania
    }

}
