<?php

namespace frontend\models;

use Yii;
use yii\base\Model;
use yii\base\Object;
use frontend\helpers\ModelLoaderHelper;
use yii\helpers\Json;
use common\models\ar\WarehouseProduct;
use common\models\ar\WarehouseProductStatusHistory;
use common\models\ar\ProductStatus;
use common\helpers\Utility;
use frontend\helpers\AdditionalFieldHelper;
use common\models\ar\WarehouseProductStatusHistoryData;
use common\models\aq\WarehouseQuery;
use common\models\ar\WarehouseDelivery;
use common\helpers\RandomSymbolGenerator;
use yii\db\Expression;
use common\models\ar\WarehouseDeliveryProduct;
use common\models\aq\ProductStatusReferenceQuery;
use common\alerts\ProductMinimalCountAlertHandler;
use common\models\aq\WarehouseProductQuery;
use common\helpers\NumberTemplateHelper;
use yii\helpers\ArrayHelper;
use common\models\ar\WarehouseProductOperation;

class WarehouseProductForm extends Model {
	
	const SCENARIO_ADD = 'add';
	const SCENARIO_UPDATE = 'update';
	
    public $warehouseId;    
    public $description;
    public $warehouseProductItemForms = [];
    
    public $productStatusId;
    public $fields = [];
    
    private $indexHelpMap = [];
    
    public function __construct($config = []) {
    	parent::__construct($config);
    }
	
	public function scenarios() {
		$scenarios = parent::scenarios();
		$scenarios[self::SCENARIO_ADD] = ['warehouseId', 'description'];
		$scenarios[self::SCENARIO_UPDATE] = ['warehouseId', 'description', 'productStatusId', 'fields'];
		return $scenarios;
	}
	
	public function rules() {
		return [
				[['warehouseId'], 'required'],
				[['productStatusId'], 'required', 'on' => [self::SCENARIO_UPDATE], 'message' => Yii::t('web', 'You have to select operation type.')],
				[['warehouseId', 'productStatusId'], 'integer'],
				[['description'], 'string', 'max' => 2048],
				[['description'], 'default', 'value' => null],
				//[['fields'], 'each', 'rule' => ['integer']],
				[['fields'], 'each', 'rule' => ['default', 'value' => null]],
				['fields', 'validateAdditionalField', 'on' => [self::SCENARIO_UPDATE]],
				['productStatusId', 'validateItem', 'on' => [self::SCENARIO_UPDATE]],
		];
	}
	
    public function attributeLabels() {
        return [
        		'warehouseId' => Yii::t('main', 'Warehouse'),
        		'description' => Yii::t('web', 'Operation comment'),
        ];
    }
    
	public function load($data, $formName = NULL) {
		$this->reindexItems($data);
		
        $result = parent::load($data);
        $this->warehouseProductItemForms = ModelLoaderHelper::loadMultiple($data, WarehouseProductItemForm::class);
        
        return $result;
    }

    public function validate($attributeNames = NULL, $clearErrors = true) {
        return parent::validate() && Model::validateMultiple($this->warehouseProductItemForms);
    }
    
	public function validateAdditionalField($attribute, $params) {
		$errors = AdditionalFieldHelper::validateAdditionalFields($this->fields, ProductStatusReferenceQuery::getAdditionalFieldsByProductStatusId($this->productStatusId));
		if(!empty($errors)) {
			foreach($errors as $error) {
				$this->addError('fields['.$error['id'].']', $error['message']);
			}
		}
	}
	
	public function validateItem($attribute, $params) {
		if(!empty($this->warehouseProductItemForms) && !empty($this->warehouseId)) {
			$reservationStatusId = ProductStatus::find()->where(['symbol' => Utility::PRODUCT_STATUS_RESERVATION])->one()->id;
			
			foreach($this->warehouseProductItemForms as $k => $item) {
				$wp = WarehouseProduct::find()->where(['warehouse_id' => $this->warehouseId, 
						'product_id' => $item->productId])->one();
				if(!empty($wp) && $wp->count_available < $item->count && $this->productStatusId != $reservationStatusId) {
					$this->addError('WarehouseProductItemForm['.$k.'][count]', Yii::t('web', 'There is no enough items of this product.'));
				}
			}
		}
	}
    
	public function saveData() {		
		$transaction = Yii::$app->db->beginTransaction();
		try {
			if(!$this->isUpdateScenarioSet()) {
				$nts = NumberTemplateHelper::getAndSaveNextNumber('', Utility::NUMBER_TEMPLATE_GOODS_RECEIVED);
				
				$delivery = new WarehouseDelivery();
				$delivery->warehouse_id = $this->warehouseId;
				$delivery->number = $nts['number'];
				$delivery->user_confirming_id = Yii::$app->user->id;
				$delivery->is_artificial = 1;
				$delivery->date_delivery = new Expression('NOW()');
				$delivery->description = $this->description;
				$delivery->is_active = 1;
				$delivery->is_confirmed = 1;
				$delivery->save();
			}
			
			if(!empty($this->warehouseProductItemForms)) {
				$status = null;
				if(!$this->isUpdateScenarioSet()) {
					$status = ProductStatus::find()->where('symbol = :symbol', [':symbol' => Utility::PRODUCT_STATUS_ADDITION])->one();
				}
				else {
					$status = ProductStatus::findOne($this->productStatusId);;
				}
				
				$operation = new WarehouseProductOperation();
				$operation->warehouse_id = $this->warehouseId;
				$operation->user_confirming_id = Yii::$app->user->id;
				$operation->description = $this->description;
				$operation->product_status_id = $status->id;				
				
				if(in_array($status->symbol, [Utility::PRODUCT_STATUS_ISSUING, Utility::PRODUCT_STATUS_ISSUING_RESERVATION])) {
					$number = NumberTemplateHelper::getAndSaveNextNumber('', Utility::NUMBER_TEMPLATE_GOODS_ISSUE);
					$operation->number = $number['number'];
					$operation->is_accounted = 0;
				}
				
				$operation->save();
				
				foreach($this->warehouseProductItemForms as $item) {
					$warehouseProduct = WarehouseProduct::find()->where(
							'product_id = :productId AND warehouse_id = :warehouseId', [
								':productId' => $item->productId, ':warehouseId' => $this->warehouseId
							])->one();
							
					if(empty($warehouseProduct)) {
						$warehouseProduct = new WarehouseProduct();
						$warehouseProduct->product_id = $item->productId;
						$warehouseProduct->warehouse_id = $this->warehouseId;
						$warehouseProduct->count = 0;
						$warehouseProduct->count_available = 0;
						$warehouseProduct->save();
					}
					
					$history = new WarehouseProductStatusHistory();
					$history->warehouse_product_id = $warehouseProduct->id;
					$history->user_confirming_id = Yii::$app->user->id;
					$history->count = $item->count;
					$history->unit = $item->unit;
					$history->description = $this->description;
					$history->operation_id = $operation->id;
					
					$isAdding = 1;
					
					if(!$this->isUpdateScenarioSet()) {						
						$history->product_status_id = $status->id;
						$history->warehouse_delivery_id = $delivery->id;
						$history->save();
						
						$isAdding = $status->is_positive;
						if($isAdding > 0) {
							$warehouseProduct->count += $item->count;
							$warehouseProduct->count_available += $item->count;
						}
						else {
							$warehouseProduct->count -= $item->count;
							$warehouseProduct->count_available -= $item->count;
						}
						
						$warehouseProduct->save();
						
						$deliveryProduct = new WarehouseDeliveryProduct();
						$deliveryProduct->warehouse_delivery_id = $delivery->id;
						$deliveryProduct->product_id = $item->productId;
						$deliveryProduct->count = $item->count;
						$deliveryProduct->unit = $item->unit;
						$deliveryProduct->save();
					}
					else {
						$history->product_status_id = $this->productStatusId;
						$history->save();
						
						if(!empty($this->fields)) {
							$fieldTypes = ArrayHelper::map(
									ProductStatusReferenceQuery::getFieldTypesByIds(
											array_keys(
													array_filter($this->fields, function($e) { 
														return !is_null($e); 
													})
											)
									), 'id', 'fieldType');
							
							foreach($this->fields as $k => $v) {
								if(!empty($v)) {
									$historyData = new WarehouseProductStatusHistoryData();
									$historyData->history_id = $history->id;
									$historyData->product_status_reference_id = $k;
									
									if(isset($fieldTypes[$k]) && $fieldTypes[$k] == Utility::ADDITIONAL_FIELD_TYPE_SELECT2) {
										$historyData->referenced_object_id = $v;
									}
									else {
										$historyData->custom_value = $v;
									}
									
									$historyData->save();
								}
							}
						}

						$isAdding = $status->is_positive;
						if($isAdding > 0) {
							$warehouseProduct->count += $item->count;
							$warehouseProduct->count_available += $item->count;
						}
						else {
							if($status->symbol == Utility::PRODUCT_STATUS_RESERVATION) {
								$warehouseProduct->count_available -= $item->count;
							}
							else {
								$warehouseProduct->count -= $item->count;
								$warehouseProduct->count_available -= $item->count;
							}
						}
						
						$warehouseProduct->save();
					}
					
					if($isAdding > 0) {
						ProductMinimalCountAlertHandler::check(['productId' => $warehouseProduct->product_id]);
					}
					else {
						$result = WarehouseProductQuery::getCountByProductId($warehouseProduct->product_id);
						if(!empty($result) && !empty($result['countMinimal']) && $result['count'] < $result['countMinimal']) {
							ProductMinimalCountAlertHandler::add(['productId' => $warehouseProduct->product_id, 'userId' => Yii::$app->user->id]);
						}
					}
				}
			}
			
			$transaction->commit();
		}
		catch(\Exception $e) {
			$transaction->rollBack();
			Yii::error($e->getMessage());
			Yii::error($e->getTraceAsString());
			throw new \Exception('Could not save data about warehouse operations');
		}		
	}
    
    public function setAddScenario() {
    	$this->setScenario(self::SCENARIO_ADD);
    }
    
    public function setUpdateScenario() {
    	$this->setScenario(self::SCENARIO_UPDATE);
    }
    
    public function isUpdateScenarioSet() {
    	return $this->getScenario() == self::SCENARIO_UPDATE;
    }
    
    private function reindexItems(&$data) {
    	if(isset($data['WarehouseProductItemForm']) && !empty($data['WarehouseProductItemForm'])) {
    		$this->indexHelpMap = [];
    		$i = 0;    		
    		ksort($data['WarehouseProductItemForm']);
    		
    		foreach($data['WarehouseProductItemForm'] as $k => $v) {
    			$this->indexHelpMap[$i++] = $k;
    		}
    		$data['WarehouseProductItemForm'] = array_values($data['WarehouseProductItemForm']);
    	}
    }
    
    public function translateErrorMessages($errors) {
    	if(!empty($errors) && !empty($this->indexHelpMap)) {
    		$newErrors = [];
    		foreach($errors as $k => $v) {
    			// hak, ale umożliwiający ręczną walidację elementów tablicy
    			$key = str_replace('warehouseproductform-warehouseproductitemform', 'warehouseproductitemform', $k);
    			
    			$index = explode('-', $key);
    			
    			if($index[0] !== 'warehouseproductitemform') {
    				$key = implode('-', $index);
    				$newErrors[$key] = $v;
    				continue;
    			}
    			
    			if(count($index) < 3) {
    				continue;
    			}
    			
    			$index[1] = $this->indexHelpMap[$index[1]];
    			$newKey = implode('-', $index);
    			$newErrors[$newKey] = $v;
    		}
    		return $newErrors;
    	}
    	
    	return $errors;
    }
    
    public function setAdditionalFields($additionalFields) {
    	foreach($additionalFields as $field) {
    		$this->fields[$field['id']] = null;
    	}
    }
}
