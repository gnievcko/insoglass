<?php

namespace frontend\models;

use Yii;
use yii\base\Model;
use common\helpers\Utility;
use common\helpers\UploadHelper;
use common\models\ar\OrderOfferedProductAttachment;

class OrderProductForm extends Model {

    public $id;
    public $productId;
    public $price;
    public $currencyId;
    public $count;
    public $productType;
    public $vatRate;
    public $unit;
    public $netValue;
    public $vatAmount;
    public $grossValue;
    public $width;
    public $height;
    public $shapeId;
    public $constructionId;
    public $muntinId;
    public $remarks;
    public $remarks2;
    public $files = [];
    public $photos = [];
    public $ralColourId;
    public $ralColourSymbol;
    public $ralRgbHash;
    public $urlPhoto;
    public $heightPhoto;
    public $widthPhoto;
    public $topPhoto;
    public $leftPhoto;

    public function rules() {
        return [
            [['unit', 'vatRate'], 'safe'],
            [['width', 'height', 'shapeId', 'constructionId', 'muntinId', 'ralColourId'], 'integer'],
            [['width', 'height'], 'isValidDimension'],
            ['price', 'number', 'numberPattern' => '/^\s*[+-]?[0-9]{1,8}[.,]?[0-9]{0,2}\s*$/'],
            ['count', 'number', 'numberPattern' => '/^\s*[+]?[1-9][0-9]*\s*$/'],
            ['productType', 'in', 'range' => [Utility::PRODUCT_TYPE_PRODUCT, Utility::PRODUCT_TYPE_OFFERED_PRODUCT]],
            [['remarks', 'remarks2'], 'string', 'max' => 512],
            [['ralRgbHash', 'ralColourSymbol'], 'string'],
            [['productId', 'count', 'currencyId'], 'validateAll'],
            [['remarks', 'remarks2'], 'default', 'value' => null],
            [['files', 'photos', 'urlPhoto', 'heightPhoto', 'widthPhoto', 'topPhoto', 'leftPhoto'], 'safe']
        ];
    }

    public function init() {
        parent::init();
        $this->unit = 'szt.';
        $this->netValue = '0.0';
        $this->vatAmount = '0.0';
        $this->grossValue = '0.0';
        $this->vatRate = Yii::$app->params['defaultVatRate'];
        $this->urlPhoto = '';
        $this->heightPhoto = 0;
        $this->widthPhoto = 0;
        $this->topPhoto = 0;
        $this->leftPhoto = 0;
    }

    public function attributeLabels() {
        $priceLabel = '';
        if(Yii::$app->params['isOrderProductDimensionsVisible']) {
            $priceLabel = Yii::t(Yii::$app->params['isPlaceholderInputLabel'] ? 'web' : 'main', 'Price [per sq. m]');
        } else {
            $priceLabel = Yii::t('web', 'Price');
        }

        return [
            'productId' => Yii::t('web', 'Product'),
            'price' => $priceLabel,
            'currencyId' => Yii::t('main', 'Currency'),
            'count' => Yii::t('web', 'Number of pieces'),
            'vatRate' => Yii::t('web', 'Vat rate'),
            'unit' => Yii::t('main', 'Unit'),
            'width' => Yii::t('web', 'Width') . ' [mm]',
            'height' => Yii::t('web', 'Height') . ' [mm]',
            'shapeId' => Yii::t('web', 'Shape'),
            'constructionId' => Yii::t('web', 'Construction'),
            'muntinId' => Yii::t('web', 'Muntin'),
            'remarks' => Yii::t('web', 'Remarks 1'),
            'remarks2' => Yii::t('web', 'Remarks 2'),
        ];
    }

    public function isValidDimension($attribute, $params) {
        if(!empty($this->width) && is_numeric($this->width)) {
            if($this->width > \Yii::$app->params['maxDimensionShort']) {
                $this->addError('width', \Yii::t('web', '{attribute} cannot be wider than ', ['attribute' => $this->getAttributeLabel('width')]) . \Yii::$app->params['maxDimensionShort']);
            }
        }

        if(!empty($this->height) && is_numeric($this->height)) {
            if($this->height > \Yii::$app->params['maxDimensionLong']) {
                $this->addError('height', \Yii::t('web', '{attribute} cannot be higher than ', ['attribute' => $this->getAttributeLabel('height')]) . \Yii::$app->params['maxDimensionLong']);
            }
        }

        if(!empty($this->height) && is_numeric($this->height) && !empty($this->width) && is_numeric($this->width)) {
            if($this->height > \Yii::$app->params['maxDimensionShort'] && $this->width > \Yii::$app->params['maxDimensionShort']) {
                $this->addError('width', \Yii::t('web', 'Total size cannot exceed ', ['attribute' => $this->getAttributeLabel('width')]) . \Yii::$app->params['maxDimensionLong'] . 'mm x ' . \Yii::$app->params['maxDimensionShort'] . 'mm.');
            }
        }
    }

    public function validateAll($attribute, $params) {
        if(!empty($this->productId) || !empty($this->price) || !empty($this->count)) {
            if(empty($this->productId)) {
                $this->addError('productId', \Yii::t('web', '{attribute} field must not be empty', ['attribute' => $this->getAttributeLabel('productId')]));
            }
            if(empty($this->currencyId)) {
                $this->addError('currencyId', \Yii::t('web', '{attribute} field must not be empty', ['attribute' => $this->getAttributeLabel('currencyId')]));
            }
            if(Yii::$app->params['isOrderProductCountRequired'] && empty($this->count)) {
                $this->addError('count', \Yii::t('web', '{attribute} field must not be empty', ['attribute' => $this->getAttributeLabel('count')]));
            }
        } else {
            $this->currencyId = '';
        }
    }

    public function restoreFiles() {
        $this->files = array_map(function($file) {
            $documentForm = new DocumentForm();
            $documentForm->document = UploadHelper::getFileNameByUrl($file->url_file);
            $documentForm->name = $file->name;
            $documentForm->makeCopy = true;

            return $documentForm;
        }, OrderOfferedProductAttachment::find()->where(['order_offered_product_id' => $this->id])->all());
    }

}
