<?php

namespace frontend\models;

use Yii;
use yii\base\Model;

class WarehouseProductListForm extends Model {
	
    public $page;
	public $name;
    public $sortDir;
    public $sortField;
    public $categoryIds;
    public $warehouseIds;
    public $location;
    
    public $allCategoriesCount;
    public $allWarehousesCount;

    const DEFAULT_PAGE = 0;
    const DEFAULT_SORT_DIR = 'asc';
    const DEFAULT_SORT_FIELD = 'name';

    public function init() {
        parent::init();
        $this->page = self::DEFAULT_PAGE;
        $this->sortDir = self::DEFAULT_SORT_DIR;
        $this->sortField = self::DEFAULT_SORT_FIELD;
    }

    public static function getSortFields() {
        $fields=[];
        if(Yii::$app->params['isProductPhotoOnListVisible']){
            $fields = [['name' => 'photo', 'label' => Yii::t('main', 'Photo'), 'isSortable' => false, 'style' => 'width: 8%']];
        }
        $fields = array_merge($fields,[
            ['name' => 'name', 'label' => Yii::t('main', 'Name'), 'isSortable' => true, 'style' => 'width: 20%'],
            ['name' => 'stillageShelf', 'label' => Yii::t('web', 'Stillage/shelf'), 'isSortable' => true, 'style' => 'width: 10%'],
            //['name' => 'alias', 'label' => Yii::t('web', 'Common names'), 'isSortable' => true],
            ['name' => 'supplier', 'label' => Yii::t('web', 'Supplier'), 'isSortable' => true],
            ['name' => 'category', 'label' => Yii::t('web', 'Categories'), 'isSortable' => true],
            ['name' => 'count', 'label' => Yii::t('web', 'Count'), 'isSortable' => true, 'style' => 'width: 8%'],
            ['name' => 'lastDeliveryCost', 'label' => Yii::t('web', 'Last delivery cost'), 'isSortable' => true, 'style' => 'width: 14%'],
            ['name' => 'dateModification', 'label' => Yii::t('main', 'Last modification date'), 'isSortable' => true, 'style' => 'width: 10%'],
        ]);
        return $fields;
    }

    public function attributeLabels() {
        return [
        		'name' => Yii::t('web', 'Write product name'),
        		'categoryIds' => Yii::t('web', 'Categories'),
        		'warehouseIds' => Yii::t('main', 'Warehouses'),
        		'location' => Yii::t('web', 'Stillage/shelf'),
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
				[['page', 'allCategoriesCount', 'allWarehousesCount'], 'integer'],
        		[['categoryIds', 'warehouseIds'], 'each', 'rule' => ['integer']],
				[['sortDir'], 'in', 'range' => ['asc', 'desc']],
				[['sortField'], 'in', 'range' => array_column(self::getSortFields(), 'name')],
				[['name', 'location', 'stillageShelf'], 'safe'],
	            ['page', 'default', 'value' => self::DEFAULT_PAGE],
	            ['sortDir', 'default', 'value' => self::DEFAULT_SORT_DIR],
	            ['sortField', 'default', 'value' => self::DEFAULT_SORT_FIELD],
        ];
    }
}
