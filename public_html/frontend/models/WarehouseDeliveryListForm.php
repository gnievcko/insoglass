<?php

namespace frontend\models;

use Yii;
use yii\base\Model;

class WarehouseDeliveryListForm extends Model {
	
    public $page;
    public $sortDir;
    public $sortField;
    
    public $number;
    public $warehouseName;
    public $supplierName;
    public $userConfirmingName;
    public $dateDelivery;
    
    public $positionCount;
    public $totalPrice;
    public $currencySymbol;
    
    public $warehouseIds;
    public $supplierIds;
    public $numberFilterString;
    
    public $allSuppliersCount;
    public $allWarehousesCount;

    const DEFAULT_PAGE = 0;
    const DEFAULT_SORT_DIR = 'asc';
    const DEFAULT_SORT_FIELD = 'number';

    public function init() {
        parent::init();
        $this->page = self::DEFAULT_PAGE;
        $this->sortDir = self::DEFAULT_SORT_DIR;
        $this->sortField = self::DEFAULT_SORT_FIELD;
    }

    public static function getSortFields() { 
        return [
        		['name' => 'number', 'label' => Yii::t('web', 'Delivery number'), 'isSortable' => true, 'isHidden' => false && !Yii::$app->params['isMoreWarehousesAvailable'], 'style' => 'width: 15%'],
        		['name' => 'warahouseName', 'label' => Yii::t('main', 'Warehouse'), 'isSortable' => true, 'isHidden' => true && !Yii::$app->params['isMoreWarehousesAvailable']],
	            ['name' => 'supplierName', 'label' => Yii::t('web', 'Supplier'), 'isSortable' => true, 'isHidden' => false && !Yii::$app->params['isMoreWarehousesAvailable'], 'style' => 'width: 20%'],
        		['name' => 'userConfirmingName', 'label' => Yii::t('web', 'Confirmed by'), 'isSortable' => true, 'isHidden' => false && !Yii::$app->params['isMoreWarehousesAvailable'], 'style' => 'width: 20%'],
        		['name' => 'dateDelivery', 'label' => Yii::t('web', 'Delivery date'), 'isSortable' => true, 'isHidden' => false && !Yii::$app->params['isMoreWarehousesAvailable']],
        		['name' => 'positionCount', 'label' => Yii::t('web', 'Number of positions'), 'isSortable' => true, 'isHidden' => false && !Yii::$app->params['isMoreWarehousesAvailable']],
        		['name' => 'totalPrice', 'label' => Yii::t('web', 'Total price'), 'isSortable' => true, 'isHidden' => false && !Yii::$app->params['isMoreWarehousesAvailable']],
        		//['name' => 'documents', 'label' => Yii::t('web', 'Documents'), 'isSortable' => false, 'isHidden' => false && !Yii::$app->params['isMoreWarehousesAvailable']],
        ];
    }

    public function attributeLabels() {
        return [
        		'numberFilterString' => Yii::t('web', 'Number delivery'),
        		'supplierIds' => Yii::t('web', 'Suppliers'),
        		'warehouseIds' => Yii::t('web', 'Warehouses'),
        		'supplierName' => Yii::t('web', 'Write supplier name'),
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
				[['page', 'allWarehousesCount', 'allSuppliersCount'], 'integer'],
        		[['supplierIds', 'warehouseIds'], 'each', 'rule' => ['integer']],
				[['sortDir'], 'in', 'range' => ['asc', 'desc']],
				[['sortField'], 'in', 'range' => array_column(self::getSortFields(), 'name')],
				[['numberFilterString', 'warehouseIds', 'supplierIds', 'supplierName'], 'safe'],
	            ['page', 'default', 'value' => self::DEFAULT_PAGE],
	            ['sortDir', 'default', 'value' => self::DEFAULT_SORT_DIR],
	            ['sortField', 'default', 'value' => self::DEFAULT_SORT_FIELD],
        ];
    }
}
