<?php

namespace frontend\models;


use Yii;
use yii\base\Model;

class WorkstationListForm extends Model {
    
    public $page;
    public $name;
    public $sortDir;
    public $sortField;
    public $departmentId;
    
    const DEFAULT_PAGE = 0;
    const DEFAULT_SORT_DIR = 'asc';
    const DEFAULT_SORT_FIELD = 'name';
    
    public function init() {
        parent::init();
        $this->page = self::DEFAULT_PAGE;
        $this->sortDir = self::DEFAULT_SORT_DIR;
        $this->sortField = self::DEFAULT_SORT_FIELD;
    }
    
    public static function getSortFields() {
        return [
            ['name' => 'name', 'label' => Yii::t('web', 'Name'), 'isSortable' => true, 'style' => 'width: 40%'],
            ['name' => 'departmentName', 'label' => Yii::t('web', 'Department name'), 'isSortable' => true,'style' => 'width: 40%'],
        ];
    }
    
    public function attributeLabels() {
        return [
            'name' => Yii::t('web', 'Name'),
        ];
    }
    
    public function rules() {
        return [
            [['page'], 'integer'],
            [['sortDir'], 'in', 'range' => ['asc', 'desc']],
            [['sortField'], 'in', 'range' => array_column(self::getSortFields(), 'name')],
            [['departmentId'], 'safe'],
            [['name'], 'safe'],
            ['page', 'default', 'value' => self::DEFAULT_PAGE],
            ['sortDir', 'default', 'value' => self::DEFAULT_SORT_DIR],
            ['sortField', 'default', 'value' => self::DEFAULT_SORT_FIELD],
        ];
    }
}