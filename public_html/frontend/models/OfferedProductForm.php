<?php

namespace frontend\models;

use Yii;
use yii\base\Model;
use common\models\ar\OfferedProduct;

class OfferedProductForm extends Model {

    public $id;
    public $categoriesIds;
	public $isAvailable;
	public $symbol;
	public $height;
	public $width;
	public $shapeId;
	public $productionPathId;
	

    public function attributeLabels() {
        
        
        return [
            'categoriesIds' => \Yii::t('web', 'Categories'),
            'isAvailable' => \Yii::t('web', 'Product is available in offer'),
            'symbol' => \Yii::t('web', 'Code'),
            'width' => Yii::t('web', 'Width') . ' [mm]',
            'height' => Yii::t('web', 'Height') . ' [mm]',
            'shapeId' => Yii::t('web', 'Shape'),
            'productionPathId' => Yii::t('web', 'Production path'),
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules() {
        $ruleTable =  [
            [['categoriesIds'], 'safe'],
            [['id'], 'integer'],
            [['isAvailable'], 'in', 'range' => [0, 1]],
            [['width', 'height', 'shapeId', 'productionPathId'], 'integer'],
            [['width' , 'height'],  'isValidDimension'],
        ];       
        
        if(\Yii::$app->params['isProductSymbolEditable']) {
            $mergeTable = [
                [['symbol'], 'required'],
                [['symbol'], 'unique', 'targetClass' => '\common\models\ar\OfferedProduct', 'targetAttribute' => 'symbol', 'when' => function($model){
                    $offeredProduct = OfferedProduct::findOne(['symbol' => $this->symbol]);
                    if(!empty($offeredProduct)) {
                        if($offeredProduct->id == $model->id) {
                            return false;
                        }
                    }
                    return true;
                }],
            ];
            
            $ruleTable = array_merge($ruleTable, $mergeTable);
        }
        
        return $ruleTable;      
    }

    public function isValidDimension($attribute, $params) {
        if(!empty($this->width) && is_numeric($this->width)) {
            if($this->width > \Yii::$app->params['maxDimensionLong']) {
                $this->addError('width', \Yii::t('web', '{attribute} cannot be wider than ', ['attribute' => $this->getAttributeLabel('width')]).\Yii::$app->params['maxDimensionLong']) ;
            }
        }

        if(!empty($this->height) && is_numeric($this->height)) {
            if($this->height >  \Yii::$app->params['maxDimensionLong']){
                $this->addError('height', \Yii::t('web', '{attribute} cannot be higher than ', ['attribute' => $this->getAttributeLabel('height')]).\Yii::$app->params['maxDimensionLong']);
            }
        }

        if(!empty($this->height) && is_numeric($this->height) && !empty($this->width) && is_numeric($this->width)) {
            if($this->height > \Yii::$app->params['maxDimensionShort'] && $this->width > \Yii::$app->params['maxDimensionShort']){
                $this->addError('width', \Yii::t('web', 'Total size cannot exceed ', ['attribute' => $this->getAttributeLabel('width')]).\Yii::$app->params['maxDimensionLong'] . 'mm x '. \Yii::$app->params['maxDimensionShort']. 'mm.' );
            }
        }
    }

}
