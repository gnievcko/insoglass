<?php

namespace frontend\models;

use Yii;
use yii\base\Model;
use yii\helpers\Json;

class ProductAttributeForm extends Model {

    public $fields = [];
    public $isRowVisible;
    
    public function attributeLabels() {
        return [];
    }

    public function rules() {
        return [
            [['fields'], 'each', 'rule' => ['default', 'value' => null]],
            [['isRowVisible'], 'boolean'],
            [['isRowVisible'], 'default', 'value' => 'false'],
        ];
    }

    public function loadFromObjects($objects) {
        if(!empty($objects)) {
            foreach($objects as $object) {
                if(isset($object->product_attribute_type_id)) {
                    $this->fields[$object->product_attribute_type_id] = isset($object->value) ? $object->value : 1;
                }
            }
        }
    }
    
    public function convertFieldsToArrayObjects() {
        $result = [];
        
        if(!empty($this->fields)) {
            foreach($this->fields as $k => $v) {
                $value = intval(filter_var($v, FILTER_VALIDATE_INT));
                if($value > 0) {
                    $result[] = [
                        'product_attribute_type_id' => $k,
                        'value' => $value,
                    ];
                }
            }
        }
        
        return $result;
    }
    
    public function convertFieldsToJson() {
        $result = null;
        
        if(!empty($this->fields)) {
            foreach($this->fields as $k => $v) {
                $value = intval(filter_var($v, FILTER_VALIDATE_INT));
                if($value > 0) {
                    $result[] = [
                        'id' => $k,
                        'value' => $value,
                    ];
                }
            }
            
            $result = !empty($result) ? Json::encode($result) : null;
        }
        
        return $result;
    }
}
