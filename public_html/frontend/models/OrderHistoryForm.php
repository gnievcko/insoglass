<?php

namespace frontend\models;

use Yii;
use yii\base\Model;
use common\helpers\StringHelper;
use yii\helpers\ArrayHelper;
use common\models\ar\OrderStatusTranslation;
use common\models\ar\OrderStatus;
use common\helpers\Utility;

class OrderHistoryForm extends Model {
	
	public $orderHistoryId;
    public $orderStatusId;
    public $dateDeadline;
    public $dateReminder;
    public $message;
    public $shippingDate;
    
    public $oldOrderStatusId;

    public function attributeLabels() {
        return [
            'orderStatusId' => StringHelper::translateOrderToOffer('web', 'Order status'),
            'dateDeadline' => \Yii::t('web', 'Deadline'),
        	'dateReminder' => \Yii::t('main', 'Reminder date'),
        	'message' => \Yii::t('main', 'Note'),
            'shippingDate' => Yii::t('web', 'Shipping date')
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            [['orderStatusId'], 'required'],
            [['dateDeadline'], 'date', 'format' => 'php:Y-m-d', 'message' => Yii::t('web', 'Invalid date format')],
        	[['dateReminder'], 'date', 'format' => 'php:Y-m-d', 'message' => Yii::t('web', 'Invalid date format')],
            [['shippingDate'], 'date', 'format' => 'php:Y-m-d', 'message' => Yii::t('web', 'Invalid date format')],
        	[['dateReminder'], 'dontAllowPastDates'],
        	[['message', 'shippingDate', 'dateDeadline', 'orderHistoryId'], 'safe'],
            ['orderStatusId', 'isAllowStatus'],
            ['orderStatusId', 'testDate'],
        ];
    }
    
    public function dontAllowPastDates() {
    	if(!empty($this->dateReminder)) {
            if (date('Y-m-d') > $this->dateReminder){
                $this->addError('dateReminder', Yii::t('web', 'Reminder date cannot be a past date.'));
            }
        }
    }
    
    public function isAllowStatus() {
		$allStatuses = ArrayHelper::map(OrderStatusTranslation::find()->successorsOf($this->oldOrderStatusId)->all(), 'order_status_id', 'name');
        
        if(!empty($this->orderStatusId) && (!isset($allStatuses[$this->orderStatusId]) && $this->oldOrderStatusId != $this->orderStatusId)) {
            $this->addError('orderStatusId', Yii::t('web', 'This status is not available.'));
        }

    }
    
    public function testDate($attribute,$params) {
        if(empty($this->dateDeadline)) {
            $orderStatus = OrderStatus::findOne(['id' => $this->orderStatusId]);
            if(!empty($orderStatus) && $orderStatus->symbol == Utility::ORDER_STATUS_ACCEPTED_BY_CLIENT) {
               $this->addError('orderStatusId', Yii::t('web', 'With this status, date of deadline is required'));
            }   
        }
    }
}
