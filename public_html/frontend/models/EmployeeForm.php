<?php

namespace frontend\models;

use Yii;
use yii\base\Model;
use common\models\ar\User;
use yii\db\Query;
use common\models\ar\UserWarehouse;
use common\models\ar\UserRole;
use common\models\ar\UserCompanySalesman;

use yii\web\UploadedFile;
use yii\helpers\FileHelper;
use common\helpers\UploadHelper;
use common\components\UploadTrait;
use common\helpers\Utility;
use common\models\ar\Role;
use common\models\aq\WarehouseQuery;
use common\models\aq\RoleQuery;
use common\models\aq\UserWarehouseQuery;
use common\models\ar\UserModificationHistory;
use common\helpers\ChangeHelper;
use yii\helpers\Url;
use frontend\helpers\AddressHelper;
use common\models\aq\UserCompanySalesmanQuery;
use common\models\aq\UserQuery;
use frontend\helpers\ModelLoaderHelper;
use common\models\ar\UserAttachment;
use yii\helpers\ArrayHelper;
use common\helpers\StrengthValidator;


class EmployeeForm extends Model {

	use UploadTrait;

	const SCENARIO_ADD = 'add';
	const SCENARIO_EDIT = 'edit';

	public $id;
    public $firstName;
	public $lastName;
	public $email;
	public $phone1;
	public $phone2;
	public $phone3;
	public $note;
	public $urlPhoto;
	public $password;
	public $repeatPassword;
	public $filePhoto;
	
	public $address;
	public $city;
	public $zipCode;
	public $provinceId;
	
	public $pesel;
	public $idNumber;
	public $dateEmployment;
	public $position;

	public $roles;
	public $clientGroupIds;
	public $warehouseGroupIds;
	public $savedUnavailableRoles;
	public $savedPredefinedGroups;
	
	public $documentsForms = [];
	public $oldDocumentsForms = [];

	public function __construct($employee = null, $config = []) {
		if(!empty($employee)) {
			$this->loadData($employee);
		}

		parent::__construct($config);
	}

	public function scenarios() {
		$scenarios = parent::scenarios();
		$scenarios[self::SCENARIO_ADD] = ['firstName', 'lastName', 'email', 'phone1',
				'phone2', 'phone3', 'note', 'urlPhoto', 'filePhoto', 'password', 'repeatPassword',
				'address', 'city', 'zipCode', 'provinceId', 'pesel', 'idNumber', 'dateEmployment',
				'roles', 'clientGroupIds', 'warehouseGroupIds', 'savedUnavailableRoles', 'savedPredefinedGroups',
				'position',
		];
		$scenarios[self::SCENARIO_EDIT] = ['id', 'firstName', 'lastName', 'email', 'phone1',
				'phone2', 'phone3', 'note', 'urlPhoto', 'filePhoto', 'password', 'repeatPassword',
				'address', 'city', 'zipCode', 'provinceId', 'pesel', 'idNumber', 'dateEmployment',
				'roles', 'clientGroupIds', 'warehouseGroupIds', 'savedUnavailableRoles', 'savedPredefinedGroups',
				'position',
		];
		return $scenarios;
	}

	public function rules() {
		return [
				[['email'], 'required'],
				[['password', 'repeatPassword'], 'required', 'on' => self::SCENARIO_ADD],
				[['email', 'password', 'address', 'city'], 'string', 'max' => 64],
				[['email'], 'validateEmailUnique'],
				[['email'], 'email'],
				[['firstName', 'lastName', 'phone1', 'phone2', 'phone3'], 'string', 'max' => 32],
				[['urlPhoto', 'position'], 'string', 'max' => 256],
				[['note'], 'string', 'max' => 1024],
				[['phone2'], 'validatePhone1'],
				[['phone1', 'phone2', 'phone3'], 'validatePhoneUnique'],
				[['file_photo'], 'file', 'skipOnEmpty' => true, 'extensions' => 'png, jpg, jpeg, gif'],
				[['roles'], 'validateRoles'],
				[['provinceId', 'addressId'], 'integer'],
				[['zipCode'], 'string', 'max' => 10],
				[['address', 'city', 'zipCode', 'provinceId'], 'validateAddress'],
				[['position'], 'default', 'value' => null],
                [['password'], StrengthValidator::className()] + (isset(Yii::$app->params['strengthPasswordConfig']) ? Yii::$app->params['strengthPasswordConfig'] : []),
                ['repeatPassword', 'compare', 'compareAttribute' => 'password', 'message'=> Yii::t('main','Passwords must be identical.')],
        ];
	}

	public function validatePhone1() {
		if(empty($this->phone1) && !empty($this->phone2)) {
			$this->addError('phone1', Yii::t('web', 'Phone must be set if alternative phone has been provided.'));
		}
		else if(!empty($this->phone1) && !empty($this->phone2) && ($this->phone1 == $this->phone2)) {
			$this->addError('phone1', Yii::t('web', 'Phone number and alternate phone number cannot be identical.'));
		}
	}
	public function validatePhoneUnique($attribute) {
		$query = (new Query())
				->select(['id', $attribute])
				->from('user')
				->where(['or',
						['phone1' => $this->$attribute],
						['phone2' => $this->$attribute],
						['phone3' => $this->$attribute],
				]);

		if ($this->isEditScenarioSet()){
			$query->andWhere('id != :id', [':id' => $this->id]);
		}
	}

	public function validateEmailUnique() {
		$query = (new Query())->select(['id', 'email'])
    			->from('user')
    			->where('email = :email', [':email' => $this->email]);
		if($this->isEditScenarioSet()) {
    		$query->andWhere('id != :id', [':id' => $this->id]);
		}

		if($query->count() != 0) {
			$this->addError('email', Yii::t('web', 'E-mail must be unique.'));
		}
	}

	public function validateRoles($attribute) {
		if(!empty($this->roles)) {
			$availableRoles = RoleQuery::findAvailableRoles()->all();
			if(count(array_intersect($this->roles, array_column($availableRoles, 'id'))) < count($this->roles)) {
				$this->addError($attribute, Yii::t('web', 'Some of selected roles are not available for a user of your privileges.'));
			}
		}
	}
	
	public function validateAddress($attribute, $params) {
		$sum = !empty($this->address) + !empty($this->city) + !empty($this->zipCode) + !empty($this->provinceId);
		if($sum > 0 && $sum < 4) {
			$this->addError('address', Yii::t('web', 'Address is incomplete.'));
			$this->addError('city', '');
			$this->addError('zipCode', '');
			$this->addError('provinceId', '');
		}
	}
	
	public function validate($attributeNames = NULL, $clearErrors = true) {
		return parent::validate() && Model::validateMultiple($this->documentsForms);
	}

    public function attributeLabels() {
        return [
        		'photo' => Yii::t('web', 'Photo'),
            	'firstName' => Yii::t('main', 'First name'),
        		'lastName' => Yii::t('main', 'Last name'),
            	'roles' => Yii::t('main', 'Roles'),
        		'email' => Yii::t('main', 'E-mail'),
        		'phone1' => Yii::t('web', 'Phone number'),
        		'phone2' => Yii::t('main', 'Alternative phone'),
        		'phone3' => Yii::t('web', 'Private phone'),
        		'note' => Yii::t('main', 'Note'),
        		'password' => Yii::t('main', 'Password'),
        		'repeatPassword' => Yii::t('web', 'Repeat password'),
        		'clientGroupIds' => Yii::t('web', 'Client groups'),
        		'warehouseGroupIds' => Yii::t('web', 'Warehouses'),
        		'city' => Yii::t('main', 'City'),
        		'zipCode' => Yii::t('main', 'Zip code'),
        		'provinceId' => Yii::t('main', 'Voivodeship'),
        		'address' => Yii::t('main', 'Address'),
        		'pesel' => Yii::t('web','PESEL'),
        		'idNumber' => Yii::t('web','ID number'),
        		'dateEmployment' => Yii::t('web','Employment date'),
        		'position' => Yii::t('web', 'Position'),
        ];
    }

	public function saveData($action) {
		if(!empty($this->id)) {
			$employee = User::findOne($this->id);
		}
		if(empty($employee)) {
			$employee = new User();
		}

		$transaction = Yii::$app->db->beginTransaction();
		try {
			$this->saveBasicData($employee);
			$changeHelper = new ChangeHelper($employee, $action);
			$this->trackChangesToEmployee($changeHelper, $employee);
			$employee->save();
			$changeHelper->saveChanges($employee->id, new UserModificationHistory());
			$this->id = $employee->id;

			if (!$this->isEditScenarioSet()) {
				$this->id = (new Query())
						->select(['id'])
						->from('user')
						->where('email = :email', [':email' => $this->email])
						->one()['id'];
			}
			else {
				UserWarehouse::deleteAll(['AND', ['=', 'user_id', $this->id],['NOT IN', 'warehouse_id', $this->warehouseGroupIds]]);
				UserRole::deleteAll(['AND', ['=', 'user_id', $this->id], ['NOT IN', 'role_id', $this->roles]]);
				UserCompanySalesman::deleteAll(['AND', ['=', 'user_id', $this->id], ['NOT IN', 'company_group_id', $this->clientGroupIds]]);
			}

			$this->saveForeignKeyData();
			$this->saveDocuments();
			
			$transaction->commit();
		}
		catch(\Exception $e) {
			Yii::info('Bledy2: '.print_r($employee->getErrors(), true), 'info');
			$transaction->rollBack();
			Yii::error($e->getTraceAsString());
			throw new \Exception('Could not save employee');
		}
	}
	
	private function saveBasicData($employee) {
		$employee->first_name = $this->firstName;
		$employee->last_name = $this->lastName;
		$employee->email = $this->email;
		$employee->phone1 = $this->phone1;
		$employee->phone2 = $this->phone2;
		$employee->phone3 = $this->phone3;
		$employee->note = $this->note;
		$employee->pesel = $this->pesel;
		$employee->id_number = $this->idNumber;
		$employee->date_employment = $this->dateEmployment;
		$employee->position = $this->position;
		
		if(!empty($this->password)){
			$employee->setPassword($this->password);
		}
		$employee->is_active = 1;
		
		$this->loadModelFiles();
		
		$employee->url_photo = $this->urlPhoto;
			
		$employee->address_id = AddressHelper::getAddressId([
				'address' => $this->address, 'city' => $this->city,
				'zipCode' => $this->zipCode, 'provinceId' => $this->provinceId
		]);
		
	}
	
	private function saveForeignKeyData() {
		try {
			$this->saveUserWarehouse();
				
			if(!empty($this->roles)) {
				foreach($this->roles as $roleId) {
					$this->saveUserRole($roleId);
				}
			
				if(!empty($this->savedUnavailableRoles)) {
					foreach($this->savedUnavailableRoles as $roleId) {
						$this->saveUserRole($roleId);
					}
				}
			}
			
			if(!empty($this->clientGroupIds)){
				foreach($this->clientGroupIds as $clientGroupId){
					$this->saveUserCompanySalesman($clientGroupId);
				}
			}
				
			if(!empty($this->savedPredefinedGroups)) {
				foreach($this->savedPredefinedGroups as $groupId) {
					$this->saveUserCompanySalesman($groupId);
				}
			}
		}
		catch(\Exception $e) {
			Yii::error($e->getTraceAsString());
			throw new \Exception('Could not save foreign keys');
		}
	}
	
	private function trackChangesToEmployee($changeHelper, $employee) {
		$oldRoles = RoleQuery::getRolesByUserIdQuery($employee->id)->all();
		$changeHelper->addAttribute(
			'role_id',
			!empty($oldRoles) ? array_column($oldRoles, 'id') : [],
			!empty($this->roles) ? $this->roles : []
		);
		 
		$oldCompanyGroups = UserCompanySalesmanQuery::getGroupsByUserId($employee->id);
		$changeHelper->addAttribute(
			'company_group_id',
			!empty($oldCompanyGroups) ? array_column($oldCompanyGroups, 'id') : [],
			!empty($this->clientGroupIds) ? $this->clientGroupIds : []
		);
		
		if(Yii::$app->params['isMoreWarehousesAvailable']) {
			$oldWarehouses = UserWarehouseQuery::getWarehouseIdsByUserId($employee->id);
			$changeHelper->addAttribute(
				'warehouse_id',
				!empty($oldWarehouses) ? array_column($oldWarehouses, 'id') : [],
				!empty($this->warehouseGroupIds) ? $this->warehouseGroupIds : []
			);
		}
	}
	
	private function saveUserRole($roleId) {
		if(empty(UserRole::findOne(['role_id' => $roleId, 'user_id' => $this->id]))) {
			$userRole = new UserRole();
			$userRole->role_id = $roleId;
			$userRole->user_id = $this->id;
			$userRole->save();
		}
	}
	
	private function saveUserCompanySalesman($companyGroupId) {
		if(empty(UserCompanySalesman::findOne(['user_id' => $this->id, 'company_group_id' => $companyGroupId]))) {
			$userCompanySalesman = new UserCompanySalesman();
			$userCompanySalesman->user_id = $this->id;
			$userCompanySalesman->company_group_id = $companyGroupId;
			$userCompanySalesman->save();
		}	
	}
	
	private function saveUserWarehouse() {
		if(!empty($this->warehouseGroupIds)) {
			foreach($this->warehouseGroupIds as $warehouseId){
				if(empty(UserWarehouse::findOne(['warehouse_id' => $warehouseId, 'user_id' => $this->id]))) {
					$userWarehouse = new UserWarehouse();
					$userWarehouse->warehouse_id = $warehouseId;
					$userWarehouse->user_id = $this->id;
					$userWarehouse->save();
				}
			}
		}
		elseif(!empty($this->roles)) {
			$roleStorekeeper = Role::find()->where(['symbol' => Utility::ROLE_STOREKEEPER])->one();
			if(!Yii::$app->params['isMoreWarehousesAvailable'] && !empty($roleStorekeeper) &&
					in_array($roleStorekeeper->id, $this->roles)) {
				$warehouse = WarehouseQuery::getDefaultWarehouse()->one();
				$userWarehouse = new UserWarehouse();
				$userWarehouse->warehouse_id = $warehouse->id;
				$userWarehouse->user_id = $this->id;
				$userWarehouse->save();
			}
		}
	}
	
	private function saveDocuments() {
		$currentUserId = Yii::$app->user->identity->id;
		$filesToSave = [];
		$copies = [];
	
		foreach($this->documentsForms as $documentForm) {
			if($documentForm->makeCopy) {
				$copy = ['hash' => UploadHelper::copy($documentForm->document), 'name' => $documentForm->name];
				$copies[] = $copy;
				$filesToSave[] = $copy;
			}
			else {
				$filesToSave[] = ['hash' => $documentForm->document, 'name' => $documentForm->name, 'description' => $documentForm->description];
			}
		}
	
		Yii::$app->db->createCommand()->batchInsert('file_temporary_storage',
				['file_hash', 'original_file_name'],
				array_map(function($copy) {
					return ['file_hash' => $copy['hash'], 'original_file_name' => $copy['name']];
				}, $copies))->execute();
	
		Yii::$app->db->createCommand()->batchInsert('user_attachment',
				['user_attached_id', 'url_file', 'name', 'user_id', 'description'],
				array_map(function($fileToSave) use($currentUserId) {
					return [$this->id, Url::to(['storage/index', 'f' => $fileToSave['hash']], true), $fileToSave['name'], $currentUserId, $fileToSave['description']];
				}, $filesToSave))->execute();
	
		Yii::$app->db->createCommand()->delete('file_temporary_storage', ['in', 'file_hash', array_column($filesToSave, 'hash')])->execute();
	}
	
	private function removeFilesFromDisk($filesToRemove) {
		foreach($filesToRemove as $file) {
			UploadHelper::removeByUrl($file->url_file);
		}
	}
	
	public function load($data, $formName = NULL) {
        $result = parent::load($data);
        $this->documentsForms = ModelLoaderHelper::loadMultiple($data, DocumentForm::class);
        
        return $result;
    }
	
    public function loadData($employee) {
    	$this->id = $employee->id;
    	$this->firstName = $employee->first_name;
    	$this->lastName = $employee->last_name;
    	$this->email = $employee->email;
    	$this->phone1 = $employee->phone1;
    	$this->phone2 = $employee->phone2;
    	$this->phone3 = $employee->phone3;
    	$this->note = $employee->note;
    	$this->urlPhoto = $employee->url_photo;
    	$this->pesel = $employee->pesel;
    	$this->idNumber = $employee->id_number;
    	$this->dateEmployment = $employee->date_employment;
    	$this->position = $employee->position;

    	$address = $employee->address;
    	
    	if(!empty($address)) {
    		$this->address = $address->main.(!empty($address->complement) ? ' '.$address->complement : '');
    		$this->city = $address->city->name;
    		$this->zipCode = $address->city->zip_code;
    	
    		if(!empty($address->city->province)) {
    			$this->provinceId = $address->city->province->id;
    		}
    	}
    	
    	$this->roles = array_map(function($role) {
    		return $role->role_id;
    	}, UserRole::findAll($this->id));

    	$this->clientGroupIds = array_map(function($clientGroupId) {
    		return $clientGroupId->company_group_id;
    	}, UserCompanySalesman::findAll($this->id));

    	$this->warehouseGroupIds = array_map(function($warehouseGroupId) {
    		return $warehouseGroupId->warehouse_id;
    	}, UserWarehouse::findAll($this->id));

    	$this->savedUnavailableRoles = array_diff($this->roles, array_column(RoleQuery::findAvailableRoles()->all(), 'id'));
    	$this->savedPredefinedGroups = array_column(UserCompanySalesman::find()->joinWith('companyGroup cg')->where(['user_id' => $this->id, 'cg.is_predefined' => 1])->all(), 'company_group_id');
    	
    	$this->restoreDocumentsForms();
    }
    
    private function restoreDocumentsForms() {
    	$this->oldDocumentsForms = array_map(function($document) {
    		$documentForm = new DocumentForm();
    		$documentForm->id = $document['id'];
    		$documentForm->document = $document['urlFile'];//UploadHelper::getFileNameByUrl($document['urlFile']);
    		$documentForm->name = $document['name'];
    		$documentForm->makeCopy = true;
    
    		return $documentForm;
    	}, UserQuery::getAttachments($this->id));
    }

    private function loadModelFiles() {
    	$this->filePhoto = UploadedFile::getInstance($this, 'filePhoto');

    	$newPhoto = $this->loadNewPhoto();

    	$oldPhotoUrl = $this->urlPhoto;
    	$needToRemoveOldPhoto = ($newPhoto !== false) || Yii::$app->request->post('delete-photo');

    	if($newPhoto !== false) {
    		$this->urlPhoto = Url::to('@web/storage/index?f=' . basename($newPhoto));
    	}

    	if($needToRemoveOldPhoto) {
    		$this->removePhotoFromDisk($oldPhotoUrl);
    		$this->urlPhoto = ($newPhoto !== false) ? $this->urlPhoto : null;
    	}
    }

    private function loadNewPhoto() {
    	if(!empty($this->filePhoto) && ($filePhoto = $this->uploadFile('filePhoto')) !== false) {
    		return $filePhoto;
    	}
    	else {
    		return false;
    	}
    }

    private function removePhotoFromDisk($photoUrl) {
    	if(!empty($photoUrl)) {
    		$oldPhotoRelativePath = preg_replace('|^'.Url::to('@web/storage/index\?f='.'|'), '', $photoUrl);
    		$oldFile = FileHelper::normalizePath(UploadHelper::getUploadPath().'/'.$oldPhotoRelativePath);
    		@unlink($oldFile);
    	}
    }

    public function setAddScenario() {
    	$this->setScenario(self::SCENARIO_ADD);
    }

    public function setEditScenario() {
    	$this->setScenario(self::SCENARIO_EDIT);
    }

    public function isEditScenarioSet() {
    	return $this->getScenario() == self::SCENARIO_EDIT;
    }
    
    public function getDocumentsForms() {
    	return $this->documentsForms;
    }
    
    public function getReadonlyDocumentsForms() {
    	return $this->oldDocumentsForms;
    }
}
