<?php

namespace frontend\models;

use Yii;
use yii\base\Model;
use common\models\ar\Order;
use common\helpers\StringHelper;

class OrderCreateForm extends Model {

    public $orderId;
    public $parentOrderId;
    public $title;
    public $number;
    public $numberForClient;
    public $userResponsibleId;
    public $entityId;
    public $description;
    public $descriptionNote;
    public $descriptionCont;
    public $clientId;
    public $contactPeopleIds;
    public $isTemplate;
    public $shippingDate;
    public $isInvoiced;
    public $durationTime;
    public $executionTime;
    public $paymentTerms;
    public $orderTypeIds;
    public $contractTypeId;
    public $orderPriorityId;
    public $priorityCheckbox;

    public function attributeLabels() {
        return [
	            'title' => Yii::$app->params['isOrderTitleIsLabeledAsConcerns'] ? Yii::t('web', 'Concerns') : (Yii::$app->params['isOrderTitleLabeledAsOrderName'] ? Yii::t('web', 'Job name') : Yii::t('web', 'Title')),
	            'number' => \Yii::t('web', 'Number'),
	            'description' => \Yii::t('web', 'Description').' ('.mb_strtolower(\Yii::t('web', 'Displayed above the table with products'), 'UTF-8').')',
        		'descriptionNote' => \Yii::t('web', 'Note').' ('.mb_strtolower(\Yii::t('web', 'Displayed under the table with products as bold'), 'UTF-8').')',
        		'descriptionCont' => \Yii::t('web', 'Remarks').' ('.mb_strtolower(\Yii::t('web', 'Displayed under the table with products'), 'UTF-8').')',
	            'clientId' => Yii::t('web', 'Client'),
        		'parentOrderId' => StringHelper::translateOrderToOffer('web', 'Parent order'),
	        	'entityId' => Yii::t('web', 'Entity'),
	            'contactPeopleIds' => Yii::t('web', 'Contact people'),
	            'isTemplate' => StringHelper::translateOrderToOffer('web', 'Create a template based on this order'),
	            'shippingDate' => Yii::t('web', 'Shipping date'),
	            'isInvoiced' => Yii::t('web', 'Is invoiced'),
	        	'durationTime' => Yii::t('web', 'Duration time'),
	        	'executionTime' => Yii::t('web', 'Execution time'),
	        	'paymentTerms' => Yii::t('web', 'Terms of payment'),
        		'orderTypeIds' => StringHelper::translateOrderToOffer('web', 'Order types'),
                'contractTypeId' => StringHelper::translateOrderToOffer('documents', 'Type of contract'),
 				'userResponsibleId' => \Yii::t('main', 'User Responsible'),
                'priorityCheckbox' => Yii::t('web', 'High priority'),
                'numberForClient' => Yii::t('main', 'Number for client'),

        ];
    }

    public function init() {
        $this->durationTime = Yii::t('web', 'One month');
        $this->executionTime = Yii::t('web', '2-3 weeks');
        $this->paymentTerms = Yii::t('web', 'To be arranged');
    }

    /**
     * @inheritdoc
     */
    public function rules() {
        $rules = [
            [['title', 'clientId', 'number', 'entityId'], 'required'],
            [['title'], 'string', 'max' => 256],
            [['number'], 'string', 'max' => 48],
            [['descriptionCont', 'descriptionNote'], 'string'],
            [['durationTime', 'executionTime', 'paymentTerms'], 'string', 'max' => 64],
            [['contactPeopleIds', 'orderTypeIds'], 'safe'],
            [['shippingDate'], 'date', 'format' => 'php:Y-m-d', 'message' => Yii::t('web', 'Invalid date format')],
            //[['number'],  'hasUniqueAttribute', 'params' => ['targetAttribute' => 'number', 'value' => function() { return $this->number; }]],
            [['contractTypeId', 'parentOrderId', 'orderPriorityId', 'priorityCheckbox'], 'integer'],
            [['orderId', 'entityId'], 'safe'],
            [['shippingDate', 'isInvoiced', 'isTemplate'], 'safe'],
            [['description', 'descriptionCont'], 'default', 'value' => null],
        ];

        if(Yii::$app->params['isNumberForClientVisible']) {
            $rules[] = [['numberForClient'], 'required'];
            $rules[] = [['numberForClient'], 'string', 'max' => 48];
        }
        else {
        	$rules[] = [['numberForClient'], 'safe'];
        }
        
        if(Yii::$app->params['isUserResponsibleVisible']) {
            $rules[] = [['userResponsibleId'], 'integer'];
        }
        else {
        	$rules[] = [['userResponsibleId'], 'safe'];
        }
        
        return $rules;
    }

    public function hasUniqueAttribute($attribute, $params) {
        $order = Order::find()->where(['=', $params['targetAttribute'], $params['value']()]);
        if(!empty($this->orderId)) {
            $order->andWhere(['!=', 'id', $this->orderId]);
        }

        if($order->exists()) {
            $this->addError($attribute, Yii::t('web', 'Order with such {attribute} already exists', ['attribute' => $this->getAttributeLabel($attribute)]));
        }
    }

}
