<?php

namespace frontend\models;

use Yii;
use yii\base\Model;
use yii\helpers\Json;
use common\models\ar\Currency;
use common\helpers\Utility;
use common\helpers\StringHelper;

class OrderTableForm extends Model {

	const EMPTY_DATA = [['theads' => [], 'rows' => [[]]]];
	
    public $columnCount;
    //public $tables = [['theads' => ['', '', '', '', ''], 'rows' => [[]]]];
    public $tables = self::EMPTY_DATA;
    public $isVisible;

    public function attributeLabels() {
        return [
	            'columnCount' => Yii::t('web', 'Number of columns in a next table')
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules() {
    	$rules = [
	            [['isVisible'], 'required'],
	            [['columnCount'], 'integer', 'max' => 7],
        		[['tables'], 'safe'],
        		[['isVisible'], 'boolean'],
        ];
    	
    	if(Yii::$app->params['isOrderCustomDataVisible']) {
    		$rules[] = ['columnCount', 'required']; 
    	}
    	
        return $rules;
    }

    public function loadData($data) {
    	if(isset($data[$this->formName()])) {
    		$formData = $data[$this->formName()];
    		$this->isVisible = $formData['isVisible'];
    		$this->columnCount = !empty($formData) ? $formData['columnCount'] : 0;
    	}
    	
    	if(isset($data['tables'])) {
    		$this->tables = $data['tables'];
    		
    		if(!isset($data[$this->formName()])) {
    			if(count($data['tables']) > 0 && isset(reset($data['tables'])['rows']) 
    					&& count(reset($data['tables'])['rows']) > 0) {
    				$this->isVisible = 1;
    				$this->columnCount = count(reset($data['tables'])['rows'][0]);
    			}
    		}
    	}
    }
    
    public static function isCustomDataEmpty($tables) {
    	return empty($tables) || Json::encode($tables) == '[{"theads":[],"rows":[[]]}]';
    }
    
    public static function isCustomDataEqual($oldContent, $newContent) {
    	$oldTable = reset($oldContent);
    	$newTable = reset($newContent);
    	
    	$oldRows = isset($oldTable['rows']) ? array_values($oldTable['rows']) : [];
    	$newRows = isset($newTable['rows']) ? array_values($newTable['rows']) : [];
    	 
    	return $oldTable['theads'] == $newTable['theads'] && $oldRows == $newRows;
    }
    
    public static function flattenRows($tables) {
    	$output = [];
    	foreach($tables as $key => $array) {
    		if(!isset($array['rows'])) {
    			return [];
    		}
    		
    		$array['rows'] = array_values($array['rows']);
    		$i = 1;
    		foreach($array['rows'] as &$row) {
    			$row[0] = $i;
    			$i++;
    		}
    		
    		$output[$key] = $array;
    	}
    	
    	return $output;
    }
    
    public static function calculateTotalPrice($data) {
    	if(empty($data)) {
    		return [];
    	}
    	
    	$plnCurrency = Currency::find()->where(['symbol' => Utility::CURRENCY_PLN])->one();
    	$table = reset($data['tables']);
    	$key = key($data['tables']);
    	
    	$result = [];
    	$sum = 0;
    	foreach($table['rows'] as $k => $row) {
    		$columnCount = count($row);
    		$price = str_replace(',', '.', $row[$columnCount-3]);
    		$count = $row[$columnCount-2];
    		
    		$name = 'tables['.$key.'][rows]['.$k.']['.($columnCount - 1).']';
    		$totalPrice = floatval($price) * intval($count);
    		$result[$name] = StringHelper::getFormattedCost($totalPrice).' '.$plnCurrency->short_symbol;
    		$sum += $totalPrice;
    	}
    	$result['tables['.$key.'][summary]'] = StringHelper::getFormattedCost($sum).' '.$plnCurrency->short_symbol;
    	
    	return $result;
    }
    
    public static function numerateRows($data) {
    	$table = reset($data['tables']);
    	$key = key($data['tables']);
    	 
    	$result = [];
    	$index = 1;
    	foreach($table['rows'] as $k => $row) {
    		$name = 'tables['.$key.'][rows]['.$k.'][0]';
    		$result[$name] = $index++;
    	}
    	
    	return $result;
    }
    
    public static function calculateTotalPriceStatic($data, $needsReset = true) {
    	$table = !empty($needsReset) ? reset($data) : $data;
    	
    	$plnCurrency = Currency::find()->where(['symbol' => Utility::CURRENCY_PLN])->one();
    	$sum = 0;
    	foreach($table['rows'] as $row) { 
    		$columnCount = count($row);
    		$price = str_replace(',', '.', $row[$columnCount-3]);
    		$count = $row[$columnCount-2];
    		
    		$totalPrice = floatval($price) * intval($count);
    		$sum += $totalPrice;
    	}
    	
    	return StringHelper::getFormattedCost($sum).' '.$plnCurrency->short_symbol;
    }
    
    public static function getCostsTable($data) {
    	$table = reset($data);
    	
    	$result = [];
    	foreach($table['rows'] as $row) {
    		$columnCount = count($row);
    
    		$result[] = [
    				'name' => $row[1],
    				'count' => $row[$columnCount-2],
    				'price' => $row[$columnCount-3],
    				'unit' => '',
    				'vatRate' => 0.23,
    		];
    	}
    	
    	return $result; 
    }
}
