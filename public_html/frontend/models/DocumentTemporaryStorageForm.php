<?php

namespace frontend\models;

use Yii;
use common\helpers\UploadHelper;

class DocumentTemporaryStorageForm extends FileTemporaryStorageForm {

	public $file;

    public static $ALLOWED_EXTENSIONS = ['xls', 'pdf', 'txt', 'doc', 'xlsx', 'docx', 'ppt', 'pptx', 'odt', 'ods', 'odp'];
	
    public function rules() {
        //CHECK jezeli problem z odrzucaniem rozszerzen, to lezy on w tym ze yii zle rozpoznaje type po przez mime type, mozna to wylaczyc http://www.yiiframework.com/doc-2.0/yii-validators-filevalidator.html#$checkExtensionByMimeType-detail
        return [
            [
                ['file'],
                'file',
                'skipOnEmpty' => false,
                'extensions' => self::$ALLOWED_EXTENSIONS,
                'maxSize' => \frontend\helpers\AdditionalFieldHelper::getMaxFileSize(),
                'tooBig' => Yii::t('main', 'File is too large'),
                'wrongExtension' => Yii::t('main','Only {extensions} types are allowed')
            ],
        ];
    }

    public function upload() {
        if ($this->validate()) {
            $documentPath = UploadHelper::saveTemporaryFile($this->file);

            $originalDocumentName = "{$this->file->getBaseName()}.{$this->file->getExtension()}";
            $documentDetails = UploadHelper::storeInformationAboutUploadedFile($documentPath['name'], $originalDocumentName);

            return $documentDetails;
        } else {
            return null;
        }
    }
}
