<?php

namespace frontend\models;

use Yii;
use yii\base\Model;

class AlertListForm extends Model {
	
    public $page;
    public $sortDir;
    public $sortField;
    public $alertTypeId;

    const DEFAULT_PAGE = 0;
    const DEFAULT_SORT_DIR = 'asc';
    const DEFAULT_SORT_FIELD = 'date_creation';

    public function init() {
        parent::init();
        $this->page = self::DEFAULT_PAGE;
        $this->sortDir = self::DEFAULT_SORT_DIR;
        $this->sortField = self::DEFAULT_SORT_FIELD;
    }

    public static function getSortFields() { 
        return [
	            ['name' => 'name', 'label' => Yii::t('main', 'Name'), 'isSortable' => false],
        ];
    }

    public function attributeLabels() {
        return [
        		'alertTypeId' => Yii::t('main', 'Alert type'),
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
				[['page', 'alertTypeId'], 'integer'],
				[['sortDir'], 'in', 'range' => ['asc', 'desc']],
				[['sortField'], 'in', 'range' => array_column(self::getSortFields(), 'name')],
	            ['page', 'default', 'value' => self::DEFAULT_PAGE],
	            ['sortDir', 'default', 'value' => self::DEFAULT_SORT_DIR],
	            ['sortField', 'default', 'value' => self::DEFAULT_SORT_FIELD],
        ];
    }
}
