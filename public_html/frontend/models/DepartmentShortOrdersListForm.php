<?php
namespace frontend\models;

use Yii;
use common\helpers\StringHelper;


class DepartmentShortOrdersListForm extends DepartmentOrdersListForm
{
    public static function getSortFields() {
        return [
            ['name' => 'customerName', 'label' => Yii::t('web', 'Customer name'), 'isSortable' => true],
            ['name' => 'orderNumber', 'label' => StringHelper::translateOrderToOffer('web', 'Order number'), 'isSortable' => true],
            ['name' => 'progress', 'label' => Yii::t('web', 'Progress'), 'isSortable' => false],
            ['name' => 'deadline', 'label' => Yii::t('web', 'Deadline'), 'isSortable' => true],
        ];

    }
}