<?php

namespace frontend\models;

use Yii;
use yii\base\Model;

class WarehouseProductDetailsForm extends Model {

	public $page;
	public $sortDir;
	public $sortField;
	public $warehouseIds;

	public $allWarehousesCount;

	const DEFAULT_PAGE = 0;
	const DEFAULT_SORT_DIR = 'desc';
	const DEFAULT_SORT_FIELD = 'dateCreation';

	public function init() {
		parent::init();
		$this->page = self::DEFAULT_PAGE;
		$this->sortDir = self::DEFAULT_SORT_DIR;
		$this->sortField = self::DEFAULT_SORT_FIELD;
	}

	public static function getSortFields() {
		return [
				['name' => 'warehouse', 'label' => Yii::t('main', 'Warehouse'), 'isSortable' => true, 'isHidden' => !Yii::$app->params['isMoreWarehousesAvailable']],
				['name' => 'version', 'label' => Yii::t('web', 'Version'), 'isSortable' => true],
				['name' => 'status', 'label' => Yii::t('web', 'Operation type'), 'isSortable' => true],
				['name' => 'count', 'label' => Yii::t('web', 'Count'), 'isSortable' => true, 'style' => 'width: 8%'],
				['name' => 'description', 'label' => Yii::t('main', 'Description'), 'isSortable' => true],
				['name' => 'user', 'label' => Yii::t('web', 'Author'), 'isSortable' => true, 'style' => 'width: 10%'],
				['name' => 'additionalData', 'label' => Yii::t('web', 'Additional data'), 'isSortable' => false],
				['name' => 'dateCreation', 'label' => Yii::t('main', 'Execution date'), 'isSortable' => true, 'style' => 'width: 10%'],
				['name' => 'price', 'label' => Yii::t('web', 'Price'), 'isSortable' => false],
				['name' => 'actions', 'label' => Yii::t('web', 'Actions'), 'isSortable' => false],
		];
	}

	public function attributeLabels() {
		return [
				'warehouseIds' => Yii::t('main', 'Warehouses'),
		];
	}

	/**
	 * @inheritdoc
	 */
	public function rules() {
		return [
				[['page', 'allWarehousesCount'], 'integer'],
				[['warehouseIds'], 'each', 'rule' => ['integer']],
				[['sortDir'], 'in', 'range' => ['asc', 'desc']],
				[['sortField'], 'in', 'range' => array_column(self::getSortFields(), 'name')],
				['page', 'default', 'value' => self::DEFAULT_PAGE],
				['sortDir', 'default', 'value' => self::DEFAULT_SORT_DIR],
				['sortField', 'default', 'value' => self::DEFAULT_SORT_FIELD],
		];
	}
}
