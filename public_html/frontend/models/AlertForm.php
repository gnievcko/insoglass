<?php

namespace frontend\models;

use yii\base\Model;
use common\components\OrderReminderTrait;

class AlertForm extends Model {

	use OrderReminderTrait;
}
