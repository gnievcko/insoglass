<?php

namespace frontend\models;

use Yii;
use yii\base\Model;
use common\components\validators\RequiredIfFormEmptyValidator;

class ProductVariantForm extends Model {

    public $variantId;
    public $name;
    public $index;
    public $description;

    public function attributeLabels() {
        return [
            'name' => \Yii::t('web', 'Name'),
        	'index' => \Yii::t('main', 'Warehouse index'),
            'description' => \Yii::t('web', 'Description'),
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            [['name'], RequiredIfFormEmptyValidator::className()],
            [['description'], 'string', 'max' => 2048],
        	[['index'], 'string', 'max' => 48],
            [['variantId'], 'safe'],
        	[['index'], 'default', 'value' => null],
        ];
    }
}
