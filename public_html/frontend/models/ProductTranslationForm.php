<?php

namespace frontend\models;

use Yii;
use yii\base\Model;

class ProductTranslationForm extends Model {

    public $name;
    public $description;
    public $remarks;
    public $price;
    public $unit;
    public $currencyId;
    public $languageId;

    public function attributeLabels() {
        return [
            	'name' => \Yii::t('web', 'Product name'),
            	'description' => \Yii::t('web', 'Description'),
        		'remarks' => \Yii::t('main', 'Remarks'),
            	'price' => \Yii::t('web', 'Price'),
            	'currencyId' => \Yii::t('main', 'Currency'),
        		'unit' => \Yii::t('main', 'Unit'),
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            	[['name', 'languageId'], 'required'],
            	[['name'], 'string', 'max' => 2048],
            	[['description'], 'string', 'max' => 2048],
        		[['remarks'], 'string', 'max' => 1024],
        		[['unit'], 'string', 'max' => 10],
            	[['price'], 'number'],
            	[['currencyId'], 'integer'],
            	[['languageId'], 'integer'],
        		[['description', 'remarks', 'unit'], 'default', 'value' => null],
        ];
    }
}
