<?php

namespace frontend\models;

use Yii;
use yii\base\Model;
use yii\helpers\Url;
use frontend\helpers\ModelLoaderHelper;
use common\helpers\UploadHelper;

use common\models\ar\Product;
use common\models\ar\ProductTranslation;
use common\models\ar\Language;
use common\models\ar\ProductPhoto;
use common\models\ar\ProductAlias;

use common\helpers\RandomSymbolGenerator;
use common\models\ar\WarehouseProduct;
use common\models\ar\WarehouseProductStatusHistory;
use common\models\ar\ProductStatus;
use common\helpers\Utility;
use common\models\aq\WarehouseQuery;
use common\alerts\ProductMinimalCountAlertHandler;
use common\models\aq\ProductQuery;
use common\models\ar\ProductAttachment;
use yii\helpers\ArrayHelper;
use common\models\ar\WarehouseProductOperation;

class ProductModel {

    private $product;
    private $languageId;

    private $productForm;
    private $productTranslationsForms = [];
    private $productVariantsForms = [];
    private $productAliasesForms = [];
    private $photosForms = [];
    private $documentsForms = [];
    private $oldDocumentsForms = [];

    public function __construct($productId = null) {
        $this->languageId = Language::find()->where(['symbol' => \Yii::$app->language])->one()->id;
        if($productId === null) {
            $this->initializeEmptyForms();
        }
        else {
            $this->restoreStateFromDatabase($productId);
        }
    }

    private function restoreStateFromDatabase($productId) {
        $this->product = Product::findOne($productId);
        $this->restoreProductForm();
        $this->restoreTranslationsForms();
        $this->restoreVariantsForms();
        $this->restoreAliasesForms();
        $this->restorePhotosForms();
        $this->restoreDocumentsForms();
    }
    
    private function restoreProductForm() {
        $this->productForm = new ProductForm();
        $this->productForm->categoriesIds = array_map(function($categorySet) {
            return $categorySet->product_category_id;
        }, $this->product->productCategorySets);
        $this->productForm->isAvailable = $this->product->is_available;
        $this->productForm->warehouseSupplierId = $this->product->warehouse_supplier_id;
        $this->productForm->isVisibleForClient = $this->product->is_visible_for_client;
        $this->productForm->countMinimal = $this->product->count_minimal;
        $this->productForm->index = $this->product->index;
        
        $warehouseProduct = $this->getWarehouseProduct();
        $this->productForm->stillage = $warehouseProduct->stillage;
        $this->productForm->shelf = $warehouseProduct->shelf;
    }
    
    private function getWarehouseProduct() {
        return $this->product->warehouseProducts[0];
    }

    private function restoreTranslationsForms() {
    	$this->productTranslationsForms = array_map(function($translation) {
            $translationForm = new ProductTranslationForm();
            $translationForm->name = $translation->name;
            $translationForm->description = $translation->description;
            $translationForm->remarks = $translation->remarks;
            $translationForm->languageId = $translation->language_id;
            $translationForm->unit = $translation->unit_default;
            
            if(Yii::$app->params['isProductLastDeliveryCostIsDefaultCost']) {
            	$warehouseProduct = $this->getWarehouseProduct();
            	$translationForm->price = $warehouseProduct->price_unit;
            	$translationForm->currencyId = $warehouseProduct->currency_id;
            }
            else {
            	$translationForm->price = $translation->price_default;
            	$translationForm->currencyId = $translation->currency_id;
            }

            return $translationForm;
        }, $this->product->productTranslations);
    }

    private function restoreVariantsForms() {
        $this->productVariantsForms = array_map(function($variant) {
            $variantForm = new ProductVariantForm();
            $variantForm->name = $variant->name;
            $variantForm->index = $variant->product->index;
            $variantForm->description = $variant->description;
            $variantForm->variantId = $variant->product_id;

            return $variantForm;
        }, ProductTranslation::find()->where([
            'product_id' => (new \yii\db\Query())->select('id')->from('product')->where(['parent_product_id' => $this->product->id]),
            'language_id' => $this->languageId,
        ])->all());
    }

    private function restoreAliasesForms() {
        $this->productAliasesForms = array_map(function($alias) {
            $aliasForm = new ProductAliasForm(); 
            $aliasForm->name = $alias->name;

            return $aliasForm;
        }, ProductAlias::find()->where([
            'product_id' => $this->product->id,
            'language_id' => $this->languageId,
        ])->all());
    }

    private function restorePhotosForms() {
        $this->photosForms = array_map(function($photo) {
            $photoForm = new PhotoForm();

            $photoForm->image = UploadHelper::getFileNameByUrl($photo->url_photo);
            $photoForm->thumbnail = UploadHelper::getFileNameByUrl($photo->url_thumbnail);
            $photoForm->description = $photo->description;

            return $photoForm;
        }, $this->product->productPhotos);
    }
    
    private function restoreDocumentsForms() {
    	$this->oldDocumentsForms = array_map(function($document) {
            $documentForm = new DocumentForm();
            $documentForm->id = $document['id'];
            $documentForm->document = $document['urlFile'];
            $documentForm->name = $document['name'];
            $documentForm->makeCopy = true;
            return $documentForm;
        }, ProductQuery::getAttachments($this->product->id));
    }

    private function initializeEmptyForms() {
        $this->productForm = new ProductForm();

        $translationForm = new ProductTranslationForm();
        $translationForm->languageId = $this->languageId;
        $this->productTranslationsForms = [$translationForm]; 

        $this->productVariantsForms = [];
        $this->productAliasesForms = [];
        $this->photosForms = [];   
        $this->documentsForms = [];
    }

    public function load($data) {
        $this->productForm = ModelLoaderHelper::loadOne($data, ProductForm::class);
        $this->productTranslationsForms = ModelLoaderHelper::loadMultiple($data, ProductTranslationForm::class);
        $this->productVariantsForms = ModelLoaderHelper::loadMultiple($data, ProductVariantForm::class);
        $this->productAliasesForms = ModelLoaderHelper::loadMultiple($data, ProductAliasForm::class);
        $this->photosForms = ModelLoaderHelper::loadMultiple($data, PhotoForm::class);
        $this->documentsForms = ModelLoaderHelper::loadMultiple($data, DocumentForm::class);
    }

    public function save() {
        if($this->validate()) {
            $this->persist();
            return true;
        }
        else {
            return false; 
        }
    }

    private function validate() {
        return $this->productForm->validate() 
	            && Model::validateMultiple($this->productTranslationsForms)
	            && Model::validateMultiple($this->productVariantsForms)
	            && Model::validateMultiple($this->productAliasesForms)
	            && Model::validateMultiple($this->photosForms)
	        	&& Model::validateMultiple($this->documentsForms);
    }
        
    private function persist() {
        $transaction = \Yii::$app->db->beginTransaction();

        try {
            if($this->product === null) {
                $this->product = $this->saveProduct();
            }
            else {
                $this->updateProduct();
            }
            $this->saveProductTranslations();
            $this->assignToCategories();
            $this->saveVariants();
            $this->saveAliases();
            $this->savePhotos();
            $this->saveDocuments();

            $transaction->commit();
        }
        catch(\Exception $e) {
            $transaction->rollback();
            throw $e;
        }
    }

    private function updateProduct() {
    	$warehouseProduct = $this->getWarehouseProduct();
        $warehouseProduct->stillage = $this->productForm->stillage;
        $warehouseProduct->shelf = $this->productForm->shelf;
        if(Yii::$app->params['isProductLastDeliveryCostIsDefaultCost']) {
        	$warehouseProduct->price_unit = $this->productTranslationsForms[0]->price;
        	$warehouseProduct->currency_id = $this->productTranslationsForms[0]->currencyId;
        	$warehouseProduct->price_group = null;
        }
        $warehouseProduct->save();
        
        $this->product->is_available = $this->productForm->isAvailable;
        $this->product->warehouse_supplier_id = $this->productForm->warehouseSupplierId;
        $this->product->index = $this->productForm->index;
        $this->product->count_minimal = $this->productForm->countMinimal;
        $this->product->is_visible_for_client = $this->productForm->isVisibleForClient;
        
        if(!$this->product->save()) {
            throw new \Exception('Could not update product');
        }
        
        if(!empty($this->product->count_minimal) && (empty($warehouseProduct->count) || $warehouseProduct->count < $this->product->count_minimal)) {
        	ProductMinimalCountAlertHandler::add(['productId' => $this->product->id, 'userId' => \Yii::$app->user->id]);
        }
        ProductMinimalCountAlertHandler::check(['productId' => $this->product->id]);
    }

    private function saveProduct() {
        $product = new Product();
        $product->symbol = RandomSymbolGenerator::generate(Product::class);
        $product->is_available = $this->productForm->isAvailable;
        $product->is_active = 1;
        $product->index = $this->productForm->index;
        $product->count_minimal = $this->productForm->countMinimal;
        $product->is_visible_for_client = $this->productForm->isVisibleForClient;
        $product->warehouse_supplier_id = $this->productForm->warehouseSupplierId;
        $product->user_id = \Yii::$app->user->identity->id;
        if(!$product->save()) {
            throw new \Exception('Could not save product');
        }

        if(!\Yii::$app->params['isMoreWarehousesAvailable'] && empty($this->productForm->warehouseId)) {
        	$this->productForm->warehouseId = WarehouseQuery::getDefaultWarehouse()->one()->id;
        }
        
        $warehouseProduct = new WarehouseProduct();
        $warehouseProduct->warehouse_id = $this->productForm->warehouseId;
        $warehouseProduct->product_id = $product->id;
        $warehouseProduct->count = $this->productForm->initialCount ?: 0;
        $warehouseProduct->count_available = $this->productForm->initialCount ?: 0;
        $warehouseProduct->stillage = $this->productForm->stillage;
        $warehouseProduct->shelf = $this->productForm->shelf;
        if(Yii::$app->params['isProductLastDeliveryCostIsDefaultCost']) {
        	$warehouseProduct->price_unit = $this->productTranslationsForms[0]->price;
        	$warehouseProduct->currency_id = $this->productTranslationsForms[0]->currencyId;
        }
        $warehouseProduct->save();
            
	    if(!empty($this->productForm->initialCount)) {
	    	$productStatusId = ProductStatus::find()->where('symbol = :symbol', [':symbol' => Utility::PRODUCT_STATUS_ADDITION])->one()->id;
	    	
	    	$operation = new WarehouseProductOperation();
			$operation->warehouse_id = $this->productForm->warehouseId;
			$operation->product_status_id = $productStatusId;
			$operation->user_confirming_id = \Yii::$app->user->id;
			$operation->description = \Yii::t('web', 'Initial count');
			$operation->save();
	    	
	        $history = new WarehouseProductStatusHistory();
	        $history->operation_id = $operation->id;
	        $history->warehouse_product_id = $warehouseProduct->id;
	        $history->user_confirming_id = \Yii::$app->user->id;
	        $history->count = $warehouseProduct->count;
	        $history->description = \Yii::t('web', 'Initial count');
			$history->product_status_id = $productStatusId;
	        $history->save();
        }
        
        if(!empty($product->count_minimal) && (empty($warehouseProduct->count) || $warehouseProduct->count < $product->count_minimal)) {
        	ProductMinimalCountAlertHandler::add(['productId' => $product->id, 'userId' => \Yii::$app->user->id]);
        }
        
        return $product;
    }

    private function saveProductTranslations() {
        \Yii::$app->db->createCommand()->delete('product_translation', ['product_id' => $this->product->id])->execute();

        \Yii::$app->db->createCommand()->batchInsert('product_translation', 
            ['product_id', 'language_id', 'name', 'description', 'remarks', 'price_default', 'unit_default', 'currency_id'], array_map(function($translationForm) {
                return [$this->product->id, $translationForm->languageId, $translationForm->name, $translationForm->description, 
                        $translationForm->remarks, $translationForm->price, 
                		$translationForm->unit, $translationForm->currencyId]; 
            }, $this->productTranslationsForms))->execute();
    }

    private function assignToCategories() {
        \Yii::$app->db->createCommand()->delete('product_category_set', ['product_id' => $this->product->id])->execute();

        \Yii::$app->db->createCommand()->batchInsert('product_category_set', 
            ['product_id', 'product_category_id'], array_map(function($categoryId) {
                return [$this->product->id, $categoryId]; 
            }, empty($this->productForm->categoriesIds) ? [] : $this->productForm->categoriesIds))->execute();
    }

    private function saveVariants() {
        $variantsIdsToPersist = [];
        $variantsTranslationsToInsert = [];
        
        foreach(ModelLoaderHelper::skipEmpty($this->productVariantsForms) as $variantForm) {
            $variantIdToPersist = $variantForm->variantId;
            if(empty($variantIdToPersist)) {
                $newVariant = $this->createNewVariant($variantForm);
                $variantIdToPersist = $newVariant->id;
            }
            else {
            	// gdy będzie więcej możliwych aktualizacji w krotce z wersją, trzeba to zrobić inaczej
            	Product::updateAll(['index' => $variantForm->index], ['id' => $variantIdToPersist]);
            }

            $variantsTranslationsToInsert[] = [$variantIdToPersist, $this->languageId, $variantForm->name, $variantForm->description];
            $variantsIdsToPersist[] = $variantIdToPersist;
        }

        \Yii::$app->db->createCommand()->delete('product_translation', ['in', 'product_id', (new \yii\db\Query())
            ->select('id')
            ->from('product')
            ->andWhere(['parent_product_id' => $this->product->id])
        ])
        ->execute();

        \Yii::$app->db->createCommand()->delete('product', ['and', 
            ['=', 'parent_product_id', $this->product->id],
            ['not in', 'id', $variantsIdsToPersist],
        ])->execute();

        \Yii::$app->db->createCommand()->batchInsert('product_translation', ['product_id', 'language_id', 'name', 'description'], $variantsTranslationsToInsert)->execute();
    }

    private function createNewVariant($variantForm) {
        $variant = new Product();
        $variant->symbol = RandomSymbolGenerator::generate(Product::class);
        $variant->index = $variantForm->index;
        $variant->is_available = $this->product->is_available;
        $variant->is_active = $this->product->is_active;
        $variant->is_visible_for_client = $this->product->is_visible_for_client;
        $variant->user_id = $this->product->user_id;
        $variant->parent_product_id = $this->product->id;
        if(!$variant->save()) {
            throw new \Exception('Could not save product variant');
        }
        
        $warehouseProduct = new WarehouseProduct();
        $warehouseProduct->warehouse_id = $this->productForm->warehouseId;
        $warehouseProduct->product_id = $variant->id;
        $warehouseProduct->count = 0;
        $warehouseProduct->count_available = 0;
        $warehouseProduct->save();

        return $variant;
    }

    private function saveAliases() {
        \Yii::$app->db->createCommand()->delete('product_alias', [
            'product_id' => $this->product->id,
            'language_id' => $this->languageId,
        ])->execute();

        \Yii::$app->db->createCommand()->batchInsert('product_alias', ['product_id', 'language_id', 'name'], array_map(function($aliasForm) {
            return [$this->product->id, $this->languageId, $aliasForm->name];
        }, ModelLoaderHelper::skipEmpty($this->productAliasesForms)))->execute();
    }

    private function savePhotos() {
        $photosToSave = array_map(function($photoForm) {
            return Url::to(['storage/index', 'f' => $photoForm->image], true);
        }, $this->photosForms);

        \Yii::$app->db->createCommand()->delete('product_photo', ['product_id' => $this->product->id])->execute();
        \Yii::$app->db->createCommand()
            ->batchInsert('product_photo', ['product_id', 'url_photo', 'url_thumbnail', 'description'], array_map(function($photoForm) {
            return [$this->product->id, Url::to(['storage/index', 'f' => $photoForm->image], true), Url::to(['storage/index', 'f' => $photoForm->thumbnail], true), $photoForm->description] ;
        }, $this->photosForms))->execute();

        $photosToRemove = ProductPhoto::find()->andWhere(['product_id' => $this->product->id])
            ->andWhere(['not in', 'url_photo', $photosToSave])->all();

        $this->removePhotosFromDisk($photosToRemove);
    }
    
    private function saveDocuments() {
    	$currentUserId = \Yii::$app->user->identity->id;
    	$filesToSave = [];
    	$copies = [];
    
    	foreach($this->documentsForms as $documentForm) {
    		if($documentForm->makeCopy) {
    			$copy = ['hash' => UploadHelper::copy($documentForm->document), 'name' => $documentForm->name];
    			$copies[] = $copy;
    			$filesToSave[] = $copy;
    		}
    		else {
    			$filesToSave[] = ['hash' => $documentForm->document, 'name' => $documentForm->name, 'description' => $documentForm->description];
    		}
    	}
    
    	\Yii::$app->db->createCommand()->batchInsert('file_temporary_storage',
    			['file_hash', 'original_file_name'],
    			array_map(function($copy) {
    				return ['file_hash' => $copy['hash'], 'original_file_name' => $copy['name']];
    			}, $copies))->execute();
    
    	\Yii::$app->db->createCommand()->batchInsert('product_attachment',
    			['product_id', 'url_file', 'name', 'user_id', 'description'],
    			array_map(function($fileToSave) use($currentUserId) {
    		return [$this->product->id, Url::to(['storage/index', 'f' => $fileToSave['hash']], true), $fileToSave['name'], $currentUserId, $fileToSave['description']];    
    	}, $filesToSave))->execute();
    
		\Yii::$app->db->createCommand()->delete('file_temporary_storage', ['in', 'file_hash', array_column($filesToSave, 'hash')])->execute();
		
	/* TODO:: redundant
	 * 	$filesToRemove = ProductAttachment::find()->andWhere(['product_id' => $this->product->id])
				->andWhere(['not in', 'url_file', array_map(function($file) {
					return Url::to(['storage/index', 'f' => $file['hash']], true);
				}, $filesToSave)])->all();
				
		if(!empty($filesToRemove)) {
			ProductAttachment::deleteAll(['id' => array_keys(ArrayHelper::map($filesToRemove, 'id', 'url_file'))]);
			$this->removeFilesFromDisk($filesToRemove);
		}
	*/
    }

    private function removePhotosFromDisk($photosToRemove) {
        foreach($photosToRemove as $photo) {
            UploadHelper::removeByUrl($photo->url_photo);
            UploadHelper::removeByUrl($photo->url_thumbnail);
        }
    }
    
    private function removeFilesFromDisk($filesToRemove) {
    	foreach($filesToRemove as $file) {
    		UploadHelper::removeByUrl($file->url_file);
    	}
    }

    public function getInvalidForms() {
        $invalidForms = [];
        if($this->productForm->hasErrors()) {
            $invalidForms[] = $this->productForm;
        }

        return $invalidForms 
            + ModelLoaderHelper::getErrorForms($this->productTranslationsForms)
            + ModelLoaderHelper::getErrorForms($this->productVariantsForms)
            + ModelLoaderHelper::getErrorForms($this->productAliasesForms)
            + ModelLoaderHelper::getErrorForms($this->photosForms)
        	+ ModelLoaderHelper::getErrorForms($this->documentsForms);
    }

    public function getAllForms() {
        return [
            	'productForm' => $this->productForm,
            	'productTranslationsForms' => $this->productTranslationsForms,
            	'productVariantsForms' => $this->productVariantsForms,
            	'productAliasesForms' => $this->productAliasesForms,
            	'photosForms' => $this->photosForms,
        		'documentsForms' => $this->documentsForms,
        		'oldDocumentsForms' => $this->oldDocumentsForms,
        ];
    }

    public function getProductForm() {
        return $this->productForm;
    }

    public function getProductTranslationsForms() {
        return $this->productTranslationsForms;
    }

    public function getProductVariantsForms() {
        return $this->productVariantsForms;
    }

    public function getProductAliasesForms() {
        return $this->productAliasesForms;
    }

    public function getPhotosForms() {
        return $this->photosForms;
    }
    
    public function getDocumentsForms() {
    	return $this->documentsForms;
    }

    public function update() {
        if($this->validate()) {
            $this->persist();

            return true;
        }
        else {
            return false;
        }
    }

    public function getProduct() {
        return $this->product;
    }
    
    public function getReadonlyDocumentsForms() {
    	return $this->oldDocumentsForms;
    }
}
