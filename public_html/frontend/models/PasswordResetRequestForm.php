<?php
namespace frontend\models;

use common\components\MessageProvider;
use common\helpers\Utility;
use common\models\ar\TokenType;
use common\models\ar\Token;
use common\models\ar\User;
use yii\base\Model;
use Yii;
use yii\validators\EmailValidator;
/**
 * Password reset request form
 */
class PasswordResetRequestForm extends Model {
	
    public $identity;
    public $verifyCode;

    private $_isEmail = false;
    
    /**
     * @inheritdoc
     */
    public function rules() {
        return [
	            ['identity', 'filter', 'filter' => 'trim'],
	            ['identity', 'required'],
	            ['identity', 'validateIdentity'],
	            ['verifyCode', 'captcha'],
        ];
    }
    
    /**
     * @inheritdoc
     */
    public function attributeLabels() {
    	return [
    			'identity' => Yii::t('web', 'Email'),
            	'verifyCode' => Yii::t('web', 'Verification code')
    	];
    }
    
    public function validateIdentity($attribute, $params) {
    	if(!$this->hasErrors()) {
    		$this->_isEmail = false;
    		
    		$validator = new EmailValidator();
    		$error = 'Not valid';
    		if($validator->validate($this->identity, $error)) {
    			if(!User::findByEmail($this->identity)) {
    				$this->addError($attribute, Yii::t('main', 'There is no user with such email.'));
    				return false;
    			}
    			$this->_isEmail = true;
    			
    			return true;
    		}    
    
    		$this->addError($attribute, Yii::t('web', 'Incorrect e-mail.'));
    	}
    }
    
    /**
    * @return \yii\db\ActiveQuery
    */
    public function getTokens() {
    	return $this->hasMany(Token::className(), ['user_id' => 'id']);
    }
    
    /**
     * Sends an email with a link, for resetting the password.
     *
     * @return boolean whether the email was send
     */
    public function sendToken() {
    	
    	$user = null;
    	$sent = false;
    	$mode = MessageProvider::MODE_EMAIL;
    	if($this->_isEmail) {
    		$user = User::findByEmail($this->identity);
    	}
        if($user) {
        	$token = $user->getResetPasswordToken();
        	$isValid = User::isPasswordResetTokenValid($token, $sent, MessageProvider::MODE_EMAIL);
        	
            if($isValid === false) {
            	$sent = false;
                $token = $user->generateToken(Utility::TOKEN_TYPE_RESET_PASSWORD);
                
                if($token) {
                	$sent = $token->regenerate($mode, true);
                }
            }
            
            if($isValid === true) {
            	$sent = $token->regenerate($mode, true);
            }
        }

        return $sent;
    }
    
    public function isEmailMode() {
    	return $this->_isEmail;
    }
}
