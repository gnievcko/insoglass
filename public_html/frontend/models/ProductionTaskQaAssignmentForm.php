<?php

namespace frontend\models;

use Yii;
use yii\base\Model;
use common\models\ar\Order;
use common\models\aq\OrderPriorityQuery;
use common\helpers\Utility;
use common\models\aq\QaFactorTypeQuery;
use common\models\ar\ProductionTaskQaFactor;
use common\helpers\ArrayHelper;
use common\models\ar\User;
use frontend\helpers\UserHelper;
use common\models\ar\ProductionTask;
use yii\helpers\Json;

class ProductionTaskQaAssignmentForm extends Model {

    public $productionTaskId;
    public $fields = [];
    
    private $qaFactorTypes = [];

    public function __construct($productionTaskId, $config = []) {
        $this->qaFactorTypes = QaFactorTypeQuery::getAllVisible();
        
        $this->productionTaskId = $productionTaskId;
        parent::__construct($config);
    }
    
	public function rules() {
		$rules = [
			[['fields'], 'each', 'rule' => ['default', 'value' => null]],
		    [['fields'], 'validateFields'],
		    [['productionTaskId'], 'required'],
		    [['productionTaskId'], 'integer'],
		];
		
		return $rules;
	}
	
	public function validateFields() {
	    foreach($this->qaFactorTypes as $qaFactorType) {
	        if(!empty($qaFactorType['isRequired']) && empty($this->fields[$qaFactorType['id']])) {
                $this->addError('fields['.$qaFactorType['id'].']', Yii::t('web', 'This field has to be filled.'));
	        }
	    }
	}

    public function attributeLabels() {
        return [];
    }

	public function saveData() {
		$transaction = Yii::$app->db->beginTransaction();
		try {		    
		    $qaFactorTypesAssoc = ArrayHelper::transformToAssociativeArray($this->qaFactorTypes, 'id');
		    $user = User::findOne(Yii::$app->user->id);
		    
		    $dateCreation = (new \DateTime())->format('Y-m-d H:i:s');
		    $userId = $user->id;
		    $userName = UserHelper::getPrettyUserName($user->email, $user->first_name, $user->last_name);
		    $fields = [];
		    
		    foreach($this->fields as $k => $v) {
		        if(!is_null($v)) {
		            $ptqf = new ProductionTaskQaFactor();
		    
		            $ptqf->production_task_id = $this->productionTaskId;
		            $ptqf->qa_factor_type_id = $k;
		            $ptqf->user_id = Yii::$app->user->id;
		            $ptqf->value = $v;
		            
		            $ptqf->save();
		            
		            $fields[] = [
		                'symbol' => $qaFactorTypesAssoc[$k]['symbol'],
                        'name' => $qaFactorTypesAssoc[$k]['name'],
		                'variableType' => $qaFactorTypesAssoc[$k]['variableType'],
		                'value' => $v,
		            ];
		        }
		    }
		    
		    $productionTask = ProductionTask::findOne($this->productionTaskId);
		    $productionTask->qa_factor_value = Json::encode([
                'dateCreation' => $dateCreation,
		        'userId' => $userId,
		        'userName' => $userName,
		        'fields' => $fields,
		    ]);
		    $productionTask->save();
		    
			$transaction->commit();
		}
		catch(\Exception $e) {
			$transaction->rollBack();
			Yii::error($e->getMessage());
			Yii::error($e->getTraceAsString());
			throw new \Exception('Could not save quality factors to production task '.$this->productionTaskId);
		}
	}
	
	public function getQaFactorTypes() {
	    return $this->qaFactorTypes;
	}
}
