<?php

namespace frontend\models;

use Yii;
use yii\base\Model;
use yii\db\Query;
use yii\web\UploadedFile;
use yii\helpers\FileHelper;
use common\helpers\UploadHelper;
use common\components\UploadTrait;
use yii\helpers\Url;
use frontend\helpers\AddressHelper;
use common\helpers\StrengthValidator;

class UserAccountForm extends Model{
	
	use UploadTrait;
	
	const SCENARIO_SHOW = 'show';
	const SCENARIO_EDIT = 'edit';
	
	public $id;
	public $email;
	public $firstName;
	public $lastName;
	public $phone1;
	public $phone2;
	public $phone3;
	public $urlPhoto;
	public $languageId;
	public $currencyId;
	public $password;
	public $currentPassword;
	public $newPassword;
	public $newPasswordRepeated;
	public $filePhoto;
	public $user;
	public $deletePhoto;
	
	public $address;
	public $city;
	public $zipCode;
	public $provinceId;
	
	public $pesel;
	public $idNumber;
	public $dateEmployment;
	public $position;
	
	public function __construct($user = null, $config = []) {
		if(!empty($user)) {
			$this->loadData($user);
		}
	
		parent::__construct($config);
	}
	
	public function rules() {
		return [
				[['email'], 'required'],
				[['email'], 'email'],
				[['email'], 'validateEmailUnique'],
				[['id', 'languageId','currency_id'], 'integer'],
				[['email', 'password', 'address', 'city'], 'string', 'max' => 64],
				[['firstName', 'lastName', 'phone1', 'phone2', 'phone3'], 'string', 'max' => 32],
				[['urlPhoto', 'position'], 'string', 'max' => 256],
				[['filePhoto'], 'file', 'skipOnEmpty' => true, 'extensions' => 'png, jpg, jpeg, gif'],
				[['currentPassword','newPassword','newPasswordRepeated'], 'validatePassword'],
				[['provinceId'], 'integer'],
				[['zipCode'], 'string', 'max' => 10],
				[['address', 'city', 'zipCode', 'provinceId'], 'validateAddress'],
				[['position'], 'default', 'value' => null],
                [['newPassword'], StrengthValidator::className()]+Yii::$app->params['strengthPasswordConfig']
		];
	}
	
	public function validateAddress($attribute, $params) {
		$sum = !empty($this->address) + !empty($this->city) + !empty($this->zipCode) + !empty($this->provinceId);
		if($sum > 0 && $sum < 4) {
			$this->addError('address', Yii::t('web', 'Address is incomplete.'));
			$this->addError('city', '');
			$this->addError('zipCode', '');
			$this->addError('provinceId', '');
		}
	}
	
	public function attributeLabels() {
		return [
				'id' => Yii::t('main', 'ID'),
				'email' => Yii::t('main', 'E-mail address'),
				'password' => Yii::t('main', 'Password'),
				'firstName' => Yii::t('main', 'First name'),
				'lastName' => Yii::t('main', 'Last name'),
				'phone1' => Yii::t('main', 'Phone'),
				'phone2' => Yii::t('main', 'Alternative phone'),
				'phone3' => Yii::t('web', 'Work phone'),
				'urlPhoto' => Yii::t('main', 'Photo'),
				'note' => Yii::t('main', 'Note'),
				'languageId' => Yii::t('main', 'Language ID'),
				'currencyId' => Yii::t('main', 'Currency ID'),
				'date_creation' => Yii::t('main', 'Create date'),
				'date_modification' => Yii::t('main', 'Modification date'),
				'fullName' => Yii::t('main', 'Full name'),
				'filePhoto' => Yii::t('main', 'Photo'),
				'newPassword' => Yii::t('web', 'New password'),
				'newPasswordRepeated' => Yii::t('web', 'Repeat new password'),
				'currentPassword' => Yii::t('web', 'Current password'),
				'city' => Yii::t('main', 'City'),
				'zipCode' => Yii::t('main', 'Zip code'),
				'provinceId' => Yii::t('main', 'Voivodeship'),
				'address' => Yii::t('main', 'Address'),
				'pesel' => Yii::t('web','PESEL'),
				'idNumber' => Yii::t('web','ID number'),
				'dateEmployment' => Yii::t('web','Employment date'),
				'position' => Yii::t('web', 'Position'),
		];
	}
	
	public function loadData($user) {
		$this->id = $user->id;
		$this->email = $user->email;
		$this->firstName = $user->first_name;
		$this->lastName = $user->last_name;
		$this->phone1 = $user->phone1;
		$this->phone2 = $user->phone2;
		$this->phone3 = $user->phone3;
		$this->urlPhoto = $user->url_photo;
		$this->languageId = $user->language_id;
		$this->password = $user->password;
		$this->currencyId = $user->currency_id;
		$this->user = $user;
		
		$this->pesel = $user->pesel;
		$this->idNumber = $user->id_number;
		$this->dateEmployment = $user->date_employment;
		$this->position = $user->position;
		
		$address = $user->address;
		 
		if(!empty($address)) {
			$this->address = $address->main.(!empty($address->complement) ? ' '.$address->complement : '');
			$this->city = $address->city->name;
			$this->zipCode = $address->city->zip_code;
			 
			if(!empty($address->city->province)) {
				$this->provinceId = $address->city->province->id;
			}
		}
		
	}
	
	public function saveData() {
		$transaction = Yii::$app->db->beginTransaction();
		try {
			$this->user->first_name = $this->firstName;
			$this->user->last_name = $this->lastName;
			$this->user->email = $this->email;
			$this->user->phone1 = $this->phone1;
			$this->user->phone2 = $this->phone2;
			$this->user->phone3 = $this->phone3;
			$this->user->language_id = $this->languageId;
			$this->user->currency_id = $this->currencyId;
			
			$this->user->pesel = $this->pesel;
			$this->user->id_number = $this->idNumber;
			$this->user->date_employment = $this->dateEmployment;
			$this->user->position = $this->position;
			
			$this->user->address_id = AddressHelper::getAddressId([
					'address' => $this->address, 'city' => $this->city,
					'zipCode' => $this->zipCode, 'provinceId' => $this->provinceId
			]);
			
			if(!empty($this->newPassword)){
				$this->user->setPassword($this->newPassword);
			}
				
			$this->loadModelFiles();
				
			$this->user->url_photo = $this->urlPhoto;
			$this->user->save();
			
			$transaction->commit();
		}
		catch(\Exception $e) {
			$transaction->rollBack();
			throw $e;
			//throw new \Exception('Could not save user data');
		}
	}
	
	public function scenarios() {
		$scenarios = parent::scenarios();
		$scenarios[self::SCENARIO_SHOW] = ['name', 'email', 'phone1', 'phone2', 'phone3', 'firstName', 'lastName', 'languageId', 'currencyId', 'photoUrl',
				'address', 'city', 'zipCode', 'provinceId', 'pesel', 'idNumber', 'dateEmployment', 'position',
		];
		$scenarios[self::SCENARIO_EDIT] = ['id', 'name', 'email', 'phone1', 'phone3', 'phone2', 'firstName', 'lastName', 'languageId', 'currencyId', 
				'address', 'city', 'zipCode', 'provinceId', 'pesel', 'idNumber', 'dateEmployment', 'position',
				'photoUrl', 'newPassword', 'newPasswordRepeated', 'currentPassword', 'deletePhoto'
				
		];
		return $scenarios;
	}
	
	public function setShowScenario() {
		$this->setScenario(self::SCENARIO_SHOW);
	}
	
	public function setEditScenario() {
		$this->setScenario(self::SCENARIO_EDIT);
	}
	
	public function isShowScenarioSet() {
		return $this->getScenario() == self::SCENARIO_SHOW;
	}
	
	public function isEditScenarioSet() {
		return $this->getScenario() == self::SCENARIO_EDIT;
	}
	
	public function validatePassword($attribute, $params) {
      if(!empty($this->currentPassword) || !empty($this->newPassword) || !empty($this->newPasswordRepeated)) {
      		if(!empty($this->currentPassword)) {
		        if(!$this->user || !$this->user->validatePassword($this->currentPassword)) {
		        	$this->addError('currentPassword', Yii::t('web', 'Incorrect current password'));
		        }
      		}
      		else {
      			$this->addError('currentPassword', Yii::t('web', 'Incorrect current password'));
      		}
	        if(empty($this->newPassword)) {
	        	$this->addError('newPassword', Yii::t('web', 'New password is empty'));
	        }
	        if($this->newPassword !== $this->newPasswordRepeated) {
	       		$this->addError('newPasswordRepeated', Yii::t('web', 'New password must be repeated correctly'));
	        }

      }
    }
    
    public function validateEmailUnique(){
    	$query = (new Query())
	    	->select(['id', 'email'])
	    	->from('user')
	    	->where('email = :email', [':email' => $this->email]);
    	
    	if ($this->isEditScenarioSet()) {
    		$query->andWhere('id != :id', [':id' => $this->id]);
    	}
    		
    	if ($query->count() != 0) {
    		$this->addError('email', Yii::t('web', 'E-mail must be unique.'));
    	}
    }
    private function loadModelFiles() {
    	$this->filePhoto = UploadedFile::getInstance($this, 'filePhoto');
    
    	$newPhoto = $this->loadNewPhoto();
    
    	$oldPhotoUrl = $this->urlPhoto;
    	$needToRemoveOldPhoto = ($newPhoto !== false) || ($this->deletePhoto == 1);
    	if($newPhoto !== false) {
    		$this->urlPhoto = Url::to('@web/storage/index?f=' . basename($newPhoto));
    	}
    
    	if($needToRemoveOldPhoto) {
    		$this->removePhotoFromDisk($oldPhotoUrl);
    		$this->urlPhoto = ($newPhoto !== false) ? $this->urlPhoto : null;
    	}
    }
    
    private function loadNewPhoto() {
    	if(!empty($this->filePhoto) && ($filePhoto = $this->uploadFile('filePhoto')) !== false) {
    		return $filePhoto;
    	}
    	else {
    		return false;
    	}
    }
    
    private function removePhotoFromDisk($photoUrl) {
    	if(!empty($photoUrl)) {
    		$oldPhotoRelativePath = preg_replace('|^'.Url::to('@web/storage/index\?f='.'|'), '', $photoUrl);
    		$oldFile = FileHelper::normalizePath(UploadHelper::getUploadPath().'/'.$oldPhotoRelativePath);
    		@unlink($oldFile);
    	}
    }
	
}