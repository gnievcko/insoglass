<?php

namespace frontend\models;

use Yii;
use yii\base\Model;
use frontend\helpers\ModelLoaderHelper;
use common\models\ar\WarehouseDelivery;
use common\models\ar\WarehouseDeliveryProduct;
use common\models\ar\WarehouseProduct;
use common\alerts\ProductMinimalCountAlertHandler;
use common\models\ar\ProductStatus;
use common\helpers\Utility;
use common\models\ar\WarehouseProductStatusHistory;
use common\helpers\NumberTemplateHelper;
use common\models\ar\WarehouseProductOperation;
use common\models\aq\WarehouseProductOperationQuery;

class WarehouseDeliveryModel {

    private $delivery = null;

    private $warehouseDeliveryForm;
    private $productsForms = [];
    
    private $warehouseDeliveryId = null;

    public function __construct($deliveryId = null, $restoreState = true) {
        if($deliveryId === null) {
            $this->initializeEmptyForms();
        }
        else {
            $this->delivery = WarehouseDelivery::findOne($deliveryId);
            if(empty($this->delivery)) {
                throw new \Exception('Delivery not exists');
            }

            if($restoreState) {
                $this->restoreStateFromDatabase();
            }
        }
    }

    private function initializeEmptyForms() {
        $this->warehouseDeliveryForm = new WarehouseDeliveryForm();
        
        $this->warehouseDeliveryForm->number = NumberTemplateHelper::getNextNumber(Utility::NUMBER_TEMPLATE_GOODS_RECEIVED);
    }

    private function restoreStateFromDatabase() {
        $this->restoreDeliveryForm();
        $this->restoreProductsForms();
    }

    private function restoreDeliveryForm() {
        $this->warehouseDeliveryForm = new WarehouseDeliveryForm();
        $this->warehouseDeliveryForm->warehouseId = $this->delivery->warehouse_id;
        $this->warehouseDeliveryForm->warehouseSupplierId = $this->delivery->warehouse_supplier_id;
        $this->warehouseDeliveryForm->number = $this->delivery->number;
        $this->warehouseDeliveryForm->userConfirmingId = $this->delivery->user_confirming_id;
        $this->warehouseDeliveryForm->dateDelivery = $this->delivery->date_delivery;
        $this->warehouseDeliveryForm->description = $this->delivery->description;
        $this->warehouseDeliveryForm->operationId = $this->delivery->operation_id;
    }

    public function restoreProductsFromIssueForms($id) {
        $productsQuery = WarehouseProductOperationQuery::getIssueProductsQuery($id, ['sortDir' => 'asc', 'sortField' => 'name'])->all();

        $this->productsForms = array_map(function($product) {
            $productAr = \common\models\ar\Product::findOne($product['id']);
            $productForm = new WarehouseDeliveryProductForm();
            $productTranslation = $productAr->getTranslation();

            $productForm->name = empty($productTranslation) ? $productAr->symbol : $productTranslation->name;

            $photos = $productAr->productPhotos;
            $productForm->thumbnail = empty($photos) ? '@web/images/placeholders/product.png' : $photos[0]->url_thumbnail;
            $productForm->productId = $product['id'];
            $productForm->count = $product['count'];
            $productForm->price = 0;
            $productForm->priceType = WarehouseDeliveryProductForm::PRICE_TYPE_PER_UNIT;
            $productForm->unit = !empty($product['unit']) ? $product['unit'] : mb_strtolower(Yii::t('web', 'Unit'), 'UTF-8');
            $productForm->note = '';
        
            return $productForm;
        }, $productsQuery);
    }
    
    private function restoreProductsForms() {
        $deliveryProducts = WarehouseDeliveryProduct::find()
            ->with('product.productTranslations')
            ->with('product.productPhotos')
            ->where(['warehouse_delivery_id' => $this->delivery->id])
            ->all();
        
        $this->productsForms = array_map(function($deliveryProduct) {
            $productForm = new WarehouseDeliveryProductForm();
            $productTranslation = $deliveryProduct->product->getTranslation();

            $productForm->name = empty($productTranslation) ? $deliveryProduct->product->symbol : $productTranslation->name;

            $photos = $deliveryProduct->product->productPhotos;
            $productForm->thumbnail = empty($photos) ? '@web/images/placeholders/product.png' : $photos[0]->url_thumbnail;
            $productForm->productId = $deliveryProduct->product_id;
            $productForm->count = $deliveryProduct->count;
            $productForm->price = $deliveryProduct->price_unit ?: $deliveryProduct->price_group;
            $productForm->priceType = ($deliveryProduct->price_unit === null) 
                ? WarehouseDeliveryProductForm::PRICE_TYPE_PER_PACKAGE 
                : WarehouseDeliveryProductForm::PRICE_TYPE_PER_UNIT;
            $productForm->note = $deliveryProduct->note;
            $productForm->unit = $deliveryProduct->unit;
        
            $this->warehouseDeliveryForm->currencyId = $deliveryProduct->currency_id;

            return $productForm;
        }, $deliveryProducts);
    }

    public function load($data) {
        $this->warehouseDeliveryForm = ModelLoaderHelper::loadOne($data, WarehouseDeliveryForm::class);
        $this->productsForms = ModelLoaderHelper::loadMultiple($data, WarehouseDeliveryProductForm::class);
    }

    public function save() {
        if($this->validate()) {
            $this->persist();
            return true;
        }
        else {
            return false; 
        }
    }

    private function validate() {
        return $this->warehouseDeliveryForm->validate() && Model::validateMultiple($this->productsForms);
    }
    private function persist() {
        $transaction = Yii::$app->db->beginTransaction();
        try {
            if($this->delivery === null) {
                $this->delivery = new WarehouseDelivery();
            }
            $this->saveDeliveryBasicData();
            $this->saveDeliveryProducts();
            $transaction->commit();
        }
        catch(\Exception $e) {
            $transaction->rollBack();
            throw $e;
        }
    }

    private function saveDeliveryBasicData() {
    	// TODO: zabezpieczyc na wypadek edycji
    	if(empty($this->delivery->id)) {
    		$numberResult = NumberTemplateHelper::getAndSaveNextNumber($this->warehouseDeliveryForm->number, Utility::NUMBER_TEMPLATE_GOODS_RECEIVED);
    		$this->delivery->number = $numberResult['number'];
    	}
    	
        $this->delivery->warehouse_id = $this->warehouseDeliveryForm->warehouseId;
        $this->delivery->warehouse_supplier_id = $this->warehouseDeliveryForm->warehouseSupplierId;
        $this->delivery->user_confirming_id = $this->warehouseDeliveryForm->userConfirmingId;
        $this->delivery->is_artificial = 0;
        $this->delivery->date_delivery = $this->warehouseDeliveryForm->dateDelivery;
        $this->delivery->description = $this->warehouseDeliveryForm->description;
        $this->delivery->is_confirmed = $this->warehouseDeliveryForm->isConfirmed;
        $this->delivery->operation_id = $this->warehouseDeliveryForm->operationId;

        if(!$this->delivery->save()) {
            throw new \Exception('Could not save delivery, errors: '.print_r($this->delivery->getErrors(), true));
        }
        
        if(empty($this->delivery) && !empty($numberResult['isDifferent'])) {
        	Yii::$app->getSession()->setFlash('info', Yii::t('web', 'Number of the delivery would be {number} because of concurrent operations.', ['number' => $numberResult['number']]));
        }
    }

    private function saveDeliveryProducts() {
        $this->updateOrderedProductsCountByDelivery('-');
        $this->addMissingProductsToWarehouse();
        Yii::$app->db->createCommand()->delete('warehouse_delivery_product', ['warehouse_delivery_id' => $this->delivery->id])->execute();

        Yii::$app->db->createCommand()->batchInsert('warehouse_delivery_product', 
            ['warehouse_delivery_id', 'product_id', 'count', 'price_unit', 'price_group', 'currency_id', 'unit', 'note'],
            array_map(function($productForm) {
                if($productForm->priceType === WarehouseDeliveryProductForm::PRICE_TYPE_PER_UNIT) {
                    $pricePerUnit = $productForm->price;
                    $pricePerPackage = null;
                }
                else {
                    $pricePerUnit = null;
                    $pricePerPackage = $productForm->price;
                }

                return [
                    	$this->delivery->id, 
                    	$productForm->productId, 
                    	$productForm->count, 
                    	$pricePerUnit, 
                    	$pricePerPackage, 
                    	$this->warehouseDeliveryForm->currencyId,
                		$productForm->unit,
                    	$productForm->note,
                ];
            }, $this->productsForms)
        )->execute();
            
        $this->updateOrderedProductsCountByDelivery('+');

        if($this->delivery->is_confirmed) {
        	$this->updateWarehouseProductStateByDelivery();
        }
        
        $operation = new WarehouseProductOperation();
        $operation->warehouse_id = $this->warehouseDeliveryForm->warehouseId;
        $operation->product_status_id = ProductStatus::find()->where('symbol = :symbol', [':symbol' => Utility::PRODUCT_STATUS_ADDITION])->one()->id;
        $operation->user_confirming_id = Yii::$app->user->id;
        $operation->description = Yii::t('web', 'Added as a part of delivery').': '.$this->warehouseDeliveryForm->number;
        $operation->save();
        
        foreach($this->productsForms as $productForm) {
        	ProductMinimalCountAlertHandler::check(['productId' => $productForm->productId]);
        	if($this->delivery->is_confirmed) {
        		$this->addHistoryEntry($productForm, $operation->id);
        	}
        }
    }

    private function addMissingProductsToWarehouse() {
        $productsIds = array_map(function($productForm) {
            return $productForm->productId;  
        }, $this->productsForms);

        $warehouseProducts = WarehouseProduct::find()->where([
            'warehouse_id' => $this->warehouseDeliveryForm->warehouseId, 
            'product_id' => $productsIds,
        ])->all();
        $warehouseProductsIds = array_map(function($product) {
            return $product->id; 
        }, $warehouseProducts);

        if(count($warehouseProducts) < count($productsIds)) {
            Yii::$app->db->createCommand()->batchInsert('warehouse_product', ['warehouse_id', 'product_id', 'count', 'count_available'], array_map(function($productId) {
                return [$this->warehouseDeliveryForm->warehouseId, $productId, 0, 0];
            }, array_diff($productsIds, $warehouseProducts)))
            ->execute();
        }
    }
    
    private function addHistoryEntry($productForm, $operationId) {
    	$warehouseProduct = WarehouseProduct::find()->where([
    			'product_id' => $productForm->productId, 'warehouse_id' => $this->warehouseDeliveryForm->warehouseId
    	])->one();
    	$status = ProductStatus::find()->where('symbol = :symbol', [':symbol' => Utility::PRODUCT_STATUS_ADDITION])->one();
    	
    	if(!empty($warehouseProduct) && !empty($status)) {
    		$history = new WarehouseProductStatusHistory();
	    	$history->operation_id = $operationId;
	    	$history->warehouse_product_id = $warehouseProduct->id;
	    	$history->user_confirming_id = Yii::$app->user->id;
	    	$history->count = $productForm->count;
	    	$history->unit = $productForm->unit;
	    	$history->price_unit = $productForm->priceType === WarehouseDeliveryProductForm::PRICE_TYPE_PER_UNIT 
	    		? $productForm->price : null;
	    	$history->price_group = $productForm->priceType === WarehouseDeliveryProductForm::PRICE_TYPE_PER_PACKAGE 
	    		? $productForm->price : null;
	    	$history->currency_id = $this->warehouseDeliveryForm->currencyId;
	    	
	    	if(empty($this->delivery->operation)) {
	    		$history->description = Yii::t('web', 'Added as a part of delivery').': '.$this->warehouseDeliveryForm->number;
	    	}
	    	else {
	    		$history->description = Yii::t('documents', 'Accounting of goods issued no. {number}', ['number' => $this->delivery->operation->number]);
	    	}
	    	
	    	$history->product_status_id = $status->id;
	    	$history->warehouse_delivery_id = $this->delivery->id;
	    	$history->save();
    	}
    }
    
    private function updateOrderedProductsCountByDelivery($operator) {
    	Yii::$app->db->createCommand("
    		UPDATE warehouse_product wp
   			SET count_ordered = count_ordered {$operator} (SELECT count FROM warehouse_delivery_product WHERE warehouse_delivery_id = :deliveryId AND product_id = wp.product_id)
   			WHERE wp.product_id IN (SELECT product_id FROM warehouse_delivery_product WHERE warehouse_delivery_id = :deliveryId) AND wp.warehouse_id = :warehouseId
    	")
    	->bindValue(':deliveryId', $this->delivery->id)
    	->bindValue(':warehouseId', $this->warehouseDeliveryForm->warehouseId)
    	->execute();
    }

    private function updateWarehouseProductStateByDelivery() {
    	$pricePart = '';
    	if(empty($this->delivery->is_artificial) && empty($this->delivery->operation)) {
    		$pricePart = ', price_unit = (SELECT price_unit FROM warehouse_delivery_product WHERE warehouse_delivery_id = :deliveryId AND product_id = wp.product_id)
    				, price_group = (SELECT price_group FROM warehouse_delivery_product WHERE warehouse_delivery_id = :deliveryId AND product_id = wp.product_id)
    				, currency_id = (SELECT currency_id FROM warehouse_delivery_product WHERE warehouse_delivery_id = :deliveryId AND product_id = wp.product_id) ';
    	}
    	
    	Yii::$app->db->createCommand('
            UPDATE warehouse_product wp 
            SET count = count + (SELECT count FROM warehouse_delivery_product WHERE warehouse_delivery_id = :deliveryId AND product_id = wp.product_id),
        		count_available = count_available + (SELECT count FROM warehouse_delivery_product WHERE warehouse_delivery_id = :deliveryId AND product_id = wp.product_id),
        		count_ordered = count_ordered - (SELECT count FROM warehouse_delivery_product WHERE warehouse_delivery_id = :deliveryId AND product_id = wp.product_id)
    			'.$pricePart.'
            WHERE wp.product_id IN (SELECT product_id FROM warehouse_delivery_product WHERE warehouse_delivery_id = :deliveryId) AND wp.warehouse_id = :warehouseId
        ')
        ->bindValue(':deliveryId', $this->delivery->id)
        ->bindValue(':warehouseId', $this->warehouseDeliveryForm->warehouseId)
        ->execute();
    }

    public function getInvalidForms() {
        $invalidForms = [];
        if($this->warehouseDeliveryForm->hasErrors()) {
            $invalidForms[] = $this->warehouseDeliveryForm;
        }

        return $invalidForms + ModelLoaderHelper::getErrorForms($this->productsForms);
    }

    public function getAllForms() {
        return [
            'warehouseDeliveryForm' => $this->warehouseDeliveryForm,
            'productsForms' => $this->productsForms,
        ];
    }

    public function getWarehouseDeliveryForm() {
        return $this->warehouseDeliveryForm;
    }

    public function getProductsForms() {
        return $this->productsForms;
    }

    public function delete() {
        $transaction = Yii::$app->db->beginTransaction();
        try {
            $this->restoreDeliveryForm();
            $this->delivery->is_active = 0;
            $this->updateWarehouseProductStateByDelivery('-');
            $this->delivery->save();

            $transaction->commit();
        }
        catch(\Exception $e) {
            $transaction->rollBack();
            throw $e;
        }
    }

    public function getDelivery() {
        return $this->delivery;
    }
}
