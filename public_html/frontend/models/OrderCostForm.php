<?php

namespace frontend\models;

use Yii;
use yii\base\Model;
use common\models\ar\OrderOfferedProductAttachment;
use common\helpers\UploadHelper;

class OrderCostForm extends Model {

    public $id;
    public $costId;
    public $name;
    public $price;
    public $currencyId;
    public $count;
    public $vatRate;
    public $unit;
    public $netValue;
    public $vatAmount;
    public $grossValue;
    public $width;
    public $height;
    public $shapeId;
    public $constructionId;
    public $muntinId;
    public $remarks;
    public $remarks2;
    public $files = [];
    public $photos = [];
    public $ralColourId;
    public $ralColourSymbol;
    public $ralRgbHash;
    public $urlPhoto;
    public $heightPhoto;
    public $widthPhoto;
    public $topPhoto;
    public $leftPhoto;

    public function attributeLabels() {
        $priceLabel = '';
        if(Yii::$app->params['isOrderProductDimensionsVisible']) {
            $priceLabel = Yii::t(Yii::$app->params['isPlaceholderInputLabel'] ? 'web' : 'main', 'Price [per sq. m]');
        } else {
            $priceLabel = Yii::t('web', 'Price');
        }

        return [
            'name' => \Yii::t('main', 'Name'),
            'price' => $priceLabel,
            'currencyId' => Yii::t('web', 'Currency'),
            'count' => Yii::t('web', 'Number of pieces'),
            'vatRate' => Yii::t('web', 'Vat rate'),
            'unit' => Yii::t('main', 'Unit'),
            'width' => Yii::t('web', 'Width') . ' [mm]',
            'height' => Yii::t('web', 'Height') . ' [mm]',
            'shapeId' => Yii::t('web', 'Shape'),
            'constructionId' => Yii::t('web', 'Construction'),
            'muntinId' => Yii::t('web', 'Muntin'),
            'remarks' => Yii::t('web', 'Remarks 1'),
            'remarks2' => Yii::t('web', 'Remarks 2'),
        ];
    }

    public function init() {
        parent::init();
        $this->unit = 'szt.';
        $this->netValue = '0.0';
        $this->vatAmount = '0.0';
        $this->grossValue = '0.0';
        $this->vatRate = Yii::$app->params['defaultVatRate'];
        $this->urlPhoto = '';
        $this->heightPhoto = 0;
        $this->widthPhoto = 0;
        $this->topPhoto = 0;
        $this->leftPhoto = 0;
    }

    public function rules() {
        return [
            [['costId', 'name', 'count', 'currencyId'], 'validateAll'],
            [['costId', 'unit', 'vatRate'], 'safe'],
            [['width', 'height'], 'isValidDimension'],
            [['width', 'height', 'shapeId', 'constructionId', 'muntinId', 'ralColourId'], 'integer'],
            ['price', 'number', 'numberPattern' => '/^\s*[+-]?[0-9]{1,8}[.,]?[0-9]{0,2}\s*$/'],
            ['count', 'number', 'numberPattern' => '/^\s*[+]?[1-9][0-9]*\s*$/'],
            [['remarks', 'remarks2'], 'string', 'max' => 512],
            [['ralRgbHash', 'ralColourSymbol'], 'string'],
            [['currencyId'], 'exist', 'targetClass' => '\common\models\ar\Currency', 'targetAttribute' => 'id'],
            [['files', 'photos', 'urlPhoto', 'heightPhoto', 'widthPhoto', 'topPhoto', 'leftPhoto'], 'safe']
        ];
    }

    public function validateAll($attribute, $params) {
        if(!empty($this->costId) || !empty($this->name) || !empty($this->price) || !empty($this->count)) {
            if(empty($this->name)) {
                $this->addError('name', \Yii::t('web', '{attribute} field must not be empty', ['attribute' => $this->getAttributeLabel('name')]));
            }
            if(empty($this->currencyId)) {
                $this->addError('currencyId', \Yii::t('web', '{attribute} field must not be empty', ['attribute' => $this->getAttributeLabel('currencyId')]));
            }
            if(Yii::$app->params['isOrderProductCountRequired'] && empty($this->count)) {
                $this->addError('count', \Yii::t('web', '{attribute} field must not be empty', ['attribute' => $this->getAttributeLabel('count')]));
            }
        } else {
            $this->currencyId = '';
        }
    }

    public function restoreFiles() {
        $this->files = array_map(function($file) {
            $documentForm = new DocumentForm();
            $documentForm->document = UploadHelper::getFileNameByUrl($file->url_file);
            $documentForm->name = $file->name;
            $documentForm->makeCopy = true;

            return $documentForm;
        }, OrderOfferedProductAttachment::find()->where(['order_offered_product_id' => $this->id])->all());
    }

    public function isValidDimension($attribute, $params) {
        if(!empty($this->width) && is_numeric($this->width)) {
            if($this->width > \Yii::$app->params['maxDimensionShort']) {
                $this->addError('width', \Yii::t('web', '{attribute} cannot be wider than ', ['attribute' => $this->getAttributeLabel('width')]) . \Yii::$app->params['maxDimensionShort']);
            }
        }

        if(!empty($this->height) && is_numeric($this->height)) {
            if($this->height > \Yii::$app->params['maxDimensionLong']) {
                $this->addError('height', \Yii::t('web', '{attribute} cannot be higher than ', ['attribute' => $this->getAttributeLabel('height')]) . \Yii::$app->params['maxDimensionLong']);
            }
        }

        if(!empty($this->height) && is_numeric($this->height) && !empty($this->width) && is_numeric($this->width)) {
            if($this->height > \Yii::$app->params['maxDimensionShort'] && $this->width > \Yii::$app->params['maxDimensionShort']) {
                $this->addError('width', \Yii::t('web', 'Total size cannot exceed ', ['attribute' => $this->getAttributeLabel('width')]) . \Yii::$app->params['maxDimensionLong'] . 'mm x ' . \Yii::$app->params['maxDimensionShort'] . 'mm.');
            }
        }
    }

}
