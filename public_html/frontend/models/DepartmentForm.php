<?php

namespace frontend\models;

use Yii;
use yii\base\Model;
use common\models\ar\Department;
use common\models\ar\UserDepartment;
use common\models\ar\DepartmentTranslation;
use common\models\ar\DepartmentActivity;
use common\models\ar\Language;
use common\models\ar\User;
use common\models\aq\DepartmentTranslationQuery;
use common\models\aq\UserDepartmentQuery;
use common\models\aq\DepartmentActivityQuery;
use common\helpers\RandomSymbolGenerator;
use yii\helpers\Html;

class DepartmentForm extends Model {
	const SCENARIO_ADD = 'add';
	const SCENARIO_EDIT = 'edit';
    
	public $id;
    public $name;
    public $departmentTypeId;
    public $workerIds;
    public $managerId;
    public $activityIds;

	public function __construct($department = null, $config = []) {
		if(!empty($department)) {
			$this->loadData($department);
		}
		parent::__construct($config);
	}

    public function loadData($department) {
        if(empty($department)) {
            return;
        }
        
    	$this->id = $department->id;
    	$this->departmentTypeId = $department->department_type_id;
        $translation = $department->getTranslation();
        $this->name = empty($translation) ? $department->symbol : $translation->name;

        $activities = DepartmentActivity::findAll(['department_id'=>$this->id]);

        $workers = UserDepartmentQuery::getUsers($this->id);
        $manager = UserDepartmentQuery::getManager($this->id);

        $this->managerId = $manager['id'];
        $this->workerIds = array_map(function($worker){
            return $worker['id'];
        }, $workers);
        $this->activityIds = array_map(function($activity){
            return $activity['activity_id'];
        }, $activities);
    }

    public function getWorkersData() {
        $workers = UserDepartmentQuery::getWorkersByIds($this->workerIds, false);
        return array_map(function($worker) {
            return [
                $worker['id'] => $worker['firstName'].' '.$worker['lastName'],
            ];
        }, $workers);
    }

    public function getManagerData(){
        $manager = UserDepartmentQuery::getWorkersByIds($this->workerIds, true);

        return array_map(function($worker) {
            return [
                $worker['id'] => $worker['firstName'].' '.$worker['lastName'],
            ];
        }, $manager);
    }

    public function getActivitiesData() {
        $activities = DepartmentActivityQuery::getActivitiesByIds($this->activityIds);
        return array_map(function($activity) {
            return [
                $activity['id'] => $activity['name'],
            ];
        }, $activities);
    }
    
	public function scenarios() {
		$scenarios = parent::scenarios();
		$scenarios[self::SCENARIO_ADD] = ['name', 'departmentTypeId', 'workerIds','managerId','activityIds'];
		$scenarios[self::SCENARIO_EDIT] = ['name', 'departmentTypeId','id', 'workerIds', 'managerId', 'activityIds'];
		return $scenarios;
	}

	public function rules() {
		return [
            [['name'], 'required'],
            [['name'], 'string', 'max' => 256],
            [['departmentTypeId'], 'required'],
            [['name'], 'validateName'],
		    [['activityIds', 'workerIds', 'managerId'], 'safe'],
		];
	}

	public function validateName($attribute, $params) {
        $lang = Language::find()->where(['symbol' => \Yii::$app->language])->one()->id;

        $name = $this->name;
        $typeId = intval($this->departmentTypeId);
        $id = intval($this->id);
        
        if($this->isEditScenarioSet()) {
            $duplicates = DepartmentTranslationQuery::getDuplicates($name, $typeId, $lang, $id)->all();
            if((!empty($duplicates))) {
                $this->addError('name', Yii::t('web', 'Name already exists'));
            }
        }
        else {
            $duplicates = DepartmentTranslationQuery::getDuplicates($name, $typeId, $lang, null)->all();
            if((!empty($duplicates))) {
                $this->addError('name', Yii::t('web', 'Name already exists'));
            }
        }
    }

    public function attributeLabels() {
        return [
                'name' => \Yii::t('web', 'Name'),
                'departmentTypeId' => Yii::t('web', 'Department type'),
                'workerIds' => Yii::t('web', 'Workers'),
                'managerId' => Yii::t('web', 'Manager'),
                'activityIds' => Yii::t('web', 'Activities')
        ];
    }

	public function saveData() {
		$transaction = \Yii::$app->db->beginTransaction();
		try {
			if($this->isEditScenarioSet()) {
                $department = Department::find()->where(['id' => intval($this->id)])->one();
			}
			else{
                $department = new Department();
                $department->symbol = RandomSymbolGenerator::generate(Department::class);
            }

            $department->department_type_id = intval($this->departmentTypeId);
            $department->save();
            
            $departmentTranslation = $department->getTranslation() ?: $this->createTranslation($department);
            $departmentTranslation->name = $this->name;
            $departmentTranslation->save();

            $this->saveAssignedActivities($department);
            $this->saveAssignedWorkers($department);
            $this->saveAssignedManager($department, $this->managerId);
            $this->id = $department->id;

            $transaction->commit();
		}
		catch(\Exception $e) {
			$transaction->rollBack();
			throw new \Exception('Could not save department data');
		}
	}

    private function saveAssignedWorkers($department) {
        UserDepartment::deleteAll(['department_id' => $department->id]);

        \Yii::$app->db->createCommand()
            ->batchInsert('user_department', ['user_id', 'department_id', 'is_lead'], array_map(function($workerId) use($department) {
                return ['user_id' => $workerId, 'department_id' => $department->id, 'is_lead' => 0];
        }, empty($this->workerIds) ? [] : $this->workerIds))->execute();
    }

    private function saveAssignedManager($department, $managerId) {
        UserDepartment::updateAll(['is_lead'=> 0],['department_id'=>$department->id]);
        if(count(UserDepartment::findAll(['=','user_id', $managerId],['department_id'=>$department->id])) == 0){
            $manager = new UserDepartment; // or $customer = Customer::findOne($id);
            $manager->department_id = $department->id;
            $manager->user_id = $managerId;
            $manager->is_lead = 1;
            $manager->save();
        }
        UserDepartment::updateAll(['is_lead'=> 1], ['=','user_id', $managerId]);
    }

    private function saveAssignedActivities($department) {
        DepartmentActivity::deleteAll(['department_id' => $department->id]);
        \Yii::$app->db->createCommand()
            ->batchInsert('department_activity', ['department_id', 'activity_id'], array_map(function($activityId) use($department) {
                return [ 'department_id' => $department->id,'activity_id' => $activityId];
            }, empty($this->activityIds) ? [] : $this->activityIds))->execute();
    }


    private function createTranslation($department) {
        $departmentTranslation = new DepartmentTranslation();
        $departmentTranslation->department_id = intval($department->id);
        $departmentTranslation->language_id = Language::find()->where(['symbol' => \Yii::$app->language])->one()->id;
        $departmentTranslation->name = $this->name;
        return $departmentTranslation;
    }

    public function setAddScenario() {
    	$this->setScenario(self::SCENARIO_ADD);
    }

    public function setEditScenario() {
    	$this->setScenario(self::SCENARIO_EDIT);
    }

    public function isEditScenarioSet() {
    	return $this->getScenario() == self::SCENARIO_EDIT;
    }
}