<?php

namespace frontend\models;

use common\components\MessageProvider;
use yii\mail\MessageInterface;
use common\models\ar\MailConfig;
use yii\swiftmailer\Mailer;
use yii\base\Model;
use common\models\ar\User;
use common\helpers\Utility;
use Yii;

/**
 * ContactForm is the model behind the contact form.
 */
class ContactForm extends Model {

		public $name;
		public $email;
		public $body;

		/**
		 * @inheritdoc
		 */
		public function rules() {
				return [
							[['name', 'email', 'body'], 'required'],
							['email', 'email'],
				];
		}
		
		/**
		 * Declares customized attribute labels.
		 * If not declared here, an attribute would have a label that is
		 * the same as its name with the first letter in upper case.
		 */
		public function attributeLabels() {
			return array(
					'name'=>Yii::t('site', 'Name and surname'),
					'email'=>Yii::t('site', 'E-mail'),
					'body'=>Yii::t('site', 'Content'),
			);
		}

		/**
		 * Sends an email to the specified email address using the information collected by this model.
		 *
		 * @param  string  $email the target email address
		 * @return boolean whether the email was sent
		 */
		public function sendEmail($email) {
			$mode = MessageProvider::MODE_EMAIL;
    					
			$obj = new User();
			$obj->email = $email;
			
    		$messageProvider = new MessageProvider();
    		$sent = $messageProvider->send($obj, NULL, Utility::EMAIL_TYPE_CONTACT, 
    				$mode, NULL, [
						'{email}'	=> $this->email,
						'{name}'	=> $this->name,
						'{body}'	=> $this->body
					], false);
    		
    		return $sent;
		}
}
