<?php

namespace frontend\models;

use Yii;
use yii\base\Model;

class ClientsGroupsListForm extends Model {
	
    public $page;
	public $name;
    public $sortDir;
    public $sortField;

    const DEFAULT_PAGE = 0;
    const DEFAULT_SORT_DIR = 'asc';
    const DEFAULT_SORT_FIELD = 'name';

    public function init() {
        parent::init();
        $this->page = self::DEFAULT_PAGE;
        $this->sortDir = self::DEFAULT_SORT_DIR;
        $this->sortField = self::DEFAULT_SORT_FIELD;
    }

    public static function getSortFields() { 
        return [
	            ['name' => 'name', 'label' => Yii::t('main', 'Name')],
        ];
    }

    public function attributeLabels() {
        return [
        		'name' => Yii::t('web', 'Name'),
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
				[['page'], 'integer'],
				[['sortDir'], 'in', 'range' => ['asc', 'desc']],
				[['sortField'], 'in', 'range' => array_column(self::getSortFields(), 'name')],
	            ['page', 'default', 'value' => self::DEFAULT_PAGE],
	            ['sortDir', 'default', 'value' => self::DEFAULT_SORT_DIR],
	            ['sortField', 'default', 'value' => self::DEFAULT_SORT_FIELD],
                [['name'], 'safe'],
        ];
    }
}
