<?php

namespace frontend\models;

use Yii;
use yii\base\Model;
use common\models\ar\OfferedProductCategory;

class OfferedProductCategoryListForm extends Model {

    public $page;
	public $companyName;
    public $sortDir;
    public $sortField;
    public $offeredProductCategorySymbol;

    const DEFAULT_PAGE = 0;
    const DEFAULT_SORT_DIR = 'asc';
    const DEFAULT_SORT_FIELD = 'name';

    public function init() {
        parent::init();
        $this->page = self::DEFAULT_PAGE;
        $this->sortDir = self::DEFAULT_SORT_DIR;
        $this->sortField = self::DEFAULT_SORT_FIELD;
    }

    public static function getSortFields() {
        return [
	            ['name' => 'name', 'label' => Yii::t('main', 'Name'), 'style' => 'width: 20%'],
	            ['name' => 'description', 'label' => Yii::t('main', 'Description')],
				['name' => 'parentCategoryName', 'label' => Yii::t('web', 'Parent category'), 'style' => 'width: 20%'],
        ];
    }

    public function attributeLabels() {
        return [
        		'Name' => Yii::t('web', 'Write category name'),
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
				[['page'], 'integer'],
				[['sortDir'], 'in', 'range' => ['asc', 'desc']],
				[['sortField'], 'in', 'range' => array_column(self::getSortFields(), 'name')],
				[['offeredProductCategorySymbol'], 'safe'],
	            ['page', 'default', 'value' => self::DEFAULT_PAGE],
	            ['sortDir', 'default', 'value' => self::DEFAULT_SORT_DIR],
	            ['sortField', 'default', 'value' => self::DEFAULT_SORT_FIELD],
        ];
    }
}
