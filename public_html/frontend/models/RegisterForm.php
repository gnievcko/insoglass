<?php

namespace frontend\models;

use Yii;
use yii\base\Model;
use common\components\MessageProvider;
use common\models\ar\Address;
use common\models\ar\City;
use common\models\ar\Company;
use common\models\ar\User;
use common\models\ar\UserAction;
use common\models\ar\Language;
use common\helpers\Utility;
use common\models\ar\Currency;
use common\models\ar\DistributionChannel;
use common\models\ar\Province;

/**
 * Register form
 */
class RegisterForm extends Model {
	
    public $email;
	public $password;
	public $password2;

    private $_user;

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
	        [['email', 'password', 'password2'], 'required'],			
			['email', 'email'],
	        ['email', 'validateIdentity'],
			['password2', 'compare', 'compareAttribute' => 'password', 'message' => Yii::t('web', 'Passwords don\'t match.')]
        ];
    }
    
    public function attributeLabels() {
    	return ['email' => Yii::t('main', 'E-mail address'),
    			'password' => Yii::t('main', 'Password'),
    			'password2' => Yii::t('main', 'Password2'),    			
    	];
    }
    
    public function validateIdentity($attribute, $params) {
    //	if(!$this->hasErrors()) {
			if(User::findOne(['email' => $this->email]) == null) {
    			return true;
    		}
    		
    		$this->addError($attribute, Yii::t('web', 'E-mail already exists.'));
    //	}
    }    

    /**
     * Logs in a user using the provided email and password.
     *
     * @return boolean whether the user is logged in successfully
     */
    public function register() {    	
        if($this->validate() == false)
			return false;
		
    	$transaction = User::getDb()->beginTransaction();
    	try {
    		$user = new User();
    		$user->email = empty($this->email) ? null : $this->email;
    		$user->setPassword($this->password);
    		$user->is_active = 0;			
			
    		if(!$user->save()) {
    			throw new \Exception('Could not save user data' . print_r($user->getErrors(), true));
    		}
    		
    		$mode = MessageProvider::MODE_EMAIL;
    		
			$template = Utility::EMAIL_TYPE_WELCOME_VERIFICATION;
			
    		$messageProvider = new MessageProvider();
    		$sent = $messageProvider->send($user, NULL, $template, $mode, NULL, [], false);
    		
    		//print_r($sent);exit;
    		if(!$sent) {
    			throw new \Exception('Rollback after unsuccessful message sending in mode ['.($mode ?: MessageProvider::MODE_ALL).'].');
    		}
    		
			Yii::$app->getSession()->setFlash('success', Yii::t('main', 'E-mail has been sent successfully.'));			    		
    		$transaction->commit();
    	}
		catch(\Exception $e) {
			throw $e;
		    $transaction->rollBack();
		    Yii::error($e->getMessage());
		    Yii::error($e->getTraceAsString());
		    return false;
		}
		
		return true;
    }	

    /**
     * Finds user by [[email]]
     *
     * @return User|null
     */
    protected function getUser() {
        if($this->_user === null) {
        	$validator = new \yii\validators\EmailValidator();
        	$error = 'Not valid';
        	if($validator->validate($this->email, $error)) {
        		$this->_user = User::findByEmail($this->email);
        	}
        }

        return $this->_user;
    }
}
