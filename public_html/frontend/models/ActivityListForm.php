<?php

namespace frontend\models;

use Yii;
use yii\base\Model;

class ActivityListForm extends Model {
    
    public $page;
    public $sortDir;
    public $sortField;
    public $name;
    
    const DEFAULT_PAGE = 0;
    const DEFAULT_SORT_DIR = 'asc';
    const DEFAULT_SORT_FIELD = 'name';
    
    public function init() {
        parent::init();
       $this->page = self::DEFAULT_PAGE;
       $this->sortDir = self::DEFAULT_SORT_DIR;
       $this->sortField = self::DEFAULT_SORT_FIELD;
    }
    
    public static function getSortFields() {
        return [
          ['name'=> 'name', 'label' => Yii::t('main', 'Name'), 'style' => 'width: 75%'],  
        ];
    }
    
    public function getAttributeLabels() {
        return [
            'Name' => Yii::t('web', 'Name'),
        ];
    }
    
    public function rules() {
        return [
            [['page'], 'integer'],
            [['sortDir'], 'in', 'range' => ['asc','desc']],
            [['sortField'], 'in', 'range' => array_column(self::getSortFields(), 'name')],
            [['name'],'string'],
            ['page', 'default', 'value' => self::DEFAULT_PAGE],
            ['sortDir', 'default', 'value' => self::DEFAULT_SORT_DIR],
            ['sortField', 'default', 'value' => self::DEFAULT_SORT_FIELD],
        ];
    }
}