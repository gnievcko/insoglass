<?php

namespace frontend\models;

use Yii;
use yii\base\Model;

class WarehouseDeliveryForm extends Model {

    public $warehouseId;
    public $warehouseSupplierId;
    public $number;
    public $userConfirmingId;
    public $dateDelivery;
    public $description;
    public $currencyId;
    public $isConfirmed;
    public $operationId;

    public function attributeLabels() {
        return [
            	'warehouseId' => \Yii::t('web', 'Warehouse'),
            	'warehouseSupplierId' => \Yii::t('web', 'Supplier'),
            	'number' => \Yii::t('web', 'Number'),
            	'userConfirmingId' => Yii::t('web', 'User confirming'),
            	'isArtificial' => Yii::t('web', 'Is artificial'),
            	'dateDelivery' => Yii::t('web', 'Date delivery'),
            	'description' => Yii::t('web', 'Delivery comment'),
            	'currencyId' => Yii::t('main', 'Currency'),
        		'isConfirmed' => Yii::t('web', 'Is receipt confirmed'),
        		'operationId' => Yii::t('web', 'Related issue'),
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            	[['warehouseId', 'number', 'userConfirmingId', 'dateDelivery', 'currencyId'], 'required'],
            	['dateDelivery', 'date', 'format' => 'php:Y-m-d H:i:s', 'message' => Yii::t('web', 'Invalid date format')],
            	[['description'], 'string', 'max' => 2048],
        		[['operationId', 'warehouseSupplierId'], 'integer'],
            	[['description', 'warehouseSupplierId', 'isConfirmed', 'operationId'], 'safe'],
        		[['description'], 'default', 'value' => null],
        ];
    }
}
