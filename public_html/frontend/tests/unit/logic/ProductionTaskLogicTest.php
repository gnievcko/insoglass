<?php

namespace frontend\tests\unit\logic;

use Yii;
use common\fixtures\ProductAttributeTypeFixture;
use common\fixtures\ActivityFixture;
use common\fixtures\ProductionPathFixture;
use common\fixtures\ProductionPathStepFixture;
use common\fixtures\ProductionPathStepAttributeFixture;
use frontend\logic\ProductionTaskLogic;
use common\fixtures\CompanyFixture;
use common\fixtures\CityFixture;
use common\fixtures\AddressFixture;
use common\fixtures\EntityFixture;
use common\fixtures\ActivityTranslationFixture;
use common\models\ar\Order;
use common\models\ar\OrderHistory;
use common\models\ar\Task;
use common\helpers\Utility;
use common\models\aq\OrderStatusQuery;
use common\models\ar\OrderOfferedProduct;
use common\models\aq\ProductionTaskQuery;
use common\fixtures\OrderGroupFixture;
use common\fixtures\OfferedProductFixture;
use common\helpers\RandomSymbolGenerator;
use common\models\ar\User;
use common\tests\_support\TestHelper;
use yii\helpers\Json;
use common\models\ar\OrderOfferedProductAttribute;
use common\fixtures\OfferedProductTranslationFixture;
use common\fixtures\OfferedProductAttributeFixture;
use yii\db\Query;
use common\models\ar\ProductionTask;
use common\tests\_support\ProjectHelper;

class ProductionTaskLogicTest extends \Codeception\Test\Unit {
    /**
     * @var \UnitTester
     */
    protected $tester;
    protected $helper;
    protected $projectHelper;

    protected function _before() {       
        $this->tester->haveFixtures([
            'city' => [
                'class' => CityFixture::className(),  
            ],
            'address' => [
                'class' => AddressFixture::className(),  
            ],
            'entity' => [
                'class' => EntityFixture::className(),  
            ],
            'company' => [
                'class' => CompanyFixture::className(),  
            ],
            'orderGroup' => [
                'class' => OrderGroupFixture::className(),  
            ],
            'productionPath' => [
                'class' => ProductionPathFixture::className(),
            ],
            'offeredProduct' => [
                'class' => OfferedProductFixture::className(),  
            ],
            'offeredProductTranslation' => [
                'class' => OfferedProductTranslationFixture::className(),
            ],
            'offeredProductAttribute' => [
                'class' => OfferedProductAttributeFixture::className(),  
            ],
            'productAttributeType' => [
                'class' => ProductAttributeTypeFixture::className(),
            ],
            'activity' => [
                'class' => ActivityFixture::className(),
            ],
            'activityTranslation' => [
                'class' => ActivityTranslationFixture::className(),  
            ],
            'productionPathStep' => [
                'class' => ProductionPathStepFixture::className(),
            ],
            'productionPathStepAttribute' => [
                'class' => ProductionPathStepAttributeFixture::className(),
            ],
        ]);
        
        $this->helper = new TestHelper();
        $this->projectHelper = new ProjectHelper();
    }

    protected function _after() {
    }
    
    public function testGenerateTasksForNullId() {
        $logic = new ProductionTaskLogic();
        
        try {
            $logic->generateTasks(null);
            $this->fail('Exception was not thrown');
        }
        catch (\Exception $e) {
            $this->assertTrue(true);
        }
    }
    
    public function testGenerateTasksForNonExistingOrder() {
        $logic = new ProductionTaskLogic();
        $nonExistingOrderId = (new Query())->select(['MAX(id) + 1'])->from('order')->scalar();
    
        try {
            $logic->generateTasks($nonExistingOrderId);
            $this->fail('Exception was not thrown');
        }
        catch (\Exception $e) {
            $this->assertEquals(0, count(ProductionTaskQuery::getByOrderId($nonExistingOrderId)), 'There are unnecessary production tasks');
        }
    }
    
    public function testGenerateTasksForOrderWithoutProperStatus() {
        $logic = new ProductionTaskLogic();
        $order = $this->projectHelper->generateOrder(Utility::ORDER_STATUS_NEW);
    
        try {
            $logic->generateTasks($order->id);
            $this->fail('Exception was not thrown');
        }
        catch (\Exception $e) {
            $this->assertEquals(0, count(ProductionTaskQuery::getByOrderId($order->id)), 'There are unnecessary production tasks');
        }
    }

    public function testGenerateTasksForOrderWithoutProducts() {
        $logic = new ProductionTaskLogic();
        $order = $this->projectHelper->generateOrder(Utility::ORDER_STATUS_IN_PROGRESS);
        
        try {
            $logic->generateTasks($order->id);
            $this->fail('Exception was not thrown');
        }
        catch (\Exception $e) {
            $this->assertEquals(0, count(ProductionTaskQuery::getByOrderId($order->id)), 'There are unnecessary production tasks');
        }
    }
    
    public function testGenerateTasksForGeneralPathWithNoAttributes() {
        $logic = new ProductionTaskLogic();
        $order = $this->projectHelper->generateOrder(Utility::ORDER_STATUS_IN_PROGRESS);
        $order = $this->projectHelper->assignOfferedProduct($order, 1);
        
        $user = User::findOne(2);
        Yii::$app->user->login($user);
        
        $logic->generateTasks($order->id, true);
        
        $productionTasks = ProductionTaskQuery::getByOrderId($order->id);
        try {
            $this->assertEquals(3, count($productionTasks), 'Number of tasks is incorrect');
            
            $this->assertEquals(1, $productionTasks[0]['offeredProductId'], 'Task 0: offered product is incorrect');
            $this->assertEquals(1, $productionTasks[0]['stepId'], 'Task 0: step is incorrect');
            $this->assertNull($productionTasks[0]['requiredIds'], 'Task 0: requiredIds is incorrect');
            $this->assertEquals($productionTasks[1]['id'], intval($productionTasks[0]['successorIds']), 'Task 0: successorIds is incorrect');
            $this->assertEquals(1, $productionTasks[0]['isAvailable'], 'Task 0: is available is incorrect');
            
            $this->assertEquals(1, $productionTasks[1]['offeredProductId'], 'Task 1: offered product is incorrect');
            $this->assertEquals(7, $productionTasks[1]['stepId'], 'Task 1: step is incorrect');
            $this->assertEquals($productionTasks[0]['id'], intval($productionTasks[1]['requiredIds']), 'Task 1: requiredIds is incorrect');
            $this->assertEquals($productionTasks[2]['id'], intval($productionTasks[1]['successorIds']), 'Task 1: successorIds is incorrect');
            $this->assertEquals(0, $productionTasks[1]['isAvailable'], 'Task 1: is available is incorrect');
            
            $this->assertEquals(1, $productionTasks[2]['offeredProductId'], 'Task 2: offered product is incorrect');
            $this->assertEquals(14, $productionTasks[2]['stepId'], 'Task 2: step is incorrect');
            $this->assertEquals($productionTasks[1]['id'], intval($productionTasks[2]['requiredIds']), 'Task 2: requiredIds is incorrect');
            $this->assertNull($productionTasks[2]['successorIds'], 'Task 2: successorIds is incorrect');
            $this->assertEquals(0, $productionTasks[2]['isAvailable'], 'Task 2: is available is incorrect');
        }
        catch(\Exception $e) {
            $this->fail($e->getMessage());
        }
    }
    
    public function testGenerateTasksForGeneralPathWithStraightAttributesForOrder() {
    	$logic = new ProductionTaskLogic();
    	$order = $this->projectHelper->generateOrder(Utility::ORDER_STATUS_IN_PROGRESS);
    	$order = $this->projectHelper->assignOfferedProduct($order, 1, [['id' => 2], ['id' => 5], ['id' => 6], ['id' => 10]]);
    
    	$user = User::findOne(2);
    	Yii::$app->user->login($user);
    
    	$logic->generateTasks($order->id, true);
    
    	$productionTasks = ProductionTaskQuery::getByOrderId($order->id);
    	try {
    		$this->assertEquals(7, count($productionTasks), 'Number of tasks is incorrect');
    
    		$this->assertEquals(1, $productionTasks[0]['offeredProductId'], 'Task 0: offered product is incorrect');
    		$this->assertEquals(1, $productionTasks[0]['stepId'], 'Task 0: step is incorrect');
    		$this->assertNull($productionTasks[0]['requiredIds'], 'Task 0: requiredIds is incorrect');
    		$this->assertEquals($productionTasks[1]['id'], intval($productionTasks[0]['successorIds']), 'Task 0: successorIds is incorrect');
    		$this->assertEquals(1, $productionTasks[0]['isAvailable'], 'Task 0: is available is incorrect');
    
    		$this->assertEquals(1, $productionTasks[1]['offeredProductId'], 'Task 1: offered product is incorrect');
    		$this->assertEquals(3, $productionTasks[1]['stepId'], 'Task 1: step is incorrect');
    		$this->assertEquals($productionTasks[0]['id'], intval($productionTasks[1]['requiredIds']), 'Task 1: requiredIds is incorrect');
    		$this->assertEquals($productionTasks[2]['id'], intval($productionTasks[1]['successorIds']), 'Task 1: successorIds is incorrect');
    		$this->assertEquals(0, $productionTasks[1]['isAvailable'], 'Task 1: is available is incorrect');
    
    		$this->assertEquals(1, $productionTasks[2]['offeredProductId'], 'Task 2: offered product is incorrect');
    		$this->assertEquals(6, $productionTasks[2]['stepId'], 'Task 2: step is incorrect');
    		$this->assertEquals($productionTasks[1]['id'], intval($productionTasks[2]['requiredIds']), 'Task 2: requiredIds is incorrect');
    		$this->assertEquals($productionTasks[3]['id'], intval($productionTasks[2]['successorIds']), 'Task 2: successorIds is incorrect');
    		$this->assertEquals(0, $productionTasks[2]['isAvailable'], 'Task 2: is available is incorrect');
    		
    		$this->assertEquals(1, $productionTasks[3]['offeredProductId'], 'Task 3: offered product is incorrect');
    		$this->assertEquals(7, $productionTasks[3]['stepId'], 'Task 3: step is incorrect');
    		$this->assertEquals($productionTasks[2]['id'], intval($productionTasks[3]['requiredIds']), 'Task 3: requiredIds is incorrect');
    		$this->assertEquals($productionTasks[4]['id'], intval($productionTasks[3]['successorIds']), 'Task 3: successorIds is incorrect');
    		$this->assertEquals(0, $productionTasks[3]['isAvailable'], 'Task 3: is available is incorrect');
    		
    		$this->assertEquals(1, $productionTasks[4]['offeredProductId'], 'Task 4: offered product is incorrect');
    		$this->assertEquals(8, $productionTasks[4]['stepId'], 'Task 4: step is incorrect');
    		$this->assertEquals($productionTasks[3]['id'], intval($productionTasks[4]['requiredIds']), 'Task 4: requiredIds is incorrect');
    		$this->assertEquals($productionTasks[5]['id'], intval($productionTasks[4]['successorIds']), 'Task 4: successorIds is incorrect');
    		$this->assertEquals(0, $productionTasks[4]['isAvailable'], 'Task 4: is available is incorrect');
    		
    		$this->assertEquals(1, $productionTasks[5]['offeredProductId'], 'Task 5: offered product is incorrect');
    		$this->assertEquals(13, $productionTasks[5]['stepId'], 'Task 5: step is incorrect');
    		$this->assertEquals($productionTasks[4]['id'], intval($productionTasks[5]['requiredIds']), 'Task 5: requiredIds is incorrect');
    		$this->assertEquals($productionTasks[6]['id'], intval($productionTasks[5]['successorIds']), 'Task 5: successorIds is incorrect');
    		$this->assertEquals(0, $productionTasks[5]['isAvailable'], 'Task 5: is available is incorrect');
    		
    		$this->assertEquals(1, $productionTasks[6]['offeredProductId'], 'Task 6: offered product is incorrect');
    		$this->assertEquals(14, $productionTasks[6]['stepId'], 'Task 6: step is incorrect');
    		$this->assertEquals($productionTasks[5]['id'], intval($productionTasks[6]['requiredIds']), 'Task 6: requiredIds is incorrect');
    		$this->assertNull($productionTasks[6]['successorIds'], 'Task 6: successorIds is incorrect');
    		$this->assertEquals(0, $productionTasks[6]['isAvailable'], 'Task 6: is available is incorrect');
    	}
    	catch(\Exception $e) {
    		$this->fail($e->getMessage());
    	}
    }
    
    public function testGenerateTasksForGeneralPathWithStraightAttributes() {
        $logic = new ProductionTaskLogic();
        $order = $this->projectHelper->generateOrder(Utility::ORDER_STATUS_IN_PROGRESS);
        $order = $this->projectHelper->assignOfferedProduct($order, 2);
    
        $user = User::findOne(2);
        Yii::$app->user->login($user);
    
        $logic->generateTasks($order->id, true);
    
        $productionTasks = ProductionTaskQuery::getByOrderId($order->id);
        try {
            $this->assertEquals(7, count($productionTasks), 'Number of tasks is incorrect');
    
            $this->assertEquals(2, $productionTasks[0]['offeredProductId'], 'Task 0: offered product is incorrect');
            $this->assertEquals(1, $productionTasks[0]['stepId'], 'Task 0: step is incorrect');
            $this->assertNull($productionTasks[0]['requiredIds'], 'Task 0: requiredIds is incorrect');
            $this->assertEquals($productionTasks[1]['id'], intval($productionTasks[0]['successorIds']), 'Task 0: successorIds is incorrect');
            $this->assertEquals(1, $productionTasks[0]['isAvailable'], 'Task 0: is available is incorrect');
    
            $this->assertEquals(2, $productionTasks[1]['offeredProductId'], 'Task 1: offered product is incorrect');
            $this->assertEquals(3, $productionTasks[1]['stepId'], 'Task 1: step is incorrect');
            $this->assertEquals($productionTasks[0]['id'], intval($productionTasks[1]['requiredIds']), 'Task 1: requiredIds is incorrect');
            $this->assertEquals($productionTasks[2]['id'], intval($productionTasks[1]['successorIds']), 'Task 1: successorIds is incorrect');
            $this->assertEquals(0, $productionTasks[1]['isAvailable'], 'Task 1: is available is incorrect');
    
            $this->assertEquals(2, $productionTasks[2]['offeredProductId'], 'Task 2: offered product is incorrect');
            $this->assertEquals(6, $productionTasks[2]['stepId'], 'Task 2: step is incorrect');
            $this->assertEquals($productionTasks[1]['id'], intval($productionTasks[2]['requiredIds']), 'Task 2: requiredIds is incorrect');
            $this->assertEquals($productionTasks[3]['id'], intval($productionTasks[2]['successorIds']), 'Task 2: successorIds is incorrect');
            $this->assertEquals(0, $productionTasks[2]['isAvailable'], 'Task 2: is available is incorrect');
    
            $this->assertEquals(2, $productionTasks[3]['offeredProductId'], 'Task 3: offered product is incorrect');
            $this->assertEquals(7, $productionTasks[3]['stepId'], 'Task 3: step is incorrect');
            $this->assertEquals($productionTasks[2]['id'], intval($productionTasks[3]['requiredIds']), 'Task 3: requiredIds is incorrect');
            $this->assertEquals($productionTasks[4]['id'], intval($productionTasks[3]['successorIds']), 'Task 3: successorIds is incorrect');
            $this->assertEquals(0, $productionTasks[3]['isAvailable'], 'Task 3: is available is incorrect');
    
            $this->assertEquals(2, $productionTasks[4]['offeredProductId'], 'Task 4: offered product is incorrect');
            $this->assertEquals(8, $productionTasks[4]['stepId'], 'Task 4: step is incorrect');
            $this->assertEquals($productionTasks[3]['id'], intval($productionTasks[4]['requiredIds']), 'Task 4: requiredIds is incorrect');
            $this->assertEquals($productionTasks[5]['id'], intval($productionTasks[4]['successorIds']), 'Task 4: successorIds is incorrect');
            $this->assertEquals(0, $productionTasks[4]['isAvailable'], 'Task 4: is available is incorrect');
    
            $this->assertEquals(2, $productionTasks[5]['offeredProductId'], 'Task 5: offered product is incorrect');
            $this->assertEquals(13, $productionTasks[5]['stepId'], 'Task 5: step is incorrect');
            $this->assertEquals($productionTasks[4]['id'], intval($productionTasks[5]['requiredIds']), 'Task 5: requiredIds is incorrect');
            $this->assertEquals($productionTasks[6]['id'], intval($productionTasks[5]['successorIds']), 'Task 5: successorIds is incorrect');
            $this->assertEquals(0, $productionTasks[5]['isAvailable'], 'Task 5: is available is incorrect');
    
            $this->assertEquals(2, $productionTasks[6]['offeredProductId'], 'Task 6: offered product is incorrect');
            $this->assertEquals(14, $productionTasks[6]['stepId'], 'Task 6: step is incorrect');
            $this->assertEquals($productionTasks[5]['id'], intval($productionTasks[6]['requiredIds']), 'Task 6: requiredIds is incorrect');
            $this->assertNull($productionTasks[6]['successorIds'], 'Task 6: successorIds is incorrect');
            $this->assertEquals(0, $productionTasks[6]['isAvailable'], 'Task 6: is available is incorrect');
        }
        catch(\Exception $e) {
            $this->fail($e->getMessage());
        }
    }    
    
    public function testGenerateTasksForGeneralPathWithParallelAttributesForOrder() {
    	$logic = new ProductionTaskLogic();
    	$order = $this->projectHelper->generateOrder(Utility::ORDER_STATUS_IN_PROGRESS);
    	$order = $this->projectHelper->assignOfferedProduct($order, 1, [['id' => 1], ['id' => 3], ['id' => 4], ['id' => 7], ['id' => 9]]);
    
    	$user = User::findOne(2);
    	Yii::$app->user->login($user);
    
    	$logic->generateTasks($order->id, true);
    
    	$productionTasks = ProductionTaskQuery::getByOrderId($order->id);
    	try {
    		$this->assertEquals(10, count($productionTasks), 'Number of tasks is incorrect');
    
    		$this->assertEquals(1, $productionTasks[0]['offeredProductId'], 'Task 0: offered product is incorrect');
    		$this->assertEquals(1, $productionTasks[0]['stepId'], 'Task 0: step is incorrect');
    		$this->assertNull($productionTasks[0]['requiredIds'], 'Task 0: requiredIds is incorrect');
    		$this->assertEquals($productionTasks[1]['id'], $productionTasks[0]['successorIds'], 'Task 0: successorIds is incorrect');
    		$this->assertEquals(1, $productionTasks[0]['isAvailable'], 'Task 0: is available is incorrect');
    
    		$this->assertEquals(1, $productionTasks[1]['offeredProductId'], 'Task 1: offered product is incorrect');
    		$this->assertEquals(2, $productionTasks[1]['stepId'], 'Task 1: step is incorrect');
    		$this->assertEquals($productionTasks[0]['id'], $productionTasks[1]['requiredIds'], 'Task 1: requiredIds is incorrect');
    		$this->assertEquals($productionTasks[2]['id'].','.$productionTasks[3]['id'], $productionTasks[1]['successorIds'], 'Task 1: successorIds is incorrect');
    		$this->assertEquals(0, $productionTasks[1]['isAvailable'], 'Task 1: is available is incorrect');
    
    		$this->assertEquals(1, $productionTasks[2]['offeredProductId'], 'Task 2: offered product is incorrect');
    		$this->assertEquals(4, $productionTasks[2]['stepId'], 'Task 2: step is incorrect');
    		$this->assertEquals($productionTasks[1]['id'], $productionTasks[2]['requiredIds'], 'Task 2: requiredIds is incorrect');
    		$this->assertEquals($productionTasks[4]['id'], $productionTasks[2]['successorIds'], 'Task 2: successorIds is incorrect');
    		$this->assertEquals(0, $productionTasks[2]['isAvailable'], 'Task 2: is available is incorrect');
    		
    		$this->assertEquals(1, $productionTasks[3]['offeredProductId'], 'Task 3: offered product is incorrect');
    		$this->assertEquals(5, $productionTasks[3]['stepId'], 'Task 3: step is incorrect');
    		$this->assertEquals($productionTasks[1]['id'], $productionTasks[3]['requiredIds'], 'Task 3: requiredIds is incorrect');
    		$this->assertEquals($productionTasks[4]['id'], $productionTasks[3]['successorIds'], 'Task 3: successorIds is incorrect');
    		$this->assertEquals(0, $productionTasks[3]['isAvailable'], 'Task 3: is available is incorrect');
    		
    		$this->assertEquals(1, $productionTasks[4]['offeredProductId'], 'Task 4: offered product is incorrect');
    		$this->assertEquals(7, $productionTasks[4]['stepId'], 'Task 4: step is incorrect');
    		$this->assertEquals($productionTasks[2]['id'].','.$productionTasks[3]['id'], $productionTasks[4]['requiredIds'], 'Task 4: requiredIds is incorrect');
    		$this->assertEquals($productionTasks[5]['id'], $productionTasks[4]['successorIds'], 'Task 4: successorIds is incorrect');
    		$this->assertEquals(0, $productionTasks[4]['isAvailable'], 'Task 4: is available is incorrect');
    		
    		$this->assertEquals(1, $productionTasks[5]['offeredProductId'], 'Task 5: offered product is incorrect');
    		$this->assertEquals(9, $productionTasks[5]['stepId'], 'Task 5: step is incorrect');
    		$this->assertEquals($productionTasks[4]['id'], $productionTasks[5]['requiredIds'], 'Task 5: requiredIds is incorrect');
    		$this->assertEquals($productionTasks[6]['id'], $productionTasks[5]['successorIds'], 'Task 5: successorIds is incorrect');
    		$this->assertEquals(0, $productionTasks[5]['isAvailable'], 'Task 5: is available is incorrect');
    		
    		$this->assertEquals(1, $productionTasks[6]['offeredProductId'], 'Task 6: offered product is incorrect');
    		$this->assertEquals(10, $productionTasks[6]['stepId'], 'Task 6: step is incorrect');
    		$this->assertEquals($productionTasks[5]['id'], $productionTasks[6]['requiredIds'], 'Task 6: requiredIds is incorrect');
    		$this->assertEquals($productionTasks[7]['id'], $productionTasks[6]['successorIds'], 'Task 6: successorIds is incorrect');
    		$this->assertEquals(0, $productionTasks[6]['isAvailable'], 'Task 6: is available is incorrect');
    		
    		$this->assertEquals(1, $productionTasks[7]['offeredProductId'], 'Task 7: offered product is incorrect');
    		$this->assertEquals(11, $productionTasks[7]['stepId'], 'Task 7: step is incorrect');
    		$this->assertEquals($productionTasks[6]['id'], $productionTasks[7]['requiredIds'], 'Task 7: requiredIds is incorrect');
    		$this->assertEquals($productionTasks[8]['id'], $productionTasks[7]['successorIds'], 'Task 7: successorIds is incorrect');
    		$this->assertEquals(0, $productionTasks[7]['isAvailable'], 'Task 7: is available is incorrect');
    		
    		$this->assertEquals(1, $productionTasks[8]['offeredProductId'], 'Task 8: offered product is incorrect');
    		$this->assertEquals(12, $productionTasks[8]['stepId'], 'Task 8: step is incorrect');
    		$this->assertEquals($productionTasks[7]['id'], $productionTasks[8]['requiredIds'], 'Task 8: requiredIds is incorrect');
    		$this->assertEquals($productionTasks[9]['id'], $productionTasks[8]['successorIds'], 'Task 8: successorIds is incorrect');
    		$this->assertEquals(0, $productionTasks[8]['isAvailable'], 'Task 8: is available is incorrect');
    		
    		$this->assertEquals(1, $productionTasks[9]['offeredProductId'], 'Task 9: offered product is incorrect');
    		$this->assertEquals(14, $productionTasks[9]['stepId'], 'Task 9: step is incorrect');
    		$this->assertEquals($productionTasks[8]['id'], $productionTasks[9]['requiredIds'], 'Task 9: requiredIds is incorrect');
    		$this->assertNull($productionTasks[9]['successorIds'], 'Task 9: successorIds is incorrect');
    		$this->assertEquals(0, $productionTasks[9]['isAvailable'], 'Task 9: is available is incorrect');
    	}
    	catch(\Exception $e) {
    		$this->fail($e->getMessage());
    	}
    }            
    
    public function testGenerateTasksForGeneralPathWithAllAttributesForOrder() {
        $logic = new ProductionTaskLogic();
        $order = $this->projectHelper->generateOrder(Utility::ORDER_STATUS_IN_PROGRESS);
        $order = $this->projectHelper->assignOfferedProduct($order, 1, [['id' => 1], ['id' => 2], ['id' => 3], ['id' => 4], ['id' => 5], ['id' => 6], ['id' => 7], ['id' => 8], ['id' => 9], ['id' => 10]]);
    
        $user = User::findOne(2);
        Yii::$app->user->login($user);
    
        $logic->generateTasks($order->id, true);
    
        $productionTasks = ProductionTaskQuery::getByOrderId($order->id);
        try {
            $this->assertEquals(14, count($productionTasks), 'Number of tasks is incorrect');
    
            $this->assertEquals(1, $productionTasks[0]['offeredProductId'], 'Task 0: offered product is incorrect');
            $this->assertEquals(1, $productionTasks[0]['stepId'], 'Task 0: step is incorrect');
            $this->assertNull($productionTasks[0]['requiredIds'], 'Task 0: requiredIds is incorrect');
            $this->assertEquals($productionTasks[1]['id'], $productionTasks[0]['successorIds'], 'Task 0: successorIds is incorrect');
            $this->assertEquals(1, $productionTasks[0]['isAvailable'], 'Task 0: is available is incorrect');
    
            $this->assertEquals(1, $productionTasks[1]['offeredProductId'], 'Task 1: offered product is incorrect');
            $this->assertEquals(2, $productionTasks[1]['stepId'], 'Task 1: step is incorrect');
            $this->assertEquals($productionTasks[0]['id'], $productionTasks[1]['requiredIds'], 'Task 1: requiredIds is incorrect');
            $this->assertEquals($productionTasks[2]['id'], $productionTasks[1]['successorIds'], 'Task 1: successorIds is incorrect');
            $this->assertEquals(0, $productionTasks[1]['isAvailable'], 'Task 1: is available is incorrect');
    
            $this->assertEquals(1, $productionTasks[2]['offeredProductId'], 'Task 2: offered product is incorrect');
            $this->assertEquals(3, $productionTasks[2]['stepId'], 'Task 2: step is incorrect');
            $this->assertEquals($productionTasks[1]['id'], $productionTasks[2]['requiredIds'], 'Task 2: requiredIds is incorrect');
            $this->assertEquals($productionTasks[3]['id'].','.$productionTasks[4]['id'].','.$productionTasks[5]['id'], $productionTasks[2]['successorIds'], 'Task 2: successorIds is incorrect');
            $this->assertEquals(0, $productionTasks[2]['isAvailable'], 'Task 2: is available is incorrect');
    
            $this->assertEquals(1, $productionTasks[3]['offeredProductId'], 'Task 3: offered product is incorrect');
            $this->assertEquals(4, $productionTasks[3]['stepId'], 'Task 3: step is incorrect');
            $this->assertEquals($productionTasks[2]['id'], $productionTasks[3]['requiredIds'], 'Task 3: requiredIds is incorrect');
            $this->assertEquals($productionTasks[6]['id'], $productionTasks[3]['successorIds'], 'Task 3: successorIds is incorrect');
            $this->assertEquals(0, $productionTasks[3]['isAvailable'], 'Task 3: is available is incorrect');
    
            $this->assertEquals(1, $productionTasks[4]['offeredProductId'], 'Task 4: offered product is incorrect');
            $this->assertEquals(5, $productionTasks[4]['stepId'], 'Task 4: step is incorrect');
            $this->assertEquals($productionTasks[2]['id'], $productionTasks[4]['requiredIds'], 'Task 4: requiredIds is incorrect');
            $this->assertEquals($productionTasks[6]['id'], $productionTasks[4]['successorIds'], 'Task 4: successorIds is incorrect');
            $this->assertEquals(0, $productionTasks[4]['isAvailable'], 'Task 4: is available is incorrect');
    
            $this->assertEquals(1, $productionTasks[5]['offeredProductId'], 'Task 5: offered product is incorrect');
            $this->assertEquals(6, $productionTasks[5]['stepId'], 'Task 5: step is incorrect');
            $this->assertEquals($productionTasks[2]['id'], $productionTasks[5]['requiredIds'], 'Task 5: requiredIds is incorrect');
            $this->assertEquals($productionTasks[6]['id'], $productionTasks[5]['successorIds'], 'Task 5: successorIds is incorrect');
            $this->assertEquals(0, $productionTasks[5]['isAvailable'], 'Task 5: is available is incorrect');
    
            $this->assertEquals(1, $productionTasks[6]['offeredProductId'], 'Task 6: offered product is incorrect');
            $this->assertEquals(7, $productionTasks[6]['stepId'], 'Task 6: step is incorrect');
            $this->assertEquals($productionTasks[3]['id'].','.$productionTasks[4]['id'].','.$productionTasks[5]['id'], $productionTasks[6]['requiredIds'], 'Task 6: requiredIds is incorrect');
            $this->assertEquals($productionTasks[7]['id'].','.$productionTasks[8]['id'], $productionTasks[6]['successorIds'], 'Task 6: successorIds is incorrect');
            $this->assertEquals(0, $productionTasks[6]['isAvailable'], 'Task 6: is available is incorrect');
    
            $this->assertEquals(1, $productionTasks[7]['offeredProductId'], 'Task 7: offered product is incorrect');
            $this->assertEquals(8, $productionTasks[7]['stepId'], 'Task 7: step is incorrect');
            $this->assertEquals($productionTasks[6]['id'], $productionTasks[7]['requiredIds'], 'Task 7: requiredIds is incorrect');
            $this->assertEquals($productionTasks[10]['id'], $productionTasks[7]['successorIds'], 'Task 7: successorIds is incorrect');
            $this->assertEquals(0, $productionTasks[7]['isAvailable'], 'Task 7: is available is incorrect');
    
            $this->assertEquals(1, $productionTasks[8]['offeredProductId'], 'Task 8: offered product is incorrect');
            $this->assertEquals(9, $productionTasks[8]['stepId'], 'Task 8: step is incorrect');
            $this->assertEquals($productionTasks[6]['id'], $productionTasks[8]['requiredIds'], 'Task 8: requiredIds is incorrect');
            $this->assertEquals($productionTasks[9]['id'], $productionTasks[8]['successorIds'], 'Task 8: successorIds is incorrect');
            $this->assertEquals(0, $productionTasks[8]['isAvailable'], 'Task 8: is available is incorrect');
    
            $this->assertEquals(1, $productionTasks[9]['offeredProductId'], 'Task 9: offered product is incorrect');
            $this->assertEquals(10, $productionTasks[9]['stepId'], 'Task 9: step is incorrect');
            $this->assertEquals($productionTasks[8]['id'], $productionTasks[9]['requiredIds'], 'Task 9: requiredIds is incorrect');
            $this->assertEquals($productionTasks[10]['id'], $productionTasks[9]['successorIds'], 'Task 9: successorIds is incorrect');
            $this->assertEquals(0, $productionTasks[9]['isAvailable'], 'Task 9: is available is incorrect');
    
            $this->assertEquals(1, $productionTasks[10]['offeredProductId'], 'Task 10: offered product is incorrect');
            $this->assertEquals(11, $productionTasks[10]['stepId'], 'Task 10: step is incorrect');
            $this->assertEquals($productionTasks[7]['id'].','.$productionTasks[9]['id'], $productionTasks[10]['requiredIds'], 'Task 10: requiredIds is incorrect');
            $this->assertEquals($productionTasks[11]['id'], $productionTasks[10]['successorIds'], 'Task 10: successorIds is incorrect');
            $this->assertEquals(0, $productionTasks[10]['isAvailable'], 'Task 10: is available is incorrect');
    
            $this->assertEquals(1, $productionTasks[11]['offeredProductId'], 'Task 11: offered product is incorrect');
            $this->assertEquals(12, $productionTasks[11]['stepId'], 'Task 11: step is incorrect');
            $this->assertEquals($productionTasks[10]['id'], $productionTasks[11]['requiredIds'], 'Task 11: requiredIds is incorrect');
            $this->assertEquals($productionTasks[12]['id'], $productionTasks[11]['successorIds'], 'Task 11: successorIds is incorrect');
            $this->assertEquals(0, $productionTasks[11]['isAvailable'], 'Task 11: is available is incorrect');
    
            $this->assertEquals(1, $productionTasks[12]['offeredProductId'], 'Task 12: offered product is incorrect');
            $this->assertEquals(13, $productionTasks[12]['stepId'], 'Task 12: step is incorrect');
            $this->assertEquals($productionTasks[11]['id'], $productionTasks[12]['requiredIds'], 'Task 12: requiredIds is incorrect');
            $this->assertEquals($productionTasks[13]['id'], $productionTasks[12]['successorIds'], 'Task 12: successorIds is incorrect');
            $this->assertEquals(0, $productionTasks[12]['isAvailable'], 'Task 12: is available is incorrect');
    
            $this->assertEquals(1, $productionTasks[13]['offeredProductId'], 'Task 13: offered product is incorrect');
            $this->assertEquals(14, $productionTasks[13]['stepId'], 'Task 13: step is incorrect');
            $this->assertEquals($productionTasks[12]['id'], $productionTasks[13]['requiredIds'], 'Task 13: requiredIds is incorrect');
            $this->assertNull($productionTasks[13]['successorIds'], 'Task 13: successorIds is incorrect');
            $this->assertEquals(0, $productionTasks[13]['isAvailable'], 'Task 13: is available is incorrect');
        }
        catch(\Exception $e) {
            $this->fail($e->getTraceAsString());
        }
    }
    
    public function testGenerateTasksForSpecificPathForOrder() {
        $logic = new ProductionTaskLogic();
        $order = $this->projectHelper->generateOrder(Utility::ORDER_STATUS_IN_PROGRESS);
        $order = $this->projectHelper->assignOfferedProduct($order, 1, [], 2);
    
        $user = User::findOne(2);
        Yii::$app->user->login($user);
    
        $logic->generateTasks($order->id, false);
    
        $productionTasks = ProductionTaskQuery::getByOrderId($order->id);
        try {
            $this->assertEquals(4, count($productionTasks), 'Number of tasks is incorrect');
    
            $this->assertEquals(1, $productionTasks[0]['offeredProductId'], 'Task 0: offered product is incorrect');
            $this->assertEquals(15, $productionTasks[0]['stepId'], 'Task 0: step is incorrect');
            $this->assertNull($productionTasks[0]['requiredIds'], 'Task 0: requiredIds is incorrect');
            $this->assertEquals($productionTasks[1]['id'].','.$productionTasks[2]['id'], $productionTasks[0]['successorIds'], 'Task 0: successorIds is incorrect');
            $this->assertEquals(1, $productionTasks[0]['isAvailable'], 'Task 0: is available is incorrect');
    
            $this->assertEquals(1, $productionTasks[1]['offeredProductId'], 'Task 1: offered product is incorrect');
            $this->assertEquals(16, $productionTasks[1]['stepId'], 'Task 1: step is incorrect');
            $this->assertEquals($productionTasks[0]['id'], $productionTasks[1]['requiredIds'], 'Task 1: requiredIds is incorrect');
            $this->assertEquals($productionTasks[3]['id'], $productionTasks[1]['successorIds'], 'Task 1: successorIds is incorrect');
            $this->assertEquals(0, $productionTasks[1]['isAvailable'], 'Task 1: is available is incorrect');
            
            $this->assertEquals(1, $productionTasks[2]['offeredProductId'], 'Task 2: offered product is incorrect');
            $this->assertEquals(17, $productionTasks[2]['stepId'], 'Task 2: step is incorrect');
            $this->assertEquals($productionTasks[0]['id'], $productionTasks[2]['requiredIds'], 'Task 2: requiredIds is incorrect');
            $this->assertEquals($productionTasks[3]['id'], $productionTasks[2]['successorIds'], 'Task 2: successorIds is incorrect');
            $this->assertEquals(0, $productionTasks[2]['isAvailable'], 'Task 2: is available is incorrect');
    
            $this->assertEquals(1, $productionTasks[3]['offeredProductId'], 'Task 3: offered product is incorrect');
            $this->assertEquals(18, $productionTasks[3]['stepId'], 'Task 3: step is incorrect');
            $this->assertEquals($productionTasks[1]['id'].','.$productionTasks[2]['id'], $productionTasks[3]['requiredIds'], 'Task 3: requiredIds is incorrect');
            $this->assertNull($productionTasks[3]['successorIds'], 'Task 3: successorIds is incorrect');
            $this->assertEquals(0, $productionTasks[3]['isAvailable'], 'Task 3: is available is incorrect');
        }
        catch(\Exception $e) {
            $this->fail($e->getMessage());
        }
    }
    
    public function testGenerateTasksForSpecificPathForProduct() {
        $logic = new ProductionTaskLogic();
        $order = $this->projectHelper->generateOrder(Utility::ORDER_STATUS_IN_PROGRESS);
        $order = $this->projectHelper->assignOfferedProduct($order, 3);
            
        $user = User::findOne(2);
        Yii::$app->user->login($user);
    
        $logic->generateTasks($order->id, false);
    
        $productionTasks = ProductionTaskQuery::getByOrderId($order->id);
        try {
            $this->assertEquals(4, count($productionTasks), 'Number of tasks is incorrect');
    
            $this->assertEquals(3, $productionTasks[0]['offeredProductId'], 'Task 0: offered product is incorrect');
            $this->assertEquals(19, $productionTasks[0]['stepId'], 'Task 0: step is incorrect');
            $this->assertNull($productionTasks[0]['requiredIds'], 'Task 0: requiredIds is incorrect');
            $this->assertEquals($productionTasks[1]['id'], $productionTasks[0]['successorIds'], 'Task 0: successorIds is incorrect');
            $this->assertEquals(1, $productionTasks[0]['isAvailable'], 'Task 0: is available is incorrect');
    
            $this->assertEquals(3, $productionTasks[1]['offeredProductId'], 'Task 1: offered product is incorrect');
            $this->assertEquals(20, $productionTasks[1]['stepId'], 'Task 1: step is incorrect');
            $this->assertEquals($productionTasks[0]['id'], $productionTasks[1]['requiredIds'], 'Task 1: requiredIds is incorrect');
            $this->assertEquals($productionTasks[2]['id'], $productionTasks[1]['successorIds'], 'Task 1: successorIds is incorrect');
            $this->assertEquals(0, $productionTasks[1]['isAvailable'], 'Task 1: is available is incorrect');
    
            $this->assertEquals(3, $productionTasks[2]['offeredProductId'], 'Task 2: offered product is incorrect');
            $this->assertEquals(21, $productionTasks[2]['stepId'], 'Task 2: step is incorrect');
            $this->assertEquals($productionTasks[1]['id'], $productionTasks[2]['requiredIds'], 'Task 2: requiredIds is incorrect');
            $this->assertEquals($productionTasks[3]['id'], $productionTasks[2]['successorIds'], 'Task 2: successorIds is incorrect');
            $this->assertEquals(0, $productionTasks[2]['isAvailable'], 'Task 2: is available is incorrect');
    
            $this->assertEquals(3, $productionTasks[3]['offeredProductId'], 'Task 3: offered product is incorrect');
            $this->assertEquals(22, $productionTasks[3]['stepId'], 'Task 3: step is incorrect');
            $this->assertEquals($productionTasks[2]['id'], $productionTasks[3]['requiredIds'], 'Task 3: requiredIds is incorrect');
            $this->assertNull($productionTasks[3]['successorIds'], 'Task 3: successorIds is incorrect');
            $this->assertEquals(0, $productionTasks[3]['isAvailable'], 'Task 3: is available is incorrect');
        }
        catch(\Exception $e) {
            $this->fail($e->getMessage());
        }
    }
    
    public function testGenerateTasksForSpecificPathForProductAndOrder() {
        $logic = new ProductionTaskLogic();
        $order = $this->projectHelper->generateOrder(Utility::ORDER_STATUS_IN_PROGRESS);
        $order = $this->projectHelper->assignOfferedProduct($order, 3, [], 2);
    
        $user = User::findOne(2);
        Yii::$app->user->login($user);
    
        $logic->generateTasks($order->id, false);
    
        $productionTasks = ProductionTaskQuery::getByOrderId($order->id);
        try {
            $this->assertEquals(4, count($productionTasks), 'Number of tasks is incorrect');
    
            $this->assertEquals(3, $productionTasks[0]['offeredProductId'], 'Task 0: offered product is incorrect');
            $this->assertEquals(15, $productionTasks[0]['stepId'], 'Task 0: step is incorrect');
            $this->assertNull($productionTasks[0]['requiredIds'], 'Task 0: requiredIds is incorrect');
            $this->assertEquals($productionTasks[1]['id'].','.$productionTasks[2]['id'], $productionTasks[0]['successorIds'], 'Task 0: successorIds is incorrect');
            $this->assertEquals(1, $productionTasks[0]['isAvailable'], 'Task 0: is available is incorrect');
    
            $this->assertEquals(3, $productionTasks[1]['offeredProductId'], 'Task 1: offered product is incorrect');
            $this->assertEquals(16, $productionTasks[1]['stepId'], 'Task 1: step is incorrect');
            $this->assertEquals($productionTasks[0]['id'], $productionTasks[1]['requiredIds'], 'Task 1: requiredIds is incorrect');
            $this->assertEquals($productionTasks[3]['id'], $productionTasks[1]['successorIds'], 'Task 1: successorIds is incorrect');
            $this->assertEquals(0, $productionTasks[1]['isAvailable'], 'Task 1: is available is incorrect');
    
            $this->assertEquals(3, $productionTasks[2]['offeredProductId'], 'Task 2: offered product is incorrect');
            $this->assertEquals(17, $productionTasks[2]['stepId'], 'Task 2: step is incorrect');
            $this->assertEquals($productionTasks[0]['id'], $productionTasks[2]['requiredIds'], 'Task 2: requiredIds is incorrect');
            $this->assertEquals($productionTasks[3]['id'], $productionTasks[2]['successorIds'], 'Task 2: successorIds is incorrect');
            $this->assertEquals(0, $productionTasks[2]['isAvailable'], 'Task 2: is available is incorrect');
    
            $this->assertEquals(3, $productionTasks[3]['offeredProductId'], 'Task 3: offered product is incorrect');
            $this->assertEquals(18, $productionTasks[3]['stepId'], 'Task 3: step is incorrect');
            $this->assertEquals($productionTasks[1]['id'].','.$productionTasks[2]['id'], $productionTasks[3]['requiredIds'], 'Task 3: requiredIds is incorrect');
            $this->assertNull($productionTasks[3]['successorIds'], 'Task 3: successorIds is incorrect');
            $this->assertEquals(0, $productionTasks[3]['isAvailable'], 'Task 3: is available is incorrect');
        }
        catch(\Exception $e) {
            $this->fail($e->getMessage());
        }
    }
           
    public function testGenerateTasksForForcedGeneralPathWithPathForOrder() {
        $logic = new ProductionTaskLogic();
        $order = $this->projectHelper->generateOrder(Utility::ORDER_STATUS_IN_PROGRESS);
        $order = $this->projectHelper->assignOfferedProduct($order, 1, [], 2);
    
        $user = User::findOne(2);
        Yii::$app->user->login($user);
    
        $logic->generateTasks($order->id, true);
    
        $productionTasks = ProductionTaskQuery::getByOrderId($order->id);
        try {
            $this->assertEquals(3, count($productionTasks), 'Number of tasks is incorrect');
    
            $this->assertEquals(1, $productionTasks[0]['offeredProductId'], 'Task 0: offered product is incorrect');
            $this->assertEquals(1, $productionTasks[0]['stepId'], 'Task 0: step is incorrect');
            $this->assertNull($productionTasks[0]['requiredIds'], 'Task 0: requiredIds is incorrect');
            $this->assertEquals($productionTasks[1]['id'], intval($productionTasks[0]['successorIds']), 'Task 0: successorIds is incorrect');
            $this->assertEquals(1, $productionTasks[0]['isAvailable'], 'Task 0: is available is incorrect');
    
            $this->assertEquals(1, $productionTasks[1]['offeredProductId'], 'Task 1: offered product is incorrect');
            $this->assertEquals(7, $productionTasks[1]['stepId'], 'Task 1: step is incorrect');
            $this->assertEquals($productionTasks[0]['id'], intval($productionTasks[1]['requiredIds']), 'Task 1: requiredIds is incorrect');
            $this->assertEquals($productionTasks[2]['id'], intval($productionTasks[1]['successorIds']), 'Task 1: successorIds is incorrect');
            $this->assertEquals(0, $productionTasks[1]['isAvailable'], 'Task 1: is available is incorrect');
    
            $this->assertEquals(1, $productionTasks[2]['offeredProductId'], 'Task 2: offered product is incorrect');
            $this->assertEquals(14, $productionTasks[2]['stepId'], 'Task 2: step is incorrect');
            $this->assertEquals($productionTasks[1]['id'], intval($productionTasks[2]['requiredIds']), 'Task 2: requiredIds is incorrect');
            $this->assertNull($productionTasks[2]['successorIds'], 'Task 2: successorIds is incorrect');
            $this->assertEquals(0, $productionTasks[2]['isAvailable'], 'Task 2: is available is incorrect');
        }
        catch(\Exception $e) {
            $this->fail($e->getMessage());
        }
    }
    
    public function testGenerateTasksForGeneralPathWithEmptyAttributes() {
        $logic = new ProductionTaskLogic();
        $order = $this->projectHelper->generateOrder(Utility::ORDER_STATUS_IN_PROGRESS);
        $order = $this->projectHelper->assignOfferedProduct($order, 1);
        OrderOfferedProduct::updateAll(['attribute_value' => '[{}]'], ['order_id' => $order->id]);
    
        $user = User::findOne(2);
        Yii::$app->user->login($user);
    
        $logic->generateTasks($order->id, true);
    
        $productionTasks = ProductionTaskQuery::getByOrderId($order->id);
        try {
            $this->assertEquals(3, count($productionTasks), 'Number of tasks is incorrect');
    
            $this->assertEquals(1, $productionTasks[0]['offeredProductId'], 'Task 0: offered product is incorrect');
            $this->assertEquals(1, $productionTasks[0]['stepId'], 'Task 0: step is incorrect');
            $this->assertNull($productionTasks[0]['requiredIds'], 'Task 0: requiredIds is incorrect');
            $this->assertEquals($productionTasks[1]['id'], intval($productionTasks[0]['successorIds']), 'Task 0: successorIds is incorrect');
            $this->assertEquals(1, $productionTasks[0]['isAvailable'], 'Task 0: is available is incorrect');
    
            $this->assertEquals(1, $productionTasks[1]['offeredProductId'], 'Task 1: offered product is incorrect');
            $this->assertEquals(7, $productionTasks[1]['stepId'], 'Task 1: step is incorrect');
            $this->assertEquals($productionTasks[0]['id'], intval($productionTasks[1]['requiredIds']), 'Task 1: requiredIds is incorrect');
            $this->assertEquals($productionTasks[2]['id'], intval($productionTasks[1]['successorIds']), 'Task 1: successorIds is incorrect');
            $this->assertEquals(0, $productionTasks[1]['isAvailable'], 'Task 1: is available is incorrect');
    
            $this->assertEquals(1, $productionTasks[2]['offeredProductId'], 'Task 2: offered product is incorrect');
            $this->assertEquals(14, $productionTasks[2]['stepId'], 'Task 2: step is incorrect');
            $this->assertEquals($productionTasks[1]['id'], intval($productionTasks[2]['requiredIds']), 'Task 2: requiredIds is incorrect');
            $this->assertNull($productionTasks[2]['successorIds'], 'Task 2: successorIds is incorrect');
            $this->assertEquals(0, $productionTasks[2]['isAvailable'], 'Task 2: is available is incorrect');
        }
        catch(\Exception $e) {
            $this->fail($e->getMessage());
        }
    }
    
    public function testGenerateTasksForSpecificPathWithDependenciesAndNoAttributes() {
        $logic = new ProductionTaskLogic();
        $order = $this->projectHelper->generateOrder(Utility::ORDER_STATUS_IN_PROGRESS);
        $order = $this->projectHelper->assignOfferedProduct($order, 4, []);
    
        $user = User::findOne(2);
        Yii::$app->user->login($user);
    
        $logic->generateTasks($order->id, false);
    
        $productionTasks = ProductionTaskQuery::getByOrderId($order->id);
        try {
            $this->assertEquals(1, count($productionTasks), 'Number of tasks is incorrect');
    
            $this->assertEquals(4, $productionTasks[0]['offeredProductId'], 'Task 0: offered product is incorrect');
            $this->assertEquals(30, $productionTasks[0]['stepId'], 'Task 0: step is incorrect');
            $this->assertNull($productionTasks[0]['requiredIds'], 'Task 0: requiredIds is incorrect');
            $this->assertNull($productionTasks[0]['successorIds'], 'Task 0: successorIds is incorrect');
            $this->assertEquals(1, $productionTasks[0]['isAvailable'], 'Task 0: is available is incorrect');    
        }
        catch(\Exception $e) {
            $this->fail($e->getMessage());
        }
    }
    
    public function testGenerateTasksForSpecificPathWithDependenciesAndFirstAttribute() {
        $logic = new ProductionTaskLogic();
        $order = $this->projectHelper->generateOrder(Utility::ORDER_STATUS_IN_PROGRESS);
        $order = $this->projectHelper->assignOfferedProduct($order, 4, [['id' => 1]]);
    
        $user = User::findOne(2);
        Yii::$app->user->login($user);
    
        $logic->generateTasks($order->id, false);
    
        $productionTasks = ProductionTaskQuery::getByOrderId($order->id);
        try {
            $this->assertEquals(2, count($productionTasks), 'Number of tasks is incorrect');
    
            $this->assertEquals(4, $productionTasks[0]['offeredProductId'], 'Task 0: offered product is incorrect');
            $this->assertEquals(30, $productionTasks[0]['stepId'], 'Task 0: step is incorrect');
            $this->assertNull($productionTasks[0]['requiredIds'], 'Task 0: requiredIds is incorrect');
            $this->assertEquals($productionTasks[1]['id'], $productionTasks[0]['successorIds'], 'Task 0: successorIds is incorrect');
            $this->assertEquals(1, $productionTasks[0]['isAvailable'], 'Task 0: is available is incorrect');
            
            $this->assertEquals(4, $productionTasks[1]['offeredProductId'], 'Task 1: offered product is incorrect');
            $this->assertEquals(31, $productionTasks[1]['stepId'], 'Task 1: step is incorrect');
            $this->assertEquals($productionTasks[0]['id'], $productionTasks[1]['requiredIds'], 'Task 1: requiredIds is incorrect');
            $this->assertNull($productionTasks[1]['successorIds'], 'Task 1: successorIds is incorrect');
            $this->assertEquals(0, $productionTasks[1]['isAvailable'], 'Task 1: is available is incorrect');
        }
        catch(\Exception $e) {
            $this->fail($e->getMessage());
        }
    }
    
    public function testGenerateTasksForSpecificPathWithDependenciesAndSecondAttribute() {
        $logic = new ProductionTaskLogic();
        $order = $this->projectHelper->generateOrder(Utility::ORDER_STATUS_IN_PROGRESS);
        $order = $this->projectHelper->assignOfferedProduct($order, 4, [['id' => 2]]);
    
        $user = User::findOne(2);
        Yii::$app->user->login($user);
    
        $logic->generateTasks($order->id, false);
    
        $productionTasks = ProductionTaskQuery::getByOrderId($order->id);
        try {
            $this->assertEquals(2, count($productionTasks), 'Number of tasks is incorrect');
    
            $this->assertEquals(4, $productionTasks[0]['offeredProductId'], 'Task 0: offered product is incorrect');
            $this->assertEquals(30, $productionTasks[0]['stepId'], 'Task 0: step is incorrect');
            $this->assertNull($productionTasks[0]['requiredIds'], 'Task 0: requiredIds is incorrect');
            $this->assertEquals($productionTasks[1]['id'], $productionTasks[0]['successorIds'], 'Task 0: successorIds is incorrect');
            $this->assertEquals(1, $productionTasks[0]['isAvailable'], 'Task 0: is available is incorrect');
    
            $this->assertEquals(4, $productionTasks[1]['offeredProductId'], 'Task 1: offered product is incorrect');
            $this->assertEquals(34, $productionTasks[1]['stepId'], 'Task 1: step is incorrect');
            $this->assertEquals($productionTasks[0]['id'], $productionTasks[1]['requiredIds'], 'Task 1: requiredIds is incorrect');
            $this->assertNull($productionTasks[1]['successorIds'], 'Task 1: successorIds is incorrect');
            $this->assertEquals(0, $productionTasks[1]['isAvailable'], 'Task 1: is available is incorrect');
        }
        catch(\Exception $e) {
            $this->fail($e->getMessage());
        }
    }
    
    public function testGenerateTasksForSpecificPathWithDependenciesAndThirdAttribute() {
        $logic = new ProductionTaskLogic();
        $order = $this->projectHelper->generateOrder(Utility::ORDER_STATUS_IN_PROGRESS);
        $order = $this->projectHelper->assignOfferedProduct($order, 4, [['id' => 3]]);
    
        $user = User::findOne(2);
        Yii::$app->user->login($user);
    
        $logic->generateTasks($order->id, false);
    
        $productionTasks = ProductionTaskQuery::getByOrderId($order->id);
        try {
            $this->assertEquals(3, count($productionTasks), 'Number of tasks is incorrect');
    
            $this->assertEquals(4, $productionTasks[0]['offeredProductId'], 'Task 0: offered product is incorrect');
            $this->assertEquals(30, $productionTasks[0]['stepId'], 'Task 0: step is incorrect');
            $this->assertNull($productionTasks[0]['requiredIds'], 'Task 0: requiredIds is incorrect');
            $this->assertEquals($productionTasks[1]['id'], $productionTasks[0]['successorIds'], 'Task 0: successorIds is incorrect');
            $this->assertEquals(1, $productionTasks[0]['isAvailable'], 'Task 0: is available is incorrect');
            
            $this->assertEquals(4, $productionTasks[1]['offeredProductId'], 'Task 1: offered product is incorrect');
            $this->assertEquals(31, $productionTasks[1]['stepId'], 'Task 1: step is incorrect');
            $this->assertEquals($productionTasks[0]['id'], $productionTasks[1]['requiredIds'], 'Task 1: requiredIds is incorrect');
            $this->assertEquals($productionTasks[2]['id'], $productionTasks[1]['successorIds'], 'Task 1: successorIds is incorrect');
            $this->assertEquals(0, $productionTasks[1]['isAvailable'], 'Task 1: is available is incorrect');
    
            $this->assertEquals(4, $productionTasks[2]['offeredProductId'], 'Task 2: offered product is incorrect');
            $this->assertEquals(33, $productionTasks[2]['stepId'], 'Task 2: step is incorrect');
            $this->assertEquals($productionTasks[1]['id'], $productionTasks[2]['requiredIds'], 'Task 2: requiredIds is incorrect');
            $this->assertNull($productionTasks[2]['successorIds'], 'Task 2: successorIds is incorrect');
            $this->assertEquals(0, $productionTasks[2]['isAvailable'], 'Task 2: is available is incorrect');
        }
        catch(\Exception $e) {
            $this->fail($e->getMessage());
        }
    }
    
    public function testGenerateTasksForSpecificPathWithDependenciesAndFirstAndSecondAttribute() {
        $logic = new ProductionTaskLogic();
        $order = $this->projectHelper->generateOrder(Utility::ORDER_STATUS_IN_PROGRESS);
        $order = $this->projectHelper->assignOfferedProduct($order, 4, [['id' => 1], ['id' => 2]]);
    
        $user = User::findOne(2);
        Yii::$app->user->login($user);
    
        $logic->generateTasks($order->id, false);
    
        $productionTasks = ProductionTaskQuery::getByOrderId($order->id);
        try {
            $this->assertEquals(2, count($productionTasks), 'Number of tasks is incorrect');
    
            $this->assertEquals(4, $productionTasks[0]['offeredProductId'], 'Task 0: offered product is incorrect');
            $this->assertEquals(30, $productionTasks[0]['stepId'], 'Task 0: step is incorrect');
            $this->assertNull($productionTasks[0]['requiredIds'], 'Task 0: requiredIds is incorrect');
            $this->assertEquals($productionTasks[1]['id'], $productionTasks[0]['successorIds'], 'Task 0: successorIds is incorrect');
            $this->assertEquals(1, $productionTasks[0]['isAvailable'], 'Task 0: is available is incorrect');
    
            $this->assertEquals(4, $productionTasks[1]['offeredProductId'], 'Task 1: offered product is incorrect');
            $this->assertEquals(34, $productionTasks[1]['stepId'], 'Task 1: step is incorrect');
            $this->assertEquals($productionTasks[0]['id'], $productionTasks[1]['requiredIds'], 'Task 1: requiredIds is incorrect');
            $this->assertNull($productionTasks[1]['successorIds'], 'Task 1: successorIds is incorrect');
            $this->assertEquals(0, $productionTasks[1]['isAvailable'], 'Task 1: is available is incorrect');
        }
        catch(\Exception $e) {
            $this->fail($e->getMessage());
        }
    }
    
    public function testGenerateTasksForSpecificPathWithDependenciesAndSecondAndThirdAttribute() {
        $logic = new ProductionTaskLogic();
        $order = $this->projectHelper->generateOrder(Utility::ORDER_STATUS_IN_PROGRESS);
        $order = $this->projectHelper->assignOfferedProduct($order, 4, [['id' => 2], ['id' => 3]]);
    
        $user = User::findOne(2);
        Yii::$app->user->login($user);
    
        $logic->generateTasks($order->id, false);
    
        $productionTasks = ProductionTaskQuery::getByOrderId($order->id);
        try {
            $this->assertEquals(3, count($productionTasks), 'Number of tasks is incorrect');
    
            $this->assertEquals(4, $productionTasks[0]['offeredProductId'], 'Task 0: offered product is incorrect');
            $this->assertEquals(30, $productionTasks[0]['stepId'], 'Task 0: step is incorrect');
            $this->assertNull($productionTasks[0]['requiredIds'], 'Task 0: requiredIds is incorrect');
            $this->assertEquals($productionTasks[1]['id'], $productionTasks[0]['successorIds'], 'Task 0: successorIds is incorrect');
            $this->assertEquals(1, $productionTasks[0]['isAvailable'], 'Task 0: is available is incorrect');
    
            $this->assertEquals(4, $productionTasks[1]['offeredProductId'], 'Task 1: offered product is incorrect');
            $this->assertEquals(32, $productionTasks[1]['stepId'], 'Task 1: step is incorrect');
            $this->assertEquals($productionTasks[0]['id'], $productionTasks[1]['requiredIds'], 'Task 1: requiredIds is incorrect');
            $this->assertEquals($productionTasks[2]['id'], $productionTasks[1]['successorIds'], 'Task 1: successorIds is incorrect');
            $this->assertEquals(0, $productionTasks[1]['isAvailable'], 'Task 1: is available is incorrect');
    
            $this->assertEquals(4, $productionTasks[2]['offeredProductId'], 'Task 2: offered product is incorrect');
            $this->assertEquals(33, $productionTasks[2]['stepId'], 'Task 2: step is incorrect');
            $this->assertEquals($productionTasks[1]['id'], $productionTasks[2]['requiredIds'], 'Task 2: requiredIds is incorrect');
            $this->assertNull($productionTasks[2]['successorIds'], 'Task 2: successorIds is incorrect');
            $this->assertEquals(0, $productionTasks[2]['isAvailable'], 'Task 2: is available is incorrect');
        }
        catch(\Exception $e) {
            $this->fail($e->getMessage());
        }
    }
    
    public function testGenerateTasksForSpecificPathWithDependenciesAndFirstAndThirdAttribute() {
        $logic = new ProductionTaskLogic();
        $order = $this->projectHelper->generateOrder(Utility::ORDER_STATUS_IN_PROGRESS);
        $order = $this->projectHelper->assignOfferedProduct($order, 4, [['id' => 1], ['id' => 3]]);
    
        $user = User::findOne(2);
        Yii::$app->user->login($user);
    
        $logic->generateTasks($order->id, false);
    
        $productionTasks = ProductionTaskQuery::getByOrderId($order->id);
        try {
            $this->assertEquals(3, count($productionTasks), 'Number of tasks is incorrect');
    
            $this->assertEquals(4, $productionTasks[0]['offeredProductId'], 'Task 0: offered product is incorrect');
            $this->assertEquals(30, $productionTasks[0]['stepId'], 'Task 0: step is incorrect');
            $this->assertNull($productionTasks[0]['requiredIds'], 'Task 0: requiredIds is incorrect');
            $this->assertEquals($productionTasks[1]['id'], $productionTasks[0]['successorIds'], 'Task 0: successorIds is incorrect');
            $this->assertEquals(1, $productionTasks[0]['isAvailable'], 'Task 0: is available is incorrect');
    
            $this->assertEquals(4, $productionTasks[1]['offeredProductId'], 'Task 1: offered product is incorrect');
            $this->assertEquals(31, $productionTasks[1]['stepId'], 'Task 1: step is incorrect');
            $this->assertEquals($productionTasks[0]['id'], $productionTasks[1]['requiredIds'], 'Task 1: requiredIds is incorrect');
            $this->assertEquals($productionTasks[2]['id'], $productionTasks[1]['successorIds'], 'Task 1: successorIds is incorrect');
            $this->assertEquals(0, $productionTasks[1]['isAvailable'], 'Task 1: is available is incorrect');
    
            $this->assertEquals(4, $productionTasks[2]['offeredProductId'], 'Task 2: offered product is incorrect');
            $this->assertEquals(33, $productionTasks[2]['stepId'], 'Task 2: step is incorrect');
            $this->assertEquals($productionTasks[1]['id'], $productionTasks[2]['requiredIds'], 'Task 2: requiredIds is incorrect');
            $this->assertNull($productionTasks[2]['successorIds'], 'Task 2: successorIds is incorrect');
            $this->assertEquals(0, $productionTasks[2]['isAvailable'], 'Task 2: is available is incorrect');
        }
        catch(\Exception $e) {
            $this->fail($e->getMessage());
        }
    }
    
    public function testGenerateTasksForSpecificPathWithDependenciesAndAllAttributes() {
        $logic = new ProductionTaskLogic();
        $order = $this->projectHelper->generateOrder(Utility::ORDER_STATUS_IN_PROGRESS);
        $order = $this->projectHelper->assignOfferedProduct($order, 4, [['id' => 1], ['id' => 2], ['id' => 3]]);
    
        $user = User::findOne(2);
        Yii::$app->user->login($user);
    
        $logic->generateTasks($order->id, false);
    
        $productionTasks = ProductionTaskQuery::getByOrderId($order->id);
        try {
            $this->assertEquals(3, count($productionTasks), 'Number of tasks is incorrect');
    
            $this->assertEquals(4, $productionTasks[0]['offeredProductId'], 'Task 0: offered product is incorrect');
            $this->assertEquals(30, $productionTasks[0]['stepId'], 'Task 0: step is incorrect');
            $this->assertNull($productionTasks[0]['requiredIds'], 'Task 0: requiredIds is incorrect');
            $this->assertEquals($productionTasks[1]['id'], $productionTasks[0]['successorIds'], 'Task 0: successorIds is incorrect');
            $this->assertEquals(1, $productionTasks[0]['isAvailable'], 'Task 0: is available is incorrect');
    
            $this->assertEquals(4, $productionTasks[1]['offeredProductId'], 'Task 1: offered product is incorrect');
            $this->assertEquals(32, $productionTasks[1]['stepId'], 'Task 1: step is incorrect');
            $this->assertEquals($productionTasks[0]['id'], $productionTasks[1]['requiredIds'], 'Task 1: requiredIds is incorrect');
            $this->assertEquals($productionTasks[2]['id'], $productionTasks[1]['successorIds'], 'Task 1: successorIds is incorrect');
            $this->assertEquals(0, $productionTasks[1]['isAvailable'], 'Task 1: is available is incorrect');
    
            $this->assertEquals(4, $productionTasks[2]['offeredProductId'], 'Task 2: offered product is incorrect');
            $this->assertEquals(33, $productionTasks[2]['stepId'], 'Task 2: step is incorrect');
            $this->assertEquals($productionTasks[1]['id'], $productionTasks[2]['requiredIds'], 'Task 2: requiredIds is incorrect');
            $this->assertNull($productionTasks[2]['successorIds'], 'Task 2: successorIds is incorrect');
            $this->assertEquals(0, $productionTasks[2]['isAvailable'], 'Task 2: is available is incorrect');
        }
        catch(\Exception $e) {
            $this->fail($e->getMessage());
        }
    }
    
    public function testGenerateTasksForSpecificPathWithManyStartingPoints() {
        $logic = new ProductionTaskLogic();
        $order = $this->projectHelper->generateOrder(Utility::ORDER_STATUS_IN_PROGRESS);
        $order = $this->projectHelper->assignOfferedProduct($order, 5);
    
        $user = User::findOne(2);
        Yii::$app->user->login($user);
    
        $logic->generateTasks($order->id, false);
    
        $productionTasks = ProductionTaskQuery::getByOrderId($order->id);
        try {
            $this->assertEquals(7, count($productionTasks), 'Number of tasks is incorrect');
    
            $this->assertEquals(5, $productionTasks[0]['offeredProductId'], 'Task 0: offered product is incorrect');
            $this->assertEquals(35, $productionTasks[0]['stepId'], 'Task 0: step is incorrect');
            $this->assertNull($productionTasks[0]['requiredIds'], 'Task 0: requiredIds is incorrect');
            $this->assertEquals($productionTasks[3]['id'], $productionTasks[0]['successorIds'], 'Task 0: successorIds is incorrect');
            $this->assertEquals(1, $productionTasks[0]['isAvailable'], 'Task 0: is available is incorrect');
    
            $this->assertEquals(5, $productionTasks[1]['offeredProductId'], 'Task 1: offered product is incorrect');
            $this->assertEquals(36, $productionTasks[1]['stepId'], 'Task 1: step is incorrect');
            $this->assertNull($productionTasks[1]['requiredIds'], 'Task 1: requiredIds is incorrect');
            $this->assertEquals($productionTasks[3]['id'], $productionTasks[1]['successorIds'], 'Task 1: successorIds is incorrect');
            $this->assertEquals(1, $productionTasks[1]['isAvailable'], 'Task 1: is available is incorrect');
            
            $this->assertEquals(5, $productionTasks[2]['offeredProductId'], 'Task 2: offered product is incorrect');
            $this->assertEquals(37, $productionTasks[2]['stepId'], 'Task 2: step is incorrect');
            $this->assertNull($productionTasks[2]['requiredIds'], 'Task 2: requiredIds is incorrect');
            $this->assertEquals($productionTasks[6]['id'], $productionTasks[2]['successorIds'], 'Task 2: successorIds is incorrect');
            $this->assertEquals(1, $productionTasks[2]['isAvailable'], 'Task 2: is available is incorrect');
            
            $this->assertEquals(5, $productionTasks[3]['offeredProductId'], 'Task 3: offered product is incorrect');
            $this->assertEquals(38, $productionTasks[3]['stepId'], 'Task 3: step is incorrect');
            $this->assertEquals($productionTasks[0]['id'].','.$productionTasks[1]['id'], $productionTasks[3]['requiredIds'], 'Task 3: requiredIds is incorrect');
            $this->assertEquals($productionTasks[4]['id'].','.$productionTasks[5]['id'], $productionTasks[3]['successorIds'], 'Task 3: successorIds is incorrect');
            $this->assertEquals(0, $productionTasks[3]['isAvailable'], 'Task 3: is available is incorrect');
            
            $this->assertEquals(5, $productionTasks[4]['offeredProductId'], 'Task 4: offered product is incorrect');
            $this->assertEquals(39, $productionTasks[4]['stepId'], 'Task 4: step is incorrect');
            $this->assertEquals($productionTasks[3]['id'], $productionTasks[4]['requiredIds'], 'Task 4: requiredIds is incorrect');
            $this->assertEquals($productionTasks[6]['id'], $productionTasks[4]['successorIds'], 'Task 4: successorIds is incorrect');
            $this->assertEquals(0, $productionTasks[4]['isAvailable'], 'Task 4: is available is incorrect');
            
            $this->assertEquals(5, $productionTasks[5]['offeredProductId'], 'Task 5: offered product is incorrect');
            $this->assertEquals(40, $productionTasks[5]['stepId'], 'Task 5: step is incorrect');
            $this->assertEquals($productionTasks[3]['id'], $productionTasks[5]['requiredIds'], 'Task 5: requiredIds is incorrect');
            $this->assertEquals($productionTasks[6]['id'], $productionTasks[5]['successorIds'], 'Task 5: successorIds is incorrect');
            $this->assertEquals(0, $productionTasks[5]['isAvailable'], 'Task 5: is available is incorrect');
            
            $this->assertEquals(5, $productionTasks[6]['offeredProductId'], 'Task 6: offered product is incorrect');
            $this->assertEquals(41, $productionTasks[6]['stepId'], 'Task 6: step is incorrect');
            $this->assertEquals($productionTasks[2]['id'].','.$productionTasks[4]['id'].','.$productionTasks[5]['id'], $productionTasks[6]['requiredIds'], 'Task 6: requiredIds is incorrect');
            $this->assertNull($productionTasks[6]['successorIds'], 'Task 6: successorIds is incorrect');
            $this->assertEquals(0, $productionTasks[6]['isAvailable'], 'Task 6: is available is incorrect');
        }
        catch(\Exception $e) {
            $this->fail($e->getMessage());
        }
    }
    
    public function testGenerateTasksForSpecificPathWithManyEndPointsWithoutAttributes() {
        $logic = new ProductionTaskLogic();
        $order = $this->projectHelper->generateOrder(Utility::ORDER_STATUS_IN_PROGRESS);
        $order = $this->projectHelper->assignOfferedProduct($order, 6);
    
        $user = User::findOne(2);
        Yii::$app->user->login($user);
    
        $logic->generateTasks($order->id, false);
    
        $productionTasks = ProductionTaskQuery::getByOrderId($order->id);
        try {
            $this->assertEquals(3, count($productionTasks), 'Number of tasks is incorrect');
    
            $this->assertEquals(6, $productionTasks[0]['offeredProductId'], 'Task 0: offered product is incorrect');
            $this->assertEquals(42, $productionTasks[0]['stepId'], 'Task 0: step is incorrect');
            $this->assertNull($productionTasks[0]['requiredIds'], 'Task 0: requiredIds is incorrect');
            $this->assertEquals($productionTasks[1]['id'], $productionTasks[0]['successorIds'], 'Task 0: successorIds is incorrect');
            $this->assertEquals(1, $productionTasks[0]['isAvailable'], 'Task 0: is available is incorrect');
    
            $this->assertEquals(6, $productionTasks[1]['offeredProductId'], 'Task 1: offered product is incorrect');
            $this->assertEquals(43, $productionTasks[1]['stepId'], 'Task 1: step is incorrect');
            $this->assertEquals($productionTasks[0]['id'], $productionTasks[1]['requiredIds'], 'Task 1: requiredIds is incorrect');
            $this->assertEquals($productionTasks[2]['id'], $productionTasks[1]['successorIds'], 'Task 1: successorIds is incorrect');
            $this->assertEquals(0, $productionTasks[1]['isAvailable'], 'Task 1: is available is incorrect');
    
            $this->assertEquals(6, $productionTasks[2]['offeredProductId'], 'Task 2: offered product is incorrect');
            $this->assertEquals(44, $productionTasks[2]['stepId'], 'Task 2: step is incorrect');
            $this->assertEquals($productionTasks[1]['id'], $productionTasks[2]['requiredIds'], 'Task 2: requiredIds is incorrect');
            $this->assertNull($productionTasks[2]['successorIds'], 'Task 2: successorIds is incorrect');
            $this->assertEquals(0, $productionTasks[2]['isAvailable'], 'Task 2: is available is incorrect');
        }
        catch(\Exception $e) {
            $this->fail($e->getMessage());
        }
    }
    
    public function testGenerateTasksForSpecificPathWithManyEndPointsWithAttributeOnEndPoint() {
        $logic = new ProductionTaskLogic();
        $order = $this->projectHelper->generateOrder(Utility::ORDER_STATUS_IN_PROGRESS);
        $order = $this->projectHelper->assignOfferedProduct($order, 6, [['id' => 1]]);
    
        $user = User::findOne(2);
        Yii::$app->user->login($user);
    
        $logic->generateTasks($order->id, false);
    
        $productionTasks = ProductionTaskQuery::getByOrderId($order->id);
        try {
            $this->assertEquals(4, count($productionTasks), 'Number of tasks is incorrect');
    
            $this->assertEquals(6, $productionTasks[0]['offeredProductId'], 'Task 0: offered product is incorrect');
            $this->assertEquals(42, $productionTasks[0]['stepId'], 'Task 0: step is incorrect');
            $this->assertNull($productionTasks[0]['requiredIds'], 'Task 0: requiredIds is incorrect');
            $this->assertEquals($productionTasks[1]['id'], $productionTasks[0]['successorIds'], 'Task 0: successorIds is incorrect');
            $this->assertEquals(1, $productionTasks[0]['isAvailable'], 'Task 0: is available is incorrect');
    
            $this->assertEquals(6, $productionTasks[1]['offeredProductId'], 'Task 1: offered product is incorrect');
            $this->assertEquals(43, $productionTasks[1]['stepId'], 'Task 1: step is incorrect');
            $this->assertEquals($productionTasks[0]['id'], $productionTasks[1]['requiredIds'], 'Task 1: requiredIds is incorrect');
            $this->assertEquals($productionTasks[2]['id'].','.$productionTasks[3]['id'], $productionTasks[1]['successorIds'], 'Task 1: successorIds is incorrect');
            $this->assertEquals(0, $productionTasks[1]['isAvailable'], 'Task 1: is available is incorrect');
    
            $this->assertEquals(6, $productionTasks[2]['offeredProductId'], 'Task 2: offered product is incorrect');
            $this->assertEquals(44, $productionTasks[2]['stepId'], 'Task 2: step is incorrect');
            $this->assertEquals($productionTasks[1]['id'], $productionTasks[2]['requiredIds'], 'Task 2: requiredIds is incorrect');
            $this->assertNull($productionTasks[2]['successorIds'], 'Task 2: successorIds is incorrect');
            $this->assertEquals(0, $productionTasks[2]['isAvailable'], 'Task 2: is available is incorrect');
            
            $this->assertEquals(6, $productionTasks[3]['offeredProductId'], 'Task 3: offered product is incorrect');
            $this->assertEquals(45, $productionTasks[3]['stepId'], 'Task 3: step is incorrect');
            $this->assertEquals($productionTasks[1]['id'], $productionTasks[3]['requiredIds'], 'Task 3: requiredIds is incorrect');
            $this->assertNull($productionTasks[3]['successorIds'], 'Task 3: successorIds is incorrect');
            $this->assertEquals(0, $productionTasks[3]['isAvailable'], 'Task 3: is available is incorrect');
        }
        catch(\Exception $e) {
            $this->fail($e->getMessage());
        }
    }
    
    public function testGetPathDataForDefaultPath() {
        $logic = new ProductionTaskLogic();
        $method = $this->helper->getMethod('frontend\logic\ProductionTaskLogic', 'getPathData');
        $actual = $method->invokeArgs($logic, []);        
        
        $this->assertEquals([
                'path' => [
                	'id' => 1,
                	'topologicalOrder' => '1,2,3,4,5,6,7,8,9,10,11,12,13,14',
                ],
                'steps' => [
                	1 => [
                		'id' => 1,
                		'activityId' => 1,
                		'requiredIds' => '',
                		'successorIds' => '2',
                		'activityName' => 'Czynność A',
                	    'attributeNeededIds' => [],
                	    'attributeForbiddenIds' => [],
                	],
                	2 => [
                		'id' => 2,
                		'activityId' => 2,
                		'requiredIds' => '1',
                		'successorIds' => '3',
                		'activityName' => 'Czynność B',
                		'attributes' => [ 1 ],
                	    'attributeNeededIds' => [],
                	    'attributeForbiddenIds' => [],
                	],
                	3 => [
                		'id' => 3,
                		'activityId' => 3,
                		'requiredIds' => '2',
                		'successorIds' => '4,5,6',
                		'activityName' => 'Czynność C',
                		'attributes' => [ 2 ],
                	    'attributeNeededIds' => [],
                	    'attributeForbiddenIds' => [],
                	],
                	4 => [
                		'id' => 4,
                		'activityId' => 4,
                		'requiredIds' => '3',
                		'successorIds' => '7',
                		'activityName' => 'Czynność D',
                		'attributes' => [ 3 ],
                	    'attributeNeededIds' => [],
                	    'attributeForbiddenIds' => [],
                	],
                	5 => [
                		'id' => 5,
                		'activityId' => 5,
                		'requiredIds' => '3',
                		'successorIds' => '7',
                		'activityName' => 'Czynność E',
                		'attributes' => [ 4 ],
                	    'attributeNeededIds' => [],
                	    'attributeForbiddenIds' => [],
                	],
                	6 => [
                		'id' => 6,
                		'activityId' => 6,
                		'requiredIds' => '3',
                		'successorIds' => '7',
                		'activityName' => 'Czynność F',
                		'attributes' => [ 5 ],
                	    'attributeNeededIds' => [],
                	    'attributeForbiddenIds' => [],
                	],
                	7 => [
                		'id' => 7,
                		'activityId' => 7,
                		'requiredIds' => '4,5,6',
                		'successorIds' => '8,9',
                		'activityName' => 'Czynność G',
                	    'attributeNeededIds' => [],
                	    'attributeForbiddenIds' => [],
                	],
                	8 => [
                		'id' => 8,
                		'activityId' => 8,
                		'requiredIds' => '7',
                		'successorIds' => '11',
                		'activityName' => 'Czynność H',
                		'attributes' => [ 6 ],
                	    'attributeNeededIds' => [],
                	    'attributeForbiddenIds' => [],
                	],
                	9 => [
                		'id' => 9,
                		'activityId' => 9,
                		'requiredIds' => '7',
                		'successorIds' => '10',
                		'activityName' => 'Czynność I',
                		'attributes' => [ 7 ],
                	    'attributeNeededIds' => [],
                	    'attributeForbiddenIds' => [],
                	],
                	10 => [
                		'id' => 10,
                		'activityId' => 10,
                		'requiredIds' => '9',
                		'successorIds' => '11',
                		'activityName' => 'Czynność J',
                		'attributes' => [ 7 ],
                	    'attributeNeededIds' => [],
                	    'attributeForbiddenIds' => [],
                	],
                	11 => [
                		'id' => 11,
                		'activityId' => 11,
                		'requiredIds' => '8,10',
                		'successorIds' => '12,13',
                		'activityName' => 'Czynność K',
                		'attributes' => [ 1, 8 ],
                	    'attributeNeededIds' => [],
                	    'attributeForbiddenIds' => [],
                	],
                	12 => [
                		'id' => 12,
                		'activityId' => 12,
                		'requiredIds' => '11',
                		'successorIds' => '13',
                		'activityName' => 'Czynność M',
                		'attributes' => [ 9 ],
                	    'attributeNeededIds' => [],
                	    'attributeForbiddenIds' => [],
                	],
                	13 => [
                		'id' => 13,
                		'activityId' => 13,
                		'requiredIds' => '11,12',
                		'successorIds' => '14',
                		'activityName' => 'Czynność L',
                		'attributes' => [ 10 ],
                	    'attributeNeededIds' => [],
                	    'attributeForbiddenIds' => [],
                	],
                	14 => [
                		'id' => 14,
                		'activityId' => 14,
                		'requiredIds' => '13',
                		'successorIds' => '',
                		'activityName' => 'Czynność N',
                	    'attributeNeededIds' => [],
                	    'attributeForbiddenIds' => [],
                	],
                ]], $actual);        
    }
    
    public function testGetPathDataForSpecificPath() {
        $logic = new ProductionTaskLogic();
        $method = $this->helper->getMethod('frontend\logic\ProductionTaskLogic', 'getPathData');
        $actual = $method->invokeArgs($logic, [2]);
    
        $this->assertEquals([
        	'path' => [
        		'id' => 2,
        		'topologicalOrder' => '15,16,17,18',
        	],
        	'steps' => [
        		15 => [
        			'id' => 15,
        			'activityId' => 1,
        			'requiredIds' => '',
        			'successorIds' => '16,17',
        			'activityName' => 'Czynność A',
        		    'attributeNeededIds' => [],
        		    'attributeForbiddenIds' => [],
        		],
        		16 => [
        			'id' => 16,
        			'activityId' => 2,
        			'requiredIds' => '15',
        			'successorIds' => '18',
        			'activityName' => 'Czynność B',
        		    'attributeNeededIds' => [],
        		    'attributeForbiddenIds' => [],
        		],
        		17 => [
        			'id' => 17,
        			'activityId' => 3,
        			'requiredIds' => '15',
        			'successorIds' => '18',
        			'activityName' => 'Czynność C',
        		    'attributeNeededIds' => [],
        		    'attributeForbiddenIds' => [],
        		],
        		18 => [
        			'id' => 18,
        			'activityId' => 4,
        			'requiredIds' => '16,17',
        			'successorIds' => '',
        			'activityName' => 'Czynność D',
        		    'attributeNeededIds' => [],
        		    'attributeForbiddenIds' => [],
        		],
        	]
        ], $actual);
    }       
    
    public function testGetActuallyRequiredStepsForPathWithoutStartingNode() {
        $logic = new ProductionTaskLogic();
        $method = $this->helper->getMethod('frontend\logic\ProductionTaskLogic', 'getActuallyRequiredSteps');
        $actual = $method->invokeArgs($logic, [2, [2, 3], [
            'steps' => [
                1 => ['requiredIds' => null],
                2 => ['requiredIds' => '1'],
                3 => ['requiredIds' => '2'],
            ],
        ]]);
        $actual = array_unique($actual);
    
        try {
            $this->assertEquals(0, count($actual), 'Number of elements is incorrect');
        }
        catch(\Exception $e) {
            $this->fail($e->getMessage());
        }
    }
    
    public function testGetActuallySuccessorStepsForPathWithoutEndNode() {
        $logic = new ProductionTaskLogic();
        $method = $this->helper->getMethod('frontend\logic\ProductionTaskLogic', 'getActuallySuccessorSteps');
        $actual = $method->invokeArgs($logic, [2, [1, 2], [
            'steps' => [
                1 => ['successorIds' => '2'],
                2 => ['successorIds' => '3'],
                3 => ['successorIds' => null],
            ],
        ]]);
        $actual = array_unique($actual);
    
        try {
            $this->assertEquals(0, count($actual), 'Number of elements is incorrect');
        }
        catch(\Exception $e) {
            $this->fail($e->getMessage());
        }
    }
    
    public function testGetActuallyRequiredStepsForMinimalPathStartingNode() {
        $logic = new ProductionTaskLogic();
        $method = $this->helper->getMethod('frontend\logic\ProductionTaskLogic', 'getActuallyRequiredSteps');
        $actual = $method->invokeArgs($logic, [23, [23, 24, 29], $this->getProductionPath(4)]);
        $actual = array_unique($actual);
        
        try {
            $this->assertEquals(0, count($actual), 'Number of elements is incorrect');
        }
        catch(\Exception $e) {
            $this->fail($e->getMessage());
        }
    }
    
    public function testGetActuallyRequiredStepsForMinimalPathMiddleNode() {
        $logic = new ProductionTaskLogic();
        $method = $this->helper->getMethod('frontend\logic\ProductionTaskLogic', 'getActuallyRequiredSteps');
        $actual = $method->invokeArgs($logic, [24, [23, 24, 29], $this->getProductionPath(4)]);
        $actual = array_unique($actual);
        
        try {
            $this->assertEquals(1, count($actual), 'Number of elements is incorrect');
            $this->assertEquals(23, $actual[0], 'Element 0 is incorrect');
        }
        catch(\Exception $e) {
            $this->fail($e->getMessage());
        }
    }
    
    public function testGetActuallyRequiredStepsForMinimalPathEndNode() {
        $logic = new ProductionTaskLogic();
        $method = $this->helper->getMethod('frontend\logic\ProductionTaskLogic', 'getActuallyRequiredSteps');
        $actual = $method->invokeArgs($logic, [29, [23, 24, 29], $this->getProductionPath(4)]);
        $actual = array_unique($actual);
        
        try {
            $this->assertEquals(1, count($actual), 'Number of elements is incorrect');
            $this->assertEquals(24, $actual[0], 'Element 0 is incorrect');
        }
        catch(\Exception $e) {
            $this->fail($e->getMessage());
        }
    }
    
    public function testGetActuallySuccessorStepsForMinimalPathStartingNode() {
        $logic = new ProductionTaskLogic();
        $method = $this->helper->getMethod('frontend\logic\ProductionTaskLogic', 'getActuallySuccessorSteps');
        $actual = $method->invokeArgs($logic, [23, [23, 24, 29], $this->getProductionPath(4)]);
        $actual = array_unique($actual);
        
        try {
            $this->assertEquals(1, count($actual), 'Number of elements is incorrect');
            $this->assertEquals(24, $actual[0], 'Element 0 is incorrect');
        }
        catch(\Exception $e) {
            $this->fail($e->getMessage());
        }
    }
    
    public function testGetActuallySuccessorStepsForMinimalPathMiddleNode() {
        $logic = new ProductionTaskLogic();
        $method = $this->helper->getMethod('frontend\logic\ProductionTaskLogic', 'getActuallySuccessorSteps');
        $actual = $method->invokeArgs($logic, [24, [23, 24, 29], $this->getProductionPath(4)]);
        $actual = array_unique($actual);
        
        try {
            $this->assertEquals(1, count($actual), 'Number of elements is incorrect');
            $this->assertEquals(29, $actual[0], 'Element 0 is incorrect');
        }
        catch(\Exception $e) {
            $this->fail($e->getMessage());
        }
    }
    
    public function testGetActuallySuccessorStepsForMinimalPathEndNode() {
        $logic = new ProductionTaskLogic();
        $method = $this->helper->getMethod('frontend\logic\ProductionTaskLogic', 'getActuallySuccessorSteps');
        $actual = $method->invokeArgs($logic, [29, [23, 24, 29], $this->getProductionPath(4)]);
        $actual = array_unique($actual);
        
        try {
            $this->assertEquals(0, count($actual), 'Number of elements is incorrect');
        }
        catch(\Exception $e) {
            $this->fail($e->getMessage());
        }
    }
    
    public function testGetActuallyRequiredStepsForMaximalPathBeforeBranch() {
        $logic = new ProductionTaskLogic();
        $method = $this->helper->getMethod('frontend\logic\ProductionTaskLogic', 'getActuallyRequiredSteps');
        $actual = $method->invokeArgs($logic, [29, [23, 24, 25, 26, 27, 28, 29], $this->getProductionPath(4)]);
        $actual = array_unique($actual);
    
        try {
            $this->assertEquals(2, count($actual), 'Number of elements is incorrect');
            $this->assertEquals(26, $actual[0], 'Element 0 is incorrect');
            $this->assertEquals(28, $actual[1], 'Element 1 is incorrect');
        }
        catch(\Exception $e) {
            $this->fail($e->getMessage());
        }
    }
    
    public function testGetActuallySuccessorStepsForMaximalPathAfterBranch() {
        $logic = new ProductionTaskLogic();
        $method = $this->helper->getMethod('frontend\logic\ProductionTaskLogic', 'getActuallySuccessorSteps');
        $actual = $method->invokeArgs($logic, [24, [23, 24, 25, 26, 27, 28, 29], $this->getProductionPath(4)]);
        $actual = array_unique($actual);
    
        try {
            $this->assertEquals(2, count($actual), 'Number of elements is incorrect');
            $this->assertEquals(25, $actual[0], 'Element 0 is incorrect');
            $this->assertEquals(27, $actual[1], 'Element 1 is incorrect');
        }
        catch(\Exception $e) {
            $this->fail($e->getMessage());
        }
    }
    
    public function testGetActuallyRequiredStepsForPathWithoutRightTop() {
        $logic = new ProductionTaskLogic();
        $method = $this->helper->getMethod('frontend\logic\ProductionTaskLogic', 'getActuallyRequiredSteps');
        $actual = $method->invokeArgs($logic, [28, [23, 24, 25, 26, 28, 29], $this->getProductionPath(4)]);
        $actual = array_unique($actual);
    
        try {
            $this->assertEquals(1, count($actual), 'Number of elements is incorrect');
            $this->assertEquals(24, $actual[0], 'Element 0 is incorrect');
        }
        catch(\Exception $e) {
            $this->fail($e->getMessage());
        }
    }
    
    public function testGetActuallySuccessorStepsForPathWithoutRightTop() {
        $logic = new ProductionTaskLogic();
        $method = $this->helper->getMethod('frontend\logic\ProductionTaskLogic', 'getActuallySuccessorSteps');
        $actual = $method->invokeArgs($logic, [24, [23, 24, 25, 26, 28, 29], $this->getProductionPath(4)]);
        $actual = array_unique($actual);
    
        try {
            $this->assertEquals(2, count($actual), 'Number of elements is incorrect');
            $this->assertEquals(25, $actual[0], 'Element 0 is incorrect');
            $this->assertEquals(28, $actual[1], 'Element 1 is incorrect');
        }
        catch(\Exception $e) {
            $this->fail($e->getMessage());
        }
    }
    
    public function testGetActuallyRequiredStepsForPathWithoutRightBottom() {
        $logic = new ProductionTaskLogic();
        $method = $this->helper->getMethod('frontend\logic\ProductionTaskLogic', 'getActuallyRequiredSteps');
        $actual = $method->invokeArgs($logic, [29, [23, 24, 25, 26, 27, 29], $this->getProductionPath(4)]);
        $actual = array_unique($actual);
    
        try {
            $this->assertEquals(2, count($actual), 'Number of elements is incorrect');
            $this->assertEquals(26, $actual[0], 'Element 0 is incorrect');
            $this->assertEquals(27, $actual[1], 'Element 1 is incorrect');
        }
        catch(\Exception $e) {
            $this->fail($e->getMessage());
        }
    }
    
    public function testGetActuallySuccessorStepsForPathWithoutRightBottom() {
        $logic = new ProductionTaskLogic();
        $method = $this->helper->getMethod('frontend\logic\ProductionTaskLogic', 'getActuallySuccessorSteps');
        $actual = $method->invokeArgs($logic, [27, [23, 24, 25, 26, 27, 29], $this->getProductionPath(4)]);
        $actual = array_unique($actual);
    
        try {
            $this->assertEquals(1, count($actual), 'Number of elements is incorrect');
            $this->assertEquals(29, $actual[0], 'Element 0 is incorrect');
        }
        catch(\Exception $e) {
            $this->fail($e->getMessage());
        }
    }
    
    public function testGetActuallyRequiredStepsForPathWithoutBothBottom() {
        $logic = new ProductionTaskLogic();
        $method = $this->helper->getMethod('frontend\logic\ProductionTaskLogic', 'getActuallyRequiredSteps');
        $actual = $method->invokeArgs($logic, [29, [23, 24, 25, 27, 29], $this->getProductionPath(4)]);
        $actual = array_unique($actual);
    
        try {
            $this->assertEquals(2, count($actual), 'Number of elements is incorrect');
            $this->assertEquals(25, $actual[0], 'Element 0 is incorrect');
            $this->assertEquals(27, $actual[1], 'Element 1 is incorrect');
        }
        catch(\Exception $e) {
            $this->fail($e->getMessage());
        }
    }
    
    public function testGetActuallySuccessorStepsForPathWithoutBothBottomWithLeftNode() {
        $logic = new ProductionTaskLogic();
        $method = $this->helper->getMethod('frontend\logic\ProductionTaskLogic', 'getActuallySuccessorSteps');
        $actual = $method->invokeArgs($logic, [25, [23, 24, 25, 27, 29], $this->getProductionPath(4)]);
        $actual = array_unique($actual);
    
        try {
            $this->assertEquals(1, count($actual), 'Number of elements is incorrect');
            $this->assertEquals(29, $actual[0], 'Element 0 is incorrect');
        }
        catch(\Exception $e) {
            $this->fail($e->getMessage());
        }
    }
    
    public function testGetActuallySuccessorStepsForPathWithoutBothBottomWithRightNode() {
        $logic = new ProductionTaskLogic();
        $method = $this->helper->getMethod('frontend\logic\ProductionTaskLogic', 'getActuallySuccessorSteps');
        $actual = $method->invokeArgs($logic, [27, [23, 24, 25, 27, 29], $this->getProductionPath(4)]);
        $actual = array_unique($actual);
    
        try {
            $this->assertEquals(1, count($actual), 'Number of elements is incorrect');
            $this->assertEquals(29, $actual[0], 'Element 0 is incorrect');
        }
        catch(\Exception $e) {
            $this->fail($e->getMessage());
        }
    }
    
    public function testGetActuallyRequiredStepsForPathWithoutBothTopWithLeftNode() {
        $logic = new ProductionTaskLogic();
        $method = $this->helper->getMethod('frontend\logic\ProductionTaskLogic', 'getActuallyRequiredSteps');
        $actual = $method->invokeArgs($logic, [26, [23, 24, 26, 28, 29], $this->getProductionPath(4)]);
        $actual = array_unique($actual);
    
        try {
            $this->assertEquals(1, count($actual), 'Number of elements is incorrect');
            $this->assertEquals(24, $actual[0], 'Element 0 is incorrect');
        }
        catch(\Exception $e) {
            $this->fail($e->getMessage());
        }
    }
    
    public function testGetActuallyRequiredStepsForPathWithoutBothTopWithRightNode() {
        $logic = new ProductionTaskLogic();
        $method = $this->helper->getMethod('frontend\logic\ProductionTaskLogic', 'getActuallyRequiredSteps');
        $actual = $method->invokeArgs($logic, [28, [23, 24, 26, 28, 29], $this->getProductionPath(4)]);
        $actual = array_unique($actual);
    
        try {
            $this->assertEquals(1, count($actual), 'Number of elements is incorrect');
            $this->assertEquals(24, $actual[0], 'Element 0 is incorrect');
        }
        catch(\Exception $e) {
            $this->fail($e->getMessage());
        }
    }
    
    public function testGetActuallySuccessorStepsForPathWithoutBothTop() {
        $logic = new ProductionTaskLogic();
        $method = $this->helper->getMethod('frontend\logic\ProductionTaskLogic', 'getActuallySuccessorSteps');
        $actual = $method->invokeArgs($logic, [24, [23, 24, 26, 28, 29], $this->getProductionPath(4)]);
        $actual = array_unique($actual);
    
        try {
            $this->assertEquals(2, count($actual), 'Number of elements is incorrect');
            $this->assertEquals(26, $actual[0], 'Element 0 is incorrect');
            $this->assertEquals(28, $actual[1], 'Element 1 is incorrect');
        }
        catch(\Exception $e) {
            $this->fail($e->getMessage());
        }
    }
    
    public function testGetActuallyRequiredStepsForNotLimitedMinimalPath() {
        $logic = new ProductionTaskLogic();
        $method = $this->helper->getMethod('frontend\logic\ProductionTaskLogic', 'getActuallyRequiredSteps');
        $actual = $method->invokeArgs($logic, [29, [23, 24, 29], $this->getProductionPath(4), true]);
        $actual = array_unique($actual);
        sort($actual);
    
        try {
            $this->assertEquals(6, count($actual), 'Number of elements is incorrect');
            $this->assertEquals(23, $actual[0], 'Element 0 is incorrect');
            $this->assertEquals(24, $actual[1], 'Element 1 is incorrect');
            $this->assertEquals(25, $actual[2], 'Element 2 is incorrect');
            $this->assertEquals(26, $actual[3], 'Element 3 is incorrect');
            $this->assertEquals(27, $actual[4], 'Element 4 is incorrect');
            $this->assertEquals(28, $actual[5], 'Element 5 is incorrect');
        }
        catch(\Exception $e) {
            $this->fail($e->getMessage());
        }
    }
    
    public function testGetActuallySuccessorStepsForNotLimitedMinimalPath() {
        $logic = new ProductionTaskLogic();
        $method = $this->helper->getMethod('frontend\logic\ProductionTaskLogic', 'getActuallySuccessorSteps');
        $actual = $method->invokeArgs($logic, [23, [23, 24, 29], $this->getProductionPath(4), true]);
        $actual = array_unique($actual);
        sort($actual);
    
        try {
            $this->assertEquals(6, count($actual), 'Number of elements is incorrect');
            $this->assertEquals(24, $actual[0], 'Element 0 is incorrect');
            $this->assertEquals(25, $actual[1], 'Element 1 is incorrect');
            $this->assertEquals(26, $actual[2], 'Element 2 is incorrect');
            $this->assertEquals(27, $actual[3], 'Element 3 is incorrect');
            $this->assertEquals(28, $actual[4], 'Element 4 is incorrect');
            $this->assertEquals(29, $actual[5], 'Element 5 is incorrect');
        }
        catch(\Exception $e) {
            $this->fail($e->getMessage());
        }
    }
    
    public function testNormalizeRequiredStepsForPathWithTopLeft() {
        $logic = new ProductionTaskLogic();
        $getMethod = $this->helper->getMethod('frontend\logic\ProductionTaskLogic', 'getActuallyRequiredSteps');
        $requiredIds = $getMethod->invokeArgs($logic, [29, [23, 24, 25, 29], $this->getProductionPath(4)]);
        
        $method = $this->helper->getMethod('frontend\logic\ProductionTaskLogic', 'normalizeRequiredSteps');
        $actual = $method->invokeArgs($logic, [$requiredIds, [23, 24, 25, 29], $this->getProductionPath(4)]);
        $actual = array_unique($actual);
    
        try {
            $this->assertEquals(1, count($actual), 'Number of elements is incorrect');
            $this->assertEquals(25, $actual[0], 'Element 0 is incorrect');
        }
        catch(\Exception $e) {
            $this->fail($e->getMessage());
        }
    }
    
    public function testNormalizeSuccessorStepsForPathWithTopLeft() {
        $logic = new ProductionTaskLogic();
        $getMethod = $this->helper->getMethod('frontend\logic\ProductionTaskLogic', 'getActuallySuccessorSteps');
        $successorIds = $getMethod->invokeArgs($logic, [24, [23, 24, 25, 29], $this->getProductionPath(4)]);
    
        $method = $this->helper->getMethod('frontend\logic\ProductionTaskLogic', 'normalizeSuccessorSteps');
        $actual = $method->invokeArgs($logic, [$successorIds, [23, 24, 25, 29], $this->getProductionPath(4)]);
        $actual = array_unique($actual);
    
        try {
            $this->assertEquals(1, count($actual), 'Number of elements is incorrect');
            $this->assertEquals(25, $actual[0], 'Element 0 is incorrect');
        }
        catch(\Exception $e) {
            $this->fail($e->getMessage());
        }
    }
    
    public function testNormalizeRequiredStepsForPathWithBothTop() {
        $logic = new ProductionTaskLogic();
        $getMethod = $this->helper->getMethod('frontend\logic\ProductionTaskLogic', 'getActuallyRequiredSteps');
        $requiredIds = $getMethod->invokeArgs($logic, [29, [23, 24, 25, 27, 29], $this->getProductionPath(4)]);
    
        $method = $this->helper->getMethod('frontend\logic\ProductionTaskLogic', 'normalizeRequiredSteps');
        $actual = $method->invokeArgs($logic, [$requiredIds, [23, 24, 25, 27, 29], $this->getProductionPath(4)]);
        $actual = array_unique($actual);
    
        try {
            $this->assertEquals(2, count($actual), 'Number of elements is incorrect');
            $this->assertEquals(25, $actual[0], 'Element 0 is incorrect');
            $this->assertEquals(27, $actual[1], 'Element 1 is incorrect');
        }
        catch(\Exception $e) {
            $this->fail($e->getMessage());
        }
    }
    
    public function testNormalizeSuccessorStepsForPathWithBothTop() {
        $logic = new ProductionTaskLogic();
        $getMethod = $this->helper->getMethod('frontend\logic\ProductionTaskLogic', 'getActuallySuccessorSteps');
        $successorIds = $getMethod->invokeArgs($logic, [24, [23, 24, 25, 27, 29], $this->getProductionPath(4)]);
    
        $method = $this->helper->getMethod('frontend\logic\ProductionTaskLogic', 'normalizeSuccessorSteps');
        $actual = $method->invokeArgs($logic, [$successorIds, [23, 24, 25, 27, 29], $this->getProductionPath(4)]);
        $actual = array_unique($actual);
    
        try {
            $this->assertEquals(2, count($actual), 'Number of elements is incorrect');
            $this->assertEquals(25, $actual[0], 'Element 0 is incorrect');
            $this->assertEquals(27, $actual[1], 'Element 1 is incorrect');
        }
        catch(\Exception $e) {
            $this->fail($e->getMessage());
        }
    }
    
    public function testNormalizeRequiredStepsForMinimalPath() {
        $logic = new ProductionTaskLogic();
        $getMethod = $this->helper->getMethod('frontend\logic\ProductionTaskLogic', 'getActuallyRequiredSteps');
        $requiredIds = $getMethod->invokeArgs($logic, [29, [23, 24, 29], $this->getProductionPath(4)]);
    
        $method = $this->helper->getMethod('frontend\logic\ProductionTaskLogic', 'normalizeRequiredSteps');
        $actual = $method->invokeArgs($logic, [$requiredIds, [23, 24, 29], $this->getProductionPath(4)]);
        $actual = array_unique($actual);
    
        try {
            $this->assertEquals(1, count($actual), 'Number of elements is incorrect');
            $this->assertEquals(24, $actual[0], 'Element 0 is incorrect');
        }
        catch(\Exception $e) {
            $this->fail($e->getMessage());
        }
    }
    
    public function testNormalizeSuccessorStepsForMinimalPath() {
        $logic = new ProductionTaskLogic();
        $getMethod = $this->helper->getMethod('frontend\logic\ProductionTaskLogic', 'getActuallySuccessorSteps');
        $successorIds = $getMethod->invokeArgs($logic, [24, [23, 24, 29], $this->getProductionPath(4)]);
    
        $method = $this->helper->getMethod('frontend\logic\ProductionTaskLogic', 'normalizeSuccessorSteps');
        $actual = $method->invokeArgs($logic, [$successorIds, [23, 24, 29], $this->getProductionPath(4)]);
        $actual = array_unique($actual);
    
        try {
            $this->assertEquals(1, count($actual), 'Number of elements is incorrect');
            $this->assertEquals(29, $actual[0], 'Element 0 is incorrect');
        }
        catch(\Exception $e) {
            $this->fail($e->getMessage());
        }
    }    
    
    public function testNormalizeRequiredStepsForGeneralPathFromKToGThroughIAndJ() {
        $logic = new ProductionTaskLogic();
        $getMethod = $this->helper->getMethod('frontend\logic\ProductionTaskLogic', 'getActuallyRequiredSteps');
        $requiredIds = $getMethod->invokeArgs($logic, [11, [7, 9, 10, 11], $this->getProductionPath(1)]);
    
        $method = $this->helper->getMethod('frontend\logic\ProductionTaskLogic', 'normalizeRequiredSteps');
        $actual = $method->invokeArgs($logic, [$requiredIds, [7, 9, 10, 11], $this->getProductionPath(1)]);
        $actual = array_unique($actual);
    
        try {
            $this->assertEquals(1, count($actual), 'Number of elements is incorrect');
            $this->assertEquals(10, $actual[0], 'Element 0 is incorrect');
        }
        catch(\Exception $e) {
            $this->fail($e->getMessage());
        }
    }
    
    public function testNormalizeSuccessorStepsForGeneralPathFromGToKThroughIAndJ() {
        $logic = new ProductionTaskLogic();
        $getMethod = $this->helper->getMethod('frontend\logic\ProductionTaskLogic', 'getActuallySuccessorSteps');
        $successorIds = $getMethod->invokeArgs($logic, [7, [7, 9, 10, 11], $this->getProductionPath(1)]);
    
        $method = $this->helper->getMethod('frontend\logic\ProductionTaskLogic', 'normalizeSuccessorSteps');
        $actual = $method->invokeArgs($logic, [$successorIds, [7, 9, 10, 11], $this->getProductionPath(1)]);
        $actual = array_unique($actual);
    
        try {
            $this->assertEquals(1, count($actual), 'Number of elements is incorrect');
            $this->assertEquals(9, $actual[0], 'Element 0 is incorrect');
        }
        catch(\Exception $e) {
            $this->fail($e->getMessage());
        }
    }
    
    public function testGetTaskIdsForStepIdsForEmptySteps() {
        $logic = new ProductionTaskLogic();
        $method = $this->helper->getMethod('frontend\logic\ProductionTaskLogic', 'getTaskIdsForStepIds');
    
        $productionTasks = [];
        $productionTask1 = new ProductionTask();
        $productionTask1->id = 11;
        $productionTask1->production_path_step_id = 1;
        $productionTasks[] = $productionTask1;
    
        $actual = $method->invokeArgs($logic, [[], $productionTasks]);
    
        $this->assertEquals(0, count($actual));
    }
    
    public function testGetTaskIdsForStepIdsForEmpty() {
        $logic = new ProductionTaskLogic();
        $method = $this->helper->getMethod('frontend\logic\ProductionTaskLogic', 'getTaskIdsForStepIds');
        $actual = $method->invokeArgs($logic, [[], []]);
        
        $this->assertEquals(0, count($actual));
    }            
    
    public function testGetTaskIdsForStepIdsForEmptyTasks() {
        $logic = new ProductionTaskLogic();
        $method = $this->helper->getMethod('frontend\logic\ProductionTaskLogic', 'getTaskIdsForStepIds');   
    
        $actual = $method->invokeArgs($logic, [[1], []]);
    
        $this->assertEquals(0, count($actual));
    }
    
    public function testGetTaskIdsForStepIdsForSingleIdenticals() {
        $logic = new ProductionTaskLogic();
        $method = $this->helper->getMethod('frontend\logic\ProductionTaskLogic', 'getTaskIdsForStepIds');
    
        $productionTasks = [];
        $productionTask1 = new ProductionTask();
        $productionTask1->id = 11;
        $productionTask1->production_path_step_id = 1;
        $productionTasks[] = $productionTask1;
    
        $actual = $method->invokeArgs($logic, [[1], $productionTasks]);
    
        $this->assertEquals(1, count($actual), 'Count is incorrect');
        $this->assertEquals(11, $actual[0], 'Element 0 is incorrect');
    }
    
    public function testGetTaskIdsForStepIdsForSingleSubset() {
        $logic = new ProductionTaskLogic();
        $method = $this->helper->getMethod('frontend\logic\ProductionTaskLogic', 'getTaskIdsForStepIds');
    
        $productionTasks = [];
        $productionTask1 = new ProductionTask();
        $productionTask1->id = 11;
        $productionTask1->production_path_step_id = 1;
        $productionTasks[] = $productionTask1;
        $productionTask2 = new ProductionTask();
        $productionTask2->id = 22;
        $productionTask2->production_path_step_id = 2;
        $productionTasks[] = $productionTask2;
        $productionTask3 = new ProductionTask();
        $productionTask3->id = 33;
        $productionTask3->production_path_step_id = 3;
        $productionTasks[] = $productionTask3;
        $productionTask4 = new ProductionTask();
        $productionTask4->id = 44;
        $productionTask4->production_path_step_id = 4;
        $productionTasks[] = $productionTask4;
    
        $actual = $method->invokeArgs($logic, [[3], $productionTasks]);
    
        $this->assertEquals(1, count($actual), 'Count is incorrect');
        $this->assertEquals(33, $actual[0], 'Element 0 is incorrect');
    }
    
    public function testGetTaskIdsForStepIdsForManyIdenticals() {
        $logic = new ProductionTaskLogic();
        $method = $this->helper->getMethod('frontend\logic\ProductionTaskLogic', 'getTaskIdsForStepIds');
    
        $productionTasks = [];
        $productionTask1 = new ProductionTask();
        $productionTask1->id = 22;
        $productionTask1->production_path_step_id = 2;
        $productionTasks[] = $productionTask1;
        $productionTask2 = new ProductionTask();
        $productionTask2->id = 33;
        $productionTask2->production_path_step_id = 3;
        $productionTasks[] = $productionTask2;
    
        $actual = $method->invokeArgs($logic, [[2, 3], $productionTasks]);
    
        $this->assertEquals(2, count($actual), 'Count is incorrect');
        $this->assertEquals(22, $actual[0], 'Element 0 is incorrect');
        $this->assertEquals(33, $actual[1], 'Element 1 is incorrect');
    }
    
    public function testGetTaskIdsForStepIdsForManySubset() {
        $logic = new ProductionTaskLogic();
        $method = $this->helper->getMethod('frontend\logic\ProductionTaskLogic', 'getTaskIdsForStepIds');
    
        $productionTasks = [];
        $productionTask1 = new ProductionTask();
        $productionTask1->id = 11;
        $productionTask1->production_path_step_id = 1;
        $productionTasks[] = $productionTask1;
        $productionTask2 = new ProductionTask();
        $productionTask2->id = 22;
        $productionTask2->production_path_step_id = 2;
        $productionTasks[] = $productionTask2;
        $productionTask3 = new ProductionTask();
        $productionTask3->id = 33;
        $productionTask3->production_path_step_id = 3;
        $productionTasks[] = $productionTask3;
        $productionTask4 = new ProductionTask();
        $productionTask4->id = 44;
        $productionTask4->production_path_step_id = 4;
        $productionTasks[] = $productionTask4;
    
        $actual = $method->invokeArgs($logic, [[2, 3], $productionTasks]);
    
        $this->assertEquals(2, count($actual), 'Count is incorrect');
        $this->assertEquals(22, $actual[0], 'Element 0 is incorrect');
        $this->assertEquals(33, $actual[1], 'Element 1 is incorrect');
    }
    
    public function testGetTaskIdsForStepIdsForNonExistingStep() {
        $logic = new ProductionTaskLogic();
        $method = $this->helper->getMethod('frontend\logic\ProductionTaskLogic', 'getTaskIdsForStepIds');
    
        $productionTasks = [];
        $productionTask1 = new ProductionTask();
        $productionTask1->id = 11;
        $productionTask1->production_path_step_id = 1;
        $productionTasks[] = $productionTask1;
        $productionTask2 = new ProductionTask();
        $productionTask2->id = 22;
        $productionTask2->production_path_step_id = 2;
        $productionTasks[] = $productionTask2;
        $productionTask3 = new ProductionTask();
        $productionTask3->id = 33;
        $productionTask3->production_path_step_id = 3;
        $productionTasks[] = $productionTask3;
    
        $actual = $method->invokeArgs($logic, [[4], $productionTasks]);
    
        $this->assertEquals(0, count($actual), 'Count is incorrect');
    }
    
    public function testGetTaskIdsForStepIdsForPartiallyMatched() {
        $logic = new ProductionTaskLogic();
        $method = $this->helper->getMethod('frontend\logic\ProductionTaskLogic', 'getTaskIdsForStepIds');
    
        $productionTasks = [];
        $productionTask1 = new ProductionTask();
        $productionTask1->id = 11;
        $productionTask1->production_path_step_id = 1;
        $productionTasks[] = $productionTask1;
        $productionTask2 = new ProductionTask();
        $productionTask2->id = 22;
        $productionTask2->production_path_step_id = 2;
        $productionTasks[] = $productionTask2;
        $productionTask3 = new ProductionTask();
        $productionTask3->id = 33;
        $productionTask3->production_path_step_id = 3;
        $productionTasks[] = $productionTask3;
    
        $actual = $method->invokeArgs($logic, [[1, 4], $productionTasks]);
    
        $this->assertEquals(1, count($actual), 'Count is incorrect');
        $this->assertEquals(11, $actual[0], 'Element 0 is incorrect');
    }
    
    private function getProductionPath($pathId) {
        $logic = new ProductionTaskLogic();
        $method = $this->helper->getMethod('frontend\logic\ProductionTaskLogic', 'getPathData');
        return $method->invokeArgs($logic, [$pathId]);
    }
}
