<?php

namespace frontend\tests\unit\logic;

use Yii;
use common\fixtures\ActivityFixture;
use common\fixtures\ProductionPathFixture;
use common\fixtures\ProductionPathStepFixture;
use common\fixtures\ActivityTranslationFixture;
use common\tests\_support\TestHelper;
use frontend\logic\ProductionPathLogic;
use common\models\aq\ProductionPathStepQuery;
use common\models\aq\ProductionPathQuery;

class ProductionPathLogicTest extends \Codeception\Test\Unit {
    /**
     * @var \UnitTester
     */
    protected $tester;
    protected $helper;

    protected function _before() {
        $this->tester->haveFixtures([
            'activity' => [
                'class' => ActivityFixture::className(),
            ],
            'activityTranslation' => [
                'class' => ActivityTranslationFixture::className(),
            ],
            'productionPath' => [
                'class' => ProductionPathFixture::className(),
            ],
            'productionPathStep' => [
                'class' => ProductionPathStepFixture::className(),
            ],
        ]);

        $this->helper = new TestHelper();
    }

    protected function _after() {
    }
    
    public function testSortTopologicallyForNoStep() {
        $logic = new ProductionPathLogic();
        
        try {
            $actual = $logic->sortTopologically([]);
            $this->fail('Exception should be thrown');    
        }
        catch(\Exception $e) {
            $this->assertTrue(true);   
        }
    }
    
    public function testSortTopologicallyForStepsWithWrongStructure() {
        $logic = new ProductionPathLogic();
    
        try {
            $actual = $logic->sortTopologically([[
                        ['id' => 1],
                        ['requiredIds' => '2,3'],
                    ]]);
            $this->fail('Exception should be thrown');
        }
        catch(\Exception $e) {
            $this->assertTrue(true);
        }
    }
    
    public function testSortTopologicallyForCyclicGraph() {
        $logic = new ProductionPathLogic();
    
        try {
            $actual = $logic->sortTopologically([
                        ['id' => 1, 'requiredIds' => ''],
                        ['id' => 2, 'requiredIds' => '1,3'],
                        ['id' => 3, 'requiredIds' => '2'],
                        ['id' => 4, 'requiredIds' => '3'],
                        ['id' => 5, 'requiredIds' => '4'],
                    ]);
            $this->fail('Exception should be thrown');
        }
        catch(\Exception $e) {
            $this->assertTrue(true);
        }
    }
    
    public function testSortTopologicallyForNonconnectedGraph() {
        $logic = new ProductionPathLogic();
        $actual = $logic->sortTopologically([
                    ['id' => 1, 'requiredIds' => ''],
                    ['id' => 2, 'requiredIds' => ''],
                    ['id' => 3, 'requiredIds' => ''],
                    ['id' => 4, 'requiredIds' => ''],
                    ['id' => 5, 'requiredIds' => ''],
                ]);
        
        try {
        $this->assertEquals([1, 2, 3, 4, 5], $actual);
        } catch(\Exception $e) {
            $this->fail($e->getMessage());
        }
    }
    
    public function testSortTopologicallyForGeneralPath() {
        $logic = new ProductionPathLogic();
        $path = ProductionPathQuery::getById(1);
        $steps = ProductionPathStepQuery::getByProductionPathId($path['id']);
        $actual = $logic->sortTopologically($steps);
    
        $this->assertEquals(explode(',', $path['topologicalOrder']), $actual);
    }
    
    public function testSortTopologicallyForSpecificParallelPath() {
        $logic = new ProductionPathLogic();
        $path = ProductionPathQuery::getById(2);
        $steps = ProductionPathStepQuery::getByProductionPathId($path['id']);
        $actual = $logic->sortTopologically($steps);
    
        $this->assertEquals(explode(',', $path['topologicalOrder']), $actual);
    }
    
    public function testSortTopologicallyForSpecificParallelLongPath() {
        $logic = new ProductionPathLogic();
        $path = ProductionPathQuery::getById(3);
        $steps = ProductionPathStepQuery::getByProductionPathId($path['id']);
        $actual = $logic->sortTopologically($steps);
    
        $this->assertEquals(explode(',', $path['topologicalOrder']), $actual);
    }
    
    public function testSortTopologicallyForSpecificDoubleParallelPath() {
        $logic = new ProductionPathLogic();
        $path = ProductionPathQuery::getById(4);
        $steps = ProductionPathStepQuery::getByProductionPathId($path['id']);
        $actual = $logic->sortTopologically($steps);
    
        $this->assertEquals(explode(',', $path['topologicalOrder']), $actual);
    }

    public function testGetStepsWithoutRequiredForOneStartingNode() {
        $logic = new ProductionPathLogic();
        $method = $this->helper->getMethod('frontend\logic\ProductionPathLogic', 'getStepsWithoutRequired');
        $actual = $method->invokeArgs($logic, [[
                    ['id' => 1, 'requiredIds' => ''],
                    ['id' => 2, 'requiredIds' => '1'],
                    ['id' => 3, 'requiredIds' => '1'],
                    ['id' => 4, 'requiredIds' => '2,3'],
                ]]);
        
        try {
            $this->assertEquals(1, count($actual), 'Count is incorrect');
            $this->assertEquals(1, $actual[0], 'Element 0 is incorrect');
        }
        catch (\Exception $e) {
            $this->fail($e->getMessage());
        }
    }
    
    public function testGetStepsWithoutRequiredForManyStartingNodes() {
        $logic = new ProductionPathLogic();
        $method = $this->helper->getMethod('frontend\logic\ProductionPathLogic', 'getStepsWithoutRequired');
        $actual = $method->invokeArgs($logic, [[
                    ['id' => 1, 'requiredIds' => ''],
                    ['id' => 2, 'requiredIds' => '1'],
                    ['id' => 3, 'requiredIds' => ''],
                    ['id' => 4, 'requiredIds' => '2,3'],
                ]]);
    
        try {
            $this->assertEquals(2, count($actual), 'Count is incorrect');
            $this->assertEquals(1, $actual[0], 'Element 0 is incorrect');
            $this->assertEquals(3, $actual[1], 'Element 1 is incorrect');
        }
        catch (\Exception $e) {
            $this->fail($e->getMessage());
        }
    }
    
    public function testGetStepsWithoutRequiredForNoStartingNode() {
        $logic = new ProductionPathLogic();
        $method = $this->helper->getMethod('frontend\logic\ProductionPathLogic', 'getStepsWithoutRequired');
        $actual = $method->invokeArgs($logic, [[
                    ['id' => 1, 'requiredIds' => '2'],
                    ['id' => 2, 'requiredIds' => '1'],
                    ['id' => 3, 'requiredIds' => '1'],
                    ['id' => 4, 'requiredIds' => '2,3'],
                ]]);
    
        $this->assertEquals(0, count($actual), 'Count is incorrect');
    }
    
    public function testGetStepsWithoutRequiredForNoNode() {
        $logic = new ProductionPathLogic();
        $method = $this->helper->getMethod('frontend\logic\ProductionPathLogic', 'getStepsWithoutRequired');
        $actual = $method->invokeArgs($logic, [[]]);
        
        $this->assertEquals(0, count($actual), 'Count is incorrect');
    }
    
    public function testGetStepsWithoutRequiredForNonconnectedGraph() {
        $logic = new ProductionPathLogic();
        $method = $this->helper->getMethod('frontend\logic\ProductionPathLogic', 'getStepsWithoutRequired');
        $actual = $method->invokeArgs($logic, [[
                    ['id' => 1, 'requiredIds' => ''],
                    ['id' => 2, 'requiredIds' => ''],
                    ['id' => 3, 'requiredIds' => ''],
                    ['id' => 4, 'requiredIds' => ''],
                ]]);
    
        $this->assertEquals([1, 2, 3, 4], $actual);
    }
    
    public function testRemoveStepFromRequiredsForRemovingSingleStep() {
        $logic = new ProductionPathLogic();
        $method = $this->helper->getMethod('frontend\logic\ProductionPathLogic', 'removeStepFromRequireds');
        $actual = $method->invokeArgs($logic, [[
                    ['id' => 1, 'requiredIds' => ''],
                    ['id' => 2, 'requiredIds' => '1'],
                    ['id' => 3, 'requiredIds' => '1'],
                    ['id' => 4, 'requiredIds' => '2,3'],
                ], [1]]);
    
        try {
            $this->assertEquals([
                        ['id' => 1, 'requiredIds' => ''],
                        ['id' => 2, 'requiredIds' => ''],
                        ['id' => 3, 'requiredIds' => ''],
                        ['id' => 4, 'requiredIds' => '2,3'],
                    ], $actual);
        }
        catch (\Exception $e) {
            $this->fail($e->getMessage());
        }
    }
    
    public function testRemoveStepFromRequiredsForRemovingSingleStepFromPairs() {
        $logic = new ProductionPathLogic();
        $method = $this->helper->getMethod('frontend\logic\ProductionPathLogic', 'removeStepFromRequireds');
        $actual = $method->invokeArgs($logic, [[
                    ['id' => 1, 'requiredIds' => ''],
                    ['id' => 2, 'requiredIds' => '1'],
                    ['id' => 3, 'requiredIds' => '1,2'],
                    ['id' => 4, 'requiredIds' => '2,3'],
                ], [2]]);
    
        try {
            $this->assertEquals([
                        ['id' => 1, 'requiredIds' => ''],
                        ['id' => 2, 'requiredIds' => '1'],
                        ['id' => 3, 'requiredIds' => '1'],
                        ['id' => 4, 'requiredIds' => '3'],
                    ], $actual);
        }
        catch (\Exception $e) {
            $this->fail($e->getMessage());
        }
    }
    
    public function testRemoveStepFromRequiredsForRemovingPairsAndPartials() {
        $logic = new ProductionPathLogic();
        $method = $this->helper->getMethod('frontend\logic\ProductionPathLogic', 'removeStepFromRequireds');
        $actual = $method->invokeArgs($logic, [[
                    ['id' => 1, 'requiredIds' => ''],
                    ['id' => 2, 'requiredIds' => '1'],
                    ['id' => 3, 'requiredIds' => '1,2'],
                    ['id' => 4, 'requiredIds' => '3'],
                    ['id' => 5, 'requiredIds' => '2,3'],
                ], [2, 3]]);
    
        try {
            $this->assertEquals([
                        ['id' => 1, 'requiredIds' => ''],
                        ['id' => 2, 'requiredIds' => '1'],
                        ['id' => 3, 'requiredIds' => '1'],
                        ['id' => 4, 'requiredIds' => ''],
                        ['id' => 5, 'requiredIds' => ''],
                    ], $actual);
        }
        catch (\Exception $e) {
            $this->fail($e->getMessage());
        }
    }
    
    public function testRemoveStepFromRequiredsForRemovingNothing() {
        $logic = new ProductionPathLogic();
        $method = $this->helper->getMethod('frontend\logic\ProductionPathLogic', 'removeStepFromRequireds');
        $actual = $method->invokeArgs($logic, [[
                    ['id' => 1, 'requiredIds' => ''],
                    ['id' => 2, 'requiredIds' => '1'],
                    ['id' => 3, 'requiredIds' => '1,2'],
                    ['id' => 4, 'requiredIds' => '2,3'],
                ], [4]]);
    
        try {
            $this->assertEquals([
                        ['id' => 1, 'requiredIds' => ''],
                        ['id' => 2, 'requiredIds' => '1'],
                        ['id' => 3, 'requiredIds' => '1,2'],
                        ['id' => 4, 'requiredIds' => '2,3'],
                    ], $actual);
        }
        catch (\Exception $e) {
            $this->fail($e->getMessage());
        }
    }
    
    public function testRemoveStepFromRequiredsForEmptyRequireds() {
        $logic = new ProductionPathLogic();
        $method = $this->helper->getMethod('frontend\logic\ProductionPathLogic', 'removeStepFromRequireds');
        $actual = $method->invokeArgs($logic, [[
                    ['id' => 1, 'requiredIds' => ''],
                    ['id' => 2, 'requiredIds' => '1'],
                    ['id' => 3, 'requiredIds' => '1,2'],
                    ['id' => 4, 'requiredIds' => '2,3'],
                ], []]);
    
        try {
            $this->assertEquals([
                        ['id' => 1, 'requiredIds' => ''],
                        ['id' => 2, 'requiredIds' => '1'],
                        ['id' => 3, 'requiredIds' => '1,2'],
                        ['id' => 4, 'requiredIds' => '2,3'],
                    ], $actual);
        }
        catch (\Exception $e) {
            $this->fail($e->getMessage());
        }
    }
    
    public function testRemoveStepFromRequiredsForNoStep() {
        $logic = new ProductionPathLogic();
        $method = $this->helper->getMethod('frontend\logic\ProductionPathLogic', 'removeStepFromRequireds');
        $actual = $method->invokeArgs($logic, [[], [1, 2]]);
    
        try {
            $this->assertEquals([], $actual);
        }
        catch (\Exception $e) {
            $this->fail($e->getMessage());
        }
    }  
}
