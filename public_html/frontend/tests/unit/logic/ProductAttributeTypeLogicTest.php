<?php
namespace frontend\tests\unit\models;

use common\fixtures\ProductAttributeTypeFixture;
use frontend\models\ProductAttributeForm;
use common\fixtures\ProductAttributeGroupFixture;
use common\fixtures\ProductAttributeGroupTranslationFixture;
use common\fixtures\ProductAttributeTypeTranslationFixture;
use frontend\logic\ProductAttributeTypeLogic;
use common\models\aq\OfferedProductAttributeQuery;
use common\fixtures\OfferedProductFixture;
use common\fixtures\OfferedProductAttributeFixture;

class ProductAttributTypeLogicTest extends \Codeception\Test\Unit {
    /**
     * @var \UnitTester
     */
    protected $tester;

    protected function _before() {
    	$this->tester->haveFixtures([
    	    'productAttributeGroup' => [
    	        'class' => ProductAttributeGroupFixture::className(),
    	    ],
    	    'productAttributeGroupTranslation' => [
    	        'class' => ProductAttributeGroupTranslationFixture::className(),
    	    ],
            'productAttributeType' => [
                'class' => ProductAttributeTypeFixture::className(),
            ],
    	    'productAttributeTypeTranslation' => [
    	        'class' => ProductAttributeTypeTranslationFixture::className(),
    	    ],
    	    'offeredProduct' => [
                'class' => OfferedProductFixture::className(),
    	    ],
    	    'offeredProductAttribute' => [
                'class' => OfferedProductAttributeFixture::className(),
    	    ],
    	]);
    }

    protected function _after() {
    }
    
    public function testGetAttributeGroupsWithTypes() {
        $logic = new ProductAttributeTypeLogic();
        
        $actual = $logic->getAttributeGroupsWithTypes();
        
        $this->assertEquals([
                	[
                		'id' => '1',
                		'name' => 'Implementacja',
                		'attributes' => [
                			[
                				'id' => 1,
                				'groupId' => 1,
                				'variableType' => 'bool',
                				'attributeCheckedIds' => null,
                				'attributeBlockedIds' => null,
                				'isVisible' => 1,
                				'name' => 'Programowanie',
                			],
                			[
                				'id' => 2,
                				'groupId' => 1,
                				'variableType' => 'bool',
                				'attributeCheckedIds' => null,
                				'attributeBlockedIds' => null,
                				'isVisible' => 1,
                				'name' => 'Projektowanie',
                			],
                			[
                				'id' => 3,
                				'groupId' => 1,
                				'variableType' => 'bool',
                				'attributeCheckedIds' => null,
                				'attributeBlockedIds' => null,
                				'isVisible' => 1,
                				'name' => 'Weryfikacja',
                			],
                			[
                				'id' => 4,
                				'groupId' => 1,
                				'variableType' => 'bool',
                				'attributeCheckedIds' => null,
                				'attributeBlockedIds' => null,
                				'isVisible' => 1,
                				'name' => 'Walidacja',
                			],
                		],
                	],
                	[
                		'id' => '2',
                		'name' => 'Zewnętrzne',
                		'attributes' => [
                			[
                				'id' => 7,
                				'groupId' => 2,
                				'variableType' => 'bool',
                				'attributeCheckedIds' => null,
                				'attributeBlockedIds' => null,
                				'isVisible' => 1,
                				'name' => 'Marketing',
                			],
                			[
                				'id' => 8,
                				'groupId' => 2,
                				'variableType' => 'bool',
                				'attributeCheckedIds' => null,
                				'attributeBlockedIds' => null,
                				'isVisible' => 1,
                				'name' => 'Obsługa serwisów',
                			],
                			[
                				'id' => 10,
                				'groupId' => 2,
                				'variableType' => 'bool',
                				'attributeCheckedIds' => null,
                				'attributeBlockedIds' => null,
                				'isVisible' => 1,
                				'name' => 'SEO',
                			],
                			[
                				'id' => 11,
                				'groupId' => 2,
                				'variableType' => 'int',
                				'attributeCheckedIds' => null,
                				'attributeBlockedIds' => null,
                				'isVisible' => 1,
                				'name' => 'Tłumaczenie',
                			],
                		],
                	],
                	[
                		'name' => '',
                		'attributes' => [
                			[
                				'id' => 5,
                				'groupId' => null,
                				'variableType' => 'bool',
                				'attributeCheckedIds' => null,
                				'attributeBlockedIds' => null,
                				'isVisible' => 1,
                				'name' => 'Złożenie wniosku',
                			],
                		],
                	],
                ], $actual);
    }
    
    public function testGetAttributeGroupsWithValuesForOfferedProduct() {
        $logic = new ProductAttributeTypeLogic();
        $attributeValues = OfferedProductAttributeQuery::getByOfferedProductId(2);
    
        $actual = $logic->getAttributeGroupsWithValues($attributeValues);
    
        $this->assertEquals([
                	[
                		'id' => '1',
                		'name' => 'Implementacja',
                		'attributes' => [
                			[
                				'id' => 2,
                				'groupId' => 1,
                				'variableType' => 'bool',
                				'isVisible' => '1',
                				'name' => 'Projektowanie',
                			    'value' => '1',
                			],
                		],
                	],
                	[
                		'id' => '2',
                		'name' => 'Zewnętrzne',
                		'attributes' => [],
                	],
                ], $actual);
    }
}