<?php
namespace frontend\tests\unit\models;

use Yii;
use common\fixtures\DepartmentTypeFixture;
use common\fixtures\DepartmentTypeTranslationFixture;
use common\fixtures\DepartmentFixture;
use common\fixtures\DepartmentTranslationFixture;
use common\fixtures\OfferedProductFixture;
use common\fixtures\OfferedProductTranslationFixture;
use common\fixtures\ActivityFixture;
use common\fixtures\ActivityTranslationFixture;
use common\fixtures\DepartmentActivityFixture;
use common\fixtures\TaskFixture;
use common\fixtures\ProductionTaskFixture;
use common\tests\_support\TestHelper;
use common\tests\_support\ProjectHelper;
use common\helpers\Utility;
use common\models\ar\Task;
use common\models\ar\ProductionTask;
use common\models\aq\TaskTypeQuery;
use common\models\aq\TaskPriorityQuery;
use frontend\models\ProductionAddItemsForm;
use common\fixtures\WorkstationFixture;
use common\models\ar\User;
use common\models\ar\Activity;
use common\models\ar\ProductionPathStep;

class ProductionAddItemsFormTest extends \Codeception\Test\Unit {
    /**
     * @var \UnitTester
     */
    protected $tester;
    protected $helper;
    protected $projectHelper;

    protected function _before() {
    	$this->tester->haveFixtures([
    	    'departmentType' => [
    	        'class' => DepartmentTypeFixture::className(),
    	    ],
    	    'departmentTypeTranslation' => [
    	        'class' => DepartmentTypeTranslationFixture::className(),
    	    ],
    	    'department' => [
    	        'class' => DepartmentFixture::className(),
    	    ],
    	    'departmentTranslation' => [
    	        'class' => DepartmentTranslationFixture::className(),
    	    ],
    	    'offeredProduct' => [
    	        'class' => OfferedProductFixture::className(),
    	    ],
    	    'offeredProductTranslation' => [
    	        'class' => OfferedProductTranslationFixture::className(),
    	    ],
    	    'activity' => [
    	        'class' => ActivityFixture::className(),
    	    ],
    	    'activityTranslation' => [
    	        'class' => ActivityTranslationFixture::className(),
    	    ],
    	    'departmentActivity' => [
    	        'class' => DepartmentActivityFixture::className(),
    	    ],
    	    'task' => [
    	        'class' => TaskFixture::className(),
    	    ],
    	    'productionTask' => [
    	        'class' => ProductionTaskFixture::className(),
    	    ],
    	    'workstation' => [
                'class' => WorkstationFixture::className(),
    	    ],
    	]);
    	
    	$this->helper = new TestHelper();
    	$this->projectHelper = new ProjectHelper();
    }

    protected function _after() {
    }
    
    public function testSaveForZeroCompletedAndOneFailedTask() {
        $user = User::findOne(2);
        Yii::$app->user->login($user);
    
        $order = $this->generateOrder();
        $productionTasks = $this->generateTaskAndFollowingTask($order);
    
        $model = new ProductionAddItemsForm();
        $model->productionTaskId = $productionTasks[0]->id;
        $model->workstationId = 1;
        $model->completedCount = 0;
        $model->damagedCount = 1;
    
        try {
            $model->save();
    
            $updatedProductionTask = ProductionTask::findOne($productionTasks[0]->id);
    
            $this->assertEquals(0, $updatedProductionTask->total_count_made, 'Total count made is incorrect');
            $this->assertEquals(1, $updatedProductionTask->total_count_failed, 'Total count failed is incorrect');
            $this->assertEquals(0, $updatedProductionTask->task->is_complete, 'Task completion is incorrect');
    
            $history = $updatedProductionTask->productionTaskHistories;
    
            $this->assertEquals(1, count($history), 'History count is incorrect');
            $this->assertEquals(2, $history[0]->user_id, 'History 0 user id is incorrect');
            $this->assertEquals(1, $history[0]->workstation_id, 'History 0 workstation id is incorrect');
            $this->assertEquals(0, $history[0]->count_made, 'History 0 count made is incorrect');
            $this->assertEquals(1, $history[0]->count_failed, 'History 0 count failed is incorrect');
    
            $followingProductionTask = ProductionTask::findOne($productionTasks[1]->id);
            $this->assertEquals(0, $followingProductionTask->is_available, 'Following task is available is incorrect');
        }
        catch(\Exception $e) {
            $this->fail($e->getMessage());
        }
    }

    public function testSaveForNonCompletedTask() {
        $user = User::findOne(2);
        Yii::$app->user->login($user);
        
        $order = $this->generateOrder();
        $productionTasks = $this->generateTaskAndFollowingTask($order);
        
        $itemsToAdd = $order->orderOfferedProducts[0]->count - 1;
        
        $model = new ProductionAddItemsForm(); 
        $model->productionTaskId = $productionTasks[0]->id;
        $model->workstationId = 1;
        $model->completedCount = $itemsToAdd;
        $model->damagedCount = 0;
        
        try {
            $model->save();
            
            $updatedProductionTask = ProductionTask::findOne($productionTasks[0]->id);
            
            $this->assertEquals($itemsToAdd, $updatedProductionTask->total_count_made, 'Total count made is incorrect');
            $this->assertEquals(0, $updatedProductionTask->total_count_failed, 'Total count failed is incorrect');
            $this->assertEquals(0, $updatedProductionTask->task->is_complete, 'Task completion is incorrect');
            
            $history = $updatedProductionTask->productionTaskHistories;
            
            $this->assertEquals(1, count($history), 'History count is incorrect');
            $this->assertEquals(2, $history[0]->user_id, 'History 0 user id is incorrect');
            $this->assertEquals(1, $history[0]->workstation_id, 'History 0 workstation id is incorrect');
            $this->assertEquals($itemsToAdd, $history[0]->count_made, 'History 0 count made is incorrect');
            $this->assertEquals(0, $history[0]->count_failed, 'History 0 count failed is incorrect');
            
            $followingProductionTask = ProductionTask::findOne($productionTasks[1]->id);
            $this->assertEquals(1, $followingProductionTask->is_available, 'Following task is available is incorrect');
        }
        catch(\Exception $e) {
            $this->fail($e->getMessage());
        }
    }
    
    public function testSaveForExactlyCompletedTask() {
        $user = User::findOne(2);
        Yii::$app->user->login($user);
    
        $order = $this->generateOrder();
        $productionTasks = $this->generateTaskAndFollowingTask($order);
    
        $itemsToAdd = $order->orderOfferedProducts[0]->count;
    
        $model = new ProductionAddItemsForm();
        $model->productionTaskId = $productionTasks[0]->id;
        $model->workstationId = 1;
        $model->completedCount = $itemsToAdd;
        $model->damagedCount = 0;
    
        try {
            $model->save();
    
            $updatedProductionTask = ProductionTask::findOne($productionTasks[0]->id);
    
            $this->assertEquals($itemsToAdd, $updatedProductionTask->total_count_made, 'Total count made is incorrect');
            $this->assertEquals(0, $updatedProductionTask->total_count_failed, 'Total count failed is incorrect');
            $this->assertEquals(1, $updatedProductionTask->task->is_complete, 'Task completion is incorrect');
    
            $history = $updatedProductionTask->productionTaskHistories;
    
            $this->assertEquals(1, count($history), 'History count is incorrect');
            $this->assertEquals(2, $history[0]->user_id, 'History 0 user id is incorrect');
            $this->assertEquals(1, $history[0]->workstation_id, 'History 0 workstation id is incorrect');
            $this->assertEquals($itemsToAdd, $history[0]->count_made, 'History 0 count made is incorrect');
            $this->assertEquals(0, $history[0]->count_failed, 'History 0 count failed is incorrect');
            
            $followingProductionTask = ProductionTask::findOne($productionTasks[1]->id);
            $this->assertEquals(1, $followingProductionTask->is_available, 'Following task is available is incorrect');
        }
        catch(\Exception $e) {
            $this->fail($e->getMessage());
        }
    }
    
    public function testSaveForExactlyCompletedTaskInTwoSteps() {
        $user = User::findOne(2);
        Yii::$app->user->login($user);
    
        $order = $this->generateOrder();
        $productionTasks = $this->generateTaskAndFollowingTask($order);
    
        $itemsToAdd = $order->orderOfferedProducts[0]->count - 1;
    
        try {
            $modelInit = new ProductionAddItemsForm();
            $modelInit->productionTaskId = $productionTasks[0]->id;
            $modelInit->workstationId = 1;
            $modelInit->completedCount = $itemsToAdd;
            $modelInit->damagedCount = 0;
            $modelInit->save();
            
            $model = new ProductionAddItemsForm();
            $model->productionTaskId = $productionTasks[0]->id;
            $model->workstationId = 3;
            $model->completedCount = 1;
            $model->damagedCount = 0;
            $model->save();
    
            $updatedProductionTask = ProductionTask::findOne($productionTasks[0]->id);
    
            $this->assertEquals($itemsToAdd + 1, $updatedProductionTask->total_count_made, 'Total count made is incorrect');
            $this->assertEquals(0, $updatedProductionTask->total_count_failed, 'Total count failed is incorrect');
            $this->assertEquals(1, $updatedProductionTask->task->is_complete, 'Task completion is incorrect');
    
            $history = $updatedProductionTask->productionTaskHistories;
    
            $this->assertEquals(2, count($history), 'History count is incorrect');
            
            $this->assertEquals(2, $history[0]->user_id, 'History 0 user id is incorrect');
            $this->assertEquals(1, $history[0]->workstation_id, 'History 0 workstation id is incorrect');
            $this->assertEquals($itemsToAdd, $history[0]->count_made, 'History 0 count made is incorrect');
            $this->assertEquals(0, $history[0]->count_failed, 'History 0 count failed is incorrect');
            
            $this->assertEquals(2, $history[1]->user_id, 'History 1 user id is incorrect');
            $this->assertEquals(3, $history[1]->workstation_id, 'History 1 workstation id is incorrect');
            $this->assertEquals(1, $history[1]->count_made, 'History 1 count made is incorrect');
            $this->assertEquals(0, $history[1]->count_failed, 'History 1 count failed is incorrect');
            
            $followingProductionTask = ProductionTask::findOne($productionTasks[1]->id);
            $this->assertEquals(1, $followingProductionTask->is_available, 'Following task is available is incorrect');
        }
        catch(\Exception $e) {
            $this->fail($e->getMessage());
        }
    }
    
    public function testSaveForPassingArtificialNode() {
        $user = User::findOne(2);
        Yii::$app->user->login($user);
        
        $order = $this->generateOrder();
        $productionTasks = $this->generateTaskAndFollowingTask($order, true);
        
        try {
            $model = new ProductionAddItemsForm();
            $model->productionTaskId = $productionTasks[0]->id;
            $model->workstationId = 1;
            $model->completedCount = $order->orderOfferedProducts[0]->count - 1;
            $model->damagedCount = 0;
            $model->save();
        
            $productionTaskInMiddle = ProductionTask::findOne($productionTasks[1]->id);
            $productionTaskAtEnd = ProductionTask::findOne($productionTasks[2]->id);
        
            $this->assertEquals(1, $productionTaskInMiddle->is_available, 'Middle (artificial) task is_available is incorrect');
            $this->assertEquals(1, $productionTaskAtEnd->is_available, 'End task is_available is incorrect');
        }
        catch(\Exception $e) {
            $this->fail($e->getMessage());
        }
    }
    
    public function testValidateForNoncompletedSingleTask() {
        $user = User::findOne(2);
        Yii::$app->user->login($user);
    
        $order = $this->generateOrder();
        $productionTasks = $this->generateTaskAndFollowingTask($order);
    
        $itemsToAdd = $order->orderOfferedProducts[0]->count - 1;
    
        $model = new ProductionAddItemsForm();
        $model->productionTaskId = $productionTasks[0]->id;
        $model->workstationId = 1;
        $model->completedCount = $itemsToAdd;
        $model->damagedCount = 0;
    
        $this->assertTrue($model->validate());
    }
    
    public function testValidateForExactlyCompletedSingleTask() {
        $user = User::findOne(2);
        Yii::$app->user->login($user);
    
        $order = $this->generateOrder();
        $productionTasks = $this->generateTaskAndFollowingTask($order);
    
        $itemsToAdd = $order->orderOfferedProducts[0]->count;
    
        $model = new ProductionAddItemsForm();
        $model->productionTaskId = $productionTasks[0]->id;
        $model->workstationId = 1;
        $model->completedCount = $itemsToAdd;
        $model->damagedCount = 0;
    
        $this->assertTrue($model->validate());
    }
    
    public function testValidateForOvercompletedSingleTask() {
        $user = User::findOne(2);
        Yii::$app->user->login($user);
    
        $order = $this->generateOrder();
        $productionTasks = $this->generateTaskAndFollowingTask($order);
    
        $itemsToAdd = $order->orderOfferedProducts[0]->count + 1;
    
        $model = new ProductionAddItemsForm();
        $model->productionTaskId = $productionTasks[0]->id;
        $model->workstationId = 1;
        $model->completedCount = $itemsToAdd;
        $model->damagedCount = 0;
    
        $this->assertFalse($model->validate());
    }
    
    public function testValidateForExactlyCompletedTaskAfterNonCompletedOne() {
        $user = User::findOne(2);
        Yii::$app->user->login($user);
        
        $order = $this->generateOrder();
        $productionTasks = $this->generateTaskAndFollowingTask($order);
        
        $modelRequired = new ProductionAddItemsForm();
        $modelRequired->productionTaskId = $productionTasks[0]->id;
        $modelRequired->workstationId = 1;
        $modelRequired->completedCount = $order->orderOfferedProducts[0]->count - 1;
        $modelRequired->damagedCount = 0;
        $modelRequired->save();
        
        $model = new ProductionAddItemsForm();
        $model->productionTaskId = $productionTasks[1]->id;
        $model->workstationId = 1;
        $model->completedCount = $order->orderOfferedProducts[0]->count - 1;
        $model->damagedCount = 0;
        
        $this->assertTrue($model->validate());
    }
    
    public function testValidateForOvercompletedTaskAfterNonCompletedOne() {
        $user = User::findOne(2);
        Yii::$app->user->login($user);
    
        $order = $this->generateOrder();
        $productionTasks = $this->generateTaskAndFollowingTask($order);
    
        $modelRequired = new ProductionAddItemsForm();
        $modelRequired->productionTaskId = $productionTasks[0]->id;
        $modelRequired->workstationId = 1;
        $modelRequired->completedCount = $order->orderOfferedProducts[0]->count - 1;
        $modelRequired->damagedCount = 0;
        try {
            $modelRequired->save();
        }
        catch(\Exception $e) {
            $this->fail($e->getMessage());
        }
    
        $model = new ProductionAddItemsForm();
        $model->productionTaskId = $productionTasks[1]->id;
        $model->workstationId = 1;
        $model->completedCount = $order->orderOfferedProducts[0]->count;
        $model->damagedCount = 0;
    
        $this->assertFalse($model->validate());
    }
    
    public function testValidateForOvercompletedTaskAfterNonCompletedOneInParts() {
        $user = User::findOne(2);
        Yii::$app->user->login($user);
    
        $order = $this->generateOrder();
        $productionTasks = $this->generateTaskAndFollowingTask($order);
    
        $modelRequired = new ProductionAddItemsForm();
        $modelRequired->productionTaskId = $productionTasks[0]->id;
        $modelRequired->workstationId = 1;
        $modelRequired->completedCount = $order->orderOfferedProducts[0]->count - 1;
        $modelRequired->damagedCount = 0;
        try {
            $modelRequired->save();
        }
        catch(\Exception $e) {
            $this->fail($e->getMessage());
        }
        
        $modelInit = new ProductionAddItemsForm();
        $modelInit->productionTaskId = $productionTasks[1]->id;
        $modelInit->workstationId = 1;
        $modelInit->completedCount = $order->orderOfferedProducts[0]->count - 1;
        $modelInit->damagedCount = 0;
        try {
            $modelInit->save();
        }
        catch(\Exception $e) {
            $this->fail($e->getMessage());
        }
    
        $model = new ProductionAddItemsForm();
        $model->productionTaskId = $productionTasks[1]->id;
        $model->workstationId = 1;
        $model->completedCount = $order->orderOfferedProducts[0]->count - 1;
        $model->damagedCount = 0;
    
        $this->assertFalse($model->validate());
    }
    
    public function testValidateForCheckFullCompletionWithArtificialNode() {
        $user = User::findOne(2);
        Yii::$app->user->login($user);
        
        $order = $this->generateOrder();
        $productionTasks = $this->generateTaskAndFollowingTask($order, true);
        
        $modelRequired = new ProductionAddItemsForm();
        $modelRequired->productionTaskId = $productionTasks[0]->id;
        $modelRequired->workstationId = 1;
        $modelRequired->completedCount = $order->orderOfferedProducts[0]->count;
        $modelRequired->damagedCount = 0;
        $modelRequired->save();
        
        $model = new ProductionAddItemsForm();
        $model->productionTaskId = $productionTasks[2]->id;
        $model->workstationId = 1;
        $model->completedCount = $order->orderOfferedProducts[0]->count;
        $model->damagedCount = 0;
        
        $this->assertTrue($model->validate());
    }
    
    public function testValidateForCheckPartialCompletionWithArtificialNode() {
        $user = User::findOne(2);
        Yii::$app->user->login($user);
    
        $order = $this->generateOrder();
        $productionTasks = $this->generateTaskAndFollowingTask($order, true);
    
        $modelRequired = new ProductionAddItemsForm();
        $modelRequired->productionTaskId = $productionTasks[0]->id;
        $modelRequired->workstationId = 1;
        $modelRequired->completedCount = $order->orderOfferedProducts[0]->count - 1;
        $modelRequired->damagedCount = 0;
        $modelRequired->save();
    
        $model = new ProductionAddItemsForm();
        $model->productionTaskId = $productionTasks[2]->id;
        $model->workstationId = 1;
        $model->completedCount = $order->orderOfferedProducts[0]->count - 1;
        $model->damagedCount = 0;
    
        $this->assertTrue($model->validate());
    }
    
    public function testValidateForCheckOverCompletionWithArtificialNode() {
        $user = User::findOne(2);
        Yii::$app->user->login($user);
    
        $order = $this->generateOrder();
        $productionTasks = $this->generateTaskAndFollowingTask($order, true);
    
        $modelRequired = new ProductionAddItemsForm();
        $modelRequired->productionTaskId = $productionTasks[0]->id;
        $modelRequired->workstationId = 1;
        $modelRequired->completedCount = $order->orderOfferedProducts[0]->count - 1;
        $modelRequired->damagedCount = 0;
        $modelRequired->save();
    
        $model = new ProductionAddItemsForm();
        $model->productionTaskId = $productionTasks[2]->id;
        $model->workstationId = 1;
        $model->completedCount = $order->orderOfferedProducts[0]->count;
        $model->damagedCount = 0;
    
        $this->assertFalse($model->validate());
    }  
    
    public function testUnlockSuccessorTasksForStraightPath() {
        $user = User::findOne(2);
        Yii::$app->user->login($user);
        
        $order = $this->generateOrder();
        $productionTasks = $this->generateTaskAndFollowingTask($order);
        
        $productionTask = $productionTasks[0];
        $productionTask->total_count_made = $order->orderOfferedProducts[0]->count - 1;
        $productionTask->save();
        
        $model = new ProductionAddItemsForm();
        $method = $this->helper->getMethod('frontend\models\ProductionAddItemsForm', 'unlockSuccessorTasks');   
        $method->invokeArgs($model, [$productionTask, true]);
        
        $actual = ProductionTask::findOne($productionTasks[1]->id);
        $this->assertEquals(1, $actual->is_available);
    }
    
    public function testUnlockSuccessorTasksForEndOfStraightPath() {
        $user = User::findOne(2);
        Yii::$app->user->login($user);
    
        $order = $this->generateOrder();
        $productionTasks = $this->generateTaskAndFollowingTask($order);
    
        $productionTask = $productionTasks[0];
        $productionTask->total_count_made = $order->orderOfferedProducts[0]->count;
        $productionTask->save();
        $productionTask = $productionTasks[1];
        $productionTask->total_count_made = $order->orderOfferedProducts[0]->count;
        $productionTask->save();
    
        $model = new ProductionAddItemsForm();
        $method = $this->helper->getMethod('frontend\models\ProductionAddItemsForm', 'unlockSuccessorTasks');
        
        try {
            $method->invokeArgs($model, [$productionTask, true]);
        }
        catch(\Exception $e) {
            $this->fail($e->getMessage());
        }
    }
    
    public function testUnlockSuccessorTasksForStraightPathWithZeroCount() {
        $user = User::findOne(2);
        Yii::$app->user->login($user);
    
        $order = $this->generateOrder();
        $productionTasks = $this->generateTaskAndFollowingTask($order);
    
        $productionTask = $productionTasks[0];
        $productionTask->total_count_made = 0;
        $productionTask->save();
    
        $model = new ProductionAddItemsForm();
        $method = $this->helper->getMethod('frontend\models\ProductionAddItemsForm', 'unlockSuccessorTasks');
        $method->invokeArgs($model, [$productionTask, true]);
    
        $actual = ProductionTask::findOne($productionTasks[1]->id);
        $this->assertEquals(0, $actual->is_available);
    }
    
    public function testUnlockSuccessorTasksForTwoFollowingByOneWithOnlyOneCompleted() {
        $user = User::findOne(2);
        Yii::$app->user->login($user);
    
        $order = $this->generateOrder();
        $productionTasks = $this->generateTwoTasksFollowingByOne($order);
    
        $productionTask = $productionTasks[0];
        $productionTask->total_count_made = $order->orderOfferedProducts[0]->count;
        $productionTask->save();
    
        $model = new ProductionAddItemsForm();
        $method = $this->helper->getMethod('frontend\models\ProductionAddItemsForm', 'unlockSuccessorTasks');
        $method->invokeArgs($model, [$productionTask, true]);
    
        $actual = ProductionTask::findOne($productionTasks[2]->id);
        $this->assertEquals(0, $actual->is_available);
    }
    
    public function testUnlockSuccessorTasksForTwoFollowingByOneWithBothCompleted() {
        $user = User::findOne(2);
        Yii::$app->user->login($user);
    
        $order = $this->generateOrder();
        $productionTasks = $this->generateTwoTasksFollowingByOne($order);
    
        $productionTask = $productionTasks[0];
        $productionTask->total_count_made = $order->orderOfferedProducts[0]->count;
        $productionTask->save();
        $productionTask = $productionTasks[1];
        $productionTask->total_count_made = $order->orderOfferedProducts[0]->count;
        $productionTask->save();
    
        $model = new ProductionAddItemsForm();
        $method = $this->helper->getMethod('frontend\models\ProductionAddItemsForm', 'unlockSuccessorTasks');
        $method->invokeArgs($model, [$productionTask, true]);
    
        $actual = ProductionTask::findOne($productionTasks[2]->id);
        $this->assertEquals(1, $actual->is_available);
    }
    
    public function testUnlockSuccessorTasksForOneFollowingByTwoUnlockingBoth() {
        $user = User::findOne(2);
        Yii::$app->user->login($user);
    
        $order = $this->generateOrder();
        $productionTasks = $this->generateOneTaskFollowingByTwo($order);
    
        $productionTask = $productionTasks[0];
        $productionTask->total_count_made = $order->orderOfferedProducts[0]->count;
        $productionTask->save();
    
        $model = new ProductionAddItemsForm();
        $method = $this->helper->getMethod('frontend\models\ProductionAddItemsForm', 'unlockSuccessorTasks');
        $method->invokeArgs($model, [$productionTask, true]);
    
        $actual1 = ProductionTask::findOne($productionTasks[1]->id);
        $actual2 = ProductionTask::findOne($productionTasks[2]->id);
        $this->assertEquals(1, $actual1->is_available, 'Left end task is available is incorrect');
        $this->assertEquals(1, $actual2->is_available, 'Left end task is available is incorrect');
    }
    
    public function testUnlockSuccessorTasksForOneFollowingByTwoUnlockingOne() {
        $user = User::findOne(2);
        Yii::$app->user->login($user);
    
        $order = $this->generateOrder();
        $productionTasks = $this->generateOneTaskFollowingByTwo($order, true);
    
        $productionTask = $productionTasks[0];
        $productionTask->total_count_made = $order->orderOfferedProducts[0]->count;
        $productionTask->save();
    
        $model = new ProductionAddItemsForm();
        $method = $this->helper->getMethod('frontend\models\ProductionAddItemsForm', 'unlockSuccessorTasks');
        $method->invokeArgs($model, [$productionTask, true]);
    
        $actual1 = ProductionTask::findOne($productionTasks[1]->id);
        $actual2 = ProductionTask::findOne($productionTasks[2]->id);
        $this->assertEquals(1, $actual1->is_available, 'Left end task is available is incorrect');
        $this->assertEquals(0, $actual2->is_available, 'Left end task is available is incorrect');
    }
    
    private function generateOrder() {
        $order = $this->projectHelper->generateOrder(Utility::ORDER_STATUS_IN_PROGRESS);
        $order = $this->projectHelper->assignOfferedProduct($order, 1);
    
        return $order;
    }
    
    private function generateTaskAndFollowingTask($order, $withArtificialNode = false) {
        $taskTypeId = TaskTypeQuery::getBySymbol(Utility::TASK_TYPE_PRODUCTION);
        $taskPriorityIds = \yii\helpers\ArrayHelper::map(
                    TaskPriorityQuery::getBySymbol([Utility::TASK_PRIORITY_MEDIUM, Utility::TASK_PRIORITY_HIGH]),
                'symbol', 'id');
        
        $task1 = new Task();
        $task1->task_type_id = $taskTypeId['id'];
        $task1->task_priority_id = $taskPriorityIds[Utility::TASK_PRIORITY_MEDIUM];
        $task1->user_id = Yii::$app->user->id;
        $task1->title = Yii::t('main', 'Production task');
        $task1->message = 'abc1';
        if(!$task1->save()) {
            throw new \Exception('Task cannot be saved: '.print_r($task1->getErrors(), true));
        }
        
        $prodTask1 = new ProductionTask();
        $prodTask1->task_id = $task1->id;
        $prodTask1->order_offered_product_id = $order->orderOfferedProducts[0]->id;
        $prodTask1->production_path_step_id = 1;
        $prodTask1->is_available = 1;
        if(!$prodTask1->save()) {
            throw new \Exception('Production task cannot be saved: '.print_r($prodTask1->getErrors(), true));
        }
        
        $task2 = new Task();
        $task2->task_type_id = $taskTypeId['id'];
        $task2->task_priority_id = $taskPriorityIds[Utility::TASK_PRIORITY_MEDIUM];
        $task2->user_id = Yii::$app->user->id;
        $task2->title = Yii::t('main', 'Production task');
        $task2->message = 'abc1';
        if(!$task2->save()) {
            throw new \Exception('Task cannot be saved: '.print_r($task2->getErrors(), true));
        }
        
        $prodTask2 = new ProductionTask();
        $prodTask2->task_id = $task2->id;
        $prodTask2->order_offered_product_id = $order->orderOfferedProducts[0]->id;
        $prodTask2->production_path_step_id = 7;
        $prodTask2->is_available = 0;
        if(!$prodTask2->save()) {
            throw new \Exception('Production task cannot be saved: '.print_r($prodTask2->getErrors(), true));
        }
        
        if($withArtificialNode) {
            $task3 = new Task();
            $task3->task_type_id = $taskTypeId['id'];
            $task3->task_priority_id = $taskPriorityIds[Utility::TASK_PRIORITY_MEDIUM];
            $task3->user_id = Yii::$app->user->id;
            $task3->title = Yii::t('main', 'Production task');
            $task3->message = 'Artificial';
            if(!$task3->save()) {
                throw new \Exception('Artificial task cannot be saved: '.print_r($task3->getErrors(), true));
            }
            
            $prodTask3 = new ProductionTask();
            $prodTask3->task_id = $task2->id;
            $prodTask3->order_offered_product_id = $order->orderOfferedProducts[0]->id;
            $prodTask3->production_path_step_id = 38;
            $prodTask3->is_available = 0;
            if(!$prodTask3->save()) {
                throw new \Exception('Artificial production task cannot be saved: '.print_r($prodTask3->getErrors(), true));
            }
            
            $prodTask1->task_successor_ids = (string)$prodTask3->id;
            $prodTask1->save();
            $prodTask3->task_required_ids = (string)$prodTask1->id;
            $prodTask3->task_successor_ids = (string)$prodTask2->id;
            $prodTask3->save();
            $prodTask2->task_required_ids = (string)$prodTask3->id;
            $prodTask2->save();
            
            return [$prodTask1, $prodTask3, $prodTask2];
        }
        else {
            $prodTask1->task_successor_ids = (string)$prodTask2->id;
            $prodTask1->save();
            $prodTask2->task_required_ids = (string)$prodTask1->id;
            $prodTask2->save();
            
            return [$prodTask1, $prodTask2];
        }
    }
    
    private function generateTwoTasksFollowingByOne($order) {
        $taskTypeId = TaskTypeQuery::getBySymbol(Utility::TASK_TYPE_PRODUCTION);
        $taskPriorityIds = \yii\helpers\ArrayHelper::map(
                TaskPriorityQuery::getBySymbol([Utility::TASK_PRIORITY_MEDIUM, Utility::TASK_PRIORITY_HIGH]),
                'symbol', 'id');
    
        $task1 = new Task();
        $task1->task_type_id = $taskTypeId['id'];
        $task1->task_priority_id = $taskPriorityIds[Utility::TASK_PRIORITY_MEDIUM];
        $task1->user_id = Yii::$app->user->id;
        $task1->title = Yii::t('main', 'Production task');
        $task1->message = 'abc1';
        if(!$task1->save()) {
            throw new \Exception('Task cannot be saved: '.print_r($task1->getErrors(), true));
        }

        $prodTask1 = new ProductionTask();
        $prodTask1->task_id = $task1->id;
        $prodTask1->order_offered_product_id = $order->orderOfferedProducts[0]->id;
        $prodTask1->production_path_step_id = 1;
        $prodTask1->is_available = 1;
        if(!$prodTask1->save()) {
            throw new \Exception('Production task cannot be saved: '.print_r($prodTask1->getErrors(), true));
        }

        $task2 = new Task();
        $task2->task_type_id = $taskTypeId['id'];
        $task2->task_priority_id = $taskPriorityIds[Utility::TASK_PRIORITY_MEDIUM];
        $task2->user_id = Yii::$app->user->id;
        $task2->title = Yii::t('main', 'Production task');
        $task2->message = 'abc1';
        if(!$task2->save()) {
            throw new \Exception('Task cannot be saved: '.print_r($task2->getErrors(), true));
        }

        $prodTask2 = new ProductionTask();
        $prodTask2->task_id = $task2->id;
        $prodTask2->order_offered_product_id = $order->orderOfferedProducts[0]->id;
        $prodTask2->production_path_step_id = 7;
        $prodTask2->is_available = 0;
        if(!$prodTask2->save()) {
            throw new \Exception('Production task cannot be saved: '.print_r($prodTask2->getErrors(), true));
        }

        $task3 = new Task();
        $task3->task_type_id = $taskTypeId['id'];
        $task3->task_priority_id = $taskPriorityIds[Utility::TASK_PRIORITY_MEDIUM];
        $task3->user_id = Yii::$app->user->id;
        $task3->title = Yii::t('main', 'Production task');
        $task3->message = 'abc1';
        if(!$task3->save()) {
            throw new \Exception('Task cannot be saved: '.print_r($task3->getErrors(), true));
        }
        
        $prodTask3 = new ProductionTask();
        $prodTask3->task_id = $task2->id;
        $prodTask3->order_offered_product_id = $order->orderOfferedProducts[0]->id;
        $prodTask3->production_path_step_id = 10;
        $prodTask3->is_available = 0;
        if(!$prodTask3->save()) {
            throw new \Exception('Production task cannot be saved: '.print_r($prodTask3->getErrors(), true));
        }
        
        $prodTask1->task_successor_ids = (string)$prodTask3->id;
        $prodTask2->task_successor_ids = (string)$prodTask3->id;
        $prodTask3->task_required_ids = $prodTask1->id.','.$prodTask2->id;
        $prodTask1->save();
        $prodTask2->save();
        $prodTask3->save();
        
        return [$prodTask1, $prodTask2, $prodTask3];
    }
    
    private function generateOneTaskFollowingByTwo($order, $withAdditionalTask = false) {
        $taskTypeId = TaskTypeQuery::getBySymbol(Utility::TASK_TYPE_PRODUCTION);
        $taskPriorityIds = \yii\helpers\ArrayHelper::map(
                TaskPriorityQuery::getBySymbol([Utility::TASK_PRIORITY_MEDIUM, Utility::TASK_PRIORITY_HIGH]),
                'symbol', 'id');
    
        $task1 = new Task();
        $task1->task_type_id = $taskTypeId['id'];
        $task1->task_priority_id = $taskPriorityIds[Utility::TASK_PRIORITY_MEDIUM];
        $task1->user_id = Yii::$app->user->id;
        $task1->title = Yii::t('main', 'Production task');
        $task1->message = 'abc1';
        if(!$task1->save()) {
            throw new \Exception('Task cannot be saved: '.print_r($task1->getErrors(), true));
        }

        $prodTask1 = new ProductionTask();
        $prodTask1->task_id = $task1->id;
        $prodTask1->order_offered_product_id = $order->orderOfferedProducts[0]->id;
        $prodTask1->production_path_step_id = 1;
        $prodTask1->is_available = 1;
        if(!$prodTask1->save()) {
            throw new \Exception('Production task cannot be saved: '.print_r($prodTask1->getErrors(), true));
        }

        $task2 = new Task();
        $task2->task_type_id = $taskTypeId['id'];
        $task2->task_priority_id = $taskPriorityIds[Utility::TASK_PRIORITY_MEDIUM];
        $task2->user_id = Yii::$app->user->id;
        $task2->title = Yii::t('main', 'Production task');
        $task2->message = 'abc1';
        if(!$task2->save()) {
            throw new \Exception('Task cannot be saved: '.print_r($task2->getErrors(), true));
        }

        $prodTask2 = new ProductionTask();
        $prodTask2->task_id = $task2->id;
        $prodTask2->order_offered_product_id = $order->orderOfferedProducts[0]->id;
        $prodTask2->production_path_step_id = 7;
        $prodTask2->is_available = 0;
        if(!$prodTask2->save()) {
            throw new \Exception('Production task cannot be saved: '.print_r($prodTask2->getErrors(), true));
        }

        $task3 = new Task();
        $task3->task_type_id = $taskTypeId['id'];
        $task3->task_priority_id = $taskPriorityIds[Utility::TASK_PRIORITY_MEDIUM];
        $task3->user_id = Yii::$app->user->id;
        $task3->title = Yii::t('main', 'Production task');
        $task3->message = 'abc1';
        if(!$task3->save()) {
            throw new \Exception('Task cannot be saved: '.print_r($task3->getErrors(), true));
        }

        $prodTask3 = new ProductionTask();
        $prodTask3->task_id = $task2->id;
        $prodTask3->order_offered_product_id = $order->orderOfferedProducts[0]->id;
        $prodTask3->production_path_step_id = 10;
        $prodTask3->is_available = 0;
        if(!$prodTask3->save()) {
            throw new \Exception('Production task cannot be saved: '.print_r($prodTask3->getErrors(), true));
        }

        $prodTask1->task_successor_ids = $prodTask2->id.','.$prodTask3->id;
        $prodTask2->task_required_ids = (string)$prodTask1->id;
        $prodTask3->task_required_ids = (string)$prodTask1->id;
        $prodTask1->save();
        $prodTask2->save();
        $prodTask3->save();
        
        if($withAdditionalTask) {
            $task4 = new Task();
            $task4->task_type_id = $taskTypeId['id'];
            $task4->task_priority_id = $taskPriorityIds[Utility::TASK_PRIORITY_MEDIUM];
            $task4->user_id = Yii::$app->user->id;
            $task4->title = Yii::t('main', 'Production task');
            $task4->message = 'abc1';
            if(!$task4->save()) {
                throw new \Exception('Task cannot be saved: '.print_r($task4->getErrors(), true));
            }
            
            $prodTask4 = new ProductionTask();
            $prodTask4->task_id = $task2->id;
            $prodTask4->order_offered_product_id = $order->orderOfferedProducts[0]->id;
            $prodTask4->production_path_step_id = 11;
            $prodTask4->is_available = 0;
            if(!$prodTask4->save()) {
                throw new \Exception('Production task cannot be saved: '.print_r($prodTask4->getErrors(), true));
            }
            
            $prodTask4->task_successor_ids = (string)$prodTask3->id;
            $prodTask3->task_required_ids = $prodTask1->id.','.$prodTask4->id;
            $prodTask3->save();
            $prodTask4->save();
        }

        return [$prodTask1, $prodTask2, $prodTask3];
    }
}