<?php
namespace frontend\tests\unit\models;

use common\fixtures\DepartmentTypeFixture;
use common\fixtures\DepartmentTypeTranslationFixture;
use frontend\models\DepartmentForm;
use common\fixtures\DepartmentFixture;
use common\fixtures\DepartmentTranslationFixture;
use common\models\ar\Department;
use common\models\ar\DepartmentTranslation;

class DepartmentFormTest extends \Codeception\Test\Unit {
    /**
     * @var \UnitTester
     */
    protected $tester;

    protected function _before() {
    	$this->tester->haveFixtures([
    			'departmentType' => [
    					'class' => DepartmentTypeFixture::className(),
    			],
    			'departmentTypeTranslation' => [
    					'class' => DepartmentTypeTranslationFixture::className(),
    			],
    			'department' => [
    					'class' => DepartmentFixture::className(),
    			],
    			'departmentTranslation' => [
    					'class' => DepartmentTranslationFixture::className(),
    			],
    	]);
    }

    protected function _after() {
    }

    public function testValidateAddScenarioForValid() {
		$model = new DepartmentForm();
		$model->setAddScenario();
		
		$model->name = 'Somehow';
		$model->departmentTypeId = 1;
		$this->assertTrue($model->validate());	
    }
    
    public function testValidateAddScenarioForDuplicatedName() {
    	$model = new DepartmentForm();
    	$model->setAddScenario();
    
    	$model->name = 'Dział kontroli jakości';
    	$model->departmentTypeId = 1;
    	$this->assertFalse($model->validate());
    }
    
    public function testValidateAddScenarioForInvalidName() {
    	$model = new DepartmentForm();
    	$model->setAddScenario();
    
    	$model->name = null;
    	$model->departmentTypeId = 1;
    	$this->assertFalse($model->validate());
    }
    
    public function testValidateAddScenarioForInvalidDepartmentType() {
    	$model = new DepartmentForm();
    	$model->setAddScenario();
    
    	$model->name = 'Anyone';
    	$model->departmentTypeId = null;
    	$this->assertFalse($model->validate());
    }
    
    public function testValidateEditScenarioForValid() {
    	$model = new DepartmentForm();
    	$model->setEditScenario();
    
    	$model->id = 2;
    	$model->name = 'Somehow';
    	$model->departmentTypeId = 1;
    	$this->assertTrue($model->validate());
    }
    
    public function testValidateEditScenarioForDuplicatedNameSame() {
    	$model = new DepartmentForm();
    	$model->setEditScenario();
    
    	$model->id = 1;
    	$model->name = 'Dział kontroli jakości';
    	$model->departmentTypeId = 1;
    	$this->assertTrue($model->validate());
    }
    
    public function testValidateEditScenarioForDuplicatedNameOther() {
    	$model = new DepartmentForm();
    	$model->setEditScenario();
    
    	$model->id = 2;
    	$model->name = 'Dział kontroli jakości';
    	$model->departmentTypeId = 1;
    	$this->assertFalse($model->validate());
    }    
    
    public function testValidateEditScenarioForInvalidName() {
    	$model = new DepartmentForm();
    	$model->setEditScenario();
    
    	$model->id = 2;
    	$model->name = null;
    	$model->departmentTypeId = 1;
    	$this->assertFalse($model->validate());
    }
    
    public function testValidateEditScenarioForInvalidDepartmentType() {
    	$model = new DepartmentForm();
    	$model->setEditScenario();
    
    	$model->id = 2;
    	$model->name = 'Anyone';
    	$model->departmentTypeId = null;
    	$this->assertFalse($model->validate());
    }
    
    public function testValidateForEmpty() {
    	$model = new DepartmentForm();
    	
    	$this->assertFalse($model->validate());
    }
    
    public function testSetAddScenario() {
    	$model = new DepartmentForm();
    	$model->setAddScenario();
    	
    	$actual = $model->isEditScenarioSet();
    	$this->assertFalse($actual);
    }
    
    public function testSetEditScenario() {
    	$model = new DepartmentForm();
    	$model->setEditScenario();
    	 
    	$actual = $model->isEditScenarioSet();
    	$this->assertTrue($actual);
    }
    
    public function testSaveDataAddScenarioForCorrect() {
    	$model = new DepartmentForm();
    	$model->setAddScenario();
    	$model->name = 'Programmers';
    	$model->departmentTypeId = 1;
    	
    	try {
    		$model->saveData();
    		$obj = Department::find()->where(['id' => $model->id])->one();
    		$trans = DepartmentTranslation::find()->where(['department_id' => $obj->id, 'language_id' => 1])->one();
    		
    		$this->assertEquals(1, $obj->department_type_id);
    		$this->assertEquals('Programmers', $trans->name);
    	} 
    	catch(\Exception $e) {
    		$this->fail($e->getTraceAsString());
    	}
    }
    
    public function testSaveDataAddScenarioForDuplicatedName() {
    	$model = new DepartmentForm();
    	$model->setAddScenario();
    	$model->name = 'Dział kontroli jakości';
    	$model->departmentTypeId = 1;
    	 
    	try {
    		$model->saveData();
    		$this->fail();    		
    	}
    	catch(\Exception $e) {
    	}
    }
    
    public function testSaveDataAddScenarioForMissingName() {
    	$model = new DepartmentForm();
    	$model->setAddScenario();
    	$model->name = null;
    	$model->departmentTypeId = 1;
    	 
    	try {
    		$model->saveData();
    		$this->fail();    		
    	}
    	catch(\Exception $e) {
    	}
    }
    
    public function testSaveDataAddScenarioForMissingDepartmentType() {
    	$model = new DepartmentForm();
    	$model->setAddScenario();
    	$model->name = 'Programmers';
    	$model->departmentTypeId = null;
    
    	try {
    		$model->saveData();
    		$this->fail();
    	}
    	catch(\Exception $e) {
    	}
    }
    
    public function testSaveDataEditScenarioForCorrect() {
    	$modelInit = new DepartmentForm();
    	$modelInit->setAddScenario();
    	$modelInit->name = 'Programmers';
    	$modelInit->departmentTypeId = 1;
    	$modelInit->saveData();
    	$id = $modelInit->id;
    	
    	$model = new DepartmentForm();
    	$model->setEditScenario();
    	$model->id = $id;
    	$model->name = 'Programmers2';
    	$model->departmentTypeId = 1;
    
    	try {
    		$model->saveData();
    		$obj = Department::find()->where(['id' => $id])->one();
    		$trans = DepartmentTranslation::find()->where(['department_id' => $obj->id, 'language_id' => 1])->one();
    		
    		$this->assertEquals(1, $obj->department_type_id);
    		$this->assertEquals('Programmers2', $trans->name);
    	}
    	catch(\Exception $e) {
    		$this->fail($e->getMessage());
    	}
    }
    
    public function testSaveDataEditScenarioForCorrectSameData() {
    	$modelInit = new DepartmentForm();
    	$modelInit->setAddScenario();
    	$modelInit->name = 'Programmers';
    	$modelInit->departmentTypeId = 1;
    	$modelInit->saveData();
    	$id = $modelInit->id;
    	 
    	$model = new DepartmentForm();
    	$model->setEditScenario();
    	$model->id = $id;
    	$model->name = 'Programmers';
    	$model->departmentTypeId = 1;
    
    	try {
    		$model->saveData();
    		$obj = Department::find()->where(['id' => $id])->one();
    		$trans = DepartmentTranslation::find()->where(['department_id' => $obj->id, 'language_id' => 1])->one();
    
    		$this->assertEquals(1, $obj->department_type_id);
    		$this->assertEquals('Programmers', $trans->name);
    	}
    	catch(\Exception $e) {
    		$this->fail();
    	}
    }    
    
    public function testSaveDataEditScenarioForDuplicatedName() {
    	$modelInit = new DepartmentForm();
    	$modelInit->setAddScenario();
    	$modelInit->name = 'Programmers';
    	$modelInit->departmentTypeId = null;
    	$modelInit->saveData();
    	$id = $modelInit->id;
    	
    	$model = new DepartmentForm();
    	$model->setEditScenario();
    	$model->id = $id;
    	$model->name = 'Dział kontroli jakości';
    	$model->departmentTypeId = 1;
    	
    	try {
    		$model->saveData();
    		$this->fail();
    	}
    	catch(\Exception $e) {
    	}
    }
    
    public function testSaveDataEditScenarioForMissingName() {
    	$modelInit = new DepartmentForm();
    	$modelInit->setAddScenario();
    	$modelInit->name = 'Programmers';
    	$modelInit->departmentTypeId = null;
    	$modelInit->saveData();
    	$id = $modelInit->id;
    	 
    	$model = new DepartmentForm();
    	$model->setEditScenario();
    	$model->id = $id;
    	$model->name = null;
    	$model->departmentTypeId = 1;
    	 
    	try {
    		$model->saveData();
    		$this->fail();
    	}
    	catch(\Exception $e) {
    	}
    }
    
    public function testSaveDataEditScenarioForMissingDepartmentType() {
    	$modelInit = new DepartmentForm();
    	$modelInit->setAddScenario();
    	$modelInit->name = 'Programmers';
    	$modelInit->departmentTypeId = null;
    	$modelInit->saveData();
    	$id = $modelInit->id;
    
    	$model = new DepartmentForm();
    	$model->setEditScenario();
    	$model->id = $id;
    	$model->name = 'Programmers';
    	$model->departmentTypeId = null;
    
    	try {
    		$model->saveData();
    		$this->fail();
    	}
    	catch(\Exception $e) {
    	}
    }
    
    public function testConstructorForEmptyObject() {
    	$model = new DepartmentForm();
    	
    	$this->assertNull($model->id);
    	$this->assertNull($model->name);
    	$this->assertNull($model->departmentTypeId);
    }
    
    public function testConstructorForObject() {
    	$obj = new Department();
    	$obj->id = 10;
    	$obj->symbol = 'prog';
    	$obj->department_type_id = 1;
    	
    	$model = new DepartmentForm($obj);
    	 
    	$this->assertEquals(10, $model->id);
    	$this->assertEquals('prog', $model->name);
    	$this->assertEquals(1, $model->departmentTypeId);
    }
    
    public function testLoadDataForEmptyObject() {
    	$model = new DepartmentForm();
    	try {
	    	$model->loadData(null);
	    	 
	    	$this->assertNull($model->id);
	    	$this->assertNull($model->name);
	    	$this->assertNull($model->departmentTypeId);
    	}
    	catch(\Exception $e) {
    		$this->fail($e->getMessage());
    	}
    }
    
    public function testLoadDataForObject() {
    	$obj = new Department();
    	$obj->id = 10;
    	$obj->symbol = 'prog';
    	$obj->department_type_id = 1;
    	 
    	$model = new DepartmentForm();
    	$model->loadData($obj);
    
    	$this->assertEquals(10, $model->id);
    	$this->assertEquals('prog', $model->name);
    	$this->assertEquals(1, $model->departmentTypeId);
    }
}