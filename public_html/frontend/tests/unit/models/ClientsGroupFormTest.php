<?php
namespace frontend\tests\unit\models;

use common\fixtures\CompanyFixture;
use common\fixtures\CompanyGroupFixture;
use common\fixtures\CompanyGroupTranslationFixture;
use frontend\models\ClientsGroupForm;
use common\models\ar\CompanyGroup;
use common\fixtures\CompanyGroupSetFixture;
use common\models\ar\CompanyGroupTranslation;
use common\models\ar\CompanyGroupSet;
use yii\db\Query;

class ClientsGroupFormTest extends \Codeception\Test\Unit {
    /**
     * @var \UnitTester
     */
    protected $tester;

    protected function _before() {
    	$this->tester->haveFixtures([
    		'company' => [
    			'class' => CompanyFixture::className(),
    		],
    		'companyGroup' => [
    			'class' => CompanyGroupFixture::className(),
    		],
    		'companyGroupTranslation' => [
    			'class' => CompanyGroupTranslationFixture::className(),
    		],
            'companyGroupSet' => [
                'class' => CompanyGroupSetFixture::className(),  
            ],
    	]);
    }

    protected function _after() {
    }
    
    public function testConstructorForNoGroup() {
        $model = new ClientsGroupForm();
        
        $this->assertEmpty($model->id, 'Id is incorrect');
        $this->assertEmpty($model->name, 'Name is incorrect');
        $this->assertEmpty($model->clientIds, 'ClientIds is incorrect');
    }
    
    public function testConstructorForGroup() {
        $companyGroup = CompanyGroup::findOne(2);
        $model = new ClientsGroupForm($companyGroup);
        
        $this->assertEquals(2, $model->id, 'Id is incorrect');
        $this->assertEquals('Grupa 1', $model->name, 'Name is incorrect');
        $this->assertEmpty($model->clientIds, 'ClientIds is incorrect');
    }
    
    public function testConstructorForGroupWithoutTranslation() {
        $companyGroup = CompanyGroup::findOne(3);
        $model = new ClientsGroupForm($companyGroup);
    
        $this->assertEquals(3, $model->id, 'Id is incorrect');
        $this->assertEquals('group02woTrans', $model->name, 'Name is incorrect');
        $this->assertEmpty($model->clientIds, 'ClientIds is incorrect');
    }
    
    public function testConstructorForGroupWithCompanies() {
        $companyGroup = CompanyGroup::findOne(4);
        $model = new ClientsGroupForm($companyGroup);
    
        $this->assertEquals(4, $model->id, 'Id is incorrect');
        $this->assertEquals('Grupa 3 - z klientami', $model->name, 'Name is incorrect');
        $this->assertNotEmpty($model->clientIds, 'ClientIds is incorrect');
        $this->assertEquals(2, $model->clientIds[0], 'ClientId 0 is incorrect');
    }
    
    public function testValidateAddScenarioWithNoData() {
        $model = new ClientsGroupForm();
        $model->setAddScenario();
        
        $this->assertFalse($model->validate());
    }
    
    public function testValidateAddScenarioWithClientIdsAsNull() {
        $model = new ClientsGroupForm();
        $model->setAddScenario();
        $model->name = 'Another next group';
        $model->clientIds = null;
    
        $this->assertTrue($model->validate());
    }
    
    public function testValidateAddScenarioWithClientIdsAsEmptyArray() {
        $model = new ClientsGroupForm();
        $model->setAddScenario();
        $model->name = 'Another next group';
        $model->clientIds = [];
    
        $this->assertTrue($model->validate());
    }
    
    public function testValidateAddScenarioWithNonArrayClientIds() {
        $model = new ClientsGroupForm();
        $model->setAddScenario();
        $model->name = 'Another next group';
        $model->clientIds = 3;
    
        $this->assertFalse($model->validate());
    }
    
    public function testValidateAddScenarioWithNonIntegerClientIds() {
        $model = new ClientsGroupForm();
        $model->setAddScenario();
        $model->name = 'Another next group';
        $model->clientIds = [ 'company1', 'company2' ];
    
        $this->assertFalse($model->validate());
    }
    
    public function testValidateAddScenarioWithIntegerClientIds() {
        $model = new ClientsGroupForm();
        $model->setAddScenario();
        $model->name = 'Another next group';
        $model->clientIds = [1, 2];
    
        $this->assertTrue($model->validate());
    }
    
    public function testValidateAddScenarioWithDuplicatedName() {
        $model = new ClientsGroupForm();
        $model->setAddScenario();
        $model->name = 'Grupa 1';
    
        $this->assertFalse($model->validate());
    }
    
    public function testValidateEditScenarioWithNoInitData() {
        $model = new ClientsGroupForm(null);
        $model->setEditScenario();
        
        $this->assertFalse($model->validate());
    }
    
    public function testValidateEditScenarioWithNoId() {
        $companyGroup = $this->createExampleGroup();
        
        $model = new ClientsGroupForm($companyGroup);
        $model->setEditScenario();
        $model->id = null;
    
        $this->assertFalse($model->validate());
    }
    
    public function testValidateEditScenarioWithNoName() {
        $companyGroup = $this->createExampleGroup();
    
        $model = new ClientsGroupForm($companyGroup);
        $model->setEditScenario();
        $model->name = null;
    
        $this->assertFalse($model->validate());
    }
    
    public function testValidateEditScenarioWithNoClientIds() {
        $companyGroup = $this->createExampleGroup();
    
        $model = new ClientsGroupForm($companyGroup);
        $model->setEditScenario();
        $model->clientIds = null;
    
        $this->assertTrue($model->validate());
    }
    
    public function testValidateEditScenarioWithChangedClientIds() {
        $companyGroup = $this->createExampleGroup();
    
        $model = new ClientsGroupForm($companyGroup);
        $model->setEditScenario();
        $model->clientIds = [1, 2];
    
        $this->assertTrue($model->validate());
    }
    
    public function testValidateEditScenarioWithDuplicatedName() {
        $companyGroup = $this->createExampleGroup();
    
        $model = new ClientsGroupForm($companyGroup);
        $model->setEditScenario();
        $model->name = 'Grupa 1';
    
        $this->assertFalse($model->validate());
    }
    
    public function testGetClientsDataWithNoClientIds() {
        $model = new ClientsGroupForm();
        $model->clientIds = [];
        
        $actual = $model->getClientsData();
        
        $this->assertEmpty($actual);
    }
    
    public function testGetClientsDataWithNonExistingClientIds() {
        $model = new ClientsGroupForm();
        $maxId = (new Query())->select(['MAX(id)'])->from('company')->scalar();
        $model->clientIds = [$maxId + 1, $maxId + 2];
    
        $actual = $model->getClientsData();
    
        $this->assertEmpty($actual);
    }
    
    public function testGetClientsDataWithExistingSingleClientIds() {
        $model = new ClientsGroupForm();
        $model->clientIds = [1];
    
        $actual = $model->getClientsData();
    
        $this->assertEquals(1, count($actual), 'Count is incorrect');
        $this->assertEquals([1], array_keys($actual[0]), 'Key is incorrect');
        $this->assertEquals(['MindsEater'], array_values($actual[0]), 'Value is incorrect');
    }
    
    public function testGetClientsDataWithExistingMultipleClientIds() {
        $model = new ClientsGroupForm();
        $model->clientIds = [1, 2];
    
        $actual = $model->getClientsData();
    
        $this->assertEquals(2, count($actual), 'Count is incorrect');
        $this->assertEquals([1], array_keys($actual[0]), 'Key 0 is incorrect');
        $this->assertEquals(['MindsEater'], array_values($actual[0]), 'Value 0 is incorrect');
        $this->assertEquals([2], array_keys($actual[1]), 'Key 1 is incorrect');
        $this->assertEquals(['Hortex'], array_values($actual[1]), 'Value 1 is incorrect');
    }
    
    public function testSaveDataForAddScenarioWithoutClientIds() {
        $model = new ClientsGroupForm();
        $model->setAddScenario();
        $model->name = 'Grupa przykładowa';
        $model->clientIds = null;

        try {
            $model->saveData();
            $id = $model->id;
            
            $obj = CompanyGroup::findOne($id);
            $trans = CompanyGroupTranslation::find()->where(['company_group_id' => $obj->id, 'language_id' => 1])->one();
            $clients = CompanyGroupSet::find()->where(['company_group_id' => $obj->id])->all();
            
            $this->assertNotEmpty($obj, 'Object is empty');
            $this->assertEquals(0, $obj->is_predefined, 'Is predefined is incorrect');
            $this->assertEquals('Grupa przykładowa', $trans->name, 'Name is incorrect');
            $this->assertEmpty($clients, 'Client Ids are incorrect');
        }
        catch(\Exception $e) {
            $this->fail($e->getMessage());
        }
    }
    
    public function testSaveDataForAddScenarioWithClientIds() {
        $model = new ClientsGroupForm();
        $model->setAddScenario();
        $model->name = 'Grupa przykładowa';
        $model->clientIds = [1, 2];
    
        try {
            $model->saveData();
            $id = $model->id;
    
            $obj = CompanyGroup::findOne($id);
            $trans = CompanyGroupTranslation::find()->where(['company_group_id' => $obj->id, 'language_id' => 1])->one();
            $clients = CompanyGroupSet::find()->where(['company_group_id' => $obj->id])->all();
    
            $this->assertNotEmpty($obj, 'Object is empty');
            $this->assertEquals(0, $obj->is_predefined, 'Is predefined is incorrect');
            $this->assertEquals('Grupa przykładowa', $trans->name, 'Name is incorrect');
            $this->assertEquals(2, count($clients), 'Client count is incorrect');
            $this->assertEquals(1, $clients[0]->company_id, 'Client 0 company id is incorrect');
            $this->assertEquals(2, $clients[1]->company_id, 'Client 1 company id is incorrect');
        }
        catch(\Exception $e) {
            $this->fail($e->getMessage());
        }
    }
    
    public function testSaveDataForEditScenarioWithoutClientIds() {
        $modelInit = new ClientsGroupForm();
        $modelInit->setAddScenario();
        $modelInit->name = 'Grupa przykładowa';
        $modelInit->clientIds = null;
        $modelInit->saveData();
        $id = $modelInit->id;
        
        $companyGroup = CompanyGroup::findOne($id);
        $model = new ClientsGroupForm($companyGroup);
        $model->setEditScenario();
        $model->name = 'Grupa przykładowa po edycji';
    
        try {
            $model->saveData();
    
            $obj = CompanyGroup::findOne($id);
            $trans = CompanyGroupTranslation::find()->where(['company_group_id' => $obj->id, 'language_id' => 1])->one();
            $clients = CompanyGroupSet::find()->where(['company_group_id' => $obj->id])->all();
    
            $this->assertNotEmpty($obj, 'Object is empty');
            $this->assertEquals(0, $obj->is_predefined, 'Is predefined is incorrect');
            $this->assertEquals('Grupa przykładowa po edycji', $trans->name, 'Name is incorrect');
            $this->assertEmpty($clients, 'Client Ids are incorrect');
        }
        catch(\Exception $e) {
            $this->fail($e->getMessage());
        }
    }
    
    public function testSaveDataForEditScenarioWithAddingClientIds() {
        $modelInit = new ClientsGroupForm();
        $modelInit->setAddScenario();
        $modelInit->name = 'Grupa przykładowa';
        $modelInit->clientIds = null;
        $modelInit->saveData();
        $id = $modelInit->id;
    
        $companyGroup = CompanyGroup::findOne($id);
        $model = new ClientsGroupForm($companyGroup);
        $model->setEditScenario();
        $model->name = 'Grupa przykładowa po edycji';
        $model->clientIds = [2];
    
        try {
            $model->saveData();
    
            $obj = CompanyGroup::findOne($id);
            $trans = CompanyGroupTranslation::find()->where(['company_group_id' => $obj->id, 'language_id' => 1])->one();
            $clients = CompanyGroupSet::find()->where(['company_group_id' => $obj->id])->all();
    
            $this->assertNotEmpty($obj, 'Object is empty');
            $this->assertEquals(0, $obj->is_predefined, 'Is predefined is incorrect');
            $this->assertEquals('Grupa przykładowa po edycji', $trans->name, 'Name is incorrect');
            $this->assertEquals(1, count($clients), 'Client count is incorrect');
            $this->assertEquals(2, $clients[0]->company_id, 'Client 0 company id is incorrect');
        }
        catch(\Exception $e) {
            $this->fail($e->getMessage());
        }
    }
    
    public function testSaveDataForEditScenarioWithRemovingClientIds() {
        $modelInit = new ClientsGroupForm();
        $modelInit->setAddScenario();
        $modelInit->name = 'Grupa przykładowa';
        $modelInit->clientIds = [2];
        $modelInit->saveData();
        $id = $modelInit->id;
    
        $companyGroup = CompanyGroup::findOne($id);
        $model = new ClientsGroupForm($companyGroup);
        $model->setEditScenario();
        $model->name = 'Grupa przykładowa po edycji';
        $model->clientIds = null;
    
        try {
            $model->saveData();
    
            $obj = CompanyGroup::findOne($id);
            $trans = CompanyGroupTranslation::find()->where(['company_group_id' => $obj->id, 'language_id' => 1])->one();
            $clients = CompanyGroupSet::find()->where(['company_group_id' => $obj->id])->all();
    
            $this->assertNotEmpty($obj, 'Object is empty');
            $this->assertEquals(0, $obj->is_predefined, 'Is predefined is incorrect');
            $this->assertEquals('Grupa przykładowa po edycji', $trans->name, 'Name is incorrect');
            $this->assertEquals(0, count($clients), 'Client count is incorrect');
        }
        catch(\Exception $e) {
            $this->fail($e->getMessage());
        }
    }
    
    public function testSaveDataForEditScenarioWithChangingClientIds() {
        $modelInit = new ClientsGroupForm();
        $modelInit->setAddScenario();
        $modelInit->name = 'Grupa przykładowa';
        $modelInit->clientIds = [1];
        $modelInit->saveData();
        $id = $modelInit->id;
    
        $companyGroup = CompanyGroup::findOne($id);
        $model = new ClientsGroupForm($companyGroup);
        $model->setEditScenario();
        $model->name = 'Grupa przykładowa po edycji';
        $model->clientIds = [2];
    
        try {
            $model->saveData();
    
            $obj = CompanyGroup::findOne($id);
            $trans = CompanyGroupTranslation::find()->where(['company_group_id' => $obj->id, 'language_id' => 1])->one();
            $clients = CompanyGroupSet::find()->where(['company_group_id' => $obj->id])->all();
    
            $this->assertNotEmpty($obj, 'Object is empty');
            $this->assertEquals(0, $obj->is_predefined, 'Is predefined is incorrect');
            $this->assertEquals('Grupa przykładowa po edycji', $trans->name, 'Name is incorrect');
            $this->assertEquals(1, count($clients), 'Client count is incorrect');
            $this->assertEquals(2, $clients[0]->company_id, 'Client 0 company id is incorrect');
        }
        catch(\Exception $e) {
            $this->fail($e->getMessage());
        }
    }
    
    private function createExampleGroup() {
        $companyGroup = new CompanyGroup();
        $companyGroup->symbol = 'anotherNextGroup_'.uniqid();
        $companyGroup->save();
        
        $translation = new CompanyGroupTranslation();
        $translation->company_group_id = $companyGroup->id;
        $translation->language_id = 1;
        $translation->name = 'Another next group_'.uniqid();
        $translation->save();
        
        $companyGroupSet = new CompanyGroupSet();
        $companyGroupSet->company_group_id = $companyGroup->id;
        $companyGroupSet->company_id = 2;
        $companyGroupSet->save();
        
        return $companyGroup;
    }
}