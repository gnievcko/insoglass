<?php
namespace frontend\tests\unit\models;

use common\fixtures\ProductAttributeTypeFixture;
use frontend\models\ProductAttributeForm;
use common\models\ar\OfferedProductAttribute;
use common\models\ar\OrderOfferedProductAttribute;
use common\models\ar\ProductionPathStepAttribute;
use common\models\ar\ActivityProductAttribute;

class ProductAttributeFormTest extends \Codeception\Test\Unit {
    /**
     * @var \UnitTester
     */
    protected $tester;

    protected function _before() {
    	$this->tester->haveFixtures([
            'productAttributeType' => [
                'class' => ProductAttributeTypeFixture::className(),
            ],
    	]);
    }

    protected function _after() {
    }
    
    public function testLoadFromObjectsForEmpty() {
        $model = new ProductAttributeForm();
        $objects = [];
        
        $model->loadFromObjects($objects);
        
        $this->assertEmpty($model->fields);
    }
    
    public function testLoadFromObjectsForOfferedProductAttribute() {
        $model = new ProductAttributeForm();
        
        $obj1 = new OfferedProductAttribute();
        $obj1->product_attribute_type_id = 3;
        $obj1->value = 1;
        $obj2 = new OfferedProductAttribute();
        $obj2->product_attribute_type_id = 11;
        $obj2->value = 2;
        $objects = [$obj1, $obj2];
    
        $model->loadFromObjects($objects);
    
        $this->assertEquals(2, count($model->fields), 'Count is incorrect');
        $this->assertEquals([3, 11], array_keys($model->fields), 'Keys are incorrect');
        $this->assertEquals([1, 2], array_values($model->fields), 'Values are incorrect');
    }
    
    public function testLoadFromObjectsForOrderOfferedProductAttribute() {
        $model = new ProductAttributeForm();
    
        $obj1 = new OrderOfferedProductAttribute();
        $obj1->product_attribute_type_id = 7;
        $obj1->value = 1;
        $obj2 = new OrderOfferedProductAttribute();
        $obj2->product_attribute_type_id = 2;
        $obj2->value = 1;
        $obj3 = new OrderOfferedProductAttribute();
        $obj3->product_attribute_type_id = 4;
        $obj3->value = 1;
        $objects = [$obj1, $obj2, $obj3];
    
        $model->loadFromObjects($objects);
    
        $this->assertEquals(3, count($model->fields), 'Count is incorrect');
        $this->assertEquals([7, 2, 4], array_keys($model->fields), 'Keys are incorrect');
        $this->assertEquals([1, 1, 1], array_values($model->fields), 'Values are incorrect');
    }
    
    public function testLoadFromObjectsForProductionPathStepAttribute() {
        $model = new ProductAttributeForm();
    
        $obj1 = new ProductionPathStepAttribute();
        $obj1->product_attribute_type_id = 6;
        $objects = [$obj1];
    
        $model->loadFromObjects($objects);
    
        $this->assertEquals(1, count($model->fields), 'Count is incorrect');
        $this->assertEquals([6], array_keys($model->fields), 'Keys are incorrect');
        $this->assertEquals([1], array_values($model->fields), 'Values are incorrect');
    }
    
    public function testLoadFromObjectsForActivityProductAttribute() {
        $model = new ProductAttributeForm();
    
        $obj1 = new ActivityProductAttribute();
        $obj1->product_attribute_type_id = 2;
        $obj2 = new ActivityProductAttribute();
        $obj2->product_attribute_type_id = 1;
        $objects = [$obj1, $obj2];
    
        $model->loadFromObjects($objects);
    
        $this->assertEquals(2, count($model->fields), 'Count is incorrect');
        $this->assertEquals([2, 1], array_keys($model->fields), 'Keys are incorrect');
        $this->assertEquals([1, 1], array_values($model->fields), 'Values are incorrect');
    }
    
    public function testLoadFromObjectsForUndefinedProductAttributeTypeId() {
        $model = new ProductAttributeForm();
    
        $obj1 = new OfferedProductAttribute();
        $objects = [$obj1];
    
        $model->loadFromObjects($objects);
    
        $this->assertEmpty($model->fields);
    }
    
    public function testLoadFromObjectsForNullProductAttributeTypeId() {
        $model = new ProductAttributeForm();
    
        $obj1 = new OfferedProductAttribute();
        $obj1->product_attribute_type_id = null;
        $objects = [$obj1];
    
        $model->loadFromObjects($objects);
    
        $this->assertEmpty($model->fields);
    }
    
    public function testLoadFromObjectsForManyDifferentObjects() {
        $model = new ProductAttributeForm();
    
        $obj1 = new OfferedProductAttribute();
        $obj1->product_attribute_type_id = 3;
        $obj1->value = 1;
        $obj2 = new OrderOfferedProductAttribute();
        $obj2->product_attribute_type_id = 11;
        $obj2->value = 800;
        $obj3 = new ProductionPathStepAttribute();
        $obj3->product_attribute_type_id = null;
        $obj4 = new ActivityProductAttribute();
        $obj4->product_attribute_type_id = 8;
        $objects = [$obj1, $obj2, $obj3, $obj4];
    
        $model->loadFromObjects($objects);
    
        $this->assertEquals(3, count($model->fields), 'Count is incorrect');
        $this->assertEquals([3, 11, 8], array_keys($model->fields), 'Keys are incorrect');
        $this->assertEquals([1, 800, 1], array_values($model->fields), 'Values are incorrect');
    }
    
    public function testLoadFromObjectsForManyWithSameProductAttributeTypeId() {
        $model = new ProductAttributeForm();
    
        $obj1 = new OfferedProductAttribute();
        $obj1->product_attribute_type_id = 3;
        $obj2 = new OfferedProductAttribute();
        $obj2->product_attribute_type_id = 3;
        $objects = [$obj1, $obj2];
    
        $model->loadFromObjects($objects);
    
        $this->assertEquals(1, count($model->fields), 'Count is incorrect');
        $this->assertEquals([3], array_keys($model->fields), 'Keys are incorrect');
        $this->assertEquals([1], array_values($model->fields), 'Values are incorrect');
    }
    
    public function testConvertFieldsToArrayObjectsForEmptyFields() {
        $model = new ProductAttributeForm();
        $model->fields = [];
        
        $actual = $model->convertFieldsToArrayObjects();
        
        $this->assertEmpty($actual);
    }
    
    public function testConvertFieldsToArrayObjectsForNullFields() {
        $model = new ProductAttributeForm();
        $model->fields = null;
    
        $actual = $model->convertFieldsToArrayObjects();
    
        $this->assertEmpty($actual);
    }
    
    public function testConvertFieldsToArrayObjectsForSinglePositive() {
        $model = new ProductAttributeForm();
        $model->fields = [
            3 => 2,
        ];
    
        $actual = $model->convertFieldsToArrayObjects();
    
        $this->assertEquals(1, count($actual), 'Count is incorrect');
        $this->assertEquals(['product_attribute_type_id' => 3, 'value' => 2], $actual[0], 'Element 0 is incorrect');
    }
    
    public function testConvertFieldsToArrayObjectsForSingleNeutral() {
        $model = new ProductAttributeForm();
        $model->fields = [
            6 => 0,
        ];
    
        $actual = $model->convertFieldsToArrayObjects();
    
        $this->assertEmpty($actual);
    }
    
    public function testConvertFieldsToArrayObjectsForSingleNegative() {
        $model = new ProductAttributeForm();
        $model->fields = [
            1 => -4,
        ];
    
        $actual = $model->convertFieldsToArrayObjects();
    
        $this->assertEmpty($actual);
    }
    
    public function testConvertFieldsToArrayObjectsForSingleString() {
        $model = new ProductAttributeForm();
        $model->fields = [
            2 => 'blah',
        ];
    
        $actual = $model->convertFieldsToArrayObjects();
    
        $this->assertEmpty($actual);
    }
    
    public function testConvertFieldsToArrayObjectsForSingleIntAsString() {
        $model = new ProductAttributeForm();
        $model->fields = [
            2 => '3',
        ];
    
        $actual = $model->convertFieldsToArrayObjects();
    
        $this->assertEquals(1, count($actual), 'Count is incorrect');
        $this->assertEquals(['product_attribute_type_id' => 2, 'value' => 3], $actual[0], 'Element 0 is incorrect');
    }
    
    public function testConvertFieldsToArrayObjectsForSingleFloat() {
        $model = new ProductAttributeForm();
        $model->fields = [
            5 => 3.4,
        ];
    
        $actual = $model->convertFieldsToArrayObjects();
    
        $this->assertEmpty($actual);
    }
    
    public function testConvertFieldsToArrayObjectsForSingleScript() {
        $model = new ProductAttributeForm();
        $model->fields = [
            4 => '<script>alert();</script>',
        ];
    
        $actual = $model->convertFieldsToArrayObjects();
    
        $this->assertEmpty($actual);
    }
    
    public function testConvertFieldsToArrayObjectsForTriplePositive() {
        $model = new ProductAttributeForm();
        $model->fields = [
            1 => 1,
            2 => 3,
            3 => 1,
        ];
    
        $actual = $model->convertFieldsToArrayObjects();
    
        $this->assertEquals(3, count($actual), 'Count is incorrect');
        $this->assertEquals(['product_attribute_type_id' => 1, 'value' => 1], $actual[0], 'Element 0 is incorrect');
        $this->assertEquals(['product_attribute_type_id' => 2, 'value' => 3], $actual[1], 'Element 1 is incorrect');
        $this->assertEquals(['product_attribute_type_id' => 3, 'value' => 1], $actual[2], 'Element 2 is incorrect');
    }
    
    public function testConvertFieldsToArrayObjectsForTriplePositiveNeutralNegative() {
        $model = new ProductAttributeForm();
        $model->fields = [
            1 => 0,
            2 => 1,
            3 => -1,
        ];
    
        $actual = $model->convertFieldsToArrayObjects();
    
        $this->assertEquals(1, count($actual), 'Count is incorrect');
        $this->assertEquals(['product_attribute_type_id' => 2, 'value' => 1], $actual[0], 'Element 0 is incorrect');
    }
    
    public function testConvertFieldsToJsonForEmptyFields() {
        $model = new ProductAttributeForm();
        $model->fields = [];
    
        $actual = $model->convertFieldsToJson();
    
        $this->assertNull($actual);
    }
    
    public function testConvertFieldsToJsonForNullFields() {
        $model = new ProductAttributeForm();
        $model->fields = null;
    
        $actual = $model->convertFieldsToJson();
    
        $this->assertNull($actual);
    }
    
    public function testConvertFieldsToJsonForSinglePositive() {
        $model = new ProductAttributeForm();
        $model->fields = [
            3 => 2,
        ];
    
        $actual = $model->convertFieldsToJson();
    
        $this->assertEquals('[{"id":3,"value":2}]', $actual);
    }
    
    public function testConvertFieldsToJsonForSingleNeutral() {
        $model = new ProductAttributeForm();
        $model->fields = [
            6 => 0,
        ];
    
        $actual = $model->convertFieldsToJson();
    
        $this->assertNull($actual);
    }
    
    public function testConvertFieldsToJsonForSingleNegative() {
        $model = new ProductAttributeForm();
        $model->fields = [
            1 => -4,
        ];
    
        $actual = $model->convertFieldsToJson();
    
        $this->assertNull($actual);
    }
    
    public function testConvertFieldsToJsonForSingleString() {
        $model = new ProductAttributeForm();
        $model->fields = [
            2 => 'blah',
        ];
    
        $actual = $model->convertFieldsToJson();
    
        $this->assertNull($actual);
    }
    
    public function testConvertFieldsToJsonForSingleIntAsString() {
        $model = new ProductAttributeForm();
        $model->fields = [
            2 => '3',
        ];
    
        $actual = $model->convertFieldsToJson();
    
        $this->assertEquals(1, count($actual), 'Count is incorrect');
        $this->assertEquals('[{"id":2,"value":3}]', $actual);
    }
    
    public function testConvertFieldsToJsonForSingleFloat() {
        $model = new ProductAttributeForm();
        $model->fields = [
            5 => 3.4,
        ];
    
        $actual = $model->convertFieldsToJson();
    
        $this->assertNull($actual);
    }
    
    public function testConvertFieldsToJsonForSingleScript() {
        $model = new ProductAttributeForm();
        $model->fields = [
            4 => '<script>alert();</script>',
        ];
    
        $actual = $model->convertFieldsToJson();
    
        $this->assertNull($actual);
    }
    
    public function testConvertFieldsToJsonForTriplePositive() {
        $model = new ProductAttributeForm();
        $model->fields = [
            1 => 1,
            2 => 3,
            3 => 1,
        ];
    
        $actual = $model->convertFieldsToJson();
    
        $this->assertEquals('[{"id":1,"value":1},{"id":2,"value":3},{"id":3,"value":1}]', $actual);
    }
    
    public function testConvertFieldsToJsonForTriplePositiveNeutralNegative() {
        $model = new ProductAttributeForm();
        $model->fields = [
            1 => 0,
            2 => 1,
            3 => -1,
        ];
    
        $actual = $model->convertFieldsToJson();
    
        $this->assertEquals(1, count($actual), 'Count is incorrect');
        $this->assertEquals('[{"id":2,"value":1}]', $actual);
    }
}