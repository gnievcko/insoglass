<?php
namespace frontend\tests\unit\models;

use Yii;
use common\tests\_support\TestHelper;
use common\tests\_support\ProjectHelper;
use common\helpers\Utility;
use common\models\ar\User;
use frontend\models\OrderPriorityForm;
use common\models\aq\OrderPriorityQuery;
use yii\helpers\ArrayHelper;
use common\models\ar\Order;

class OrderPriorityFormTest extends \Codeception\Test\Unit {
    /**
     * @var \UnitTester
     */
    protected $tester;
    protected $helper;
    protected $projectHelper;

    protected function _before() {
    	$this->tester->haveFixtures([
    	    
    	]);
    	
    	$this->helper = new TestHelper();
    	$this->projectHelper = new ProjectHelper();
    }

    protected function _after() {
    }
    
    public function testSaveDataForPriorityWithNullIdAndNullCheckbox() {
        $user = User::findOne(2);
        Yii::$app->user->login($user);
        $order = $this->projectHelper->generateOrder(Utility::ORDER_STATUS_NEW);
        $orderPriorities = ArrayHelper::map(OrderPriorityQuery::getAll(), 'symbol', 'id');
        
        $model = new OrderPriorityForm();
        $model->orderId = $order->id;
        $model->orderPriorityId = null;
        $model->orderPriorityCheckbox = null;
        
        try {
            $model->saveData();
            
            $updatedOrder = Order::findOne($order->id);
            $this->assertEquals($orderPriorities[Utility::ORDER_PRIORITY_MEDIUM], $updatedOrder->order_priority_id);
        }
        catch(\Exception $e) {
            $this->fail($e->getMessage());
        }
    }
    
    public function testSaveDataForPriorityWithHighIdAndNullCheckbox() {
        $user = User::findOne(2);
        Yii::$app->user->login($user);
        $order = $this->projectHelper->generateOrder(Utility::ORDER_STATUS_NEW);
        $orderPriorities = ArrayHelper::map(OrderPriorityQuery::getAll(), 'symbol', 'id');
        
        $model = new OrderPriorityForm();
        $model->orderId = $order->id;
        $model->orderPriorityId = $orderPriorities[Utility::ORDER_PRIORITY_HIGH];
        $model->orderPriorityCheckbox = null;
        
        try {
            $model->saveData();
            
            $updatedOrder = Order::findOne($order->id);
            $this->assertEquals($orderPriorities[Utility::ORDER_PRIORITY_HIGH], $updatedOrder->order_priority_id);
        }
        catch(\Exception $e) {
            $this->fail($e->getMessage());
        }
    }

    public function testSaveDataForPriorityWithMediumIdAndNullCheckbox() {
        $user = User::findOne(2);
        Yii::$app->user->login($user);
        $order = $this->projectHelper->generateOrder(Utility::ORDER_STATUS_NEW);
        $orderPriorities = ArrayHelper::map(OrderPriorityQuery::getAll(), 'symbol', 'id');
    
        $model = new OrderPriorityForm();
        $model->orderId = $order->id;
        $model->orderPriorityId = $orderPriorities[Utility::ORDER_PRIORITY_MEDIUM];
        $model->orderPriorityCheckbox = null;
    
        try {
            $model->saveData();
    
            $updatedOrder = Order::findOne($order->id);
            $this->assertEquals($orderPriorities[Utility::ORDER_PRIORITY_MEDIUM], $updatedOrder->order_priority_id);
        }
        catch(\Exception $e) {
            $this->fail($e->getMessage());
        }
    }
    
    public function testSaveDataForPriorityWithMediumIdAndMediumCheckbox() {
        $user = User::findOne(2);
        Yii::$app->user->login($user);
        $order = $this->projectHelper->generateOrder(Utility::ORDER_STATUS_NEW);
        $orderPriorities = ArrayHelper::map(OrderPriorityQuery::getAll(), 'symbol', 'id');
    
        $model = new OrderPriorityForm();
        $model->orderId = $order->id;
        $model->orderPriorityId = $orderPriorities[Utility::ORDER_PRIORITY_MEDIUM];
        $model->orderPriorityCheckbox = 0;
    
        try {
            $model->saveData();
    
            $updatedOrder = Order::findOne($order->id);
            $this->assertEquals($orderPriorities[Utility::ORDER_PRIORITY_MEDIUM], $updatedOrder->order_priority_id);
        }
        catch(\Exception $e) {
            $this->fail($e->getMessage());
        }
    }
    
    public function testSaveDataForPriorityWithMediumIdAndHighCheckbox() {
        $user = User::findOne(2);
        Yii::$app->user->login($user);
        $order = $this->projectHelper->generateOrder(Utility::ORDER_STATUS_NEW);
        $orderPriorities = ArrayHelper::map(OrderPriorityQuery::getAll(), 'symbol', 'id');
    
        $model = new OrderPriorityForm();
        $model->orderId = $order->id;
        $model->orderPriorityId = $orderPriorities[Utility::ORDER_PRIORITY_MEDIUM];
        $model->orderPriorityCheckbox = 1;
    
        try {
            $model->saveData();
    
            $updatedOrder = Order::findOne($order->id);
            $this->assertEquals($orderPriorities[Utility::ORDER_PRIORITY_MEDIUM], $updatedOrder->order_priority_id);
        }
        catch(\Exception $e) {
            $this->fail($e->getMessage());
        }
    }
    
    public function testSaveDataForPriorityWithHighIdAndMediumCheckbox() {
        $user = User::findOne(2);
        Yii::$app->user->login($user);
        $order = $this->projectHelper->generateOrder(Utility::ORDER_STATUS_NEW);
        $orderPriorities = ArrayHelper::map(OrderPriorityQuery::getAll(), 'symbol', 'id');
    
        $model = new OrderPriorityForm();
        $model->orderId = $order->id;
        $model->orderPriorityId = $orderPriorities[Utility::ORDER_PRIORITY_HIGH];
        $model->orderPriorityCheckbox = 0;
    
        try {
            $model->saveData();
    
            $updatedOrder = Order::findOne($order->id);
            $this->assertEquals($orderPriorities[Utility::ORDER_PRIORITY_HIGH], $updatedOrder->order_priority_id);
        }
        catch(\Exception $e) {
            $this->fail($e->getMessage());
        }
    }
    
    public function testSaveDataForPriorityWithHighIdAndHighCheckbox() {
        $user = User::findOne(2);
        Yii::$app->user->login($user);
        $order = $this->projectHelper->generateOrder(Utility::ORDER_STATUS_NEW);
        $orderPriorities = ArrayHelper::map(OrderPriorityQuery::getAll(), 'symbol', 'id');
    
        $model = new OrderPriorityForm();
        $model->orderId = $order->id;
        $model->orderPriorityId = $orderPriorities[Utility::ORDER_PRIORITY_HIGH];
        $model->orderPriorityCheckbox = 1;
    
        try {
            $model->saveData();
    
            $updatedOrder = Order::findOne($order->id);
            $this->assertEquals($orderPriorities[Utility::ORDER_PRIORITY_HIGH], $updatedOrder->order_priority_id);
        }
        catch(\Exception $e) {
            $this->fail($e->getMessage());
        }
    }
    
    public function testSaveDataForPriorityWithNullIdAndMediumCheckbox() {
        $user = User::findOne(2);
        Yii::$app->user->login($user);
        $order = $this->projectHelper->generateOrder(Utility::ORDER_STATUS_NEW);
        $orderPriorities = ArrayHelper::map(OrderPriorityQuery::getAll(), 'symbol', 'id');
    
        $model = new OrderPriorityForm();
        $model->orderId = $order->id;
        $model->orderPriorityId = null;
        $model->orderPriorityCheckbox = 0;
    
        try {
            $model->saveData();
    
            $updatedOrder = Order::findOne($order->id);
            $this->assertEquals($orderPriorities[Utility::ORDER_PRIORITY_MEDIUM], $updatedOrder->order_priority_id);
        }
        catch(\Exception $e) {
            $this->fail($e->getMessage());
        }
    }
    
    public function testSaveDataForPriorityWithNullIdAndHighCheckbox() {
        $user = User::findOne(2);
        Yii::$app->user->login($user);
        $order = $this->projectHelper->generateOrder(Utility::ORDER_STATUS_NEW);
        $orderPriorities = ArrayHelper::map(OrderPriorityQuery::getAll(), 'symbol', 'id');
    
        $model = new OrderPriorityForm();
        $model->orderId = $order->id;
        $model->orderPriorityId = null;
        $model->orderPriorityCheckbox = 1;
    
        try {
            $model->saveData();
    
            $updatedOrder = Order::findOne($order->id);
            $this->assertEquals($orderPriorities[Utility::ORDER_PRIORITY_HIGH], $updatedOrder->order_priority_id);
        }
        catch(\Exception $e) {
            $this->fail($e->getMessage());
        }
    }
}