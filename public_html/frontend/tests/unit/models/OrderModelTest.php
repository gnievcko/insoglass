<?php
namespace frontend\tests\unit\models;

use Yii;
use common\fixtures\OfferedProductFixture;
use common\fixtures\OfferedProductTranslationFixture;
use common\tests\_support\TestHelper;
use common\tests\_support\ProjectHelper;
use common\models\ar\User;
use frontend\models\OrderModel;
use frontend\models\OrderCreateForm;
use yii\db\Query;
use frontend\models\ProductAttributeForm;
use frontend\models\OrderProductForm;
use common\fixtures\CityFixture;
use common\fixtures\AddressFixture;
use common\fixtures\EntityFixture;
use common\fixtures\CompanyFixture;
use common\fixtures\ProductAttributeTypeFixture;
use common\models\ar\OrderPriority;
use common\helpers\Utility;

class OrderModelTest extends \Codeception\Test\Unit {
    /**
     * @var \UnitTester
     */
    protected $tester;
    protected $helper;
    protected $projectHelper;

    protected function _before() {
    	$this->tester->haveFixtures([
    	    'city' => [
    	        'class' => CityFixture::className(),
    	    ],
    	    'address' => [
    	        'class' => AddressFixture::className(),
    	    ],
    	    'entity' => [
    	        'class' => EntityFixture::className(),
    	    ],
    	    'company' => [
    	        'class' => CompanyFixture::className(),
    	    ],
    	    'offeredProduct' => [
    	        'class' => OfferedProductFixture::className(),
    	    ],
    	    'offeredProductTranslation' => [
    	        'class' => OfferedProductTranslationFixture::className(),
    	    ],
    	    'productAttributeType' => [
    	        'class' => ProductAttributeTypeFixture::className(),
    	    ],
    	]);
    	
    	$this->helper = new TestHelper();
    	$this->projectHelper = new ProjectHelper();
    }

    protected function _after() {
    }
    
    public function testSaveNewOrderForNoProduct() {
        $user = User::findOne(2);
        Yii::$app->user->login($user);
    
        $model = new OrderModel(OrderModel::SCENARIO_CREATE);
        $model = $this->generateNewOrderData($model, 0, []);
    
        $method = $this->helper->getMethodForInstance($model, 'saveNewOrder');
    
        try {
            $method->invoke($model, [], false);
    
            $actual = $this->projectHelper->getLastOrder();
            $normalOrderPriority = OrderPriority::find()->where(['symbol' => Utility::ORDER_PRIORITY_MEDIUM])->one();
    
            $this->assertNotNull($actual, 'Order does not exist');
            $this->assertEquals(1, $actual->company_id, 'Order company id is incorrect');
            $this->assertEquals($normalOrderPriority->id, $actual->order_priority_id, 'Order priority id is incorrect');
            $this->assertEquals('abc123', $actual->title, 'Order title is incorrect');
            $this->assertEquals(1, $actual->entity_id, 'Order entity in incorrect');
            $this->assertEquals(0, $actual->is_template, 'Order is template is incorrect');
    
            $this->assertEquals(0, count($actual->orderOfferedProducts), 'Order product count is incorrect');
        }
        catch(\Exception $e) {
            $this->fail($e->getMessage());
        }
    }
    
    public function testSaveNewOrderForOneProductWithoutAttributes() {
        $user = User::findOne(2);
        Yii::$app->user->login($user);
        
        $model = new OrderModel(OrderModel::SCENARIO_CREATE);
        $model = $this->generateNewOrderData($model, 1, [[]]);
        
        $method = $this->helper->getMethodForInstance($model, 'saveNewOrder');
        
        try {
            $method->invoke($model, []);
            
            $actual = $this->projectHelper->getLastOrder();
            $normalOrderPriority = OrderPriority::find()->where(['symbol' => Utility::ORDER_PRIORITY_MEDIUM])->one();
            
            $this->assertNotNull($actual, 'Order does not exist');
            $this->assertEquals(1, $actual->company_id, 'Order company id is incorrect');
            $this->assertEquals($normalOrderPriority->id, $actual->order_priority_id, 'Order priority id is incorrect');
            $this->assertEquals('abc123', $actual->title, 'Order title is incorrect');
            $this->assertEquals(1, $actual->entity_id, 'Order entity in incorrect');
            $this->assertEquals(0, $actual->is_template, 'Order is template is incorrect');
            
            $this->assertEquals(1, count($actual->orderOfferedProducts), 'Order product count is incorrect');
            $this->assertEquals(1, $actual->orderOfferedProducts[0]->offered_product_id, 'Order product 0 offered product ID is incorrect');
            $this->assertEquals(3, $actual->orderOfferedProducts[0]->count, 'Order product 0 count is incorrect');
            $this->assertEquals(20.00, $actual->orderOfferedProducts[0]->price, 'Order product 0 price is incorrect');
            $this->assertEquals(1, $actual->orderOfferedProducts[0]->currency_id, 'Order product 0 currency ID is incorrect');
            $this->assertEquals(0.23, $actual->orderOfferedProducts[0]->vat_rate, 'Order product 0 VAT rate is incorrect');
            $this->assertNull($actual->orderOfferedProducts[0]->attribute_value, 'Order product 0 attribute_value is incorrect');
            $this->assertEmpty($actual->orderOfferedProducts[0]->orderOfferedProductAttributes, 'Order product 0 attributes is incorrect');
        }
        catch(\Exception $e) {
            $this->fail($e->getMessage());
        }
    }
    
    public function testSaveNewOrderForOneProductWithAttributes() {
        $user = User::findOne(2);
        Yii::$app->user->login($user);
    
        $model = new OrderModel(OrderModel::SCENARIO_CREATE);
        $model = $this->generateNewOrderData($model, 1, [[2, 4, 8]]);
    
        $method = $this->helper->getMethodForInstance($model, 'saveNewOrder');
        
        try {
            $method->invoke($model, []);
        
            $actual = $this->projectHelper->getLastOrder();
            $normalOrderPriority = OrderPriority::find()->where(['symbol' => Utility::ORDER_PRIORITY_MEDIUM])->one();
        
            $this->assertNotNull($actual, 'Order does not exist');
            $this->assertEquals(1, $actual->company_id, 'Order company id is incorrect');
            $this->assertEquals($normalOrderPriority->id, $actual->order_priority_id, 'Order priority id is incorrect');
            $this->assertEquals('abc123', $actual->title, 'Order title is incorrect');
            $this->assertEquals(1, $actual->entity_id, 'Order entity in incorrect');
            $this->assertEquals(0, $actual->is_template, 'Order is template is incorrect');
        
            $this->assertEquals(1, count($actual->orderOfferedProducts), 'Order product count is incorrect');
            $this->assertEquals(1, $actual->orderOfferedProducts[0]->offered_product_id, 'Order product 0 offered product ID is incorrect');
            $this->assertEquals(3, $actual->orderOfferedProducts[0]->count, 'Order product 0 count is incorrect');
            $this->assertEquals(20.00, $actual->orderOfferedProducts[0]->price, 'Order product 0 price is incorrect');
            $this->assertEquals(1, $actual->orderOfferedProducts[0]->currency_id, 'Order product 0 currency ID is incorrect');
            $this->assertEquals(0.23, $actual->orderOfferedProducts[0]->vat_rate, 'Order product 0 VAT rate is incorrect');
            $this->assertEquals('[{"id":2,"value":1},{"id":4,"value":1},{"id":8,"value":1}]', $actual->orderOfferedProducts[0]->attribute_value, 'Order product 0 attribute_value is incorrect');
            
            $attributes0 = (new Query())->select(['product_attribute_type_id'])->from('order_offered_product_attribute')
                    ->where(['order_offered_product_id' => intval($actual->orderOfferedProducts[0]->id)])
                    ->orderBy(['product_attribute_type_id' => SORT_ASC])->column();
            
            $this->assertEquals(3, count($attributes0), 'Order product 0 attributes count is incorrect');
            $this->assertEquals(2, $attributes0[0], 'Order product 0 attributes 0 id is incorrect');
            $this->assertEquals(4, $attributes0[1], 'Order product 0 attributes 1 id is incorrect');
            $this->assertEquals(8, $attributes0[2], 'Order product 0 attributes 2 id is incorrect');
        }
        catch(\Exception $e) {
            $this->fail($e->getMessage());
        }
    }
    
    public function testSaveNewOrderForTwoProductsOneWithAttributes() {
        $user = User::findOne(2);
        Yii::$app->user->login($user);
    
        $model = new OrderModel(OrderModel::SCENARIO_CREATE);
        $model = $this->generateNewOrderData($model, 2, [[1, 3], []]);
    
        $method = $this->helper->getMethodForInstance($model, 'saveNewOrder');
    
        try {
            $method->invoke($model, []);
    
            $actual = $this->projectHelper->getLastOrder();
            $normalOrderPriority = OrderPriority::find()->where(['symbol' => Utility::ORDER_PRIORITY_MEDIUM])->one();
    
            $this->assertNotNull($actual, 'Order does not exist');
            $this->assertEquals(1, $actual->company_id, 'Order company id is incorrect');
            $this->assertEquals($normalOrderPriority->id, $actual->order_priority_id, 'Order priority id is incorrect');
            $this->assertEquals('abc123', $actual->title, 'Order title is incorrect');
            $this->assertEquals(1, $actual->entity_id, 'Order entity in incorrect');
            $this->assertEquals(0, $actual->is_template, 'Order is template is incorrect');
    
            $this->assertEquals(2, count($actual->orderOfferedProducts), 'Order product count is incorrect');
            
            $this->assertEquals(1, $actual->orderOfferedProducts[0]->offered_product_id, 'Order product 0 offered product ID is incorrect');
            $this->assertEquals(3, $actual->orderOfferedProducts[0]->count, 'Order product 0 count is incorrect');
            $this->assertEquals(20.00, $actual->orderOfferedProducts[0]->price, 'Order product 0 price is incorrect');
            $this->assertEquals(1, $actual->orderOfferedProducts[0]->currency_id, 'Order product 0 currency ID is incorrect');
            $this->assertEquals(0.23, $actual->orderOfferedProducts[0]->vat_rate, 'Order product 0 VAT rate is incorrect');
            $this->assertEquals('[{"id":1,"value":1},{"id":3,"value":1}]', $actual->orderOfferedProducts[0]->attribute_value, 'Order product 0 attribute_value is incorrect');
    
            $attributes0 = (new Query())->select(['product_attribute_type_id'])->from('order_offered_product_attribute')
                    ->where(['order_offered_product_id' => intval($actual->orderOfferedProducts[0]->id)])
                    ->orderBy(['product_attribute_type_id' => SORT_ASC])->column();
    
            $this->assertEquals(2, count($attributes0), 'Order product 0 attributes count is incorrect');
            $this->assertEquals(1, $attributes0[0], 'Order product 0 attributes 0 id is incorrect');
            $this->assertEquals(3, $attributes0[1], 'Order product 0 attributes 1 id is incorrect');
            
            $this->assertEquals(1, $actual->orderOfferedProducts[1]->offered_product_id, 'Order product 1 offered product ID is incorrect');
            $this->assertEquals(3, $actual->orderOfferedProducts[1]->count, 'Order product 1 count is incorrect');
            $this->assertEquals(20.00, $actual->orderOfferedProducts[1]->price, 'Order product 1 price is incorrect');
            $this->assertEquals(1, $actual->orderOfferedProducts[1]->currency_id, 'Order product 1 currency ID is incorrect');
            $this->assertEquals(0.23, $actual->orderOfferedProducts[1]->vat_rate, 'Order product 1 VAT rate is incorrect');
            $this->assertNull($actual->orderOfferedProducts[1]->attribute_value, 'Order product 1 attribute_value is incorrect');
        }
        catch(\Exception $e) {
            $this->fail($e->getMessage());
        }
    }
    
    public function testSaveNewOrderForTwoProductsBothWithAttributes() {
        $user = User::findOne(2);
        Yii::$app->user->login($user);
    
        $model = new OrderModel(OrderModel::SCENARIO_CREATE);
        $model = $this->generateNewOrderData($model, 2, [[4, 7], [1, 3, 5]]);
    
        $method = $this->helper->getMethodForInstance($model, 'saveNewOrder');
    
        try {
            $method->invoke($model, []);
    
            $actual = $this->projectHelper->getLastOrder();
            $normalOrderPriority = OrderPriority::find()->where(['symbol' => Utility::ORDER_PRIORITY_MEDIUM])->one();
    
            $this->assertNotNull($actual, 'Order does not exist');
            $this->assertEquals(1, $actual->company_id, 'Order company id is incorrect');
            $this->assertEquals($normalOrderPriority->id, $actual->order_priority_id, 'Order priority id is incorrect');
            $this->assertEquals('abc123', $actual->title, 'Order title is incorrect');
            $this->assertEquals(1, $actual->entity_id, 'Order entity in incorrect');
            $this->assertEquals(0, $actual->is_template, 'Order is template is incorrect');
    
            $this->assertEquals(2, count($actual->orderOfferedProducts), 'Order product count is incorrect');
    
            $this->assertEquals(1, $actual->orderOfferedProducts[0]->offered_product_id, 'Order product 0 offered product ID is incorrect');
            $this->assertEquals(3, $actual->orderOfferedProducts[0]->count, 'Order product 0 count is incorrect');
            $this->assertEquals(20.00, $actual->orderOfferedProducts[0]->price, 'Order product 0 price is incorrect');
            $this->assertEquals(1, $actual->orderOfferedProducts[0]->currency_id, 'Order product 0 currency ID is incorrect');
            $this->assertEquals(0.23, $actual->orderOfferedProducts[0]->vat_rate, 'Order product 0 VAT rate is incorrect');
            $this->assertEquals('[{"id":4,"value":1},{"id":7,"value":1}]', $actual->orderOfferedProducts[0]->attribute_value, 'Order product 0 attribute_value is incorrect');
    
            $attributes0 = (new Query())->select(['product_attribute_type_id'])->from('order_offered_product_attribute')
                    ->where(['order_offered_product_id' => intval($actual->orderOfferedProducts[0]->id)])
                    ->orderBy(['product_attribute_type_id' => SORT_ASC])->column();
    
            $this->assertEquals(2, count($attributes0), 'Order product 1 attributes count is incorrect');
            $this->assertEquals(4, $attributes0[0], 'Order product 1 attributes 0 id is incorrect');
            $this->assertEquals(7, $attributes0[1], 'Order product 1 attributes 1 id is incorrect');
    
            $this->assertEquals(1, $actual->orderOfferedProducts[1]->offered_product_id, 'Order product 1 offered product ID is incorrect');
            $this->assertEquals(3, $actual->orderOfferedProducts[1]->count, 'Order product 1 count is incorrect');
            $this->assertEquals(20.00, $actual->orderOfferedProducts[1]->price, 'Order product 1 price is incorrect');
            $this->assertEquals(1, $actual->orderOfferedProducts[1]->currency_id, 'Order product 1 currency ID is incorrect');
            $this->assertEquals(0.23, $actual->orderOfferedProducts[1]->vat_rate, 'Order product 1 VAT rate is incorrect');
            $this->assertEquals('[{"id":1,"value":1},{"id":3,"value":1},{"id":5,"value":1}]', $actual->orderOfferedProducts[1]->attribute_value, 'Order product 1 attribute_value is incorrect');
    
            $attributes1 = (new Query())->select(['product_attribute_type_id'])->from('order_offered_product_attribute')
                    ->where(['order_offered_product_id' => intval($actual->orderOfferedProducts[1]->id)])
                    ->orderBy(['product_attribute_type_id' => SORT_ASC])->column();
    
            $this->assertEquals(3, count($attributes1), 'Order product 1 attributes count is incorrect');
            $this->assertEquals(1, $attributes1[0], 'Order product 1 attributes 0 id is incorrect');
            $this->assertEquals(3, $attributes1[1], 'Order product 1 attributes 1 id is incorrect');
            $this->assertEquals(5, $attributes1[2], 'Order product 1 attributes 2 id is incorrect');
        }
        catch(\Exception $e) {
            $this->fail($e->getMessage());
        }
    }
    
    private function generateNewOrderData($model, $productCount, $attributeIdsToCheck = []) {
        $orderCreateFormField = $this->helper->getFieldForInstance($model, 'orderCreateForm');
        $orderProductsFormsField = $this->helper->getFieldForInstance($model, 'orderProductsForms');
        $productAttributeFormsField = $this->helper->getFieldForInstance($model, 'productAttributeForms');
        
        $orderCreateForm = new OrderCreateForm();
        $orderCreateForm->clientId = 1;
        $orderCreateForm->priorityCheckbox = 0;
        $orderCreateForm->title = 'abc123';
        $orderCreateForm->number = uniqid();
        $orderCreateForm->entityId = 1;
        $orderCreateForm->isTemplate = 0;
        
        $orderCreateFormField->setValue($model, $orderCreateForm);
        
        if(intval($productCount) > 0) {
            $orderProductsForms = [];
            for($i = 0; $i < $productCount; ++$i) {
                $opForm1 = new OrderProductForm();
                $opForm1->productId = 1;
                $opForm1->productType = 'offered-product';
                $opForm1->count = 3;
                $opForm1->price = 20;
                $opForm1->currencyId = 1;
                $opForm1->vatRate = 0.23;
                $orderProductsForms[] = $opForm1;
            }
            
            $productAttributeForms = [];
            $productAttributeTypeIds = (new Query())->select(['id'])->from('product_attribute_type')->orderBy(['id' => SORT_ASC])->column();
            for($i = 0; $i < $productCount; ++$i) {
                $paForm1 = new ProductAttributeForm();
                $paForm1->isRowVisible = 1;
                $paForm1->fields = [];
                
                foreach($productAttributeTypeIds as $patId) {
                    if(in_array($patId, $attributeIdsToCheck[$i])) {
                        $paForm1->fields[$patId] = 1;
                    }
                    else {
                        $paForm1->fields[$patId] = 0;
                    }
                }
                
                $productAttributeForms[] = $paForm1;
            }
            
            $orderProductsFormsField->setValue($model, $orderProductsForms);
            $productAttributeFormsField->setValue($model, $productAttributeForms);
        }
        
        return $model;
    }
}