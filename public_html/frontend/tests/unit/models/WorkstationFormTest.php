<?php
namespace frontend\tests\unit\models;

use frontend\models\WorkstationForm;
use common\models\ar\Workstation;
use common\models\ar\WorkstationTranslation;
use common\models\ar\UserWorkstation;
use common\fixtures\DepartmentTypeFixture;
use common\fixtures\DepartmentTypeTranslationFixture;
use common\fixtures\DepartmentFixture;
use common\fixtures\DepartmentTranslationFixture;
use common\fixtures\CompanyFixture;
use common\fixtures\UserFixture;
use yii\db\Query;
use common\fixtures\WorkstationFixture;
use common\fixtures\WorkstationTranslationFixture;
use common\fixtures\UserWorkstationFixture;

class WorkstationFormTest extends \Codeception\Test\Unit {
    /**
     * @var \UnitTester
     */
    protected $tester;

    protected function _before() {
        $this->tester->haveFixtures([
            'departmentType' => [
                'class' => DepartmentTypeFixture::className(),
            ],
            'departmentTypeTranslation' => [
                'class' => DepartmentTypeTranslationFixture::className(),
            ],
            'department' => [
                'class' => DepartmentFixture::className(),
            ],
            'departmentTranslation' => [
                'class' => DepartmentTranslationFixture::className(),
            ],
            'company' => [
                'class' => CompanyFixture::className(),
            ],
            'user' => [
                'class' => UserFixture::className(),
            ], 
            'workstation' => [
                'class' => WorkstationFixture::className(),  
            ],
            'workstationTranslation' => [
                'class' => WorkstationTranslationFixture::className(),  
            ],
            'userWorkstation' => [
                'class' => UserWorkstationFixture::className(),  
            ],
        ]);
    }

    protected function _after() {
    }

    public function testAttributeLabelsMethodExistence() {
        $model = new WorkstationForm();
        
        $actual = method_exists($model, 'attributeLabels');
        
        $this->assertTrue($actual, 'Method does not exist');
        $this->assertNotEmpty($model->attributeLabels(), 'Labels are empty');
    }
    
    public function testValidateAddScenarioForDuplicatedName() {
        $model = new WorkstationForm();
        $model->setAddScenario();
    
        $model->name = 'Obrabiarka';
        $model->departmentId = 1;
        $this->assertFalse($model->validate());
    }
    
    public function testValidateEditScenarioForDuplicatedNameSame() {
        $model = new WorkstationForm();
        $model->setEditScenario();
    
        $model->id = 1;
        $model->name = 'Obrabiarka';
        $model->departmentId = 1;
        if(!$model->validate()) $this->fail(print_r($model->getErrors(), true));
        $this->assertTrue($model->validate());
    }
    
    public function testValidateEditScenarioForDuplicatedNameOther() {
        $model = new WorkstationForm();
        $model->setEditScenario();
    
        $model->id = 2;
        $model->name = 'Obrabiarka';
        $model->departmentId = 1;
        $this->assertFalse($model->validate());
    }
    
    public function testSaveDataAddScenarioForCorrect() {
        $model = new WorkstationForm();
        $model->setAddScenario();
        $model->name = 'Grinder';
        $model->departmentId = 1;
        $model->workerIds = [ 3, 4 ];
        $model->saveData();
        
        try {
            $model->saveData();
            $id = $model->id;
            $obj = Workstation::find()->where(['id' => $id])->one();
            $trans = WorkstationTranslation::find()->where(['workstation_id' => $obj->id, 'language_id' => 1])->one();
            $workers = UserWorkstation::find()->where(['workstation_id' => $obj->id])->orderBy(['user_id' => SORT_ASC])->all();
        
            $this->assertEquals(1, $obj->department_id, 'Department id is incorrect');
            $this->assertEquals('Grinder', $trans->name, 'Name is incorrect');
            $this->assertEquals(2, count($workers), 'Workers count is incorrect');
            $this->assertEquals(3, $workers[0]->user_id, 'Worker 0 id is incorrect');
            $this->assertEquals(4, $workers[1]->user_id, 'Worker 1 id is incorrect');
        }
        catch(\Exception $e) {
            $this->fail($e->getMessage());
        }
    }
    
    public function testSaveDataAddScenarioForDuplicatedName() {
        $model = new WorkstationForm();
        $model->setAddScenario();
        $model->name = 'Obrabiarka';
        $model->departmentId = 1;
    
        try {
            $model->saveData();
            $this->fail();
        }
        catch(\Exception $e) {
            $this->assertTrue(true);
        }
    }
    
    public function testSaveDataEditScenarioForCorrect() {
        $modelInit = new WorkstationForm();
        $modelInit->setAddScenario();
        $modelInit->name = 'Grinder';
        $modelInit->departmentId = 1;
        $modelInit->workerIds = [ 3, 4 ];
        $modelInit->saveData();        
        $id = $modelInit->id;
         
        $model = new WorkstationForm();
        $model->setEditScenario();
        $model->id = $id;
        $model->departmentId = 1;
        $model->name = 'Bigger grinder';
        $model->workerIds = [ 3, 4 ];
    
        try {
            $model->saveData();
            $obj = Workstation::find()->where(['id' => $id])->one();
            $trans = WorkstationTranslation::find()->where(['workstation_id' => $obj->id, 'language_id' => 1])->one();
            $workers = UserWorkstation::find()->where(['workstation_id' => $obj->id])->orderBy(['user_id' => SORT_ASC])->all();
            
            $this->assertEquals(1, $obj->department_id, 'Department id is incorrect');
            $this->assertEquals('Bigger grinder', $trans->name, 'Name is incorrect');
            $this->assertEquals(2, count($workers), 'Workers count is incorrect');
            $this->assertEquals(3, $workers[0]->user_id, 'Worker 0 id is incorrect');
            $this->assertEquals(4, $workers[1]->user_id, 'Worker 1 id is incorrect');
            $this->assertTrue(true);
        }
        catch(\Exception $e) {
            $this->fail($e->getMessage());
        }
    }
    
    public function testSaveDataEditScenarioForDuplicatedName() {
        $modelInit = new WorkstationForm();
        $modelInit->setAddScenario();
        $modelInit->name = 'Tokarka';
        $modelInit->departmentId = 1;
        $modelInit->saveData();
        $id = $modelInit->id;
         
        $model = new WorkstationForm();
        $model->setEditScenario();
        $model->id = $id;
        $model->departmentId = 1;
        $model->name = 'Obrabiarka';
         
        try {
            $model->saveData();
            $this->fail();
        }
        catch(\Exception $e) {
            $this->assertTrue(true);
        }
    }
}