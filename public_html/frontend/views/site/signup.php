<?php

/* @var $this yii\web\View */
/* @var $form kartik\widgets\ActiveForm */
/* @var $model \frontend\models\RegisterForm */

use common\helpers\Utility;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\JsExpression;
use kartik\widgets\ActiveForm;
use common\components\Captcha;
use kartik\widgets\Select2;
use kartik\dialog\Dialog;

$this->title = Yii::t('web', 'Signup').' - ' . Yii::$app->name;
$this->params['breadcrumbs'][] = Yii::t('web', 'Signup');
?>
<div class="site-signup">
	<div class="row">
		<h1 class="col-sm-6" style="margin-top: 0;"><?= Yii::t('web', 'Signup') ?></h1>
	</div>

	<div id="registration" class="row">
		<div class="col-lg-offset-1 col-lg-10">
			<?php $form = ActiveForm::begin([
					'id' => 'registration-form',
					'type' => 'horizontal',
					'fullSpan' => 9,
					'formConfig' => ['labelSpan' => 3, 'deviceSize' => ActiveForm::SIZE_SMALL],
					'enableAjaxValidation' => true
			]); ?>

				<?=  $form->field($model, 'email', ['addon' => ['prepend' => ['content'=>'@']]]); ?>
				
				<?=	$form->field($model, 'password')->passwordInput(['maxlength' => true]) ?>
				
				<?=	$form->field($model, 'password2')->passwordInput(['maxlength' => true]) ?>

				<div class="form-group">
					<div class="col-sm-offset-3 col-sm-9">
						<?= Html::submitButton(Yii::t('web', 'Sign up'), ['class' => 'btn btn-warning btn-lg', 'name' => 'register-button']) ?>
					</div>
				</div>

			<?php ActiveForm::end(); ?>

			<?= Dialog::widget([
					'dialogDefaults' => [
							Dialog::DIALOG_CONFIRM => [
									'type' => Dialog::TYPE_WARNING,
									'title' => Yii::t('web', 'Information'),
									'btnOKClass' => 'btn-warning',
									'btnOKLabel' => '<span class="' . Dialog::ICON_OK . '"></span> ' . Yii::t('web', 'Yes'),
									'btnCancelLabel' => '<span class="' .Dialog::ICON_CANCEL . '"></span> ' . Yii::t('web', 'Cancel')
							],
					]
			]); ?>
		</div>
	</div>
</div>
