<?php

/* @var $this yii\web\View */
/* @var $name string */
/* @var $message string */
/* @var $exception Exception */

use yii\helpers\Html;

$this->title = $name;
?>
<div class="row site-error">
	<div class="col-sm-8 col-sm-offset-2 col-xs-12 alert-danger text-center">
	    <h1><?= Html::encode($this->title) ?></h1>
		<h4><?= nl2br(Html::encode($message)) ?></h4>


	    <p>
	        <?= Yii::t('web', 'The above error occurred while the Web server was processing your request') ?>.
	    </p>
	    <p>
	        <?= Yii::t('web', 'Please contact us if you think this is a server error. Thank you.') ?>
	    </p>
		<?= Html::a(Yii::t('main', 'Back'), ['dashboard/index'], ['class' => 'btn alert-danger']) ?> 
	</div>
</div>
