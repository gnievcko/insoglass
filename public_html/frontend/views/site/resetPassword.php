<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \frontend\models\ResetPasswordForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use common\widgets\Alert;

$this->title = Yii::t('web', 'Reset password').' - ' . Yii::$app->name;
$this->params['breadcrumbs'][] = Yii::t('web', 'Reset password');
?>
<div class="site-login">
	<div class="col-lg-6 col-lg-offset-3 col-sm-8 col-sm-offset-2 col-xs-10 col-xs-offset-1">
		<?= Alert::widget() ?>
	    <h1><?= Html::encode(Yii::t('web', 'Reset password')) ?></h1>

	 	<?php $form = ActiveForm::begin(['id' => 'reset-password-form']); ?>
	    <div class="col-sm-12">
			<?= $form->field($model, 'password')->passwordInput(['placeHolder' => $model->getAttributeLabel('password')])->label(false) ?>
		</div>
		<div class="col-sm-12">
			<?= $form->field($model, 'password2')->passwordInput(['placeHolder' => $model->getAttributeLabel('password2')])->label(false) ?>
		</div>
	    <div class="form-group">
	    	<?= Html::submitButton(Yii::t('web', 'Change password'), ['class' => 'btn btn-primary']) ?>
	    </div>

	    <?php ActiveForm::end(); ?>

    </div>
</div>
