<?php
use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\View;

$this->title = Yii::t('web', 'List of alerts') . ' - ' . Yii::$app->name;

$this->registerJsFile('@web/js/dynamic-table.js?'.uniqid(), []);
$this->registerJsFile('@web/js/pages/alert-short-list.js?'.uniqid(), ['depends' => [\yii\web\JqueryAsset::className()]]);
?>

<div id="alert-short-list-wrapper">
    <div id="alert-short-list-table" class="dynamic-table">
        <div class="row dynamic-table-filters">
            <div class="col-xs-3">
                    <div class="form-group">
                		<?=
                			Html::activeDropDownList($listForm, 'alertTypeId', $alertTypes, ['class' => 'dynamic-table-filter form-control']);
                		?>
                    </div>
            </div>
            <div class="col-xs-6 text-center"><p class="alert-title"><?= Yii::t('web', 'List of alerts') ?></p></div>
        </div>
        <div class="dynamic-table-data">
            <?= Yii::$app->controller->renderPartial('_alerts', [
	                'items' => $items,
	                'pages' => $pages,
	                'listForm' => $listForm,
            ]) ?>
        </div>
    </div>
    <?php $this->registerJs('dynamicTable("alert-short-list-table", "'.Url::to(['alert/short-list']).'", {url: "'.(Yii::$app->request->get('dashboard') ? Url::to(['dashboard/index']) : null).'"});', View::POS_END); ?>
</div>

<?php
$this->registerJs('initDynamicFeatures("'.Url::to(['alert/']).'");', View::POS_END);
?>