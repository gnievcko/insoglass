<?php
use yii\helpers\Html;
use yii\widgets\LinkPager;
use yii\helpers\Url;
use common\alerts\AlertConstants;
use common\helpers\StringHelper;
?>

<?php
if (empty($items)) { ?>
	<label><?= Yii::t('web', 'No search results');?></label>
	<br><br>
<?php }
else { ?>
	<div class="table-responsive">
	    <table class="table">
	        <tbody>
	            <?php foreach($items as $item): ?>
	                <tr data-id="<?= $item['id'] ?>">
	                	<td data-id="<?= $item['id'] ?>">
	                		<!-- <span class="alert-icon"><img src="-->
	                		<?php /* echo
	                			Html::encode($item['prioritySymbol']) == AlertConstants::ALERT_PRIORITY_PROBLEM ? 
	                				Url::to('@web/images/icons/error.png'):
									Url::to('@web/images/icons/warning.png')
	                		*/?>
	                		<!-- "/></span>-->
	                		<span class="alert-icon <?= Html::encode($item['prioritySymbol']) == AlertConstants::ALERT_PRIORITY_PROBLEM ? 'danger-single' : 'warning-single'?>"/></span>
	                		
	                    	<span class="alert-title not-extended <?= 
	                    		Html::encode($item['prioritySymbol']) == AlertConstants::ALERT_PRIORITY_PROBLEM ? 'alert-problem' : '' 
	                    	?>"><?= Html::encode($item['message']) ?></span>
	                    	<div class="alert-details" style="display: none"></div>
	                    	<span class="alert-date pull-right"><?= StringHelper::getFormattedDateFromDate(Html::encode($item['dateCreation']), true) ?></span>
	                    </td>
	                </tr>
	            <?php endforeach ?>
	        </tbody>
	    </table>
	</div>
<?php }?>

<input class="dynamic-table-sort-dir" type="hidden" name="<?= Html::getInputName($listForm, 'sortDir') ?>" value="<?= $listForm->sortDir ?>" />
<input class="dynamic-table-sort-field" type="hidden" name="<?= Html::getInputName($listForm, 'sortField') ?>" value="<?= $listForm->sortField ?>" />
<input class="dynamic-table-page" type="hidden" name="<?= Html::getInputName($listForm, 'page') ?>" value="<?= $listForm->page ?>" />

