<?php
use frontend\helpers\AdditionalFieldHelper;
use frontend\helpers\UserHelper;
?>

<p class="task-details-title"><?= Yii::t('web', 'Task details') ?></p>
<p class="task-details-body">
	<span class="task-header"><?= Yii::t('main', 'Title') ?>:</span> <?= $data['title'] ?><br/>
	<span class="task-header"><?= Yii::t('main', 'Type') ?>:</span> <?= $data['type'] ?><br/>
	<span class="task-header"><?= Yii::t('main', 'Priority') ?>:</span> <?= mb_strtolower($data['priority'], 'UTF-8') ?><br/>
	<span class="task-header"><?= Yii::t('main', 'Detailed task description') ?>:</span><br/> <?= nl2br($data['message']) ?><br/>
	<span class="task-header"><?= Yii::t('main', 'Author') ?>:</span> <?= UserHelper::getPrettyUserName($data['authorEmail'], $data['authorFirstName'], $data['authorLastName']) ?>
	<?php if(!empty($data['notifiedEmail'])) { ?>
		<br/><span class="task-header"><?= Yii::t('main', 'Employee receiving task') ?>:</span> <?= UserHelper::getPrettyUserName($data['notifiedEmail'], $data['notifiedFirstName'], $data['notifiedLastName']) ?>
	<?php } ?>
	<?php if(!empty($data['dateDeadline'])) { ?>
		<br/><span class="task-header"><?= Yii::t('main', 'Deadline date') ?>:</span> <?= $data['dateDeadline']; ?>
	<?php } ?>
	<?php if(!empty($data['dateReminder'])) { ?>
		<br/><span class="task-header"><?= Yii::t('main', 'Reminder date') ?>:</span> <?= $data['dateReminder']; ?>
	<?php } ?>
	<?php if(!empty($data['results'])) { ?>
		<br/><span class="task-header"><?= Yii::t('web', 'Additional data') ?>:</span><br/> <?= AdditionalFieldHelper::getFormattedData($data['results']); ?>
	<?php } ?>
</p>