<?php

use yii\helpers\Url;
use yii\helpers\Html;
use common\components\ActiveForm;
use yii\helpers\ArrayHelper;
use kartik\widgets\Select2;
use yii\web\JsExpression;
use kartik\slider\Slider;
use kartik\datecontrol\DateControl;
use frontend\helpers\AdditionalFieldHelper;
use yii\web\View;
use common\helpers\Utility;

$this->registerCssFile('@web/css/select2.min.css');
$this->registerCssFile('@web/css/select2-bootstrap.min.css');
$this->registerJsFile('@web/js/select2/select2.min.js?' . uniqid(), ['depends' => [\yii\web\JqueryAsset::className()]]);
$this->registerJsFile('@web/js/select2/i18n/' . Yii::$app->language . '.js?' . uniqid(), ['depends' => [\yii\web\JqueryAsset::className()]]);

$subtitle = $model->isEditScenarioSet() ? Yii::t('web', 'Update task data') : Yii::t('web', 'Add task');
$this->title = $subtitle . ' - ' . Yii::$app->name;

$this->params['breadcrumbs'][] = ['label' => Yii::t('web', 'Dashboard'), 'url' => Url::to(['dashboard/index'])];
$this->params['breadcrumbs'][] = $subtitle;
?>

<div class="task-form">
    <h2 class="page-title"><?= $subtitle ?></h2>

    <?php
    $form = ActiveForm::begin([
        'id' => 'task-form-ajax',
        'enableAjaxValidation' => true,
    ]);
    ?>

    <?php
    if($model->isEditScenarioSet()) {
        $form->field($model, 'id')->hiddenInput()->label(false);
    }
    ?>

    <div class="col-xs-12">
    	<div class="row">
            <div class="col-sm-6"><?= $form->field($model, 'title')->textInput(['maxlength' => true, 'placeholder' => Yii::t('main', 'Task title')]) ?></div>
            <div class="col-sm-5 col-sm-offset-1">
                <?php if(!Yii::$app->params['isPlaceholderInputLabel']) { ?>
                    <label class="control-label" for="taskform-priorityvalue-slider"><?= $model->getAttributeLabel('priorityValue') ?></label>
                <?php } ?>
                <?=
                $form->field($model, 'priorityValue')->widget(Slider::className(), [
                    'name' => 'slider-task-priority',
                    'value' => 1,
                    'sliderColor' => Slider::TYPE_GREY,
                    'handleColor' => Slider::TYPE_PRIMARY,
                    'pluginOptions' => [
                        'min' => $priorityData['min'],
                        'step' => $priorityData['step'],
                        'max' => $priorityData['max'],
                        'tooltip' => 'always',
                        'formatter' => new JsExpression('function(val) {' .
                                $priorityData['labels']
                                . '}'),
                    ]
                ])->label(false);
                ?>
            </div>
        </div>
    </div>	

    <div class="col-xs-12">
    	<div class="row">
            <div class="col-sm-6"><?= $form->field($model, 'message')->textarea(['rows' => 5, 'maxlength' => true, 'placeholder' => Yii::t('main', 'Detailed task description')]) ?></div>
            <div class="col-sm-5 col-sm-offset-1">
                <?php
                echo $form->field($model, 'dateDeadline')->widget(DateControl::classname(), [
                    'language' => Yii::$app->language,
                    'type' => DateControl::FORMAT_DATE,
                    'ajaxConversion' => false,
                    'displayFormat' => 'php:Y-m-d',
                    'options' => [
                        'id' => 'dateDeadlineId',
                        'pluginOptions' => [
                            'placeholder' => 'wwre',
                            'autoclose' => true
                        ],
                        'pluginEvents' => [
                            'change' => 'function() {
                        					window.rr();
                        				}',
                        ]
                    ]
                ]);
                ?>		
                <?php
                echo $form->field($model, 'dateReminder')->widget(DateControl::classname(), [
                    'language' => Yii::$app->language,
                    'type' => DateControl::FORMAT_DATE,
                    'ajaxConversion' => false,
                    'displayFormat' => 'php:Y-m-d',
                    'options' => [
                        'id' => 'dateReminderId',
                        'pluginOptions' => [
                            'autoclose' => true,
                        ]
                    ]
                ]);
                ?>
            </div>
        </div>
    </div>

    <div class="col-xs-12">
    	<div class="row">
            <?php if(Yii::$app->user->can(Utility::ROLE_MAIN_SALESMAN) || Yii::$app->user->can(Utility::ROLE_MAIN_STOREKEEPER)) { ?>
                <div class="col-sm-6">
                    <div class="supp-form">
                        <?= Yii::t('main', 'If no one is selected in this field, the assignment is assigned to the author.'); ?>
                    </div>
                    <?php if(!Yii::$app->params['isPlaceholderInputLabel']) { ?>
                        <label class="control-label" for="taskform-usernotifiedid"><?= $model->getAttributeLabel('userNotifiedId') ?></label>
                    <?php } ?>
    
                    <?=
                            $form->field($model, 'userNotifiedId')
                            ->widget(Select2::classname(), [
                                'language' => Yii::$app->language,
                                'options' => ['placeholder' => Yii::t('main', 'Attach an employee')],
                                'data' => $userNotifiedData,
                                'pluginOptions' => [
                                    'allowClear' => true,
                                    'minimumInputLength' => 1,
                                    'ajax' => [
                                        'url' => Url::to(['employee/find']),
                                        'dataType' => 'json',
                                        'delay' => 250,
                                        'data' => new JsExpression('function(params) { return { term:params.term, page:params.page, size: 20 }; }'),
                                        'processResults' => new JsExpression('function (data, params) {
    													params.page = params.page || 1;
    													var results = data.items.map(function(item) { return { id : item.id, name : (item.firstName != null && item.lastName != null) ? (item.firstName + " " + item.lastName) : item.email }; });	
    																				
    													return {
    														results: results,
    														pagination: {
    															more: data.more
    														}
    													};
    												}'),
                                        'cache' => true
                                    ],
                                    'templateResult' => new JsExpression('function(item) { if(item.loading) return item.text; return item.name; }'),
                                    'templateSelection' => new JsExpression('function (item) { return item.name || item.text; }'),
                                ]
                            ])->label(false)
                    ?> 
                </div>
                <?php if(!Yii::$app->params['isPlaceholderInputLabel']) { ?>
                    <label class="control-label" for="taskform-usernotifiedid"><?= $model->getAttributeLabel('userNotifiedId') ?></label>
                <?php } ?>

                <?=
                        $form->field($model, 'userNotifiedId')
                        ->widget(Select2::classname(), [
                            'language' => Yii::$app->language,
                            'options' => ['placeholder' => Yii::t('main', 'Attach an employee')],
                            'data' => $userNotifiedData,
                            'pluginOptions' => [
                                'allowClear' => true,
                                'minimumInputLength' => 1,
                                'ajax' => [
                                    'url' => Url::to(['employee/find']),
                                    'dataType' => 'json',
                                    'delay' => 250,
                                    'data' => new JsExpression('function(params) { return { term:params.term, page:params.page, size: 20 }; }'),
                                    'processResults' => new JsExpression('function (data, params) {
													params.page = params.page || 1;
													var results = data.items.map(function(item) { return { id : item.id, name : (item.firstName != null && item.lastName != null) ? (item.firstName + " " + item.lastName) : item.email }; });	
																				
													return {
														results: results,
														pagination: {
															more: data.more
														}
													};
												}'),
                                    'cache' => true
                                ],
                                'templateResult' => new JsExpression('function(item) { if(item.loading) return item.text; return item.name; }'),
                                'templateSelection' => new JsExpression('function (item) { return item.name || item.text; }'),
                            ]
                        ])->label(false)
                ?>
                <a href="<?= Url::to(['employee/create']); ?>" target="_blank" class="focusable-link  add-item-after-select2 add-item-after-select2-no-required">
                    <span class="glyphicon glyphicon-plus-sign pull-left"></span>
                    <span> <?=Yii::t('web', 'Create new employee')?></span>
                </a>
            </div>
        <?php } else { ?>
            <?= $form->field($model, 'userNotifiedId')->hiddenInput()->label(false); ?>
        <?php } ?>
    </div>
    <div class="col-xs-12">
    	<div class="row">
            <div class="col-sm-6">
                <?= $form->field($model, 'taskTypeId')->dropDownList(ArrayHelper::map($taskTypes, 'id', 'name'), ['placeholder' => Yii::t('main', 'Task type')]) ?>
            </div>
		</div>
    </div>
    <div class="col-xs-12">
    	<div class="row">
            <div class="col-sm-6" id="additional-fields-container">
                <?php echo AdditionalFieldHelper::renderAdditionalFields($model, $form, $fields, 'taskTypeId', $fieldsInitialValues); ?>

                <a href="<?= Url::to(['client/create']); ?>" target="_blank" class="focusable-link  add-item-after-select2 add-item-after-select2-big-margin">
                    <span class="glyphicon glyphicon-plus-sign pull-left"></span>
                    <span> <?=Yii::t('web', 'Create new client')?></span>
                </a>
            </div>
        </div>
    </div>

    <div class="col-xs-12">
        <div class="pull-left"><?=
            Html::a(Yii::t('web', 'Cancel'), !empty(Yii::$app->request->getReferrer()) ? Yii::$app->request->getReferrer() : ['dashboard/index'], ['class' => 'btn btn-default'])
            ?></div>
        <div class="pull-right"><?= Html::submitButton(($model->isEditScenarioSet() ? Yii::t('main', 'Save') : Yii::t('web', 'Add')), ['class' => 'btn btn-primary']); ?></div>
    </div>

    <?php ActiveForm::end(); ?>	
</div>

<?php
$this->registerJs('
				$("#taskform-tasktypeid").on("change", function(e) {
					var taskTypeId = $(this).val();
					var fields = ' . json_encode($fields) . ';
			
					$.each(fields, function(i, v) {
						var componentSelect2 = $("#select2-" + v.taskTypeId + "-" + v.id + "-" + v.symbol);
						componentSelect2.css("display", "none");
						var componentTextInput = $("#text-input-" + v.taskTypeId + "-" + v.id + "-" + v.symbol);
						componentTextInput.css("display", "none");
			
					    if(v.taskTypeId == taskTypeId) {
					        componentSelect2.css("display", "block");
							componentTextInput.css("display", "block");
					    }
					});
				}).change();
			
				$("#dateDeadlineId-disp").attr("placeholder","' . Yii::t('main', 'Choose deadline date') . '");
				$("#dateReminderId-disp").attr("placeholder","' . Yii::t('main', 'Choose reminder date') . '");
			', View::POS_END);

$this->registerJs('
				window.rr = function() {
					var d = new Date($("#dateDeadlineId-disp").val());
                    d.setDate(d.getDate() - 3);
                    var dateToday = new Date();
                    dateToday.setHours(0,0,0,0);
            
                    if(d.getTime() > dateToday.getTime()) {
                    	var dateFormat = d.getFullYear() + "-" + ("0" + (d.getMonth() + 1)).slice(-2) + "-" + ("0" + (d.getDate())).slice(-2);
                    	$("#dateReminderId").val(dateFormat);
                    	$("#dateReminderId-disp-kvdate").kvDatepicker("update", dateFormat);
                    }}
			');
?>
