<?php
use yii\helpers\Html;
use yii\widgets\LinkPager;
use yii\web\View;
use yii\helpers\Url;
use common\helpers\StringHelper;
?>

<?php
if (empty($items)) { ?>
	<label><?= Yii::t('web', 'No search results');?></label>
	<br><br>
<?php }
else { ?>
	<div class="table-responsive">
	    <table class="table">
	        <tbody>
	            <?php foreach($items as $item): ?>
	                <tr data-id="<?= $item['id'] ?>">
	                	<td data-id="<?= $item['id'] ?>" class="<?= !empty($item['isComplete']) ? 'strikeout' : '' ?> <?= $item['isWarning'] ? 'warned' : '' ?>">
	                		<span class="checkbox-completion-column">
	                			<?= Html::checkbox('checkbox-complete-'.$item['id'], !empty($item['isComplete']), [
	                					'class' => 'checkbox-completion',
	                					'data-id' => $item['id'],
	                					'disabled' => !empty($item['isComplete']),
	                			]); ?>
	                		</span>
	                    	<span class="task-title not-extended"><?= Html::encode($item['title']) ?></span>
	                    	<div class="task-details" style="display: none"></div>
	                    	<?php if(!empty($item['dateDeadline'])) { ?>
	                    		<span class="task-date pull-right"><?= StringHelper::getFormattedDateFromDate(Html::encode($item['dateDeadline']), false) ?></span>
	                    	<?php } ?>
	                    </td>
	                    <td>
	                    	<?= Html::a('<span class="base-icon alert-icon task-edit-icon task-edit"></span>', 
	                    			Url::to(['task/edit', 'id' => $item['id']]), 
	                    			['class' => 'task-edit', 'data-id' => $item['id'], 
	                    			'style' => 'display: '.(empty($item['isComplete']) ? 'inline-block' : 'none')]) ?>
	                    	<span class="base-icon alert-icon delete-icon task-deletion" data-id="<?= $item['id'] ?>"
	                    		style="display: <?= !empty($item['isComplete']) ? 'inline-block' : 'none' ?>"></span>
	                    </td>
	                </tr>
	            <?php endforeach ?>
	        </tbody>
	    </table>
	</div>
<?php }?>

<input class="dynamic-table-sort-dir" type="hidden" name="<?= Html::getInputName($listForm, 'sortDir') ?>" value="<?= $listForm->sortDir ?>" />
<input class="dynamic-table-sort-field" type="hidden" name="<?= Html::getInputName($listForm, 'sortField') ?>" value="<?= $listForm->sortField ?>" />
<input class="dynamic-table-page" type="hidden" name="<?= Html::getInputName($listForm, 'page') ?>" value="<?= $listForm->page ?>" />
