<?php
use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\View;

$this->title = Yii::t('web', 'List of tasks') . ' - ' . Yii::$app->name;

$this->registerJsFile('@web/js/dynamic-table.js?'.uniqid(), []);
$this->registerJsFile('@web/js/pages/task-short-list.js?'.uniqid(), ['depends' => [\yii\web\JqueryAsset::className()]]);
?>

<div id="task-short-list-wrapper">
    <div id="task-short-list-table" class="dynamic-table">
        <div class="row dynamic-table-filters">
            <div class="col-xs-3">
                    <div class="form-group">
                		<?=
                			Html::activeDropDownList($listForm, 'taskTypeId', $taskTypes, ['class' => 'dynamic-table-filter form-control']);
                		?>
                    </div>
            </div>
            <div class="col-xs-6 text-center"><p class=""><?= Yii::t('web', 'List of tasks') ?></h2></div>
            <div class="col-xs-3 text-right">
                <?= Html::a('<span class="icon-img plus-icon" aria-hidden="true"></span>', ['task/create'], ['class' => 'base-icon alert-icon add-task-icon']) ?>
            </div>
        </div>
        <div class="dynamic-table-data">
            <?= Yii::$app->controller->renderPartial('_tasks', [
	                'items' => $items,
	                'pages' => $pages,
	                'listForm' => $listForm,
            ]) ?>
        </div>
    </div>

    <?php /*$this->registerJs('
            	var table = new dynamicTable("task-short-list-table", "'.Url::to(['task/short-list']).'", 
            		{url: "'.(Yii::$app->request->get('dashboard') ? Url::to(['dashboard/index']) : null).'"});
            ', View::POS_END)*/ 
    ?>
</div>

<?php
$this->registerJs('
		var table = new dynamicTable("task-short-list-table", "'.Url::to(['task/short-list']).'", 
            		{url: "'.(Yii::$app->request->get('dashboard') ? Url::to(['dashboard/index']) : null).'"});	
		initDynamicFeatures("'.Url::to(['task/']).'", "task-short-list-table");
', View::POS_END);
?>
