<?php

use yii\helpers\Url;
use yii\helpers\Html;
use common\components\ActiveForm;
use common\helpers\Utility;

$subtitle = Yii::t('web', 'Assign quality note');
$this->title = $subtitle . ' - ' . Yii::$app->name;

$this->params['breadcrumbs'][] = ['label' => Yii::t('web', 'Production'), 'url' => Url::to(['production/index'])];
$this->params['breadcrumbs'][] = $subtitle;
?>

<div class="production-task-qa-assignment-form">
    <h2 class="page-title"><?= $subtitle ?></h2>

    <?php $form = ActiveForm::begin([
        'id' => 'production-task-qa-assignment-form-ajax',
        'enableAjaxValidation' => true,
    ]); ?>

    <div class="col-xs-12">
    	<div class="row"><?php 
        	foreach($model->getQaFactorTypes() as $qaFactorType) {
        	    $field = $form->field($model, 'fields['.$qaFactorType['id'].']');
        	    $field->template = '<div class="col-xs-10">
                    <div class="col-xs-3 field-label">{label}</div>
                    <div class="col-xs-5 field-input">{input}</div>
                    <div class="col-xs-2 field-error">{error}</div>
                </div>';
        	    
        	    if($qaFactorType['variableType'] == Utility::QA_FACTOR_TYPE_VARIABLE_TYPE_RANGE_15) {
        	        $field = $field->radioList([1 => 1, 2 => 2, 3 => 3, 4 => 4, 5 => 5]);
        	    }
        	    elseif($qaFactorType['variableType'] == Utility::QA_FACTOR_TYPE_VARIABLE_TYPE_TEXT) {
        	        $field = $field->textarea(['maxLength' => true]);
        	    }
        	
        	    echo $field->label($qaFactorType['name']);
        	}    
        ?></div>
    </div>	

    <div class="col-xs-12">
        <div class="pull-left"><?=
            Html::a(Yii::t('web', 'Cancel'), !empty(Yii::$app->request->getReferrer()) ? Yii::$app->request->getReferrer() : ['dashboard/index'], ['class' => 'btn btn-default'])
            ?></div>
        <div class="pull-right"><?= Html::submitButton(Yii::t('main', 'Save'), ['class' => 'btn btn-primary']); ?></div>
    </div>

    <?php ActiveForm::end(); ?>	
</div>

