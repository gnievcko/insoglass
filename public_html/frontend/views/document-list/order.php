<?php
use yii\bootstrap\Html;
use yii\helpers\Url;
use yii\web\View;
use dosamigos\multiselect\MultiSelect;
use common\helpers\StringHelper;
use kartik\datecontrol\DateControl;
use frontend\models\OrderDocumentListForm;
use yii\web\JqueryAsset;

$title = Yii::t('web', 'History documents').' - '.strtolower(StringHelper::translateOrderToOffer('main', 'Orders'));
$this->title = $title . ' - ' . Yii::$app->name;

$this->registerJsFile('@web/js/dynamic-table.js?'.uniqid(), []);
$this->registerJsFile('@web/js/layout/moreFilters.js?'.uniqid(), ['depends' => [JqueryAsset::className()]]);
?>

<div id="document-list">    
    <div class="row">
        <div class="col-sm-6"><h2 class="page-title"><?= Yii::t('web', $title) ?></h2></div>
    </div>
</div>

<div id="document-list-table" class="dynamic-table">
    <h5><?php echo Yii::t('main', 'Filters') ?></h5>
    <div class="row dynamic-table-filters">  
    	<div class="col-sm-4 cat-select">
            <label><?php echo Yii::t('web', 'Filter by type') ?></label>
            <?php
            $data = $documentListForm->getTypeList();

            if (!empty($data)) {
                //echo $listForm->getAttributeLabel('categoryIds');
                echo MultiSelect::widget([
                    'id' => 'categories',
                    "options" => ['multiple' => 'multiple', 'class' => 'dynamic-table-filter'],
                    'data' => $data,
                    'name' => Html::getInputName($documentListForm, 'type'),
                    'value' => $documentListForm->type,
                    "clientOptions" => [
                        'nonSelectedText' => '',
                        'nSelectedText' => mb_strtolower(Yii::t('main', 'Selected'), 'UTF-8'),
                        'numberDisplayed' => 1,
                        'buttonWidth' => '100%',
                    ],
                ]);
            }
            ?>
        </div>
        <div class="col-sm-4">
            <label><?php echo Yii::t('web', 'Search by number') ?></label>
            <input 
              value="<?= $documentListForm->filterString ?>" 
              class="form-control dynamic-table-filter" 
              type="text"
              placeHolder="<?= Yii::t('web', 'Number') ?>"
              name="<?= Html::getInputName($documentListForm, 'filterString') ?>"/>
        </div> 
        <div class="col-sm-4">
        	<label><?php echo Yii::t('web', 'Search by client') ?></label>
            <input value="<?= $documentListForm->clientName ?>" 
              class="form-control dynamic-table-filter" 
              type="text"
              placeHolder="<?= Yii::t('web', 'Client name') ?>"
              name="<?= Html::getInputName($documentListForm, 'clientName') ?>"/>
        </div>
        <div class="more-filters">
            <div class="col-sm-4">
	            <label><?php echo StringHelper::translateOrderToOffer('web', 'Search by order') ?></label>
	            <input 
	              value="<?= $documentListForm->orderNumber ?>" 
	              class="form-control dynamic-table-filter" 
	              type="text"
	              placeHolder="<?= Yii::t('web', 'Number') ?>"
	              name="<?= Html::getInputName($documentListForm, 'orderNumber') ?>"/>
	        </div> 
            <div class="col-sm-4">
            	<label><?= $documentListForm->getAttributeLabel('dateFrom') ?></label>
            	<?= DateControl::widget([
            			'name' => (new OrderDocumentListForm())->formName().'[dateFrom]',
            			'value' => $documentListForm->dateFrom, 
	                    'language' => \Yii::$app->language,
	                    'type' => DateControl::FORMAT_DATE,
	                    'ajaxConversion' => false,
	                    'displayFormat' => 'php:Y-m-d',
	            		'options' => [
	            		    'placeholder' => $documentListForm->getAttributeLabel('dateFrom')
	            		],
                	    'options' => [
                	        'pluginOptions' => [
                	            'autoclose' => true,
                	        ],
                	        'options' => ['class'=>'form-control dynamic-table-filter']
                	    ],
	                ]);
	            ?>
            </div>
            <div class="col-sm-4">
            	<label><?= $documentListForm->getAttributeLabel('dateTo') ?></label>
            	<?= DateControl::widget([
            			'name' => (new OrderDocumentListForm())->formName().'[dateTo]',
            			'value' => $documentListForm->dateTo, 
	                    'language' => \Yii::$app->language,
	                    'type' => DateControl::FORMAT_DATE,
	                    'ajaxConversion' => false,
	                    'displayFormat' => 'php:Y-m-d',
	            		'options' => [
	            		    'placeholder' => $documentListForm->getAttributeLabel('dateTo')
	            		],
	                    'options' => [
	                        'pluginOptions' => [
                        	    'autoclose' => true
	                        ],
                    		'options' => ['class'=>'form-control dynamic-table-filter'],
	                    ]
	                ]);
	            ?>
            </div>
            <div class="col-sm-4">
                <label><?php echo Yii::t('web', 'Search by author') ?></label>
                <input 
                	value="<?= $documentListForm->author ?>" 
                    class="form-control dynamic-table-filter" 
                    type="text"
                    placeholder="<?= $documentListForm->getAttributeLabel('author') ?>"
                    name="<?= Html::getInputName($documentListForm, 'author') ?>">
            </div>
            <div class="col-sm-4">
            	<label><?php echo Yii::t('main', 'Parent company') ?></label>
		        <input value="<?= $documentListForm->parentClientName ?>" 
		              class="form-control dynamic-table-filter" 
		              type="text"
		              placeHolder="<?= Yii::t('web', 'Client name') ?>"
		              name="<?= Html::getInputName($documentListForm, 'parentClientName') ?>"/>
			</div>
            <div class="col-sm-4">
            	<label><?php echo Yii::t('main', 'Address')?></label>
                    <input
                        value="<?= $documentListForm->address ?>"
                        class="form-control dynamic-table-filter"
                        type="text"
						placeholder="<?= $documentListForm->getAttributeLabel('address') ?>"
                        name="<?= Html::getInputName($documentListForm, 'address') ?>">
			</div>
		</div>
		<?= Yii::$app->controller->renderPartial('/inc/listFiltersButtons', ['isMoreFilters' => true, 'isClearFilters' => true]) ?>
    </div>
    <div class="dynamic-table-data">
        <?=
        Yii::$app->controller->renderPartial('list', [
            'items' => $items,
            'pages' => $pages,
            'documentListForm' => $documentListForm,
            'documentTypeTranslations' => $documentTypeTranslations,
            'sortFields' => $sortFields
        ])
        ?>
    </div>
</div>
<?php 
	$this->registerJs('dynamicTable("document-list-table", "' . Url::to(['document-list/order']) . '");', View::POS_END); 
	$this->registerJs('moreFilters('.Yii::$app->params['isListFiltersVisibleByDefault'].');', View::POS_END);
?>
