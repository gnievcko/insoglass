<?php
use yii\bootstrap\Html;
use yii\helpers\Url;
use yii\web\View;
use dosamigos\multiselect\MultiSelect;

$title = Yii::t('web', 'History documents').' - '.strtolower(Yii::t('web', 'Warehouse'));
$this->title = $title . ' - ' . Yii::$app->name;
$this->registerJsFile('@web/js/dynamic-table.js?'.uniqid(), []);

?>

<div id="warehouse-document-list">    
    <div class="row">
        <div class="col-sm-6"><h2 class="page-title"><?= Yii::t('web', $title) ?></h2></div>
    </div>
</div>

<div id="warehouse-document-list-table" class="dynamic-table">
    <h5><?php echo Yii::t('main', 'Filters') ?></h5>
    <div class="row dynamic-table-filters">  
    	<div class="col-md-3 col-sm-4 cat-select">
            <label><?php echo Yii::t('web', 'Filter by type') ?></label>
            <?php
            $data = $documentListForm->getTypeList();

            if (!empty($data)) {
                //echo $listForm->getAttributeLabel('categoryIds');
                echo MultiSelect::widget([
                    'id' => 'categories',
                    "options" => ['multiple' => 'multiple', 'class' => 'dynamic-table-filter'],
                    'data' => $data,
                    'name' => Html::getInputName($documentListForm, 'type'),
                    'value' => $documentListForm->type,
                    "clientOptions" => [
                        'nonSelectedText' => '',
                        'nSelectedText' => mb_strtolower(Yii::t('main', 'Selected'), 'UTF-8'),
                        'numberDisplayed' => 1,
                        'buttonWidth' => '100%',
                    ],
                ]);
            }
            ?>
        </div>
        <div class="col-md-3 col-sm-4">
            <label><?php echo Yii::t('web', 'Search by number') ?></label>
            <input value="<?= $documentListForm->filterString ?>" 
              class="form-control dynamic-table-filter" 
              type="text"
              placeHolder="<?= Yii::t('web', 'Number') ?>"
              name="<?= Html::getInputName($documentListForm, 'filterString') ?>"/>
        </div> 
        <div class="col-md-3 col-sm-4">
        	<label><?php echo Yii::t('web', 'Search by employee') ?></label>
            <input value="<?= $documentListForm->employeeName ?>" 
              class="form-control dynamic-table-filter" 
              type="text"
              placeHolder="<?= Yii::t('web', 'First or last name') ?>"
              name="<?= Html::getInputName($documentListForm, 'employeeName') ?>"/>
        </div>
        <div class="col-md-3 col-sm-4">
        	<label><?php echo Yii::t('web', 'Search by client') ?></label>
            <input value="<?= $documentListForm->clientName ?>" 
              class="form-control dynamic-table-filter" 
              type="text"
              placeHolder="<?= Yii::t('web', 'Client name') ?>"
              name="<?= Html::getInputName($documentListForm, 'clientName') ?>"/>
        </div>
        <?= Yii::$app->controller->renderPartial('/inc/listFiltersButtons', ['isMoreFilters' => false, 'isClearFilters' => true]) ?>
    </div>
    <div class="dynamic-table-data">
        <?=
        Yii::$app->controller->renderPartial('list', [
            'items' => $items,
            'pages' => $pages,
            'documentListForm' => $documentListForm,
            'documentTypeTranslations' => $documentTypeTranslations,
            'sortFields' => $sortFields,
            
        ])
        ?>
    </div>
</div>
<?php $this->registerJs('dynamicTable("warehouse-document-list-table", "' . Url::to(['document-list/warehouse']) . '");', View::POS_END); ?>
</div>
