<?php

use yii\helpers\Html;
use yii\widgets\LinkPager;
use common\bundles\EnhancedDialogAsset;
use common\widgets\TableHeaderWidget;
use common\documents\DocumentTypeUtility;
use frontend\helpers\UserHelper;
?>

<?php if (empty($items)) { ?>
    <label><?= Yii::t('web', 'No search results'); ?></label>	
    <br><br>				
    <?php
} else {

    $enableEditButton = true;
    ?>
    <div class=" table-responsive">
        <table class="<?= TableHeaderWidget::TABLECLASS ?>">
          <?php echo TableHeaderWidget::widget(['fields' => $sortFields, 'listForm' => $documentListForm]); ?>
            <tbody>
              <?php
                foreach($items as $item):
                    $idD = $item['_id'];
                    $idH = $item['history']['_id'];
                    $number = '';
                    $userOrderingName = '';
                    foreach($item['history']['sections'] as $section) {
                        if(!empty($section['number'])) {
                            $number = $section['number'];
                        }
                        if(!empty($section['userOrderingName'])) {
                            $userOrderingName = $section['userOrderingName'];
                        }
                        if(!empty($section['userConfirmingName'])) {
                            $userOrderingName = $section['userConfirmingName'];
                        }
                    }
                    
                    ?>
                    <tr>           
                        <td><?= isset($documentTypeTranslations[$item['type']]) ? $documentTypeTranslations[$item['type']] : $item['type'] ?></td>   
                        <td><?= date('d.m.Y', $item['history']['createdAt']) ?></td>  
                        <td><?= $number ?></td>
                        <td>
                          <?= (!empty($item['company']) ? (Html::a($item['company']->name, ['client/details', 'id' => $item['company']->id])) : '') ?>
                          <?= $userOrderingName ?>
                        </td>
                        <?php if($documentListForm->name() == 'OrderDocumentList') { ?>
                        	<td>
                        		<?= !empty($item['order']) ? (Html::a($item['order']->number, ['order/details', 'id' => $item['order']->id])) : '' ?>
                        	</td>
	                        <td>
	                            <?= empty($item['author']) ? '' :
	                                Html::a(UserHelper::getPrettyUserName(
	                                        $item['author']->email,
	                                        $item['author']->first_name,
	                                        $item['author']->last_name),
	                                        [
	                                            'employee/details',
	                                            'id' => $item['author']->id
	                                        ]) 
	                            ?>
	                        </td>
                        <?php } ?>
                        <td><?=
                          isset(DocumentTypeUtility::URL[$item['type']]) ? (Html::a(Html::img('@web/images/placeholders/document_icon.png') . ' ' . Yii::t('web', 'See'), [DocumentTypeUtility::URL[$item['type']], 'documentId' => $idD->__toString(), 'historyId' => $idH->__toString()], ['target' => '_blank'])) : ''
                                  . ' ' .
                                  ($enableEditButton && !empty($item['order']) ? (Html::a(Html::img('@web/images/placeholders/document_icon.png') . ' ' . Yii::t('web', 'Edit'), [DocumentTypeUtility::URL[$item['type']], 'documentId' => $idD->__toString(), 'historyId' => $idH->__toString(), 'orderId' => $item['order']->id, 'edit' => true], ['target' => '_blank'])) : '')
                          ?></td>
                    </tr>
                <?php endforeach ?>
            </tbody>
        </table>
    </div>
    <div class="col-sm-12">
		<div class="col-sm-4 text-left dynamic-table-counter">
			<?= Yii::t('web', 'Number of found items: {n}', ['n' => $pages->totalCount]) ?>
		</div>
		<div class="col-sm-8 text-right dynamic-table-pagination">
		    <?= LinkPager::widget(['pagination' => $pages ]) ?>
		</div>
	</div>
<?php } ?>

<input class="dynamic-table-sort-dir" type="hidden" name="<?= Html::getInputName($documentListForm, 'sortDir') ?>" value="<?= $documentListForm->sortDir ?>" />
<input class="dynamic-table-sort-field" type="hidden" name="<?= Html::getInputName($documentListForm, 'sortField') ?>" value="<?= $documentListForm->sortField ?>" />
<input class="dynamic-table-page" type="hidden" name="<?= Html::getInputName($documentListForm, 'page') ?>" value="<?= $documentListForm->page ?>" />

<?php
EnhancedDialogAsset::register($this);
?>
