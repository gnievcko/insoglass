<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\View;
use dosamigos\multiselect\MultiSelect;

$this->title = Yii::t('web', 'List of workstations').' - '.Yii::$app->name;

$this->params['breadcrumbs'][] = ['label' => Yii::t('main', 'Production'), 'url' => Url::to(['production/index'])];
$this->params['breadcrumbs'][] = Yii::t('web', 'List of workstations');

$this->registerJsFile('@web/js/dynamic-table.js?'.uniqid(), []);
?>

<div id="workstations-list-wrapper">
	<div class="row">
		<div class="col-sm-6">
			<h2 class="page-title"><?= Yii::t('web', 'List of workstations')?></h2>
		</div>
		<div class="col-sm-6 text-right"><?= Html::a(Yii::t('web', 'Add workstation'),['workstation/create'], ['class' => 'btn btn-primary'])?></div>
	</div>
	
	<div id="workstation-list-table" class="dynamic-table">
		<h5><?php echo Yii::t('main', 'Filters')?></h5>
		<div class="row dynamic-table-filters">
			<div class="col-md-3 col-sm-4">
				<label><?php echo Yii::t('web', 'Search by name')?></label>
				<input 
					value="<?= $workstationListForm->name?>"
					class="form-control dynamic-table-filter"
					type="text"
					placeholder="<?= $workstationListForm->getAttributeLabel('name')?>"
					name="<?= Html::getInputName($workstationListForm, 'name')?>">
				
			</div>
			<div class="col-md-3 col-sm-4 cat-select">
				<label><?php echo Yii::t('web', 'Search by department')?></label>
					<?php 
					$data = array_reduce($departmentTranslations, function($options, $t) {
					    $options[$t['id']] = Html::encode($t['name']);
					    return $options;
					},[]);

					   if(!empty($data)) {
					       echo MultiSelect::widget([
					           'id' => 'departments',
					           'options' => ['multiple' => 'multiple', 'class' => 'dynamic-table-filter'],
					           'data' => array_map(function($name) { return Html::decode($name); }, $data),
					           'name' => Html::getInputName($workstationListForm, 'departmentId'),
					           'value' => empty($workstationListForm->departmentId) ? array_keys($data) : $workstationListForm->departmentId,
					           "clientOptions" => [
					               'nonSelectedText' => mb_strtolower(Yii::t('web', 'Select department...'), 'UTF-8'),
					               'nSelectedText' => mb_strtolower(Yii::t('web', 'Selected department'), 'UTF-8'),
					               'buttonWidth' => '100%',
					               'numberDisplayed' => 0,
					           ],
					       ]);
					   } 
					?>
			</div>
			<?= Yii::$app->controller->renderPartial('/inc/listFiltersButtons', ['isMoreFilters' => false, 'isClearFilters' => true])?>
		</div>
		<div class="dynamic-table-data">
			<?= Yii::$app->controller->renderPartial('_workstations', [
			    'workstations' => $workstations,
			    'pages' => $pages,
			    'workstationListForm' => $workstationListForm,
			])?>
		</div>
	</div>
	<?php $this->registerJs('dynamicTable("workstation-list-table", "'.Url::to(['workstation/list']).'");', View::POS_END);?>
</div>