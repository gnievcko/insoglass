<?php

use yii\helpers\Html;
use yii\helpers\Url;
use common\components\ActiveForm;
use kartik\widgets\Select2;
use yii\web\JsExpression;
use yii\web\View;
use common\helpers\Utility;

$this->registerCssFile('@web/css/select2.min.css');
$this->registerCssFile('@web/css/select2-bootstrap.min.css');
$this->registerJsFile('@web/js/forms.js?'.uniqid(),['depends' => [yii\web\JqueryAsset::className()]]);
$this->registerJsFile('@web/js/select2/select2.min.js?'.uniqid(), ['depends' => [\yii\web\JqueryAsset::className()]]);
$this->registerJsFile('@web/js/select2/i18n/'.Yii::$app->language.'.js?'.uniqid(), ['depends' => [\yii\web\JqueryAsset::className()]]);

$subtitle = $model->isEditScenarioSet() ? Yii::t('web', 'Update workstation data') : Yii::t('web', 'Add workstation');
$this->title = $subtitle.' - '.Yii::$app->name;

$this->params['breadcrumbs'][] = ['label' => Yii::t('main', 'Production'), 'url' => Url::to(['production/index'])];
$this->params['breadcrumbs'][] = [ 'label' => Yii::t('web', 'List of workstations'), 'url' => Url::to(['workstation/list'])];
$this->params['breadcrumbs'][] = $subtitle;

?>
<div class="workstation-form">
	<h2 class="page-title"><?= $subtitle?></h2>
	
	<?php $form = ActiveForm::begin([
	    'id' => 'workstation-form-ajax',
	    'enableAjaxValidation' => true,
	]); ?>
	
	<?php if($model->isEditScenarioSet()) {
        echo $form->field($model, 'id')->hiddenInput();
    } ?>
    
	<div class="col-xs-12">
		<div class="row">
			<div class="col-sm-6"><?= $form->field($model,'name')->textInput(['class' => 'form-control', 'placeholder' => Yii::t('web', 'Workstation name')])?></div>
		</div>
	</div>
	<div class="col-xs-12">
		<div class="row">
			<div class="col-sm-6">
				<?php 
				    $function = '
                        function (data, params) {
                            params.page = params.page || 1;
                            return {
                                results: data.items,
                                pagination: {   
                                    more: data.more
                                }
                            };
                        }
                    ';
				    echo $form->field($model, 'departmentId')
				        ->widget(Select2::className(), [
				            'language' => Yii::$app->language,
				            'name' =>'select2_department',
				            'options' => [
				                'placeholder' => Yii::t('web', 'Select department...'),
				            ],
				            'data' => $departmentData,
				            'pluginOptions' => [
				                'allowClear' => false,
				                'minimumInputLength' => 1,
				                'tags' => false,
				                'ajax' => [
				                    'url' => Url::to(['department/find']),
				                    'dataType' => 'json',
				                    'delay' => 250,
				                    'data' => new JsExpression('function(params) {
                                     return {
                                         term: params.term,
                                         page: params.page || 1,
                                         size: 20,
                                     }; }'),
				                    'processResults' => new JsExpression($function),
				                    'cache' => true,
				                ],
				                'escapeMarkup' => new JsExpression('function (markup) { return markup; }'),
				                'templateResult' => new JsExpression('function(item) { if(item.loading) return item.text; if(item.id) return item.name; else return item.name; }'),
				                'templateSelection' => new JsExpression('function (item) {if(item.loading) return item.text; return item.text || item.name}'),
				            ]
				        ]); 	
				?>
                <a href="<?= Url::to(['department/create']); ?>" target="_blank" class="focusable-link  add-item-after-select2">
                    <span class="glyphicon glyphicon-plus-sign pull-left"></span>
                    <span> <?=Yii::t('web', 'Create new department')?></span>
                </a>
			</div>
		</div>
	</div>
	<div class="col-xs-12">
		<div class="row">
			<div class="col-sm-6">
				<?php   echo $form->field($model, 'workerIds')
				            ->widget(Select2::className(),[
				                'language' => Yii::$app->language,
				                'name' => 'select2_workers',
				                'options' => ['placeholder' => Yii::t('web', 'Select worker...'), 'multiple' => true],
				                'showToggleAll' => false,
				                'data' => $workersData,
				                'pluginOptions' => [
				                    'allowClear' => false,
				                    'minimalInputLenght' => 1,
				                    'tags' => false,
				                    'ajax' => [
				                        'url' => Url::to(['employee/find']),
				                        'dataType' => 'json',
				                        'delay' => 250,
				                        'cache' => true,
				                        'data' => new JsExpression('function(params) {
                                            return {
                                                term : params.term,
                                                page : params.page || 1,
                                                size : 20,
                                                roles: ["'.implode('","', [Utility::ROLE_WORKER, Utility::ROLE_QA_SPECIALIST, Utility::ROLE_MAIN_WORKER]).'"]
                                        }; }'),
				                        'processResults' => new JsExpression('function(data, params) {
                                            params.page = params.page || 1;
                                            return {
                                                results: data.items,
                                                pagination: {
                                                    more: data.more
                                                }
				                            };
				                         }')
				                    ],
				                    'escapeMarkup' => new JsExpression('function (markup) { return markup; }'),
				                    'templateResult' => new JsExpression('function(item) { if(item.loading) return item.text; if(item.id) return item.firstName+" "+item.lastName; else return item.name; }'),
				                    'templateSelection' => new JsExpression('function (item) {if(item.loading) return item.text; return item.text || item.firstName+" "+item.lastName }'),
				                ],
				            ])
				
				?>
                <a href="<?= Url::to(['employee/create']); ?>" target="_blank" class="focusable-link  add-item-after-select2 add-item-after-select2-no-required">
                    <span class="glyphicon glyphicon-plus-sign pull-left"></span>
                    <span> <?=Yii::t('web', 'Create new employee')?></span>
                </a>

			</div>
		</div>
	</div>
	<div class="col-sm-12">
		<div class="row">
			<div class="pull-left">
				<?= Html::a(Yii::t('web', 'Cancel'),
				    !empty(Yii::$app->request->getReferrer()) ? Yii::$app->request->getReferrer() : Url::to(['workstation/list']),
				    ['class' => 'btn btn-default'])?>
			</div>
			<div class="pull-right">
				<?= Html::submitButton($model->isEditScenarioSet() ? Yii::t('main','Save') : Yii::t('web', 'Add'), ['class' => 'btn btn-primary'])?>
			</div>
		</div>
	</div>
	<?php ActiveForm::end();
	$this->registerJs('
        (function() {
        var dynamicForm = new DynamicForm("workstation-form-ajax");
        dynamicForm.activateRemoveButtons();
        dynamicForm.activateAddItemButtons();
    })();
    ', View::POS_END);
	?>

</div>