<?php 
use common\bundles\EnhancedDialogAsset;
use common\widgets\TableHeaderWidget;
use frontend\models\WorkstationListForm;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\LinkPager;
?>

<?php
if(empty($workstations)) { ?>
	<label><?= \Yii::t('web', 'No search results');?></label>
	<br /><br />
<?php }
    else { ?>
    <div class="table-responsive">
    	<table class="<?= TableHeaderWidget::TABLECLASS?>">
    		<?php echo TableHeaderWidget::widget(['fields' => WorkstationListForm::getSortFields(), 'listForm' => $workstationListForm]);?>
    		<tbody>
    			<?php foreach($workstations as $workstation): ?>
    				<tr>
    					<td><?= Html::encode($workstation['name']);?> </td>
    					<td><?= Html::encode($workstation['departmentName']);?> </td>
    					<td><?php 
    					   echo Html::a('', Url::to(['workstation/details', 'id' => $workstation['id']]),
    					       ['class' => 'base-icon details-icon action-icon', 'title' => Yii::t('web', 'Details')]);
    					   echo '&nbsp;'.Html::a('', Url::to(['workstation/edit', 'id' => $workstation['id']]),
    					       ['class' => 'base-icon editing-icon action-icon', 'title' => Yii::t('web', 'Edit')]);
    					   echo '&nbsp;'.Html::a('', Url::to(['workstation/delete']), [
    					       'class' => 'base-icon delete-icon action-icon',
    					       'title' => Yii::t('web', 'Delete'),
    					       'data' => [
    					           'confirm' => Yii::t('main', 'Are you sure you want to delete this item?'),
    					           'method' => 'post',
    					           'params' => [
    					               'id' => $workstation['id'],
    					           ],
    					       ],
    					   ]);
    					?>
    					</td>
    				</tr>
    			<?php endforeach;?>
    		</tbody>
    	</table>
    </div>
    <div class="col-sm-12">
    	<div class="col-sm-4 text-left dynamic-table-counter">
    		<?= Yii::t('web', 'Number of found items: {n}', ['n' => $pages->totalCount])?>
    	</div>
    	<div class="col-sm-8 text-right dynamic-table-pagination">
    		<?= LinkPager::widget(['pagination' => $pages])?>
    	</div>
    </div>
    <input class="dynamic-table-sort-dir" type="hidden" name="<?= Html::getInputName($workstationListForm, 'sortDir')?>" value="<?= $workstationListForm->sortDir ?>" />
    <input class="dynamic-table-sort-field" type="hidden" name="<?= Html::getInputName($workstationListForm, 'sortField')?>" value="<?= $workstationListForm->sortField ?>" />
    <input class="dynamic-table-page" type="hidden" name="<?= Html::getInputName($workstationListForm, 'page')?>" value="<?= $workstationListForm->page ?>" />
<?php }

EnhancedDialogAsset::register($this);
?>