<?php


use yii\helpers\Html;
use yii\helpers\Url;

$this->title = Yii::t('web', 'Workstation details').' - '.Yii::$app->name;

$this->params['breadcrumbs'][] = ['label' => Yii::t('main', 'Production'), 'url' => Url::to(['production/index'])];
$this->params['breadcrumbs'][] = ['label' => Yii::t('web', 'List of workstations'), 'url' => Url::to(['workstation/list'])];
$this->params['breadcrumbs'][] = Yii::t('web', 'Workstation details');
?>

<div class="workstation-details">
	<div class="row">
		<div class="col-md-7 col-sm-6"><h2 class="page-title"><?= Yii::t('web', 'Workstation details')?></h2></div>
		<div class="col-md-5 col-sm-6 text-right">
			<?php
			    $buttons = '';
			    $referrer = Yii::$app->request->getReferrer();
			    if(substr($referrer, strripos($referrer, '/') + 1) == 'create') {
			        $buttons .= Html::a(Yii::t('web', 'Add another workstation'),
			            ['workstation/create'], ['class' => 'btn btn-primary']);
			    }
			    $buttons .= Html::a(Yii::t('main', 'Update'),
			        ['workstation/edit', 'id' => $details['id']], ['class' => 'btn btn-default']);
			    $buttons .= Html::a(Yii::t('main', 'Delete'),
			        ['workstation/delete'],
			        [
			            'class' => 'btn btn-default',
			            'data' => [
			                'confirm' => Yii::t('main', 'Are you sure you want to delete this item?'),
			                'method' => 'post',
			                'params' => [
			                    'id' => $details['id'],
			                ],
			            ],
			        ]);
			    echo $buttons;
			?>
			</div>
		</div>
	</div>
	<div class="col-xs-12 title-with-icon">
		<div class="base-icon basic-data-icon main-content-icon"></div>
		<h3 class="page-subtitle"><?= Yii::t('web', 'General information')?></h3>
	</div>
	<div class="col-xs-12">
		<div class="table-responsive">
			<table class="table table-bordered detail-table">
				<tbody>
					<tr>
						<th><?= Yii::t('web', 'Name')?></th>
						<td><?= Html::encode($details['name'])?></td>
					</tr>
					<tr>
						<th><?= Yii::t('web', 'Department name')?></th>
						<td><?= Html::encode($details['departmentName'])?></td>
					</tr>
					<?php if(!empty($workers)) { ?>
    					<tr>
    						<th><?= Yii::t('web', 'Workers')?></th>
    						<td><?= $this->render('inc/usersRow', ['users' => $workers])?> </td>
    					</tr>
					<?php }?>
				</tbody>
			</table>
		</div>
	</div>
</div>
