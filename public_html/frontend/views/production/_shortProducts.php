<?php
use yii\helpers\Url;
use yii\helpers\Html;
use frontend\models\DepartmentProductOrderListForm;
use yii\widgets\LinkPager;
use common\bundles\EnhancedDialogAsset;
use common\helpers\StringHelper;
use common\widgets\TableHeaderWidget;
use yii\web\View;

$this->registerJsFile('@web/js/dynamic-table.js?'.uniqid(), []);
$this->registerJsFile('@web/js/hint-tooltip.js?'.uniqid(), ['depends' => [\yii\web\JqueryAsset::className()]]);
?>
<?php 
if (empty($items)) { ?>
	<label><?= Yii::t('web', 'No search results');?></label>
	<br><br>				
<?php }
else { ?>
	<div class="table-responsive">
		<table class="<?= TableHeaderWidget::TABLECLASS ?>">
	    	<?php echo TableHeaderWidget::widget(['fields' => DepartmentProductOrderListForm::getSortFields() , 'listForm' => $listForm, 'action' => true]);?>
	        <tbody>
	            <?php foreach($items as $item): ?>
	                <tr class="clickable-table-row" data-href="<?= Url::to(['production/production-details', 'orderOfferedProductId' => $item['orderOfferedProductId']])?>">
	                    <td><?= Html::encode($item['productCode']) ?></td>
	                    <td><?= Html::encode($item['completedCount']) ?></td>
	                    <td>
	                    	<div class="progress">
    							<div class="progress-bar" role="progressbar" aria-valuenow="<?= Html::encode($item['completionPercent'])?>"
      								aria-valuemin="0" aria-valuemax="100" style="width:<?= Html::encode($item['completionPercent']) . '%' ?>">
        							<?= Html::encode($item['completionPercent']) . '%' ?>
								</div>
							</div>
	                    </td>
	                    <!--<td>TODO</td> -->
	                    <td><?= Html::encode(StringHelper::getFormattedDateFromDate($item['deadline'])) ?></td>  
	                    <td><?php if(!empty($item['qualityNote'])) { 
                            echo Yii::$app->controller->renderPartial('inc/qualityNote', ['rawData' => $item['qualityNote']]); 
                        } ?></td>
	                    <td>
                       		<?= Html::a('', Url::to(['production/production-details', 'orderOfferedProductId' => $item['orderOfferedProductId']]), ['class' => 'base-icon details-icon action-icon', 'title' => Yii::t('web', 'Details')]); ?>
                       		<?php if(array_key_exists($item['orderOfferedProductId'], $qaAssignmentAvailableProducts)) {
                       		    echo Html::a('', Url::to(['qa/create', 'productionTaskId' => $qaAssignmentAvailableProducts[$item['orderOfferedProductId']]['productionTaskId']]), ['class' => 'base-icon star-icon action-icon', 'title' => Yii::t('web', 'Assign quality note')]);
                            } ?>
                       	</td>             
	                </tr>
	            <?php endforeach ?>
	        </tbody>
	    </table>
	</div>
	<div class="col-sm-12">
		<div class="col-sm-4 text-left dynamic-table-counter">
			<?= Yii::t('web', 'Number of found items: {n}', ['n' => $pages->totalCount]) ?>
		</div>
		<div class="col-sm-8 text-right dynamic-table-pagination">
		    <?= LinkPager::widget(['pagination' => $pages ]) ?>
		</div>
	</div>
<?php }?>

<input class="dynamic-table-sort-dir" type="hidden" name="<?= Html::getInputName($listForm, 'sortDir') ?>" value="<?= $listForm->sortDir ?>" />
<input class="dynamic-table-sort-field" type="hidden" name="<?= Html::getInputName($listForm, 'sortField') ?>" value="<?= $listForm->sortField ?>" />
<input class="dynamic-table-page" type="hidden" name="<?= Html::getInputName($listForm, 'page') ?>" value="<?= $listForm->page ?>" />

<?php 
    $this->registerJs('dynamicTable("production-department-orders-list-table", "' . Url::to(['production/production-order-details']) . '", {url: "'.(Yii::$app->request->get('dashboard') ? Url::to(['production/index']) : null).'"});', View::POS_END);
    $this->registerJs('initAttributeTooltip();', View::POS_END);
    EnhancedDialogAsset::register($this);
    
    $this->registerJs('$(".table-responsive")
            .css("overflow-x","visible")
            .closest(".container")
            .css("overflow-x","auto")', View::POS_END);
?>


