<?php
use yii\helpers\Url;
use yii\web\View;

$this->registerJsFile('@web/js/dynamic-table.js?'.uniqid(), []);
?>

<div id="production-product-list-wrapper">

    <div id="production-department-orders-list-table" class="dynamic-table">
        <div class="dynamic-table-data">
            <?= Yii::$app->controller->renderPartial('_shortOrders', [
                'items' => $items,
                'pages' => $pages,
                'listForm' => $listForm,
            ]) ?>
        </div>
    </div>
    <?php 
    	$this->registerJs('dynamicTable("production-department-orders-list-table", "' . Url::to(['production/short-department-orders']) . '", {url: "'.(Yii::$app->request->get('dashboard') ? Url::to(['production/index']) : null).'"});', View::POS_END);
    ?>
</div>