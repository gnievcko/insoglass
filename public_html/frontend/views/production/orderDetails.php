<?php

use yii\helpers\Html;
use yii\helpers\Url;
use common\helpers\StringHelper;
use common\bundles\EnhancedDialogAsset;
use yii\web\JqueryAsset;
use yii\web\View;

$this->title = StringHelper::translateOrderToOffer('web', 'Order details') . ' - ' . Yii::$app->name;

$this->params['breadcrumbs'][] = ['label' => StringHelper::translateOrderToOffer('main', 'Orders'), 'url' => Url::to(['order/index'])];
$this->params['breadcrumbs'][] = ['label' => StringHelper::translateOrderToOffer('main', 'List of orders'), 'url' => Url::to(['order/list'])];
$this->params['breadcrumbs'][] = StringHelper::translateOrderToOffer('web', 'Order details');


$this->registerJsFile('@web/js/pages/toggle-attributes.js?'.uniqid(), ['depends' => [JqueryAsset::className()]]);
?>
<div class="order-details">
    <div class="row">
        <div class="col-md-4 col-sm-6"><h2 class="page-title"><?= StringHelper::translateOrderToOffer('web', 'Order details') ?></h2></div>
    </div>
  
	<div class="col-xs-12 title-with-icon">
		<div class="base-icon basic-data-icon main-content-icons"></div>
		<h3><?= Yii::t('web', 'General information') ?></h3>
	</div>	
	
	<div class="col-xs-12">
	    <div class="row">
            <div class="table-responsive">
                <table class="table table-bordered detail-table">
                    <tbody>
                        <tr>
                            <th><?= StringHelper::translateOrderToOffer('web', 'Order name') ?></th>
                            <td><?= Html::encode($data['title']) ?></td>
                        </tr>
                    	<tr>
                            <th><?= StringHelper::translateOrderToOffer('web', 'Order number') ?></th>
                            <td><?= Html::encode($data['number']) ?></td>
                        </tr>
                        <tr>
                            <th><?= Yii::t('web', 'Deadline') ?></th>
                            <td><?= Html::encode(StringHelper::getFormattedDateFromDate($data['dateDeadline'])) ?></td>
                        </tr>
                    	<tr>
                            <th><?= Yii::t('web', 'Date shipping') ?></th>
                            <td><?= Html::encode(StringHelper::getFormattedDateFromDate($data['dateShipping'])) ?></td>
                        </tr>
                        <tr>
                            <th><?= Yii::t('main', 'Priority') ?></th>
                            <td><?= !empty($data['priorityName']) ? Html::encode($data['priorityName']) : Yii::t('web', 'Normal') ?></td>
                        </tr>
                    	<tr>
                            <th><?= Yii::t('main', 'Customer') ?></th>
                            <td><?= Html::a(Html::encode($data['customerName']), ['/client/details', 'id' => $data['customerId']])?></td>
                        </tr>
                        <?php if(!empty($salesmen)) { ?>
				    		<tr>
				    			<th><?= Yii::t('web', 'Contact people') ?></th>
				    			<td><?= $this->render('/inc/usersRow', ['users' => $salesmen]) ?></td>
				    		</tr>
			    		<?php } ?>
                    </tbody>
                </table>
	        </div>
	    </div>
	</div>

    <div class="col-xs-12 title-with-icon">
		<div class="base-icon box-icon main-content-icons"></div>
		<h3><?= StringHelper::translateOrderToOffer('web', 'Products') ?></h3>
	</div>

    <div class="col-xs-12">
        <?php if(!empty($products)) { ?>
        	<div class="row">
    			<div class="col-xs-12">
    				<div class="row">
        				<?php foreach($products as $product) {?>
        				<div class="col-xs-12">
        					<h4><?= Html::encode($product['symbol'])?></h4>
        					
        					<div class="table-responsive">
                            	<table class="table table-bordered table-hover detail-table">
                                	<tbody>
                                    	<tr>
                                    		<th><?= Yii::t('web', 'Product code') ?></th>
                                			<td><?= Html::a(Html::encode($product['symbol']), ['/offered-product/details', 'id' => $product['productId'], 'type' => 'offered-product'])?></td>
                                		</tr>
                                    	<tr>
                                    		<th><?= Yii::t('web', 'Product name') ?></th>
                                    		<td><?= Html::encode($product['productName']) ?></td>
                                		</tr>
                                    	<tr>
                                    		<th><?= Yii::t('web', 'Count') ?></th>
                                    		<td><?= Html::encode($product['count']) ?></td>
                                		</tr>
                                    	<tr>
                                    		<th><?= Yii::t('web', 'Width') . ' [mm]' ?></th>
                                    		<td><?= Html::encode($product['width']) ?></td>
                                		</tr>
                                    	<tr>
                                    		<th><?= Yii::t('web', 'Height') . ' [mm]'?></th>
                                    		<td><?= Html::encode($product['height']) ?></td>
                                		</tr>
                                    	<tr>
                                    		<th><?= Yii::t('web', 'Shape')?></th>
                                    		<td><?= Html::encode($product['shape']) ?></td>
                                		</tr>
                                		<tr>
                                    		<th><?= Yii::t('web', 'Construction')?></th>
                                    		<td><?= Html::encode($product['construction']) ?></td>
                                		</tr>
                                		<tr>
                                    		<th><?= Yii::t('web', 'Muntin')?></th>
                                    		<td><?= Html::encode($product['muntin']) ?></td>
                                		</tr>
            		            		<?php if(!empty($product['ralColourSymbol'])) {?>
                            				<tr>
                                				<th><?= Yii::t('web', 'RAL colour')?></th>
                                				<td><?= Html::encode($product['ralColourSymbol']) . '&nbsp'?> 
                                					<span class="colour-rectangle" style="background-color:<?= Html::encode($product['ralRgbHash'])?>"></span></td>
                            				</tr>
                    					<?php }?>
                                		<?php if(!empty($product['remarks'])) {?>
                                    		<tr>
                                        		<th><?= Yii::t('web', 'Remarks 1')?></th>
                                        		<td><?= Html::encode($product['remarks']) ?></td>
                                    		</tr>
                                    	<?php }?>
                                    	<?php if(!empty($product['remarks2'])) {?>
                                    		<tr>
                                        		<th><?= Yii::t('web', 'Remarks 2')?></th>
                                        		<td><?= Html::encode($product['remarks2']) ?></td>
                                    		</tr>
                                		<?php }?>
                                	</tbody>
                            	</table>
                        	</div>
                        
                        	<?php $isVisibleByDefault = true;?>
                        
                        	<div class="col-xs-12 text-center">
                    			<div class="display-attributes-button">
                    				<span class="glyphicon glyphicon-menu-<?= $isVisibleByDefault ? 'up' : 'down' ?>"></span>&nbsp;
                    			    <span class="text-down-up">
                    			    	<?= mb_strtoupper(Yii::t('web', 'Attribute list'), 'UTF-8') ?>
                    			    </span>
                    			</div>
                        	</div>
                        	
                        	<div class="col-xs-12 attributes-row">
                            	<?= Yii::$app->controller->renderPartial('/inc/productAttributeSummary', [
                            	    'groups' => $product['attributes'],
                                    ]);
                            	?>
                            </div>
                            
                            <?php if(!empty($product['attachments'])) { ?>
                            	<div class="col-xs-12 attachments-row">
                                	<?= Yii::$app->controller->renderPartial('_productionAttachments', [
                                	    'attachments' => $product['attachments'],
                                        ]);
                                    ?>
                            	</div>
                            <?php } ?>
                            
                            <div class="col-xs-12">	
                            	<hr>
                            </div>
                            
                        </div>
        				<?php } ?>
    				</div>
    			</div>
			</div>
       	<?php } else { ?>
            <div class="row">
                <div class="col-sm-8 desc no-desc">
            		<?php echo Yii::t('web', 'No data'); ?>
                </div>
            </div>
    	<?php } ?>
    </div>
    
    
   	<div class="col-xs-12 title-with-icon">
		<div class="base-icon form-icon main-content-icons"></div>
		<h3><?= StringHelper::translateOrderToOffer('web', 'Documents') ?></h3>
	</div>
    
    <div class=" col-xs-12 attributes-row">
        <div class="row">
    		<?php if(empty($attachments)) {
    		    echo Yii::t('web', 'None');
            } else {
                echo Yii::$app->controller->renderPartial('_productionAttachments', [
                    'attachments' => $attachments,
                ]);
            }?>
        </div>
    </div>
    
    <div class="col-xs-12 title-with-icon">
		<div class="base-icon description-icon main-content-icons"></div>
		<h3><?= StringHelper::translateOrderToOffer('web', 'Order description') ?></h3>
	</div>
	
    <div class=" col-xs-12">
        <div class="row">
            <div class="col-sm-8 desc">
    		<?= empty($data['description']) ? Yii::t('web', 'None') : nl2br(Html::encode($data['description'])); ?>
            </div>
        </div>
    </div>

    
<?php
    $this->registerJs('toggleAttributes();', View::POS_END);
	EnhancedDialogAsset::register($this);
?>
</div>