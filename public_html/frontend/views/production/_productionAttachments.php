<?php 
use yii\helpers\Html;
?>

<?php 
$attachmentsCount = count($attachments);
$i = 1;
foreach($attachments as $attachment) {
	if($i % 4 == 1) { ?>
		<div class="col-xs-12">
	<?php } ?>
		<div class="col-xs-3">
			<span>
				<?= Html::a($attachment['name'], \common\helpers\UploadHelper::getNormalPath($attachment['urlFile'], $attachment['name']), ['target' => '_blank']) ?>
			</span>
		</div>
	<?php if($i % 4 == 0) {?>
		</div>
	<?php }
	$i++;
}
if($attachmentsCount % 4 != 0) {?>
	</div>
<?php }?>