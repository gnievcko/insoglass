<?php
use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\View;

$this->title = Yii::t('web', 'Orders for department') . ' - ' . Yii::$app->name;

$this->params['breadcrumbs'][] = ['label' => Yii::t('web', 'Production'), 'url' => Url::to(['production/index'])];
$this->params['breadcrumbs'][] = Yii::t('web', 'Orders for department');

$this->registerJsFile('@web/js/dynamic-table.js?'.uniqid(), []);
$this->registerJsFile('@web/js/pages/production-department-orders.js?'.uniqid(), ['depends' => [\yii\web\JqueryAsset::className()]]);
$this->registerJsFile('@web/js/hint-tooltip.js?'.uniqid(), ['depends' => [\yii\web\JqueryAsset::className()]]);
?>

<div id="production-product-list-wrapper">
    <div class="row">
        <div class="col-sm-6"><h2 class="page-title"><?= Yii::t('web', 'Orders for department') ?></h2></div>
	</div>

    <div id="production-department-orders-list-table" class="dynamic-table">
    	<h5><?php echo Yii::t('main','Filters')?></h5>
        <div class="row dynamic-table-filters">
            <div class="col-sm-3">
            	<label><?= Yii::t('web', 'Select department') ?></label>
            		<?php
            		echo Html::activeDropDownList($listForm, 'departmentId', $departments, ['class' => 'form-control dynamic-table-filter', 'prompt' => Yii::t('web', 'All')]);
            		?>
            </div>
        </div>
        <div class="dynamic-table-data">
            <?= Yii::$app->controller->renderPartial('_orders', [
                'items' => $items,
                'pages' => $pages,
                'listForm' => $listForm,
                'qaAssignmentAvailableProducts' => $qaAssignmentAvailableProducts,
            ]) ?>
        </div>
    </div>
    <?php 
        echo Yii::$app->controller->renderPartial('inc/priorityModal', [
                    'orderPriorityModel' => $orderPriorityModel,
                    'orderPriorities' => $orderPriorities,
                ]);
    	$this->registerJs('dynamicTable("production-department-orders-list-table", "'.Url::to(['production/department-orders']).'");', View::POS_END);
    	$this->registerJs('initAttributeTooltip();', View::POS_END);
    	$this->registerJs('initDynamicFeatures("'.Url::to(['production/']).'")', View::POS_END);
    ?>
</div>
