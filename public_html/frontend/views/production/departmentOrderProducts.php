<?php
use yii\helpers\Url;
use yii\web\View;
use common\helpers\StringHelper;

$this->registerJsFile('@web/js/dynamic-table.js?'.uniqid(), []);
$this->registerJsFile('@web/js/hint-tooltip.js?'.uniqid(), ['depends' => [\yii\web\JqueryAsset::className()]]);

$this->title = StringHelper::translateOrderToOffer('web', 'Products in order') . ' - ' . Yii::$app->name;

$this->params['breadcrumbs'][] = ['label' => Yii::t('web', 'Production'), 'url' => Url::to(['production/index'])];
$this->params['breadcrumbs'][] = StringHelper::translateOrderToOffer('web', 'Products in order');
?>
<div class="row"><div class="col-xs-6"><h2 class="page-title"><?= StringHelper::translateOrderToOffer('web', 'Products in order') ?></h2></div></div>

    <div id="production-product-list-wrapper">

    <div id="production-department-products-list-table" class="dynamic-table">
        <div class="dynamic-table-data">
            <?= Yii::$app->controller->renderPartial('_shortProducts', [
                'items' => $items,
                'pages' => $pages,
                'listForm' => $listForm,
                'qaAssignmentAvailableProducts' => $qaAssignmentAvailableProducts,
            ]) ?>
        </div>
    </div>
    <?php 
    	$this->registerJs('dynamicTable("production-department-products-list-table", "' . Url::to(['production/production-order-details']) . '", {params : {"orderId": "'.Yii::$app->request->get('orderId').'"}});', View::POS_END);
    ?>
</div>