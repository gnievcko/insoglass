<?php

use yii\helpers\Html;
use yii\helpers\Json;
use common\helpers\ArrayHelper;
use common\helpers\StringHelper;

$data = Json::decode($rawData, true);

// TODO: Widok stricte dostosowany do prostej wersji - dla pełnej wersji musi być to napisane od nowa
$generalEvaluationIndex = ArrayHelper::searchAssociativeArrayForValue($data['fields'], 'symbol', 'general_evaluation');
$commentIndex = ArrayHelper::searchAssociativeArrayForValue($data['fields'], 'symbol', 'comment');

$generalEvaluation = !is_null($generalEvaluationIndex) ? $data['fields'][$generalEvaluationIndex] : null;
$comment = !is_null($commentIndex) ? $data['fields'][$commentIndex] : null;

echo Html::encode($generalEvaluation['name'].': '.$generalEvaluation['value']); ?>

<span class="glyphicon glyphicon-info-sign extended-tooltip">
	<div class="hint-container">
		<div class="extended-hint"><?php
            echo Yii::t('web', 'Employee').': '.Html::encode($data['userName']).'<br/>';
            echo Yii::t('web', 'Date').': '.StringHelper::getFormattedDateFromDate($data['dateCreation'], true).'<br/>';
            echo Html::encode($generalEvaluation['name'].': '.$generalEvaluation['value']).'<br/>';
            echo Html::encode($comment['name'].': '.$comment['value']).'<br/>';
		?></div>
	</div>
</span>