<?php
use common\helpers\StringHelper;
use common\components\ActiveForm;
use yii\helpers\Html;
?>

<div id="order-priority-modal" class="modal fade">
    <div class="modal-dialog">
    	<div class="modal-content">
    		<?php $form = ActiveForm::begin([
                        'id' => 'order-priority-form-ajax',
                        'enableAjaxValidation' => true,
                    ]); ?>
    	
    		<div class="modal-header">
    			<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
    			<h4 class="modal-title"><?= Yii::t('web', 'Change priority') ?></h4>
            </div>
            <div class="modal-body">
            	<p><?= StringHelper::translateOrderToOffer('web', 'You can change priority of this product\'s production process. If you do, it affects on the whole order, not only this particular product.') ?></p>
            	
                <?= $form->field($orderPriorityModel, 'orderId')->hiddenInput()->label(false); ?>
                
                <div class="row">
                	<div class="col-xs-12">
                		<?= $form->field($orderPriorityModel, 'orderPriorityCheckbox')->checkbox() ?>
                	
                		<?php //$form->field($orderPriorityModel, 'orderPriorityId')->dropDownList($orderPriorities); ?>
                	</div>
                </div>
            </div>
            <div class="modal-footer">
            	<?= Html::a(Yii::t('web', 'Cancel'), '', ['class' => 'btn btn-default', 'data-dismiss' => 'modal']) ?>
        		<?= Html::submitButton(Yii::t('main', 'Save'), ['class' => 'btn btn-primary']); ?>
            </div>
            
            <?php ActiveForm::end(); ?>	
        </div>
    </div>
</div>
