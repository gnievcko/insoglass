<?php

use yii\helpers\Url;
use yii\web\JqueryAsset;
use yii\helpers\Html;
use common\helpers\Utility;
use common\helpers\StringHelper;
use yii\web\View;

$this->registerJsFile('@web/js/chart/rolling.js?' . uniqid(), ['depends' => [JqueryAsset::className(), dosamigos\chartjs\ChartJsAsset::className()]]);
$this->registerJsFile('@web/js/pages/dashboard-calendar.js?' . uniqid(), ['depends' => [JqueryAsset::className(), yii2fullcalendar\MomentAsset::className(), yii2fullcalendar\CoreAsset::className(), yii2fullcalendar\PrintAsset::className()]]);
$this->title = Yii::t('web', 'Production dashboard') . ' - ' . Yii::$app->name;
?>
<?php
$script = '
    $.get("' . Url::to(['production/short-department-orders']) . '"+window.location.search, {dashboard: true}, function(data) {
  		$("#department-orders-list").html(data);
	});
    
';
$this->registerJs($script);
?>
<div class="calendar-order-list">
    <div class="col-sm-9">
    	<div class="row">
            <div class="col-sm-6 calendar">
                <?php
                $arrLocales = ['pl_PL', 'pl', 'Polish_Poland.28592'];
                setlocale(LC_ALL, $arrLocales);
                ?>
                <div class="calendar-date"><?= StringHelper::getCurrentDateAsText(); ?></div>
                <div id="calendar"></div>
            </div>
            <div class="col-sm-6 calendar-orders">
            	<div class="calendar-orders-border">
                    <div><?= Html::dropDownList('addDate', null, [Utility::DATE_DEADLINE => Yii::t('web', 'Deadline'), Utility::DATE_SHIPPING => Yii::t('web', 'Date shipping')], ['class' => 'dynamic-table-filter form-control', 'id' => 'short-order-date']); ?></div>
                    <div id="short-order">
                        <div class="empty-orders">
                            <?= StringHelper::translateOrderToOffer('main', 'No orders to view') ?>!
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    
    <div class="col-sm-3">
        <div class="col-sm-12 well rollingChart">
        	<div class="chart">
    			<div class="title"><?= Yii::t('web', 'Number of delays')?></div>
    			<h1 class="text-center"><?= $delayCount ?></h1>
    		</div>
        </div>
    </div>
</div>
    <div class="row">
        <div class="col-sm-12">
        	<div class="row">
            	<div class="col-sm-6">
            		<h3><?= StringHelper::translateOrderToOffer('main', 'Orders') ?></h3>
            	</div>
                <div class="col-sm-12" id="department-orders-list">
            
                </div>
           	</div>
        </div>
    </div>

<?php
$jsonCalendar = Url::to(['order/json-calendar']);
$shortOrder = Url::to(['order/short-order']);
//$details = Url::to(['order/details']);
$details = Url::to(['production/order-details']);
$this->registerJs('init("' . $jsonCalendar . '", "' . $shortOrder . '", "' . $details . '");', View::POS_END);
?>
