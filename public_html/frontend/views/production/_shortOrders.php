<?php
use yii\helpers\Url;
use yii\helpers\Html;
use frontend\models\DepartmentShortOrdersListForm;
use yii\widgets\LinkPager;
use common\bundles\EnhancedDialogAsset;
use common\helpers\StringHelper;
use common\widgets\TableHeaderWidget;
?>
<?php 
if (empty($items)) { ?>
	<label><?= Yii::t('web', 'No search results');?></label>
<?php }
else { ?>
	<div class="table-responsive">
		<table class="<?= TableHeaderWidget::TABLECLASS ?>">
	    	<?php echo TableHeaderWidget::widget(['fields' => DepartmentShortOrdersListForm::getSortFields() , 'listForm' => $listForm, 'action' => true]);?>
	        <tbody>
	            <?php foreach($items as $item): ?>
	                <tr class="clickable-table-row" data-href="<?= Url::to(['production/production-order-details', 'orderId' => $item['id']])?>">
                        <td><?= Html::encode($item['customerName']) ?></td>
	                    <td><?= Html::encode($item['orderNumber']) ?></td>
	                    <td>
	                    	<div class="progress">
    							<div class="progress-bar" role="progressbar" aria-valuenow="<?= Html::encode($item['completionPercent'])?>"
      								aria-valuemin="0" aria-valuemax="100" style="width:<?= Html::encode($item['completionPercent']) . '%' ?>">
        							<?= Html::encode($item['completionPercent']) . '%' ?>
								</div>
							</div>
	                    </td>
	                    <!--<td>TODO</td> -->
	                    <td><?= Html::encode(StringHelper::getFormattedDateFromDate($item['deadline'])) ?></td>
	                    <td>
                       		<?= Html::a('', Url::to(['production/production-order-details', 'orderId' => $item['id']]), ['class' => 'base-icon details-icon action-icon', 'title' => Yii::t('web', 'Details')]); ?>
                       	</td>             
	                </tr>
	            <?php endforeach ?>
	        </tbody>
	    </table>
	</div>
	<div class="col-sm-12">
		<div class="col-sm-4 text-left dynamic-table-counter">
			<?= Yii::t('web', 'Number of found items: {n}', ['n' => $pages->totalCount]) ?>
		</div>
		<div class="col-sm-8 text-right dynamic-table-pagination">
		    <?= LinkPager::widget(['pagination' => $pages ]) ?>
		</div>
	</div>
<?php }?>

<input class="dynamic-table-sort-dir" type="hidden" name="<?= Html::getInputName($listForm, 'sortDir') ?>" value="<?= $listForm->sortDir ?>" />
<input class="dynamic-table-sort-field" type="hidden" name="<?= Html::getInputName($listForm, 'sortField') ?>" value="<?= $listForm->sortField ?>" />
<input class="dynamic-table-page" type="hidden" name="<?= Html::getInputName($listForm, 'page') ?>" value="<?= $listForm->page ?>" />

<?php 
	EnhancedDialogAsset::register($this);
?>


