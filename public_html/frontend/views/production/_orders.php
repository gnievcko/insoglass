<?php
use yii\helpers\Url;
use yii\helpers\Html;
use frontend\models\DepartmentOrdersListForm;
use yii\widgets\LinkPager;
use common\bundles\EnhancedDialogAsset;
use common\helpers\StringHelper;
use common\widgets\TableHeaderWidget;
?>
<?php 
if (empty($items)) { ?>
	<label><?= Yii::t('web', 'No search results');?></label>
	<br><br>				
<?php }
else { ?>
	<div class="table-responsive">
		<table class="<?= TableHeaderWidget::TABLECLASS ?>">
	    	<?php echo TableHeaderWidget::widget(['fields' => DepartmentOrdersListForm::getSortFields() , 'listForm' => $listForm, 'action' => true]);?>
	        <tbody>
	            <?php foreach($items as $item): ?>
	                <tr>
	                    <td><?= Html::encode($item['customerName']) ?></td>   
	                    <td><?= Html::encode($item['orderNumber']) ?></td>
	                    <td><?= Html::encode($item['productCode']) ?></td>
	                    <td><?= Html::encode(StringHelper::getFormattedDateFromDate($item['deadline'])) ?></td>
	                    <td><?= Html::encode($item['completedCount']) ?></td>
	                    <td><?= Html::encode($item['priority']) ?></td>  
	                    <td><?php if(!empty($item['qualityNote'])) { 
                            echo Yii::$app->controller->renderPartial('inc/qualityNote', ['rawData' => $item['qualityNote']]); 
                        } ?></td>  
	                    <td>
                       		<?= Html::a('', Url::to(['production/production-details', 'orderOfferedProductId' => $item['orderOfferedProductId']]), ['class' => 'base-icon details-icon action-icon', 'title' => Yii::t('web', 'Details')]); ?>
                       		<?= Html::a('', '#order-priority-modal', ['class' => 'priority-change base-icon priority-icon action-icon', 'data-toggle' => 'modal', 'data-order-offered-product-id' => $item['orderOfferedProductId'], 'title' => StringHelper::translateOrderToOffer('web', 'Change priority of the whole order')]); ?>
                       		<?php if(array_key_exists($item['orderOfferedProductId'], $qaAssignmentAvailableProducts)) {
                             echo Html::a('', Url::to(['qa/create', 'productionTaskId' => $qaAssignmentAvailableProducts[$item['orderOfferedProductId']]['productionTaskId']]), ['class' => 'base-icon star-icon action-icon', 'title' => Yii::t('web', 'Assign quality note')]);
                            } ?>
                       	</td>                
	                </tr>
	            <?php endforeach; ?>
	        </tbody>
	    </table>
	</div>
	<div class="col-sm-12">
		<div class="col-sm-4 text-left dynamic-table-counter">
			<?= Yii::t('web', 'Number of found items: {n}', ['n' => $pages->totalCount]) ?>
		</div>
		<div class="col-sm-8 text-right dynamic-table-pagination">
		    <?= LinkPager::widget(['pagination' => $pages ]) ?>
		</div>
	</div>
<?php } ?>

<input class="dynamic-table-sort-dir" type="hidden" name="<?= Html::getInputName($listForm, 'sortDir') ?>" value="<?= $listForm->sortDir ?>" />
<input class="dynamic-table-sort-field" type="hidden" name="<?= Html::getInputName($listForm, 'sortField') ?>" value="<?= $listForm->sortField ?>" />
<input class="dynamic-table-page" type="hidden" name="<?= Html::getInputName($listForm, 'page') ?>" value="<?= $listForm->page ?>" />

<?php
	EnhancedDialogAsset::register($this);
?>

<?php
//fix for hint tooltip being cut off by overflow-x parent.
$this->registerJs('
$(".table-responsive")
        .css("overflow-x","visible")
        .closest(".container")
        .css("overflow-x","auto");
'); ?>
