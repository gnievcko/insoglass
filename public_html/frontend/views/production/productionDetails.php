<?php 
use yii\helpers\Html;
use yii\helpers\Url;
use common\helpers\StringHelper;
use common\components\ActiveForm;

$pageTitle = Yii::t('web', 'Production details') . ': ' . $detailsData['productName'] . ' (' . $detailsData['customerName'] . ')';
$this->title = $pageTitle . ' - ' . Yii::$app->name;

$this->params['breadcrumbs'][] = ['label' => Yii::t('web', 'Production'), 'url' => Url::to(['production/index'])];
$this->params['breadcrumbs'][] = ['label' => Yii::t('web', 'Orders for department'), 'url' => Url::to(['production/department-orders'])];
$this->params['breadcrumbs'][] = $pageTitle;

$this->registerJsFile('@web/js/pages/production-order-details.js?'.uniqid(), ['depends' => [\yii\web\JqueryAsset::className()]]);
?>

<div class="production-products">

    <div class="row">
        <div class="col-md-8 col-xs-12"><h2 class="page-title"><?= $pageTitle ?></h2></div>
        <div class="col-md-4 col-xs-12 text-right">
            <div><?= Html::a(StringHelper::translateOrderToOffer('web', 'Order details'), ['/production/order-details', 'id' => $detailsData['orderId']], ['class' => 'btn btn-default']) ?></div>
        </div>
    </div>

    <div class="col-xs-12 title-with-icon">
    	<div class="base-icon basic-data-icon main-content-icons"></div>
    	<h3><?= Yii::t('web', 'General information') ?></h3>
    </div>

    <div class="col-xs-12">
        <div class="row">
            <div class=" col-sm-8">
                <div class="table-responsive">
                    <table class="table table-bordered detail-table">
                        <tbody>
                            <tr>
                                <th><?= Yii::t('web', 'Customer name') ?></th>
                                <td><?= Html::encode($detailsData['customerName']) ?></td>
                            </tr>
                            <tr>
    	                        <th><?= Yii::t('web', 'Product code') ?></th>
    	                        <td><?= Html::a(Html::encode($detailsData['productCode']), ['/offered-product/details', 'id' => $detailsData['productId'], 'type' => 'offered-product'])?></td>
    	                    </tr>
    	                    <tr>
    	                        <th><?= Yii::t('web', 'Product name') ?></th>
    	                        <td><?= Html::encode($detailsData['productName'])?></td>
    	                    </tr>
    	                    <tr>
    	                        <th><?= StringHelper::translateOrderToOffer('web', 'Order number') ?></th>
    	                        <td><?= Html::encode($detailsData['orderNumber'])?></td>
    	                    </tr>
                            <tr>
                                <th><?= Yii::t('web', 'Deadline') ?></th>
                                <td><?= Html::encode(StringHelper::getFormattedDateFromDate($detailsData['deadline'])) ?></td>
                            </tr>
                            <tr>
                                <th><?= Yii::t('web', 'Completed count') ?></th>
                                <td><?= Html::encode($detailsData['completedCount'])?></td>
                            </tr>
                            <tr>
                                <th><?= Yii::t('web', 'Required count') ?></th>
                                <td><?= Html::encode($detailsData['requiredCount'])?></td>
                            </tr>
    				    	<tr>
    				    		<th><?= Yii::t('main', 'Priority') ?></th>
    				    		<td><?= !empty($detailsData['priorityName']) ? Html::encode($detailsData['priorityName']) : Yii::t('web', 'Normal')?></td>
    				    	</tr>
                    		<?php if(!empty($detailsData['remarks'])) {?>
                        		<tr>
                            		<th><?= Yii::t('web', 'Remarks 1')?></th>
                            		<td><?= Html::encode($detailsData['remarks']) ?></td>
                        		</tr>
                        	<?php }?>
                        	<?php if(!empty($detailsData['remarks2'])) {?>
                        		<tr>
                            		<th><?= Yii::t('web', 'Remarks 2')?></th>
                            		<td><?= Html::encode($detailsData['remarks2']) ?></td>
                        		</tr>
                    		<?php }?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
    
	<div class="col-xs-12 title-with-icon">
		<div class="base-icon description-icon main-content-icons"></div>
		<h3><?= StringHelper::translateOrderToOffer('web', 'Task list') ?></h3>
		<?php if(!empty($qaAssignmentProductionTaskId)) { ?>
            <div class="col-xs-12 text-right">
                <div><?= Html::a(Yii::t('web', 'Assign quality note'), Url::to(['qa/create', 'productionTaskId' => $qaAssignmentProductionTaskId]), ['class' => 'btn btn-default']) ?></div>
            </div>
        <?php } ?>
	</div>

    <div class="col-xs-12">
        <?php if(!empty($listData)) { ?>
            <div class="table-responsive list-table">
                <table class="table table-bordered table-hover grid-table">
                    <thead>
                        <tr>
                            <th><?= Yii::t('web', 'Activity name') ?></th>
                            <th><?= Yii::t('web', 'Completed count')?></th>
                            <th><?= Yii::t('web', 'Damaged count')?>
                        </tr>
                    </thead>
                    <tbody>
                        <?php foreach($listData as $data) { ?>
                            <tr data-target="#modal-<?=$data['productionTaskId']?>" class="tr-show-modal<?= !$data['isTaskAvailable'] || $data['isTaskComplete'] ? ' inactive-row' : '' ?>">
                                <td><?= $data['activityName'] ?></td>
                                <td>
                                	<div><?= $data['completedCount'] . '/' . $data['requiredCount'] ?></div>
                                	<div id="modal-<?=$data['productionTaskId']?>" class="modal fade" role="dialog">
                              			<div class="modal-dialog">
                                            <!-- Modal content-->
                                            <div class="modal-content">             
                                                <?php
                                                    $form = ActiveForm::begin([
                                                        'id' => 'update-product-count-' . $data['productionTaskId'],
                                                        'action' => Url::to(['production/add-items', 'productionTaskId' => $data['productionTaskId']]),
                                                        'enableAjaxValidation' => true,
                                                        'validateOnBlur' => false,
                                            			'validateOnChange' => false,
                                                        'options' => [
                                                            'class' => 'form',
                                                        ]
                                                    ]);
                                                ?>
                                
                                               	<div class="modal-header">
                                                  	<button type="button" class="close" data-dismiss="modal">&times;</button>
                                                    <h4 class="modal-title"><?= Yii::t('web', 'Update number of completed products') . ' - ' . $data['activityName'] ?></h4>
                                                </div>
                                                <div class="modal-body col-xs-12">
                                                	<div class="row">
                                                        <div class="col-xs-12">
                                                        	<div class="row">
                                                        		<div class="col-xs-12">
                                                        			<label><?= Yii::t('web','Number of completed')?></label>
                                                        		</div>
                                                                <div class="col-xs-3 text-center">
                                                                	<button class="remove-10 change-value btn btn-default"><strong>- 10</strong></button>
                                                                	<button class="remove-1 change-value btn btn-default"><strong>-</strong></button>
                                                                </div>
        
                                                                <div class="col-xs-6">
                                                            		<?= $form->field($addItemsForm, 'completedCount')->textInput(['class' => 'form-control completed-count', 'type' => 'number', 'step' => 1, 'placeHolder' => $addItemsForm->getAttributeLabel('completedCount')])->label(false)?>
                                                        		</div>
                                                                <div class="col-xs-3 text-center">
                                                                	<button class="add-1 change-value btn btn-default"><strong>+</strong></button>
                                                                	<button class="add-10 change-value btn btn-default"><strong>+ 10</strong></button>
                                                                </div>
                                                            </div>
                                                    	</div>
                                                    	<div class="col-xs-12">
                                                        	<?= $form->field($addItemsForm, 'damagedCount')->textInput(['type' => 'number', 'step' => 1, 'placeHolder' => $addItemsForm->getAttributeLabel('damagedCount')])?>
                                                        </div>
                                                        <div class="col-xs-12">
                                                        	<?= $form->field($addItemsForm, 'workstationId')->dropDownList($workstations, [])?>
                                                        </div>
                                                        <div class="col-xs-12">
                                                        	<?= $form->field($addItemsForm, 'message')->textarea(['placeHolder' => $addItemsForm->getAttributeLabel('message')]) ?>
                                                        </div>
                                                        <div class="col-xs-12">
                                                        	<?= $form->field($addItemsForm, 'productionTaskId')->hiddenInput()->label(false) ?>
                                                		</div>
                                                	</div>
                                            	</div>
                               
                                          		<div class="modal-footer">
                                          			<div class="col-xs-12">
                                          				<button id="orderQuickUpdateButton" class="btn btn-primary pull-right"><?= Yii::t('main', 'Save') ?></button>
                                            			<button type="button" class="btn btn-default" data-dismiss="modal"><?= Yii::t('main', 'Cancel') ?></button>
                                          			</div>
                                          		</div>
                                      			<?php ActiveForm::end() ?>
                                			</div>
                              			</div>
                            		</div>
                                </td>
                                <td><?= $data['failedCount'] ?></td>
                            </tr>    
                    	<?php } ?>
                    </tbody>
                </table>
            </div>
		<?php } else { ?>
            <div class="row">
                <div class="col-sm-8 desc no-desc">
            		<?php echo Yii::t('web', 'No data'); ?>
                </div>
            </div>
    	<?php } ?>
    </div>
</div>
