<?php

use common\documents\DocumentTypeUtility;
use common\models\ar\Order;
use yii\bootstrap\Html;
use common\documents\DocumentType;
use common\bundles\EnhancedDialogAsset;
use common\helpers\Utility;
use frontend\helpers\UserHelper;
use common\models\ar\User;
?>

<?php
if(!empty($documents)):
    $hideOrder = false;
    if(isset($orderId)) {
        $hideOrder = true;
    }
    ?>
    <h3><div class="icon-img his"></div> <?= Yii::t('web', 'History documents') ?></h3>
    <div class="col-sm-12">
        <table class="table">
            <thead>
                <tr>
                    <th><?= Yii::t('web', 'Type'); ?></th>
                    <?= !$hideOrder ? '<th>'.Yii::t('web', 'Order name').'</th>' : '' ?>
                    <th><?= Yii::t('web', 'Date'); ?></th>
                    <th><?= Yii::t('web', 'Author'); ?></th>
                    <th style="width: 40%"><?= Yii::t('web', 'Action'); ?></th>
                </tr>
            </thead>
            <tbody>
                <?php
                $translation = DocumentTypeUtility::translation();


                foreach($documents as $document) {
                	$histories = $document['history'];
                    usort($histories, function($a, $b) {
                        return $a['createdAt'] < $b['createdAt'];
                    });
                    $history = $histories[0];

                    $idD = $document['_id'];
                    $idH = $history['_id'];
                    $type = isset($translation[$document['type']]) ? $translation[$document['type']] : $document['type'];
					$numberDoc = null;
					$orderId = 0;
                    
                    foreach($history['sections'] as $section) {
                        if(isset($section['orderId'])) {
                        	$orderId = $section['orderId'];
                        }
                        if(isset($section['number'])) {
                        	$numberDoc = $section['number'];
                        }
                            
                        if(!empty($order) && !empty($numberDoc)) {
                        	break;
                    	}
					}
                    
                    $order = Order::find()->where(['id' => $orderId])->one();
                    $date = date('d.m.Y', $history['createdAt']);
                    $authorId = isset($history['authorId']) ? $history['authorId'] : null;
                    $author = !empty($authorId) ? User::find()->where(['id' => $authorId])->one() : null;
                    ?>
                    <tr>
                    	<td><?= $type.(!empty($numberDoc) ? ' ('.$numberDoc.')' : '') ?></td>
                    	<?php if(!$hideOrder) { ?>
                    		<td><?= (!empty($order) ? (Html::a($order->title, ['order/details', 'id' => $order->id])) : Yii::t('web', 'No name')) ?></td>
                    	<?php } ?>
                    	<td><?= $date ?></td>
                        <td>
                            <?= empty($author) ? '' :
                                Html::a(UserHelper::getPrettyUserName(
                                        $author->email,
                                        $author->first_name,
                                        $author->last_name),
                                        [
                                            'employee/details',
                                            'id' => $author->id
                                        ]) 
                            ?>
                        </td>
                    	<td>
	                    	<?= Html::a(
	                    			Html::img('@web/images/placeholders/document_icon.png').' '.Yii::t('web', 'See'), 
	                    			[DocumentTypeUtility::URL[$document['type']], 'documentId' => $idD->__toString(), 'historyId' => $idH->__toString()], 
	                    			['target' => '_blank'] 
	                    	) ?>
	                    	<?php if($enableEditButton && !in_array($document['type'], [DocumentType::DUPLICATE_INVOICE, DocumentType::DUPLICATE_CORRECTIVE_INVOICE])) { ?>
	                    		<?= Html::a(
	                    				Html::img('@web/images/placeholders/document_icon.png').' '.Yii::t('web', 'Edit'), 
	                    				[DocumentTypeUtility::URL[$document['type']], 'documentId' => $idD->__toString(), 'historyId' => $idH->__toString(), 'orderId' => $orderId, 'edit' => true], 
	                    				['target' => '_blank']	                    			
	                    		) ?>
	                    	<?php } ?>
	                    	<?php if($document['type'] == DocumentType::INVOICE && in_array(DocumentType::CORRECTIVE_INVOICE, $availableDocuments)) { ?>
	                    		<?= Html::a(
	                    				Html::img('@web/images/placeholders/document_icon.png').' '.Yii::t('web', 'Correct'), 
	                    				[DocumentTypeUtility::URL[DocumentType::CORRECTIVE_INVOICE], 'invoiceId' => $idD->__toString(), 'historyId' => $idH->__toString(), 'orderId' => $orderId], 
	                    				['target' => '_blank']	                    			
	                    		) ?>
	                    	<?php } ?>
	                    	<?php if($document['type'] == DocumentType::PROFORMA_INVOICE && in_array(DocumentType::ADVANCE_INVOICE, $availableDocuments)) { ?>
	                    		<?= Html::a(
	                    				Html::img('@web/images/placeholders/document_icon.png').' '.Yii::t('web', 'Gen. adv. inv.'), 
	                    				[DocumentTypeUtility::URL[DocumentType::ADVANCE_INVOICE], 'invoiceId' => $idD->__toString(), 'historyId' => $idH->__toString(), 'orderId' => $orderId], 
	                    				['target' => '_blank']	                    			
	                    		) ?>
	                    	<?php } ?>
	                    	<?php if(in_array($document['type'], array_diff(DocumentType::getInvoiceTypes(), [DocumentType::DUPLICATE_INVOICE, DocumentType::DUPLICATE_CORRECTIVE_INVOICE, DocumentType::CORRECTIVE_INVOICE]))) { ?>
		                    	<?= Html::a(
	                    				Html::img('@web/images/placeholders/document_icon.png').' '.Yii::t('web', 'Gen. duplicate'), 
	                    				[DocumentTypeUtility::URL[DocumentType::DUPLICATE_INVOICE], 'invoiceId' => $idD->__toString(), 'historyId' => $idH->__toString(), 'orderId' => $orderId], 
	                    				['target' => '_blank']	                    			
	                    		) ?>
                    		<?php } ?>
                    		<?php if(in_array($document['type'], [DocumentType::CORRECTIVE_INVOICE])) { ?>
		                    	<?= Html::a(
	                    				Html::img('@web/images/placeholders/document_icon.png').' '.Yii::t('web', 'Gen. duplicate'), 
	                    				[DocumentTypeUtility::URL[DocumentType::DUPLICATE_CORRECTIVE_INVOICE], 'invoiceId' => $idD->__toString(), 'historyId' => $idH->__toString(), 'orderId' => $orderId], 
	                    				['target' => '_blank']	                    			
	                    		) ?>
                    		<?php } ?>
	                    	<?php if(Yii::$app->user->can(Utility::ROLE_ADMIN) && in_array($idD->__toString(), $documentsAbleToDelete)) { ?>
	                    		<?= Html::a(
	                    				Html::img('@web/images/placeholders/document_icon.png').' '.Yii::t('main', 'Delete'), 
	                    				['document/delete'], 
	                    				[
												'data' => [
										                'confirm' => Yii::t('main', 'Are you sure you want to delete this document?'),
										                'method' => 'post',
														'params' => [
																'documentId' => $idD->__toString(), 
																'historyId' => $idH->__toString(),
														],
									            ],
										]	                    			
	                    		) ?>
	                    	<?php } ?>
                    	</td>
                    </tr>                    
                <?php } ?>
            </tbody>
        </table>
    </div>
    <?php endIf; ?>
    
<?php 
	EnhancedDialogAsset::register($this);
?>