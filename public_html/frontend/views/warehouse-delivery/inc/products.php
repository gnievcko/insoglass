<?php
use kartik\widgets\Select2;
use yii\web\JsExpression;
use yii\helpers\Url;
?>
<div class="product-select">
	<div class="col-xs-12 title-with-icon">
		<div class="base-icon box-icon main-content-icons"></div>
		<h3><?= Yii::t('web', 'Products') ?></h3>
	</div>
	<div class="col-xs-12">
		<div id="delivery-products" class="items-container">
		    <?php 
		        foreach($productsForms as $prefix => $productForm) {
		            echo Yii::$app->controller->renderPartial('inc/product', ['form' => $form, 'productForm' => $productForm, 'prefix' => $prefix]);
		        }
		    ?>
		</div>
	</div>
	<div class="col-xs-12">
		<div class="row">
		    <div class="col-sm-6">
		        <?= Select2::widget([
		                'id' => 'select2-search-product',
		                'name' => 'select2-search-product',
		                'language' => Yii::$app->language,
		                'options' => ['placeholder' => Yii::t('web', 'Select a product...')],
		                'data' => null,
		                'pluginOptions' => [
		                        'allowClear' => true,
		                        'minimumInputLength' => 1,
		                        'ajax' => [
		                                'url' => Url::to(['product/find']),
		                                'dataType' => 'json',
		                                'delay' => 250,
		                                'data' => new JsExpression('function(params) { return { term:params.term, page:params.page, size: 20 }; }'),
		                                'processResults' => new JsExpression('function (data, params) {
		                                    params.page = params.page || 1;
		
		                                    return {
		                                        results: data.items,
		                                        pagination: {
		                                            more: data.more
		                                        }
		                                    };
		                                }'),
		                                'cache' => true
		                        ],
		                        'templateResult' => new JsExpression('function(item) { if(item.loading) return item.text; return item.name; }'),
		                        'templateSelection' => new JsExpression('function (item) { return item.name || item.text; }'),
		                ],
		            ]); 
		        ?>
                <a href="<?= Url::to(['product/create']); ?>" target="_blank" class="focusable-link  add-item-after-select2  add-item-after-select2-no-required">
                    <span class="glyphicon glyphicon-plus-sign pull-left"></span>
                    <span> <?=Yii::t('web', 'Create new product')?></span>
                </a>
		    </div>
		    <div class="col-xs-12">
		        <?= Yii::$app->controller->renderPartial('inc/deliverySummary', ['productsForms' => $productsForms]); ?>
		    </div>
		</div>
	</div>
</div>