<?php 
use yii\web\View;
use yii\helpers\Url;
use common\helpers\StringHelper;

$this->registerJsFile('@web/js/accounting.min.js?'.uniqid());
?>

<div id="delivery-summary">
    <span>
        <?= Yii::t('web', 'Total') ?>:
    </span>
    <span id="delivery-summary-amout">
        <?php 
            $total = array_reduce($productsForms, function($total, $productForm) {
                return $total + $productForm->price*($productForm->priceType === $productForm::PRICE_TYPE_PER_UNIT ? $productForm->count : 1);  
            }, 0); 

            echo StringHelper::getFormattedCost($total);
        ?>
    </span>
    <span id="delivery-summary-currency">
    </span>
</div>

<?php
    $this->registerJs('
        (function() {

            var deliveryFormId = "warehouse-delivery-form";
            var dynamicForm = new DynamicForm(deliveryFormId);
            dynamicForm.activateRemoveButtons();

            var $addProductSelect = $("#select2-search-product");
            var $productsContainer = $("#delivery-products");
            var $totalField = $("#delivery-summary-amout");

            var calculateProductPrice = function($product) {
                var $productPrice = $product.find(".product-price:first");
                var $productCount = $product.find(".product-count:first");
                var $productPriceType = $product.find(".product-price-type:first");

                var priceMultipler = 0;
                var priceType = $productPriceType.val();
                if(priceType === "PRICE_TYPE_PER_PACKAGE") {
                    priceMultipler = 1;
                } else if(priceType === "PRICE_TYPE_PER_UNIT") {
                    priceMultipler = ($productCount.val() || 0);
                }

                return parseFloat($productPrice.val() || 0)*parseInt(priceMultipler);
            }

            var recalculateTotal = function() {
                var total = 0;
                $productsContainer.children(".warehouse-delivery-product").each(function(_, deliveryProduct) {
                    total += calculateProductPrice($(deliveryProduct))
                });
                $totalField.html((total == 0) ? 0 : accounting.formatNumber(total, 2, " ", ","));
            }

            $addProductSelect.on("change", function() {
                var productId = $addProductSelect.val();
                var chosenProductsIds = {};
                $productsContainer.find(".product-id").each(function(_, idField) {
                    var id = $(idField).val();
                    chosenProductsIds[id] = id;
                });
                if(productId != "" && chosenProductsIds[productId] === undefined) {
                    dynamicForm.addItem($productsContainer, "'.Url::to(['warehouse-delivery/get-product-form']).'", {productId: productId});
                }

                $addProductSelect.val("");
            });

            var initProduct = function($product) {
                $product.find(".product-price-type:first").on("change", function() {
                    recalculateTotal();
                })
                $product.find(".product-price:first").on("change", function() {
                    recalculateTotal();
                })
                $product.find(".product-count:first").on("change", function() {
                    recalculateTotal();
                })
            }

            $productsContainer.children().each(function(_, product) {
                initProduct($(product));
            });

            dynamicForm.onAddItem = function($product) {
                initProduct($product);
                recalculateTotal();
            }
            dynamicForm.onRemoveItem = recalculateTotal;

            $deliverySummaryCurrency = $("#delivery-summary-currency");
            $currencySelect = $("#" + deliveryFormId).find(".delivery-currency:first");
            $currencySelect.on("change", function() {
                $deliverySummaryCurrency.text($currencySelect.find("option:selected").text());
            }).change();
        })();
    ', View::POS_READY);
?>
