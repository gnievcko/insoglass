<?php
use kartik\widgets\Select2;
use yii\web\JsExpression;
use yii\helpers\Url;
use yii\helpers\ArrayHelper;
use common\models\aq\WarehouseQuery;
use common\helpers\Utility;
use kartik\datecontrol\DateControl;
use common\models\ar\Currency;
use common\models\ar\User;
use common\models\ar\WarehouseSupplier;
use frontend\helpers\UserHelper;
use common\models\ar\WarehouseProductOperation;
use yii\helpers\Json;
?>

<div class="col-xs-12 title-with-icon">
	<div class="base-icon basic-data-icon main-content-icons"></div>
	<h3><?= Yii::t('web', 'Basic data') ?></h3>
</div>
<div class="col-xs-12">
	<div class="row">
	    <div class="col-sm-6">
            <?= $form->field($warehouseDeliveryForm, 'number')->textInput(['placeHolder' => Yii::t('web','Number delivery'), 'readonly' => true]) ?>
	    </div>
	    <div class="col-sm-5 col-sm-offset-1">
	            <?= $form->field($warehouseDeliveryForm, 'warehouseSupplierId')->widget(Select2::classname(), [
	                'language' => Yii::$app->language,
	                'showToggleAll' => false,
	                'options' => ['placeholder' => Yii::t('web', 'Find supplier')],
	                'data' => empty($warehouseDeliveryForm->warehouseSupplierId) 
	                            ? [] 
	                            : ArrayHelper::map(WarehouseSupplier::find()->where(['id' => $warehouseDeliveryForm->warehouseSupplierId])->all(), 'id', 'name'),
	                'pluginOptions' => [
	                        'allowClear' => true,
	                        'minimumInputLength' => 1,
	                        'ajax' => [
	                                'url' => Url::to(['warehouse-supplier/find']),
	                                'dataType' => 'json',
	                                'delay' => 250,
	                                'data' => new JsExpression('function(params) { 
	                                    return {term:params.term, page:params.page, size: 20}; 
	                                }'),
	                                'processResults' => new JsExpression('function (data, params) { 
	                                    params.page = params.page || 1; 
	                                    
	                                    return {
	                                        results: data.items,
	                                        pagination: {
	                                            more: data.more
	                                        }
	                                    };
	                                }'),
	                                'cache' => true
	                        ],
	                        'templateResult' => new JsExpression('function(item) { if(item.loading) return item.text; return item.name; }'),
	                        'templateSelection' => new JsExpression('function (item) { return item.name || item.text; }'),
	                ],
	            ]) ?>
                <a href="<?= Url::to(['warehouse-supplier/create']); ?>" target="_blank" class="focusable-link  add-item-after-select2  add-item-after-select2-no-required">
                    <span class="glyphicon glyphicon-plus-sign pull-left"></span>
                    <span> <?=Yii::t('web', 'Create new client')?></span>
                </a>
	        </div>
	    </div>
	</div>
<div class="col-xs-12">
	<div class="row">
	    <div class="col-sm-6">  
	        <p><?= $form->field($warehouseDeliveryForm, 'warehouseId', ['options' => ['style' => 'display: '.(Yii::$app->params['isMoreWarehousesAvailable'] ? 'block' : 'none')]])->dropDownList(ArrayHelper::map(WarehouseQuery::getWarehouseList()->all(), 'id', 'name')) ?></p>
	        <div class="row">
		        <div class="col-xs-12">
		            <?= $form->field($warehouseDeliveryForm, 'userConfirmingId')->widget(Select2::classname(), [
		                'language' => Yii::$app->language,
		                'showToggleAll' => false,
		                'options' => ['placeholder' => Yii::t('web', 'Find warehouseman confirming delivery')],
		                'data' => empty($warehouseDeliveryForm->userConfirmingId)
		                            ? []
		                            : array_reduce(User::find()->where(['id' => $warehouseDeliveryForm->userConfirmingId])->all(), function($users, $user) {
		                                $users[$user->id] = UserHelper::getPrettyUserName($user->email, $user->first_name, $user->last_name);
		                                return $users;
		                            }, []),
		                'pluginOptions' => [
		                        'allowClear' => true,
		                        'minimumInputLength' => 1,
		                        'ajax' => [
		                                'url' => Url::to(['employee/find']),
		                                'dataType' => 'json',
		                                'delay' => 250,
		                                'data' => new JsExpression('function(params) { 
		                                    return {term:params.term, page:params.page, size: 20, roles: ["'.Utility::ROLE_STOREKEEPER.'", "'.Utility::ROLE_MAIN_STOREKEEPER.'"]}; 
		                                }'),
		                                'processResults' => new JsExpression('function (data, params) { 
		                                    params.page = params.page || 1; 
		                                    
		                                    return {
		                                        results: data.items.map(function(user) {
		                                            var fullName = user.firstName + ((user.firstName == null) ? "" : " " ) + user.lastName;
		                                            user.name = (fullName == "") ? user.email : fullName;
		                                            return user;
		                                        }),
		                                        pagination: {
		                                            more: data.more
		                                        }
		                                    };
		                                }'),
		                                'cache' => true
		                        ],
		                        'templateResult' => new JsExpression('function(item) { if(item.loading) return item.text; return item.name; }'),
		                        'templateSelection' => new JsExpression('function (item) { return item.name || item.text; }'),
		                ],
		            ]) ?>
                    <a href="<?= Url::to(['employee/create']); ?>" target="_blank" class="focusable-link  add-item-after-select2">
                        <span class="glyphicon glyphicon-plus-sign pull-left"></span>
                        <span> <?=Yii::t('web', 'Create new employee')?></span>
                    </a>
		        </div>
		        <div class="col-sm-6 col-xs-12">
					<?php if(Yii::$app->params['isPlaceholderInputLabel']) { ?>
						<label class="control-label"><?= $warehouseDeliveryForm->getAttributeLabel('dateDelivery')?></label>
					<?php }?>
		        	<?= $form->field($warehouseDeliveryForm, 'dateDelivery')->widget(DateControl::classname(), [
		                    'language' => \Yii::$app->language,
		                    'type' => DateControl::FORMAT_DATETIME,
		                    'ajaxConversion' => false,
		                    'displayFormat' => 'php:Y-m-d H:i',
		            		'options' => ['placeholder' => $warehouseDeliveryForm->getAttributeLabel('dateDelivery')],
		                    'options' => [
		                        'pluginOptions' => [
		                            'autoclose' => true
		                        ]
		                    ]
		                ]);
		            ?>
		        </div>
		        <div class="col-sm-4 col-sm-offset-2 col-xs-6">
					<?php if(Yii::$app->params['isPlaceholderInputLabel']) { ?>
						<label class="control-label"><?= $warehouseDeliveryForm->getAttributeLabel('currencyId')?></label>
					<?php }?>
		            <?= $form->field($warehouseDeliveryForm, 'currencyId')->dropDownList(ArrayHelper::map(Currency::find()->all(), 'id', 'short_symbol'), [
		                'class' => 'delivery-currency form-control'
		            ]) ?>
		    	</div>
		    </div>
	    </div>
	    <div class="col-sm-5 col-sm-offset-1">
            <?= $form->field($warehouseDeliveryForm, 'description')->textarea(['maxlength' => true, 'placeHolder' => $warehouseDeliveryForm->getAttributeLabel('description'), 'class' => 'form-control desc2']); ?>
    	</div>
    </div>
    
</div>

<?php if(Yii::$app->params['isWarehouseIssueCanBeAssignedToDelivery']) { ?>
	<div class="col-xs-12">
		<div class="row">
    		<div class="col-sm-6">
    			<?= $form->field($warehouseDeliveryForm, 'operationId')->widget(Select2::classname(), [
    					'language' => Yii::$app->language,
    					'options' => ['placeholder' => Yii::t('main', 'Select a related warehouse issue...')],
    					'data' => empty($warehouseDeliveryForm->operationId) 
    							? [] 
    							: (($operation = WarehouseProductOperation::findOne($warehouseDeliveryForm->operationId)) != null) 
    									? [$operation->id => !empty($operation->number) ? $operation->number : Yii::t('web', 'Issue').' #'.$operation->id] 
    									: [],
    					'pluginOptions' => [
    							'allowClear' => true,
    							'minimumInputLength' => 1,
    							'ajax' => [
    									'url' => Url::to(['warehouse-product-operation/find']),
    									'dataType' => 'json',
    									'delay' => 250,
    									'data' => new JsExpression('function(params) {
    										return {
    											term: params.term,
    											page: params.page,
    											size: 20,
    											types: '.Json::encode([Utility::PRODUCT_STATUS_ISSUING, Utility::PRODUCT_STATUS_ISSUING_RESERVATION]).'
    										};
    									}'),
    									'processResults' => new JsExpression('function (data, params) {
    										params.page = params.page || 1;
    			
    										return {
    											results: data.items,
    											pagination: {
    												more: data.more
    											}
    										};
    									}'),
    									'cache' => true
    							],
    							'templateResult' => new JsExpression('function(item) { if(item.loading) return item.text; return item.number; }'),
    							'templateSelection' => new JsExpression('function (item) { return item.number || item.text; }'),
    					]
    			]) ?>
    			<p class="hint-block"><?= Yii::t('web', 'The delivery can be related to warehouse issue if represents the return of unused products from the client.') ?></p> 
    		</div>
    		<div class="col-sm-5 col-sm-offset-1">
    		<?php if(!$warehouseDeliveryForm->isConfirmed): ?>
    			<?= $form->field($warehouseDeliveryForm, 'isConfirmed')->checkbox(); ?>
    			<p class="hint-block"><?= Yii::t('web', 'Confirmation of the delivery is irreversible and causes adding products to current warehouse state.') ?></p>
    		<?php else: ?>
    			<?= $form->field($warehouseDeliveryForm, 'isConfirmed')->hiddenInput()->label(false); ?>
    		<?php endif; ?>
    	</div>
		</div>
	</div>
<?php } 
else { ?>
	<?= $form->field($warehouseDeliveryForm, 'operationId')->hiddenInput()->label(false); ?>
<div class="col-xs-12">
	<div class="row">
    	<div class="col-sm-6">
    		<?php if(!$warehouseDeliveryForm->isConfirmed): ?>
    			<?= $form->field($warehouseDeliveryForm, 'isConfirmed')->checkbox(); ?>
    			<p class="hint-block"><?= Yii::t('web', 'Confirmation of the delivery is irreversible and causes adding products to current warehouse state.') ?></p>
    		<?php else: ?>
    			<?= $form->field($warehouseDeliveryForm, 'isConfirmed')->hiddenInput()->label(false); ?>
    		<?php endif; ?>
    	</div>
	</div>
</div>
<?php } ?>
