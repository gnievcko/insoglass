<?php
    use yii\helpers\Html;
    use yii\web\View;
    use frontend\models\WarehouseDeliveryProductForm;

    $formBaseName = $productForm->formName();
    $activeFormBaseName = strtolower($formBaseName);
    $formName = "{$activeFormBaseName}-{$prefix}";
?>

<div class="row item warehouse-delivery-product" data-form-base-name="<?= $formBaseName ?>" data-active-form-base-name="<?= $activeFormBaseName ?>">
    <div class="col-sm-2 product-thumbnail-container">
        <?= Html::img($productForm->thumbnail) ?>
    </div>

    <div class="col-sm-10">
        <div class="row">
            <div class="col-xs-12 product-name">
                <?= $productForm->name ?>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-2">
                <?php $inputOptions = ['type' => 'number', 'min' => 0, 'step' => 0.01, 'integer' => true, 'class' => 'form-control product-price'] ?>
                <?php if(empty($form)): ?>
                    <?php $fieldId = "{$formName}-price"; ?>

                    <div id="container-<?= $fieldId ?>" class="form-group <?= $productForm->isAttributeRequired('price') ? 'required' : '' ?>">  
                        <?=  Html::activeTextInput($productForm, "[{$prefix}]price", $inputOptions + ['class' => 'form-control', 'id' => "{$fieldId}", 'placeHolder' => $productForm->getAttributeLabel('price')]) ?>
                        <div class="help-block"></div>
                    </div>

                    <?php
                        $this->registerJs('
                            (function() {
                                $("#'.$formId.'").yiiActiveForm("add", {
                                    id: "'.$fieldId.'",
                                    name: "'."[{$prefix}]productsCount".'",
                                    container: "#container-'.$fieldId.'",
                                    input: "#'."$fieldId".'",
                                    enableClientValidation: false,
                                    enableAjaxValidation: true
                                });
                            })();
                            ', View::POS_END); 
                    ?>
                <?php else: ?>
                    <?= $form->field($productForm, "[{$prefix}]price")->textInput($inputOptions); ?>
                <?php endif ?>
            </div>
            <div class="col-sm-2 product-price-type-container">
                    <?php 
                        $pricesTypes = [
                            WarehouseDeliveryProductForm::PRICE_TYPE_PER_UNIT => Yii::t('web', 'Per unit'), 
                            WarehouseDeliveryProductForm::PRICE_TYPE_PER_PACKAGE => Yii::t('web', 'Per package'),
                        ];

                        $inputOptions = ['class' => 'form-control product-price-type'];
                    ?>
                    <?php if(empty($form)): ?>
                        <?php $fieldId = "{$formName}-pricetype"; ?>

                        <div id="container-<?= $fieldId ?>" class="form-group <?= $productForm->isAttributeRequired('priceType') ? 'required' : '' ?>">
                            <?=  Html::activeDropDownList($productForm, "[{$prefix}]priceType", $pricesTypes, $inputOptions + ['id' => "{$fieldId}"]) ?>
                            <div class="help-block"></div>
                        </div>

                        <?php
                            $this->registerJs('
                                (function() {
                                    $("#'.$formId.'").yiiActiveForm("add", {
                                        id: "'.$fieldId.'",
                                        name: "'."[{$prefix}]priceType".'",
                                        container: "#container-'.$fieldId.'",
                                        input: "#'."$fieldId".'",
                                        enableClientValidation: false,
                                        enableAjaxValidation: true
                                    });
                                })();
                                ', View::POS_END); 
                        ?>
                    <?php else: ?>
                        <?= $form->field($productForm, "[{$prefix}]priceType")->dropDownList($pricesTypes, $inputOptions); ?>
                    <?php endif ?>
            </div>
            <div class="col-sm-2">
                    <?php $inputOptions = ['type' => 'number', 'min' => 0, 'step' => 1, 'integer' => true, 'class' => 'form-control product-count'] ?>
                    <?php if(empty($form)): ?>
                        <?php $fieldId = "{$formName}-count"; ?>

                        <div id="container-<?= $fieldId ?>" class="form-group <?= $productForm->isAttributeRequired('count') ? 'required' : '' ?>">
                            <?=  Html::activeTextInput($productForm, "[{$prefix}]count", $inputOptions + ['id' => "{$fieldId}"]) ?>
                            <div class="help-block"></div>
                        </div>

                        <?php
                            $this->registerJs('
                                (function() {
                                    $("#'.$formId.'").yiiActiveForm("add", {
                                        id: "'.$fieldId.'",
                                        name: "'."[{$prefix}]count".'",
                                        container: "#container-'.$fieldId.'",
                                        input: "#'."$fieldId".'",
                                        enableClientValidation: false,
                                        enableAjaxValidation: true
                                    });
                                })();
                                ', View::POS_END); 
                        ?>
                    <?php else: ?>
                        <?= $form->field($productForm, "[{$prefix}]count")->textInput($inputOptions); ?>
                    <?php endif ?>
            </div>
            <div class="col-sm-1">
                    <?php if(empty($form)): ?>
                        <?php $fieldId = "{$formName}-unit"; ?>

                        <div id="container-<?= $fieldId ?>" class="form-group <?= $productForm->isAttributeRequired('unit') ? 'required' : '' ?>">
                        	<?php if(!empty(Yii::$app->params['defaultUnit'])): ?>
                        	     <?=  Html::activeTextInput($productForm, "[{$prefix}]unit", ['class' => 'form-control', 'id' => "{$fieldId}", 'value' => Yii::t('documents',Yii::$app->params['defaultUnit']),'readonly'=>true]) ?>
                        	<?php else: ?>
                            	<?=  Html::activeTextInput($productForm, "[{$prefix}]unit", ['class' => 'form-control', 'id' => "{$fieldId}", 'placeholder' => Yii::t('main', 'Unit')]) ?>
                            <?php endif; ?>
                            <div class="help-block"></div>
                        </div>

                        <?php
                            $this->registerJs('
                                (function() {
                                    $("#'.$formId.'").yiiActiveForm("add", {
                                        id: "'.$fieldId.'",
                                        name: "'."[{$prefix}]unit".'",
                                        container: "#container-'.$fieldId.'",
                                        input: "#'."$fieldId".'",
                                        enableClientValidation: false,
                                        enableAjaxValidation: true
                                    });
                                })();
                                ', View::POS_END); 
                        ?>
                    <?php else: ?>
                        <?= $form->field($productForm, "[{$prefix}]unit")->textInput(); ?>
                    <?php endif ?>
            </div>
            <div class="col-sm-4">
                    <?php if(empty($form)): ?>
                        <?php $fieldId = "{$formName}-note"; ?>

                        <div id="container-<?= $fieldId ?>" class="form-group <?= $productForm->isAttributeRequired('note') ? 'required' : '' ?>">
                            <?=  Html::activeTextInput($productForm, "[{$prefix}]note", ['class' => 'form-control', 'id' => "{$fieldId}", 'placeholder' => Yii::t('main','Description')]) ?>
                            <div class="help-block"></div>
                        </div>

                        <?php
                            $this->registerJs('
                                (function() {
                                    $("#'.$formId.'").yiiActiveForm("add", {
                                        id: "'.$fieldId.'",
                                        name: "'."[{$prefix}]note".'",
                                        container: "#container-'.$fieldId.'",
                                        input: "#'."$fieldId".'",
                                        enableClientValidation: false,
                                        enableAjaxValidation: true
                                    });
                                })();
                                ', View::POS_END); 
                        ?>
                    <?php else: ?>
                        <?= $form->field($productForm, "[{$prefix}]note")->textInput(); ?>
                    <?php endif ?>
            </div>
            <div class="col-sm-1">
                <div class="base-icon garbage-icon action-icon remove-item-btn" style="margin-top: 3px"></div>
                <?= Html::activeHiddenInput($productForm, "[{$prefix}]productId", ['class' => 'product-id']) ?>
            </div>
        </div>
    </div>
</div>
