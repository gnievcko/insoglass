<?php
use yii\helpers\Html;
use yii\helpers\Url;
use frontend\helpers\UserHelper;
use common\bundles\EnhancedDialogAsset;
use common\helpers\StringHelper;
use common\helpers\Utility;
use common\documents\DocumentType;

$this->title = Yii::t('web', 'Delivery details') . ' - ' . Yii::$app->name;

$this->params['breadcrumbs'][] = ['label' => Yii::t('main', 'Warehouse'), 'url' => Url::to(['warehouse/index'])];
$this->params['breadcrumbs'][] = ['label' => Yii::t('web', 'List of deliveries'), 'url' => Url::to(['warehouse-delivery/list'])];
$this->params['breadcrumbs'][] = Yii::t('web', 'Delivery details');
?>

<div class="delivery-details">
	<div class="row">
		<div class="col-md-4 col-sm-6"><h2 class="page-title"><?= Yii::t('web', 'Delivery details') ?></h2></div>
        <div class="col-md-8 col-sm-6 text-right"><?php 
	        $buttons = '';
			$referrer = Yii::$app->request->getReferrer();
			if (substr($referrer, strripos($referrer, '/') + 1) == 'create') {
				$buttons .= Html::a('<span class="glyphicon glyphicon-plus"></span>&nbsp;'.Yii::t('web', 'Add another delivery'), 
					['warehouse-delivery/create'], ['class' => 'btn btn-primary']);
			}
			if(!$delivery['isConfirmed']) {
				$buttons .= Html::a(Yii::t('web', 'Confirm delivery'), Url::to(['warehouse-delivery/confirm']), [
						'class' => 'btn btn-primary',
						'data' => [
								'confirm' => Yii::t('web', 'Are you sure you want to confirm this delivery?')
								.'<br />'
								.Yii::t('web', 'Warning, this action cannot be reverted and will cause warehouse state update'),
								'method' => 'post',
								'params' => [
										'id' => $delivery['id'],
								],
						],
				]);
				
				$buttons .= Html::a(Yii::t('main', 'Delete'), ['warehouse-delivery/delete'], [
	        			'class' => 'btn btn-default',
	        			'data' => [
	        					'confirm' => Yii::t('main', 'Are you sure you want to delete this item?'),
	        					'method' => 'post',
	        					'params' => [
	        							'id' => $delivery['id'],
	        					],
	        			],
	        	]);
			}
        	/*$buttons .= Html::a(Yii::t('web', 'Edit'), ['warehouse-delivery/edit', 'id' => $delivery['id']], ['class' => 'btn btn-primary'])		*/
        	echo $buttons;
        	?>

		</div>
	</div>
	
	<div class="col-xs-12 title-with-icon">
		<div class="base-icon basic-data-icon main-content-icons"></div>
		<h3><?= Yii::t('web', 'General information') ?></h3>
	</div>
	<div class="col-xs-12">
		<div class="row">
		    <div class="col-sm-9">	
				<div class="table-responsive">
					<table class="table table-bordered detail-table">
				    	<tbody>
				    		<?php if(Yii::$app->params['isMoreWarehousesAvailable']) { ?> 
					    		<tr>
					    			<th><?= Yii::t('web', 'Warehouse') ?></th>
					    			<td><?= Html::a($delivery['warehouseName'], Url::to(['warehouse/details', 'id' => $delivery['warehouseId']])) ?></td>
					    		</tr>
				    		<?php } ?>
			                <?php if(!empty($delivery['warehouseSupplier'])) { ?>
			                    <tr>
			                        <th><?= Yii::t('web', 'Supplier') ?></th>
			                        <td><?= Html::a($delivery['warehouseSupplier'], Url::to(['warehouse-supplier/details', 'id' => $delivery['warehouseSupplierId']])) ?></td>
			                    </tr>
			                <?php } ?>
				    		<tr>
				    			<th><?= Yii::t('web', 'Number') ?></th>
				    			<td><?= $delivery['number'] ?></td>
				    		</tr>
				    		<tr>
				    			<th><?= Yii::t('web', 'User confirming') ?></th>
			                    <td>
			                        <?php $userName = UserHelper::getPrettyUserName($delivery['userEmail'], $delivery['userFirstName'], $delivery['userLastName']) ?>
			                        <?= Html::a($userName, Url::to(['employee/details', 'id' => $delivery['userConfirmingId']])) ?>
			                    </td>
				    		</tr>
				    		<tr>
				    			<th><?= Yii::t('web', 'Date delivery') ?></th>
				    			<td><?= StringHelper::getFormattedDateFromTimestamp($delivery['dateDelivery']) ?></td>
				    		</tr>
				    		<tr>
				    			<th><?= Yii::t('web', 'Summary delivery cost') ?></th>
			                    <td>
			                        <?php 
			                            $currencySymbol = empty($listData['products']) ? '' : $listData['products'][0]['currencySymbol'];
			                            $totalCost = array_reduce($listData['products'], function($totalCost, $product) {
			                                if(!empty($product['price_group'])) {
			                                    return $totalCost + $product['price_group']*$product['currencyConversionValue']; 
			                                }
			                                else {
			                                    return $totalCost + $product['price_unit']*$product['count']*$product['currencyConversionValue'];
			                                }
			                            }, 0);
			                        ?>
			                        <?= StringHelper::getFormattedCost($totalCost).' '.$currencySymbol ?>
			                    </td>
				    		</tr>
				    		<?php if(!empty($delivery['operationNumber'])) { ?>
				    			<tr>
				    				<th><?= Yii::t('web', 'Related issue') ?></th>
				    				<td><?= Html::a($delivery['operationNumber'], Url::to(['warehouse-product-release/details', 'id' => $delivery['operationId']])) ?></td>
				    			</tr>
				    		<?php } ?>
				    		<tr>
				    			<th><?= Yii::t('web', 'Confirmed') ?></th>
				    			<td><?= StringHelper::boolTranslation($delivery['isConfirmed'], false) ?></td>
				    		</tr>
				    	</tbody>
					</table>
				</div>
			</div>
		    <div class="col-sm-3">
		    	<div class="row">
			       	<h3 class=""><?= Yii::t('web', 'Documents') ?></h3>
		            <?php if($delivery['isConfirmed'] && in_array(DocumentType::GOODS_RECEIVED, $availableDocuments)) { ?>
		            	<div class="document-link-container" ><div class="base-icon form-icon doc-icons"></div>
	                        <?= Html::a(''.' '.Yii::t('documents', 'Goods Received Note (GRN)'), 
	                            ['document/generate-goods-received', 'deliveryId' => $delivery['id']],
	                            ['target' => '_blank']
	                        ); ?>
	                    </div>
	                <?php } 
	                elseif(!$delivery['isConfirmed'] && in_array(DocumentType::DELIVERY_DETAILS, $availableDocuments)) { ?>
	                    <div class="document-link-container" ><div class="base-icon form-icon doc-icons"></div>
	                        <?= Html::a(''.' '.Yii::t('web', 'Order details'), 
	                            ['document/delivery-details', 'deliveryId' => $delivery['id']],
	                            ['target' => '_blank']
	                        ); ?>
						</div>
	                <?php } ?>
			     </div>
		    </div>
	    </div>
	</div>
	<div class="col-xs-12 title-with-icon">
		<div class="base-icon comment-icon main-content-icons"></div>
		<h3 class="page-subtitle"><?= Yii::t('web', 'Delivery comment') ?></h3>
	</div>
	<div class="description">
		<div class="col-xs-12">
			<?php 
				if (!empty($delivery['description'])) {
			?>
			<div class="col-sm-8 desc">
				<?php echo nl2br(Html::encode($delivery['description'])); ?>
			</div>
			<?php }
				else {
			?>
			<div class="col-sm-8 desc no-desc">
				<?php echo Yii::t('web', 'No comment.'); ?>
			</div>
			<?php 
				}
			?>
		</div>
	</div>
    <?= Yii::$app->controller->renderPartial('_products_list', $listData + ['deliveryId' => $delivery['id']]) ?>
</div>

<?php 
	EnhancedDialogAsset::register($this);
?>
