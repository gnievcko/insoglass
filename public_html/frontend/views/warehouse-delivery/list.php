<?php
use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\View;
use dosamigos\multiselect\MultiSelect;

$this->title = Yii::t('web', 'Warehouse deliveries') . ' - ' . Yii::$app->name;

$this->params['breadcrumbs'][] = ['label' => Yii::t('main', 'Warehouse'), 'url' => Url::to(['warehouse/index'])];
$this->params['breadcrumbs'][] = Yii::t('web', 'Warehouse deliveries');

$this->registerJsFile('@web/js/dynamic-table.js?'.uniqid(), []);
?>

<div id="warehouse-delivery-list-wrapper">    
    <div class="row">
		<div class="col-xs-6"><h2 class="page-title"><?= Yii::t('web', 'Warehouse deliveries') ?></h2></div>
		<div class="col-xs-6"><?= Html::a(Yii::t('web', 'Add warehouse delivery'), ['warehouse-delivery/create'], ['class' => 'btn btn-primary pull-right']) ?></div>
	</div>

    <div id="warehouse-delivery-list-table" class="dynamic-table">
    	<h5><?php echo Yii::t('main','Filters')?></h5>
        <div class="row dynamic-table-filters">
            <div class="col-md-3 col-sm-4">
            	<label><?php echo Yii::t('web','Search by number')?></label>
                    <input 
                        value="<?= $listForm->numberFilterString ?>" 
                        class="form-control dynamic-table-filter " 
                        type="text"
                        placeHolder="<?= $listForm->getAttributeLabel('numberFilterString') ?>"
                        name="<?= Html::getInputName($listForm, 'numberFilterString') ?>">

            </div>
            <?php if(Yii::$app->params['isMoreWarehousesAvailable']) { ?>
            	<input value="<?= $listForm->allWarehousesCount ?>" class="form-control dynamic-table-filter" type="hidden" name="<?= Html::getInputName($listForm, 'allWarehousesCount') ?>"/> 
	            <div class="col-md-3 col-sm-4">
	                <label><?php echo Yii::t('web','Filter by warehouse')?></label>
                    <?php 
                        $data = array_reduce($warehouses, function($options, $t) {
                            $options[$t['id']] = Html::encode($t['name']);
                            return $options;
                        }, []);
                        
                        if(!empty($data)) {
                        	echo $listForm->getAttributeLabel('warehouseIds');
                        	echo MultiSelect::widget([
                        			'id' => 'warehouses',
                        			"options" => ['multiple' => 'multiple', 'class' => 'dynamic-table-filter'],
                        			'data' => $data,
                        			'name' => Html::getInputName($listForm, 'warehouseIds'),
                        			'value' => empty($listForm->warehouseIds) ? array_keys($data) : $listForm->warehouseIds,
                        			"clientOptions" => [
                        					'nonSelectedText' => '',
                        					'nSelectedText' => mb_strtolower(Yii::t('main', 'Selected'), 'UTF-8'),
                        					'numberDisplayed' => 2,
                        					'buttonWidth' => '100%',
                        			],
                        	]);
                        } 
                    ?>	                
	            </div>
            <?php } ?>
            
            <div class="col-md-3 col-sm-4">
            	<label><?php echo Yii::t('web','Filter by supplier (verbally)')?></label>
                    <input 
                        value="<?= $listForm->supplierName ?>" 
                        class="form-control dynamic-table-filter " 
                        type="text"
                        placeHolder="<?= $listForm->getAttributeLabel('supplierName') ?>"
                        name="<?= Html::getInputName($listForm, 'supplierName') ?>">

            </div>
            
            <input value="<?= $listForm->allSuppliersCount ?>" class="form-control dynamic-table-filter" type="hidden" name="<?= Html::getInputName($listForm, 'allSuppliersCount') ?>"/> 
            <div class="col-md-3 col-sm-4 cat-select">
                <label><?php echo Yii::t('web','Filter by supplier')?></label>
                    <?php 
                        $data = array_reduce($suppliers, function($options, $t) {
                            $options[$t['id']] = Html::encode($t['name']);
                            return $options;
                        }, []);
                        
                        if(!empty($data)) {
                        	//echo $listForm->getAttributeLabel('supplierIds');
                        	echo MultiSelect::widget([
                        			'id' => 'suppliers',
                        			"options" => ['multiple' => 'multiple', 'class' => 'dynamic-table-filter'],
                        			'data' => array_map(function($name) { return Html::decode($name); }, $data),
                        			'name' => Html::getInputName($listForm, 'supplierIds'),
                        			'value' => empty($listForm->supplierIds) ? array_keys($data) : $listForm->supplierIds,
                        			"clientOptions" => [
											'nonSelectedText' => mb_strtolower(Yii::t('web', 'Select a supplier...'), 'UTF-8'),
                        					'nSelectedText' => mb_strtolower(Yii::t('web', 'Selected supplier'), 'UTF-8'),
                        					'numberDisplayed' => 0,
                        					'buttonWidth' => '100%',
                        			],
                        	]);
                        }                        		       
                    ?>               
            </div>  
            <?= Yii::$app->controller->renderPartial('/inc/listFiltersButtons', ['isMoreFilters' => false, 'isClearFilters' => true]) ?>
        </div>
        <div class="dynamic-table-data">
            <?= Yii::$app->controller->renderPartial('_deliveries', [
	                'items' => $items,
	                'pages' => $pages,
	                'listForm' => $listForm,
            ]) ?>
        </div>
    </div>
    <?php $this->registerJs('dynamicTable("warehouse-delivery-list-table", "'.Url::to(['warehouse-delivery/list']).'");', View::POS_END); ?>
</div>