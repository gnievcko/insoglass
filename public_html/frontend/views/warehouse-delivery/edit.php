<?php
use yii\helpers\Url;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

$this->registerJsFile('@web/js/forms.js?'.uniqid(), ['depends' => [\yii\web\JqueryAsset::className()]]);

$subtitle = Yii::t('web', 'Update delivery');
$this->title = $subtitle.' - '.Yii::$app->name;

$this->params['breadcrumbs'][] = ['label' => Yii::t('main', 'Warehouse'), 'url' => Url::to(['warehouse/index'])];
$this->params['breadcrumbs'][] = ['label' => Yii::t('web', 'List of deliveries'), 'url' => Url::to(['warehouse-delivery/list'])];
$this->params['breadcrumbs'][] = $subtitle;
?>

<div class="form">
	<div>
        <h2 class="page-title"><?= $subtitle ?></h2>
	</div>

	<?php $form = ActiveForm::begin([
			'id' => 'warehouse-delivery-form',
			'enableAjaxValidation' => true,
	]); ?>

    <?= Yii::$app->controller->renderPartial('inc/basicData', ['form' => $form, 'warehouseDeliveryForm' => $warehouseDeliveryForm]) ?>
    <?= Yii::$app->controller->renderPartial('inc/products', ['form' => $form, 'productsForms' => $productsForms]) ?>

	<div>
		<?= Html::a(Yii::t('web', 'Cancel'), 
        		!empty(Yii::$app->request->getReferrer()) ? Yii::$app->request->getReferrer() : ['warehouse-delivery/list'], 
        		['class' => 'btn btn-default']) ?>
       <?= Html::submitButton(Yii::t('main', 'Save'), ['class' => 'pull-right btn btn-primary']); ?>
    </div>
	
	<?php ActiveForm::end(); ?>	
</div>
