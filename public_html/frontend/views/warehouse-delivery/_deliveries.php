<?php 
use yii\helpers\Url;
use yii\helpers\Html;
use yii\widgets\LinkPager;
use frontend\models\WarehouseDeliveryListForm;
use common\bundles\EnhancedDialogAsset;
use common\helpers\StringHelper;
use common\widgets\TableHeaderWidget;
?>

<?php 
if (empty($items)) { ?>
	<label><?= Yii::t('web', 'No search results');?></label>
	<br><br>					
<?php }
else { ?>
<div class="table-responsive">
	<table class="<?= TableHeaderWidget::TABLECLASS ?>">
	    <?php echo TableHeaderWidget::widget(['fields' => WarehouseDeliveryListForm::getSortFields(), 'listForm' => $listForm]);?>
        <tbody>
            <?php foreach($items as $item): ?>
                <tr>
                    <td><?= Html::encode($item['number']) ?></td>
                    <?php if(Yii::$app->params['isMoreWarehousesAvailable']) { ?> 
                    	<td><?= Html::encode($item['warehouseName']) ?></td>
                    <?php } ?>
                    <td><?= Html::encode($item['supplierName']) ?></td>
                    <td><?= Html::encode($item['userConfirmingName']) ?></td>
                    <td><?= Html::encode(StringHelper::getFormattedDateFromTimestamp($item['dateDelivery'])) ?></td>
                    <td><?= Html::encode($item['positionCount']) ?></td>
                    <td><?= Html::encode(StringHelper::getFormattedCost($item['totalPrice']) . ' ' . $item['currencySymbol']) ?></td>
                    <td><?php 
                    	echo Html::a('', Url::to(['warehouse-delivery/details', 'id' => $item['id']]), ['class' => 'base-icon details-icon action-icon', 'title' => Yii::t('web', 'Details')]);
                    	if(!$item['isConfirmed']) {
                    		echo '&nbsp;'.Html::a('', Url::to(['warehouse-delivery/confirm']), [
                    				'class' => 'base-icon ok-icon action-icon',
                    				'data' => [
                    						'confirm' => Yii::t('web', 'Are you sure you want to confirm this delivery?')
                    									 .'<br />'
                    									 .Yii::t('web', 'Warning, this action cannot be reverted and will cause warehouse state update'),
                    						'method' => 'post',
                    						'params' => [
                    								'id' => $item['id'],
                    						],
                    				],
                    		]);
                    		echo '&nbsp;'.Html::a('', Url::to(['warehouse-delivery/delete']), [
                    				'class' => 'base-icon delete-icon action-icon',
                    				'data' => [
                    						'confirm' => Yii::t('main', 'Are you sure you want to delete this item?'),
                    						'method' => 'post',
                    						'params' => [
                    								'id' => $item['id'],
                    						],
                    				],
                    		]);
                    	}
                    	//echo '&nbsp;'.Html::a('', Url::to(['warehouse-delivery/edit', 'id' => $item['id']]), ['class' => 'icon-img edit-icon']);
                    ?></td>
                </tr>
            <?php endforeach ?>
        </tbody>
    </table>
</div>
<div class="col-sm-12">
	<div class="col-sm-4 text-left dynamic-table-counter">
		<?= Yii::t('web', 'Number of found items: {n}', ['n' => $pages->totalCount]) ?>
	</div>
	<div class="col-sm-8 text-right dynamic-table-pagination">
	    <?= LinkPager::widget(['pagination' => $pages ]) ?>
	</div>
</div>
<?php }?>

<input class="dynamic-table-sort-dir" type="hidden" name="<?= Html::getInputName($listForm, 'sortDir') ?>" value="<?= $listForm->sortDir ?>" />
<input class="dynamic-table-sort-field" type="hidden" name="<?= Html::getInputName($listForm, 'sortField') ?>" value="<?= $listForm->sortField ?>" />
<input class="dynamic-table-page" type="hidden" name="<?= Html::getInputName($listForm, 'page') ?>" value="<?= $listForm->page ?>" />

<?php 
	EnhancedDialogAsset::register($this);
?>
