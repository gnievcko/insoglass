<?php

use common\models\aq\TaskTypeQuery;
use common\models\aq\AlertTypeQuery;
use common\models\aq\OrderStatusQuery;
use yii\helpers\Url;
use yii\web\JqueryAsset;
use common\models\aq\AlertQuery;
use common\models\aq\TaskQuery;
use common\helpers\StringHelper;

$this->registerJsFile('@web/js/chart/rolling.js?'.uniqid(), ['depends' => [JqueryAsset::className(), dosamigos\chartjs\ChartJsAsset::className()]]);
$this->title = Yii::t('web', 'Dashboard') . ' - ' . Yii::$app->name;
?>
<div class="col-xs-12">
	<div class="row">
		<div class="col-md-4 col-sm-6">
				<div class="col-sm-12 well rollingChart">
					<?php
						echo Yii::$app->controller->renderPartial('/chart/piechart', [
								'data' => TaskTypeQuery::getTaskTypeAndTaskCountQuery(),
								'legendText' => '{n,plural,=0{tasks} =1{task} other{tasks}}',
								'icon' => 'glyphicon glyphicon-tasks',
								'title' => Yii::t('web', 'Number of tasks'),
						]);
					?>
				</div>
		</div>
		<div class="col-md-4 col-sm-6">
				<div class="col-sm-12 well rollingChart">
					<?php
						echo Yii::$app->controller->renderPartial('/chart/piechart', [
								'data' => AlertTypeQuery::getAlertTypeAndAlertCountQuery(),
								'legendText' => '{n,plural,=0{problems} =1{problem} other{problems}}',
								'icon' => 'glyphicon glyphicon-alert',
								'title' => Yii::t('web', 'Number of alerts'),
						]);
					?>
				</div>
		</div>
		<div class="col-md-4 col-sm-6 col-md-offset-0 col-sm-offset-3">
				<div class="col-sm-12 well rollingChart">
					<?php
						echo Yii::$app->controller->renderPartial('/chart/piechart', [
								'data' => OrderStatusQuery::getOrderStatusAndOrderCountQuery(),
								'legendText' => Yii::$app->params['isOrdersCalledOffers'] ? '{n,plural,=0{offers} =1{offer} other{offers}}' : '{n,plural,=0{orders} =1{order} other{orders}}' ,
								'icon' => 'glyphicon glyphicon-file',
								'title' => StringHelper::translateOrderToOffer('web', 'Current orders'),
						]);
					?>
				</div>
		</div>
	</div>
</div>
<?php
$script = '
	$.get("'.Url::to(['alert/short-list']).'"+window.location.search, {dashboard: true}, function(data) {
  		$("#alert-short-list").html(data);
	});	
		
	$.get("'.Url::to(['task/short-list']).'"+window.location.search, {dashboard: true}, function(data) {
  		$("#task-short-list").html(data);
	});
';
$this->registerJs($script); ?>
<div class="col-xs-12"><hr class="hr-dashboard"/></div>
<div class="col-sm-6 col-xs-12">
	<div class="row">
		<div class="col-sm-12 well" id="alert-short-list">
	
		</div>
	</div>
</div>
<div class="col-sm-6 col-xs-12">
	<div class="row">
    	<div class="col-sm-12 well" id="task-short-list">
    		
    	</div>
	</div>
</div>
<div class="col-xs-12"><hr/></div>
<div class="col-sm-6 col-xs-12">
	<div class="row">
		<div class="col-xs-12 well line-chart" id="lineChart">
			<?php
		
	
				$this->registerJsFile('@web/js/chart/tooltip.js?'.uniqid(), ['depends' => [JqueryAsset::className(),dosamigos\chartjs\ChartJsAsset::className()]]);
				$this->registerJsFile('@web/js/chart/loader.js?'.uniqid(), ['depends' => [JqueryAsset::className(),dosamigos\chartjs\ChartJsAsset::className()]]);
	            $this->registerJs('var loaderLine = function(){new loader(1, '."'".Url::to(['chart/line'])."'".','."'".'#lineChart'."'".', null, '."'".'line1'."'".',0);}');
	//TODO	
	/*
				$data = array();
				$data['xAxis'] = ['STY', 'LUT', 'MAR', 'KWI', 'MAJ', 'CZE', 'LIP', 'SIE', 'WRZ', 'PAZ', 'LIS', 'GRU'];
				$queryData = AlertQuery::getAlertPriorityCountInGivenInterval(date('Y-m-d', strtotime('Jan 1')), date('Y-m-d', strtotime('Dec 31')));
				$data['data'] = $queryData[1];
				$data['labels'] = $queryData[0];
				$data['div'] = '#lineChart'; // outter div
				$data['chartId'] = 'line1';
				$data['urlAjax'] = Url::to(['chart/line']);
				$data['type'] = 1;
				$data['title'] = Yii::t('web', 'Alert number in time');
				echo Yii::$app->controller->renderPartial('/chart/line', $data);
	*/
			?>
		</div>
	</div>
</div>
<div class="col-sm-6 col-xs-12">
	<div class="row hidden-xs">
		<div class="col-sm-12 well line-chart" id="barChart">
			<?php
		
				$this->registerJsFile('@web/js/chart/tooltip.js?'.uniqid(), ['depends' => [JqueryAsset::className(),dosamigos\chartjs\ChartJsAsset::className()]]);
				$this->registerJsFile('@web/js/chart/loader.js?'.uniqid(), ['depends' => [JqueryAsset::className(),dosamigos\chartjs\ChartJsAsset::className()]]);
				$this->registerJs('var loaderBar = function(){new loader(1, '."'".Url::to(['chart/bar'])."'".','."'".'#barChart'."'".', null, '."'".'bar1'."'".', 0);}');
		//TODO
	            /*
	
				$data = array();
				$data['xAxis'] = ['STY', 'LUT', 'MAR', 'KWI', 'MAJ', 'CZE', 'LIP', 'SIE', 'WRZ', 'PAZ', 'LIS', 'GRU'];
				$data['data'] = TaskQuery::getFinishedTaskaCountInGivenInterval(date('Y-m-d', strtotime('Jan 1')), date('Y-m-d', strtotime('Dec 31')));
				$data['labels'] = [Yii::t('web', 'Completed tasks')];
				$data['div'] = '#barChart'; // outter div
				$data['chartId'] = 'bar1';
				$data['urlAjax'] = Url::to(['chart/bar']);
				$data['type'] = 1;
				$data['title'] = common\helpers\StringHelper::translateOrderToOffer('web', 'Order value');
				echo Yii::$app->controller->renderPartial('/chart/bar', $data);
	*/
			?>
		</div>
	</div>
</div>

<?php
$loadScript = '
var l=3;//count of chart    
Chart.pluginService.register({
    afterDraw : function(chart){
        l--;
        if(l == 0) {
            loadOtherCharts();
        }
    }
});
loadOtherCharts = function() {
console.log(loaderBar);
    loaderBar();
    loaderLine();
};

';
$this->registerJs($loadScript);

?>
