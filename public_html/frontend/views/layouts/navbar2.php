<?php
use yii\web\JqueryAsset;
use frontend\helpers\MenuHelper;

$this->registerJsFile('@web/js/layout/leftMenu.js?'.uniqid(), ['depends' => [JqueryAsset::className()]]);
?>

<nav class="navbar navbar-default navbar2">
	<div class="container-fluid">
		<div class="navbar-header">
			<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-2" aria-expanded="false">
				<span class="sr-only">Toggle navigation</span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
			</button>
		</div>
		<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-2">
<?php echo MenuHelper::leftMenuRender($menus); ?>
		</div>
	</div>
</nav>