<?php

$menuOptions = [
	'link' => ['production', 'index'],
	'linkUrl' => ['production/index'],
	'text' => Yii::t('web', 'Production'),
	'id' => 'production',
	'menu' => [
		[
			'link' => ['production', 'index'],
			'linkUrl' => ['production/index'],
			'text' => Yii::t('web', 'Production'),
			'icon' => '<div class="base-icon production-icon nav-left-icons"></div>',
			'id' => '',
		    'menu' => [
		        [
                    'link' => ['production', 'production-order-details'],
		        ],
		    ],
		],
		[
			'link' => ['production', 'department-orders'],
			'linkUrl' => ['production/department-orders'],
			'text' => Yii::t('web', 'Orders for department'),
			'icon' => '<div class="base-icon order-icon nav-left-icons"></div>',
			'id' => '',
		    'menu' => [
                [
                    'link' => ['production', 'order-details'],
		        ],
		        [
		            'link' => ['production', 'add-items'],  
		        ],
		        [
		            'link' => ['production', 'production-details'],  
		        ],
		        [
                    'link' => ['qa', 'create'],    
		        ],
		    ],
		],
		[
			'link' => ['production-path ', 'index'],
			'linkUrl' => ['production-path/list'],
			'text' => Yii::t('web', 'Production paths'),
			'icon' => '<div class="base-icon production-path-icon nav-left-icons"></div>',
			'id' => '',
		    'menu' => [
                [
                    'link' => ['production-path', 'list'],
                ],
		        [
                    'link' => ['production-path', 'create'],
		        ],
		        [
                    'link' => ['production-path', 'edit'],
		        ],
		        [
		            'link' => ['production-path', 'save-step-path'],
		        ],
		        [
                    'link' => ['production-path', 'get-production-path-steps'],
		        ],
		        [
                    'link' => ['production-path', 'details'],
		        ],
		    ],
		],
	    [
    	    'link' => ['department', 'list'],
    	    'linkUrl' => ['department/list'],
    	    'text' => Yii::t('web', 'Departments'),
    	    'icon' => '<div class="base-icon department-icon nav-left-icons"></div>',
    	    'id' => '',
    	    'menu' => [
    	        [
    	            'link' => ['department', 'create'],
    	        ],
    	        [
    	            'link' => ['department', 'edit'],
    	        ],
    	        [
    	            'link' => ['department', 'details'],
    	        ],
    	    ],
	    ],
	    [
    	    'link' => ['activity ', 'list'],
    	    'linkUrl' => ['activity/list'],
    	    'text' => Yii::t('web', 'Activities'),
    	    'icon' => '<div class="base-icon activity-icon nav-left-icons"></div>',
    	    'id' => '',
    	    'menu' => [
    	        [
    	            'link' => ['activity', 'list'],
    	        ],
    	        [
    	            'link' => ['activity', 'create'],
    	        ],
    	        [
    	            'link' => ['activity', 'edit'],
    	        ],
    	        [
    	            'link' => ['activity', 'details'],
    	        ],
    	    ],
	    ],
	    [
    	    'link' => ['workstation ', 'list'],
    	    'linkUrl' => ['workstation/list'],
    	    'text' => Yii::t('web', 'Workstations'),
    	    'icon' => '<div class="base-icon workstation-icon nav-left-icons"></div>',
    	    'id' => '',
    	    'menu' => [
    	        [
    	            'link' => ['workstation', 'list'],
    	        ],
    	        [
    	            'link' => ['workstation', 'create'],
    	        ],
    	        [
    	            'link' => ['workstation', 'edit'],
    	        ],
    	        [
    	            'link' => ['workstation', 'details'],
    	        ],
    	    ],
	    ],
	],
];

return $menuOptions;