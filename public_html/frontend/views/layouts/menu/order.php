<?php

use common\helpers\StringHelper;

$clientOption = [];
if(Yii::$app->params['isClientManagementVisibie']) {
    $clientOption[] = [
        'link' => ['client', 'create'],
        'linkUrl' => ['client/create'],
        'text' => Yii::t('web', 'Add'),
        'icon' => '<div class="base-icon add-icon nav-more"></div>',
        'id' => '',
    ];
}

$clientOptions = [
    [
        'link' => ['client', 'list'],
        'linkUrl' => ['client/list'],
        'text' => Yii::t('web', 'List of clients'),
        'icon' => '<div class="base-icon list-icon-list nav-more"></div>',
        'id' => '',
    ],
    [
        'link' => ['client-group', 'list'],
        'linkUrl' => ['client-group/list'],
        'text' => Yii::t('web', 'List of clients groups'),
        'icon' => '<div class="base-icon list-icon-list nav-more"></div>',
        'id' => '',
    ],
	[
		'link' => ['client', 'create'],	
	],
    [
        'link' => ['client', 'edit'],
    ],
    [
        'link' => ['client', 'details'],
    ],
    [
        'link' => ['client-group', 'edit'],
    ],
    [
        'link' => ['client-group', 'details'],
    ],
    [
        'link' => ['client-group', 'create'],
    ],
	[
		'link' => ['client-representative', 'list'],
	],
	[
		'link' => ['client-representative', 'create'],
	],
	[
		'link' => ['client-representative', 'edit'],
	],
	[
		'link' => ['client-representative', 'details'],
	],
];

if(Yii::$app->params['isClientRepresentativeManagementVisible']) {
	$clientOptions[] = [
		'link' => ['client-representative', 'list'],
		'linkUrl' => ['client-representative/list'],
		'text' => Yii::t('web', 'List of clients with access to the system'),
		'icon' => '<div class="base-icon list-icon-list nav-more"></div>',
		'id' => '',
	];
}
$clientOptions = array_merge($clientOption, $clientOptions);

$offeredProductOption = [];
if(Yii::$app->params['canCudOfferedProducts']) {
	$offeredProductOption[] = [
			'link' => ['offered-product', 'create'],
			'linkUrl' => ['offered-product/create'],
			'text' => Yii::t('web', 'Add'),
			'icon' => '<div class="base-icon add-icon nav-more"></div>',
			'id' => '',
	];
}

$offeredProductOptions = [
    [
    	'link' => ['offered-product', 'list'],
        'linkUrl' => ['offered-product/list'],
        'text' => Yii::t('web', 'List of offered products'),
        'icon' => '<div class="base-icon list-icon-list nav-more"></div>',
        'id' => '',
    ],
    [
        'link' => ['offered-product-category', 'list'],
        'linkUrl' => ['offered-product-category/list'],
        'text' => Yii::t('web', 'List of categories'),
        'icon' => '<div class="base-icon list-icon-list nav-more"></div>',
        'id' => '',
    ],
	[
		'link' => ['offered-product', 'create'],
	],
    [
        'link' => ['offered-product', 'edit'],
    ],
    [
        'link' => ['offered-product', 'details'],
    ],
    [
        'link' => ['offered-product-category', 'edit'],
    ],
    [
        'link' => ['offered-product-category', 'details'],
    ],
    [
        'link' => ['offered-product-category', 'create'],
    ]
];

$offeredProductOptions = array_merge($offeredProductOption, $offeredProductOptions);

$menuOptions = [
    'link' => ['order', 'index'],
    'linkUrl' => ['order/index'],
    'text' => StringHelper::translateOrderToOffer('main', 'Orders'),
    'id' => 'order',
    'menu' => [
        [
            'link' => ['order', 'list'],
            'linkUrl' => ['order/list'],
            'text' => StringHelper::translateOrderToOffer('main', 'List of orders'),
            'icon' => '<div class="base-icon list-icon-list nav-left-icons"></div>',
            'id' => '',
        ],
        [
            'link' => ['order', 'create'],
            'linkUrl' => ['order/create'],
            'text' => StringHelper::translateOrderToOffer('web', 'Create order'),
            'icon' => '<div class="base-icon add-task-icon nav-left-icons"></div>',
            'id' => '',
        ],
        [
            'link' => ['client ', 'list'],
            'linkUrl' => '#',
            'text' => Yii::t('main', 'Clients'),
            'icon' => '<div class="base-icon client-icon nav-left-icons"></div>',
            'id' => '',
            'menu' => $clientOptions
                
        ],
        [
            'link' => ['offered-product ', 'list'],
            'linkUrl' => '#',
            'text' => Yii::t('main', 'Offered products'),
            'icon' => '<div class="base-icon basket-icon nav-left-icons"></div>',
            'id' => '',
            'menu' => $offeredProductOptions,
        ],
        [
            'link' => ['order', 'details'],
        ],
        [
            'link' => ['order', 'update'],
        ],
        [
            'link' => ['order', 'quick-update'],
        ],
        [
            'link' => ['order', 'copy'],
        ],
        [
            'link' => ['order', 'edit'],
        ],
        [
            'link' => ['order-type', 'list'],
        ],
        [
            'link' => ['order-type', 'details'],
        ],
        [
            'link' => ['order-type', 'create'],
        ],
        [
            'link' => ['order-type', 'edit'],
        ],
    ],
];

if(isset(Yii::$app->params['isDocumentStorageAvailable']) && Yii::$app->params['isDocumentStorageAvailable']) {
    $menuOptions['menu'][] = [
        'link' => ['document-list', 'order'],
        'linkUrl' => ['document-list/order'],
        'text' => Yii::t('web', 'History documents'),
        'icon' => '<div class="base-icon pdf-icon-list nav-left-icons"></div>',
        'id' => '',
    ];
}

return $menuOptions;
