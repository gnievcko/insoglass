<?php
return [
		'link' => ['employee', 'index'],
		'linkUrl' => ['employee/index'],
		'text' => Yii::t('main', 'Employees'),
		'id' => 'employee',
		'menu' => [
				[
						'link' => ['employee', 'list'],
						'linkUrl' => ['employee/list'],
						'text' => Yii::t('web', 'List of employees'),
						'icon' => '<div class="base-icon employee-icon nav-left-icons"></div>',
						'id' => '',
				],
				[
						'link' => ['employee', 'create'],
						'linkUrl' => ['employee/create'],
						'text' => Yii::t('web', 'Add employee'),
						'icon' => '<div class="base-icon add-employee-icon nav-left-icons"></div>',
						'id' => '',
				],
				[
						'link' => ['employee', 'details'],	
				],
				[
						'link' => ['employee', 'edit'],
				],
		],
];