<?php

$menuOptions = [
		'link' => array('warehouse','index'),
		'linkUrl' => array('warehouse/index'),
		'text' => Yii::t('web', 'Warehouse'),
		'id' => 'warehouse',
		'menu' => [
				[
						'link' => array('warehouse', 'index'),
						'linkUrl' => '#',
						'text' => Yii::t('web', 'Warehouse'),
						'icon' => '<div class="base-icon warehouse-icon nav-left-icons"></div>',
						'id' => '',
						'menu' => [
								[
										'link' => array('warehouse-product', 'list'),
										'linkUrl' => array('warehouse-product/list'),
										'text' => Yii::t('web', 'Products in warehouse'),
										'icon' => '<div class="base-icon warehouse-icon nav-more"></div>',
										'id' => '',
								],
								[
										'link' => array('warehouse', 'details'),
										'linkUrl' => array('warehouse/details'),
										'text' => Yii::t('web', 'Warehouse details'),
										'icon' => '<div class="base-icon info-icon nav-more"></div>',
										'id' => '',
								],
								[
										'link' => array('warehouse-product', 'details'),
								],
								[
										'link' => array('warehouse', 'edit'),
								]
						],
		
				],
				[
						'link' => array('warehouse-product', 'create'),
						'linkUrl' => '#',
						'text' => Yii::t('web', 'Products warehouse'),
						'icon' => '<div class="base-icon box-icon nav-left-icons"></div>',
						'id' => '',
						'menu' => [
								[
										'link' => array('warehouse-product', 'create'),
										'linkUrl' => array('warehouse-product/create'),
										'text' => Yii::t('web', 'Add'),
										'icon' => '<div class="base-icon add-icon nav-more"></div>',
										'id' => '',
								],
								[
										'link' => array('warehouse-product', 'update'),
										'linkUrl' => array('warehouse-product/update'),
										'text' => Yii::t('web', 'Operations'),
										'icon' => '<div class="base-icon operation-icon nav-more"></div>',
										'id' => '',
								],
								[
										'link' => ['warehouse-product', 'reservation-list'],
										'linkUrl' => ['warehouse-product/reservation-list'],
										'text' => Yii::t('web', 'Reservations'),
										'icon' => '<div class="base-icon reservation-icon nav-more"></div>',
										'id' => '',
								],
								[
										'link' => ['warehouse-product-issue', 'list'],
										'linkUrl' => ['warehouse-product-issue/list'],
										'text' => Yii::t('web', 'Issues'),
										'icon' => '<div class="base-icon issue-icon nav-more"></div>',
										'id' => '',
								],
								[
										'link' => array('warehouse-product', 'create'),
								],
								[
										'link' => array('warehouse-product-issue', 'details'),
								],
						],
				],
				[
						'link' => array('product ', 'list'),
						'linkUrl' => '#',
						'text' => Yii::t('web', 'Products directory'),
						'icon' => '<div class="base-icon product-category nav-left-icons"></div>',
						'id' => '',
						'menu' => [
								[
										'link' => array('product', 'create'),
										'linkUrl' => array('product/create'),
										'text' => Yii::$app->params['isOfferedProductDependOnProduct'] ? Yii::t('web', 'Add products and intermediate products') : Yii::t('web', 'Add'),
										'icon' => '<div class="base-icon add-icon nav-more"></div>',
										'id' => '',
								],
								[
										'link' => array('product', 'list'),
										'linkUrl' => array('product/list'),
										'text' => Yii::t('web', 'Product list'),
										'icon' => '<div class="base-icon list-icon-list nav-more"></div>',
										'id' => '',
								],
								[
										'link' => array('product-category', 'list'),
										'linkUrl' => array('product-category/list '),
										'text' => Yii::t('web', 'List of categories'),
										'icon' => '<div class="base-icon list-icon-list nav-more"></div>',
										'id' => '',
								],
								[
										'link' => array('product', 'edit'),
								],
								[
										'link' => array('product', 'details'),
								],
								[
										'link' => array('product-category', 'edit'),
								],
								[
										'link' => array('product-category', 'details'),
								],
								[
										'link' =>  array('product-category', 'create'),
								]
						],
				],
				[
						'link' => array('warehouse-delivery', 'list'),
						'linkUrl' => '#',
						'text' => Yii::t('web', 'Warehouse deliveries'),
						'icon' => '<div class="base-icon delivery-icon nav-left-icons"></div>',
						'id' => '',
						'menu' => [
								[
										'link' => array('warehouse-delivery', 'create'),
										'linkUrl' => array('warehouse-delivery/create'),
										'text' => Yii::t('web', 'Add warehouse delivery'),
										'icon' => '<div class="base-icon add-icon nav-more"></div>',
										'id' => '',
								],
								[
										'link' => array('warehouse-delivery', 'list'),
										'linkUrl' => array('warehouse-delivery/list '),
										'text' => Yii::t('web', 'List of delivery'),
										'icon' => '<div class="base-icon list-icon-list nav-more"></div>',
										'id' => '',
								],
								[
										'link' => array('warehouse-supplier', 'list'),
										'linkUrl' => array('warehouse-supplier/list'),
										'text' => Yii::t('web', 'Storekeepers'),
										'icon' => '<div class="base-icon supplier-icon nav-more"></div>',
										'id' => '',
								],
								[
										'link' => array('warehouse-delivery', 'edit'),
								],
								[
										'link' => array('warehouse-delivery', 'details'),
								],
								[
										'link' => array('warehouse-supplier', 'create'),
								],
								[
										'link' => array('warehouse-supplier', 'details'),
								],
								[
										'link' => array('warehouse-supplier', 'edit'),
								]
						],
				],
		],
];

if(isset(Yii::$app->params['isDocumentStorageAvailable']) && Yii::$app->params['isDocumentStorageAvailable']) {
	$menuOptions['menu'][] = [
			'link' => ['document-list', 'warehouse'],
			'linkUrl' => ['document-list/warehouse'],
			'text' => Yii::t('web', 'History documents'),
			'icon' => '<div class="base-icon pdf-icon-list nav-left-icons"></div>',
			'id' => '',
	];
}

return $menuOptions;