<?php
	use yii\helpers\Html;
	use yii\bootstrap\Nav;
	use yii\bootstrap\NavBar;
	use yii\widgets\Breadcrumbs;
	use frontend\assets\AppAsset;
	use common\helpers\Utility;
	use common\widgets\Alert;
	use common\helpers\StringHelper;
?>
<footer class="container-fluid footer">
	<div class="container">
		<div class="row">
			<!--  <div class="col-sm-12 lead col-xs-10 col-xs-offset-1 col-sm-offset-0">INFOLINIA XXX</div>-->
		</div>
	<div class="row">
		<div class="col-xs-12 text-center">
			<?= Html::img(Yii::$app->getUrlManager()->getBaseUrl() . '/images/ue.png', ['class' => 'ue-logo', 'alt' => 'ue-logo']) ?>
		</div>
		<div class="col-sm-12 col-xs-10 col-xs-offset-1 col-sm-offset-0 small text-center">
			<?= Yii::$app->params['owner']['name'] ?>
			<?= Yii::$app->params['owner']['address'] ?>
			<?= Yii::$app->params['owner']['phone'] ?>
			<?= Yii::t('main', 'E-mail') ?>: <?= Html::mailto(Yii::$app->params['owner']['email'], Yii::$app->params['owner']['email']) ?>
			<?php //echo Html::a(Yii::$app->getRequest()->getHostInfo(), Yii::$app->getRequest()->getHostInfo()) ?>
		</div>
	</div>
	<div class="row text-center mindseater small">
		<div class="col-xs-12">&copy; <?= date('Y') ?> by INSOGLAS Andrzej i Karol Woźniak sp.j. &#183; created by <?= Html::a(Yii::$app->params['implementingCompany'], 'http://mindseater.com/') ?></div>
	</div>
	</div>
</footer>
