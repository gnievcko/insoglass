<?php
use dosamigos\chartjs\ChartJs;
use yii\web\JsExpression;
use yii\web\JqueryAsset;
use frontend\helpers\ChartHelper;

if(!Yii::$app->request->isAjax){
	$this->registerJsFile('@web/js/chart/tooltip.js?'.uniqid(), ['depends' => [JqueryAsset::className(),dosamigos\chartjs\ChartJsAsset::className()]]);
	$this->registerJsFile('@web/js/chart/loader.js?'.uniqid(), ['depends' => [JqueryAsset::className(),dosamigos\chartjs\ChartJsAsset::className()]]);
}
?>
<div class="chart row">
	<div class="col-xs-8 col-xs-offset-2">
		<div class="title"><?= $title ?></div>
	</div>
    <?=
        Yii::$app->controller->renderPartial('/chart/changePeriod', [
            'begin' => $begin,
            'end' => $end,
        ]);
    ?>
    <?= ChartHelper::generateChanger($type, $urlAjax, $div, $chartId); ?>

	<?= ChartHelper::generateLegend($labels)?>

	<?=
	ChartJs::widget([
			'type' => 'bar',
			'options' => [
					'height' => 350,
					'width' => 600,
					'id' => $chartId,
			],
			'clientOptions' => [
					'legend' => [
							'display' => false,
					],
					'responsive' => true,
					'scales' => [
							'xAxes' => [[
									'stacked' => true,
							]],
							'yAxes' => [[
									'stacked'=> true,
									'ticks' => [
											// TODO: Zobaczyc, czy da sie lepiej to zrobic
											'beginAtZero' => true,
									],
							]]
					],
					'tooltips' =>[
							'enabled' => false,
							'mode' => 'label',
							'bodyFontSize' => 12,
							'bodyFontFamily' => 'Arial',
							'bodyFontColor' => '#000',
							'caretSize' => 11,
							'custom' => new JsExpression('window.custom'),

					]
			],

			'data' => [
					'labels' => $xAxis,
					'datasets' => ChartHelper::generateDataLine($data, $labels),
			]
	]);


	?>
</div>
