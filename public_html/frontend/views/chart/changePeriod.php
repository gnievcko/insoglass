

<div class="col-xs-12">
	<div class="row">
		<div class="col-xs-2">
    		<button class="btn btn-primary prevInterval">
    			<span class="glyphicon glyphicon-chevron-left"></span>
    		</button>
    	</div>
    	<div class="col-xs-8 text-center">
    		<div class="date"><?= $begin.' - '.$end ?></div>
    	</div>
    	<div class="col-xs-2">
    		<button class="btn btn-primary nextInterval">
    			<span class="glyphicon glyphicon-chevron-right"></span>
    		</button>
    	</div>
    </div>
</div>