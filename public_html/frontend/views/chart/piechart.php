<?php
use dosamigos\chartjs\ChartJs;
use yii\helpers\ArrayHelper;
use yii\web\JsExpression;
use frontend\helpers\ChartHelper;
?>

<div class="chart">
	<div class="title"><?= $title ?></div>

	<?php
		$sumInDoughnut = array_sum(ArrayHelper::getColumn($data, 'number'));

		echo ChartJs::widget([
    			'type' => 'doughnut',
    			'options' => [
						'height' => 100,
						'width' => 100,
				],
				'clientOptions' => [
						'legend' => [
								//'position' => 'bottom',
								'display' => 'false',
						],
						'cutoutPercentage' => 80,
						'tooltips' => [
								'enabled' => false,
						],
						'animation' => [
								'animateScale' => true,
								'animateRotate' => false,
						],
						'onComplete' => new JsExpression('window.rolling'),
						'elements' => [
								'center' => [
										// the longest text that could appear in the center
										'maxText' => 'YYYYYYYYYYYYY',
										'text' =>  $sumInDoughnut > 0 ? $sumInDoughnut : '' ,
										'text2' => $sumInDoughnut > 0 ? Yii::t('web', $legendText, ['n' => $sumInDoughnut]) : Yii::t('web', 'No data'),
										//'fontColor' => '#36A2EB',
										'fontFamily' => "tReg",
										'fontStyle' => 'normal',
										'fontSize' => 40,
										'fontSize2' => 25,
										// if a fontSize is NOT specified, we will scale (within the below limits) maxText to take up the maximum space in the center
										// if these are not specified either, we default to 1 and 256
										'minFontSize' => 1,
										'maxFontSize' => 256,
								]
						],
				],
    			'data' => [
        				'labels' => [],//ArrayHelper::getColumn($data, 'name'),
        				'datasets' => [
            					[
            							'backgroundColor' => ArrayHelper::getColumn(ChartHelper::COLORS, 'background'),
            							'hoverBackgroundColor' => ArrayHelper::getColumn(ChartHelper::COLORS, 'background'),
						                'data' => ArrayHelper::getColumn($data, 'number'),
            					],
        				]
   		 		]
		]);
		?>
		<br>
		<?php for ($i = 0; $i < count($data); $i++) { ?>
		<div class="legendItem">
			<?php if(!empty($data[$i]['tooltipText'])) {?>
				<a data-toggle="tooltip" data-placement="top" title="<?= $data[$i]['tooltipText']?>">
			<?php }?>
			<span class="<?= $icon ?>" aria-hidden="true" style="color:<?= ChartHelper::COLORS[$i % count(ChartHelper::COLORS)]['background']?>"></span>
				<span><?= $data[$i]['number'] . ' ' . Yii::t('web', $legendText, ['n' => intval($data[$i]['number'])])
						. ' ' . Yii::t('web', 'of type') . ' ' . $data[$i]['name']; ?></span>
				<?php if(!empty($data[$i]['tooltipText'])) {?>
				  </a>
				<?php }?>
			</div>
		<?php }
		$this->registerJsFile('@web/js/chart/doughnut-chart-center.js?'.uniqid(), ['depends' => [dosamigos\chartjs\ChartJsAsset::className()]]);
	?>
</div>
