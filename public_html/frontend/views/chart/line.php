<?php
use dosamigos\chartjs\ChartJs;
use yii\web\JqueryAsset;
use yii\web\JsExpression;
use frontend\helpers\ChartHelper;

if(!Yii::$app->request->isAjax){
	$this->registerJsFile('@web/js/chart/tooltip.js?'.uniqid(), ['depends' => [JqueryAsset::className(),dosamigos\chartjs\ChartJsAsset::className()]]);
	$this->registerJsFile('@web/js/chart/loader.js?'.uniqid(), ['depends' => [JqueryAsset::className(),dosamigos\chartjs\ChartJsAsset::className()]]);
}
?>
<div class="chart row">
	<div class="col-sm-8 col-sm-offset-2">
		<div class="title"><?= $title ?></div>
	</div>
    
    <?=
        Yii::$app->controller->renderPartial('/chart/changePeriod', [
            'begin' => $begin,
            'end' => $end,
        ]);
    ?>
    
		<?= ChartHelper::generateChanger($type, $urlAjax, $div, $chartId);?>
	
		<?= ChartHelper::generateLegend($labels)?>
	
		<?=
		ChartJs::widget([
				'type' => 'line',
				'options' => [
						'height' => 350,
						'width' => 600,
						'id' => $chartId,
				],
				'clientOptions' => [
						'legend' => [
								'display' => false,
								'labels' => [
										'hidden' => true,
								]
						],
						'scales' => [
								'yAxes' => [[
										'ticks' => [
												// TODO: Zobaczyc, czy da sie lepiej to zrobic
												'override' => true,
												'stepValue' => 1,
												'beginAtZero' => true,
										],
								]]
						],
						'tooltips' =>[
								'enabled' => false,
								'custom' => new JsExpression('window.custom'),
								'bodyFontSize' => 12,
								'bodyFontFamily' => 'tReg',
								'bodyFontColor' => '#000',
								'caretSize' => 11,
						]
				],
				'data' => [
						'labels' => $xAxis,
						'datasets' => ChartHelper::generateDataLine($data, $labels),
				]
		]);
		?>
	
</div>
