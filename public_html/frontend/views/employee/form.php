<?php
use yii\helpers\Url;
use yii\helpers\Html;
use common\components\ActiveForm;
use yii\web\View;
use yii\helpers\ArrayHelper;
use yii\web\JsExpression;
use kartik\widgets\Select2;
use common\helpers\ProvinceHelper;
use common\helpers\Utility;
use kartik\widgets\FileInput;
use common\models\ar\Province;
use common\models\ar\Language;
use kartik\datecontrol\DateControl;

$this->registerCssFile('@web/css/select2.min.css');
$this->registerCssFile('@web/css/select2-bootstrap.min.css');
$this->registerJsFile('@web/js/select2/select2.min.js?'.uniqid(), ['depends' => [\yii\web\JqueryAsset::className()]]);
$this->registerJsFile('@web/js/select2/i18n/'.Yii::$app->language.'.js?'.uniqid(), ['depends' => [\yii\web\JqueryAsset::className()]]);
$this->registerJsFile('@web/js/province-by-zip-code.js?'.uniqid(), ['depends' => [\yii\web\JqueryAsset::className()]]);
$this->registerJsFile('@web/js/forms.js?'.uniqid(), ['depends' => [\yii\web\JqueryAsset::className()]]);

$subtitle = $model->isEditScenarioSet() ? Yii::t('web', 'Update employee') : Yii::t('web', 'Add employee');
$this->title = $subtitle.' - '.Yii::$app->name;

$this->params['breadcrumbs'][] = ['label' => Yii::t('web', 'Employees'), 'url' => Url::to(['employee/index'])];
$this->params['breadcrumbs'][] = ['label' => Yii::t('web', 'List of employees'), 'url' => Url::to(['employee/list'])];
$this->params['breadcrumbs'][] = $subtitle;
?>

<div class="employee-form">
	<h2 class="page-title"><?= $subtitle ?></h2>

	<?php $form = ActiveForm::begin([
			'id' => 'employee-form-ajax',
			'enableAjaxValidation' => true,
			'enableClientValidation' => false,
			'options' => [
					'enctype' => 'multipart/form-data',
					'class' => 'form',
			],
	]); ?>
	<div class="col-xs-12 title-with-icon">
		<div class="base-icon basic-data-icon main-content-icons"></div><h3><?= Yii::t('web', 'Basic data') ?></h3>
	</div>
	<?php if($model->isEditScenarioSet()) {
		$form->field($model, 'id')->hiddenInput()->label(false);
	} ?>

	<div class="col-sm-12">
		<div class="row">
			<div class="col-sm-6"><?= $form->field($model, 'firstName')->textInput(['maxlength' => true, 'placeholder' => Yii::t('web','Employee name')]) ?></div>
		</div>		
	</div>
	<div class="col-sm-12">
		<div class="row">
			<div class="col-sm-6"><?= $form->field($model, 'lastName')->textInput(['maxlength' => true, 'placeholder' => Yii::t('web','Employee surname')]) ?></div>
		</div>
	</div>
	<div class="col-sm-12">
		<div class="row">
			<div class="col-sm-6"><?= $form->field($model, 'pesel')->textInput(['maxlength' => true, 'placeholder' => $model->getAttributeLabel('pesel')]) ?></div>
		</div>
	</div>
	<div class="col-sm-12">
		<div class="row">
			<div class="col-sm-6"><?= $form->field($model, 'idNumber')->textInput(['maxlength' => true, 'placeholder' => $model->getAttributeLabel('idNumber')]) ?></div>
		</div>
	</div>
	<div class="col-sm-12">
		<div class="row">
			<div class="col-sm-6"><?= $form->field($model, 'position')->textInput(['maxlength' => true, 'placeholder' => $model->getAttributeLabel('position')]) ?></div>
		</div>
	</div>
	<div class="col-xs-12 title-with-icon">
		<div class="base-icon basic-data-icon main-content-icons"></div>
		<h3><?= Yii::t('web', 'Contact data') ?></h3>
	</div>
	<div class="col-sm-12">
		<div class="row">
			<div class="col-sm-6"><?= $form->field($model, 'email')->textInput(['maxlength' => true, 'placeholder' => Yii::t('web','E-mail')]) ?></div>
		</div>
	</div>
	<div class="col-sm-12">
		<div class="row">
			<div class="col-sm-6"><?= $form->field($model, 'phone1')->textInput(['maxLength' => true, 'placeholder' => $model->getAttributeLabel('phone1'), 'class' => 'form-control']) ?></div>
			<div class="col-sm-5 col-sm-offset-1"><?= $form->field($model, 'phone2')->textInput(['maxLength' => true, 'placeholder' => $model->getAttributeLabel('phone2'), 'class' => 'form-control']) ?></div>
		</div>
	</div>
	<div class="col-sm-12">
		<div class="row">
			<div class="col-sm-6"><?= $form->field($model, 'phone3')->textInput(['maxLength' => true, 'placeholder' => $model->getAttributeLabel('phone3'), 'class' => 'form-control']) ?></div>
		</div>
	</div>
	<div class="col-sm-12">
		<div class="row">
		 	<div class="col-sm-6"><?=$form->field($model, 'password')->passwordInput(['maxlength' => true, 'placeholder' => $model->getAttributeLabel('password')]) ?></div>
		 </div>
	</div>
	<div class="col-sm-12">
		<div class="row">
		 	<div class="col-sm-6"><?=$form->field($model, 'repeatPassword')->passwordInput(['maxlength' => true, 'placeholder' => $model->getAttributeLabel('repeatPassword')]) ?></div>
		</div>
	</div>
	
	<?php $provinces = ProvinceHelper::getListOfNonArtificialProvinces(); ?>
	
	<div class="col-sm-12">
		<div class="row">
			<h4><?= Yii::t('main', 'Address') ?></h4>
			<div class="col-md-4 col-sm-8"><?= $form->field($model, 'address')->textInput(['maxlength' => true, 'placeholder' => $model->getAttributeLabel('address')]) ?></div>
			<div class="col-md-2 col-sm-4"><?= $form->field($model, 'zipCode')->textInput(['maxlength' => true, 'placeholder' => $model->getAttributeLabel('zipCode')]) ?></div>
			<div class="col-md-3 col-sm-6"><?= $form->field($model, 'city')->textInput(['maxlength' => true, 'placeholder' => $model->getAttributeLabel('city')]) ?></div>
			<div class="col-md-3 col-sm-6"><?=
				$form->field($model, 'provinceId')->dropDownList($provinces, ['prompt' => Yii::t('main','Choose Voivodeship')]);
			?></div>
		</div>
	</div>
	<div class="col-xs-12 title-with-icon">
		<div class="base-icon basic-data-icon main-content-icons"></div>
		<h3><?= Yii::t('web', 'Additional informations') ?></h3>
	</div>
	<div class="col-sm-12">
		<div class="row">
			<h4> <?php echo Yii::t('web','Profile photo')?></h4>
			<div class="col-sm-12"> 
			<?php if($model->urlPhoto): ?>
				<div id="photo-preview-container" class="form-group">
				            <div class="file-preview">
				                <div id="photo-preview-container" class="form-group">
				                    <?= Html::img(\common\helpers\UploadHelper::getNormalPath($model->urlPhoto, 'user_photo'),
				                                  ['style' => 'max-width: 300px'],
				                                  ['class' => 'file-preview-frame', 'style' => 'display: block; height: auto; float: none;'])
				                    ?>
				                    <button type="button" id="delete-photo-button" class="btn btn-default"><?= Yii::t('main', 'Delete') ?></button>
				                </div>
				            </div><br>
				        </div>
				        <input id="delete-photo-field" type="hidden" name="delete-photo" value="0" />
				        <?php
				            $this->registerJs('
				                $("#delete-photo-button").on("click", function() {
				                    $("#photo-preview-container").remove();
				                    $("#delete-photo-field")[0].value = 1;
				                })
				            ', View::POS_END);
				        ?>
				    <?php endif; ?>
			</div>

		    <?php
	        echo $form->field($model, 'filePhoto')->widget(FileInput::classname(), [
	                        'options' => [
	                                'multiple' => false,
	                        ],
	                        'pluginOptions' => [
	                                'language' => Yii::$app->language,
	                                'showCaption' => false,
	                                'showUpload' => false,
	                                'uploadAsync' => false,
	                                'resizeImageQuality' => 1.00, //not required
	                                'allowedFileTypes' => ['image'],
	                                'allowedFileExtensions' => ['jpg', 'png', 'jpeg', 'gif'],
	                                'allowedPreviewFileTypes' => ['image'],
	                                'allowedPreviewMimeTypes' => ['image/jpeg', 'image/png'],
	                                'previewFileType' => 'image',
	                                'previewSettings' => ['image' => ['width' => '300px', 'height' => 'auto']],
	                        		'layoutTemplates' => ['main2' => '{preview}' . '<br>' . ' {remove} {browse}'],
	                        ],
	        ])->label(false);
	    	?>
	    </div>
	</div>
	<div class="col-sm-12">
		<div class="row">
			<div class="col-md-6">
				<?php if(Yii::$app->params['isPlaceholderInputLabel']) { ?>
					<label class="control-label"><?= $model->getAttributeLabel('dateEmployment')?></label>
				<?php }?>
	        		<?= $form->field($model, 'dateEmployment')->widget(DateControl::classname(), [
	                    'language' => \Yii::$app->language,
	                    'type' => DateControl::FORMAT_DATE,
	                    'ajaxConversion' => false,
	                    'displayFormat' => 'php:Y-m-d',
	        			'saveFormat' => 'php:Y-m-d',
	            		'options' => ['placeholder' => $model->getAttributeLabel('dateEmployment')],
	                    'options' => [
	                        'pluginOptions' => [
	                            'autoclose' => true
	                        ]
	                    ]
	                ]);
	            	?>
			</div>
		</div>
	</div>
	<div class="col-sm-12">
		<div class="row">
			<div class="col-sm-6">
				<?php echo $form->field($model, 'roles', ['enableAjaxValidation' => false])->
						widget(Select2::className(), [
								'language' => Yii::$app->language,
								'options' => ['placeholder' => Yii::t('web', 'Select a role...'), 'multiple' => true],
								'showToggleAll' => false,
								'data' => $rolesData,
								'pluginOptions' => [
										'allowClear' => false,
								],
								'pluginEvents' => [
								],
						]
				) ?>
	
	        </div>        
		</div>
	</div>
	<div class="col-sm-12">
		<div class="row">
			<div class="col-sm-6">
				<?php if(Yii::$app->user->can(Utility::ROLE_MAIN_SALESMAN)){
							echo $form->field($model, 'clientGroupIds', ['enableAjaxValidation' => false])->
								widget(Select2::className(), [
										'language' => Yii::$app->language,
										'name' => 'select2_client-groups',
										'options' => ['placeholder' => Yii::t('web', 'Select a group...'), 'multiple' => true],
										'showToggleAll' => false,
										'data' => $clientGroupsData,
										'pluginOptions' => [
												'allowClear' => false,
												'minimumInputLength' => 1,
												'tags' => false,
												'ajax' => [
														'url' => Url::to(['/company/find-company-groups']),
														'dataType' => 'json',
														'delay' => 250,
														'cache' => true,
														'data' => new JsExpression('function(params) { return {term:params.term, page:params.page, size: 20}; }'),
														'processResults' => new JsExpression('function (data, params) {
															params.page = params.page || 1;
															return {
																results: data.items,
																pagination: {
																	more: data.more
																}
															};
														}')
												],
												'templateResult' => new JsExpression('function(item) { if(item.loading) return item.text; return item.name; }'),
												'templateSelection' => new JsExpression('function (item) { return item.name || item.text; }'),
										],
										'pluginEvents' => [
										],
								]
	
						);
				}?>
			</div>
			<div class="col-sm-6">
				<?php if(Yii::$app->params['isMoreWarehousesAvailable']) { ?>
					<?php if(Yii::$app->user->can(Utility::ROLE_MAIN_STOREKEEPER)){
								echo $form->field($model, 'warehouseGroupIds', ['enableAjaxValidation' => false])->
									widget(Select2::className(), [
											'language' => Yii::$app->language,
											'name' => 'select2_warehouse-groups',
											'options' => ['placeholder' => Yii::t('web', 'Select a warehouse...'), 'multiple' => true],
											'showToggleAll' => false,
											'data' => $warehouseGroupsData,
											'pluginOptions' => [
													'allowClear' => false,
													'minimumInputLength' => 1,
													'tags' => false,
													'ajax' => [
															'url' => Url::to(['/warehouse/find-warehouse-groups']),
															'dataType' => 'json',
															'delay' => 250,
															'cache' => true,
															'data' => new JsExpression('function(params) { return {term:params.term, page:params.page, size: 20}; }'),
															'processResults' => new JsExpression('function (data, params) {
																params.page = params.page || 1;
																return {
																	results: data.items,
																	pagination: {
																		more: data.more
																	}
																};
															}')
													],
													'templateResult' => new JsExpression('function(item) { if(item.loading) return item.text; return item.name; }'),
													'templateSelection' => new JsExpression('function (item) { return item.name || item.text; }'),
											],
											'pluginEvents' => [
											],
									]
							);
					}
				} ?>
			</div>
		</div>
	</div>

	<div class="col-sm-12">
		<div class="row">
			<div class="col-md-6"><?= $form->field($model, 'note')->textArea(['maxlength' => true, 'placeholder' => $model->getAttributeLabel('note')]) ?></div>
		</div>
	</div>
	
	<?= 
        Yii::$app->controller->renderPartial('/inc/files', [
            	'form' => $form,
        		'readOnlyDocumentsForms' => $model->getReadonlyDocumentsForms(),
        		'documentsForms' => $model->getDocumentsForms(),
        		'arePhotosAvailable' => false,
        		'areAttachmentsAvailable' => true,
        ]);
    ?>

	<div class="col-sm-12">
		<div class="row">
			<div class="pull-left">
			    <?= Html::a(Yii::t('web', 'Cancel'),
	        		!empty(Yii::$app->request->getReferrer()) ? Yii::$app->request->getReferrer() : ['employee/list'],
	        		['class' => 'btn btn-default']) ?>
			</div>
			<div class="pull-right"><?= Html::submitButton(($model->isEditScenarioSet() ? Yii::t('main', 'Save') : Yii::t('web', 'Add')), ['class' => 'btn btn-primary']); ?></div>
		</div>
	</div>

	<?php ActiveForm::end(); ?>
	<?php $this->registerJs('fillProvince("employeeform-zipcode", "employeeform-provinceid", "' . Url::to(['employee/get-province-by-zip-code']) . '");', View::POS_END); ?>
</div>

<?php $this->registerJs('
    (function() {
        var dynamicForm = new DynamicForm("employee-form-ajax");
		dynamicForm.activateDocumentUpload();
    })();
    ', View::POS_END); 
?>

<?php $this->registerJs("
	window['removedFileSuccessMsg'] = '" . Yii::t('web', 'The attachment has been deleted.') . "';
	window['removedFileFailMsg'] = '" . Yii::t('web', 'The attachment cannot be deleted. Please contact the administrator.') . "';",
	View::POS_END);
?>
