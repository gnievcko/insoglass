<?php
use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\View;
use dosamigos\multiselect\MultiSelect;

$this->title = Yii::t('web', 'List of employees') . ' - ' . Yii::$app->name;

$this->params['breadcrumbs'][] = ['label' => Yii::t('web', 'Employees'), 'url' => Url::to(['employee/index'])];
$this->params['breadcrumbs'][] = Yii::t('web', 'List of employees');

$this->registerJsFile('@web/js/dynamic-table.js?'.uniqid(), []);
$this->registerJsFile('@web/js/photo-tooltip.js?'.uniqid(), ['depends' => [\yii\web\JqueryAsset::className()]]);
?>

<div id="employee-list-wrapper">    
    <div class="row">
		<div class="col-sm-6"><h2 class="page-title"><?= Yii::t('web', 'List of employees') ?></h2></div>
		<div class="col-sm-6"><?= Html::a(Yii::t('web', 'Add employee'), ['employee/create'], ['class' => 'btn btn-primary pull-right']) ?>
		</div>
	</div>

    <div id="employee-list-table" class="dynamic-table">
    	<h5><?php echo Yii::t('main','Filters')?></h5>
        <div class="row dynamic-table-filters">
        	<input value="<?= $listForm->allRolesCount ?>" class="form-control dynamic-table-filter" type="hidden" name="<?= Html::getInputName($listForm, 'allRolesCount') ?>"/>
            <div class="col-md-3 col-sm-4">
                <label><?php echo Yii::t('web','Search by name')?></label>
                    <input 
                        value="<?= $listForm->filterString ?>" 
                        class="form-control dynamic-table-filter" 
                        type="text"
                        placeHolder="<?= Yii::t('web', 'Name') ?>"
                        name="<?= Html::getInputName($listForm, 'filterString') ?>">
                
            </div> 
            <div class="col-md-3 col-sm-4 cat-select">
                <label><?php echo Yii::t('web','Filter by role')?></label>
                    <?php              
                        $data = array_reduce($roleTranslations, function($options, $t) {
                            $options[$t['id']] = Html::encode($t['name']);
                            return $options;
                        }, []);
                        
                        if (!empty($data)) {
                        	//echo $listForm->getAttributeLabel('Role').'<br>';
                        	echo MultiSelect::widget([
                        			'id' => 'roles',
                        			"options" => ['multiple' => 'multiple', 'class' => 'dynamic-table-filter'],
                        			'data' => $data,
                        			'name' => Html::getInputName($listForm, 'roleIds'),
                        			'value' => empty($listForm->roleIds) ? array_keys($data) : $listForm->roleIds,
                        			"clientOptions" => [
                        					'nonSelectedText' => '',
                        					'nSelectedText' => mb_strtolower(Yii::t('main', 'Selected'), 'UTF-8'),
                        					'numberDisplayed' => 2,
                        					'buttonWidth' => '100%',
                        			],
                        	]);
                        }
                    ?>
                
            </div>  
            <?= Yii::$app->controller->renderPartial('/inc/listFiltersButtons', ['isMoreFilters' => false, 'isClearFilters' => true]) ?>
        </div>
        <div class="dynamic-table-data">
            <?= Yii::$app->controller->renderPartial('_users', [
	                'items' => $items,
	                'pages' => $pages,
	                'listForm' => $listForm,
            ]) ?>
        </div>
    </div>
    <?php 
    	$this->registerJs('dynamicTable("employee-list-table", "'.Url::to(['employee/list']).'");', View::POS_END);
    	$this->registerJs('initPhotoTooltip("'.Url::to(['employee/get-main-photo']).'");', View::POS_END);
    ?>
</div>
