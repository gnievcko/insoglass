<?php 
use yii\helpers\Url;
use yii\helpers\Html;
use frontend\models\EmployeeListForm;
use yii\widgets\LinkPager;
use common\bundles\EnhancedDialogAsset;
use common\widgets\TableHeaderWidget;
?>
<?php 
if (empty($items)) { ?>
	<label><?= Yii::t('web', 'No search results');?></label>
	<br><br>				
<?php }
else { ?>
	<div class=" table-responsive">
		<table class="<?= TableHeaderWidget::TABLECLASS ?>">
	    	<?php echo TableHeaderWidget::widget(['fields' => EmployeeListForm::getSortFields() , 'listForm' => $listForm]);?>
	        <tbody>
	            <?php foreach($items as $item): ?>
	                <tr>
	                	<td><span class="photo-container">
							<?= '<div class="photo-medium" style="background: url('.(!empty($item['url_photo']) ? \common\helpers\UploadHelper::getNormalPath($item['url_photo'], 'user_photo') : Url::to('@web/images/placeholders/person.png')).')"></div>'; ?>
							<?php if(!empty($item['url_photo'])) { ?>
								<div class="photo-tooltip" data-id="<?= $item['id'] ?>"></div>
							<?php } ?>
						</span></td>
	                    <td><?= Html::encode($item['first_name']) ?></td>   
	                    <td><?= Html::encode($item['last_name']) ?></td>
	                    <td><?= Html::encode($item['role']) ?></td>
	                    <td><?= Html::encode($item['email']) ?></td>
	                    <td><?= Html::encode($item['phone1']) ?></td>
	                    
	                    <td><?php
	                    	echo Html::a('', Url::to(['employee/details', 'id' => $item['id']]), ['class' => 'base-icon details-icon action-icon', 'title' => Yii::t('web', 'Details')]);
	                    	echo '&nbsp;'.Html::a('', Url::to(['employee/edit', 'id' => $item['id']]), ['class' => 'base-icon editing-icon action-icon', 'title' => Yii::t('web', 'Edit')]);
	                    	echo '&nbsp;'.Html::a('', Url::to(['employee/delete']), [
									'class' => 'base-icon delete-icon action-icon',
                                    'title' => Yii::t('main', 'Delete'),
                                    'data' => [
							                'confirm' => Yii::t('main', 'Are you sure you want to delete this item?'),
							                'method' => 'post',
											'params' => [
													'id' => $item['id'],
											],
						            ],
							]);
	                    ?>
	                    </td>
	                </tr>
	            <?php endforeach ?>
	        </tbody>
	    </table>
	</div>
	<div class="col-sm-12">
		<div class="col-sm-4 text-left dynamic-table-counter">
			<?= Yii::t('web', 'Number of found items: {n}', ['n' => $pages->totalCount]) ?>
		</div>
		<div class="col-sm-8 text-right dynamic-table-pagination">
		    <?= LinkPager::widget(['pagination' => $pages ]) ?>
		</div>
	</div>
<?php }?>

<input class="dynamic-table-sort-dir" type="hidden" name="<?= Html::getInputName($listForm, 'sortDir') ?>" value="<?= $listForm->sortDir ?>" />
<input class="dynamic-table-sort-field" type="hidden" name="<?= Html::getInputName($listForm, 'sortField') ?>" value="<?= $listForm->sortField ?>" />
<input class="dynamic-table-page" type="hidden" name="<?= Html::getInputName($listForm, 'page') ?>" value="<?= $listForm->page ?>" />

<?php 
	EnhancedDialogAsset::register($this);
?>
