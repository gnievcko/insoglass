<?php
use common\bundles\EnhancedDialogAsset;
use yii\helpers\Html;
use yii\helpers\Url;
use frontend\helpers\UserHelper;
use dosamigos\gallery\Gallery;
use common\helpers\StringHelper;
use frontend\helpers\AddressHelper;

$this->title = Yii::t('web', 'Employee details') . ' - ' . Yii::$app->name;

$this->params['breadcrumbs'][] = ['label' => Yii::t('web', 'Employees'), 'url' => Url::to(['employee/index'])];
$this->params['breadcrumbs'][] = ['label' => Yii::t('web', 'List of employees'), 'url' => Url::to(['employee/list'])];
$this->params['breadcrumbs'][] = Yii::t('web', 'Employee details');
?>

<div class="employee-details">
	<div class="row">
		<div class="col-sm-6"><h2 class="page-title"><?= Yii::t('web', 'Employee details') ?></h2></div>
		<div class="col-sm-6">
			<div class="pull-right"><?php
				$buttons = null;
				$referrer = Yii::$app->request->getReferrer();
				if (substr($referrer, strripos($referrer, '/') + 1) == 'create') {
					$buttons .= Html::a(Yii::t('web', 'Add another employee'),
										['employee/create'], ['class' => 'btn btn-primary']);
				}
				$buttons .= Html::a(Yii::t('main', 'Update'),
						['employee/edit', 'id' => $data['id']], ['class' => 'btn btn-default'])
				.Html::a(Yii::t('main', 'Delete'),
						['employee/delete'],
						[
								'class' => 'btn btn-default',
								'data' => [
						                'confirm' => Yii::t('main', 'Are you sure you want to delete this item?'),
						                'method' => 'post',
										'params' => [
												'id' => $data['id'],
										],
					            ],
						]);
				echo $buttons;
			?></div>
		</div>
	</div>

	<div class="row">
		<div class="col-sm-4 text-right">
			<div class="row"><h1> <?= Html::encode($data['first_name'] . ' ' . $data['last_name']) ?></h1></div>
			<div class="row"><h3> <?= Html::encode($roles) ?></h3></div>
		</div>
		<div class="col-sm-4 text-center">
			<span class="photo-container center-block">
				<?php
				$src = null;
					if (empty($data['url_photo'])) {
						$src = Url::to('@web/images/placeholders/person.png');	
					}
					else {
						$src = \common\helpers\UploadHelper::getNormalPath($data['url_photo'], 'user_photo');
					}
				?>
					<?= '<img src=' . $src .' alt="..." class="img-circle" align="middle" width="200px" height="auto">';?>
			</span>
		</div>
		<div class="col-sm-4 text-left">
			<div class="row"><h4> <?= Html::encode($data['email']) ?></h4></div>
			<div class="row"><h4> <?= Html::encode($data['phone1']) ?></h4></div>
			<div class="row"><h4> <?= Html::encode($data['phone2']) ?></h4></div>
			<div class="row"><h4> <?= Html::encode($data['phone3']) ?></h4></div>
		</div>
	</div>
	<div class="row">
		<div class="col-sm-6 text-left">
			<div class="row">
				<?php ?>
				<?php if(!empty($companyGroups)) { ?>
					<h5><?= Yii::t('web', 'Belongs to the following client groups:')?></h5>
					<?= $this->render('/inc/groupsRow', ['groups' => $companyGroups])?>
				<?php }?>
			</div>
			<div class="row">
				<?php ?>
				<?php if(!empty($warehouses)) { ?>
					<h5><?= Yii::t('web', 'Belongs to the following warehouses:')?></h5>
					<?= $this->render('/inc/warehousesRow', ['warehouses' => $warehouses])?>
				<?php } ?>
			</div>
			<br>
			<div class="row">
				<?= Yii::t('main', 'Last login') . ':'?>
				<div class="pull-right"><?= Html::encode(StringHelper::getFormattedDateFromDate($data['last_login'], true)) ?></div>
			</div>
			<div class="row">
				<?= Yii::t('main', 'Creation date') . ':'?>
				<div class="pull-right"><?= Html::encode(StringHelper::getFormattedDateFromDate($data['date_creation'], true)) ?></div>
			</div>
			<div class="row">
				<?= Yii::t('web', 'Employment date') . ':'?>
				<div class="pull-right"><?= Html::encode(StringHelper::getFormattedDateFromDate($data['date_employment'])) ?></div>
			</div>
			<div class="row">
				<?= Yii::t('web', 'PESEL') . ':'?>
				<div class="pull-right"><?= Html::encode($data['pesel']) ?></div>
			</div>
			<div class="row">
				<?= Yii::t('web', 'ID number') . ':'?>
				<div class="pull-right"><?= Html::encode($data['id_number']) ?></div>
			</div>
			<div class="row">
				<?= Yii::t('web', 'Position').':' ?>
				<div class="pull-right"><?= Html::encode($data['position']) ?></div>
			</div>
			<div class="row">
				<?= Yii::t('main', 'Address') . ':'?>
				<div class="pull-right">
					<?= AddressHelper::getFullAddress([
		                    		'addressMain' => $data['addressMain'],
		                    		'addressComplement' => $data['addressComplement'],
		                    		'cityName' => $data['cityName'],
		                    		'cityZipCode' => $data['cityZipCode'],
		                    		'provinceName' => $data['provinceName'],
		                    		'countryName' => $data['countryName']
		                    ]) ?>
		    	</div>
            </div>
			<br>
			<?php if(!empty($data['note'])) { ?>
				<h5><div class="row"><?= Yii::t('main', 'Note') ?></div></h5>
				<div class="row"><?= nl2br(Html::encode($data['note']))?></div>
			<?php } 
			if(!empty($attachments)) { ?>
				<h5><div class="row"><?= Yii::t('web', 'Attachments') ?></div></h5>
				<div class="row"><?= $this->render('/inc/attachmentList', ['attachments' => $attachments]) ?></div>
			<?php } ?>
		</div>
	</div>

</div>
<?php
	EnhancedDialogAsset::register($this);
?>
