<?php
use yii\helpers\Url;
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use common\helpers\StringHelper;

$this->registerCssFile('@web/css/select2.min.css');
$this->registerCssFile('@web/css/select2-bootstrap.min.css');
$this->registerJsFile('@web/js/select2/select2.min.js?'.uniqid(), ['depends' => [\yii\web\JqueryAsset::className()]]);
$this->registerJsFile('@web/js/select2/i18n/'.Yii::$app->language.'.js?'.uniqid(), ['depends' => [\yii\web\JqueryAsset::className()]]);

$subtitle = $model->isEditScenarioSet() ? StringHelper::translateOrderToOffer('web', 'Update order type') : StringHelper::translateOrderToOffer('web', 'Add order type');
$this->title = $subtitle.' - '.Yii::$app->name;

$this->params['breadcrumbs'][] = ['label' => StringHelper::translateOrderToOffer('main', 'Orders'), 'url' => Url::to(['order/index'])];
$this->params['breadcrumbs'][] = ['label' => StringHelper::translateOrderToOffer('web', 'List of order types'), 'url' => Url::to(['order-type/list'])];
$this->params['breadcrumbs'][] = $subtitle;

?>

<div class="order-type-form">
	<h2 class="page-title"><?= $subtitle ?></h2>
	
	<?php $form = ActiveForm::begin([
			'id' => 'order-type-form-ajax',
			'enableAjaxValidation' => true,
	]); ?>
	
	<?php if($model->isEditScenarioSet()) {
		$form->field($model, 'id')->hiddenInput()->label(false);
	} ?>
				
	<div class="col-xs-12">
		<div class="row">
			<div class="col-md-6 col-xs-12">
				<?= $form->field($model, 'name')->textarea(['class' => 'form-control smallTextarea', 'maxlength' => true, 'placeHolder' => $model->getAttributeLabel('name')]) ?>
			</div>
		</div>
	</div>
	
	<div class="col-xs-12">
		<div class="row">
			<div class="pull-left">
				<?= Html::a(Yii::t('web', 'Cancel'), 
	        		!empty(Yii::$app->request->getReferrer()) ? Yii::$app->request->getReferrer() : Url::to(['order-type/list']), 
	        		['class' => 'btn btn-default']) ?>
	        </div>
			<div class="pull-right"><?= Html::submitButton(($model->isEditScenarioSet() ? Yii::t('main', 'Save') : Yii::t('web', 'Add')), ['class' => 'btn btn-primary']); ?></div>
		</div>
	</div>
	
	<?php ActiveForm::end(); ?>	
</div>
