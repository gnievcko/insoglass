<?php
use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\View;
use common\helpers\StringHelper;

$this->title = StringHelper::translateOrderToOffer('web', 'List of order types') . ' - ' . Yii::$app->name;

$this->params['breadcrumbs'][] = ['label' => StringHelper::translateOrderToOffer('main', 'Orders'), 'url' => Url::to(['order/index'])];
$this->params['breadcrumbs'][] = StringHelper::translateOrderToOffer('web', 'List of order types');

$this->registerJsFile('@web/js/dynamic-table.js?'.uniqid(), []);
?>

<div id="order-type-list-wrapper">
    <div class="row">
		<div class="col-sm-6 col-xs-12"><h2 class="page-title"><?= StringHelper::translateOrderToOffer('web', 'List of order types') ?></h2></div>
		<div class="col-sm-6 col-xs-12 text-right"><?= Html::a(StringHelper::translateOrderToOffer('web', 'Add order type'), ['order-type/create'], ['class' => 'btn btn-primary']) ?></div>
	</div>

    <div id="order-type-list-table" class="dynamic-table">
    	<h5><?php echo Yii::t('main','Filters')?></h5>
        <div class="row dynamic-table-filters">
            <div class="col-md-3 col-sm-4">
            	<label><?php echo Yii::t('web','Search by name')?></label>
                    <input 
                        value="<?= $listForm->name ?>" 
                        class="form-control dynamic-table-filter" 
                        type="text"
                        placeHolder="<?= $listForm->getAttributeLabel('Name') ?>"
                        name="<?= Html::getInputName($listForm, 'name') ?>">
            </div> 
            <?= Yii::$app->controller->renderPartial('/inc/listFiltersButtons', ['isMoreFilters' => false, 'isClearFilters' => true]) ?>
        </div>
        <div class="dynamic-table-data">
            <?= Yii::$app->controller->renderPartial('_types', [
	                'data' => $data,
	                'pages' => $pages,
	                'listForm' => $listForm,
            ]) ?>
        </div>
    </div>
    <?php $this->registerJs('dynamicTable("order-type-list-table", "'.Url::to(['order-type/list']).'");', View::POS_END); ?>
</div>
