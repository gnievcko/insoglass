<?php
use yii\helpers\Html;
use yii\helpers\Url;
use frontend\helpers\UserHelper;
use dosamigos\gallery\Gallery;
use common\bundles\EnhancedDialogAsset;
use common\helpers\StringHelper;

$this->title = StringHelper::translateOrderToOffer('web', 'Order type details') . ' - ' . Yii::$app->name;

$this->params['breadcrumbs'][] = ['label' => StringHelper::translateOrderToOffer('main', 'Orders'), 'url' => Url::to(['order/index'])];
$this->params['breadcrumbs'][] = ['label' => StringHelper::translateOrderToOffer('web', 'List of order types'), 'url' => Url::to(['order-type/list'])];
$this->params['breadcrumbs'][] = StringHelper::translateOrderToOffer('web', 'Order type details');

?>

<div class="order-type-details">
	<div class="row">
		<div class="col-md-4 col-sm-6">
			<h2 class="page-title"><?= StringHelper::translateOrderToOffer('web', 'Order type details') ?></h2>
		</div>
		<div class="col-md-8 col-sm-6 text-right">
			<?php
				$buttons = '';
				$referrer = Yii::$app->request->getReferrer();
				if (substr($referrer, strripos($referrer, '/') + 1) == 'create') {
					$buttons .= Html::a('<span class="glyphicon glyphicon-plus"></span>&nbsp;'.Yii::t('web', 'Add another type'),
										['order-type/create'], ['class' => 'btn btn-primary']);
				}
				$buttons .= Html::a('<span class="base-icon action-icon editing-icon"></span>&nbsp;'.Yii::t('main', 'Update'), 
						['order-type/edit', 'id' => $data['id']], ['class' => 'btn btn-default']);
				$buttons .= Html::a('<span class="base-icon action-icon delete-icon"></span>&nbsp;'.Yii::t('main', 'Delete'), 
						['order-type/delete'], 
						[
								'class' => 'btn btn-default',
								'data' => [
						                'confirm' => Yii::t('main', 'Are you sure you want to delete this item?'),
						                'method' => 'post',
										'params' => [
												'id' => $data['id'],
										],
					            ],		
						]);
				echo $buttons; 
			?>
		</div>
	</div>
	<div class="col-xs-12 title-with-icon">
		<div class="base-icon basic-data-icon main-content-icons"></div>
		<h3 class="page-subtitle"><?= Yii::t('web', 'General information') ?></h3>
	</div>
	<div class="col-xs-12">
		<div class="table-responsive">
			<table class="table table-bordered detail-table">
		    	<tbody>
		    		<tr>
		    			<th><?= Yii::t('web', 'Name') ?></th>
		    			<td><?= Html::encode($data['name']) ?></td>
		    		</tr>
		    	</tbody>
			</table>
		</div>	
	</div>
	<?php 
		EnhancedDialogAsset::register($this);
	?>
</div>