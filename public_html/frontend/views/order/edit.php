<?php

use yii\helpers\Url;
use yii\helpers\Html;
use common\components\ActiveForm;
use common\helpers\StringHelper;

$this->registerJsFile('@web/js/forms.js?' . uniqid(), ['depends' => [\yii\web\JqueryAsset::className()]]);
$this->registerJsFile('@web/js/order-form.js?' . uniqid(), ['depends' => [\yii\web\JqueryAsset::className()]]);


$this->title = StringHelper::translateOrderToOffer('web', 'Edit order') . ' - ' . Yii::$app->name;

$this->params['breadcrumbs'][] = ['label' => StringHelper::translateOrderToOffer('main', 'Orders'), 'url' => Url::to(['order/index'])];
$this->params['breadcrumbs'][] = ['label' => StringHelper::translateOrderToOffer('main', 'List of orders'), 'url' => Url::to(['order/list'])];
$this->params['breadcrumbs'][] = ['label' => StringHelper::translateOrderToOffer('web', 'Order details'), 'url' => Url::to(['order/details', 'id' => $id])];
$this->params['breadcrumbs'][] = StringHelper::translateOrderToOffer('web', 'Edit order');
?>

<div class="order-form">
    <h2 class="page-title"><?= StringHelper::translateOrderToOffer('web', 'Edit order') ?></h2>

    <?php
    $form = ActiveForm::begin([
                'id' => 'order-edit-form',
                'enableAjaxValidation' => true,
                'enableClientValidation' => false,
                'options' => [
                    'class' => 'form',
                ]
    ]);

    echo $form->errorSummary($invalidForms);
    echo $form->field($orderCreateForm, 'orderId')->hiddenInput()->label(false);
    ?>

    <?=
    Yii::$app->controller->renderPartial('inc/_basic_data', [
        'entities' => $entities,
        'orderCreateForm' => $orderCreateForm,
        'orderProductsForms' => $orderProductsForms,
        'orderCostsForms' => $orderCostsForms,
        'currencies' => $currencies,
        'orderTypes' => $orderTypes,
        'contractTypes' => $contractTypes,
        'form' => $form,
        'noProducts' => true,
        'scenario' => $scenario,
    ])
    ?>


    <div>
        <?=
        Html::a(Yii::t('web', 'Cancel'), !empty(Yii::$app->request->getReferrer()) ? Yii::$app->request->getReferrer() : Url::to(['order/list']), ['class' => 'btn btn-default'])
        ?>
        <button class="btn btn-primary pull-right"><?= Yii::t('main', 'Save') ?></button>
    </div>

    <?php ActiveForm::end() ?>
</div>
