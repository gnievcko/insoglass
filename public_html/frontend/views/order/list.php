<?php
use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\View;
use dosamigos\multiselect\MultiSelect;
use kartik\select2\Select2;
use yii\web\JsExpression;
use common\models\ar\Company;
use common\helpers\StringHelper;
use common\helpers\Utility;
use yii\web\JqueryAsset;
use kartik\datecontrol\DateControl;
use frontend\models\OrdersListForm;

$this->title = StringHelper::translateOrderToOffer('main', 'List of orders') . ' - ' . Yii::$app->name;

$this->params['breadcrumbs'][] = ['label' => StringHelper::translateOrderToOffer('main', 'Orders'), 'url' => Url::to(['order/index'])];
$this->params['breadcrumbs'][] = StringHelper::translateOrderToOffer('main', 'List of orders');

$this->registerJsFile('@web/js/dynamic-table.js?'.uniqid(), []);
$this->registerJsFile('@web/js/layout/moreFilters.js?'.uniqid(), ['depends' => [JqueryAsset::className()]]);
?>

<div id="orders-list-wrapper">
    <div class="row">
        <div class="col-sm-6"><h2 class="page-title"><?= StringHelper::translateOrderToOffer('main', 'List of orders') ?></h2></div>
        <div class="col-sm-6">
        	<div class="pull-right">
        		<?= Html::a(StringHelper::translateOrderToOffer('web', 'Add order'), ['order/create'], ['class' => 'btn btn-primary']) ?>
        		<?php if(Yii::$app->params['isOrderTypeVisible']) { ?>
        			<?= Html::a(StringHelper::translateOrderToOffer('web', 'Order types'), ['order-type/list'], ['class' => 'btn btn-default', 'target' => '_blank']); ?>
        		<?php } ?>
        	</div>
        </div>
	</div>

    <div id="orders-list-table" class="dynamic-table">
    	<h5><?php echo Yii::t('main','Filters')?></h5>
        <div class="row dynamic-table-filters">
        	<div class="col-md-3 col-sm-6">
                <label><?= $ordersListForm->getAttributeLabel('orderNumber') ?></label>
                    <input 
                        value="<?= $ordersListForm->orderNumber ?>" 
                        class="form-control dynamic-table-filter" 
                        type="text"
                        placeholder="<?php echo Yii::t('web','Search by number')?>"
                        name="<?= Html::getInputName($ordersListForm, 'orderNumber') ?>">
            </div>
            <div class="col-md-3 col-sm-6">
                <label><?= $ordersListForm->getAttributeLabel('orderClientName') ?></label>
                    <input 
                        value="<?= $ordersListForm->orderClientName ?>" 
                        class="form-control dynamic-table-filter" 
                        type="text"
                        placeholder="<?php echo Yii::t('web','Search by name')?>"
                        name="<?= Html::getInputName($ordersListForm, 'orderClientName') ?>">
            </div>
            <div class="col-md-3 col-sm-6">
            	<label><?php echo Yii::t('main', 'City')?></label>
                    <input
                        value="<?= $ordersListForm->cityName ?>"
                        class="form-control dynamic-table-filter"
                        type="text"
						placeholder="<?= $ordersListForm->getAttributeLabel('cityName') ?>"
                        name="<?= Html::getInputName($ordersListForm, 'cityName') ?>">
            </div>
            <div class="col-md-3 col-sm-6">
                <label><?= $ordersListForm->getAttributeLabel('orderName') ?></label>
                    <input 
                        value="<?= $ordersListForm->orderName ?>" 
                        class="form-control dynamic-table-filter" 
                        type="text"
                        placeholder="<?php echo Yii::t('web','Search by name')?>"
                        name="<?= Html::getInputName($ordersListForm, 'orderName') ?>">
                
            </div>  
            <div class="more-filters">
	            <div class="col-md-3 col-sm-6">
	                <label><?= $ordersListForm->getAttributeLabel('orderClientId') ?></label>	                    
	                    <?php
	                        $company = empty($ordersListForm->orderClientId) ? null : Company::findOne($ordersListForm->orderClientId);
	                        echo Select2::widget([
	                            'language' => Yii::$app->language,
	                            'model' => $ordersListForm,
	                            'attribute' => 'orderClientId',
	                            'options' => ['placeholder' => Yii::t('web','Filter by client'), 'id' => 'order-client', 'class' => 'dynamic-table-filter f-select2'],
	                            'data' => empty($company) ? [] : [$company->id => $company->name],
	                            'pluginOptions' => [
	                                    'allowClear' => true,
	                                    'minimumInputLength' => 1,
	                                    'ajax' => [
	                                            'url' => Url::to(['site/find-companies']),
	                                            'dataType' => 'json',
	                                            'delay' => 250,
	                                            'data' => new JsExpression('function(params) {
	                                                return {term:params.term, page:params.page, size: 20};
	                                            }'),
	                                            'processResults' => new JsExpression('function (data, params) { params.page = params.page || 1; var companies = $(data.items).map(function(index, company) {
	                                                    return {id: company.id, name: company.name};
	                                                });
	
	                                                return {
	                                                    results: companies,
	                                                    pagination: {
	                                                        more: data.more
	                                                    }
	                                                };
	                                            }'),
	                                            'cache' => true
	                                    ],
	                                    'templateResult' => new JsExpression('function(item) { if(item.loading) return item.text; return item.name; }'),
	                                    'templateSelection' => new JsExpression('function (item) { return item.name || item.text; }'),
	                            ]
	                        ]);
	                    ?>
	            </div>
	            <div class="col-md-3 col-sm-6">
	                <label><?= $ordersListForm->getAttributeLabel('orderParentClientName') ?></label>
	                    <input 
	                        value="<?= $ordersListForm->orderParentClientName ?>" 
	                        class="form-control dynamic-table-filter" 
	                        type="text"
	                        placeholder="<?php echo Yii::t('web','Search by name')?>"
	                        name="<?= Html::getInputName($ordersListForm, 'orderParentClientName') ?>">
	            </div>
	            <div class="col-md-3 col-sm-6">
	            	<label><?= $ordersListForm->getAttributeLabel('dateFrom') ?></label>
	            	<?= DateControl::widget([
	            			'name' => (new OrdersListForm())->formName().'[dateFrom]',
	            			'value' => $ordersListForm->dateFrom, 
		                    'language' => \Yii::$app->language,
		                    'type' => DateControl::FORMAT_DATE,
		                    'ajaxConversion' => false,
		                    'displayFormat' => 'php:Y-m-d',
		                    'options' => [
		                        'pluginOptions' => [
		                            'autoclose' => true
	                            ],
                    		    'options' => ['class'=>'form-control dynamic-table-filter', 'placeholder' => $ordersListForm->getAttributeLabel('dateFrom')],
		                    ]
		                ]);
		            ?>
	            </div>
	            <div class="col-md-3 col-sm-6">
	            	<label><?= $ordersListForm->getAttributeLabel('dateTo') ?></label>
	            	<?= DateControl::widget([
	            			'name' => (new OrdersListForm())->formName().'[dateTo]',
	            			'value' => $ordersListForm->dateTo, 
		                    'language' => \Yii::$app->language,
		                    'type' => DateControl::FORMAT_DATE,
		                    'ajaxConversion' => false,
		                    'displayFormat' => 'php:Y-m-d',
		                    'options' => [
		                        'pluginOptions' => [
	                        	    'autoclose' => true
		                        ],
	                    		'options' => ['class'=>'form-control dynamic-table-filter', 'placeholder' => $ordersListForm->getAttributeLabel('dateTo')],
		                    ]
		                ]);
		            ?>
	            </div>
	            <div class="col-md-3 col-sm-6 cat-select">
	                <label><?php echo $ordersListForm->getAttributeLabel('orderStatusesIds');?></label>
	                    <?php 
	                        $orderStatusesData = array_reduce($ordersStatusesTranslations, function($options, $statusTranslation) {
	                            $options[$statusTranslation->order_status_id] = Html::encode($statusTranslation->name);
	                            return $options;
	                        }, []);
	
	                        if (!empty($orderStatusesData)) {
	                        	
	                        	echo MultiSelect::widget([
	                        			'id' => 'order-statuses',
	                        			"options" => ['multiple' => 'multiple', 'class' => 'dynamic-table-filter f-multiselect'],
	                        			'data' => $orderStatusesData,
	                        			'name' => Html::getInputName($ordersListForm, 'orderStatusesIds'),
	                        			'value' => empty($ordersListForm->orderStatusesIds) ? array_keys($orderStatusesData) : $ordersListForm->orderStatusesIds,
	                        			"clientOptions" => [
	                        					'nonSelectedText' => '',
	                        					'nSelectedText' => mb_strtolower(Yii::t('main', 'Selected'), 'UTF-8'),
	                        					'numberDisplayed' => 2,
	                        					'buttonWidth' => '100%',
	                        					'includeSelectAllOption' => 'true',
	                        					'selectAllText' => Yii::t('web', 'Select all'),
	                        			],
	                        	]);
	                        }
	                    ?>               
	            </div>
	            <div class="col-md-3 col-sm-6">
	            	<label><?= $ordersListForm->getAttributeLabel('orderType') ?></label>
	            		<?php
	            		$values = [
	            				Utility::ORDER_TYPE_ALL => Yii::t('web', 'All'),
	            				Utility::ORDER_TYPE_OPEN => Yii::t('web', 'Open'),
	            				Utility::ORDER_TYPE_ARCHIVAL => Yii::t('web', 'Archival'),
	            		];
	            		if(Yii::$app->params['isOrderTemplateVisible']) {
	            			$values[Utility::ORDER_TYPE_TEMPLATE] = Yii::t('web', 'Templates');
	            		}
	
	            		echo Html::activeDropDownList($ordersListForm, 'orderType', $values, ['class' => 'form-control dynamic-table-filter no-margin-bottom']);
	            		?>
	            </div>
            </div>
            <?= Yii::$app->controller->renderPartial('/inc/listFiltersButtons', ['isMoreFilters' => true, 'isClearFilters' => true]) ?>
        </div>
        <div class="dynamic-table-data">
            <?= Yii::$app->controller->renderPartial('inc/_orders', [
                'orders' => $orders,
                'pages' => $pages,
                'ordersListForm' => $ordersListForm,
            ]) ?>
        </div>
    </div>
    <?php 
    	$this->registerJs('dynamicTable("orders-list-table", "'.Url::to(['order/list']).'");', View::POS_END);
    	$this->registerJs('moreFilters('.Yii::$app->params['isListFiltersVisibleByDefault'].');', View::POS_END);
    ?>
</div>
