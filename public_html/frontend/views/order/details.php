<?php

use yii\helpers\Html;
use yii\helpers\Url;
use frontend\helpers\UserHelper;
use dosamigos\gallery\Gallery;
use common\helpers\StringHelper;
use common\models\ar\User;
use yii\bootstrap\Modal;
use common\helpers\Utility;
use frontend\models\AlertForm;
use common\bundles\EnhancedDialogAsset;
use yii\helpers\Json;
use yii\web\JqueryAsset;
use common\documents\DocumentType;
use yii\web\View;

$this->title = StringHelper::translateOrderToOffer('web', 'Order details') . ' - ' . Yii::$app->name;

$this->params['breadcrumbs'][] = ['label' => StringHelper::translateOrderToOffer('main', 'Orders'), 'url' => Url::to(['order/index'])];
$this->params['breadcrumbs'][] = ['label' => StringHelper::translateOrderToOffer('main', 'List of orders'), 'url' => Url::to(['order/list'])];
$this->params['breadcrumbs'][] = StringHelper::translateOrderToOffer('web', 'Order details');
$this->registerJsFile('@web/js/dynamic-table.js', []);
$this->registerJsFile('@web/js/pages/toggle-attributes.js?' . uniqid(), ['depends' => [JqueryAsset::className()]]);

$this->registerJs('$(document).ready(function() {
    $("body").tooltip({ selector: "[data-toggle=tooltip]" });
});');
?>
<div class="order-details">
    <div class="row">
        <div class="col-md-4 col-sm-6"><h2 class="page-title"><?= StringHelper::translateOrderToOffer('web', 'Order details') ?></h2></div>
        <div class="col-md-8 col-sm-6 order-buttons">
            <div><?=
                Html::a(Yii::t('main', 'Delete'), ['order/delete'], [
                    'class' => 'btn btn-default',
                    'data' => [
                        'confirm' => StringHelper::translateOrderToOffer('main', 'Are you sure you want to delete this order?<br/><strong>It would be not available as well as its related specific data.</strong>'),
                        'method' => 'post',
                        'params' => [
                            'id' => $data['id'],
                        ],
                    ],
                        ]
                );
                ?></div>
            <?php if(Yii::$app->params['isCopyOrderVisible']) { ?>
                <div><?= Html::a(Yii::t('web', 'Copy'), ['order/copy', 'id' => $data['id']], ['class' => 'btn btn-default']) ?></div>
            <?php } ?>

            <div><?= Html::a(Yii::t('web', 'Edit'), ['order/edit', 'id' => $data['id']], ['class' => 'btn btn-default']) ?></div>
            <?php
            $updateOption = ['class' => 'btn btn-primary'];
            $quickUpdateOption = ['value' => Url::to(['order/quick-update', 'id' => $data['id']]), 'class' => 'btn btn-default pull-right', 'id' => 'modalButton'];
            if($data['isActive'] == 0 && !(Yii::$app->user->can(Utility::ROLE_ADMIN) && Yii::$app->params['isUnlimitedOrderChangeStatusAvailable'])) {
                $updateOption['onclick'] = 'return false;';
                $updateOption['disabled'] = 'true';
                //$updateOption['data-toggle'] = 'tooltip';
                //$updateOption['data-placement'] = 'bottom';
                //$updateOption['title'] = StringHelper::translateOrderToOffer('web', 'Order is inactive.');
                $quickUpdateOption['disabled'] = 'true';
                //$quickUpdateOption['data-toggle'] = 'tooltip';
                //$quickUpdateOption['data-placement'] = 'bottom';
                //$quickUpdateOption['title'] = StringHelper::translateOrderToOffer('web', 'Order is inactive.');
            }
            ?>
            <div><?= Html::button(Yii::t('web', 'Update status'), $quickUpdateOption) ?></div>
            <div><?= Html::a(StringHelper::translateOrderToOffer('web', 'Update order'), ['order/update', 'id' => $data['id']], $updateOption) ?></div>
        </div>
    </div>

    <?php
    $positiveTerminalStatus = Yii::$app->getSession()->get('positiveTerminalStatus');
    Yii::$app->getSession()->remove('positiveTerminalStatus');
    Modal::begin([
        'id' => 'modal',
        'size' => 'modal-lg',
        'clientOptions' => ['show' => !empty($positiveTerminalStatus)]
    ]);
    ?>
    <div id="modalContent">
        <?php
        if(!empty($positiveTerminalStatus)) {
            echo Yii::$app->controller->renderPartial('inc/_alert', [
                'order' => $data,
                'form' => new AlertForm()
            ]);
        }
        ?>
    </div>
    <?php Modal::end(); ?>

    <div class="col-xs-12 title-with-icon">
        <div class="base-icon basic-data-icon main-content-icons"></div>
        <h3><?= Yii::t('web', 'General information') ?></h3>
    </div>
    <div class="col-xs-12">
        <div class="row">
            <div class=" col-sm-8">
                <div class="table-responsive">
                    <table class="table table-bordered detail-table">
                        <tbody>
                            <tr>
                                <th><?= Yii::t('web', 'Number') ?></th>
                                <td><?= Html::encode($data['number']) ?></td>
                            </tr>
                            <?php if(Yii::$app->params['isNumberForClientVisible'] && !empty($data['numberForClient'])) { ?>
                                <tr>
                                    <th><?= Yii::t('main', 'Number for client') ?></th>
                                    <td><?= Html::encode($data['numberForClient']) ?></td>
                                </tr>
                            <?php } ?>
                            <?php if(!empty($data['parentOrderNumber'])) { ?>
                                <tr>
                                    <th><?= StringHelper::translateOrderToOffer('web', 'Parent order') ?></th>
                                    <td><?= Html::a(Html::encode($data['parentOrderNumber'] . ' (' . $data['parentOrderTitle']) . ')', ['order/details', 'id' => $data['parentOrderId']]) ?></td>
                                </tr>
                            <?php } ?>
                            <?php if(!empty($suborders)) { ?>
                                <tr>
                                    <th><?= StringHelper::translateOrderToOffer('web', 'Suborders') ?></th>
                                    <td><?php foreach($suborders as $suborder) { ?>
                                            <?php echo Html::a(Html::encode($suborder['number'] . ' (' . $suborder['title']) . ')', ['order/details', 'id' => $suborder['id']]) . '  ' ?>
                                        <?php } ?>
                                    </td>
                                </tr>
                            <?php } ?>
                            <?php if(Yii::$app->params['isProductionVisible']) { ?>
                                <tr>
                                    <th><?= Yii::t('main', 'Priority') ?></th>
                                    <td><?= $data['priorityName'] ?></td>
                                </tr>

                            <?php } ?>
                            <tr>
                                <th><?= Yii::$app->params['isOrderTitleIsLabeledAsConcerns'] ? Yii::t('web', 'Concerns') : (Yii::$app->params['isOrderTitleLabeledAsOrderName'] ? Yii::t('web', 'Job name') : Yii::t('web', 'Title')) ?></th>
                                <td><?= Html::encode($data['title']) ?></td>
                            </tr>
                            <tr>
                                <th><?= Yii::t('main', 'Creation date') ?></th>
                                <td><?= Html::encode(StringHelper::getFormattedDateFromDate($data['dateCreation'])) ?></td>
                            </tr>
                            <tr>
                                <th><?= Yii::t('web', 'Client') ?></th>
                                <td>
                                    <?php
                                    echo Html::a(Html::encode($data['companyName']), ['client/details', 'id' => $data['companyId']]);
                                    if(!empty($data['parentCompanyId']) && !empty($data['parentCompanyName'])) {
                                        echo ' (' . Html::a(Html::encode($data['parentCompanyName']), ['client/details', 'id' => $data['parentCompanyId']]) . ')';
                                    }
                                    ?>
                                </td>
                            </tr>
                            <?php if(Yii::$app->params['isContractTypeVisible']) { ?>
                                <tr>
                                    <th><?= Yii::t('main', 'Contract type') ?></th>
                                    <td><?= Html::encode($data['contractType']) ?></td>
                                </tr>
                            <?php } ?>
                            <?php if(!empty($contactPeople)) { ?>
                                <tr>
                                    <th><?= Yii::t('web', 'Contact people') ?></th>
                                    <td><?= $this->render('inc/_userRepresentatives', ['users' => $contactPeople, 'number' => HTML::encode($data['number']), 'entityName' => Html::encode($data['entity'])]) ?></td>
                                </tr>
                            <?php } ?>
                            <?php if(!empty($data['email'])) { ?>
                                <tr>
                                    <th><?= Yii::t('web', 'Author') ?></th>
                                    <td><?= UserHelper::getPrettyUserName($data['email'], $data['firstName'], $data['lastName']); ?></td>
                                </tr>
                            <?php } ?>
                            <?php if(Yii::$app->params['isEntityVisible']) { ?>
                                <tr>
                                    <th><?= Yii::t('web', 'Entity') ?></th>
                                    <td><?= Html::encode($data['entity']) ?></td>
                                </tr>
                            <?php } ?>
                            <?php if(!empty($orderTypes) && Yii::$app->params['isOrderTypeVisible']) { ?>
                                <tr>
                                    <th><?= StringHelper::translateOrderToOffer('web', 'Order types') ?></th>
                                    <td><?= Html::encode(implode(', ', array_column($orderTypes, 'name'))) ?></td>
                                </tr>
                            <?php } ?>
                            <tr>
                                <th><?= Yii::$app->params['isAssignedSalesmenLabeledAsResponsiblePeople'] ? Yii::t('web', 'Responsible people') : Yii::t('web', 'Assigned salesmen') ?></th>
                                <td><?= Yii::$app->params['isAssignedSalesmenLabeledAsResponsiblePeople'] ? isset($data['userResponsible']) ? $data['userResponsible'] : '' : $this->render('/inc/usersRow', ['users' => $salesmen]) ?></td>
                            </tr>
                            <tr>
                                <th><?= Yii::t('web', 'Status') ?></th>
                                <td><?= Html::encode($data['status']) ?></td>
                            </tr>
                            <tr>
                                <th><?= StringHelper::translateOrderToOffer('web', 'Order completion time') ?></th>
                                <td><?= Html::encode(StringHelper::getFormattedDateFromDate($data['dateDeadline'])) ?></td>
                            </tr>
                            <tr>
                                <th><?= Yii::t('main', 'Reminder date') ?></th>
                                <td><?= StringHelper::getFormattedDateFromDate($data['dateReminder']) ?></td>
                            </tr>
                            <tr>
                                <th><?= Yii::t('web', 'Last update date') ?></th>
                                <td><?= Html::encode(StringHelper::getFormattedDateFromDate($data['dateLastModification'])) ?></td>
                            </tr>
                            <?php if(Yii::$app->params['isShippingDateAvailable']) : ?>
                                <tr>
                                    <th><?= Yii::t('web', 'Shipping date') ?></th>
                                    <td><?= Html::encode(StringHelper::getFormattedDateFromDate($data['shippingDate'])) ?></td>
                                </tr>
                            <?php endif; ?>
                            <?php if(!empty($attachments)) { ?>
                                <tr>
                                    <th><?= Yii::t('web', 'Attachments') ?></th>
                                    <td><?= $this->render('/inc/attachmentList', ['attachments' => $attachments]) ?></td>
                                </tr>
                            <?php } ?>

                            <?php if(Yii::$app->params['isInvoicedIsVisible']) { ?>
                                <tr>
                                    <th><?= Yii::t('web', 'Is invoiced') ?></th>
                                    <td><?= StringHelper::boolTranslation($data['isInvoiced'], false) ?></td>
                                </tr>
                            <?php } ?>
                            <?php if(Yii::$app->params['isDurationTimeVisible']) { ?>
                                <tr>
                                    <th><?= Yii::t('web', 'Duration time') ?></th>
                                    <td><?= Html::encode($data['duration_time']) ?></td>
                                </tr>
                            <?php } ?>
                            <?php if(Yii::$app->params['isExecutionTimeVisible']) { ?>
                                <tr>
                                    <th><?= Yii::t('web', 'Execution time') ?></th>
                                    <td><?= Html::encode($data['execution_time']) ?></td>
                                </tr>
                            <?php } ?>
                            <?php if(Yii::$app->params['isPaymentTermsVisible']) { ?>
                                <tr>
                                    <th><?= Yii::t('web', 'Terms of payment') ?></th>
                                    <td><?= Html::encode($data['payment_terms']) ?></td>
                                </tr>
                            <?php } ?>
                        </tbody>
                    </table>
                </div>
            </div>
            <div class="col-sm-4">
                <h3 class=""><?= Yii::t('web', 'Documents') ?></h3>
                <?php if(in_array(DocumentType::INVOICE, $availableDocuments)) { ?>
                    <div class="document-link-container"><div class="base-icon form-icon doc-icons"></div>
                        <?=
                        Html::a('' . ' ' . Yii::t('documents', 'Invoice'), ['document/invoice', 'orderId' => $data['id']], ['target' => '_blank']
                        )
                        ?>
                    </div>
                <?php } ?>
                <?php if(in_array(DocumentType::PROFORMA_INVOICE, $availableDocuments)) { ?>
                    <div class="document-link-container"><div class="base-icon form-icon doc-icons"></div>
                        <?=
                        Html::a('' . ' ' . Yii::t('documents', 'Proforma invoice'), ['document/proforma-invoice', 'orderId' => $data['id']], ['target' => '_blank']
                        )
                        ?>
                    </div>
                <?php } ?>
                <?php if(in_array(DocumentType::ACCEPTANCE_PROTOCOL, $availableDocuments)) { ?>
                    <div class="document-link-container"><div class="base-icon form-icon doc-icons"></div>
                        <?=
                        Html::a('' . ' ' . Yii::t('web', 'Acceptance protocol'), ['documents/acceptance-protocol', 'orderId' => $data['id']], ['target' => '_blank']
                        )
                        ?>
                    </div>
                <?php } ?>
                <?php if(in_array(DocumentType::OFFER_DETAILS, $availableDocuments)) { ?>
                    <div class="document-link-container"><div class="base-icon form-icon doc-icons"></div>
                        <?=
                        Html::a('' . ' ' . Yii::t('web', 'Offer details'), ['documents/offer-details', 'orderId' => $data['id']], ['target' => '_blank']
                        )
                        ?>
                        <span class="separator">|</span>
                        <?=
                        Html::a('' . ' ' . Yii::t('web', 'Fast printing'), ['documents/offer-details', 'orderId' => $data['id'], 'fastPrinting' => 1], ['target' => '_blank']
                        )
                        ?>
                    </div>
                <?php } ?>
                <?php if(in_array(DocumentType::WEIGHT_PROTOCOL, $availableDocuments)) { ?>
                    <div class="document-link-container"><div class="base-icon form-icon doc-icons"></div>
                        <?=
                        Html::a('' . ' ' . Yii::t('documents', 'Weighting CO2 protocol'), ['protocol/weight', 'orderId' => $data['id']], ['target' => '_blank']
                        )
                        ?>
                    </div>
                <?php } ?>
                <?php if(in_array(DocumentType::LIGHT_MEASUREMENT, $availableDocuments)) { ?>
                    <div class="document-link-container"><div class="base-icon form-icon doc-icons"></div>
                        <?=
                        Html::a('' . ' ' . Yii::t('documents', 'Protocol of light intensity measurement'), ['protocol/light-measurement', 'orderId' => $data['id']], ['target' => '_blank']
                        )
                        ?>
                    </div>
                <?php } ?>
                <?php if(in_array(DocumentType::SERVICE_PROTOCOL, $availableDocuments)) { ?>
                    <div class="document-link-container"><div class="base-icon form-icon doc-icons"></div>
                        <?=
                        Html::a('' . ' ' . Yii::t('documents', 'Service protocol'), ['protocol/service', 'orderId' => $data['id']], ['target' => '_blank']
                        )
                        ?>
                    </div>
                <?php } ?>
                <?php if(in_array(DocumentType::SERVICE_PROTOCOL_FDAFAS, $availableDocuments)) { ?>
                    <div class="document-link-container"><div class="base-icon form-icon doc-icons"></div>
                        <?=
                        Html::a('' . ' ' . Yii::t('documents', 'Service protocol of fire alarm system'), ['protocol/service-fdafas', 'orderId' => $data['id']], ['target' => '_blank']
                        )
                        ?>
                    </div>
                <?php } ?>
                <?php if(in_array(DocumentType::REPAIR_PROTOCOL, $availableDocuments)) { ?>
                    <div class="document-link-container"><div class="base-icon form-icon doc-icons"></div>
                        <?=
                        Html::a('' . ' ' . Yii::t('documents', 'Repair protocol'), ['protocol/repair', 'orderId' => $data['id']], ['target' => '_blank']
                        )
                        ?>
                    </div>
                <?php } ?>
                <?php if(in_array(DocumentType::FIREEQ_ACCEPTANCE_PROTOCOL, $availableDocuments)) { ?>
                    <div class="document-link-container"><div class="base-icon form-icon doc-icons"></div>
                        <?=
                        Html::a('' . ' ' . Yii::t('documents', 'Protocol of inspection'), ['document/generate-firefighting-eq-acceptance-protocol', 'orderId' => $data['id']], ['target' => '_blank']
                        )
                        ?>
                    </div>
                <?php } ?>
                <?php if(in_array(DocumentType::MALFUNCTION_PROTOCOL, $availableDocuments)) { ?>
                    <div class="document-link-container"><div class="base-icon form-icon doc-icons"></div>
                        <?=
                        Html::a('' . ' ' . Yii::t('documents', 'Break-down protocol'), ['document/generate-malfunction-protocol', 'orderId' => $data['id']], ['target' => '_blank']
                        )
                        ?>
                    </div>
                <?php } ?>
                <?php if(in_array(DocumentType::ORDER_CONFIRMATION, $availableDocuments)) { ?>
                    <div class="document-link-container"><div class="base-icon form-icon doc-icons"></div>
                        <?=
                        Html::a('' . ' ' . Yii::t('documents', 'Order confirmation'), ['documents/order-confirmation', 'orderId' => $data['id']], ['target' => '_blank']
                        )
                        ?>
                    </div>
                <?php } ?>
                <?php if(in_array(DocumentType::CUSTOM_PROTOCOL, $availableDocuments)) { ?>
                    <div class="document-link-container"><div class="base-icon form-icon doc-icons"></div>
                        <?=
                        Html::a('' . ' ' . Yii::t('documents', 'Custom protocol'), ['protocol/custom', 'orderId' => $data['id']], ['target' => '_blank']
                        )
                        ?>
                    </div>
                <?php } ?>
            </div>
        </div>
    </div>

    <div class="col-xs-12 title-with-icon">
        <div class="base-icon box-icon main-content-icons"></div>
        <h3><?= StringHelper::translateOrderToOffer('web', 'Order products') ?></h3>
    </div>

    <div class="col-xs-12 well">
        <?php if(!empty($products)) { ?>
            <?php if(Yii::$app->params['isProductionVisible']) { ?>
                <?=
                Yii::$app->controller->renderPartial('inc/_detailsProductionProducts', [
                    'products' => $products,
                    'vats' => $vats,
                    'plnCurrency' => $plnCurrency,
                    'summary' => $summary,
                ]);
                ?>
            <?php } else { ?>
                <?=
                Yii::$app->controller->renderPartial('inc/_detailsProducts', [
                    'products' => $products,
                    'vats' => $vats,
                    'plnCurrency' => $plnCurrency,
                    'summary' => $summary,
                ]);
                ?>
                    <?php } ?>
<?php } else { ?>
            <div class="row">
                <div class="col-sm-8 desc no-desc">
    <?php echo Yii::t('web', 'No data'); ?>
                </div>
            </div>
    <?php } ?>
    </div>

    <?php if(!empty($reservedProducts)) { ?>
        <?= $this->render('/order/inc/_reservedProductsList', ['orderId' => $data['id'], 'reservedProducts' => $reservedProducts]) ?>
    <?php } ?>

    <?php
    if(!empty($data['customData']) && Yii::$app->params['isOrderCustomDataVisible']) {
        $array = Json::decode($data['customData']);
        $customData = reset($array);
        ?>
        <div class="col-xs-12 title-with-icon">
            <div class="base-icon table-icon main-content-icons"></div>
            <h3><?= Yii::t('web', 'Additional data') ?></h3>
        </div>

        <div class="col-xs-12">
            <div class="table-responsive">
                <table class="table table-bordered table-hover list-grid-table">
                    <thead>
                        <tr>
    <?php foreach($customData['theads'] as $thead) { ?>
                                <th><?= $thead ?></th>
                        <?php } ?>
                        </tr>
                    </thead>
                    <tbody>
                            <?php foreach($customData['rows'] as $row) { ?>
                            <tr>
                                <?php
                                for($i = 0; $i < count($row); ++$i) {
                                    if($i == count($row) - 3) {
                                        ?>
                                        <td><?= StringHelper::getFormattedCost($row[$i], true) ?></td>
                                <?php } else {
                                    ?>
                                        <td><?= $row[$i] ?></td>	
            <?php } ?>
                                <?php } ?>
                            </tr>
                            <?php } ?>
                    </tbody>
                    <tfoot>
                        <tr>
    <?php for($i = 0; $i < count($customData['theads']) - 2; ++$i) { ?>
                                <td></td>
    <?php } ?>
                            <td><?= Yii::t('main', 'Total') ?></td>
                            <td><?= $data['customDataSummary'] ?></td> 
                        </tr>
                    </tfoot>
                </table>
            </div>
        </div>
<?php } ?>

<?php if(Yii::$app->params['isOrderGalleryVisible']) { ?>
        <div class="col-xs-12 title-with-icon">
            <div class="base-icon gallery-icon main-content-icons"></div>
            <h3><?= Yii::t('web', 'Photo gallery') ?></h3>
        </div>

        <div class="col-xs-12">
            <div class="row">
                <div class="col-sm-8 desc <?= !empty($photos) ? '' : 'no-desc' ?>">
    <?php if(!empty($photos)) { ?>
                        <div class="gallery">
            <?= Gallery::widget(['items' => $photos]); ?>
                        </div>
    <?php } else echo Yii::t('web', 'No images to display') ?>
                </div>
            </div>
        </div>
<?php } ?>

    <div class="col-xs-12 title-with-icon">
        <div class="base-icon description-icon main-content-icons"></div>
        <h3><?= StringHelper::translateOrderToOffer('web', 'Order description') . ' (' . mb_strtolower(\Yii::t('web', 'Displayed above the table with products'), 'UTF-8') . ')' ?></h3>
    </div>

    <div class=" col-xs-12">
        <div class="row">
            <div class="col-sm-8 desc">
<?= empty($data['description']) ? Yii::t('web', 'None') : nl2br(Html::encode($data['description'])); ?>
            </div>
        </div>
    </div>
<?php if(Yii::$app->params['isOrderNoteVisible']) { ?>
        <div class="col-xs-12 title-with-icon">
            <div class="base-icon description-icon main-content-icons"></div>
            <h3><?= StringHelper::translateOrderToOffer('web', 'Note') . ' (' . mb_strtolower(\Yii::t('web', 'Displayed under the table with products as bold'), 'UTF-8') . ')' ?></h3>
        </div>
        <div class=" col-xs-12">
            <div class="row">
                <div class="col-sm-8 desc">
        <?= empty($data['descriptionNote']) ? Yii::t('web', 'None') : '<b>' . nl2br(Html::encode($data['descriptionNote'])) . '</b>'; ?>
                </div>
            </div>
        </div>
<?php } ?>
<?php if(Yii::$app->params['isOrderRemarksVisible']) { ?>
        <div class="col-xs-12 title-with-icon">
            <div class="base-icon description-icon main-content-icons"></div>
            <h3><?= StringHelper::translateOrderToOffer('web', 'Remarks') . ' (' . mb_strtolower(\Yii::t('web', 'Displayed under the table with products'), 'UTF-8') . ')' ?></h3>
        </div>
        <div class=" col-sm-12">
            <div class="row">
                <div class="col-sm-8 desc">
        <?= empty($data['descriptionCont']) ? Yii::t('web', 'None') : nl2br(Html::encode($data['descriptionCont'])); ?>
                </div>
            </div>
        </div>

<?php } ?>

<?php if(Yii::$app->params['isOrderTasksVisible']) { ?>
        <div class="col-xs-12 title-with-icon">
            <div class="base-icon history-icon main-content-icons"></div>
            <h3><?= StringHelper::translateOrderToOffer('web', 'Tasks for this order') ?></h3>
        </div>
        <div class="col-xs-12">
            <div id="task-object-table" class="dynamic-table">

                <div><?= Html::a(Yii::t('web', 'Add task'), ['task/create', 'orderId' => $data['id'], 'taskTypeId' => $listData['taskTypeId']], ['class' => 'btn btn-primary']) ?></div>
                <div>
                    <!--<div class="row dynamic-table-filters">
                
                    </div>-->
                    <div class="dynamic-table-data">
        <?= Yii::$app->controller->renderPartial('inc/_tasks', ['items' => $listData['items'], 'pages' => $listData['pages'], 'listForm' => $listData['listForm']]) ?>
                    </div>
                </div>
            </div>
        </div>
    <?php } ?>

    <?=
    $this->renderFile('@frontend/views/document/list.php', [
        'documents' => $documents,
        'documentsAbleToDelete' => $documentsAbleToDelete,
        'orderId' => $data['id'],
        'enableEditButton' => true,
        'availableDocuments' => $availableDocuments,
    ]);
    ?>

    <?=
    $this->renderFile('@frontend/views/order/inc/_history.php', [
        'entries' => $entries,
        'orderEditions' => $orderEditions,
        'orderId' => $data['id'],
    ]);
    ?>

    <?php
    EnhancedDialogAsset::register($this);
    $this->registerJs('toggleAttributes();', View::POS_END);
    $this->registerJs('dynamicTable("task-object-table", "' . Url::to(['order/details']) . '", { params: { id: ' . $data['id'] . ' }});', View::POS_END);
    ?>
</div>