<?php

use yii\helpers\Url;
use yii\helpers\Html;
use common\components\ActiveForm;
use yii\web\View;
use common\helpers\StringHelper;
use common\helpers\Utility;

$this->registerJsFile('@web/js/forms.js?' . uniqid(), ['depends' => [\yii\web\JqueryAsset::className()]]);
$this->registerJsFile('@web/js/pages/toggle-attributes.js?' . uniqid(), ['depends' => [\yii\web\JqueryAsset::className()]]);
$this->registerJsFile('@web/js/pages/ral-colour-picker.js?' . uniqid(), ['depends' => [\yii\web\JqueryAsset::className()]]);
if(Yii::$app->params['isVisualisationVisible']) {
    $this->registerJsFile('@web/js/pages/order-visualisation.js?' . uniqid(), ['depends' => [\yii\web\JqueryAsset::className()]]);
}


$this->title = StringHelper::translateOrderToOffer('web', 'Create order') . ' - ' . Yii::$app->name;

$this->params['breadcrumbs'][] = ['label' => StringHelper::translateOrderToOffer('main', 'Orders'), 'url' => Url::to(['order/index'])];
$this->params['breadcrumbs'][] = ['label' => StringHelper::translateOrderToOffer('main', 'List of orders'), 'url' => Url::to(['order/list'])];
$this->params['breadcrumbs'][] = StringHelper::translateOrderToOffer('web', 'Create order');
?>

<div class="order-form">
    <h2 class="page-title"><?= StringHelper::translateOrderToOffer('web', 'Create order') ?></h2>

    <?php
    $form = ActiveForm::begin([
                'id' => 'order-create-form',
                'enableAjaxValidation' => true,
                'enableClientValidation' => false,
                'options' => [
                    'class' => 'form',
                ]
    ]);

    echo $form->errorSummary($invalidForms);
    ?>

    <?=
    Yii::$app->controller->renderPartial('inc/_basic_data', [
        'entities' => $entities,
        'orderCreateForm' => $orderCreateForm,
        'orderProductsForms' => $orderProductsForms,
        'currencies' => $currencies,
        'orderTypes' => $orderTypes,
        'contractTypes' => $contractTypes,
        'form' => $form,
        'scenario' => $scenario,
    ])
    ?>

    <div id="products">
        <?=
        Yii::$app->controller->renderPartial('inc/_products', [
            'orderProductsForms' => $orderProductsForms,
            'currencies' => $currencies,
            'constructions' => $constructions,
            'shapes' => $shapes,
            'muntins' => $muntins,
            'form' => $form,
            'displayFirstAttributes' => $displayFirstAttributes,
            'orderProductsVats' => $orderProductsVats,
            'orderProductsSummary' => $orderProductsSummary,
            'plnCurrency' => $plnCurrency,
            'productAttributeForms' => !empty($productAttributeForms) ? $productAttributeForms : [],
        ]);
        ?>
    </div>
    <div id="costs">
        <?=
        Yii::$app->controller->renderPartial('inc/_costs', [
            'orderCostsForms' => $orderCostsForms,
            'currencies' => $currencies,
            'constructions' => $constructions,
            'shapes' => $shapes,
            'muntins' => $muntins,
            'form' => $form,
            'displayFirstAttributes' => $displayFirstAttributes,
            'orderCostsVats' => $orderCostsVats,
            'orderCostsSummary' => $orderCostsSummary,
            'plnCurrency' => $plnCurrency,
            'productAttributeForms' => !empty($artificialProductAttributeForms) ? $artificialProductAttributeForms : [],
        ]);
        ?>
    </div>

    <div id="product-summary">
        <?= Yii::$app->controller->renderPartial('inc/_productSummary', ['summary' => $summary, 'plnCurrency' => $plnCurrency]) ?>
    </div>

    <?= Yii::$app->controller->renderPartial('inc/_table', ['orderTableForm' => $orderTableForm, 'form' => $form]) ?>

    <?= Yii::$app->controller->renderPartial('inc/_employees', ['orderEmployeesForm' => $orderEmployeesForm, 'form' => $form, 'orderCreateForm' => $orderCreateForm]) ?>

    <?=
    Yii::$app->controller->renderPartial('inc/_files', [
        'orderCreateForm' => $orderCreateForm,
        'photosForms' => $photosForms,
        'documentsForms' => $documentsForms,
        'form' => $form,
    ]);
    ?>

    <div class="col-sm-12">
        <?= Html::a(Yii::t('web', 'Cancel'), Url::to(['order/list']), ['class' => 'btn btn-default']) ?>
        <input type="hidden" name="save" value="0"/>
        <input type="hidden" name="OrderCreateForm[orderId]" value=""/>
        <button  type="button" class="btn btn-primary pull-right" id="add-order"><?= Yii::t('web', 'Add') ?></button>
        <div class="save-order-div">
            <div id="info">
                .....
            </div>
            <button class="btn btn-primary pull-right" id="save-order" style="display: <?php echo Yii::$app->params['isOrderQuickSaveVisible'] ? 'block' : 'none' ?>">
                <span class="glyphicon glyphicon-floppy-disk" aria-hidden="true"></span>
            </button>
        </div>
    </div>

    <?php ActiveForm::end() ?>
</div>

<?php
$this->registerJsFile('@web/js/order-form.js?' . uniqid(), ['depends' => [\frontend\assets\AppAsset::className(), \yii\web\JqueryAsset::className()]]);

$this->registerJs('
    (function() {
        var dynamicForm = new DynamicForm("order-create-form");
        dynamicForm.activateRemoveButtons();
        dynamicForm.activateAddItemButtons();

        OrderForm.activateEmployeesChooser($("#employee-select"), $("#selected-employees"));
        OrderForm.save("order-create-form", "OrderCreateForm[orderId]", "<div class=\"alert alert-success\" role=\"alert\">' . Yii::t('web', 'Success save') . '</div>", "<div class=\"alert alert-danger\" role=\"alert\">' . Yii::t('web', 'Error') . '</div>");
        dynamicForm.activatePhotoUpload();
        dynamicForm.activateDocumentUpload();
        dynamicForm.activateProductFileUpload();
        OrderForm.initRecalculate("OrderProductForm", "' . Url::to(['order/get-products-form']) . '", "order-create-form", "products", "ProductAttributeForm");
        OrderForm.initRecalculate("OrderCostForm", "' . Url::to(['order/get-costs-form']) . '", "order-create-form", "costs", "ArtificialProductAttributeForm");
		OrderForm.initRecalculateSummary("' . Url::to(['order/get-all-products-summary']) . '", "product-summary");
		OrderForm.initRecalculateCustomData("' . Url::to(['order/get-custom-data-summary']) . '", "' . Url::to(['order/get-custom-data-numeration']) . '", "custom-tables");
        toggleAttributes();
        handleColourPicker("' . Url::to(['colour/get-modal-content']) . '");
        
    })();
    ', View::POS_READY);

$this->registerJs('        
    	$("#ordercreateform-clientid").trigger("select2:select");    
    ', View::POS_END);

if(Yii::$app->params['isOrderProductDimensionsVisible']) {
    $this->registerJs('     
        $(document).ready(function() {
            localStorage.removeItem("lastId");
                
            $(document).on("keydown", function (e) {
                if (e.which == 9) {
                    e.preventDefault();
                    if($("#" + e.target.id).hasClass("tab-me")) {
                
                        var $others = $("#" + e.target.id).parents().addBack().nextAll().find("*").addBack().filter(".tab-me");
                        if($others.length == 0) {
                            $others = $(document).find(".tab-me");
                        }
                
                        $others.first().focus();
                        localStorage.setItem("lastId", $others.first().attr("id"));
                    }
                }
            });
        });
                
        ', View::POS_END);
}
// zastanowić się, czy to jest potrzebne
/* $this->registerJs('            	
  $(document).ready(function() {
  console.log("clear session");
  localStorage.removeItem("lastId");
  });

  var saveCurrentId = function(){
  if (document.activeElement.id != null && document.activeElement.id != ""){
  localStorage.setItem("lastId", document.activeElement.id);
  }
  }

  $("#order-create-form").on("change", function(){
  if (localStorage.getItem("lastId") != null && localStorage.getItem("lastId") != ""){
  setTimeout(function(){
  var elem = document.getElementById(localStorage.getItem("lastId"));
  elem.focus();
  },100);
  }
  $("input").on("focus", saveCurrentId);
  $(".select2-selection__rendered").on("focus", saveCurrentId);
  $(document).on("keyup click", saveCurrentId);


  });
  ', View::POS_END); */
?>

<?php
if(Yii::$app->params['isVisualisationVisible']) {
    $shapes = implode('-', Utility::getShapes());
    $this->registerJs('init("' . Url::to(['order/get-color-and-shape']) . '", "' . $shapes . '", "'.Url::to(['storage/index']).'");', View::POS_END);
}
?>




