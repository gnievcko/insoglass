<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\View;
use dosamigos\multiselect\MultiSelect;
use kartik\select2\Select2;
use yii\web\JsExpression;
use common\models\ar\Company;
use common\helpers\StringHelper;
use common\helpers\Utility;
use yii\web\JqueryAsset;
use kartik\datecontrol\DateControl;
use frontend\models\OrdersListForm;

$this->title = StringHelper::translateOrderToOffer('main', 'List of orders') . ' - ' . Yii::$app->name;

$this->params['breadcrumbs'][] = ['label' => StringHelper::translateOrderToOffer('main', 'Orders'), 'url' => Url::to(['order/index'])];
$this->params['breadcrumbs'][] = StringHelper::translateOrderToOffer('main', 'List of orders');

$this->registerJsFile('@web/js/dynamic-table.js?' . uniqid(), []);
$this->registerJsFile('@web/js/layout/moreFilters.js?' . uniqid(), ['depends' => [JqueryAsset::className()]]);
?>

<div id="orders-list-wrapper">

    <div id="dashboard-orders-list-table" class="dynamic-table">
        <div class="dynamic-table-data">
            <?=
            Yii::$app->controller->renderPartial('inc/_orders', [
                'orders' => $orders,
                'pages' => $pages,
                'ordersListForm' => $ordersListForm,
                'dashboard' => true
            ])
            ?>
        </div>
    </div>
    <?php
    $this->registerJs('dynamicTable("dashboard-orders-list-table", "' . Url::to(['order/short-list-dashboard']) . '", {url: "'.(Yii::$app->request->get('dashboard') ? Url::to(['dashboard/index']) : null).'"});', View::POS_END);
    $this->registerJs('moreFilters(' . Yii::$app->params['isListFiltersVisibleByDefault'] . ');', View::POS_END);
    ?>
</div>
