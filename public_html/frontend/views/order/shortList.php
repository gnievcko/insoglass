<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\View;

$this->title = Yii::t('web', 'List of orders') . ' - ' . Yii::$app->name;

$this->registerJsFile('@web/js/dynamic-table.js?' . uniqid(), []);
$this->registerJsFile('@web/js/pages/task-short-list.js?' . uniqid(), ['depends' => [\yii\web\JqueryAsset::className()]]);
?>

<div id="task-short-list-wrapper">
    <div id="order-short-list-table" class="dynamic-table">
        <div class="row dynamic-table-filters borderless">
            <div class="col-xs-3">
                <div class="form-group">
                    <?=
                    Html::activeDropDownList($listForm, 'orderStatusId', $orderStatuses, ['class' => 'dynamic-table-filter form-control']);
                    ?>
                </div>
            </div>
            <div class="col-xs-6 text-center"><h2 class="short-order-header"><?= Yii::t('main', 'List of orders') ?></h2></div>
            <div class="col-xs-3 text-right">
                <?= Html::a('<span class="icon-img plus-icon" aria-hidden="true"></span>', ['order/create'], ['class' => 'base-icon alert-icon add-task-icon']) ?>
            </div>
        </div>
        <div class="dynamic-table-data">
            <?=
            Yii::$app->controller->renderPartial('inc/_shortOrder', [
                'items' => $items,
                'pages' => $pages,
                'listForm' => $listForm,
            ])
            ?>
        </div>
    </div>
</div>

<?php
$this->registerJs('
		var table = new dynamicTable("order-short-list-table", "' . Url::to(['order/short-list']) . '", 
            		{url: "' . (Yii::$app->request->get('dashboard') ? Url::to(['dashboard/index']) : null) . '"});	
		initDynamicFeatures("' . Url::to(['order/']) . '", "order-short-list-table");
', View::POS_END);
?>
