<?php
    use yii\bootstrap\Html;
    use common\helpers\StringHelper;
use common\models\ar\Currency;
use common\helpers\Utility;
    $option = ['class' => 'form-control xSmallTextarea', 'placeHolder' => Yii::t('documents', 'Write value')];   
 ?>

<tr class="document-table-row">
    <?php
    	$index = 0;
    	foreach($table['theads'] as $key => $th) {
    		$name = 'tables['.$uniq.'][rows]['.$idx.']['.$key.']';
    		
    		$text = isset($row[$key]) ? $row[$key] : null;
    		if(isset($initData) && !empty($initData)) {
    			$text = $initData[$index];
    		}
    		
    		if($key == 0) {
    			echo '<td>'.(Html::textarea($name, ($idx + 1), array_merge($option, ['readonly' => true]))).'</td>';
    		}
    		elseif($key + 3 < count($table['theads'])) {
    	    	echo '<td>'.Html::textarea($name, $text, $option).'</td>';
    		}
    		elseif($key + 2 < count($table['theads'])) {
    			echo '<td>'.Html::input('number', $name, $text, array_merge($option, ['min' => 0, 'step' => 0.01])).'</td>';
    		}
    		elseif($key + 1 < count($table['theads'])) {
    			echo '<td>'.Html::input('number', $name, $text, array_merge($option, ['min' => 0])).'</td>';
    		}
    		else {
    			if(empty($text)) {
    				// TODO: Przeniesc pobieranie waluty gdzie indziej
    				$plnCurrency = Currency::find()->where(['symbol' => Utility::CURRENCY_PLN])->one();
    				$text = StringHelper::getFormattedCost(0).' '.$plnCurrency->short_symbol;
    			}
    			
    			echo '<td>'.(Html::textarea($name, $text, array_merge($option, ['readonly' => true]))).'</td>';
    		}
    		$index++;
    	}
    ?>
    <td style="width: 72px">
    	<div class="base-icon copy-icon action-icon copy-row-btn-tmp"></div>
        <div class="copy-row-btn hidden"></div>
    	<div class="base-icon garbage-icon action-icon remove-row-btn"></div>
    </td>
</tr>
