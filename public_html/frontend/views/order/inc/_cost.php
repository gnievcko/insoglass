<?php

use yii\helpers\Html;
use yii\web\View;
use common\helpers\StringHelper;

$formBaseName = $orderCostForm->formName();
$activeFormBaseName = strtolower($formBaseName);
$formName = "{$activeFormBaseName}-{$prefix}";
?>
<div class="item">
    <div class="order-cost col-xs-11" data-form-base-name="<?= $formBaseName ?>" data-active-form-base-name="<?= $activeFormBaseName ?>">
        <div class="row">
            <div class="col-xs-12">
                <div class="row">
                    <div class="col-sm-5">
                        <?php $inputOptions = ['placeholder' => $orderCostForm->getAttributeLabel('name')] ?>

                        <?php if(empty($form)): ?>
                            <?php $fieldId = "{$formName}-name"; ?>

                            <div id="container-<?= $fieldId ?>" class="form-group <?= $orderCostForm->isAttributeRequired('name') ? 'required' : '' ?>">
                                <?= Html::activeTextarea($orderCostForm, "[{$prefix}]name", $inputOptions + ['class' => 'form-control smallTextarea', 'id' => "{$fieldId}"]) ?>
                                <div class="help-block"></div>
                            </div>

                            <?php
                            $this->registerJs('
            	                    (function() {
            	                        $("#' . $formId . '").yiiActiveForm("add", {
            	                            id: "' . $fieldId . '",
            	                            name: "' . "[{$prefix}]name" . '",
            	                            container: "#container-' . $fieldId . '",
            	                            input: "#' . "$fieldId" . '",
            	                            enableClientValidation: false,
            	                            enableAjaxValidation: true
            	                        });
            	                    })();
            	                    ', View::POS_END);
                            ?>
                        <?php else: ?>

                            <?php $field = $form->field($orderCostForm, "[{$prefix}]name")->textArea($inputOptions + ['class' => 'form-control smallTextarea']) ?>
                            <?= Yii::$app->params['isOrderProductDimensionsVisible'] ? $field : $field->label(false) ?>
                        <?php endif ?>
                    </div>



                    <div class="col-sm-<?= Yii::$app->params['isOrderGrossValuesVisible'] ? 1 : 2 ?>">
                        <?php $inputOptions = ['type' => 'number', 'class' => 'form-control tab-me', 'min' => 0, 'step' => 1, 'integer' => true, 'placeholder' => Yii::t('web', 'Number of pieces')] ?>
                        <?php if(empty($form)): ?>
                            <?php $fieldId = "{$formName}-count"; ?>

                            <div id="container-<?= $fieldId ?>" class="form-group <?= $orderCostForm->isAttributeRequired('count') ? 'required' : '' ?>">
                                <?= Html::activeTextInput($orderCostForm, "[{$prefix}]count", $inputOptions + ['class' => 'form-control', 'id' => "{$fieldId}"]) ?>
                                <div class="help-block"></div>
                            </div>

                            <?php
                            $this->registerJs('
            	                    (function() {
            	                        $("#' . $formId . '").yiiActiveForm("add", {
            	                            id: "' . $fieldId . '",
            	                            name: "' . "[{$prefix}]count" . '",
            	                            container: "#container-' . $fieldId . '",
            	                            input: "#' . "$fieldId" . '",
            	                            enableClientValidation: false,
            	                            enableAjaxValidation: true
            	                        });
            	                    })();
            	                    ', View::POS_END);
                            ?>
                        <?php else: ?>
                            <?php $field = $form->field($orderCostForm, "[{$prefix}]count")->textInput($inputOptions) ?>
                            <?= Yii::$app->params['isOrderProductDimensionsVisible'] ? $field : $field->label(false) ?>
                        <?php endif ?>
                    </div>

                    <?php if(!Yii::$app->params['isOrderProductDimensionsVisible']) { ?>
                        <div class="col-sm-1">
                            <?php $inputOptions = ['type' => 'text', 'class' => 'form-control', 'value' => !empty(Yii::$app->params['defaultUnit']) ? Yii::t('documents', Yii::$app->params['defaultUnit']) : Yii::t('web', 'Unit'), 'readonly' => true] ?>
                            <?php if(empty($form)): ?>
                                <?php $fieldId = "{$formName}-unit"; ?>

                                <div id="container-<?= $fieldId ?>" class="form-group <?= $orderCostForm->isAttributeRequired('unit') ? 'required' : '' ?>">
                                    <?= Html::activeTextInput($orderCostForm, "[{$prefix}]unit", $inputOptions + ['id' => "{$fieldId}"]) ?>
                                    <div class="help-block"></div>
                                </div>

                                <?php
                                $this->registerJs('
            	                    (function() {
            	                        $("#' . $formId . '").yiiActiveForm("add", {
            	                            id: "' . $fieldId . '",
            	                            name: "' . "[{$prefix}]unit" . '",
            	                            container: "#container-' . $fieldId . '",
            	                            input: "#' . "$fieldId" . '",
            	                            enableClientValidation: false,
            	                            enableAjaxValidation: true
            	                        });
            	                    })();
            	                    ', View::POS_END);
                                ?>
                            <?php else: ?>
                                <?= $form->field($orderCostForm, "[{$prefix}]unit")->textInput($inputOptions)->label(false) ?>
                            <?php endif ?>
                        </div>
                    <?php } ?>

                    <div class="col-sm-<?= Yii::$app->params['isOrderGrossValuesVisible'] ? 1 : 2 ?>">
                        <?php $inputOptions = ['type' => 'number', 'step' => 0.01, 'placeholder' => $orderCostForm->getAttributeLabel('price')] ?>
                        <?php if(empty($form)): ?>
                            <?php $fieldId = "{$formName}-price"; ?>

                            <div id="container-<?= $fieldId ?>" class="form-group <?= $orderCostForm->isAttributeRequired('price') ? 'required' : '' ?>">
                                <?= Html::activeTextInput($orderCostForm, "[{$prefix}]price", $inputOptions + ['class' => 'form-control', 'id' => "{$fieldId}"]) ?>
                                <div class="help-block"></div>
                            </div>

                            <?php
                            $this->registerJs('
            	                    (function() {
            	                        $("#' . $formId . '").yiiActiveForm("add", {
            	                            id: "' . $fieldId . '",
            	                            name: "' . "[{$prefix}]price" . '",
            	                            container: "#container-' . $fieldId . '",
            	                            input: "#' . "$fieldId" . '",
            	                            enableClientValidation: false,
            	                            enableAjaxValidation: true
            	                        });
            	                    })();
            	                    ', View::POS_END);
                            ?>
                        <?php else: ?>
                            <?php $field = $form->field($orderCostForm, "[{$prefix}]price")->textInput($inputOptions) ?>
                            <?= Yii::$app->params['isOrderProductDimensionsVisible'] ? $field->label($orderCostForm->getAttributeLabel('price')) : $field->label(false) ?>
                        <?php endif ?>
                    </div>

                    <div class="col-sm-1" style="display:none">
                        <?php if(empty($form)): ?>
                            <?php $fieldId = "{$formName}-currencyId"; ?>

                            <div id="container-<?= $fieldId ?>" class="form-group <?= $orderCostForm->isAttributeRequired('currencyId') ? 'required' : '' ?>">
                                <?= Html::activeDropDownList($orderCostForm, "[{$prefix}]currencyId", $currencies, ['id' => "{$fieldId}", 'class' => 'form-control currency']) ?>
                                <div class="help-block"></div>
                            </div>

                            <?php
                            $this->registerJs('
            	                    (function() {
            	                        $("#' . $formId . '").yiiActiveForm("add", {
            	                            id: "' . $fieldId . '",
            	                            name: "' . "[{$prefix}]currencyId" . '",
            	                            container: "#container-' . $fieldId . '",
            	                            input: "#' . "$fieldId" . '",
            	                            enableClientValidation: false,
            	                            enableAjaxValidation: true
            	                        });
            	                    })();
            	                    ', View::POS_END);
                            ?>
                        <?php else: ?>
                            <?= $form->field($orderCostForm, "[{$prefix}]currencyId")->dropDownList($currencies)->label(false) ?>
                        <?php endif ?>
                    </div>

                    <div class="col-sm-<?= Yii::$app->params['isOrderGrossValuesVisible'] ? 1 : 2 ?>">
                        <div class="row">
                            <?php if(Yii::$app->params['isOrderProductDimensionsVisible']) { ?>
                                <div class="col-xs-12">
                                    <label class="control-label"><?= Yii::t('web', 'Net value') ?></label>
                                </div>
                            <?php } ?>
                            <div class="col-xs-12">
                                <?= StringHelper::getFormattedCost($orderCostForm->netValue) . ' ' . $plnCurrency->short_symbol ?>
                            </div>
                        </div>
                    </div>

                    <?php if(Yii::$app->params['isOrderGrossValuesVisible']) { ?>
                        <div class="col-sm-1">
                            <?php if(empty($form)): ?>
                                <?php $fieldId = "{$formName}-vatRate"; ?>

                                <div id="container-<?= $fieldId ?>" class="form-group <?= $orderCostForm->isAttributeRequired('vatRate') ? 'required' : '' ?>">
                                    <?= Html::activeDropDownList($orderCostForm, "[{$prefix}]vatRate", Yii::$app->params['vatRate'], ['id' => "{$fieldId}", 'selected' => '0.23', 'class' => 'form-control currency']) ?>
                                    <div class="help-block"></div>
                                </div>

                                <?php
                                $this->registerJs('
            		                    (function() {
            		                        $("#' . $formId . '").yiiActiveForm("add", {
            		                            id: "' . $fieldId . '",
            		                            name: "' . "[{$prefix}]vatRate" . '",
            		                            container: "#container-' . $fieldId . '",
            		                            input: "#' . "$fieldId" . '",
            		                            enableClientValidation: false,
            		                            enableAjaxValidation: true
            		                        });
            		                    })();
            		                    ', View::POS_END);
                                ?>
                            <?php else: ?>
                                <?php $field = $form->field($orderCostForm, "[{$prefix}]vatRate")->dropDownList(Yii::$app->params['vatRate']) ?>
                                <?= Yii::$app->params['isOrderProductDimensionsVisible'] ? $field : $field->label(false) ?>
                            <?php endif ?>
                        </div>
                        <div class="col-sm-1">
                            <?php if(Yii::$app->params['isOrderProductDimensionsVisible']) { ?>
                                <label class="control-label"><?= Yii::t('web', 'Vat amount') ?></label>
                            <?php } ?>
                            <?= StringHelper::getFormattedCost($orderCostForm->vatAmount) . ' ' . $plnCurrency->short_symbol ?>
                        </div>
                        <div class="col-sm-1">
                            <?php if(Yii::$app->params['isOrderProductDimensionsVisible']) { ?>
                                <label class="control-label"><?= Yii::t('web', 'Gross value') ?></label>
                            <?php } ?>
                            <?= StringHelper::getFormattedCost($orderCostForm->grossValue) . ' ' . $plnCurrency->short_symbol ?>
                        </div>
                        <?php
                    } else {
                        ?>
                        <div class="col-sm-1" style="display: none">
                            <?php $field = $form->field($orderCostForm, "[{$prefix}]vatRate")->hiddenInput()->label(false); ?>
                            <?= Yii::$app->params['isOrderProductDimensionsVisible'] ? $field : $field->label(false) ?>
                        </div>
                    <?php } ?>
                </div>
            </div>

            <div class="col-xs-12">
                <?php if(Yii::$app->params['isOrderProductDimensionsVisible']) { ?>
                    <div class="row">   
                        <div class="col-sm-2">
                            <?php $inputOptions = ['type' => 'text', 'class' => 'form-control width tab-me', 'placeholder' => Yii::t('web', 'Width') . ' [mm]'] ?>
                            <?php if(empty($form)): ?>
                                <?php $fieldId = "{$formName}-width"; ?>

                                <div id="container-<?= $fieldId ?>" class="form-group <?= $orderCostForm->isAttributeRequired('width') ? 'required' : '' ?>">
                                    <?= Html::activeTextInput($orderCostForm, "[{$prefix}]width", $inputOptions + ['id' => "{$fieldId}"]) ?>
                                    <div class="help-block"></div>
                                </div>

                                <?php
                                $this->registerJs('
            	                    (function() {
            	                        $("#' . $formId . '").yiiActiveForm("add", {
            	                            id: "' . $fieldId . '",
            	                            name: "' . "[{$prefix}]width" . '",
            	                            container: "#container-' . $fieldId . '",
            	                            input: "#' . "$fieldId" . '",
            	                            enableClientValidation: false,
            	                            enableAjaxValidation: true
            	                        });
            	                    })();
            	                    ', View::POS_END);
                                ?>
                            <?php else: ?>
                                <?= $form->field($orderCostForm, "[{$prefix}]width")->textInput($inputOptions) ?>
                            <?php endif ?>
                        </div>

                        <div class="col-sm-2">
                            <?php $inputOptions = ['type' => 'text', 'class' => 'form-control height tab-me', 'placeholder' => Yii::t('web', 'Height') . ' [mm]'] ?>
                            <?php if(empty($form)): ?>
                                <?php $fieldId = "{$formName}-height"; ?>

                                <div id="container-<?= $fieldId ?>" class="form-group <?= $orderCostForm->isAttributeRequired('height') ? 'required' : '' ?>">
                                    <?= Html::activeTextInput($orderCostForm, "[{$prefix}]height", $inputOptions + ['id' => "{$fieldId}"]) ?>
                                    <div class="help-block"></div>
                                </div>

                                <?php
                                $this->registerJs('
            	                    (function() {
            	                        $("#' . $formId . '").yiiActiveForm("add", {
            	                            id: "' . $fieldId . '",
            	                            name: "' . "[{$prefix}]height" . '",
            	                            container: "#container-' . $fieldId . '",
            	                            input: "#' . "$fieldId" . '",
            	                            enableClientValidation: false,
            	                            enableAjaxValidation: true
            	                        });
            	                    })();
            	                    ', View::POS_END);
                                ?>
                            <?php else: ?>
                                <?= $form->field($orderCostForm, "[{$prefix}]height")->textInput($inputOptions) ?>
                            <?php endif ?>
                        </div>

                        <div class="col-sm-3">
                            <?php $inputOptions = ['class' => 'form-control shape-id', 'prompt' => Yii::t('web', 'Select shape')] ?>
                            <?php if(empty($form)): ?>
                                <?php $fieldId = "{$formName}-shapeId"; ?>

                                <div id="container-<?= $fieldId ?>" class="form-group <?= $orderCostForm->isAttributeRequired('shapeId') ? 'required' : '' ?>">
                                    <?= Html::activeDropDownList($orderCostForm, "[{$prefix}]shapeId", $shapes, $inputOptions + ['id' => "{$fieldId}"]) ?>
                                    <div class="help-block"></div>
                                </div>

                                <?php
                                $this->registerJs('
            	                    (function() {
            	                        $("#' . $formId . '").yiiActiveForm("add", {
            	                            id: "' . $fieldId . '",
            	                            name: "' . "[{$prefix}]shapeId" . '",
            	                            container: "#container-' . $fieldId . '",
            	                            input: "#' . "$fieldId" . '",
            	                            enableClientValidation: false,
            	                            enableAjaxValidation: true
            	                        });
            	                    })();
            	                    ', View::POS_END);
                                ?>
                            <?php else: ?>
                                <?= $form->field($orderCostForm, "[{$prefix}]shapeId")->dropDownList($shapes, $inputOptions) ?>
                            <?php endif ?>
                        </div>

                        <div class="col-sm-3">
                            <?php $inputOptions = ['class' => 'form-control construction-id', 'prompt' => Yii::t('web', 'Select construction')] ?>
                            <?php if(empty($form)): ?>
                                <?php $fieldId = "{$formName}-constructionId"; ?>

                                <div id="container-<?= $fieldId ?>" class="form-group <?= $orderCostForm->isAttributeRequired('constructionId') ? 'required' : '' ?>">
                                    <?= Html::activeDropDownList($orderCostForm, "[{$prefix}]constructionId", $constructions, $inputOptions + ['id' => "{$fieldId}"]) ?>
                                    <div class="help-block"></div>
                                </div>

                                <?php
                                $this->registerJs('
            	                    (function() {
            	                        $("#' . $formId . '").yiiActiveForm("add", {
            	                            id: "' . $fieldId . '",
            	                            name: "' . "[{$prefix}]constructionId" . '",
            	                            container: "#container-' . $fieldId . '",
            	                            input: "#' . "$fieldId" . '",
            	                            enableClientValidation: false,
            	                            enableAjaxValidation: true
            	                        });
            	                    })();
            	                    ', View::POS_END);
                                ?>
                            <?php else: ?>
                                <?= $form->field($orderCostForm, "[{$prefix}]constructionId")->dropDownList($constructions, $inputOptions) ?>
                            <?php endif ?>
                        </div>
                        <div class="col-sm-2">
                            <?php $inputOptions = ['class' => 'form-control muntin-id', 'prompt' => Yii::t('web', 'Select muntin')] ?>
                            <?php if(empty($form)): ?>
                                <?php $fieldId = "{$formName}-muntinId"; ?>

                                <div id="container-<?= $fieldId ?>" class="form-group <?= $orderCostForm->isAttributeRequired('muntinId') ? 'required' : '' ?>">
                                    <?= Html::activeDropDownList($orderCostForm, "[{$prefix}]muntinId", $muntins, $inputOptions + ['id' => "{$fieldId}"]) ?>
                                    <div class="help-block"></div>
                                </div>

                                <?php
                                $this->registerJs('
            	                    (function() {
            	                        $("#' . $formId . '").yiiActiveForm("add", {
            	                            id: "' . $fieldId . '",
            	                            name: "' . "[{$prefix}]muntinId" . '",
            	                            container: "#container-' . $fieldId . '",
            	                            input: "#' . "$fieldId" . '",
            	                            enableClientValidation: false,
            	                            enableAjaxValidation: true
            	                        });
            	                    })();
            	                    ', View::POS_END);
                                ?>
                            <?php else: ?>
                                <?= $form->field($orderCostForm, "[{$prefix}]muntinId")->dropDownList($muntins, $inputOptions) ?>
                            <?php endif ?>
                        </div>
                    </div>
                <?php } ?>
            </div>

            <div class="col-xs-12">
                <div class="row">
                    <div class="col-md-7">
                        <div class="row">
                            <div class="col-xs-12">
                                <h4><?= Yii::t('web', 'Product color') ?></h4>
                                <?=
                                Yii::$app->controller->renderPartial('/colour/ralColourPicker', [
                                    'prefix' => $prefix,
                                    'form' => $orderCostForm,
                                    'formName' => $formName,
                                    'modalType' => 'cost',
                                ])
                                ?>
                            </div>
                            <div class="col-xs-12">
                                <div class="row">
                                    <div class="col-xs-12">
                                        <?php $inputOptions = ['class' => 'form-control smallTextarea', 'placeholder' => $orderCostForm->getAttributeLabel('remarks')] ?>

                                        <?php if(empty($form)): ?>
                                            <?php $fieldId = "{$formName}-remarks"; ?>

                                            <div id="container-<?= $fieldId ?>" class="form-group <?= $orderCostForm->isAttributeRequired('remarks') ? 'required' : '' ?>">
                                                <?= Html::activeTextarea($orderCostForm, "[{$prefix}]remarks", $inputOptions + ['id' => "{$fieldId}"]) ?>
                                                <div class="help-block"></div>
                                            </div>

                                            <?php
                                            $this->registerJs('
                        	                    (function() {
                        	                        $("#' . $formId . '").yiiActiveForm("add", {
                        	                            id: "' . $fieldId . '",
                        	                            name: "' . "[{$prefix}]remarks" . '",
                        	                            container: "#container-' . $fieldId . '",
                        	                            input: "#' . "$fieldId" . '",
                        	                            enableClientValidation: false,
                        	                            enableAjaxValidation: true
                        	                        });
                        	                    })();
                        	                    ', View::POS_END);
                                            ?>
                                        <?php else: ?>
                                            <?php $field = $form->field($orderCostForm, "[{$prefix}]remarks")->textArea($inputOptions) ?>
                                            <?= Yii::$app->params['isOrderProductDimensionsVisible'] ? $field : $field->label(false) ?>
                                        <?php endif ?>
                                    </div>
                                    <div class="col-xs-12">
                                        <?php $inputOptions = ['class' => 'form-control smallTextarea', 'placeholder' => $orderCostForm->getAttributeLabel('remarks2')] ?>

                                        <?php if(empty($form)): ?>
                                            <?php $fieldId = "{$formName}-remarks2"; ?>

                                            <div id="container-<?= $fieldId ?>" class="form-group <?= $orderCostForm->isAttributeRequired('remarks2') ? 'required' : '' ?>">
                                                <?= Html::activeTextarea($orderCostForm, "[{$prefix}]remarks2", $inputOptions + ['id' => "{$fieldId}"]) ?>
                                                <div class="help-block"></div>
                                            </div>

                                            <?php
                                            $this->registerJs('
                        	                    (function() {
                        	                        $("#' . $formId . '").yiiActiveForm("add", {
                        	                            id: "' . $fieldId . '",
                        	                            name: "' . "[{$prefix}]remarks2" . '",
                        	                            container: "#container-' . $fieldId . '",
                        	                            input: "#' . "$fieldId" . '",
                        	                            enableClientValidation: false,
                        	                            enableAjaxValidation: true
                        	                        });
                        	                    })();
                        	                    ', View::POS_END);
                                            ?>
                                        <?php else: ?>
                                            <?php $field = $form->field($orderCostForm, "[{$prefix}]remarks2")->textArea($inputOptions) ?>
                                            <?= Yii::$app->params['isOrderProductDimensionsVisible'] ? $field : $field->label(false) ?>
                                        <?php endif ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <?php if(Yii::$app->params['isVisualisationVisible']) { ?>
                        <?=
                        Yii::$app->controller->renderPartial('inc/_visualisation', [
                            'photos' => $orderCostForm->photos,
                            'productForm' => $orderCostForm,
                            'form' => $form,
                            'productPrefix' => $prefix,
                            'typeProduct' => 'ordercostform'
                        ]);
                        ?>
                    <?php } ?>
                </div>
            </div>
        </div>
    </div>

    <div class="col-xs-1 text-right">
        <input type="button" class="base-icon copy-icon action-icon copy-item-btn-custom" data-index="<?= $prefix ?>">
        <div class="base-icon delete-icon action-icon remove-item-btn-custom"></div>
    </div>
    <?php if(Yii::$app->params['isProductAttributesVisible']) { ?>
        <?php
        $unfoldAttributes = $displayFirstAttributes || $productAttributeForm->isRowVisible == 1
        ?>

        <div class="col-xs-12 text-center">
            <div class="display-attributes-button" data-artificial="1" data-index=<?= $prefix ?> >
                <span class="glyphicon glyphicon-menu-<?= $unfoldAttributes ? 'up' : 'down' ?>"></span>&nbsp;
                <span class="text-down-up">
                    <?= mb_strtoupper(Yii::t('web', 'Attribute list'), 'UTF-8') ?>
                </span>
            </div>
        </div>
        <div class="attributes-row col-xs-12" id="attributes-row-<?= $prefix ?>"<?= !$unfoldAttributes ? 'style="display:none;"' : '' ?>>
            <?= $form->field($productAttributeForm, "[{$prefix}]isRowVisible")->hiddenInput(['class' => 'display-row-field', 'value' => $unfoldAttributes])->label(false); ?>
            <?php
            echo Yii::$app->controller->renderPartial('/inc/productAttributeForm', [
                'form' => $form,
                'model' => $productAttributeForm,
                'prefix' => $prefix,
            ]);
            ?>

        </div>
    <?php } ?>

    <?php
    if(Yii::$app->params['isAttachmentForProductVisible']) {
        echo Yii::$app->controller->renderPartial('inc/_productAttachment', [
            'files' => $orderCostForm->files,
            'productForm' => $orderCostForm,
            'form' => $form,
            'productPrefix' => $prefix
        ]);
    }
    ?>
    <?php if(Yii::$app->params['isOrderProductDimensionsVisible']) { ?>
        <div class="col-xs-12 focusable-btn-div copy-item-btn-custom" data-index="<?php echo $prefix ?>">
            <a class="btn btn-default tab-me copy-item-btn-custom" href="#" data-index="<?= $prefix ?>" id="cost-copy-btn-<?= $prefix ?>">
                <?= Yii::t('web', 'Copy product') ?>
            </a>
        </div>
    <?php } ?>
    <div class="col-xs-12">
        <div class="row">
            <hr>
        </div>
    </div>
</div>
<?php $this->registerJs('            	
  $(document).ready(function() {
  $(".regenerateVisualizations").click();
  });
  ', View::POS_END); ?>
