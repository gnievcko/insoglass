<?php 
use yii\helpers\Html;
use common\helpers\StringHelper;
?>
<div class="row">
	<?php foreach($entries as $entry) { ?>
    	<div class="col-xs-12 hist">
    		<div class="row">
		        <div class="col-xs-6"><?php
	    	    	$text = '';
	                	if(!empty($entry['firstName']) && !empty($entry['lastName'])) {
	                        $text = $entry['firstName'] . ' ' . $entry['lastName'];
	                    } else {
	                        $text = $entry['email'];
	                    }
	
	                    echo Html::mailto(Html::encode($text), $entry['email']);
				?></div>
	            <div class="col-xs-6 text-right"><?php echo Yii::t('web', 'Status') . ': <strong>' . $entry['status'] . '</strong>' ?></div>
	            <div class="col-xs-12 data"><?= Html::encode(StringHelper::getFormattedDateFromDate($entry['dateCreation'], true)) ?></div>
				<div class="col-xs-12"><?= nl2br(Html::encode($entry['message'])) ?></div>
			</div>
		</div>
	<?php } ?>
</div>