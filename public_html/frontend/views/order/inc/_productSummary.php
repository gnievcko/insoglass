<?php
use common\helpers\StringHelper;

if(Yii::$app->params['isOrderProductSummaryCommon']) { 
	if(Yii::$app->params['isOrderGrossValuesVisible']) {
		foreach($summary['vats'] as $vat) { ?>
	        <div class="col-xs-12">
	        	<div class="row">
		        	<div class="col-sm-1 col-sm-offset-7">
		            	<?= StringHelper::getFormattedCost($vat['netValue']). ' ' . $plnCurrency->short_symbol ?>
		            </div>
		            <div class="col-sm-1">
		            	<?= $vat['vatLabel'] ?>
		            </div>
		            <div class="col-sm-1">
		            	<?= StringHelper::getFormattedCost($vat['vatAmount']). ' ' . $plnCurrency->short_symbol ?>
		            </div>
		            <div class="col-sm-1">
		            	<?= StringHelper::getFormattedCost($vat['grossValue']). ' ' . $plnCurrency->short_symbol ?>
					</div>
				</div>
			</div>
		<?php } ?>
	<?php } ?>
	<div class="col-xs-12">
		<div class="row">
			<div class="col-sm-2 col-sm-offset-<?= Yii::$app->params['isOrderGrossValuesVisible'] ? 5 : 6 ?>">
		    	<h4><?= Yii::t('main', 'Total') ?>:</h4>
			</div>
		    <div class="col-sm-2 <?= Yii::$app->params['isOrderGrossValuesVisible'] ? '' : 'col-sm-offset-1' ?>">
		    	<h4 class="strong"><?= StringHelper::getFormattedCost($summary['total']['netValue']). ' ' . $plnCurrency->short_symbol ?></h4>
			</div>
			<?php if(Yii::$app->params['isOrderGrossValuesVisible']) { ?>
		    	<div class="col-sm-1 col-sm-offset-1 ">
		        	<?= StringHelper::getFormattedCost($summary['total']['vatAmount'] ). ' ' . $plnCurrency->short_symbol ?>
				</div>
		        <div class="col-sm-1">
		        	<?= StringHelper::getFormattedCost($summary['total']['grossValue']). ' ' . $plnCurrency->short_symbol ?>
				</div>
		    <?php } ?>
	    </div>
	</div>
<?php } ?>