<?php
use yii\helpers\Html;
use yii\helpers\Url;
use frontend\models\ProductTemporaryStorageForm;
use yii\web\View;

$productTemporaryStorageForm = new ProductTemporaryStorageForm();
?>

<div class="col-xs-12 gal-padding product-files" data-prefix="<?= $productPrefix ?>">
	<h4><?= Yii::t('web','Add attachments')?></h4>
	<div class="row">		
		<div class="col-sm-3">
			<div class="">		
			        <div class="file-upload-trigger">
			        	<div class="ajax-loader">
			            	<?= Html::img('@web/images/ajax-loader.gif', []) ?>
			            </div>
			            <div class="add-photo text-center btn-sm">
							<div class="icon-img add-pic"></div>
			                <div class="file-upload-title"><?= Yii::t('web', 'Add file') ?></div>
			            </div>
			            
			            <?= $form->field($productTemporaryStorageForm, 'file')->fileInput([
			                'accept' => ProductTemporaryStorageForm::getExtensionsString('.'),
			                'class' => 'product-files-upload',
			            ])->label(false) ?>
			            <div class="supp-form text-center"><?= Yii::t('web', 'Supported formats') ?>: <?= ProductTemporaryStorageForm::getExtensionsString() ?></div>
			        </div>
			</div>
		</div>
		<div class="col-sm-9">
		    <h5 class="text-center"><?= Yii::t('web', 'Files') ?></h5>
		    <div class="product-files-container" data-file-storage-url="<?= Url::to(['storage/upload']) ?>" data-document-form-url="<?= Url::to(['storage/get-product-file-form', 'form' => $productForm->formName()]) ?>"> 
		        <?php 
		            $prefix = 0;
		            foreach($files as $file) {
		                echo Yii::$app->controller->renderPartial('/inc/productFile', [
                            'prefix' => $prefix,
                            'productForm' => $productForm,
                            'file' => (object)$file,
                            'productPrefix' => $productPrefix
                        ]);
		                ++$prefix;
		            }
		        ?>
		    </div>
		    <div class="product-files-hint" style="display:<?= empty($files) ? 'block' : 'none' ?>">
		        <?= Html::tag('span', Yii::t('web', 'No attachments'), ['class' => 'hint-block text-center']) ?>
		    </div>
		</div>
	</div>
</div>

<?php $this->registerJs("
	window['removedFileSuccessMsg'] = '" . Yii::t('web', 'The attachment has been deleted.') . "';
	window['removedFileFailMsg'] = '" . Yii::t('web', 'The attachment cannot be deleted. Please contact the administrator.') . "';",
	View::POS_END);
?>