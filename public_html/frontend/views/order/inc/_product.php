<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\JsExpression;
use common\helpers\Utility;
use kartik\select2\Select2;
use yii\web\View;
use common\models\ar\Product;
use common\models\ar\OfferedProduct;
use common\helpers\StringHelper;
use common\widgets\RalColourPickerWidget;
?>

<?php
$formBaseName = $orderProductForm->formName();
$activeFormBaseName = strtolower($formBaseName);
$formName = "{$activeFormBaseName}-{$prefix}";
?>

<div class="item"> <!-- main container of product -->
    <div class="col-xs-11" data-form-base-name="<?= $formBaseName ?>" data-active-form-base-name="<?= $activeFormBaseName ?>"> <!-- contains forms -->
        <div class="row">
            <div class="col-xs-12">
                <div class="row">
                    <div class="col-sm-5">
                        <div class="row">
                            <div class="col-xs-12">
                                <?php
                                $productData = [];
                                if($orderProductForm->productType == Utility::PRODUCT_TYPE_PRODUCT) {
                                    $product = Product::findOne($orderProductForm->productId);
                                    if(!empty($product)) {
                                        $productTranslation = $product->getTranslation();
                                        $productName = !empty($productTranslation) ? $productTranslation->name : $product->symbol;
                                        if(!empty($product->parentProduct)) {
                                            $parentProductTranslation = $product->parentProduct->getTranslation();
                                            $parentProductName = !empty($parentProductTranslation) ? $parentProductTranslation->name : $product->parentProduct->symbol;
                                            $productName = $parentProductName . ' (' . $productName . ')';
                                        }

                                        $productData = [[$product->id => $productName]];
                                    }
                                } else if($orderProductForm->productType == Utility::PRODUCT_TYPE_OFFERED_PRODUCT) {
                                    $product = OfferedProduct::findOne($orderProductForm->productId);
                                    if(!empty($product)) {
                                        $productTranslation = $product->getTranslation();
                                        $productName = !empty($productTranslation) ? $productTranslation->name : $product->symbol;
                                        if(!empty($product->parentOfferedProduct)) {
                                            $parentProductTranslation = $product->parentOfferedProduct->getTranslation();
                                            $parentProductName = !empty($parentProductTranslation) ? $parentProductTranslation->name : $product->parentOfferedProduct->symbol;
                                            $productName = $parentProductName . ' (' . $productName . ')';
                                        }

                                        $productData = [[$product->id => $productName]];
                                    }
                                }

                                $select2Config = [
                                    'language' => Yii::$app->language,
                                    'showToggleAll' => false,
                                    'options' => ['placeholder' => Yii::t('main', 'Select a product...')],
                                    'data' => $productData,
                                    'pluginOptions' => [
                                        'allowClear' => true,
                                        'minimumInputLength' => 1,
                                        'ajax' => [
                                            'url' => Url::to(['offered-product/find']),
                                            'dataType' => 'json',
                                            'delay' => 250,
                                            'data' => new JsExpression('function(params) {
                    	                                        return {term:params.term, page:params.page, size: 20};
                    	                                    }'),
                                            'processResults' => new JsExpression('function (data, params) {
                    	                                        params.page = params.page || 1;
                    	
                    	                                        return {
                    	                                            results: data.items,
                    	                                            pagination: {
                    	                                                more: data.more
                    	                                            }
                    	                                        };
                    	                                    }'),
                                            'cache' => true
                                        ],
                                        'templateResult' => new JsExpression('function(item) { if(item.loading) return item.text; return item.name; }'),
                                        'templateSelection' => new JsExpression('function (item) { return item.name || item.text; }'),
                                    ],
                                    'pluginEvents' => [
                                        'change' => 'function() {
                    	                            var $thisElement = $(this);
                    	                            var selectedItemData = $thisElement.select2("data");
                    	                            if(selectedItemData && selectedItemData[0]) {
                                                        var id = selectedItemData[0]["id"];
                                                        var $productDiv = $thisElement.closest(".item");
                                                        var rowId = $productDiv.find(".copy-item-btn-custom").attr("data-index");
                                                        var jqxhr = $.get("' . Url::to(['offered-product/find-attachments-ids']) . '", {productId: id}, function(data) {
                    
                                                            $.each( $("input[id^=\'productattributeform-" + rowId + "-fields-\']"), function(){
                                                                $(this).prop("checked", false);
                                                            });
                    
                                                            $.each(JSON.parse(data), function(value){
                                                                $("#productattributeform-" + rowId + "-fields-" + value).prop("checked", true);
                                                            });
                    
                    
                        	                                $productDiv.find(".product-type:first").val(selectedItemData[0].type);
                        	                                $productDiv.find(".price:first").val(selectedItemData[0].price);
                                                            $productDiv.find(".construction-id:first").val(selectedItemData[0].constructionId);
                                                            $productDiv.find(".shape-id:first").val(selectedItemData[0].shapeId);
                                                            $productDiv.find(".muntin-id:first").val(selectedItemData[0].muntinId);
                                                            $productDiv.find(".height:first").val(selectedItemData[0].height);
                                                            $productDiv.find(".width:first").val(selectedItemData[0].width);
                                                                                                
                        	                    			if(selectedItemData[0].unit != null) {
                        	                    				$productDiv.find(".unit:first").val(selectedItemData[0].unit);
                        	                    			}
                        	                    			else {
                        	                    				$productDiv.find(".unit:first").val("' . mb_strtolower(!empty(Yii::$app->params['defaultUnit']) ? Yii::t('documents', Yii::$app->params['defaultUnit']) : Yii::t('web', 'Unit'), 'UTF-8') . '");
                        	                    			}
                        	                                if(selectedItemData[0].currency_id != null) $productDiv.find(".currency:first").val(selectedItemData[0].currency_id);
                                                            $productDiv.find(".price:first").change();
                    
                                                        });
                                                       
                    	                                
                    	                            }
                    	                        }',
                                    ]
                                ];
                                ?>

                                <?php if(empty($form)): ?>
                                    <?php $fieldId = "{$formName}-productid"; ?>

                                    <div id="container-<?= $fieldId ?>" class="form-group <?= $orderProductForm->isAttributeRequired('productId') ? 'required' : '' ?>">
                                        <?php
                                        $select2Config['model'] = $orderProductForm;
                                        $select2Config['attribute'] = "[{$prefix}]productId";
                                        echo Select2::widget($select2Config);

                                        $this->registerJs('
                    	                            (function() {
                    	                                $("#' . $formId . '").yiiActiveForm("add", {
                    	                                    id: "' . $fieldId . '",
                    	                                    name: "' . "[{$prefix}]productId" . '",
                    	                                    container: "#container-' . $fieldId . '",
                    	                                    input: "#' . "$fieldId" . '",
                    	                                    enableClientValidation: false,
                    	                                    enableAjaxValidation: true
                    	                                });
                    	                            })();
                    	                            ', View::POS_END);
                                        ?>

                                        <div class="help-block"></div>
                                    </div>
                                <?php else: ?>
                                    <?php $field = $form->field($orderProductForm, "[{$prefix}]productId")->widget(Select2::classname(), $select2Config); ?>
                                    <?= Yii::$app->params['isOrderProductDimensionsVisible'] ? $field : $field->label(false) ?>
                                <?php endif ?>
                            </div>
                            <div class="col-xs-12">
                                <a href="<?= Url::to(['offered-product/create']); ?>" target="_blank" class="focusable-link  add-item-after-select2 add-item-after-select2-no-required">
                                    <span class="glyphicon glyphicon-plus-sign pull-left"></span>
                                    <span> <?= Yii::t('web', 'Create new product') ?></span>
                                </a>
                            </div>
                        </div>

                    </div>

                    <div class="col-sm-<?= Yii::$app->params['isOrderGrossValuesVisible'] ? 1 : 2 ?>">
                        <?php $inputOptions = ['type' => 'number', 'class' => 'form-control tab-me', 'min' => 0, 'step' => 1, 'integer' => true, 'placeholder' => Yii::t('web', 'Number of pieces')] ?>
                        <?php if(empty($form)): ?>
                            <?php $fieldId = "{$formName}-count"; ?>

                            <div id="container-<?= $fieldId ?>" class="form-group <?= $orderProductForm->isAttributeRequired('count') ? 'required' : '' ?>">
                                <?= Html::activeTextInput($orderProductForm, "[{$prefix}]count", $inputOptions + ['class' => 'form-control', 'id' => "{$fieldId}"]) ?>
                                <div class="help-block"></div>
                            </div>

                            <?php
                            $this->registerJs('
                	                      (function() {
                	                          $("#' . $formId . '").yiiActiveForm("add", {
                	                              id: "' . $fieldId . '",
                	                              name: "' . "[{$prefix}]count" . '",
                	                              container: "#container-' . $fieldId . '",
                	                              input: "#' . "$fieldId" . '",
                	                              enableClientValidation: false,
                	                              enableAjaxValidation: true
                	                          });
                	                      })();
                	                      ', View::POS_END);
                            ?>
                        <?php else: ?>
                            <?php $field = $form->field($orderProductForm, "[{$prefix}]count")->textInput($inputOptions) ?>
                            <?= Yii::$app->params['isOrderProductDimensionsVisible'] ? $field : $field->label(false) ?>
                        <?php endif ?>

                    </div>
                    <?php if(!Yii::$app->params['isOrderProductDimensionsVisible']) { ?>
                        <div class="col-sm-1">

                            <?php $inputOptions = ['type' => 'text', 'class' => 'form-control unit', 'value' => !empty(Yii::$app->params['defaultUnit']) ? Yii::t('documents', Yii::$app->params['defaultUnit']) : Yii::t('web', 'Unit'), 'readonly' => true] ?>
                            <?php if(empty($form)): ?>
                                <?php $fieldId = "{$formName}-unit"; ?>

                                <div id="container-<?= $fieldId ?>" class="form-group <?= $orderProductForm->isAttributeRequired('unit') ? 'required' : '' ?>">
                                    <?= Html::activeTextInput($orderProductForm, "[{$prefix}]unit", $inputOptions + ['id' => "{$fieldId}"]) ?>
                                    <div class="help-block"></div>
                                </div>

                                <?php
                                $this->registerJs('
                	                    (function() {
                	                        $("#' . $formId . '").yiiActiveForm("add", {
                	                            id: "' . $fieldId . '",
                	                            name: "' . "[{$prefix}]unit" . '",
                	                            container: "#container-' . $fieldId . '",
                	                            input: "#' . "$fieldId" . '",
                	                            enableClientValidation: false,
                	                            enableAjaxValidation: true
                	                        });
                	                    })();
                	                    ', View::POS_END);
                                ?>
                            <?php else: ?>
                                <?= $form->field($orderProductForm, "[{$prefix}]unit")->textInput($inputOptions)->label(false) ?>
                            <?php endif ?>
                        </div>
                    <?php } ?>
                    <div class="col-sm-<?= Yii::$app->params['isOrderGrossValuesVisible'] ? 1 : 2 ?>">

                        <?php $inputOptions = ['type' => 'number', 'step' => 0.01, 'class' => 'form-control price', 'placeholder' => $orderProductForm->getAttributeLabel('price')] ?>
                        <?php if(empty($form)): ?>
                            <?php $fieldId = "{$formName}-price"; ?>

                            <div id="container-<?= $fieldId ?>" class="form-group <?= $orderProductForm->isAttributeRequired('price') ? 'required' : '' ?>">
                                <?= Html::activeTextInput($orderProductForm, "[{$prefix}]price", $inputOptions + ['id' => "{$fieldId}"]) ?>
                                <div class="help-block"></div>
                            </div>

                            <?php
                            $this->registerJs('
                	                            (function() {
                	                                $("#' . $formId . '").yiiActiveForm("add", {
                	                                    id: "' . $fieldId . '",
                	                                    name: "' . "[{$prefix}]price" . '",
                	                                    container: "#container-' . $fieldId . '",
                	                                    input: "#' . "$fieldId" . '",
                	                                    enableClientValidation: false,
                	                                    enableAjaxValidation: true
                	                                });
                	                            })();
                	                            ', View::POS_READY);
                            ?>
                        <?php else: ?>
                            <?php $field = $form->field($orderProductForm, "[{$prefix}]price")->textInput($inputOptions) ?>
                            <?= Yii::$app->params['isOrderProductDimensionsVisible'] ? $field->label($orderProductForm->getAttributeLabel('price')) : $field->label(false) ?>
                        <?php endif ?>

                    </div>
                    <div class="col-sm-1" style="display:none">
                        <?php
                        $currencyOptions = ['class' => 'form-control currency'];
                        //$currencies = ['' => \Yii::t('web', 'Choose currency...')] + $currencies;
                        ?>
                        <?php if(empty($form)): ?>
                            <?php
                            $fieldId = "{$formName}-currencyid";
                            ?>

                            <div id="container-<?= $fieldId ?>" class="form-group <?= $orderProductForm->isAttributeRequired('currencyId') ? 'required' : '' ?>">
                                <?= Html::activeDropDownList($orderProductForm, "[{$prefix}]currencyId", $currencies, $currencyOptions + ['id' => "{$fieldId}"]) ?>
                                <div class="help-block"></div>
                            </div>

                            <?php
                            $this->registerJs('
                	                            (function() {
                	                                $("#' . $formId . '").yiiActiveForm("add", {
                	                                    id: "' . $fieldId . '",
                	                                    name: "' . "[{$prefix}]currencyId" . '",
                	                                    container: "#container-' . $fieldId . '",
                	                                    input: "#' . "$fieldId" . '",
                	                                    enableClientValidation: false,
                	                                    enableAjaxValidation: true
                	                                });
                	                            })();
                	                            ', View::POS_END);
                            ?>
                        <?php else: ?>
                            <?= $form->field($orderProductForm, "[{$prefix}]currencyId")->dropDownList($currencies, $currencyOptions)->label('', ['style' => 'visibility: hidden'])->label(false) ?>
                        <?php endif ?>
                    </div>

                    <div class="col-sm-<?= Yii::$app->params['isOrderGrossValuesVisible'] ? 1 : 2 ?>">
                        <div class="row">
                            <?php if(Yii::$app->params['isOrderProductDimensionsVisible']) { ?>
                                <div class="col-xs-12">
                                    <label class="control-label"><?= Yii::t('web', 'Net value') ?></label>
                                </div>
                            <?php } ?>
                            <div class="col-xs-12">
                                <?= StringHelper::getFormattedCost($orderProductForm->netValue) . ' ' . $plnCurrency->short_symbol ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-xs-12">
                <div class="row">
                    <?php if(Yii::$app->params['isOrderGrossValuesVisible']) { ?>
                        <div class="col-sm-1">
                            <?php if(empty($form)): ?>
                                <?php $fieldId = "{$formName}-vatRate"; ?>

                                <div id="container-<?= $fieldId ?>" class="form-group <?= $orderProductForm->isAttributeRequired('vatRate') ? 'required' : '' ?>">
                                    <?= Html::activeDropDownList($orderProductForm, "[{$prefix}]vatRate", Yii::$app->params['vatRate'], ['id' => "{$fieldId}", 'class' => 'form-control currency']) ?>
                                    <div class="help-block"></div>
                                </div>

                                <?php
                                $this->registerJs('
											(function() {
												$("#' . $formId . '").yiiActiveForm("add", {
													id: "' . $fieldId . '",
													name: "' . "[{$prefix}]vatRate" . '",
													container: "#container-' . $fieldId . '",
													input: "#' . "$fieldId" . '",
													enableClientValidation: false,
													enableAjaxValidation: true
												});
											})();
											', View::POS_END);
                                ?>
                            <?php else: ?>
                                <?php $field = $form->field($orderProductForm, "[{$prefix}]vatRate")->dropDownList(Yii::$app->params['vatRate']) ?>
                                <?= Yii::$app->params['isOrderProductDimensionsVisible'] ? $field : $field->label(false) ?>
                            <?php endif ?>
                        </div>
                        <div class="col-sm-1">
                            <?php if(Yii::$app->params['isOrderProductDimensionsVisible']) { ?>
                                <label class="control-label"><?= Yii::t('web', 'Vat amount') ?></label>
                            <?php } ?>
                            <?= StringHelper::getFormattedCost($orderProductForm->vatAmount) . ' ' . $plnCurrency->short_symbol ?>
                        </div>
                        <div class="col-sm-1">
                            <?php if(Yii::$app->params['isOrderProductDimensionsVisible']) { ?>
                                <label class="control-label"><?= Yii::t('web', 'Gross value') ?></label>
                            <?php } ?>
                            <?= StringHelper::getFormattedCost($orderProductForm->grossValue) . ' ' . $plnCurrency->short_symbol ?>
                        </div>
                        <?php
                    } else {
                        ?>
                        <div class="col-sm-1" style="display: none">
                            <?php $field = $form->field($orderProductForm, "[{$prefix}]vatRate")->hiddenInput() ?>
                            <?= Yii::$app->params['isOrderProductDimensionsVisible'] ? $field : $field->label(false) ?>
                        </div>
                    <?php } ?>
                </div>
            </div>
            <div class="col-xs-12">
                <?php if(Yii::$app->params['isOrderProductDimensionsVisible']) { ?>
                    <div class="row">   
                        <div class="col-sm-2">
                            <?php $inputOptions = ['type' => 'text', 'class' => 'form-control width tab-me', 'placeholder' => Yii::t('web', 'Width') . ' [mm]'] ?>
                            <?php if(empty($form)): ?>
                                <?php $fieldId = "{$formName}-width"; ?>
                                <div id="container-<?= $fieldId ?>" class="form-group <?= $orderProductForm->isAttributeRequired('width') ? 'required' : '' ?>">
                                    <?= Html::activeTextInput($orderProductForm, "[{$prefix}]width", $inputOptions + ['id' => "{$fieldId}"]) ?>
                                    <div class="help-block"></div>
                                </div>

                                <?php
                                $this->registerJs('
									(function() {
										$("#' . $formId . '").yiiActiveForm("add", {
											id: "' . $fieldId . '",
											name: "' . "[{$prefix}]width" . '",
											container: "#container-' . $fieldId . '",
											input: "#' . "$fieldId" . '",
											enableClientValidation: false,
											enableAjaxValidation: true
										});
									})();
									', View::POS_END);
                                ?>
                            <?php else: ?>
                                <?= $form->field($orderProductForm, "[{$prefix}]width")->textInput($inputOptions) ?>
                            <?php endif ?>
                        </div>
                        <div class="col-sm-2">
                            <?php $inputOptions = ['type' => 'text', 'class' => 'form-control height tab-me', 'placeholder' => Yii::t('web', 'Height') . ' [mm]'] ?>
                            <?php if(empty($form)): ?>
                                <?php $fieldId = "{$formName}-height"; ?>

                                <div id="container-<?= $fieldId ?>" class="form-group <?= $orderProductForm->isAttributeRequired('height') ? 'required' : '' ?>">
                                    <?= Html::activeTextInput($orderProductForm, "[{$prefix}]height", $inputOptions + ['id' => "{$fieldId}"]) ?>
                                    <div class="help-block"></div>
                                </div>

                                <?php
                                $this->registerJs('
									(function() {
										$("#' . $formId . '").yiiActiveForm("add", {
											id: "' . $fieldId . '",
											name: "' . "[{$prefix}]height" . '",
											container: "#container-' . $fieldId . '",
											input: "#' . "$fieldId" . '",
											enableClientValidation: false,
											enableAjaxValidation: true
										});
									})();
									', View::POS_END);
                                ?>
                            <?php else: ?>
                                <?= $form->field($orderProductForm, "[{$prefix}]height")->textInput($inputOptions) ?>
                            <?php endif ?>
                        </div>
                        <div class="col-sm-3">
                            <?php $inputOptions = ['class' => 'form-control shape-id', 'prompt' => Yii::t('web', 'Select shape')] ?>
                            <?php if(empty($form)): ?>
                                <?php $fieldId = "{$formName}-shapeId"; ?>
                                <div id="container-<?= $fieldId ?>" class="form-group <?= $orderProductForm->isAttributeRequired('shapeId') ? 'required' : '' ?>">
                                    <?= Html::activeDropDownList($orderProductForm, "[{$prefix}]shapeId", $shapes, $inputOptions + ['id' => "{$fieldId}"]) ?>
                                    <div class="help-block"></div>
                                </div>
                                <?php
                                $this->registerJs('
									(function() {
										$("#' . $formId . '").yiiActiveForm("add", {
											id: "' . $fieldId . '",
											name: "' . "[{$prefix}]shapeId" . '",
											container: "#container-' . $fieldId . '",
											input: "#' . "$fieldId" . '",
											enableClientValidation: false,
											enableAjaxValidation: true
										});
									})();
									', View::POS_END);
                                ?>
                            <?php else: ?>
                                <?= $form->field($orderProductForm, "[{$prefix}]shapeId")->dropDownList($shapes, $inputOptions) ?>
                            <?php endif ?>
                        </div>
                        <div class="col-sm-3">
                            <?php $inputOptions = ['class' => 'form-control construction-id', 'prompt' => Yii::t('web', 'Select construction')] ?>
                            <?php if(empty($form)): ?>
                                <?php $fieldId = "{$formName}-constructionId"; ?>
                                <div id="container-<?= $fieldId ?>" class="form-group <?= $orderProductForm->isAttributeRequired('constructionId') ? 'required' : '' ?>">
                                    <?= Html::activeDropDownList($orderProductForm, "[{$prefix}]constructionId", $constructions, $inputOptions + ['id' => "{$fieldId}"]) ?>
                                    <div class="help-block"></div>
                                </div>
                                <?php
                                $this->registerJs('
									(function() {
										$("#' . $formId . '").yiiActiveForm("add", {
											id: "' . $fieldId . '",
											name: "' . "[{$prefix}]constructionId" . '",
											container: "#container-' . $fieldId . '",
											input: "#' . "$fieldId" . '",
											enableClientValidation: false,
											enableAjaxValidation: true
										});
									})();
									', View::POS_END);
                                ?>
                            <?php else: ?>
                                <?= $form->field($orderProductForm, "[{$prefix}]constructionId")->dropDownList($constructions, $inputOptions) ?>
                            <?php endif ?>
                        </div>
                        <div class="col-sm-2">
                            <?php $inputOptions = ['class' => 'form-control muntin-id', 'prompt' => Yii::t('web', 'Select muntin')] ?>
                            <?php if(empty($form)): ?>
                                <?php $fieldId = "{$formName}-muntinId"; ?>
                                <div id="container-<?= $fieldId ?>" class="form-group <?= $orderProductForm->isAttributeRequired('muntinId') ? 'required' : '' ?>">
                                    <?= Html::activeDropDownList($orderProductForm, "[{$prefix}]muntinId", $muntins, $inputOptions + ['id' => "{$fieldId}"]) ?>
                                    <div class="help-block"></div>
                                </div>
                                <?php
                                $this->registerJs('
									(function() {
										$("#' . $formId . '").yiiActiveForm("add", {
											id: "' . $fieldId . '",
											name: "' . "[{$prefix}]muntinId" . '",
											container: "#container-' . $fieldId . '",
											input: "#' . "$fieldId" . '",
											enableClientValidation: false,
											enableAjaxValidation: true
										});
									})();
									', View::POS_END);
                                ?>
                            <?php else: ?>
                                <?= $form->field($orderProductForm, "[{$prefix}]muntinId")->dropDownList($muntins, $inputOptions) ?>
                            <?php endif ?>
                        </div>
                    </div>
                </div>

                <div class="col-xs-12">
                    <div class="row">
                        <div class="col-md-7">
                            <div class="row">
                                <div class="col-xs-12"> <!-- color picker -->
                                    <h4><?= Yii::t('web', 'Product color') ?></h4>
                                    <?=
                                    Yii::$app->controller->renderPartial('/colour/ralColourPicker', [
                                        'prefix' => $prefix,
                                        'form' => $orderProductForm,
                                        'formName' => $formName,
                                        'modalType' => 'product',
                                    ])
                                    ?>
                                </div>
                                <div class="col-xs-12">	<!-- remarks text area -->
                                    <div class="row">
                                        <div class="col-xs-12">
                                            <?php $inputOptions = ['class' => 'form-control smallTextarea', 'placeholder' => $orderProductForm->getAttributeLabel('remarks')] ?>

                                            <?php if(empty($form)): ?>
                                                <?php $fieldId = "{$formName}-remarks"; ?>

                                                <div id="container-<?= $fieldId ?>" class="form-group <?= $orderProductForm->isAttributeRequired('remarks') ? 'required' : '' ?>">
                                                    <?= Html::activeTextarea($orderProductForm, "[{$prefix}]remarks", $inputOptions + ['id' => "{$fieldId}"]) ?>
                                                    <div class="help-block"></div>
                                                </div>

                                                <?php
                                                $this->registerJs('
                									(function() {
                										$("#' . $formId . '").yiiActiveForm("add", {
                											id: "' . $fieldId . '",
                											name: "' . "[{$prefix}]remarks" . '",
                											container: "#container-' . $fieldId . '",
                											input: "#' . "$fieldId" . '",
                											enableClientValidation: false,
                											enableAjaxValidation: true
                										});
                									})();
                									', View::POS_END);
                                                ?>
                                            <?php else: ?>
                                                <?php $field = $form->field($orderProductForm, "[{$prefix}]remarks")->textArea($inputOptions) ?>
                                                <?= Yii::$app->params['isOrderProductDimensionsVisible'] ? $field : $field->label(false) ?>
                                            <?php endif ?>
                                        </div>
                                        <div class="col-xs-12">
                                            <?php $inputOptions = ['class' => 'form-control smallTextarea', 'placeholder' => $orderProductForm->getAttributeLabel('remarks2')] ?>

                                            <?php if(empty($form)): ?>
                                                <?php $fieldId = "{$formName}-remarks2"; ?>

                                                <div id="container-<?= $fieldId ?>" class="form-group <?= $orderProductForm->isAttributeRequired('remarks2') ? 'required' : '' ?>">
                                                    <?= Html::activeTextarea($orderProductForm, "[{$prefix}]remarks2", $inputOptions + ['id' => "{$fieldId}"]) ?>
                                                    <div class="help-block"></div>
                                                </div>

                                                <?php
                                                $this->registerJs('
                									(function() {
                										$("#' . $formId . '").yiiActiveForm("add", {
                											id: "' . $fieldId . '",
                											name: "' . "[{$prefix}]remarks2" . '",
                											container: "#container-' . $fieldId . '",
                											input: "#' . "$fieldId" . '",
                											enableClientValidation: false,
                											enableAjaxValidation: true
                										});
                									})();
                									', View::POS_END);
                                                ?>
                                            <?php else: ?>
                                                <?php $field = $form->field($orderProductForm, "[{$prefix}]remarks2")->textArea($inputOptions) ?>
                                                <?= Yii::$app->params['isOrderProductDimensionsVisible'] ? $field : $field->label(false) ?>
                                            <?php endif ?>
                                        </div>
                                    </div>
                                </div>
                            </div>	
                        </div>
                        <?php if(Yii::$app->params['isVisualisationVisible']) { ?>
                            <?=
                            Yii::$app->controller->renderPartial('inc/_visualisation', [
                                'photos' => $orderProductForm->photos,
                                'productForm' => $orderProductForm,
                                'form' => $form,
                                'productPrefix' => $prefix,
                                'typeProduct' => 'orderproductform'
                            ]);
                            ?>
                        <?php } ?>
                    </div>
                </div>

            <?php } ?>
            <?= Html::activeHiddenInput($orderProductForm, "[{$prefix}]productType", ['class' => 'product-type']) ?>
        </div>
    </div>

    <div class="col-xs-1"> <!-- contains copy and delete icons -->
        <?php if(Yii::$app->params['isOrderProductDimensionsVisible']) { ?>
            <input type="button" class="base-icon copy-icon action-icon copy-item-btn-custom" data-index="<?= $prefix ?>">
        <?php } ?>
        <input type="button" class="base-icon delete-icon action-icon remove-item-btn-custom">
    </div>

    <?php if(Yii::$app->params['isProductAttributesVisible']) { ?>
        <?php $unfoldAttributes = $displayFirstAttributes || $productAttributeForm->isRowVisible == 1 ?> 
        <div class="col-xs-12 text-center">
            <div class="display-attributes-button" data-index=<?= $prefix ?>>
                <span class="glyphicon glyphicon-menu-<?= $unfoldAttributes ? 'up' : 'down' ?>"></span>&nbsp;
                <span class="text-down-up">
                    <?= mb_strtoupper(Yii::t('web', 'Attribute list'), 'UTF-8') ?>
                </span>
            </div>
        </div>

        <div class="attributes-row col-xs-12" id="attributes-row-<?= $prefix ?>"<?= !$unfoldAttributes ? 'style="display:none;"' : '' ?>>
            <?= $form->field($productAttributeForm, "[{$prefix}]isRowVisible")->hiddenInput(['class' => 'display-row-field', 'value' => $unfoldAttributes])->label(false); ?>
            <?php
            echo Yii::$app->controller->renderPartial('/inc/productAttributeForm', [
                'form' => $form,
                'model' => $productAttributeForm,
                'prefix' => $prefix,
            ]);
            ?>

        </div>
    <?php } ?>

    <?php
    if(Yii::$app->params['isAttachmentForProductVisible']) {
        echo Yii::$app->controller->renderPartial('inc/_productAttachment', [
            'files' => $orderProductForm->files,
            'productForm' => $orderProductForm,
            'form' => $form,
            'productPrefix' => $prefix
        ]);
    }
    ?>
    <?php if(Yii::$app->params['isOrderProductDimensionsVisible']) { ?>
        <div class="col-xs-12 focusable-btn-div">
            <a class="btn btn-default tab-me copy-item-btn-custom" href="#" data-index="<?= $prefix ?>" id="product-copy-btn-<?= $prefix ?>">
                <?= Yii::t('web', 'Copy product') ?>
            </a>
        </div>
    <?php } ?>
    <div class="col-xs-12">
        <div class="row">
            <hr>
        </div>
    </div>
</div>

<?php $this->registerJs('            	
    $(document).ready(function() {
        $(".regenerateVisualizations").click();
    });
', View::POS_END); ?>
