<?php

use yii\helpers\Html;
use yii\widgets\LinkPager;
use common\helpers\StringHelper;
use common\widgets\TableHeaderWidget;
use frontend\models\TaskObjectListForm;
use yii\helpers\Url;
use frontend\helpers\UserHelper;
?>

<?php if(empty($items)) { ?>
    <label><?= Yii::t('web', 'No search results'); ?></label>
    <br><br>					
<?php } else {
    ?>
    <div class="table-responsive col-sm-12">
        <table class="<?= TableHeaderWidget::TABLECLASS ?>">
            <?php echo TableHeaderWidget::widget(['fields' => TaskObjectListForm::getSortFields(), 'listForm' => $listForm, 'action' => true]); ?>
            <tbody>
                <?php foreach($items as $item): ?>
                    <tr <?= !empty($item['isComplete']) ? 'class="complete"' : '' ?>>
                        <td><?= Html::encode($item['title']) ?></td>
                        <td><?= Html::encode($item['taskTypeName']) ?></td>
                        <td><?=
                            Html::encode(!empty($item['assignedId']) ? UserHelper::getPrettyUserName($item['assignedEmail'], $item['assignedFirstName'], $item['assignedLastName']) : UserHelper::getPrettyUserName($item['authorEmail'], $item['authorFirstName'], $item['authorLastName'])
                            )
                            ?></td>
                        <td><?= Html::encode($item['message']) ?></td>
                        <td><?= Html::encode($item['taskPriorityName']) ?></td>
                        <td><?= StringHelper::getFormattedDateFromDate($item['dateCreation'], false) ?></td>
                        <td><?= StringHelper::getFormattedDateFromDate($item['dateDeadline'], false) ?></td>
                        <td><?php
                            if(empty($item['isComplete'])) {
                                echo Html::a('', Url::to(['task/edit', 'id' => $item['id']]), ['class' => 'base-icon editing-icon action-icon', 'title' => Yii::t('web', 'Edit')]);
                                echo '&nbsp;' . Html::a('', Url::to(['task/complete']), [
                                    'class' => 'base-icon ok-icon action-icon',
                                    'title' => Yii::t('web', 'Mark task as completed'),
                                    'data' => [
                                        'confirm' => Yii::t('web', 'Are you sure you want to mark this task as complete?'),
                                        'method' => 'post',
                                        'params' => [
                                            'id' => $item['id'],
                                        ],
                                    ],
                                ]);
                                echo '&nbsp;' . Html::a('', Url::to(['task/delete']), [
                                    'class' => 'base-icon delete-icon action-icon',
                                    'title' => Yii::t('main', 'Delete'),
                                    'data' => [
                                        'confirm' => Yii::t('main', 'Are you sure you want to delete this item?'),
                                        'method' => 'post',
                                        'params' => [
                                            'id' => $item['id'],
                                        ],
                                    ],
                                ]);
                            } else {
                                echo Html::a('', Url::to(['task/delete']), [
                                    'class' => 'base-icon delete-icon action-icon',
                                    'title' => Yii::t('main', 'Delete'),
                                    'data' => [
                                        'confirm' => Yii::t('main', 'Are you sure you want to delete this item?'),
                                        'method' => 'post',
                                        'params' => [
                                            'id' => $item['id'],
                                        ],
                                    ],
                                ]);
                            }
                            ?></td>
                    </tr>
                <?php endforeach ?>
            </tbody>
        </table>
    </div>
    <div class="col-sm-12">
        <div class="col-sm-8 text-right dynamic-table-pagination">
            <?= LinkPager::widget(['pagination' => $pages]) ?>
        </div>
    </div>	
<?php } ?>

<input class="dynamic-table-sort-dir" type="hidden" name="<?= Html::getInputName($listForm, 'sortDir') ?>" value="<?= $listForm->sortDir ?>" />
<input class="dynamic-table-sort-field" type="hidden" name="<?= Html::getInputName($listForm, 'sortField') ?>" value="<?= $listForm->sortField ?>" />
<input class="dynamic-table-page" type="hidden" name="<?= Html::getInputName($listForm, 'page') ?>" value="<?= $listForm->page ?>" />


