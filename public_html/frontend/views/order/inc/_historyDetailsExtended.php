<?php 
use yii\helpers\Html;
use common\helpers\StringHelper;
?>
<div class="row">
    <?php foreach($entries as $entry) { ?>
        <div class="col-sm-12 hist">
            <div class="row">
        	    <div class="col-xs-6"><?php
                	$text = '';
                       	if(!empty($entry['firstName']) && !empty($entry['lastName'])) {
                            $text = $entry['firstName'] . ' ' . $entry['lastName'];
                        } else {
                            $text = $entry['email'];
                        }
        
                        echo Html::mailto(Html::encode($text), $entry['email']);
        		?></div>
                <div class="col-xs-6 text-right"><?php echo Yii::t('web', 'Status') . ': <strong>' . $entry['status'] . '</strong>' ?></div>
                <div class="col-xs-12 data"><?= Html::encode(StringHelper::getFormattedDateFromDate($entry['dateCreation'], true)) ?></div>
        		<div class="col-xs-12"><?= nl2br(Html::encode($entry['message'])) ?></div>
        		
        		<?php if(!empty($entry['modification']['products']['removed'])) {
        			echo $this->render('_historyDetails_productTable', [
        					'products' => $entry['modification']['products']['removed'],
        					'text' => 'Removed products'
        			]);
        		}?>
        		
        		<?php if(!empty($entry['modification']['products']['added'])) {
        			echo $this->render('_historyDetails_productTable', [
        					'products' => $entry['modification']['products']['added'],
        					'text' => 'Added products',
        			]);
        		}?>
        		
        		<?php if(!empty($entry['modification']['history']['date_reminder'])) {?>
        		<div class="col-xs-12">
        			<?php echo Yii::t('main', 'Reminder date') . ': ' . $entry['modification']['history']['date_reminder']['old'] . ' --> ' . $entry['modification']['history']['date_reminder']['new']?>
        		</div>
        		<?php } ?>
        		
        		<?php if(!empty($entry['modification']['history']['date_deadline'])) {?>
        		<div class="col-xs-12">
        			<?php echo Yii::t('main', 'Deadline date') . ': ' . $entry['modification']['history']['date_deadline']['old'] . ' --> ' . $entry['modification']['history']['date_deadline']['new']?>
        		</div>
        		<?php } ?>
        		
        		<?php if(!empty($entry['modification']['history']['date_shipping'])) {?>
        		<div class="col-xs-12">
        			<?php echo Yii::t('web', 'Shipping date') . ': ' . $entry['modification']['history']['date_shipping']['old'] . ' --> ' . $entry['modification']['history']['date_shipping']['new']?>
        		</div>
        		<?php } ?>
        		
        		<?php if(!empty($entry['modification']['userAssignment'])) {?>
        		<div class="col-xs-12">
        			<table class="table">
        				<thead>
        					<tr>
        		                <th><?= Yii::t('web', 'User') ?></th>
        		                <th><?= Yii::t('web', 'Assignment') ?></th>
        					</tr>
        				</thead>
        				<tbody>
        				<?php foreach($entry['modification']['userAssignment'] as $user) {?>
        					<tr>
        						<td><?= $user['first_name'] . ' ' . $user['last_name'] ?></td>
        						<td><?= $user['is_added'] == 0 ? Yii::t('main', 'removed') : Yii::t('main', 'added')?></td>
        					</tr>
        				<?php }?>
        				</tbody>
        			</table>
        		</div>
        		<?php } ?>
        	</div>
    	</div>
    <?php } ?>
</div>