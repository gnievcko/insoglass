<?php 
use yii\helpers\Html;
use common\helpers\StringHelper;
?>

<?php foreach($orderEditions as $edition) {?>
    <div class="col-xs-12 hist">
    	<div class="row">
		    <div class="col-xs-12"><?php
		       	$text = '';
		       	if(!empty($edition['user']['first_name']) && !empty($edition['user']['last_name'])) {
		       		$text = $edition['user']['first_name']. ' ' . $edition['user']['last_name'];
		        } else {
		            $text = $edition['user']['email'];
		        }
		
		        echo Html::mailto(Html::encode($text), $edition['user']['email']);
			?></div>
			<div class="col-xs-12 data"><?= Html::encode(StringHelper::getFormattedDateFromDate($edition['date'], true)) ?></div>
			
			<?php if(!empty($edition['fields changed']['parent_order'])) {?>
			<div class="col-xs-12">
				<?php echo StringHelper::translateOrderToOffer('web', 'Parent order') . ': ';
					echo !empty($edition['fields changed']['parent_order']['old']) ? 
						Html::a($edition['fields changed']['parent_order']['old']['name'], ['order/details', 'id' => $edition['fields changed']['parent_order']['old']['id']]) : 
						Yii::t('web', 'None'); 
					echo ' --> '; 
					echo !empty($edition['fields changed']['parent_order']['new']) ?
						Html::a($edition['fields changed']['parent_order']['new']['name'], ['order/details', 'id' => $edition['fields changed']['parent_order']['new']['id']]) :
						Yii::t('web', 'None'); 
				?>
			</div>
			<?php } ?>
			
			<?php if(!empty($edition['fields changed']['company'])) {?>
			<div class="col-xs-12">
				<?php echo Yii::t('main', 'Client') . ': ';
					echo !empty($edition['fields changed']['company']['old']) ? 
						Html::a($edition['fields changed']['company']['old']->name, ['client/details', 'id' => $edition['fields changed']['company']['old']->id]) : 
						Yii::t('web', 'None'); 
					echo ' --> '; 
					echo !empty($edition['fields changed']['company']['new']) ?
						Html::a($edition['fields changed']['company']['new']->name, ['client/details', 'id' => $edition['fields changed']['company']['new']->id]) :
						Yii::t('web', 'None'); 
				?>
			</div>
			<?php } ?>
			
			<?php if(!empty($edition['fields changed']['entity'])) {?>
			<div class="col-xs-12">
				<?php echo Yii::t('web', 'Entity') . ': ';
					echo !empty($edition['fields changed']['entity']['old']) ? 
						Html::a($edition['fields changed']['entity']['old']->name, ['client/details', 'id' => $edition['fields changed']['entity']['old']->id]) : 
						Yii::t('web', 'None'); 
					echo ' --> '; 
					echo !empty($edition['fields changed']['entity']['new']) ?
						Html::a($edition['fields changed']['entity']['new']->name, ['client/details', 'id' => $edition['fields changed']['entity']['new']->id]) :
						Yii::t('web', 'None'); 
				?>
			</div>
			<?php } ?>
			
			<?php if(!empty($edition['fields changed']['number'])) {?>
			<div class="col-xs-12">
				<?php echo Yii::t('web', 'Number') . ': ';
					echo !empty($edition['fields changed']['number']['old']) ? 
						$edition['fields changed']['number']['old'] : 
						Yii::t('web', 'None'); 
					echo ' --> '; 
					echo !empty($edition['fields changed']['number']['new']) ?
						$edition['fields changed']['number']['new'] :
						Yii::t('web', 'None'); 
				?>
			</div>
			<?php } ?>
			
			<?php if(!empty($edition['fields changed']['title'])) {?>
			<div class="col-xs-12">
				<?php echo Yii::t('web', 'Title') . ': ';
					echo !empty($edition['fields changed']['title']['old']) ? 
						$edition['fields changed']['title']['old'] : 
						Yii::t('web', 'None'); 
					echo ' --> '; 
					echo !empty($edition['fields changed']['title']['new']) ?
						$edition['fields changed']['title']['new'] :
						Yii::t('web', 'None'); 
				?>
			</div>
			<?php } ?>
	
			<?php if(!empty($edition['fields changed']['description'])) {?>
			<div class="col-xs-12">
				<?php echo Yii::t('web', 'Description') . ': ';
					echo !empty($edition['fields changed']['description']['old']) ? 
						$edition['fields changed']['description']['old'] : 
						Yii::t('web', 'None'); 
					echo ' --> '; 
					echo !empty($edition['fields changed']['description']['new']) ?
						$edition['fields changed']['description']['new'] :
						Yii::t('web', 'None'); 
				?>
			</div>
			<?php } ?>
			
			<?php if(!empty($edition['fields changed']['description_note'])) {?>
			<div class="col-xs-12">
				<?php echo Yii::t('web', 'Note') . ': ';
					echo !empty($edition['fields changed']['description_note']['old']) ? 
						$edition['fields changed']['description_note']['old'] : 
						Yii::t('web', 'None'); 
					echo ' --> '; 
					echo !empty($edition['fields changed']['description_note']['new']) ?
						$edition['fields changed']['description_note']['new'] :
						Yii::t('web', 'None'); 
				?>
			</div>
			<?php } ?>
			
			<?php if(!empty($edition['fields changed']['description_cont'])) {?>
			<div class="col-xs-12">
				<?php echo Yii::t('web', 'Remarks') . ': ';
					echo !empty($edition['fields changed']['description_cont']['old']) ? 
						$edition['fields changed']['description_cont']['old'] : 
						Yii::t('web', 'None'); 
					echo ' --> '; 
					echo !empty($edition['fields changed']['description_cont']['new']) ?
						$edition['fields changed']['description_cont']['new'] :
						Yii::t('web', 'None'); 
				?>
			</div>
			<?php } ?>
			
			<?php if(!empty($edition['fields changed']['payment_terms'])) {?>
			<div class="col-xs-12">
				<?php echo Yii::t('web', 'Terms of payment') . ': ';
					echo !empty($edition['fields changed']['payment_terms']['old']) ? 
						$edition['fields changed']['payment_terms']['old'] : 
						Yii::t('web', 'None'); 
					echo ' --> '; 
					echo !empty($edition['fields changed']['payment_terms']['new']) ?
						$edition['fields changed']['payment_terms']['new'] :
						Yii::t('web', 'None'); 
				?>
			</div>
			<?php } ?>
			
			<?php if(!empty($edition['fields changed']['contract_type'])) {?>
			<div class="col-xs-12">
				<?php echo StringHelper::translateOrderToOffer('documents', 'Type of contract') . ': ';
					echo !empty($edition['fields changed']['contract_type']['old']) ? 
						$edition['fields changed']['contract_type']['old'] : 
						Yii::t('web', 'None'); 
					echo ' --> '; 
					echo !empty($edition['fields changed']['contract_type']['new']) ?
						$edition['fields changed']['contract_type']['new'] :
						Yii::t('web', 'None'); 
				?>
			</div>
			<?php } ?>
		</div>
	</div>
<?php } ?>