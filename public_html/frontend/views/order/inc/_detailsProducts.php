<?php 

use common\helpers\StringHelper;
use yii\helpers\Html;
use yii\helpers\Url;
?>

<div class="table-responsive">
    <table class="table table-bordered table-hover list-grid-table">
        <thead>
            <tr>
                <th><?= Yii::t('web', 'Name') ?></th>
                <th><?= Yii::t('web', 'Unit price') ?></th>
                <th><?= Yii::t('web', 'Count unit') ?></th>
                <th><?= Yii::t('web', 'Count') ?></th>
                <th><?= Yii::t('web', 'Net value') ?></th>
                <?php if(Yii::$app->params['isOrderGrossValuesVisible']) { ?>
                    <th><?= Yii::t('web', 'Vat rate') ?></th>
                    <th><?= Yii::t('web', 'Vat amount') ?></th>
                    <th><?= Yii::t('web', 'Gross value') ?></th>

                <?php } ?>
                <th><?= Yii::t('web', 'Action') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach($products as $product) { ?>
                <tr <?= !empty($product['isArtificial']) ? 'class="artificial"' : '' ?>>
                    <td>
                        <?= ($product['parentName'] ?: $product['name']) . (!empty($product['parentName']) ? ' (' . strtolower(Yii::t('web', 'Version')) . ': ' . $product['name'] . ')' : '') ?>
                    </td>
                    <td class="text-center"><?= (!empty($product['price']) ? StringHelper::getFormattedCost($product['price']) . ' ' . $product['currency'] : '') ?></td>
                    <td><?= $product['unit'] ?></td>
                    <td><?= $product['count'] ?></td>
                    <td><?= StringHelper::getFormattedCost($product['netValue']). ' ' . $plnCurrency->short_symbol ?></td>
                    <?php if(Yii::$app->params['isOrderGrossValuesVisible']) { ?>
                        <td><?= Yii::$app->params['vatRate'][$product['vatRate']] ?></td>
                        <td><?= StringHelper::getFormattedCost($product['vatAmount']). ' ' . $plnCurrency->short_symbol ?></td>
                        <td><?= StringHelper::getFormattedCost($product['grossValue']). ' ' . $plnCurrency->short_symbol ?></td>
                    <?php } ?>
                    <td class="action text-center">
                        <?=
                        !empty($product['isArtificial']) ? '-' :
                                Html::a('', Url::to(['offered-product/details', 'id' => !empty($product['parentId']) ? $product['parentId'] : $product['productId'], 'type' => $product['type']]), ['class' => 'base-icon details-icon action-icon']);
                        ?>
                    </td>
                </tr>
			<?php }
			if(Yii::$app->params['isOrderGrossValuesVisible']) { 
                foreach($vats as $vat) { ?>
                    <tr class="vat-summary">
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td><?= StringHelper::getFormattedCost($vat['netValue']). ' ' . $plnCurrency->short_symbol ?></td>
                        
                            <td><?= $vat['vatLabel'] ?></td>
                            <td><?= StringHelper::getFormattedCost($vat['vatAmount']). ' ' . $plnCurrency->short_symbol ?></td>
                            <td><?= StringHelper::getFormattedCost($vat['grossValue']). ' ' . $plnCurrency->short_symbol ?></td>
                        
                        <td></td>
                    </tr>
            	<?php } ?>
        	<?php } ?>
        </tbody>
        <tfoot>
            <tr>
                <td><?= Yii::t('web', 'Summary') ?></td>
                <td></td>
                <td></td>
                <td></td>
                <td><?= StringHelper::getFormattedCost($summary['netValue']). ' ' . $plnCurrency->short_symbol ?></td>
                <?php if(Yii::$app->params['isOrderGrossValuesVisible']) { ?>
                    <td></td>
                    <td><?= StringHelper::getFormattedCost($summary['vatAmount']). ' ' . $plnCurrency->short_symbol ?></td>
                    <td><?= StringHelper::getFormattedCost($summary['grossValue']). ' ' . $plnCurrency->short_symbol ?></td>
                <?php } ?>
                <td></td>
            </tr>
        </tfoot>
    </table>
</div>