<?php
    use kartik\datecontrol\DateControl;
    use kartik\widgets\Select2;
    use yii\web\JsExpression;

    $this->registerJs('window.rr = function(){var d = $("#dateDeadlineId-disp-kvdate").kvDatepicker("getDate");
                                if(d == null) d = new Date();
                    			d.setDate(d.getDate() - 3);
                    			var dateToday = new Date();
                    			dateToday.setHours(0,0,0,0);

                    			if(d.getTime() > dateToday.getTime()) {
    								
    								var dateFormat2 = d.getFullYear() + "-" + ("0" + (d.getMonth() + 1)).slice(-2) + "-" + ("0" + (d.getDate())).slice(-2);
    								var dateFormat =  ("0" + (d.getDate())).slice(-2) + "." + ("0" + (d.getMonth() + 1)).slice(-2) + "." + d.getFullYear() ;
    					
    								$("#dateReminderId").val(dateFormat2);
									$("#dateReminderId-disp-kvdate").kvDatepicker("update", dateFormat);
    					
                    			}}');
?>

<div class="row">
    <div class="col-sm-4">
        <div class="form-group">
            <?= $form->field($orderHistoryForm, 'dateDeadline')->widget(DateControl::classname(), [
                    'language' => \Yii::$app->language,
                    'type' => DateControl::FORMAT_DATE,
                    'ajaxConversion' => false,
					'displayFormat' => 'php:d.m.Y',
					'saveFormat' => 'php:Y-m-d',
                    'options' => [
                    	'id' => 'dateDeadlineId',
                        'pluginOptions' => [
                            'autoclose' => true
                        ],
                    	'pluginEvents' => [
                    		'change' => 'function(){
                    			window.rr();
                    		}',
                        ]
                    ]
                ]);
            ?>
        </div>
    </div>

    <div class="col-sm-6 col-sm-offset-2">
        <div class="form-group">
            <?= $form->field($orderHistoryForm, 'orderStatusId')->dropDownList($orderStatuses, ['placeholder' => '']);?>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-sm-4">
        <div class="form-group">
            <?= $form->field($orderHistoryForm, 'dateReminder')->widget(DateControl::classname(), [
                    'language' => \Yii::$app->language,
                    'type' => DateControl::FORMAT_DATE,
                    'ajaxConversion' => false,
					'displayFormat' => 'php:d.m.Y',
					'saveFormat' => 'php:Y-m-d',
                    'options' => [
                    	'id' => 'dateReminderId',
                        'pluginOptions' => [
                            'autoclose' => true,
                        ]
                    ]
                ]);
            ?>
        </div>
    </div>
    <div class="col-sm-6 col-sm-offset-2">
        <div class="form-group">
            <?= $form->field($orderHistoryForm, 'message')->textArea();?>
        </div>
    </div>
</div>
<?php if(Yii::$app->params['isShippingDateAvailable']): ?>
    <div class="row">
        <div class="col-sm-4">
             <div class="form-group">
                <?= $form->field($orderHistoryForm, 'shippingDate')->widget(DateControl::classname(), [
                        'language' => \Yii::$app->language,
                        'type' => DateControl::FORMAT_DATE,
                        'ajaxConversion' => false,
                                            'displayFormat' => 'php:d.m.Y',
                                            'saveFormat' => 'php:Y-m-d',
                        'options' => [
                            'id' => 'dateShippingId',
                            'pluginOptions' => [
                                'autoclose' => true,
                            ]
                        ]
                    ]);
                ?>
             </div>
        </div>
    </div>
<?php endif; ?>
<?php
    $this->registerJs('
        $("#dateDeadlineId-disp").attr("placeholder","'.Yii::t('web','Write deadline date').'");
        $("#dateReminderId-disp").attr("placeholder","'.Yii::t('web','Write reminder date').'");
        $("#dateShippingId-disp").attr("placeholder","'.Yii::t('web','Write shipping date').'");
    ');

 ?>