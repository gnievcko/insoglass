<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\JsExpression;
use common\models\ar\User;
use common\models\ar\Company;
use common\helpers\UserNameHelper;
use frontend\helpers\UserHelper;
use kartik\select2\Select2;
use yii\web\View;
use frontend\helpers\AddressHelper;
?>
<?php if(Yii::$app->params['isParentCompanyFilterInOrderVisible']) { ?>
	<div class="col-xs-12">
		<div class="row">
			<div class="col-sm-6">
				<?php echo Select2::widget([
						'language' => Yii::$app->language,
						'name' => 'filter-parent-company',
		                'options' => ['placeholder' => Yii::t('web','Filter by parent company'), 'id' => 'filter-parent-company', 'class' => 'form-control'],
		                'data' => [],
		                'pluginOptions' => [
				                'allowClear' => true,
				                'minimumInputLength' => Yii::$app->params['isClientListImmediatelyVisible'] ? 0 : 1,
				                'ajax' => [
			                    		'url' => Url::to(['company/find']),
			                            'dataType' => 'json',
			                            'delay' => 250,
			                            'data' => new JsExpression('function(params) {
			                            	return {term:params.term, page:params.page, size: 20, onlyParentCompanies: true};
			                            }'),
			                            'processResults' => new JsExpression('function (data, params) { 
			                            		params.page = params.page || 1; 
			                            		var companies = $(data.items).map(function(index, company) {
			                            			return {id: company.id, name: company.name, address: company.address};
			                            		});
			
			                            		return {
			                                    	results: companies,
			                                        pagination: {
			                                        	more: data.more
			                                        }
			                                    };
			                            }'),
			                            'cache' => true
				                ],
		                		'escapeMarkup' => new JsExpression('function (markup) { return markup; }'),
				                'templateResult' => new JsExpression('function(item) { var address = item.address ? "<span class=\"address-highlight\">, " + item.address + "</span>" : ""; if(item.loading) return item.text; return item.name + address; }'),
				                'templateSelection' => new JsExpression('function (item) { var address = item.address ? "<span class=\"address-highlight\">, " + item.address + "</span>" : ""; return (item.name || item.text) + address; }'),
				        ]
				]); 		
			?>
            </div>

        </div>
	</div>
<?php } ?>

<div class="col-xs-12">
	<div class="row">
	    <div class="col-sm-6">
	        <?php 
	        	if(!empty($orderCreateForm->clientId)) {
		            $company = Company::findOne($orderCreateForm->clientId);
		            $address = !empty($company->address) ? AddressHelper::getFullAddress(['addressMain' => $company->address->main, 'cityName' => $company->address->city->name, 'cityZipCode' => $company->address->city->zip_code]) : '';
		            $clientData = [];
		            $clientData[$company->id] = '['.$company->id.'] '.$company->name.(!empty($address) ? '<span class="address-highlight">, '.$address.'</span>' : '');
	        	}
	        	else {
	        		$clientData = [];
	        	}
	        ?>

	        <?php echo $form->field($orderCreateForm, 'clientId')->widget(Select2::classname(), [
					'language' => Yii::$app->language,
	                'showToggleAll' => false,
	                'options' => ['placeholder' => Yii::t('main','Choose client'), 'id' => 'ordercreateform-clientid'],
	                'data' => $clientData,
	                'pluginOptions' => [
	                        'allowClear' => true,
	                        'minimumInputLength' => Yii::$app->params['isClientListImmediatelyVisible'] ? 0 : 1,
	                        'ajax' => [
	                                'url' => Url::to(['company/find']),
	                                'dataType' => 'json',
	                                'delay' => 250,
	                                'data' => new JsExpression('function(params) { 
	                                 	var pcId = $("#filter-parent-company").val() ? $("#filter-parent-company").val() : 0;
	                                    return {term:params.term, page:params.page, size: 20, parentCompanyId: pcId}; 
	                                }'),
	                                'processResults' => new JsExpression('function (data, params) { 
	                                    params.page = params.page || 1; 
	                                        
	                                    return {
	                                        results: data.items,
	                                        pagination: {
	                                            more: data.more
	                                        }
	                                    };
	                                }'),
	                                'cache' => true
	                        ],
	                   		'escapeMarkup' => new JsExpression('function (markup) { return markup; }'),
	                        'templateResult' => new JsExpression('function(item) { var address = item.address ? "<span class=\"address-highlight\">, " + item.address + "</span>" : ""; if(item.loading) return item.text; if(item.parent_company_id) return "<span class=\"object-highlight\">"+item.name+"</span>"+address; else return item.name + address; }'),
	                        'templateSelection' => new JsExpression('function (item) { var address = item.address ? "<span class=\"address-highlight\">, " + item.address + "</span>" : ""; return (item.name || item.text) + address; }'),
	                ],
	                'pluginEvents' => [
	                    'change' => 'function() { 
	                        $("#ordercreateform-contactpeopleids").select2("val", ""); 
	                        $("#employee-select").select2("val", ""); 
	                        $("#selected-employees").empty();
	                    }'
	                ],
	            ]);
	        ?>
            <a href="<?= Url::to(['client/create']); ?>" target="_blank" class="focusable-link link-sm add-item-after-select2">
                <span class="glyphicon glyphicon-plus-sign pull-left"></span>
                <span> <?=Yii::t('web', 'Create new client')?></span>
            </a>
        </div>
	    <div class="col-sm-5 col-sm-offset-1">
	        <?php 
	            $contactPeopleData = [];
	            foreach(User::find()->where(['id' => $orderCreateForm->contactPeopleIds])->all() as $contactPerson) {
	                $contactPersonData = $contactPerson->toArray();
	                $contactPeopleData[$contactPersonData['id']] = //UserNameHelper::generateDisplayedName($contactPersonData);
	                UserHelper::getPrettyUserName(
	                					!empty($contactPersonData['email']) ? $contactPersonData['email'] : '', 
	                					 !empty($contactPersonData['first_name']) ? $contactPersonData['first_name'] : '',
	                					!empty($contactPersonData['last_name']) ? $contactPersonData['last_name'] : ''
	            	);
	            }
	        ?>
	        
	        <?= $form->field($orderCreateForm, 'contactPeopleIds')->widget(Select2::classname(), [
	                    'language' => Yii::$app->language,
	                    'showToggleAll' => false,
	                    'options' => ['placeholder' => Yii::t('web','Choose contact person'), 'id' => 'ordercreateform-contactpeopleids'],
	                    'data' => $contactPeopleData,
	                    'pluginOptions' => [
	                            'multiple' => true,
	                            //'maximumSelectionLength' => 1,
	                            'allowClear' => true,
	                            'minimumInputLength' => 0,
	                            'ajax' => [
	                                    'url' => Url::to(['company/find-employees']),
	                                    'dataType' => 'json',
	                                    'delay' => 250,
	                                    'data' => new JsExpression('function(params) { 
	                                        var orderClientId = $("#ordercreateform-clientid").val();
	                                        return {term:params.term, page:params.page, size: 20, companyId: orderClientId}; 
	                                    }'),
	                                    'processResults' => new JsExpression('function (data, params) { 
	                                        params.page = params.page || 1; 
	                                        
	                                        return {
	                                            results: data.items,
	                                            pagination: {
	                                                more: data.more
	                                            }
	                                        };
	                                    }'),
	                                    'cache' => true
	                            ],
	                            'templateResult' => new JsExpression('function(item) { 
	                                if(item.loading) return item.text; 
	                                var fullName = item.first_name + ((item.first_name == null) ? "" : " ") + item.last_name; 
	                                return (fullName == null) ? item.email : fullName;
	                            }'),
	                            'templateSelection' => new JsExpression('function (item) { 
	                                if(item.first_name == null && item.last_name == null && item.email == null) {
	                                    return item.text;
	                                }
	                                else {
	                                    var fullName = item.first_name + ((item.first_name == null) ? "" : " ") + item.last_name; 
	                                    return (fullName == null) ? item.email : fullName;
	                                }
	                            }'),
	                    ],
	        		'pluginEvents' => [
	        				'change' => 'function() {
	                            
	                        }'
	        		],
	                ]); 
	        ?>
	    </div>
	</div>
</div>
<?php $this->registerJs('
		$("#filter-parent-company").select2().on("change", function() {
			if($(this).val()) {
				$("#ordercreateform-clientid").select2("val", "");
				//$("#ordercreateform-clientid").select2("data", null);
				//$.get("'.Url::to(['company/find']).'", { size: 100, parentCompanyId: $(this).val(), raw: true }, function(data) {
				//	$("#ordercreateform-clientid").select2({
				//		data: JSON.parse(data),
				//	});
				//	$("#ordercreateform-clientid").select2("open");
				//});
			}
			else {
				//$("#ordercreateform-clientid").select2("data", null);
			}
		});
		', View::POS_END); 
?>
