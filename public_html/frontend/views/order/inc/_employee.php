<?php
use yii\helpers\Html;
;use common\helpers\UserNameHelper;
use frontend\helpers\UserHelper;
?>

<li class="selected-employee">
    <?= Html::activeHiddenInput($orderEmployeesForm, 'employeesIds[]', ['class' => 'selected-employee-id', 'value' => $employee->id]) ?>
    <?php
		$worker = $employee->toArray();
     	echo UserHelper::getPrettyUserName(
                					!empty($worker['email']) ? $worker['email'] : '', 
                					 !empty($worker['first_name']) ? $worker['first_name'] : '',
                					!empty($worker['last_name']) ? $worker['last_name'] : ''
            	);  
     ?>

    <?= Html::img('@web/images/placeholders/garbage.png', ['class' => 'remove-employee-btn pull-right', 'alt' => Yii::t('main', 'Delete'), 'title' => Yii::t('main', 'Delete')]) ?>
</li>
