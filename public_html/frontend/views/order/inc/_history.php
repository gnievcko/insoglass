<?php 
use common\helpers\StringHelper;
use yii\helpers\Html;
use yii\web\View;
use yii\helpers\Url;

$this->registerJsFile('@web/js/pages/order-history-details.js', ['depends' => [\yii\web\JqueryAsset::className()]]);

?>

<div class="col-xs-12 title-with-icon">
	<div class="base-icon history-icon main-content-icons"></div>
	<h3><?= StringHelper::translateOrderToOffer('web', 'History order status') ?></h3>
</div>
<div class="col-xs-12">
	<?php if(Yii::$app->params['isChangeOfOrderHistoryTracked']) {?>
	<div class="text-left">
		<button type="button" id="order-details-history-btn" class="btn btn-primary" data-extend=1><?= Yii::t('web', 'Show detailed history') ?></button>
	</div>
	<?php } ?>
</div>
<div class="col-xs-12">
	<div class="row">
		<div class="col-sm-6" id="order-details-history">
			<?= $this->render('_historyDetailsShort', ['entries' => $entries])?>
		</div>
		<div class="col-sm-6" id="order-edit-history">
			<?= $this->render('_history_editions', ['orderEditions' => $orderEditions])?>
		</div>
	</div>
</div>
<?php $this->registerJs('getDetailedHistory("order-details-history-btn",
     "order-details-history",
     "' . Url::to(['order/get-history-details', 'id' => $orderId]) . '",
     "' . Yii::t('web', 'Show detailed history') . '",
    "' . Yii::t('web', 'Hide detailed history') . '")', View::POS_END); ?>
