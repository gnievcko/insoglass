<?php

use yii\web\View;
use yii\bootstrap\Modal;
use yii\helpers\Url;
use yii\helpers\Html;
use frontend\models\OrdersListForm;
use yii\widgets\LinkPager;
use common\widgets\TableHeaderWidget;
use common\helpers\StringHelper;
use common\helpers\Utility;
use common\bundles\EnhancedDialogAsset;
?>

<?php
$positiveTerminalStatus = Yii::$app->getSession()->get('positiveTerminalStatus');
Yii::$app->getSession()->remove('positiveTerminalStatus');
Modal::begin([
    'id' => 'modal',
    'size' => 'modal-lg',
    'clientOptions' => ['show' => !empty($positiveTerminalStatus)]
]);
?>
<div id="modalContent">
    <?php
    if(!empty($positiveTerminalStatus)) {
        echo Yii::$app->controller->renderPartial('inc/_alert', [
            'order' => $data,
            'form' => new AlertForm()
        ]);
    }
    ?>
</div>
<?php Modal::end(); ?>


<?php if(empty($orders)) { ?>
    <label><?= Yii::t('web', 'No search results'); ?></label>
    <br><br>

<?php } else {
    ?>
    <div class="table-responsive">
        <table class="<?= TableHeaderWidget::TABLECLASS ?>">
            <?php echo TableHeaderWidget::widget(['fields' => OrdersListForm::getSortFields(), 'listForm' => $ordersListForm, 'iconCount' => 4]); ?>
            <tbody>
                <?php foreach($orders as $order): ?>
                    <tr class="<?php
                    if(\Yii::$app->params['isNewOrderColoredOnList'] && $order['orderStatusSymbol']==="new"){
                        if(empty($order['companyIdOfCreator'])){
                            echo 'created-by-worker';
                        }else{
                            echo 'created-by-client';
                        }
                    } ?>">
                        <td><?= Html::encode($order['number']) ?></td>
                        <td><?= Html::encode($order['title']) ?></td>
                        <td><?= Html::encode(StringHelper::getFormattedDateFromTimestamp($order['date_creation_timestamp'])) ?></td>
                        <td><?= Html::encode($order['date_shipping']) ?></td>
                        <td><?= Html::encode($order['companyName']) . (!empty($order['parentCompanyName']) ? Html::encode(' (' . $order['parentCompanyName'] . ')') : '') ?></td>
                        <?php if(Yii::$app->params['isContractTypeVisible']) { ?>
                            <td><?= Html::encode($order['contractType']) ?></td>
                        <?php } ?>
                        <td>
                            <?php // echo Html::encode($order['statusName']) ?>
                            <!-- <span value=" --><?php // echo Url::to(['order/quick-update', 'id' => $order['id']]) ?><!-- " class="modalButton focusable-link glyphicon glyphicon-edit  --><?php //echo  $order['is_active']?:'hidden'?><!-- "></span> -->
                            <?= Html::a($order['statusName'],Url::to(['order/quick-update', 'id' => $order['id']]), ['class' => 'modalButton focusable-link'])?>
                        </td>
                        <!--<td><?= Html::encode(StringHelper::getFormattedDateFromTimestamp($order['date_modification_timestamp'])) ?></td> -->
                        <?php if(Yii::$app->params['isFileVisibleInOrderList']) { ?>
                            <td class="orders-list-files">
                                <?= $order['has_attachment'] ? '<div class="base-icon link-icon file-icon"></div>' : '' ?> 
                                <?= $order['has_photo'] ? '<div class="base-icon link-icon image-icon"></div>' : '' ?>                        
                            </td>
                        <?php } ?>
                        <td>
                            <?php
                            echo Html::a('', Url::to(['order/details', 'id' => $order['id']]), ['class' => 'base-icon details-icon action-icon', 'title' => Yii::t('web', 'Details')]);
                            if(Yii::$app->params['isCopyOrderVisible']) {
                                echo '&nbsp;' . Html::a('', Url::to(['order/copy', 'id' => $order['id']]), ['class' => 'base-icon copy-icon action-icon', 'title' => Yii::t('web', 'Copy')]);
                            }
                            if(Yii::$app->params['isOfferDetailsVisible']) {
                                echo '&nbsp;' . Html::a('', ['documents/offer-details', 'orderId' => $order['id'], 'fastPrinting' => 1], ['class' => 'base-icon print-icon action-icon', 'target' => '_blank', 'title' => Yii::t('web', 'Fast printing')]);
                            }
                            if(Yii::$app->user->can(Utility::ROLE_MAIN_SALESMAN)) {
                                echo '&nbsp;' . Html::a('', Url::to(['order/delete']), [
                                    'class' => 'base-icon delete-icon action-icon',
                                    'title' => Yii::t('main', 'Delete'),
                                    'data' => [
                                        'confirm' => StringHelper::translateOrderToOffer('main', 'Are you sure you want to delete this order?<br/><strong>It would be not available as well as its related specific data.</strong>'),
                                        'method' => 'post',
                                        'params' => [
                                            'id' => $order['id'],
                                        ],
                                    ],
                                ]);
                            }
                            ?>
                        </td>
                    </tr>
                <?php endforeach ?>
            </tbody>
        </table>
    </div>
    <div class="col-sm-12">
        <div class="col-sm-4 text-left dynamic-table-counter">
            <?= Yii::t('web', 'Number of found items: {n}', ['n' => $pages->totalCount]) ?>
        </div>
        <div class="col-sm-8 text-right dynamic-table-pagination">
            <?= LinkPager::widget(['pagination' => $pages, 'firstPageLabel' => true, 'lastPageLabel' => true]) ?>
        </div>
    </div>
<?php } ?>

<input class="dynamic-table-sort-dir" type="hidden" name="<?= Html::getInputName($ordersListForm, 'sortDir') ?>" value="<?= $ordersListForm->sortDir ?>" />
<input class="dynamic-table-sort-field" type="hidden" name="<?= Html::getInputName($ordersListForm, 'sortField') ?>" value="<?= $ordersListForm->sortField ?>" />
<input class="dynamic-table-page" type="hidden" name="<?= Html::getInputName($ordersListForm, 'page') ?>" value="<?= $ordersListForm->page ?>" />

<?php
EnhancedDialogAsset::register($this);
?>


<?php
    /*
    $this->registerJsFile(
        '@web/js/cookie/js.cookie.js',
        ['depends' => [\yii\web\JqueryAsset::className()]]
    );

    */

    $this->registerJs('
    $(".modalButton").click(function(e){
        e.preventDefault();
        //Cookies.set("scroll", $(document).scrollTop());
        $("#modal").modal("show")
                .find("#modalContent")
                .load($(this).attr("href"));
    });
    ');

    /*
    $this->registerJs('    
    	if ( Cookies.get("scroll") !== null ) {
            $(document).scrollTop( Cookies.get("scroll") );
    }', View::POS_END);
     *
    **/
?>

