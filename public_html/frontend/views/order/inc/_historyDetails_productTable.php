<?php 
use common\helpers\StringHelper;
?>

<div class="col-xs-12 text-center"> <?= Yii::t('web', $text)?> </div>
<div class="col-xs-12">
	<table class="table">
		<thead>
			<tr>
                <th><?= Yii::t('web', 'Name') ?></th>
                <th><?= Yii::t('web', 'Unit price') ?></th>
                <th><?= Yii::t('web', 'Count unit') ?></th>
                <th><?= Yii::t('web', 'Count') ?></th>
                <th><?= Yii::t('web', 'Net value') ?></th>
			</tr>
		</thead>
		<tbody>
		<?php foreach($products as $product) {?>
			<tr>
				<td><?= $product['name'] ?></td>
				<td><?= !empty($product['price']) ? StringHelper::getFormattedCost($product['price']) . ' ' . $product['currencyShortSymbol'] : ''?></td>
				<td><?= $product['unit'] ?></td>
				<td><?= $product['count'] ?></td>
				<td><?= StringHelper::getFormattedCost($product['count'] * $product['price']) . ' ' . $product['currencyShortSymbol']?></td>
			</tr>
		<?php }?>
		</tbody>
	</table>
</div>

