<?php
use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\JsExpression;
use common\models\ar\User;
use kartik\select2\Select2;
?>

<div class="col-xs-12 title-with-icon">
	<div class="base-icon employee-icon main-content-icons"></div>
	<h3><?= Yii::t('main', 'Employees') ?></h3>
</div>
<div class="text-center order-employee-list">
    <div class="col-sm-5">
        <label class="form-group"><?= Yii::t('web', 'Employees list') ?></label>
        <div class="text-left">
            <?=
                Select2::widget([
                    'name' => 'employeeSelect',
                    'language' => Yii::$app->language,
                    'showToggleAll' => false,
                    'options' => ['placeholder' => Yii::t('main','Select an employee...'), 'id' => 'employee-select', 'data-form-url' => Url::to(['order/get-employee-form'])],
                    'data' => [],
                    'pluginOptions' => [
                            'allowClear' => true,
                            'minimumInputLength' => 1,
                            'ajax' => [
                                    'url' => Url::to(['salesman/find']),
                                    'dataType' => 'json',
                                    'delay' => 250,
                                    'data' => new JsExpression('function(params) {
                                        var clientId = '.(empty($orderCreateForm->clientId) ? 'null' : $orderCreateForm->clientId).' || $("#ordercreateform-clientid").val();
                                        var ignoredSalespeopleIds = [];
                                        $(".selected-employee-id").each(function(idx, input) {
                                            ignoredSalespeopleIds.push($(input).val());
                                        });

                                        return {term:params.term, page:params.page, size: 20, clientId: clientId, ignoredSalespeopleIds: ignoredSalespeopleIds};
                                    }'),
                                    'processResults' => new JsExpression('function (data, params) {
                                        params.page = params.page || 1;

                                        return {
                                            results: data.items,
                                            pagination: {
                                                more: data.more
                                            }
                                        };
                                    }'),
                                    'cache' => true
                            ],
                            'templateResult' => new JsExpression('function(item) {
                                if(item.loading) return;

                                var fullName = item.first_name + ((item.first_name == null) ? "" : " ") + item.last_name;
                                return (fullName == null) ? item.email : fullName;
                            }'),
                            'templateSelection' => new JsExpression('function (item) {
                                if(item.first_name == null && item.last_name == null && item.email == null) {
                                    return item.text;
                                }
                                else {
                                    var fullName = item.first_name + ((item.first_name == null) ? "" : " ") + item.last_name;
                                    return (fullName == null) ? item.email : fullName;
                                }
                            }'),
                    ],
                ]);
            ?>
        </div>
    </div>

    <div class="col-sm-6 col-sm-offset-1">
        <?php
            $field = $form->field($orderEmployeesForm, 'employeesIds[]')->label(Yii::t('web', 'Selected employees'));
            $field->template = "{label}\n{error}";
            echo $field;
        ?>
        <ul id="selected-employees">
            <?php
                foreach(User::find()->where(['id' => $orderEmployeesForm->employeesIds])->all() as $employee) {
                    echo Yii::$app->controller->renderPartial('inc/_employee', ['orderEmployeesForm' => $orderEmployeesForm, 'employee' => $employee]);
                }
            ?>
        </ul>
    </div>
</div>
