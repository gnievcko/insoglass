<?php

use yii\helpers\Html;
use yii\helpers\Url;
use common\helpers\Utility;
use common\helpers\StringHelper;
?>

<?php if(empty($items)) { ?>
    <label><?= Yii::t('web', 'No search results'); ?></label>
    <br><br>
<?php } else {
    ?>
    <div class="table-responsive">
        <table class="table borderless">
            <tbody>
                <?php foreach($items as $item): ?>
                    <tr data-id="<?= $item['id'] ?>">
                        <td data-id="<?= $item['id'] ?>" >
                            <a href="<?= Url::to(['order/details', 'id' => $item['id']]); ?>" class="short-order-title task-title not-extended"><?= Html::encode($item['title']) ?></a>
                            <?php if(!empty($item['dateDeadline'])) { ?>
                                <span class="task-date pull-right"><?= StringHelper::getFormattedDateFromDate(Html::encode($item['dateDeadline']), false) ?></span>
                            <?php } ?>
                        </td>
                    </tr>
                <?php endforeach ?>
            </tbody>
        </table>
    </div>
<?php } ?>

<input class="dynamic-table-sort-dir" type="hidden" name="<?= Html::getInputName($listForm, 'sortDir') ?>" value="<?= $listForm->sortDir ?>" />
<input class="dynamic-table-sort-field" type="hidden" name="<?= Html::getInputName($listForm, 'sortField') ?>" value="<?= $listForm->sortField ?>" />
<input class="dynamic-table-page" type="hidden" name="<?= Html::getInputName($listForm, 'page') ?>" value="<?= $listForm->page ?>" />
