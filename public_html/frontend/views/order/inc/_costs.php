<?php
    use common\helpers\StringHelper;
    use frontend\models\ArtificialProductAttributeForm;
    use yii\helpers\Url;
    use yii\helpers\Html;
?>

<div class="col-xs-12 title-with-icon">
	<div class="base-icon box-icon main-content-icons"></div>
	<h3><?= StringHelper::translateOrderToOffer('web', 'Add custom products') ?></h3>
</div>
<div class="col-xs-12 well">
	<div class="row">
	<?php if(!Yii::$app->params['isOrderProductDimensionsVisible']) { ?>
         <div class="items-container col-xs-12">
         	<div class="row">
                <div class="col-sm-4">
                    <?= Yii::t('web', 'Name') ?>        
                </div>
                <div class="col-sm-<?= Yii::$app->params['isOrderGrossValuesVisible'] ? 1 : 2 ?>">
                    <?= Yii::t('web', 'Count') ?>
                </div>
                <div class="col-sm-1">
                    <?= Yii::t('web', 'Count unit') ?>
                </div>
                <div class="col-sm-<?= Yii::$app->params['isOrderGrossValuesVisible'] ? 1 : 2 ?>">
                   <?= Yii::t('web', 'Price for unit') ?>
                </div>
                <div class="col-sm-1" style="display:none">
                   <?= Yii::t('main', 'Currency') ?>
                </div>
                <div class="col-sm-<?= Yii::$app->params['isOrderGrossValuesVisible'] ? 1 : 2 ?>">
                    <?= Yii::t('web', 'Net value') ?>
                </div>
                <?php if(Yii::$app->params['isOrderGrossValuesVisible']) { ?>
        	        <div class="col-sm-1">
        	            <?= Yii::t('web', 'Vat rate') ?>
        	        </div>
        	        <div class="col-sm-1">
        	            <?= Yii::t('web', 'Vat amount') ?>
        	        </div>
        	        <div class="col-sm-1">
        	            <?= Yii::t('web', 'Gross value') ?>
        	        </div>
                <?php } ?>
                <div class="col-sm-1">
                   <?= Yii::t('web', 'Action') ?>
                </div>
                
            </div>
        </div>
    	<?php } ?>
        <div id="order-additional-costs" class="items-container">
           
            <?php 
            foreach($orderCostsForms as $prefix => $orderCostForm) {
                echo Yii::$app->controller->renderPartial('inc/_cost', [
                    'displayFirstAttributes' => $displayFirstAttributes && $prefix == 0,
                    'orderCostForm' => $orderCostForm,
                    'currencies' => $currencies,
                    'prefix' => $prefix,
                    'constructions' => $constructions,
                    'shapes' => $shapes,
                    'muntins' => $muntins,
                    'form' => $form,
                    'plnCurrency' =>  $plnCurrency,
                    'productAttributeForm' => empty($productAttributeForms[$prefix]) ? new ArtificialProductAttributeForm() : $productAttributeForms[$prefix],
                ]);
            }?>
            <?php if(!Yii::$app->params['isOrderProductSummaryCommon']) { ?>
        	    <?php if(Yii::$app->params['isOrderGrossValuesVisible']) {
        	    	foreach ($orderCostsVats as $vat) { ?>
        			    <div class="col-sm-12">
        			        <div class="col-sm-1 col-sm-offset-7">
        			            <?= StringHelper::getFormattedCost($vat['netValue']). ' ' . $plnCurrency->short_symbol ?>
        			        </div>
        			        <div class="col-sm-1">
        			            <?= $vat['vatLabel'] ?>
        			        </div>
        			        <div class="col-sm-1">
        			            <?= StringHelper::getFormattedCost($vat['vatAmount']). ' ' . $plnCurrency->short_symbol ?>
        			        </div>
        			        <div class="col-sm-1">
        			            <?= StringHelper::getFormattedCost($vat['grossValue']). ' ' . $plnCurrency->short_symbol ?>
        			        </div>
        			    </div>
        	    	<?php } ?>
        	    <?php } ?>
        	    <div class="col-sm-12">
        	        <div class="col-sm-2 col-sm-offset-<?= Yii::$app->params['isOrderGrossValuesVisible'] ? 5 : 6 ?>">
        	            <?= Yii::t('main', 'Total') ?>
        	        </div>
        	        <div class="col-sm-1 <?= Yii::$app->params['isOrderGrossValuesVisible'] ? '' : 'col-sm-offset-1' ?>">
        	            <?= StringHelper::getFormattedCost($orderCostsSummary['netValue']). ' ' . $plnCurrency->short_symbol ?>
        	        </div>
        	        <?php if(Yii::$app->params['isOrderGrossValuesVisible']) { ?>
        		        <div class="col-sm-1 col-sm-offset-1 ">
        		            <?= StringHelper::getFormattedCost($orderCostsSummary['vatAmount']). ' ' . $plnCurrency->short_symbol ?>
        		        </div>
        		        <div class="col-sm-1">
        		            <?= StringHelper::getFormattedCost($orderCostsSummary['grossValue']). ' ' . $plnCurrency->short_symbol ?>
        		        </div>
        	        <?php } ?>
        	    </div>
        	<?php } ?>
        </div>
        <div class="col-xs-12">
			<a href="#" id="add-cost-link" class="tab-me focusable-link add-item-link">
            	<span class="glyphicon glyphicon-plus-sign"></span>
            	<span><?= Yii::t('web', 'Add product') ?></span>
			</a>
        </div>
	</div>
</div>


