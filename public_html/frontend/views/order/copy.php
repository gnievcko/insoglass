<?php
use yii\helpers\Url;
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\web\View;
use common\helpers\StringHelper;
use common\helpers\Utility;

$this->registerJsFile('@web/js/forms.js?'.uniqid(), ['depends' => [\yii\web\JqueryAsset::className()]]);
$this->registerJsFile('@web/js/order-form.js?'.uniqid(), ['depends' => [\yii\web\JqueryAsset::className()]]);
$this->registerJsFile('@web/js/pages/toggle-attributes.js?'.uniqid(), ['depends' => [\yii\web\JqueryAsset::className()]]);
$this->registerJsFile('@web/js/pages/ral-colour-picker.js?'.uniqid(), ['depends' => [\yii\web\JqueryAsset::className()]]);
if(Yii::$app->params['isVisualisationVisible']) {
    $this->registerJsFile('@web/js/pages/order-visualisation.js?' . uniqid(), ['depends' => [\yii\web\JqueryAsset::className()]]);
}

$this->title = StringHelper::translateOrderToOffer('web', 'Copy order') . ' - ' . Yii::$app->name;

$this->params['breadcrumbs'][] = ['label' => StringHelper::translateOrderToOffer('main', 'Orders'), 'url' => Url::to(['order/index'])];
$this->params['breadcrumbs'][] = ['label' => StringHelper::translateOrderToOffer('main', 'List of orders'), 'url' => Url::to(['order/list'])];
$this->params['breadcrumbs'][] = StringHelper::translateOrderToOffer('web', 'Copy order');
?>

<div class="order-form">
	<h2 class="page-title"><?= StringHelper::translateOrderToOffer('web', 'Copy order') ?></h2>
    
    <?php 
        $form = ActiveForm::begin([
            'id' => 'order-create-form',
            'enableAjaxValidation' => true,
            'enableClientValidation' => false,
            'options' => [
                'class' => 'form',
            ]
        ]);

        echo $form->errorSummary($invalidForms);
    ?>

    <?= Yii::$app->controller->renderPartial('inc/_basic_data', [
    		'entities' => $entities,
        	'orderCreateForm' => $orderCreateForm, 
        	'orderProductsForms' => $orderProductsForms,
        	'currencies' => $currencies,
    		'orderTypes' => $orderTypes,
            'contractTypes' => $contractTypes,
        	'form' => $form,
    ]) ?>
    
  <div id="products">
        <?= Yii::$app->controller->renderPartial('inc/_products', [
                'orderProductsForms' => $orderProductsForms,
                'displayFirstAttributes' => $displayFirstAttributes,
                'currencies' => $currencies,
                'constructions' => $constructions,
                'shapes' => $shapes,
                'muntins' => $muntins,
                'form' => $form,
                'orderProductsVats' => $orderProductsVats,
                'orderProductsSummary' => $orderProductsSummary,
                'plnCurrency' =>  $plnCurrency,
                'productAttributeForms' => !empty($productAttributeForms) ? $productAttributeForms : [],
        ]); ?>
    </div>
    <div id="costs">
        <?= Yii::$app->controller->renderPartial('inc/_costs', [
                'orderCostsForms' => $orderCostsForms,
                'currencies' => $currencies,
                'displayFirstAttributes' => $displayFirstAttributes,
                'constructions' => $constructions,
                'shapes' => $shapes,
                'muntins' => $muntins,
                'form' => $form,
                'orderCostsVats' => $orderCostsVats,
                'orderCostsSummary' =>$orderCostsSummary,
                'plnCurrency' =>  $plnCurrency
        ]); ?>
    </div>
    
    <div id="product-summary">
    	<?= Yii::$app->controller->renderPartial('inc/_productSummary', ['summary' => $summary, 'plnCurrency' => $plnCurrency]) ?>
    </div>
    
	<?= Yii::$app->controller->renderPartial('inc/_table', ['orderTableForm' => $orderTableForm, 'form' => $form]) ?>	

    <?= Yii::$app->controller->renderPartial('inc/_employees', ['orderEmployeesForm' => $orderEmployeesForm, 'form' => $form, 'orderCreateForm' => $orderCreateForm]) ?>

    <?= Yii::$app->controller->renderPartial('inc/_files', [
            'orderCreateForm' => $orderCreateForm,
            'photosForms' => $photosForms,
            'documentsForms' => $documentsForms,
            'form' => $form,
        ]);
    ?>

    <div>
        <?= Html::a(Yii::t('web', 'Cancel'), 
        		!empty(Yii::$app->request->getReferrer()) ? Yii::$app->request->getReferrer() : ['order/list'], 
        		['class' => 'btn btn-default']) ?>
        <button class="btn btn-primary pull-right"><?= Yii::t('web', 'Copy') ?></button>
    </div>

    <?php ActiveForm::end() ?>
</div>

<?php $this->registerJs('
    (function() {
        var dynamicForm = new DynamicForm("order-create-form");
        dynamicForm.activateRemoveButtons();
        dynamicForm.activateAddItemButtons();

        OrderForm.activateEmployeesChooser($("#employee-select"), $("#selected-employees"));

        dynamicForm.activatePhotoUpload();
        dynamicForm.activateDocumentUpload();
        OrderForm.initRecalculate("OrderProductForm", "'. Url::to(['order/get-products-form']).'", "order-create-form", "products", "ProductAttributeForm");
        OrderForm.initRecalculate("OrderCostForm", "'.Url::to(['order/get-costs-form']).'", "order-create-form", "costs");
		OrderForm.initRecalculateSummary("'.Url::to(['order/get-all-products-summary']).'", "product-summary");
		OrderForm.initRecalculateCustomData("'.Url::to(['order/get-custom-data-summary']).'", "'.Url::to(['order/get-custom-data-numeration']).'", "custom-tables");
        toggleAttributes();
        handleColourPicker("' . Url::to(['colour/get-modal-content']) . '");

    })();
    ', View::POS_READY);

if(Yii::$app->params['isOrderProductDimensionsVisible']) {
    $this->registerJs('
        $(document).ready(function() {
            localStorage.removeItem("lastId");
                
            $(document).on("keydown", function (e) {
                if (e.which == 9) {
                    e.preventDefault();
                    if($("#" + e.target.id).hasClass("tab-me")) {
                
                        var $others = $("#" + e.target.id).parents().addBack().nextAll().find("*").addBack().filter(".tab-me");
                
                        if($others.length == 0) {
                            $others = $(document).find(".tab-me");
                        }
                
                        $others.first().focus();
                        localStorage.setItem("lastId", $others.first().attr("id"));
                    }
                }
            });

            $(document).on("click", ".tab-me", function (e) {
                e.preventDefault();
                console.log($(this).attr("id"));
                localStorage.setItem("lastId", $(this).attr("id"));
            });
        });
                
        ', View::POS_READY);
}
?>

<?php
if(Yii::$app->params['isVisualisationVisible']) {
    $shapes = implode('-', Utility::getShapes());
    $this->registerJs('init("' . Url::to(['order/get-color-and-shape']) . '", "' . $shapes . '");', View::POS_READY);
}
?>
