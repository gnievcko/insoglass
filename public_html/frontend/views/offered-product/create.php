<?php
use yii\helpers\Url;
use yii\helpers\Html;
use common\components\ActiveForm;
use yii\web\View;
use common\helpers\StringHelper;
use common\helpers\Utility;

$this->registerJsFile('@web/js/forms.js?'.uniqid(), ['depends' => [\yii\web\JqueryAsset::className()]]);

$this->title = Yii::t('web', 'Create product') . ' - ' . Yii::$app->name;

$this->params['breadcrumbs'][] = ['label' => StringHelper::translateOrderToOffer('main', 'Orders'), 'url' => Url::to(['order/index'])];
$this->params['breadcrumbs'][] = ['label' => Yii::t('web', 'List of products'), 'url' => Url::to(['offered-product/list'])];
$this->params['breadcrumbs'][] = Yii::t('web', 'Create product');
?>

<div class="product-form" id="product-create">
	<h2 class="page-title"><?= Yii::t('web', 'Create product') ?></h2>
    
    <?php 
        $form = ActiveForm::begin([
            'id' => 'offered-products-form',
            'enableAjaxValidation' => true,
            'enableClientValidation' => false,
            'options' => [
                'class' => 'form',
            ]
        ]);
        echo $form->errorSummary([]);
    ?>
	<div class="col-xs-12 title-with-icon">
		<div class="base-icon box-icon main-content-icons"></div>
		<h3><?= Yii::t('web', 'Basic data') ?></h3>
	</div>
    <?= 
        Yii::$app->controller->renderPartial('inc/_basic_data', [
            'form' => $form,
            'shapes' => $shapes,
            'constructions' => $constructions,
            'muntins' => $muntins,
            'productionPaths' => $productionPaths,
            'offeredProductTranslationsForms' => $offeredProductTranslationsForms,
            'currencies' => $currencies,
            'offeredProductForm' => $offeredProductForm,
            'offeredProductCategories' => $offeredProductCategories,
        ]);
    ?>

	<?php // TODO: Dlaczego zależy od akurat tego parametru? 
	if(!Yii::$app->params['isOrderProductDimensionsVisible']) {?>
        <?= 
            Yii::$app->controller->renderPartial('/inc/product_variants', [
                'form' => $form,
                'productVariantsForms' => $productVariantsForms,
            	'isWarehouse' => false,
            ]);
        ?>
    <?php } ?>
    <?= 
        Yii::$app->controller->renderPartial('/inc/productAttributeForm', [
            'form' => $form,
            'model' => $productAttributeForm,
            'prefix' => null,
        ]);
    ?>
    
    <?php if(Yii::$app->params['isOfferedProductDependOnProduct'] && $type == Utility::PRODUCT_TYPE_OFFERED_PRODUCT) { ?>
    	<?= Yii::$app->controller->renderPartial('inc/requiredProducts', [
    	    'form' => $form, 
    	    'requiredProductsForms' => $requiredProductsForms
    	]); ?>
    <?php } ?>

    <?= 
        Yii::$app->controller->renderPartial('/inc/files', [
            	'form' => $form,
            	'photosForms' => $photosForms,
        		'documentsForms' => $documentsForms,
        		'arePhotosAvailable' => true,
        		'areAttachmentsAvailable' => true,
        ]);
    ?>

    <div class="clearfix">
        <?= Html::a(Yii::t('web', 'Cancel'), Url::to(['offered-product/list']), ['class' => 'btn btn-default pull-left']) ?>
        <button class="btn btn-primary pull-right"><?= Yii::t('web', 'Add') ?></button>
    </div>

    <?php ActiveForm::end() ?>
</div>

<?php $this->registerJs('
    (function() {
        var dynamicForm = new DynamicForm("offered-products-form");
        dynamicForm.activateRemoveButtons();
        dynamicForm.activateAddItemButtons();
        dynamicForm.activatePhotoUpload();
		dynamicForm.activateDocumentUpload();
    })();
    ', View::POS_END); 
?>
