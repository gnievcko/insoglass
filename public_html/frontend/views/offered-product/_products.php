<?php 
use yii\helpers\Url;
use yii\helpers\Html;
use frontend\models\OrdersListForm;
use yii\widgets\LinkPager;
use frontend\models\CompanyListForm;
use frontend\helpers\AddressHelper;
use common\bundles\EnhancedDialogAsset;
use frontend\models\OfferedProductListForm;
use common\widgets\TableHeaderWidget;
?>

<?php 
if (empty($items)) { ?>
	<label><?= Yii::t('web', 'No search results');?></label>	
	<br><br>				
<?php }
else { ?>
	<div class=" table-responsive">
		<table class="<?= TableHeaderWidget::TABLECLASS ?>">
	    	<?php echo TableHeaderWidget::widget(['fields' => OfferedProductListForm::getSortFields() , 'listForm' => $listForm]);?>
	        <tbody>
	            <?php foreach($items as $item): ?>
	                <tr>
                    <?php if(Yii::$app->params['isProductPhotoOnListVisible']){ ?>
                        <td><span class="photo-container">
							<?= '<div class="photo-medium" style="background: url('.(!empty($item['urlPhoto']) ? \common\helpers\UploadHelper::getNormalPath($item['urlPhoto'], 'product_photo') : Url::to('@web/images/placeholders/product.png')).')"></div>'; ?>
							<?php if(!empty($item['urlPhoto'])) { ?>
								<div class="photo-tooltip" data-id="<?= $item['id'] ?>" data-type="<?= $item['type'] ?>"></div>
							<?php } ?>
						</span></td>
                    <?php } ?>
	                    <td><?= Html::encode($item['name']) ?></td>
	                    <?php if(\Yii::$app->params['isProductSymbolEditable']) {?>
	                    <td><?= Html::encode($item['symbol'])?></td>
	                    <?php }?>   
	                    <td><?= Html::encode($item['category']) ?></td>
	                    <td><?= Html::encode($item['version']) ?></td>
	                    <td><?= Html::encode(!empty($item['isAvailable']) ? Yii::t('web', 'Available') : Yii::t('web', 'Not available')) ?></td>
	                    <td><?php 
	                    	echo Html::a('', Url::to(['offered-product/details', 'id' => $item['id'], 'type' => $item['type']]), ['class' => 'base-icon details-icon action-icon', 'title' => Yii::t('web', 'Details')]);
	                    	if(Yii::$app->params['canCudOfferedProducts']) {
		                    	echo '&nbsp;'.Html::a('', Url::to(['offered-product/edit', 'id' => $item['id'], 'type' => $item['type']]), ['class' => 'base-icon editing-icon action-icon', 'title' => Yii::t('web', 'Edit')]);
		                    	echo '&nbsp;'.Html::a('', Url::to(['offered-product/delete']), [
										'class' => 'base-icon delete-icon action-icon',
	                                    'title' => Yii::t('main', 'Delete'),
										'data' => [
								                'confirm' => Yii::t('main', 'Are you sure you want to delete this item?'),
								                'method' => 'post',
												'params' => [
														'id' => $item['id'],
														'type' => $item['type'],
												],
							            ],		
								]);
	                    	}
	                    ?>
	                </tr>
	            <?php endforeach ?>
	        </tbody>
	    </table>
	</div>
	<div class="col-sm-12">
		<div class="col-sm-4 text-left dynamic-table-counter">
			<?= Yii::t('web', 'Number of found items: {n}', ['n' => $pages->totalCount]) ?>
		</div>
		<div class="col-sm-8 text-right dynamic-table-pagination">
		    <?= LinkPager::widget(['pagination' => $pages ]) ?>
		</div>
	</div>
<?php }?>

<input class="dynamic-table-sort-dir" type="hidden" name="<?= Html::getInputName($listForm, 'sortDir') ?>" value="<?= $listForm->sortDir ?>" />
<input class="dynamic-table-sort-field" type="hidden" name="<?= Html::getInputName($listForm, 'sortField') ?>" value="<?= $listForm->sortField ?>" />
<input class="dynamic-table-page" type="hidden" name="<?= Html::getInputName($listForm, 'page') ?>" value="<?= $listForm->page ?>" />

<?php 
	EnhancedDialogAsset::register($this);
?>
