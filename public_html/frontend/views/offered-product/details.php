<?php
use yii\helpers\Html;
use yii\helpers\Url;
use frontend\helpers\UserHelper;
use dosamigos\gallery\Gallery;
use common\bundles\EnhancedDialogAsset;
use common\models\ar\User;
use common\helpers\StringHelper;
use common\helpers\Utility;

$this->title = Yii::t('web', 'Offered product details') . ' - ' . Yii::$app->name;

$this->params['breadcrumbs'][] = ['label' => StringHelper::translateOrderToOffer('main', 'Orders'), 'url' => Url::to(['order/index'])];
$this->params['breadcrumbs'][] = ['label' => Yii::t('web', 'List of offered products'), 'url' => Url::to(['offered-product/list'])];
$this->params['breadcrumbs'][] = Yii::t('web', 'Offered product details');
?>

<div class="order-details">
	<div class="row">
		<div class="col-sm-6"><h2 class="page-title"><?= Yii::t('web', 'Offered product details') ?></h2></div>
		<div class="col-sm-6">
		<?php if(Yii::$app->params['canCudOfferedProducts']) {?>
			<div class="pull-right"><?php 
				$buttons = null;
				$referrer = Yii::$app->request->getReferrer();
				if (substr($referrer, strripos($referrer, '/') + 1) == 'create') {
					$buttons .= Html::a(Yii::t('web', 'Add another product'),
										['offered-product/create'], ['class' => 'btn btn-primary']);
				}
				$buttons .= Html::a(Yii::t('main', 'Update'), 
						['offered-product/edit', 'id' => $data['id'], 'type' => $data['type']], ['class' => 'btn btn-default'])
				.Html::a(Yii::t('main', 'Delete'), 
						['offered-product/delete'], 
						[
								'class' => 'btn btn-default',
								'data' => [
						                'confirm' => Yii::t('main', 'Are you sure you want to delete this item?'),
						                'method' => 'post',
										'params' => [
												'id' => $data['id'],
												'type' => $data['type'],
										],
					            ],		
						]);
				echo $buttons; 
			?></div>
		<?php }?>
		</div>
	</div>
<div class="col-xs-12 title-with-icon">
	<div class="base-icon basic-data-icon main-content-icons"></div><h3 class="page-subtitle"><?= Yii::t('web', 'General information') ?></h3>
</div>	
	<div class="table-responsive">
		
		<table class="table table-bordered detail-table">
	    	<tbody>
	    		<tr>
	    			<th><?= Yii::t('web', 'Name') ?></th>
	    			<td><?= Html::encode($data['name']) ?></td>
	    		</tr>
	    		<?php if(Yii::$app->params['isProductSymbolEditable']) {?>
	    		<tr>
	    			<th><?= Yii::t('web', 'Code') ?></th>
	    			<td><?= Html::encode($data['symbol']) ?></td>
	    		</tr>
	    		<?php }?>
	    		<tr>
	    			<th><?= Yii::t('web', 'Addition date') ?></th>
	    			<td><?= Html::encode(StringHelper::getFormattedDateFromDate($data['dateCreation'])) ?></td>
	    		</tr>
	    		<?php if(!empty($data['price'])) { ?> 
	    			<tr>
	    				<th><?= Yii::t('web', 'Price') ?></th>
	    				<td><?= $data['price'].' '.$data['currency'] ?></td>
	    			</tr>
				<?php } ?>
				<?php if(!empty($data['unit'])) { ?> 
	    			<tr>
	    				<th><?= Yii::t('main', 'Unit') ?></th>
	    				<td><?= Html::encode($data['unit']) ?></td>
	    			</tr>
				<?php } ?>
	    		<tr>
	    			<th><?= Yii::t('web', 'Categories') ?></th>
	    			<td><?= Html::encode($data['category']) ?></td>
	    		</tr>
	    		<?php if(!empty($data['version'])) { ?>
		    		<tr>
		    			<th><?= Yii::t('web', 'Versions') ?></th>
		    			<td><?= Html::encode($data['version']) ?></td>
		    		</tr>
	    		<?php } ?>
	    		<tr>
	    			<th><?= Yii::t('web', 'Is available for offer') ?></th>
	    			<td><?= Html::encode(!empty($data['isAvailable']) ? Yii::t('web', 'Available') : Yii::t('web', 'Not available')) ?></td>
	    		</tr>
	    		<tr>
	    			<th><?= Yii::t('web', 'Added by') ?></th>
	    			<td><?= Html::a(Html::encode(UserHelper::getPrettyUserName
		    						($data['email'], $data['firstName'], $data['lastName'])) , 
		    						['employee/details', 'id' => User::find()->where(['email' => $data['email']])->one()->id]);
	    			 ?></td>
	    		</tr>
	    		<?php if(!empty($attachments)) { ?>
					<tr>
                        <th><?= Yii::t('web', 'Attachments') ?></th>
                    	<td><?= $this->render('/inc/attachmentList', ['attachments' => $attachments]) ?></td>
                	</tr>
                <?php } ?>
	    	</tbody>
		</table>
	</div>	

<?php if(Yii::$app->params['isOrderProductDimensionsVisible']) {?>
    <div class="col-xs-12 title-with-icon">
    	<div class="base-icon basic-data-icon main-content-icons"></div><h3 class="page-subtitle"><?= Yii::t('web', 'Parameters') ?></h3>
    </div>	
    
    <div class="table-responsive">
		<table class="table table-bordered detail-table">
	    	<tbody>
	    		<tr>
	    			<th><?= Yii::t('web', 'Width') . ' [mm]'?></th>
	    			<td><?= Html::encode($data['width']) ?></td>
	    		</tr>
	    		<tr>
	    			<th><?= Yii::t('web', 'Height') . ' [mm]'?></th>
	    			<td><?= Html::encode($data['height']) ?></td>
	    		</tr>
	    		<tr>
	    			<th><?= Yii::t('web', 'Shape') ?></th>
	    			<td><?= Html::encode($data['shape']) ?></td>
	    		</tr>
	    	</tbody>
		</table>
	</div>	
<?php }?>

<?php if(Yii::$app->params['isProductAttributesVisible']) {?>
	<div class="col-xs-12">
    	<div class="row">
    	<?= 
            Yii::$app->controller->renderPartial('/inc/productAttributeSummary', [
                'groups' => $groups,
            ]);
        ?>
        </div>
    </div>
<?php }?>
	<div class="col-xs-12 title-with-icon">
		<div class="base-icon description-icon main-content-icons"></div><h3 class="page-subtitle"><?= Yii::t('main', 'Description') ?></h3>
	</div>
	<div class="col-xs-12">
		<div class="description">
			<div class="row">
				<?php if(!empty($data['description'])) { ?>	
				<div class="col-sm-8 desc">
					<?php echo nl2br(Html::encode($data['description']));?>
				</div>
				<?php } else {?>
				<div class="col-sm-8 desc no-desc">
					<?php echo Yii::t('web','No data');?>
				</div>
				<?php } ?>
			</div>
		</div>
	</div>
	
	<?php if(Yii::$app->params['isOfferedProductDependOnProduct'] && $type == Utility::PRODUCT_TYPE_OFFERED_PRODUCT) {
        echo Yii::$app->controller->renderPartial('_required_products_list', $listData + ['offeredProductId' => $data['id'], 'type' => $type]); 
    } ?>
	
	<?php if(!empty($photos)) { ?>
		<div class="gallery">
			<h3 class="page-subtitle"><?= Yii::t('web', 'Photo gallery') ?></h3>
			<?= Gallery::widget(['items' => $photos]); ?>
		</div>
	<?php } ?>
</div>
<?php 
	EnhancedDialogAsset::register($this);
?>
