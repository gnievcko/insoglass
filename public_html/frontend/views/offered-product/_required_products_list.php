<?php
use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\View;

$this->registerJsFile('@web/js/dynamic-table.js?'.uniqid(), []);
?>
<div class="col-xs-12 title-with-icon">
	<div class="base-icon delivery-icon main-content-icons"></div>
	<h3 class="page-subtitle"><?= Yii::t('web', 'Required intermediate products') ?></h3>
</div>
<div class="col-xs-12">
	<div class="row">
		<div id="offered-product-required-products-list-table" class="dynamic-table">
		    <div class="row dynamic-table-filters">
		        <div class="col-sm-2">
		        <label><?php echo Yii::t('web','Product name')?></label>
		                <input 
		                    value="<?= $listForm->name ?>" 
		                    class="form-control dynamic-table-filter" 
		                    type="text"
		                    placeholder="<?php echo Yii::t('web','Search by name')?>"
		                    name="<?= Html::getInputName($listForm, 'name') ?>">
		           
		        </div> 
		    </div>
		    <div class="dynamic-table-data">
		        <?= Yii::$app->controller->renderPartial('_required_products', [
		                'requiredProducts' => $requiredProducts,
		                'pages' => $pages,
		                'listForm' => $listForm,
		        ]) ?>
		    </div>
		</div>
		<?php $this->registerJs('dynamicTable("offered-product-required-products-list-table", "'.Url::to(['offered-product/details', 'id' => $offeredProductId, 'type' => $type]).'");', View::POS_END); ?>
	</div>
</div>
