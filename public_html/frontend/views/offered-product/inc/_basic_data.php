<?php
    use kartik\select2\Select2;
    use yii\helpers\Url;
    use yii\web\JsExpression;
?>

    <?php foreach($offeredProductTranslationsForms as $i => $translationForm) { ?>
        <?= $form->field($translationForm, "[{$i}]languageId")->hiddenInput()->label(false) ?>
        <div class="col-xs-12">
        	<div class="row">
	            <div class="col-sm-6">
	                <?= $form->field($translationForm, "[{$i}]name")->textArea(['placeholder' => Yii::t('web','Product name'), 'class' => 'form-control smallTextarea']) ?>
	            </div>
	            <div class="col-sm-5 col-sm-offset-1">
	            	<div class="row">
	            		<div class="col-xs-4">
	            			<?= $form->field($translationForm, "[{$i}]price")->textInput(['type' => 'number', 'step' => 0.01, 'placeholder' => $translationForm->getAttributeLabel('price')]) ?>
	            		</div>
	            		<div class="col-xs-4">
	            			<?= $form->field($translationForm, "[{$i}]currencyId")->dropDownList($currencies) ?>
	            		</div>
	            		<?php if(!Yii::$app->params['isOrderProductDimensionsVisible']) { ?>
	            		<div class="col-xs-4">
							<?php if(!empty(Yii::$app->params['defaultUnit'])): ?>
	                			<?= $form->field($translationForm, "[{$i}]unit")->textInput(['maxLength' => true, 'value' => Yii::t('documents',Yii::$app->params['defaultUnit']),'readonly'=>true]) ?>
		                	<?php else: ?>
	                			<?= $form->field($translationForm, "[{$i}]unit")->textInput(['maxLength' => true, 'placeHolder' => $translationForm->getAttributeLabel('unit')]) ?>
	                		<?php endif; ?>  	
	            		</div>
	            		<?php } ?>
	            	</div>
	            </div>
            </div>
        </div>
        <?php if(Yii::$app->params['isProductSymbolEditable']) {?>
        <div class="col-xs-12">
        	<div class="row">
	        	<div class="col-sm-6">
	        		<?= $form->field($offeredProductForm, 'symbol')->textInput(['maxLength' => true, 'placeHolder' => $offeredProductForm->getAttributeLabel('symbol')]) ?>
	        		<?= $form->field($offeredProductForm, 'id')->hiddenInput()->label(false) ?>
	        	</div>
	       	</div>
	    </div>
        
        <?php }?>
        
        
        <div class="col-xs-12">
        	<div class="row">
	        	<div class="col-sm-6">
		            <?= $form->field($offeredProductForm, 'categoriesIds')->widget(Select2::classname(), [
		                    'language' => Yii::$app->language,
		                    'showToggleAll' => false,
		                    'options' => ['placeholder' => Yii::t('main','Choose category')],
		                    'data' => array_map(function($category) { return [$category->offered_product_category_id => $category->name]; }, $offeredProductCategories),
		                    'pluginOptions' => [
		                            'multiple' => true,
		                            'allowClear' => true,
		                            'minimumInputLength' => 1,
		                            'ajax' => [
		                                    'url' => Url::to(['offered-product-category/find']),
		                                    'dataType' => 'json',
		                                    'delay' => 250,
		                                    'data' => new JsExpression('function(params) { 
		                                        return {term:params.term, page:params.page, size: 20}; 
		                                    }'),
		                                    'processResults' => new JsExpression('function (data, params) { 
		                                        params.page = params.page || 1; 
		                                        
		                                        return {
		                                            results: data.items,
		                                            pagination: {
		                                                more: data.more
		                                            }
		                                        };
		                                    }'),
		                                    'cache' => true
		                            ],
		                            'templateResult' => new JsExpression('function(item) { if(item.loading) return item.text; return item.name; }'),
		                            'templateSelection' => new JsExpression('function (item) { return item.name || item.text; }'),
		                    ]
		                ]);
		            ?>
                    <a href="<?= Url::to(['offered-product-category/create']); ?>" target="_blank" class="focusable-link  add-item-after-select2  add-item-after-select2-no-required">
                        <span class="glyphicon glyphicon-plus-sign pull-left"></span>
                        <span> <?=Yii::t('web', 'Create new category')?></span>
                    </a>
	       		</div>
	       		<div class="col-sm-5 col-sm-offset-1">
	                <?= $form->field($offeredProductForm, 'isAvailable')->checkbox() ?>
	            </div>
	    	</div>
        </div>
        <div class="col-xs-12">
        	<div class="row">
	            <div class="col-sm-6">
	                <?= $form->field($translationForm, "[{$i}]description")->textarea(['placeholder' => Yii::t('web','Description')]) ?>
	            </div>
	            <?php if(Yii::$app->params['isProductionVisible']) { ?>
    	            <div class="col-sm-6">
    	                <?= $form->field($offeredProductForm, "productionPathId")->dropDownList($productionPaths, ['prompt' => Yii::t('web','Select production path')]) ?>
    	            </div>
	            <?php }?>
        	</div>    
        </div>
    <?php } ?>
    <?php if(Yii::$app->params['isOrderProductDimensionsVisible']) {?>
    	<div class="col-xs-12 title-with-icon">
    		<div class="base-icon box-icon main-content-icons"></div>
    		<h3><?= Yii::t('web', 'Parameters') ?></h3>
		</div>
        <div class="col-xs-12">
        	<div class="row">
            	<div class="col-sm-2">
            		<?= $form->field($offeredProductForm, "width")->textInput(['placeholder' => $offeredProductForm->getAttributeLabel('width')])?>
            	</div>
            	<div class="col-sm-2">
            		<?= $form->field($offeredProductForm, "height")->textInput(['placeholder' => $offeredProductForm->getAttributeLabel('height')])?>
            	</div>
            	<div class="col-sm-2">
            		<?= $form->field($offeredProductForm, "shapeId")->dropDownList($shapes, ['prompt' => Yii::t('web', 'Select shape')]) ?>
            	</div>
           	</div>
        </div>
	<?php }?>