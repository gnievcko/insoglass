<?php
use kartik\widgets\Select2;
use yii\web\JsExpression;
use yii\helpers\Url;
use yii\web\View;
?>
<div class="product-select">
	<div class="col-xs-12 title-with-icon">
		<div class="base-icon delivery-icon main-content-icons"></div>
		<h3><?= Yii::t('web', 'Required intermediate products') ?></h3>
	</div>
	<div class="col-xs-12">
		<div id="required-products" class="items-container">
		    <?php 
		        foreach($requiredProductsForms as $prefix => $requiredProductForm) {
		            echo Yii::$app->controller->renderPartial('inc/requiredProduct', ['form' => $form, 'requiredProductForm' => $requiredProductForm, 'prefix' => $prefix]);
		        }
		    ?>
		</div>
	</div>
	<div class="col-xs-12">
		<div class="row">
		    <div class="col-sm-6">
		        <?= Select2::widget([
		                'id' => 'select2-search-product',
		                'name' => 'select2-search-product',
		                'language' => Yii::$app->language,
		                'options' => ['placeholder' => Yii::t('web', 'Select a product...')],
		                'data' => null,
		                'pluginOptions' => [
		                        'allowClear' => true,
		                        'minimumInputLength' => 1,
		                        'ajax' => [
		                                'url' => Url::to(['product/find']),
		                                'dataType' => 'json',
		                                'delay' => 250,
		                                'data' => new JsExpression('function(params) { return { term:params.term, page:params.page, size: 20 }; }'),
		                                'processResults' => new JsExpression('function (data, params) {
		                                    params.page = params.page || 1;
		
		                                    return {
		                                        results: data.items,
		                                        pagination: {
		                                            more: data.more
		                                        }
		                                    };
		                                }'),
		                                'cache' => true
		                        ],
		                        'templateResult' => new JsExpression('function(item) { if(item.loading) return item.text; return item.name; }'),
		                        'templateSelection' => new JsExpression('function (item) { return item.name || item.text; }'),
		                ],
		            ]); 
		        ?>
		    </div>
		</div>
	</div>
</div>

<?php
    $this->registerJs('
        (function() {
            var formId = "offered-product-required-product-form";
            var dynamicForm = new DynamicForm(formId);
            dynamicForm.activateRemoveButtons();

            var $addProductSelect = $("#select2-search-product");
            var $productsContainer = $("#required-products");

            $addProductSelect.on("change", function() {
                var productId = $addProductSelect.val();
                var chosenProductsIds = {};
                $productsContainer.find(".product-id").each(function(_, idField) {
                    var id = $(idField).val();
                    chosenProductsIds[id] = id;
                });
                if(productId != "" && chosenProductsIds[productId] === undefined) {
                    dynamicForm.addItem($productsContainer, "'.Url::to(['offered-product/get-required-product-form']).'", {productId: productId});
                }

                $addProductSelect.val("");
            });
        })();
    ', View::POS_END); 
?>