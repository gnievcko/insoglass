<?php
    use yii\helpers\Html;
    use yii\web\View;

    $formBaseName = $requiredProductForm->formName();
    $activeFormBaseName = strtolower($formBaseName);
    $formName = "{$activeFormBaseName}-{$prefix}";
?>

<div class="row item required-product" data-form-base-name="<?= $formBaseName ?>" data-active-form-base-name="<?= $activeFormBaseName ?>">
    <div class="col-sm-2 product-thumbnail-container">
        <?= Html::img($requiredProductForm->thumbnail) ?>
    </div>

    <div class="col-sm-10">
        <div class="row">
            <div class="col-xs-12 product-name">
                <?= $requiredProductForm->name ?>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-2">
                <?php $inputOptions = ['type' => 'number', 'min' => 0, 'step' => 1, 'integer' => true, 'class' => 'form-control product-count'] ?>
                <?php if(empty($form)): ?>
                    <?php $fieldId = "{$formName}-count"; ?>

                    <div id="container-<?= $fieldId ?>" class="form-group <?= $requiredProductForm->isAttributeRequired('count') ? 'required' : '' ?>">
                        <?=  Html::activeTextInput($requiredProductForm, "[{$prefix}]count", $inputOptions + ['id' => "{$fieldId}"]) ?>
                        <div class="help-block"></div>
                    </div>

                    <?php
                        $this->registerJs('
                            (function() {
                                $("#'.$formId.'").yiiActiveForm("add", {
                                    id: "'.$fieldId.'",
                                    name: "'."[{$prefix}]count".'",
                                    container: "#container-'.$fieldId.'",
                                    input: "#'."$fieldId".'",
                                    enableClientValidation: false,
                                    enableAjaxValidation: true
                                });
                            })();
                            ', View::POS_END); 
                    ?>
                <?php else: ?>
                    <?= $form->field($requiredProductForm, "[{$prefix}]count")->textInput($inputOptions); ?>
                <?php endif ?>
            </div>
            <div class="col-sm-1">
                    <?php if(empty($form)): ?>
                        <?php $fieldId = "{$formName}-unit"; ?>

                        <div id="container-<?= $fieldId ?>" class="form-group <?= $requiredProductForm->isAttributeRequired('unit') ? 'required' : '' ?>">
                        	<?php if(!empty(Yii::$app->params['defaultUnit'])): ?>
                        	     <?=  Html::activeTextInput($requiredProductForm, "[{$prefix}]unit", ['class' => 'form-control', 'id' => "{$fieldId}", 'value' => Yii::t('documents',Yii::$app->params['defaultUnit']),'readonly'=>true]) ?>
                        	<?php else: ?>
                            	<?=  Html::activeTextInput($requiredProductForm, "[{$prefix}]unit", ['class' => 'form-control', 'id' => "{$fieldId}", 'placeholder' => Yii::t('main', 'Unit')]) ?>
                            <?php endif; ?>
                            <div class="help-block"></div>
                        </div>

                        <?php
                            $this->registerJs('
                                (function() {
                                    $("#'.$formId.'").yiiActiveForm("add", {
                                        id: "'.$fieldId.'",
                                        name: "'."[{$prefix}]unit".'",
                                        container: "#container-'.$fieldId.'",
                                        input: "#'."$fieldId".'",
                                        enableClientValidation: false,
                                        enableAjaxValidation: true
                                    });
                                })();
                                ', View::POS_END); 
                        ?>
                    <?php else: ?>
                        <?= $form->field($requiredProductForm, "[{$prefix}]unit")->textInput(); ?>
                    <?php endif ?>
            </div>
            <div class="col-sm-4">
                    <?php if(empty($form)): ?>
                        <?php $fieldId = "{$formName}-note"; ?>

                        <div id="container-<?= $fieldId ?>" class="form-group <?= $requiredProductForm->isAttributeRequired('note') ? 'required' : '' ?>">
                            <?=  Html::activeTextInput($requiredProductForm, "[{$prefix}]note", ['class' => 'form-control', 'id' => "{$fieldId}", 'placeholder' => Yii::t('main','Description')]) ?>
                            <div class="help-block"></div>
                        </div>

                        <?php
                            $this->registerJs('
                                (function() {
                                    $("#'.$formId.'").yiiActiveForm("add", {
                                        id: "'.$fieldId.'",
                                        name: "'."[{$prefix}]note".'",
                                        container: "#container-'.$fieldId.'",
                                        input: "#'."$fieldId".'",
                                        enableClientValidation: false,
                                        enableAjaxValidation: true
                                    });
                                })();
                                ', View::POS_END); 
                        ?>
                    <?php else: ?>
                        <?= $form->field($requiredProductForm, "[{$prefix}]note")->textInput(); ?>
                    <?php endif ?>
            </div>
            <div class="col-sm-1">
                <div class="base-icon garbage-icon action-icon remove-item-btn" style="margin-top: 3px"></div>
                <?= Html::activeHiddenInput($requiredProductForm, "[{$prefix}]productId", ['class' => 'product-id']) ?>
            </div>
        </div>
    </div>
</div>
