<?php 
use yii\helpers\Url;
use yii\helpers\Html;
use yii\widgets\LinkPager;
use common\widgets\TableHeaderWidget;
use frontend\models\OfferedProductRequiredProductListForm;
?>

<?php if(empty($requiredProducts)) { ?>
	<label><?= Yii::t('web', 'No search results');?></label>			
<?php }
else { ?>
	<div class="table-responsive">
		<table class="<?= TableHeaderWidget::TABLECLASS ?>">
	    	<?php echo TableHeaderWidget::widget(['fields' => OfferedProductRequiredProductListForm::getSortFields(), 'listForm' => $listForm]);?>
	        <tbody>
	            <?php foreach($requiredProducts as $requiredProduct): ?>
	                <tr>
                        <?php if(Yii::$app->params['isProductPhotoOnListVisible']){ ?>
                            <td class="photo-container">
                                <?= Html::img(empty($requiredProduct['urlThumbnail']) ? '@web/images/placeholders/product.png' : $requiredProduct['urlThumbnail']) ?>
                            </td>
                        <?php } ?>
	                    <td><?= Html::encode($requiredProduct['name']) ?></td>
	                    <td><?= Html::encode($requiredProduct['count']) ?></td>
	                    <td><?= Html::encode($requiredProduct['unit']) ?></td>
                        <td>
                            <?= $requiredProduct['note'] ?>
                        </td>
	                    <td><?= Html::a('', Url::to(['warehouse-product/details', 'productId' => $requiredProduct['id']]), ['class' => 'base-icon details-icon action-icon']) ?></td>
	                </tr>
	            <?php endforeach; ?>
	        </tbody>
	    </table>
	</div>
	<div class="text-right dynamic-table-pagination">
	    <?= LinkPager::widget(['pagination' => $pages ]) ?>
	</div>
<?php } ?>

<input class="dynamic-table-sort-dir" type="hidden" name="<?= Html::getInputName($listForm, 'sortDir') ?>" value="<?= $listForm->sortDir ?>" />
<input class="dynamic-table-sort-field" type="hidden" name="<?= Html::getInputName($listForm, 'sortField') ?>" value="<?= $listForm->sortField ?>" />
<input class="dynamic-table-page" type="hidden" name="<?= Html::getInputName($listForm, 'page') ?>" value="<?= $listForm->page ?>" />
