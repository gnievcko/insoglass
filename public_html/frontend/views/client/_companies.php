<?php

use yii\helpers\Url;
use yii\helpers\Html;
use common\helpers\UserNameHelper;
use yii\widgets\LinkPager;
use frontend\models\CompanyListForm;
use frontend\helpers\AddressHelper;
use common\bundles\EnhancedDialogAsset;
use common\widgets\TableHeaderWidget;
use common\helpers\StringHelper;
?>

<?php if(empty($companies)) { ?>
    <label><?= Yii::t('web', 'No search results'); ?></label>
<?php } else {
    ?>
    <div class=" table-responsive">
        <table class="<?= TableHeaderWidget::TABLECLASS ?>">
            <?php echo TableHeaderWidget::widget(['fields' => CompanyListForm::getSortFields(), 'listForm' => $companyListForm]); ?>

            <tbody>
                <?php foreach($companies as $company): ?>
                    <tr>
                        <td><?= Html::encode($company['name']) ?></td>                    
                        <td><?= Html::encode($company['email']) ?></td>
                        <td><?= Html::encode($company['phone']) ?></td>
                        <td><?=
                            Html::encode(AddressHelper::getFullAddress([
                                        'addressMain' => $company['addressMain'],
                                        'addressComplement' => $company['addressComplement'],
                                        'cityName' => $company['cityName'],
                                        'cityZipCode' => $company['cityZipCode'],
                                        'provinceName' => $company['provinceName'],
                                        'countryName' => null
                            ]))
                            ?></td>
                        <td><?= Html::encode(StringHelper::getFormattedDateFromTimestamp($company['dateCreation'])) ?></td>
                        <td><?= Html::encode(UserNameHelper::generateDisplayedName(['first_name' => $company['creatorFirstName'], 'last_name' => $company['creatorLastName']])) ?></td>	                    
                        <td><?php
                            echo Html::a('', Url::to(['client/details', 'id' => $company['id']]), ['class' => 'base-icon details-icon action-icon', 'title' => Yii::t('web', 'Details')]);
                            if(Yii::$app->params['isClientManagementVisibie']) {
                                echo '&nbsp;' . Html::a('', Url::to(['client/edit', 'id' => $company['id']]), ['class' => 'base-icon editing-icon action-icon', 'title' => Yii::t('web', 'Edit')]);
                                echo '&nbsp;' . Html::a('', Url::to(['client/delete']), [
                                    'class' => 'base-icon delete-icon action-icon',
                                    'title' => Yii::t('main', 'Delete'),
                                    'data' => [
                                        'confirm' => Yii::t('main', 'Are you sure you want to delete this item?'),
                                        'method' => 'post',
                                        'params' => [
                                            'id' => $company['id'],
                                        ],
                                    ],
                                ]);
                            }
                            ?></td>
                    </tr>
                <?php endforeach ?>
            </tbody>
        </table>
    </div>
    <div class="col-sm-12">
        <div class="col-sm-4 text-left dynamic-table-counter">
            <?= Yii::t('web', 'Number of found items: {n}', ['n' => $pages->totalCount]) ?>
        </div>
        <div class="col-sm-8 text-right dynamic-table-pagination">
            <?= LinkPager::widget(['pagination' => $pages, 'firstPageLabel' => true, 'lastPageLabel' => true]) ?>
        </div>
    </div>
<?php } ?>

<input class="dynamic-table-sort-dir" type="hidden" name="<?= Html::getInputName($companyListForm, 'sortDir') ?>" value="<?= $companyListForm->sortDir ?>" />
<input class="dynamic-table-sort-field" type="hidden" name="<?= Html::getInputName($companyListForm, 'sortField') ?>" value="<?= $companyListForm->sortField ?>" />
<input class="dynamic-table-page" type="hidden" name="<?= Html::getInputName($companyListForm, 'page') ?>" value="<?= $companyListForm->page ?>" />

<?php
EnhancedDialogAsset::register($this);
?>