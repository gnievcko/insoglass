<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\View;
use dosamigos\multiselect\MultiSelect;
use common\helpers\StringHelper;
use common\helpers\Utility;

$this->title = Yii::t('web', 'List of clients') . ' - ' . Yii::$app->name;

$this->params['breadcrumbs'][] = ['label' => StringHelper::translateOrderToOffer('main', 'Orders'), 'url' => Url::to(['order/index'])];
$this->params['breadcrumbs'][] = Yii::t('web', 'List of clients');

$this->registerJsFile('@web/js/dynamic-table.js?' . uniqid(), []);
?>

<div id="client-list-wrapper">
    <div class="row">
        <div class="col-sm-8 col-xs-12"><h2 class="page-title"><?= Yii::t('web', 'List of clients') ?></h2></div>
        <?php if(Yii::$app->params['isClientManagementVisibie']) { ?>
            <div class="col-sm-4 col-xs-12 text-right"><?= Html::a(Yii::t('web', 'Add client'), ['client/create'], ['class' => 'btn btn-primary']) ?></div>
        <?php } ?>
    </div>

    <div id="client-list-table" class="dynamic-table">
        <h5><?php echo Yii::t('main', 'Filters') ?></h5>
        <div class="row dynamic-table-filters">
            <div class="col-md-3 col-sm-6">
                <label><?php echo Yii::t('web', 'Search by name') ?></label>
                <input
                    value="<?= $companyListForm->companyName ?>"
                    class="form-control dynamic-table-filter"
                    type="text"
                    placeholder="<?= $companyListForm->getAttributeLabel('companyName') ?>"
                    name="<?= Html::getInputName($companyListForm, 'companyName') ?>">
            </div>
            <div class="col-md-3 col-sm-6">
                <label><?php echo Yii::t('web', 'Search by city') ?></label>
                <input
                    value="<?= $companyListForm->cityName ?>"
                    class="form-control dynamic-table-filter"
                    type="text"
                    placeholder="<?= $companyListForm->getAttributeLabel('cityName') ?>"
                    name="<?= Html::getInputName($companyListForm, 'cityName') ?>">
            </div>
            <div class="col-md-3 col-sm-6 cat-select">
                <label><?php echo Yii::t('web', 'Filter by category') ?></label>
                <?php
                $companyGroupData = array_reduce($companyGroupTranslations, function($options, $t) {
                    $options[$t->company_group_id] = Html::encode($t->name);
                    return $options;
                }, []);
                if(!empty($companyGroupData)) {
                    //echo $companyListForm->getAttributeLabel('companyGroupIds');

                    reset($companyGroupData);
                    $key = key($companyGroupData);

                    echo MultiSelect::widget([
                        'id' => 'client-groups',
                        "options" => ['multiple' => 'multiple', 'class' => 'dynamic-table-filter'],
                        'data' => $companyGroupData,
                        'name' => Html::getInputName($companyListForm, 'companyGroupIds'),
                        'value' => empty($companyListForm->companyGroupIds) ? [$key] : $companyListForm->companyGroupIds,
                        "clientOptions" => [
                            'nonSelectedText' => '',
                            'nSelectedText' => mb_strtolower(Yii::t('main', 'Selected'), 'UTF-8'),
                            'numberDisplayed' => 2,
                            'buttonWidth' => '100%',
                        ],
                    ]);
                }
                ?>
            </div>
            <div class="col-md-3 col-sm-6">
                <label><?php echo Yii::t('web', 'Filter by client type') ?></label>
                <?php
                $values = [
                    Yii::t('web', 'All') => Yii::t('web', 'All'),
                    Utility::PARENT_COMPANY => Yii::t('web', 'Parent company'),
                    Utility::CHILD_COMPANY => Yii::$app->params['isChildCompanyCalledObject'] ? Yii::t('web', 'Object') : Yii::t('web', 'Child company'),
                ];

                echo Html::activeDropDownList($companyListForm, 'companyType', $values, ['class' => 'form-control dynamic-table-filter']);
                ?>
            </div>
            <?= Yii::$app->controller->renderPartial('/inc/listFiltersButtons', ['isMoreFilters' => false, 'isClearFilters' => true]) ?>
        </div>
        <div class="dynamic-table-data">
            <?=
            Yii::$app->controller->renderPartial('_companies', [
                'companies' => $companies,
                'pages' => $pages,
                'companyListForm' => $companyListForm,
            ])
            ?>
        </div>
    </div>
    <?php $this->registerJs('dynamicTable("client-list-table", "' . Url::to(['client/list']) . '");', View::POS_END); ?>
</div>
