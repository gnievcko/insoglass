<?php

use yii\helpers\Html;
use yii\helpers\Url;
use frontend\helpers\AddressHelper;
use common\bundles\EnhancedDialogAsset;
use common\helpers\StringHelper;

$this->title = Yii::t('web', 'Client details') . ' - ' . Yii::$app->name;

$this->params['breadcrumbs'][] = ['label' => StringHelper::translateOrderToOffer('main', 'Orders'), 'url' => Url::to(['order/index'])];
$this->params['breadcrumbs'][] = ['label' => Yii::t('web', 'List of clients'), 'url' => Url::to(['client/list'])];
$this->params['breadcrumbs'][] = Yii::t('web', 'Client details');
?>

<div class="client-details">
    <div class="row">
        <?php
        $referrer = Yii::$app->request->getReferrer();
        $referredFromCreate = (substr($referrer, strripos($referrer, '/') + 1) == 'create');
        ?>

        <div class="col-sm-<?= $referredFromCreate ? '3' : '4' ?>"><h2 class="page-title"><?= Yii::t('web', 'Client details') ?></h2></div>
        <div class="col-sm-<?= $referredFromCreate ? '9' : '8' ?> text-right">
            <?php
                $buttons = '';
                if($referredFromCreate) {
                    if(Yii::$app->params['isClientManagementVisibie']) {
                        $buttons .= Html::a('<span class="glyphicon glyphicon-plus"></span>&nbsp;' . Yii::t('web', 'Add another client'), ['client/create'], ['class' => 'btn btn-primary']);
                    }
                }
                $buttons .= Html::a(Yii::t('web', 'Add contact person'), ['client-representative/create', 'companyId' => $data['id']], ['class' => 'btn btn-primary btn-md']);
                $buttons .= Html::a(StringHelper::translateOrderToOffer('web', 'Add order'), ['order/create', 'companyId' => $data['id']], ['class' => 'btn btn-primary btn-md', 'target' => '_blank']);
                if(Yii::$app->params['isClientManagementVisibie']) {
                    $buttons .= Html::a(Yii::t('main', 'Update'), ['client/edit', 'id' => $data['id']], ['class' => 'btn btn-default']);
                    $buttons .= Html::a(Yii::t('main', 'Delete'), ['client/delete'], [
                                'class' => 'btn btn-default',
                                'data' => [
                                    'confirm' => Yii::t('main', 'Are you sure you want to delete this item?'),
                                    'method' => 'post',
                                    'params' => [
                                        'id' => $data['id'],
                                    ],
                                ],
                    ]);
                }
                echo $buttons;
                ?>
        </div>
    </div>
	
	<div class="col-xs-12 title-with-icon">
		<div class="base-icon basic-data-icon main-content-icons"></div><h3 class="page-subtitle"><?= Yii::t('web', 'General information') ?></h3>
	</div>
	<div class="col-xs-12">
		<div class="table-responsive">
			<table class="table table-bordered detail-table">
		    	<tbody>
		    		<tr>
		    			<th><?= Yii::t('web', 'Id') ?></th>
		    			<td><?= Html::encode($data['id']) ?></td>
		    		</tr>
		    		<tr>
		    			<th><?= Yii::t('web', 'Name') ?></th>
		    			<td><?= Html::encode($data['name']) ?></td>
		    		</tr>
		    		<?php if(!empty($data['parentCompanyName'])) { ?>
			    		<tr>
			    			<th><?= Yii::t('main', 'Parent company') ?></th>
			    			<td><?= Html::a(Html::encode($data['parentCompanyName']), ['client/details', 'id' => $data['parentCompanyId']]) ?></td>
			    		</tr>
		    		<?php } ?>
		    		<tr>
		    			<th><?= Yii::t('main', 'E-mail address') ?></th>
		    			<td><?= Html::mailto(Html::encode($data['email'])) ?></td>
		    		</tr>
		    		<tr>
		    			<th><?= Yii::t('main', 'Phone') ?></th>
		    			<td><?= $data['phone1'] ?></td>
		    		</tr>
		    		<?php if(!empty($data['phone2'])) { ?>
			    		<tr>
			    			<th><?= Yii::t('main', 'Alternative phone') ?></th>
			    			<td><?= $data['phone2'] ?></td>
			    		</tr>
		    		<?php } ?>
		    		<?php if(!empty($data['fax1'])) { ?>
		    			<tr>
		    				<th><?= Yii::t('main', 'Fax') ?></th>
		    				<td><?= $data['fax1'] ?></td>
		    			</tr>
		    		<?php } ?>
		    		<?php if(!empty($data['vatIdentificationNumber'])) { ?>
		    			<tr>
		    				<th><?= Yii::t('main', 'VAT Identification Number') ?></th>
		    				<td><?= $data['vatIdentificationNumber'] ?></td>
		    			</tr>
		    		<?php } ?>
		    		<?php if(!empty($data['taxpayerIdentificationNumber'])) { ?>
		    			<tr>
		    				<th><?= Yii::t('main', 'Taxpayer Identification Number') ?></th>
		    				<td><?= $data['taxpayerIdentificationNumber'] ?></td>
		    			</tr>
		    		<?php } ?>
		    		<?php if(!empty($data['krsNumber'])) { ?>
		    			<tr>
		    				<th><?= Yii::t('main', 'KRS number') ?></th>
		    				<td><?= $data['krsNumber'] ?></td>
		    			</tr>
		    		<?php } ?>
		    		<tr>
		    			<th><?= Yii::t('main', 'Address') ?></th>
		    			<td><?= AddressHelper::getFullAddress([
	                    		'addressMain' => $data['address1Main'],
	                    		'addressComplement' => $data['address1Complement'],
	                    		'cityName' => $data['city1Name'],
	                    		'cityZipCode' => $data['city1ZipCode'],
	                    		'provinceName' => $data['province1Name'],
	                    		'countryName' => $data['country1Name']
	                    ]) ?></td>
		    		</tr>
		    		<?php if(!empty($data['address2Main'])) { ?>
						<tr>
			    			<th><?= Yii::t('main', 'Address for correspondence') ?></th>
			    			<td><?= AddressHelper::getFullAddress([
		                    		'addressMain' => $data['address2Main'],
		                    		'addressComplement' => $data['address2Complement'],
		                    		'cityName' => $data['city2Name'],
		                    		'cityZipCode' => $data['city2ZipCode'],
		                    		'provinceName' => $data['province2Name'],
		                    		'countryName' => $data['country2Name']
		                    ]) ?></td>
			    		</tr>
					<?php } ?>
					<tr>
		    			<th><?= Yii::t('main', 'Creation date') ?></th>
		    			<td><?= Html::encode(StringHelper::getFormattedDateFromTimestamp(strtotime($data['dateCreation']), true)) ?></td>
		    		</tr>
		    		<?php if(!empty($contactPeople)) { ?>
			    		<tr>
			    			<th><?= Yii::t('web', 'Contact people') ?></th>
			    			<td><?= $this->render('/inc/internalUserList', ['users' => $contactPeople]) ?></td>
			    		</tr>
		    		<?php } ?>
		    		<tr>
		    			<th><?= Yii::t('web', 'Authorized salesmen') ?></th>
		    			<td><?= $this->render('/inc/usersRow', ['users' => $salesmen]) ?></td>
		    		</tr>
		    		<?php if(!empty($groups)) { ?>
			    		<tr>
			    			<th><?= Yii::t('web', 'Groups') ?></th>
			    			<td><?= $this->render('/inc/groupsRow', ['groups' => $groups]) ?></td>
			    		</tr>
		    		<?php } ?>
		    	</tbody>
			</table>
		</div>
	</div>
	
	<div class="col-xs-12 title-with-icon">
		<div class="base-icon description-icon main-content-icons"></div><h3 class="page-subtitle"><?= Yii::$app->params['isChildCompanyCalledObject'] ?  Yii::t('web', 'Objects') : Yii::t('web', 'Child companies') ?></h3>
	</div>	
	<div class="col-xs-12">
		<div class="row">
			<?php if(!empty($childCompanies)) { ?>
				<div class="col-sm-4">
					<div class="table-responsive">
						<table class="table table-bordered table-hover list-grid-table">
							<thead>
								<tr>
									<th><?=Yii::t('main', 'Name') ?></th>
								</tr>
							</thead>
							<tbody>
								<?php foreach($childCompanies as $childCompany) { ?>
									<tr>
										<td><?= Html::a(Html::encode($childCompany['name']), ['client/details', 'id' => $childCompany['id']])?></td>		
									</tr>
								<?php } ?>			
						</tbody>
						</table>
					</div>
				</div>
			<?php
			} else {
				?>
				<div class="col-sm-8 desc"><?= Yii::t('web', 'None'); ?></div>
			<?php } ?>
		</div>
	</div>
    <?php 
    	echo $this->renderFile('@frontend/views/document/list.php', [
    			'documents' => $documents, 
    			'documentsAbleToDelete' => $documentsAbleToDelete, 
    			'enableEditButton' => false,
    			'availableDocuments' => $availableDocuments,
    	]);
    ?>    
	
	<?php if(!empty($data['note'])) { ?>
		<div class="col-xs-12 title-with-icon">
			<div class="base-icon description-icon main-content-icons"></div><h3><?= Yii::t('main', 'Note') ?></h3>
		</div>
		<div class=" col-xs-12">
			<div class="row">
				<div class="col-sm-8 desc">
					<?= nl2br(Html::encode($data['note'])); ?>
				</div>
			</div>
		</div>
	<?php } ?>
</div>

<?php
EnhancedDialogAsset::register($this);
?>
