<?php
use yii\helpers\Url;

?>

<div class="col-xs-12 title-with-icon">
	<div class="base-icon contact-icon main-content-icons"></div> 
	<h3><?= Yii::t('web', 'Contact people') ?></h3>
</div>
<div id="contact-people" class="items-container">
    <?php
        if(!empty($contactPeopleForms)) {
	        foreach($contactPeopleForms as $prefix => $contactPersonForm) {
	            echo Yii::$app->controller->renderPartial('inc/_contactPerson', [
	                'contactPersonForm' => $contactPersonForm,
	                'prefix' => $prefix,
	                'form' => $form,
	            	'isEditScenarioSet' => $isEditScenarioSet,
	            ]);
	        }
        }
    ?>
</div>

<div class="row">
    <div class="col-sm-6">
        <div class="btn btn-link add-item-button" data-items-container-selector="#contact-people" data-item-view-url="<?= Url::to(['client/get-contact-person-form']) ?>">
            <span class="glyphicon glyphicon-plus-sign"></span>
            <span><?= Yii::t('web', 'Add contact person') ?></span>
        </div>
    </div>    
    <div class="col-sm-6 text-right">
        <div class="btn btn-link remove-all-items-btn" data-items-container-selector="#contact-people">
            <span class="glyphicon glyphicon-minus-sign"></span>
            <span>
                <?= Yii::t('web', 'Remove all contact people') ?>
            </span>
        </div>
    </div>
</div>
