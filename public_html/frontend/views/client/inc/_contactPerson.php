<?php
use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\JsExpression;
use common\helpers\Utility;
use yii\web\View;
?>

<?php
    $formBaseName = $contactPersonForm->formName();
    $activeFormBaseName = strtolower($formBaseName);
    $formName = "{$activeFormBaseName}-{$prefix}";
?>

<div class="item row col-sm-12" data-form-base-name="<?= $formBaseName ?>" data-active-form-base-name="<?= $activeFormBaseName ?>">
	<div class="col-sm-2">
		<?php $inputOptions = ['type' => 'text', 'class' => 'form-control firstName'];
			if(Yii::$app->params['isPlaceholderInputLabel']) {
				$inputOptions['placeholder'] = $contactPersonForm->getAttributeLabel('firstName');
			}
		?>
     	<?php if(empty($form)) { ?>
        	<?php $fieldId = "{$formName}-firstName"; ?>
             <div id="container-<?= $fieldId ?>" class="form-group <?= $contactPersonForm->isAttributeRequired('firstName') ? 'required' : '' ?>">
             
             	<?php if(!Yii::$app->params['isPlaceholderInputLabel']) { ?>
             		<label class="control-label" for="<?="{$formName}-firstName"?>"><?= $contactPersonForm->getAttributeLabel('firstName') ?></label>
             	<?php }?>
             	
             	<?=  Html::activeTextInput($contactPersonForm, "[{$prefix}]firstName", $inputOptions + ['id' => "{$fieldId}"]) ?>
             	<div class="help-block"></div>
             </div>
             <?php
             $this->registerJs('
             	(function() {
                	$("#'.$formId.'").yiiActiveForm("add", {
                    	id: "'.$fieldId.'",
                        name: "'."[{$prefix}]firstName".'",
                        container: "#container-'.$fieldId.'",
                        input: "#'."$fieldId".'",
                        enableClientValidation: false,
                        enableAjaxValidation: true
                   });
              })();
              ', View::POS_END);
              ?>
		<?php } else if($isEditScenarioSet) { ?>
      		<?= $form->field($contactPersonForm, "[{$prefix}]firstName")->textInput($inputOptions) ?>
		<?php } ?>
	</div>
	
	<div class="col-sm-2">
		<?php $inputOptions = ['type' => 'text', 'class' => 'form-control lastName']; 
			if(Yii::$app->params['isPlaceholderInputLabel']) {
				$inputOptions['placeholder'] = $contactPersonForm->getAttributeLabel('lastName');
			}
		?>
     	<?php if(empty($form)) { ?>
        	<?php $fieldId = "{$formName}-lastName"; ?>
             <div id="container-<?= $fieldId ?>" class="form-group <?= $contactPersonForm->isAttributeRequired('lastName') ? 'required' : '' ?>">
             	<?php if(!Yii::$app->params['isPlaceholderInputLabel']) { ?>
             		<label class="control-label" for="<?="{$formName}-lastName"?>"><?= $contactPersonForm->getAttributeLabel('lastName');?></label>
             	<?php }?>
             	<?=  Html::activeTextInput($contactPersonForm, "[{$prefix}]lastName", $inputOptions + ['id' => "{$fieldId}"]) ?>
             	<div class="help-block"></div>
             </div>
             <?php
             $this->registerJs('
             	(function() {
                	$("#'.$formId.'").yiiActiveForm("add", {
                    	id: "'.$fieldId.'",
                        name: "'."[{$prefix}]lastName".'",
                        container: "#container-'.$fieldId.'",
                        input: "#'."$fieldId".'",
                        enableClientValidation: false,
                        enableAjaxValidation: true
                   });
              })();
              ', View::POS_END);
              ?>
		<?php } else if($isEditScenarioSet) { ?>
      		<?= $form->field($contactPersonForm, "[{$prefix}]lastName")->textInput($inputOptions) ?>
		<?php } ?>
	</div>
	
	<div class="col-sm-2">
		<?php $inputOptions = ['type' => 'text', 'class' => 'form-control email'];
			if(Yii::$app->params['isPlaceholderInputLabel']) {
				$inputOptions['placeholder'] = $contactPersonForm->getAttributeLabel('email');
			}	
		?>
     	<?php if(empty($form)) { ?>
        	<?php $fieldId = "{$formName}-email"; ?>
             <div id="container-<?= $fieldId ?>" class="form-group <?= $contactPersonForm->isAttributeRequired('email') ? 'required' : '' ?>">
             	<?php if(!Yii::$app->params['isPlaceholderInputLabel']) { ?>
             		<label class="control-label" for="<?="{$formName}-email"?>"><?= $contactPersonForm->getAttributeLabel('email');?></label>
             	<?php }?>
             	<?=  Html::activeTextInput($contactPersonForm, "[{$prefix}]email", $inputOptions + ['id' => "{$fieldId}"]) ?>
             	<div class="help-block"></div>
             </div>
             <?php
             $this->registerJs('
             	(function() {
                	$("#'.$formId.'").yiiActiveForm("add", {
                    	id: "'.$fieldId.'",
                        name: "'."[{$prefix}]email".'",
                        container: "#container-'.$fieldId.'",
                        input: "#'."$fieldId".'",
                        enableClientValidation: false,
                        enableAjaxValidation: true
                   });
              })();
              ', View::POS_END);
              ?>
		<?php } else if($isEditScenarioSet) { ?>
      		<?= $form->field($contactPersonForm, "[{$prefix}]email")->textInput($inputOptions) ?>
		<?php } ?>
	</div>
	
	<div class="col-sm-2">
		<?php $inputOptions = ['type' => 'text', 'class' => 'form-control phone1'];
			if(Yii::$app->params['isPlaceholderInputLabel']) {
				$inputOptions['placeholder'] = $contactPersonForm->getAttributeLabel('phone1');
			}
		?>
     	<?php if(empty($form)) { ?>
        	<?php $fieldId = "{$formName}-phone1"; ?>
             <div id="container-<?= $fieldId ?>" class="form-group <?= $contactPersonForm->isAttributeRequired('phone1') ? 'required' : '' ?>">
             	<?php if(!Yii::$app->params['isPlaceholderInputLabel']) { ?>
             		<label class="control-label" for="<?="{$formName}-phone1"?>"><?= $contactPersonForm->getAttributeLabel('phone1');?></label>
             	<?php }?>
             	<?=  Html::activeTextInput($contactPersonForm, "[{$prefix}]phone1", $inputOptions + ['id' => "{$fieldId}"]) ?>
             	<div class="help-block"></div>
             </div>
             <?php
             $this->registerJs('
             	(function() {
                	$("#'.$formId.'").yiiActiveForm("add", {
                    	id: "'.$fieldId.'",
                        name: "'."[{$prefix}]phone1".'",
                        container: "#container-'.$fieldId.'",
                        input: "#'."$fieldId".'",
                        enableClientValidation: false,
                        enableAjaxValidation: true
                   });
              })();
              ', View::POS_END);
              ?>
		<?php } else if($isEditScenarioSet) { ?>
      		<?= $form->field($contactPersonForm, "[{$prefix}]phone1")->textInput($inputOptions) ?>
		<?php } ?>
	</div> 
	
	<div class="col-sm-3">
		<?php $inputOptions = ['type' => 'text', 'class' => 'form-control note'];
			if(Yii::$app->params['isPlaceholderInputLabel']) {
				$inputOptions['placeholder'] = $contactPersonForm->getAttributeLabel('note');
			}
		?>
     	<?php if(empty($form)) { ?>
        	<?php $fieldId = "{$formName}-note"; ?>
             <div id="container-<?= $fieldId ?>" class="form-group <?= $contactPersonForm->isAttributeRequired('note') ? 'required' : '' ?>">
             	<?php if(!Yii::$app->params['isPlaceholderInputLabel']) { ?>
             		<label class="control-label" for="<?="{$formName}-note"?>"><?= $contactPersonForm->getAttributeLabel('note');?></label>
             	<?php }?>
             	<?=  Html::activeTextarea($contactPersonForm, "[{$prefix}]note", $inputOptions + ['id' => "{$fieldId}"]) ?>
             	<div class="help-block"></div>
             </div>
             <?php
             $this->registerJs('
             	(function() {
                	$("#'.$formId.'").yiiActiveForm("add", {
                    	id: "'.$fieldId.'",
                        name: "'."[{$prefix}]note".'",
                        container: "#container-'.$fieldId.'",
                        input: "#'."$fieldId".'",
                        enableClientValidation: false,
                        enableAjaxValidation: true
                   });
              })();
              ', View::POS_END);
              ?>
		<?php } else if($isEditScenarioSet) { ?>
      		<?= $form->field($contactPersonForm, "[{$prefix}]note")->textArea($inputOptions) ?>
		<?php } ?>
	</div>
	
	<div>
		<?php $inputOptions = ['class' => 'form-control id']  ?>
     	<?php if(empty($form)) { ?>
        	<?php $fieldId = "{$formName}-id"; ?>
             <div id="container-<?= $fieldId ?>" class="form-group">
             	<?=  Html::hiddenInput('id', "[{$prefix}]id", $inputOptions + ['id' => "{$fieldId}"]) ?>
             	<div class="help-block"></div>
             </div>
             <?php
             $this->registerJs('
             	(function() {
                	$("#'.$formId.'").yiiActiveForm("add", {
                    	id: "'.$fieldId.'",
                        name: "'."[{$prefix}]id".'",
                        container: "#container-'.$fieldId.'",
                        input: "#'."$fieldId".'",
                        enableClientValidation: false,
                        enableAjaxValidation: true
                   });
              })();
              ', View::POS_END);
              ?>
		<?php } else if($isEditScenarioSet) { ?>
      		<?= $form->field($contactPersonForm, "[{$prefix}]id")->hiddenInput($inputOptions)->label(false) ?>
		<?php } ?>
	</div>
	
	<?php if(empty($form) ||$isEditScenarioSet) { ?>
		<div class="col-sm-1">
	        <div class="base-icon garbage-icon action-icon remove-item-btn"></div>
	    </div>
    <?php } ?>                      
</div>