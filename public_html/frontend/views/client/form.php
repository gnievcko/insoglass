<?php

use yii\helpers\Url;
use yii\helpers\Html;
use common\components\ActiveForm;
use yii\web\View;
use yii\helpers\ArrayHelper;
use common\models\ar\Province;
use common\models\ar\Language;
use kartik\widgets\Select2;
use yii\web\JsExpression;
use common\helpers\ProvinceHelper;
use common\helpers\StringHelper;

$this->registerCssFile('@web/css/select2.min.css');
$this->registerCssFile('@web/css/select2-bootstrap.min.css');
$this->registerJsFile('@web/js/forms.js?'.uniqid(), ['depends' => [\yii\web\JqueryAsset::className()]]);
$this->registerJsFile('@web/js/select2/select2.min.js?'.uniqid(), ['depends' => [\yii\web\JqueryAsset::className()]]);
$this->registerJsFile('@web/js/select2/i18n/'.Yii::$app->language.'.js?'.uniqid(), ['depends' => [\yii\web\JqueryAsset::className()]]);
$this->registerJsFile('@web/js/province-by-zip-code.js?'.uniqid(), ['depends' => [\yii\web\JqueryAsset::className()]]);

$subtitle = $model->isEditScenarioSet() ? Yii::t('web', 'Update client data') : Yii::t('web', 'Add client');
$this->title = $subtitle . ' - ' . Yii::$app->name;

$this->params['breadcrumbs'][] = ['label' => StringHelper::translateOrderToOffer('main', 'Orders'), 'url' => Url::to(['order/index'])];
$this->params['breadcrumbs'][] = ['label' => Yii::t('web', 'List of clients'), 'url' => Url::to(['client/list'])];
$this->params['breadcrumbs'][] = $subtitle;
?>
<div class="client-form">
	<h2 class="page-title"><?= $subtitle ?></h2>

	<?php $form = ActiveForm::begin([
			'id' => 'client-form-ajax',
			'enableAjaxValidation' => true,
			'validateOnBlur' => false,
			'validateOnChange' => false,

	]); ?>
	<div class="col-xs-12 title-with-icon">
		<div class="base-icon basic-data-icon main-content-icons"></div>
		<h3><?= Yii::t('web', 'Basic data') ?></h3>
	</div>
	<?php if($model->isEditScenarioSet()) {
		$form->field($model, 'id')->hiddenInput()->label(false);

	} ?>
    <div class="col-sm-12">
    	<div class="row">
	        <div class="col-sm-6"><?= $form->field($model, 'name')->textarea(['class' => 'smallTextarea form-control', 'maxlength' => true, 'placeholder' => Yii::t('web', 'Client name')]) ?></div>
	        <div class="col-sm-5 col-sm-offset-1">
	            <?php
	            echo $form->field($model, 'groupIds', ['enableAjaxValidation' => false])->
	                    widget(Select2::className(), [
	                        'language' => Yii::$app->language,
	                        'name' => 'select2_client-groups',
	                        'options' => ['placeholder' => Yii::t('web', 'Select a group...'), 'multiple' => true],
	                        'showToggleAll' => false,
	                        'data' => $groupsData,
	                        'pluginOptions' => [
	                            'allowClear' => false,
	                            'minimumInputLength' => 1,
	                            'tags' => true,
	                            'ajax' => [
	                                'url' => Url::to(['/company/find-company-groups']),
	                                'dataType' => 'json',
	                                'delay' => 250,
	                                'cache' => true,
	                                'data' => new JsExpression('function(params) { return {term:params.term, page:params.page, size: 20}; }'),
	                                'processResults' => new JsExpression('function (data, params) {
													params.page = params.page || 1;
													return {
														results: data.items,
														pagination: {
															more: data.more
														}
													};
												}')
	                            ],
	                            'templateResult' => new JsExpression('function(item) { if(item.loading) return item.text; return item.name; }'),
	                            'templateSelection' => new JsExpression('function (item) { return item.name || item.text; }'),
	                        ],
	                        'pluginEvents' => [
	                        ],
	                            ]
	                    );
	            ?>
                <a href="<?= Url::to(['client-group/create']); ?>" target="_blank" class="focusable-link  add-item-after-select2  add-item-after-select2-no-required">
                    <span class="glyphicon glyphicon-plus-sign pull-left"></span>
                    <span> <?=Yii::t('web', 'Create new group')?></span>
                </a>
	        </div>
        </div>
    </div>
    <div class="col-sm-12">
    	<div class="row">
	        <div class="col-sm-6">
	        	<?php
	            	$function = '
						function(params) {
							return {
								term:params.term,
								page:params.page,
								size: 20,
								excludedCompanyId: ' . ($model->isEditScenarioSet() ? $model->id : 0) . '
							};
						}
					';
	
	            	echo $form->field($model, 'parentCompanyId')
						->widget(Select2::classname(), [
	                        'language' => Yii::$app->language,
	                        'options' => ['placeholder' => Yii::t('main', 'Select a parent company...')],
	                        'data' => $parentCompanyData,
	                        'pluginOptions' => [
	                            'allowClear' => true,
	                            'minimumInputLength' => Yii::$app->params['isClientListImmediatelyVisible'] ? 0 : 1,
	                            'ajax' => [
	                                'url' => Url::to(['company/find']),
	                                'dataType' => 'json',
	                                'delay' => 250,
	                                'data' => new JsExpression($function),
	                                'processResults' => new JsExpression('function (data, params) {
													params.page = params.page || 1;
	
													return {
														results: data.items,
														pagination: {
															more: data.more
														}
													};
												}'),
	                                'cache' => true
	                            ],
	                        	'escapeMarkup' => new JsExpression('function (markup) { return markup; }'),
	                            //'templateResult' => new JsExpression('function(item) { if(item.loading) return item.text; return item.name; }'),
	                        	'templateResult' => new JsExpression('function(item) { var address = item.address ? "<span class=\"address-highlight\">, " + item.address + "</span>" : ""; if(item.loading) return item.text; if(item.parent_company_id) return "<span class=\"object-highlight\">"+item.name+"</span>" + address; else return item.name + address; }'),
	                            'templateSelection' => new JsExpression('function (item) { var address = item.address ? "<span class=\"address-highlight\">, " + item.address + "</span>" : ""; return (item.name || item.text) + address; }'),
	                        ]
						]);
	            ?>
	    	</div>
		</div>
    </div>
    <div class="col-sm-12">
    	<div class="row">
	        <div class="col-sm-6"><?=
	            $form->field($model, 'vatIdentificationNumber')->textInput(['maxLength' => true, 'placeholder' => $model->getAttributeLabel('vatIdentificationNumber')]);
	            ?></div>
		</div>
    </div>
    <div class="col-sm-12">
    	<div class="row">
	        <div class="col-sm-6"><?=
	            $form->field($model, 'taxpayerIdentificationNumber')->textInput(['maxLength' => true, 'placeholder' => $model->getAttributeLabel('taxpayerIdentificationNumber')]);
	            ?></div>
		</div>
    </div>
    <div class="col-sm-12">
    	<div class="row">
	        <div class="col-sm-6"><?=
	            $form->field($model, 'krsNumber')->textInput(['maxLength' => true, 'placeholder' => $model->getAttributeLabel('krsNumber')]);
	            ?></div>
        </div>
    </div>
	<div class="col-xs-12 title-with-icon">
		<div class="base-icon contact-icon main-content-icons"></div>
   		<h3><?= Yii::t('web', 'Contact data') ?></h3>
	</div>
    <div class="col-xs-12">
    	<div class="row">
	        <div class="col-sm-6"><?= $form->field($model, 'email')->textInput(['maxlength' => true, 'placeholder' => Yii::t('web', 'E-mail address')]) ?></div>
	        <div class="col-sm-5 col-sm-offset-1"><?=
	            $form->field($model, 'fax1')->textInput(['maxLength' => true, 'placeholder' => $model->getAttributeLabel('fax1')]);
	            ?></div>
        </div>
    </div>

    <div class="col-xs-12">
    	<div class="row">
	        <div class="col-sm-6"><?=
	            $form->field($model, 'phone1')->textInput(['maxLength' => true, 'placeholder' => $model->getAttributeLabel('phone1'), 'class' => 'form-control']);
	            ?></div>
	        <div class="col-sm-5 col-sm-offset-1"><?=
	            $form->field($model, 'phone2')->textInput(['maxLength' => true, 'placeholder' => $model->getAttributeLabel('phone2'), 'class' => 'form-control']);
	            ?></div>
	
	        <div class="col-sm-6">
	        	<?= $form->field($model, 'note')->textarea(['maxLength' => true, 'placeholder' => $model->getAttributeLabel('note')]);?>
	     	</div>
	     </div>
    </div>

    <?php
        $provinces = ProvinceHelper::getListOfNonArtificialProvinces();
        
        echo Yii::$app->controller->renderPartial('inc/_contactPeople', [
    		'contactPeopleForms' => $contactPeopleForms, 
   			'form' => $form,
    		'isEditScenarioSet' => $isEditScenarioSet,
       	]);
    ?>
    
    <div class="col-xs-12">
        <h4><?= Yii::t('web', 'Main address') ?></h4>
        <div class="col-md-4 col-sm-8"><?= $form->field($model, 'address1')->textInput(['maxlength' => true, 'placeholder' => $model->getAttributeLabel('address1')]) ?></div>
        <div class="col-md-2 col-sm-4"><?= $form->field($model, 'zipCode1')->textInput(['maxlength' => true, 'placeholder' => $model->getAttributeLabel('zipCode1')]) ?></div>
        <div class="col-md-3 col-sm-6"><?= $form->field($model, 'city1')->textInput(['maxlength' => true, 'placeholder' => $model->getAttributeLabel('city1')]) ?></div>
        <div class="col-md-3 col-sm-6"><?= $form->field($model, 'province1Id')->dropDownList($provinces, ['prompt' => Yii::t('main', 'Choose Voivodeship')]);?></div>
    </div>


    <div class="col-xs-12">
        <h4><?= Yii::t('main', 'Address for correspondence') ?></h4>
        <div class="hint-block"><?= Yii::t('web', 'Provide this address if is different than main one.') ?></div>
        <div class="col-md-4 col-sm-8"><?= $form->field($model, 'address2')->textInput(['maxlength' => true, 'placeholder' => $model->getAttributeLabel('address2')]) ?></div>
        <div class="col-md-2 col-sm-4"><?= $form->field($model, 'zipCode2')->textInput(['maxlength' => true, 'placeholder' => $model->getAttributeLabel('zipCode2')]) ?></div>
        <div class="col-md-3 col-sm-6"><?= $form->field($model, 'city2')->textInput(['maxlength' => true, 'placeholder' => $model->getAttributeLabel('city2')]) ?></div>
        <div class="col-md-3 col-sm-6"><?= $form->field($model, 'province2Id')->dropDownList($provinces, ['prompt' => Yii::t('main', 'Choose Voivodeship')]);?></div>

    </div>

    <div class="col-xs-12">
        <!--<div class="col-sm-6">
        <?php
        echo $form->field($model, 'authorizedSalesmanIds', ['enableAjaxValidation' => false])->
                widget(Select2::className(), [
                    'language' => Yii::$app->language,
                    'name' => 'select2_client-authorized-salesmen',
                    'options' => ['placeholder' => Yii::t('web', 'Select a salesman...'), 'multiple' => true],
                    'showToggleAll' => false,
                    'data' => $authorizedSalesmenData,
                    'pluginOptions' => [
                        'allowClear' => false,
                        'minimumInputLength' => 1,
                        'tags' => false,
                        'ajax' => [
                            'url' => Url::to(['/salesman/find-without-client']),
                            'dataType' => 'json',
                            'delay' => 250,
                            'cache' => true,
                            'data' => new JsExpression('function(params) { return {term:params.term, page:params.page, size: 20}; }'),
                            'processResults' => new JsExpression('function (data, params) {
												params.page = params.page || 1;
												var results = data.items.map(function(item) { return { id : item.id, name : (item.first_name + " " + item.last_name) ? (item.first_name + " " + item.last_name) : item.email }; });

												return {
													results: results,
													pagination: {
														more: data.more
													}
												};
											}')

									],
									'templateResult' => new JsExpression('function(item) { if(item.loading) return item.text; return item.name; }'),
									'templateSelection' => new JsExpression('function (item) { return item.name || item.text; }'),
							],
							'pluginEvents' => [
							],
					]
			); ?>
        </div>-->
	</div>

	<div class="col-sm-12">
		<div class="row">
			<div class="pull-left">
				<?= Html::a(Yii::t('web', 'Cancel'),
	        		!empty(Yii::$app->request->getReferrer()) ? Yii::$app->request->getReferrer() : ['client/list'],
	        		['class' => 'btn btn-default']) ?>
			</div>
			<div class="pull-right"><?= Html::submitButton(($model->isEditScenarioSet() ? Yii::t('main', 'Save') : Yii::t('web', 'Add')), ['class' => 'btn btn-primary']); ?></div>
		</div>
	</div>

	<?php ActiveForm::end(); ?>
	<?php $this->registerJs('fillProvince("companyform-zipcode1", "companyform-province1id", "' . Url::to(['employee/get-province-by-zip-code']) . '");', View::POS_END); ?>
	<?php $this->registerJs('fillProvince("companyform-zipcode2", "companyform-province2id", "' . Url::to(['employee/get-province-by-zip-code']) . '");', View::POS_END); ?>
</div>

<?php $this->registerJs('
    (function() {
        var dynamicForm = new DynamicForm("client-form-ajax");
        dynamicForm.activateRemoveButtons();
        dynamicForm.activateAddItemButtons();
    })();
    ', View::POS_END); 
?>
