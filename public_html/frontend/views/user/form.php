<?php
use yii\helpers\Url;
use yii\helpers\Html;
use common\components\ActiveForm;
use yii\web\View;
use yii\helpers\ArrayHelper;
use yii\widgets\MaskedInput;
use common\models\ar\Province;
use common\models\ar\Language;
use kartik\widgets\Select2;
use yii\web\JsExpression;
use common\helpers\StringHelper;
use frontend\models\UserAccountForm;
use common\models\ar\Currency;
use kartik\widgets\FileInput;
use yii\bootstrap\Progress;
use kartik\datecontrol\DateControl;


$subtitle = $model->isShowScenarioSet() ? Yii::t('web', 'Your account') : Yii::t('web', 'Edit your account');
$this->title = $subtitle.' - '.Yii::$app->name;

$this->params['breadcrumbs'][] = ['label' => Yii::t('main', 'Home page'), 'url' => Url::to(['dashbord/index'])];
$this->params['breadcrumbs'][] = Yii::t('web', 'Edit your account');

?>

<div class="user-account-form">
	<h2 class="page-title"><?= $subtitle ?></h2>

	<?php $form = ActiveForm::begin([
			'id' => 'user-account-form-ajax',
			'enableAjaxValidation' => true,
			'validateOnBlur' => false,
			'validateOnChange' => false,
			'options' => ['enctype' => 'multipart/form-data'],
	]); ?>

	<?php if($model->isEditScenarioSet()) {
		echo $form->field($model, 'id')->hiddenInput()->label(false);
		echo $form->field($model, 'deletePhoto')->hiddenInput()->label(false);
	} ?>

	<div class="col-xs-12">
		<div class="row">
    		<div class="col-md-8">
    			<div class="row">
        			<div class="col-md-6">
        				<?= $form->field($model, 'firstName')->textInput(['readonly' => $model->isShowScenarioSet(), 'maxlength' => true, 'placeHolder' => $model->getAttributeLabel('firstName')]) ?>
        			</div>
        			<div class="col-md-6">
        				<?= $form->field($model, 'lastName')->textInput(['readonly' => $model->isShowScenarioSet(), 'maxlength' => true, 'placeHolder' => $model->getAttributeLabel('lastName')]) ?>
        			</div>
        			<div class="col-md-6">
        				<?= $form->field($model, 'phone1')->textInput(['maxLength' => true, 'placeholder' => $model->getAttributeLabel('phone1'), 'class' => 'form-control']) ?>
        			</div>
        			<div class="col-md-6">
        				<?= $form->field($model, 'phone2')->textInput(['maxLength' => true, 'placeholder' => $model->getAttributeLabel('phone2'), 'class' => 'form-control']) ?>
        			</div>
        			<div style="width: 100%; display:inline-block;">
        			<div class="col-md-6">
        				<?= $form->field($model, 'phone3')->textInput(['maxLength' => true, 'placeholder' => $model->getAttributeLabel('phone3'), 'class' => 'form-control']) ?>
        			</div>
        			</div>
        			<div class="col-md-6">
        				<?= $form->field($model, 'languageId')->dropDownList(ArrayHelper::map(
        						Language::find()->where('is_active = 1')->all(), 'id', 'name'
        					), ['disabled' => $model->isShowScenarioSet()])->label(Yii::t('main', 'Language')) ?>
        			</div>
        			<div class="col-md-6">
        				<?= $form->field($model, 'currencyId')->dropDownList(ArrayHelper::map(
        						Currency::find()->all(), 'id', 'symbol'
        					), ['disabled' => $model->isShowScenarioSet()])->label(Yii::t('main', 'Currency')) ?>
        			</div>
        			
        			<div class="col-md-6">
        				<?= $form->field($model, 'pesel')->textInput(['maxlength' => true, 'placeholder' => $model->getAttributeLabel('pesel')]) ?>
        			</div>
        
        			<div class="col-md-6">
        				<?= $form->field($model, 'idNumber')->textInput(['maxlength' => true, 'placeholder' => $model->getAttributeLabel('idNumber')]) ?>
        			</div>
        			<div class="col-md-6">
        				<?= $form->field($model, 'position')->textInput(['maxLength' => true, 'placeholder' => $model->getAttributeLabel('position')]) ?>
        			</div>
        			
        			<div class="col-md-6">
        			<h5><?= $model->getAttributeLabel('dateEmployment')?></h5>
                	<?= $form->field($model, 'dateEmployment')->widget(DateControl::classname(), [
                            'language' => \Yii::$app->language,
                            'type' => DateControl::FORMAT_DATE,
                            'ajaxConversion' => false,
                            'displayFormat' => 'php:Y-m-d',
                			'saveFormat' => 'php:Y-m-d H:i:s',
                    		'options' => ['placeholder' => $model->getAttributeLabel('dateEmployment')],
                            'options' => [
                                'pluginOptions' => [
                                    'autoclose' => true
                                ]
                            ]
                        ])->label(false);
                    ?>
        			</div>
        			<hr/>
        			<div class="col-xs-12">
        				<?= $form->field($model, 'email')->textInput(['readonly' => $model->isShowScenarioSet(), 'maxlength' => true, 'placeHolder' => $model->getAttributeLabel('email')])  ?>
        			</div>
        			<?php if($model->isEditScenarioSet()): ?>
        			<div class="col-md-6">
        				<div class="row">
            				<div class="col-xs-12">
            				<h3><?= Html::encode(Yii::t('web', 'Change password')) ?></h3>
            				</div>
            				<div class="col-xs-12">
            					<?= $form->field($model, 'currentPassword')->passwordInput(['maxlength' => true, 'placeHolder' => $model->getAttributeLabel('currentPassword')]) ?>
            				</div>
            				<div class="col-xs-12">
            					<?= $form->field($model, 'newPassword')->passwordInput(['maxlength' => true, 'placeHolder' => $model->getAttributeLabel('newPassword')]) ?>
            				</div>
            				<div class="col-xs-12">
            					<?= $form->field($model, 'newPasswordRepeated')->passwordInput(['maxlength' => true, 'placeHolder' => $model->getAttributeLabel('newPasswordRepeated')]) ?>
            				</div>
        				</div>
        			</div>
        			<?php endif ?>
    			</div>
    		</div>
    		<div class="col-md-4">
    			<?php if($model->isShowScenarioSet()): ?>
    		        <div id="photo-preview-container" class="form-group">
    		            <label><?= Yii::t('main', 'Current photo') ?></label>
    		            <div class="file-preview">
    		                <div id="photo-preview-container" class="form-group text-center">
    		                    <?= Html::img(!empty($model->urlPhoto) ? \common\helpers\UploadHelper::getNormalPath($model->urlPhoto, 'user_photo') : Url::to('@web/images/placeholders/person.png'),
    		                                  ['style' => 'max-width: 300px'],
    		                                  ['class' => 'file-preview-frame', 'style' => 'display: block; height: auto; float: none;'])
    		                    ?>
    		                    	<!-- <button type="button" id="delete-photo-button" class="btn btn-danger"><?= Yii::t('main', 'Delete') ?></button> -->
    		                </div>
    		            </div>
    		        </div>
    
    	    	<?php else:
    	    		echo '<h3 class="loadImage">'.Yii::t('web', 'Upload photo').'</h3>';
    		        echo $form->field($model, 'filePhoto')->widget(FileInput::classname(), [
    		                        'options' => [
    		                                'multiple' => false,
    		                        ],
    		                        'pluginOptions' => [
    		                                'language' => Yii::$app->language,
    		                                'showCaption' => false,
    		                                'showUpload' => false,
    		                                'uploadAsync' => false,
    		                                'resizeImageQuality' => 1.00, //not required
    		                                'allowedFileTypes' => ['image'],
    		                                'allowedFileExtensions' => ['jpg', 'png', 'jpeg', 'gif'],
    		                                'allowedPreviewFileTypes' => ['image'],
    		                                'allowedPreviewMimeTypes' => ['image/jpeg', 'image/png'],
    		                                'previewFileType' => 'image',
    		                                'previewSettings' => ['image' => ['width' => '300px', 'height' => 'auto']],
    		                        		'initialPreview' => !empty($model->urlPhoto) ? Html::img(\common\helpers\UploadHelper::getNormalPath($model->urlPhoto, 'url_photo'),
    		                                 		['style' => 'max-width: 300px'],
    		                                 		['class' => 'file-preview-frame', 'style' => 'display: block; height: auto; float: none;']
    		                        		) : [],
    		                        ],
    		        				'pluginEvents' => [
    		        						'filecleared' => 'function() {$("#useraccountform-deletephoto")[0].value = 1}',
    		        				]
    		        ])->label(false);
    		        endif;
    		    ?>
    		</div>
		</div>
	</div>
	<?php if($model->isEditScenarioSet()):?>
		<div class="col-xs-12">
			<div class="pull-left"><?= Html::a(Yii::t('web', 'Cancel'), ['account'], ['class' => 'btn btn-default']) ?></div>
			<div class="pull-right"><?= Html::submitButton(($model->isEditScenarioSet() ? Yii::t('main', 'Save') : Yii::t('web', 'Add')), ['class' => 'btn btn-primary']); ?></div>
		</div>
	<?php else: ?>
		<div class="col-xs-12">
			<?= Html::a(Yii::t('main', 'Update'), ['account-edit'], ['class' => 'btn btn-primary']) ?>
		</div>
	<?php endif;?>
	<?php ActiveForm::end(); ?>
</div>
