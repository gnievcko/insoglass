<?php

use yii\helpers\Html;
use yii\helpers\Url;
use frontend\helpers\UserHelper;
use dosamigos\gallery\Gallery;
use common\helpers\StringHelper;
use frontend\helpers\AddressHelper;

$subtitle = Yii::t ( 'web', 'User information' );
$this->title = $subtitle . ' - ' . Yii::$app->name;
?>

<div class="account-details">
	<div class="row">
		<div class="col-sm-6 col-xs-9">
			<h2 class="page-title"><?= $subtitle ?></h2>
		</div>
		<div class="col-sm-6 col-xs-3">
			<div class="pull-right"><?= Html::a(Yii::t('main', 'Update'), ['account-edit'], ['class' => 'btn btn-default']) ?></div>
		</div>
	</div>
	<div class="col-xs-12">
		<div class="row">
			<div class="col-lg-3 col-md-4 col-sm-6 col-xs-12">
					<div class="photo-container ">
								<?php
								$src = null;
								if(empty ($model->url_photo)) {
									$src = Url::to('@web/images/placeholders/person.png');
								} else {
									$src = \common\helpers\UploadHelper::getNormalPath($model->url_photo, 'user_photo');
								}
								?>
									<?= '<img src=' . $src .' alt="..." class="img-circle" align="middle" width="200px" >';?>
							</div>
			</div>	
			<div class="col-lg-9 col-md-8 col-sm-6 col-xs-12 user-details">
					<h3><?= $model->first_name . ' ' . $model->last_name ?></h3>
					<h4><?= $model->email ?></h4>
					<h4><?= $model->phone1 ?></h4>
					<h4><?= $model->phone2 ?></h4>
					<h4><?= $model->phone3 ?></h4>
			</div>
			<div class="col-xs-12">
				<div class="table-responsive">
					<table class="data">
						<tr>
							<td><?= Yii::t('main', 'Roles') . ':'?></td>
							<td><?= $roles?></td>
						</tr>
						<tr>
							<td><?= Yii::t('main', 'Creation date') . ':'?></td>
							<td><?= $model->date_creation ?></td>
						</tr>
						<tr>
							<td><?= Yii::t('main', 'Language') ?></td>
							<td><?= isset($model->language->name) ? $model->language->name : '' ?></td>
						</tr>
						<tr>
							<td><?= Yii::t('main', 'Currency') ?></td>
							<td><?= isset($model->currency->symbol) ? $model->currency->symbol : '' ?></td>
						</tr>
						<tr>
							<td><?= Yii::t('web', 'Employment date') ?></td>
							<td><?= Html::encode(StringHelper::getFormattedDateFromDate($model->date_employment)) ?></td>
						</tr>
						<tr>
							<td><?= Yii::t('web', 'PESEL')?></td>
							<td><?= Html::encode($model->pesel) ?></td>
						</tr>
						<tr>
							<td><?= Yii::t('web', 'ID number') ?></td>
							<td><?= Html::encode($model->id_number) ?> </td>
						</tr>
						<tr>
							<td><?= Yii::t('web', 'Position') ?></td>
							<td><?= Html::encode($model->position) ?></td>
						</tr>
						<tr>
							<td><?= Yii::t('main', 'Address') ?></td>
							<td><?php
	                            if(!empty($model->address)) {
	                                AddressHelper::getFullAddress([
	                                        'addressMain' => $model->address->main,
	                                        'addressComplement' => $model->address->complement,
	                                        'cityName' => $model->address->city->name,
	                                        'cityZipCode' => $model->address->city->zip_code,
	                                        'provinceName' => $model->address->city->province->name,
	                                        'countryName' => $model->address->city->province->countryName
	                                ]);
	                            }?> 
			            	</td>
						</tr>
					</table>
				</div>
			</div>
		</div>
	</div>
</div>
