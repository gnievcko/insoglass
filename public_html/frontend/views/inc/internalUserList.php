<?php
use yii\helpers\Html;
use frontend\helpers\UserHelper;
use yii\helpers\Url;
?>

<div class="internal-user-list">
	<?php foreach($users as $user) { ?>
		<div class="user-row col-sm-9">
			<span class="photo-container">
				<?= '<div class="photo" style="background: url('.(!empty($user['urlPhoto']) ? \common\helpers\UploadHelper::getNormalPath($user['urlPhoto'], 'user_photo') : Url::to('@web/images/placeholders/person.png')).')"></div>'; ?>
			</span>
			<span class="name"><?php 
				$line = UserHelper::getPrettyUserName($user['email'], $user['firstName'], $user['lastName']);
				if(!empty($user['email'])) {
					$line .= ', '.Html::mailto($user['email'], $user['email']);
				}
				if(!empty($user['phone'])) {
					$line .= ', '.Yii::t('web', 'ph.').' '.$user['phone'];
				}
				
				echo $line;
			?></span>
    		<span class="actions pull-right"><?php 
    			echo Html::a('', Url::to(['client-representative/edit', 'id' => $user['id'], 'ref' => 'client']), ['class' => 'base-icon action-icon editing-icon']);
				echo '&nbsp;'.Html::a('', Url::to(['client-representative/delete']), [
						'class' => 'base-icon action-icon delete-icon',
						'data' => [
								'confirm' => Yii::t('web', 'Are you sure you want to delete this contact person?'),
								'method' => 'post',
								'params' => [
										'id' => $user['id'],
								],
						],
				]);
    		?></span>
		</div>
	<?php } ?>
</div>