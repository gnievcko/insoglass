<?php
use yii\bootstrap\Html;
use yii\web\View;

$this->registerJsFile('@web/js/hint-tooltip.js?'.uniqid(), ['depends' => [\yii\web\JqueryAsset::className()]]);


if(Yii::$app->params['isProductAttributesVisible']) { ?>
    <div class="product-attribute-container">
        
        <?php 
        if(!empty($groups)) {
            $colWidth = floor(12 / count($groups));
            $emptyGroups = 0;
            foreach($groups as $group) {
                if(empty($group['attributes'])) {
                    $emptyGroups++;
                    continue;
                }
                
                echo '<div class="col-xs-'.$colWidth.'">';
                echo '<div class="attribute-title">'.Html::encode($group['name']).'</div>';    
                
                foreach($group['attributes'] as $attribute) {

                    $hint = (empty($attribute['hint']))?'': ('<span class="glyphicon glyphicon-info-sign extended-tooltip"><div class="hint-container"><div class="extended-hint">'.$attribute['hint'].'</div></div></span>');

                    if($attribute['variableType'] == 'bool') {
                        echo '<div class="row attribute-static-item"><div class="col-xs-2"><span class="glyphicon glyphicon-ok">
                                </div><div class="col-xs-10">'.Html::encode($attribute['name']).' '.$hint.'</div></div>';
                    }
                    elseif($attribute['variableType'] == 'int') {
                        echo '<div class="row attribute-static-item"><div class="col-xs-7">'.Html::encode($attribute['name']).' '.$hint.'</div>
                                <div class="col-xs-5">'.Html::encode($attribute['value']).'</div></div>';
                    }
                }



                echo '</div>';
            }
            
            if($emptyGroups == count($groups)) {
                echo Yii::t('web', 'No attributes have been defined yet.');
            }
        }
        else {
            echo Yii::t('web', 'No attributes have been defined yet.');
        }
        ?>
    
    </div>
<?php } ?>

<?php
$this->registerJs('initAttributeTooltip();', View::POS_END);
?>
