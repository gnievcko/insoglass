<?php 
	use yii\helpers\Html;
use yii\helpers\Url;
?>

<div class="warehouses-row">
	<?php foreach($warehouses as $warehouse) { ?>
		<div class="warehouse-row">
			<span class="photo-container">
				<?= '<div class="photo" style="background: url('.Url::to('@web/images/placeholders/warehouse.png').')"></div>'; ?>
			</span>
			<span class="name"><?= Html::encode($warehouse['name']) ?></span>    		
		</div>
	<?php } ?>
</div>