<?php
use frontend\logic\ProductAttributeTypeLogic;
use common\helpers\Utility;
use yii\web\View;

$this->registerJsFile('@web/js/pages/product-attribute-form.js?'.uniqid(), ['depends' => [\yii\web\JqueryAsset::className()]]);
$this->registerJsFile('@web/js/hint-tooltip.js?'.uniqid(), ['depends' => [\yii\web\JqueryAsset::className()]]);

?>

<div class="product-attribute-container parent-container" style="display: <?= Yii::$app->params['isProductAttributesVisible'] ? 'block' : 'none' ?>">
    <?php 
    $groups = (new ProductAttributeTypeLogic())->getAttributeGroupsWithTypes();
    if(!empty($groups)) {
        $colWidth = floor(12 / count(array_filter($groups, function($g) { return !empty($g['attributes']); })));
        foreach($groups as $group) {
            if(empty($group['attributes'])) {
                continue;
            }
            
            echo '<div class="col-xs-'.$colWidth.'">';
            echo '<div class="attribute-title">'.$group['name'].'</div>';    
            
            foreach($group['attributes'] as $attribute) {
                $field = $form->field($model, !is_null($prefix) ? '[' . $prefix . ']fields['.$attribute['id'].']' : 'fields['.$attribute['id'].']');
                
                $nodeData = 'data-id = "'.$attribute['id'].'"';
                if(!empty($attribute['attributeCheckedIds'])) {
                    $nodeData .= ' data-checks="'.$attribute['attributeCheckedIds'].'"';
                }
                if(!empty($attribute['attributeBlockedIds'])) {
                    $nodeData .= ' data-unchecks="'.$attribute['attributeBlockedIds'].'"';
                }
                
                if($attribute['variableType'] == Utility::PRODUCT_ATTRIBUTE_TYPE_VARIABLE_TYPE_BOOL) {
                    $field->template = '<div '.$nodeData.' class="parent">
                            {input}{label}
                                '.(empty($attribute['hint'])?'':'<span class="glyphicon glyphicon-info-sign extended-tooltip"><div class="hint-container"><div class="extended-hint">'.$attribute['hint'].'</div></div></span>') .'
                            {error}</div>';
                    $field = $field->checkbox([], false);
                }
                elseif($attribute['variableType'] == Utility::PRODUCT_ATTRIBUTE_TYPE_VARIABLE_TYPE_INT) {
                    $field->template = '<div '.$nodeData.' class="parent">
                            {label}
                             '.(empty($attribute['hint'])?'':'<span class="glyphicon glyphicon-info-sign extended-tooltip"><div class="hint-container"><div class="extended-hint">'.$attribute['hint'].'</div></div></span>') .'
                            {input}{error}</div>';
                    $field = $field->input('number');
                }
                
                echo $field->label($attribute['name']);
            }
            
            echo '</div>';
        }
    }
    else {
        echo Yii::t('web', 'No attributes have been defined yet.');
    }
    ?>

</div>
<?php
    $this->registerJs('initAttributeTooltip();', View::POS_END);
?>
