<?php 
$isVisibleByDefault = Yii::$app->params['isListFiltersVisibleByDefault'];
?>

<div class="col-sm-12">
	<div class="col-sm-4"></div>
	<div class="col-sm-4">
		<?php if(isset($isMoreFilters) && $isMoreFilters) { ?>
			<div class="text-center more-filters-button">
				<span class="glyphicon glyphicon-menu-<?= $isVisibleByDefault ? 'up' : 'down' ?>"></span>&nbsp;
			    <span class="text-down" style="display: <?= $isVisibleByDefault ? 'none' : 'inline' ?>">
			    	<?= mb_strtoupper(Yii::t('web', 'More filters'), 'UTF-8') ?>
			    </span>
			    <span class="text-up" style="display: <?= $isVisibleByDefault ? 'inline' : 'none' ?>">
			    	<?= mb_strtoupper(Yii::t('web', 'Less filters'), 'UTF-8') ?>
			    </span>
			</div>
		<?php } ?>
	</div>
	<div class="col-sm-4">
		<?php if(isset($isClearFilters) && $isClearFilters) { ?>
			<div class="pull-right clear-filters-button">
				<?= mb_strtoupper(Yii::t('web', 'Clear filters'), 'UTF-8') ?>
			</div>
		<?php } ?>
	</div>
</div>
