<?php
use yii\helpers\Html;
?>

<div class="document document-form">
    <?= Html::img('@web/images/placeholders/document_icon.png') ?>
    <span><?= $file->name ?></span>
  
    <?= Html::img('@web/images/placeholders/garbage.png', ['alt' => \Yii::t('main', 'Delete'), 'class' => 'document-remove-btn', 'title' => \Yii::t('main', 'Delete')]) ?>
    <?= Html::hiddenInput($productForm->formName()."[{$productPrefix}][files][{$prefix}][document]", $file->document) ?>
    <?= Html::hiddenInput($productForm->formName()."[{$productPrefix}][files][{$prefix}][name]", $file->name) ?>
    <?= Html::hiddenInput($productForm->formName()."[{$productPrefix}][files][{$prefix}][makeCopy]", $file->makeCopy) ?>
</div>
