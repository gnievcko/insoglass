<?php 
	use yii\helpers\Html;
use yii\helpers\Url;
?>

<div class="groups-row">
	<?php foreach($groups as $group) { ?>
		<div class="group-row">
			<span class="photo-container">
				<?= '<div class="photo" style="background: url('.Url::to('@web/images/placeholders/group.png').')"></div>'; ?>
			</span>
			<span class="name"><?=  Html::a(Html::encode($group['name']) ,
								['client-group/details', 'id' => !empty($group['company_group_id']) ? $group['company_group_id'] : $group['id']]); 
//Html::encode($group['name']);
			?></span>    		
		</div>
	<?php } ?>
</div>