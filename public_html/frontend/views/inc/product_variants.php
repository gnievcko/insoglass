<?php
use yii\helpers\Url;
use frontend\models\ProductVariantForm;
?>

<div class="col-xs-12">
	<h4><?= Yii::t('web', 'Product versions') ?></h4>
	<div class="row">
		<div id="product-variants" class="items-container col-xs-12">
	        <?php 
	            $variantsForms = empty($productVariantsForms) ? [new ProductVariantForm()] : $productVariantsForms;
	            foreach($variantsForms as $prefix => $productVariantForm) {
	                echo \Yii::$app->controller->renderPartial('/inc/product_variant', [
	                    'form' => $form, 
	                    'prefix' => $prefix, 
	                    'productVariantForm' => $productVariantForm,
	                    'isWarehouse' => $isWarehouse,
	                ]);
	            }
	        ?>
		</div>
		<div class="col-sm-12">
			<div class="btn btn-link add-item-button" data-items-container-selector="#product-variants" data-item-view-url="<?= Url::to(['site/get-product-variant-form']) ?>">
				<span class="glyphicon glyphicon-plus-sign"></span>
		        <span><?= Yii::t('web', 'Add version') ?></span>
			</div>
		</div>
	</div>
</div>
