<?php
    use yii\helpers\Html;
    use yii\web\View;

    $formBaseName = $productVariantForm->formName();
    $activeFormBaseName = strtolower($formBaseName);
    $formName = "{$activeFormBaseName}-{$prefix}";
?>

<div class="row item" data-form-base-name="<?= $formBaseName ?>" data-active-form-base-name="<?= $activeFormBaseName ?>">
    <div class="col-sm-<?= !empty($isWarehouse) ? '4' : '5' ?>">
        <?php $fieldOptions = ['placeholder' => $productVariantForm->getAttributeLabel('name'), 'class' => 'form-control'] ?>
        <?php if(empty($form)): ?>
            <?php $fieldId = "{$formName}-name"; ?>

            <div id="container-<?= $fieldId ?>" class="form-group <?= $productVariantForm->isAttributeRequired('name') ? 'required' : '' ?>">
                 <?php //echo '<label class="control-label" for="'.$fieldId.'">'.$productVariantForm->getAttributeLabel('name').'</label>'; ?>
                <?=  Html::activeTextInput($productVariantForm, "[{$prefix}]name", $fieldOptions + ['id' => "{$fieldId}"]); ?>
                <div class="help-block"></div>
            </div>

            <?php
                $this->registerJs('
                    (function() {
                        $("#'.$formId.'").yiiActiveForm("add", {
                            id: "'.$fieldId.'",
                            name: "'."[{$prefix}]name".'",
                            container: "#container-'.$fieldId.'",
                            input: "#'."$fieldId".'",
                            enableClientValidation: false,
                            enableAjaxValidation: true
                        });
                    })();
                    ', View::POS_END); 
            ?>


        <?php else: ?>

            <?= $form->field($productVariantForm, "[{$prefix}]name")->textInput($fieldOptions) ?>

        <?php endif ?>
    </div>
    <?php $fieldOptions = ['placeholder' => $productVariantForm->getAttributeLabel('index'), 'class' => 'form-control'] ?>
    <?php if(!empty($isWarehouse)) { ?>
    	<div class="col-sm-2">
    		<?php if(empty($form)): ?>
	            <?php $fieldId = "{$formName}-index"; ?>
	
	            <div id="container-<?= $fieldId ?>" class="form-group <?= $productVariantForm->isAttributeRequired('index') ? 'required' : '' ?>">
					<?php //echo '<label class="control-label" for="'.$fieldId.'">'.$productVariantForm->getAttributeLabel('index').'</label>'; ?>
               		<?=  Html::activeTextInput($productVariantForm, "[{$prefix}]index", $fieldOptions + ['id' => "{$fieldId}"]); ?>
	                <div class="help-block"></div>
	            </div>
	
	            <?php
	                $this->registerJs('
	                    (function() {
	                        $("#'.$formId.'").yiiActiveForm("add", {
	                            id: "'.$fieldId.'",
	                            name: "'."[{$prefix}]index".'",
	                            container: "#container-'.$fieldId.'",
	                            input: "#'."$fieldId".'",
	                            enableClientValidation: false,
	                            enableAjaxValidation: true
	                        });
	                    })();
	                    ', View::POS_END); 
	            ?>
	        <?php else: ?>
	            <?= $form->field($productVariantForm, "[{$prefix}]index")->textInput($fieldOptions) ?>
	        <?php endif ?>
   		</div>
    <?php } ?>
    <?php $fieldOptions = ['placeholder' => $productVariantForm->getAttributeLabel('description'), 'class' => 'form-control'] ?>
    <div class="col-sm-<?= !empty($isWarehouse) ? '5' : '6' ?>">
        <?php if(empty($form)): ?>
            <?php $fieldId = "{$formName}-description"; ?>

            <div id="container-<?= $fieldId ?>" class="form-group <?= $productVariantForm->isAttributeRequired('description') ? 'required' : '' ?>">
                <?php //echo '<label class="control-label" for="'.$fieldId.'">'.$productVariantForm->getAttributeLabel('description').'</label>'; ?>
               	<?=  Html::activeTextInput($productVariantForm, "[{$prefix}]description", $fieldOptions + ['id' => "{$fieldId}"]); ?>
				<div class="help-block"></div>
            </div>

            <?php
                $this->registerJs('
                    (function() {
                        $("#'.$formId.'").yiiActiveForm("add", {
                            id: "'.$fieldId.'",
                            name: "'."[{$prefix}]name".'",
                            container: "#container-'.$fieldId.'",
                            input: "#'."$fieldId".'",
                            enableClientValidation: false,
                            enableAjaxValidation: true
                        });
                    })();
                    ', View::POS_END); 
            ?>

        <?php else: ?>
            <?= $form->field($productVariantForm, "[{$prefix}]description")->textInput($fieldOptions) ?>
        <?php endif ?>
    </div>
    <div class="col-sm-1">
        <div class="base-icon garbage-icon action-icon remove-item-btn"></div>

        <?php if(!empty($productVariantForm->variantId) && !empty($form)) {
            echo Html::activeHiddenInput($productVariantForm, "[$prefix]variantId");
        } ?>
    </div>
</div>
