<?php
use yii\helpers\Html;
use frontend\helpers\UserHelper;
use common\models\ar\User;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
?>

<div class="users-row">
	
	<?php foreach($users as $userRow) {
			$user = [];
			$user['id'] = !isset($userRow->id) ? $userRow['id'] : $userRow->id; 
			$user['email'] = !isset($userRow->id) ? $userRow['email'] : $userRow->email;
			$user['firstName'] = !isset($userRow->id) ? $userRow['firstName'] : $userRow->first_name;
			$user['lastName'] = !isset($userRow->id) ? $userRow['lastName'] : $userRow->last_name;
			$user['isMain'] = isset($userRow['isMain']) ? $userRow['isMain'] : '';	
			$user['urlPhoto'] = \common\helpers\UploadHelper::getNormalPath(!isset($userRow->urlPhoto) ? $userRow['urlPhoto'] : $userRow->url_photo, 'user_photo');
		?>
		<div class="user-row">
			<span class="photo-container">
				<?= '<div class="photo" style="background: url('.(!empty($user['urlPhoto']) ? $user['urlPhoto'] : Url::to('@web/images/placeholders/person.png')).')"></div>'; ?>
			</span>
			<span class="name"> <?= Html::a(UserHelper::getPrettyUserName($user['email'], $user['firstName'], $user['lastName']) , ['employee/details', 'id' => User::find()->where(['email' => $user['email']])->one()->id]);?></span>
    		<?php if(isset($user['isMain']) && !empty($user['isMain'])) { ?>
    			<span class="main"><?= '('.strtolower(Yii::t('web', 'Main')).')' ?></span>
    		<?php } ?>
		</div>
	<?php } ?>
</div>