<?php
use yii\helpers\Html;
use yii\helpers\Url;
use common\models\ar\FileTemporaryStorage;
?>

<div class="document">
    <?= Html::img('@web/images/placeholders/document_icon.png') ?>
    <span><?= $documentForm->name ?></span>
    <?= Html::img('@web/images/placeholders/garbage.png', 
    		[
    				'alt' => \Yii::t('main', 'Delete'), 
    				'class' => 'attachment-delete-btn', 
    				'data-id'=>$documentForm->id, 
    				'data-url'=>$documentForm->document , 
    				'title' => \Yii::t('main', 'Delete')
    				
    		]
    )?>
</div>

