<?php
use yii\helpers\Html;
use yii\helpers\Url;
?>

<div class="photo">
    <div>
        <?= Html::img($photoForm->thumbnail) ?>
    </div>
    <div class="photo-description">
    	<?= $photoForm->description; ?>
    </div>
</div>
