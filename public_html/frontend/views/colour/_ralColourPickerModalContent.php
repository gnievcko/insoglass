<?php

?>

<article class="groups">
	<ul>
		<?php foreach($groups as $group) { ?>
			<li><span class="group" data-target="<?= $group['symbol'] ?>" style="background-color: <?= $group['rgbHash'] ?>"><?= $group['name'] ?></span></li>
		<?php }?>
	</ul>
</article>

<?php foreach($colours as $groupName => $colourGroup) { ?>
	 <article class="group" data-id="<?= $groupName ?>">
	 	<ul>
	 		<?php foreach($colourGroup as $colour) {?>
	 			<li><span style="background-color: <?= $colour['rgbHash'] ?>" class="ral-span <?= $colour['isColourReversed'] ? 'reversed-colour' : ''?>" data-colour-id="<?= $colour['id'] ?>"><?= $colour['symbol'] ?></span></li>
	 		<?php } ?>
	 	</ul>
 	 	
     </article>
<?php }?>
