<?php
use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\View;
use dosamigos\multiselect\MultiSelect;
use common\helpers\StringHelper;
use common\helpers\Utility;
$this->title = Yii::t('web', 'List of clients with access to the system') . ' - ' . Yii::$app->name;

$this->params['breadcrumbs'][] = ['label' => StringHelper::translateOrderToOffer('main', 'Orders'), 'url' => Url::to(['order/index'])];
$this->params['breadcrumbs'][] = Yii::t('web', 'List of clients with access to the system');

$this->registerJsFile('@web/js/dynamic-table.js?' . uniqid(), []);
?>

<div id="company-representative-list-wrapper">
    <div class="row">
        <div class="col-sm-8 col-xs-12"><h2 class="page-title"><?= Yii::t('web', 'List of clients with access to the system') ?></h2></div>
        <div class="col-sm-4 col-xs-12 text-right"><?= Html::a(Yii::t('web', 'Add client representative'), ['client-representative/create'], ['class' => 'btn btn-primary']) ?></div>
        
    </div>
     <div id="client-representative-list-table" class="dynamic-table">
     	<h5><?php echo Yii::t('main','Filters')?></h5>
     	<div class="row dynamic-table-filters">
     		<div class="col-md-3 col-sm-6">
        		<label><?php echo Yii::t('web','Search by company name')?></label>
                    <input 
                        value="<?= $listForm->companyName ?>" 
                        class="form-control dynamic-table-filter" 
                        type="text"
                        placeholder = "<?= $listForm->getAttributeLabel('companyName') ?>"
                        name="<?= Html::getInputName($listForm, 'companyName') ?>">
            </div>
            <div class="col-md-3 col-sm-6">
        		<label><?php echo Yii::t('web','Search by name')?></label>
                    <input 
                        value="<?= $listForm->name ?>" 
                        class="form-control dynamic-table-filter" 
                        type="text"
                        placeholder = "<?= $listForm->getAttributeLabel('name') ?>"
                        name="<?= Html::getInputName($listForm, 'name') ?>">
            </div>
            <?php if(Yii::$app->params['canLoggedContactPerson']) { ?>
	            <div class="col-md-3 col-sm-6">
		            	<label><?= Yii::t('web', 'Search by log in possiblity') ?></label>
		            		<?php
		            		$values = [
		            				Utility::CLIENT_REPRESENTATIVE_ALL => Yii::t('web', 'All'),
		            				Utility::CLIENT_REPRESENTATIVE_LOG_PERMISSION_ONLY => Yii::t('web', 'Only with logging in possibility'),
		            				Utility::CLIENT_REPRESENTATIVE_LOG_PROHIBITION_ONLY => Yii::t('web', 'Only without logging in possibility'),
		            		];
		            		echo Html::activeDropDownList($listForm, 'canLog', $values, ['class' => 'form-control dynamic-table-filter no-margin-bottom']);
		            		?>
		        </div>
	        <?php } ?>
     	<?= Yii::$app->controller->renderPartial('/inc/listFiltersButtons', ['isMoreFilters' => false, 'isClearFilters' => true]) ?>
     	</div>
     	<div class="dynamic-table-data">
	     	<?= Yii::$app->controller->renderPartial('_client-representatives', [
		                'items' => $items,
		                'pages' => $pages,
		                'listForm' => $listForm,
	            ]) ?>
	    </div>
     </div>
     <?php 
    	$this->registerJs('dynamicTable("client-representative-list-table", "'.Url::to(['client-representative/list']).'");', View::POS_END); 
    ?>
</div>