<?php

use common\bundles\EnhancedDialogAsset;
use yii\helpers\Html;
use yii\helpers\Url;
use common\helpers\StringHelper;

$this->title = Yii::t('web', 'Client representative details') . ' - ' . Yii::$app->name;

$this->params['breadcrumbs'][] = ['label' => StringHelper::translateOrderToOffer('main', 'Orders'), 'url' => Url::to(['order/index'])];
$this->params['breadcrumbs'][] = ['label' => Yii::t('web', 'List of clients with access to the system'), 'url' => Url::to(['client-representative/list'])];
$this->params['breadcrumbs'][] = Yii::t('web', 'Client representative details');

$referrer = \Yii::$app->request->getReferrer();
$referredFromCreate = (substr($referrer, strripos($referrer, '/') + 1) == 'create');
?>

<div class="employee-details">
	<div class="row">
		<div class="col-sm-6"><h2 class="page-title"><?= Yii::t('web', 'Client representative details')?></h2></div>
		<div class="col-sm-6">
			<div class="pull-right"><?php
				$buttons = null;
				if($referredFromCreate) {
					if(Yii::$app->params['isClientRepresentativeManagementVisible']) {
						$buttons .= Html::a('<span class="glyphicon glyphicon-plus"></span>&nbsp;' . Yii::t('web', 'Add another contact person'), ['client-representative/create'], ['class' => 'btn btn-primary']);
					}
				}
				$buttons .= Html::a(Yii::t('main', 'Update'), Url::to(['client-representative/edit', 'id' => $data['id']]), ['class' => 'btn btn-default',])
				.Html::a(Yii::t('main', 'Delete'),
						['client-representative/delete'],
						[
								'class' => 'btn btn-default',
								'data' => [
						                'confirm' => Yii::t('main', 'Are you sure you want to delete this item?'),
						                'method' => 'post',
										'params' => [
												'id' => $data['id'],
										],
					            ],
						]);
				echo $buttons;
			?></div>
		</div>
	</div>
	
	<div class="col-xs-12 title-with-icon">
		<div class="base-icon basic-data-icon main-content-icons"></div><h3 class="page-subtitle"><?= Yii::t('web', 'General information') ?></h3>
	</div>
	<div class="col-xs-12">
		<div class="table-responsive">
			<table class="table table-bordered detail-table">
		    	<tbody>
		    		<tr>
		    			<th><?= Yii::t('web', 'Id') ?></th>
		    			<td><?= Html::encode($data['id']) ?></td>
		    		</tr>
		    		<tr>
		    			<th><?= Yii::t('web', 'Client') ?></th>
		    			<td><?= Html::a(Html::encode($data['companyName']), ['client/details', 'id' => $data['companyId']]) ?></td>
		    		</tr>
		    		<tr>
		    			<th><?= Yii::t('main', 'First name') ?></th>
		    			<td><?= Html::encode($data['firstName']) ?></td>
		    		</tr>
		    		<tr>
		    			<th><?= Yii::t('main', 'Last name') ?></th>
		    			<td><?= Html::encode($data['lastName']) ?></td>
		    		</tr>
		    		<tr>
		    			<th><?= Yii::t('main', 'E-mail address') ?></th>
		    			<td><?= Html::mailto(Html::encode($data['contactEmail'])) ?></td>
		    		</tr>
		    		<tr>
		    			<th><?= Yii::t('main', 'Phone') ?></th>
		    			<td><?= $data['phone1'] ?></td>
		    		</tr>
		    		<?php if(Yii::$app->params['canLoggedContactPerson']) { ?>
		    			<tr>
		    				<th><?= Yii::t('main', 'Can log in') ?></th>
		    				<td><?= StringHelper::boolTranslation($data['canLogged'], false) ?></td>
		    			</tr>
		    			<tr>
			    			<th><?= Yii::t('main', 'Last login') ?></th>
			    			<td><?= Html::encode(StringHelper::getFormattedDateFromTimestamp(strtotime($data['lastLogin']), true)) ?></td>
			    		</tr>
		    		<?php } ?>
					<tr>
		    			<th><?= Yii::t('main', 'Creation date') ?></th>
		    			<td><?= Html::encode(StringHelper::getFormattedDateFromTimestamp(strtotime($data['dateCreation']), true)) ?></td>
		    		</tr>
		    	</tbody>
			</table>
		</div>
	</div>
	<?php if(!empty($data['note'])) { ?>
		<div class="col-xs-12 title-with-icon">
			<div class="base-icon description-icon main-content-icons"></div><h3><?= Yii::t('main', 'Note') ?></h3>
		</div>
		<div class=" col-xs-12">
			<div class="row">
				<div class="col-sm-8 desc">
					<?= nl2br(Html::encode($data['note'])); ?>
				</div>
			</div>
		</div>
	<?php } ?>
</div>
<?php
	EnhancedDialogAsset::register($this);
?>