<?php

use yii\helpers\Url;
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\widgets\Select2;
use yii\web\JsExpression;
use common\helpers\StringHelper;

$subtitle = $model->isEditScenarioSet() ? Yii::t('web', 'Update contact person data') : Yii::t('web', 'Add contact person');
$this->title = $subtitle . ' - ' . Yii::$app->name;

if(($model->isEditScenarioSet() && strpos(Yii::$app->request->getReferrer(), '/client/') !== false) || (!$model->isEditScenarioSet() && $companyId)) {
	$this->params['breadcrumbs'][] = ['label' => StringHelper::translateOrderToOffer('main', 'Orders'), 'url' => Url::to(['order/index'])];
	$this->params['breadcrumbs'][] = ['label' => Yii::t('web', 'List of clients'), 'url' => Url::to(['client/list'])];
	$this->params['breadcrumbs'][] = ['label' => Yii::t('web', 'Client details'), 'url' => Url::to(['client/details', 'id' => $companyId])];
	$this->params['breadcrumbs'][] = $subtitle;
} else {
	$this->params['breadcrumbs'][] = ['label' => StringHelper::translateOrderToOffer('main', 'Orders'), 'url' => Url::to(['order/index'])];
	$this->params['breadcrumbs'][] = ['label' => Yii::t('web', 'List of clients with access to the system'), 'url' => Url::to(['client-representative/list'])];
	$this->params['breadcrumbs'][] = $subtitle;
}

?>

<div class="client-representative-form">
    <h2 class="page-title"><?= $subtitle ?></h2>

    <?php
    $form = ActiveForm::begin([
                'id' => 'client-representative-form-ajax',
                'enableAjaxValidation' => true,
    ]);
    ?>

    <?php
    if($model->isEditScenarioSet()) {
        $form->field($model, 'id')->hiddenInput()->label(false);
    }
    ?>

    <div class="row">
        <div class="col-sm-6"><?php
            echo $form->field($model, 'companyId')
                    ->widget(Select2::classname(), [
                        'language' => Yii::$app->language,
                        'options' => ['placeholder' => Yii::t('main', 'Select a company...')],
                        'data' => $companyData,
                        'pluginOptions' => [
                            'allowClear' => true,
                            'minimumInputLength' => Yii::$app->params['isClientListImmediatelyVisible'] ? 0 : 1,
                            'ajax' => [
                                'url' => Url::to(['company/find']),
                                'dataType' => 'json',
                                'delay' => 250,
                                'data' => new JsExpression('function(params) { return { term:params.term, page:params.page, size: 20 }; }'),
                                'processResults' => new JsExpression('function (data, params) {
												params.page = params.page || 1;
	
												return {
													results: data.items,
													pagination: {
														more: data.more
													}
												};
											}'),
                                'cache' => true
                            ],
                            'templateResult' => new JsExpression('function(item) { if(item.loading) return item.text; return item.name; }'),
                            'templateSelection' => new JsExpression('function (item) { return item.name || item.text; }'),
                        ]
            ]);
            ?>
            <a href="<?= Url::to(['client/create']); ?>" target="_blank" class="focusable-link  add-item-after-select2">
                <span class="glyphicon glyphicon-plus-sign pull-left"></span>
                <span> <?=Yii::t('web', 'Create new company')?></span>
            </a>
        </div>
    </div>

    <div class="row">
        <div class="col-sm-6"><?= $form->field($model, 'firstName')->textInput(['maxlength' => true]) ?></div>
        <div class="col-sm-6"><?= $form->field($model, 'lastName')->textInput(['maxlength' => true]) ?></div>
    </div>

    <div class="row">
        <div class="col-sm-6"><?= $form->field($model, 'email')->textInput(['maxlength' => true]) ?></div>
        <div class="col-sm-6"><?= $form->field($model, 'phone1')->textInput(['maxLength' => true]); ?></div>
    </div>

    <div class="row">
        <div class="col-sm-6"><?= $form->field($model, 'note')->textarea(['maxlength' => true]) ?></div>
    </div>
    <?php if(Yii::$app->params['canLoggedContactPerson']) { ?>
        <div class="row">
            <div class="col-sm-6">
                <?= $form->field($model, 'password')->passwordInput(['maxlength' => true]); ?>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-6"><?= $form->field($model, 'canLogged')->checkbox() ?></div>
        </div>
    <?php } ?>
    <div class="row col-sm-12">
    	<?php $cancelButtonLink = strpos(Yii::$app->request->getReferrer(), '/client/') !== false ? ['client/details', 'id' => $companyId] : ['client-representative/list'] ?>
        <div class="pull-left"><?= Html::a(Yii::t('web', 'Cancel'), $cancelButtonLink , ['class' => 'btn btn-default']) ?></div>
        <div class="pull-right"><?= Html::submitButton(($model->isEditScenarioSet() ? Yii::t('main', 'Save') : Yii::t('web', 'Add')), ['class' => 'btn btn-primary']); ?></div>
    </div>

    <?php ActiveForm::end(); ?>	
</div>

