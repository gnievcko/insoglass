<?php
use yii\helpers\Url;
use yii\helpers\Html;
use yii\widgets\LinkPager;
use common\bundles\EnhancedDialogAsset;
use frontend\models\ProductListForm;
use common\helpers\Utility;
use common\helpers\StringHelper;
use common\widgets\TableHeaderWidget;
use yii\base\Widget;
use common\models\aq\WarehouseProductQuery;
?>

<?php
if (empty($items)) { ?>
	<label><?= Yii::t('web', 'No search results');?></label>
	<br><br>
<?php }
	else { ?>
	<div class="table-responsive">
	    <table class="<?= TableHeaderWidget::TABLECLASS ?>">
	    <?php echo TableHeaderWidget::widget(['fields' => ProductListForm::getSortFields(), 'listForm' => $listForm]);?>

	        <tbody>
	            <?php foreach($items as $item): ?>
	                <tr>
                    <?php if(Yii::$app->params['isProductPhotoOnListVisible']){ ?>
                        <td><span class="photo-container">
							<?= '<div class="photo-medium" style="background: url('.(!empty($item['urlPhoto']) ? \common\helpers\UploadHelper::getNormalPath($item['urlPhoto'], 'product_photo') : Url::to('@web/images/placeholders/product.png')).')"></div>'; ?>
							<?php if(!empty($item['urlPhoto'])) { ?>
								<div class="photo-tooltip" data-id="<?= $item['id'] ?>"></div>
							<?php } ?>
						</span></td>
                    <?php } ?>
	                    <td><?= Html::encode($item['name']) ?></td>
	                   <!--  <td><?= Html::encode($item['index']) ?></td>  -->
	                   	<td><?php if(!empty($item['stillage']) && !empty($item['shelf'])) {
	                   		echo !empty($item['stillage']) ? Html::encode($item['stillage']) : '-';
	                   		echo '/';
	    	                echo !empty($item['shelf']) ? Html::encode($item['shelf']) : '-';
	                   	}
	                   	else {
	                   		Html::encode($item['stillage']);
	                   	}
	                   	 ?></td>
	                    <td><?= Html::encode($item['supplier']) ?></td>
	                    <td><?= Html::encode($item['category']) ?></td>
	                    <td><?= Html::encode($item['version']) ?></td>
                        <?php if(Yii::$app->params['isProductCanBeOffered']) { ?>
                            <td><?= Html::encode(StringHelper::boolTranslation($item['isVisibleForClient'], false)) ?></td>
                        <?php }?>
	                    <td><?= Html::encode(StringHelper::getFormattedDateFromTimestamp($item['dateCreation'])) ?>
	                    <td><?php
	                    	echo Html::a('', Url::to(['product/details', 'id' => $item['id']]), ['class' => 'base-icon details-icon action-icon', 'title' => Yii::t('web', 'Details')]);
	                    	echo '&nbsp;'.Html::a('', Url::to(['product/edit', 'id' => $item['id']]), ['class' => 'base-icon editing-icon action-icon', 'title' => Yii::t('web', 'Edit')]);
							
	                    	$confirmMessage = Yii::t('main', 'Are you sure you want to delete this item?');
							if (WarehouseProductQuery::productCount($item['id'])->one()['count'] > 0) {
								$confirmMessage .= '<br/><strong>' . Yii::t('main', 'If you confirm, all products of this kind will be deleted from warehouses!') . '</strong>';
							}
		                    echo '&nbsp;'.Html::a('', Url::to(['product/delete']), [
									'class' => 'base-icon delete-icon action-icon',
                                    'title' => Yii::t('main', 'Delete'),
									'data' => [
										    'confirm' => $confirmMessage,
										    'method' => 'post',
											'params' => [
													'id' => $item['id'],
											],
								    ],
							]);
	                    ?>
	                </tr>
	            <?php endforeach ?>
	        </tbody>
	    </table>
	</div>
	<div class="col-sm-12">
		<div class="col-sm-4 text-left dynamic-table-counter">
			<?= Yii::t('web', 'Number of found items: {n}', ['n' => $pages->totalCount]) ?>
		</div>
		<div class="col-sm-8 text-right dynamic-table-pagination">
		    <?= LinkPager::widget(['pagination' => $pages ]) ?>
		</div>
	</div>
<?php }?>

<input class="dynamic-table-sort-dir" type="hidden" name="<?= Html::getInputName($listForm, 'sortDir') ?>" value="<?= $listForm->sortDir ?>" />
<input class="dynamic-table-sort-field" type="hidden" name="<?= Html::getInputName($listForm, 'sortField') ?>" value="<?= $listForm->sortField ?>" />
<input class="dynamic-table-page" type="hidden" name="<?= Html::getInputName($listForm, 'page') ?>" value="<?= $listForm->page ?>" />

<?php
	EnhancedDialogAsset::register($this);
?>
