<?php
use yii\helpers\Url;
use frontend\models\ProductAliasForm;
?>
<div class="col-xs-12">
	<h4><?= Yii::t('web', 'Aliases') ?></h4>
	<div id="product-aliases" class="items-container">
        <?php 
            $aliasesForms = empty($productAliasesForms) ? [new ProductAliasForm()] : $productAliasesForms;
            foreach($aliasesForms as $prefix => $productAliasForm) {
                echo \Yii::$app->controller->renderPartial('inc/_alias', [
                    'form' => $form, 
                    'prefix' => $prefix, 
                    'productAliasForm' => $productAliasForm,
                ]);
            }
        ?>
	</div>
    <div class="btn btn-link add-item-button" data-items-container-selector="#product-aliases" data-item-view-url="<?= Url::to(['product/get-alias-form']) ?>">
		<span class="glyphicon glyphicon-plus-sign"></span>
        <span><?= Yii::t('web', 'Add alias') ?></span>
	</div>
</div>
