<?php
    use yii\helpers\Html;
    use yii\web\View;

    $formBaseName = $productAliasForm->formName();
    $activeFormBaseName = strtolower($formBaseName);
    $formName = "{$activeFormBaseName}-{$prefix}";
?>

<div class="row item" data-form-base-name="<?= $formBaseName ?>" data-active-form-base-name="<?= $activeFormBaseName ?>">
    <div class="col-sm-5">
        <?php $fieldOptions = ['class' => 'form-control', 'placeholder' => $productAliasForm->getAttributeLabel('name')] ?>
        <?php if(empty($form)): ?>
            <?php $fieldId = "{$formName}-name"; ?>

            <div id="container-<?= $fieldId ?>" class="form-group <?= $productAliasForm->isAttributeRequired('name') ? 'required' : '' ?>">
                
                <?php //echo '<label class="control-label" for="'.$fieldId.'">'.$productAliasForm->getAttributeLabel('name').'</label>'; ?>
               
                <?=  Html::activeTextInput($productAliasForm, "[{$prefix}]name", $fieldOptions + ['id' => "{$fieldId}"]); ?>
                <div class="help-block"></div>
            </div>

            <?php
                $this->registerJs('
                    (function() {
                        $("#'.$formId.'").yiiActiveForm("add", {
                            id: "'.$fieldId.'",
                            name: "'."[{$prefix}]name".'",
                            container: "#container-'.$fieldId.'",
                            input: "#'."$fieldId".'",
                            enableClientValidation: false,
                            enableAjaxValidation: true
                        });
                    })();
                    ', View::POS_END); 
            ?>


        <?php else: ?>
            <?= $form->field($productAliasForm, "[{$prefix}]name")->textInput($fieldOptions)->label(false) ?>
        <?php endif ?>
    </div>
    <div class="col-sm-1">
        <div class="base-icon garbage-icon action-icon remove-item-btn"></div>
          
        <?php if(!empty($productVariantForm->variantId) && !empty($form)) {
            echo Html::activeHiddenInput($productVariantForm, "[$prefix]aliasId");
        } ?>
    </div>
</div>
