<?php


use kartik\select2\Select2;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\JsExpression;
use yii\helpers\ArrayHelper;
use common\models\aq\WarehouseQuery;
use common\models\ar\WarehouseSupplier;
use yii\web\View;
?>

<div class="col-xs-12 title-with-icon">
	<div class="base-icon basic-data-icon main-content-icons"></div>
	<h3><?= Yii::t('web', 'Basic data') ?></h3>
</div>

    <?php foreach($productTranslationsForms as $i => $translationForm) { ?>
        <?= $form->field($translationForm, "[{$i}]languageId")->hiddenInput()->label(false) ?>
        <div class="col-xs-12">
        	<div class="row">
	            <div class="col-sm-6">
	                <?= $form->field($translationForm, "[{$i}]name")->textArea(['placeHolder' => $translationForm->getAttributeLabel('name'), 'class' => 'form-control smallTextarea']) ?>
	            </div>
	            <div class="col-sm-5 col-sm-offset-1">
	            	<div class="row">
	            		<div class="col-xs-4">
	                		<?= $form->field($translationForm, "[{$i}]price")->textInput(['type' => 'number', 'step' => 0.01, 'placeHolder' => $translationForm->getAttributeLabel('price')]) ?>
	           			</div>
	           			<div class="col-xs-4">
	                		<?= $form->field($translationForm, "[{$i}]currencyId")->dropDownList($currencies) ?>
	                	</div>
	                	<div class="col-xs-4">
	                		<?php if(!empty(Yii::$app->params['defaultUnit'])): ?>
	                			<?= $form->field($translationForm, "[{$i}]unit")->textInput(['maxLength' => true, 'value' => Yii::t('documents',Yii::$app->params['defaultUnit']),'readonly'=>true]) ?>
	                		<?php else: ?>
	                			<?= $form->field($translationForm, "[{$i}]unit")->textInput(['maxLength' => true, 'placeHolder' => $translationForm->getAttributeLabel('unit')]) ?>
	                		<?php endif; ?>
	                	</div>
	                </div>
				</div>
			</div>
		</div>
		<div class="col-xs-12">
			<div class="row">
				<div class="col-sm-6">
					<div class="row">
    					<div class="col-xs-12">
    			      		<?= $form->field($productForm, 'warehouseSupplierId')->widget(Select2::classname(), [
    			                    'language' => Yii::$app->language,
    			                    'showToggleAll' => false,
    			                    'options' => ['placeholder' => Yii::t('web', 'Select a supplier...')],
    			                    'data' => array_map(function($manufacturer) { return [$manufacturer->id => $manufacturer->name]; }, WarehouseSupplier::find()->all()),
    			                    'pluginOptions' => [
    			                            'multiple' => false,
    			                            'allowClear' => true,
    			                            'minimumInputLength' => 1,
    			                            'ajax' => [
    			                                    'url' => Url::to(['warehouse-supplier/find']),
    			                                    'dataType' => 'json',
    			                                    'delay' => 250,
    			                                    'data' => new JsExpression('function(params) { 
    			                                        return {term:params.term, page:params.page, size: 20}; 
    			                                    }'),
    			                                    'processResults' => new JsExpression('function (data, params) { 
    			                                        params.page = params.page || 1; 
    			                                        
    			                                        return {
    			                                            results: data.items,
    			                                            pagination: {
    			                                                more: data.more
    			                                            }
    			                                        };
    			                                    }'),
    			                                    'cache' => true
    			                            ],
    			                            'templateResult' => new JsExpression('function(item) { if(item.loading) return item.text; return item.name; }'),
    			                            'templateSelection' => new JsExpression('function (item) { return item.name || item.text; }'),
    			                    ]
    			                ]);
    			           ?>
                            <a href="<?= Url::to(['warehouse-supplier/create']); ?>" target="_blank" class="focusable-link  add-item-after-select2 add-item-after-select2-no-required">
                                <span class="glyphicon glyphicon-plus-sign pull-left"></span>
                                <span> <?=Yii::t('web', 'Create new supplier')?></span>
                            </a>
    		       		</div>

    		       	</div>
				</div>
				<div class="col-sm-5 col-sm-offset-1">
					<div id="manufacturer-info"></div>
				</div>
			</div>
		</div>
		<div class="col-xs-12">
			<div class="row">
	            <div class="col-sm-6">
	            	<div class="row">
    	            	<div class="col-xs-12">
    	            		<?= $form->field($productForm, 'index')->textInput(['maxLength' => true, 'placeHolder' => $productForm->getAttributeLabel('index')]) ?>
    	            	</div>
    	            	<div class="col-xs-12">
    			            <?= $form->field($productForm, 'categoriesIds')->widget(Select2::classname(), [
    			                    'language' => Yii::$app->language,
    			                    'showToggleAll' => false,
    			                    'options' => ['placeholder' => Yii::t('web', 'Select a category...')],
    			                    'data' => array_map(function($category) { return [$category->product_category_id => $category->name]; }, $productCategories),
    			                    'pluginOptions' => [
    			                            'multiple' => true,
    			                            'allowClear' => true,
    			                            'minimumInputLength' => 1,
    			                            'ajax' => [
    			                                    'url' => Url::to(['product-category/find']),
    			                                    'dataType' => 'json',
    			                                    'delay' => 250,
    			                                    'data' => new JsExpression('function(params) { 
    			                                        return {term:params.term, page:params.page, size: 20}; 
    			                                    }'),
    			                                    'processResults' => new JsExpression('function (data, params) { 
    			                                        params.page = params.page || 1; 
    			                                        
    			                                        return {
    			                                            results: data.items,
    			                                            pagination: {
    			                                                more: data.more
    			                                            }
    			                                        };
    			                                    }'),
    			                                    'cache' => true
    			                            ],
    			                            'templateResult' => new JsExpression('function(item) { if(item.loading) return item.text; return item.name; }'),
    			                            'templateSelection' => new JsExpression('function (item) { return item.name || item.text; }'),
    			                    ]
    			                ]);
    		            	?>
                            <a href="<?= Url::to(['product-category/create']); ?>" target="_blank" class="focusable-link  add-item-after-select2 add-item-after-select2-no-required">
                                <span class="glyphicon glyphicon-plus-sign pull-left"></span>
                                <span> <?=Yii::t('web', 'Create new product category')?></span>
                            </a>
    	        		</div>
	        		</div>
	            </div>
	            <div class="col-sm-5 col-sm-offset-1">
		            <?= $form->field($translationForm, "[{$i}]description")->textarea(['placeHolder' => $translationForm->getAttributeLabel('description'), 'class' => 'form-control desc']) ?>
		       </div>
	  		</div>
        </div>
		
    <?php } ?>
    <div class="col-xs-12">
    	<div class="row">
	        <div class="col-sm-5">
                <?= $form->field($productForm, 'isAvailable')->checkbox() ?>
                <?= $form->field($productForm, 'isVisibleForClient', ['options'=>['class'=> (Yii::$app->params['isProductCanBeOffered'] ? '' : 'hidden')]])->checkbox(); ?>
            </div>
	        <div class="col-sm-5 col-sm-offset-2">
	            <?= $form->field($productForm, 'countMinimal')->textInput(['type' => 'number', 'integer' => true, 'placeHolder' => $productForm->getAttributeLabel('countMinimal')]) ?>
	            <?php if($isCreate) { ?>
	        		<?= $form->field($productForm, 'initialCount')->textInput(['type' => 'number', 'integer' => true, 'placeHolder' => $productForm->getAttributeLabel('initialCount')]) ?>
		        	<?php if(Yii::$app->params['isMoreWarehousesAvailable']) { ?>	        	
		        		<?= $form->field($productForm, 'warehouseId')->dropDownList(ArrayHelper::map(
		        				WarehouseQuery::getWarehouseList()->all(), 'id', 'name'
		        		)) ?>	        	
	        		<?php } ?>
				<?php }
				else { ?>
					<?= Html::a(Yii::t('web', 'Add items'), Url::to(['warehouse-product/create', 'productId' => $productId]), ['class' => 'btn btn-primary btn-sm', 'target' => '_blank']) ?>
					<?= Html::a(Yii::t('web', 'Change number of items'), Url::to(['warehouse-product/update', 'productId' => $productId]), ['class' => 'btn btn-primary btn-sm', 'target' => '_blank']) ?>
				<?php } ?>
	        </div>
        </div>
    </div>
    <div class="col-xs-12">
    	<div class="row">
	        <div class="col-sm-6">
	        	<?= $form->field($translationForm, "[{$i}]remarks")->textarea(['maxLength' => true, 'placeHolder' => $translationForm->getAttributeLabel('remarks')])
	        			->hint(Yii::t('web', 'Remarks will be displayed on warehouse count summary of this product.')) ?>
	        </div>
	        <div class="col-sm-6">
	            <?= $form->field($productForm, "stillage")
	                    ->textInput(['maxLength' => true, 'placeHolder' => $productForm->getAttributeLabel('stillage')])
	            ?>
	            <?= $form->field($productForm, "shelf")
	                    ->textInput(['maxLength' => true, 'placeHolder' => $productForm->getAttributeLabel('shelf')])
	            ?>
	        </div>
		</div>
	</div>
	
<?php
$this->registerJs('initProductFormDynamicFeatures("'.Url::to(['product/']).'");', View::POS_END);
?>