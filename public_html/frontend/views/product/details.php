<?php
use common\models\aq\WarehouseProductQuery;
use yii\helpers\Html;
use yii\helpers\Url;
use frontend\helpers\UserHelper;
use dosamigos\gallery\Gallery;
use common\bundles\EnhancedDialogAsset;
use common\models\ar\User;
use common\helpers\StringHelper;
use frontend\helpers\AddressHelper;


$this->title = Yii::t('web', 'Product details') . ' - ' . Yii::$app->name;

$this->params['breadcrumbs'][] = ['label' => Yii::t('main', 'Warehouse'), 'url' => Url::to(['warehouse/index'])];
$this->params['breadcrumbs'][] = ['label' => Yii::t('web', 'List of products'), 'url' => Url::to(['product/list'])];
$this->params['breadcrumbs'][] = Yii::t('web', 'Product details');

?>

<div class="product-details">
	<div class="row">
		<div class="col-sm-6">
			<h2 class="page-title"><?= Yii::t('web', 'Product details') ?></h2>
		</div>
		<div class="col-sm-6 text-right">
			<?php
				$buttons = '';
				$referrer = Yii::$app->request->getReferrer();
				if (substr($referrer, strripos($referrer, '/') + 1) == 'create') {
					$buttons .= Html::a(Yii::t('web', 'Add another product'),
										['product/create'], ['class' => 'btn btn-primary']);
				}
				$buttons .= Html::a(Yii::t('main', 'Update'),
						['product/edit', 'id' => $data['details']['id']], ['class' => 'btn btn-default']);
				$options = [
						'class' => 'btn btn-default',
				];
				
				$confirmMessage = Yii::t('main', 'Are you sure you want to delete this item?');
				if(WarehouseProductQuery::productCount($data['details']['id'])->one()['count'] != 0) {
					$confirmMessage .= '<br/><strong>' . Yii::t('main', 'If you confirm, all products of this kind will be deleted from warehouses!') . '</strong>';
				}
				$options['data'] = [
						'confirm' =>  $confirmMessage,
						'method' => 'post',
						'params' => [
								'id' => $data['details']['id'],
						],
				];

				$buttons .= Html::a(Yii::t('main', 'Delete'),
						['product/delete'], $options);
				echo $buttons;
			?>
		</div>
	</div>
	<div class="col-xs-12 title-with-icon">
		<div class="base-icon basic-data-icon main-content-icons"></div>
		<h3><?= Yii::t('web', 'General information') ?></h3>
	</div>
	<div class="col-xs-12">
		<div class="table-responsive">
			<table class="table table-bordered detail-table">
		    	<tbody>
		    		<tr>
		    			<th><?= Yii::t('web', 'Name') ?></th>
		    			<td><?= Html::encode($data['details']['name']) ?></td>
		    		</tr>
		    		<tr>
		    			<th><?= Yii::t('web', 'Supplier') ?></th>
		    			<td><?= $data['details']['manufacturer']; ?></td>
		    		</tr>
		    		<tr>
		    			<th><?= Yii::t('main', 'Warehouse index') ?></th>
		    			<td><?= Html::encode($data['details']['index']) ?></td>
		    		</tr>
		    		<tr>
		    			<th><?= Yii::t('web', 'Addition date') ?></th>
		    			<td><?= Html::encode(StringHelper::getFormattedDateFromDate($data['details']['dateCreation'], true)) ?></td>
		    		</tr>
		    		<?php if(Yii::$app->params['isProductLastDeliveryCostIsDefaultCost'] && !empty($data['details']['priceLastDelivery'])) { ?>
		    			<tr>
		    				<th><?= Yii::t('web', 'Price') ?></th>
		    				<td><?= Html::encode(StringHelper::getFormattedCost($data['details']['priceLastDelivery']).' '.$data['details']['currencyLastDelivery']) ?></td>
		    			</tr>
					<?php } 
		    		elseif(!Yii::$app->params['isProductLastDeliveryCostIsDefaultCost'] && !empty($data['details']['price'])) { ?>
		    			<tr>
		    				<th><?= Yii::t('web', 'Price') ?></th>
		    				<td><?= Html::encode(StringHelper::getFormattedCost($data['details']['price']).' '.$data['details']['currency']) ?></td>
		    			</tr>
					<?php } ?>
					<?php if(!empty($data['details']['unit'])) { ?>
						<tr>
							<th><?= Yii::t('main', 'Unit') ?></th>
							<td><?= Html::encode($data['details']['unit']) ?>
						</tr>
					<?php } ?>
					<?php if(!empty($data['details']['alias'])) { ?>
			    		<tr>
			    			<th><?= Yii::t('web', 'Common names') ?></th>
			    			<td><?= Html::encode($data['details']['alias']) ?></td>
			    		</tr>
		    		<?php } ?>
		    		<tr>
		    			<th><?= Yii::t('web', 'Categories') ?></th>
		    			<td><?= Html::encode($data['details']['category']) ?></td>
		    		</tr>
		    		<?php if(!empty($data['details']['version'])) { ?>
			    		<tr>
			    			<th><?= Yii::t('web', 'Versions') ?></th>
			    			<td><?= Html::encode($data['details']['version']) ?></td>
			    		</tr>
		    		<?php } ?>
		    		<?php if(!empty($data['details']['countMinimal'])) { ?>
		    			<tr>
		    				<th><?= Yii::t('main', 'Desired minimal count') ?></th>
		    				<td><?= Html::encode($data['details']['countMinimal']) ?></td>
		    			</tr>
		    		<?php } ?>
		    		<tr>
		    			<th><?= Yii::t('web', 'Is available') ?></th>
		    			<td><?= Html::encode(StringHelper::boolTranslation($data['details']['isAvailable'], false)) ?></td>
		    		</tr>
                    <?php if(\Yii::$app->params['isProductCanBeOffered']) { ?>
		    		<tr>
		    			<th><?= Yii::t('main', 'Is visible for client') ?></th>
		    			<td><?= Html::encode(StringHelper::boolTranslation($data['details']['isVisibleForClient'], false)) ?></td>
		    		</tr>
                    <?php } ?>
		    		<tr>
		    			<th><?= Yii::t('web', 'Added by') ?></th>
		    			<td><?= Html::a(Html::encode(UserHelper::getPrettyUserName
			    						($data['details']['email'], $data['details']['firstName'], $data['details']['lastName'])) ,
			    						['employee/details', 'id' => User::find()->where(['email' => $data['details']['email']])->one()->id]);
		    			 ?></td>
		    		</tr>
		    		<tr>
		    			<th><?= Yii::t('web', 'Stillage') ?></th>
		    			<td><?= Html::encode($data['details']['stillage']) ?></td>
		    		</tr>
	                <tr>
		    			<th><?= Yii::t('web', 'Shelf') ?></th>
		    			<td><?= Html::encode($data['details']['shelf']) ?></td>
		    		</tr>
		    		<?php if(!empty($data['attachments'])) { ?>
						<tr>
	                        <th><?= Yii::t('web', 'Attachments') ?></th>
	                    	<td><?= $this->render('/inc/attachmentList', ['attachments' => $data['attachments']]) ?></td>
	                	</tr>
	                <?php } ?>
		    	</tbody>
			</table>
		</div>
	</div>
	<div class="col-xs-12 title-with-icon">
		<div class="base-icon description-icon main-content-icons"></div>
		<h3><?= Yii::t('main', 'Description') ?></h3>
	</div>
		
	<!-- erased class "description" from the div below -->	
	<div class="col-xs-12">
		<div class="row">
			<?php if (!empty($data['details']['description'])) { ?>
			<div class="col-sm-8 desc">
				<?php echo Html::encode($data['details']['description']); ?>
			</div>
			<?php }	else { ?>
			<div class="col-sm-8 desc no-desc">
				<?php echo Yii::t('web', 'No data'); ?>
			</div>
			<?php } ?>
			
		</div>
	</div>

	<?php if(!empty($data['photos'])) { ?>
	<div class="col-xs-12">
		<div class="gallery">
			<h3 class="page-subtitle"><?= Yii::t('web', 'Photo gallery') ?></h3>
			<?= Gallery::widget(['items' => $data['photos']]); ?>
		</div>
	</div>
	<?php } ?>
</div>
<?php
	EnhancedDialogAsset::register($this);
?>
