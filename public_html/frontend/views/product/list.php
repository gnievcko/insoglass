<?php
use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\View;
use dosamigos\multiselect\MultiSelect;

$this->title = Yii::t('web', 'List of products') . ' - ' . Yii::$app->name;

$this->params['breadcrumbs'][] = ['label' => Yii::t('main', 'Warehouse'), 'url' => Url::to(['warehouse/index'])];
$this->params['breadcrumbs'][] = Yii::t('web', 'List of products');

$this->registerJsFile('@web/js/dynamic-table.js?'.uniqid(), []);
$this->registerJsFile('@web/js/photo-tooltip.js?'.uniqid(), ['depends' => [\yii\web\JqueryAsset::className()]]);
?>

<div id="product-list-wrapper">    
    <div class="row">
		<div class="col-sm-6 col-xs-9"><h2 class="page-title"><?= Yii::t('web', 'List of products') ?></h2></div>
		<div class="col-sm-6 col-xs-3 text-right"><?= Html::a(Yii::t('web', 'Add product'), ['product/create'], ['class' => 'btn btn-primary']) ?>
		</div>
	</div>

    <div id="product-list-table" class="dynamic-table">
    	<h5><?php echo Yii::t('main','Filters')?></h5>
        <div class="row dynamic-table-filters">
        	<input value="<?= $listForm->allCategoriesCount ?>" class="form-control dynamic-table-filter" type="hidden" name="<?= Html::getInputName($listForm, 'allCategoriesCount') ?>"/>
            <div class="col-md-3 col-sm-4">
                <label><?php echo Yii::t('web','Search by name')?></label>
                    <input 
                        value="<?= $listForm->name ?>" 
                        class="form-control dynamic-table-filter" 
                        type="text"
                        placeHolder="<?= $listForm->getAttributeLabel('name')?>"
                        name="<?= Html::getInputName($listForm, 'name') ?>">
                
            </div> 
            <div class="col-md-3 col-sm-4 cat-select">
                <label><?php echo Yii::t('web','Filter by category')?></label>
                    <?php 
                        $data = array_reduce($categoryTranslations, function($options, $t) {
                            $options[$t['id']] = Html::encode($t['name']);
                            return $options;
                        }, []);
                        
                        if(!empty($data)) {
                        	//echo $listForm->getAttributeLabel('categoryIds');	
                        	echo MultiSelect::widget([
                        			'id' => 'categories',
                        			"options" => ['multiple' => 'multiple', 'class' => 'dynamic-table-filter'],
                        			'data' => $data,
                        			'name' => Html::getInputName($listForm, 'categoryIds'),
                        			'value' => empty($listForm->categoryIds) ? array_keys($data) : $listForm->categoryIds,
                        			"clientOptions" => [
                        					'nonSelectedText' => mb_strtolower(Yii::t('web', 'Select a category...'), 'UTF-8'),
                        					'nSelectedText' => mb_strtolower(Yii::t('web', 'Selected category'), 'UTF-8'),
                        					'numberDisplayed' => 0,
                        					'buttonWidth' => '100%',
                        			],
                        	]);	
                        }        
                    ?>
            </div>
            <div class="col-md-3 col-sm-4">
                <label><?php echo Yii::t('web','Search by location')?></label>
                    <input 
                        value="<?= $listForm->location ?>" 
                        class="form-control dynamic-table-filter" 
                        type="text"
                        placeHolder="<?= $listForm->getAttributeLabel('location')?>"
                        name="<?= Html::getInputName($listForm, 'location') ?>">
                
            </div>   
            <?= Yii::$app->controller->renderPartial('/inc/listFiltersButtons', ['isMoreFilters' => false, 'isClearFilters' => true]) ?>
        </div>
        <div class="dynamic-table-data">
            <?= Yii::$app->controller->renderPartial('_products', [
	                'items' => $items,
	                'pages' => $pages,
	                'listForm' => $listForm,
            ]) ?>
        </div>
    </div>
    <?php 
    	$this->registerJs('dynamicTable("product-list-table", "'.Url::to(['product/list']).'");', View::POS_END); 
    	$this->registerJs('initPhotoTooltip("'.Url::to(['product/get-main-photo']).'");', View::POS_END);
    ?>
</div>
