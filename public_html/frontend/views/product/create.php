<?php
use yii\helpers\Url;
use yii\helpers\Html;
use common\components\ActiveForm;
use yii\web\View;

$this->registerJsFile('@web/js/forms.js?'.uniqid(), ['depends' => [\yii\web\JqueryAsset::className()]]);
$this->registerJsFile('@web/js/pages/product-form.js?'.uniqid(), ['depends' => [\yii\web\JqueryAsset::className()]]);

$subtitle = Yii::$app->params['isOfferedProductDependOnProduct'] ? Yii::t('web', 'Create product or intermediate product') : Yii::t('web', 'Create product');
$this->title = $subtitle . ' - ' . Yii::$app->name;

$this->params['breadcrumbs'][] = ['label' => Yii::t('main', 'Warehouse'), 'url' => Url::to(['warehouse/index'])];
$this->params['breadcrumbs'][] = ['label' => Yii::t('web', 'List of products'), 'url' => Url::to(['product/list'])];
$this->params['breadcrumbs'][] = $subtitle;
?>

<div class="product-form" id="product-create">
	<h2 class="page-title"><?= $subtitle ?></h2>
    
    <?php 
        $form = ActiveForm::begin([
            'id' => 'products-form',
            'enableAjaxValidation' => true,
            'enableClientValidation' => false,
            'options' => [
                'class' => 'form',
            ]
        ]);
        echo $form->errorSummary([]);
    ?>

    <?php echo 
        Yii::$app->controller->renderPartial('inc/_basic_data', [
            	'form' => $form,
            	'productTranslationsForms' => $productTranslationsForms,
            	'currencies' => $currencies,
            	'productForm' => $productForm,
            	'productCategories' => $productCategories,
        		'isCreate' => true,
        ]);
    ?>

    <?php echo 
        Yii::$app->controller->renderPartial('/inc/product_variants', [
            'form' => $form,
            'productVariantsForms' => $productVariantsForms,
        	'isWarehouse' => true,
        ]);
    ?>

    <?php echo 
        Yii::$app->controller->renderPartial('inc/_aliases', [
            'form' => $form,
            'productAliasesForms' => $productAliasesForms,
        ]);
    ?>

    <?php echo 
        Yii::$app->controller->renderPartial('/inc/files', [
            	'form' => $form,
            	'photosForms' => $photosForms,
        		'documentsForms' => $documentsForms,
        		'arePhotosAvailable' => true,
        		'areAttachmentsAvailable' => true,
        ]);
    ?>

    <div class="col-sm-12 margint-16">
        <?php echo Html::a(Yii::t('web', 'Cancel'), Url::to(['product/list']), ['class' => 'btn btn-default']) ?>
        <button class="btn btn-primary pull-right"><?php echo Yii::t('web', 'Add') ?></button>
    </div>

    <?php ActiveForm::end() ?>
</div>

<?php $this->registerJs('
    (function() {
        var dynamicForm = new DynamicForm("products-form");
        dynamicForm.activateRemoveButtons();
        dynamicForm.activateAddItemButtons();
        dynamicForm.activatePhotoUpload();
		dynamicForm.activateDocumentUpload();
    })();
    ', View::POS_END); 
?>
