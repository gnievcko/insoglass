<?php
use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\View;

$this->title = Yii::t('web', 'List of product categories') . ' - ' . Yii::$app->name;

$this->params['breadcrumbs'][] = ['label' => Yii::t('main', 'Warehouse'), 'url' => Url::to(['warehouse/index'])];
$this->params['breadcrumbs'][] = Yii::t('web', 'List of product categories');

$this->registerJsFile('@web/js/dynamic-table.js?'.uniqid(), []);
?>

<div id="product-category-list-wrapper">    
    <div class="row">
		<div class="col-sm-8 col-xs-12"><h2 class="page-title"><?= Yii::t('web', 'List of product categories') ?></h2></div>
		<div class="col-sm-4 col-xs-12 text-right"><?= Html::a(Yii::t('web', 'Add product category'), ['product-category/create'], ['class' => 'btn btn-primary']) ?></div>
	</div>
	<h5><?php echo Yii::t('main','Filters')?></h5>
    <div id="product-category-list-table" class="dynamic-table">
        <div class="row dynamic-table-filters">
            <div class="col-md-3 col-sm-4">
            	<label><?php echo Yii::t('web','Search by name')?></label>
                    <input 
                        value="<?= $productCategoryListForm->productCategorySymbol ?>" 
                        class="form-control dynamic-table-filter" 
                        type="text"
                        placeHolder="<?= $productCategoryListForm->getAttributeLabel('Name') ?>"
                        name="<?= Html::getInputName($productCategoryListForm, 'productCategorySymbol') ?>">
            </div> 
            <?= Yii::$app->controller->renderPartial('/inc/listFiltersButtons', ['isMoreFilters' => false, 'isClearFilters' => true]) ?>
        </div>
        <div class="dynamic-table-data">
            <?= Yii::$app->controller->renderPartial('_productCategories', [
	                'productCategories' => $productCategories,
	                'pages' => $pages,
	                'productCategoryListForm' => $productCategoryListForm,
            ]) ?>
        </div>
    </div>
    <?php
     $this->registerJs('dynamicTable("product-category-list-table", "'.Url::to(['product-category/list']).'");', View::POS_END); 
     ?>
</div>
