<?php
use yii\helpers\Url;
use yii\helpers\Html;
use common\components\ActiveForm;
use yii\helpers\ArrayHelper;
use common\models\ar\ProductCategory;

$this->registerCssFile('@web/css/select2.min.css');
$this->registerCssFile('@web/css/select2-bootstrap.min.css');
$this->registerJsFile('@web/js/select2/select2.min.js?'.uniqid(), ['depends' => [\yii\web\JqueryAsset::className()]]);
$this->registerJsFile('@web/js/select2/i18n/'.Yii::$app->language.'.js?'.uniqid(), ['depends' => [\yii\web\JqueryAsset::className()]]);

$subtitle = $model->isEditScenarioSet() ? Yii::t('web', 'Update product category') : Yii::t('web', 'Add product category');
$this->title = $subtitle.' - '.Yii::$app->name;

$this->params['breadcrumbs'][] = ['label' => Yii::t('main', 'Warehouse'), 'url' => Url::to(['warehouse/index'])];
$this->params['breadcrumbs'][] = ['label' => Yii::t('web', 'List of product categories'), 'url' => Url::to(['product-category/list'])];
$this->params['breadcrumbs'][] = $subtitle;
?>

<div class="product-category-form">
	<h2 class="page-title"><?= $subtitle ?></h2>
	
	<?php $form = ActiveForm::begin([
			'id' => 'client-form-ajax',
			'enableAjaxValidation' => true,
	]); ?>
	
	<?php if($model->isEditScenarioSet()) {
		$form->field($model, 'id')->hiddenInput()->label(false);
	} ?>
				
	<div class="col-xs-12">
		<div class="row">
			<div class="col-md-6">
				<?= $form->field($model, 'name')->textarea(['class' => 'form-control smallTextarea', 'maxlength' => true, 'placeHolder' => $model->getAttributeLabel('name')]) ?>
			</div>
			<div class="col-md-6">
			
			<?php
				$query = ProductCategory::find()->select(['product_category.id as id', 'pct.name as symbol'])
					->orderBy('symbol')
					->joinWith(['productCategoryTranslations pct' => function($q) {
						$q->joinWith('language');
						$q->andWhere('language.symbol = :languageSymbol', [':languageSymbol' => Yii::$app->language]);
					}]);
				if($model->isEditScenarioSet()) {
					$query->andWhere('product_category.id != :id', [':id' => $model->id]);
				}
			?>
			<?= $form->field($model, 'parentCategoryId', ['enableAjaxValidation' => true])->dropDownList(
						ArrayHelper::map($query->all(), 'id', 'symbol'), ['prompt' => Yii::t('web', 'Parent category select')]);
			?>
	  		</div>
			 <div class="col-xs-12">
			 	<?= $form->field($model, 'description')->textArea(['maxlength' => true, 'placeHolder' => $model->getAttributeLabel('description')]) ?>
			 </div>
		</div>
	</div>
	
	<div class="col-xs-12">
		<div class="row">
			<div class="pull-left">
				<?= Html::a(Yii::t('web', 'Cancel'), 
	        		!empty(Yii::$app->request->getReferrer()) ? Yii::$app->request->getReferrer() : Url::to(['product-category/list']), 
	        		['class' => 'btn btn-default']) ?>
	        </div>
			<div class="pull-right"><?= Html::submitButton(($model->isEditScenarioSet() ? Yii::t('main', 'Save') : Yii::t('web', 'Add')), ['class' => 'btn btn-primary']); ?></div>
		</div>
	</div>
	
	<?php ActiveForm::end(); ?>	
</div>
