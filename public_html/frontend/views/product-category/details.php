<?php
use yii\helpers\Html;
use yii\helpers\Url;
use frontend\helpers\UserHelper;
use dosamigos\gallery\Gallery;
use common\bundles\EnhancedDialogAsset;

$this->title = Yii::t('web', 'Product details') . ' - ' . Yii::$app->name;

$this->params['breadcrumbs'][] = ['label' => Yii::t('main', 'Warehouse'), 'url' => Url::to(['warehouse/index'])];
$this->params['breadcrumbs'][] = ['label' => Yii::t('web', 'List of product categories'), 'url' => Url::to(['product-category/list'])];
$this->params['breadcrumbs'][] = Yii::t('web', 'Product category details');
?>

<div class="product-category--details">
	<div class="row">
		<div class="col-md-4 col-sm-6"><h2 class="page-title"><?= Yii::t('web', 'Product category details') ?></h2></div>
		<div class="col-md-8 col-sm-6 text-right">
			<?php
				$buttons = '';
				$referrer = Yii::$app->request->getReferrer();
				if (substr($referrer, strripos($referrer, '/') + 1) == 'create') {
					$buttons .= Html::a(Yii::t('web', 'Add another product category'),
										['product-category/create'], ['class' => 'btn btn-primary']);
				}
				$buttons .= Html::a(Yii::t('main', 'Update'), 
						['product-category/edit', 'id' => $data['id']], ['class' => 'btn btn-default']);
				$buttons .= Html::a(Yii::t('main', 'Delete'), 
						['product-category/delete'], 
						[
								'class' => 'btn btn-default',
								'data' => [
						                'confirm' => Yii::t('main', 'Are you sure you want to delete this item?'),
						                'method' => 'post',
										'params' => [
												'id' => $data['id'],
										],
					            ],		
						]);
				echo $buttons; 
			?>
		</div>
	</div>
<div class="col-xs-12 title-with-icon">
	<div class="base-icon basic-data-icon main-content-icons"></div>
	<h3 class="page-subtitle"><?= Yii::t('web', 'General information') ?></h3>
</div>
<div class="col-xs-12">
	<div class="table-responsive">
		<table class="table table-bordered detail-table">
	    	<tbody>
	    		<tr>
	    			<th><?= Yii::t('web', 'Name') ?></th>
	    			<td><?= Html::encode($data['name']) ?></td>
	    		</tr>
	    		<tr>
	    			<th><?= Yii::t('web', 'Parent category') ?></th>
	    			<td><?= Html::encode($data['parentCategoryName']) ?></td>
	    		</tr>
	    	</tbody>
		</table>
	</div>
</div>
<div class="col-xs-12 title-with-icon">
	<div class="base-icon description-icon main-content-icons"></div>
	<h3 class="page-subtitle"><?= Yii::t('main', 'Description') ?></h3>
</div>
	<!-- erased class "description" from the div below -->	
<div class="col-xs-12">
	<div class="row">
			<?php if (!empty($data['description'])) { ?>
			<div class="col-sm-8 desc">
				<?php echo Html::encode($data['description']); ?>
			</div>
			<?php }	else ?>
			<div class="col-sm-8 desc no-desc">
				<?php echo Yii::t('web', 'No data'); ?>
			</div>	
	</div>
</div>
	
	<?php if(!empty($photos)) { ?>
	<div class="col-xs-12">
		<div class="gallery">
			<h3 class="page-subtitle"><?= Yii::t('web', 'Photo gallery') ?></h3>
			<?= Gallery::widget(['items' => $photos]); ?>
		</div>
	</div>
	<?php } ?>
</div>
<?php 
	EnhancedDialogAsset::register($this);
?>