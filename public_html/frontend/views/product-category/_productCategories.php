<?php
	use yii\helpers\Url;
	use yii\helpers\Html;
	use frontend\models\ProductCategoryListForm;
	use yii\widgets\LinkPager;
	use common\bundles\EnhancedDialogAsset;
	use common\helpers\Utility;
	use common\widgets\TableHeaderWidget;
?>

<?php
if (empty($productCategories)) { ?>
	<label><?= Yii::t('web', 'No search results');?></label>
	<br><br>
<?php }
else { ?>
	<div class=" table-responsive">
		<table class="<?= TableHeaderWidget::TABLECLASS ?>">
	    	<?php echo TableHeaderWidget::widget(['fields' => ProductCategoryListForm::getSortFields() , 'listForm' => $productCategoryListForm]);?>
	        <tbody>
	            <?php foreach($productCategories as $productCategory): ?>
	                <tr>
	                    <td><?= Html::encode($productCategory['name']) ?></td>
	                    <td class="text-left"><?php
	                    		if (strlen($productCategory['description']) > Utility::MAX_DESCRIPTION_LENGTH) {
	                    			echo Html::encode(mb_substr($productCategory['description'], 0, Utility::MAX_DESCRIPTION_LENGTH) . '...');
	                    		}
	                    		else {
	                    			echo Html::encode($productCategory['description']);
	                    		}
	                    ?></td>
	                    <td><?= Html::encode($productCategory['parentCategoryName']) ?></td>
	                    <td><?php
	                    	echo Html::a('', Url::to(['product-category/details', 'id' => $productCategory['id']]), ['class' => 'base-icon details-icon action-icon', 'title' => Yii::t('web', 'Details')]);
	                    	echo '&nbsp;'.Html::a('', Url::to(['product-category/edit', 'id' => $productCategory['id']]), ['class' => 'base-icon editing-icon action-icon', 'title' => Yii::t('web', 'Edit')]);
	                    	echo '&nbsp;'.Html::a('', Url::to(['product-category/delete']), [
									'class' => 'base-icon delete-icon action-icon',
                                    'title' => Yii::t('main', 'Delete'),
									'data' => [
							                'confirm' => Yii::t('main', 'Are you sure you want to delete this item?'),
							                'method' => 'post',
											'params' => [
													'id' => $productCategory['id'],
											],
						            ],
							]);
	                    ?>
	                    </td>
	                </tr>
	            <?php endforeach ?>
	        </tbody>
	    </table>
	</div>
	<div class="col-sm-12">
		<div class="col-sm-4 text-left dynamic-table-counter">
			<?= Yii::t('web', 'Number of found items: {n}', ['n' => $pages->totalCount]) ?>
		</div>
		<div class="col-sm-8 text-right dynamic-table-pagination">
		    <?= LinkPager::widget(['pagination' => $pages ]) ?>
		</div>
	</div>
<?php }?>

<input class="dynamic-table-sort-dir" type="hidden" name="<?= Html::getInputName($productCategoryListForm, 'sortDir') ?>" value="<?= $productCategoryListForm->sortDir ?>" />
<input class="dynamic-table-sort-field" type="hidden" name="<?= Html::getInputName($productCategoryListForm, 'sortField') ?>" value="<?= $productCategoryListForm->sortField ?>" />
<input class="dynamic-table-page" type="hidden" name="<?= Html::getInputName($productCategoryListForm, 'page') ?>" value="<?= $productCategoryListForm->page ?>" />

<?php
	EnhancedDialogAsset::register($this);
?>
