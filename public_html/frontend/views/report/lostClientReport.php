<?php

use kartik\datecontrol\DateControl;
use kartik\datecontrol\DateControlAsset;
use yii\bootstrap\ActiveForm;
use yii\helpers\Html;
use yii\web\JqueryAsset;

$this->title = Yii::t('web', 'Report lost client') . ' - ' . Yii::$app->name;
$this->registerJsFile('@web/js/pages/lost-client-index.js?'.uniqid(), ['depends' => [JqueryAsset::className(), DateControlAsset::className()]]);
?>
<script>
    window.alertMessage = "<?= Yii::t('web', 'Input valid dates.') ?>";
</script>
<div class="report-index">
    <div class="row">
        <div class="col-sm-12">
            <h2><?= Yii::t('web', 'Lost Clients') ?></h2>
        </div>
        <?php
        $form = ActiveForm::begin([
                    'id' => 'lost-client-form',
                    'enableAjaxValidation' => true,
                    'enableClientValidation' => false,
                    'options' => [
                        'class' => 'form',
                    ]
        ]);
        ?>
        <div class="col-sm-12">
            <h4><?= Yii::t('web', 'Source period') ?></h4>
            <div class="col-sm-2">
                <?=
                $form->field($model, 'dateFrom')->widget(DateControl::classname(), [
                    'language' => \Yii::$app->language,
                    'type' => DateControl::FORMAT_DATE,
                    'ajaxConversion' => false,
                    'displayFormat' => 'php:d.m.Y',
                    'saveFormat' => 'php:d.m.Y',
                    'options' => [
                        'id' => 'dateFromId',
                        'pluginOptions' => [
                            'autoclose' => true
                        ],
                    ]
                ])->label(Yii::t('documents_orders', 'From'));
                ?>
            </div>
            <div class="col-sm-2">
                <?=
                $form->field($model, 'dateTo')->widget(DateControl::classname(), [
                    'language' => \Yii::$app->language,
                    'type' => DateControl::FORMAT_DATE,
                    'ajaxConversion' => false,
                    'displayFormat' => 'php:d.m.Y',
                    'saveFormat' => 'php:d.m.Y',
                    'options' => [
                        'id' => 'dateToId',
                        'pluginOptions' => [
                            'autoclose' => true
                        ]
                    ]
                ])->label(Yii::t('documents_orders', 'To'));
                ?>
            </div>
        </div>

        <div class="col-sm-12">
            <h4><?= Yii::t('web', 'Comparative period') ?></h4>
            <div class="col-sm-2">
                <?=
                $form->field($model, 'compareDateFrom')->widget(DateControl::classname(), [
                    'language' => \Yii::$app->language,
                    'type' => DateControl::FORMAT_DATE,
                    'ajaxConversion' => false,
                    'displayFormat' => 'php:d.m.Y',
                    'saveFormat' => 'php:d.m.Y',
                    'options' => [
                        'id' => 'compareDateFromId',
                        'pluginOptions' => [
                            'autoclose' => true
                        ]
                    ]
                ])->label(Yii::t('documents_orders', 'From'));
                ?>
            </div>
            <div class="col-sm-2">
                <?=
                $form->field($model, 'compareDateTo')->widget(DateControl::classname(), [
                    'language' => \Yii::$app->language,
                    'type' => DateControl::FORMAT_DATE,
                    'ajaxConversion' => false,
                    'displayFormat' => 'php:d.m.Y',
                    'saveFormat' => 'php:d.m.Y',
                    'options' => [
                        'id' => 'compareDateToId',
                        'pluginOptions' => [
                            'autoclose' => true
                        ]
                    ]
                ])->label(Yii::t('documents_orders', 'To'));
                ?>
            </div>
        </div>
        <div class="col-sm-12 document-icon">
            <div class="pull-left"><?= Html::submitButton(Yii::t('main', 'Generate'), ['class' => 'btn btn-primary']); ?></div>
        </div>
        <?php
        ActiveForm::end();
        ?>
    </div>
</div>
