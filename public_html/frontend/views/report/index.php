<?php

use kartik\datecontrol\DateControl;
use kartik\datecontrol\DateControlAsset;
use yii\bootstrap\ActiveForm;
use yii\web\JqueryAsset;
use yii\web\View;

$this->title = Yii::t('web', 'Reports') . ' - ' . Yii::$app->name;
$this->registerJsFile('@web/js/pages/report-index.js?'.uniqid(), ['depends' => [JqueryAsset::className(), DateControlAsset::className()]]);
?>
<script>
    window.alertMessage = "<?= Yii::t('web', 'Input valid dates.') ?>";
</script>

<!-- test ikonek -->
<div class="base-icon to-do-list-icon"></div>

<div class="report-index">
    <div class="row">
        <div class="col-sm-12">
            <h2><?= Yii::t('web', 'Report list') ?></h2>
        </div>
        <?php
        $form = ActiveForm::begin([
                    'id' => 'report-form',
                    'enableAjaxValidation' => true,
                    'enableClientValidation' => false,
                    'options' => [
                        'class' => 'form',
                    ]
        ]);
        ?>

        <div class="col-sm-2">
            <?= $form->field($model, 'type')->dropDownList($options, ['class' => 'form-control dynamic-table-filter'])->label(false); ?>
        </div>

        <div class="col-sm-2">
            <?=
            $form->field($model, 'dateFrom')->widget(DateControl::classname(), [
                'language' => \Yii::$app->language,
                'type' => DateControl::FORMAT_DATE,
                'ajaxConversion' => false,
                'displayFormat' => 'php:d.m.Y',
                'saveFormat' => 'php:d.m.Y',
                'options' => [
                    'id' => 'dateFromId',
                    'pluginOptions' => [
                        'autoclose' => true
                    ],
                ]
            ])->label(false);
            ?>
        </div>
        <div class="col-sm-2">
            <?=
            $form->field($model, 'dateTo')->widget(DateControl::classname(), [
                'language' => \Yii::$app->language,
                'type' => DateControl::FORMAT_DATE,
                'ajaxConversion' => false,
                'displayFormat' => 'php:d.m.Y',
                'saveFormat' => 'php:d.m.Y',
                'options' => [
                    'id' => 'dateToId',
                    'pluginOptions' => [
                        'autoclose' => true
                    ]
                ]
            ])->label(false);
            ?>
        </div>
        <div class="col-sm-12">
            <?php
            foreach($reports as $report) {
                if($report['actionList'] == 'show') {
                    echo '
				<div class="col-sm-12 well">
							<div class="document-icon">
								<a href="' . $report['url']['pdf'] . '" class="open-document">
									<div class="pdf"></div>
									<div class="report-description">
										<h4><span class="name">' . $report['name'] . '</span></h4>
										<span class="text">' . $report['text'] . '</span>
									</div>
								</a>
							</div>
				</div>';
                }
            }
            ?>
        </div>
<?php
ActiveForm::end();
?>
    </div>
</div>