<?php

use kartik\datecontrol\DateControl;
use yii\bootstrap\ActiveForm;
use yii\helpers\Html;
use common\helpers\StringHelper;
use dosamigos\multiselect\MultiSelect;

$this->title = Yii::t('web', 'Order register') . ' - ' . Yii::$app->name;
?>

<div class="report-order-register">
    <div class="row">
        <div class="col-sm-12">
            <h2><?= Yii::t('web', 'Order register') ?></h2>
        </div>
        <?php
        $form = ActiveForm::begin([
                    'id' => 'order-register-form',
                    'enableAjaxValidation' => true,
                    'enableClientValidation' => false,
                    'options' => [
                        'class' => 'form',
                    ]
        ]);
        ?>
        
        <div class="col-sm-12">
            <h4><?= Yii::t('web', 'Source period') ?></h4>
            <div class="col-sm-2">
                <?=
                $form->field($model, 'dateFrom')->widget(DateControl::classname(), [
                    'language' => \Yii::$app->language,
                    'type' => DateControl::FORMAT_DATE,
                    'ajaxConversion' => false,
                    'displayFormat' => 'php:d.m.Y',
                    'saveFormat' => 'php:d.m.Y',
                    'options' => [
                        'id' => 'dateFromId',
                        'pluginOptions' => [
                            'autoclose' => true
                        ],
                    ]
                ])->label(Yii::t('documents_orders', 'From'));
                ?>
            </div>
            <div class="col-sm-2">
                <?=
                $form->field($model, 'dateTo')->widget(DateControl::classname(), [
                    'language' => \Yii::$app->language,
                    'type' => DateControl::FORMAT_DATE,
                    'ajaxConversion' => false,
                    'displayFormat' => 'php:d.m.Y',
                    'saveFormat' => 'php:d.m.Y',
                    'options' => [
                        'id' => 'dateToId',
                        'pluginOptions' => [
                            'autoclose' => true
                        ]
                    ]
                ])->label(Yii::t('documents_orders', 'To'));
                ?>
            </div>
        </div>
        <?php if(!empty($model->getOrderTypeOptions()) && Yii::$app->params['isOrderTypeVisible']) { ?>
	        <div class="cat-select col-sm-12">
	        	<div class="col-sm-12">
	        		<h4><?= StringHelper::translateOrderToOffer('web', 'Included order types') ?></h4>
	        	</div>
	        	<div class="col-sm-4">
		        	<?= $form->field($model, 'orderTypeIds')->widget(MultiSelect::className(), [
							'options' => ['multiple' => 'multiple', 'class' => 'dynamic-table-filter'],
		        			'data' => $model->getOrderTypeOptions(),
		        			//'name' => Html::getInputName($model, 'orderTypeIds'),
		        			'value' => empty($model->orderTypeIds) ? array_keys($model->getOrderTypeOptions()) : $model->orderTypeIds,
		        			'clientOptions' => [
		        					'nonSelectedText' => '',
		        					'nSelectedText' => mb_strtolower(Yii::t('main', 'Selected'), 'UTF-8'),
		        					'numberDisplayed' => 2,
		        					'buttonWidth' => '100%',
		        			],
		        	]); ?>
	        	</div>
	        </div>
        <?php } ?>

        <div class="col-sm-12 document-icon">
            <div class="pull-left"><?= Html::submitButton(Yii::t('main', 'Generate'), ['class' => 'btn btn-primary']); ?></div>
        </div>
        <?php
        ActiveForm::end();
        ?>
    </div>
</div>
