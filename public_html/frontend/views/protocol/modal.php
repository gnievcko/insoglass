<?php
use yii\bootstrap\Html;
?>
<div id="document-modal" class="modal fade" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">
                    <?= Yii::t('web', 'Old documents') ?>
                </h4>
            </div>
            <div class="modal-body">
                <table>
                    <table class="table">
                        <thead>
                        <th>
                            <?= common\helpers\StringHelper::translateOrderToOffer('web', 'Order name'); ?>
                        </th>
                        <th>
                            <?= Yii::t('web', 'Date'); ?>
                        </th>
                        <th>
                            <?= Yii::t('web', 'Author'); ?>
                        </th>
                        <th>
                            <?= Yii::t('web', 'Action'); ?>
                        </th>
                        </thead>
                        <tbody>
                            <?php
                            foreach($documents as $document) {
                                
                                foreach($document['history'] as $history) {
                                    $idD = $document['_id'];
                                    $idH = $history['_id'];
                                    $orderId = 0;
                                    foreach($history['sections'] as $section) {
                                        if(isset($section['orderId'])) {
                                            $orderId = $section['orderId'];
                                            break;
                                        }
                                    }
                                    
                                    $order = common\models\ar\Order::find()->where(['id' => $orderId])->one();
                                    
                                    $date = date('d.m.Y', $history['createdAt']);
                                    $authorId = isset($history['authorId']) ? $history['authorId'] : null;
                                    $author = !empty($authorId) ? User::find()->where(['id' => $authorId])->one() : null;
                                    echo '<tr>'
                                    .'<td>'.(!empty($order) ? $order->title : Yii::t('web', 'No name')).'</td>'
                                    .'<td>'.$date.'</td>'
                                    .'<td>'.(Html::a(Html::img('@web/images/placeholders/document_icon.png').' '.Yii::t('web', 'Import'), [
                                        \common\documents\DocumentTypeUtility::URL[$document['type']],
                                        'documentId' => $idD->__toString(),
                                        'historyId' => $idH->__toString(),
                                        'orderId' => Yii::$app->request->get('orderId')]
                                    )).'</td>'
                                    .'</tr>';
                                }
                            }
                            ?>
                        </tbody>
                    </table>
                </table>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->