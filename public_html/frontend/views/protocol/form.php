<?php if(!empty($documents)): ?>
    <div>
        <div class="btn btn-primary import">
            <?= Yii::t('web', 'Import') ?>
        </div>
    </div><br/>
    <?php endIf; ?>
<?php
$this->registerJsFile('@web/js/documents/import.js?'.uniqid(), ['depends' => [\yii\web\JqueryAsset::className()]]);
echo $documentView;
if(!empty($documents)) {
    echo Yii::$app->controller->renderFile('@common/documents/html/views/modal.php', ['documents' => $documents, 'orderId' => $orderId]);
}
?>
