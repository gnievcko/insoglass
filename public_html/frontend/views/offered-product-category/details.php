<?php
use yii\helpers\Html;
use yii\helpers\Url;
use frontend\helpers\UserHelper;
use dosamigos\gallery\Gallery;
use common\bundles\EnhancedDialogAsset;
use common\helpers\StringHelper;
$this->title = Yii::t('web', 'Product details') . ' - ' . Yii::$app->name;

$this->params['breadcrumbs'][] = ['label' => StringHelper::translateOrderToOffer('main', 'Orders'), 'url' => Url::to(['order/index'])];
$this->params['breadcrumbs'][] = ['label' => Yii::t('web', 'List of product categories'), 'url' => Url::to(['offered-product-category/list'])];
$this->params['breadcrumbs'][] = Yii::t('web', 'Offered product category details');
?>

<div class="product-category--details">
	<div class="row">
		<div class="col-sm-6"><h2 class="page-title"><?= Yii::t('web', 'Offered product category details') ?></h2></div>
		<div class="col-sm-6 text-right">
		<?php if(Yii::$app->params['canCudOfferedProducts']) { ?>
			<?php
				$buttons = '';
				$referrer = Yii::$app->request->getReferrer();
				if (substr($referrer, strripos($referrer, '/') + 1) == 'create') {
					$buttons .= Html::a(Yii::t('web', 'Add another product category'),
										['offered-product-category/create'], ['class' => 'btn btn-primary']);
				}
				$buttons .= Html::a(Yii::t('main', 'Update'),
						['offered-product-category/edit', 'id' => $data['id']], ['class' => 'btn btn-default']);
				$buttons .= Html::a(Yii::t('main', 'Delete'),
						['offered-product-category/delete'],
						[
								'class' => 'btn btn-default',
								'data' => [
						                'confirm' => Yii::t('main', 'Are you sure you want to delete this item?'),
						                'method' => 'post',
										'params' => [
												'id' => $data['id'],
										],
					            ],
						]);
				echo $buttons;
			?>
		<?php }?>
		</div>
	</div>

	<div class="table-responsive col-sm-12">
		<h3 class="page-subtitle"><?= Yii::t('web', 'General information') ?></h3>
		<table class="table table-bordered detail-table">
	    	<tbody>
	    		<tr>
	    			<th><?= Yii::t('web', 'Name') ?></th>
	    			<td><?= Html::encode($data['name']) ?></td>
	    		</tr>
	    		<tr>
	    			<th><?= Yii::t('web', 'Parent category') ?></th>
	    			<td><?= Html::encode($data['parentCategoryName']) ?></td>
	    		</tr>
                <tr class="<?= Yii::$app->params['isProductionVisible']?:'hidden'?>">
                    <th><?= Yii::t('web', 'Effort factor') ?></th>
                    <td><?= Html::encode($data['effortFactor']) ?></td>
                </tr>
	    	</tbody>
		</table>
	</div>

	<div class="description col-sm-12">
		<h3 class="page-subtitle"><?= Yii::t('main', 'Description') ?></h3>
		<?php 
		if(!empty($data['description'])) {
			echo Html::encode($data['description']);
		}
		else echo Yii::t('web', 'No description');
		?>
	</div>

	<?php if(!empty($photos)) { ?>
		<div class="gallery">
			<h3 class="page-subtitle"><?= Yii::t('web', 'Photo gallery') ?></h3>
			<?= Gallery::widget(['items' => $photos]); ?>
		</div>
	<?php } ?>
</div>
<?php
	EnhancedDialogAsset::register($this);
?>
