<?php
use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\View;
use common\helpers\StringHelper;

$this->title = Yii::t('web', 'List of offered product categories') . ' - ' . Yii::$app->name;

$this->params['breadcrumbs'][] = ['label' => StringHelper::translateOrderToOffer('main', 'Orders'), 'url' => Url::to(['order/index'])];
$this->params['breadcrumbs'][] = Yii::t('web', 'List of offered product categories');

$this->registerJsFile('@web/js/dynamic-table.js?'.uniqid(), []);
?>

<div id="offered-product-category-list-wrapper">
    <div class="row">
		<div class="col-sm-6 col-xs-9"><h2 class="page-title"><?= Yii::t('web', 'List of offered product categories') ?></h2></div>
		<?php if(Yii::$app->params['canCudOfferedProducts']) { ?>
			<div class="col-sm-6 col-xs-3"><?= Html::a(Yii::t('web', 'Add offered product category'), ['offered-product-category/create'], ['class' => 'btn btn-primary pull-right']) ?>
		<?php }?>
		</div>
	</div>

    <div id="offered-product-category-list-table" class="dynamic-table">
    	<h5><?php echo Yii::t('main','Filters')?></h5>
        <div class="row dynamic-table-filters">
            <div class="col-md-3 col-sm-4">
            	<label><?php echo Yii::t('web','Search by name')?></label>
                    <input
                        value="<?= $offeredProductCategoryListForm->offeredProductCategorySymbol ?>"
                        class="form-control dynamic-table-filter"
                        type="text"
                        placeHolder="<?= $offeredProductCategoryListForm->getAttributeLabel('Name') ?>"
                        name="<?= Html::getInputName($offeredProductCategoryListForm, 'offeredProductCategorySymbol') ?>">

            </div>
            <?= Yii::$app->controller->renderPartial('/inc/listFiltersButtons', ['isMoreFilters' => false, 'isClearFilters' => true]) ?>
        </div>
        <div class="dynamic-table-data">
            <?= Yii::$app->controller->renderPartial('_offeredProductCategories', [
	                'offeredProductCategories' => $offeredProductCategories,
	                'pages' => $pages,
	                'offeredProductCategoryListForm' => $offeredProductCategoryListForm,
            ]) ?>
        </div>
        
    </div>
    <?php
     $this->registerJs('dynamicTable("offered-product-category-list-table", "'.Url::to(['offered-product-category/list']).'");', View::POS_END);
     ?>
</div>
