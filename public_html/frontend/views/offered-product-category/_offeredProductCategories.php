<?php
use yii\helpers\Url;
use yii\helpers\Html;
use frontend\models\OfferedProductCategoryListForm;
use yii\widgets\LinkPager;
use common\bundles\EnhancedDialogAsset;
use common\widgets\TableHeaderWidget;
use common\helpers\Utility;
?>

<?php
if (empty($offeredProductCategories)) { ?>
	<label><?= Yii::t('web', 'No search results');?></label>
	<br><br>
<?php }
else { ?>
<div class=" table-responsive">
	<table class="<?= TableHeaderWidget::TABLECLASS ?>">
	    <?php echo TableHeaderWidget::widget(['fields' => OfferedProductCategoryListForm::getSortFields() , 'listForm' => $offeredProductCategoryListForm]);?>
        <tbody>
            <?php foreach($offeredProductCategories as $offeredProductCategory): ?>
                <tr>
                    <td><?= Html::encode($offeredProductCategory['name']) ?></td>
					<td class="text-left"><?php
							if (strlen($offeredProductCategory['description']) > Utility::MAX_DESCRIPTION_LENGTH) {
								echo Html::encode(mb_substr($offeredProductCategory['description'], 0, Utility::MAX_DESCRIPTION_LENGTH) . '...');
							}
							else {
								echo Html::encode($offeredProductCategory['description']);
							}
					?></td>
					<td><?= Html::encode($offeredProductCategory['parentCategoryName']) ?></td>
                    <td><?php
						echo Html::a('', Url::to(['offered-product-category/details', 'id' => $offeredProductCategory['id']]), ['class' => 'base-icon details-icon action-icon', 'title' => Yii::t('web', 'Details')]);
                    	
						if(Yii::$app->params['canCudOfferedProducts']) {
							echo '&nbsp;'.Html::a('', Url::to(['offered-product-category/edit', 'id' => $offeredProductCategory['id']]), ['class' => 'base-icon editing-icon action-icon', 'title' => Yii::t('web', 'Edit')]);
	                    	echo '&nbsp;'.Html::a('', Url::to(['offered-product-category/delete']), [
									'class' => 'base-icon delete-icon action-icon',
	                                'title' => Yii::t('main', 'Delete'),
									'data' => [
							                'confirm' => Yii::t('main', 'Are you sure you want to delete this item?'),
							                'method' => 'post',
											'params' => [
													'id' => $offeredProductCategory['id'],
											],
						            ],
							]);
						}
                    ?>
                </tr>
            <?php endforeach ?>
        </tbody>
    </table>
</div>
<div class="col-sm-12">
	<div class="col-sm-4 text-left dynamic-table-counter">
		<?= Yii::t('web', 'Number of found items: {n}', ['n' => $pages->totalCount]) ?>
	</div>
	<div class="col-sm-8 text-right dynamic-table-pagination">
	    <?= LinkPager::widget(['pagination' => $pages ]) ?>
	</div>
</div>
<?php }?>

<input class="dynamic-table-sort-dir" type="hidden" name="<?= Html::getInputName($offeredProductCategoryListForm, 'sortDir') ?>" value="<?= $offeredProductCategoryListForm->sortDir ?>" />
<input class="dynamic-table-sort-field" type="hidden" name="<?= Html::getInputName($offeredProductCategoryListForm, 'sortField') ?>" value="<?= $offeredProductCategoryListForm->sortField ?>" />
<input class="dynamic-table-page" type="hidden" name="<?= Html::getInputName($offeredProductCategoryListForm, 'page') ?>" value="<?= $offeredProductCategoryListForm->page ?>" />

<?php
	EnhancedDialogAsset::register($this);
?>
