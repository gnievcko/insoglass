<?php
use yii\helpers\Url;
use yii\helpers\Html;
use common\components\ActiveForm;
use yii\helpers\ArrayHelper;
use common\models\ar\OfferedProductCategory;
use common\helpers\StringHelper;

$this->registerCssFile('@web/css/select2.min.css');
$this->registerCssFile('@web/css/select2-bootstrap.min.css');
$this->registerJsFile('@web/js/select2/select2.min.js?'.uniqid(), ['depends' => [\yii\web\JqueryAsset::className()]]);
$this->registerJsFile('@web/js/select2/i18n/'.Yii::$app->language.'.js?'.uniqid(), ['depends' => [\yii\web\JqueryAsset::className()]]);

$subtitle = $model->isEditScenarioSet() ? Yii::t('web', 'Update offered product category') : Yii::t('web', 'Add offered product category');
$this->title = $subtitle.' - '.Yii::$app->name;

$this->params['breadcrumbs'][] = ['label' => StringHelper::translateOrderToOffer('main', 'Orders'), 'url' => Url::to(['order/index'])];
$this->params['breadcrumbs'][] = ['label' => Yii::t('web', 'List of offered product categories'), 'url' => Url::to(['offered-product-category/list'])];
$this->params['breadcrumbs'][] = $subtitle;
?>

<div class="offered-product-category-form">
	<h2 class="page-title"><?= $subtitle ?></h2>

	<?php $form = ActiveForm::begin([
			'id' => 'client-form-ajax',
			'enableAjaxValidation' => true,
	]); ?>

	<?php if($model->isEditScenarioSet()) {
		$form->field($model, 'id')->hiddenInput()->label(false);
	} ?>

	<div class="col-xs-12">
		<div class="row">
			<div class="col-md-6"><?= $form->field($model, 'name')->textInput(['maxlength' => true, 'placeholder' => Yii::t('web','Category name')]) ?></div>
			<div class="col-md-5 col-sm-offset-1">
	
			<?php
				$query = OfferedProductCategory::find()->select(['offered_product_category.id as id', 'opct.name as symbol'])
					->orderBy('symbol')
					->joinWith(['offeredProductCategoryTranslations opct' => function($q) {
						$q->joinWith('language');
						$q->andWhere('language.symbol = :languageSymbol', [':languageSymbol' => Yii::$app->language]);
					}]);
				if($model->isEditScenarioSet()) {
					$query->andWhere('offered_product_category.id != :id', [':id' => $model->id]);
				}
			?>
			<?= $form->field($model, 'parentCategoryId', ['enableAjaxValidation' => true])->dropDownList(
					ArrayHelper::map($query->all(), 'id', 'symbol'), ['prompt' => Yii::t('web', 'Parent category select')]);
			?>
	  		</div>
  		</div>
	</div>

    <div class="col-xs-12 <?= Yii::$app->params['isProductionVisible']?:'hidden'?>">
        <div class="row">
            <div class="col-md-6"><?= $form->field($model, 'effortFactor')->textInput(['type' => 'number']) ?></div>
        </div>
    </div>

    <div class="col-xs-12">
		<div class="row">
			<div class="col-sm-6"><?= $form->field($model, 'description')->textArea(['maxlength' => true, 'placeholder' => Yii::t('web','Description')]) ?></div>
		</div>
	</div>

	<div class="col-xs-12">
		<div class="row">
			<div class="pull-left"><?= Html::a(Yii::t('web', 'Cancel'), ['offered-product-category/list'], ['class' => 'btn btn-default']) ?></div>
			<div class="pull-right"><?= Html::submitButton(($model->isEditScenarioSet() ? Yii::t('main', 'Save') : Yii::t('web', 'Add')), ['class' => 'btn btn-primary']); ?></div>
		</div>
	</div>

	<?php ActiveForm::end(); ?>
</div>
