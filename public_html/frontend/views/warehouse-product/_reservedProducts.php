<?php 
use yii\helpers\Url;
use yii\helpers\Html;
use yii\widgets\LinkPager;
use common\bundles\EnhancedDialogAsset;
use frontend\models\ProductReservationListForm;
use yii\web\View;
use common\helpers\StringHelper;
use frontend\helpers\AdditionalFieldHelper;
use common\models\ar\TaskType;
use common\helpers\Utility;
use common\widgets\TableHeaderWidget;
?>

<?php 
if (empty($items)) { ?>
	<label><?= Yii::t('web', 'No search results');?></label>
	<br><br>					
<?php }
else { ?>
	<div class="table-responsive">
	    <table class="<?= TableHeaderWidget::TABLECLASS ?>">
	        <?php echo TableHeaderWidget::widget(['fields' => ProductReservationListForm::getSortFields(), 'listForm' => $listForm, 'iconCount' => 4]);?>
	        <tbody>
	            <?php 
	            	$taskTypeId = TaskType::find()->where(['symbol' => Utility::TASK_TYPE_ORDER_PRODUCT])->one()->id;	
	            
	            	foreach($items as $item): ?>
	                <tr>
	                    <td><?= Html::encode($item['title']) ?></td>
	                    <td><?= Html::encode($item['name']) ?></td>   
	                    <td><?= Html::encode($item['count']) ?></td>
	                    <td><?= Html::encode(StringHelper::getFormattedDateFromDate($item['date_creation'], true)) ?></td>
	                    <td><?= Html::encode(AdditionalFieldHelper::getFormattedData([['table' => 'user', 'name' => $item['user']]], true)) ?></td>
	                    
	                    <td><?php 
	                    	echo Html::a('', Url::to(['warehouse-product/details', 'productId' => $item['productId']]), ['class' => 'base-icon details-icon action-icon', 'title' => Yii::t('web', 'Details')])
	                    			.'&nbsp;'.Html::a('', Url::to(['task/create', 
			                    					'productId' => $item['productId'], 
			                    					'userNotifiedId' => $item['userConfirmingId'],
	                    							'taskTypeId' => $taskTypeId,
			                    			]), [
                                                'class' => 'base-icon reservation-icon action-icon', 
                                                'target' => '_blank',
                                                'title' => Yii::t('web', 'Create reservation task'),
                                                ])

	                    			.'&nbsp;'.Html::a('', ['warehouse-product/undo-reservation'], [
	                    							'class' => 'base-icon cancel-icon action-icon',
                                                    'title' => Yii::t('web', 'Undo reservation'),
	                    							'data' => [
	                    									'confirm' => Yii::t('web', 'Are you sure you want to undo this reservation?'),
	                    									'method' => 'post',
	                    									'params' => [
	                    											'operationId' => $item['id'],
	                    											'orderId' => $item['orderId'],
	                    									],
	                    							],
	                    					])
                    				.'&nbsp;'.Html::a('', ['warehouse-product/issue-reservation'], [
                    								'class' => 'base-icon issue-icon action-icon',
                                                    'title' => Yii::t('web', 'Issue product related to reservation'),
                    								'data' => [
                    										'confirm' => Yii::t('web', 'Are you sure you want to issue products related to this reservation?'),
                    										'method' => 'post',
                    										'params' => [
                    												'operationId' => $item['id'],
                    												'orderId' => $item['orderId'],
                    										],
                    								],
                    						]);
	                    ?></td>
	                </tr>
	            <?php endforeach ?>
	        </tbody>
	    </table>
	</div>
	<div class="col-sm-12">
		<div class="col-sm-4 text-left dynamic-table-counter">
			<?= Yii::t('web', 'Number of found items: {n}', ['n' => $pages->totalCount]) ?>
		</div>
		<div class="col-sm-8 text-right dynamic-table-pagination">
		    <?= LinkPager::widget(['pagination' => $pages ]) ?>
		</div>
	</div>
<?php }?>

<input class="dynamic-table-sort-dir" type="hidden" name="<?= Html::getInputName($listForm, 'sortDir') ?>" value="<?= $listForm->sortDir ?>" />
<input class="dynamic-table-sort-field" type="hidden" name="<?= Html::getInputName($listForm, 'sortField') ?>" value="<?= $listForm->sortField ?>" />
<input class="dynamic-table-page" type="hidden" name="<?= Html::getInputName($listForm, 'page') ?>" value="<?= $listForm->page ?>" />

<?php EnhancedDialogAsset::register($this); ?>
