<?php
use yii\helpers\Url;
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\web\View;
use yii\helpers\ArrayHelper;
use kartik\widgets\Select2;
use yii\web\JsExpression;
use common\models\aq\WarehouseQuery;
use frontend\helpers\AdditionalFieldHelper;

$this->registerCssFile('@web/css/select2.min.css');
$this->registerCssFile('@web/css/select2-bootstrap.min.css');
$this->registerJsFile('@web/js/select2/select2.min.js?'.uniqid(), ['depends' => [\yii\web\JqueryAsset::className()]]);
$this->registerJsFile('@web/js/select2/i18n/'.Yii::$app->language.'.js?'.uniqid(), ['depends' => [\yii\web\JqueryAsset::className()]]);

$subtitle = $model->isUpdateScenarioSet() ? Yii::t('web', 'Update products in warehouse') : Yii::t('web', 'Add products to warehouse');
$this->title = $subtitle.' - '.Yii::$app->name;

$this->params['breadcrumbs'][] = ['label' => Yii::t('main', 'Warehouse'), 'url' => Url::to(['warehouse/index'])];
$this->params['breadcrumbs'][] = ['label' => Yii::t('web', 'Current products in warehouse'), 'url' => Url::to(['warehouse-product/list'])];
$this->params['breadcrumbs'][] = $subtitle;
?>

<div class="warehouse-product-form">
		
	<div class="row">
		<div class="col-sm-8 col-xs-12"><h2 class="page-title"><?= $subtitle ?></h2></div>
		<div class="col-sm-4 col-xs-12 text-right"><?= Html::a(Yii::t('web', 'Add warehouse product'), ['product/create'], ['class' => 'btn btn-primary', 'target' => '_blank']) ?>
		</div>
	</div>

	<?php $form = ActiveForm::begin([
			'id' => 'warehouse-product-form',
			'enableAjaxValidation' => true,
	]); ?>
					
	<div class="row">
		<div class="col-sm-6"><?php
			echo $form->field($model, 'warehouseId', ['options' => ['style' => 'display: '.(Yii::$app->params['isMoreWarehousesAvailable'] ? 'block' : 'none')]])->dropDownList(ArrayHelper::map(WarehouseQuery::getWarehouseList()->all(), 'id', 'name'));
		?></div>
	</div>

    <div id="delivery-products" class="items-container">
    <?php 
        if(!empty($product)) {
            echo $this->renderAjax('inc/_item', [
                            'warehouseProductItemForm' => $product,
                            'prefix' => 0,
                            'formId' => 'warehouse-product-form',
                    ]); 
        }
    ?>
    </div>
    <div class="product-select col-sm-12">
    	<div class="row">
            <div class="col-sm-6">
            <h4><?= Yii::t('web', 'Add product') ?></h4>
            
                <?= Select2::widget([
                        'id' => 'select2-search-product',
                        'name' => 'select2-search-product',
                        'language' => Yii::$app->language,
                        'options' => ['placeholder' => Yii::t('web', 'Select a product...')],
                        'data' => null,
                        'pluginOptions' => [
                                'allowClear' => true,
                                'minimumInputLength' => 1,
                                'ajax' => [
                                        'url' => Url::to(['product/find']),
                                        'dataType' => 'json',
                                        'delay' => 250,
                                        'data' => new JsExpression('function(params) { return { term:params.term, page:params.page, size: 20 }; }'),
                                        'processResults' => new JsExpression('function (data, params) {
                                            params.page = params.page || 1;
    
                                            return {
                                                results: data.items,
                                                pagination: {
                                                    more: data.more
                                                }
                                            };
                                        }'),
                                        'cache' => true
                                ],
                                'templateResult' => new JsExpression('function(item) { if(item.loading) return item.text; return item.name; }'),
                                'templateSelection' => new JsExpression('function (item) { return item.name || item.text; }'),
                        ],
                        'pluginEvents' => [
                                'select2:selecting' => 'function(e) {
                                    var productId = e.params.args.data.id;
                                    var fetchProduct = function() {
                                        window.wpiCurrentPrefix = window.wpiCurrentPrefix || 1;
                                        
                                        $.ajax({
                                            method: "GET",
                                            url: "'.Url::to(['warehouse-product/get-item-form']).'",
                                            data: { 
                                                productId: productId,
                                                prefix: window.wpiCurrentPrefix++,
                                                formId: "warehouse-product-form"
                                            },									
                                            async: false
                                        }).done(function(data) {
                                            var $product = $(data);
                                            $("#delivery-products").append($product);
                                            $product.find(".remove-item-btn:first").on("click", function() {
                        						$product.remove();
                                            })
                                        });	
                                    }
                                    
                                    var selectedProductsIds = {};
                                    $("#delivery-products .product-id").each(function(idx, idField) {
                                        selectedProductsIds[idField.value] = idField.value
                                    });
    
                                    if(selectedProductsIds[productId] === undefined) {
                                        fetchProduct();
                                    }
                                }',	
                        ],
                    ]); 
                ?>
    
                <?php
                    $this->registerJs('
                        (function() {
                            var $addProductSelect = $("#select2-search-product");
                            $addProductSelect.on("change", function() {
                                $addProductSelect.val("");
                            });
                        })();
                ', View::POS_END); 
                ?>
                <a href="<?= Url::to(['product/create']); ?>" target="_blank" class="focusable-link  add-item-after-select2 add-item-after-select2-no-required">
                    <span class="glyphicon glyphicon-plus-sign pull-left"></span>
                    <span> <?=Yii::t('web', 'Create new product')?></span>
                </a>
            </div>
		</div>
    </div>
    

		<div class="col-sm-10">
			<h4><?= Yii::t('web', 'Comment') ?></h4>
			<?php 
				echo $form->field($model, 'description')->textarea(['maxlength' => true, 'placeHolder' => $model->getAttributeLabel('description'),])->label(false);
			?>
		</div>

	
	<?php if($model->isUpdateScenarioSet()) { ?>
	<div class="col-sm-12">
		<h4><?= Yii::t('web', 'Select operations') ?></h4>
			<div class="col-lg-8 col-lg-offset-2 col-xs-12">
				<?= $form->field($model, 'productStatusId')->hiddenInput()->label(false); ?>
				<div class="btn-group buttons-operation-types" role="group" aria-label="...">
					<?php foreach($operationTypes as $operationType) { ?>
						<button type="button" class="btn btn-primary button-operation-type <?= $model->productStatusId == $operationType['id'] ? 'buttonClick' : '' ?>" 
								data-id="<?= $operationType['id'] ?>"><?= $operationType['name'] ?></button>
					<?php } ?>
				</div>
			</div>
		</div>
		
		<div class="col-sm-12">
			<div class="row">
    			<div class="col-sm-6 col-sm-offset-3 col-xs-12" id="additional-fields-container">
    				<?php echo AdditionalFieldHelper::renderAdditionalFields($model, $form, $fields, 'productStatusId'); ?>
    			</div>
			</div>
		</div>
	<?php } ?>

	<div class="col-sm-12">
		<div class="pull-left"><?= Html::a(Yii::t('web', 'Cancel'), ['warehouse-product/list'], ['class' => 'btn btn-default']) ?></div>
		<div class="pull-right"><?= Html::submitButton(($model->isUpdateScenarioSet() ? Yii::t('web', 'Execute') : Yii::t('web', 'Add')), ['class' => 'btn btn-primary']); ?></div>
	</div>
	
	<?php ActiveForm::end(); ?>	
</div>

<?php 
	if($model->isUpdateScenarioSet()) {
		$this->registerJs('
					$("#warehouseproductform-productstatusid").on("change", function(e) {
						console.log("Wchodzi");
						var productStatusId = $("#warehouseproductform-productstatusid").val();
						var fields = '.json_encode($fields).';
				
						$.each(fields, function(i, v) {
							var componentSelect2 = $("#select2-" + v.productStatusId + "-" + v.id + "-" + v.symbol);
							componentSelect2.css("display", "none");
							var componentTextInput = $("#text-input-" + v.productStatusId + "-" + v.id + "-" + v.symbol);
							componentTextInput.css("display", "none");
				
						    if(v.productStatusId == productStatusId) {
						        componentSelect2.css("display", "block");
								componentTextInput.css("display", "block");
						    }
						});
					}).trigger("change");
				
					$(".button-operation-type").click(function(e) {
						console.log("Wchodzi2");
						var productStatusId = $(this).data("id");
						$("#warehouseproductform-productstatusid").val(productStatusId).trigger("change");
					});
				', View::POS_END);
	}
?>
