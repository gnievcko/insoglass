<?php
    use yii\helpers\Html;
    use yii\web\View;

    $formName = "warehouseproductitemform-{$prefix}";
?>

<div class="row item warehouse-delivery-product">
    <div class="col-sm-2 product-thumbnail-container">
    	<?php if(!empty($warehouseProductItemForm)) { 
        	echo Html::img($warehouseProductItemForm->urlThumbnail);
		} else {
			echo Html::img('@web/images/placeholders/product.png',['alt' => 'placeholder image']);
        }?>
    </div>

    <div class="col-sm-10">
        <div class="row">
            <div class="col-xs-12 product-name">
                <?= $warehouseProductItemForm->name ?>
            </div>
        </div>

        <div class="row">
            <div class="col-sm-11">
                <div id="container-<?= "{$formName}-count" ?>" class="col-sm-11 form-group <?= $warehouseProductItemForm->isAttributeRequired('count') ? 'required' : '' ?> ">
                    <?= Html::activeInput('number', $warehouseProductItemForm, "[{$prefix}]count", ['class' => 'form-control item-count', 'id' => "{$formName}-count", 'min' => 1, 'placeholder' => $warehouseProductItemForm->getAttributeLabel('count')]) ?>
                    <div class="help-block"></div>
                </div>
                <?php
                    $this->registerJs('
                        (function() {
                            $("#'.$formId.'").yiiActiveForm("add", {
                                id: "'.$formName.'-count'.'",
                                name: "'."[{$prefix}]count".'",
                                container: "#container-'.$formName.'-count'.'",
                                input: "#'.$formName.'-count'.'",
                                enableClientValidation: false,
                                enableAjaxValidation: true
                            });
                        })(); ', View::POS_END);
                ?>
                <div id="container-<?= "{$formName}-unit" ?>" class="col-sm-1 form-group <?= $warehouseProductItemForm->isAttributeRequired('unit') ? 'required' : '' ?> ">
                	<?php if(!empty(Yii::$app->params['defaultUnit'])): ?>
                	    <?= Html::activeTextInput($warehouseProductItemForm, "[{$prefix}]unit", ['class' => 'form-control', 'id' => "{$formName}-unit", 'value' => Yii::t('documents',Yii::$app->params['defaultUnit']),'readonly'=>true]) ?>
                	<?php else:?>
                    	<?= Html::activeTextInput($warehouseProductItemForm, "[{$prefix}]unit", ['class' => 'form-control', 'id' => "{$formName}-unit", 'placeholder' => $warehouseProductItemForm->getAttributeLabel('unit')]) ?>
                    <?php endif;?>
                    <div class="help-block"></div>
                </div>
                <?php
                    $this->registerJs('
                        (function() {
                            $("#'.$formId.'").yiiActiveForm("add", {
                                id: "'.$formName.'-unit'.'",
                                name: "'."[{$prefix}]unit".'",
                                container: "#container-'.$formName.'-unit'.'",
                                input: "#'.$formName.'-unit'.'",
                                enableClientValidation: false,
                                enableAjaxValidation: true
                            });
                        })(); ', View::POS_END);
                ?> 
            </div>
            <div class="col-sm-1">
                <div class="base-icon garbage-icon action-icon remove-item-btn" style="margin-top: 3px"></div>
                <?= Html::activeHiddenInput($warehouseProductItemForm, "[{$prefix}]productId", ['id' => "{$formName}-id", 'class' => 'product-id']) ?>
            </div>
        </div>
    </div>
</div>
