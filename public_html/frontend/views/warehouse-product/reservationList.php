<?php
use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\View;

$this->title = Yii::t('web', 'Current product reservations') . ' - ' . Yii::$app->name;

$this->params['breadcrumbs'][] = ['label' => Yii::t('main', 'Warehouse'), 'url' => Url::to(['warehouse/index'])];
$this->params['breadcrumbs'][] = Yii::t('web', 'Current product reservations');

$this->registerJsFile('@web/js/dynamic-table.js?'.uniqid(), []);

?>

<div id="product-reservation-list-wrapper">    
    <div class="row">
		<div class="col-sm-6 col-xs-9"><h2 class="page-title"><?= Yii::t('web', 'Current product reservations') ?></h2></div>
	</div>

    <div id="product-reservation-list-table" class="dynamic-table">
    	<h5><?php echo Yii::t('main','Filters')?></h5>
        <div class="row dynamic-table-filters">
            <div class="col-md-3 col-sm-4">
                <label><?= $listForm->getAttributeLabel('orderName') ?></label>
                    <input 
                        value="<?= $listForm->orderName ?>" 
                        class="form-control dynamic-table-filter" 
                        
                        type="text"
                        name="<?= Html::getInputName($listForm, 'orderName') ?>">
                
            </div> 
             <div class="col-md-3 col-sm-4">
                <label><?= $listForm->getAttributeLabel('productName') ?> </label>
                    <input 
                        value="<?= $listForm->productName ?>" 
                        class="form-control dynamic-table-filter" 
                        type="text"
                        name="<?= Html::getInputName($listForm, 'productName') ?>">
               
            </div> 
            <?= Yii::$app->controller->renderPartial('/inc/listFiltersButtons', ['isMoreFilters' => false, 'isClearFilters' => true]) ?>
        </div>
        <div class="dynamic-table-data">
            <?= Yii::$app->controller->renderPartial('_reservedProducts', [
	                'items' => $items,
	                'pages' => $pages,
	                'listForm' => $listForm,
            ]) ?>
        </div>
    </div>
    <?php $this->registerJs('dynamicTable("product-reservation-list-table", "'.Url::to(['warehouse-product/reservation-list']).'");', View::POS_END); ?>
</div>