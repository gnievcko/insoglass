<?php 
use yii\helpers\Html;
use yii\widgets\LinkPager;
use frontend\models\WarehouseProductDetailsForm;
use frontend\helpers\UserHelper;
use frontend\helpers\AdditionalFieldHelper;
use common\helpers\Utility;
use common\helpers\StringHelper;
use common\widgets\TableHeaderWidget;
?>

<div class="list-grid-table">
	<table class="<?= TableHeaderWidget::TABLECLASS ?>">
    	<?php echo TableHeaderWidget::widget(['fields' => WarehouseProductDetailsForm::getSortFields(), 'listForm' => $listForm, 'action' => false]);?>

        <tbody>
            <?php foreach($items as $item): ?>
                <tr>
                	<?php if(Yii::$app->params['isMoreWarehousesAvailable']) { ?> 
                    	<td><?= Html::encode($item['warehouse']) ?></td>
                    <?php } ?>
                    <td><?= !empty($item['parent']) ? Html::encode($item['version']) : '' ?></td>   
                    <td><?= Html::encode($item['status']) ?></td>
                    <td><span class="count-cell <?= $item['statusSymbol'] == Utility::PRODUCT_STATUS_ADDITION ? 'positive' : 'negative' ?>">
                    	<?= Html::encode(($item['statusSymbol'] == Utility::PRODUCT_STATUS_ADDITION ? '+' : '-').$item['count']) ?>
                    </span>
                    <td><?= Html::encode($item['description']) ?></td>
                    <td><?= UserHelper::getPrettyUserName($item['email'], $item['firstName'], $item['lastName']) ?></td>
                    <td><?php if(isset($item['additionalData'])) { 
                    	echo AdditionalFieldHelper::getFormattedData($item['additionalData']);
					} ?></td>
                    <td><?= Html::encode(StringHelper::getFormattedDateFromTimestamp($item['dateCreation'], true)) ?></td>
                </tr>
            <?php endforeach ?>
        </tbody>
    </table>
</div>
<div class="text-right dynamic-table-pagination">
    <?= LinkPager::widget(['pagination' => $pages ]) ?>
</div>

<input class="dynamic-table-sort-dir" type="hidden" name="<?= Html::getInputName($listForm, 'sortDir') ?>" value="<?= $listForm->sortDir ?>" />
<input class="dynamic-table-sort-field" type="hidden" name="<?= Html::getInputName($listForm, 'sortField') ?>" value="<?= $listForm->sortField ?>" />
<input class="dynamic-table-page" type="hidden" name="<?= Html::getInputName($listForm, 'page') ?>" value="<?= $listForm->page ?>" />
