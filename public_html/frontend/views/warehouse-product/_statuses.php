<?php 
use yii\helpers\Html;
use yii\widgets\LinkPager;
use frontend\models\WarehouseProductDetailsForm;
use frontend\helpers\UserHelper;
use frontend\helpers\AdditionalFieldHelper;
use common\helpers\Utility;
use common\helpers\StringHelper;
use common\widgets\TableHeaderWidget;
use yii\web\View;
use yii\helpers\Url;
use common\documents\DocumentType;
?>

<?php 
if (empty($items)) { ?>
	<label><?= Yii::t('web', 'No search results');?></label>
	<br><br>					
<?php }
else { ?>
	<div class="table-responsive col-sm-12">
		<table class="<?= TableHeaderWidget::TABLECLASS ?>">
	    	<?php echo TableHeaderWidget::widget(['fields' => WarehouseProductDetailsForm::getSortFields(), 'listForm' => $listForm, 'action' => false]);?>
	        <tbody>
	            <?php foreach($items as $item): ?>
	                <tr>
	                	<?php if(Yii::$app->params['isMoreWarehousesAvailable']) { ?> 
	                    	<td><?= Html::encode($item['warehouse']) ?></td>
	                    <?php } ?>
	                    <td><?= !empty($item['parent']) ? Html::encode($item['version']) : '' ?></td>   
	                    <td><?= Html::encode($item['status']) ?></td>
	                    <td><span class="count-cell <?= $item['isPositive'] > 0 ? 'positive' : 'negative' ?>">
	                    	<?= Html::encode(($item['isPositive'] > 0 ? '+' : '-').$item['count']) ?>
	                    </span>
	                    <td><?= Html::encode($item['description']) ?></td>
	                    <td><?= UserHelper::getPrettyUserName($item['email'], $item['firstName'], $item['lastName']) ?></td>
	                    <td><?php if(isset($item['additionalData'])) { 
	                    	echo AdditionalFieldHelper::getFormattedData($item['additionalData']);
						} ?></td>
	                    <td><?= Html::encode(StringHelper::getFormattedDateFromTimestamp($item['dateCreation'], true)) ?></td>
	                    <td>
                            <?php if($item['currencyShortSymbol'] && ($item['price_unit'] || $item['price_group'])): ?>
                                <?php $price = ($item['price_unit'] === null) ? $item['price_group'] : $item['price_unit'] ?>
                                <?= StringHelper::getFormattedCost($price).' '.$item['currencyShortSymbol']
                                        .'/'
                                        .mb_strtolower(Yii::t('web', ($item['price_unit'] === null) ? 'Package': 'Unit'), 'utf-8') 
                                ?>
                            <?php endif ?>
	                    </td>       
	                    <td>
							<div class="cloud-button glyphicon glyphicon-menu-down" style="display: <?= 
								in_array($item['statusSymbol'], [Utility::PRODUCT_STATUS_ISSUING, Utility::PRODUCT_STATUS_ISSUING_RESERVATION]) ||
								($item['statusSymbol'] == Utility::PRODUCT_STATUS_ADDITION && !empty($item['deliveryId'])) || 
								($item['statusSymbol'] == Utility::PRODUCT_STATUS_RESERVATION && empty($item['blockingStatusId'])) ?
									'inline-block' : 'none'
							?>"></div>
						  	<div class="dropdown-content">
						  		<?php if(in_array($item['statusSymbol'], [Utility::PRODUCT_STATUS_ISSUING, Utility::PRODUCT_STATUS_ISSUING_RESERVATION]) 
						  				&& in_array(DocumentType::GOODS_ISSUED, $availableDocuments)) {
						  			echo Html::a(Yii::t('documents', 'Goods Issue Note (GIN)'), 
						                    ['document/generate-goods-issued', 'historyId' => $item['id']],
						                    ['target' => '_blank']
						  			);
						  		} ?>
						  		
						  		<?php if($item['statusSymbol'] == Utility::PRODUCT_STATUS_RESERVATION && empty($item['blockingStatusId'])) {
						  			echo Html::a(Yii::t('web', 'Undo reservation'), ['warehouse-product/undo-reservation'], [
	                    							'data' => [
	                    									'confirm' => Yii::t('web', 'Are you sure you want to undo this reservation?'),
	                    									'method' => 'post',
	                    									'params' => [
	                    											'operationId' => $item['id'],
	                    											'orderId' => $item['orderId'],
	                    									],
	                    							],
	                    					]);
						  			echo Html::a(Yii::t('web', 'Issue product related to reservation'), ['warehouse-product/issue-reservation'], [
                    								'data' => [
                    										'confirm' => Yii::t('web', 'Are you sure you want to issue products related to this reservation?'),
                    										'method' => 'post',
                    										'params' => [
                    												'operationId' => $item['id'],
                    												'orderId' => $item['orderId'],
                    										],
                    								],
                    						]);
						  		} ?>
						  		<?php if($item['statusSymbol'] == Utility::PRODUCT_STATUS_ADDITION && in_array(DocumentType::GOODS_RECEIVED, $availableDocuments)) { 
						  			echo Html::a(Yii::t('documents', 'Goods Received Note (GRN)'),
						  					['document/generate-goods-received', 'deliveryId' => $item['deliveryId']],
						  					['target' => '_blank']
						  			);
						  		} ?>
						  	</div>
						</td> 
	                </tr>
	            <?php endforeach ?>
	        </tbody>
	    </table>
	</div>
	<div class="col-sm-12">
		<div class="col-sm-4 text-left dynamic-table-counter">
			<?= Yii::t('web', 'Number of found items: {n}', ['n' => $pages->totalCount]) ?>
		</div>
		<div class="col-sm-8 text-right dynamic-table-pagination">
		    <?= LinkPager::widget(['pagination' => $pages ]) ?>
		</div>
	</div>	
<?php }?>

<input class="dynamic-table-sort-dir" type="hidden" name="<?= Html::getInputName($listForm, 'sortDir') ?>" value="<?= $listForm->sortDir ?>" />
<input class="dynamic-table-sort-field" type="hidden" name="<?= Html::getInputName($listForm, 'sortField') ?>" value="<?= $listForm->sortField ?>" />
<input class="dynamic-table-page" type="hidden" name="<?= Html::getInputName($listForm, 'page') ?>" value="<?= $listForm->page ?>" />
<?php 
$this->registerJs('
		$(".cloud-button").on("click", function() {	
			$cloud = $("#cloud");
			$cloud.html($(this).next().html());
			offset = $(this).offset();
	 		$cloud.css("top", offset.top + $(this).height() + "px");
			$cloud.css("left", offset.left - $cloud.width() + "px");
			if($cloud.children().length > 0 || $cloud.css("display") == "block") {
				$cloud.toggle();
				
				if($(this).hasClass("glyphicon-menu-down")) {
					$(this).removeClass("glyphicon-menu-down");
					$(this).addClass("glyphicon-menu-up");
				}
				else {
					$(this).removeClass("glyphicon-menu-up");
					$(this).addClass("glyphicon-menu-down");
				}
			}
		});
					
		window.onclick = function(event) {	
			$cloud = $("#cloud");
	  		if (!event.target.matches(".cloud-button") && $cloud.css("display") == "block") {	
				$("#cloud").toggle();
  			}
}',  View::POS_END);
?>

