<?php
use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\View;
use dosamigos\multiselect\MultiSelect;
use common\helpers\StringHelper;
use common\bundles\EnhancedDialogAsset;

$this->title = Yii::t ( 'web', 'Product status in warehouse' ) . ' - ' . Yii::$app->name;

$this->params ['breadcrumbs'] [] = [ 
		'label' => Yii::t ( 'main', 'Warehouse' ),
		'url' => Url::to ( [ 
				'warehouse/index' 
		] ) 
];
$this->params ['breadcrumbs'] [] = [ 
		'label' => Yii::t ( 'web', 'Current products in warehouse' ),
		'url' => Url::to ( [ 
				'warehouse-product/list' 
		] ) 
];
$this->params ['breadcrumbs'] [] = Yii::t ( 'web', 'Product status in warehouse' );

$this->registerJsFile ( '@web/js/dynamic-table.js', [ ] );
?>

<div id="warehouse-product-details-wrapper">    
    <div class="row">
		<div class="col-sm-6"><h2 class="page-title"><?= Html::encode($productDetails['name']); ?></h2></div>
		<div class="col-sm-6 text-right"><?php 
				$buttons = '';
				$buttons .= Html::a(Yii::t('web', 'Update data'),
						['product/edit', 'id' => $productId], ['class' => 'btn btn-default']);
				$options = [
						'class' => 'btn btn-default',
				];
				
				$confirmMessage = Yii::t('main', 'Are you sure you want to delete this item?');
				if($productDetails['countSum'] != 0) {
					$confirmMessage .= '<br/><strong>' . Yii::t('main', 'If you confirm, all products of this kind will be deleted from warehouses!') . '</strong>';
				}
				$options['data'] = [
						'confirm' =>  $confirmMessage,
						'method' => 'post',
						'params' => [
								'id' => $productId,
						],
				];

				$buttons .= Html::a(Yii::t('web', 'Delete product'),
						['product/delete'], $options);
				echo $buttons;
			?>
		</div>		
	</div>	
	
	<div class="col-xs-12 title-with-icon">
		<div class="base-icon basic-data-icon main-content-icons"></div>
		<h3><?= Yii::t('web', 'General information') ?></h3>
	</div>
	<div class="col-xs-12">
		<div class="row">
			<div class="col-sm-9 col-xs-12">
				<div class="table-responsive">
					<?php $data = $productDetails; ?>
					<table class="table table-bordered detail-table">
				    	<tbody>
				    		<tr>
				    			<th><?= Yii::t('web', 'Name') ?></th>
				    			<td><?= Html::encode($data['name']) ?></td>
				    		</tr>
				    		<tr>
				    			<th><?= Yii::t('main', 'Warehouse index') ?></th>
				    			<td><?= Html::encode($data['index']) ?></td>
				    		</tr>
							<?php if(!empty($data['alias'])) { ?>
					    		<tr>
					    			<th><?= Yii::t('web', 'Common names') ?></th>
					    			<td><?= Html::encode($data['alias']) ?></td>
					    		</tr>
				    		<?php } ?>
				    		<tr>
				    			<th><?= Yii::t('web', 'Categories') ?></th>
				    			<td><?= $data['category'] ?></td>
				    		</tr>
				    		<?php if(!empty($data['version'])) { ?>
					    		<tr>
					    			<th><?= Yii::t('web', 'Versions') ?></th>
					    			<td><?= Html::encode($data['version']) ?></td>
					    		</tr>
				    		<?php } ?>
				    		<tr>
				    			<th><?= Yii::t('web', 'Addition date') ?></th>
				    			<td><?= Html::encode(StringHelper::getFormattedDateFromTimestamp($data['dateCreation'], true)) ?></td>
				    		</tr>
				    		<tr>
				    			<th><?= Yii::t('main', 'Last modification date') ?></th>
				    			<td><?= Html::encode(StringHelper::getFormattedDateFromTimestamp($data['dateModification'], true)) ?></td>
				    		</tr>
				    		<?php if(!empty($data['attachments'])) { ?>
								<tr>
			                        <th><?= Yii::t('web', 'Attachments') ?></th>
			                    	<td><?= $this->render('/inc/attachmentList', ['attachments' => $data['attachments']]) ?></td>
			                	</tr>
			                <?php } ?>
				    		<?php if(!empty($data['countMinimal'])) { ?>
				    			<tr>
				    				<th><?= Yii::t('main', 'Desired minimal count') ?></th>
				    				<td><?= Html::encode($data['countMinimal']) ?></td>
				    			</tr>
				    		<?php } ?>
				    		<tr>
				    			<th><?= Yii::t('web', 'Count of available items') ?></th>
				    			<td><?= Html::encode($data['countAvailableSum']) ?>
				    		</tr>
				    		<tr>
				    			<th><?= Yii::t('web', 'Count of reserved items') ?></th>
				    			<td><?= Html::encode($data['countSum']-$data['countAvailableSum']) ?>
				    		</tr>
				    	</tbody>
					</table>
				</div>
			</div>
		
			<div class="col-sm-3 asRow photo" >
				<span class="photo-container center-block <?= Yii::$app->params['isProductPhotoOnListVisible']?"":"hidden"?>">
						<?php
							$src = null;
							$imgElem = null;
							if(empty($data['urlPhoto'])) {
								$src = Url::to('@web/images/placeholders/product.png');
							} 
							else {
								$src = \common\helpers\UploadHelper::getNormalPath($data ['urlPhoto'], 'product_photo');
							}
							$imgElem = '<img src=' . $src .' alt="..." class="img-responsive center-block" align="middle"  vertical-align="middle" width="128px" height="auto">';
					
							if(!empty($data['urlPhoto'])) {
								echo '<a href="'.\common\helpers\UploadHelper::getNormalPath($data['urlPhoto'], 'product_photo').'" target="_blank">'.$imgElem.'</a>';
							}
							else {
								echo $imgElem;
							}
						?>		
					</span>
			</div>
		</div>
	</div>	
	
	<?php if(!empty($data['remarks'])) { ?>
		<div class="remarks">
			<div class="col-xs-12 title-with-icon">
				<div class="base-icon remark-icon main-content-icons"></div>
				<h3><?= Yii::t('main', 'Remarks') ?></h3>
			</div>
			<div class="col-xs-12">
				<?php echo nl2br(Html::encode($data['remarks'])); ?>
			</div>
		</div>
		<?php } ?>
		
		
		<div class="col-xs-12 title-with-icon">
			<div class="base-icon warehouse-icon main-content-icons"></div>
			<h3><?= Yii::t('web', 'Particular versions count') ?></h3>
		</div>
		<?php if($subversionsDim[0] > 1) { ?>
		<div class="col-sm-6 col-xs-12">
			<div class="table-responsive">
				<table class="table table-bordered detail-table">
					<thead>
						<tr>
							<?php for($j = 0; $j < $subversionsDim[1]; $j++) { ?>
								<th><?= Html::encode($subversions[0][$j]) ?></th>
						<?php } ?>	
						</tr>
					</thead>
					<tbody>
						<?php for($i = 1; $i < $subversionsDim[0]; $i++) { ?>
							<tr>
							<?php for($j = 0; $j < $subversionsDim[1]; $j++) { ?>
								<td><?= Html::encode($subversions[$i][$j]) ?></td>		
							<?php } ?>
							</tr>
						<?php } ?>			
				</tbody>
				</table>
			</div>
		</div>
		<?php
		} else {
			?>
			<div class="col-xs-12">
				<label><?= Yii::t('web', 'None');?></label>
			</div>
		<?php } ?>
		
	</div>
	<div class="col-xs-12 title-with-icon">
		<div class="base-icon history-icon main-content-icons"></div>
		<h3><?= Yii::t('web', 'History of operations on product') ?></h3>
	</div>
	<div id="warehouse-product-details-table" class="dynamic-table col-xs-12">
    	<?php if(Yii::$app->params['isMoreWarehousesAvailable']) { ?>
	    	<div class="row dynamic-table-filters">
				<input value="<?= $listForm->allWarehousesCount ?>"
					class="form-control dynamic-table-filter" type="hidden"
					name="<?= Html::getInputName($listForm, 'allWarehousesCount') ?>" />
				<div class="col-sm-2">
					<label>
	                    <?= $listForm->getAttributeLabel('warehouseIds')?>
	                    <?php
						$data = array_reduce ( $warehouses, function ($options, $t) {
							$options [$t ['id']] = Html::encode ( $t ['name'] );
							return $options;
						}, [ ] );
						
						echo MultiSelect::widget ( [ 
								'id' => 'warehouses',
								"options" => [ 
										'multiple' => 'multiple',
										'class' => 'dynamic-table-filter' 
								],
								'data' => $data,
								'name' => Html::getInputName ( $listForm, 'warehouseIds' ),
								'value' => empty ( $listForm->warehouseIds ) ? array_keys ( $data ) : $listForm->warehouseIds,
								"clientOptions" => [ 
										'nonSelectedText' => '',
										'nSelectedText' => mb_strtolower ( Yii::t ( 'main', 'Selected' ), 'UTF-8' ),
										'numberDisplayed' => 2,
										'buttonWidth' => '100%' 
								] 
						] );
						?>
	                </label>
				</div>
    	<?php } ?>
	        <div class="dynamic-table-data">
	            <?=Yii::$app->controller->renderPartial ( '_statuses', [ 'items' => $items,'pages' => $pages,'listForm' => $listForm, 'availableDocuments' => $availableDocuments ] )?>
	        </div>
		</div>
	</div>


<div id="cloud" class="col-xs-12"></div>

<?php 
	$this->registerJs('dynamicTable("warehouse-product-details-table", "'.Url::to(['warehouse-product/details']).'", { params: { productId: '.$productId.' }});', View::POS_END); 
	EnhancedDialogAsset::register($this);	
?>

