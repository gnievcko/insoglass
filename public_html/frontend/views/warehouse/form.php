<?php
use yii\helpers\Url;
use yii\helpers\Html;
use common\components\ActiveForm;
use common\helpers\ProvinceHelper;
use yii\web\View;
use yii\helpers\ArrayHelper;
use common\models\ar\Province;
use common\models\ar\Language;
use common\models\ar\Warehouse;

$this->registerCssFile('@web/css/select2.min.css');
$this->registerCssFile('@web/css/select2-bootstrap.min.css');
$this->registerJsFile('@web/js/select2/select2.min.js?'.uniqid(), ['depends' => [\yii\web\JqueryAsset::className()]]);
$this->registerJsFile('@web/js/select2/i18n/'.Yii::$app->language.'.js?'.uniqid(), ['depends' => [\yii\web\JqueryAsset::className()]]);
$this->registerJsFile('@web/js/province-by-zip-code.js?'.uniqid(), ['depends' => [\yii\web\JqueryAsset::className()]]);

$subtitle = $model->isEditScenarioSet() ? Yii::t('web', 'Update warehouse') : Yii::t('web', 'Add warehouse');
$this->title = $subtitle.' - '.Yii::$app->name;

$this->params['breadcrumbs'][] = ['label' => Yii::t('main', 'Warehouse'), 'url' => Url::to(['warehouse/index'])];
$this->params['breadcrumbs'][] = $subtitle;
?>

<div class="warehouse-form">
	<h2 class="page-title"><?= $subtitle ?></h2>

	<?php $form = ActiveForm::begin([
			'id' => 'client-form-ajax',
			'enableAjaxValidation' => true,
			'validateOnBlur' => false,
			'validateOnChange' => false,
			
	]); ?>

	<?php if($model->isEditScenarioSet()) {
		$form->field($model, 'id')->hiddenInput()->label(false);
	} ?>

	<div class="col-xs-12">	
		<div class="row">
			<div class="col-sm-6"><?= $form->field($model, 'name')->textInput(['maxlength' => true, 'placeHolder' => $model->getAttributeLabel('name')]) ?></div>	
			<div class="col-sm-6"><?php
				$warehouseQuery = Warehouse::find()->where('is_active = 1');
				if ($model->isEditScenarioSet()) {
					$warehouseQuery->andWhere('id != :id', [':id' => $model->id]);
				}
				
				echo $form->field($model, 'parentWarehouseId', ['enableAjaxValidation' => true])->dropDownList(
								ArrayHelper::map($warehouseQuery->all(), 'id', 'name'
			  	),['prompt' =>  Yii::t('main', 'Select a parent warehouse...')]); ?>		
			</div>
		</div>
	</div>
	<div class="col-xs-12">
		<div class="row">
			<div class="col-sm-6">
				<?= $form->field($model, 'index')->textInput(['maxlength' => true, 'placeHolder' => $model->getAttributeLabel('index')]) ?>
			</div>
		</div>
	</div>
	
	<div class="col-xs-12">
		<div class="row">
			<div class="col-sm-4 "><?= $form->field($model, 'address')->textInput(['maxlength' => true, 'placeHolder' => $model->getAttributeLabel('address')]) ?></div>
			<div class="col-sm-3"><?= $form->field($model, 'zipCode')->textInput(['maxlength' => true, 'placeHolder' => $model->getAttributeLabel('zipCode')]) ?></div>
			<div class="col-sm-2"><?= $form->field($model, 'city')->textInput(['maxlength' => true, 'placeHolder' => $model->getAttributeLabel('city')]) ?></div>
			<div class="col-sm-3"><?php
				$provinces = ProvinceHelper::getListOfNonArtificialProvinces();
				echo $form->field($model, 'provinceId')->dropDownList($provinces, ['prompt' => $model->getAttributeLabel('provinceId')]); 
			?></div>
		</div>
	</div>

	<div class="col-xs-12">
		<div class="row">
			<div class="pull-left"><?php 
				$returnUrl = null;
				Yii::$app->params['isMoreWarehousesAvailable'] ? $returnUrl = 'warehouse/list' : $returnUrl = 'warehouse/details';
				echo Html::a(Yii::t('web', 'Cancel'),
					!empty(Yii::$app->request->getReferrer()) ? Yii::$app->request->getReferrer() : [$returnUrl],
					['class' => 'btn btn-default']) 
			?></div>
			<div class="pull-right"><?= Html::submitButton(($model->isEditScenarioSet() ? Yii::t('main', 'Save') : Yii::t('web', 'Add')), ['class' => 'btn btn-primary']); ?></div>
		</div>
	</div>
	
	<?php ActiveForm::end(); ?>	
	<?php $this->registerJs('fillProvince("warehouseform-zipcode", "warehouseform-provinceid", "' . Url::to(['employee/get-province-by-zip-code']) . '");', View::POS_END); ?>
</div>
