<?php
use yii\helpers\Html;
use yii\helpers\Url;
use frontend\helpers\AddressHelper;
use common\bundles\EnhancedDialogAsset;
use common\helpers\StringHelper;
use common\models\ar\Warehouse;
use yii\helpers\ArrayHelper;

$this->title = Yii::t('web', 'Warehouse details') . ' - ' . Yii::$app->name;

$this->params['breadcrumbs'][] = ['label' => StringHelper::translateOrderToOffer('main', 'Warehouse'), 'url' => Url::to(['warehouse/index'])];
//$this->params['breadcrumbs'][] = ['label' => Yii::t('web', 'List of warehouses'), 'url' => Url::to(['warehouse/list'])];
$this->params['breadcrumbs'][] = Yii::t('web', 'Warehouse details');
?>

<div class="client-details">
	<div class="row">
		<div class="col-sm-6"><h2 class="page-title"><?= Yii::t('web', 'Warehouse details') ?></h2></div>
		<div class="col-sm-6 text-right">
			
				<?= 
					Html::a(Yii::t('main', 'Update'), 
							['warehouse/edit', 'id' => $model->id], ['class' => 'btn btn-primary']);
				?>
		</div>
	</div>
	
	<div class="table-responsive col-sm-12">
		<table class="table table-bordered detail-table">
	    	<tbody>
	    		<tr>
	    			<th><?= Yii::t('web', 'Name') ?></th>
	    			<td><?= Html::encode($model->name) ?></td>
	    		</tr>
	    		<tr>
	    			<th><?= Yii::t('main', 'Warehouse index') ?></th>
	    			<td><?= Html::encode($model->index) ?></td>
	    		</tr>
				<?php if(!empty($model->address)):?>
		    		<tr>
		    			<th><?= Yii::t('main', 'Address') ?></th>
		    			<td><?= Html::encode(AddressHelper::getFullAddress([
	                    		'addressMain' => $model->address->main,
	                    		'addressComplement' => $model->address->complement,
	                    		'cityName' => $model->address->city->name,
	                    		'cityZipCode' => $model->address->city->zip_code,
	                    		'provinceName' => $model->address->city->province->name,
	                    		'countryName' => $model->address->city->province->country->translation->name
	                    ])) ?></td>
		    		</tr>
	    		<?php endif ?>
	    		<?php if(!empty($model->parentWarehouse)):?>
		    		<tr>
		    			<th><?= Yii::t('main', 'Parent warehouse') ?></th>
		    			<td><?= Html::a(Html::encode($model->parentWarehouse->name), ['warehouse/details', 'id' => $model->parent_warehouse_id]); ?></td>
		    		</tr>
	    		<?php endif; ?>
	    		<tr>
	    			<th><?= Yii::t('main', 'Creation date') ?></th>
	    			<td><?= Html::encode(StringHelper::getFormattedDateFromDate($model->date_creation, 'true')) ?></td>
	    		</tr>
	    	</tbody>
		</table>
	</div>
	
</div>

<?php 
	EnhancedDialogAsset::register($this);
?>