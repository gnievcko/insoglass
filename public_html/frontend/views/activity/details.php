<?php

use yii\helpers\Html;
use yii\helpers\Url;

$this->title = Yii::t('web', 'Activity details').' - '.Yii::$app->name;

$this->params['breadcrumbs'][] = ['label' => Yii::t('main', 'Production'), 'url' => Url::to(['production/index'])];
$this->params['breadcrumbs'][] = ['label' => Yii::t('web', 'List of activities'), 'url' => Url::to(['activity/list'])];
$this->params['breadcrumbs'][] = Yii::t('web', 'Activity details');
?>

<div class="activity-details">
	<div class="row">
		<div class="col-md-7 col-sm-6"><h2 class="page-title"><?= Yii::t('web', 'Activity details')?></h2></div>
		<div class="col-md-5 col-sm-6 text-right">
    		<?php 
    		     $buttons = '';
    		     $refferrer = Yii::$app->request->getReferrer();
    		     if(substr($refferrer, strripos($refferrer, '/') + 1) == 'create') {
    		         $buttons .= Html::a(Yii::t('web','Add another activity'),
    		             ['activity/create'], ['class' => 'btn btn-primary']);
    		     }
    		     $buttons .= Html::a(Yii::t('main', 'Update'),
    		         ['activity/edit', 'id' => $details['id']], ['class' => 'btn btn-default']);
    		     $buttons .= Html::a(Yii::t('main', 'Delete'),
    		         ['activity/delete'],
    		         [
    		             'class' => 'btn btn-default',
    		             'data' => [
    		                 'confirm' => Yii::t('main', 'Are you sure you want to delete this item?'),
    		                 'method' => 'post',
    		                 'params' => [
    		                     'id' => $details['id'],
    		                 ],
     		             ],
    		         ]);
    		     echo $buttons;
    		?>
		</div>
	</div>
	<div class="col-xs-12 title-with-icon">
		<div class="base-icon basic-data-icon main-content-icon"></div>
		<h3 class="page-subtitle"><?= Yii::t('web', 'General information')?></h3>
	</div>
	<div class="col-xs-12">
		<div class="table-responsive">
			<table class="table table-bordered detail-table">
				<tbody>
					<tr>
						<th><?= Yii::t('web', 'Name')?> </th>
						<td><?= Html::encode($details['name'])?></td>
					</tr>
					<tr>
						<th><?= Yii::t('web', 'Effort factor')?></th>
						<td><?= $details['effort_factor']?></td>
					</tr>	
				</tbody>
			</table>
		</div>
	</div>
</div>
