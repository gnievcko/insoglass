<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\View;

$this->title = Yii::t('web', 'List of activities'). ' - ' .Yii::$app->name;

$this->params['breadcrumbs'][] = ['label' => Yii::t('main', 'Production'), 'url' => Url::to(['production/index'])];
$this->params['breadcrumbs'][] = Yii::t('web', 'List of activities');

$this->registerJsFile('@web/js/dynamic-table.js?'.uniqid(), []);
?>

<div id="activities-list-wrapper">
	<div class="row">
		<div class="col-sm-8">
			<h2 class="page-title"><?= Yii::t('web', 'List of activities') ?></h2>
		</div>
		<div class="col-sm-4 text-right"><?= Html::a(Yii::t('web', 'Add activity'),['activity/create'],['class' => 'btn btn-primary'])?></div>
	</div>
	
	<div id="activities-list-table" class="dynamic-table">
		<h5><?php echo Yii::t('main','Filters') ?></h5>
		<div class="row dynamic-table-filters">
			<div class="col-md-3 col-sm-4">
				<label><?php echo Yii::t('web','Search by name')?></label>
				<input
					value="<?= $activitiesListForm->name?>"
					class="form-control dynamic-table-filter"
					placeholder="<?= $activitiesListForm->getAttributeLabel('Name')?>"
					type="text"
					name="<?= Html::getInputName($activitiesListForm, 'name')?>" >
			</div>
			<?= Yii::$app->controller->renderPartial('/inc/listFiltersButtons', ['isMoreFilters' => false, 'isClearFilters' => true])?>
		</div>
		<div class="dynamic-table-data">
			<?= Yii::$app->controller->renderPartial('_activities', [
			    'activities' => $activities,
			    'pages' => $pages,
			    'activitiesListForm' => $activitiesListForm,
			]) ?>
		</div>
	</div>
	<?php
     $this->registerJs('dynamicTable("activities-list-table", "'.Url::to(['activity/list']).'");', View::POS_END); 
     ?>
</div>