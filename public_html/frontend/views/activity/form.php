<?php

use common\components\ActiveForm;
use yii\helpers\Html;
use yii\helpers\Url;

$this->registerCssFile('@web/css/select2.min.css');
$this->registerCssFile('@web/css/select2-bootstrap.min.css');
$this->registerJsFile('@web/js/select2/select2.min.js?'.uniqid(), ['depends' => [\yii\web\JqueryAsset::className()]]);
$this->registerJsFile('@web/js/select2/i18n/'.Yii::$app->language.'.js?'.uniqid(), ['depends' => [\yii\web\JqueryAsset::className()]]);

$subtitle = $model->isEditScenarioSet() ? Yii::t('web', 'Update activity data') : Yii::t('web', 'Add activity');
$this->title = $subtitle.' - '.Yii::$app->name;

$this->params['breadcrumbs'][] = ['label' => Yii::t('main', 'Production'), 'url' => Url::to(['production/index'])];
$this->params['breadcrumbs'][] = ['label' => Yii::t('web', 'List of activities'), 'url' =>Url::to(['activity/list'])];
$this->params['breadcrumbs'][] = $subtitle;
?>

<div class="activity-form">
	<h2 class="page-title"><?= $subtitle ?></h2>
	
	<?php $form = ActiveForm::begin([
	    'id' => 'activity-form-ajax',
	    'enableAjaxValidation' => true,
	]);?>
	
	<?php if($model->isEditScenarioSet()) {
	       $form->field($model, 'id')->hiddenInput()->label(false);
	}?>
	
	<div class="col-xs-12">
		<div class="row">
			<div class="col-sm-6">
				<?= $form->field($model, 'name')->textInput(['maxlength' => true, 'placeholder' => Yii::t('web','Activity name')]) ?>
				<?= $form->field($model, 'effortFactor')->textInput(['type' => 'number', 'placeholder' => Yii::t('web', 'Effort factor')])?>
			</div>
		</div>
	</div>
	<div class="col-xs-12">
		<div class="row">
			<div class="pull-left">
				<?= Html::a(Yii::t('web', 'Cancel'),
				    !empty(Yii::$app->request->getReferrer()) ? Yii::$app->request->getReferrer() : Url::to(['activity/list']),
				    ['class' => 'btn btn-default'])?>
			</div>
			<div class="pull-right">
				<?= Html::submitButton(($model->isEditScenarioSet() ? Yii::t('main', 'Save') : Yii::t('web','Add')),['class' => 'btn btn-primary'])?>
			</div>
		</div>
	</div>
	
	<?php ActiveForm::end();?>
</div>