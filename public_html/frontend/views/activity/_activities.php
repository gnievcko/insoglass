<?php 
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\LinkPager;
use common\bundles\EnhancedDialogAsset;
use common\widgets\TableHeaderWidget;
use frontend\models\ActivityListForm;
?>
<?php
if(empty($activities)) { ?>
	<label> <?= Yii::t('web', 'No search results')?></label>
	<br><br>
<?php }
else { ?>
<div class=" table-responsive">
	<table class="<?= TableHeaderWidget::TABLECLASS?>" >
		<?php echo TableHeaderWidget::widget(['fields'=> ActivityListForm::getSortFields(), 'listForm' => $activitiesListForm ]); ?>
		<tbody>
			<?php  foreach($activities as $activity): ?>
			<tr>
				<td><?= Html::encode($activity['name']) ?></td>
				<td><?php
				    echo Html::a('',Url::to(['activity/details', 'id' => $activity['id']]), ['class' => 'base-icon details-icon action-icon', 'title' => Yii::t('web','Details')]);
				    echo '&nbsp;'.Html::a('',Url::to(['activity/edit', 'id' => $activity['id']]), ['class' => 'base-icon editing-icon action-icon', 'title' => Yii::t('web', 'Edit')]);
				    echo '&nbsp;'.Html::a('',Url::to(['activity/delete']), [
				        'class' => 'base-icon delete-icon action-icon',
				        'title' => Yii::t('web','Delete'),
				        'data' => [
				            'confirm' => Yii::t('main','Are you sure you want to delete this item?'),
				            'method' => 'post',
				            'params' => [
				                    'id' => $activity['id'],
				            ],
				        ],
				    ]);
				?>
				</td>
			</tr>
			<?php endforeach;?>
		</tbody>
	</table>	
</div>
<div class="col-sm-12">
	<div class="col-sm-4 text-left dynamic-table-counter">
		<?= Yii::t('web', 'Number of found items: {n}', ['n' => $pages->totalCount]) ?>
	</div>
	<div class="col-sm-8 text-right dynamic-table-pagination">
		<?= LinkPager::widget(['pagination' => $pages ]) ?>
	</div>
</div>
<?php } ?>

<input class="dynamic-table-sort-dir" type="hidden" name="<?= Html::getInputName($activitiesListForm, 'sortDir') ?>" value="<?= $activitiesListForm->sortDir ?>" />
<input class="dynamic-table-sort-field" type="hidden" name="<?= Html::getInputName($activitiesListForm, 'sortField') ?>" value="<?= $activitiesListForm->sortField ?>" />
<input class="dynamic-table-page" type="hidden" name="<?= Html::getInputName($activitiesListForm, 'page') ?>" value="<?= $activitiesListForm->page ?>" />

<?php
	EnhancedDialogAsset::register($this);
?>

