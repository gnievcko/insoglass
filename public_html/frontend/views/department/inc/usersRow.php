<?php
use yii\helpers\Html;
use frontend\helpers\UserHelper;
use yii\helpers\Url;
?>

<div class="users-row">
	<?php foreach($users as $user) { 
	    if(!empty($managerOnly) && empty($user['is_lead'])) {
	        continue;
	    }
	    ?>
		<div class="user-row">
			<span class="photo-container">
				<?= '<div class="photo" style="background: url('.(!empty($user['urlPhoto']) ? \common\helpers\UploadHelper::getNormalPath($user['urlPhoto'], 'user_photo') : Url::to('@web/images/placeholders/person.png')).')"></div>'; ?>
			</span>
			<span class="name"> <?= Html::a(UserHelper::getPrettyUserName($user['email'], $user['first_name'], $user['last_name']) , ['employee/details', 'id' => $user['id']]);?></span>
		</div>
	<?php } ?>
</div>