<?php
use yii\helpers\Url;
use yii\helpers\Html;
use common\components\ActiveForm;
use kartik\widgets\Select2;
use yii\web\JsExpression;
use common\helpers\Utility;

$this->registerCssFile('@web/css/select2.min.css');
$this->registerCssFile('@web/css/select2-bootstrap.min.css');
$this->registerJsFile('@web/js/select2/select2.min.js?'.uniqid(), ['depends' => [\yii\web\JqueryAsset::className()]]);
$this->registerJsFile('@web/js/select2/i18n/'.Yii::$app->language.'.js?'.uniqid(), ['depends' => [\yii\web\JqueryAsset::className()]]);
$subtitle = $model->isEditScenarioSet() ? Yii::t('web', 'Update department data') : Yii::t('web', 'Add department');
$this->title = $subtitle.' - '.Yii::$app->name;

$this->params['breadcrumbs'][] = ['label' => Yii::t('main', 'Production'), 'url' => Url::to(['production/index'])];
$this->params['breadcrumbs'][] = ['label' => Yii::t('web', 'List of departments'), 'url' => Url::to(['department/list'])];
$this->params['breadcrumbs'][] = $subtitle;
?>
<div class="department-form">
	<h2 class="page-title"><?= $subtitle ?></h2>

	<?php $form = ActiveForm::begin([
			'id' => 'department-form-ajax',
			'enableAjaxValidation' => true,
	]); ?>

    <?php if($model->isEditScenarioSet()) {
        echo $form->field($model, 'id')->hiddenInput()->label(false);
    } ?>

	<div class="col-xs-12">
		<div class="row">
			<div class="col-sm-6"><?= $form->field($model, 'name')->textInput(['maxlength' => true, 'placeholder'=> Yii::t('main','Department name')]) ?></div>
		</div>
	</div>
	<div class="col-xs-12">
		<div class="row">
	        <div class="col-sm-6">
                <?= $form->field($model, 'departmentTypeId')->dropDownList($departmentTypes, ['prompt' => Yii::t('web', 'Select department type')]) ?>
	        </div>
		</div>
	</div>

    <div class="col-xs-12">
        <div class="row">
            <div class="col-sm-6">
                <?php echo $form->field($model, 'activityIds', ['enableAjaxValidation' => false])
                    ->widget(Select2::className(), [
                            'language' => Yii::$app->language,
                            'name' => 'select2_activities',
                            'options' => [
                                'placeholder' => Yii::t('web', 'Select activity...'), 'multiple' => true],
                            'showToggleAll' => false,
                            'data' => $activities,
                            'pluginOptions' => [
                                'allowClear' => false,
                                'minimumInputLength' => 1,
                                'tags' => false,
                                'ajax' => [
                                    'url' => Url::to(['activity/find']),
                                    'dataType' => 'json',
                                    'delay' => 250,
                                    'cache' => true,
                                    'data' => new JsExpression('function(params) {
                                     return {
                                         term: params.term,
                                         page: params.page || 1, 
                                         size: 20, 
                                     }; }'),
                                    'processResults' => new JsExpression('function (data, params) {
	                                                params.page = params.page || 1;
	                                                return {
	                                                    results: data.items,
	                                                    pagination: {   
	                                                        more: data.more
	                                                    }
	                                                };
	                                            }')
                                ],
                                'escapeMarkup' => new JsExpression('function (markup) { return markup; }'),
                                'templateResult' => new JsExpression('function(item) { if(item.loading) return item.text; if(item.id) return item.name; else return item.name; }'),
                                'templateSelection' => new JsExpression('function (item) {if(item.loading) return item.text; return item.text || item.name }'),
                            ],
                        ]
                    )?>
                <a href="<?= Url::to(['activity/create']); ?>" target="_blank" class="focusable-link  add-item-after-select2 add-item-after-select2-no-required">
                    <span class="glyphicon glyphicon-plus-sign pull-left"></span>
                    <span> <?=Yii::t('web', 'Create new activity')?></span>
                </a>
            </div>
        </div>
    </div>

    <div class="col-xs-12">
        <div class="row">
            <div class="col-sm-6">
                <?php echo $form->field($model, 'workerIds')
                    ->widget(Select2::className(), [
                            'language' => Yii::$app->language,
                            'name' => 'select2_workers',
                            'options' => [
                                'placeholder' => Yii::t('web', 'Select worker...'), 'multiple' => true],
                            'showToggleAll' => false,
                            'data' => $workers,
                            'pluginOptions' => [
                                'allowClear' => false,
                                'minimumInputLength' => 1,
                                'tags' => false,
                                'ajax' => [
                                    'url' => Url::to(['employee/find']),
                                    'dataType' => 'json',
                                    'delay' => 250,
                                    'cache' => true,
                                    'data' => new JsExpression('function(params) {
                                     return {
                                         term: params.term,
                                         page: params.page || 1, 
                                         size: 20, 
                                         roles: ["'.implode('","', [Utility::ROLE_WORKER, Utility::ROLE_QA_SPECIALIST, Utility::ROLE_MAIN_WORKER]).'"] 
                                     }; }'),
                                    'processResults' => new JsExpression('function (data, params) {
	                                                params.page = params.page || 1;
	                                                return {
	                                                    results: data.items,
	                                                    pagination: {   
	                                                        more: data.more
	                                                    }
	                                                };
	                                            }')
                                ],
                                'escapeMarkup' => new JsExpression('function (markup) { return markup; }'),
                                'templateResult' => new JsExpression('function(item) { if(item.loading) return item.text; if(item.id) return item.firstName+" "+item.lastName; else return item.name; }'),
                                'templateSelection' => new JsExpression('function (item) {if(item.loading) return item.text; return item.text || item.firstName+" "+item.lastName }'),
                            ],
                        ]
                    )?>
                <a href="<?= Url::to(['employee/create']); ?>" target="_blank" class="focusable-link  add-item-after-select2 add-item-after-select2-no-required">
                    <span class="glyphicon glyphicon-plus-sign pull-left"></span>
                    <span> <?=Yii::t('web', 'Create new employee')?></span>
                </a>
            </div>
        </div>
    </div>

    <div class="col-xs-12">
        <div class="row">
            <div class="col-sm-6">
                <?php echo $form->field($model, 'managerId', ['enableAjaxValidation' => false])
                    ->widget(Select2::className(), [
                            'language' => Yii::$app->language,
                            'name' => 'select2_manager',
                            'options' => [
                                    'placeholder' => Yii::t('web', 'Select manager')."...", 'multiple' => false],
                            'showToggleAll' => false,
                            'data' => $manager,
                            'pluginOptions' => [
                                'allowClear' => true,
                                'minimumInputLength' => 1,
                                'tags' => false,
                                'ajax' => [
                                    'url' => Url::to(['employee/find']),
                                    'dataType' => 'json',
                                    'delay' => 250,
                                    'cache' => true,
                                    'data' => new JsExpression('function(params) {
                                     return {
                                         term: params.term,
                                         page: params.page || 1, 
                                         size: 20, 
                                         roles: ["'.implode('","', [Utility::ROLE_WORKER, Utility::ROLE_QA_SPECIALIST, Utility::ROLE_MAIN_WORKER]).'"] 
                                     }; }'),
                                    'processResults' => new JsExpression('function (data, params) {
	                                                params.page = params.page || 1;
	                                                return {
	                                                    results: data.items,
	                                                    pagination: {   
	                                                        more: data.more
	                                                    }
	                                                };
	                                            }')
                                ],
                                'escapeMarkup' => new JsExpression('function (markup) { return markup; }'),
                                'templateResult' => new JsExpression('function(item) { if(item.loading) return item.text; if(item.id) return item.firstName+" "+item.lastName; else return item.name; }'),
                                'templateSelection' => new JsExpression('function (item) {if(item.loading) return item.text; return item.text || item.firstName+" "+item.lastName }'),
                            ],
                        ]
                    ) ?>
                <a href="<?= Url::to(['employee/create']); ?>" target="_blank" class="focusable-link  add-item-after-select2 add-item-after-select2-no-required">
                    <span class="glyphicon glyphicon-plus-sign pull-left"></span>
                    <span> <?=Yii::t('web', 'Create new employee')?></span>
                </a>
            </div>
        </div>
    </div>

	<div class="col-xs-12">
		<div class="pull-left">
			<?= Html::a(Yii::t('web', 'Cancel'),
 				!empty(Yii::$app->request->getReferrer()) ? Yii::$app->request->getReferrer() : ['department/list'],
				['class' => 'btn btn-default']) ?>
		</div>
		<div class="pull-right">
			<?= Html::submitButton(($model->isEditScenarioSet() ? Yii::t('main', 'Save') : Yii::t('web', 'Add')), ['class' => 'btn btn-primary']); ?>
		</div>
	</div>
</div>
<div>
	<?php ActiveForm::end(); ?>
</div>

