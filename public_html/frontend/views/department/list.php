<?php
use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\View;
use common\bundles\EnhancedDialogAsset;
use common\helpers\StringHelper;

$this->title = Yii::t('web', 'List of departments') . ' - ' . Yii::$app->name;

$this->params['breadcrumbs'][] = ['label' => \Yii::t('main', 'Production'), 'url' => Url::to(['production/index'])];
$this->params['breadcrumbs'][] = Yii::t('web', 'List of departments');

$this->registerJsFile('@web/js/dynamic-table.js?'.uniqid(), []);
?>

<div id="departments-list-wrapper">
    <div class="row">
        <div class="col-sm-6 col-xs-9"><h2 class="page-title"><?= Yii::t('web', 'List of departments') ?></h2></div>
        <div class="col-sm-6 col-xs-3"><?= Html::a(Yii::t('web', 'Add department'), ['department/create'], ['class' => 'btn btn-primary pull-right btn-md']) ?>
        </div>
    </div>

    <div id="departments-list-table" class="dynamic-table">
        <h5><?php echo Yii::t('main','Filters')?></h5>
        <div class="row dynamic-table-filters">
            <div class="col-md-3 col-sm-6">
                <label><?php echo Yii::t('web','Search by name')?></label>
                <input
                    value="<?= $listForm->name ?>"
                    class="form-control dynamic-table-filter"
                    type="text"
                    placeholder="<?= $listForm->getAttributeLabel('name') ?>"
                    name="<?= Html::getInputName($listForm, 'name') ?>">
            </div>
            <div class="col-md-3 col-sm-6">
                <label><?php echo Yii::t('web','Search by type')?></label>
                <?php
                echo Html::activeDropDownList($listForm, 'typeId', $departmentTypes, ['class' => 'form-control dynamic-table-filter no-margin-bottom','prompt'=>Yii::t('web','Any')]);
                ?>
            </div>
            <?= Yii::$app->controller->renderPartial('/inc/listFiltersButtons', ['isMoreFilters' => false, 'isClearFilters' => true]) ?>
        </div>
        <div class="dynamic-table-data">
            <?= Yii::$app->controller->renderPartial('_departments', [
                'departments' => $departments,
                'pages' => $pages,
                'listForm' => $listForm,
            ]) ?>
        </div>
    </div>
    <?php $this->registerJs('dynamicTable("departments-list-table", "'.Url::to(['department/list']).'");', View::POS_END); ?>
</div>
