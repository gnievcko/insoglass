<?php
use yii\helpers\Url;
use yii\helpers\Html;
use yii\widgets\LinkPager;
use frontend\models\DepartmentsListForm;
use common\widgets\TableHeaderWidget;
use common\bundles\EnhancedDialogAsset;
?>

<?php
if(empty($departments)) { ?>
    <label><?= Yii::t('web', 'No search results');?></label>
    <br><br>
<?php }
else { ?>
    <div class=" table-responsive">
        <table class="<?= TableHeaderWidget::TABLECLASS ?>">
            <?php echo TableHeaderWidget::widget(['fields' => DepartmentsListForm::getSortFields() , 'listForm' => $listForm]);?>

            <tbody>
            <?php foreach($departments as $department): ?>
                <tr>
                    <td><?= Html::encode($department['name']) ?></td>
                    <td><?= Html::encode($department['type']) ?></td>
                    <td><?= Html::encode($department['manager']) ?></td>

                        <td><?php
                            echo Html::a('', Url::to(['department/details', 'id' => $department['id']]), ['class' => 'base-icon details-icon action-icon', 'title' => Yii::t('web', 'Details')]);
                            echo '&nbsp;'.Html::a('', Url::to(['department/edit', 'id' => $department['id']]), ['class' => 'base-icon editing-icon action-icon', 'title' => Yii::t('web', 'Edit')]);
                            echo '&nbsp;'.Html::a('', Url::to(['department/delete']), [
                                    'class' => 'base-icon delete-icon action-icon',
                                    'title' => Yii::t('main', 'Delete'),
                                    'data' => [
                                        'confirm' => Yii::t('main', 'Are you sure you want to delete this item?'),
                                        'method' => 'post',
                                        'params' => [
                                            'id' => $department['id'],
                                        ],
                                    ],
                                ]);
                            ?>
                        </td>
                </tr>
            <?php endforeach ?>
            </tbody>
        </table>
    </div>
    <div class="col-sm-12">
        <div class="col-sm-4 text-left dynamic-table-counter">
            <?= Yii::t('web', 'Number of found items: {n}', ['n' => $pages->totalCount]) ?>
        </div>
        <div class="col-sm-8 text-right dynamic-table-pagination">
            <?= LinkPager::widget(['pagination' => $pages ]) ?>
        </div>
    </div>
<?php }?>

<input class="dynamic-table-sort-dir" type="hidden" name="<?= Html::getInputName($listForm, 'sortDir') ?>" value="<?= $listForm->sortDir ?>" />
<input class="dynamic-table-sort-field" type="hidden" name="<?= Html::getInputName($listForm, 'sortField') ?>" value="<?= $listForm->sortField ?>" />
<input class="dynamic-table-page" type="hidden" name="<?= Html::getInputName($listForm, 'page') ?>" value="<?= $listForm->page ?>" />

<?php
    EnhancedDialogAsset::register($this);
?>