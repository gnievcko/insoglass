<?php

use yii\helpers\Html;
use yii\helpers\Url;
use common\bundles\EnhancedDialogAsset;

$this->title = Yii::t('web', 'Department details') . ' - ' . Yii::$app->name;

$this->params['breadcrumbs'][] = ['label' => Yii::t('main', 'Production'), 'url' => Url::to(['production/index'])];
$this->params['breadcrumbs'][] = ['label' => Yii::t('web', 'List of departments'), 'url' => Url::to(['department/list'])];
$this->params['breadcrumbs'][] = Yii::t('web', 'Department details');
?>

<div class="department-details">
    <div class="row">
        <div class="col-sm-6"><h2 class="page-title"><?= Yii::t('web', 'Department details') ?></h2></div>
        <div class="col-sm-6">
            <div class="pull-right"><?php
                $buttons = null;
                $referrer = Yii::$app->request->getReferrer();
                if(substr($referrer, strripos($referrer, '/') + 1) == 'create') {
                    $buttons .= Html::a('<span class="glyphicon glyphicon-plus"></span>&nbsp;'.Yii::t('web', 'Add another department'),
                        ['department/create'], ['class' => 'btn btn-primary']);
                }
                $buttons .= Html::a(Yii::t('main', 'Update'),
                        ['department/edit', 'id' => $details['id']], ['class' => 'btn btn-default'])
                    .Html::a(Yii::t('main', 'Delete'),
                        ['department/delete'],
                        [
                            'class' => 'btn btn-default',
                            'data' => [
                                'confirm' => Yii::t('main', 'Are you sure you want to delete this item?'),
                                'method' => 'post',
                                'params' => [
                                    'id' => $details['id'],
                                ],
                            ],
                        ]);
                echo $buttons;
                ?></div>
        </div>
    </div>

    <div class="table-responsive">
        <table class="table table-bordered detail-table">
            <tbody>
            <tr>
                <th><?= Yii::t('web', 'Id') ?></th>
                <td><?= $details['id'] ?></td>
            </tr>
            <tr>
                <th><?= Yii::t('web', 'Name') ?></th>
                <td><?= Html::encode($details['name']) ?></td>
            </tr>
            <tr>
                <th><?= Yii::t('web', 'Type') ?></th>
                <td><?= Html::encode($details['type']) ?></td>
            </tr>
            <tr>
                <th><?= Yii::t('web', 'Activities') ?></th>
                <td>
                    <?php if(count($activities) > 0) {
                        foreach ($activities as $activity) { ?>
                            <span class="name"><?= Html::a(Html::encode($activity['name']) ,
                                    ['activity/details', 'id' => $activity['id']]);
                                ?></span>
                            <?php
                        }
                    }
                    ?>
                </td>
            </tr>
                <tr>
                    <th><?= Yii::t('web', 'Manager') ?></th>
                    <td>
                        <?php if(count($users) > 0) {
                            echo $this->render('inc/usersRow', ['users' => $users, 'managerOnly' => true]);
                        }
                         ?>
                    </td>
                </tr>

            	<?php if(!empty($users)) { ?>
                    <tr>
                        <th><?= Yii::t('web', 'Workers') ?></th>
                        <td><?= $this->render('inc/usersRow', ['users' => $users, 'managerOnly' => false]) ?></td>
                    </tr>
            	<?php } ?>

            </tbody>
        </table>
    </div>

</div>

<?php
EnhancedDialogAsset::register($this);
?>
