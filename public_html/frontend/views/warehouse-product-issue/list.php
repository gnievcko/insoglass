<?php
use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\View;
use common\helpers\Utility;
use common\models\ar\ProductStatus;

$this->title = Yii::t('web', 'List of issues') . ' - ' . Yii::$app->name;

$this->params['breadcrumbs'][] = ['label' => Yii::t('main', 'Warehouse'), 'url' => Url::to(['warehouse/index'])];
$this->params['breadcrumbs'][] = Yii::t('web', 'List of issues');

$this->registerJsFile('@web/js/dynamic-table.js?'.uniqid(), []);
?>

<div id="issue-list-wrapper">
    <div class="row">
		<div class="col-sm-6 col-xs-9"><h2 class="page-title"><?= Yii::t('web', 'List of issues') ?></h2></div>
		<div class="col-xs-6"><?= Html::a(Yii::t('web', 'Issue products'), ['warehouse-product/update', 'productStatusId' => ProductStatus::find()->where(['symbol' => Utility::PRODUCT_STATUS_ISSUING])->one()->id], ['class' => 'btn btn-primary pull-right']) ?></div>
	</div>

    <div id="issue-list-table" class="dynamic-table">
    	<h5><?php echo Yii::t('main','Filters')?></h5>
        <div class="row dynamic-table-filters">
            <div class="col-md-3 col-sm-4">
            	<label><?php echo Yii::t('web','Search by number')?></label>
                    <input
                        value="<?= $listForm->number ?>"
                        class="form-control dynamic-table-filter"
                        type="text"
						placeholder="<?= $listForm->getAttributeLabel('number') ?>"
                        name="<?= Html::getInputName($listForm, 'number') ?>">

            </div>
            <?= Yii::$app->controller->renderPartial('/inc/listFiltersButtons', ['isMoreFilters' => false, 'isClearFilters' => true]) ?>
        </div>
        <div class="dynamic-table-data">
            <?= Yii::$app->controller->renderPartial('_issues', [
	                'data' => $data,
	                'pages' => $pages,
	                'listForm' => $listForm,
            ]) ?>
        </div>
    </div>
    <?php $this->registerJs('dynamicTable("issue-list-table", "'.Url::to(['warehouse-product-issue/list']).'");', View::POS_END); ?>
</div>
