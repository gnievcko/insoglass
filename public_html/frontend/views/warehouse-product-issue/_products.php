<?php 
use yii\helpers\Url;
use yii\helpers\Html;
use yii\widgets\LinkPager;
use common\bundles\EnhancedDialogAsset;
use frontend\models\WarehouseDeliveryProductListForm;
use common\helpers\StringHelper;
use common\widgets\TableHeaderWidget;
use frontend\models\WarehouseProductIssueProductListForm;
?>

<?php if(empty($products)) { ?>
	<label><?= Yii::t('web', 'No search results');?></label>			
<?php }
else { ?>
	<div class="table-responsive">
		<table class="<?= TableHeaderWidget::TABLECLASS ?>">
	    	<?php echo TableHeaderWidget::widget(['fields' => WarehouseProductIssueProductListForm::getSortFields(), 'listForm' => $listForm]);?>
	        <tbody>
	            <?php foreach($products as $product): ?>
	                <tr>
	                	<td class="photo-container">
	                        <?= Html::img(empty($product['urlThumbnail']) ? '@web/images/placeholders/product.png' : $product['urlThumbnail']) ?>
						</td>
	                    <td><?= Html::encode($product['name']) ?></td>
	                    <td><?= Html::encode($product['count']) ?></td>
	                    <td><?= Html::encode($product['unit']) ?></td>
	                    <td><?= Html::a('', Url::to(['warehouse-product/details', 'productId' => $product['id']]), ['class' => 'base-icon details-icon action-icon']) ?></td>
	                </tr>
	            <?php endforeach ?>
	        </tbody>
	    </table>
	</div>
	<div class="text-right dynamic-table-pagination">
	    <?= LinkPager::widget(['pagination' => $pages ]) ?>
	</div>
<?php } ?>

<input class="dynamic-table-sort-dir" type="hidden" name="<?= Html::getInputName($listForm, 'sortDir') ?>" value="<?= $listForm->sortDir ?>" />
<input class="dynamic-table-sort-field" type="hidden" name="<?= Html::getInputName($listForm, 'sortField') ?>" value="<?= $listForm->sortField ?>" />
<input class="dynamic-table-page" type="hidden" name="<?= Html::getInputName($listForm, 'page') ?>" value="<?= $listForm->page ?>" />
