<?php 
use yii\helpers\Url;
use yii\helpers\Html;
use common\helpers\UserNameHelper;
use yii\widgets\LinkPager;
use frontend\models\CompanyListForm;
use frontend\helpers\AddressHelper;
use common\bundles\EnhancedDialogAsset;
use common\widgets\TableHeaderWidget;
use common\helpers\StringHelper;
use frontend\models\WarehouseProductIssueListForm;
use frontend\helpers\UserHelper;
?>

<?php 
if (empty($data)) { ?>
	<label><?= Yii::t('web', 'No search results');?></label>
<?php }
else { ?>
	<div class=" table-responsive">
		<table class="<?= TableHeaderWidget::TABLECLASS ?>">
	    	<?php echo TableHeaderWidget::widget(['fields' => WarehouseProductIssueListForm::getSortFields(), 'listForm' => $listForm]);?>

	        <tbody>
	            <?php foreach($data as $row): ?>
	                <tr>
	                    <td><?= !empty($row['number']) ? Html::encode($row['number']) : '-' ?></td>                    
	                    <td><?= Html::encode($row['positionCount']) ?></td>
	                    <td><?= UserHelper::getPrettyUserName($row['userConfirmingEmail'], $row['userConfirmingFirstName'], $row['userConfirmingLastName']) ?></td>
	                    <td><?= UserHelper::getPrettyUserName($row['userReceivingEmail'], $row['userReceivingFirstName'], $row['userReceivingLastName']) ?></td>	                    
	                    <td><?= Html::encode($row['companyName']) ?></td>
	                    <td><?= StringHelper::boolTranslation($row['isAccounted'], false) ?></td>
                        <td><?= Html::encode(StringHelper::getFormattedDateFromTimestamp($row['dateCreation'])) ?></td>
                        <td><?php 
                    		echo Html::a('', Url::to(['warehouse-product-issue/details', 'id' => $row['id']]), ['class' => 'base-icon details-icon action-icon', 'title' => Yii::t('web', 'Details')]);
                    	?></td>
	                </tr>
	            <?php endforeach ?>
	        </tbody>
	    </table>
	</div>
	<div class="col-sm-12">
		<div class="col-sm-4 text-left dynamic-table-counter">
			<?= Yii::t('web', 'Number of found items: {n}', ['n' => $pages->totalCount]) ?>
		</div>
		<div class="col-sm-8 text-right dynamic-table-pagination">
		    <?= LinkPager::widget(['pagination' => $pages ]) ?>
		</div>
	</div>
<?php }?>

<input class="dynamic-table-sort-dir" type="hidden" name="<?= Html::getInputName($listForm, 'sortDir') ?>" value="<?= $listForm->sortDir ?>" />
<input class="dynamic-table-sort-field" type="hidden" name="<?= Html::getInputName($listForm, 'sortField') ?>" value="<?= $listForm->sortField ?>" />
<input class="dynamic-table-page" type="hidden" name="<?= Html::getInputName($listForm, 'page') ?>" value="<?= $listForm->page ?>" />

<?php 
	EnhancedDialogAsset::register($this);
?>