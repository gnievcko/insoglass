<?php
use yii\helpers\Html;
use yii\helpers\Url;
use frontend\helpers\UserHelper;
use common\bundles\EnhancedDialogAsset;
use common\helpers\StringHelper;
use common\documents\DocumentType;

$this->title = Yii::t('web', 'Issue details') . ' - ' . Yii::$app->name;

$this->params['breadcrumbs'][] = ['label' => Yii::t('main', 'Warehouse'), 'url' => Url::to(['warehouse/index'])];
$this->params['breadcrumbs'][] = ['label' => Yii::t('web', 'List of issues'), 'url' => Url::to(['warehouse-product-issue/list'])];
$this->params['breadcrumbs'][] = Yii::t('web', 'Issue details');
?>

<div class="issue-details">
	<div class="row">
		<div class="col-md-4 col-sm-6"><h2 class="page-title"><?= Yii::t('web', 'Issue details') ?></h2></div>
        <div class="col-md-8 col-sm-6 text-right"><?php 
		        $buttons = '';
		        $buttons .= Html::a(Yii::t('web', 'Update data'), ['warehouse-product-issue/edit', 'id' => $operation['id']], ['class' => 'btn btn-default']);
		        $buttons .= Html::a(Yii::t('web', 'Return products to warehouse'), ['warehouse-delivery/create', 'issueId' => $operation['id']], ['class' => 'btn btn-default', 'target' => '_blank']);
	        	echo $buttons;
        	?>
		</div>
	</div>
	
	<div class="col-xs-12 title-with-icon">
		<div class="base-icon basic-data-icon main-content-icons"></div>
		<h3><?= Yii::t('web', 'General information') ?></h3>
	</div>
	<div class="col-xs-12">
		<div class="row">
		    <div class="col-sm-9">	
				<div class="table-responsive">
					<table class="table table-bordered detail-table">
				    	<tbody>
				    		<?php if(Yii::$app->params['isMoreWarehousesAvailable']) { ?> 
					    		<tr>
					    			<th><?= Yii::t('web', 'Warehouse') ?></th>
					    			<td><?= Html::a($operation['warehouseName'], Url::to(['warehouse/details', 'id' => $operation['warehouseId']])) ?></td>
					    		</tr>
				    		<?php } ?>
			                <tr>
		                        <th><?= Yii::t('web', 'Type') ?></th>
		                        <td><?= Html::encode($operation['type']) ?></td>
		                    </tr>
				    		<tr>
				    			<th><?= Yii::t('web', 'Number') ?></th>
				    			<td><?= $operation['number'] ?></td>
				    		</tr>
				    		<?php if(!empty($operation['userConfirmingEmail'])) { ?>
					    		<tr>
					    			<th><?= Yii::t('web', 'Issuing user') ?></th>
				                    <td>
				                        <?php $userName = UserHelper::getPrettyUserName($operation['userConfirmingEmail'], $operation['userConfirmingFirstName'], $operation['userConfirmingLastName']) ?>
				                        <?= Html::a($userName, Url::to(['employee/details', 'id' => $operation['userConfirmingId']])) ?>
				                    </td>
					    		</tr>
				    		<?php } ?>
				    		<?php if($operation['userReceivingEmail']) { ?>
					    		<tr>
					    			<th><?= Yii::t('web', 'Receiving user') ?></th>
				                    <td>
				                        <?php $userName = UserHelper::getPrettyUserName($operation['userReceivingEmail'], $operation['userReceivingFirstName'], $operation['userReceivingLastName']) ?>
				                        <?= Html::a($userName, Url::to(['employee/details', 'id' => $operation['userReceivingId']])) ?>
				                    </td>
					    		</tr>
				    		<?php } ?>
				    		<?php if(!empty($operation['companyName'])) { ?>
					    		<tr>
					    			<th><?= Yii::t('web', 'Client'); ?></th>
					    			<td>
					    				<?php $companyParts = explode('|', $operation['companyName'], 2); ?>
					    				<?= Html::a($companyParts[1], Url::to(['client/details', 'id' => $companyParts[0]])) ?>
					    			</td>
					    		</tr>
				    		<?php } ?>
				    		<tr>
				    			<th><?= Yii::t('main', 'Creation date') ?></th>
				    			<td><?= StringHelper::getFormattedDateFromDate($operation['dateCreation'], true) ?></td>
				    		</tr>
				    		<tr>
				    			<th><?= Yii::t('web', 'Is accounted') ?></th>
				    			<td><?= StringHelper::boolTranslation(!empty($operation['isAccounted']) ? $operation['isAccounted'] : 0, false) ?></td>
				    		</tr>
				    	</tbody>
					</table>
				</div>
			</div>
		    <div class="col-sm-3">
		    	<?php if(!empty($operation['firstProductId'])) { ?>
			    	<div class="row">
				       	<h3 class=""><?= Yii::t('web', 'Documents') ?></h3>
				        <?php if(in_array(DocumentType::GOODS_ISSUED, $availableDocuments)) { ?>
			            	<div class="document-link-container" ><div class="base-icon form-icon doc-icons"></div>
		                        <?= Html::a(''.' '.Yii::t('documents', 'Goods Issue Note (GIN)'), 
				                    		['document/generate-goods-issued', 'historyId' => $operation['firstProductId']],
				                            ['target' => '_blank']
								); ?>
		                    </div>
		                <?php } ?>
		            </div>
			     <?php } ?>
		    </div>
	    </div>
	</div>
	<div class="col-xs-12 title-with-icon">
		<div class="base-icon comment-icon main-content-icons"></div>
		<h3 class="page-subtitle"><?= Yii::t('web', 'Issue comment') ?></h3>
	</div>
	<div class="description">
		<div class="col-xs-12">
			<?php if(!empty($operation['description'])) { ?>
				<div class="col-sm-8 desc">
					<?php echo nl2br(Html::encode($operation['description'])); ?>
				</div>
			<?php }
			else { ?>
				<div class="col-sm-8 desc no-desc">
					<?php echo Yii::t('web', 'No comment.'); ?>
				</div>
			<?php } ?>
		</div>
	</div>
	
    <?= Yii::$app->controller->renderPartial('_products_list', $listData + ['operationId' => $operation['id']]) ?>
</div>

<?php 
	EnhancedDialogAsset::register($this);
?>
