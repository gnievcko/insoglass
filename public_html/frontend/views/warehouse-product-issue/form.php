<?php
use yii\helpers\Url;
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\web\View;
use yii\helpers\ArrayHelper;
use common\models\ar\Province;
use common\models\ar\Language;
use yii\web\JsExpression;
use common\helpers\StringHelper;
use common\models\ar\Warehouse;

$subtitle = Yii::t('web', 'Update issue data');
$this->title = $subtitle.' - '.Yii::$app->name;

$this->params['breadcrumbs'][] = ['label' => Yii::t('main', 'Warehouse'), 'url' => Url::to(['warehouse/index'])];
$this->params['breadcrumbs'][] = ['label' => Yii::t('web', 'List of issues'), 'url' => Url::to(['warehouse-product-issue/list'])];
$this->params['breadcrumbs'][] = $subtitle;
?>

<div class="warehouse-form">
	<h2 class="page-title"><?= $subtitle ?></h2>

	<?php $form = ActiveForm::begin([
			'id' => 'warehouse-product-issue-form-ajax',
			'enableAjaxValidation' => true,
			'validateOnBlur' => false,
			'validateOnChange' => false,
			
	]); ?>

	<?= $form->field($model, 'id')->hiddenInput()->label(false); ?>

	<div class="col-xs-12">	
		<div class="row">
			<div class="col-sm-9">
				<?= $form->field($model, 'number')->textInput(['maxlength' => true, 'placeHolder' => $model->getAttributeLabel('number'),
							'disabled' => !empty($model->number), 'readonly' => !empty($model->number),
						])->label(false)
						->hint(Yii::t('web', 'The issue number is assigned from template in a form "x/y/z", where: <ul><li>"x" means next number of sequence in the current year, </li><li>"y" are initials of the user and </li><li>"z" is the current year.</ul>It is possible to some archived issues do not have number from the sequence and it can be introduced by modifying content of the input above. There is impossible to assign another number if it was assigned before. Also, manual modification of the number does not imply on system\'s sequence so incompatibilities can occur.')); 
				?>
			</div>	
		</div>
	</div>
	<div class="col-xs-12">	
		<div class="row">
			<div class="col-sm-6"><?= $form->field($model, 'isAccounted')->checkbox() ?></div>
		</div>
	</div>
		
	<div class="col-xs-12">
		<div class="pull-left"><?= Html::a(Yii::t('web', 'Cancel'), ['warehouse-product-issue/details', 'id' => $model->id], ['class' => 'btn btn-default']) ?></div>
		<div class="pull-right"><?= Html::submitButton(Yii::t('main', 'Save'), ['class' => 'btn btn-primary']); ?></div>
	</div>
	
	<?php ActiveForm::end(); ?>	
</div>
