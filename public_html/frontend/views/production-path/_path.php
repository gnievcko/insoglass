<?php

use yii\helpers\Html;
?>

<div class="row">
    <div id="production-path" class="col-sm-8"></div>
    <div class="col-sm-4">
        <div class="col-sm-12" id="activities-list">
            <?= Html::input('text', 'production-path-name', '', ['id' => 'production-path-name', 'class' => 'form-control', 'placeholder' => Yii::t('main', 'Write production path name')]) ?>
        </div>
        <div class="col-sm-12" id="activities-list">
            <?= Html::input('text', 'activities-search', '', ['id' => 'search-activity', 'class' => 'form-control', 'placeholder' => Yii::t('main', 'Select activity')]) ?>
            <ul id="activities"></ul>
        </div> 
    </div>
</div>
<div class="row product-attribute-container parent-container " id="attributes"></div>
<div class="col-sm-12" id="activities-list">
    <?= Html::button(Yii::t('main', 'Save'), ['class' => 'btn btn-primary pull-right', 'id' => 'saveNetworkButton']) ?>
</div>
