<?php

use yii\web\View;
use yii\helpers\Url;
use yii\helpers\Html;
use common\helpers\StringHelper;
use common\bundles\EnhancedDialogAsset;

$this->registerJsFile('@web/js/vis-utility.js', ['depends' => [\frontend\assets\AppAsset::className()]]);
$this->registerJsFile('@web/js/pages/production-path.js?' . uniqid(), ['depends' => [\frontend\assets\AppAsset::className()]]);

$this->title = Yii::t('web', 'Production path details') . ' - ' . Yii::$app->name;

$this->params['breadcrumbs'][] = ['label' => Yii::t('web', 'Production'), 'url' => Url::to(['production/index'])];
$this->params['breadcrumbs'][] = ['label' => Yii::t('web', 'List of production paths'), 'url' => Url::to(['production-path/list'])];
$this->params['breadcrumbs'][] = Yii::t('web', 'Production path details');
?>

<div class="production-path-details">
    <div class="row">
        <div class="col-sm-6"><h2 class="page-title"><?= StringHelper::translateOrderToOffer('web', 'Production path details') ?></h2></div>
        <div class="col-sm-6 text-right">
            <div>
                <?= Html::a(Yii::t('web', 'Edit'), ['production-path/edit', 'id' => $data['id']], ['class' => 'btn btn-default']) ?>
                <?=
                Html::a(Yii::t('main', 'Delete'), ['production-path/delete', 'id' => $data['id']], [
                    'class' => 'btn btn-default',
                    'data' => [
                        'confirm' => Yii::t('main', 'Are you sure you want to delete this item?'),
                        'method' => 'post',
                    ],
                ])
                ?>
            </div>
        </div>
    </div>

    <div class="col-xs-12 title-with-icon">
        <div class="base-icon basic-data-icon main-content-icons"></div>
        <h3><?= Yii::t('web', 'General information') ?></h3>
    </div>
    <div class="col-xs-12">
        <div class="row">
            <div class=" col-sm-8">
                <div class="table-responsive">
                    <table class="table table-bordered detail-table">
                        <tbody>
                            <tr>
                                <th><?= Yii::t('web', 'Name') ?></th>
                                <td><?= Html::encode($data['name']) ?></td>
                            </tr>
                            <tr>
                                <th><?= Yii::t('web', 'Number of nodes') ?></th>
                                <td><?= Html::encode(count(explode(',', $data['topologicalOrder']))) ?></td>
                            </tr>
                            <?php if($data['isDefault'] == 1) { ?>
                                <tr>
                                    <th><?= Yii::t('web', 'Is default (f)') ?></th>
                                    <td><?= Html::encode(StringHelper::boolTranslation($data['isDefault'], false)) ?></td>
                                </tr>
                            <?php } ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>

    <div class="col-xs-12 title-with-icon">
        <div class="base-icon gallery-icon main-content-icons"></div>
        <h3><?= Yii::t('web', 'Graph') ?></h3>
    </div>

    <div class="col-xs-12">
        <div id="production-path" class="col-sm-8"></div>
    </div>


    <?php
    EnhancedDialogAsset::register($this);
    ?>
</div>
<?php
$attributePath = Url::to(['production-path/get-attributes-for-production-path-steps']);
$pathStepPath = Url::to(['production-path/get-production-path-steps']);
$activityPath = Url::to(['activity/find']);
$savePath = Url::to(['production-path/save-step-path']);
$this->registerJs('init("' . Yii::$app->language . '", "' . $attributePath . '", "' . $pathStepPath . '", "' . $activityPath . '", "' . $savePath . '", "' . $id . '", "' . $id . '", "' . true . '");', View::POS_END);
?>