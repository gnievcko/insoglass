<?php
use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\View;

$this->title = Yii::t('web', 'List of production paths') . ' - ' . Yii::$app->name;

$this->params['breadcrumbs'][] = ['label' => Yii::t('web', 'Production'), 'url' => Url::to(['production/index'])];
$this->params['breadcrumbs'][] = Yii::t('web', 'List of production paths');

$this->registerJsFile('@web/js/dynamic-table.js?'.uniqid(), []);
?>

<div id="production-path-list-wrapper">
    <div class="row">
        <div class="col-xs-6"><h2 class="page-title"><?= Yii::t('web', 'List of production paths') ?></h2></div>
        <div class="col-xs-6 text-right">
    		<?= Html::a(Yii::t('main', 'Add production path'), ['production-path/create'], ['class' => 'btn btn-primary']) ?>
        </div>
	</div>

    <div id="production-paths-list-table" class="dynamic-table">
    	<h5><?php echo Yii::t('main','Filters')?></h5>
        <div class="row dynamic-table-filters">
            <div class="col-sm-3">
            	 <label><?= $listForm->getAttributeLabel('name') ?></label>
                    <input 
                        value="<?= $listForm->name ?>" 
                        class="form-control dynamic-table-filter" 
                        type="text"
                        placeholder="<?php echo Yii::t('web','Search by name')?>"
                        name="<?= Html::getInputName($listForm, 'name') ?>">
            </div>
        </div>
        <div class="dynamic-table-data">
            <?= Yii::$app->controller->renderPartial('_productionPaths', [
                'items' => $items,
                'pages' => $pages,
                'listForm' => $listForm,
            ]) ?>
        </div>
    </div>
    <?php 
    	$this->registerJs('dynamicTable("production-paths-list-table", "'.Url::to(['production-path/list']).'");', View::POS_END);
    ?>
</div>
