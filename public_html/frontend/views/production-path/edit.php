<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\View;

$this->registerJsFile('@web/js/vis-utility.js', ['depends' => [\frontend\assets\AppAsset::className()]]);
$this->registerJsFile('@web/js/pages/production-path.js?' . uniqid(), ['depends' => [\frontend\assets\AppAsset::className()]]);

$this->title = Yii::t('web', 'Edit production path') . ' - ' . Yii::$app->name;
$this->params['breadcrumbs'][] = ['label' => Yii::t('web', 'Production'), 'url' => Url::to(['production/index'])];
$this->params['breadcrumbs'][] = ['label' => Yii::t('web', 'List of production paths'), 'url' => Url::to(['production-path/list'])];
$this->params['breadcrumbs'][] = Yii::t('web', 'Edit production path');
?>

<?= Yii::$app->controller->renderPartial('_path', []); ?>

<?php
$attributePath = Url::to(['production-path/get-attributes-for-production-path-steps']);
$pathStepPath = Url::to(['production-path/get-production-path-steps']);
$activityPath = Url::to(['activity/find']);
$savePath = Url::to(['production-path/save-step-path']);
$this->registerJs('init("' . Yii::$app->language . '", "' . $attributePath . '", "' . $pathStepPath . '", "' . $activityPath . '", "' . $savePath . '", "' . $id . '");', View::POS_END);
?>
