<?php

use yii\helpers\Url;
use yii\helpers\Html;
use frontend\models\ProductionPathListForm;
use yii\widgets\LinkPager;
use common\bundles\EnhancedDialogAsset;
use common\widgets\TableHeaderWidget;
use common\helpers\StringHelper;
?>
<?php if(empty($items)) { ?>
    <label><?= Yii::t('web', 'No search results'); ?></label>
    <br><br>				
<?php } else {
    ?>
    <div class="table-responsive">
        <table class="<?= TableHeaderWidget::TABLECLASS ?>">
            <?php echo TableHeaderWidget::widget(['fields' => ProductionPathListForm::getSortFields(), 'listForm' => $listForm]); ?>
            <tbody>
                <?php foreach($items as $item): ?>
                    <tr>
                        <td><?= Html::encode($item['name']) ?></td>
                        <td><?= Html::encode(count(explode(',', $item['topologicalOrder']))) ?></td>
                        <td><?php
                            echo Html::a('', Url::to(['production-path/details', 'id' => $item['id']]), ['class' => 'base-icon details-icon action-icon', 'title' => Yii::t('web', 'Details')]);
                            echo '&nbsp;' . Html::a('', Url::to(['production-path/edit', 'id' => $item['id']]), ['class' => 'base-icon editing-icon action-icon', 'title' => Yii::t('web', 'Edit')]);
                            echo '&nbsp;' . Html::a('', Url::to(['production-path/delete', 'id' => $item['id']]), [
                                    'class' => 'base-icon delete-icon action-icon',
                                    'title' => Yii::t('main', 'Delete'),
                                    'data' => [
                                        'confirm' => Yii::t('main', 'Are you sure you want to delete this item?'),
                                        'method' => 'post',
                                        'params' => [
                                            'id' => $item['id'],
                                        ],
                                    ],
                                ]);
                            ?></td>
                    </tr>
                <?php endforeach ?>
            </tbody>
        </table>
    </div>
    <div class="col-sm-12">
        <div class="col-sm-4 text-left dynamic-table-counter">
            <?= Yii::t('web', 'Number of found items: {n}', ['n' => $pages->totalCount]) ?>
        </div>
        <div class="col-sm-8 text-right dynamic-table-pagination">
            <?= LinkPager::widget(['pagination' => $pages]) ?>
        </div>
    </div>
<?php } ?>

<input class="dynamic-table-sort-dir" type="hidden" name="<?= Html::getInputName($listForm, 'sortDir') ?>" value="<?= $listForm->sortDir ?>" />
<input class="dynamic-table-sort-field" type="hidden" name="<?= Html::getInputName($listForm, 'sortField') ?>" value="<?= $listForm->sortField ?>" />
<input class="dynamic-table-page" type="hidden" name="<?= Html::getInputName($listForm, 'page') ?>" value="<?= $listForm->page ?>" />

<?php
EnhancedDialogAsset::register($this);
?>


