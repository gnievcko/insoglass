<?php
use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\View;
use common\bundles\EnhancedDialogAsset;
use common\helpers\StringHelper;

$this->title = Yii::t('web', 'List of clients groups') . ' - ' . Yii::$app->name;

$this->params['breadcrumbs'][] = ['label' => StringHelper::translateOrderToOffer('main', 'Orders'), 'url' => Url::to(['order/index'])];
$this->params['breadcrumbs'][] = Yii::t('web', 'List of clients groups');

$this->registerJsFile('@web/js/dynamic-table.js?'.uniqid(), []);
?>

<div id="client-list-wrapper">    
    <div class="row">
		<div class="col-sm-6 col-xs-9"><h2 class="page-title"><?= Yii::t('web', 'List of clients groups') ?></h2></div>
		<div class="col-sm-6 col-xs-3"><?= Html::a(Yii::t('web', 'Add group'), ['client-group/create'], ['class' => 'btn btn-primary pull-right btn-md']) ?>
		</div>
	</div>

    <div id="clients-groups-list-table" class="dynamic-table">
    	<h5><?php echo Yii::t('main','Filters')?></h5>
        <div class="row dynamic-table-filters">
            <div class="col-md-3 col-sm-6">
               <label><?php echo Yii::t('web','Search by name')?></label>
                    <input 
                        value="<?= $clientsGroupsForm->name ?>" 
                        class="form-control dynamic-table-filter" 
                        type="text"
                        placeholder="<?= $clientsGroupsForm->getAttributeLabel('name') ?>"
                        name="<?= Html::getInputName($clientsGroupsForm, 'name') ?>">
                
            </div>
            <?= Yii::$app->controller->renderPartial('/inc/listFiltersButtons', ['isMoreFilters' => false, 'isClearFilters' => true]) ?> 
        </div>
        <div class="dynamic-table-data">
            <?= Yii::$app->controller->renderPartial('_groups', [
	                'groups' => $groups,
	                'pages' => $pages,
	                'clientsGroupsForm' => $clientsGroupsForm,
            ]) ?>
        </div>
    </div>
    <?php $this->registerJs('dynamicTable("clients-groups-list-table", "'.Url::to(['client-group/list']).'");', View::POS_END); ?>
</div>

<?php 
	EnhancedDialogAsset::register($this);
?>
