<?php
use yii\helpers\Html;
use yii\helpers\Url;
use common\bundles\EnhancedDialogAsset;
use common\helpers\StringHelper;

$this->title = Yii::t('web', 'Clients group details') . ' - ' . Yii::$app->name;

$this->params['breadcrumbs'][] = ['label' => StringHelper::translateOrderToOffer('main', 'Orders'), 'url' => Url::to(['order/index'])];
$this->params['breadcrumbs'][] = ['label' => Yii::t('web', 'List of clients groups'), 'url' => Url::to(['client-group/list'])];
$this->params['breadcrumbs'][] = Yii::t('web', 'Clients group details');
?>

<div class="client-group-details">
	<div class="row">
		<div class="col-sm-6"><h2 class="page-title"><?= Yii::t('web', 'Clients group details') ?></h2></div>
		<div class="col-sm-6">
			<div class="pull-right"><?php
				$buttons = null;
				$referrer = Yii::$app->request->getReferrer();
				if (substr($referrer, strripos($referrer, '/') + 1) == 'create') {
					$buttons .= Html::a('<span class="glyphicon glyphicon-plus"></span>&nbsp;'.Yii::t('web', 'Add another clients group'),
										['client-group/create'], ['class' => 'btn btn-primary']);
				}
				$buttons .= Html::a(Yii::t('main', 'Update'),
						['client-group/edit', 'id' => $groupDetails['id']], ['class' => 'btn btn-default'])
				.Html::a(Yii::t('main', 'Delete'),
						['client-group/delete'],
						[
								'class' => 'btn btn-default',
								'data' => [
						                'confirm' => Yii::t('main', 'Are you sure you want to delete this item?'),
						                'method' => 'post',
										'params' => [
												'id' => $groupDetails['id'],
										],
					            ],
						]);
				echo $buttons;
			?></div>
		</div>
	</div>

	<div class="table-responsive">
		<table class="table table-bordered detail-table">
	    	<tbody>
	    		<tr>
	    			<th><?= Yii::t('web', 'Name') ?></th>
	    			<td><?= Html::encode($groupDetails['name']) ?></td>
	    		</tr>
    			<tr>
    				<th><?= Yii::t('web', 'Clients') ?></th>
                    <td><?php if(!empty($clients)) { ?>
                    	<div class="clients-row">
                        	<div class="client-row clearfix">
                            	<?php foreach($clients as $client) { ?>
                                	<div class="pull-left" style="margin-right: 10px">
                                    	<span class="name"><?= Html::a(Html::encode($client['name']) ,
												['client/details', 'id' => $client['id']]);
                                    	?></span>
                                	</div>
                            	<?php } ?>
                        	</div>
                    	</div>
                	<?php } ?></td>
		    	</tr>
	    	</tbody>
		</table>
	</div>

</div>

<?php
	EnhancedDialogAsset::register($this);
?>
