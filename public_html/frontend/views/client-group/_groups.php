<?php 
use yii\helpers\Url;
use yii\helpers\Html;
use yii\widgets\LinkPager;
use common\helpers\Utility;
use frontend\models\ClientsGroupsListForm;
use common\widgets\TableHeaderWidget;
?>

<?php $actionsColumnsVisible = \Yii::$app->user->can(Utility::ROLE_MAIN_SALESMAN); ?>

<?php 
if (empty($groups)) { ?>
	<label><?= Yii::t('web', 'No search results');?></label>
	<br><br>				
<?php }
else { ?>
<div class=" table-responsive">
	<table class="<?= TableHeaderWidget::TABLECLASS ?>">
	    <?php echo TableHeaderWidget::widget(['fields' => ClientsGroupsListForm::getSortFields() , 'listForm' => $clientsGroupsForm]);?>
        <tbody>
            <?php foreach($groups as $group): ?>
                <tr>
                    <td><?= Html::encode($group['name']) ?></td>                    
                    <?php if($actionsColumnsVisible) { ?>
                        
                        <td><?php
	                    	echo Html::a('', Url::to(['client-group/details', 'id' => $group['id']]), ['class' => 'base-icon details-icon action-icon', 'title' => Yii::t('web', 'Details')]);
	                    	echo '&nbsp;'.Html::a('', Url::to(['client-group/edit', 'id' => $group['id']]), ['class' => 'base-icon editing-icon action-icon', 'title' => Yii::t('web', 'Edit')]);
	                    	echo '&nbsp;'.Html::a('', Url::to(['client-group/delete']), [
									'class' => 'base-icon delete-icon action-icon',
                                    'title' => Yii::t('main', 'Delete'),
									'data' => [
							                'confirm' => Yii::t('main', 'Are you sure you want to delete this item?'),
							                'method' => 'post',
											'params' => [
													'id' => $group['id'],
											],
						            ],
							]);
	                    ?>
	                    </td>
                    <?php } ?>
                </tr>
            <?php endforeach ?>
        </tbody>
    </table>
</div>
<div class="col-sm-12">
	<div class="col-sm-4 text-left dynamic-table-counter">
		<?= Yii::t('web', 'Number of found items: {n}', ['n' => $pages->totalCount]) ?>
	</div>
	<div class="col-sm-8 text-right dynamic-table-pagination">
	    <?= LinkPager::widget(['pagination' => $pages ]) ?>
	</div>
</div>
<?php }?>

<input class="dynamic-table-sort-dir" type="hidden" name="<?= Html::getInputName($clientsGroupsForm, 'sortDir') ?>" value="<?= $clientsGroupsForm->sortDir ?>" />
<input class="dynamic-table-sort-field" type="hidden" name="<?= Html::getInputName($clientsGroupsForm, 'sortField') ?>" value="<?= $clientsGroupsForm->sortField ?>" />
<input class="dynamic-table-page" type="hidden" name="<?= Html::getInputName($clientsGroupsForm, 'page') ?>" value="<?= $clientsGroupsForm->page ?>" />
