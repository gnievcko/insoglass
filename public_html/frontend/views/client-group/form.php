<?php
use yii\helpers\Url;
use yii\helpers\Html;
use common\components\ActiveForm;
use kartik\widgets\Select2;
use yii\web\JsExpression;
use common\helpers\StringHelper;

$this->registerCssFile('@web/css/select2.min.css');
$this->registerCssFile('@web/css/select2-bootstrap.min.css');
$this->registerJsFile('@web/js/select2/select2.min.js?'.uniqid(), ['depends' => [\yii\web\JqueryAsset::className()]]);
$this->registerJsFile('@web/js/select2/i18n/'.Yii::$app->language.'.js?'.uniqid(), ['depends' => [\yii\web\JqueryAsset::className()]]);

$subtitle = $model->isEditScenarioSet() ? Yii::t('web', 'Update client data') : Yii::t('web', 'Add clients group');
$this->title = $subtitle.' - '.Yii::$app->name;

$this->params['breadcrumbs'][] = ['label' => StringHelper::translateOrderToOffer('main', 'Orders'), 'url' => Url::to(['order/index'])];
$this->params['breadcrumbs'][] = ['label' => Yii::t('web', 'List of clients groups'), 'url' => Url::to(['client-group/list'])];
$this->params['breadcrumbs'][] = $subtitle;
?>

<div class="client-form">
	<h2 class="page-title"><?= $subtitle ?></h2>

	<?php $form = ActiveForm::begin([
			'id' => 'client-form-ajax',
			'enableAjaxValidation' => true,
	]); ?>

	<?php if($model->isEditScenarioSet()) {
		$form->field($model, 'id')->hiddenInput()->label(false);
	} ?>

	<div class="col-xs-12">
		<div class="row">
			<div class="col-sm-6"><?= $form->field($model, 'name')->textInput(['maxlength' => true, 'placeholder'=>Yii::t('main','Client group name')]) ?></div>
		</div>
	</div>
	<div class="col-xs-12">
		<div class="row">
	        <div class="col-sm-6">
	            <?php echo $form->field($model, 'clientIds', ['enableAjaxValidation' => false])
	                    ->widget(Select2::className(), [
	                            'language' => Yii::$app->language,
	                            'name' => 'select2_group-clients',
	                            'options' => ['placeholder' => Yii::t('web', 'Select client...'), 'multiple' => true],
	                            'showToggleAll' => false,
	                            'data' => $clientsData,
	                            'pluginOptions' => [
	                                    'allowClear' => false,
	                                    'minimumInputLength' => Yii::$app->params['isClientListImmediatelyVisible'] ? 0 : 1,
	                                    'tags' => false,
	                                    'ajax' => [
	                                            'url' => Url::to(['company/find']),
	                                            'dataType' => 'json',
	                                            'delay' => 250,
	                                            'cache' => true,
	                                            'data' => new JsExpression('function(params) { return {term:params.term, page:params.page, size: 20}; }'),
	                                            'processResults' => new JsExpression('function (data, params) {
	                                                params.page = params.page || 1;
	                                                return {
	                                                    results: data.items,
	                                                    pagination: {
	                                                        more: data.more
	                                                    }
	                                                };
	                                            }')
	                                    ],
	                            		'escapeMarkup' => new JsExpression('function (markup) { return markup; }'),
	                                    //'templateResult' => new JsExpression('function(item) { if(item.loading) return item.text; return item.name; }'),
	                            		'templateResult' => new JsExpression('function(item) { if(item.loading) return item.text; if(item.parent_company_id) return "<span class=\"object-highlight\">"+item.name+"</span>"; else return item.name; }'),
	                                    'templateSelection' => new JsExpression('function (item) { return item.name || item.text; }'),
	                            ],
	                    ]
	            ) ?>
                <a href="<?= Url::to(['client/create']); ?>" target="_blank" class="focusable-link  add-item-after-select2  add-item-after-select2-no-required">
                    <span class="glyphicon glyphicon-plus-sign pull-left"></span>
                    <span> <?=Yii::t('web', 'Create new client')?></span>
                </a>
	        </div>
		</div>
	</div>
	<div class="col-xs-12">
		<div class="pull-left">
			<?= Html::a(Yii::t('web', 'Cancel'),
 				!empty(Yii::$app->request->getReferrer()) ? Yii::$app->request->getReferrer() : ['client-group/list'],
				['class' => 'btn btn-default']) ?>
		</div>
		<div class="pull-right">
			<?= Html::submitButton(($model->isEditScenarioSet() ? Yii::t('main', 'Save') : Yii::t('web', 'Add')), ['class' => 'btn btn-primary']); ?>
		</div>
	</div>
</div>
<div>
	<?php ActiveForm::end(); ?>
</div>
