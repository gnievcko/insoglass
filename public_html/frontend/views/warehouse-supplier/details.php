<?php
use yii\helpers\Html;
use yii\helpers\Url;
use common\helpers\StringHelper;
use common\bundles\EnhancedDialogAsset;
use frontend\helpers\AddressHelper;

$this->title = Yii::t('web', 'Supplier details') . ' - ' . Yii::$app->name;

$this->params['breadcrumbs'][] = ['label' => Yii::t('main', 'Warehouse'), 'url' => Url::to(['warehouse/list'])];
$this->params['breadcrumbs'][] = ['label' => Yii::t('web', 'List of suppliers'), 'url' => Url::to(['warehouse-supplier/list'])];
$this->params['breadcrumbs'][] = Yii::t('web', 'Supplier');
?>

<div class="warehouse-supplier">
	<div class="row">
		<div class="col-sm-6"><h2 class="page-title"><?= Yii::t('web', 'Supplier details') ?></h2></div>
		<div class="col-sm-6 text-right">
			<?php
				$buttons = '';
				$referrer = Yii::$app->request->getReferrer();
				if (substr($referrer, strripos($referrer, '/') + 1) == 'create') {
					$buttons .= Html::a('<span class="glyphicon glyphicon-plus"></span>&nbsp;'.Yii::t('web', 'Add another supplier'),
							['warehouse-supplier/create'], ['class' => 'btn btn-primary']);
				}
				$buttons .= Html::a(Yii::t('main', 'Update'),
						['warehouse-supplier/edit', 'id' => $data['id']], ['class' => 'btn btn-default']);
				$buttons .= Html::a(Yii::t('main', 'Delete'),
						Url::to(['warehouse-supplier/delete']),
						[
								'class' => 'btn btn-default',
								'data' => [
						                'confirm' => Yii::t('main', 'Are you sure you want to delete this item?'),
						                'method' => 'post',
										'params' => [
												'id' => $data['id'],
										],
					            ],
						]);
				echo $buttons;
			?>
		</div>
	</div>

	<div class="table-responsive col-sm-12">
		<h3 class="page-subtitle"><?= Yii::t('web', 'General information') ?></h3>
		<table class="table table-bordered detail-table">
	    	<tbody>
	    		<tr>
	    			<th><?= Yii::t('web', 'Name') ?></th>
	    			<td><?= $data['name'] ?></td>
	    		</tr>
	    		<tr>
	    			<th><?= Yii::t('web', 'Contact person') ?></th>
	    			<td><?= $data['contactPerson'] ?></td>
	    		</tr>
	    		<tr>
	    			<th> <?= Yii::t('main', 'Phone') ?> </th>
	    			<td><?= Html::encode($data['phone']) ?></td>
	    		</tr>
	    		<tr>
	    			<th> <?= Yii::t('main', 'E-mail address') ?> </th>
	    			<td><?= Html::mailto(Html::encode($data['email'])) ?></td>
	    		</tr>
	    		<tr>
	    			<th> <?= Yii::t('main', 'Address') ?> </th>
	    			<td><?= Html::encode(AddressHelper::getFullAddress([
		                    		'addressMain' => $data['addressMain'],
		                    		'addressComplement' => $data['addressComplement'],
		                    		'cityName' => $data['cityName'],
		                    		'cityZipCode' => $data['cityZipCode'],
		                    		'provinceName' => $data['provinceName'],
		                    		'countryName' => $data['countryName']
		                    ])) 
	    			?></td>
	    		</tr>
	    		<tr>
	    			<th><?= Yii::t('main', 'Creation date') ?></th>
	    			<td><?= Html::encode(StringHelper::getFormattedDateFromDate($data['date_creation'], true)) ?></td>
	    		</tr>
	    	</tbody>
		</table>
	</div>

</div>
<?php
	EnhancedDialogAsset::register($this);
?>
