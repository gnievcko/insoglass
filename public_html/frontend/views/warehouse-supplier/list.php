<?php
use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\View;

$this->title = Yii::t('web', 'List of suppliers') . ' - ' . Yii::$app->name;

$this->params['breadcrumbs'][] = ['label' => Yii::t('main', 'Warehouse'), 'url' => Url::to(['warehouse/index'])];
$this->params['breadcrumbs'][] = Yii::t('web', 'List of suppliers');

$this->registerJsFile('@web/js/dynamic-table.js?'.uniqid(), []);
?>

<div id="warehouse-suppliers-list-wrapper">    
    <div class="row">
		<div class="col-sm-6 col-xs-9"><h2 class="page-title"><?= Yii::t('web', 'List of suppliers') ?></h2></div>
		<div class="col-sm-6 col-xs-3 text-right"><?= Html::a(Yii::t('web', 'Add supplier'), ['warehouse-supplier/create'], ['class' => 'btn btn-primary']) ?>
		</div>
	</div>

    <div id="warehouse-supplier-list-table" class="dynamic-table">
    <h5><?php echo Yii::t('main','Filters')?></h5>
        <div class="row dynamic-table-filters">
            <div class="col-md-3 col-sm-4">
             	<label><?php echo Yii::t('web','Search by name')?></label>
                    <input 
                        value="<?= $listForm->nameFilterString ?>" 
                        class="form-control dynamic-table-filter" 
                        type="text"
                        placeHolder="<?= $listForm->getAttributeLabel('Name') ?>"
                        name="<?= Html::getInputName($listForm, 'nameFilterString') ?>">
            </div> 
            <?= Yii::$app->controller->renderPartial('/inc/listFiltersButtons', ['isMoreFilters' => false, 'isClearFilters' => true]) ?>
        </div>
        <div class="dynamic-table-data">
            <?= Yii::$app->controller->renderPartial('_warehouseSuppliers', [
	                'items' => $items,
	                'pages' => $pages,
	                'listForm' => $listForm,
            ]) ?>
        </div>
    </div>
    <?php $this->registerJs('dynamicTable("warehouse-supplier-list-table", "'.Url::to(['warehouse-supplier/list']).'");', View::POS_END); ?>
</div>