<?php
use yii\helpers\Url;
use yii\helpers\Html;
use common\components\ActiveForm;
use yii\web\View;
use common\models\ar\Warehouse;

$this->registerCssFile('@web/css/select2.min.css');
$this->registerCssFile('@web/css/select2-bootstrap.min.css');
$this->registerJsFile('@web/js/select2/select2.min.js?'.uniqid(), ['depends' => [\yii\web\JqueryAsset::className()]]);
$this->registerJsFile('@web/js/select2/i18n/'.Yii::$app->language.'.js?'.uniqid(), ['depends' => [\yii\web\JqueryAsset::className()]]);
$this->registerJsFile('@web/js/province-by-zip-code.js?'.uniqid(), ['depends' => [\yii\web\JqueryAsset::className()]]);

$subtitle = $model->isEditScenarioSet() ? Yii::t('web', 'Update supplier') : Yii::t('web', 'Add supplier');
$this->title = $subtitle.' - '.Yii::$app->name;

$this->params['breadcrumbs'][] = ['label' => Yii::t('main', 'Warehouse'), 'url' => Url::to(['warehouse/index'])];
$this->params['breadcrumbs'][] = ['label' => Yii::t('web', 'List of suppliers'), 'url' => Url::to(['warehouse-supplier/list'])];
$this->params['breadcrumbs'][] = $subtitle;
?>

<div class="warehouse-supplier-form">
	<h2 class="page-title"><?= $subtitle ?></h2>
	<?php $form = ActiveForm::begin([
			'id' => 'warehouse-supplier-form-ajax',
			'enableAjaxValidation' => true,
	]); ?>
	
	<?php if($model->isEditScenarioSet()) {
		$form->field($model, 'id')->hiddenInput()->label(false);
	} ?>
	<div class="col-xs-12">
		<div class="row">
			<div class="col-sm-6"><?= $form->field($model, 'name')->textarea(['class' => 'form-control smallTextarea', 'maxlength' => true, 'placeHolder' => $model->getAttributeLabel('name')]) ?></div>
			<div class="col-sm-6"><?= $form->field($model, 'contactPerson')->textInput(['maxLength' => true, 'placeHolder' => $model->getAttributeLabel('contactPerson')]) ?></div>
		</div>
	</div>
	<div class="col-xs-12">
		<div class="row">
			<div class="col-sm-6"><?= $form->field($model, 'email')->textInput(['maxlength' => true, 'placeholder' => Yii::t('main','E-mail address')]) ?></div>
			<div class="col-sm-6"><?= $form->field($model, 'phone')->textInput(['maxLength' => true, 'placeholder' => $model->getAttributeLabel('phone'), 'class' => 'form-control']) ?></div>
		</div>
	</div>
	<div class="col-xs-12">
		<h4><?= Yii::t('main', 'Address') ?></h4>
		<div class="row">
			<div class="col-md-4 col-sm-8"><?= $form->field($model, 'address')->textInput(['maxlength' => true, 'placeholder' => $model->getAttributeLabel('address')]) ?></div>
			<div class="col-md-2 col-sm-4"><?= $form->field($model, 'zipCode')->textInput(['maxlength' => true, 'placeholder' => $model->getAttributeLabel('zipCode')]) ?></div>
			<div class="col-md-3 col-sm-6"><?= $form->field($model, 'city')->textInput(['maxlength' => true, 'placeholder' => $model->getAttributeLabel('city')]) ?></div>
			<div class="col-md-3 col-sm-6"><?= $form->field($model, 'provinceId')->dropDownList($model->provinces, ['prompt' => Yii::t('main','Choose Voivodeship')]) ?></div>
		</div>
	</div>
	<div class="col-xs-12"><?php if($model->isEditScenarioSet()){
			//echo $form->field($model, 'dateCreation')->textInput(['readonly' => true]);
		} ?>
	</div>
	<div class="col-xs-12">
		<div class="pull-left"><?= Html::a(Yii::t('web', 'Cancel'), 
									!empty(Yii::$app->request->getReferrer()) ? Yii::$app->request->getReferrer() : ['warehouse-supplier/list'], 
									['class' => 'btn btn-default']) ?>
		</div>
		<div class="pull-right"><?= Html::submitButton(($model->isEditScenarioSet() ? Yii::t('main', 'Save') : Yii::t('web', 'Add')), ['class' => 'btn btn-primary']); ?></div>
	</div>
	
	<?php ActiveForm::end(); ?>	
	<?php $this->registerJs('fillProvince("warehousesupplierform-zipcode", "warehousesupplierform-provinceid", "' . Url::to(['employee/get-province-by-zip-code']) . '");', View::POS_END); ?>
</div>
