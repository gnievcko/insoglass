<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace frontend\helpers;
use yii\mongodb\Query;
use Yii;
/**
 * Description of DocumentHelper
 *
 * @author Bartek
 */
class DocumentHelper {
    
    static public function getDocuments($id, $type) {
        if(isset(Yii::$app->params['isDocumentStorageAvailable']) && Yii::$app->params['isDocumentStorageAvailable']) {
            $order = DocumentHelper::getOrder($id);
            
            $where = [
                'history.sections.companyId' => (int)$order->company_id,
                'type' => $type,
            ];
            
            $query = new Query();

            $query->select([])
                    ->from('documents')
                    ->orderBy(['history.createdAt' => SORT_DESC])
                    ->where($where);
			
            $rows = $query->all();
            
            return $rows;
        } else {
            return [];           
        }
    }

    static public function getOrder($id = null) {
        if(isset($id)) {
            $order = \common\models\ar\Order::findOne(['id' => $id]);
            return $order;
        }
        return null;
    }
}
