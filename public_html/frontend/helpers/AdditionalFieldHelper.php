<?php
namespace frontend\helpers;

use Yii;
use kartik\widgets\Select2;
use yii\helpers\Url;
use yii\web\JsExpression;
use common\helpers\StringHelper;
use yii\helpers\Html;
use common\models\ar\Product;
use common\helpers\Utility;

class AdditionalFieldHelper {
	
	public static function renderAdditionalFields($model, $form, $fields, $typeKeyname, $initialValues = []) {
		if(empty($fields)) {
			return;
		}
		
		foreach($fields as $field) {
			if(!isset($field['fieldType'])) {
				$field['fieldType'] = Utility::ADDITIONAL_FIELD_TYPE_SELECT2;
			}
			
			if($field['fieldType'] == Utility::ADDITIONAL_FIELD_TYPE_SELECT2) {			
				$urlData = self::getUrlData($field);
				if(empty($urlData)) {
					continue;
				}
				
				$id = 'select2-'.$field[$typeKeyname].'-'.$field['id'].'-'.$field['symbol'];
				$initialName = null;
				if(isset($initialValues[$field['id']])) {
					$initialName = [$initialValues[$field['id']]['id'] => $initialValues[$field['id']]['name']];
				}				
				
				echo '<div id="'.$id.'" class="additional-field-box" style="display: none">';
				echo $form->field($model, 'fields['.$field['id'].']')
						->widget(Select2::classname(), [
								'language' => Yii::$app->language,
								'options' => ['placeholder' => $urlData['placeholder']],
								'data' => $initialName,
								'pluginOptions' => [
										'allowClear' => true,
										'minimumInputLength' => 1,
										'ajax' => [
												'url' => Url::to([$urlData['url']]),
												'dataType' => 'json',
												'delay' => 250,
												'data' => new JsExpression('function(params) { return { term:params.term, page:params.page, size: 20 }; }'),
												'processResults' => new JsExpression('function (data, params) {'.
															$urlData['processResultsBody']
														.'}'),
												'cache' => true
										],
										'templateResult' => new JsExpression('function(item) { if(item.loading) return item.text; return item.name; }'),
										'templateSelection' => new JsExpression('function (item) { return item.name || item.text; }'),
								]
						])->label($urlData['label']);
				echo '</div>';
			}
			elseif($field['fieldType'] == Utility::ADDITIONAL_FIELD_TYPE_TEXT_INPUT) {
				$id = 'text-input-'.$field[$typeKeyname].'-'.$field['id'].'-'.$field['symbol'];
				echo '<div id="'.$id.'" class="additional-field-box" style="display: none">';
				echo $form->field($model, 'fields['.$field['id'].']')
						->textInput(['maxLength' => 1024])->label($field['name']);
				echo '</div>';
			}
		}
	}
	
	private static function getUrlData($field) {
		$defaultProcessResults = '
				params.page = params.page || 1;
		
				return {
					results: data.items,
					pagination: {
						more: data.more
					}
				};
		';
		$defaultLabel = $field['name'];
		
		switch($field['nameTable']) {
			case 'order':
				return [
						'url' => 'order/find',
						'label' => StringHelper::translateOrderToOffer('main', 'Order'),
						'placeholder' => StringHelper::translateOrderToOffer('main', 'Select an order...'), 
						'processResultsBody' => $defaultProcessResults,
				];
				break;
				
			case 'user':
				return [
						'url' => 'employee/find', 
						'label' => $defaultLabel,
						'placeholder' => Yii::t('main', 'Select an employee...'), 
						'processResultsBody' => '
								params.page = params.page || 1;
								var results = data.items.map(function(item) { return { id : item.id, name : (item.firstName != null && item.lastName != null) ? (item.firstName + " " + item.lastName) : item.email }; });	
													
								return {
									results: results,
										pagination: {
											more: data.more
										}
								};
						',
				];
				break;
			
			case 'company':
				return [
						'url' => 'company/find',
						'label' => $defaultLabel,
						'placeholder' => Yii::t('main', 'Select a client...'),
						'processResultsBody' => $defaultProcessResults,
				];
				break;
				
			case 'product':
				return [
						'url' => 'product/find',
						'label' => $defaultLabel,
						'placeholder' => Yii::t('main', 'Select a product...'),
						'processResultsBody' => $defaultProcessResults,
				];
				break;
				
			default:
				return [];
		}
	}
	
	public static function validateAdditionalFields($fields, $statusFields) {
		if(empty($statusFields) || empty($fields)) {
			return [];
		}
		
		$errors = [];
		foreach($statusFields as $statusField) {
			if(!empty($statusField['isRequired']) && empty($fields[$statusField['id']])) {
				$error = [];
				$error['id'] = $statusField['id'];
				$error['message'] = Yii::t('web', 'The field {name} must contain value.', ['name' => '"'.$statusField['name'].'"']);
				$errors[] = $error;
			}
		}
		
		return $errors;
	}
	
	public static function getFormattedData($data, $withoutLabel = false) {
		$html = '';
		
		foreach($data as $row) {
			$html .= (!empty($html)) ? '<br/>' : '';
			
			$text = $row['name'];
			$isLink = !empty($row['url']);
			
			if(!empty($row['table'])) {
				if($row['table'] == 'user') {
					$piece = explode('|', $row['name']);
					$text = UserHelper::getPrettyUserName($piece[2], $piece[0], $piece[1]);
				}
				elseif($row['table'] == 'product') {
					$product = Product::findOne($row['id']);
					$productTranslation = $product->getTranslation();
					if(!empty($productTranslation)) {
						$text = $productTranslation->name;
					}
					
					$isLink = $isLink && !empty($product->is_active);
				}
			}
			
			if(!$withoutLabel) {
				$html .= $row['label'].': ';
			}
			
			if($isLink) {
				$url = str_replace('/{id}', '', $row['url']);
				$html .= Html::a($text, Url::to([$url, 'id' => $row['id']]), ['target' => '_blank']);
			}
			else {
				$html .= $text;
			}
		}
		
		return $html;
	}
    
    public static function getMaxFileSize() {
        $value = ini_get('upload_max_filesize');
        if ( is_numeric( $value ) ) {
            return $value;
        } else {
            $value_length = strlen($value);
            $qty = substr( $value, 0, $value_length - 1 );
            $unit = strtolower( substr( $value, $value_length - 1 ) );
            switch ( $unit ) {
                case 'k':
                    $qty *= 1024;
                    break;
                case 'm':
                    $qty *= 1048576;
                    break;
                case 'g':
                    $qty *= 1073741824;
                    break;
            }
            return $qty;
        }
    }
}
