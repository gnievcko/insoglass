<?php
namespace frontend\helpers;

use common\models\ar\City;
use common\models\ar\Address;

class AddressHelper {
	
	public static function getFullAddress($params) {
		if(empty($params['addressMain'])) {
			return '';
		}
		
		return $params['addressMain']
				.(!empty($params['addressComplement']) ? ' '.$params['addressComplement'] : '')
				.', '.$params['cityZipCode'].' '.$params['cityName']
				.(!empty($params['provinceName']) ? ', '.$params['provinceName'] : '')
				.(!empty($params['countryName']) ? ', '.$params['countryName'] : '');
	}
	
	public static function getAddressId($params) {
		if(empty($params) || (empty($params['address']))) {
			return null;
		}
	
		$addressQuery = Address::find()->alias('a')->joinWith('city c')
				->where('a.main = :addressMain AND c.name = :cityName AND c.zip_code = :zipCode', [
						':addressMain' => $params['address'],
						':cityName' => $params['city'],
						':zipCode' => $params['zipCode'],
				]);
	
		if(!empty($params['provinceId'])) {
			$addressQuery->andWhere('c.province_id = :provinceId', [':provinceId' => $params['provinceId']]);
		}
	
		$address = $addressQuery->one();
	
		if(!empty($address)) {
			return $address->id;
		}
	
		// TODO: Usuwanie starych adresów?
		$city = new City();
		$city->province_id = $params['provinceId'];
		$city->name = $params['city'];
		$city->zip_code = $params['zipCode'];
		$city->save();
	
		$address = new Address();
		$address->city_id = $city->id;
		$address->main = $params['address'];
		$address->save();
	
		return $address->id;
	}
	
}