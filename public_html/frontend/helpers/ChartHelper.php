<?php
namespace frontend\helpers;

use Yii;

class ChartHelper{

	const COLORS = array(
			array('background' => '#3F76BA', 'border'=> '#000'),
			array('background' => '#14253A', 'border'=> '#000'),
			array('background' => '#294E7A', 'border'=> '#000'),
			array('background' => '#437EC7', 'border'=> '#000'),
			array('background' => '#3666A0', 'border'=> '#000'),
			array('background' => '#555', 'border'=> '#000'),
			array('background' => '#666', 'border'=> '#000'),
			array('background' => '#777', 'border'=> '#000'),
			array('background' => '#888', 'border'=> '#000'),
			array('background' => '#999', 'border'=> '#000'),
			array('background' => '#AAA', 'border'=> '#000'),
			array('background' => '#BBB', 'border'=> '#000'),
			array('background' => '#CCC', 'border'=> '#000'),
			array('border'=> '#000', 'border'=> '#000'),
			array('border'=> '#000', 'border'=> '#000'),
			array('border'=> '#000', 'border'=> '#000'),
			array('border'=> '#000', 'border'=> '#000'),
			array('border'=> '#000', 'border'=> '#000'),
			array('border'=> '#000', 'border'=> '#000'),
			array('border'=> '#000', 'border'=> '#000'),
	);


	public static function generateLegend($data){
		$string = '';
		$string .= '<div class="col-xs-6 legend">';
		$i = 0;
		foreach($data as $row){
			$string .= '<div class="item"><div class="box" style="border-color:'.ChartHelper::COLORS[$i]['border'].';background-color:'.ChartHelper::COLORS[$i]['background'].'"></div>'.$row.'</div>';
			$i++;
		}

		$string.='</div>';
		return $string;
	}

	public static function generateChanger($type = 1, $load, $div, $chartId){
		$string = '';
		$string .= '
				<div class="col-xs-6 time">
						<div data-type="1" data-url="'.$load.'" class="changePeriod '.($type == 1 ? 'select':'').'" >'.Yii::t('web','year').'</div>
						<div data-type="2" data-url="'.$load.'" class="changePeriod '.($type == 2 ? 'select':'').'" >'.Yii::t('web','month').'</div>
						<div data-type="3" data-url="'.$load.'" class="changePeriod '.($type == 3 ? 'select':'').'" >'.Yii::t('web','week').'</div>
				</div>
		';
		return $string;
	}

	public static function generateDataLine($chartData, $labels){
		$data = [];
		$i = 0;
		foreach($chartData as $row){
			$data[] = [
					'label' => count($labels) > $i ? $labels[$i] : '',
					'fill' => false,
					'data' => $row,
					'borderColor' => ChartHelper::COLORS[$i]['border'],
					'backgroundColor' => ChartHelper::COLORS[$i]['background'],
					'lineTension' => 0,
			];
			$i++;
		}
		return $data;
	}
}
