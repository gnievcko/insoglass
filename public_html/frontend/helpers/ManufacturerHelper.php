<?php
namespace frontend\helpers;

use Yii;
use yii\helpers\Html;
use frontend\helpers\AddressHelper;

class ManufacturerHelper {
	
	public static function getFullManufacturerData($params) {
		$manufacturer = '';
		if(!empty($params['id'])) {
			$manufacturer = Html::a(Html::encode($params['name']), ['warehouse-supplier/details', 'id' => $params['id']]);
				
			if(!empty($params['contactPerson'])) {
				$manufacturer .= '<br/>'.Yii::t('web', 'Contact person').': '.Html::encode($params['contactPerson']);
			}
				
			$contactData = [];
			if(!empty($params['email'])) {
				$contactData[] = Html::mailto(Html::encode($params['email']));
			}
			if(!empty($params['phone'])) {
				$contactData[] = Html::encode($params['phone']);
			}
				
			if(!empty($contactData)) {
				$manufacturer .= '<br/>'.Yii::t('web', 'Contact').': '.implode(', ', $contactData);
			}
				
			if(!empty($params['address'])) {
				$manufacturer .= '<br/>'.Yii::t('main', 'Address').': '.AddressHelper::getFullAddress([
						'addressMain' => Html::encode($params['address']),
						'addressComplement' => Html::encode($params['complement']),
						'cityName' => Html::encode($params['city']),
						'cityZipCode' => Html::encode($params['zipCode']),
						'provinceName' => Html::encode($params['province']),
						'countryName' => Html::encode($params['country']),
				]);
			}
		}
		
		return $manufacturer;
	}
	
}
