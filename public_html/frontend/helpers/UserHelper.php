<?php
namespace frontend\helpers;

class UserHelper {
	
	public static function getPrettyUserName($email, $firstName, $lastName) {
		$text = '';
		if(!empty($firstName) && !empty($lastName)) {
			$text = $firstName.' '.$lastName;
		}
		else {
			$text = $email;
		}
		
		return $text;
	}
	
}