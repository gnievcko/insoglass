<?php

namespace frontend\helpers;

use yii\base\Model;

class ModelLoaderHelper {

    public static function loadOne($data, $formClass) {
        $form = new $formClass();
        $form->load($data);

        return $form;
    }

    public static function loadMultiple($data, $formClass, $setEditScenario = false) {
    	$dummyForm = new $formClass();
    	$formsData = empty($data[$dummyForm->formName()]) ? [] : $data[$dummyForm->formName()];
		
        $forms = [];
        foreach($formsData as $formIdx => $formData) {
        	$forms[$formIdx] = new $formClass();
        	if($setEditScenario) {
        		$forms[$formIdx]->setEditScenario();
        	}
        }
		
        Model::loadMultiple($forms, $data);
        return $forms;
    }

    public static function getErrorForms($forms) {
        return array_filter($forms, function($form) {
            return $form->hasErrors(); 
        });
    }

    public static function isEmpty($form) {
        return array_reduce($form->getAttributes(), function($isEmpty, $attributeValue) {
            return $isEmpty && empty($attributeValue);
        }, true);
    }

    public static function skipEmpty($forms) {
        return array_filter($forms, function($form) {
            return !self::isEmpty($form);
        });
    }
}
