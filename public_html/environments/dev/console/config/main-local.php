<?php
return [
	    'bootstrap' => ['gii'],
	    'modules' => [
	        	'gii' => [
			        	'class' => 'yii\gii\Module',
			        	'allowedIPs' => ['127.0.0.1', '::1', '192.168.1.*'],
			        	'generators' => [
					        	'model' => [
							        	'class' => 'yii\gii\generators\model\Generator',
							        	'templates' => [
							        			'mindseater-template' => '@common/templates/gii/model',
							        	]
					        	]
			        	]
	        	],
	    ]
];
