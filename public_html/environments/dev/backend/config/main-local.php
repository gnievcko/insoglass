<?php

$config = [
	    'components' => [
		        'request' => [
			            // !!! insert a secret key in the following (if it is empty) - this is required by cookie validation
			            'cookieValidationKey' => '',
		        ],
		        'urlManagerFrontend' => [
		        		'baseUrl' => '/manman'
		        ],
	    ],
];

if (!YII_ENV_TEST) {
    // configuration adjustments for 'dev' environment
    $config['bootstrap'][] = 'debug';
    $config['modules']['debug'] = [
        	'class' => 'yii\debug\Module',
    ];

    $config['bootstrap'][] = 'gii';
    $config['modules']['gii'] = [
    		'class' => 'yii\gii\Module',
    		'allowedIPs' => ['127.0.0.1', '::1', '192.168.1.*'],
    		'generators' => [
	    			'model' => [
				    		'class' => 'yii\gii\generators\model\Generator',
				    		'templates' => [ 
				    				'mindseater-template' => '@common/templates/gii/model',
		    				]
		    		],
		    		'crud' => [
				    		'class' => 'yii\gii\generators\crud\Generator',
				    		'templates' => [
				    				'mindseater-template' => '@common/templates/gii/crud',
				    		]
		    		]
		    ],
    ];
}

return $config;
