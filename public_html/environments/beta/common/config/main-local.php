<?php
return [
	    'components' => [
		        'db' => [
			            'class' => 'yii\db\Connection',
			            'dsn' => 'mysql:host=localhost;dbname=matbud_dev_3',
			            'username' => 'matbud_dev',
			            'password' => 'Bai9vaik6oh',
			            'charset' => 'utf8',
		        ],
                'mongodb' => [
			            'class' => 'yii\mongodb\Connection',
			            'dsn' => 'mongodb://localhost:27017/insoglas',
		        ],
		        'mailer' => [
			            'class' => 'yii\swiftmailer\Mailer',
			            'viewPath' => '@common/mail',
		        ],
	    ],
];
