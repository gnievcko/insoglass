<?php

namespace Mindseater\PHPUtils\Tests\Arrays;

use Mindseater\PHPUtils\Tests\TestCase;
use Mindseater\PHPUtils\Arrays\ValuesReplacer;

class ValuesReplacerTest extends TestCase {

    public function testReplacingProvidedValues() {
        $replacer = new ValuesReplacer([null, '', 1, 'string']);

        $this->assertEquals([null, '', 'replaced', 'string'], $replacer->replace(function($element) { return $element === 1; }, 'replaced'));
    }

    public function testReplaceEmpty() {
        $replacer = new ValuesReplacer([null, '', 1, 'string']);

        $this->assertEquals(['replaced', 'replaced', 1, 'string'], $replacer->replaceEmpty('replaced'));
    }
}
