<?php

namespace Mindseater\PHPUtils\Tests\Dates;

use Mindseater\PHPUtils\Tests\TestCase;
use Mindseater\PHPUtils\Dates\Formatter;

class FormatterTest extends TestCase {

	private $formatter;
	
	public function setUp() {
		parent::setUp();
		$this->formatter = new Formatter();
	}
	
    public function testChangeFormat() {
        $this->assertEquals('07.01.2015', $this->formatter->from('2015-01-07', 'Y-m-d')->format('d.m.Y')->getDate());
    }
    

    public function testConvertingDateFormatUsingRememberedFormat() {
    	$this->assertEquals('07.01.2015', $this->formatter->setFormat('d.m.Y')->getFormattedDate('2015-01-07'));
    }
}
