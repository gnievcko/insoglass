<?php

namespace Mindseater\PHPUtils\Tests\Text;

use Mindseater\PHPUtils\Tests\TestCase;
use Mindseater\PHPUtils\Text\StringSequenceGenerator;

class StringSequenceGeneratorTest extends TestCase {

    public function testSequenceRepeating() {
        $generator = new StringSequenceGenerator('.');

        $this->assertEquals('...', $generator->repeat(3));
    }
}
