<?php

namespace Mindseater\PHPUtils\Tests;

use PHPUnit\Framework\TestCase as PHPUnitTestCase;

class TestCase extends PHPUnitTestCase {

    public function tearDown() {
        parent::tearDown();
        \Mockery::close();
    }
}
