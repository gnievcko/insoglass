<?php

namespace Mindseater\PHPUtils\Tests\Lists;

use Mindseater\PHPUtils\Tests\TestCase;
use Mindseater\PHPUtils\Lists\AlphabeticalEnumerator;

class AlphabeticalEnumeratorTest extends TestCase {

    public function testMakeAlphaIndexReturnsCorrectLetter() {
        $enumerator = new AlphabeticalEnumerator();

        $this->assertSame('a', $enumerator->makeAlphaIndex(0));
        $this->assertSame('b', $enumerator->makeAlphaIndex(1));
    }

    public function testMakeItemReturnsItemWithCorrectSeparator() {
        $separator = ') ';
        $enumerator = new AlphabeticalEnumerator($separator);

        $itemContent = 'content';
        $this->assertEquals('a) content', $enumerator->makeItem(0, $itemContent));
    }

    public function testEnumerateItemsGeneratesArrayOfItems() {
        $enumerator = new AlphabeticalEnumerator();

        $itemsContent = ['content1', 'content2'];
        $this->assertEquals(["a {$itemsContent[0]}", "b {$itemsContent[1]}"], $enumerator->enumerate($itemsContent));
    }
    
}
