<?php

namespace Mindseater\PHPUtils\Arrays;

class ValuesReplacer {

    private $array;

    public function __construct($array = []) {
        $this->array = $array;
    }

    public function replaceEmpty($replacement) {
        return $this->replace(function($element) { 
            return empty($element);
        }, $replacement);
    }

    public function replace($shouldBeReplaced, $replacement) {
        $this->array = array_map(function($value) use($shouldBeReplaced, $replacement) {
            return $shouldBeReplaced($value) ? $replacement : $value;
        }, $this->array);

        return $this->array;
    }
}
