<?php

namespace Mindseater\PHPUtils\Dates;

class Formatter {

    private $date;
    private $format = 'Y-m-d';
    
    public function __construct($date = null) {
    	$this->date = $date === null ? date($this->format) : $date;
    }
    
    public function from($date, $format) {
    	$this->date = $date;
    	$this->format = $format;

        return $this;
    }

    public function format($format) {
    	$this->format = $format;
        return $this->formatDate($this->date);
    }

    public function setFormat($format) {
    	$this->format = $format;
    	return $this;
    }
    
    public function getDate() {
        return $this->date;
    }
    
    public function getFormattedDate($date) {
    	return $this->formatDate($date)->getDate();
    }
    
    private function formatDate($date) {
    	$this->date = date($this->format, strtotime($date));
    	return $this;
    }
}
