<?php

namespace Mindseater\PHPUtils\Lists;

class AlphabeticalEnumerator {

    private $separator;

    public function __construct($separator = ' ') {
        $this->separator = $separator;
    }

    public function enumerate($itemsContent) {
        return array_map(function($itemContent, $index) {
            return $this->makeItem($index, $itemContent);
        }, $itemsContent, array_keys($itemsContent));
    }

    public function makeItem($index, $content) {
        return "{$this->makeAlphaIndex($index)}{$this->separator}{$content}";
    }

    public function makeAlphaIndex($numericalIndex) {
        return chr(ord('a') + $numericalIndex);
    }
}
