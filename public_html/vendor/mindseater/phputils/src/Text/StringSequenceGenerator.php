<?php

namespace Mindseater\PHPUtils\Text;

class StringSequenceGenerator {

    private $string;

    public function __construct($string) {
        $this->string = $string;
    }

    public function repeat($times) {
        $sequence = '';
        for($i = 0; $i < $times; ++$i) {
            $sequence .= $this->string;
        }
        return $sequence;
    }
}
