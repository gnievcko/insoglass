<?php

namespace backend\controllers;

use Yii;
use common\models\ar\TimeUnitTranslation;
use common\models\ars\TimeUnitTranslationSearch;
use backend\components\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\widgets\ActiveForm;
use yii\web\Response;
use yii\web\ForbiddenHttpException;

/**
 * TimeUnitTranslationController implements the CRUD actions for TimeUnitTranslation model.
 */
class TimeUnitTranslationController extends Controller {

	protected $creatable = true;
	protected $editable = true;
	protected $removable = true;

	/* Behaviours are inherited */

    /**
     * Lists all TimeUnitTranslation models.
     * @return mixed
     */
    public function actionIndex() {
        $searchModel = new TimeUnitTranslationSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
	            'searchModel' => $searchModel,
	            'dataProvider' => $dataProvider,
        		'withoutCreating' => !$this->creatable,
        		'withoutUpdating' => !$this->editable,
        		'withoutDeleting' => !$this->removable,
        ]);
    }

    /**
     * Displays a single TimeUnitTranslation model.
     * @param integer $time_unit_id
     * @param integer $language_id
     * @param integer $value
     * @return mixed
     */
    public function actionView($time_unit_id, $language_id, $value) {
        return $this->render('view', [
				'model' => $this->findModel($time_unit_id, $language_id, $value),
        		'withoutUpdating' => !$this->editable,
        		'withoutDeleting' => !$this->removable,
        ]);
    }

    /**
     * Creates a new TimeUnitTranslation model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate($timeUnitId = null) {
    	if(!$this->creatable) {
    		throw new ForbiddenHttpException(Yii::t('main', 'You cannot complete your request for security reasons.'));
    	}
    
        $model = new TimeUnitTranslation();

        if(Yii::$app->request->isAjax && $model->load(Yii::$app->request->post())) {
        	Yii::$app->response->format = Response::FORMAT_JSON;
        	return ActiveForm::validate($model);
        }
        
        if($model->load(Yii::$app->request->post()) && $model->save()) {
        	if(!empty($timeUnitId)) {
        		return $this->redirect(['time-unit/view', 'id' => $model->time_unit_id]);
        	}
        	else {
            	return $this->redirect(['view', 'time_unit_id' => $model->time_unit_id, 'language_id' => $model->language_id, 'value' => $model->value]);
        	}
        } 
        else {
            return $this->render('create', [
					'model' => $model,
            		'timeUnitId' => $timeUnitId,
            ]);
        }
    }

    /**
     * Updates an existing TimeUnitTranslation model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $time_unit_id
     * @param integer $language_id
     * @param integer $value
     * @return mixed
     */
    public function actionUpdate($time_unit_id, $language_id, $value) {
    	if(!$this->editable) {
    		throw new ForbiddenHttpException(Yii::t('main', 'You cannot complete your request for security reasons.'));
    	}
    
        $model = $this->findModel($time_unit_id, $language_id, $value);

        if(Yii::$app->request->isAjax && $model->load(Yii::$app->request->post())) {
        	Yii::$app->response->format = Response::FORMAT_JSON;
        	return ActiveForm::validate($model);
        }
        
        if($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'time_unit_id' => $model->time_unit_id, 'language_id' => $model->language_id, 'value' => $model->value]);
        } 
        else {
            return $this->render('update', [
					'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing TimeUnitTranslation model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $time_unit_id
     * @param integer $language_id
     * @param integer $value
     * @return mixed
     */
    public function actionDelete($time_unit_id, $language_id, $value) {
    	if(!$this->removable) {
    		throw new ForbiddenHttpException(Yii::t('main', 'You cannot complete your request for security reasons.'));
    	}
    
    	$transaction = Yii::$app->db->beginTransaction();
    	
    	try {
    		$this->findModel($time_unit_id, $language_id, $value)->delete();
    		$transaction->commit();
        }
        catch(\Exception $e) {
        	$transaction->rollBack();
			Yii::$app->getSession()->setFlash('error', Yii::t('main', 'The object cannot be deleted because it is used by other objects.'));
        }
        
        if(strpos(Yii::$app->request->getReferrer(), Yii::$app->controller->id.'/view') !== false) {
        	return $this->redirect(['index']);
        }
        else {
        	return $this->redirect(Yii::$app->request->getReferrer());
        }
    }

    /**
     * Finds the TimeUnitTranslation model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $time_unit_id
     * @param integer $language_id
     * @param integer $value
     * @return TimeUnitTranslation the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($time_unit_id, $language_id, $value) {
        if(($model = TimeUnitTranslation::findOne(['time_unit_id' => $time_unit_id, 'language_id' => $language_id, 'value' => $value])) !== null) {
            return $model;
        } 
        else {
            throw new NotFoundHttpException(Yii::t('main', 'The requested page does not exist. Please verify the address of the page.'));
        }
    }
}
