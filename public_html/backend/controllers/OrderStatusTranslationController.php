<?php

namespace backend\controllers;

use Yii;
use common\models\ar\OrderStatusTranslation;
use common\models\ars\OrderStatusTranslationSearch;
use backend\components\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\widgets\ActiveForm;
use yii\web\Response;
use yii\web\ForbiddenHttpException;

/**
 * OrderStatusTranslationController implements the CRUD actions for OrderStatusTranslation model.
 */
class OrderStatusTranslationController extends Controller {

	protected $creatable = true;
	protected $editable = true;
	protected $removable = true;

	/* Behaviours are inherited */

    /**
     * Lists all OrderStatusTranslation models.
     * @return mixed
     */
    public function actionIndex() {
        $searchModel = new OrderStatusTranslationSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
	            'searchModel' => $searchModel,
	            'dataProvider' => $dataProvider,
        		'withoutCreating' => !$this->creatable,
        		'withoutUpdating' => !$this->editable,
        		'withoutDeleting' => !$this->removable,
        ]);
    }

    /**
     * Displays a single OrderStatusTranslation model.
     * @param integer $order_status_id
     * @param integer $language_id
     * @return mixed
     */
    public function actionView($order_status_id, $language_id) {
        return $this->render('view', [
				'model' => $this->findModel($order_status_id, $language_id),
        		'withoutUpdating' => !$this->editable,
        		'withoutDeleting' => !$this->removable,
        ]);
    }

    /**
     * Creates a new OrderStatusTranslation model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate($orderStatusId = null) {
    	if(!$this->creatable) {
    		throw new ForbiddenHttpException(Yii::t('main', 'You can not complete your request for security reasons.'));
    	}
    
        $model = new OrderStatusTranslation();

        if(Yii::$app->request->isAjax && $model->load(Yii::$app->request->post())) {
        	Yii::$app->response->format = Response::FORMAT_JSON;
        	return ActiveForm::validate($model);
        }
        
        if($model->load(Yii::$app->request->post()) && $model->save()) {
        	if(!empty($orderStatusId)) {
        		return $this->redirect(['order-status/view', 'id' => $model->order_status_id]);
        	}
        	else {
                return $this->redirect(['view', 'order_status_id' => $model->order_status_id, 'language_id' => $model->language_id]);
        	}
        } 
        else {
            return $this->render('create', [
					'model' => $model,
                    'orderStatusId' => $orderStatusId,
            ]);
        }
    }

    /**
     * Updates an existing OrderStatusTranslation model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $order_status_id
     * @param integer $language_id
     * @return mixed
     */
    public function actionUpdate($order_status_id, $language_id) {
    	if(!$this->editable) {
    		throw new ForbiddenHttpException(Yii::t('main', 'You can not complete your request for security reasons.'));
    	}
    
        $model = $this->findModel($order_status_id, $language_id);

        if(Yii::$app->request->isAjax && $model->load(Yii::$app->request->post())) {
        	Yii::$app->response->format = Response::FORMAT_JSON;
        	return ActiveForm::validate($model);
        }
        
        if($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'order_status_id' => $model->order_status_id, 'language_id' => $model->language_id]);
        } 
        else {
            return $this->render('update', [
					'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing OrderStatusTranslation model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $order_status_id
     * @param integer $language_id
     * @return mixed
     */
    public function actionDelete($order_status_id, $language_id) {
    	if(!$this->removable) {
    		throw new ForbiddenHttpException(Yii::t('main', 'You can not complete your request for security reasons.'));
    	}
    
    	$transaction = Yii::$app->db->beginTransaction();
    	
    	try {
    		$this->findModel($order_status_id, $language_id)->delete();
    		$transaction->commit();
        }
        catch(\Exception $e) {
        	$transaction->rollBack();
			Yii::$app->getSession()->setFlash('error', Yii::t('main', 'The object cannot be deleted because it is used by other objects.'));
        }

    	if(strpos(Yii::$app->request->getReferrer(), Yii::$app->controller->id.'/view') !== false) {
        	return $this->redirect(['index']);
        }
        else {
        	return $this->redirect(Yii::$app->request->getReferrer());
        }
    }

    /**
     * Finds the OrderStatusTranslation model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $order_status_id
     * @param integer $language_id
     * @return OrderStatusTranslation the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($order_status_id, $language_id) {
        if(($model = OrderStatusTranslation::findOne(['order_status_id' => $order_status_id, 'language_id' => $language_id])) !== null) {
            return $model;
        } 
        else {
            throw new NotFoundHttpException(Yii::t('main', 'The requested page does not exist. Please verify the address of the page.'));
        }
    }
}
