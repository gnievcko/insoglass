<?php

namespace backend\controllers;

use Yii;
use common\models\ar\ContractTypeTranslation;
use common\models\ars\ContractTypeTranslationSearch;
use backend\components\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\widgets\ActiveForm;
use yii\web\Response;
use yii\web\ForbiddenHttpException;

/**
 * ContractTypeTranslationController implements the CRUD actions for ContractTypeTranslation model.
 */
class ContractTypeTranslationController extends Controller {
//    public $enableCsrfValidation = false;
    protected $creatable = true;
    protected $editable = true;
    protected $removable = true;

    /* Behaviours are inherited */

    /**
     * Lists all ContractTypeTranslation models.
     * @return mixed
     */
    public function actionIndex() {
        $searchModel = new ContractTypeTranslationSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
                    'searchModel' => $searchModel,
                    'dataProvider' => $dataProvider,
                    'withoutCreating' => !$this->creatable,
                    'withoutUpdating' => !$this->editable,
                    'withoutDeleting' => !$this->removable,
        ]);
    }

    /**
     * Displays a single ContractTypeTranslation model.
     * @param integer $language_id
     * @param integer $contract_type_id
     * @return mixed
     */
    public function actionView($language_id, $contract_type_id) {
        return $this->render('view', [
                    'model' => $this->findModel($language_id, $contract_type_id),
                    'withoutUpdating' => !$this->editable,
                    'withoutDeleting' => !$this->removable,
        ]);
    }

    /**
     * Creates a new ContractTypeTranslation model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate($contractTypeId = null) {
        if(!$this->creatable) {
            throw new ForbiddenHttpException(Yii::t('main', 'You cannot complete your request for security reasons.'));
        }

        $model = new ContractTypeTranslation();

        if(Yii::$app->request->isAjax && $model->load(Yii::$app->request->post())) {
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ActiveForm::validate($model);
        }

        if($model->load(Yii::$app->request->post()) && $model->save()) {
            if(!empty($contractTypeId)) {
                return $this->redirect(['contract-type/view', 'id' => $model->contract_type_id]);
            } else {
                return $this->redirect(['view', 'language_id' => $model->language_id, 'contract_type_id' => $model->contract_type_id]);
            }
        } else {
            return $this->render('create', [
                        'model' => $model,
                        'contractTypeId' => $contractTypeId,
            ]);
        }
    }

    /**
     * Updates an existing ContractTypeTranslation model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $language_id
     * @param integer $contract_type_id
     * @return mixed
     */
    public function actionUpdate($language_id, $contract_type_id) {
        if (!$this->editable) {
            throw new ForbiddenHttpException(Yii::t('main', 'You cannot complete your request for security reasons.'));
        }

        $model = $this->findModel($language_id, $contract_type_id);

        if (Yii::$app->request->isAjax && $model->load(Yii::$app->request->post())) {
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ActiveForm::validate($model);
        }

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'language_id' => $model->language_id, 'contract_type_id' => $model->contract_type_id]);
        } else {
            return $this->render('update', [
                        'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing ContractTypeTranslation model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $language_id
     * @param integer $contract_type_id
     * @return mixed
     */
    public function actionDelete($language_id, $contract_type_id) {
        if (!$this->removable) {
            throw new ForbiddenHttpException(Yii::t('main', 'You cannot complete your request for security reasons.'));
        }

        $transaction = Yii::$app->db->beginTransaction();

        try {
            $this->findModel($language_id, $contract_type_id)->delete();
            $transaction->commit();
        } catch (\Exception $e) {
            $transaction->rollBack();
            Yii::$app->getSession()->setFlash('error', Yii::t('main', 'The object cannot be deleted because it is used by other objects.'));
        }

        if (strpos(Yii::$app->request->getReferrer(), Yii::$app->controller->id . '/view') !== false) {
            return $this->redirect(['index']);
        } else {
            return $this->redirect(Yii::$app->request->getReferrer());
        }
    }

    /**
     * Finds the ContractTypeTranslation model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $language_id
     * @param integer $contract_type_id
     * @return ContractTypeTranslation the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($language_id, $contract_type_id) {
        if (($model = ContractTypeTranslation::findOne(['language_id' => $language_id, 'contract_type_id' => $contract_type_id])) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException(Yii::t('main', 'The requested page does not exist. Please verify the address of the page.'));
        }
    }
}
