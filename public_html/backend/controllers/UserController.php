<?php

namespace backend\controllers;

use Yii;
use common\models\ar\User;
use common\models\ar\UserRole;
use common\models\ars\UserSearch;
use backend\components\Controller;
use yii\web\NotFoundHttpException;
use yii\helpers\Url;
use yii\filters\VerbFilter;
use yii\widgets\ActiveForm;
use yii\data\ActiveDataProvider;
use yii\web\Response;
use yii\web\ForbiddenHttpException;
use yii\web\UploadedFile;
use yii\helpers\FileHelper;
use common\helpers\UploadHelper;
use common\models\ar\UserCompanySalesman;
use common\models\ar\UserWarehouse;
use common\models\ar\UserOrderSalesman;
use common\models\ar\UserAction;
use common\models\ar\UserOrderSalesmanHistory;
use common\models\ar\Alert;
use common\models\ar\Task;
use common\models\ar\Token;
use common\models\ar\Address;
use yii\helpers\ArrayHelper;

/**
 * UserController implements the CRUD actions for User model.
 */
class UserController extends Controller {

	protected $creatable = true;
	protected $editable = true;
	protected $removable = true;

	/* Behaviours are inherited */

    /**
     * Lists all User models.
     * @return mixed
     */
    public function actionIndex() {
        $searchModel = new UserSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
	            'searchModel' => $searchModel,
	            'dataProvider' => $dataProvider,
        		'withoutCreating' => !$this->creatable,
        		'withoutUpdating' => !$this->editable,
        		'withoutDeleting' => !$this->removable,
        ]);
    }

    /**
     * Displays a single User model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id) {
        return $this->render('view', [
				'model' => $this->findModel($id),
        		'withoutUpdating' => !$this->editable,
        		'withoutDeleting' => !$this->removable,
				'userRoleDataProvider' => new ActiveDataProvider([
						'query' => UserRole::find()->where(['user_id' => $id])
				]),
        		'userCompanySalesmanDataProvider' => new ActiveDataProvider([
        				'query' => UserCompanySalesman::find()->where(['user_id' => $id])
        		]),
        		'userWarehouseDataProvider' => new ActiveDataProvider([
        				'query' => UserWarehouse::find()->where(['user_id' => $id])
        		]),
        ]);
    }

    /**
     * Creates a new User model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate() {
    	if(!$this->creatable) {
    		throw new ForbiddenHttpException(Yii::t('main', 'You cannot complete your request for security reasons.'));
    	}
    
        $model = new User();

        if(Yii::$app->request->isAjax && $model->load(Yii::$app->request->post())) {
        	Yii::$app->response->format = Response::FORMAT_JSON;
        	return ActiveForm::validate($model);
        }
        
        if($model->load(Yii::$app->request->post()) && $model->validate()) {
            $model->setPassword($model->password);
            $this->loadModelFiles($model);

            if($model->save(false)) {
                return $this->redirect(['view', 'id' => $model->id]);
            }
        } 
        else {
            return $this->render('create', [
					'model' => $model,
            		'addressData' => [],
            ]);
        }
    }

    private function loadModelFiles($model) {
		$model->file_photo = UploadedFile::getInstance($model, 'file_photo');

        $newPhoto = $newPhoto = $this->loadNewPhoto($model);
        $oldPhotoUrl = $model->url_photo;
        $needToRemoveOldPhoto = ($newPhoto !== false) || Yii::$app->request->post('delete-photo');

        if($newPhoto !== false) {
            //$model->url_photo = Yii::$app->request->getFrontendBaseUrl().'storage/index?f=' . basename($newPhoto);
        	$model->url_photo = Url::to('@web/storage/index?f=' . basename($newPhoto));
        }

        if($needToRemoveOldPhoto) {
            $this->removePhotoFromDisk($oldPhotoUrl);
            $model->url_photo = ($newPhoto !== false) ? $model->url_photo : null;
        }
	}

    private function loadNewPhoto($model) {
        if(!empty($model->file_photo) && ($file_photo = $model->uploadFile('file_photo')) !== false) {
            return $file_photo;
        }
        else {
            return false;
        }
    }

    private function removePhotoFromDisk($photoUrl) {
        if(!empty($photoUrl)) {
            //$oldPhotoRelativePath = preg_replace('|^'.Yii::$app->request->getFrontendBaseUrl().'storage/index\?f='.'|', '', $photoUrl);
        	$oldPhotoRelativePath = preg_replace('|^'.Url::to('@web/storage/index\?f='.'|'), '', $photoUrl);
            $oldFile = FileHelper::normalizePath(UploadHelper::getUploadPath().'/'.$oldPhotoRelativePath);
            @unlink($oldFile);
        }
    }

    /**
     * Updates an existing User model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id) {
    	if(!$this->editable) {
    		throw new ForbiddenHttpException(Yii::t('main', 'You cannot complete your request for security reasons.'));
    	}
    
        $model = $this->findModel($id);

        if(Yii::$app->request->isAjax && $model->load(Yii::$app->request->post())) {
        	Yii::$app->response->format = Response::FORMAT_JSON;
        	return ActiveForm::validate($model);
        }

        $oldPassword = $model->password; 
        if($model->load(Yii::$app->request->post()) && $model->validate()) {
            if($model->password != $oldPassword) {
                $model->setPassword($model->password);
            }
            $this->loadModelFiles($model);

            if($model->save(false)) {
                return $this->redirect(['view', 'id' => $model->id]);
            }
        } 
        else {
        	$addressData = Address::find()->where('id = :id', [':id' => $model->address_id])->all();
        	$addressData = ArrayHelper::map($addressData, 'id', function($item) {
        		return $item->getFullAddress();
        	});
        	
        	//echo print_r($addressData);exit;
        	
            return $this->render('update', [
					'model' => $model,
            		'addressData' => $addressData,
            ]);
        }
    }

    /**
     * Deletes an existing User model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id) {
    	if(!$this->removable || $id === 1) {
    		throw new ForbiddenHttpException(Yii::t('main', 'You cannot complete your request for security reasons.'));
    	}
    
    	$transaction = Yii::$app->db->beginTransaction();
    	$user = $this->findModel($id);
    	try {
    		UserRole::deleteAll(['user_id' => $id]);
    		UserAction::deleteAll(['user_id' => $id]);
    		UserWarehouse::deleteAll(['user_id' => $id]);
    		UserOrderSalesman::deleteAll(['user_id' => $id]);
    		UserOrderSalesmanHistory::deleteAll(['user_id' => $id]);
    		UserCompanySalesman::deleteAll(['user_id' => $id]);
    		Token::deleteAll(['user_id' => $id]);
    	
    		Alert::updateAll(['user_id' => null], ['user_id' => $id]);
    		Task::deleteAll(['user_id' => null, 'user_notified_id' => null]);
    		Task::updateAll(['user_id' => 'user_notified_id', 'user_notified_id' => null], 'user_id = :userId AND user_notified_id IS NOT NULL', [':userId' => $id]);
    	
    		$userPhoto = $user->url_photo;
    		$user->delete();
    		$this->removePhotoFromDisk($userPhoto);
    		$transaction->commit();
    	}
    	catch(\Exception $e) {
    		$transaction->rollBack();
			Yii::$app->getSession()->setFlash('error', Yii::t('main', 'The object cannot be deleted because it is used by other objects.'));
        }

        return $this->redirect(['index']);
    }

    /**
     * Finds the User model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return User the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id) {
        if(($model = User::find()->employee()->andWhere(['id' => $id])->one()) !== null) {
            return $model;
        } 
        else {
            throw new NotFoundHttpException(Yii::t('main', 'The requested page does not exist. Please verify the address of the page.'));
        }
    }
}
