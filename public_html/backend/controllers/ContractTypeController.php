<?php

namespace backend\controllers;

use Yii;
use common\models\ar\ContractType;
use common\models\ars\ContractTypeSearch;
use backend\components\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\widgets\ActiveForm;
use yii\web\Response;
use yii\web\ForbiddenHttpException;
use common\models\ar\ContractTypeTranslation;
use common\models\ar\Province;
use yii\data\ActiveDataProvider;
use common\helpers\Utility;

/**
 * ContractTypeController implements the CRUD actions for ContractType model.
 */
class ContractTypeController extends Controller {

    protected $creatable = true;
    protected $editable = true;
    protected $removable = true;

    /* Behaviours are inherited */

    /**
     * Lists all ContractType models.
     * @return mixed
     */
    public function actionIndex() {
        $searchModel = new ContractTypeSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
                    'searchModel' => $searchModel,
                    'dataProvider' => $dataProvider,
                    'withoutCreating' => !$this->creatable,
                    'withoutUpdating' => !$this->editable,
                    'withoutDeleting' => !$this->removable,
        ]);
    }

    /**
     * Displays a single ContractType model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id) {
        return $this->render('view', [
                    'model' => $this->findModel($id),
                    'contractTypeTranslationDataProvider' => new ActiveDataProvider([
                        'query' => ContractTypeTranslation::find()->where(['contract_type_id' => $id])
                    ]),
                    'withoutUpdating' => !$this->editable,
                    'withoutDeleting' => !$this->removable,
        ]);
    }

    /**
     * Creates a new ContractType model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate() {
        if(!$this->creatable) {
            throw new ForbiddenHttpException(Yii::t('main', 'You cannot complete your request for security reasons.'));
        }

        $model = new ContractType();

        if(Yii::$app->request->isAjax && $model->load(Yii::$app->request->post())) {
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ActiveForm::validate($model);
        }

        if($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('create', ['model' => $model,]);
        }
    }

    /**
     * Updates an existing ContractType model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id) {
        if(!$this->editable) {
            throw new ForbiddenHttpException(Yii::t('main', 'You cannot complete your request for security reasons.'));
        }

        $model = $this->findModel($id);
        $symbol = $model->attributes['symbol'];
        if($symbol != Utility::CONTRACT_TYPE_MALFUNCTION_REQUEST) {

            if(Yii::$app->request->isAjax && $model->load(Yii::$app->request->post())) {
                Yii::$app->response->format = Response::FORMAT_JSON;
                return ActiveForm::validate($model);
            }

            if($model->load(Yii::$app->request->post()) && $model->save()) {
                return $this->redirect(['view', 'id' => $model->id]);
            } else {
                return $this->render('update', ['model' => $model,]);
            }
        }
    }

    /**
     * Deletes an existing ContractType model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id) {
        if(!$this->removable) {
            throw new ForbiddenHttpException(Yii::t('main', 'You cannot complete your request for security reasons.'));
        }
        $model = $this->findModel($id);
        $symbol = $model->attributes['symbol'];
        if($symbol != Utility::CONTRACT_TYPE_MALFUNCTION_REQUEST) {
            $transaction = Yii::$app->db->beginTransaction();

            try {
                ContractTypeTranslation::deleteAll(['contract_type_id' => $id]);
                $model->delete();
                $transaction->commit();
            } catch (\Exception $e) {
                $transaction->rollBack();
                Yii::$app->getSession()->setFlash('error', Yii::t('main', 'The object cannot be deleted because it is used by other objects.'));
            }
        }
        return $this->redirect(['index']);
    }

    /**
     * Finds the ContractType model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return ContractType the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id) {
        if(($model = ContractType::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException(Yii::t('main', 'The requested page does not exist. Please verify the address of the page.'));
        }
    }

}
