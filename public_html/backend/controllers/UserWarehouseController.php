<?php

namespace backend\controllers;

use Yii;
use common\models\ar\UserWarehouse;
use common\models\ars\UserWarehouseSearch;
use backend\components\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\widgets\ActiveForm;
use yii\web\Response;
use yii\web\ForbiddenHttpException;

/**
 * UserWarehouseController implements the CRUD actions for UserWarehouse model.
 */
class UserWarehouseController extends Controller {

	protected $creatable = true;
	protected $editable = false;
	protected $removable = true;

	/* Behaviours are inherited */

    /**
     * Lists all UserWarehouse models.
     * @return mixed
     */
    public function actionIndex() {
        $searchModel = new UserWarehouseSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
	            'searchModel' => $searchModel,
	            'dataProvider' => $dataProvider,
        		'withoutCreating' => !$this->creatable,
        		'withoutUpdating' => !$this->editable,
        		'withoutDeleting' => !$this->removable,
        ]);
    }

    /**
     * Displays a single UserWarehouse model.
     * @param integer $user_id
     * @param integer $warehouse_id
     * @return mixed
     */
    public function actionView($user_id, $warehouse_id) {
        return $this->render('view', [
				'model' => $this->findModel($user_id, $warehouse_id),
        		'withoutUpdating' => !$this->editable,
        		'withoutDeleting' => !$this->removable,
        ]);
    }

    /**
     * Creates a new UserWarehouse model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate($userId = null, $warehouseId = null) {
    	if(!$this->creatable) {
    		throw new ForbiddenHttpException(Yii::t('main', 'You cannot complete your request for security reasons.'));
    	}
    
        $model = new UserWarehouse();

        if(Yii::$app->request->isAjax && $model->load(Yii::$app->request->post())) {
        	Yii::$app->response->format = Response::FORMAT_JSON;
        	return ActiveForm::validate($model);
        }
        
        if($model->load(Yii::$app->request->post()) && $model->save()) {      
        	if(empty($userId)) {
        		return $this->redirect(['view', 'user_id' => $model->user_id, 'warehouse_id' => $model->warehouse_id]);
        	}
        	else {
        		return $this->redirect(['user/view', 'id' => $model->user_id]);
        	}
        }
        else {
        	return $this->render('create', [
					'model' => $model,
            		'userId' => $userId,
        			'warehouseId' => $warehouseId,
        	]);
        }
    }

    /**
     * Updates an existing UserWarehouse model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $user_id
     * @param integer $warehouse_id
     * @return mixed
     */
    public function actionUpdate($user_id, $warehouse_id) {
    	if(!$this->editable) {
    		throw new ForbiddenHttpException(Yii::t('main', 'You cannot complete your request for security reasons.'));
    	}
    
        $model = $this->findModel($user_id, $warehouse_id);

        if(Yii::$app->request->isAjax && $model->load(Yii::$app->request->post())) {
        	Yii::$app->response->format = Response::FORMAT_JSON;
        	return ActiveForm::validate($model);
        }
        
        if($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'user_id' => $model->user_id, 'warehouse_id' => $model->warehouse_id]);
        } 
        else {
            return $this->render('update', [
					'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing UserWarehouse model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $user_id
     * @param integer $warehouse_id
     * @return mixed
     */
    public function actionDelete($user_id, $warehouse_id) {
    	if(!$this->removable) {
    		throw new ForbiddenHttpException(Yii::t('main', 'You cannot complete your request for security reasons.'));
    	}
    
    	$transaction = Yii::$app->db->beginTransaction();
    	
    	try {
    		$this->findModel($user_id, $warehouse_id)->delete();
    		$transaction->commit();
        }
        catch(\Exception $e) {
        	$transaction->rollBack();
			Yii::$app->getSession()->setFlash('error', Yii::t('main', 'The object cannot be deleted because it is used by other objects.'));
        }

        
        if(strpos(Yii::$app->request->getReferrer(), Yii::$app->controller->id.'/view') !== false) {
        	return $this->redirect(['index']);
        }
        else {
        	return $this->redirect(Yii::$app->request->getReferrer());
        }      
    }

    /**
     * Finds the UserWarehouse model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $user_id
     * @param integer $warehouse_id
     * @return UserWarehouse the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($user_id, $warehouse_id) {
        if(($model = UserWarehouse::findOne(['user_id' => $user_id, 'warehouse_id' => $warehouse_id])) !== null) {
            return $model;
        } 
        else {
            throw new NotFoundHttpException(Yii::t('main', 'The requested page does not exist. Please verify the address of the page.'));
        }
    }
}
