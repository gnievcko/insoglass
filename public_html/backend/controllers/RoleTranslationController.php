<?php

namespace backend\controllers;

use Yii;
use common\models\ar\RoleTranslation;
use common\models\ars\RoleTranslationSearch;
use backend\components\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\widgets\ActiveForm;
use yii\web\Response;
use yii\web\ForbiddenHttpException;

/**
 * RoleTranslationController implements the CRUD actions for RoleTranslation model.
 */
class RoleTranslationController extends Controller {

	protected $creatable = true;
	protected $editable = true;
	protected $removable = true;

	/* Behaviours are inherited */

    /**
     * Lists all RoleTranslation models.
     * @return mixed
     */
    public function actionIndex() {
        $searchModel = new RoleTranslationSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
	            'searchModel' => $searchModel,
	            'dataProvider' => $dataProvider,
        		'withoutCreating' => !$this->creatable,
        		'withoutUpdating' => !$this->editable,
        		'withoutDeleting' => !$this->removable,
        ]);
    }

    /**
     * Displays a single RoleTranslation model.
     * @param integer $role_id
     * @param integer $language_id
     * @return mixed
     */
    public function actionView($role_id, $language_id) {
        return $this->render('view', [
				'model' => $this->findModel($role_id, $language_id),
        		'withoutUpdating' => !$this->editable,
        		'withoutDeleting' => !$this->removable,
        ]);
    }

    /**
     * Creates a new RoleTranslation model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate($roleId = null) {
    	if(!$this->creatable) {
    		throw new ForbiddenHttpException(Yii::t('main', 'You can not complete your request for security reasons.'));
    	}
    
        $model = new RoleTranslation();

        if(Yii::$app->request->isAjax && $model->load(Yii::$app->request->post())) {
        	Yii::$app->response->format = Response::FORMAT_JSON;
        	return ActiveForm::validate($model);
        }
        
        if($model->load(Yii::$app->request->post()) && $model->save()) {
            if(empty($roleId)) {
                return $this->redirect(['view', 'role_id' => $model->role_id, 'language_id' => $model->language_id]);
            }
            else {
        		return $this->redirect(['role/view', 'id' => $model->role_id]);
            }
        } 
        else {
            return $this->render('create', [
					'model' => $model,
                    'roleId' => $roleId,
            ]);
        }
    }

    /**
     * Updates an existing RoleTranslation model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $role_id
     * @param integer $language_id
     * @return mixed
     */
    public function actionUpdate($role_id, $language_id) {
    	if(!$this->editable) {
    		throw new ForbiddenHttpException(Yii::t('main', 'You can not complete your request for security reasons.'));
    	}
    
        $model = $this->findModel($role_id, $language_id);

        if(Yii::$app->request->isAjax && $model->load(Yii::$app->request->post())) {
        	Yii::$app->response->format = Response::FORMAT_JSON;
        	return ActiveForm::validate($model);
        }
        
        if($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'role_id' => $model->role_id, 'language_id' => $model->language_id]);
        } 
        else {
            return $this->render('update', [
					'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing RoleTranslation model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $role_id
     * @param integer $language_id
     * @return mixed
     */
    public function actionDelete($role_id, $language_id) {
    	if(!$this->removable) {
    		throw new ForbiddenHttpException(Yii::t('main', 'You can not complete your request for security reasons.'));
    	}
    
    	$transaction = Yii::$app->db->beginTransaction();
    	
    	try {
    		$this->findModel($role_id, $language_id)->delete();
    		$transaction->commit();
        }
        catch(\Exception $e) {
        	$transaction->rollBack();
			Yii::$app->getSession()->setFlash('error', Yii::t('main', 'The object cannot be deleted because it is used by other objects.'));
        }

    	if(strpos(Yii::$app->request->getReferrer(), Yii::$app->controller->id.'/view') !== false) {
            return $this->redirect(['index']);
        }
        else {
        	return $this->redirect(Yii::$app->request->getReferrer());
        }
    }

    /**
     * Finds the RoleTranslation model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $role_id
     * @param integer $language_id
     * @return RoleTranslation the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($role_id, $language_id) {
        if(($model = RoleTranslation::findOne(['role_id' => $role_id, 'language_id' => $language_id])) !== null) {
            return $model;
        } 
        else {
            throw new NotFoundHttpException(Yii::t('main', 'The requested page does not exist. Please verify the address of the page.'));
        }
    }
}
