<?php

namespace backend\controllers;

use Yii;
use common\models\ar\Warehouse;
use common\models\ars\WarehouseSearch;
use common\models\ar\Address;
use common\models\ar\UserWarehouse;
use backend\components\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\widgets\ActiveForm;
use yii\web\Response;
use yii\web\ForbiddenHttpException;
use yii\helpers\ArrayHelper;
use yii\data\ActiveDataProvider;
use common\helpers\utility;


/**
 * WarehouseController implements the CRUD actions for Warehouse model.
 */
class WarehouseController extends Controller {

	protected $creatable = true;
	protected $editable = true;
	protected $removable = true;

	/* Behaviours are inherited */

    /**
     * Lists all Warehouse models.
     * @return mixed
     */
    public function actionIndex() {
    	if(!Yii::$app->params['isMoreWarehousesAvailable']) {
    		throw new ForbiddenHttpException(Yii::t('main', 'You cannot complete your request for security reasons.'));
    	}
    	
        $searchModel = new WarehouseSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        return $this->render('index', [
	            'searchModel' => $searchModel,
	            'dataProvider' => $dataProvider,
        		'withoutCreating' => !$this->creatable,
        		'withoutUpdating' => !$this->editable,
        		'withoutDeleting' => !$this->removable,
        ]);
    }

    /**
     * Displays a single Warehouse model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id) {
    	if(!Yii::$app->params['isMoreWarehousesAvailable']) {
    		throw new ForbiddenHttpException(Yii::t('main', 'You cannot complete your request for security reasons.'));
    	}
    	
        return $this->render('view', [
				'model' => $this->findModel($id),
        		'withoutUpdating' => !$this->editable,
        		'withoutDeleting' => !$this->removable,
        		'warehouseDataProvider' => new ActiveDataProvider([
        				'query' => Warehouse::find()->where(['parent_warehouse_id' => $id])
        		]),
        		'userWarehouseDataProvider' => new ActiveDataProvider([
        				'query' => UserWarehouse::find()
        						->joinWith('user.userRoles.role')
        						->where(['warehouse_id' => $id])
        						->andWhere(['role.symbol' => [Utility::ROLE_MAIN_STOREKEEPER, Utility::ROLE_STOREKEEPER]])
        		]),
        ]);
    }

    /**
     * Creates a new Warehouse model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate($parentWarehouseId = null) {
    	if(!Yii::$app->params['isMoreWarehousesAvailable'] || !$this->creatable) {
    		throw new ForbiddenHttpException(Yii::t('main', 'You cannot complete your request for security reasons.'));
    	}
    
        $model = new Warehouse();

        if(Yii::$app->request->isAjax && $model->load(Yii::$app->request->post())) {
        	Yii::$app->response->format = Response::FORMAT_JSON;
        	return ActiveForm::validate($model);
        }
        
        if($model->load(Yii::$app->request->post()) && $model->save()) {
        	if(!empty($parentWarehouseId)) {
        		return $this->redirect(['warehouse/view', 'id' => $model->parent_warehouse_id]);
        	}
        	else {
        		return $this->redirect(['view', 'id' => $model->id]);
        	}
        } 
        else {
            return $this->render('create', [
					'model' => $model,
            		'parentWarehouseId' => $parentWarehouseId,
            		'addressData' => [],
            ]);
        }
    }

    /**
     * Updates an existing Warehouse model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id) {
    	if(!Yii::$app->params['isMoreWarehousesAvailable'] || !$this->editable) {
    		throw new ForbiddenHttpException(Yii::t('main', 'You cannot complete your request for security reasons.'));
    	}
    
        $model = $this->findModel($id);

        if(Yii::$app->request->isAjax && $model->load(Yii::$app->request->post())) {
        	Yii::$app->response->format = Response::FORMAT_JSON;
        	return ActiveForm::validate($model);
        }
        
        if($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } 
        else {
        	
        	$addressData = Address::find()->where('id = :id', [':id' => $model->address_id])->all();
        	$addressData = ArrayHelper::map($addressData, 'id', function($item) {
        		return $item->getFullAddress();
        	});
        	
            return $this->render('update', [
					'model' => $model,
            		'addressData' => $addressData,
            ]);
        }
    }

    /**
     * Deletes an existing Warehouse model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id) {
    	if(!Yii::$app->params['isMoreWarehousesAvailable'] || !$this->removable) {
    		throw new ForbiddenHttpException(Yii::t('main', 'You cannot complete your request for security reasons.'));
    	}
    
    	$transaction = Yii::$app->db->beginTransaction();
    	$parentWarehouseId = 0;
    	try {
    		$warehouse = $this->findModel($id);
    		$parentWarehouseId = $warehouse->parent_warehouse_id;
    		Warehouse::updateAll(['parent_warehouse_id' => null], ['parent_warehouse_id' => $id]);
    		$warehouse->delete();
    		$transaction->commit();
        }
        catch(\Exception $e) {
        	$transaction->rollBack();
			Yii::$app->getSession()->setFlash('error', Yii::t('main', 'The object cannot be deleted because it is used by other objects.'));
        }

        if(strpos(Yii::$app->request->getReferrer(), Yii::$app->controller->id.'/view/'.$parentWarehouseId) == false) {
        	return $this->redirect(['index']);
        }
        else {
        	return $this->redirect(Yii::$app->request->getReferrer());
        }
    }

    /**
     * Finds the Warehouse model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Warehouse the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id) {
        if(($model = Warehouse::findOne($id)) !== null) {
            return $model;
        } 
        else {
            throw new NotFoundHttpException(Yii::t('main', 'The requested page does not exist. Please verify the address of the page.'));
        }
    }
}
