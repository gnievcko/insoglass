<?php

namespace backend\controllers;

use Yii;
use common\models\ar\CompanyGroup;
use common\models\ars\CompanyGroupSearch;
use backend\components\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\widgets\ActiveForm;
use yii\web\Response;
use yii\web\ForbiddenHttpException;
use yii\data\ActiveDataProvider;
use common\models\ar\CompanyGroupTranslation;
use common\models\ar\CompanyGroupSet;
use common\models\ar\UserCompanySalesman;
use common\helpers\Utility;

/**
 * CompanyGroupController implements the CRUD actions for CompanyGroup model.
 */
class CompanyGroupController extends Controller {

	protected $creatable = true;
	protected $editable = true;
	protected $removable = true;

	/* Behaviours are inherited */

    /**
     * Lists all CompanyGroup models.
     * @return mixed
     */
    public function actionIndex() {
        $searchModel = new CompanyGroupSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
	            'searchModel' => $searchModel,
	            'dataProvider' => $dataProvider,
        		'withoutCreating' => !$this->creatable,
        		'withoutUpdating' => !$this->editable,
        		'withoutDeleting' => !$this->removable,
        ]);
    }

    /**
     * Displays a single CompanyGroup model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id) {
        return $this->render('view', [
				'model' => $this->findModel($id),
        		'companyGroupTranslationDataProvider' => new ActiveDataProvider([
        				'query' => CompanyGroupTranslation::find()->where(['company_group_id' => $id])
        		]),
        		'companyGroupSetDataProvider' => new ActiveDataProvider([
        				'query' => CompanyGroupSet::find()->where(['company_group_id' => $id])
        		]),
        		'userCompanySalesmanDataProvider' => new ActiveDataProvider([
        				'query' => UserCompanySalesman::find()->where(['company_group_id' => $id])
        		]),
        		'withoutUpdating' => !$this->editable,
        		'withoutDeleting' => !$this->removable,
        ]);
    }

    /**
     * Creates a new CompanyGroup model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate() {
    	if(!$this->creatable) {
    		throw new ForbiddenHttpException(Yii::t('main', 'You cannot complete your request for security reasons.'));
    	}
    
        $model = new CompanyGroup();

        if(Yii::$app->request->isAjax && $model->load(Yii::$app->request->post())) {
        	Yii::$app->response->format = Response::FORMAT_JSON;
        	return ActiveForm::validate($model);
        }
        
        if($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } 
        else {
            return $this->render('create', [
					'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing CompanyGroup model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id) {
    	if(!$this->editable) {
    		throw new ForbiddenHttpException(Yii::t('main', 'You cannot complete your request for security reasons.'));
    	}
    
        $model = $this->findModel($id);

        if(Yii::$app->request->isAjax && $model->load(Yii::$app->request->post())) {
        	Yii::$app->response->format = Response::FORMAT_JSON;
        	return ActiveForm::validate($model);
        }
        
        if(!empty($model->is_predefined) || ($model->load(Yii::$app->request->post()) && $model->save())) {
            return $this->redirect(['view', 'id' => $model->id]);
        } 
        else {
            return $this->render('update', [
					'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing CompanyGroup model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id) {
    	if(!$this->removable) {
    		throw new ForbiddenHttpException(Yii::t('main', 'You cannot complete your request for security reasons.'));
    	}
    	
    	$model = $this->findModel($id);
    	
    	if (!empty($model->is_predefined)) {
    		throw new ForbiddenHttpException(Yii::t('main', 'You cannot complete your request for security reasons.'));	
    	}
    	
    	$transaction = Yii::$app->db->beginTransaction();
    	
    	try {
    		CompanyGroupTranslation::deleteAll(['company_group_id' => $id]);
    		CompanyGroupSet::deleteAll(['company_group_id' => $id]);
    		UserCompanySalesman::deleteAll(['company_group_id' => $id]);
    		$this->findModel($id)->delete();
    		$transaction->commit();
        }
        catch(\Exception $e) {
        	$transaction->rollBack();
			Yii::$app->getSession()->setFlash('error', Yii::t('main', 'The object cannot be deleted because it is used by other objects.'));
        }

        return $this->redirect(['index']);
    }

    /**
     * Finds the CompanyGroup model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return CompanyGroup the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id) {
        if(($model = CompanyGroup::findOne($id)) !== null) {
            return $model;
        } 
        else {
            throw new NotFoundHttpException(Yii::t('main', 'The requested page does not exist. Please verify the address of the page.'));
        }
    }
}
