<?php

namespace backend\controllers;

use Yii;
use common\models\ar\EmailConfig;
use common\models\ars\EmailConfigSearch;
use backend\components\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\widgets\ActiveForm;
use yii\web\Response;
use yii\web\ForbiddenHttpException;
use backend\models\EmailConfigTestForm;

/**
 * EmailConfigController implements the CRUD actions for EmailConfig model.
 */
class EmailConfigController extends Controller {

	protected $creatable = true;
	protected $editable = true;
	protected $removable = true;

	/* Behaviours are inherited */

    /**
     * Lists all EmailConfig models.
     * @return mixed
     */
    public function actionIndex() {
        $searchModel = new EmailConfigSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
	            'searchModel' => $searchModel,
	            'dataProvider' => $dataProvider,
        		'withoutCreating' => !$this->creatable,
        		'withoutUpdating' => !$this->editable,
        		'withoutDeleting' => !$this->removable || !$this->isConfigCanBeRemoved(),
        ]);
    }

    /**
     * Displays a single EmailConfig model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id) {
    	$emailConfigTestForm = new EmailConfigTestForm($id);
    	$emailConfigTestForm->email = Yii::$app->params['adminEmail'];
    	
    	if($emailConfigTestForm->load(Yii::$app->request->post()) && $emailConfigTestForm->validate()) {
    		if($emailConfigTestForm->sendMessage()) {
    			Yii::$app->getSession()->setFlash('success', Yii::t('main', 'Test message was sent successfully.'));
    		}
    		else {
    			Yii::$app->getSession()->setFlash('success', Yii::t('main', 'An error occurred during sending test message.'));
    		}
    	}
    	
        return $this->render('view', [
				'model' => $this->findModel($id),
        		'emailConfigTestForm' => $emailConfigTestForm,
        		'withoutUpdating' => !$this->editable,
        		'withoutDeleting' => !$this->removable || !$this->isConfigCanBeRemoved(),
        ]);
    }

    /**
     * Creates a new EmailConfig model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate() {
    	if(!$this->creatable) {
    		throw new ForbiddenHttpException(Yii::t('main', 'You cannot complete your request for security reasons.'));
    	}
    
        $model = new EmailConfig();

        if(Yii::$app->request->isAjax && $model->load(Yii::$app->request->post())) {
        	Yii::$app->response->format = Response::FORMAT_JSON;
        	return ActiveForm::validate($model);
        }
        
        if($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } 
        else {
            return $this->render('create', [
					'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing EmailConfig model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id) {
    	if(!$this->editable) {
    		throw new ForbiddenHttpException(Yii::t('main', 'You cannot complete your request for security reasons.'));
    	}
    
        $model = $this->findModel($id);

        if(Yii::$app->request->isAjax && $model->load(Yii::$app->request->post())) {
        	Yii::$app->response->format = Response::FORMAT_JSON;
        	return ActiveForm::validate($model);
        }
        
        if($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } 
        else {
            return $this->render('update', [
					'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing EmailConfig model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id) {
    	if(!$this->removable || !$this->isConfigCanBeRemoved()) {
    		throw new ForbiddenHttpException(Yii::t('main', 'You cannot complete your request for security reasons.'));
    	}
    
    	$transaction = Yii::$app->db->beginTransaction();
    	
    	try {
    		$this->findModel($id)->delete();
    		$transaction->commit();
        }
        catch(\Exception $e) {
        	$transaction->rollBack();
			Yii::$app->getSession()->setFlash('error', Yii::t('main', 'The object cannot be deleted because it is used by other objects.'));
        }

        return $this->redirect(['index']);
    }

    /**
     * Finds the EmailConfig model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return EmailConfig the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id) {
        if(($model = EmailConfig::findOne($id)) !== null) {
            return $model;
        } 
        else {
            throw new NotFoundHttpException(Yii::t('main', 'The requested page does not exist. Please verify the address of the page.'));
        }
    }
    
    private function isConfigCanBeRemoved() {
    	return EmailConfig::find()->count() > 1 ? true : false;
    }
}
