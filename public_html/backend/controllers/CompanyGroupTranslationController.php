<?php

namespace backend\controllers;

use Yii;
use common\models\ar\CompanyGroupTranslation;
use common\models\ars\CompanyGroupTranslationSearch;
use backend\components\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\widgets\ActiveForm;
use yii\web\Response;
use yii\web\ForbiddenHttpException;

/**
 * CompanyGroupTranslationController implements the CRUD actions for CompanyGroupTranslation model.
 */
class CompanyGroupTranslationController extends Controller {

	protected $creatable = true;
	protected $editable = true;
	protected $removable = true;

	/* Behaviours are inherited */

    /**
     * Lists all CompanyGroupTranslation models.
     * @return mixed
     */
    public function actionIndex() {
        $searchModel = new CompanyGroupTranslationSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
	            'searchModel' => $searchModel,
	            'dataProvider' => $dataProvider,
        		'withoutCreating' => !$this->creatable,
        		'withoutUpdating' => !$this->editable,
        		'withoutDeleting' => !$this->removable,
        ]);
    }

    /**
     * Displays a single CompanyGroupTranslation model.
     * @param integer $company_group_id
     * @param integer $language_id
     * @return mixed
     */
    public function actionView($company_group_id, $language_id) {
        return $this->render('view', [
				'model' => $this->findModel($company_group_id, $language_id),
        		'withoutUpdating' => !$this->editable,
        		'withoutDeleting' => !$this->removable,
        ]);
    }

    /**
     * Creates a new CompanyGroupTranslation model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate($companyGroupId = null) {
    	if(!$this->creatable) {
    		throw new ForbiddenHttpException(Yii::t('main', 'You cannot complete your request for security reasons.'));
    	}
    
        $model = new CompanyGroupTranslation();

        if(Yii::$app->request->isAjax && $model->load(Yii::$app->request->post())) {
        	Yii::$app->response->format = Response::FORMAT_JSON;
        	return ActiveForm::validate($model);
        }
        
        if($model->load(Yii::$app->request->post()) && $model->save()) {
        	if(!empty($companyGroupId)) {
        		return $this->redirect(['company-group/view', 'id' => $model->company_group_id]);
        	}
        	else {
        		return $this->redirect(['view', 'company_group_id' => $model->company_group_id, 'language_id' => $model->language_id]);
        	}           
        } 
        else {
            return $this->render('create', [
					'model' => $model,
            		'companyGroupId' => $companyGroupId,
            ]);
        }
    }

    /**
     * Updates an existing CompanyGroupTranslation model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $company_group_id
     * @param integer $language_id
     * @return mixed
     */
    public function actionUpdate($company_group_id, $language_id) {
    	if(!$this->editable) {
    		throw new ForbiddenHttpException(Yii::t('main', 'You cannot complete your request for security reasons.'));
    	}
    
        $model = $this->findModel($company_group_id, $language_id);

        if(Yii::$app->request->isAjax && $model->load(Yii::$app->request->post())) {
        	Yii::$app->response->format = Response::FORMAT_JSON;
        	return ActiveForm::validate($model);
        }
        
        if($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'company_group_id' => $model->company_group_id, 'language_id' => $model->language_id]);
        } 
        else {
            return $this->render('update', [
					'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing CompanyGroupTranslation model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $company_group_id
     * @param integer $language_id
     * @return mixed
     */
    public function actionDelete($company_group_id, $language_id) {
    	if(!$this->removable) {
    		throw new ForbiddenHttpException(Yii::t('main', 'You cannot complete your request for security reasons.'));
    	}
    
    	$transaction = Yii::$app->db->beginTransaction();
    	
    	try {
    		$this->findModel($company_group_id, $language_id)->delete();
    		$transaction->commit();
        }
        catch(\Exception $e) {
        	$transaction->rollBack();
			Yii::$app->getSession()->setFlash('error', Yii::t('main', 'The object cannot be deleted because it is used by other objects.'));
        }

        if(strpos(Yii::$app->request->getReferrer(), Yii::$app->controller->id.'/view') !== false) {
        	return $this->redirect(['index']);
        }
        else {
        	return $this->redirect(Yii::$app->request->getReferrer());
        }
        
        
        
        //return $this->redirect(['index']);
    }

    /**
     * Finds the CompanyGroupTranslation model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $company_group_id
     * @param integer $language_id
     * @return CompanyGroupTranslation the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($company_group_id, $language_id) {
        if(($model = CompanyGroupTranslation::findOne(['company_group_id' => $company_group_id, 'language_id' => $language_id])) !== null) {
            return $model;
        } 
        else {
            throw new NotFoundHttpException(Yii::t('main', 'The requested page does not exist. Please verify the address of the page.'));
        }
    }
}
