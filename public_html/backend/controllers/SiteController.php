<?php
namespace backend\controllers;

use Yii;
use yii\filters\AccessControl;
use backend\components\Controller;
use common\models\LoginForm;
use common\helpers\Utility;
use yii\filters\VerbFilter;
use yii\helpers\Json;
use yii\web\UploadedFile;
use yii\helpers\FileHelper;
use common\helpers\UploadHelper;
use common\models\ar\User;
use common\models\ar\Order;
use common\models\ar\Product;
use common\models\ar\Task;
use yii\helpers\Url;
use yii\web\Session;

/**
 * Site controller
 */
class SiteController extends Controller {

	/**
	 * @inheritdoc
	 */
	public function behaviors() {
		return [
				'access' => [
						'class' => AccessControl::className(),
						'rules' => [
								[
										'actions' => ['login', 'error', 'maintenance'],
										'allow' => true,
								],
								[
										'actions' => ['logout', 'index', 'upload-image-from-editor', 
												'find-company', 'find-address', 'find-users', 'find-task', 'find-orders', 'find-products', 'find-company-group'],
										'allow' => true,
										'roles' => ['@'],
								],
								[
										'roles' => [Utility::ROLE_ADMIN]
								]
						],
				],
				'verbs' => [
						'class' => VerbFilter::className(),
						'actions' => [
								'logout' => ['post'],
						],
				],
		];
	}

	/**
	 * @inheritdoc
	 */
	public function actions() {
		return [
				'error' => [
						'class' => 'yii\web\ErrorAction',
				],
		];
	}

	public function actionMaintenance() {
	    if(!Yii::$app->params['maintenance']) {
	        return $this->redirect(['/']);
	    }    
		return $this->render('maintenance');
	}

	public function actionIndex() {
		return $this->render('index');
	}

	public function actionLogin() {
		if(!\Yii::$app->user->isGuest) {
			return $this->goHome();
		}

		$model = new LoginForm();

		if(Yii::$app->request->isAjax && $model->load(Yii::$app->request->post())) {
			Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
			return \yii\widgets\ActiveForm::validate($model);
		}

		if($model->load(Yii::$app->request->post()) && $model->login()) {
			return $this->goBack();
		}
		else {
			return $this->render('login', [
					'model' => $model,
			]);
		}
	}

	public function actionLogout() {
		Yii::$app->user->logout();

		return $this->goHome();
	}	
	
	public function actionFindCompany() {
		$term = isset($_GET['term']) ? $_GET['term'] : null;
		$page = isset($_GET['page']) ? intval($_GET['page']) : 1;
		$size = isset($_GET['size']) ? intval($_GET['size']) : null;
		$excludedCompanyId = isset($_GET['excludedCompanyId']) ? intval($_GET['excludedCompanyId']) : 0;
		
		if(empty($term) || empty($page) || empty($size)) {
			Yii::app()->end();
		}
		
		$count = Yii::$app->db->createCommand('
					SELECT COUNT(c.id)
					FROM company c
					WHERE LOWER(c.name) LIKE :term
					AND c.is_artificial = 0
					AND c.is_active = 1
					AND (0 = :excludedCompanyId OR c.id <> :excludedCompanyId)  
				')
				->bindValue(':term', '%'.strtolower($term).'%')
				->bindValue(':excludedCompanyId', intval($excludedCompanyId))
				->queryScalar();

		$result = Yii::$app->db->createCommand('
					SELECT c.id, c.name
					FROM company c
					WHERE LOWER(c.name) LIKE :term
					AND c.is_artificial = 0 
					AND c.is_active = 1
					AND (0 = :excludedCompanyId OR c.id <> :excludedCompanyId)  
					ORDER BY c.name
					LIMIT :offset, :limit
				')
				->bindValue(':excludedCompanyId', intval($excludedCompanyId))
				->bindValue(':term', '%'.strtolower($term).'%')
				->bindValue(':offset', ($page - 1) * $size)
				->bindValue(':limit', $size)
				->queryAll();
				
		echo Json::encode(['items' => $result, 'more' => (intval($count) > $page * $size)]);
		Yii::$app->end();
	}
	
	public function actionFindCompanyGroup() {
		$term = isset($_GET['term']) ? $_GET['term'] : null;
		$page = isset($_GET['page']) ? intval($_GET['page']) : 1;
		$size = isset($_GET['size']) ? intval($_GET['size']) : null;
	
		if(empty($term) || empty($page) || empty($size)) {
			Yii::app()->end();
		}
	
		$count = Yii::$app->db->createCommand('
					SELECT COUNT(cg.id)
					FROM company_group cg
					LEFT JOIN company_group_translation cgt
					ON cg.id = cgt.company_group_id
					LEFT JOIN language l on cgt.language_id = l.id		
					WHERE (
                        (LOWER(cgt.name) LIKE :term AND l.symbol = :lsymbol)
                        OR
                        (cgt.name IS NULL AND cg.symbol LIKE :term)
                    )    
					AND cg.is_predefined = 0
				')
					->bindValue(':term', '%'.strtolower($term).'%')
					->bindvalue(':lsymbol', Yii::$app->language)
					->queryScalar();
	
		$result = Yii::$app->db->createCommand('
					SELECT cg.id, COALESCE(cgt.name, cg.symbol) as name
					FROM company_group cg
					LEFT JOIN company_group_translation cgt
                        ON cg.id = cgt.company_group_id
					LEFT JOIN language l on cgt.language_id = l.id
					WHERE (
                        (LOWER(cgt.name) LIKE :term AND l.symbol = :lsymbol)
                        OR
                        (cgt.name IS NULL AND cg.symbol LIKE :term)
                    )
					AND cg.is_predefined = 0
					ORDER BY cg.symbol
					LIMIT :offset, :limit
				')
					->bindValue(':term', '%'.strtolower($term).'%')
					->bindValue(':offset', ($page - 1) * $size)
					->bindValue(':limit', $size)
					->bindvalue(':lsymbol', Yii::$app->language)
					->queryAll();
		
		echo Json::encode(['items' => $result, 'more' => (intval($count) > $page * $size)]);
		Yii::$app->end();
	}
	
	public function actionFindAddress() {
		$term = isset($_GET['term']) ? $_GET['term'] : null;
		$page = isset($_GET['page']) ? intval($_GET['page']) : 1;
		$size = isset($_GET['size']) ? intval($_GET['size']) : null;
	
		if(empty($term) || empty($page) || empty($size)) {
			Yii::app()->end();
		}
	
		$count = Yii::$app->db->createCommand('
					SELECT COUNT(a.id)
					FROM address a
					JOIN city c ON a.city_id = c.id
					LEFT JOIN province p ON c.province_id = p.id
					LEFT JOIN country cr ON p.country_id = cr.id
					LEFT JOIN country_translation crt ON cr.id = crt.country_id
					LEFT JOIN language l ON crt.language_id = l.id
					WHERE (
						LOWER(a.main) LIKE :term
						OR LOWER(a.complement) LIKE :term
						OR LOWER(c.name) LIKE :term
						OR LOWER(c.zip_code) LIKE :term
						OR LOWER(p.name) LIKE :term
						OR LOWER(crt.name) LIKE :term
					)
					AND l.symbol = :languageSymbol
				')
				->bindValue(':term', '%'.strtolower($term).'%')
				->bindValue(':languageSymbol', Yii::$app->language)
				->queryScalar();

		$result = Yii::$app->db->createCommand('
					SELECT a.id, CONCAT(a.main, COALESCE(CONCAT(" ", a.complement), ""), ", ", c.zip_code, " ", c.name, ", ", p.name, ", ", COALESCE(crt.name, cr.symbol)) as name
					FROM address a
					JOIN city c ON a.city_id = c.id
					LEFT JOIN province p ON c.province_id = p.id
					LEFT JOIN country cr ON p.country_id = cr.id
					LEFT JOIN country_translation crt ON cr.id = crt.country_id
					LEFT JOIN language l ON crt.language_id = l.id
					WHERE (
						LOWER(a.main) LIKE :term
						OR LOWER(a.complement) LIKE :term
						OR LOWER(c.name) LIKE :term
						OR LOWER(c.zip_code) LIKE :term
						OR LOWER(p.name) LIKE :term
						OR LOWER(crt.name) LIKE :term
					)
					AND l.symbol = :languageSymbol
					ORDER BY a.main
					LIMIT :offset, :limit
				')
				->bindValue(':term', '%'.strtolower($term).'%')
				->bindValue(':languageSymbol', Yii::$app->language)
				->bindValue(':offset', ($page - 1) * $size)
				->bindValue(':limit', $size)
				->queryAll();
	
		echo Json::encode(['items' => $result, 'more' => (intval($count) > $page * $size)]);
		Yii::$app->end();
	}
	
	public function actionUploadImageFromEditor() {	
		$uploadedFile = UploadedFile::getInstanceByName('upload');
		$mime = FileHelper::getMimeType($uploadedFile->tempName);		
		$nameSet = UploadHelper::generatePath($uploadedFile->extension);
		$url = '';
	
		if ($uploadedFile == null) {
			$message = Yii::t('main', 'No file has been uploaded.');
		}
		else if ($uploadedFile->size == 0) {
			$message = Yii::t('main', 'The file seems to be empty.');
		}
		else if ($mime != "image/jpeg" && $mime != "image/png") {
			$message = Yii::t('main', 'The image must be in either JPG or PNG format.');
		}
		else if ($uploadedFile->tempName == null) {
			$message = Yii::t('main', 'Unrecognized file name');
		}
		else {
			$message = "";
			$result = $uploadedFile->saveAs($nameSet['fullName']) ? $nameSet['fullName'] : false;
			
			if($result) {
				//$url = Yii::$app->request->getFrontendBaseUrl().'storage/index?f='.$nameSet['name'];
				$url = Url::to('@web/storage/index?f='.$nameSet['name']);
			}
			else {
				$message = Yii::t('main', 'Error during uploading file. Please check server configuration.');
			}
		}
		$funcNum = $_GET['CKEditorFuncNum'] ;
		echo "<script type='text/javascript'>window.parent.CKEDITOR.tools.callFunction($funcNum, '$url', '$message');</script>";
	}

    public function actionFindUsers(array $roles = null) {

        $request = Yii::$app->request;
        $term = $request->get('term');
        $page = $request->get('page', 1);
        $size = $request->get('size');
        $type = $request->get('type');
        $onlyActives = $request->get('onlyActives');

		if(empty($term) || empty($page) || empty($size)) {
			Yii::$app->end();
		}

        $query = User::find();
        if($type == User::USER_TYPE_EMPLOYEE) {
            $query->employee();
        }
            
        $query->andWhere(['or', 
	                ['like', 'CONCAT(first_name,  " ",  last_name)', $term],
	                ['like', 'email', $term],
	            ]);

        if($onlyActives) {
        	$query->andWhere(['is_active' => true]);
        }
        
        if ($roles != null){
        	$query->join('INNER JOIN', 'user_role', 'user_role.user_id = user.id')
        	->join('INNER JOIN', 'role', 'user_role.role_id = role.id')
        	->andWhere(['IN', 'role.symbol', $roles]);
        }
		
        $matchedUsersCount = $query->count();

        $users = $query
            ->limit($size)
            ->offset($size*($page-1))
            ->all();

        echo json_encode([
            'items' => array_map(function($user) {
                return ['id' => $user->id, 'first_name' => $user->first_name, 'last_name' => $user->last_name, 'email' => $user->email]; 
            }, $users), 
            'more' => (intval($matchedUsersCount) > $page*$size)]);
        Yii::$app->end();
    }

    public function actionFindOrders() {

        $request = Yii::$app->request;
        $term = $request->get('term');
        $page = $request->get('page', 1);
        $size = $request->get('size');

		if(empty($term) || empty($page) || empty($size)) {
			Yii::$app->end();
		}
        
		$query = Order::find()->andWhere('is_active=1')
        ->andWhere('is_deleted=0');

        $query->andWhere(['or', 
                ['like', 'number', $term],
                ['like', 'title', $term],
            ]);
		
        $matchedOrdersCount = $query->count();

        $orders = $query
            ->limit($size)
            ->offset($size*($page-1))
            ->all();

        echo json_encode([
            'items' => array_map(function($order) {
                return ['id' => $order->id, 'number' => $order->number, 'title' => $order->title]; 
            }, $orders), 
            'more' => (intval($matchedOrdersCount) > $page*$size)]);
        Yii::$app->end();
    }

    public function actionFindProducts() {

        $request = Yii::$app->request;
        $term = $request->get('term');
        $page = $request->get('page', 1);
        $size = $request->get('size');

		if(empty($term) || empty($page) || empty($size)) {
			Yii::$app->end();
		}
        
		$query = Product::find()->andWhere('is_active=1');

		$query->join('INNER JOIN', 'product_translation', 'product.id = product_translation.product_id');

        $query->andWhere(['or', 
                ['like', 'symbol', $term],
                ['like', 'name', $term],
            ]);

        $matchedProductsCount = $query->count();

        $products = $query
            ->limit($size)
            ->offset($size*($page-1))
            ->all();

        echo json_encode([
            'items' => array_map(function($product) {
                return ['id' => $product->id, 'symbol' => $product->symbol, 'name' => $product->getProductTranslations()->one()->name]; 
            }, $products), 
            'more' => (intval($matchedProductsCount) > $page*$size)]);
        Yii::$app->end();
    }

    public function actionFindTask() {

        $request = Yii::$app->request;
        $term = $request->get('term');
        $page = $request->get('page', 1);
        $size = $request->get('size');

		if(empty($term) || empty($page) || empty($size)) {
			Yii::$app->end();
		}
        
		$query = Task::find()->andWhere('is_active=1');

        $query->andWhere(['like', 'title', $term]);
		
        $matchedTaskCount = $query->count();

        $tasks = $query
            ->limit($size)
            ->offset($size*($page-1))
            ->all();

        echo json_encode([
            'items' => array_map(function($task) {
                return ['id' => $task->id, 'title' => $task->title]; 
            }, $tasks), 
            'more' => (intval($matchedTaskCount) > $page*$size)]);
        Yii::$app->end();
    }
}
