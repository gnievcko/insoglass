<?php

namespace backend\controllers;

use Yii;
use common\models\ar\EmailTemplate;
use common\models\ars\EmailTemplateSearch;
use backend\components\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\widgets\ActiveForm;
use yii\web\Response;
use yii\web\ForbiddenHttpException;
use yii\data\ActiveDataProvider;
use backend\models\EmailTemplateTestForm;
use common\components\MessageTrait;

/**
 * EmailTemplateController implements the CRUD actions for EmailTemplate model.
 */
class EmailTemplateController extends Controller {

	use MessageTrait;
	
	protected $creatable = true;
	protected $editable = true;
	protected $removable = true;

	/* Behaviours are inherited */

    /**
     * Lists all EmailTemplate models.
     * @return mixed
     */
    public function actionIndex() {
        $searchModel = new EmailTemplateSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
	            'searchModel' => $searchModel,
	            'dataProvider' => $dataProvider,
        		'withoutCreating' => !$this->creatable,
        		'withoutUpdating' => !$this->editable,
        		'withoutDeleting' => !$this->removable,
        ]);
    }

    /**
     * Displays a single EmailTemplate model.
     * @param integer $email_type_id
     * @param integer $language_id
     * @return mixed
     */
    public function actionView($email_type_id, $language_id) {
    	$emailTemplateTestForm = new EmailTemplateTestForm($email_type_id, $language_id);
    	$emailTemplateTestForm->email = Yii::$app->params['adminEmail'];
    	 
    	if($emailTemplateTestForm->load(Yii::$app->request->post()) && $emailTemplateTestForm->validate()) {
    		if($emailTemplateTestForm->sendMessage($this)) {
    			Yii::$app->getSession()->setFlash('success', Yii::t('main', 'Test message was sent successfully.'));
    		}
    		else {
    			Yii::$app->getSession()->setFlash('error', Yii::t('main', 'An error occurred during sending test message.'));
    		}
    	}    	
    	
        return $this->render('view', [
				'model' => $this->findModel($email_type_id, $language_id),
        		'withoutUpdating' => !$this->editable,
        		'withoutDeleting' => !$this->removable,
        		'emailTemplateTestForm' => $emailTemplateTestForm,
        ]);
    }
    
    public function actionTemplate($email_type_id, $language_id) {
    	echo $this->findModel($email_type_id, $language_id)->content_html;
    }

    /**
     * Creates a new EmailTemplate model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate($emailTypeId = null) {
    	if(!$this->creatable) {
    		throw new ForbiddenHttpException(Yii::t('main', 'You cannot complete your request for security reasons.'));
    	}
    
        $model = new EmailTemplate();

        if(Yii::$app->request->isAjax && $model->load(Yii::$app->request->post())) {
        	Yii::$app->response->format = Response::FORMAT_JSON;
        	return ActiveForm::validate($model);
        }
        
        if($model->load(Yii::$app->request->post()) && $model->save()) {
            if(!empty($emailTypeId)) {
            	return $this->redirect(['email-type/view', 'id' => $model->email_type_id]);
            }
            else {
            	return $this->redirect(['view', 'email_type_id' => $model->email_type_id, 'language_id' => $model->language_id]);
            }
        } 
        else {
            return $this->render('create', [
					'model' => $model,
            		'emailTypeId' => $emailTypeId,
            ]);
        }
    }

    /**
     * Updates an existing EmailTemplate model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $email_type_id
     * @param integer $language_id
     * @return mixed
     */
    public function actionUpdate($email_type_id, $language_id) {
    	if(!$this->editable) {
    		throw new ForbiddenHttpException(Yii::t('main', 'You cannot complete your request for security reasons.'));
    	}
    
        $model = $this->findModel($email_type_id, $language_id);

        if(Yii::$app->request->isAjax && $model->load(Yii::$app->request->post())) {
        	Yii::$app->response->format = Response::FORMAT_JSON;
        	return ActiveForm::validate($model);
        }
        
        if($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'email_type_id' => $model->email_type_id, 'language_id' => $model->language_id]);
        } 
        else {
            return $this->render('update', [
					'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing EmailTemplate model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $email_type_id
     * @param integer $language_id
     * @return mixed
     */
    public function actionDelete($email_type_id, $language_id) {
    	if(!$this->removable) {
    		throw new ForbiddenHttpException(Yii::t('main', 'You cannot complete your request for security reasons.'));
    	}
    
    	$transaction = Yii::$app->db->beginTransaction();
    	
    	try {
    		$this->findModel($email_type_id, $language_id)->delete();
    		$transaction->commit();
        }
        catch(\Exception $e) {
        	$transaction->rollBack();
			Yii::$app->getSession()->setFlash('error', Yii::t('main', 'The object cannot be deleted because it is used by other objects.'));
        }

	    if(strpos(Yii::$app->request->getReferrer(), Yii::$app->controller->id.'/view') !== false) {
        	return $this->redirect(['index']);
        }
        else {
        	return $this->redirect(Yii::$app->request->getReferrer());
        }	
    }

    /**
     * Finds the EmailTemplate model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $email_type_id
     * @param integer $language_id
     * @return EmailTemplate the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($email_type_id, $language_id) {
        if(($model = EmailTemplate::findOne(['email_type_id' => $email_type_id, 'language_id' => $language_id])) !== null) {
            return $model;
        } 
        else {
            throw new NotFoundHttpException(Yii::t('main', 'The requested page does not exist. Please verify the address of the page.'));
        }
    }
}
