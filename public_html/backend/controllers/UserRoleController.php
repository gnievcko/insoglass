<?php

namespace backend\controllers;

use Yii;
use common\models\ar\UserRole;
use console\controllers\RbacController;
use common\models\ars\UserRoleSearch;
use common\models\ar\Role;
use backend\components\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\widgets\ActiveForm;
use yii\web\Response;
use yii\web\ForbiddenHttpException;
use common\helpers\Utility;

/**
 * UserRoleController implements the CRUD actions for UserRole model.
 */
class UserRoleController extends Controller {

	protected $creatable = true;
	protected $editable = false;
	protected $removable = true;

	/* Behaviours are inherited */

    /**
     * Lists all UserRole models.
     * @return mixed
     */
    public function actionIndex() {
        $searchModel = new UserRoleSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
	            'searchModel' => $searchModel,
	            'dataProvider' => $dataProvider,
        		'withoutCreating' => !$this->creatable,
        		'withoutUpdating' => !$this->editable,
        		'withoutDeleting' => !$this->removable,
        ]);
    }

    /**
     * Displays a single UserRole model.
     * @param integer $user_id
     * @param integer $role_id
     * @return mixed
     */
    public function actionView($user_id, $role_id) {
        return $this->render('view', [
				'model' => $this->findModel($user_id, $role_id),
        		'withoutUpdating' => !$this->editable,
        		'withoutDeleting' => !$this->removable,
        ]);
    }

    /**
     * Creates a new UserRole model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate($userId = null) {
    	if(!$this->creatable) {
    		throw new ForbiddenHttpException(Yii::t('main', 'You can not complete your request for security reasons.'));
    	}
    
        $model = new UserRole();

        if(Yii::$app->request->isAjax && $model->load(Yii::$app->request->post())) {
        	Yii::$app->response->format = Response::FORMAT_JSON;
            if($model->role->symbol == Utility::ROLE_CLIENT) {
                throw new ForbiddenHttpException(Yii::t('main', 'You can not complete your request for security reasons.'));
            }

        	return ActiveForm::validate($model);
        }
        
        if($model->load(Yii::$app->request->post()) && $model->save()) {
        	$rbacController = new RbacController('rbac', Yii::$app->module);
        	$rbacController->assignRoleToUser($model->role->symbol, $model->user->id);

            if(empty($userId)) {
                return $this->redirect(['view', 'role_id' => $model->role_id, 'user_id' => $model->user_id]);
            }
            else {
        		return $this->redirect(['user/view', 'id' => $model->user_id]);
            }
        } 
        else {
            return $this->render('create', [
					'model' => $model,
                    'userData' => [], 
                    'userId' => $userId,
            ]);
        }
    }

    /**
     * Updates an existing UserRole model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $user_id
     * @param integer $role_id
     * @return mixed
     */
    public function actionUpdate($user_id, $role_id) {
    	if(!$this->editable) {
    		throw new ForbiddenHttpException(Yii::t('main', 'You can not complete your request for security reasons.'));
    	}
    
        $model = $this->findModel($user_id, $role_id);

        if(Yii::$app->request->isAjax && $model->load(Yii::$app->request->post())) {
        	Yii::$app->response->format = Response::FORMAT_JSON;
        	return ActiveForm::validate($model);
        }
        
        if($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'user_id' => $model->user_id, 'role_id' => $model->role_id]);
        } 
        else {
            return $this->render('update', [
					'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing UserRole model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $user_id
     * @param integer $role_id
     * @return mixed
     */
    public function actionDelete($user_id, $role_id) {
        $userRole = $this->findModel($user_id, $role_id);
    	if(!$this->removable || ($user_id == 1 && $role_id == 1) || $userRole->role->symbol == Utility::ROLE_CLIENT) {
    		throw new ForbiddenHttpException(Yii::t('main', 'You can not complete your request for security reasons.'));
    	}
    
    	$transaction = Yii::$app->db->beginTransaction();
    	
    	try {
            $userRole->delete();
    		$transaction->commit();

    		$rbacController = new RbacController('rbac', Yii::$app->module);
    		$rbacController->revokeRoleFromUser(Role::find()->where(['id' => $role_id])->one()->symbol, $user_id);
        }
        catch(\Exception $e) {
        	$transaction->rollBack();
			Yii::$app->getSession()->setFlash('error', Yii::t('main', 'The object cannot be deleted because it is used by other objects.'));
        }

    	if(strpos(Yii::$app->request->getReferrer(), Yii::$app->controller->id.'/view') !== false) {
            return $this->redirect(['index']);
        }
        else {
        	return $this->redirect(Yii::$app->request->getReferrer());
        }
    }

    /**
     * Finds the UserRole model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $user_id
     * @param integer $role_id
     * @return UserRole the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($user_id, $role_id) {
        if(($model = UserRole::findOne(['user_id' => $user_id, 'role_id' => $role_id])) !== null) {
            return $model;
        } 
        else {
            throw new NotFoundHttpException(Yii::t('main', 'The requested page does not exist. Please verify the address of the page.'));
        }
    }
}
