<?php

namespace backend\controllers;

use Yii;
use common\models\ar\ActivityTranslation;
use common\models\ars\ActivityTranslationSearch;
use backend\components\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\widgets\ActiveForm;
use yii\web\Response;
use yii\web\ForbiddenHttpException;

/**
 * ActivityTranslationController implements the CRUD actions for ActivityTranslation model.
 */
class ActivityTranslationController extends Controller {

	protected $creatable = true;
	protected $editable = true;
	protected $removable = true;

	/* Behaviours are inherited */

    /**
     * Lists all ActivityTranslation models.
     * @return mixed
     */
    public function actionIndex() {
        $searchModel = new ActivityTranslationSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
	            'searchModel' => $searchModel,
	            'dataProvider' => $dataProvider,
        		'withoutCreating' => !$this->creatable,
        		'withoutUpdating' => !$this->editable,
        		'withoutDeleting' => !$this->removable,
        ]);
    }

    /**
     * Displays a single ActivityTranslation model.
     * @param integer $activity_id
     * @param integer $language_id
     * @return mixed
     */
    public function actionView($activity_id, $language_id) {
        return $this->render('view', [
				'model' => $this->findModel($activity_id, $language_id),
        		'withoutUpdating' => !$this->editable,
        		'withoutDeleting' => !$this->removable,
        ]);
    }

    /**
     * Creates a new ActivityTranslation model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate() {
    	if(!$this->creatable) {
    		throw new ForbiddenHttpException(Yii::t('main', 'You cannot complete your request for security reasons.'));
    	}
    
        $model = new ActivityTranslation();

        if(Yii::$app->request->isAjax && $model->load(Yii::$app->request->post())) {
        	Yii::$app->response->format = Response::FORMAT_JSON;
        	return ActiveForm::validate($model);
        }
        
        if($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'activity_id' => $model->activity_id, 'language_id' => $model->language_id]);
        } 
        else {
            return $this->render('create', [
					'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing ActivityTranslation model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $activity_id
     * @param integer $language_id
     * @return mixed
     */
    public function actionUpdate($activity_id, $language_id) {
    	if(!$this->editable) {
    		throw new ForbiddenHttpException(Yii::t('main', 'You cannot complete your request for security reasons.'));
    	}
    
        $model = $this->findModel($activity_id, $language_id);

        if(Yii::$app->request->isAjax && $model->load(Yii::$app->request->post())) {
        	Yii::$app->response->format = Response::FORMAT_JSON;
        	return ActiveForm::validate($model);
        }
        
        if($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'activity_id' => $model->activity_id, 'language_id' => $model->language_id]);
        } 
        else {
            return $this->render('update', [
					'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing ActivityTranslation model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $activity_id
     * @param integer $language_id
     * @return mixed
     */
    public function actionDelete($activity_id, $language_id) {
    	if(!$this->removable) {
    		throw new ForbiddenHttpException(Yii::t('main', 'You cannot complete your request for security reasons.'));
    	}
    
    	$transaction = Yii::$app->db->beginTransaction();
    	
    	try {
    		$this->findModel($activity_id, $language_id)->delete();
    		$transaction->commit();
        }
        catch(\Exception $e) {
        	$transaction->rollBack();
			Yii::$app->getSession()->setFlash('error', Yii::t('main', 'The object cannot be deleted because it is used by other objects.'));
        }

        return $this->redirect(['index']);
    }

    /**
     * Finds the ActivityTranslation model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $activity_id
     * @param integer $language_id
     * @return ActivityTranslation the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($activity_id, $language_id) {
        if(($model = ActivityTranslation::findOne(['activity_id' => $activity_id, 'language_id' => $language_id])) !== null) {
            return $model;
        } 
        else {
            throw new NotFoundHttpException(Yii::t('main', 'The requested page does not exist. Please verify the address of the page.'));
        }
    }
}
