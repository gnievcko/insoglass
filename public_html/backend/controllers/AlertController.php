<?php

namespace backend\controllers;

use Yii;
use common\models\ar\Alert;
use common\models\ar\AlertHistory;
use common\models\ar\Company;
use common\models\ar\Task;
use common\models\ar\Product;
use common\models\ar\Order;
use common\models\ar\User;
use common\models\ars\AlertSearch;
use backend\components\Controller;
use common\alerts\AlertHandler;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use yii\web\Response;
use yii\web\ForbiddenHttpException;
use yii\db\Expression;

/**
 * AlertController implements the CRUD actions for Alert model.
 */
class AlertController extends Controller {

	protected $creatable = true;
	protected $editable = true;
	protected $removable = true;

	/* Behaviours are inherited */

    /**
     * Lists all Alert models.
     * @return mixed
     */
    public function actionIndex() {
        $searchModel = new AlertSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
			'searchModel' => $searchModel,
	        'dataProvider' => $dataProvider,
        	'withoutCreating' => !$this->creatable,
        	'withoutUpdating' => !$this->editable,
        	'withoutDeleting' => !$this->removable,
        ]);
    }

    /**
     * Displays a single Alert model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id) {
        return $this->render('view', [
			'model' => $this->findModel($id),
        	'withoutUpdating' => !$this->editable,
        	'withoutDeleting' => !$this->removable,
        ]);
    }

    /**
     * Creates a new Alert model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate() {
    	if(!$this->creatable) {
    		throw new ForbiddenHttpException(Yii::t('main', 'You cannot complete your request for security reasons.'));
    	}
    
        $model = new Alert();

        if(Yii::$app->request->isAjax && $model->load(Yii::$app->request->post())) {
        	Yii::$app->response->format = Response::FORMAT_JSON;
        	return ActiveForm::validate($model);
        }
        
        if($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } 
        else {
            return $this->render('create', [
				'model' => $model,
                'companyData' => [],
                'taskData' => [],
                'productData' => [],
                'orderData' => [],
                'userData' => [],
            ]);
        }
    }

    /**
     * Updates an existing Alert model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id) {
    	if(!$this->editable) {
    		throw new ForbiddenHttpException(Yii::t('main', 'You cannot complete your request for security reasons.'));
    	}
    
        $model = $this->findModel($id);

        if(Yii::$app->request->isAjax && $model->load(Yii::$app->request->post())) {
        	Yii::$app->response->format = Response::FORMAT_JSON;
        	return ActiveForm::validate($model);
        }
        
        if($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } 
        else {

            $companyData = Company::find()->where('id = :id', [':id' => $model->company_id])->all();
            $companyData = ArrayHelper::map($companyData, 'id', 'name');

            $taskData = Task::find()->where('id = :id', [':id' => $model->task_id])->all();
            $taskData = ArrayHelper::map($taskData, 'id', 'title');

            $productData = Product::find()->where('id = :id', [':id' => $model->product_id])->all();
            $productData = ArrayHelper::map($productData, 'id', 'symbol');

            $orderData = Order::find()->where('id = :id', [':id' => $model->order_id])->all();
            $orderData = ArrayHelper::map($orderData, 'id', 'number');

            $userData = User::find()->where('id = :id', [':id' => $model->user_id])->all();
            $userData = ArrayHelper::map($userData, 'id', 'email');

            return $this->render('update', [
				'model' => $model,
                'companyData' => $companyData,
                'taskData' => $taskData,
                'productData' => $productData,
                'orderData' => $orderData,
                'userData' => $userData,
            ]);
        }
    }

    /**
     * Deletes an existing Alert model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id) {
    	if(!$this->removable) {
    		throw new ForbiddenHttpException(Yii::t('main', 'You cannot complete your request for security reasons.'));
    	}
    
        AlertHandler::complete($id);

        return $this->redirect(['index']);
    }

    /**
     * Finds the Alert model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Alert the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id) {
        if(($model = Alert::findOne($id)) !== null) {
            return $model;
        } 
        else {
            throw new NotFoundHttpException(Yii::t('main', 'The requested page does not exist. Please verify the address of the page.'));
        }
    }
}
