<?php

namespace backend\controllers;

use Yii;
use common\models\ar\Company;
use common\models\ars\CompanySearch;
use backend\components\Controller;
use yii\web\NotFoundHttpException;
use yii\widgets\ActiveForm;
use yii\web\Response;
use yii\web\ForbiddenHttpException;
use yii\helpers\ArrayHelper;
use common\models\ar\Address;
use common\helpers\CompanyHelper;

/**
 * CompanyController implements the CRUD actions for Company model.
 */
class CompanyController extends Controller {

	protected $creatable = true;
	protected $editable = true;
	protected $removable = true;

	/* Behaviours are inherited */

    /**
     * Lists all Company models.
     * @return mixed
     */
    public function actionIndex() {
        $searchModel = new CompanySearch();        
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
	            'searchModel' => $searchModel,
	            'dataProvider' => $dataProvider,
        		'withoutCreating' => !$this->creatable,
        		'withoutUpdating' => !$this->editable,
        		'withoutDeleting' => !$this->removable,
        ]);
    }

    /**
     * Displays a single Company model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id) {
        return $this->render('view', [
				'model' => $this->findModel($id),
        		'withoutUpdating' => !$this->editable,
        		'withoutDeleting' => !$this->removable,
        ]);
    }

    /**
     * Creates a new Company model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate() {
    	if(!$this->creatable) {
    		throw new ForbiddenHttpException(Yii::t('main', 'You cannot complete your request for security reasons.'));
    	}
    
        $model = new Company();

        if(Yii::$app->request->isAjax && $model->load(Yii::$app->request->post())) {
        	Yii::$app->response->format = Response::FORMAT_JSON;
        	return ActiveForm::validate($model);
        }
        
        if($model->load(Yii::$app->request->post()) && $model->save()) {
        	CompanyHelper::addNewCompanyGroup($model);
            return $this->redirect(['view', 'id' => $model->id]);
        } 
        else {
            return $this->render('create', [
					'model' => $model,
            		'companyData' => [],
            		'addressData' => [],
            		'addressPostalData' => [],
            ]);
        }
    }    

    /**
     * Updates an existing Company model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id) {
    	if(!$this->editable) {
    		throw new ForbiddenHttpException(Yii::t('main', 'You cannot complete your request for security reasons.'));
    	}
    
        $model = $this->findModel($id);

        if(Yii::$app->request->isAjax && $model->load(Yii::$app->request->post())) {
        	Yii::$app->response->format = Response::FORMAT_JSON;
        	return ActiveForm::validate($model);
        }
        
        if($model->load(Yii::$app->request->post()) && $model->save()) {
        	CompanyHelper::updateCompanyGroup($model);
            return $this->redirect(['view', 'id' => $model->id]);
        } 
        else {
        	$companyData = Company::find()->where('id = :id', [':id' => $model->parent_company_id])->all();
        	$companyData = ArrayHelper::map($companyData, 'id', 'name');
        	
        	$addressData = Address::find()->where('id = :id', [':id' => $model->address_id])->all();
        	$addressData = ArrayHelper::map($addressData, 'id', function($item) {
        		return $item->getFullAddress();
        	});
        	
        	$addressPostalData = Address::find()->where('id = :id', [':id' => $model->address_postal_id])->all();
        	$addressPostalData = ArrayHelper::map($addressPostalData, 'id', function($item) {
        		return $item->getFullAddress();
        	});
        	
            return $this->render('update', [
					'model' => $model,
            		'companyData' => $companyData,
            		'addressData' => $addressData,
            		'addressPostalData' => $addressPostalData,
            ]);
        }
    }

    /**
     * Deletes an existing Company model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id) {
    	if(!$this->removable) {
    		throw new ForbiddenHttpException(Yii::t('main', 'You cannot complete your request for security reasons.'));
    	}
    
    	$transaction = Yii::$app->db->beginTransaction();
    	
    	try {
    		$model = $this->findModel($id);
    		CompanyHelper::deleteCompanyGroups($model);
    		$model->delete();
    		$transaction->commit();
        }
        catch(\Exception $e) {
        	$transaction->rollBack();
			Yii::$app->getSession()->setFlash('error', Yii::t('main', 'The object cannot be deleted because it is used by other objects.'));
        }

        return $this->redirect(['index']);
    }

    /**
     * Finds the Company model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Company the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id) {
        if(($model = Company::findOne($id)) !== null) {
            return $model;
        } 
        else {
            throw new NotFoundHttpException(Yii::t('main', 'The requested page does not exist. Please verify the address of the page.'));
        }
    }
}
