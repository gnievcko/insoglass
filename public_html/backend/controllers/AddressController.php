<?php

namespace backend\controllers;

use Yii;
use common\models\ar\Address;
use common\models\ars\AddressSearch;
use backend\components\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\widgets\ActiveForm;
use yii\web\Response;
use yii\web\ForbiddenHttpException;

/**
 * AddressController implements the CRUD actions for Address model.
 */
class AddressController extends Controller {

	protected $creatable = true;
	protected $editable = true;
	protected $removable = true;

	/* Behaviours are inherited */

    /**
     * Lists all Address models.
     * @return mixed
     */
    public function actionIndex() {
        $searchModel = new AddressSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
	            'searchModel' => $searchModel,
	            'dataProvider' => $dataProvider,
        		'withoutCreating' => !$this->creatable,
        		'withoutUpdating' => !$this->editable,
        		'withoutDeleting' => !$this->removable,
        ]);
    }

    /**
     * Displays a single Address model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id) {
        return $this->render('view', [
				'model' => $this->findModel($id),
        		'withoutUpdating' => !$this->editable,
        		'withoutDeleting' => !$this->removable,
        ]);
    }

    /**
     * Creates a new Address model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate($cityId = null) {
    	if(!$this->creatable) {
    		throw new ForbiddenHttpException(Yii::t('main', 'You cannot complete your request for security reasons.'));
    	}
    
        $model = new Address();

        if(Yii::$app->request->isAjax && $model->load(Yii::$app->request->post())) {
        	Yii::$app->response->format = Response::FORMAT_JSON;
        	return ActiveForm::validate($model);
        }
        
        if($model->load(Yii::$app->request->post()) && $model->save()) {
            if(!empty($cityId)) {
            	return $this->redirect(['city/view', 'id' => $model->city_id]);
            }
            else {
            	return $this->redirect(['view', 'id' => $model->id]);
            }
        } 
        else {
            return $this->render('create', [
					'model' => $model,
            		'cityId' => $cityId,
            ]);
        }
    }

    /**
     * Updates an existing Address model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id) {
    	if(!$this->editable) {
    		throw new ForbiddenHttpException(Yii::t('main', 'You cannot complete your request for security reasons.'));
    	}
    
        $model = $this->findModel($id);

        if(Yii::$app->request->isAjax && $model->load(Yii::$app->request->post())) {
        	Yii::$app->response->format = Response::FORMAT_JSON;
        	return ActiveForm::validate($model);
        }
        
        if($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } 
        else {
            return $this->render('update', [
					'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing Address model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id) {
    	if(!$this->removable) {
    		throw new ForbiddenHttpException(Yii::t('main', 'You cannot complete your request for security reasons.'));
    	}
    
    	$transaction = Yii::$app->db->beginTransaction();
    	
    	try {
    		$this->findModel($id)->delete();
    		$transaction->commit();
        }
        catch(\Exception $e) {
        	$transaction->rollBack();
			Yii::$app->getSession()->setFlash('error', Yii::t('main', 'The object cannot be deleted because it is used by other objects.'));
        }

    	if(strpos(Yii::$app->request->getReferrer(), Yii::$app->controller->id.'/view') !== false) {
        	return $this->redirect(['index']);
        }
        else {
        	return $this->redirect(Yii::$app->request->getReferrer());
        }	
    }

    /**
     * Finds the Address model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Address the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id) {
        if(($model = Address::findOne($id)) !== null) {
            return $model;
        } 
        else {
            throw new NotFoundHttpException(Yii::t('main', 'The requested page does not exist. Please verify the address of the page.'));
        }
    }
}
