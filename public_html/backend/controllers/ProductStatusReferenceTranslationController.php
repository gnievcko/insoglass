<?php

namespace backend\controllers;

use Yii;
use common\models\ar\ProductStatusReferenceTranslation;
use common\models\ars\ProductStatusReferenceTranslationSearch;
use backend\components\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\widgets\ActiveForm;
use yii\web\Response;
use yii\web\ForbiddenHttpException;

/**
 * ProductStatusReferenceTranslationController implements the CRUD actions for ProductStatusReferenceTranslation model.
 */
class ProductStatusReferenceTranslationController extends Controller {

	protected $creatable = true;
	protected $editable = true;
	protected $removable = true;

	/* Behaviours are inherited */

    /**
     * Lists all ProductStatusReferenceTranslation models.
     * @return mixed
     */
    public function actionIndex() {
        $searchModel = new ProductStatusReferenceTranslationSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
	            'searchModel' => $searchModel,
	            'dataProvider' => $dataProvider,
        		'withoutCreating' => !$this->creatable,
        		'withoutUpdating' => !$this->editable,
        		'withoutDeleting' => !$this->removable,
        ]);
    }

    /**
     * Displays a single ProductStatusReferenceTranslation model.
     * @param integer $reference_id
     * @param integer $language_id
     * @return mixed
     */
    public function actionView($reference_id, $language_id) {
        return $this->render('view', [
				'model' => $this->findModel($reference_id, $language_id),
        		'withoutUpdating' => !$this->editable,
        		'withoutDeleting' => !$this->removable,
        ]);
    }

    /**
     * Creates a new ProductStatusReferenceTranslation model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate($referenceId = null ) {
    	if(!$this->creatable) {
    		throw new ForbiddenHttpException(Yii::t('main', 'You cannot complete your request for security reasons.'));
    	}
    
        $model = new ProductStatusReferenceTranslation();

        if(Yii::$app->request->isAjax && $model->load(Yii::$app->request->post())) {
        	Yii::$app->response->format = Response::FORMAT_JSON;
        	return ActiveForm::validate($model);
        }
        
        if($model->load(Yii::$app->request->post()) && $model->save()) {    
	        if(!empty($referenceId)) {
	        	return $this->redirect(['product-status-reference/view', 'id' => $model->reference_id]);
	        }
	        else {
	        	return $this->redirect(['view', 'reference_id' => $model->reference_id, 'language_id' => $model->language_id]);
	        }
        } 
        else {
            return $this->render('create', [
					'model' => $model,
            		'referenceId' => $referenceId,
            ]);
        }
    }

    /**
     * Updates an existing ProductStatusReferenceTranslation model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $reference_id
     * @param integer $language_id
     * @return mixed
     */
    public function actionUpdate($reference_id, $language_id) {
    	if(!$this->editable) {
    		throw new ForbiddenHttpException(Yii::t('main', 'You cannot complete your request for security reasons.'));
    	}
    
        $model = $this->findModel($reference_id, $language_id);

        if(Yii::$app->request->isAjax && $model->load(Yii::$app->request->post())) {
        	Yii::$app->response->format = Response::FORMAT_JSON;
        	return ActiveForm::validate($model);
        }
        
        if($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'reference_id' => $model->reference_id, 'language_id' => $model->language_id]);
        } 
        else {
            return $this->render('update', [
					'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing ProductStatusReferenceTranslation model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $reference_id
     * @param integer $language_id
     * @return mixed
     */
    public function actionDelete($reference_id, $language_id) {
    	if(!$this->removable) {
    		throw new ForbiddenHttpException(Yii::t('main', 'You cannot complete your request for security reasons.'));
    	}
    
    	$transaction = Yii::$app->db->beginTransaction();
    	
    	try {
    		$this->findModel($reference_id, $language_id)->delete();
    		$transaction->commit();
        }
        catch(\Exception $e) {
        	$transaction->rollBack();
			Yii::$app->getSession()->setFlash('error', Yii::t('main', 'The object cannot be deleted because it is used by other objects.'));
        }

        if(strpos(Yii::$app->request->getReferrer(), Yii::$app->controller->id.'/view') !== false) {
        	return $this->redirect(['index']);
        }
        else {
        	return $this->redirect(Yii::$app->request->getReferrer());
        }
    }

    /**
     * Finds the ProductStatusReferenceTranslation model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $reference_id
     * @param integer $language_id
     * @return ProductStatusReferenceTranslation the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($reference_id, $language_id) {
        if(($model = ProductStatusReferenceTranslation::findOne(['reference_id' => $reference_id, 'language_id' => $language_id])) !== null) {
            return $model;
        } 
        else {
            throw new NotFoundHttpException(Yii::t('main', 'The requested page does not exist. Please verify the address of the page.'));
        }
    }
}
