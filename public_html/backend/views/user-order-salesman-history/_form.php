<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\ar\UserOrderSalesmanHistory */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="user-order-salesman-history-form">

    <?php $form = ActiveForm::begin([
    		'id' => 'user-order-salesman-history-form-ajax',
    		'enableAjaxValidation' => true,
    ]); ?>

	<?= $form->field($model, 'user_id')->textInput() ?>

	<?= $form->field($model, 'date')->textInput() ?>

	<?= $form->field($model, 'history_id')->textInput() ?>

	<?= $form->field($model, 'is_added')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('main', 'Create') : Yii::t('main', 'Save'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>