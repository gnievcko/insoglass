<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use common\helpers\StringHelper;

/* @var $this yii\web\View */
/* @var $model common\models\ar\UserOrderSalesmanHistory */

$this->title = StringHelper::translateOrderToOffer('main', 'History of salesman assignment to order') . ' - ' . $model->userName;
$this->params['breadcrumbs'][] = ['label' => StringHelper::translateOrderToOffer('main', 'History of salesmen assignment to orders'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="user-order-salesman-history-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
    	<?php if(empty($withoutUpdating)) { ?>
    		<?= Html::a(Yii::t('main', 'Update'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
    	<?php } ?>
        <?php if(empty($withoutDeleting)) { ?>
	        <?= Html::a(Yii::t('main', 'Delete'), ['delete', 'id' => $model->id], [
		            'class' => 'btn btn-danger',
		            'data' => [
			                'confirm' => Yii::t('main', 'Are you sure you want to delete this item?'),
			                'method' => 'post',
		            ],
	        ]) ?>
		<?php } ?>
    </p>

    <?= DetailView::widget([
	        'model' => $model,
	        'formatter' => ['class' => 'yii\i18n\Formatter', 'nullDisplay' => ''],
	        'attributes' => [
					'id',
	        		[
	        				'attribute' => 'userName',
	        				'format' => 'raw',
	        				'value' => Html::a($model->userName, ['/user/view', 'id' => $model->user_id]),
	        		],
					'date',
					'history_id',
	        		[
	        				'attribute' => 'is_added',
	        				'format' => 'raw',
	        				'value' => $model->is_added ? Yii::t('main', 'added') : Yii::t('main', 'removed'),
	        		],
        	],
    ]) ?>

</div>
