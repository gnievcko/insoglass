<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\ar\UserOrderSalesmanHistory */

$this->title = Yii::t('main', 'Update user order salesman history') . ' #' . $model->id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('main', 'User order salesman histories'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => Yii::t('main', 'User order salesman history') . ' #' . $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('main', 'Update');
?>
<div class="user-order-salesman-history-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        	'model' => $model,
    ]) ?>

</div>
