<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\ar\UserOrderSalesmanHistory */

$this->title = Yii::t('main', 'Create user order salesman history');
$this->params['breadcrumbs'][] = ['label' => Yii::t('main', 'User order salesman histories'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="user-order-salesman-history-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        	'model' => $model,
    ]) ?>

</div>
