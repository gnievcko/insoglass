<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use common\models\ar\ProductStatusReference;
use yii\helpers\ArrayHelper;
use common\models\ar\Language;

/* @var $this yii\web\View */
/* @var $model common\models\ar\ProductStatusReferenceTranslation */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="product-status-reference-translation-form">

    <?php $form = ActiveForm::begin([
    		'id' => 'product-status-reference-translation-form-ajax',
    		'enableAjaxValidation' => true,
    ]); ?>

	<?= $form->field($model, 'reference_id')->dropDownList(ArrayHelper::map(
			empty($referenceId) ? ProductStatusReference::find()->all() : ProductStatusReference::find()->where(['id' => $referenceId])->all(), 'id', 'symbol'		
	))->label(Yii::t('main', 'Symbol')) ?>
	
	<?= $form->field($model, 'language_id')->dropDownList(ArrayHelper::map(
			Language::find()->where('is_active = 1')->all(), 'id', 'name'
	))->label(Yii::t('main', 'Language')) ?>
	
	<?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('main', 'Create') : Yii::t('main', 'Save'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>