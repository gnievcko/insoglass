<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\ar\ProductStatusReferenceTranslation */

$this->title = Yii::t('main', 'Update warehouse operation type reference translation') . ' - ' . $model->name;
$this->params['breadcrumbs'][] = ['label' => Yii::t('main', 'Warehouse operation type reference translations'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => Yii::t('main', 'Warehouse operation type reference translation') . ' - ' . $model->name, 'url' => ['view', 'reference_id' => $model->reference_id, 'language_id' => $model->language_id]];
$this->params['breadcrumbs'][] = Yii::t('main', 'Update');
?>
<div class="product-status-reference-translation-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        	'model' => $model,
    ]) ?>

</div>
