<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use common\models\ar\ProductStatus;
use common\helpers\Utility;

/* @var $this yii\web\View */
/* @var $model common\models\ar\ProductStatusReference */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="product-status-reference-form">

    <?php $form = ActiveForm::begin([
    		'id' => 'product-status-reference-form-ajax',
    		'enableAjaxValidation' => true,
    ]); ?>

	<?= $form->field($model, 'product_status_id')->dropDownList(ArrayHelper::map(
			empty($productStatusId) ? ProductStatus::find()->all() : ProductStatus::find()->where(['id' => $productStatusId])->all(), 'id', 'symbolTranslation'
	))->label(Yii::t('main', 'Status symbol')) ?>

	<?= $form->field($model, 'symbol')->textInput(['maxlength' => true]) ?>

	<?= $form->field($model, 'name_table')->textInput(['maxlength' => true]) ?>

	<?= $form->field($model, 'name_column')->textInput(['maxlength' => true]) ?>

	<?= $form->field($model, 'name_expression')->textInput(['maxlength' => true]) ?>

	<?= $form->field($model, 'url_details')->textInput(['maxlength' => true]) ?>

	<?= $form->field($model, 'is_required')->checkbox() ?>
	
	<?= $form->field($model, 'field_type')->dropDownList([
			Utility::ADDITIONAL_FIELD_TYPE_SELECT2 => Utility::ADDITIONAL_FIELD_TYPE_SELECT2, 
			Utility::ADDITIONAL_FIELD_TYPE_TEXT_INPUT => Utility::ADDITIONAL_FIELD_TYPE_TEXT_INPUT
	], ['prompt' => '']); ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('main', 'Create') : Yii::t('main', 'Save'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>