<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use common\helpers\StringHelper;

/* @var $this yii\web\View */
/* @var $model common\models\ar\ProductStatusReference */

$this->title = Yii::t('main', 'Warehouse operation type reference') . ' - ' . $model->symbol;
$this->params['breadcrumbs'][] = ['label' => Yii::t('main', 'Warehouse operation type references'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="product-status-reference-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
    	<?php if(empty($withoutUpdating)) { ?>
    		<?= Html::a(Yii::t('main', 'Update'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
    	<?php } ?>
        <?php if(empty($withoutDeleting)) { ?>
	        <?= Html::a(Yii::t('main', 'Delete'), ['delete', 'id' => $model->id], [
		            'class' => 'btn btn-danger',
		            'data' => [
			                'confirm' => Yii::t('main', 'Are you sure you want to delete this item?'),
			                'method' => 'post',
		            ],
	        ]) ?>
		<?php } ?>
    </p>

    <?= DetailView::widget([
	        'model' => $model,
	        'formatter' => ['class' => 'yii\i18n\Formatter', 'nullDisplay' => ''],
	        'attributes' => [
					'id',
					[
	        			'attribute' => 'productStatusSymbol',
	        			'format' => 'raw',
	        			'value' => Html::a($model->productStatusSymbol, ['/product-status/view', 'id' => $model->product_status_id]),
	        		],
					'symbol',
					'name_table',
					'name_column',
					'name_expression',
					'url_details:url',
					[
							'attribute' => 'is_required',
							'value' => StringHelper::boolTranslation($model->is_required)
					],
	        		'field_type',
        	],
    ]) ?>

</div>

<?php
         echo Yii::$app->controller->renderPartial('/product-status-reference-translation/index', [
         		'dataProvider' => $productStatusReferenceTranslationDataProvider,
         		'referenceId' => $model->id,
         		'withoutDecorations' => true,
        ]);
 ?>
