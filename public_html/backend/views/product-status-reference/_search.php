<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use common\helpers\Utility;

/* @var $this yii\web\View */
/* @var $model common\models\ars\ProductStatusReferenceSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<?php
$this->registerJs('
	$(".reset-button").click(function(e){
		e.preventDefault();
		$(".form-control").val("");
	});
');
?>

<div class="product-status-reference-search">

    <?php $form = ActiveForm::begin([
	        'action' => ['index'],
	        'method' => 'get',
    ]); ?>
	<div class="row">
		<div class="col-md-4">
			<?= $form->field($model, 'id')->textInput(['type' => 'number']) ?>
		</div>
		<div class="col-md-4">
			<?= $form->field($model, 'productStatusSymbol') ?>
		</div>
		<div class="col-md-4">
			<?= $form->field($model, 'symbol') ?>
		</div>
		<div class="col-md-4">
			<?= $form->field($model, 'name_table') ?>
		</div>
		<div class="col-md-4">
			<?= $form->field($model, 'name_column') ?>
		</div>
		<div class="col-md-4">
			<?= $form->field($model, 'name_expression') ?>
		</div>
		<div class="col-md-4">
			<?= $form->field($model, 'url_details') ?>
		</div>
		<div class="col-md-4">
			<?= $form->field($model, 'is_required')->dropDownList([ 
					0 => Yii::t('main', 'No'), 1 => Yii::t('main', 'Yes')				
			], ['prompt' => '']) ?>
		</div>
		<div class="col-md-4">
			<?= $form->field($model, 'field_type')->dropDownList([
					Utility::ADDITIONAL_FIELD_TYPE_SELECT2 => Utility::ADDITIONAL_FIELD_TYPE_SELECT2, 
					Utility::ADDITIONAL_FIELD_TYPE_TEXT_INPUT => Utility::ADDITIONAL_FIELD_TYPE_TEXT_INPUT
			], ['prompt' => '']) ?>
		</div>
	</div>
    <div class="form-group">
        <?= Html::submitButton(Yii::t('main', 'Search'), ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton(Yii::t('main', 'Reset'), ['class' => 'btn btn-default reset-button']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
