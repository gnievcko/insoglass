<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\ar\ProductStatusReference */

$this->title = Yii::t('main', 'Create warehouse operation type reference');
$this->params['breadcrumbs'][] = ['label' => Yii::t('main', 'Warehouse operation type references'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="product-status-reference-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        	'model' => $model,
    		'productStatusId' => $productStatusId
    ]) ?>

</div>
