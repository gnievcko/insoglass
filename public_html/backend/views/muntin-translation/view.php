<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\ar\MuntinTranslation */

$this->title = Yii::t('main', 'Muntin translation') . ' - ' . $model->muntinSymbol;
$this->params['breadcrumbs'][] = ['label' => Yii::t('main', 'Muntin translations'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="muntin-translation-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
    	<?php if(empty($withoutUpdating)) { ?>
    		<?= Html::a(Yii::t('main', 'Update'), ['update', 'muntin_id' => $model->muntin_id, 'language_id' => $model->language_id], ['class' => 'btn btn-primary']) ?>
    	<?php } ?>
        <?php if(empty($withoutDeleting)) { ?>
	        <?= Html::a(Yii::t('main', 'Delete'), ['delete', 'muntin_id' => $model->muntin_id, 'language_id' => $model->language_id], [
		            'class' => 'btn btn-danger',
		            'data' => [
			                'confirm' => Yii::t('main', 'Are you sure you want to delete this item?'),
			                'method' => 'post',
		            ],
	        ]) ?>
		<?php } ?>
    </p>

    <?= DetailView::widget([
	        'model' => $model,
	        'formatter' => ['class' => 'yii\i18n\Formatter', 'nullDisplay' => ''],
	        'attributes' => [
	        		[
			        		'attribute' => 'muntinSymbol',
			        		'format' => 'raw',
			        		'value' => Html::a($model->muntinSymbol, ['/muntin/view', 'id' => $model->muntin_id]),
	        		],
					[
			        		'attribute' => 'languageName',
			        		'format' => 'raw',
			        		'value' => Html::a($model->languageName, ['/language/view', 'id' => $model->language_id]),
	        		],
					'name',
        	],
    ]) ?>

</div>
