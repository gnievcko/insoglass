<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use common\models\ar\Muntin;
use common\models\ar\Language;

/* @var $this yii\web\View */
/* @var $model common\models\ar\MuntinTranslation */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="muntin-translation-form">

    <?php $form = ActiveForm::begin([
    		'id' => 'muntin-translation-form-ajax',
    		'enableAjaxValidation' => true,
    ]); ?>

    <?= $form->field($model, 'muntin_id')->dropDownList(ArrayHelper::map(
            empty($muntinId) ? Muntin::find()->orderBy('id')->all() : Muntin::find()->where(['id' => $muntinId])->all(), 'id', 'symbol'       
    ))->label(Yii::t('main', 'Muntin')) ?>

    <?= $form->field($model, 'language_id')->dropDownList(ArrayHelper::map(
            Language::find()->where('is_active = 1')->all(), 'id', 'name'
    ))->label(Yii::t('main', 'Language')) ?>

	<?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('main', 'Create') : Yii::t('main', 'Save'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>