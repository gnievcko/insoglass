<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\ar\DepartmentTypeTranslation */

$this->title = Yii::t('main', 'Department type translation') . ' - ' . $model->name;
$this->params['breadcrumbs'][] = ['label' => Yii::t('main', 'Department type translations'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="department-type-translation-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
    	<?php if(empty($withoutUpdating)) { ?>
    		<?= Html::a(Yii::t('main', 'Update'), ['update', 'department_type_id' => $model->department_type_id, 'language_id' => $model->language_id], ['class' => 'btn btn-primary']) ?>
    	<?php } ?>
        <?php if(empty($withoutDeleting)) { ?>
	        <?= Html::a(Yii::t('main', 'Delete'), ['delete', 'department_type_id' => $model->department_type_id, 'language_id' => $model->language_id], [
		            'class' => 'btn btn-danger',
		            'data' => [
			                'confirm' => Yii::t('main', 'Are you sure you want to delete this item?'),
			                'method' => 'post',
		            ],
	        ]) ?>
		<?php } ?>
    </p>

    <?= DetailView::widget([
	        'model' => $model,
	        'formatter' => ['class' => 'yii\i18n\Formatter', 'nullDisplay' => ''],
	        'attributes' => [
                    [
                        'attribute' => 'departmentTypeSymbol',
                        'format' => 'raw',
                        'value' => Html::a($model->departmentTypeSymbol, ['department-type/view/'.$model->department_type_id, ]),
                    ],
                    [
                            'attribute' => 'languageName',
                            'format' => 'raw',
                            'value' => Html::a($model->languageName, ['language/view/'.$model->language_id, ]),
                    ],
                    'name',
            ],
    ]) ?>

</div>
