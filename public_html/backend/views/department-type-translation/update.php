<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\ar\DepartmentTypeTranslation */

$this->title = Yii::t('main', 'Update department type translation') . " - ". $model->name;
$this->params['breadcrumbs'][] = ['label' => Yii::t('main', 'Department type translations'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => Yii::t('main', 'Department type translation') ." ". $model->name, 'url' => ['view', 'department_type_id' => $model->department_type_id, 'language_id' => $model->language_id]];
$this->params['breadcrumbs'][] = Yii::t('main', 'Update');
?>
<div class="department-type-translation-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        	'model' => $model,
    ]) ?>

</div>
