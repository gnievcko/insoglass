<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\ar\DepartmentTypeTranslation */

$this->title = Yii::t('main', 'Create department type translation');
$this->params['breadcrumbs'][] = ['label' => Yii::t('main', 'Department type translations'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="department-type-translation-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        	'model' => $model,
            'departmentTypeId' => $departmentTypeId,
    ]) ?>

</div>
