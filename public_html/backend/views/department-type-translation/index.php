<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $searchModel common\models\ars\DepartmentTypeTranslationSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

if(empty($withoutDecorations)) {
	if(!empty($searchModel)) {
		$this->title = Yii::t('main', 'Department type translations');
		$this->params['breadcrumbs'][] = $this->title;
		
		$this->registerJs('
			$(".filter-button").click(function(e){
				e.preventDefault();
				$(".search-form").toggle();
			});
		');
	}
}
?>
<div class="department-type-translation-index">

	<?php if(empty($withoutDecorations)) { ?>
    	<h1><?= Html::encode($this->title) ?></h1>
	<?php } else { ?>
    	<h2><?= Yii::t('main', 'Translations') ?></h2>
	<?php } ?>

	<?php if(!empty($searchModel)) { ?>
		<div class="search-form" style="display:none">
			<?php // echo $this->render('_search', ['model' => $searchModel]); ?>
		</div>
	<?php } ?>

    <p>
    	<?php if(empty($withoutCreating)) { ?>
			<?= Html::a(Yii::t('main', 'Create translation'), ['department-type-translation/create',  'departmentTypeId' => !empty($departmentTypeId) ? $departmentTypeId : null], ['class' => 'btn btn-success']) ?>
        <?php } ?>
		<?php if(!empty($searchModel)) {
			//echo '&nbsp'. Html::button(Yii::t('main', 'Filters'), ['class' => 'btn btn-primary filter-button']);
		} ?>

        <?php if(!empty($departmentTypeId)) {
            echo Html::a(Yii::t('main', 'List of all translations'), ['/department-type-translation/index'], ['class' => 'btn btn-info']);
        } ?>
    </p>

	<?php Pjax::begin(); ?>

    	<?=  GridView::widget([
        		'dataProvider' => $dataProvider,
        		'formatter' => ['class' => 'yii\i18n\Formatter', 'nullDisplay' => ''],
        		'filterModel' => !empty($searchModel) ? $searchModel : null,
				'columns' => [
                        [
                            'attribute' => 'departmentTypeSymbol',
                            'format' => 'raw',
                            'value' => function($data, $key, $index, $column) {
                                return Html::a($data->departmentTypeSymbol, ['department-type/view/'.$data->department_type_id, ]);
    	                    },
                        ],
                        [
                            'attribute' => 'languageName',
                            'format' => 'raw',
                            'value' => function($data, $key, $index, $column) {
                                return Html::a($data->languageName, ['language/view/'.$data->language_id, ]);
                            },
                        ],
                        'name',
            			[
            					'class' => 'backend\components\ActionColumn',
            					'deleteConfirmMessage' => Yii::t('main', 'Are you sure you want to delete this item?'),
            					'visibleButtons' => ['delete' => empty($withoutDeleting), 'update' => empty($withoutUpdating)],
								'contentOptions' => ['class' => 'action-column'],
                                'controller' => 'department-type-translation',
                        ],

        		],
    	]); ?>
	<?php Pjax::end(); ?>

</div>
