<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\ar\ProductAttributeGroup */

$this->title = Yii::t('main', 'Product attribute group') . ' - ' . $model->symbol;
$this->params['breadcrumbs'][] = ['label' => Yii::t('main', 'Product attribute groups'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="product-attribute-group-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
    	<?php if(empty($withoutUpdating)) { ?>
    		<?= Html::a(Yii::t('main', 'Update'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
    	<?php } ?>
        <?php if(empty($withoutDeleting)) { ?>
	        <?= Html::a(Yii::t('main', 'Delete'), ['delete', 'id' => $model->id], [
		            'class' => 'btn btn-danger',
		            'data' => [
			                'confirm' => Yii::t('main', 'Are you sure you want to delete this item?'),
			                'method' => 'post',
		            ],
	        ]) ?>
		<?php } ?>
    </p>

    <?= DetailView::widget([
	        'model' => $model,
	        'formatter' => ['class' => 'yii\i18n\Formatter', 'nullDisplay' => ''],
	        'attributes' => [
					'id',
					'symbol',
        	],
    ]) ?>

</div>

<?php 
    echo Yii::$app->controller->renderPartial('/product-attribute-group-translation/index',[
        'dataProvider' => $productAttributeGroupTranslationDataProvider,
        'withoutDecorations' => true,
        'productAttributeGroupId' => $model->id,
    ]);
    
    echo Yii::$app->controller->renderPartial('/product-attribute-type/index', [
        'dataProvider' => $productAttributeTypeDataProvider,
        'withoutDecorations' => true,
        'productAttributeGroupId' => $model->id,
    ])
?> 

