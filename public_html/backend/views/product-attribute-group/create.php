<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\ar\ProductAttributeGroup */

$this->title = Yii::t('main', 'Create product attribute group');
$this->params['breadcrumbs'][] = ['label' => Yii::t('main', 'Product attribute groups'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="product-attribute-group-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        	'model' => $model,
    ]) ?>

</div>
