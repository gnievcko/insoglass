<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\ar\FileTemporaryStorage */

$this->title = Yii::t('main', 'Create file temporary storage');
$this->params['breadcrumbs'][] = ['label' => Yii::t('main', 'File temporary storages'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="file-temporary-storage-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        	'model' => $model,
    ]) ?>

</div>
