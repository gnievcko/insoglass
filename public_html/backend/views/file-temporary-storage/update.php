<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\ar\FileTemporaryStorage */

$this->title = Yii::t('main', 'Update file temporary storage') . ' #' . $model->id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('main', 'File temporary storages'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => Yii::t('main', 'File temporary storage') . ' #' . $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('main', 'Update');
?>
<div class="file-temporary-storage-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        	'model' => $model,
    ]) ?>

</div>
