<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\ar\FileTemporaryStorage */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="file-temporary-storage-form">

    <?php $form = ActiveForm::begin([
    		'id' => 'file-temporary-storage-form-ajax',
    		'enableAjaxValidation' => true,
    ]); ?>

	<?= $form->field($model, 'id')->textInput(['maxlength' => true]) ?>

	<?= $form->field($model, 'file_hash')->textInput(['maxlength' => true]) ?>

	<?= $form->field($model, 'date_creation')->textInput() ?>

	<?= $form->field($model, 'original_file_name')->textInput(['maxlength' => true]) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('main', 'Create') : Yii::t('main', 'Save'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>