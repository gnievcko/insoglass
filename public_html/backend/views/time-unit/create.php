<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\ar\TimeUnit */

$this->title = Yii::t('main', 'Create time unit');
$this->params['breadcrumbs'][] = ['label' => Yii::t('main', 'Time units'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="time-unit-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        	'model' => $model,
    ]) ?>

</div>
