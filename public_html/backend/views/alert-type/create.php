<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\ar\AlertType */

$this->title = Yii::t('main', 'Create alert type');
$this->params['breadcrumbs'][] = ['label' => Yii::t('main', 'Alert types'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="alert-type-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        	'model' => $model,
    ]) ?>

</div>
