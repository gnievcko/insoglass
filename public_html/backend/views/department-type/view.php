<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\ar\DepartmentType */

$this->title = Yii::t('main', 'Department type') . ' - ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('main', 'Department types'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="department-type-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
    	<?php if(empty($withoutUpdating)) { ?>
    		<?= Html::a(Yii::t('main', 'Update'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
    	<?php } ?>
        <?php if(empty($withoutDeleting)) { ?>
	        <?= Html::a(Yii::t('main', 'Delete'), ['delete', 'id' => $model->id], [
		            'class' => 'btn btn-danger',
		            'data' => [
			                'confirm' => Yii::t('main', 'Are you sure you want to delete this item?'),
			                'method' => 'post',
		            ],
	        ]) ?>
		<?php } ?>
    </p>

    <?= DetailView::widget([
	        'model' => $model,
	        'formatter' => ['class' => 'yii\i18n\Formatter', 'nullDisplay' => ''],
	        'attributes' => [
					'id',
                    'symbol',
        	],
    ]) ?>




</div>

<?php
 echo Yii::$app->controller->renderPartial('/department-type-translation/index', [
        'dataProvider' => $departmentTypeTranslationDataProvider,
        'withoutDecorations' => true,
        'departmentTypeId' => $model->id,
    ]);
?>
