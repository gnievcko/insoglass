<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\ar\DepartmentType */

$this->title = Yii::t('main', 'Create department type');
$this->params['breadcrumbs'][] = ['label' => Yii::t('main', 'Department types'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="department-type-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        	'model' => $model,
    ]) ?>

</div>
