<?php

use yii\helpers\Html;
use common\helpers\StringHelper;

/* @var $this yii\web\View */
/* @var $model common\models\ar\OrderStatus */

$this->title = StringHelper::translateOrderToOffer('main', 'Create order status');
$this->params['breadcrumbs'][] = ['label' => StringHelper::translateOrderToOffer('main', 'Order statuses'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="order-status-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        	'model' => $model,
    ]) ?>

</div>
