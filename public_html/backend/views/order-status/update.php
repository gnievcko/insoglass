<?php

use yii\helpers\Html;
use common\helpers\StringHelper;

/* @var $this yii\web\View */
/* @var $model common\models\ar\OrderStatus */

$this->title = StringHelper::translateOrderToOffer('main', 'Update order status') . ' - ' . $model->symbol;
$this->params['breadcrumbs'][] = ['label' => StringHelper::translateOrderToOffer('main', 'Order statuses'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => StringHelper::translateOrderToOffer('main', 'Order status') . ' - ' . $model->symbol, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('main', 'Update');
?>
<div class="order-status-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        	'model' => $model,
    ]) ?>

</div>
