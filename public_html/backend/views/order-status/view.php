<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use backend\helpers\Utility;
use common\helpers\StringHelper;

/* @var $this yii\web\View */
/* @var $model common\models\ar\OrderStatus */

$this->title = StringHelper::translateOrdertoOffer('main', 'Order status') . ' - ' . $model->symbol;
$this->params['breadcrumbs'][] = ['label' => StringHelper::translateOrderToOffer('main', 'Order statuses'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="order-status-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
    	<?php if(empty($withoutUpdating)) { ?>
    		<?= Html::a(Yii::t('main', 'Update'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
    	<?php } ?>
        <?php if(empty($withoutDeleting)) { ?>
	        <?= Html::a(Yii::t('main', 'Delete'), ['delete', 'id' => $model->id], [
		            'class' => 'btn btn-danger',
		            'data' => [
			                'confirm' => Yii::t('main', 'Are you sure you want to delete this item?'),
			                'method' => 'post',
		            ],
	        ]) ?>
		<?php } ?>
    </p>

    <?= DetailView::widget([
	        'model' => $model,
	        'formatter' => ['class' => 'yii\i18n\Formatter', 'nullDisplay' => ''],
	        'attributes' => [
					'id',
					'symbol',
					[
							'label' => StringHelper::translateOrderToOffer('main', 'Is order'),
							'value' => StringHelper::boolTranslation($model->is_order)
					],
					[
							'label' => Yii::t('main', 'Is visible'),
							'value' => StringHelper::boolTranslation($model->is_visible)
					],
					[
							'label' => Yii::t('main', 'Is visible for client'),
							'value' => StringHelper::boolTranslation($model->is_visible_for_client)
					],
					[
							'label' => Yii::t('main', 'Is terminal'),
							'value' => StringHelper::boolTranslation($model->is_terminal)
					],
        	],
    ]) ?>

</div>


<?php 
	echo Yii::$app->controller->renderPartial('/order-status-translation/index', [
			'dataProvider' => $orderStatusTranslationDataProvider,
			'withoutDecorations' => true,
			'orderStatusId' => $model->id,
	]);
?>

<?php 
	echo Yii::$app->controller->renderPartial('/order-status-transition/index', [
			'dataProvider' => $orderStatusSuccesorDataProvider,
			'withoutDecorations' => true,
			'withoutUpdating' => true,
			'orderStatusId' => $model->id,
			'successorsOnly' => true,
	]);
?>

<?php 
	echo Yii::$app->controller->renderPartial('/order-status-transition/index', [
			'dataProvider' => $orderStatusPredecessorDataProvider,
			'withoutDecorations' => true,
			'withoutUpdating' => true,
			'orderStatusId' => $model->id,
			'predecessorsOnly' => true,
	]);
?>
<div>
    <?= Html::a(Yii::t('main', 'List of all status transitions'), ['order-status-transition/index'], ['class' => 'btn btn-info']); ?>
</div>
