<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\ar\TaskType */

$this->title = Yii::t('main', 'Create task type');
$this->params['breadcrumbs'][] = ['label' => Yii::t('main', 'Task types'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="task-type-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        	'model' => $model,
    ]) ?>

</div>
