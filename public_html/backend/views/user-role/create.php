<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\ar\UserRole */

$this->title = Yii::t('main', 'Create user role');
$this->params['breadcrumbs'][] = ['label' => Yii::t('main', 'User roles'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="user-role-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        	'model' => $model,
            'userData' => $userData,
            'userId' => $userId,
    ]) ?>

</div>
