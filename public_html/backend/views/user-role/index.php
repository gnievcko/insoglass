<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
use common\helpers\Utility;
use common\models\ar\UserRole;

/* @var $this yii\web\View */
/* @var $searchModel common\models\ars\UserRoleSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

if(empty($withoutDecorations)) {
	if(!empty($searchModel)) {
		$this->title = Yii::t('main', 'User roles');
		$this->params['breadcrumbs'][] = $this->title;
		
		$this->registerJs('
			$(".filter-button").click(function(e){
				e.preventDefault();
				$(".search-form").toggle();
			});
		');
	}
}
?>
<div class="user-role-index">

	<?php if(empty($withoutDecorations)) { ?>
    	<h1><?= Html::encode($this->title) ?></h1>
	<?php } else { ?>
    	<h2><?= Yii::t('main', 'Roles') ?></h2>
	<?php } ?>

	<?php if(!empty($searchModel)) { ?>
		<div class="search-form" style="display:none">
			<?php // echo $this->render('_search', ['model' => $searchModel]); ?>
		</div>
	<?php } ?>

    <p>
    	<?php if(empty($withoutCreating)) { ?>

			<?= Html::a(Yii::t('main', 'Create user role'), ['user-role/create', 'userId' => !empty($userId) ? $userId : null], 
					['class' => 'btn btn-success']) ?>
		<?php } ?>
		<?php if(!empty($searchModel)) {
			//echo '&nbsp'. Html::button(Yii::t('main', 'Filters'), ['class' => 'btn btn-primary filter-button']);
		} ?>

		<?php if(!empty($userId)) {
			echo Html::a(Yii::t('main', 'List of roles of all users'), ['/user-role/index'], ['class' => 'btn btn-info']);
		} ?>
    </p>

	<?php Pjax::begin(); ?>
    	<?= GridView::widget([
        		'dataProvider' => $dataProvider,
        		'formatter' => ['class' => 'yii\i18n\Formatter', 'nullDisplay' => ''],
        		'filterModel' => !empty($searchModel) ? $searchModel : null,
				'columns' => [
						[
								'attribute' => 'userName',
								'visible' => !empty($searchModel),
						],
						'roleName',
            			[
            					'class' => 'backend\components\ActionColumn',
            					'deleteConfirmMessage' => Yii::t('main', 'Are you sure you want to delete this item?'),
								'controller' => 'user-role',
                                'visibleButtons' => [
                                    'delete' => function($userRole) { 
                                        return empty($withoutDeleting) 
                                            && !($userRole->user_id === 1 && $userRole->role_id === 1); }, 
                                    'update' => empty($withoutUpdating)], 
								'contentOptions' => ['class' => 'action-column'],
            			],
        		],
    	]); ?>
	<?php Pjax::end(); ?>

</div>
