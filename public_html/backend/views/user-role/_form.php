<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\widgets\Select2;
use yii\helpers\Url;
use yii\web\JsExpression;
use common\models\ar\Role;
use common\models\ar\User;
use common\helpers\Utility;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $model common\models\ar\UserRole */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="user-role-form">

    <?php
    $form = ActiveForm::begin([
                'id' => 'user-role-form-ajax',
                'enableAjaxValidation' => true,
    ]);
    ?>

    <?php
    if(empty($userId)) {
        echo $form->field($model, 'user_id')
                ->label(Yii::t('main', 'User'))
                ->widget(Select2::classname(), [
                    'language' => Yii::$app->language,
                    'options' => ['placeholder' => Yii::t('main', 'Select user...')],
                    'data' => $userData,
                    'pluginOptions' => [
                        'allowClear' => true,
                        'minimumInputLength' => 1,
                        'ajax' => [
                            'url' => Url::to(['site/find-users']),
                            'dataType' => 'json',
                            'delay' => 250,
                            'data' => new JsExpression('function(params) { 
                                                return {term:params.term, page:params.page, size: 20, type: "' . User::USER_TYPE_EMPLOYEE . '"}; 
                                            }'),
                            'processResults' => new JsExpression('function (data, params) {
												params.page = params.page || 1;
                                                var users = $(data.items).map(function(index, user) {
                                                    return {id: user.id, name: user.first_name + ((user.first_name == null) ? "" : " ") + user.last_name};
                                                });
                                                
												return {
													results: users,
													pagination: {
														more: data.more
													}
												};
											}'),
                            'cache' => true
                        ],
                        'templateResult' => new JsExpression('function(item) { if(item.loading) return item.text; return item.name; }'),
                        'templateSelection' => new JsExpression('function (item) { return item.name || item.text; }'),
                    ]
        ]);
    } else {
        echo $form->field($model, 'user_id')->dropDownList(ArrayHelper::map(
                        User::find()->where(['id' => $userId])->all(), 'id', function($item) {
                    return User::discoverNameForm($item);
                }
        ))->label(Yii::t('main', 'User'));
    }
    ?>

    <?php
    $roleOptions = Role::find()->select(['role.id as id', 'role_translation.name as symbol'])
            ->where('role.symbol != :roleClient', [':roleClient' => Utility::ROLE_CLIENT])
            ->andWhere(['role.is_visible' => 1])
            ->orderBy('role_translation.name')
            ->joinWith(['roleTranslations' => function($q) {
            $q->joinWith(['language']);
            $q->where('language.symbol = :languageSymbol', [':languageSymbol' => Yii::$app->language]);
        }]);

    echo $form->field($model, 'role_id', ['enableAjaxValidation' => true])->dropDownList(ArrayHelper::map(
                    $roleOptions->all(), 'id', 'symbol'
    ))->label(Yii::t('main', 'Role'));
    ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('main', 'Create') : Yii::t('main', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
