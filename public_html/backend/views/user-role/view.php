<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\ar\UserRole */

$this->title = Yii::t('main', 'User role') . ' #' . $model->user_id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('main', 'User roles'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="user-role-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
    	<?php if(empty($withoutUpdating)) { ?>
    		<?= Html::a(Yii::t('main', 'Update'), ['update', 'user_id' => $model->user_id, 'role_id' => $model->role_id], ['class' => 'btn btn-primary']) ?>
    	<?php } ?>
        <?php if(empty($withoutDeleting) && !($model->user_id == 1 && $model->role_id == 1)) { ?>
	        <?= Html::a(Yii::t('main', 'Delete'), ['delete', 'user_id' => $model->user_id, 'role_id' => $model->role_id], [
		            'class' => 'btn btn-danger',
		            'data' => [
			                'confirm' => Yii::t('main', 'Are you sure you want to delete this item?'),
			                'method' => 'post',
		            ],
	        ]) ?>
		<?php } ?>
    </p>

    <?= DetailView::widget([
	        'model' => $model,
    		'attributes' => [
    				[
    						'label' => Yii::t('main', 'User'),
    						'format' => 'raw',
    						'value' => Html::a($model->userName, ['/user/view', 'id' => $model->user_id]),
    				],
    				[
    						'label' => Yii::t('main', 'Role'),
    						'format' => 'raw',
    						'value' => Html::a($model->roleName, ['/role/view', 'id' => $model->role_id]),
    				],
        	],
    ]) ?>

</div>
