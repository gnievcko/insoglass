<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\ar\UserRole */

$this->title = Yii::t('main', 'Update user role') . ' #' . $model->user_id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('main', 'User roles'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => Yii::t('main', 'User role') . ' #' . $model->user_id, 'url' => ['view', 'user_id' => $model->user_id, 'role_id' => $model->role_id]];
$this->params['breadcrumbs'][] = Yii::t('main', 'Update');
?>
<div class="user-role-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        	'model' => $model,
    ]) ?>

</div>
