<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\ar\InformationPage */

$this->title = Yii::t('main', 'Update information page') . ' - ' . $model->symbol;
$this->params['breadcrumbs'][] = ['label' => Yii::t('main', 'Information pages'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => Yii::t('main', 'Information page') . ' - ' . $model->symbol, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('main', 'Update');
?>
<div class="information-page-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        	'model' => $model,
    ]) ?>

</div>
