<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\ar\InformationPage */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="information-page-form">

    <?php $form = ActiveForm::begin([
    		'id' => 'information-page-form-ajax',
    		'enableAjaxValidation' => true,
    ]); ?>

	<?= $form->field($model, 'symbol')->textInput(['maxlength' => true]) ?>

	<?= $form->field($model, 'priority')->input('number', ['min' => 0]) ?>

	<?= $form->field($model, 'is_active')->checkbox() ?>

<!--  <?= $form->field($model, 'date_creation')->textInput() ?> -->	

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('main', 'Create') : Yii::t('main', 'Save'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>