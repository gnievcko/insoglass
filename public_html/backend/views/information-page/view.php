<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use common\helpers\StringHelper;

/* @var $this yii\web\View */
/* @var $model common\models\ar\InformationPage */

$this->title = Yii::t('main', 'Information page') . ' - ' . $model->symbol;
$this->params['breadcrumbs'][] = ['label' => Yii::t('main', 'Information pages'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="information-page-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
    	<?php if(empty($withoutUpdating)) { ?>
    		<?= Html::a(Yii::t('main', 'Update'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
    	<?php } ?>
        <?php if(empty($withoutDeleting)) { ?>
	        <?= Html::a(Yii::t('main', 'Delete'), ['delete', 'id' => $model->id], [
		            'class' => 'btn btn-danger',
		            'data' => [
			                'confirm' => Yii::t('main', 'Are you sure you want to delete this item?'),
			                'method' => 'post',
		            ],
	        ]) ?>
		<?php } ?>
    </p>

    <?= DetailView::widget([
	        'model' => $model,
	        'formatter' => ['class' => 'yii\i18n\Formatter', 'nullDisplay' => ''],
	        'attributes' => [
					'id',
					'symbol',
					'priority',
	        		[
	        				'label' => Yii::t('main', 'Is active'),
	        				'value' => StringHelper::boolTranslation($model->is_active),
	        		],
					'date_creation',
        	],
    ]) ?>

</div>
<?php
         echo Yii::$app->controller->renderPartial('/information-page-translation/index', [
                         'dataProvider' => $informationPageTranslationDataProvider,
                         'informationPageId' => $model->id,
                         'withoutDecorations' => true,
        ]);
 ?>