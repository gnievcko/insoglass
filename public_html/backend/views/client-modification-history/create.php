<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\ar\ClientModificationHistory */

$this->title = Yii::t('main', 'Create client modification history');
$this->params['breadcrumbs'][] = ['label' => Yii::t('main', 'Client modification histories'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="client-modification-history-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        	'model' => $model,
    ]) ?>

</div>
