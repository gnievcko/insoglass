<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\BaseJson;

/* @var $this yii\web\View */
/* @var $model common\models\ar\ClientModificationHistory */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="client-modification-history-form">

    <?php $form = ActiveForm::begin([
    		'id' => 'client-modification-history-form-ajax',
    		'enableAjaxValidation' => true,
    ]); ?>

	<?= $form->field($model, 'modification')->textarea(['rows' => 6]) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('main', 'Create') : Yii::t('main', 'Save'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>