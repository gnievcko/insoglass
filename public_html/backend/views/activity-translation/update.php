<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\ar\ActivityTranslation */

$this->title = Yii::t('main', 'Update activity translation') . ' #' . $model->name;
$this->params['breadcrumbs'][] = ['label' => Yii::t('main', 'Activity translations'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => Yii::t('main', 'Activity translation') . ' #' . $model->name, 'url' => ['view', 'activity_id' => $model->activity_id, 'language_id' => $model->language_id]];
$this->params['breadcrumbs'][] = Yii::t('main', 'Update');
?>
<div class="activity-translation-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        	'model' => $model,
    ]) ?>

</div>
