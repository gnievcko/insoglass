<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\ar\ActivityTranslation */

$this->title = Yii::t('main', 'Create activity translation');
$this->params['breadcrumbs'][] = ['label' => Yii::t('main', 'Activity translations'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="activity-translation-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        	'model' => $model,
    ]) ?>

</div>
