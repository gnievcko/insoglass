<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use common\models\ar\Country;
use yii\helpers\ArrayHelper;
use yii\db\Expression;

/* @var $this yii\web\View */
/* @var $model common\models\ar\Province */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="province-form">

    <?php $form = ActiveForm::begin([
    		'id' => 'province-form-ajax',
    		'enableAjaxValidation' => true,
    ]); ?>

	<?php 
	$query = Country::find()->select(['country.id as id', 'COALESCE(country_translation.name, country.symbol) as symbol'])
			->orderBy(new Expression('COALESCE(country_translation.name, country.symbol)'))
	     	->joinWith(['countryTranslations' => function($q) {
				$q->joinWith('language');
		      	$q->where('language.symbol IS NULL OR language.symbol = :languageSymbol', [':languageSymbol' => Yii::$app->language]);
	     	}], true, 'LEFT OUTER JOIN');
	     
	if(!empty($countryId)) {
		$query->where('country.id = :countryId', [':countryId' => $countryId]);
	}
    
    echo $form->field($model, 'country_id')->dropDownList(ArrayHelper::map(		   
			$query->all(), 'id', 'symbol'
	))->label(Yii::t('main', 'Country'))	
	?>

	<?= $form->field($model, 'symbol')->textInput(['maxlength' => true]) ?>

	<?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('main', 'Create') : Yii::t('main', 'Save'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>