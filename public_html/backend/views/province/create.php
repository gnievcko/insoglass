<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\ar\Province */

$this->title = Yii::t('main', 'Create province');
$this->params['breadcrumbs'][] = ['label' => Yii::t('main', 'Provinces'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="province-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        	'model' => $model,
    		'countryId' => $countryId,
    ]) ?>

</div>
