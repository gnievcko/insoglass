<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\ar\AlertTypeTranslation */

$this->title = Yii::t('main', 'Alert type translation') . ' - ' . $model->name;
$this->params['breadcrumbs'][] = ['label' => Yii::t('main', 'Alert type translations'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="alert-type-translation-view">

    	<h1><?= Html::encode($this->title) ?></h1>
	
    <p>
    	<?php if(empty($withoutUpdating)) { ?>
    		<?= Html::a(Yii::t('main', 'Update'), ['update', 'alert_type_id' => $model->alert_type_id, 'language_id' => $model->language_id], ['class' => 'btn btn-primary']) ?>
    	<?php } ?>
        <?php if(empty($withoutDeleting)) { ?>
	        <?= Html::a(Yii::t('main', 'Delete'), ['delete', 'alert_type_id' => $model->alert_type_id, 'language_id' => $model->language_id], [
		            'class' => 'btn btn-danger',
		            'data' => [
			                'confirm' => Yii::t('main', 'Are you sure you want to delete this item?'),
			                'method' => 'post',
		            ],
	        ]) ?>
		<?php } ?>
    </p>

    <?= DetailView::widget([
	        'model' => $model,
	        'formatter' => ['class' => 'yii\i18n\Formatter', 'nullDisplay' => ''],
	        'attributes' => [
	        		[
	        			'attribute' => 'alertTypeSymbol',
	        			'format' => 'raw',
	        			'value' => Html::a($model->alertTypeSymbol, ['/alert-type/view', 'id' => $model->alert_type_id]),
	        		],
	        		[
	        			'attribute' => 'languageName',
	        			'format' => 'raw',
	        			'value' => Html::a($model->languageName, ['/language/view', 'id' => $model->language_id]),
	        		],
					'name',
        	],
    ]) ?>

</div>
