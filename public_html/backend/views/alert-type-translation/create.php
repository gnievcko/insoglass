<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\ar\AlertTypeTranslation */

$this->title = Yii::t('main', 'Create alert type translation');
$this->params['breadcrumbs'][] = ['label' => Yii::t('main', 'Alert type translations'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="alert-type-translation-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        	'model' => $model,
    		'alertTypeId' => $alertTypeId
    ]) ?>

</div>
