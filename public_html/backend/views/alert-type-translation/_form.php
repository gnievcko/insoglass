<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use common\models\ar\Language;
use common\models\ar\AlertType;


/* @var $this yii\web\View */
/* @var $model common\models\ar\AlertTypeTranslation */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="alert-type-translation-form">

    <?php $form = ActiveForm::begin([
    		'id' => 'alert-type-translation-form-ajax',
    		'enableAjaxValidation' => true,
    ]); ?>

	<?= $form->field($model, 'alert_type_id')->dropDownList(ArrayHelper::map(
			empty($alertTypeId) ? AlertType::find()->all() : AlertType::find()->where(['id' => $alertTypeId])->all(), 'id', 'symbol'		
	))->label(Yii::t('main', 'Symbol')) ?>
	
	<?= $form->field($model, 'language_id')->dropDownList(ArrayHelper::map(
			Language::find()->where('is_active = 1')->all(), 'id', 'name'
	))->label(Yii::t('main', 'Language')) ?>


	<?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('main', 'Create') : Yii::t('main', 'Save'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>