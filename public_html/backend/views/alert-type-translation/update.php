<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\ar\AlertTypeTranslation */

$this->title = Yii::t('main', 'Update alert type translation') . ' - ' . $model->alertTypeSymbol;
$this->params['breadcrumbs'][] = ['label' => Yii::t('main', 'Alert type translations'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => Yii::t('main', 'Alert type translation') . ' - ' . $model->alertTypeSymbol, 'url' => ['view', 'alert_type_id' => $model->alert_type_id, 'language_id' => $model->language_id]];
$this->params['breadcrumbs'][] = Yii::t('main', 'Update');
?>
<div class="alert-type-translation-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        	'model' => $model,
    ]) ?>

</div>
