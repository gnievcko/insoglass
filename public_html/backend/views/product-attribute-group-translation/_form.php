<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use common\models\ar\ProductAttributeGroup;
use common\models\ar\Language;

/* @var $this yii\web\View */
/* @var $model common\models\ar\ProductAttributeGroupTranslation */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="product-attribute-group-translation-form">

    <?php $form = ActiveForm::begin([
    		'id' => 'product-attribute-group-translation-form-ajax',
    		'enableAjaxValidation' => true,
    ]); ?>

	<?= $form->field($model, 'product_attribute_group_id')->dropDownList(ArrayHelper::map(
	       empty($productAttributeGroupId) ? ProductAttributeGroup::find()->orderBy('id')->all() : ProductAttributeGroup::find()
	               ->where(['id' => $productAttributeGroupId])->all(), 'id', 'symbol'
	    ))->label(Yii::t('main', 'Product attribute group')) ?>

	<?= $form->field($model, 'language_id')->dropDownList(ArrayHelper::map(
	       Language::find()->where('is_active = 1')->all(), 'id', 'name'
	    ))->label(Yii::t('main', 'Language')) ?>

	<?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('main', 'Create') : Yii::t('main', 'Save'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>