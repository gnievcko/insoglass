<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\ar\ProductAttributeGroupTranslation */

$this->title = Yii::t('main', 'Update product attribute group translation') . ' - ' . $model->name;
$this->params['breadcrumbs'][] = ['label' => Yii::t('main', 'Product attribute group translations'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => Yii::t('main', 'Product attribute group translation') . ' - ' . $model->name, 'url' => ['view', 'product_attribute_group_id' => $model->product_attribute_group_id, 'language_id' => $model->language_id]];
$this->params['breadcrumbs'][] = Yii::t('main', 'Update');
?>
<div class="product-attribute-group-translation-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        	'model' => $model,
    ]) ?>

</div>
