<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\ar\ProductAttributeGroupTranslation */

$this->title = Yii::t('main', 'Create product attribute group translation');
$this->params['breadcrumbs'][] = ['label' => Yii::t('main', 'Product attribute group translations'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="product-attribute-group-translation-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        	'model' => $model,
            'productAttributeGroupId' => $productAttributeGroupId,
    ]) ?>

</div>
