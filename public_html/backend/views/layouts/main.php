<?php

/* @var $this \yii\web\View */
/* @var $content string */

use backend\assets\AppAsset;
use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use common\widgets\Alert;
use common\helpers\Utility;
use common\helpers\StringHelper;

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
	<meta charset="<?= Yii::$app->charset ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<?= Html::csrfMetaTags() ?>
	<title><?= Html::encode($this->title) ?></title>
	<link rel='shortcut icon' type='image/x-icon' href='/favicon.ico' />
	<?php $this->head() ?>
</head>
<body>
<?php $this->beginBody() ?>

<div class="wrap">
	<?php
	NavBar::begin([
			'brandLabel' => '<img class="logo" src="'.Yii::$app->urlManagerFrontend->getBaseUrl().'/images/logo.png" /><span>Panel<span class="colorize-1">.</span>zarządzania</span>',
			'brandUrl' => Yii::$app->homeUrl,
			'options' => [
					'class' => 'navbar-inverse navbar-fixed-top',
			],
	]);

	$menuItems = [];

	if(Yii::$app->user->isGuest) {
		$menuItems[] = ['label' => Yii::t('web', 'Login'), 'url' => ['/site/login']];
	}
	else {
		$menuItems[] = [
				'label' => Yii::t('main', 'Economic entities'),	
				'url' => ['entity/index'],
		];
		$menuItems[] = [
				'label' => StringHelper::translateOrdertoOffer('main', 'Orders'),
				'items' => [
						['label' => Yii::t('main', 'Addresses'), 'url' => ['address/index']],
						['label' => Yii::t('main', 'Companies'), 'url' => ['company/index']],
						['label' => Yii::t('main', 'Company groups'), 'url' => ['company-group/index']],
                                                ['label' => Yii::t('main', 'Document sections'), 'url' => ['document-section/index']]
				],
		];
		$menuItems[] = [
				'label' => Yii::t('main', 'Users'),
				'items' => [
						['label' => Yii::t('main', 'Employees'), 'url' => ['user/index']],
						'<li class="divider hidden-xs"></li>',
						['label' => Yii::t('main', 'Assignment group salesmen'), 'url' => ['user-company-salesman/index']]
				],
		];
		$menuItems[] = [
				'label' => Yii::t('main', 'Settings'),
				'items' => [
						['label' => Yii::t('main', 'Languages'), 'url'=>['language/index']],
						['label' => Yii::t('main', 'Currencies'), 'url'=>['currency/index']],
						['label' => Yii::t('main', 'Roles'), 'url' => ['/role/index']],
						'<li class="divider hidden-xs"></li>',
						['label' => Yii::t('main', 'E-mail outboxes configurations'), 'url' => ['email-config/index']],
						['label' => Yii::t('main', 'E-mail templates'), 'url' => ['email-template/index']],
						'<li class="divider hidden-xs"></li>',
						['label' => Yii::t('main', 'Information pages'), 'url' => ['information-page/index']],
						'<li class="divider hidden-xs"></li>',
						['label' => StringHelper::translateOrdertoOffer('main', 'Orders statuses'), 'url' => ['order-status/index']],
						['label' => StringHelper::translateOrderToOffer('main', 'Transitions between statuses orders'), 'url' => ['order-status-transition/index']],
				]
		];

		$menuItems[] = [
				'label' => Yii::t('main', 'Logout').' (' . Yii::$app->user->identity->getIdentityName() . ')',
				'url' => ['/site/logout'],
				'linkOptions' => ['data-method' => 'post']
		];
	}

	echo Nav::widget([
			'options' => ['class' => 'navbar-nav navbar-right'],
			'items' => $menuItems,
	]);
	NavBar::end();
	?>

	<div class="container">
		<?= Breadcrumbs::widget([
				'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
		]) ?>
		<?= Alert::widget() ?>
		<?= $content ?>
	</div>
</div>

<footer class="footer">
	<div class="container">
		<div class="row">
			<div class="col-sm-12 lead col-xs-10 col-xs-offset-1 col-sm-offset-0">INSOGLAS Andrzej i Karol Woźniak sp.j.</div>
		</div>
		<div class="row">
			<div class="col-sm-12 col-xs-10 col-xs-offset-1 col-sm-offset-0">
				<?= Html::a(Yii::$app->getRequest()->getHostInfo(), Yii::$app->getRequest()->getHostInfo()) ?>
			</div>
		</div>
		<div class="row text-center mindseater small">
			<div class="col-xs-12">&copy; <?= date('Y') ?> by INSOGLAS Andrzej i Karol Woźniak sp.j. &#183; created by <?= Html::a(Yii::$app->params['implementingCompany'], 'http://mindseater.com/') ?></span></div>
		</div>
	</div>
</footer>

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
