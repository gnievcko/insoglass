<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\ar\CountryTranslation */

$this->title = Yii::t('main', 'Update translation');
$this->params['breadcrumbs'][] = ['label' => Yii::t('main', 'Country translations'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => Yii::t('main', 'Country translation') . ' - ' . $model->countrySymbol, 'url' => ['view', 'country_id' => $model->country_id, 'language_id' => $model->language_id]];
$this->params['breadcrumbs'][] = Yii::t('main', 'Update');
?>
<div class="country-translation-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        	'model' => $model,
    ]) ?>

</div>
