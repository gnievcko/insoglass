<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\ar\CountryTranslation */

$this->title = Yii::t('main', 'Country translation') . ' - ' . $model->countrySymbol;
$this->params['breadcrumbs'][] = ['label' => Yii::t('main', 'Country translations'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="country-translation-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
    	<?php if(empty($withoutUpdating)) { ?>
    		<?= Html::a(Yii::t('main', 'Update'), ['update', 'country_id' => $model->country_id, 'language_id' => $model->language_id], ['class' => 'btn btn-primary']) ?>
    	<?php } ?>
        <?php if(empty($withoutDeleting)) { ?>
	        <?= Html::a(Yii::t('main', 'Delete'), ['delete', 'country_id' => $model->country_id, 'language_id' => $model->language_id], [
		            'class' => 'btn btn-danger',
		            'data' => [
			                'confirm' => Yii::t('main', 'Are you sure you want to delete this item?'),
			                'method' => 'post',
		            ],
	        ]) ?>
		<?php } ?>
    </p>

    <?= DetailView::widget([
	        'model' => $model,
    		'formatter' => ['class' => 'yii\i18n\Formatter', 'nullDisplay' => ''],
	        'attributes' => [
	        		[
			        		'attribute' => 'countrySymbol',
			        		'format' => 'raw',
			        		'value' => Html::a($model->countrySymbol, ['/country/view', 'id' => $model->country_id]),
	        		],
					[
			        		'attribute' => 'languageName',
			        		'format' => 'raw',
			        		'value' => Html::a($model->languageName, ['/language/view', 'id' => $model->language_id]),
	        		],
					'name',
        	],
    ]) ?>

</div>
