<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use common\models\ar\Country;
use common\models\ar\Language;

/* @var $this yii\web\View */
/* @var $model common\models\ar\CountryTranslation */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="country-translation-form">

    <?php $form = ActiveForm::begin([
    		'id' => 'country-translation-form-ajax',
    		'enableAjaxValidation' => true,
    ]); ?>

	<?= $form->field($model, 'country_id')->dropDownList(ArrayHelper::map(
			empty($countryId) ? Country::find()->orderBy('priority')->all() : Country::find()->where(['id' => $countryId])->all(), 'id', 'symbol'		
	))->label(Yii::t('main', 'Country')) ?>

	<?= $form->field($model, 'language_id')->dropDownList(ArrayHelper::map(
			Language::find()->where('is_active = 1')->all(), 'id', 'name'
	))->label(Yii::t('main', 'Language')) ?>

	<?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('main', 'Create') : Yii::t('main', 'Save'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>