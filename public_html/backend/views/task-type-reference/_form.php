<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use common\models\ar\TaskType;
use yii\db\Query;

/* @var $this yii\web\View */
/* @var $model common\models\ar\TaskTypeReference */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="task-type-reference-form">

    <?php $form = ActiveForm::begin([
    		'id' => 'task-type-reference-form-ajax',
    		'enableAjaxValidation' => true,
    ]); ?>    
  
        <?= $form->field($model, 'task_type_id')->dropDownList(ArrayHelper::map(
        			(new Query())->select(['tt.id as id', 'COALESCE(ttt.name, tt.symbol) as name'])
        					->from('task_type tt')
        					->join('LEFT JOIN', 'task_type_translation ttt', 'tt.id = ttt.task_type_id')
        					->join('INNER JOIN', 'language l', 'ttt.language_id = l.id')
        					->where(['l.symbol' => Yii::$app->language])->all(),		
    			 'id', 'name'		
		),['prompt' => '-'])->label(Yii::t('main', 'Task type')) ?>
    

	<?= $form->field($model, 'symbol')->textInput(['maxlength' => true]) ?>

	<?= $form->field($model, 'name_table')->textInput(['maxlength' => true]) ?>

	<?= $form->field($model, 'name_column')->textInput(['maxlength' => true]) ?>
	
	<?= $form->field($model, 'name_expression')->textInput(['maxlength' => true]) ?>
	
	<?= $form->field($model, 'url_details')->textInput(['maxlength' => true]) ?>
	
	<?= $form->field($model, 'is_required')->checkbox() ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('main', 'Create') : Yii::t('main', 'Save'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>