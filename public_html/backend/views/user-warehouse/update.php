<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\ar\UserWarehouse */

$this->title = Yii::t('main', 'Update user warehouse') . ' #' . $model->user_id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('main', 'User warehouses'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => Yii::t('main', 'User warehouse') . ' #' . $model->user_id, 'url' => ['view', 'user_id' => $model->user_id, 'warehouse_id' => $model->warehouse_id]];
$this->params['breadcrumbs'][] = Yii::t('main', 'Update');
?>
<div class="user-warehouse-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        	'model' => $model,
    ]) ?>

</div>
