<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\ar\UserWarehouse */

$this->title = Yii::t('main', 'Create assignment of user to warehouse');
$this->params['breadcrumbs'][] = ['label' => Yii::t('main', 'Assignment of users to warehouses'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="user-warehouse-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        	'model' => $model,
    		'userId' => $userId,
    ]) ?>

</div>
