<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\ar\UserWarehouse */

$this->title = Yii::t('main', 'Assignment of user to warehouse') . ' - ' . $model->userName;
$this->params['breadcrumbs'][] = ['label' => Yii::t('main', 'Assignment of users to warehouses'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="user-warehouse-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
    	<?php if(empty($withoutUpdating)) { ?>
    		<?= Html::a(Yii::t('main', 'Update'), ['update', 'user_id' => $model->user_id, 'warehouse_id' => $model->warehouse_id], ['class' => 'btn btn-primary']) ?>
    	<?php } ?>
        <?php if(empty($withoutDeleting)) { ?>
	        <?= Html::a(Yii::t('main', 'Delete'), ['delete', 'user_id' => $model->user_id, 'warehouse_id' => $model->warehouse_id], [
		            'class' => 'btn btn-danger',
		            'data' => [
			                'confirm' => Yii::t('main', 'Are you sure you want to delete this item?'),
			                'method' => 'post',
		            ],
	        ]) ?>
		<?php } ?>
    </p>

    <?= DetailView::widget([
	        'model' => $model,
	        'formatter' => ['class' => 'yii\i18n\Formatter', 'nullDisplay' => ''],
	        'attributes' => [
					[
    						'label' => Yii::t('main', 'User'),
    						'format' => 'raw',
    						'value' => Html::a($model->userName, ['/user/view', 'id' => $model->user_id]),
    				],
    				[
    						'label' => Yii::t('main', 'Warehouse'),
    						'format' => 'raw',
    						'value' => Html::a($model->warehouseName, ['/warehouse/view', 'id' => $model->warehouse_id]),
    				],
        	],
    ]) ?>

</div>
