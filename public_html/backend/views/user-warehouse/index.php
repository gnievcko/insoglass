<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $searchModel common\models\ars\UserWarehouseSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

if(empty($withoutDecorations)) {
	if(!empty($searchModel)) {
		$this->title = Yii::t('main', 'Assignment of users to warehouses');
		$this->params['breadcrumbs'][] = $this->title;
		
		$this->registerJs('
			$(".filter-button").click(function(e){
				e.preventDefault();
				$(".search-form").toggle();
			});
		');
	}
}
?>
<div class="user-warehouse-index">

	<?php if(empty($withoutDecorations)) { ?>
    	<h1><?= Html::encode($this->title) ?></h1>
	<?php } else { ?>
    	<h2><?= empty($warehouseId) ? Yii::t('main', 'Warehouses') : Yii::t('main', 'Users')?></h2>
	<?php } ?>

	<?php if(!empty($searchModel)) { ?>
		<div class="search-form" style="display:none">
			<?php // echo $this->render('_search', ['model' => $searchModel]); ?>
		</div>
	<?php } ?>

	    <p>
    	<?php if(empty($withoutCreating)) { ?>
			<?= Html::a(
						Yii::t('main', 'Create assignment of user to warehouse'), 
						['user-warehouse/create', 'userId' => !empty($userId) ? $userId : null],
						['class' => 'btn btn-success']
				); 
			if(!empty($withoutDecorations)){
				echo ' ' .Html::a(Yii::t('main', 'List of all assignments of usert to warehouses'), ['/user-warehouse/index'], ['class' => 'btn btn-info']);
			}
			?>
		<?php } ?>
		<?php if(!empty($searchModel)) {
			//echo  Html::button(Yii::t('main', 'Filters'), ['class' => 'btn btn-primary filter-button']);
		} ?>
    </p>

	<?php Pjax::begin(); ?>
    	<?= GridView::widget([
        		'dataProvider' => $dataProvider,
        		'formatter' => ['class' => 'yii\i18n\Formatter', 'nullDisplay' => ''],
        		'filterModel' => !empty($searchModel) ? $searchModel : null,
				'columns' => [
						[
								'attribute' => 'userName',
								'visible' => empty($userId),
						],
						[
								'attribute' => 'warehouseName',
								'visible' => empty($warehouseId),
						],
            			[
            					'class' => 'backend\components\ActionColumn',
            					'controller' => 'user-warehouse',
            					'deleteConfirmMessage' => Yii::t('main', 'Are you sure you want to delete this item?'),
            					'visibleButtons' => ['delete' => empty($withoutDeleting), 'update' => empty($withoutUpdating)],
								'contentOptions' => ['class' => 'action-column'],
            			],
        		],
    	]); ?>
	<?php Pjax::end(); ?>

</div>
