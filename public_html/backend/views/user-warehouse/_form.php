<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use common\models\ar\User;
use common\models\ar\Warehouse;
use common\helpers\Utility;
use frontend\helpers\UserHelper;
use yii\helpers\ArrayHelper;
use yii\db\Query;

/* @var $this yii\web\View */
/* @var $model common\models\ar\UserWarehouse */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="user-warehouse-form">

    <?php $form = ActiveForm::begin([
    		'id' => 'user-warehouse-form-ajax',
    		'enableAjaxValidation' => true,
    ]); ?>
	<?php 
		$users = (new Query())->select(['u.id', 'u.email', 'u.first_name', 'u.last_name'])
				->from('user u')
				->join('INNER JOIN', 'user_role ur', 'u.id = ur.user_id')
				->join('INNER JOIN', 'role r', 'ur.role_id = r.id')
				->where(['r.symbol' => [Utility::ROLE_STOREKEEPER, Utility::ROLE_MAIN_STOREKEEPER, Utility::ROLE_ADMIN]]);
	?>
	
    <?= //TODO:odpowiednie wyświetlanie
    $form->field($model, 'user_id', ['enableAjaxValidation' => true])->dropDownList(ArrayHelper::map(
			(empty($userId) ? $users->all() : $users->where(['u.id' => $userId])->all()), 
			'id', 
    		'email'
    ))->label(Yii::t('main', 'User')) ?>
    
	<?= $form->field($model, 'warehouse_id', ['enableAjaxValidation' => true])->dropDownList(ArrayHelper::map(
			(empty($warehouseId) ? Warehouse::find()->orderBy('name')->all() : Warehouse::find()->where(['id' => $warehouseId])->one()), 
			'id', 
    		'name'
    ))->label(Yii::t('main', 'Warehouse')) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('main', 'Create') : Yii::t('main', 'Save'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>