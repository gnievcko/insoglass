<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use common\models\ar\ProductAttributeGroup;
use common\models\ar\ProductAttributeGroupTranslation;
use common\models\aq\ProductAttributeGroupQuery;

/* @var $this yii\web\View */
/* @var $model common\models\ar\ProductAttributeType */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="product-attribute-type-form">

    <?php $form = ActiveForm::begin([
    		'id' => 'product-attribute-type-form-ajax',
    		'enableAjaxValidation' => true,
    ]); ?>

	<?php 
        if(!empty($productAttributeGroupId)) {
            $groupList = ProductAttributeGroupQuery::getNamesList($productAttributeGroupId);
        }
        else {
            $groupList = ProductAttributeGroupQuery::getNamesList();
        }
	      
	    echo $form->field($model, 'product_attribute_group_id')->dropDownList(ArrayHelper::map(
	       $groupList,'id', 'name')
	        )->label(Yii::t('main', 'Group')) ?>

	<?= $form->field($model, 'symbol')->textInput(['maxlength' => true]) ?>

	<?= $form->field($model, 'variable_type')->textInput(['maxlength' => true]) ?>

	<?= $form->field($model, 'attribute_checked_ids')->textInput(['maxlength' => true]) ?>

	<?= $form->field($model, 'attribute_blocked_ids')->textInput(['maxlength' => true]) ?>

	<?= $form->field($model, 'is_visible')->checkbox() ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('main', 'Create') : Yii::t('main', 'Save'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>