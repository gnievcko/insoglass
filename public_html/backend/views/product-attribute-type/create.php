<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\ar\ProductAttributeType */

$this->title = Yii::t('main', 'Create product attribute type');
$this->params['breadcrumbs'][] = ['label' => Yii::t('main', 'Product attribute types'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="product-attribute-type-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        	'model' => $model,
            'productAttributeGroupId' => $productAttributeGroupId,
    ]) ?>

</div>
