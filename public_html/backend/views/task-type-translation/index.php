<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $searchModel common\models\ars\TaskTypeTranslationSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

if(empty($withoutDecorations)) {
	if(!empty($searchModel)) {
		$this->title = Yii::t('main', 'Task type translations');
		$this->params['breadcrumbs'][] = $this->title;
		
		$this->registerJs('
			$(".filter-button").click(function(e){
				e.preventDefault();
				$(".search-form").toggle();
			});
		');
	}
}
?>
<div class="task-type-translation-index">

	<?php if(empty($withoutDecorations)) { ?>
    	<h1><?= Html::encode($this->title) ?></h1>
	<?php } else { ?>
    	<h2><?= Yii::t('main', 'Translations')?></h2>
	<?php } ?>

	<?php if(!empty($searchModel)) { ?>
		<div class="search-form" style="display:none">
			<?php // echo $this->render('_search', ['model' => $searchModel]); ?>
		</div>
	<?php } ?>

	
	
	    <p>
    	<?php if(empty($withoutCreating)) { ?>
    			<?php
    			echo Html::a(
    				Yii::t('main', 'Create task type translation'),
    				['task-type-translation/create', 'taskTypeId' => !empty($taskTypeId) ? $taskTypeId : null],
    				['class' => 'btn btn-success']
    			);
    			
    			if(!empty($taskTypeId)) {
    				echo ' '. Html::a(Yii::t('main', 'List of all translations'), ['/task-type-translation/index'], ['class' => 'btn btn-info']);
    			}
    		?>
		<?php } ?>
			<?php if(!empty($searchModel)) {
				//echo Html::button(Yii::t('main', 'Filters'), ['class' => 'btn btn-primary filter-button']);
			} ?>
    </p>
	

	<?php Pjax::begin(); ?>
    	<?= GridView::widget([
        		'dataProvider' => $dataProvider,
        		'formatter' => ['class' => 'yii\i18n\Formatter', 'nullDisplay' => ''],
        		'filterModel' => !empty($searchModel) ? $searchModel : null,
				'columns' => [
						[
							'attribute' => 'taskTypeSymbol',
							'visible' => empty($withoutDecorations),
						], 
						'languageName',
						'name',
            			[
            					'class' => 'backend\components\ActionColumn',
            					'controller' => 'task-type-translation',
            					'deleteConfirmMessage' => Yii::t('main', 'Are you sure you want to delete this item?'),
            					'visibleButtons' => ['delete' => empty($withoutDeleting), 'update' => empty($withoutUpdating)],
								'contentOptions' => ['class' => 'action-column'],
            			],
        		],
    	]); ?>
	<?php Pjax::end(); ?>

</div>
