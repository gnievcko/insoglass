<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\ar\TaskTypeTranslation */

$this->title = Yii::t('main', 'Create task type translation');
$this->params['breadcrumbs'][] = ['label' => Yii::t('main', 'Task type translations'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="task-type-translation-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        	'model' => $model,
    		'taskTypeId' => $taskTypeId,
    ]) ?>

</div>
