<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use common\models\ar\TaskType;
use common\models\ar\Language;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $model common\models\ar\TaskTypeTranslation */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="task-type-translation-form">

    <?php $form = ActiveForm::begin([
    		'id' => 'task-type-translation-form-ajax',
    		'enableAjaxValidation' => true,
    ]); ?>
  
    <?= $form->field($model, 'task_type_id')->dropDownList(ArrayHelper::map(
	       empty($taskTypeId) ? TaskType::find()->all() : TaskType::find()->where(['id' => $taskTypeId])->all(),
    			 'id', 'symbol' 		
		),['prompt' => '-'])->label(Yii::t('main', 'Task type')) ?>

	<?= $form->field($model, 'language_id')->dropDownList(ArrayHelper::map(
	        Language::find()->all(), 'id', 'name'		
		),['prompt' => '-'])->label(Yii::t('main', 'Language')) ?>
		
	<?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('main', 'Create') : Yii::t('main', 'Save'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>