<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\ar\TaskTypeTranslation */

$this->title = Yii::t('main', 'Task type translation') . ' - ' . $model->name;
$this->params['breadcrumbs'][] = ['label' => Yii::t('main', 'Task type translations'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="task-type-translation-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
    	<?php if(empty($withoutUpdating)) { ?>
    		<?= Html::a(Yii::t('main', 'Update'), ['update', 'task_type_id' => $model->task_type_id, 'language_id' => $model->language_id], ['class' => 'btn btn-primary']) ?>
    	<?php } ?>
        <?php if(empty($withoutDeleting)) { ?>
	        <?= Html::a(Yii::t('main', 'Delete'), ['delete', 'task_type_id' => $model->task_type_id, 'language_id' => $model->language_id], [
		            'class' => 'btn btn-danger',
		            'data' => [
			                'confirm' => Yii::t('main', 'Are you sure you want to delete this item?'),
			                'method' => 'post',
		            ],
	        ]) ?>
		<?php } ?>
    </p>

    <?= DetailView::widget([
	        'model' => $model,
	        'formatter' => ['class' => 'yii\i18n\Formatter', 'nullDisplay' => ''],
	        'attributes' => [
					[
                        'attribute' => 'taskTypeSymbol',
                        'format' => 'raw',
                        'value' => Html::a($model->taskTypeSymbol, ['/task-type/view', 'id' => $model->task_type_id]),
                    ],
					[
                        'attribute' => 'languageName',
                        'format' => 'raw',
                        'value' => Html::a($model->languageName, ['/language/view', 'id' => $model->language_id]),
                    ],
					'name',
        	],
    ]) ?>

</div>