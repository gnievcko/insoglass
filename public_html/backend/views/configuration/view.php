<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\ar\Configuration */

$this->title = Yii::t('main', 'Configuration'); //. '  ' . $model->id;
//$this->params['breadcrumbs'][] = ['label' => Yii::t('main', 'Configurations'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="configuration-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
    	<?php if(empty($withoutUpdating)) { ?>
    		<?= Html::a(Yii::t('main', 'Update'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
    	<?php } ?>
    </p>

    <?= DetailView::widget([
	        'model' => $model,
	        'formatter' => ['class' => 'yii\i18n\Formatter', 'nullDisplay' => ''],
	        'attributes' => [
					//'id',
					'path_mysqldump',
					'path_mongodbdump',
					'path_dbbackups',
					'days_dbbackups_expiration',
        	],
    ]) ?>

</div>
