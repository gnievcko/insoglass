<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\ar\Configuration */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="configuration-form">

    <?php $form = ActiveForm::begin([
    		'id' => 'configuration-form-ajax',
    		'enableAjaxValidation' => true,
    ]); ?>

	<?= $form->field($model, 'path_mysqldump')->textInput(['maxlength' => true]) ?>

	<?= $form->field($model, 'path_mongodbdump')->textInput(['maxlength' => true]) ?>

	<?= $form->field($model, 'path_dbbackups')->textInput(['maxlength' => true]) ?>

	<?= $form->field($model, 'days_dbbackups_expiration')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('main', 'Create') : Yii::t('main', 'Save'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>