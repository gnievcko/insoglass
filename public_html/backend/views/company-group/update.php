<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\ar\CompanyGroup */

$this->title = Yii::t('main', 'Update client group') . ' - ' . $model->symbol;
$this->params['breadcrumbs'][] = ['label' => Yii::t('main', 'Client groups'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => Yii::t('main', 'Client group') . ' - ' . $model->symbol, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('main', 'Update');
?>
<div class="company-group-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        	'model' => $model,
    ]) ?>

</div>
