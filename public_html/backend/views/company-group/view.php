<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use common\helpers\StringHelper;
use common\helpers\Utility as UtilityCommon;

/* @var $this yii\web\View */
/* @var $model common\models\ar\CompanyGroup */

$this->title = Yii::t('main', 'Client group') . ' - ' . $model->symbol;
$this->params['breadcrumbs'][] = ['label' => Yii::t('main', 'Client groups'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="company-group-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
    <?php if(empty($model->is_predefined)) {?>
    	<?php if(empty($withoutUpdating)) { ?>
    		<?= Html::a(Yii::t('main', 'Update'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
    	<?php } ?>
        <?php if(empty($withoutDeleting)) { ?>
	        <?= Html::a(Yii::t('main', 'Delete'), ['delete', 'id' => $model->id], [
		            'class' => 'btn btn-danger',
		            'data' => [
			                'confirm' => Yii::t('main', 'Are you sure you want to delete this item?'),
			                'method' => 'post',
		            ],
	        ]) ?>
		<?php } ?>
	<?php } ?>
    </p>

    <?= DetailView::widget([
	        'model' => $model,
	        'formatter' => ['class' => 'yii\i18n\Formatter', 'nullDisplay' => ''],
	        'attributes' => [
					'id',
					'symbol',
	        		[
	        				'label' => Yii::t('main', 'Is predefined'),
	        				'value' => StringHelper::boolTranslation($model->is_predefined),
	        		],
        	],
    ]) ?>

</div>
<?php
	echo Yii::$app->controller->renderPartial('/company-group-translation/index', [
    		'dataProvider' => $companyGroupTranslationDataProvider,
    		'companyGroupId' => $model->id,
    		'withoutDecorations' => true,
        ]);
 ?>
  
 <?php 
	echo Yii::$app->controller->renderPartial('/company-group-set/index', [
			'dataProvider' => $companyGroupSetDataProvider,
			'companyGroupId' => $model->id,
			'withoutDecorations' => true,
        ]);
 ?>
 
 <?php
 	echo Yii::$app->controller->renderPartial('/user-company-salesman/index', [
 			'dataProvider' => $userCompanySalesmanDataProvider,
 			'companyGroupId' => $model->id,
 			'withoutDecorations' => true,
 			'hideCompanyGroup' => true,
 			'withoutUpdating' => true,
 	]);	
?>
