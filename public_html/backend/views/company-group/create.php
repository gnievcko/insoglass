<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\ar\CompanyGroup */

$this->title = Yii::t('main', 'Create client group');
$this->params['breadcrumbs'][] = ['label' => Yii::t('main', 'Client groups'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="company-group-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        	'model' => $model,
    ]) ?>

</div>
