<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use common\helpers\StringHelper;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\ar\EmailConfig */

$this->title = Yii::t('main', 'E-mail outbox configuration') . ' - ' . $model->email;
$this->params['breadcrumbs'][] = ['label' => Yii::t('main', 'E-mail outboxes configurations'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="email-config-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
    	<?php if(empty($withoutUpdating)) { ?>
    		<?= Html::a(Yii::t('main', 'Update'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
    	<?php } ?>
        <?php if(empty($withoutDeleting)) { ?>
	        <?= Html::a(Yii::t('main', 'Delete'), ['delete', 'id' => $model->id], [
		            'class' => 'btn btn-danger',
		            'data' => [
			                'confirm' => Yii::t('main', 'Are you sure you want to delete this item?'),
			                'method' => 'post',
		            ],
	        ]) ?>
		<?php } ?>
    </p>

    <?= DetailView::widget([
	        'model' => $model,
    		'formatter' => ['class' => 'yii\i18n\Formatter', 'nullDisplay' => ''],
	        'attributes' => [
					'id',
					'email:email',
					[
							'attribute' => 'password',
							'value' => str_repeat('*', strlen($model->password)),
					],
					'port',
					'protocol',
					'host',
	        		[
	        				'attribute' => 'starttls',
	        				'value' => StringHelper::boolTranslation($model->starttls),
	        		],
	        		[
	        				'attribute' => 'smtp_auth',
	        				'value' => StringHelper::boolTranslation($model->smtp_auth),
	        		],
					'noreply_email:email',
        	],
    ]) ?>
    
    <?php $form = ActiveForm::begin([
    		'id' => 'email-config-test-form-ajax',
    		'enableAjaxValidation' => false,
    ]); ?>
	
	<div class="panel panel-default">
		<div class="panel-body">
			<fieldset>
		    	<legend><?= Yii::t('main', 'Testing message sending with this config') ?></legend>
			    <?= $form->field($emailConfigTestForm, 'email', ['enableAjaxValidation' => false])->input('text', []); ?>
			    <div class="form-group">
			    	<?= Html::submitButton(Yii::t('main', 'Send test message'), ['id' => 'test-mail', 'class' => 'btn btn-info']) ?>
			    </div>
		    </fieldset>
	    </div>
    </div>
    
    <?php ActiveForm::end(); ?>

</div>
