<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\ar\EmailConfig */

$this->title = Yii::t('main', 'Update configuration');
$this->params['breadcrumbs'][] = ['label' => Yii::t('main', 'E-mail outboxes configurations'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => Yii::t('main', 'E-mail outbox configuration') . ' - ' . $model->email, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('main', 'Update');
?>
<div class="email-config-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        	'model' => $model,
    ]) ?>

</div>
