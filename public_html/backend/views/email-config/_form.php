<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\ar\EmailConfig */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="email-config-form">

    <?php $form = ActiveForm::begin([
    		'id' => 'email-config-form-ajax',
    		'enableAjaxValidation' => true,
    ]); ?>

	<?= $form->field($model, 'email')->textInput(['maxlength' => true]) ?>

	<?= $form->field($model, 'password')->passwordInput(['maxlength' => true]) ?>

	<?= $form->field($model, 'port')->input('number', ['min' => 1]) ?>

	<?= $form->field($model, 'protocol')->textInput(['maxlength' => true]) ?>

	<?= $form->field($model, 'host')->textInput(['maxlength' => true]) ?>

	<?= $form->field($model, 'starttls')->checkbox() ?>

	<?= $form->field($model, 'smtp_auth')->checkbox() ?>

	<?= $form->field($model, 'noreply_email')->textInput(['maxlength' => true]) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('main', 'Create') : Yii::t('main', 'Save'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>