<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\ar\EmailConfig */

$this->title = Yii::t('main', 'Create configuration');
$this->params['breadcrumbs'][] = ['label' => Yii::t('main', 'E-mail outboxes configurations'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="email-config-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        	'model' => $model,
    ]) ?>

</div>
