<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\ar\ShapeTranslation */

$this->title = Yii::t('main', 'Shape translation') . ' - ' . $model->shapeSymbol;
$this->params['breadcrumbs'][] = ['label' => Yii::t('main', 'Shape translations'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="shape-translation-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
    	<?php if(empty($withoutUpdating)) { ?>
    		<?= Html::a(Yii::t('main', 'Update'), ['update', 'shape_id' => $model->shape_id, 'language_id' => $model->language_id], ['class' => 'btn btn-primary']) ?>
    	<?php } ?>
        <?php if(empty($withoutDeleting)) { ?>
	        <?= Html::a(Yii::t('main', 'Delete'), ['delete', 'shape_id' => $model->shape_id, 'language_id' => $model->language_id], [
		            'class' => 'btn btn-danger',
		            'data' => [
			                'confirm' => Yii::t('main', 'Are you sure you want to delete this item?'),
			                'method' => 'post',
		            ],
	        ]) ?>
		<?php } ?>
    </p>

    <?= DetailView::widget([
	        'model' => $model,
	        'formatter' => ['class' => 'yii\i18n\Formatter', 'nullDisplay' => ''],
	        'attributes' => [
	        		[
			        		'attribute' => 'shapeSymbol',
			        		'format' => 'raw',
			        		'value' => Html::a($model->shapeSymbol, ['/shape/view', 'id' => $model->shape_id]),
	        		],
					[
			        		'attribute' => 'languageName',
			        		'format' => 'raw',
			        		'value' => Html::a($model->languageName, ['/language/view', 'id' => $model->language_id]),
	        		],
					'name',
        	],
    ]) ?>

</div>
