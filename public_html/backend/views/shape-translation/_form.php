<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use common\models\ar\Shape;
use common\models\ar\Language;

/* @var $this yii\web\View */
/* @var $model common\models\ar\ShapeTranslation */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="shape-translation-form">

    <?php $form = ActiveForm::begin([
    		'id' => 'shape-translation-form-ajax',
    		'enableAjaxValidation' => true,
    ]); ?>

    <?= $form->field($model, 'shape_id')->dropDownList(ArrayHelper::map(
            empty($shapeId) ? Shape::find()->orderBy('id')->all() : Shape::find()->where(['id' => $shapeId])->all(), 'id', 'symbol'       
    ))->label(Yii::t('main', 'Shape')) ?>

    <?= $form->field($model, 'language_id')->dropDownList(ArrayHelper::map(
            Language::find()->where('is_active = 1')->all(), 'id', 'name'
    ))->label(Yii::t('main', 'Language')) ?>

	<?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('main', 'Create') : Yii::t('main', 'Save'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>