<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\ar\ShapeTranslation */

$this->title = Yii::t('main', 'Create translation');
$this->params['breadcrumbs'][] = ['label' => Yii::t('main', 'Shape translations'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="shape-translation-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        	'model' => $model,
        	'shapeId' => $shapeId,
    ]) ?>

</div>
