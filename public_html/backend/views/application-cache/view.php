<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\ar\ApplicationCache */

$this->title = Yii::t('main', 'Application cache') . ' - ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('main', 'Application cache'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="application-cache-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
    	<?php if(empty($withoutUpdating)) { ?>
    		<?= Html::a(Yii::t('main', 'Update'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
    	<?php } ?>
        <?php if(empty($withoutDeleting)) { ?>
	        <?= Html::a(Yii::t('main', 'Delete'), ['delete', 'id' => $model->id], [
		            'class' => 'btn btn-danger',
		            'data' => [
			                'confirm' => Yii::t('main', 'Are you sure you want to delete this item?'),
			                'method' => 'post',
		            ],
	        ]) ?>
		<?php } ?>
    </p>

    <?= DetailView::widget([
	        'model' => $model,
	        'formatter' => ['class' => 'yii\i18n\Formatter', 'nullDisplay' => ''],
	        'attributes' => [
                'entityName',
                ['attribute' => 'date_last_invoice', 'format' => 'raw', 'value' => explode(" ",$model->date_last_invoice)[0]],

            ],
    ]) ?>

</div>
