<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\ar\ApplicationCache */

$this->title = Yii::t('main', 'Update application cache') . ' - ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('main', 'Application cache'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => Yii::t('main', 'Application cache') . ' -' . $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('main', 'Update');
?>
<div class="application-cache-update">

    <h1><?= Html::encode($this->title) ?></h1>
    <?= $this->render('_form', [
        	'model' => $model,
    ]) ?>
</div>
