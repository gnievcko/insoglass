<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\ar\ApplicationCache */

$this->title = Yii::t('main', 'Create application cache');
$this->params['breadcrumbs'][] = ['label' => Yii::t('main', 'Application cache'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="application-cache-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        	'model' => $model,
            'entities' => $entities,
    ]) ?>

</div>
