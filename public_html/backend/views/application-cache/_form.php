<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\ar\ApplicationCache */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="application-cache-form">

    <?php $form = ActiveForm::begin([
    		'id' => 'application-cache-form-ajax',
    		'enableAjaxValidation' => true,
    ]); ?>

	<?= $form->field($model, 'date_last_invoice')->widget(\yii\jui\DatePicker::classname(), [
        'dateFormat' => 'yyyy-MM-dd',
    ]) ?>
    <?php if(!($model->isEditScenarioSet())){ ?>
	    <?= $form->field($model, 'entity_id')->dropdownList($entities, ['prompt'=>Yii::t('main','None')])->label(Yii::t('main','Economic entity')) ?>
    <?php } else{?>
        <?= $form->field($model, 'entity_id')->hiddenInput()->label(false); ?>

    <?php } ?>
    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('main', 'Create') : Yii::t('main', 'Save'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>