<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\ar\InformationPageTranslation */

$this->title = Yii::t('main', 'Create information page translation');
$this->params['breadcrumbs'][] = ['label' => Yii::t('main', 'Information page translations'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="information-page-translation-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        	'model' => $model,
    		'informationPageId' => $informationPageId,
    ]) ?>

</div>
