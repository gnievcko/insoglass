<?php

use yii\web\View;
use backend\helpers\Utility;
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use dosamigos\ckeditor\CKEditor;
use common\models\ar\InformationPage;
use yii\helpers\ArrayHelper;
use common\models\ar\Language;

/* @var $this yii\web\View */
/* @var $model common\models\ar\InformationPageTranslation */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="information-page-translation-form">

    <?php $form = ActiveForm::begin([
    		'id' => 'information-page-translation-form-ajax',
    		'enableAjaxValidation' => true,
    ]); ?>
	
	<?= $form->field($model, 'information_page_id', ['enableAjaxValidation' => true])->dropDownList(ArrayHelper::map(
         	(empty($informationPageId) ? InformationPage::find()->orderBy('symbol')->all() : InformationPage::find()->where(['id' => $informationPageId])->all()), 
             'id', 
             'symbol'
      ))->label(Yii::t('main', 'Information page')) ?>

	<?= $form->field($model, 'language_id', ['enableAjaxValidation' => true])->dropDownList(
  		ArrayHelper::map(Language::find()->where('is_active = 1')->all(), 'id', 'name'
  	))->label(Yii::t('main', 'Language')) ?>

	<?= $form->field($model, 'title')->textInput(['maxlength' => true]) ?>

	<?= $form->field($model, 'content')->widget(CKEditor::className(), Utility::getCKEditorParams()) ?>

	<?= $form->field($model, 'keyword')->textInput(['maxlength' => true]) ?>

	<?= $form->field($model, 'description')->textInput(['maxlength' => true]) ?>

	<?= $form->field($model, 'is_active')->checkbox() ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('main', 'Create') : Yii::t('main', 'Save'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>