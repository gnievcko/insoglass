<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\ar\InformationPageTranslation */
$this->title = Yii::t('main', 'Update information page translation') . ' - ' . $model->informationPageSymbol;
$this->params['breadcrumbs'][] = ['label' => Yii::t('main', 'Information page translations'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => Yii::t('main', 'Information page translation') . ' - ' . $model->informationPageSymbol, 'url' => ['view', 'information_page_id' => $model->information_page_id, 'language_id' => $model->language_id]];
$this->params['breadcrumbs'][] = Yii::t('main', 'Update');
?>
<div class="information-page-translation-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        	'model' => $model,
    ]) ?>

</div>
