<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use common\models\ar\InformationPage;
use yii\helpers\ArrayHelper;
use common\models\ar\Language;

/* @var $this yii\web\View */
/* @var $model common\models\ars\InformationPageTranslationSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<?php
$this->registerJs('
	$(".reset-button").click(function(e){
		e.preventDefault();
		$(".form-control").val("");
	});
');
?>

<div class="information-page-translation-search container">

    <?php $form = ActiveForm::begin([
	        'action' => ['index'],
	        'method' => 'get',
    ]); ?>
    
    <div class="col-md-6">
  			<?= $form->field($model, 'information_page_id')->dropDownList(ArrayHelper::map(	
  					InformationPage::find()->orderBy('symbol')->all(), 
  					'id', 
  					'symbol'
 			), ['prompt' => '-'])->label(Yii::t('main', 'Information page')) ?>

  			<?= $form->field($model, 'language_id')->dropDownList(
  					ArrayHelper::map(Language::find()->where('is_active = 1')->all(), 'id', 'name'
  			), ['prompt' => '-'])->label(Yii::t('main', 'Language')) ?>
  			
			<?= $form->field($model, 'title') ?>

			<?= $form->field($model, 'content') ?>
	</div>
	
	<div class="col-md-6">
		<?= $form->field($model, 'keyword') ?>

		<?= $form->field($model, 'description') ?>
		
		<?php echo $form->field($model, 'is_active')->dropDownList([''=>'', 0=>Yii::t('main', 'No'), 1=>Yii::t('main', 'Yes')]) ?>
	</div>


    <div class="form-group col-md-12">
        <?= Html::submitButton(Yii::t('main', 'Search'), ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton(Yii::t('main', 'Reset'), ['class' => 'btn btn-default reset-button']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
