<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use common\helpers\StringHelper;

/* @var $this yii\web\View */
/* @var $model common\models\ar\InformationPageTranslation */

$this->title = Yii::t('main', 'Information page translation') . ' - ' . $model->informationPageSymbol;
$this->params['breadcrumbs'][] = ['label' => Yii::t('main', 'Information page translations'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="information-page-translation-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
    	<?php if(empty($withoutUpdating)) { ?>
    		<?= Html::a(Yii::t('main', 'Update'), ['update', 'information_page_id' => $model->information_page_id, 'language_id' => $model->language_id], ['class' => 'btn btn-primary']) ?>
    	<?php } ?>
        <?php if(empty($withoutDeleting)) { ?>
	        <?= Html::a(Yii::t('main', 'Delete'), ['delete', 'information_page_id' => $model->information_page_id, 'language_id' => $model->language_id], [
		            'class' => 'btn btn-danger',
		            'data' => [
			                'confirm' => Yii::t('main', 'Are you sure you want to delete this item?'),
			                'method' => 'post',
		            ],
	        ]) ?>
		<?php } ?>
    </p>

    <?= DetailView::widget([
	        'model' => $model,
	        'formatter' => ['class' => 'yii\i18n\Formatter', 'nullDisplay' => ''],
	        'attributes' => [
	        		[
                    		'label' => Yii::t('main', 'Information page'),
                             'format' => 'raw',
                             'value' => Html::a($model->informationPageSymbol, ['/information-page/view', 'id' => $model->information_page_id]),
                    ],
               		[
                       		'label' => Yii::t('main', 'Language'),
                            'format' => 'raw',
                            'value' => Html::a($model->languageName, ['/language/view', 'id' => $model->language_id]),
                    ],
					'title',
	        		[
	        				'label' => Yii::t('main', 'Content'),
	        				'format' => 'html',
	        				'value' => $model->content,
	        		],
					'keyword',
					'description',
	        		[
	        	                 'label' => Yii::t('main', 'Is active'),
	        	                 'value' => StringHelper::boolTranslation($model->is_active),
	                ],
        	],
    ]) ?>

</div>
