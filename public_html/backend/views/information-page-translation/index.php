<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
use common\helpers\StringHelper;

/* @var $this yii\web\View */
/* @var $searchModel common\models\ars\InformationPageTranslationSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

if(empty($withoutDecorations)) {
	if(!empty($searchModel)) {
		$this->title = Yii::t('main', 'Information page translations');
		$this->params['breadcrumbs'][] = $this->title;
		
		$this->registerJs('
			$(".filter-button").click(function(e){
				e.preventDefault();
				$(".search-form").toggle();
			});
		');
	}
}
?>
<div class="information-page-translation-index">

	<?php if(empty($withoutDecorations)) { ?>
    	<h1><?= Html::encode(Yii::t('main', 'Information page translations')) ?></h1>
	<?php } 
	else { ?>
    	<h2><?= Yii::t('main', 'Translations') ?></h2>
	<?php } ?>

	<?php if(!empty($searchModel)) { ?>
		<div class="search-form" style="display:none">
			<?php echo $this->render('_search', ['model' => $searchModel]); ?>
		</div>
	<?php } ?>

    <p>
    	<?php if(empty($withoutCreating)) { ?>
    			<?php
    			echo Html::a(
    				Yii::t('main', 'Create information page translation'),
    				['information-page-translation/create', 'informationPageId' => !empty($informationPageId) ? $informationPageId : null],
    				['class' => 'btn btn-success']
    			);
    			
    			if(!empty($informationPageId)) {
    				echo ' '. Html::a(Yii::t('main', 'List of all translations'), ['/information-page-translation/index'], ['class' => 'btn btn-info']);
    			}
    		?>
		<?php } ?>
			<?php if(!empty($searchModel)) {
				echo Html::button(Yii::t('main', 'Filters'), ['class' => 'btn btn-primary filter-button']);
			} ?>
    </p>

	<?php Pjax::begin(); ?>
    	<?= GridView::widget([
        		'dataProvider' => $dataProvider,
        		'formatter' => ['class' => 'yii\i18n\Formatter', 'nullDisplay' => ''],
        		'filterModel' => !empty($searchModel) ? $searchModel : null,
				'columns' => [
						[
								'attribute' => 'informationPageSymbol',
								'visible' => !empty($searchModel),
						],
						'languageName',
						'title',
						[
								'attribute' => 'is_active',
								'filter' => [0 => Yii::t('main', 'No'), 1 => Yii::t('main', 'Yes')],
								'value' => function($model) { return StringHelper::boolTranslation($model->is_active); },
						],
            			[
            					'class' => 'backend\components\ActionColumn',
            					'controller' => 'information-page-translation',
            					'deleteConfirmMessage' => Yii::t('main', 'Are you sure you want to delete this item?'),
            					'visibleButtons' => ['delete' => empty($withoutDeleting), 'update' => empty($withoutUpdating)],
								'contentOptions' => ['class' => 'action-column'],
            			],
        		],
    	]); ?>
	<?php Pjax::end(); ?>

</div>
