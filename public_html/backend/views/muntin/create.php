<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\ar\Muntin */

$this->title = Yii::t('main', 'Create muntin');
$this->params['breadcrumbs'][] = ['label' => Yii::t('main', 'Muntins'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="muntin-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        	'model' => $model,
    ]) ?>

</div>
