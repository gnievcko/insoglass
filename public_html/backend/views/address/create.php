<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\ar\Address */

$this->title = Yii::t('main', 'Create address');
$this->params['breadcrumbs'][] = ['label' => Yii::t('main', 'Addresses'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="address-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        	'model' => $model,
    		'cityId' => $cityId,
    ]) ?>

</div>
