<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use common\models\ar\City;

/* @var $this yii\web\View */
/* @var $model common\models\ar\Address */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="address-form">

    <?php $form = ActiveForm::begin([
    		'id' => 'address-form-ajax',
    		'enableAjaxValidation' => true,
    ]); ?>

	<div class="row">
		<div class="col-sm-9">
			<?= $form->field($model, 'city_id')->dropDownList(ArrayHelper::map(
					empty($cityId) ? City::find()->orderBy('name')->all() : City::find()->where(['id' => $cityId])->all(), 'id', function($item) { return $item->zip_code.' '.$item->name; }	
			))->label(Yii::t('main', 'City')) ?>
		</div>
		<div class="col-sm-3 link-button-container">
			<?= Html::a(Yii::t('main', 'Create city'), ['/city/create'], ['class' => 'btn btn-primary filter-button create-link-button', 'target' => '_blank']);?>
		</div>
	</div>

	<?= $form->field($model, 'main')->textInput(['maxlength' => true]) ?>

	<?= $form->field($model, 'complement')->textInput(['maxlength' => true]) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('main', 'Create') : Yii::t('main', 'Save'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>