<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use common\models\ar\Construction;
use common\models\ar\Language;

/* @var $this yii\web\View */
/* @var $model common\models\ar\ConstructionTranslation */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="construction-translation-form">

    <?php $form = ActiveForm::begin([
    		'id' => 'construction-translation-form-ajax',
    		'enableAjaxValidation' => true,
    ]); ?>

    <?= $form->field($model, 'construction_id')->dropDownList(ArrayHelper::map(
            empty($constructionId) ? Construction::find()->orderBy('id')->all() : Construction::find()->where(['id' => $constructionId])->all(), 'id', 'symbol'       
    ))->label(Yii::t('main', 'Construction')) ?>

    <?= $form->field($model, 'language_id')->dropDownList(ArrayHelper::map(
            Language::find()->where('is_active = 1')->all(), 'id', 'name'
    ))->label(Yii::t('main', 'Language')) ?>

	<?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('main', 'Create') : Yii::t('main', 'Save'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>