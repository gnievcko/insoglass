<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use common\models\ar\CompanyGroup;
use common\models\ar\Language;

/* @var $this yii\web\View */
/* @var $model common\models\ar\CompanyGroupTranslation */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="company-group-translation-form">

    <?php $form = ActiveForm::begin([
    		'id' => 'company-group-translation-form-ajax',
    		'enableAjaxValidation' => true,
    ]); ?>

    <?= $form->field($model, 'company_group_id', ['enableAjaxValidation' => true])->dropDownList(ArrayHelper::map(
    	(empty($companyGroupId) ? CompanyGroup::find()->orderBy('symbol')->all() : CompanyGroup::find()->where(['id' => $companyGroupId])->all()), 
             'id', 
             'symbol'))->label(Yii::t('main', 'Client group')) ?> 			
				
	<?= $form->field($model, 'language_id', ['enableAjaxValidation' => true])->dropDownList(
  		ArrayHelper::map(Language::find()->where('is_active = 1')->all(), 'id', 'name'
  	))->label(Yii::t('main', 'Language')) ?>

	<?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('main', 'Create') : Yii::t('main', 'Save'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>