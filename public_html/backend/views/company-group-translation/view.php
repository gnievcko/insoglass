<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\ar\CompanyGroupTranslation */

$this->title = Yii::t('main', 'Client group translation') . ' - ' . $model->name;
$this->params['breadcrumbs'][] = ['label' => Yii::t('main', 'Client group translations'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="company-group-translation-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
    	<?php if(empty($withoutUpdating)) { ?>
    		<?= Html::a(Yii::t('main', 'Update'), ['update', 'company_group_id' => $model->company_group_id, 'language_id' => $model->language_id], ['class' => 'btn btn-primary']) ?>
    	<?php } ?>
        <?php if(empty($withoutDeleting)) { ?>
	        <?= Html::a(Yii::t('main', 'Delete'), ['delete', 'company_group_id' => $model->company_group_id, 'language_id' => $model->language_id], [
		            'class' => 'btn btn-danger',
		            'data' => [
			                'confirm' => Yii::t('main', 'Are you sure you want to delete this item?'),
			                'method' => 'post',
		            ],
	        ]) ?>
		<?php } ?>
    </p>

    <?= DetailView::widget([
	        'model' => $model,
	        'formatter' => ['class' => 'yii\i18n\Formatter', 'nullDisplay' => ''],
	        'attributes' => [
	        		[
	        				'label' => Yii::t('main', 'Client group'),
	        				'format' => 'raw',
	        				'value' => Html::a($model->companyGroupSymbol, ['/company-group/view', 'id' => $model->company_group_id]),
	        		],
					[
                       		'label' => Yii::t('main', 'Language'),
                            'format' => 'raw',
                            'value' => Html::a($model->languageName, ['/language/view', 'id' => $model->language_id]),
                    ],
					'name',
        	],
    ]) ?>

</div>
