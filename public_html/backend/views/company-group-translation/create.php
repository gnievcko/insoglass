<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\ar\CompanyGroupTranslation */

$this->title = Yii::t('main', 'Create company group translation');
$this->params['breadcrumbs'][] = ['label' => Yii::t('main', 'Client group translations'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="company-group-translation-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        	'model' => $model,
    		'companyGroupId' => $companyGroupId,
    ]) ?>

</div>
