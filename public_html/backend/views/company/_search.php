<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use common\models\ar\Language;
use yii\helpers\ArrayHelper;
use common\models\ar\Currency;

/* @var $this yii\web\View */
/* @var $model common\models\ars\CompanySearch */
/* @var $form yii\widgets\ActiveForm */
?>

<?php
$this->registerJs('
	$(".reset-button").click(function(e){
		e.preventDefault();
		$(".form-control").val("");
	});
');
?>

<div class="company-search container">

    <?php $form = ActiveForm::begin([
	        'action' => ['index'],
	        'method' => 'get',
    ]); ?>

	<div class="col-md-4">
		<?= $form->field($model, 'id') ?>
	
		<?= $form->field($model, 'parentCompanyName') ?>
	
		<?= $form->field($model, 'name') ?>
	
		<?= $form->field($model, 'email') ?>
	
		<?= $form->field($model, 'phone1') ?>
		
		<?= $form->field($model, 'fax1') ?>
	</div>

	<div class="col-md-4">
		<?= $form->field($model, 'addressName') ?>
	
		<?= $form->field($model, 'addressPostalName') ?>
		
		<?= $form->field($model, 'note') ?>
		
		<?= $form->field($model, 'vat_identification_number') ?>
	
		<?= $form->field($model, 'taxpayer_identification_number') ?>
		
		<?= $form->field($model, 'krs_number') ?>
	</div>

	<div class="col-md-4">
		<?= $form->field($model, 'language_id')->dropDownList(ArrayHelper::map(
				Language::find()->where('is_active = 1')->all(), 'id', 'name'
		), ['prompt' => '-'])->label(Yii::t('main', 'Language')) ?>
	
		<?= $form->field($model, 'currency_id')->dropDownList(ArrayHelper::map(
				Currency::find()->all(), 'id', 'symbol'
		), ['prompt' => '-'])->label(Yii::t('main', 'Currency')) ?>
	
		<?= $form->field($model, 'is_artificial')->dropDownList([ 
				0 => Yii::t('main', 'No'), 1 => Yii::t('main', 'Yes')				
		], ['prompt' => '-']) ?>
		
		<?= $form->field($model, 'is_active')->dropDownList([ 
				0 => Yii::t('main', 'No'), 1 => Yii::t('main', 'Yes')				
		], ['prompt' => '-']) ?>
	
		<?= $form->field($model, 'date_creation') ?>
	
		<?= $form->field($model, 'date_modification') ?>
	</div>

    <div class="form-group col-md-12">
        <?= Html::submitButton(Yii::t('main', 'Search'), ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton(Yii::t('main', 'Reset'), ['class' => 'btn btn-default reset-button']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
