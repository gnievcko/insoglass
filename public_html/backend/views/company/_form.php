<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\widgets\Select2;
use yii\helpers\Url;
use yii\web\JsExpression;
use yii\helpers\ArrayHelper;
use common\models\ar\Language;
use common\models\ar\Currency;

/* @var $this yii\web\View */
/* @var $model common\models\ar\Company */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="company-form">

    <?php $form = ActiveForm::begin([
    		'id' => 'company-form-ajax',
    		'enableAjaxValidation' => true,
    ]); ?>

	<?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

	<?= $form->field($model, 'email')->textInput(['maxlength' => true]) ?>

	<?= $form->field($model, 'phone1')->textInput(['maxlength' => true]) ?>

	<?= $form->field($model, 'phone2')->textInput(['maxlength' => true]) ?>
	
	<?= $form->field($model, 'fax1')->textInput(['maxLength' => true]) ?>
	
	<?= $form->field($model, 'vat_identification_number')->textInput(['maxLength' => true]) ?>
	
	<?= $form->field($model, 'taxpayer_identification_number')->textInput(['maxLength' => true]) ?>
	
	<?= $form->field($model, 'krs_number')->textInput(['maxLength' => true]) ?>
	
	<?php
		$function = '
				function(params) { 
					return {
						term:params.term, 
						page:params.page, 
						size: 20,
						excludedCompanyId: '.(!$model->isNewRecord ? $model->id : 0).'
					}; 
				}
		';
	
		echo $form->field($model, 'parent_company_id')
				->label(Yii::t('main', 'Parent company'))
				->widget(Select2::classname(), [
						'language' => Yii::$app->language,
						'options' => ['placeholder' => Yii::t('main', 'Select a company...')],
						'data' => $companyData,
						'pluginOptions' => [
								'allowClear' => true,
								'minimumInputLength' => 1,
								'ajax' => [
										'url' => Url::to(['site/find-company']),
										'dataType' => 'json',
										'delay' => 250,
										'data' => new JsExpression($function),
										'processResults' => new JsExpression('function (data, params) {
											params.page = params.page || 1;

											return {
												results: data.items,
												pagination: {
													more: data.more
												}
											};
										}'),
										'cache' => true
								],
								'templateResult' => new JsExpression('function(item) { if(item.loading) return item.text; return item.name; }'),
								'templateSelection' => new JsExpression('function (item) { return item.name || item.text; }'),
						]
		]);
	?>
	
	<div class="row">
		<div class="col-sm-9">
			<?php
				echo $form->field($model, 'address_id')
						->label(Yii::t('main', 'Address'))
						->widget(Select2::classname(), [
								'language' => Yii::$app->language,
								'options' => ['placeholder' => Yii::t('main', 'Select an address...')],
								'data' => $addressData,
								'pluginOptions' => [
										'allowClear' => true,
										'minimumInputLength' => 1,
										'ajax' => [
												'url' => Url::to(['site/find-address']),
												'dataType' => 'json',
												'delay' => 250,
												'data' => new JsExpression('function(params) { return {term:params.term, page:params.page, size: 20}; }'),
												'processResults' => new JsExpression('function (data, params) {
													params.page = params.page || 1;

													return {
														results: data.items,
														pagination: {
															more: data.more
														}
													};
												}'),
												'cache' => true
										],
										'templateResult' => new JsExpression('function(item) { if(item.loading) return item.text; return item.name; }'),
										'templateSelection' => new JsExpression('function (item) { return item.name || item.text; }'),
								]
				]);
			?>
		</div>
		<div class="col-sm-3 link-button-container">
			<?= Html::a(Yii::t('main', 'Create address'), ['/address/create'], ['class' => 'btn btn-primary filter-button create-link-button', 'target' => '_blank']);?>
		</div>
	</div>
	
	<div class="row">
		<div class="col-sm-9">
			<?php
				echo $form->field($model, 'address_postal_id')
						->label(Yii::t('main', 'Address for correspondence'))
						->widget(Select2::classname(), [
								'language' => Yii::$app->language,
								'options' => ['placeholder' => Yii::t('main', 'Select an address...')],
								'data' => $addressData,
								'pluginOptions' => [
										'allowClear' => true,
										'minimumInputLength' => 1,
										'ajax' => [
												'url' => Url::to(['site/find-address']),
												'dataType' => 'json',
												'delay' => 250,
												'data' => new JsExpression('function(params) { return {term:params.term, page:params.page, size: 20}; }'),
												'processResults' => new JsExpression('function (data, params) {
													params.page = params.page || 1;
		
													return {
														results: data.items,
														pagination: {
															more: data.more
														}
													};
												}'),
												'cache' => true
										],
										'templateResult' => new JsExpression('function(item) { if(item.loading) return item.text; return item.name; }'),
										'templateSelection' => new JsExpression('function (item) { return item.name || item.text; }'),
								]
						])
						->hint(Yii::t('main', 'Is required in case if is different than main address.'));
			?>
		</div>
	</div>
	
	<?= $form->field($model, 'note')->textarea(['maxLength' => true]) ?>

	<?= $form->field($model, 'language_id')->dropDownList(ArrayHelper::map(
			Language::find()->where('is_active = 1')->all(), 'id', 'name'
	), ['prompt' => '-'])->label(Yii::t('main', 'Language')) ?>

	<?= $form->field($model, 'currency_id')->dropDownList(ArrayHelper::map(
			Currency::find()->all(), 'id', 'symbol'
	), ['prompt' => '-'])->label(Yii::t('main', 'Currency')) ?>

	<?= $form->field($model, 'is_artificial')->checkbox()->hint(Yii::t('main', 'If the client does not belong to a real company.')) ?>

	<?= $form->field($model, 'is_active')->checkbox() ?>
	
	<?php if(!$model->isNewRecord) {
		echo $form->field($model, 'date_creation')->textInput(['readonly' => true]);
	} ?>
	
	<?php if(!$model->isNewRecord) {
		echo $form->field($model, 'date_modification')->textInput(['readonly' => true]);
	} ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('main', 'Create') : Yii::t('main', 'Save'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>