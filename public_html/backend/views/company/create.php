<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\ar\Company */

$this->title = Yii::t('main', 'Create company');
$this->params['breadcrumbs'][] = ['label' => Yii::t('main', 'Companies'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="company-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        	'model' => $model,
    		'companyData' => $companyData,
    		'addressData' => $addressData,
    		'addressPostalData' => $addressPostalData,
    ]) ?>

</div>
