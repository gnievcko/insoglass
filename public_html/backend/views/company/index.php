<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
use common\helpers\StringHelper;

/* @var $this yii\web\View */
/* @var $searchModel common\models\ars\CompanySearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

if(empty($withoutDecorations)) {
	if(!empty($searchModel)) {
		$this->title = Yii::t('main', 'Companies');
		$this->params['breadcrumbs'][] = $this->title;
		
		$this->registerJs('
			$(".filter-button").click(function(e){
				e.preventDefault();
				$(".search-form").toggle();
			});
		');
	}
}
?>
<div class="company-index">

	<?php if(empty($withoutDecorations)) { ?>
    	<h1><?= Html::encode($this->title) ?></h1>
	<?php } else { ?>
    	<h1><?= Html::encode($this->title) ?></h1>
	<?php } ?>

	<?php if(!empty($searchModel)) { ?>
		<div class="search-form" style="display:none">
			<?php echo $this->render('_search', ['model' => $searchModel]); ?>
		</div>
	<?php } ?>

    <p>
    	<?php if(empty($withoutCreating)) { ?>
			<?= Html::a(Yii::t('main', 'Create client'), ['create'], ['class' => 'btn btn-success']) ?>
		<?php } ?>
		<?php if(!empty($searchModel)) {
			echo Html::button(Yii::t('main', 'Filters'), ['class' => 'btn btn-primary filter-button']);
		} ?>
    </p>

	<?php Pjax::begin(); ?>
    	<?= GridView::widget([
        		'dataProvider' => $dataProvider,
        		'formatter' => ['class' => 'yii\i18n\Formatter', 'nullDisplay' => ''],
        		'filterModel' => !empty($searchModel) ? $searchModel : null,
				'columns' => [
						[
								'attribute' => 'id',
								'label' => Yii::t('main', 'ID'),
								'contentOptions' => ['class' => 'column-id'],
						],
						[
								'attribute' => 'name',
								'label' => Yii::t('main', 'Name'),
						],
						[
								'attribute' => 'email',
								'format' => 'email',
								'label' => Yii::t('main', 'E-mail address'),
						],
						[
								'attribute' => 'phone1',
								'label' => Yii::t('main', 'Phone'),
						],
						[
								'attribute' => 'is_active',
								'filter' => [0 => Yii::t('main', 'No'), 1 => Yii::t('main', 'Yes')],
								'label' => Yii::t('main', 'Is active'),
								'value' => function($model) { return StringHelper::boolTranslation($model['is_active'], false); },
						],
						[
								'attribute' => 'date_creation',
								'label' => Yii::t('main', 'Creation date'),
						],
            			[
            					'class' => 'backend\components\ActionColumn',
            					'deleteConfirmMessage' => Yii::t('main', 'Are you sure you want to delete this item?'),
            					'visibleButtons' => ['delete' => empty($withoutDeleting), 'update' => empty($withoutUpdating)],
								'contentOptions' => ['class' => 'action-column'],
            			],
        		],
    	]); ?>
	<?php Pjax::end(); ?>

</div>
