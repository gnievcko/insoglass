<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use common\helpers\StringHelper;

/* @var $this yii\web\View */
/* @var $model common\models\ar\Company */

$this->title = Yii::t('main', 'Client') . ' - ' . $model->name;
$this->params['breadcrumbs'][] = ['label' => Yii::t('main', 'Clients'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="company-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
    	<?php if(empty($withoutUpdating)) { ?>
    		<?= Html::a(Yii::t('main', 'Update'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
    	<?php } ?>
        <?php if(empty($withoutDeleting)) { ?>
	        <?= Html::a(Yii::t('main', 'Delete'), ['delete', 'id' => $model->id], [
		            'class' => 'btn btn-danger',
		            'data' => [
			                'confirm' => Yii::t('main', 'Are you sure you want to delete this item?'),
			                'method' => 'post',
		            ],
	        ]) ?>
		<?php } ?>
    </p>

    <?= DetailView::widget([
	        'model' => $model,
    		'formatter' => ['class' => 'yii\i18n\Formatter', 'nullDisplay' => ''],
	        'attributes' => [
					'id',
					'name',
					'email:email',
					'phone1',
					'phone2',
	        		'fax1',
	        		'vat_identification_number',
	        		'taxpayer_identification_number',
	        		'krs_number',
	        		[
			        		'attribute' => 'parentCompanyName',
			        		'format' => 'raw',
			        		'value' => Html::a($model->parentCompanyName, ['/company/view', 'id' => $model->parent_company_id]),
	        		],
	        		[
	        				'attribute' => 'addressName',
	        				'format' => 'raw',
	        				'value' => Html::a($model->addressName, ['/address/view', 'id' => $model->address_id]),
    				],
	        		[
			        		'attribute' => 'addressPostalName',
			        		'format' => 'raw',
			        		'value' => Html::a($model->addressPostalName, ['/address/view', 'id' => $model->address_postal_id]),
	        		],
	        		'note:ntext',
	        		[
	        				'attribute' => 'languageName',
	        				'format' => 'raw',
	        				'value' => Html::a($model->languageName, ['/language/view', 'id' => $model->language_id]),
	        		],
					[
	        				'attribute' => 'currencyName',
	        				'format' => 'raw',
	        				'value' => Html::a($model->currencyName, ['/currency/view', 'id' => $model->currency_id]),
	        		],
	        		[
			        		'attribute' => 'is_artificial',
			        		'value' => StringHelper::boolTranslation($model->is_artificial)
	        		],
	        		[
			        		'attribute' => 'is_active',
			        		'value' => StringHelper::boolTranslation($model->is_active)
	        		],
					'date_creation',
					'date_modification',
        	],
    ]) ?>

</div>
