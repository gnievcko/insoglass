<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use common\models\ar\Language;
use common\models\ar\Currency;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $model common\models\ars\UserSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<?php
$this->registerJs('
	$(".reset-button").click(function(e){
		e.preventDefault();
		$(".form-control").val("");
	});
');
?>

<div class="user-search">

    <?php $form = ActiveForm::begin([
	        'action' => ['index'],
	        'method' => 'get',
    ]); ?>

    <div class="row">
        <div class="col-md-4">
            <?= $form->field($model, 'id') ?>
            <?= $form->field($model, 'email') ?>
            <?= $form->field($model, 'first_name') ?>
            <?= $form->field($model, 'last_name') ?>
        </div>

        <div class="col-md-4">
            <?= $form->field($model, 'phone') ?>
            <?= $form->field($model, 'language_id')->dropDownList(ArrayHelper::map(
                    Language::find()->where('is_active = 1')->all(), 
                    'id', 
                    'name'),
                ['prompt' => '-'])->label(Yii::t('main', 'Language')) ?>
            <?= $form->field($model, 'currency_id')->dropDownList(ArrayHelper::map(
                    Currency::find()->all(), 
                    'id', 
                    'symbol'),
                ['prompt' => '-'])->label(Yii::t('main', 'Currency')) ?>
            <?= $form->field($model, 'is_active')->dropDownList(['' => '-', 0 => \Yii::t('main', 'No'), 1 => \Yii::t('main', 'Yes')]) ?>
        </div>
        <div class="col-md-4">
            <?= $form->field($model, 'date_creation') ?>
            <?= $form->field($model, 'date_modification') ?>
            <?= $form->field($model, 'note') ?>
        </div>
    </div>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('main', 'Search'), ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton(Yii::t('main', 'Reset'), ['class' => 'btn btn-default reset-button']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
