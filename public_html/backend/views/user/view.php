<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use common\helpers\StringHelper;
use common\helpers\Utility;
use common\models\ar\User;

/* @var $this yii\web\View */
/* @var $model common\models\ar\User */

$this->title = Yii::t('main', 'Employee') . ' #' . $model->id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('main', 'Employees'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="user-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
    	<?php if(empty($withoutUpdating)) { ?>
    		<?= Html::a(Yii::t('main', 'Update'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
    	<?php } ?>
        <?php if(empty($withoutDeleting) && $model->id !== 1) { ?>
	        <?= Html::a(Yii::t('main', 'Delete'), ['delete', 'id' => $model->id], [
		            'class' => 'btn btn-danger',
		            'data' => [
			                'confirm' => Yii::t('main', 'Are you sure you want to delete this item?'),
			                'method' => 'post',
		            ],
	        ]) ?>
		<?php } ?>
    </p>

    <?= DetailView::widget([
	        'model' => $model,
            'formatter' => ['class' => 'yii\i18n\Formatter', 'nullDisplay' => ''],
	        'attributes' => [
					'id',
					'email:email',
                    [
                        'label' => \Yii::t('main', 'Full name'),
                        'value' => ($model->first_name === null || $model->last_name === null) ? null : Html::encode($model->getFullName()),
                    ],
					'phone1',
					'phone2',
	        		'phone3',
	        		'pesel',
	        		'id_number',
	        		[
	        				'attribute' => 'addressName',
	        				'format' => 'raw',
	        				'value' => Html::a($model->addressName, ['/address/view', 'id' => $model->address_id]),
	        		],
                    [
                        'attribute' => 'url_photo',
                        'label' => \Yii::t('main', 'Photo'),
                        'value' => Html::img(\common\helpers\UploadHelper::getNormalPath($userModel->url_photo, 'user_photo'), ['style' => 'max-width: 300px']),
                        'format' => 'raw',
                    ],
					'note',
	        		[
	        				'label' => Yii::t('main', 'Default language'),
	        				'format' => 'raw',
	        				'value' => $model->language_id ? Html::a($model->language->name, ['/language/view', 'id' => $model->language_id]) : null,
	        		],
	        		[
	        				'label' => Yii::t('main', 'Default currency'),
	        				'format' => 'raw',
	        				'value' => $model->currency_id ? Html::a($model->currency->symbol, ['/currency/view', 'id' => $model->currency_id]) : null,
	        		],
					[
							'label' => Yii::t('main', 'Is active'),
							'value' => StringHelper::boolTranslation($model->is_active)
					],
	        		[
	        				'label' => Yii::t('main', 'Last login'),
	        				'format' => 'raw',
	        				'value' => $model->lastActionTime ? Html::a($model->lastActionTime, ['/user-action/index', 'user_id' => $model->id]) : null,
	        		],
					'date_creation',
					'date_modification',
	        		'date_employment',
        	],
    ]) ?>

</div>

<?php
	echo Yii::$app->controller->renderPartial('/user-role/index', [
			'dataProvider' => $userRoleDataProvider,
			'userId' => $model->id,
			'withoutDecorations' => true,
			'withoutUpdating' => true,
	]);
?>

<?php
	$roles = $model->getRoles()->all();
	foreach ($roles as $role) {
		if (in_array($role->symbol, [Utility::ROLE_SALESMAN, Utility::ROLE_MAIN_SALESMAN])) {
			echo Yii::$app->controller->renderPartial('/user-company-salesman/index', [
					'dataProvider' => $userCompanySalesmanDataProvider,
					'userId' => $model->id,
					'withoutDecorations' => true,
					'hideUser' => true,
					'withoutUpdating' => true,
			]);
		break;
		}					
	} 
		
?>

<?php
	$roles = $model->getRoles()->all();
	foreach ($roles as $role) {
		if (in_array($role->symbol, [Utility::ROLE_STOREKEEPER, Utility::ROLE_MAIN_STOREKEEPER])) {
			echo Yii::$app->controller->renderPartial('/user-warehouse/index', [
					'dataProvider' => $userWarehouseDataProvider,
					'userId' => $model->id,
					'withoutDecorations' => true,
					'withoutUpdating' => true,
			]);
			break;
		}
	}
?>

