<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use common\models\ar\Language;
use common\models\ar\Currency;
use yii\helpers\ArrayHelper;
use kartik\widgets\FileInput;
use yii\web\View;
use kartik\datecontrol\DateControl;
use kartik\widgets\Select2;
use yii\helpers\Url;
use yii\web\JsExpression;

/* @var $this yii\web\View */
/* @var $model common\models\ar\User */
/* @var $form yii\widgets\ActiveForm */

?>

<div class="user-form">

    <?php $form = ActiveForm::begin([
    		'id' => 'user-form-ajax',
    		'enableAjaxValidation' => true,
            'options' => ['enctype' => 'multipart/form-data'],
    ]); ?>

	<?= $form->field($model, 'email')->textInput(['maxlength' => true]) ?>

    <?php 
        $passwordField = $form->field($model, 'password')->passwordInput(['maxlength' => true]);
        if(!$model->isNewRecord) {
            $passwordField->hint(Yii::t('main', 'If password would not be changed, please leave it unmodified.'));
        }
        echo $passwordField;
    ?>

	<?= $form->field($model, 'first_name')->textInput(['maxlength' => true]) ?>

	<?= $form->field($model, 'last_name')->textInput(['maxlength' => true]) ?>

	<?= $form->field($model, 'phone1')->textInput(['maxlength' => true]) ?>

	<?= $form->field($model, 'phone2')->textInput(['maxlength' => true]) ?>
	
	<?= $form->field($model, 'phone3')->textInput(['maxlength' => true]) ?>
	
	<?= $form->field($model, 'pesel')->textInput(['maxlength' => true]) ?>
		
	<?= $form->field($model, 'id_number')->textInput(['maxlength' => true]) ?>
	
	<div class="row">
	<div class="col-sm-9">
		<?php
			echo $form->field($model, 'address_id')
					->label(Yii::t('main', 'Address'))
					->widget(Select2::classname(), [
							'language' => Yii::$app->language,
							'options' => ['placeholder' => Yii::t('main', 'Select an address...')],
							'data' => $addressData,
							'pluginOptions' => [
									'allowClear' => true,
									'minimumInputLength' => 1,
									'ajax' => [
											'url' => Url::to(['site/find-address']),
											'dataType' => 'json',
											'delay' => 250,
											'data' => new JsExpression('function(params) { return {term:params.term, page:params.page, size: 20}; }'),
											'processResults' => new JsExpression('function (data, params) {
												params.page = params.page || 1;

												return {
													results: data.items,
													pagination: {
														more: data.more
													}
												};
											}'),
											'cache' => true
									],
									'templateResult' => new JsExpression('function(item) { if(item.loading) return item.text; return item.name; }'),
									'templateSelection' => new JsExpression('function (item) { return item.name || item.text; }'),
							]
			]);
		?>
	</div>
	<div class="col-sm-3 link-button-container">
		<?= Html::a(Yii::t('main', 'Create address'), ['/address/create'], ['class' => 'btn btn-primary filter-button create-link-button', 'target' => '_blank']);?>
	</div>
</div>
	
    <?php if($model->url_photo): ?>
        <div id="photo-preview-container" class="form-group">
            <label><?= Yii::t('main', 'Current photo') ?></label>
            <div class="file-preview">
                <div id="photo-preview-container" class="form-group">
                    <?= Html::img(\common\helpers\UploadHelper::getNormalPath($userModel->url_photo, 'user_photo'), 
                                  ['style' => 'max-width: 300px'], 
                                  ['class' => 'file-preview-frame', 'style' => 'display: block; height: auto; float: none;']) 
                    ?>
                    <button type="button" id="delete-photo-button" class="btn btn-danger"><?= Yii::t('main', 'Delete') ?></button>
                </div> 
            </div>
        </div>
        <input id="delete-photo-field" type="hidden" name="delete-photo" value="0" />
        <?php
            $this->registerJs('
                $("#delete-photo-button").on("click", function() {
                    $("#photo-preview-container").remove();
                    $("#delete-photo-field")[0].value = 1;
                })
            ', View::POS_END);
        ?>
    <?php endif; ?>
    <?php
        echo $form->field($model, 'file_photo')->widget(FileInput::classname(), [
                        'options' => [
                                'multiple' => false,
                        ],
                        'pluginOptions' => [
                                'language' => Yii::$app->language,
                                'showCaption' => false,
                                'showUpload' => false,
                                'uploadAsync' => false,
                                'resizeImageQuality' => 1.00, //not required
                                'allowedFileTypes' => ['image'],
                                'allowedFileExtensions' => ['jpg', 'png', 'jpeg'],
                                'allowedPreviewFileTypes' => ['image'],
                                'allowedPreviewMimeTypes' => ['image/jpeg', 'image/png'],
                                'previewFileType' => 'image',
                                'previewSettings' => ['image' => ['width' => '300px', 'height' => 'auto']]
                        ],
        ]);
    ?>

	<?= $form->field($model, 'note')->textArea(['maxlength' => true]) ?>

	<?= $form->field($model, 'language_id')->dropDownList(ArrayHelper::map(
			Language::find()->where('is_active=1')->all(), 
			'id', 
    		'name'
    ), ['prompt' => '-'])->label(\Yii::t('main', 'Default language')) ?>

	<?= $form->field($model, 'currency_id')->dropDownList(ArrayHelper::map(
			Currency::find()->all(), 
			'id', 
    		'symbol'
    ), ['prompt' => '-'])->label(\Yii::t('main', 'Default currency')) ?>

	<?= $form->field($model, 'is_active')->checkBox() ?>

    <?php if(!$model->isNewRecord): ?> 
        <?= $form->field($model, 'lastActionTime')->textInput(['disabled' => true]) ?>
        <?= $form->field($model, 'date_creation')->textInput(['disabled' => true]) ?>
        <?= $form->field($model, 'date_modification')->textInput(['disabled' => true]) ?>
    <?php endif; ?>
    

    <div class="form-group">
        <?= $form->field($model, 'date_employment')->widget(DateControl::classname(), [
                'language' => \Yii::$app->language,
                'type' => DateControl::FORMAT_DATE,
                'ajaxConversion' => false,
				'saveFormat' => 'php:Y-m-d H:i:s',
        		'displayFormat' => 'php:Y-m-d',
                'options' => [
                		'id' => 'dateReminderId',
                        'pluginOptions' => [
                        		'autoclose' => true,
                        ]
         		]
         ]);
        ?>
    </div>


    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('main', 'Create') : Yii::t('main', 'Save'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
