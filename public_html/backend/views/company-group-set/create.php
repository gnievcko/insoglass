<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\ar\CompanyGroupSet */

$this->title = Yii::t('main', 'Create assignment of client to group');
$this->params['breadcrumbs'][] = ['label' => Yii::t('main', 'Assignment of clients to groups'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="company-group-set-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        	'model' => $model,
    		'companyGroupId' => $companyGroupId,
    ]) ?>

</div>
