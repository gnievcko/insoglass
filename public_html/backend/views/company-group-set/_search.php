<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use common\models\ar\InformationPage;
use yii\helpers\ArrayHelper;
use common\models\ar\Company;
use common\models\ar\CompanyGroup;

/* @var $this yii\web\View */
/* @var $model common\models\ars\CompanyGroupSetSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<?php
$this->registerJs('
	$(".reset-button").click(function(e){
		e.preventDefault();
		$(".form-control").val("");
	});
');
?>

<div class="company-group-set-search container">

    <?php $form = ActiveForm::begin([
	        'action' => ['index'],
	        'method' => 'get',
    ]); ?>
    
    <div class="col-md-6">
    		<?= $form->field($model, 'company_id')->dropDownList(ArrayHelper::map(	
  					Company::find()->orderBy('name')->all(), 
  					'id', 
  					'name'
 			), ['prompt' => '-'])->label(Yii::t('main', 'Client')) ?>
    </div>
    
        <div class="col-md-6">
    		<?= $form->field($model, 'company_group_id')->dropDownList(ArrayHelper::map(	
  					CompanyGroup::find()->orderBy('symbol')->all(), 
  					'id', 
  					'symbol'
 			), ['prompt' => '-'])->label(Yii::t('main', 'Client group')) ?>
    </div>

    <div class="form-group col-md-12">
        <?= Html::submitButton(Yii::t('main', 'Search'), ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton(Yii::t('main', 'Reset'), ['class' => 'btn btn-default reset-button']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
