<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\ar\CompanyGroupSet */

$this->title = Yii::t('main', 'Update company group set') . ' #' . $model->company_id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('main', 'Assignment of clients to groups'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => Yii::t('main', 'Assignment of client to group') . ' - ' . $model->company_id, 'url' => ['view', 'company_id' => $model->company_id, 'company_group_id' => $model->company_group_id]];
$this->params['breadcrumbs'][] = Yii::t('main', 'Update');
?>
<div class="company-group-set-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        	'model' => $model,
    ]) ?>

</div>
