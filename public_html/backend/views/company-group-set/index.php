<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $searchModel common\models\ars\CompanyGroupSetSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

if(empty($withoutDecorations)) {
	if(!empty($searchModel)) {
		$this->title = Yii::t('main', 'Assignment of clients to groups');
		$this->params['breadcrumbs'][] = $this->title;
		
		$this->registerJs('
			$(".filter-button").click(function(e){
				e.preventDefault();
				$(".search-form").toggle();
			});
		');
	}
}
?>
<div class="company-group-set-index">

	<?php if(empty($withoutDecorations)) { ?>
    	<h1><?= Html::encode($this->title) ?></h1>
	<?php } 
		else { ?>
    	<h2><?= Yii::t('main', 'Client') ?></h2>
	<?php } ?>

	<?php if(!empty($searchModel)) { ?>
		<div class="search-form" style="display:none">
			<?php  echo $this->render('_search', ['model' => $searchModel]); ?>
		</div>
	<?php } ?>

	
		<p>
    	<?php if(empty($withoutCreating)) { ?>
    			<?php
    			echo Html::a(
    				Yii::t('main', 'Create assignment of client to group'),
    				['company-group-set/create', 'companyGroupId' => !empty($companyGroupId) ? $companyGroupId : null],
    				['class' => 'btn btn-success']
    			);
    					
    			if(!empty($companyGroupId)){
    				echo ' ' .Html::a(Yii::t('main', 'List of all assignments of clients to groups'), ['/company-group-set/index'], ['class' => 'btn btn-info']);
    			}
    		?>
		<?php } ?>
    </p>

	<?php Pjax::begin(); ?>
    	<?= GridView::widget([
        		'dataProvider' => $dataProvider,
        		'formatter' => ['class' => 'yii\i18n\Formatter', 'nullDisplay' => ''],
        		'filterModel' => !empty($searchModel) ? $searchModel : null,
				'columns' => [
						[
								'attribute' => 'companyName',
								'label' => Yii::t('main', 'Client'),
    					],
						[
								'attribute' => 'companyGroupSymbol',
								'label' => Yii::t('main', 'Client group'),
								'visible' => !empty($searchModel),
    					],
            			[
            					'class' => 'backend\components\ActionColumn',
            					'controller' => 'company-group-set',
            					'deleteConfirmMessage' => Yii::t('main', 'Are you sure you want to delete this item?'),
            					'visibleButtons' => ['delete' => empty($withoutDeleting), 'update' => empty($withoutUpdating)],
								'contentOptions' => ['class' => 'action-column'],
            			],
        		],
    	]); ?>
	<?php Pjax::end(); ?>

</div>
