<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use common\models\ar\CompanyGroup;
use common\models\ar\Company;
use common\models\ar\InformationPage;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $model common\models\ar\CompanyGroupSet */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="company-group-set-form">

    <?php $form = ActiveForm::begin([
    		'id' => 'company-group-set-form-ajax',
    		'enableAjaxValidation' => true,
    ]); ?>

    <?= $form->field($model, 'company_id', ['enableAjaxValidation' => true])->dropDownList(ArrayHelper::map(
        (empty($companyId) ? Company::find()->orderBy('name')->all() : Company::find()->where(['id' => $companyId])->all()), 
           'id', 
           'name'
     ))->label(Yii::t('main', 'Client')) ?>

	<?= $form->field($model, 'company_group_id', ['enableAjaxValidation' => true])->dropDownList(ArrayHelper::map(
        (empty($companyGroupId) ? CompanyGroup::find()->orderBy('symbol')->all() : CompanyGroup::find()->where(['id' => $companyGroupId])->all()), 
           'id', 
           'symbol'
     ))->label(Yii::t('main', 'Client group')) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('main', 'Create') : Yii::t('main', 'Save'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>