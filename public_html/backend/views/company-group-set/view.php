<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use backend\helpers\Utility;

/* @var $this yii\web\View */
/* @var $model common\models\ar\CompanyGroupSet */

$this->title = Yii::t('main', 'Assignment of client to group') . ' - ' . $model->companyName;
$this->params['breadcrumbs'][] = ['label' => Yii::t('main', 'Assignment of clients to groups'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="company-group-set-view">


    <p>
    	<?php if(empty($withoutUpdating)) { ?>
    		<?= Html::a(Yii::t('main', 'Update'), ['update', 'company_id' => $model->company_id, 'company_group_id' => $model->company_group_id], ['class' => 'btn btn-primary']) ?>
    	<?php } ?>
        <?php if(empty($withoutDeleting)) { ?>
	        <?= Html::a(Yii::t('main', 'Delete'), ['delete', 'company_id' => $model->company_id, 'company_group_id' => $model->company_group_id], [
		            'class' => 'btn btn-danger',
		            'data' => [
			                'confirm' => Yii::t('main', 'Are you sure you want to delete this item?'),
			                'method' => 'post',
		            ],
	        ]) ?>
		<?php } ?>
    </p>

    <?= DetailView::widget([
	        'model' => $model,
	        'formatter' => ['class' => 'yii\i18n\Formatter', 'nullDisplay' => ''],
	        'attributes' => [
	        		[
	        				'label' => Yii::t('main', 'Client'),
	        				'format' => 'raw',
	        				'value' => Html::a($model->companyName, ['/company/view', 'id' => $model->company_id]),
	        		],
	        		[
	        				'label' => Yii::t('main', 'Client group'),
	        				'format' => 'raw',
	        				'value' => Html::a($model->companyGroupSymbol, ['/company-group/view', 'id' => $model->company_group_id]),
	        		],
        	],
    ]) ?>

</div>
