<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use dosamigos\ckeditor\CKEditor;
use backend\helpers\Utility;
use common\models\ar\Language;
use yii\helpers\ArrayHelper;
use common\models\ar\EmailType;

/* @var $this yii\web\View */
/* @var $model common\models\ar\EmailTemplate */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="email-template-form">

    <?php $form = ActiveForm::begin([
    		'id' => 'email-template-form-ajax',
    		'enableAjaxValidation' => true,
    ]); ?>

	<?= $form->field($model, 'email_type_id')->dropDownList(ArrayHelper::map(
			empty($emailTypeId) ? EmailType::find()->orderBy('symbol')->all() : EmailType::find()->where(['id' => $emailTypeId])->all(), 'id', 'symbol'	
	))->label(Yii::t('main', 'E-mail type')) ?>

	<?= $form->field($model, 'language_id')->dropDownList(ArrayHelper::map(
			Language::find()->where('is_active = 1')->all(), 'id', 'name'
	))->label(Yii::t('main', 'Language')) ?>

	<?= $form->field($model, 'subject')->textInput(['maxlength' => true]) ?>

	<?= $form->field($model, 'content_html')->widget(CKEditor::className(), Utility::getCKEditorParams()) ?>

	<?= $form->field($model, 'content_text')->textarea(['rows' => 6]) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('main', 'Create') : Yii::t('main', 'Save'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>