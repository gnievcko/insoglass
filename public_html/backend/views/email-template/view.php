<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use yii\helpers\Url;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\ar\EmailTemplate */

$this->title = Yii::t('main', 'E-mail template') . ' - ' . $model->emailTypeName;
$this->params['breadcrumbs'][] = ['label' => Yii::t('main', 'E-mail templates'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="email-template-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
    	<?php if(empty($withoutUpdating)) { ?>
    		<?= Html::a(Yii::t('main', 'Update'), ['update', 'email_type_id' => $model->email_type_id, 'language_id' => $model->language_id], ['class' => 'btn btn-primary']) ?>
    	<?php } ?>
        <?php if(empty($withoutDeleting)) { ?>
	        <?= Html::a(Yii::t('main', 'Delete'), ['delete', 'email_type_id' => $model->email_type_id, 'language_id' => $model->language_id], [
		            'class' => 'btn btn-danger',
		            'data' => [
			                'confirm' => Yii::t('main', 'Are you sure you want to delete this item?'),
			                'method' => 'post',
		            ],
	        ]) ?>
		<?php } ?>
    </p>

    <?= DetailView::widget([
	        'model' => $model,
    		'formatter' => ['class' => 'yii\i18n\Formatter', 'nullDisplay' => ''],
	        'attributes' => [
					[
							'attribute' => 'emailTypeName',
							'format' => 'raw',
							'value' => Html::a(
									$model->emailTypeName,
									['/email-type/view', 'id' => $model->email_type_id]
							)
					],
					[
							'attribute' => 'languageName',
							'format' => 'raw',
							'value' => Html::a(
									$model->languageName,
									['/language/view', 'id' => $model->language_id]
							)
					],
					'subject',
					[
							'attribute' => 'content_html',
							'format' => 'raw',
							'value' => '<iframe id="email-template-iframe" width="100%" src="' .
									Url::to([
											'/email-template/template',
											'email_type_id' => $model->email_type_id,
											'language_id' => $model->language_id,
									]) . '" onload="adjustIframeHeight(\'email-template-iframe\')"></iframe>'
					],
					'content_text:ntext',
        	],
    ]) ?>

	<?php $form = ActiveForm::begin([
    		'id' => 'email-template-test-form-ajax',
    		'enableAjaxValidation' => false,
    ]); ?>
	
	<div class="panel panel-default">
		<div class="panel-body">
			<fieldset>
		    	<legend><?= Yii::t('main', 'Testing message sending with this template') ?></legend>
			    <?= $form->field($emailTemplateTestForm, 'email', ['enableAjaxValidation' => false])->input('text', []); ?>
			    <div class="form-group">
			    	<?= Html::submitButton(Yii::t('main', 'Send test message'), ['id' => 'test-mail', 'class' => 'btn btn-info']) ?>
			    </div>
		    </fieldset>
	    </div>
    </div>
    
    <?php ActiveForm::end(); ?>

</div>
