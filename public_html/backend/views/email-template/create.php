<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\ar\EmailTemplate */

$this->title = Yii::t('main', 'Create e-mail template');
$this->params['breadcrumbs'][] = ['label' => Yii::t('main', 'E-mail templates'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="email-template-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        	'model' => $model,
    		'emailTypeId' => $emailTypeId,
    ]) ?>

</div>
