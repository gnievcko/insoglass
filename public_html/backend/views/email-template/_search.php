<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use common\models\ar\Language;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $model common\models\ars\EmailTemplateSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<?php
$this->registerJs('
	$(".reset-button").click(function(e){
		e.preventDefault();
		$(".form-control").val("");
	});
');
?>

<div class="email-template-search container">

    <?php $form = ActiveForm::begin([
	        'action' => ['index'],
	        'method' => 'get',
    ]); ?>

	<div class="col-md-6">
		<?= $form->field($model, 'emailTypeName') ?>
	
		<?= $form->field($model, 'language_id')->dropDownList(ArrayHelper::map(
				Language::find()->where('is_active = 1')->all(), 'id', 'name'
		), ['prompt' => '-'])->label(Yii::t('main', 'Language')) ?>
	
		<?= $form->field($model, 'subject') ?>
	</div>
	<div class="col-md-6">
		<?= $form->field($model, 'content_html') ?>
	
		<?= $form->field($model, 'content_text') ?>
	</div>
    <div class="form-group col-md-12">
        <?= Html::submitButton(Yii::t('main', 'Search'), ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton(Yii::t('main', 'Reset'), ['class' => 'btn btn-default reset-button']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
