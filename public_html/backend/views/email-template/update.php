<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\ar\EmailTemplate */

$this->title = Yii::t('main', 'Update e-mail template') . ' - ' . $model->emailTypeName;
$this->params['breadcrumbs'][] = ['label' => Yii::t('main', 'E-mail templates'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => Yii::t('main', 'E-mail template') . ' - ' . $model->emailTypeName, 'url' => ['view', 'email_type_id' => $model->email_type_id, 'language_id' => $model->language_id]];
$this->params['breadcrumbs'][] = Yii::t('main', 'Update');
?>
<div class="email-template-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        	'model' => $model,
    ]) ?>

</div>
