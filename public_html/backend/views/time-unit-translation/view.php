<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\ar\TimeUnitTranslation */

$this->title = Yii::t('main', 'Time unit translation') . ' - ' . $model->timeUnitSymbol;
$this->params['breadcrumbs'][] = ['label' => Yii::t('main', 'Time unit translations'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="time-unit-translation-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
    	<?php if(empty($withoutUpdating)) { ?>
    		<?= Html::a(Yii::t('main', 'Update'), ['update', 'time_unit_id' => $model->time_unit_id, 'language_id' => $model->language_id, 'value' => $model->value], ['class' => 'btn btn-primary']) ?>
    	<?php } ?>
        <?php if(empty($withoutDeleting)) { ?>
	        <?= Html::a(Yii::t('main', 'Delete'), ['delete', 'time_unit_id' => $model->time_unit_id, 'language_id' => $model->language_id, 'value' => $model->value], [
		            'class' => 'btn btn-danger',
		            'data' => [
			                'confirm' => Yii::t('main', 'Are you sure you want to delete this item?'),
			                'method' => 'post',
		            ],
	        ]) ?>
		<?php } ?>
    </p>

    <?= DetailView::widget([
	        'model' => $model,
	        'formatter' => ['class' => 'yii\i18n\Formatter', 'nullDisplay' => ''],
	        'attributes' => [
					'timeUnitSymbol',
					'languageName',
					'value',
					'name',
        	],
    ]) ?>

</div>
