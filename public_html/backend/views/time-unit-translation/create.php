<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\ar\TimeUnitTranslation */

$this->title = Yii::t('main', 'Create time unit translation');
$this->params['breadcrumbs'][] = ['label' => Yii::t('main', 'Time unit translations'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="time-unit-translation-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        	'model' => $model,
    		'timeUnitId' => $timeUnitId,
    ]) ?>

</div>
