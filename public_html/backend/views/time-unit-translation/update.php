<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\ar\TimeUnitTranslation */

$this->title = Yii::t('main', 'Update time unit translation') . ' - ' . $model->timeUnitSymbol;
$this->params['breadcrumbs'][] = ['label' => Yii::t('main', 'Time unit translations'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => Yii::t('main', 'Time unit translation') . ' - ' . $model->timeUnitSymbol, 'url' => ['view', 'time_unit_id' => $model->time_unit_id, 'language_id' => $model->language_id, 'value' => $model->value]];
$this->params['breadcrumbs'][] = Yii::t('main', 'Update');
?>
<div class="time-unit-translation-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        	'model' => $model,
    ]) ?>

</div>
