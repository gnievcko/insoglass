<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use common\models\ar\TimeUnit;
use common\models\ar\Language;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $model common\models\ar\TimeUnitTranslation */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="time-unit-translation-form">

    <?php $form = ActiveForm::begin([
    		'id' => 'time-unit-translation-form-ajax',
    		'enableAjaxValidation' => true,
    ]); ?>

    <?= $form->field($model, 'time_unit_id', ['enableAjaxValidation' => true])->dropDownList(ArrayHelper::map(
         	(empty($timeUnitId) ? TimeUnit::find()->orderBy('symbol')->all() : TimeUnit::find()->where(['id' => $timeUnitId])->orderBy('symbol')->all()), 
             'id', 
             'symbol'
      ))->label(Yii::t('main', 'Time unit')) ?>
		
	<?= $form->field($model, 'language_id')->dropDownList(ArrayHelper::map(
			Language::find()->all(), 'id', 'name'
		))->label(Yii::t('main', 'Language')) ?>

	<?= $form->field($model, 'value')->textInput(['type' => 'number']) ?>

	<?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('main', 'Create') : Yii::t('main', 'Save'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>