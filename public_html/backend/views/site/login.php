<?php

/* @var $this yii\web\View */
/* @var $model \common\models\LoginForm */

use yii\helpers\Html;
use kartik\widgets\ActiveForm;

$this->title = Yii::t('web', 'Login') . ' - ' . Yii::$app->name;
$this->params['breadcrumbs'][] = Yii::t('web', 'Login');
?>
<div class="site-login">
    <h1><?= Yii::t('web', 'Login') ?></h1>

    <p><?= Yii::t('web', 'Please fill out the following fields to login') ?>:</p>

    <div class="row">
    	<div class="col-md-5">
            <?php $form = ActiveForm::begin(['id' => 'login-form', 'enableAjaxValidation' => true ]); ?>
            
				<?= $form->field($model, 'email')->textInput() ?>

                <?= $form->field($model, 'password', ['enableAjaxValidation' => false])->passwordInput() ?>

                <div class="form-group">
                    <?= Html::submitButton(Yii::t('web', 'Log in'), ['class' => 'btn btn-success', 'name' => 'login-button']) ?>
                </div>

            <?php ActiveForm::end(); ?>
            <br />
        </div>
    </div>
</div>
