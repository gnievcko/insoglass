<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\ar\Alert */

$this->title = Yii::t('main', 'Create alert');
$this->params['breadcrumbs'][] = ['label' => Yii::t('main', 'Alerts'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="alert-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        	'model' => $model,
        	'companyData' => $companyData,
        	'taskData' => $taskData,
        	'productData' => $productData,
        	'orderData' => $orderData,
        	'userData' => $userData,
    ]) ?>

</div>
