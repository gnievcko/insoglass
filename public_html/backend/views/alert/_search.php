<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\ars\AlertSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<?php
$this->registerJs('
	$(".reset-button").click(function(e){
		e.preventDefault();
		$(".form-control").val("");
	});
');
?>

<div class="alert-search">

    <?php $form = ActiveForm::begin([
	        'action' => ['index'],
	        'method' => 'get',
    ]); ?>

    <div class="row">
        <div class="col-md-4">
			<?= $form->field($model, 'id') ?>
			<?= $form->field($model, 'alert_type_id') ?>
			<?= $form->field($model, 'user_id') ?>
			<?= $form->field($model, 'message') ?>
		</div>
		<div class="col-md-4">
			<?= $form->field($model, 'is_active')->dropDownList(['' => '', 0 => \Yii::t('main', 'No'), 1 => \Yii::t('main', 'Yes')]) ?>
			<?= $form->field($model, 'date_creation') ?>
			<?= $form->field($model, 'order_id') ?>
			<?= $form->field($model, 'product_id') ?>
		</div>
		<div class="col-md-4">
			<?= $form->field($model, 'company_id') ?>
			<?= $form->field($model, 'task_id') ?>
			<?= $form->field($model, 'warehouse_id') ?>
		</div>
	</div>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('main', 'Search'), ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton(Yii::t('main', 'Reset'), ['class' => 'btn btn-default reset-button']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
