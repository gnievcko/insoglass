<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\ar\Alert */

$this->title = Yii::t('main', 'Update alert') . ' - ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('main', 'Alerts'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => Yii::t('main', 'Alert') . ' - ' . $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('main', 'Update');
?>
<div class="alert-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        	'model' => $model,
        	'companyData' => $companyData,
        	'taskData' => $taskData,
        	'productData' => $productData,
        	'orderData' => $orderData,
        	'userData' => $userData,
    ]) ?>

</div>
