<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\widgets\Select2;
use yii\helpers\Url;
use yii\web\JsExpression;
use yii\helpers\ArrayHelper;
use common\models\ar\AlertTypeTranslation;
use common\models\ar\Warehouse;
use common\models\ar\User;
use common\models\ar\Order;
use common\models\ar\Product;
use common\models\ar\Company;
use common\models\ar\Task;

/* @var $this yii\web\View */
/* @var $model common\models\ar\Alert */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="alert-form">

    <?php $form = ActiveForm::begin([
    		'id' => 'alert-form-ajax',
    		'enableAjaxValidation' => true,
    ]); ?>

	<?= $form->field($model, 'alert_type_id')->dropDownList(ArrayHelper::map(
		empty($alert_type_id) ? AlertTypeTranslation::find()->orderBy('alert_type_id')->all() : AlertTypeTranslation::find()->where(['id' => $alert_type_id])->all(), 'alert_type_id', 'name'))->label(Yii::t('main', 'Alert Type')) ?>

	<?php
		echo $form->field($model, 'user_id')
				->label(Yii::t('main', 'User'))
				->widget(Select2::classname(), [
						'language' => Yii::$app->language,
						'options' => ['placeholder' => Yii::t('main', 'Select user...')],
						'data' => $userData,
						'pluginOptions' => [
								'allowClear' => true,
								'minimumInputLength' => 1,
								'ajax' => [
										'url' => Url::to(['site/find-users']),
										'dataType' => 'json',
										'delay' => 250,
										'data' => new JsExpression('function(params) { return {term:params.term, page:params.page, size: 20, type: "' . User::USER_TYPE_EMPLOYEE . '", 
											onlyActives: 1}; 
                                            }'),
										'processResults' => new JsExpression('function (data, params) {
											params.page = params.page || 1;
						                    var users = $(data.items).map(function(index, user) {
						                    return {
						                    	id: user.id, name: user.first_name + ((user.first_name == null) ? "" : " ") + user.last_name + " " + user.email
						                    };
						                             });
                                             
													return {
														results: users,
														pagination: {
															more: data.more
														}
													};
										}'),
										'cache' => true
								],
								'templateResult' => new JsExpression('function(item) { if(item.loading) return item.text; return item.name; }'),
								'templateSelection' => new JsExpression('function (item) { return item.name || item.text; }'),
						]
		]);
	?>

	<?= $form->field($model, 'message')->textArea(['maxlength' => true]) ?>

	<?= $form->field($model, 'is_active')->checkbox() ?>

	<?= $form->field($model, 'date_creation')->textInput(['disabled' => true]) ?>

	<?php
		echo $form->field($model, 'order_id')
				->label(Yii::t('main', 'Order'))
				->widget(Select2::classname(), [
						'language' => Yii::$app->language,
						'options' => ['placeholder' => Yii::t('main', 'Select order...')],
						'data' => $orderData,
						'pluginOptions' => [
								'allowClear' => true,
								'minimumInputLength' => 1,
								'ajax' => [
										'url' => Url::to(['site/find-orders']),
										'dataType' => 'json',
										'delay' => 250,
										'data' => new JsExpression('function(params) { return {term:params.term, page:params.page, size: 20}; }'),
										'processResults' => new JsExpression('function (data, params) {
											params.page = params.page || 1;
						                    var orders = $(data.items).map(function(index, order) {
						                    return {
						                    	id: order.id, name: order.number + ((order.number == null) ? "" : " ") + order.title
						                    };
						                             });
                                             
													return {
														results: orders,
														pagination: {
															more: data.more
														}
													};
										}'),
										'cache' => true
								],
								'templateResult' => new JsExpression('function(item) { if(item.loading) return item.text; return item.name; }'),
								'templateSelection' => new JsExpression('function (item) { return item.name || item.text; }'),
						]
		]);
	?>

	<?php
		echo $form->field($model, 'product_id')
				->label(Yii::t('main', 'Product'))
				->widget(Select2::classname(), [
						'language' => Yii::$app->language,
						'options' => ['placeholder' => Yii::t('main', 'Select product...')],
						'data' => $productData,
						'pluginOptions' => [
								'allowClear' => true,
								'minimumInputLength' => 1,
								'ajax' => [
										'url' => Url::to(['site/find-products']),
										'dataType' => 'json',
										'delay' => 250,
										'data' => new JsExpression('function(params) { return {term:params.term, page:params.page, size: 20}; }'),
										'processResults' => new JsExpression('function (data, params) {
											params.page = params.page || 1;
						                    var products = $(data.items).map(function(index, product) {
						                    return {
						                    	id: product.id, name: product.symbol + ((product.symbol == null) ? "" : " ") + product.name
						                    };
						                             });
                                             
													return {
														results: products,
														pagination: {
															more: data.more
														}
													};
										}'),
										'cache' => true
								],
								'templateResult' => new JsExpression('function(item) { if(item.loading) return item.text; return item.name; }'),
								'templateSelection' => new JsExpression('function (item) { return item.name || item.text; }'),
						]
		]);
	?>

	<?php
		echo $form->field($model, 'company_id')
				->label(Yii::t('main', 'Company'))
				->widget(Select2::classname(), [
						'language' => Yii::$app->language,
						'options' => ['placeholder' => Yii::t('main', 'Select a company...')],
						'data' => $companyData,
						'pluginOptions' => [
								'allowClear' => true,
								'minimumInputLength' => 1,
								'ajax' => [
										'url' => Url::to(['site/find-company']),
										'dataType' => 'json',
										'delay' => 250,
										'data' => new JsExpression('function(params) { return {term:params.term, page:params.page, size: 20, excludedCompanyId: '.(!$model->isNewRecord ? $model->id : 0).'}; }'),
										'processResults' => new JsExpression('function (data, params) {
											params.page = params.page || 1;
						                    var company = $(data.items).map(function(index, company) {
						                    return {
						                    	id: company.id, name: company.name + ((company.name == null) ? "" : " ")
						                    };
						                             });
                                             
													return {
														results: company,
														pagination: {
															more: data.more
														}
													};
										}'),
										'cache' => true
								],
								'templateResult' => new JsExpression('function(item) { if(item.loading) return item.text; return item.name; }'),
								'templateSelection' => new JsExpression('function (item) { return item.name || item.text; }'),
						]
		]);
	?>

	<?php
		echo $form->field($model, 'task_id')
				->label(Yii::t('main', 'Task'))
				->widget(Select2::classname(), [
						'language' => Yii::$app->language,
						'options' => ['placeholder' => Yii::t('main', 'Select a task...')],
						'data' => $taskData,
						'pluginOptions' => [
								'allowClear' => true,
								'minimumInputLength' => 1,
								'ajax' => [
										'url' => Url::to(['site/find-task']),
										'dataType' => 'json',
										'delay' => 250,
										'data' => new JsExpression('function(params) { return {term:params.term, page:params.page, size: 20}; }'),
										'processResults' => new JsExpression('function (data, params) {
											params.page = params.page || 1;
						                    var tasks = $(data.items).map(function(index, task) {
						                    return {
						                    	id: task.id, name: task.title + ((task.title == null) ? "" : " ")
						                    };
						                             });
                                             
													return {
														results: tasks,
														pagination: {
															more: data.more
														}
													};
										}'),
										'cache' => true
								],
								'templateResult' => new JsExpression('function(item) { if(item.loading) return item.text; return item.name; }'),
								'templateSelection' => new JsExpression('function (item) { return item.name || item.text; }'),
						]
		]);
	?>

	<?= $form->field($model, 'warehouse_id')->dropDownList(ArrayHelper::map(
		empty($warehouse_id) ? Warehouse::find()->orderBy('id')->all() : Warehouse::find()->where(['id' => $id])->all(), 'id', 'name'))->label(Yii::t('main', 'Warehouse')) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('main', 'Create') : Yii::t('main', 'Save'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>