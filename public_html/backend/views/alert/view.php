<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use common\helpers\StringHelper;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $model common\models\ar\Alert */

$this->title = Yii::t('main', 'Alert') . ' - ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('main', 'Alerts'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="alert-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
    	<?php if(empty($withoutUpdating)) { ?>
    		<?= Html::a(Yii::t('main', 'Update'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
    	<?php } ?>
        <?php if(empty($withoutDeleting)) { ?>
	        <?= Html::a(Yii::t('main', 'Delete'), ['delete', 'id' => $model->id], [
		            'class' => 'btn btn-danger',
		            'data' => [
			                'confirm' => Yii::t('main', 'Are you sure you want to delete this item?'),
			                'method' => 'post',
		            ],
	        ]) ?>
		<?php } ?>
    </p>

    <?= DetailView::widget([
	        'model' => $model,
	        'formatter' => ['class' => 'yii\i18n\Formatter', 'nullDisplay' => ''],
	        'attributes' => [
					'id',
					[
	        				'attribute' => 'alert_type_id',
	        				'format' => 'raw',
							'value' => $model->alert_type_id ? Html::a($model->getAlertType()->one()->getAlertTypeTranslations()->one()->name, ['/alert-type/view', 'id' => $model->alert_type_id]) : null
	        		],
					[
	        				'attribute' => 'user_id',
       					    'format' => 'raw',
							'value' => $model->user_id ? Html::a($model->user->getFullName(), ['/user/view', 'id' => $model->user_id]) : null
	        		],
					'message',
					[
							'label' => Yii::t('main', 'Is active'),
							'value' => StringHelper::boolTranslation($model->is_active)
					],
					'date_creation',
					[
	        				'attribute' => 'order_id',
	        				'format' => 'raw',
	        				'value'=> $model->order_id ? Html::a($model->order->number, ['/order/view', 'id' => $model->order_id]) : null
	        		],
					[
	        				'attribute' => 'product_id',
	        				'format' => 'raw',
	        				'value'=> $model->product_id ? Html::a($model->product->getProductTranslations()->one()->name, ['/product/view', 'id' => $model->product_id]) : null
	        		],
					[
	        				'attribute' => 'company_id',
	        				'format' => 'raw',
	        				'value'=> $model->company_id ? Html::a($model->company->name, ['/company/view', 'id' => $model->company_id]) : null
	        		],
					[
	        				'attribute' => 'task_id',
	        				'format' => 'raw',
	        				'value'=> $model->task_id ? Html::a($model->task->title, ['/task/view', 'id' => $model->task_id]) : null
	        		],
					[
	        				'attribute' => 'warehouse_id',
	        				'format' => 'raw',
	        				'value'=> $model->warehouse_id ? Html::a($model->warehouse->name, ['/warehouse/view', 'id' => $model->warehouse_id]) : null
	        		],
        	],
    ]) ?>

</div>
