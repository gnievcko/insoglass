<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\ar\UserModificationHistory */

$this->title = Yii::t('main', 'Update user modification history') . ' #' . $model->id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('main', 'User modification histories'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => Yii::t('main', 'User modification history') . ' #' . $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('main', 'Update');
?>
<div class="user-modification-history-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        	'model' => $model,
    ]) ?>

</div>
