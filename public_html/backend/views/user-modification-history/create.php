<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\ar\UserModificationHistory */

$this->title = Yii::t('main', 'Create user modification history');
$this->params['breadcrumbs'][] = ['label' => Yii::t('main', 'User modification histories'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="user-modification-history-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        	'model' => $model,
    ]) ?>

</div>
