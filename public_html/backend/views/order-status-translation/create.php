<?php

use yii\helpers\Html;
use common\helpers\StringHelper;


/* @var $this yii\web\View */
/* @var $model common\models\ar\OrderStatusTranslation */

$this->title = Yii::t('main', 'Create translation');
$this->params['breadcrumbs'][] = ['label' => StringHelper::translateOrderToOffer('main', 'Order status translations'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="order-status-translation-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        	'model' => $model,
    		'orderStatusId' => $orderStatusId,
    ]) ?>

</div>
