<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use common\models\ar\OrderStatus;
use common\models\ar\Language;
use yii\helpers\ArrayHelper;
use common\helpers\StringHelper;

/* @var $this yii\web\View */
/* @var $model common\models\ar\OrderStatusTranslation */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="order-status-translation-form">

    <?php $form = ActiveForm::begin([
    		'id' => 'order-status-translation-form-ajax',
    		'enableAjaxValidation' => true,
    ]); ?>

	<?= $form->field($model, 'order_status_id')->dropDownList(ArrayHelper::map(
			empty($orderStatusId) ? OrderStatus::find()->all() : OrderStatus::find()->where(['id' => $orderStatusId])->all(), 'id', 'symbol'		
	))->label(StringHelper::translateOrderToOffer('main', 'Order status')) ?>

	<?= $form->field($model, 'language_id')->dropDownList(ArrayHelper::map(
	        Language::find()->all(), 'id', 'name'		
	))->label(Yii::t('main', 'Language')) ?>

	<?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

	<?= $form->field($model, 'name_client')->textInput(['maxlength' => true]) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('main', 'Create') : Yii::t('main', 'Save'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
