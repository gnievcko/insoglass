<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use common\helpers\StringHelper;

/* @var $this yii\web\View */
/* @var $model common\models\ar\OrderStatusTranslation */

$this->title = StringHelper::translateOrderToOffer('main', 'Order status translation') . ' - ' . $model->orderStatusSymbol;
$this->params['breadcrumbs'][] = ['label' => StringHelper::translateOrderToOffer('main', 'Order status translations'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="order-status-translation-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
    	<?php if(empty($withoutUpdating)) { ?>
    		<?= Html::a(Yii::t('main', 'Update'), ['update', 'order_status_id' => $model->order_status_id, 'language_id' => $model->language_id], ['class' => 'btn btn-primary']) ?>
    	<?php } ?>
        <?php if(empty($withoutDeleting)) { ?>
	        <?= Html::a(Yii::t('main', 'Delete'), ['delete', 'order_status_id' => $model->order_status_id, 'language_id' => $model->language_id], [
		            'class' => 'btn btn-danger',
		            'data' => [
			                'confirm' => Yii::t('main', 'Are you sure you want to delete this item?'),
			                'method' => 'post',
		            ],
	        ]) ?>
		<?php } ?>
    </p>

    <?= DetailView::widget([
	        'model' => $model,
	        'formatter' => ['class' => 'yii\i18n\Formatter', 'nullDisplay' => ''],
	        'attributes' => [
                    [
                        'attribute' => 'orderStatusSymbol',
                        'format' => 'raw',
                        'value' => Html::a($model->orderStatusSymbol, ['/order-status/view', 'id' => $model->order_status_id]),
                    ],
                    [
                        'attribute' => 'languageName',
                        'format' => 'raw',
                        'value' => Html::a($model->languageName, ['/language/view', 'id' => $model->language_id]),
                    ],
					'name',
					'name_client',
        	],
    ]) ?>

</div>
