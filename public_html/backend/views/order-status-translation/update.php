<?php

use yii\helpers\Html;
use common\helpers\StringHelper;

/* @var $this yii\web\View */
/* @var $model common\models\ar\OrderStatusTranslation */

$this->title = Yii::t('main', 'Update translation');
$this->params['breadcrumbs'][] = ['label' => StringHelper::translateOrderToOffer('main', 'Order status translations'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => StringHelper::translateOrderToOffer('main', 'Order status translation') . ' - ' . $model->orderStatusSymbol, 'url' => ['view', 'order_status_id' => $model->order_status_id, 'language_id' => $model->language_id]];
$this->params['breadcrumbs'][] = Yii::t('main', 'Update');
?>
<div class="order-status-translation-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        	'model' => $model,
    ]) ?>

</div>
