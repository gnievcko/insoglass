<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\ar\RoleTranslation */

$this->title = Yii::t('main', 'Update translation');
$this->params['breadcrumbs'][] = ['label' => Yii::t('main', 'Role translations'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => Yii::t('main', 'Role translation') . ' - ' . $model->name, 'url' => ['view', 'role_id' => $model->role_id, 'language_id' => $model->language_id]];
$this->params['breadcrumbs'][] = Yii::t('main', 'Update');
?>
<div class="role-translation-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        	'model' => $model,
    ]) ?>

</div>
