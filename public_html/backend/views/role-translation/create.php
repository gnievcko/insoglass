<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\ar\RoleTranslation */

$this->title = Yii::t('main', 'Create translation');
$this->params['breadcrumbs'][] = ['label' => Yii::t('main', 'Role translations'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="role-translation-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        	'model' => $model,
            'roleId' => $roleId,
    ]) ?>

</div>
