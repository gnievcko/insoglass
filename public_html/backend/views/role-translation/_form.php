<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use common\models\ar\Role;
use common\models\ar\Language;

/* @var $this yii\web\View */
/* @var $model common\models\ar\RoleTranslation */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="role-translation-form">

    <?php $form = ActiveForm::begin([
    		'id' => 'role-translation-form-ajax',
    		'enableAjaxValidation' => true,
    ]); ?>

	<?= $form->field($model, 'role_id', ['enableAjaxValidation' => true])->dropDownList(ArrayHelper::map(
			(empty($roleId) ? Role::find()->orderBy('symbol')->all() : Role::find()->where(['id' => $roleId])->all()), 
			'id', 
    		'symbol'
    ))->label(Yii::t('main', 'Role')) ?>

	<?= $form->field($model, 'language_id', ['enableAjaxValidation' => true])->dropDownList(
    		ArrayHelper::map(Language::find()->where('is_active=1')->all(), 'id', 'name'
    ))->label(Yii::t('main', 'Language')) ?>

	<?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('main', 'Create') : Yii::t('main', 'Save'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
