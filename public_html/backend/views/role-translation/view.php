<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\ar\RoleTranslation */

$this->title = Yii::t('main', 'Role translation') . ' - ' . $model->name;
$this->params['breadcrumbs'][] = ['label' => Yii::t('main', 'Role translations'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="role-translation-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
    	<?php if(empty($withoutUpdating)) { ?>
    		<?= Html::a(Yii::t('main', 'Update'), ['update', 'role_id' => $model->role_id, 'language_id' => $model->language_id], ['class' => 'btn btn-primary']) ?>
    	<?php } ?>
        <?php if(empty($withoutDeleting)) { ?>
	        <?= Html::a(Yii::t('main', 'Delete'), ['delete', 'role_id' => $model->role_id, 'language_id' => $model->language_id], [
		            'class' => 'btn btn-danger',
		            'data' => [
			                'confirm' => Yii::t('main', 'Are you sure you want to delete this item?'),
			                'method' => 'post',
		            ],
	        ]) ?>
		<?php } ?>
    </p>

    <?= DetailView::widget([
	        'model' => $model,
	        'attributes' => [
					[
	        				'label' => Yii::t('main', 'Role symbol'),
	        				'format' => 'raw',
	        				'value' => Html::a($model->roleSymbol, ['/role/view', 'id' => $model->role_id]),
    				],
					[
	        				'label' => Yii::t('main', 'Language'),
	        				'format' => 'raw',
	        				'value' => Html::a($model->languageName, ['/language/view', 'id' => $model->language_id]),
    				],
					'name',
        	],
    ]) ?>

</div>
