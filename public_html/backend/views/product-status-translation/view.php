<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\ar\ProductStatusTranslation */

$this->title = Yii::t('main', 'Warehouse operation type translation') . ' - ' . $model->name;
$this->params['breadcrumbs'][] = ['label' => Yii::t('main', 'Warehouse operation type translations'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="product-status-translation-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
    	<?php if(empty($withoutUpdating)) { ?>
    		<?= Html::a(Yii::t('main', 'Update'), ['update', 'product_status_id' => $model->product_status_id, 'language_id' => $model->language_id], ['class' => 'btn btn-primary']) ?>
    	<?php } ?>
        <?php if(empty($withoutDeleting)) { ?>
	        <?= Html::a(Yii::t('main', 'Delete'), ['delete', 'product_status_id' => $model->product_status_id, 'language_id' => $model->language_id], [
		            'class' => 'btn btn-danger',
		            'data' => [
			                'confirm' => Yii::t('main', 'Are you sure you want to delete this item?'),
			                'method' => 'post',
		            ],
	        ]) ?>
		<?php } ?>
    </p>

    <?= DetailView::widget([
	        'model' => $model,
	        'formatter' => ['class' => 'yii\i18n\Formatter', 'nullDisplay' => ''],
	        'attributes' => [
					[
	        			'attribute' => 'productStatusSymbol',
	        			'format' => 'raw',
	        			'value' => Html::a($model->productStatusSymbol, ['/product-status/view', 'id' => $model->product_status_id]),
	        		],
	        		[
	        			'attribute' => 'languageName',
	        			'format' => 'raw',
	        			'value' => Html::a($model->languageName, ['/language/view', 'id' => $model->language_id]),
	        		],
					'name',
        	],
    ]) ?>

</div>
