<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\ar\ProductStatusTranslation */

$this->title = Yii::t('main', 'Create warehouse operation type translation');
$this->params['breadcrumbs'][] = ['label' => Yii::t('main', 'Warehouse operation type translations'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="product-status-translation-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        	'model' => $model,
    		'productStatusId' => $productStatusId
    ]) ?>

</div>
