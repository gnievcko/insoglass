<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use common\models\ar\Warehouse;
use kartik\widgets\Select2;
use yii\helpers\Url;
use yii\web\JsExpression;

/* @var $this yii\web\View */
/* @var $model common\models\ar\Warehouse */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="warehouse-form">

    <?php $form = ActiveForm::begin([
    		'id' => 'warehouse-form-ajax',
    		'enableAjaxValidation' => true,
    ]); ?>

	<?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>
	
	<?= $form->field($model, 'index')->textInput(['maxlength' => true]) ?>

	<?= $form->field($model, 'parent_warehouse_id')->dropDownList(ArrayHelper::map(
			$model->isNewRecord ? Warehouse::find()->all():Warehouse::find()->where('id != :id', ['id' => $model->id])->all(), 'id', 'name'		
	), !empty($parentWarehouseId) ? array('prompt' => '-', 'options' => array($parentWarehouseId => array('selected' => true))):array('prompt' => '-'))->label(Yii::t('main', 'Parent warehouse')) ?>
	<div class="row">
		<div class="col-sm-9">	
			<?php 
				echo $form->field($model, 'address_id')
								->label(Yii::t('main', 'Address'))
								->widget(Select2::classname(), [
										'language' => Yii::$app->language,
										'options' => ['placeholder' => Yii::t('main', 'Select an address...')],
										'data' => $addressData,
										'pluginOptions' => [
												'allowClear' => true,
												'minimumInputLength' => 1,
												'ajax' => [
														'url' => Url::to(['site/find-address']),
														'dataType' => 'json',
														'delay' => 250,
														'data' => new JsExpression('function(params) { return {term:params.term, page:params.page, size: 20}; }'),
														'processResults' => new JsExpression('function (data, params) {
															params.page = params.page || 1;
		
															return {
																results: data.items,
																pagination: {
																	more: data.more
																}
															};
														}'),
														'cache' => true
												],
												'templateResult' => new JsExpression('function(item) { if(item.loading) return item.text; return item.name; }'),
												'templateSelection' => new JsExpression('function (item) { return item.name || item.text; }'),
										]
						]);
			?>
		</div>
		<div class="col-sm-3 link-button-container">
			<?= Html::a(Yii::t('main', 'Create address'), ['/address/create'], ['class' => 'btn btn-primary filter-button create-link-button', 'target' => '_blank']);?>
		</div>
	</div>
	
	<?= $form->field($model, 'is_active')->checkbox() ?>

	<?php  //echo $form->field($model, 'date_creation')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('main', 'Create') : Yii::t('main', 'Save'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>