<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\ar\Warehouse */

$this->title = Yii::t('main', 'Create warehouse');
$this->params['breadcrumbs'][] = ['label' => Yii::t('main', 'Warehouses'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="warehouse-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        	'model' => $model,
    		'addressData' => $addressData,
    		'parentWarehouseId' => $parentWarehouseId,
    ]) ?>

</div>
