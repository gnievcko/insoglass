<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\ars\WarehouseSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<?php
$this->registerJs('
	$(".reset-button").click(function(e){
		e.preventDefault();
		$(".form-control").val("");
	});
');
?>

<div class="warehouse-search">

    <?php $form = ActiveForm::begin([
	        'action' => ['index'],
	        'method' => 'get',
    ]); ?>
	<div class="row">
	<div class="col-md-6">
		<?= $form->field($model, 'id')->textInput(['type' => 'number']) ?>
	</div>
	<div class="col-md-6">
	<?= $form->field($model, 'name') ?>
	</div>
	<div class="col-md-6">
	<?= $form->field($model, 'index') ?>
	</div>
	<div class="col-md-6">
		<?= $form->field($model, 'parentWarehouseName') ?>
	</div>
	<div class="col-md-6">
		<?= $form->field($model, 'addressData') ?>
	</div>
	<div class="col-md-6">
		<?= $form->field($model, 'is_active')->dropDownList([ 
					0 => Yii::t('main', 'No'), 1 => Yii::t('main', 'Yes')				
			], ['prompt' => '']) ?>
	</div>
	<div class="col-md-6">
		<?= $form->field($model, 'date_creation') ?>
	</div>
	</div>
    <div class="form-group">
        <?= Html::submitButton(Yii::t('main', 'Search'), ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton(Yii::t('main', 'Reset'), ['class' => 'btn btn-default reset-button']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
