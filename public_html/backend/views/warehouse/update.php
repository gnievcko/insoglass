<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\ar\Warehouse */

$this->title = Yii::t('main', 'Update warehouse') . ' - ' . $model->name;
$this->params['breadcrumbs'][] = ['label' => Yii::t('main', 'Warehouses'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => Yii::t('main', 'Warehouse') . ' - ' . $model->name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('main', 'Update');
?>
<div class="warehouse-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        	'model' => $model,
    		'addressData' => $addressData
    ]) ?>

</div>
