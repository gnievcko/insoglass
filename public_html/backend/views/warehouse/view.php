<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use common\helpers\StringHelper;

/* @var $this yii\web\View */
/* @var $model common\models\ar\Warehouse */

$this->title = Yii::t('main', 'Warehouse') . ' - ' . $model->name;
$this->params['breadcrumbs'][] = ['label' => Yii::t('main', 'Warehouses'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="warehouse-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
    	<?php if(empty($withoutUpdating)) { ?>
    		<?= Html::a(Yii::t('main', 'Update'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
    	<?php } ?>
        <?php if(empty($withoutDeleting)) { ?>
	        <?= Html::a(Yii::t('main', 'Delete'), ['delete', 'id' => $model->id], [
		            'class' => 'btn btn-danger',
		            'data' => [
			                'confirm' => Yii::t('main', 'Are you sure you want to delete this item?'),
			                'method' => 'post',
		            ],
	        ]) ?>
		<?php } ?>
    </p>

    <?= DetailView::widget([
	        'model' => $model,
	        'formatter' => ['class' => 'yii\i18n\Formatter', 'nullDisplay' => ''],
	        'attributes' => [
					'id',
					'name',
	        		'index',
					[
	        			'attribute' => 'parentWarehouseName',
	        			'format' => 'raw',
	        			'value' => Html::a($model->parentWarehouseName, ['/warehouse/view', 'id' => $model->parent_warehouse_id]),
	        		],
					[
	        			'attribute' => 'addressData',
	        			'format' => 'raw',
	        			'value' => Html::a($model->addressData, ['/address/view', 'id' => $model->address_id]),
	        		],
					[
							'attribute' => 'is_active',
							'value' => StringHelper::boolTranslation($model->is_active)
					],
					'date_creation',
        	],
    ]) ?>

</div>

<?php
         echo Yii::$app->controller->renderPartial('/warehouse/index', [
         		'dataProvider' => $warehouseDataProvider,
         		'parentWarehouseId' => $model->id,
         		'withoutDecorations' => true,
        ]);
 ?>
<?php 
	 echo Yii::$app->controller->renderPartial('/user-warehouse/index', [
						'dataProvider' => $userWarehouseDataProvider,
						'warehouseId' => $model->id,
						'withoutDecorations' => true,
						'withoutUpdating' => true,
				]);
 ?>