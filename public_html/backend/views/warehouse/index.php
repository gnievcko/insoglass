<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
use common\helpers\StringHelper;

/* @var $this yii\web\View */
/* @var $searchModel common\models\ars\WarehouseSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

if(empty($withoutDecorations)) {
	if(!empty($searchModel)) {
		$this->title = Yii::t('main', 'Warehouses');
		$this->params['breadcrumbs'][] = $this->title;
		
		$this->registerJs('
			$(".filter-button").click(function(e){
				e.preventDefault();
				$(".search-form").toggle();
			});
		');
	}
}
?>
<div class="warehouse-index">

	<?php if(empty($withoutDecorations)) { ?>
    	<h1><?= Html::encode($this->title) ?></h1>
	<?php } else { ?>
    	<h2><?=Yii::t('main', 'Warehouses') ?></h2>
	<?php } ?>

	<?php if(!empty($searchModel)) { ?>
		<div class="search-form" style="display:none">
			<?php  echo $this->render('_search', ['model' => $searchModel]); ?>
		</div>
	<?php } ?>

    <p>
        <?php if(empty($withoutCreating)) { ?>
		<?= Html::a(Yii::t('main', 'Create warehouse'),
				['warehouse/create', 'parentWarehouseId' => !empty($parentWarehouseId) ? $parentWarehouseId : null],
				['class' => 'btn btn-success']) ?>
		<?php } ?>
		<?php if(!empty($searchModel)) {
			echo '&nbsp'. Html::button(Yii::t('main', 'Filters'), ['class' => 'btn btn-primary filter-button']);
		} ?>
		<?php if(!empty($parentWarehouseId)) {
			//echo Html::a(Yii::t('main', 'List of all warehouses'), ['/warehouse/index'], ['class' => 'btn btn-info']);
		} ?>
    </p>

	<?php Pjax::begin(); ?>
    	<?= GridView::widget([
        		'dataProvider' => $dataProvider,
        		'formatter' => ['class' => 'yii\i18n\Formatter', 'nullDisplay' => ''],
        		'filterModel' => !empty($searchModel) ? $searchModel : null,
				'columns' => [
						[
								'attribute' => 'id',
								'contentOptions' => ['class' => 'column-id'],
						],
						'name',
						'index',
						[
								'attribute' => 'parentWarehouseName',
								'visible' => !empty($searchModel),
    					],
						[
								'attribute' => 'is_active',
								'filter' => [0 => Yii::t('main', 'No'), 1 => Yii::t('main', 'Yes')],
								'label' => Yii::t('main', 'Is active'),
								'value' => function($model) { return StringHelper::boolTranslation($model['is_active'], false); },
						],
						'date_creation',
						[
								'class' => 'backend\components\ActionColumn',
								'deleteConfirmMessage' => Yii::t('main', 'Are you sure you want to delete this item?'),
								'visibleButtons' => ['delete' => empty($withoutDeleting), 'update' => empty($withoutUpdating)],
								'contentOptions' => ['class' => 'action-column'],
						]	
    			],
    	]); ?>
	<?php Pjax::end(); ?>

</div>
