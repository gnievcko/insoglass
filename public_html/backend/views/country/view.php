<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\ar\Country */

$this->title = Yii::t('main', 'Country') . ' - ' . $model->symbol;
$this->params['breadcrumbs'][] = ['label' => Yii::t('main', 'Countries'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="country-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
    	<?php if(empty($withoutUpdating)) { ?>
    		<?= Html::a(Yii::t('main', 'Update'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
    	<?php } ?>
        <?php if(empty($withoutDeleting)) { ?>
	        <?= Html::a(Yii::t('main', 'Delete'), ['delete', 'id' => $model->id], [
		            'class' => 'btn btn-danger',
		            'data' => [
			                'confirm' => Yii::t('main', 'Are you sure you want to delete this item?'),
			                'method' => 'post',
		            ],
	        ]) ?>
		<?php } ?>
    </p>

    <?= DetailView::widget([
	        'model' => $model,
    		'formatter' => ['class' => 'yii\i18n\Formatter', 'nullDisplay' => ''],
	        'attributes' => [
					'id',
					'symbol',
					'priority',
        	],
    ]) ?>

</div>
<?php 
	echo Yii::$app->controller->renderPartial('/country-translation/index', [
			'dataProvider' => $countryTranslationDataProvider,
			'withoutDecorations' => true,
			'countryId' => $model->id,
	]);
	
	echo Yii::$app->controller->renderPartial('/province/index', [
			'dataProvider' => $provinceDataProvider,
			'withoutDecorations' => true,
			'countryId' => $model->id,
	]);
?>
