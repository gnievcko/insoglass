<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use common\helpers\StringHelper;

/* @var $this yii\web\View */
/* @var $model common\models\ar\ProductStatus */

$this->title = Yii::t('main', 'Warehouse operation type') . ' - ' . $model->symbol;
$this->params['breadcrumbs'][] = ['label' => Yii::t('main', 'Warehouse operation types'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="product-status-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
    	<?php if(empty($withoutUpdating)) { ?>
    		<?= Html::a(Yii::t('main', 'Update'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
    	<?php } ?>
        <?php if(empty($withoutDeleting)) { ?>
	        <?= Html::a(Yii::t('main', 'Delete'), ['delete', 'id' => $model->id], [
		            'class' => 'btn btn-danger',
		            'data' => [
			                'confirm' => Yii::t('main', 'Are you sure you want to delete this item?'),
			                'method' => 'post',
		            ],
	        ]) ?>
		<?php } ?>
    </p>

    <?= DetailView::widget([
	        'model' => $model,
	        'formatter' => ['class' => 'yii\i18n\Formatter', 'nullDisplay' => ''],
	        'attributes' => [
					'id',
					'symbol',
					[
							'attribute' => 'is_active',
							'value' => StringHelper::boolTranslation($model->is_active)
					],
	        		[
	        				'attribute' => 'is_positive',
	        				'value' => StringHelper::boolTranslation($model->is_positive)
	        		],
        	],
    ]) ?>

</div>

<?php
         echo Yii::$app->controller->renderPartial('/product-status-translation/index', [
         		'dataProvider' => $productStatusTranslationDataProvider,
         		'productStatusId' => $model->id,
         		'withoutDecorations' => true,
        ]);
 ?>
 
 <?php
         echo Yii::$app->controller->renderPartial('/product-status-reference/index', [
         		'dataProvider' => $productStatusReferenceDataProvider,
         		'productStatusId' => $model->id,
         		'withoutDecorations' => true,
        ]);
 ?>
