<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\ar\ProductStatus */

$this->title = Yii::t('main', 'Create warehouse operation type');
$this->params['breadcrumbs'][] = ['label' => Yii::t('main', 'Warehouse operation types'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="product-status-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        	'model' => $model,
    ]) ?>

</div>
