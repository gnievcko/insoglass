<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
use common\helpers\StringHelper;

/* @var $this yii\web\View */
/* @var $searchModel common\models\ars\OrderStatusTransitionSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

if(empty($withoutDecorations)) {
	if(!empty($searchModel)) {
		$this->title = StringHelper::translateOrderToOffer('main', 'Transitions between statuses orders');
		$this->params['breadcrumbs'][] = $this->title;
		
		$this->registerJs('
			$(".filter-button").click(function(e){
				e.preventDefault();
				$(".search-form").toggle();
			});
		');
	}
}
?>
<div class="order-status-transition-index">

	<?php if(empty($withoutDecorations)) { ?>
    	<h1><?= Html::encode($this->title) ?></h1>
	<?php } else if(!empty($predecessorsOnly)) { ?>
    	<h2><?= Yii::t('main', 'Predecessors') ?></h2>
	<?php } else if(!empty($successorsOnly)) { ?>
    	<h2><?= Yii::t('main', 'Successors') ?></h2>
	<?php } ?>

	<?php if(!empty($searchModel)) { ?>
		<div class="search-form" style="display:none">
			<?php // echo $this->render('_search', ['model' => $searchModel]); ?>
		</div>
	<?php } ?>

    <p>
    	<?php if(empty($withoutCreating)) { ?>
            <?php 
                $createBtnTitle = Yii::t('main', 'Create status transition');
                if(!empty($predecessorsOnly)) {
                    $createBtnTitle = Yii::t('main', 'Create predecessor');
                }
                else if(!empty($successorsOnly)) {
                    $createBtnTitle = Yii::t('main', 'Create successor');
                }
            ?>

            <?= Html::a($createBtnTitle, ['order-status-transition/create', 
                'successorId' => empty($predecessorsOnly) ? null : $orderStatusId,
                'predecessorId' => empty($successorsOnly) ? null : $orderStatusId,
            ], ['class' => 'btn btn-success']) ?>
		<?php } ?>
		<?php if(!empty($searchModel)) {
			//echo '&nbsp'. Html::button(Yii::t('main', 'Filters'), ['class' => 'btn btn-primary filter-button']);
		} ?>
    </p>

	<?php Pjax::begin(); ?>
    	<?= GridView::widget([
        		'dataProvider' => $dataProvider,
        		'formatter' => ['class' => 'yii\i18n\Formatter', 'nullDisplay' => ''],
        		'filterModel' => !empty($searchModel) ? $searchModel : null,
				'columns' => [
                        [
                            'attribute' => 'orderStatusPredecessorTranslation',
                            'visible' => empty($successorsOnly),
                        ],
                        [
                            'attribute' => 'orderStatusSuccessorTranslation',
                            'visible' => empty($predecessorsOnly),
                        ],
            			[
            					'class' => 'backend\components\ActionColumn',
            					'controller' => 'order-status-transition',
            					'deleteConfirmMessage' => Yii::t('main', 'Are you sure you want to delete this item?'),
            					'visibleButtons' => ['delete' => empty($withoutDeleting), 'update' => empty($withoutUpdating)],
								'contentOptions' => ['class' => 'action-column'],
            			],
        		],
    	]); ?>
	<?php Pjax::end(); ?>

</div>
