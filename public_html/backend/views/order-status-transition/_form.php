<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use common\models\ar\OrderStatus;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $model common\models\ar\OrderStatusTransition */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="order-status-transition-form">

    <?php $form = ActiveForm::begin([
    		'id' => 'order-status-transition-form-ajax',
    		'enableAjaxValidation' => true,
    ]); ?>

    <?php 
        $successors = OrderStatus::find()->joinWith(['orderStatusTranslations.language'])
            ->andWhere(['or', 
                ['language.symbol' => \Yii::$app->language],
                ['IS', 'language.symbol', NULL],
        ]);
        $predecessors = clone $successors;

        if(!empty($successorId)) {
            $successors->andWhere(['order_status.id' => $successorId]);
            $predecessors->andWhere(['!=', 'order_status.id', $successorId]);
            $model->order_status_successor_id = $successorId;
        }
        else if(!empty($predecessorId)) {
            $successors->andWhere(['!=', 'order_status.id',  $predecessorId]);
            $predecessors->andWhere(['order_status.id' => $predecessorId]);
            $model->order_status_predecessor_id = $predecessorId;
        }

        $extractTranslation = function($status) {
            return empty($status->orderStatusTranslation) ? $status->symbol : $status->orderStatusTranslation->name;
        };

        $predecessorsList = ArrayHelper::map($predecessors->all(), 'id', $extractTranslation);
        $successorsList = ArrayHelper::map($successors->all(), 'id', $extractTranslation);
    ?>

	<?= $form->field($model, 'order_status_predecessor_id')->dropDownList($predecessorsList, empty($predecessorId) ? [] : ['readOnly' => true])->label(Yii::t('main', 'Predecessor status')) ?>

	<?= $form->field($model, 'order_status_successor_id')->dropDownList($successorsList, empty($successorId) ? [] : ['readOnly' => true])->label(Yii::t('main', 'Successor status')) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('main', 'Create') : Yii::t('main', 'Save'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
