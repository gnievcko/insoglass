<?php

use yii\helpers\Html;
use common\helpers\StringHelper;

/* @var $this yii\web\View */
/* @var $model common\models\ar\OrderStatusTransition */

$this->title = StringHelper::translateOrderToOffer('main', 'Update order status transition') . ' #' . $model->order_status_predecessor_id;
$this->params['breadcrumbs'][] = ['label' => StringHelper::translateOrderToOffer('main', 'Transitions between statuses orders'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => StringHelper::translateOrderToOffer('main', 'Transition between statuses orders') . ' #' . $model->order_status_predecessor_id, 'url' => ['view', 'order_status_predecessor_id' => $model->order_status_predecessor_id, 'order_status_successor_id' => $model->order_status_successor_id]];
$this->params['breadcrumbs'][] = Yii::t('main', 'Update');
?>
<div class="order-status-transition-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        	'model' => $model,
    ]) ?>

</div>
