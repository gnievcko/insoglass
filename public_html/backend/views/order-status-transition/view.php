<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use common\helpers\StringHelper;

/* @var $this yii\web\View */
/* @var $model common\models\ar\OrderStatusTransition */

$this->title = StringHelper::translateOrderToOffer('main', 'Transition between statuses orders') . ' - ' . "{$model->orderStatusPredecessorTranslation}-{$model->orderStatusSuccessorTranslation}";
$this->params['breadcrumbs'][] = ['label' => StringHelper::translateOrderToOffer('main', 'Transitions between statuses orders'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="order-status-transition-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
    	<?php if(empty($withoutUpdating)) { ?>
    		<?= Html::a(Yii::t('main', 'Update'), ['update', 'order_status_predecessor_id' => $model->order_status_predecessor_id, 'order_status_successor_id' => $model->order_status_successor_id], ['class' => 'btn btn-primary']) ?>
    	<?php } ?>
        <?php if(empty($withoutDeleting)) { ?>
	        <?= Html::a(Yii::t('main', 'Delete'), ['delete', 'order_status_predecessor_id' => $model->order_status_predecessor_id, 'order_status_successor_id' => $model->order_status_successor_id], [
		            'class' => 'btn btn-danger',
		            'data' => [
			                'confirm' => Yii::t('main', 'Are you sure you want to delete this item?'),
			                'method' => 'post',
		            ],
	        ]) ?>
		<?php } ?>
    </p>

    <?= DetailView::widget([
	        'model' => $model,
	        'formatter' => ['class' => 'yii\i18n\Formatter', 'nullDisplay' => ''],
	        'attributes' => [
	        		[
	        				'attribute' => Yii::t('main', 'Predecessor'),
	        				'format' => 'raw',
	        				'value' => $model->order_status_predecessor_id ? Html::a($model->orderStatusPredecessorTranslation, ['/order-status/view', 'id' => $model->order_status_predecessor_id]) : null,
	        		],
	        		[
	        				'label' => Yii::t('main', 'Successor'),
	        				'format' => 'raw',
	        				'value' => $model->order_status_successor_id ? Html::a($model->orderStatusSuccessorTranslation, ['/order-status/view', 'id' => $model->order_status_successor_id]) : null,
	        		],
        	],
    ]) ?>

</div>
