<?php

use yii\helpers\Html;
use common\helpers\StringHelper;

/* @var $this yii\web\View */
/* @var $model common\models\ar\OrderStatusTransition */

$this->title = StringHelper::translateOrderToOffer('main', 'Create status transition');
$this->params['breadcrumbs'][] = ['label' => StringHelper::translateOrderToOffer('main', 'Transitions between statuses orders'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="order-status-transition-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        	'model' => $model,
            'successorId' => $successorId,
            'predecessorId' => $predecessorId,
    ]) ?>

</div>
