<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $searchModel common\models\ars\UserCompanySalesmanSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

if(empty($withoutDecorations)) {
	if(!empty($searchModel)) {
		$this->title = Yii::t('main', 'Assignment of salesmen to groups');
		$this->params['breadcrumbs'][] = $this->title;
		
		$this->registerJs('
			$(".filter-button").click(function(e){
				e.preventDefault();
				$(".search-form").toggle();
			});
		');
	}
}
?>
<div class="user-company-salesman-index">

	<?php if(empty($withoutDecorations)) { ?>
    	<h1><?= Html::encode($this->title) ?></h1>
	<?php } else { ?>
    	<h2> <?= 
    			empty($hideCompanyGroup) ? Yii::t('main', 'Assignment of salesman to groups') : Yii::t('main', 'Assignment of salesmen to group');
    	 ?></h2>
	<?php } ?>

	<?php if(!empty($searchModel)) { ?>
		<div class="search-form" style="display:none">
			<?php // echo $this->render('_search', ['model' => $searchModel]); ?>
		</div>
	<?php } ?>

    <p>
    	<?php if(empty($withoutCreating)) { ?>
			<?= Html::a(
						Yii::t('main', 'Create assignment of salesman to groups'), 
						['user-company-salesman/create', 'companyGroupId' => !empty($companyGroupId) ? $companyGroupId : null],
						['class' => 'btn btn-success']
				); 
			if(!empty($withoutDecorations)){
				echo ' ' .Html::a(Yii::t('main', 'List of all assignments of salesmen to groups'), ['/user-company-salesman/index'], ['class' => 'btn btn-info']);
			}
			?>
		<?php } ?>
		<?php if(!empty($searchModel)) {
			//echo  Html::button(Yii::t('main', 'Filters'), ['class' => 'btn btn-primary filter-button']);
		} ?>
    </p>

	<?php Pjax::begin(); ?>
    	<?= GridView::widget([
        		'dataProvider' => $dataProvider,
        		'formatter' => ['class' => 'yii\i18n\Formatter', 'nullDisplay' => ''],
        		'filterModel' => !empty($searchModel) ? $searchModel : null,
				'columns' => [
						[
								'attribute' => 'userName',
								'label' => Yii::t('main', 'Salesman'),
								'visible' => empty($hideUser),
						],
						[
								'attribute' => 'companyGroupSymbol',
								'label' => Yii::t('main', 'Client group'),
								'visible' => empty($hideCompanyGroup),
    					],
            			[
            					'class' => 'backend\components\ActionColumn',
            					'controller' => 'user-company-salesman',
            					'deleteConfirmMessage' => Yii::t('main', 'Are you sure you want to delete this item?'),
            					'visibleButtons' => ['delete' => empty($withoutDeleting), 'update' => empty($withoutUpdating)],
								'contentOptions' => ['class' => 'action-column'],
            			],
        		],
    	]); ?>
	<?php Pjax::end(); ?>

</div>
