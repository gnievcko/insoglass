<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\ar\UserCompanySalesman */

$this->title = Yii::t('main', 'Create assignment of salesman to groups');
$this->params['breadcrumbs'][] = ['label' => Yii::t('main', 'Assignment of salesmen to groups'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="user-company-salesman-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
            'userData' => $userData,
            'companyGroupData' => $companyGroupData,
    		'companyGroupId' => $companyGroupId,
        	'model' => $model,
    ]) ?>

</div>
