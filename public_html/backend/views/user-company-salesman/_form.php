<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\widgets\Select2;
use yii\helpers\ArrayHelper;
use common\models\ar\Language;
use yii\helpers\Url;
use yii\web\JsExpression;
use common\models\ar\CompanyGroup;
use common\helpers\Utility;
use common\models\ar\CompanyGroupTranslation;

/* @var $this yii\web\View */
/* @var $model common\models\ar\UserCompanySalesman */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="user-company-salesman-form">

    <?php $form = ActiveForm::begin([
    		'id' => 'user-company-salesman-form-ajax',
    		'enableAjaxValidation' => true,
    ]); ?>
	
	<?php
		echo $form->field($model, 'user_id')
				->label(Yii::t('main', 'Salesman'))
				->widget(Select2::classname(), [
							'language' => Yii::$app->language,
							'options' => ['placeholder' => Yii::t('main', 'Select a salesman...')],
							'data' => $companyGroupData,
							'pluginOptions' => [
									'allowClear' => true,
									'minimumInputLength' => 1,
											'ajax' => [
													'url' => Url::to(['site/find-users', 'roles' => [Utility::ROLE_SALESMAN, Utility::ROLE_MAIN_SALESMAN]]),
													'dataType' => 'json',
													'delay' => 250,
													'data' => new JsExpression('function(params) { return {term:params.term, page:params.page, size: 20}; }'),
													'processResults' => new JsExpression('function (data, params) {
														params.page = params.page || 1;
						                            	var users = $(data.items).map(function(index, user) {
						                                return {id: user.id, name: user.first_name + ((user.first_name == null) ? "" : " ") + user.last_name};
						                             });
                                             
													return {
														results: users,
														pagination: {
															more: data.more
														}
													};
													}'),
													'cache' => true
											],
											'templateResult' => new JsExpression('function(item) { if(item.loading) return item.text; return item.name; }'),
											'templateSelection' => new JsExpression('function (item) { return item.name || item.text; }'),
							]
			]);
	?>
	<?php
				if(!empty(Yii::$app->request->get('companyGroupId'))){					
					echo $form->field($model, 'company_group_id', ['enableAjaxValidation' => true])->dropDownList(ArrayHelper::map(
						(CompanyGroupTranslation::find()->where(['company_group_id' => $companyGroupId])
								->andWhere(['language_id' => Language::find()->where(['symbol' => Yii::$app->language])->one()->id])->all()),
							'company_group_id',
							'name'
					     ))->label(Yii::t('main', 'Client group'));				
				}
				else{
					echo $form->field($model, 'company_group_id')
					->label(Yii::t('main', 'Client group'))
					->widget(Select2::classname(), [
							'language' => Yii::$app->language,
							'options' => ['placeholder' => Yii::t('main', 'Select a client group...'), ],
							'data' => $companyGroupData,
							'pluginOptions' => [
									'allowClear' => true,
									'minimumInputLength' => 1,
									'ajax' => [
											'url' => Url::to(['site/find-company-group']),
											'dataType' => 'json',
											'delay' => 250,
											'data' => new JsExpression('function(params) { return {term:params.term, page:params.page, size: 20}; }'),
											'processResults' => new JsExpression('function (data, params) {
													params.page = params.page || 1;
									
													return {
														results: data.items,
														pagination: {
															more: data.more
														}
													};
												}'),
											'cache' => true
									],
									'templateResult' => new JsExpression('function(item) { if(item.loading) return item.text; return item.name; }'),
									'templateSelection' => new JsExpression('function (item) { return item.name || item.text; }'),
							]
					]);
				}				
	?>
					

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('main', 'Create') : Yii::t('main', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>