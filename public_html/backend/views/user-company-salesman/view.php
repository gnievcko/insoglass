<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use common\models\ar\User;

/* @var $this yii\web\View */
/* @var $model common\models\ar\UserCompanySalesman */

$this->title = Yii::t('main', 'Assignment of salesman to groups') . ' - ' . User::findone($model->user_id)->email;
$this->params['breadcrumbs'][] = ['label' => Yii::t('main', 'Assignment of salesmen to groups'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="user-company-salesman-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
    	<?php if(empty($withoutUpdating)) { ?>
    		<?= Html::a(Yii::t('main', 'Update'), ['update', 'user_id' => $model->user_id, 'company_group_id' => $model->company_group_id], ['class' => 'btn btn-primary']) ?>
    	<?php } ?>
        <?php if(empty($withoutDeleting)) { ?>
	        <?= Html::a(Yii::t('main', 'Delete'), ['delete', 'user_id' => $model->user_id, 'company_group_id' => $model->company_group_id], [
		            'class' => 'btn btn-danger',
		            'data' => [
			                'confirm' => Yii::t('main', 'Are you sure you want to delete this item?'),
			                'method' => 'post',
		            ],
	        ]) ?>
		<?php } ?>
    </p>

    <?= DetailView::widget([
	        'model' => $model,
	        'formatter' => ['class' => 'yii\i18n\Formatter', 'nullDisplay' => ''],
	        'attributes' => [
	        		[
	        				'label' => Yii::t('main', 'User'),
	        				'format' => 'raw',
	        				'value' => Html::a($model->userName, ['/user/view', 'id' => $model->user_id]),
	        		],
	        		[
	        				'label' => Yii::t('main', 'Client group'),
	        				'format' => 'raw',
	        				'value' => Html::a($model->companyGroupSymbol, ['/company-group/view', 'id' => $model->company_group_id]),
	        		],
        	],
    ]) ?>

</div>
