<?php

use yii\helpers\Html;
use common\models\ar\User;

/* @var $this yii\web\View */
/* @var $model common\models\ar\UserCompanySalesman */

$this->title = Yii::t('main', 'Update assignment of salesman to groups') . ' - ' . User::findone($model->user_id)->email;
$this->params['breadcrumbs'][] = ['label' => Yii::t('main', 'Assignment of salesmen to groups'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => Yii::t('main', 'Assignment of salesman to groups') . ' - ' . User::findone($model->user_id)->email, 'url' => ['view', 'user_id' => $model->user_id, 'company_group_id' => $model->company_group_id]];
$this->params['breadcrumbs'][] = Yii::t('main', 'Update');
?>
<div class="user-company-salesman-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        	'model' => $model,
    ]) ?>

</div>
