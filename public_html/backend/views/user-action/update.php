<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\ar\UserAction */

$this->title = Yii::t('main', 'Update user action') . ' #' . $model->id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('main', 'User actions'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => Yii::t('main', 'User action') . ' #' . $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('main', 'Update');
?>
<div class="user-action-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        	'model' => $model,
    ]) ?>

</div>
