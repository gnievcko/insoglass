<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
use frontend\helpers\UserHelper;
use common\models\ar\User;

/* @var $this yii\web\View */
/* @var $searchModel common\models\ars\UserActionSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

if(empty($withoutDecorations)) {
	if(!empty($searchModel)) {
		$this->title = Yii::t('main', 'Login history');
		$this->params['breadcrumbs'][] = $this->title;
		
		$this->registerJs('
			$(".filter-button").click(function(e){
				e.preventDefault();
				$(".search-form").toggle();
			});
		');
	}
}
?>
<div class="user-action-index">

	<?php if(empty($withoutDecorations)) { ?>
		<?php if (!empty($_GET['user_id'])) {
			$user = User::find()->where(['id' => $_GET['user_id']])->one();
			$this->title .= ' - ' . UserHelper::getPrettyUserName($user->email, $user->first_name, $user->last_name);
		}?>
    	<h1><?= Html::encode($this->title) ?></h1>
	<?php } else { ?>
    	<h2>Yii::t('main', 'User actions')</h2>
	<?php } ?>

	<?php if(!empty($searchModel)) { ?>
		<div class="search-form" style="display:none">
			<?php // echo $this->render('_search', ['model' => $searchModel]); ?>
		</div>
	<?php } ?>

    <p>
    	<?php if(empty($withoutCreating)) { ?>
			<?= Html::a(Yii::t('main', 'Create user action'), ['create'], ['class' => 'btn btn-success']) ?>
		<?php } ?>
		<?php if(!empty($searchModel)) {
			//echo '&nbsp'. Html::button(Yii::t('main', 'Filters'), ['class' => 'btn btn-primary filter-button']);
		} ?>
    </p>

	<?php Pjax::begin(); ?>
    	<?= GridView::widget([
        		'dataProvider' => $dataProvider,
        		'formatter' => ['class' => 'yii\i18n\Formatter', 'nullDisplay' => ''],
        		'filterModel' => !empty($searchModel) ? $searchModel : null,
				'columns' => [
						[
								'attribute' => 'id',
								'contentOptions' => ['class' => 'column-id'],
						],
						'userName',
						'date_action',
            	/*		[
            					'class' => 'backend\components\ActionColumn',
            					'deleteConfirmMessage' => Yii::t('main', 'Are you sure you want to delete this item?'),
            					'visibleButtons' => ['delete' => empty($withoutDeleting), 'update' => empty($withoutUpdating)],
								'contentOptions' => ['class' => 'action-column'],
            			],
            	*/
        		],
    	]); ?>
	<?php Pjax::end(); ?>

</div>
