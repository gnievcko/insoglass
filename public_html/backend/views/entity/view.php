<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use common\helpers\StringHelper;

/* @var $this yii\web\View */
/* @var $model common\models\ar\Entity */

$this->title = Yii::t('main', 'Economic entity') . ' ' . $model->name;
$this->params['breadcrumbs'][] = ['label' => Yii::t('main', 'Economic entities'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="entity-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
    	<?php if(empty($withoutUpdating)) { ?>
    		<?= Html::a(Yii::t('main', 'Update'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
    	<?php } ?>
        <?php if(empty($withoutDeleting)) { ?>
	        <?= Html::a(Yii::t('main', 'Delete'), ['delete', 'id' => $model->id], [
		            'class' => 'btn btn-danger',
		            'data' => [
			                'confirm' => Yii::t('main', 'Are you sure you want to delete this item?'),
			                'method' => 'post',
		            ],
	        ]) ?>
		<?php } ?>
    </p>

    <?= DetailView::widget([
	        'model' => $model,
	        'formatter' => ['class' => 'yii\i18n\Formatter', 'nullDisplay' => ''],
	        'attributes' => [
					'id',
					'name',
					[
	        				'attribute' => 'addressName',
	        				'format' => 'raw',
	        				'value' => Html::a($model->addressName, ['/address/view', 'id' => $model->address_id]),
    				],
					'taxpayer_identification_number',
					'vat_identification_number',
					'krs_number',
					'email:email',
					'phone',
					'fax',
					'account_number',
					'bank_name',
					'share_capital',
					'website_address',
					[
			        		'attribute' => 'is_active',
			        		'value' => StringHelper::boolTranslation($model->is_active)
	        		],
        	],
    ]) ?>

</div>
