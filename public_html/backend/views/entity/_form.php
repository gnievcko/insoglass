<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\widgets\Select2;
use yii\helpers\Url;
use yii\web\JsExpression;

/* @var $this yii\web\View */
/* @var $model common\models\ar\Entity */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="entity-form">

    <?php $form = ActiveForm::begin([
    		'id' => 'entity-form-ajax',
    		'enableAjaxValidation' => true,
    ]); ?>

	<?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

	<div class="row">
		<div class="col-sm-9">
			<?php
				echo $form->field($model, 'address_id')
						->label(Yii::t('main', 'Address'))
						->widget(Select2::classname(), [
								'language' => Yii::$app->language,
								'options' => ['placeholder' => Yii::t('main', 'Select an address...')],
								'data' => $addressData,
								'pluginOptions' => [
										'allowClear' => true,
										'minimumInputLength' => 1,
										'ajax' => [
												'url' => Url::to(['site/find-address']),
												'dataType' => 'json',
												'delay' => 250,
												'data' => new JsExpression('function(params) { return {term:params.term, page:params.page, size: 20}; }'),
												'processResults' => new JsExpression('function (data, params) {
													params.page = params.page || 1;

													return {
														results: data.items,
														pagination: {
															more: data.more
														}
													};
												}'),
												'cache' => true
										],
										'templateResult' => new JsExpression('function(item) { if(item.loading) return item.text; return item.name; }'),
										'templateSelection' => new JsExpression('function (item) { return item.name || item.text; }'),
								]
				]);
			?>
		</div>
		<div class="col-sm-3 link-button-container">
			<?= Html::a(Yii::t('main', 'Create address'), ['/address/create'], ['class' => 'btn btn-primary filter-button create-link-button', 'target' => '_blank']);?>
		</div>
	</div>

	<?= $form->field($model, 'taxpayer_identification_number')->textInput(['maxlength' => true]) ?>

	<?= $form->field($model, 'vat_identification_number')->textInput(['maxlength' => true]) ?>

	<?= $form->field($model, 'krs_number')->textInput(['maxlength' => true]) ?>

	<?= $form->field($model, 'email')->textInput(['maxlength' => true]) ?>

	<?= $form->field($model, 'phone')->textInput(['maxlength' => true]) ?>

	<?= $form->field($model, 'fax')->textInput(['maxlength' => true]) ?>

	<?= $form->field($model, 'account_number')->textInput(['maxlength' => true]) ?>

	<?= $form->field($model, 'bank_name')->textInput(['maxlength' => true]) ?>

	<?= $form->field($model, 'share_capital')->textInput(['maxlength' => true]) ?>

	<?= $form->field($model, 'website_address')->textInput(['maxlength' => true]) ?>

	<?= $form->field($model, 'is_active')->checkbox() ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('main', 'Create') : Yii::t('main', 'Save'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>