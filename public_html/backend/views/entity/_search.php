<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\ar\EntitySearch */
/* @var $form yii\widgets\ActiveForm */
?>

<?php
$this->registerJs('
	$(".reset-button").click(function(e){
		e.preventDefault();
		$(".form-control").val("");
	});
');
?>

<div class="entity-search">

    <?php $form = ActiveForm::begin([
	        'action' => ['index'],
	        'method' => 'get',
    ]); ?>

	<?= $form->field($model, 'id') ?>

	<?= $form->field($model, 'name') ?>

	<?= $form->field($model, 'address_id') ?>

	<?= $form->field($model, 'taxpayer_identification_number') ?>

	<?= $form->field($model, 'vat_identification_number') ?>

	<?= $form->field($model, 'krs_number') ?>

	<?= $form->field($model, 'email') ?>

	<?= $form->field($model, 'phone') ?>

	<?= $form->field($model, 'fax') ?>

	<?= $form->field($model, 'account_number') ?>

	<?= $form->field($model, 'bank_name') ?>

	<?= $form->field($model, 'share_capital') ?>

	<?= $form->field($model, 'website_address') ?>

	<?= $form->field($model, 'is_active') ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('main', 'Search'), ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton(Yii::t('main', 'Reset'), ['class' => 'btn btn-default reset-button']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
