<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\ar\Entity */

$this->title = Yii::t('main', 'Create economic entity');
$this->params['breadcrumbs'][] = ['label' => Yii::t('main', 'Economic entities'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="entity-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        	'model' => $model,
    		'addressData' => $addressData,
    ]) ?>

</div>
