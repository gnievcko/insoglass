<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\ar\Entity */

$this->title = Yii::t('main', 'Update economic entity') . ' ' . $model->name;
$this->params['breadcrumbs'][] = ['label' => Yii::t('main', 'Economic entities'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => Yii::t('main', 'Economic entity') . ' ' . $model->name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('main', 'Update');
?>
<div class="entity-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        	'model' => $model,
    		'addressData' => $addressData,
    ]) ?>

</div>
