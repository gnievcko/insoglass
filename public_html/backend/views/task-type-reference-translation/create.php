<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\ar\TaskTypeReferenceTranslation */

$this->title = Yii::t('main', 'Create task type reference translation');
$this->params['breadcrumbs'][] = ['label' => Yii::t('main', 'Task type reference translations'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="task-type-reference-translation-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        	'model' => $model,
    		'referenceId' => $referenceId,
    ]) ?>

</div>
