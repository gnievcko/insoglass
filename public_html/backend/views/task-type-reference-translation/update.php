<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\ar\TaskTypeReferenceTranslation */

$this->title = Yii::t('main', 'Update task type reference translation') . ' #' . $model->name;
$this->params['breadcrumbs'][] = ['label' => Yii::t('main', 'Task type reference translations'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => Yii::t('main', 'Task type reference translation') . ' #' . $model->name, 'url' => ['view', 'reference_id' => $model->reference_id, 'language_id' => $model->language_id]];
$this->params['breadcrumbs'][] = Yii::t('main', 'Update');
?>
<div class="task-type-reference-translation-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        	'model' => $model,
    ]) ?>

</div>
