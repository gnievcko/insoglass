<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use common\models\ar\TaskTypeReference;
use common\models\ar\Language;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $model common\models\ar\TaskTypeReferenceTranslation */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="task-type-reference-translation-form">

    <?php $form = ActiveForm::begin([
    		'id' => 'task-type-reference-translation-form-ajax',
    		'enableAjaxValidation' => true,
    ]); ?>

    <?= $form->field($model, 'reference_id')->dropDownList(ArrayHelper::map(
	       empty($referenceId) ? TaskTypeReference::find()->all() : TaskTypeReference::find()->where(['id' => $referenceId])->all(),
    			 'id', 'symbol' 		
		),['prompt' => '-'])->label(Yii::t('main', 'Task type reference')) ?>

	<?= $form->field($model, 'language_id')->dropDownList(ArrayHelper::map(
	        Language::find()->all(), 'id', 'name'		
		),['prompt' => '-'])->label(Yii::t('main', 'Language')) ?>

	<?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('main', 'Create') : Yii::t('main', 'Save'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>