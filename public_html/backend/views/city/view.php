<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\ar\City */

$this->title = Yii::t('main', 'City') . ' - ' . $model->name;
$this->params['breadcrumbs'][] = ['label' => Yii::t('main', 'Cities'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="city-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
    	<?php if(empty($withoutUpdating)) { ?>
    		<?= Html::a(Yii::t('main', 'Update'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
    	<?php } ?>
        <?php if(empty($withoutDeleting)) { ?>
	        <?= Html::a(Yii::t('main', 'Delete'), ['delete', 'id' => $model->id], [
		            'class' => 'btn btn-danger',
		            'data' => [
			                'confirm' => Yii::t('main', 'Are you sure you want to delete this item?'),
			                'method' => 'post',
		            ],
	        ]) ?>
		<?php } ?>
    </p>

    <?= DetailView::widget([
	        'model' => $model,
	        'attributes' => [
					'id',
	        		[
			        		'attribute' => 'provinceName',
			        		'format' => 'raw',
			        		'value' => Html::a($model->provinceName, ['/province/view', 'id' => $model->province_id]),
	        		],
					'name',
					'zip_code',
        	],
    ]) ?>

</div>
<?php 
	echo Yii::$app->controller->renderPartial('/address/index', [
			'dataProvider' => $addressDataProvider,
			'withoutDecorations' => true,
			'cityId' => $model->id,
	]);
?>
