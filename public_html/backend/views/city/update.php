<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\ar\City */

$this->title = Yii::t('main', 'Update city') . ' - ' . $model->name;
$this->params['breadcrumbs'][] = ['label' => Yii::t('main', 'Cities'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => Yii::t('main', 'City') . ' - ' . $model->name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('main', 'Update');
?>
<div class="city-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        	'model' => $model,
    ]) ?>

</div>
