<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\ar\City */

$this->title = Yii::t('main', 'Create city');
$this->params['breadcrumbs'][] = ['label' => Yii::t('main', 'Cities'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="city-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        	'model' => $model,
    ]) ?>

</div>
