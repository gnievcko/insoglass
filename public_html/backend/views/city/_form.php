<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use common\models\ar\Province;

/* @var $this yii\web\View */
/* @var $model common\models\ar\City */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="city-form">

    <?php $form = ActiveForm::begin([
    		'id' => 'city-form-ajax',
    		'enableAjaxValidation' => true,
    ]); ?>
    
	<?= $form->field($model, 'province_id')->dropDownList(ArrayHelper::map(
			empty($provinceId) ? Province::find()->orderBy('name')->all() : Province::find()->where(['id' => $provinceId])->all(), 'id', 'name'		
	), ['prompt' => '-'])->label(Yii::t('main', 'Province')) ?>

	<?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

	<?= $form->field($model, 'zip_code')->textInput(['maxlength' => true]) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('main', 'Create') : Yii::t('main', 'Save'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>