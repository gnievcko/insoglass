<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
use common\helpers\StringHelper;
use common\helpers\Utility;

/* @var $this yii\web\View */
/* @var $searchModel common\models\ars\ContractTypeSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

if(empty($withoutDecorations)) {
	if(!empty($searchModel)) {
		$this->title = Yii::t('main', 'Contract types');
		$this->params['breadcrumbs'][] = $this->title;
		
		$this->registerJs('
			$(".filter-button").click(function(e){
				e.preventDefault();
				$(".search-form").toggle();
			});
		');
	}
}
?>
<div class="contract-type-index">

	<?php if(empty($withoutDecorations)) { ?>
    	<h1><?= Html::encode($this->title) ?></h1>
	<?php } else { ?>
    	<h2><?= Yii::t('main', 'Contract types') ?></h2>
	<?php } ?>

	<?php if(!empty($searchModel)) { ?>
		<div class="search-form" style="display:none">
			<?php // echo $this->render('_search', ['model' => $searchModel]); ?>
		</div>
	<?php } ?>

    <p>
    	<?php if(empty($withoutCreating)) { ?>
			<?= Html::a(Yii::t('main', 'Create contract type'), ['create'], ['class' => 'btn btn-success']) ?>
		<?php } ?>
		<?php if(!empty($searchModel)) {
			//echo '&nbsp'. Html::button(Yii::t('main', 'Filters'), ['class' => 'btn btn-primary filter-button']);
		} ?>
    </p>

	<?php Pjax::begin(); ?>
    	<?= GridView::widget([
        		'dataProvider' => $dataProvider,
        		'formatter' => ['class' => 'yii\i18n\Formatter', 'nullDisplay' => ''],
        		'filterModel' => !empty($searchModel) ? $searchModel : null,
				'columns' => [
						[
                                                    'attribute' => 'id',
                                                    'contentOptions' => ['class' => 'column-id'],
						],
						'symbol',
						[
                                                    'attribute' => 'is_active',
                                                    'value' => function($model) { return StringHelper::boolTranslation($model->is_active); },
                                                    'filter' => [0 => Yii::t('main', 'No'), 1 => Yii::t('main', 'Yes')],
						],
                                                [
                                                    'class' => 'backend\components\ActionColumn',
                                                    'deleteConfirmMessage' => Yii::t('main', 'Are you sure you want to delete this item?'),
                                                    'visibleButtons' => ['delete' => function($model) use($withoutDeleting) { return empty($withoutDeleting) && ($model->symbol != Utility::CONTRACT_TYPE_MALFUNCTION_REQUEST);}, 
                                                                         'update' => function($model) use($withoutUpdating) { return empty($withoutUpdating) && ($model->symbol != Utility::CONTRACT_TYPE_MALFUNCTION_REQUEST);}],
                                                    'contentOptions' => ['class' => 'action-column'],
                                                    
                                                ],
                                            ],
    	]); ?>
	<?php Pjax::end(); ?>

</div>
