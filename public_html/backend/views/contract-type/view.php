<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use common\helpers\Utility;
use common\helpers\StringHelper;

/* @var $this yii\web\View */
/* @var $model common\models\ar\ContractType */

$this->title = Yii::t('main', 'Contract type') . ' - ' . $model->symbol;
$this->params['breadcrumbs'][] = ['label' => Yii::t('main', 'Contract types'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="contract-type-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
    	<?php if(empty($withoutUpdating) && ($model->symbol != Utility::CONTRACT_TYPE_MALFUNCTION_REQUEST)) { ?>
    		<?= Html::a(Yii::t('main', 'Update'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
    	<?php } ?>
        <?php if(empty($withoutDeleting) && ($model->symbol != Utility::CONTRACT_TYPE_MALFUNCTION_REQUEST)) { ?>
	        <?= Html::a(Yii::t('main', 'Delete'), ['delete', 'id' => $model->id], [
		            'class' => 'btn btn-danger',
		            'data' => [
			                'confirm' => Yii::t('main', 'Are you sure you want to delete this item?'),
			                'method' => 'post',
		            ],
	        ]) ?>
		<?php } ?>
    </p>

    <?= DetailView::widget([
	        'model' => $model,
	        'formatter' => ['class' => 'yii\i18n\Formatter', 'nullDisplay' => ''],
	        'attributes' => [
                                    'id',
                                    'symbol',
                                    [
					'attribute' => 'is_active',
                                        'value' => StringHelper::boolTranslation($model->is_active),
                                    ],
        	],
    ]) ?>

</div>
<?php 
	echo Yii::$app->controller->renderPartial('/contract-type-translation/index', [
			'dataProvider' => $contractTypeTranslationDataProvider,
			'withoutDecorations' => true,
			'contractTypeId' => $model->id,
	]);
?>
