<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\ar\EmailType */

$this->title = Yii::t('main', 'Update e-mail type') . ' - ' . $model->symbol;
$this->params['breadcrumbs'][] = ['label' => Yii::t('main', 'E-mail types'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => Yii::t('main', 'E-mail type') . ' - ' . $model->symbol, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('main', 'Update');
?>
<div class="email-type-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        	'model' => $model,
    ]) ?>

</div>
