<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use common\helpers\StringHelper;

/* @var $this yii\web\View */
/* @var $model common\models\ar\EmailType */

$this->title = Yii::t('main', 'E-mail type') . ' - ' . $model->symbol;
$this->params['breadcrumbs'][] = ['label' => Yii::t('main', 'E-mail types'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="email-type-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
    	<?php if(empty($withoutUpdating)) { ?>
    		<?= Html::a(Yii::t('main', 'Update'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
    	<?php } ?>
        <?php if(empty($withoutDeleting)) { ?>
	        <?= Html::a(Yii::t('main', 'Delete'), ['delete', 'id' => $model->id], [
		            'class' => 'btn btn-danger',
		            'data' => [
			                'confirm' => Yii::t('main', 'Are you sure you want to delete this item?'),
			                'method' => 'post',
		            ],
	        ]) ?>
		<?php } ?>
    </p>

    <?= DetailView::widget([
	        'model' => $model,
    		'formatter' => ['class' => 'yii\i18n\Formatter', 'nullDisplay' => ''],
	        'attributes' => [
					'id',
					'symbol',
	        		'tags',
					[
							'attribute' => 'is_archived',
							'value' => StringHelper::boolTranslation($model->is_archived),
					],
	        		[
							'attribute' => 'is_active',
	        				'value' => StringHelper::boolTranslation($model->is_active),
	        		],
        	],
    ]) ?>

</div>
<?php 
	echo Yii::$app->controller->renderPartial('/email-template/index', [
			'dataProvider' => $emailTemplateDataProvider,
			'withoutDecorations' => true,
			'emailTypeId' => $model->id,
	]);
?>