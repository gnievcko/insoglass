<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\ar\EmailType */

$this->title = Yii::t('main', 'Create e-mail type');
$this->params['breadcrumbs'][] = ['label' => Yii::t('main', 'E-mail types'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="email-type-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        	'model' => $model,
    ]) ?>

</div>
