<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\ar\EmailType */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="email-type-form">

    <?php $form = ActiveForm::begin([
    		'id' => 'email-type-form-ajax',
    		'enableAjaxValidation' => true,
    ]); ?>

	<?= $form->field($model, 'symbol')->textInput(['maxlength' => true]) ?>
	
	<?= $form->field($model, 'tags')->textInput(['maxLength' => true])->hint(Yii::t('main', 'Tags should be typed in curly braces, separated by commas.')) ?>

	<?= $form->field($model, 'is_archived')->textInput() ?>

	<?= $form->field($model, 'is_active')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('main', 'Create') : Yii::t('main', 'Save'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>