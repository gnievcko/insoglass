<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\ar\ProductAttributeTypeTranslation */

$this->title = Yii::t('main', 'Update product attribute type translation') . ' - ' . $model->name;
$this->params['breadcrumbs'][] = ['label' => Yii::t('main', 'Product attribute type translations'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => Yii::t('main', 'Product attribute type translation') . ' - ' . $model->name, 'url' => ['view', 'product_attribute_type_id' => $model->product_attribute_type_id, 'language_id' => $model->language_id]];
$this->params['breadcrumbs'][] = Yii::t('main', 'Update');
?>
<div class="product-attribute-type-translation-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        	'model' => $model,
    ]) ?>

</div>
