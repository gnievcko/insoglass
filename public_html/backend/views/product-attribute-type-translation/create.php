<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\ar\ProductAttributeTypeTranslation */

$this->title = Yii::t('main', 'Create product attribute type translation');
$this->params['breadcrumbs'][] = ['label' => Yii::t('main', 'Product attribute type translations'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="product-attribute-type-translation-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        	'model' => $model,
            'productAttributeTypeId' => $productAttributeTypeId,
    ]) ?>

</div>
