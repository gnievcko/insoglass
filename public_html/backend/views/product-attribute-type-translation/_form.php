<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use common\models\ar\Language;
use common\models\ar\ProductAttributeType;

/* @var $this yii\web\View */
/* @var $model common\models\ar\ProductAttributeTypeTranslation */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="product-attribute-type-translation-form">

    <?php $form = ActiveForm::begin([
    		'id' => 'product-attribute-type-translation-form-ajax',
    		'enableAjaxValidation' => true,
    ]); ?>

	<?= $form->field($model, 'product_attribute_type_id')->dropDownList(ArrayHelper::map(
	       empty($productAttributeTypeId) ? ProductAttributeType::find()->orderBy('id')->all() : ProductAttributeType::find()
	           ->where(['id' => $productAttributeTypeId])->all(), 'id', 'symbol'
	    ))->label(Yii::t('main', 'Product Attribute Type')) ?>

	<?= $form->field($model, 'language_id')->dropDownList(ArrayHelper::map(
	       Language::find()->where('is_active = 1')->all(), 'id', 'name'
	    ))->label(Yii::t('main', 'Language')) ?>

	<?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('main', 'Create') : Yii::t('main', 'Save'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>