<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use common\models\ar\ContractType;
use common\models\ar\Language;

/* @var $this yii\web\View */
/* @var $model common\models\ar\ContractTypeTranslation */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="contract-type-translation-form">

    <?php $form = ActiveForm::begin([
    		'id' => 'contract-type-translation-form-ajax',
    		'enableAjaxValidation' => true,
    ]); ?>
        <?= $form->field($model, 'language_id')->dropDownList(ArrayHelper::map(
			Language::find()->where('is_active = 1')->all(), 'id', 'name'
	))->label(Yii::t('main', 'Language')) ?>
    
        <?= $form->field($model, 'contract_type_id')->dropDownList(ArrayHelper::map(
			empty($contractTypeId) ? ContractType::find()->orderBy('id')->all() : ContractType::find()->where(['id' => $contractTypeId])->all(), 'id', 'symbol'		
	))->label(Yii::t('main', 'Contract type')) ?>

	<?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('main', 'Create') : Yii::t('main', 'Save'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>