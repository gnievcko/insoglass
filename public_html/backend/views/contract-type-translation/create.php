<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\ar\ContractTypeTranslation */

$this->title = Yii::t('main', 'Create contract type translation');
$this->params['breadcrumbs'][] = ['label' => Yii::t('main', 'Contract type translations'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="contract-type-translation-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        	'model' => $model,
                'contractTypeId' => $contractTypeId,
    ]) ?>

</div>
