<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\ar\ContractTypeTranslation */

$this->title = Yii::t('main', 'Update translation');
$this->params['breadcrumbs'][] = ['label' => Yii::t('main', 'Contract type translations'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => Yii::t('main', 'Contract type translation') . ' - ' . $model->name, 'url' => ['view', 'language_id' => $model->language_id, 'contract_type_id' => $model->contract_type_id]];
$this->params['breadcrumbs'][] = Yii::t('main', 'Update');
?>
<div class="contract-type-translation-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        	'model' => $model,
    ]) ?>

</div>
