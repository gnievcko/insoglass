<?php
namespace backend\helpers;

use Yii;
use common\models\ar\User;
use yii\web\ForbiddenHttpException;
use yii\helpers\Url;

class Utility {

	public static function getCKEditorParams() {
		return [
		        'options' => ['rows' => 6],
		        'preset' => 'custom',
				'clientOptions' => [
						'fullPage' => true,
						'height' => 400,
						'allowedContent'=>true,
						'enterMode'=>2,
						'removeFormatTags'=>'',
						'removeFormatAttributes'=>'',
						//'extraAllowedContent' => 'body;html;tbody;thead;style;*[id,rel](*){*}',
						'ignoreEmptyParagraph' => false,
						'entities' => false,
						'filebrowserUploadUrl' => Url::to(['site/upload-image-from-editor']),
						'toolbar' => [
								[ 'name' => 'document', 'items' => [ 'Source', '-', 'Save', 'NewPage', 'Preview', 'Print', '-', 'Templates' ] ],
								[ 'name' => 'clipboard', 'items' => [ 'Cut', 'Copy', 'Paste', 'PasteText', 'PasteFromWord', '-', 'Undo', 'Redo' ] ],
								[ 'name' => 'editing', 'items' => [ 'Find', 'Replace', '-', 'SelectAll' ] ],
								//[ 'name' => 'forms', 'items' => [ 'Form', 'Checkbox', 'Radio', 'TextField', 'Textarea', 'Select', 'Button', 'ImageButton', 'HiddenField' ] ],
								'/',
								[ 'name' => 'basicstyles', 'items' => [ 'Bold', 'Italic', 'Underline', 'Strike', 'Subscript', 'Superscript', '-', 'RemoveFormat' ] ],
								[ 'name' => 'paragraph', 'items' => [ 'NumberedList', 'BulletedList', '-', 'Outdent', 'Indent', '-', 'Blockquote', 'CreateDiv', '-', 'JustifyLeft', 'JustifyCenter', 'JustifyRight', 'JustifyBlock'] ],
								[ 'name' => 'links', 'items' => [ 'Link', 'Unlink', 'Anchor' ] ],
								[ 'name' => 'insert', 'items' => [ 'Image', 'Table', 'HorizontalRule', 'Smiley', 'SpecialChar', /* 'PageBreak', 'Iframe' */ ] ],
								'/',
								[ 'name' => 'styles', 'items' => [ /* 'Styles', 'Format', */ 'Font', 'FontSize' ] ],
								[ 'name' => 'colors', 'items' => [ 'TextColor', 'BGColor' ] ],
								[ 'name' => 'tools', 'items' => [ 'Maximize'/* , 'ShowBlocks'  */] ],
								[ 'name' => 'about', 'items' => [ 'About' ] ]
						],
				]
    	];
	}

}
?>
