<?php
return [
	    'components' => [
		        'request' => [
			            // !!! insert a secret key in the following (if it is empty) - this is required by cookie validation
			            'cookieValidationKey' => 'U9gouIML06Q3jyT8ID8jjI_O30vg8OhB',
		        ],
		        'urlManagerFrontend' => [
		        		'baseUrl' => ''
		        ],
	    ],
];
