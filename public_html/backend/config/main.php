<?php
$params = array_merge(
		require(__DIR__ . '/../../common/config/params.php'),
		require(__DIR__ . '/../../common/config/params-local.php'),
		require(__DIR__ . '/params.php'),
		require(__DIR__ . '/params-local.php')
);

return [
		'id' => 'manufactory-manager-backend',
		'name' => 'Insoglas - panel administracyjny',
		'basePath' => dirname(__DIR__),
		'controllerNamespace' => 'backend\controllers',
		'bootstrap' => ['log'],
		'components' => [		
				'request'=> [
						'class' => 'common\components\Request',
						'web'=> '/backend/web',
                        //'adminUrl' => '' //in case of problems with style URLs.
						'adminUrl' => '/admin',
				],
				'urlManager' => [
						'enablePrettyUrl' => true,
						'showScriptName' => false,
						'enableStrictParsing' => false,
						'rules' => [
								'/' => 'site/index',

								'<controller:[a-z\-]+>/view/<id:\d+>'=>'<controller>/view',
								'<controller:[a-z\-]+>/<action:[a-z\-]+>/<id:\d+>'=>'<controller>/<action>',
								'<controller:[a-z\-]+>/<action:[a-z\-]+>'=>'<controller>/<action>',
						]
				],
				'urlManagerFrontend' => [
						'class' => 'yii\web\UrlManager',
						'baseUrl' => '/',
						'enablePrettyUrl' => true,
						'showScriptName' => false,
						'enableStrictParsing' => false
				],
		        'user' => [
			            'identityClass' => 'common\models\ar\User',
						'loginUrl'		=> '@web/site/login',
                        'authTimeout' => 86400,
		        ],
                'session' => [
                    'class' => 'yii\web\Session',
                     'timeout' => 86400,
                ],
				'log' => [
						'traceLevel' => YII_DEBUG ? 3 : 0,
						'targets' => [
								[
										'class' => 'yii\log\FileTarget',
										'levels' => ['error', 'warning'],
										'logVars' => ['_GET','_POST'],
								],
								[
										'class' => 'yii\log\FileTarget',
										'levels' => ['info'],
										'logFile' => '@runtime/logs/info.log',
										'logVars' => [],
										'categories' => ['info']
								],
						],
				],
				'errorHandler' => [
						'errorAction' => 'site/error',
				]
		],
		/*'on beforeRequest' => function() { //Uncomment to enable support for maintenance mode in backend panel
			if(Yii::$app->params['maintenance']) {
				Yii::$app->catchAll = ['site/maintenance'];
			}
		},*/
		'params' => $params,
];
