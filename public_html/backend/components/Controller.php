<?php
namespace backend\components;

use Yii;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use common\bundles\EnhancedDialogAsset;
use common\helpers\Utility;
use common\models\ar\User;
use yii\db\Expression;

/**
 * Controller is the base class of backend controllers.
 */
class Controller extends \common\components\Controller {

	/**
	 * @inheritdoc
	 */
	public function behaviors() {
		return [
				'access' => [
						'class' => AccessControl::className(),
						'rules' => [
								[
										'roles' => [Utility::ROLE_ADMIN], 'allow' => true
								]
						],
				],
				'verbs' => [
						'class' => VerbFilter::className(),
						'actions' => [
								'delete' => ['post'],
						],
				],
		];
	}

	/**
	 * @inheritdoc
	 */
	public function actions() {
		return [
				'error' => [
						'class' => 'yii\web\ErrorAction',
				],
		];
	}
	
	public function beforeAction($action) {
        if(Yii::$app->getRequest()->serverName === "manman.pl") {
            Yii::$app->controller->enableCsrfValidation = false;
        }
        
        if(Yii::$app->params['maintenance']) {
		    if(!($action->controller->id == 'site' && $action->id == 'maintenance')) {
		        return $this->redirect(['/site/maintenance']);    
		    }
		} 

		if(parent::beforeAction($action)) {
			EnhancedDialogAsset::register($this->getView());					
			return true;
		}
		return false;
	}
}
