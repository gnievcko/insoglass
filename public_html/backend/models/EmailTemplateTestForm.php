<?php
namespace backend\models;

use Yii;
use yii\base\Model;
use common\components\MessageProvider;
use common\models\ar\EmailTemplate;
use common\components\EmailHelper;

class EmailTemplateTestForm extends Model {
	
	public $emailTypeId;
	public $languageId;
	public $email;
	
	public function __construct($emailTypeId, $languageId) {
		$this->emailTypeId = $emailTypeId;
		$this->languageId = $languageId;
	}
	
	public function rules() {
		return [
				[['emailTypeId', 'languageId', 'email'], 'required'],
				[['emailTypeId', 'languageId'], 'integer'],
				[['email'], 'email'],
		];
	}
	
	public function attributeLabels() {
		return [
				'email' => Yii::t('main', 'E-mail address'),
		];
	}
	
	public function sendMessage($controller) {
		$model = EmailTemplate::findOne($this->emailTypeId, $this->languageId);
		$type = $model->emailType;
		
		// TODO: Rozwiązać problem z tagami
		return EmailHelper::STATUS_OK === ($status = $controller->sendEmailTestMessage(empty($this->email) ? Yii::$app->params['adminEmail'] : $this->email, $model->subject, $model->content_html, $type, array_combine(explode(',', $type->tags), explode(',', $type->tags))));
	}
}
