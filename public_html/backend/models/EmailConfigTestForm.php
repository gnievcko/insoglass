<?php
namespace backend\models;

use Yii;
use yii\base\Model;
use common\components\MessageProvider;
use yii\base\Object;

class EmailConfigTestForm extends Model {
	
	public $emailConfigId;
	public $email;
	
	public function __construct($emailConfigId) {
		$this->emailConfigId = $emailConfigId;
	}
	
	public function rules() {
		return [
				[['emailConfigId', 'email'], 'required'],
				[['emailConfigId'], 'integer'],
				[['email'], 'email'],
		];
	}
	
	public function attributeLabels() {
		return [
				'email' => Yii::t('main', 'E-mail address'),
		];
	}
	
	public function sendMessage() {
		$messageProvider = new MessageProvider();
		return $messageProvider->sendStaticEmail(
				Yii::$app->params['adminEmail'], 
				\Yii::$app->name, 
				$this->email, 
				Yii::t('main', 'Test message for checking an e-mail configuration'), 
				Yii::t('main', 'Test message for checking an e-mail configuration'),
				true,
				$this->emailConfigId
		);
	}
}
