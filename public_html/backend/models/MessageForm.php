<?php
namespace backend\models;

use Yii;
use yii\base\Model;

/**
 * MessageForm is the data structure for keeping
 * user login form data. It is used by the 'login' action of 'NewsletterController'.
 */
class MessageForm extends Model {
	
	public $subject;
	
	public $contentEmail;
	
	public $contentSms;
	
	public $smsMode;
	
	public $testPhone;
	
	public $testEmail;

	/**
	 * @inheritdoc
	 */
	public function rules() {
		return [
				[['smsMode'], 'required'],
				[['testEmail'], 'email'],
				[['testPhone', 'subject', 'contentEmail', 'contentSms'], 'string'],
				[['testPhone'], 'match', 'pattern'=>'/^\+[0-9]+$/'],
		];
	}

	/**
	 * Declares attribute labels.
	 */
	public function attributeLabels() {
		return [
				'subject' => Yii::t('main', 'Subject'),
				'contentEmail' => Yii::t('main', 'Content'),
				'contentSms' => Yii::t('main', 'Content'),
				'smsMode' => Yii::t('main', 'SMS'),
				'testPhone' => Yii::t('main', 'Send to'),
				'testEmail' => Yii::t('main', 'Send to')
		];
	}
}
