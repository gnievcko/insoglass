var addNoty = function(text, type, layout, additional) {
	type = type || "information";
	layout = layout || "topRight";
	additional = additional || {};
	
	var params = {
			theme: "relax",
			text: text, 
			type: type,
			layout: "topRight",
			closeWith: ["click"],
			animation: {
				open: 'animated flipInX',
		        close: 'animated flipOutX',
		        easing: 'swing',
		        speed: 500
			}
		};
	
	$.extend(params, additional);
	
	noty(params);
};